<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="LTKTViewDetail.aspx.vb" Inherits="LTKTViewDetail"   %>



<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

	<script src="Script/popcalendar.js"></script>

	<script language="javascript" type="text/javascript">
	  function hidePanel(objhide,objpanel,imgmin,imgmax)
	  {
		document.getElementById(objhide).style.display='none';
		document.getElementById(objpanel).src=imgmax;
	  }
	  // JScript File

	  function popWin2(txtKelurahan, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerKelurahan.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
			paramId = myargs[0].toString();
			paramName = myargs[1].toString();

			txtKelurahan.value = paramName;
			hiddenField.value = paramId;
		}
	  }
	  
	  function popWinKecamatan(txtKecamatan, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerKecamatan.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
			paramId = myargs[0].toString();
			paramName = myargs[1].toString();

			txtKecamatan.value = paramName;
			hiddenField.value = paramId;
		}
	  }
	  function popWinNegara(txtNegara, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerNegara.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
			paramId = myargs[0].toString();
			paramName = myargs[1].toString();

			txtNegara.value = paramName;
			hiddenField.value = paramId;
		}
	  }
	  function popWinMataUang(txtMataUang, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerMataUang.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
			paramId = myargs[0].toString();
			paramName = myargs[1].toString();

			txtMataUang.value = paramId + ' - ' +paramName;
			hiddenField.value = paramId;
		}
	  }
	  function popWinProvinsi(txtProvinsi, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerProvinsi.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
			paramId = myargs[0].toString();
			paramName = myargs[1].toString();

			txtProvinsi.value = paramName;
			hiddenField.value = paramId;
		}
	  }
	  function popWinPekerjaan(txtPekerjaan, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerPekerjaan.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
			paramId = myargs[0].toString();
			paramName = myargs[1].toString();

			txtPekerjaan.value = paramName;
			hiddenField.value = paramId;
		}
	  }
	  function popWinKotaKab(txtKotaKab, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerKotaKab.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
			paramId = myargs[0].toString();
			paramName = myargs[1].toString();

			txtKotaKab.value = paramName;
			hiddenField.value = paramId;
		}
	  }
	  function popWinBidangUsaha(txtBidangUsaha, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerBidangUsaha.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
			paramId = myargs[0].toString();
			paramName = myargs[1].toString();

			txtBidangUsaha.value = paramName;
			hiddenField.value = paramId;
		}
	  }
	</script>

	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td>
				<img src="Images/blank.gif" width="5" height="1" /></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td>
							</td>
						<td width="99%" bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" /></td>
						<td bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" /></td>
					</tr>
				</table>
			</td>
			<td>
				<img src="Images/blank.gif" width="5" height="1" /></td>
		</tr>
		<tr>
			<td>
			</td>
			<td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
				<div id="divcontent" >
					<table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td bgcolor="#ffffff" class="divcontentinside" colspan="2">
								<img src="Images/blank.gif" width="20" height="100%" /><ajax:AjaxPanel ID="a" runat="server">
								</ajax:AjaxPanel>
								<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" border="0"
									style="border-top-style: none; border-right-style: none; border-left-style: none;
									border-bottom-style: none">
									<tr>
										<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
											border-right-style: none; border-left-style: none; border-bottom-style: none">
											<img src="Images/dot_title.gif" width="17" height="17">
											<strong>
												<asp:Label ID="Label1" runat="server" Text="LTKT - View Detail"></asp:Label></strong>
											<hr />
										</td>
									</tr>
									<tr>
										<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
											border-right-style: none; border-left-style: none; border-bottom-style: none">
											<ajax:AjaxPanel ID="AjaxPanel1" runat="server">
												<asp:Label ID="LblSucces" CssClass="validationok" Width="94%" runat="server" Visible="False"></asp:Label>
											</ajax:AjaxPanel>
										</td>
									</tr>
								</table>
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											&nbsp;</td>
									</tr>
								</table>
								<asp:MultiView runat="server" ID="mtvPage" ActiveViewIndex="0">
									<asp:View runat="server" ID="vwTransaksi">
										<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" border="0"
											bgcolor="#dddddd">
											<tr>
												<td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
													style="height: 6px">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td class="formtext">
																<asp:Label ID="Label8" runat="server" Text="A. UMUM" Font-Bold="True"></asp:Label>&nbsp;</td>
															<td>
																<a href="#" onclick="javascript:ShowHidePanel('Umum','searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
																	title="click to minimize or maximize">
																	<img id="searchimage2" src="Images/search-bar-minimize.gif" border="0" height="12px"
																		width="12px"></a></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr id="Umum">
												<td bgcolor="#ffffff" colspan="2" style="height: 10px">
													<table>
														<tr>
															<td class="formtext">
																<asp:Label ID="Label4" runat="server" Text="1. PIHAK PELAPOR" Font-Bold="True"></asp:Label>&nbsp;
															</td>
														</tr>
														<tr>
															<td class="formtext">
																<table>
																	<tr>
																		<td style="width: 5px;">
																		</td>
																		<td class="formText">
																			1.1. Nama PJK Pelapor <span style="color: #cc0000">*</span>
																		</td>
																		<td style="width: 5px;">
																			:
																		</td>
																		<td class="formtext">
																			<asp:Label runat="server" ID="txtUmumPJKPelapor" Width="240px"></asp:Label>
																			&nbsp;
																		</td>
																	</tr>
																	<tr>
																		<td style="width: 5px;">
																		</td>
																		<td class="formText">
																			1.2. Tanggal Pelaporan <span style="color: #cc0000">*</span>
																		</td>
																		<td style="width: 5px;">
																			:
																		</td>
																		<td class="formtext">
																			<asp:Label ID="txtTglLaporan" runat="server" MaxLength="50" Width="96px"></asp:Label>
																		</td>
																	</tr>
																	<tr>
																		<td style="width: 5px;">
																		</td>
																		<td class="formText">
																			1.3. Nama Pejabat PJK Pelapor <span style="color: #cc0000">*</span>
																		</td>
																		<td style="width: 5px;">
																			:
																		</td>
																		<td class="formtext">
																			<asp:Label runat="server" ID="txtPejabatPelapor" Width="240px"></asp:Label>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class="formtext">
																<asp:Label ID="Label5" runat="server" Text="2. JENIS LAPORAN (PILIH SALAH SATU)"
																	Font-Bold="True"></asp:Label>&nbsp;
															</td>
														</tr>
														<tr>
															<td class="formtext">
																<ajax:AjaxPanel runat="server" ID="pnlUpdate">
																	<table>
																		<tr>
																			<td style="width: 5px;">
																			</td>
																			<td class="formText">
																				2.1. Tipe Laporan <span style="color: #cc0000">*</span>
																			</td>
																			<td style="width: 5px;">
																				:
																			</td>
																			<td class="formtext">
																				<asp:DropDownList runat="server" ID="cboTipeLaporan" CssClass="combobox" AutoPostBack="True"
																					Enabled="False">
																					<asp:ListItem Value="0">Laporan Baru</asp:ListItem>
																					<asp:ListItem Value="1">Laporan Koreksi</asp:ListItem>
																				</asp:DropDownList>
																			</td>
																		</tr>
																		<tr>
																			<td style="width: 5px;">
																			</td>
																			<td class="formtext">
																			</td>
																			<td style="width: 5px;">
																				&nbsp;</td>
																			<td class="formtext">
																				<table id="tblLTKTKoreksi" runat="server" visible="False">
																					<tr>
																						<td class="formText">
																							2.1.1. No. LTKT yang dikoreksi <span style="color: #cc0000">*</span>
																						</td>
																						<td style="width: 5px;">
																							:
																						</td>
																						<td class="formtext">
																							<asp:Label runat="server" ID="txtNoLTKTKoreksi"></asp:Label>
																							&nbsp;
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</ajax:AjaxPanel>
															</td>
														</tr>
													</table>
													
												</td>
											</tr>
											<tr>
												<td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
													style="height: 6px">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td class="formtext">
																<asp:Label ID="Label2" runat="server" Text="B. IDENTITAS TERLAPOR" Font-Bold="True"></asp:Label>&nbsp;</td>
															<td>
																<a href="#" onclick="javascript:ShowHidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
																	title="click to minimize or maximize">
																	<img id="Img1" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px"></a></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr id="IdTerlapor">
												<td bgcolor="#ffffff" colspan="2">
													<ajax:AjaxPanel runat="server" ID="ajxPnlTerlapor" Width="100%">
														<table width="100%">
															<tbody>
																<tr>
																	<td class="formtext">
																		<asp:Label ID="Label6" runat="server" Text="3. TERLAPOR" Font-Bold="True"></asp:Label>&nbsp;
																	</td>
																</tr>
																<tr>
																	<td class="formtext">
																		<table>
																			<tbody>
																				<tr>
																					<td style="width: 5px">
																					</td>
																					<td class="formtext">
																						<strong>3.1. Kepemilikan</strong> <span style="color: #cc0000">*</span>
																					</td>
																					<td style="width: 5px">
																						:
																					</td>
																					<td class="formtext">
																						<asp:DropDownList ID="cboTerlaporKepemilikan" runat="server" CssClass="combobox"
																							Enabled="False">
																						</asp:DropDownList></td>
																				</tr>
																				<tr>
																					<td style="width: 5px">
																					</td>
																					<td class="formtext">
																					</td>
																					<td style="width: 5px">
																					</td>
																					<td class="formtext">
																						<table width="100%">
																							<tbody>
																								<tr>
																									<td style="width: 139px; height: 28px" class="formText">
																										<strong>3.1.1. No. Rekening</strong><span style="color: #cc0000">*</span>&nbsp;</td>
																									<td style="width: 5px; height: 28px">
																										:
																									</td>
																									<td style="height: 28px" class="formtext">
																										<asp:Label ID="txtTerlaporNoRekening" runat="server" Width="258px"></asp:Label>
																									</td>
																								</tr>
																								<tr>
																									<td style="width: 139px" class="formText">
																										Tipe Pelapor</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:RadioButtonList ID="rblTerlaporTipePelapor" runat="server" Enabled="False" AutoPostBack="True"
																											RepeatDirection="Horizontal">
																											<asp:ListItem Value="1">Perorangan</asp:ListItem>
																											<asp:ListItem Value="2">Korporasi</asp:ListItem>
																										</asp:RadioButtonList>
																									</td>
																								</tr>
																								<tr>
																									<td class="formText">
																										CIF No.</td>
																									<td class="formText">
																										:</td>
																									<td class="formText">
																										<asp:Label ID="txtCIFNo" runat="server"></asp:Label></td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<div id="divPerorangan" runat="server" hidden="false">
																											<table>
																												<tbody>
																													<tr>
																														<td class="formtext" colspan="3">
																															<strong>3.1.2. Perorangan</strong>
																														</td>
																													</tr>
																													<tr>
																														<td class="formText">
																															a. Gelar</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTerlaporGelar" runat="server"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formText">
																															b. Nama Lengkap <span style="color: #cc0000">*</span></td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTerlaporNamaLengkap" runat="server" Width="370px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formText">
																															c. Tempat Lahir</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTerlaporTempatLahir" runat="server" Width="240px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formText">
																															d. Tanggal Lahir <span style="color: #cc0000">*</span></td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<br />
																															<asp:Label ID="txtTerlaporTglLahir" runat="server" Width="96px" MaxLength="50"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formText">
																															e. Kewarganegaraan (Pilih salah satu) <span style="color: #cc0000">*</span></td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:RadioButtonList ID="rblTerlaporKewarganegaraan" runat="server" Enabled="False"
																																AutoPostBack="True" RepeatDirection="Horizontal">
																																<asp:ListItem Selected="True" Value="1">WNI</asp:ListItem>
																																<asp:ListItem Value="2">WNA</asp:ListItem>
																															</asp:RadioButtonList></td>
																													</tr>
																													<tr>
																														<td class="formText">
																															f. Negara <span style="color: #cc0000">*</span></td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:DropDownList ID="cboTerlaporNegara" runat="server" CssClass="combobox" Enabled="False">
																															</asp:DropDownList></td>
																													</tr>
																													<tr>
																														<td class="formText">
																															g. Alamat Domisili</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formText">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTerlaporDOMNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			RT/RW</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTerlaporDOMRTRW" runat="server"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			Kelurahan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporDOMKelurahan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporDOMKelurahan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			Kecamatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			&nbsp;<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporDOMKecamatan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporDOMKecamatan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			Kota / Kabupaten<span style="color: #cc0000">*</span></td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporDOMKotaKab" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporDOMKotaKab" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporDOMProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporDOMProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
                                                                                                                                    <tr>
                                                                                                                                        <td class="formtext">
                                                                                                                                            &nbsp;</td>
                                                                                                                                        <td class="formText">
                                                                                                                                            Negara</td>
                                                                                                                                        <td style="width: 5px">
                                                                                                                                            :</td>
                                                                                                                                        <td class="formtext">
                                                                                                                                            <table style="width: 127px">
                                                                                                                                                <tbody>
                                                                                                                                                    <tr>
                                                                                                                                                        <td>
                                                                                                                                                            <asp:Label ID="txtTerlaporDOMNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                                        </td>
                                                                                                                                                        <td>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                            <asp:HiddenField ID="hfTerlaporDOMNegara" runat="server" />
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTerlaporDOMKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formText">
																															h. Alamat Sesuai Bukti Identitas</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formText">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formText">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTerlaporIDNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formText">
																																		</td>
																																		<td class="formText">
																																			RT/RW</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTerlaporIDRTRW" runat="server"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formText">
																																		</td>
																																		<td class="formText">
																																			Kelurahan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporIDKelurahan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporIDKelurahan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formText">
																																		</td>
																																		<td class="formText">
																																			Kecamatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			&nbsp;<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporIDKecamatan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporIDKecamatan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formText">
																																		</td>
																																		<td class="formText">
																																			Kota / Kabupaten<span style="color: #cc0000">*</span></td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporIDKotaKab" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporIDKotaKab" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																
																																	<tr>
																																		<td class="formText">
																																		</td>
																																		<td class="formText">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td style="height: 22px">
																																							<asp:Label ID="txtTerlaporIDProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td style="height: 22px">
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporIDProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
                                                                                                                                    	<tr>
                                                                                                                                            <td class="formText">
                                                                                                                                                &nbsp;</td>
                                                                                                                                            <td class="formText">
                                                                                                                                                Negara<br />
                                                                                                                                            </td>
                                                                                                                                            <td style="width: 5px">
                                                                                                                                                :</td>
                                                                                                                                            <td class="formtext">
                                                                                                                                                <table style="width: 127px">
                                                                                                                                                    <tbody>
                                                                                                                                                        <tr>
                                                                                                                                                            <td style="height: 22px">
                                                                                                                                                                <asp:Label ID="txtTerlaporIDNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                                            </td>
                                                                                                                                                            <td style="height: 22px">
                                                                                                                                                            </td>
                                                                                                                                                        </tr>
                                                                                                                                                    </tbody>
                                                                                                                                                </table>
                                                                                                                                                <asp:HiddenField ID="hfTerlaporIDNegara" runat="server" />
                                                                                                                                            </td>
                                                                                                                                    </tr>
                                                                                                                                    	<tr>
																																		<td class="formText">
																																		</td>
																																		<td class="formText">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTerlaporIDKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formText">
																															i. Alamat Sesuai Negara Asal</td>
																														<td style="width: 5px">
																														</td>
																														<td>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formText">
																																		</td>
																																		<td class="formText">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTerlaporNANamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formText">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formText">
																																			Negara</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporNANegara" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporIDNANegara" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formText">
																																		</td>
																																		<td class="formText">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporNAProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporNAProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formText">
																																		</td>
																																		<td class="formText">
																																			Kota
																																		</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporNAKota" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporIDKota" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formText">
																																		</td>
																																		<td class="formText">
																																			Kode Pos<span style="color: #cc0000">*</span></td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTerlaporNAKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formText">
																															j. Jenis Dokumen Identitas<span style="color: #cc0000">*</span></td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<ajax:AjaxPanel ID="ajPnlDokId" runat="server">
																																<asp:DropDownList runat="server" ID="cboTerlaporJenisDocID" CssClass="combobox" Enabled="False">
																																</asp:DropDownList>
																															</ajax:AjaxPanel>
																														</td>
																													</tr>
																													<tr>
																														<td id="tdNomorId" class="formtext" colspan="3" runat="server">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formText">
																																		  Nomor Identitas <span style="color: #cc0000">*</span></td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="txtTerlaporNomorID" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formText">
																															k. Nomor Pokok Wajib Pajak (NPWP)</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTerlaporNPWP" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formText">
																															l. Pekerjaan</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formText">
																																			Pekerjaan<span style="color: #cc0000">*</span></td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="txtTerlaporPekerjaan" runat="server" Width="257px"></asp:Label>
																																			<asp:HiddenField ID="hfTerlaporPekerjaan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			Jabatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="txtTerlaporJabatan" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			Penghasilan rata-rata/th (Rp)</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="txtTerlaporPenghasilanRataRata" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			Tempat kerja</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="txtTerlaporTempatKerja" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																										</div>
																										<div id="divKorporasi" runat="server" hidden="true">
																											<table>
																												<tbody>
																													<tr>
																														<td class="formtext" colspan="3">
																															<strong>3.1.3. Korporasi</strong></td>
																													</tr>
																													<tr>
																														<td class="formText">
																															a. Bentuk Badan Usaha<span style="color: #cc0000">*</span></td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:DropDownList ID="cboTerlaporCORPBentukBadanUsaha" runat="server" Enabled="False">
																															</asp:DropDownList></td>
																													</tr>
																													<tr>
																														<td class="formText">
																															b. Nama Korporasi <span style="color: #cc0000">*</span></td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTerlaporCORPNama" runat="server" Width="370px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formText">
																															c. Bidang Usaha Korporasi<span style="color: #cc0000">*</span></td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<table style="width: 127px">
																																<tbody>
																																	<tr>
																																		<td>
																																			<asp:Label ID="txtTerlaporCORPBidangUsaha" runat="server" Width="167px"></asp:Label></td>
																																		<td>
																																		</td>
																																	</tr>
																																</tbody>
																															</table>
																															<asp:HiddenField ID="hfTerlaporCORPBidangUsaha" runat="server"></asp:HiddenField>
																														</td>
																													</tr>
																													<tr>
																														<td class="formText">
																															d. Alamat Korporasi<span style="color: #cc0000">*</span></td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:RadioButtonList ID="rblTerlaporCORPTipeAlamat" runat="server" Enabled="False"
																																AutoPostBack="True" RepeatDirection="Horizontal">
																																<asp:ListItem>Dalam Negeri</asp:ListItem>
																																<asp:ListItem>Luar Negeri</asp:ListItem>
																															</asp:RadioButtonList></td>
																													</tr>
																													<tr>
																														<td class="formText">
																															e. Alamat Lengkap Korporasi</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formText">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTerlaporCORPDLNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			RT/RW</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTerlaporCORPDLRTRW" runat="server"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			Kelurahan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporCORPDLKelurahan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporCORPDLKelurahan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			Kecamatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			&nbsp;<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporCORPDLKecamatan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporCORPDLKecamatan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			Kota / Kabupaten<span style="color: #cc0000">*</span></td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporCORPDLKotaKab" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporCORPDLKotaKab" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporCORPDLProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporCORPDLProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			Negara</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporCORPDLNegara" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporCORPDLNegara" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
                                                                                                                                    <tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formText">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTerlaporCORPDLKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formText">
																															i. Alamat Korporasi Luar Negeri</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td style="width: 69px" class="formText">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTerlaporCORPLNNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td style="width: 69px" class="formText">
																																			Negara</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporCORPLNNegara" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporCORPLNNegara" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td style="width: 69px" class="formText">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td style="height: 22px">
																																							<asp:Label ID="txtTerlaporCORPLNProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td style="height: 22px">
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporCORPLNProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td style="width: 69px" class="formText">
																																			Kota
																																		</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTerlaporCORPLNKota" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTerlaporCORPLNKota" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td style="width: 69px" class="formText">
																																			Kode Pos<span style="color: #cc0000">*</span></td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTerlaporCORPLNKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formText">
																															k. Nomor Pokok Wajib Pajak (NPWP)</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTerlaporCORPNPWP" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																												</tbody>
																											</table>
																										</div>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</td>
																</tr>
															</tbody>
														</table>
													</ajax:AjaxPanel>
												</td>
											</tr>
											<tr>
												<td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
													style="height: 6px">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td class="formtext">
																<asp:Label ID="Label3" runat="server" Text="C. TRANSAKSI" Font-Bold="True"></asp:Label>&nbsp;</td>
															<td>
																<a href="#" onclick="javascript:ShowHidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
																	title="click to minimize or maximize">
																	<img id="Img2" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px"></a></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr id="Transaksi">
												<td bgcolor="#ffffff" colspan="2">
													<ajax:AjaxPanel ID="AjaxPanel4" runat="server" Width="100%">
														<table>
															<tbody>
																<tr>
																	<td>
																		<asp:Menu ID="Menu1" runat="server" CssClass="tabs" StaticEnableDefaultPopOutImage="False"
																			Orientation="Horizontal">
																			<Items>
																				<asp:MenuItem Text="Kas Masuk" Value="0" Selected="True"></asp:MenuItem>
																				<asp:MenuItem Text="Kas Keluar" Value="1"></asp:MenuItem>
																			</Items>
																			<StaticSelectedStyle CssClass="selectedTab" />
																			<StaticMenuItemStyle CssClass="tab" />
																		</asp:Menu>
																		<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
																			<asp:View ID="ViewKasMasuk" runat="server">
																				<table width="100%">
																					<tbody>
																						<tr>
																							<td style="width: 300px" class="formtext">
																								4.1. Tanggal Transaksi (tgl/bln/thn)<span style="color: #cc0000">*</span></td>
																							<td style="width: 2px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKMTanggalTrx" runat="server" Width="96px" MaxLength="50"></asp:Label>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								4.2. Nama Kantor PJK tempat terjadinya transaksi</td>
																							<td style="width: 2px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<div>
																									<table>
																										<tbody>
																											<tr>
																												<td class="formtext">
																													&nbsp; &nbsp;&nbsp;
																												</td>
																												<td class="formtext">
																													a. Nama Kantor<span style="color: #cc0000">*</span></td>
																												<td style="width: 5px">
																													:</td>
																												<td class="formtext">
																													<asp:Label ID="txtTRXKMNamaKantor" runat="server" Width="370px"></asp:Label>
																													&nbsp;</td>
																											</tr>
																											<tr>
																												<td class="formtext">
																												</td>
																												<td class="formtext">
																													b. Kota Kabupaten<span style="color: #cc0000">*</span></td>
																												<td style="width: 5px">
																													:</td>
																												<td class="formtext">
																													<table style="width: 127px">
																														<tbody>
																															<tr>
																																<td>
																																	<asp:Label ID="txtTRXKMKotaKab" runat="server" Width="167px"></asp:Label></td>
																																<td>
																																</td>
																															</tr>
																														</tbody>
																													</table>
																													<asp:HiddenField ID="hfTRXKMKotaKab" runat="server"></asp:HiddenField>
																													&nbsp;
																												</td>
																											</tr>
																											<tr>
																												<td class="formtext">
																												</td>
																												<td class="formtext">
																													c. Provinsi<span style="color: #cc0000">*</span></td>
																												<td style="width: 5px">
																													:</td>
																												<td class="formtext">
																													<table style="width: 251px">
																														<tbody>
																															<tr>
																																<td style="width: 170px; height: 15px">
																																	<asp:Label ID="txtTRXKMProvinsi" runat="server" Width="167px"></asp:Label></td>
																															</tr>
																														</tbody>
																													</table>
																													<asp:HiddenField ID="hfTRXKMProvinsi" runat="server"></asp:HiddenField>
																												</td>
																											</tr>
																										</tbody>
																									</table>
																								</div>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								4.3. Detail Kas Masuk</td>
																							<td style="width: 2px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												a. Kas Masuk (Rp)</td>
																											<td style="width: 2px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMDetilKasMasuk" runat="server" Width="167px" AutoPostBack="True"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												b. Kas Masuk Dalam valuta asing (Dapat &gt; 1)</td>
																											<td style="width: 2px">
																											</td>
																											<td class="formtext">
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext" colspan="1">
																											</td>
																											<td class="formtext" colspan="3">
																												<table>
																													<tbody>
																														<tr>
																															<td class="formtext">
																																&nbsp; &nbsp;&nbsp;
																															</td>
																															<td class="formtext">
																																i. Mata Uang</td>
																															<td style="width: 2px">
																																:</td>
																															<td class="formtext">
																																<table style="width: 127px">
																																	<tbody>
																																		<tr>
																																			<td>
																																				<asp:Label ID="txtTRXKMDetilMataUang" runat="server" Width="167px"></asp:Label></td>
																																			<td>
																																			</td>
																																		</tr>
																																	</tbody>
																																</table>
																																<asp:HiddenField ID="hfTRXKMDetilMataUang" runat="server"></asp:HiddenField>
																															</td>
																														</tr>
																														<tr>
																															<td class="formtext">
																															</td>
																															<td class="formtext">
																																ii. Kurs Transaksi</td>
																															<td style="width: 2px">
																																:</td>
																															<td class="formtext">
																																<asp:Label ID="txtTRXKMDetailKursTrx" runat="server" Width="167px"></asp:Label></td>
																														</tr>
																														<tr>
																															<td class="formtext">
																															</td>
																															<td class="formtext">
																																iii. Jumlah&nbsp;</td>
																															<td style="width: 2px">
																																:</td>
																															<td class="formtext">
																																<asp:Label ID="txtTRXKMDetilJumlah" runat="server" Width="167px"></asp:Label></td>
																														</tr>
																														<tr>
																															<td class="formtext">
																															</td>
																															<td class="formtext" colspan="3">
																															</td>
																														</tr>
																														<tr>
																															<td class="formtext">
																															</td>
																															<td class="formtext" colspan="3">
																																<asp:GridView ID="grvTRXKMDetilValutaAsing" runat="server" SkinID="grv2" 
																																	AutoGenerateColumns="False" AllowPaging="True">
																																	<Columns>
																																		<asp:BoundField HeaderText="No" />
																																		<asp:BoundField HeaderText="Mata Uang" DataField="MataUang" />
																																		<asp:BoundField HeaderText="Kurs Transaksi" DataField="KursTransaksi" />
																																		<asp:BoundField HeaderText="Jumlah" DataField="Jumlah" />
																																		<asp:BoundField DataField="JumlahRp" HeaderText="Jumlah (Rp)" ItemStyle-HorizontalAlign="Right" />
																																	</Columns>
																																</asp:GridView>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												c. Total Kas Masuk (a+b) (Rp)</td>
																											<td style="width: 2px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="lblTRXKMDetilValutaAsingJumlahRp" runat="server"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								4.4. Nomor Rekening Nasabah</td>
																							<td style="width: 2px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKMNoRekening" runat="server" Width="167px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td style="height: 15px" class="formtext">
																								4.5. Identitas pihak terkait dengan laporan</td>
																							<td style="width: 2px; height: 15px">
																							</td>
																							<td style="height: 15px" class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								&nbsp; &nbsp; &nbsp; Tipe Pelapor</td>
																							<td style="width: 2px">
																								:</td>
																							<td class="formtext">
																								<asp:RadioButtonList ID="rblTRXKMTipePelapor" runat="server" AutoPostBack="True"
																									RepeatDirection="Horizontal" Enabled="False">
																									<asp:ListItem Value="1">Perorangan</asp:ListItem>
																									<asp:ListItem Value="2">Korporasi</asp:ListItem>
																								</asp:RadioButtonList></td>
																						</tr>
																					</tbody>
																				</table>
																				<table id="tblTRXKMTipePelapor" runat="server">
																					<tbody>
																						<tr>
																							<td class="formtext" colspan="3">
																								<strong>4.5.1. Perorangan</strong>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								a. Gelar</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKMINDVGelar" runat="server"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								b. Nama Lengkap
																							</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKMINDVNamaLengkap" runat="server" Width="370px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								c. Tempat Lahir</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKMINDVTempatLahir" runat="server" Width="240px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								d. Tanggal Lahir
																							</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<br />
																								<asp:Label ID="txtTRXKMINDVTanggalLahir" runat="server" Width="96px" MaxLength="50"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								e. Kewarganegaraan
																							</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:RadioButtonList ID="rblTRXKMINDVKewarganegaraan" runat="server" Enabled="False"
																									AutoPostBack="True" RepeatDirection="Horizontal">
																									<asp:ListItem Selected="True" Value="1">WNI</asp:ListItem>
																									<asp:ListItem Value="2">WNA</asp:ListItem>
																								</asp:RadioButtonList></td>
																						</tr>
																						<tr style="color: #cc0000">
																							<td class="formtext">
																								f. Negara
																							</td>
																							<td style="width: 5px; color: #000000">
																								:</td>
																							<td class="formtext">
																								<asp:DropDownList ID="cboTRXKMINDVNegara" runat="server" CssClass="combobox" Enabled="False">
																								</asp:DropDownList></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								g. Alamat Domisili</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												Nama Jalan</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMINDVDOMNamaJalan" runat="server" Width="370px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												RT/RW</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMINDVDOMRTRW" runat="server"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kelurahan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext" valign="top">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKMINDVDOMKelurahan" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMINDVDOMKelurahan" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kecamatan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												&nbsp;<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKMINDVDOMKecamatan" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMINDVDOMKecamatan" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kota / Kabupaten</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKMINDVDOMKotaKab" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMINDVDOMKotaKab" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																									
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Provinsi</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKMINDVDOMProvinsi" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMINDVDOMProvinsi" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
                                                                                                        	<tr>
                                                                                                                <td class="formtext">
                                                                                                                    &nbsp;</td>
                                                                                                                <td class="formtext">
                                                                                                                    Negara</td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :</td>
                                                                                                                <td class="formtext">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:Label ID="txtTRXKMINDVDOMNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfTRXKMINDVDOMNegara" runat="server" />
                                                                                                                </td>
                                                                                                        </tr>
                                                                                                        	<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kode Pos</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMINDVDOMKodePos" runat="server"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								h. Alamat Sesuai Bukti Identitas</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												Nama Jalan</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMINDVIDNamaJalan" runat="server" Width="370px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												RT/RW</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMINDVIDRTRW" runat="server"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kelurahan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext" valign="top">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKMINDVIDKelurahan" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMINDVIDKelurahan" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kecamatan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												&nbsp;<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td style="height: 23px">
																																<asp:Label ID="txtTRXKMINDVIDKecamatan" runat="server" Width="167px"></asp:Label></td>
																															<td style="height: 23px">
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMINDVIDKecamatan" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kota / Kabupaten</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKMINDVIDKotaKab" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMINDVIDKotaKab" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																							
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Provinsi</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKMINDVIDProvinsi" runat="server" Width="167px"></asp:Label></td>
																															<td>
																																&nbsp;
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMINDVIDProvinsi" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
                                                                                                        			<tr>
                                                                                                                        <td class="formtext">
                                                                                                                            &nbsp;</td>
                                                                                                                        <td class="formtext">
                                                                                                                            Negara</td>
                                                                                                                        <td style="width: 5px">
                                                                                                                            :</td>
                                                                                                                        <td class="formtext">
                                                                                                                            <table style="width: 127px">
                                                                                                                                <tbody>
                                                                                                                                    <tr>
                                                                                                                                        <td>
                                                                                                                                            <asp:Label ID="txtTRXKMINDVIDNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                        </td>
                                                                                                                                        <td>
                                                                                                                                            &nbsp;
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </tbody>
                                                                                                                            </table>
                                                                                                                            <asp:HiddenField ID="hfTRXKMINDVIDNegara" runat="server" />
                                                                                                                        </td>
                                                                                                        </tr>
                                                                                                        			<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kode Pos</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMINDVIDKodePos" runat="server"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								i. Alamat Sesuai Negara Asal</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												Nama Jalan</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMINDVNANamaJalan" runat="server" Width="370px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Negara</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td style="height: 22px">
																																<asp:Label ID="txtTRXKMINDVNANegara" runat="server" Width="167px"></asp:Label></td>
																															<td style="height: 22px">
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMINDVNANegara" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Provinsi</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext" valign="top">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKMINDVNAProvinsi" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMINDVNAProvinsi" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kota
																											</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKMINDVNAKota" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMINDVNAKota" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kode Pos</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMINDVNAKodePos" runat="server"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								j. Jenis Dokumen Identitas</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<ajax:AjaxPanel ID="AjaxPanel2" runat="server">
																									<asp:DropDownList runat="server" ID="cboTRXKMINDVJenisID" CssClass="combobox" Enabled="False">
																									</asp:DropDownList>
																								</ajax:AjaxPanel>
																							</td>
																						</tr>
																						<tr>
																							<td id="Td1" class="formtext" colspan="3" runat="server">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																											  Nomor Identitas <span style="color: #cc0000">*</span></td>
																											<td style="width: 5px">
																												:</td>
																											<td style="width: 260px" class="formtext">
																												<asp:Label ID="txtTRXKMINDVNomorID" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								k. Nomor Pokok Wajib Pajak (NPWP)</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKMINDVNPWP" runat="server" Width="257px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								l. Pekerjaan</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td style="height: 40px" class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td style="height: 40px" class="formtext">
																												Pekerjaan</td>
																											<td style="width: 5px; height: 40px">
																												:</td>
																											<td style="width: 260px; height: 40px" class="formtext">
																												<asp:Label ID="txtTRXKMINDVPekerjaan" runat="server" Width="257px"></asp:Label>
																												<asp:HiddenField ID="hfTRXKMINDVPekerjaan" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Jabatan</td>
																											<td style="width: 5px">
																												:</td>
																											<td style="width: 260px" class="formtext">
																												<asp:Label ID="txtTRXKMINDVJabatan" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Penghasilan rata-rata/th (Rp)</td>
																											<td style="width: 5px">
																												:</td>
																											<td style="width: 260px" class="formtext">
																												<asp:Label ID="txtTRXKMINDVPenghasilanRataRata" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Tempat kerja</td>
																											<td style="width: 5px">
																												:</td>
																											<td style="width: 260px" class="formtext">
																												<asp:Label ID="txtTRXKMINDVTempatKerja" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								m. Tujuan Transaksi</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKMINDVTujuanTrx" runat="server" Width="257px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								n. Sumber Dana</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKMINDVSumberDana" runat="server" Width="257px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												ii. Nama Bank Lain</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMINDVNamaBankLain" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												ii. Nomor Rekening Tujuan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMINDVNoRekeningTujuan" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																					</tbody>
																				</table>
																				<table id="tblTRXKMTipePelaporKorporasi" runat="server">
																					<tbody>
																						<tr>
																							<td class="formtext" colspan="3">
																								<strong>4.5.2. Korporasi</strong></td>
																						</tr>
																						<tr>
																							<td style="height: 27px" class="formtext">
																								a. Bentuk Badan Usaha</td>
																							<td style="width: 5px; height: 27px">
																								:</td>
																							<td style="height: 27px" class="formtext">
																								&nbsp; &nbsp;
																								<asp:DropDownList ID="cboTRXKMCORPBentukBadanUsaha" runat="server" Enabled="False">
																								</asp:DropDownList></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								b. Nama Korporasi
																							</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKMCORPNama" runat="server" Width="370px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								c. Bidang Usaha Korporasi</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<table style="width: 127px">
																									<tbody>
																										<tr>
																											<td>
																												<asp:Label ID="txtTRXKMCORPBidangUsaha" runat="server" Width="167px"></asp:Label></td>
																											<td>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																								<asp:HiddenField ID="hfTRXKMCORPBidangUsaha" runat="server"></asp:HiddenField>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								d. Alamat Korporasi</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:RadioButtonList ID="rblTRXKMCORPTipeAlamat" runat="server" Enabled="False" AutoPostBack="True"
																									RepeatDirection="Horizontal">
																									<asp:ListItem Selected="True">Dalam Negeri</asp:ListItem>
																									<asp:ListItem>Luar Negeri</asp:ListItem>
																								</asp:RadioButtonList></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								e. Alamat Lengkap Korporasi</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												Nama Jalan</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMCORPDLNamaJalan" runat="server" Width="370px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												RT/RW</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMCORPDLRTRW" runat="server"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kelurahan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext" valign="top">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td style="height: 22px">
																																<asp:Label ID="txtTRXKMCORPDLKelurahan" runat="server" Width="167px"></asp:Label></td>
																															<td style="height: 22px">
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMCORPDLKelurahan" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kecamatan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												&nbsp;<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKMCORPDLKecamatan" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMCORPDLKecamatan" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kota / Kabupaten</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKMCORPDLKotaKab" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMCORPDLKotaKab" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																									
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Provinsi</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td style="height: 22px">
																																<asp:Label ID="txtTRXKMCORPDLProvinsi" runat="server" Width="167px"></asp:Label></td>
																															<td style="height: 22px">
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMCORPDLProvinsi" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Negara</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKMCORPDLNegara" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMCORPDLNegara" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
                                                                                                        	<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kode Pos</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMCORPDLKodePos" runat="server"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								i. Alamat Korporasi Luar Negeri</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												Nama Jalan</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMCORPLNNamaJalan" runat="server" Width="370px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Negara</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKMCORPLNNegara" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMCORPLNNegara" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Provinsi</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext" valign="top">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKMCORPLNProvinsi" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMCORPLNProvinsi" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kota
																											</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKMCORPLNKota" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKMCORPLNKota" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kode Pos</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMCORPLNKodePos" runat="server"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								k. Nomor Pokok Wajib Pajak (NPWP)</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKMCORPNPWP" runat="server" Width="257px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								l. Tujuan Transaksi</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKMCORPTujuanTrx" runat="server" Width="257px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								m. Sumber Dana</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKMCORPSumberDana" runat="server" Width="257px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												ii. Nama Bank Lain</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMCORPNamaBankLain" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												ii. Nomor Rekening Tujuan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKMCORPNoRekeningTujuan" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																					</tbody>
																				</table>
																			</asp:View>
																			<asp:View ID="ViewKasKeluar" runat="server">
																				<table width="100%">
																					<tbody>
																						<tr>
																							<td style="width: 300px" class="formtext">
																								5.1. Tanggal Transaksi (tgl/bln/thn)<span style="color: #cc0000">*</span></td>
																							<td style="width: 2px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKKTanggalTransaksi" runat="server" Width="96px" MaxLength="50"></asp:Label>
																							</td>
																						</tr>
																						<tr>
																							<td style="height: 27px" class="formtext">
																								5.2. Nama Kantor PJK tempat terjadinya transaksi</td>
																							<td style="width: 2px; height: 27px">
																							</td>
																							<td style="height: 27px" class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<div>
																									<table>
																										<tbody>
																											<tr>
																												<td class="formtext">
																													&nbsp; &nbsp;&nbsp;
																												</td>
																												<td class="formtext">
																													a. Nama Kantor<span style="color: #cc0000">*</span></td>
																												<td style="width: 5px">
																													:</td>
																												<td class="formtext">
																													<asp:Label ID="txtTRXKKNamaKantor" runat="server" Width="370px"></asp:Label>
																													&nbsp;</td>
																											</tr>
																											<tr>
																												<td class="formtext">
																												</td>
																												<td class="formtext">
																													b. Kota Kabupaten<span style="color: #cc0000">*</span></td>
																												<td style="width: 5px">
																													:</td>
																												<td class="formtext">
																													<table style="width: 127px">
																														<tbody>
																															<tr>
																																<td>
																																	<asp:Label ID="txtTRXKKKotaKab" runat="server" Width="167px"></asp:Label></td>
																																<td>
																																</td>
																															</tr>
																														</tbody>
																													</table>
																													<asp:HiddenField ID="hfTRXKKKotaKab" runat="server"></asp:HiddenField>
																													&nbsp;
																												</td>
																											</tr>
																											<tr>
																												<td class="formtext">
																												</td>
																												<td class="formtext">
																													c. Provinsi<span style="color: #cc0000">*</span></td>
																												<td style="width: 5px">
																													:</td>
																												<td class="formtext">
																													<table style="width: 127px">
																														<tbody>
																															<tr>
																																<td>
																																	<asp:Label ID="txtTRXKKProvinsi" runat="server" Width="167px"></asp:Label></td>
																															</tr>
																														</tbody>
																													</table>
																													<asp:HiddenField ID="hfTRXKKProvinsi" runat="server"></asp:HiddenField>
																													&nbsp;
																												</td>
																											</tr>
																										</tbody>
																									</table>
																								</div>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								5.3. Detail Kas Keluar</td>
																							<td style="width: 2px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												a. Kas Keluar (Rp)</td>
																											<td style="width: 2px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKDetailKasKeluar" runat="server" Width="167px" AutoPostBack="True"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																												b. Kas Keluar Dalam valuta asing (Dapat &gt; 1)</td>
																											<td style="width: 2px">
																											</td>
																											<td class="formtext">
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext" colspan="3">
																												<table>
																													<tbody>
																														<tr>
																															<td class="formtext">
																																&nbsp; &nbsp;&nbsp;
																															</td>
																															<td class="formtext">
																																i. Mata Uang</td>
																															<td style="width: 2px">
																																:</td>
																															<td class="formtext">
																																<table style="width: 127px">
																																	<tbody>
																																		<tr>
																																			<td>
																																				<asp:Label ID="txtTRXKKDetilMataUang" runat="server" Width="167px"></asp:Label></td>
																																			<td>
																																			</td>
																																		</tr>
																																	</tbody>
																																</table>
																																<asp:HiddenField ID="hfTRXKKDetilMataUang" runat="server"></asp:HiddenField>
																															</td>
																														</tr>
																														<tr>
																															<td class="formtext">
																															</td>
																															<td class="formtext">
																																ii. Kurs Transaksi</td>
																															<td style="width: 2px">
																																:</td>
																															<td class="formtext">
																																<asp:Label ID="txtTRXKKDetilKursTrx" runat="server" Width="167px"></asp:Label></td>
																														</tr>
																														<tr>
																															<td class="formtext">
																															</td>
																															<td class="formtext">
																																iii. Jumlah&nbsp;</td>
																															<td style="width: 2px">
																																:</td>
																															<td class="formtext">
																																<asp:Label ID="txtTRXKKDetilJumlah" runat="server" Width="167px"></asp:Label></td>
																														</tr>
																														<tr>
																															<td class="formtext">
																															</td>
																															<td class="formtext" colspan="3">
																															</td>
																														</tr>
																														<tr>
																															<td class="formtext">
																															</td>
																															<td class="formtext" colspan="3">
																																<asp:GridView ID="grvDetilKasKeluar" runat="server" Width="290px" SkinID="grv2"
																																	AutoGenerateColumns="False">
																																	<Columns>
																																		<asp:BoundField HeaderText="No" />
																																		<asp:BoundField HeaderText="Mata Uang" DataField="MataUang" />
																																		<asp:BoundField HeaderText="Kurs Transaksi" DataField="KursTransaksi" />
																																		<asp:BoundField HeaderText="Jumlah" DataField="Jumlah" />
																																		<asp:BoundField DataField="JumlahRp" HeaderText="Jumlah (Rp)" ItemStyle-HorizontalAlign="Right" />
																																	</Columns>
																																</asp:GridView>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																												c. Total Kas Keluar (a+b) (Rp)</td>
																											<td style="width: 2px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="lblDetilKasKeluarJumlahRp" runat="server"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
                                                                                        <tr>
                                                                                            <td class="formtext">
                                                                                                5.4 Nomor Rekening Nasabah</td>
                                                                                            <td style="width: 2px">
                                                                                                :</td>
                                                                                            <td class="formtext">
                                                                                                <asp:Label ID="txtTRXKKRekeningKK" runat="server"></asp:Label></td>
                                                                                        </tr>
																						<tr>
																							<td class="formtext">
																								5.5. Identitas pihak terkait dengan laporan</td>
																							<td style="width: 2px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td style="height: 41px" class="formtext">
																								&nbsp; &nbsp; &nbsp; Tipe Pelapor</td>
																							<td style="width: 2px; height: 41px">
																								:</td>
																							<td style="height: 41px" class="formtext">
																								<asp:RadioButtonList ID="rblTRXKKTipePelapor" runat="server" AutoPostBack="True"
																									RepeatDirection="Horizontal" Enabled="False">
																									<asp:ListItem Value="1">Perorangan</asp:ListItem>
																									<asp:ListItem Value="2">Korporasi</asp:ListItem>
																								</asp:RadioButtonList></td>
																						</tr>
																					</tbody>
																				</table>
																				<table id="tblTRXKKTipePelaporPerorangan" runat="server">
																					<tbody>
																						<tr>
																							<td class="formtext" colspan="3">
																								<strong>5.5.1. Perorangan</strong>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								a. Gelar</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKKINDVGelar" runat="server"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								b. Nama Lengkap
																							</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKKINDVNamaLengkap" runat="server" Width="370px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								c. Tempat Lahir</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKKINDVTempatLahir" runat="server" Width="240px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								d. Tanggal Lahir
																							</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<br />
																								<asp:Label ID="txtTRXKKINDVTglLahir" runat="server" Width="96px" MaxLength="50"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								e. Kewarganegaraan
																							</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:RadioButtonList ID="rblTRXKKINDVKewarganegaraan" runat="server" Enabled="False"
																									AutoPostBack="True" RepeatDirection="Horizontal">
																									<asp:ListItem Selected="True" Value="1">WNI</asp:ListItem>
																									<asp:ListItem Value="2">WNA</asp:ListItem>
																								</asp:RadioButtonList></td>
																						</tr>
																						<tr style="color: #cc0000">
																							<td class="formtext">
																								f. Negara
																							</td>
																							<td style="width: 5px; color: #000000">
																								:</td>
																							<td class="formtext">
																								<asp:DropDownList ID="cboTRXKKINDVNegara" runat="server" CssClass="combobox" Enabled="False">
																								</asp:DropDownList></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								g. Alamat Domisili</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												Nama Jalan</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKINDVDOMNamaJalan" runat="server" Width="370px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												RT/RW</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKINDVDOMRTRW" runat="server"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kelurahan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext" valign="top">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKINDVDOMKelurahan" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKINDVDOMKelurahan" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kecamatan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												&nbsp;<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKINDVDOMKecamatan" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKINDVDOMKecamatan" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kota / Kabupaten</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKINDVDOMKotaKab" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKINDVDOMKotaKab" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																									
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Provinsi</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKINDVDOMProvinsi" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKINDVDOMProvinsi" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
                                                                                                        	<tr>
                                                                                                                <td class="formtext">
                                                                                                                    &nbsp;</td>
                                                                                                                <td class="formtext">
                                                                                                                    Negara</td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :</td>
                                                                                                                <td class="formtext">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:Label ID="txtTRXKKINDVDOMNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfTRXKKINDVDOMNegara" runat="server" />
                                                                                                                </td>
                                                                                                        </tr>
                                                                                                        	<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kode Pos</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKINDVDOMKodePos" runat="server"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								h. Alamat Sesuai Bukti Identitas</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												Nama Jalan</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKINDVIDNamaJalan" runat="server" Width="370px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												RT/RW</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKINDVIDRTRW" runat="server"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kelurahan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext" valign="top">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKINDVIDKelurahan" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKINDVIDKelurahan" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kecamatan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												&nbsp;<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKINDVIDKecamatan" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKINDVIDKecamatan" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kota / Kabupaten</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKINDVIDKotaKab" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKINDVIDKotaKab" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																								
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Provinsi</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKINDVIDProvinsi" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKINDVIDProvinsi" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
                                                                                                        		<tr>
                                                                                                                    <td class="formtext">
                                                                                                                        &nbsp;</td>
                                                                                                                    <td class="formtext">
                                                                                                                        Negara</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <asp:Label ID="txtTRXKKINDVIDNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTRXKKINDVIDNegara" runat="server" />
                                                                                                                    </td>
                                                                                                        </tr>
                                                                                                        		<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kode Pos</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKINDVIDKodePos" runat="server"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								i. Alamat Sesuai Negara Asal</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												Nama Jalan</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKINDVNANamaJalan" runat="server" Width="370px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Negara</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKINDVNANegara" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKINDVNANegara" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Provinsi</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext" valign="top">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKINDVNAProvinsi" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKINDVNAProvinsi" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kota
																											</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKINDVNAKota" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKINDVNAKota" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kode Pos</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKINDVNAKodePos" runat="server"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								j. Jenis Dokumen Identitas</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<ajax:AjaxPanel ID="AjaxPanel3" runat="server">
																									<asp:DropDownList runat="server" ID="cboTRXKKINDVJenisID" CssClass="combobox" Enabled="False">
																									</asp:DropDownList>
																								</ajax:AjaxPanel>
																							</td>
																						</tr>
																						<tr>
																							<td id="Td2" class="formtext" colspan="3" runat="server">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																											  Nomor Identitas <span style="color: #cc0000">*</span></td>
																											<td style="width: 5px">
																												:</td>
																											<td style="width: 260px" class="formtext">
																												<asp:Label ID="txtTRXKKINDVNomorId" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								k. Nomor Pokok Wajib Pajak (NPWP)</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKKINDVNPWP" runat="server" Width="257px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								l. Pekerjaan</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												Pekerjaan</td>
																											<td style="width: 5px">
																												:</td>
																											<td style="width: 260px" class="formtext">
																												<asp:Label ID="txtTRXKKINDVPekerjaan" runat="server" Width="257px"></asp:Label>
																												&nbsp;<asp:HiddenField ID="hfTRXKKINDVPekerjaan" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Jabatan</td>
																											<td style="width: 5px">
																												:</td>
																											<td style="width: 260px" class="formtext">
																												<asp:Label ID="txtTRXKKINDVJabatan" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Penghasilan rata-rata/th (Rp)</td>
																											<td style="width: 5px">
																												:</td>
																											<td style="width: 260px" class="formtext">
																												<asp:Label ID="txtTRXKKINDVPenghasilanRataRata" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Tempat kerja</td>
																											<td style="width: 5px">
																												:</td>
																											<td style="width: 260px" class="formtext">
																												<asp:Label ID="txtTRXKKINDVTempatKerja" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								m. Tujuan Transaksi</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKKINDVTujuanTrx" runat="server" Width="257px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								n. Sumber Dana</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKKINDVSumberDana" runat="server" Width="257px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												ii. Nama Bank Lain</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKINDVNamaBankLain" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												ii. Nomor Rekening Tujuan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKINDVNoRekTujuan" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																					</tbody>
																				</table>
																				<table id="tblTRXKKTipePelaporKorporasi" runat="server">
																					<tbody>
																						<tr>
																							<td class="formtext" colspan="3">
																								<strong>5.5.2. Korporasi</strong></td>
																						</tr>
																						<tr>
																							<td style="height: 28px" class="formtext">
																								a. Bentuk Badan Usaha</td>
																							<td style="width: 5px; height: 28px">
																								:</td>
																							<td style="height: 28px" class="formtext">
																								&nbsp;&nbsp;
																								<asp:DropDownList ID="cboTRXKKCORPBentukBadanUsaha" runat="server" Enabled="False">
																								</asp:DropDownList></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								b. Nama Korporasi
																							</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKKCORPNama" runat="server" Width="370px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								c. Bidang Usaha Korporasi</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<table style="width: 127px">
																									<tbody>
																										<tr>
																											<td>
																												<asp:Label ID="txtTRXKKCORPBidangUsaha" runat="server" Width="167px"></asp:Label></td>
																											<td>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																								<asp:HiddenField ID="hfTRXKKCORPBidangUsaha" runat="server"></asp:HiddenField>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								d. Alamat Korporasi</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:RadioButtonList ID="rblTRXKKCORPTipeAlamat" runat="server" Enabled="False" AutoPostBack="True"
																									RepeatDirection="Horizontal">
																									<asp:ListItem Selected="True">Dalam Negeri</asp:ListItem>
																									<asp:ListItem>Luar Negeri</asp:ListItem>
																								</asp:RadioButtonList></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								e. Alamat Lengkap Korporasi</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												Nama Jalan</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKCORPDLNamaJalan" runat="server" Width="370px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												RT/RW</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKCORPDLRTRW" runat="server"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kelurahan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext" valign="top">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKCORPDLKelurahan" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKCORPDLKelurahan" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kecamatan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												&nbsp;<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKCORPDLKecamatan" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKCORPDLKecamatan" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kota / Kabupaten</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKCORPDLKotaKab" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKCORPDLKotaKab" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																								
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Provinsi</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKCORPDLProvinsi" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKCORPDLProvinsi" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Negara</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKCORPDLNegara" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKCORPDLNegara" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
                                                                                                        		<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kode Pos</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKCORPDLKodePos" runat="server"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								i. Alamat Korporasi Luar Negeri</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												Nama Jalan</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKCORPLNNamaJalan" runat="server" Width="370px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Negara</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKCORPLNNegara" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKCORPLNNegara" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Provinsi</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext" valign="top">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKCORPLNProvinsi" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKCORPLNProvinsi" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kota
																											</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<table style="width: 127px">
																													<tbody>
																														<tr>
																															<td>
																																<asp:Label ID="txtTRXKKCORPLNKota" runat="server" Width="167px"></asp:Label></td>
																															<td>
																															</td>
																														</tr>
																													</tbody>
																												</table>
																												<asp:HiddenField ID="hfTRXKKCORPLNKota" runat="server"></asp:HiddenField>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												Kode Pos</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKCORPLNKodePos" runat="server"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								k. Nomor Pokok Wajib Pajak (NPWP)</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKKCORPNPWP" runat="server" Width="257px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								l. Tujuan Transaksi</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKKCORPTujuanTrx" runat="server" Width="257px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								m. Sumber Dana</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="txtTRXKKCORPSumberDana" runat="server" Width="257px"></asp:Label></td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext" colspan="3">
																								<table>
																									<tbody>
																										<tr>
																											<td class="formtext">
																												&nbsp; &nbsp;&nbsp;
																											</td>
																											<td class="formtext">
																												ii. Nama Bank Lain</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKCORPNamaBankLain" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																										<tr>
																											<td class="formtext">
																											</td>
																											<td class="formtext">
																												ii. Nomor Rekening Tujuan</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:Label ID="txtTRXKKCORPNoRekeningTujuan" runat="server" Width="257px"></asp:Label></td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																					</tbody>
																				</table>
																			</asp:View>
																		</asp:MultiView>
																		<table>
																			<tbody>
																				<tr>
																					<td bgcolor="#ffffff">
																						&nbsp;&nbsp;
																					</td>
																					<td style="width: 76px" bgcolor="#ffffff">
																						&nbsp;
																					</td>
																				</tr>
																			</tbody>
																		</table>
																		<table>
																			<tbody>
																				<tr>
																					<td colspan="3">
																						<asp:GridView ID="grvTransaksi" runat="server" SkinID="grv2" AutoGenerateColumns="False">
																							<Columns>
																								<asp:BoundField HeaderText="No" />
																								<asp:BoundField HeaderText="Kas" DataField="Kas" />
																								<asp:BoundField HeaderText="Transaction Date" DataField="TransactionDate" />
																								<asp:BoundField HeaderText="Branch" DataField="Branch" />
																								<asp:BoundField HeaderText="Account No" DataField="AccountNumber" />
																								<asp:BoundField HeaderText="Customer / LTKT" DataField="Type" Visible="false" />
																								<asp:BoundField HeaderText="Transaction Nominal" DataField="TransactionNominal" ItemStyle-HorizontalAlign="Right" />
																								<asp:TemplateField>
																									<ItemTemplate>
																										<asp:LinkButton ID="EditResume" runat="server" CausesValidation="False" OnClick="EditResume_Click">View</asp:LinkButton>
																									</ItemTemplate>
																								</asp:TemplateField>
																							</Columns>
																						</asp:GridView>
																					</td>
																				</tr>
																				<tr>
																					<td class="formtext">
																						Total Seluruh Kas Masuk</td>
																					<td style="width: 5px">
																						:</td>
																					<td class="formtext">
																						<asp:Label ID="lblTotalKasMasuk" runat="server"></asp:Label>
																					</td>
																				</tr>
																				<tr>
																					<td style="height: 15px" class="formtext">
																						Total Seluruh Kas Keluar</td>
																					<td style="width: 5px; height: 15px">
																						:</td>
																					<td style="height: 15px" class="formtext">
																						<asp:Label ID="lblTotalKasKeluar" runat="server"></asp:Label>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</td>
																</tr>
																<tr>
																<td>
																<table border="0">
														<tr valign="top">
															<td class="formtext" style="height: 45px">
                                                                6.Sebutkan informasi lainnya yang ada</td>
															<td style="width: 2px; height: 45px">
																:</td>
															<td class="formtext" style="height: 45px">
																<asp:Label ID="txtLTKTInformasiLainnya" runat="server" Height="46px" Width="257px"></asp:Label></td>
														</tr>
													</table>
																</td>
																</tr>
															</tbody>
														</table>
													</ajax:AjaxPanel>
												</td>
											</tr>
										</table>
									</asp:View>
									<asp:View runat="server" ID="vwMessage">
										<table width="100%" style="horiz-align: center;">
											<tr>
												<td class="formtext" align="center">
													<asp:Label runat="server" ID="lblMsg"></asp:Label>
												</td>
											</tr>
											<tr>
												<td class="formtext" align="center">
													<asp:ImageButton ID="imgOKMsg" runat="server" ImageUrl="~/Images/button/Ok.gif" />
												</td>
											</tr>
										</table>
									</asp:View>
								</asp:MultiView>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
				<ajax:AjaxPanel ID="AjaxPanel5" runat="server">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td background="Images/button-bground.gif" align="left" valign="middle">
								<img src="Images/blank.gif" width="5" height="1" /></td>
							<td background="Images/button-bground.gif" align="left" valign="middle">
								<img src="images/arrow.gif" width="15" height="15" />&nbsp;</td>
							<td background="Images/button-bground.gif" style="width: 5px">
								&nbsp;</td>
							<td background="Images/button-bground.gif">
								&nbsp;</td>
							<td background="Images/button-bground.gif">
								<asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" 
									ImageUrl="~/Images/button/back.gif"></asp:ImageButton>&nbsp;</td>
							<td width="99%" background="Images/button-bground.gif">
								<img src="Images/blank.gif" width="1" height="1" /></td>
							<td>
								</td>
						</tr>
					</table>
				</ajax:AjaxPanel>
			</td>
		</tr>
	</table>
	<asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
