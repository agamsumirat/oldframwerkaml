
Partial Class CaseManagementView
    Inherits Parent
#Region " Property "
    Private Property SearchCaseID() As String
        Get
            If Not Session("CaseManagementViewCaseID") Is Nothing Then
                Return Session("CaseManagementViewCaseID")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewCaseID") = value
        End Set
    End Property
    Private Property SearchCaseDescription() As String
        Get
            If Not Session("CaseManagementViewCaseDescription") Is Nothing Then
                Return Session("CaseManagementViewCaseDescription")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewCaseDescription") = value
        End Set
    End Property
    Private Property SearchCaseStatus() As String
        Get
            If Not Session("CaseManagementViewCaseStatus") Is Nothing Then
                Return Session("CaseManagementViewCaseStatus")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewCaseStatus") = value
        End Set
    End Property
    Private Property SearchWorkFlowStep() As String
        Get
            If Not Session("CaseManagementViewWorkFlowStep") Is Nothing Then
                Return Session("CaseManagementViewWorkFlowStep")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewWorkFlowStep") = value
        End Set
    End Property
    Private Property SearchAccountOwner() As String
        Get
            If Not Session("CaseManagementViewAccountOwner") Is Nothing Then
                Return Session("CaseManagementViewAccountOwner")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewAccountOwner") = value
        End Set
    End Property
    Private Property SearchLastProposedAction() As String
        Get
            If Not Session("CaseManagementViewLastProposedAction") Is Nothing Then
                Return Session("CaseManagementViewLastProposedAction")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewLastProposedAction") = value
        End Set
    End Property
    Private Property SearchCreateDateFirst() As String
        Get
            If Not Session("CaseManagementViewCreatedDateFirst") Is Nothing Then
                Return Session("CaseManagementViewCreatedDateFirst")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewCreatedDateFirst") = value
        End Set
    End Property
    Private Property SearchCreateDateLast() As String
        Get
            If Not Session("CaseManagementViewCreatedDateLast") Is Nothing Then
                Return Session("CaseManagementViewCreatedDateLast")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewCreatedDateLast") = value
        End Set
    End Property
    Private Property SearchLastUpdatedDateFirst() As String
        Get
            If Not Session("CaseManagementViewLastUpdatedDateFirst") Is Nothing Then
                Return Session("CaseManagementViewLastUpdatedDateFirst")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewLastUpdatedDateFirst") = value
        End Set
    End Property
    Private Property SearchLastUpdatedDateLast() As String
        Get
            If Not Session("CaseManagementViewLastUpdatedDateLast") Is Nothing Then
                Return Session("CaseManagementViewLastUpdatedDateLast")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewLastUpdatedDateLast") = value
        End Set
    End Property
    Private Property SearchPIC() As String
        Get
            If Not Session("CaseManagementViewPIC") Is Nothing Then
                Return Session("CaseManagementViewPIC")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewPIC") = value
        End Set
    End Property
    Private Property SearchHighRiskCustomer() As String
        Get
            If Not Session("CaseManagementViewHighRiskCustomer") Is Nothing Then
                Return Session("CaseManagementViewHighRiskCustomer")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewHighRiskCustomer") = value
        End Set
    End Property
    Private Property SearchSameCaseCount() As String
        Get
            If Not Session("CaseManagementSearchSameCaseCount") Is Nothing Then
                Return Session("CaseManagementSearchSameCaseCount")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementSearchSameCaseCount") = value
        End Set
    End Property
    Private Property SearchAging() As String
        Get
            If Not Session("CaseManagementViewAging") Is Nothing Then
                Return Session("CaseManagementViewAging")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewAging") = value
        End Set
    End Property
    Private Property SearchCustomerName() As String
        Get
            If Not Session("CaseManagementViewCustomerName") Is Nothing Then
                Return Session("CaseManagementViewCustomerName")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewCustomerName") = value
        End Set
    End Property
    Private Property SearchRMCode() As String
        Get
            If Not Session("CaseManagementViewRMCode") Is Nothing Then
                Return Session("CaseManagementViewRMCode")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewRMCode") = value
        End Set
    End Property
 
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("CaseManagementViewSelected") Is Nothing, New ArrayList, Session("CaseManagementViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CaseManagementViewSelected") = value
        End Set
    End Property


    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CaseManagementViewSort") Is Nothing, "Aging desc", Session("CaseManagementViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("CaseManagementViewSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CaseManagementViewCurrentPage") Is Nothing, 0, Session("CaseManagementViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CaseManagementViewCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CaseManagementViewRowTotal") Is Nothing, 0, Session("CaseManagementViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CaseManagementViewRowTotal") = Value
        End Set
    End Property
    Private ReadOnly Property Tables() As String
        Get
            Dim StrQuery As New StringBuilder
            StrQuery.Append(" CaseManagement INNER JOIN CaseStatus ON CaseManagement.FK_CaseStatusID = CaseStatus.CaseStatusId INNER JOIN AccountOwner ON CaseManagement.FK_AccountOwnerID = AccountOwner.AccountOwnerId LEFT OUTER JOIN CaseManagementProposedAction ON CaseManagement.FK_LastProposedStatusID = CaseManagementProposedAction.PK_CaseManagementProposedActionID ")
            StrQuery.Append(" INNER JOIN CaseManagementWorkflow ON CaseManagement.FK_AccountOwnerId=CaseManagementWorkflow.AccountOwnerId AND (CaseManagement.Segment=CaseManagementWorkflow.Segment OR 'ALL'=CaseManagementWorkflow.Segment) ")
            StrQuery.Append(" INNER JOIN CaseManagementWorkflowDetail ON CaseManagementWorkflow.PK_CMW_ID=CaseManagementWorkflowDetail.FK_CMW_ID ")
            StrQuery.Append(" INNER JOIN MapCaseManagementWorkflowHistory mcmwh ON mcmwh.FK_CaseManagementId=casemanagement.PK_CaseManagementID ")
            StrQuery.Append(" Left OUTER JOIN SSOFFR ON SSOFFR.SSOOFF= casemanagement.RMCODE ")
            Return StrQuery.ToString()
        End Get
    End Property
    Private ReadOnly Property PK() As String
        Get
            Return "CaseManagement.PK_CaseManagementID"
        End Get
    End Property
    Private ReadOnly Property Fields() As String

        Get
            Dim strField As New StringBuilder
            strField.Append("CaseManagement.CustomerName,CaseManagement.RMCode + ' - ' + SSOFFR.SSONAM RMCODE,CaseManagement.PK_CaseManagementID, CaseManagement.CaseDescription, CaseManagement.FK_CaseStatusID,")
            strField.Append("CaseStatus.CaseStatusDescription, CaseManagement.WorkflowStep, CaseManagement.CreatedDate, CaseManagement.LastUpdated,")

            'update PIC
            strField.Append("CaseManagement.FK_AccountOwnerID, AccountOwner.AccountOwnerName, CAST(CaseManagement.FK_AccountOwnerID AS VARCHAR) + ' - ' + AccountOwner.AccountOwnerName AS AccountOwnerIDName, CaseManagement.PIC, CaseManagement.HasOpenIssue,")
            strField.Append("CaseManagement.Aging, CaseManagement.IsReportedtoRegulator, CaseManagement.PPATKConfirmationno,")            
            strField.Append("CaseManagementProposedAction.ProposedAction,SameCaseCount=(SELECT COUNT(1) FROM CaseManagementSameCaseHistory cmsch WHERE cmsch.FK_CaseManagement_ID=CaseManagement.PK_CaseManagementID),HighRiskCustomer =ISNULL((SELECT cmr.Risk FROM CaseManagementRisk cmr WHERE cmr.FK_CaseManagement_ID=casemanagement.PK_CaseManagementID),'Low') ")

            Return strField.ToString
        End Get
    End Property

    Private ReadOnly Property GroupBy() As String
        Get
            Dim strField As New StringBuilder
            strField.Append("CaseManagement.CustomerName,CaseManagement.RMCode + ' - ' + SSOFFR.SSONAM, CaseManagement.PK_CaseManagementID, CaseManagement.CaseDescription, CaseManagement.FK_CaseStatusID,")
            strField.Append("CaseStatus.CaseStatusDescription, CaseManagement.WorkflowStep, CaseManagement.CreatedDate, CaseManagement.LastUpdated,")

            'update PIC
            strField.Append("CaseManagement.FK_AccountOwnerID, AccountOwner.AccountOwnerName, CAST(CaseManagement.FK_AccountOwnerID AS VARCHAR) + ' - ' + AccountOwner.AccountOwnerName, CaseManagement.PIC, CaseManagement.HasOpenIssue,")
            strField.Append("CaseManagement.Aging, CaseManagement.IsReportedtoRegulator, CaseManagement.PPATKConfirmationno,")
            strField.Append("CaseManagementProposedAction.ProposedAction")
            Return strField.ToString
        End Get
    End Property


    Private ReadOnly Property SetnGetBindTableAll() As Data.DataTable
        Get
            ' Return IIf(Session("CaseManagementViewData") Is Nothing, New AMLDAL.CaseManagement.CaseManagementDataTable, Session("CaseManagementViewData"))

            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            If SearchCaseID.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.PK_CaseManagementID like'%" & SearchCaseID.Replace("'", "''") & "%' "
            End If
            If SearchCaseDescription.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.CaseDescription like'%" & SearchCaseDescription.Replace("'", "''") & "%' "
            End If
            If SearchCaseStatus.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseStatus.CaseStatusDescription like'%" & SearchCaseStatus.Replace("'", "''") & "%' "
            End If
            If SearchWorkFlowStep.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.WorkflowStep like'%" & SearchWorkFlowStep.Replace("'", "''") & "%' "
            End If
            If SearchAccountOwner.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CAST(CaseManagement.FK_AccountOwnerID AS VARCHAR) + ' - ' + AccountOwner.AccountOwnerName like'%" & SearchAccountOwner.Replace("'", "''") & "%' "
            End If
            If SearchLastProposedAction.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagementProposedAction.ProposedAction like'%" & SearchLastProposedAction.Replace("'", "''") & "%' "
            End If
            If SearchCreateDateFirst.Trim.Length > 0 And SearchCreateDateLast.Trim.Length = 0 Then
                Throw New Exception("Created Date until must be filled")
            End If
            If SearchCreateDateFirst.Trim.Length = 0 And SearchCreateDateLast.Trim.Length > 0 Then
                Throw New Exception("Created Date from must be filled")
            End If
            If SearchCreateDateFirst.Trim.Length > 0 And SearchCreateDateLast.Trim.Length > 0 Then
                Dim DCreatedFirst As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SearchCreateDateFirst.Trim)
                Dim DCreatedLast As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SearchCreateDateLast.Trim)

                If DCreatedFirst.CompareTo(DateTime.MinValue) > 0 And DCreatedLast.CompareTo(DateTime.MinValue) > 0 Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "CaseManagement.CreatedDate between '" & Format(DCreatedFirst, "yyyy-MM-dd").Replace("'", "''") & " 00:00:00' and '" & Format(DCreatedLast, "yyyy-MM-dd") & " 23:59:59'"
                End If
            End If
            If SearchLastUpdatedDateFirst.Trim.Length > 0 And SearchLastUpdatedDateLast.Trim.Length > 0 Then
                Dim DUpdatedFirst As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SearchLastUpdatedDateFirst.Trim)
                Dim DUpdatedLast As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SearchLastUpdatedDateLast.Trim)

                If DUpdatedFirst.CompareTo(DateTime.MinValue) > 0 And DUpdatedLast.CompareTo(DateTime.MinValue) > 0 Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "CaseManagement.LastUpdated between '" & Format(DUpdatedFirst, "yyyy-MM-dd").Replace("'", "''") & " 00:00:00' and '" & Format(DUpdatedLast, "yyyy-MM-dd") & " 23:59:59'"
                End If
            End If
            If SearchPIC.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.PIC like'%" & SearchPIC.Replace("'", "''") & "%' "
            End If
            If SearchHighRiskCustomer.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                'strWhereClause(strWhereClause.Length - 1) = "CASE WHEN CaseManagement.HasOpenIssue=1 THEN 'TRUE' ELSE 'FALSE' END LIKE '%" & TempSearchHasOpenIssue & "%' "
                'strWhereClause(strWhereClause.Length - 1) = "CASE WHEN CaseManagement.HasOpenIssue=1 THEN 'TRUE' ELSE 'FALSE' END LIKE '%" & SearchHighRiskCustomer.Replace("'", "''") & "%' "
                'strWhereClause(strWhereClause.Length - 1) = " ( SELECT CASE  when COUNT(1) >0 THEN 'High Risk Customer' ELSE 'Non High Risk Customer' end  FROM CaseManagementProfileTopology cmpt INNER JOIN AllAccount_AllInfo aaai ON aaai.AccountNo=cmpt.AccountNo INNER JOIN CustomerInList cil ON cil.CIFNo=aaai.CIFNO WHERE cmpt.FK_CaseManagement_ID=casemanagement.PK_CaseManagementID)  like '%" & SearchHighRiskCustomer & "%'"
                strWhereClause(strWhereClause.Length - 1) = " ISNULL((SELECT cmr.Risk FROM CaseManagementRisk cmr WHERE cmr.FK_CaseManagement_ID=casemanagement.PK_CaseManagementID),'Low') LIKE '%" & SearchHighRiskCustomer & "%'"
            End If
            If SearchAging.Trim.Length > 0 Then
                If IsNumeric(SearchAging) Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "CaseManagement.Aging =" & SearchAging.Replace("'", "''") & " "
                Else
                    Throw New Exception("Aging must be numeric.")
                End If
            End If

            If Me.SearchSameCaseCount.Trim.Length > 0 Then
                If Integer.TryParse(SearchSameCaseCount, 0) Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "(SELECT COUNT(1) FROM CaseManagementSameCaseHistory cmsch WHERE cmsch.FK_CaseManagement_ID=CaseManagement.PK_CaseManagementID)=" & SearchSameCaseCount.Replace("'", "''") & " "
                Else
                    Throw New Exception("Same Case Count must be numeric.")
                End If
            End If
            If SearchCustomerName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.CustomerName like'%" & SearchCustomerName.Replace("'", "''") & "%' "
            End If
            If SearchRMCode.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.RMCode like'%" & SearchRMCode.Replace("'", "''") & "%' "
            End If
            strAllWhereClause = String.Join(" and ", strWhereClause)

            If strAllWhereClause.Trim = "" Then
                strAllWhereClause += " ("
                strAllWhereClause += " '|' + CaseManagementWorkflowDetail.Approvers + '|'  like '%|" & Sahassa.AML.Commonly.SessionPkUserId & "|%'"
                'strAllWhereClause += " OR '|' + CaseManagementWorkflowDetail.NotifyOthers + '|'  like '%|" & Sahassa.AML.Commonly.SessionPkUserId & "|%'"
                strAllWhereClause += " )"
                strAllWhereClause += " and mcmwh.userid + ';' like '%" & Sahassa.AML.Commonly.SessionUserId & ";%' and mcmwh.fk_eventtypeid IN (2, 8)  and mcmwh.bcomplate=0"

            Else
                strAllWhereClause += " AND ("
                strAllWhereClause += " '|' + CaseManagementWorkflowDetail.Approvers + '|'  like '%|" & Sahassa.AML.Commonly.SessionPkUserId & "|%'"
                'strAllWhereClause += " OR '|' + CaseManagementWorkflowDetail.NotifyOthers + '|'  like '%|" & Sahassa.AML.Commonly.SessionPkUserId & "|%'"
                strAllWhereClause += " )"
                strAllWhereClause += " and mcmwh.userid + ';' like '%" & Sahassa.AML.Commonly.SessionUserId & ";%' and mcmwh.fk_eventtypeid IN (2, 8)  and mcmwh.bcomplate=0"
            End If
            Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCountDistinctByColumn(Me.Tables, strAllWhereClause, Me.PK)
            Dim pageSize As Long = Integer.MaxValue
            'If Sahassa.AML.Commonly.SessionPagingLimit = "" Then
            '    pageSize = 10
            'Else
            '    pageSize = Sahassa.AML.Commonly.SessionPagingLimit
            'End If
            'Return Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, pageSize, Me.Fields, strAllWhereClause, Me.GroupBy)
            Dim strsql As String = ""
            strsql = "select " & Me.Fields & " from " & Me.Tables & " where " & strAllWhereClause & "  group by " & GroupBy & " order by " & Me.SetnGetSort


            Return SahassaNettier.Data.DataRepository.Provider.ExecuteDataSet(Sahassa.AML.Commonly.GetSQLCommand(strsql)).Tables(0)
        End Get

    End Property

    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property SetnGetBindTable() As Data.DataTable
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            If SearchCaseID.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.PK_CaseManagementID like'%" & SearchCaseID.Replace("'", "''") & "%' "
            End If
            If SearchCaseDescription.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.CaseDescription like'%" & SearchCaseDescription.Replace("'", "''") & "%' "
            End If
            If SearchCaseStatus.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseStatus.CaseStatusDescription like'%" & SearchCaseStatus.Replace("'", "''") & "%' "
            End If
            If SearchWorkFlowStep.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.WorkflowStep like'%" & SearchWorkFlowStep.Replace("'", "''") & "%' "
            End If
            If SearchAccountOwner.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CAST(CaseManagement.FK_AccountOwnerID AS VARCHAR) + ' - ' + AccountOwner.AccountOwnerName like'%" & SearchAccountOwner.Replace("'", "''") & "%' "
            End If
            If SearchLastProposedAction.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagementProposedAction.ProposedAction like'%" & SearchLastProposedAction.Replace("'", "''") & "%' "
            End If
            If SearchCreateDateFirst.Trim.Length > 0 And SearchCreateDateLast.Trim.Length = 0 Then
                Throw New Exception("Created Date until must be filled")
            End If
            If SearchCreateDateFirst.Trim.Length = 0 And SearchCreateDateLast.Trim.Length > 0 Then
                Throw New Exception("Created Date from must be filled")
            End If
            If SearchCreateDateFirst.Trim.Length > 0 And SearchCreateDateLast.Trim.Length > 0 Then
                Dim DCreatedFirst As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SearchCreateDateFirst.Trim)
                Dim DCreatedLast As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SearchCreateDateLast.Trim)

                If DCreatedFirst.CompareTo(DateTime.MinValue) > 0 And DCreatedLast.CompareTo(DateTime.MinValue) > 0 Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "CaseManagement.CreatedDate between '" & Format(DCreatedFirst, "yyyy-MM-dd").Replace("'", "''") & " 00:00:00' and '" & Format(DCreatedLast, "yyyy-MM-dd") & " 23:59:59'"
                End If
            End If
            If SearchLastUpdatedDateFirst.Trim.Length > 0 And SearchLastUpdatedDateLast.Trim.Length > 0 Then
                Dim DUpdatedFirst As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SearchLastUpdatedDateFirst.Trim)
                Dim DUpdatedLast As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SearchLastUpdatedDateLast.Trim)

                If DUpdatedFirst.CompareTo(DateTime.MinValue) > 0 And DUpdatedLast.CompareTo(DateTime.MinValue) > 0 Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "CaseManagement.LastUpdated between '" & Format(DUpdatedFirst, "yyyy-MM-dd").Replace("'", "''") & " 00:00:00' and '" & Format(DUpdatedLast, "yyyy-MM-dd") & " 23:59:59'"
                End If
            End If
            If SearchPIC.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.PIC like'%" & SearchPIC.Replace("'", "''") & "%' "
            End If
            If SearchHighRiskCustomer.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                'strWhereClause(strWhereClause.Length - 1) = "CASE WHEN CaseManagement.HasOpenIssue=1 THEN 'TRUE' ELSE 'FALSE' END LIKE '%" & TempSearchHasOpenIssue & "%' "
                'strWhereClause(strWhereClause.Length - 1) = "CASE WHEN CaseManagement.HasOpenIssue=1 THEN 'TRUE' ELSE 'FALSE' END LIKE '%" & SearchHighRiskCustomer.Replace("'", "''") & "%' "
                'strWhereClause(strWhereClause.Length - 1) = " ( SELECT CASE  when COUNT(1) >0 THEN 'High Risk Customer' ELSE 'Non High Risk Customer' end  FROM CaseManagementProfileTopology cmpt INNER JOIN AllAccount_AllInfo aaai ON aaai.AccountNo=cmpt.AccountNo INNER JOIN CustomerInList cil ON cil.CIFNo=aaai.CIFNO WHERE cmpt.FK_CaseManagement_ID=casemanagement.PK_CaseManagementID)  like '%" & SearchHighRiskCustomer & "%'"
                strWhereClause(strWhereClause.Length - 1) = " ISNULL((SELECT cmr.Risk FROM CaseManagementRisk cmr WHERE cmr.FK_CaseManagement_ID=casemanagement.PK_CaseManagementID),'Low') LIKE '%" & SearchHighRiskCustomer & "%'"
            End If
            If SearchAging.Trim.Length > 0 Then
                If IsNumeric(SearchAging) Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "CaseManagement.Aging =" & SearchAging.Replace("'", "''") & " "
                Else
                    Throw New Exception("Aging must be numeric.")
                End If
            End If

            If Me.SearchSameCaseCount.Trim.Length > 0 Then
                If Integer.TryParse(SearchSameCaseCount, 0) Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "(SELECT COUNT(1) FROM CaseManagementSameCaseHistory cmsch WHERE cmsch.FK_CaseManagement_ID=CaseManagement.PK_CaseManagementID)=" & SearchSameCaseCount.Replace("'", "''") & " "
                Else
                    Throw New Exception("Same Case Count must be numeric.")
                End If
            End If
            If SearchCustomerName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.CustomerName like'%" & SearchCustomerName.Replace("'", "''") & "%' "
            End If
            If SearchRMCode.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.RMCode like'%" & SearchRMCode.Replace("'", "''") & "%' "
            End If
            strAllWhereClause = String.Join(" and ", strWhereClause)

            If strAllWhereClause.Trim = "" Then
                strAllWhereClause += " ("
                strAllWhereClause += " '|' + CaseManagementWorkflowDetail.Approvers + '|'  like '%|" & Sahassa.AML.Commonly.SessionPkUserId & "|%'"
                'strAllWhereClause += " OR '|' + CaseManagementWorkflowDetail.NotifyOthers + '|'  like '%|" & Sahassa.AML.Commonly.SessionPkUserId & "|%'"
                strAllWhereClause += " )"
                strAllWhereClause += "     and mcmwh.userid + ';' like '%" & Sahassa.AML.Commonly.SessionUserId & ";%' and mcmwh.fk_eventtypeid IN (2, 8)  and mcmwh.bcomplate=0"
            Else
                strAllWhereClause += " AND ("
                strAllWhereClause += " '|' + CaseManagementWorkflowDetail.Approvers + '|'  like '%|" & Sahassa.AML.Commonly.SessionPkUserId & "|%'"
                'strAllWhereClause += " OR '|' + CaseManagementWorkflowDetail.NotifyOthers + '|'  like '%|" & Sahassa.AML.Commonly.SessionPkUserId & "|%'"
                strAllWhereClause += " )"
                strAllWhereClause += "     and mcmwh.userid + ';' like '%" & Sahassa.AML.Commonly.SessionUserId & ";%' and mcmwh.fk_eventtypeid IN (2, 8)  and mcmwh.bcomplate=0"
            End If
            Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCountDistinctByColumn(Me.Tables, strAllWhereClause, Me.PK)
            Dim pageSize As Long
            If Sahassa.AML.Commonly.SessionPagingLimit = "" Then
                pageSize = 10
            Else
                pageSize = Sahassa.AML.Commonly.SessionPagingLimit
            End If
            Return Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, pageSize, Me.Fields, strAllWhereClause, Me.GroupBy)
        End Get

    End Property
#End Region




    Private Sub ClearThisPageSessions()
        Session("CaseManagementSearchSameCaseCount") = Nothing
        Session("CaseManagementViewSelected") = Nothing
        Session("CaseManagementViewFieldSearch") = Nothing
        Session("CaseManagementViewValueSearch") = Nothing
        Session("CaseManagementViewSort") = Nothing
        Session("CaseManagementViewCurrentPage") = Nothing
        Session("CaseManagementViewRowTotal") = Nothing
        Session("CaseManagementViewData") = Nothing
        Session("CaseManagementViewRMCode") = Nothing
        Session("CaseManagementViewCustomerName") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then

                Me.ClearThisPageSessions()

                ImageClearSearch_Click(Nothing, Nothing)
                Me.popupCreatedFirst.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtCreatedDate1.ClientID & "'), 'dd-mm-yyyy')")
                Me.popupCreatedLast.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtCreatedDate2.ClientID & "'), 'dd-mm-yyyy')")
                Me.popupLastUpdatedFirst.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtLastUpdatedDate1.ClientID & "'), 'dd-mm-yyyy')")
                Me.popupLastUpdatedLast.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtLastUpdatedDate2.ClientID & "'), 'dd-mm-yyyy')")

                If Request.Params("CaseDescription") <> "" Then

                    SearchCaseDescription = Request.Params("CaseDescription")
                End If
                Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    If Request.Params("msg") <> "" Then
                        Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, "  window.alert(' " & Request.Params("msg") & ".')", True)
                    End If

                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            SettingControlSearch()
            Me.GridMSUserView.DataSource = Me.SetnGetBindTable
            'Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
            Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridMSUserView.DataBind()
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            CollectSelected()
            Me.SettingPropertySearch()
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid edit command handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Dim strCaseManagementID As String = e.Item.Cells(1).Text
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "CaseManagementViewDetail.aspx?PK_CaseManagementID=" & strCaseManagementID
            Me.Response.Redirect("CaseManagementViewDetail.aspx?PK_CaseManagementID=" & strCaseManagementID, False)
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 14/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim PKID As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PKID) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()
        Try
            Dim listPK As New System.Collections.Generic.List(Of String)
            listPK.Add("'0'")
            For Each IdPk As String In Me.SetnGetSelectedItem
                listPK.Add("'" & IdPk.ToString & "'")
            Next

            Me.GridMSUserView.DataSource = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, 0, Int32.MaxValue, Me.Fields, "PK_CaseManagementID IN (" & String.Join(",", listPK.ToArray) & ")", Me.GroupBy)
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0,1,4 & 5 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(13).Visible = False
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    ''' <summary>
    ''' export button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CaseManagementView.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                'Dim objLabel As Label = e.Item.FindControl("RMCODE")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub


    ''' <summary>
    ''' Setting Property Searching
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SettingPropertySearch()
        Me.SearchCaseID = txtCaseID.Text.Trim
        Me.SearchCaseDescription = txtDescription.Text.Trim
        Me.SearchCaseStatus = txtCaseStatus.Text.Trim
        Me.SearchWorkFlowStep = txtWorkFlowStep.Text.Trim
        Me.SearchAccountOwner = txtAccountOwner.Text.Trim
        Me.SearchLastProposedAction = txtLastProposedAction.Text.Trim
        Me.SearchLastUpdatedDateFirst = txtLastUpdatedDate1.Text.Trim
        Me.SearchLastUpdatedDateLast = txtLastUpdatedDate2.Text.Trim
        Me.SearchCreateDateFirst = txtCreatedDate1.Text.Trim
        Me.SearchCreateDateLast = txtCreatedDate2.Text.Trim
        Me.SearchPIC = txtPIC.Text.Trim
        Me.SearchHighRiskCustomer = TxtHighRiskCustomer.Text.Trim
        Me.SearchAging = txtAging.Text.Trim

        Me.SearchSameCaseCount = txtCountSameCase.Text.Trim
        Me.SearchCustomerName = txtCustomerName.Text.Trim
        Me.SearchRMCode = txtRMCODE.Text.Trim
    End Sub
    Private Sub SettingControlSearch()
        txtCaseID.Text = Me.SearchCaseID
        txtDescription.Text = Me.SearchCaseDescription
        txtCaseStatus.Text = Me.SearchCaseStatus
        txtWorkFlowStep.Text = Me.SearchWorkFlowStep
        txtAccountOwner.Text = Me.SearchAccountOwner
        txtLastProposedAction.Text = Me.SearchLastProposedAction
        txtCreatedDate1.Text = Me.SearchCreateDateFirst
        txtCreatedDate2.Text = Me.SearchCreateDateLast
        txtLastUpdatedDate1.Text = Me.SearchLastUpdatedDateFirst
        txtLastUpdatedDate2.Text = Me.SearchLastUpdatedDateLast
        txtPIC.Text = Me.SearchPIC
        TxtHighRiskCustomer.Text = Me.SearchHighRiskCustomer
        txtAging.Text = Me.SearchAging
        txtCountSameCase.Text = Me.SearchSameCaseCount
        txtCustomerName.Text = Me.SearchCustomerName
        txtRMCODE.Text = Me.SearchRMCode
    End Sub

    Protected Sub ImageClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageClearSearch.Click
        txtCaseID.Text = ""
        txtDescription.Text = ""
        txtCaseStatus.Text = ""
        txtWorkFlowStep.Text = ""
        txtAccountOwner.Text = ""
        txtLastProposedAction.Text = ""
        txtCreatedDate1.Text = ""
        txtCreatedDate2.Text = ""
        txtLastUpdatedDate1.Text = ""
        txtLastUpdatedDate2.Text = ""
        txtPIC.Text = ""
        TxtHighRiskCustomer.Text = ""
        txtAging.Text = ""
        txtCountSameCase.Text = ""
        txtCustomerName.Text = ""
        txtRMCODE.Text = ""
        Me.SettingPropertySearch()
    End Sub

    Sub BindExportAll()
        SettingControlSearch()
        GridMSUserView.PageSize = Integer.MaxValue
        Me.GridMSUserView.DataSource = Me.SetnGetBindTableAll
        Me.GridMSUserView.DataBind()
        Me.GridMSUserView.Columns(0).Visible = False
        Me.GridMSUserView.Columns(13).Visible = False
    End Sub

    Protected Sub LnkBtnExportAllToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkBtnExportAllToExcel.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindExportAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CaseManagementALL.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class '1250