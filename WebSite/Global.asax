
<%@ Application Language="VB" %>

<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
        log4net.Config.XmlConfigurator.Configure()
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
        Dim ex As Exception = Server.GetLastError().GetBaseException()
        Dim Logger As log4net.ILog = log4net.LogManager.GetLogger("AMLError")
        Logger.Error("Unhandled exception was occured", ex)
        ex = Nothing
        Logger = Nothing
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
       
    Protected Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim app As HttpApplication = CType(sender, HttpApplication)
        If app.Request.IsAuthenticated AndAlso TypeOf app.User.Identity Is FormsIdentity Then
            Dim identity As FormsIdentity = CType(app.User.Identity, FormsIdentity)
            If Not (identity.Name Is Nothing Or identity.Name.Length = 0) Then
                                
                Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Using dtUser As AMLDAL.AMLDataSet.UserDataTable = AccessUser.GetUserLoginInfo(identity.Name)
                        
                        If dtUser.Rows.Count > 0 Then
                            Dim RowUser As AMLDAL.AMLDataSet.UserRow = dtUser.Rows(0)
                            Dim GroupId As Integer = RowUser.UserGroupId
                            Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                                Using dtGroup As AMLDAL.AMLDataSet.GroupDataTable = AccessGroup.GetDataByGroupID(GroupId)
                                    If dtGroup.Rows.Count > 0 Then
                                        Dim RowGroup As AMLDAL.AMLDataSet.GroupRow = dtGroup.Rows(0)
                                        Dim GroupName As String = RowGroup.GroupName
                                        app.Context.User = New System.Security.Principal.GenericPrincipal(identity, New String() {GroupName})
                                    End If
                                End Using
                            End Using
                        Else
                            If identity.Name = "ForceChangePassword" or identity.Name = "NoWorkingUnitAssignedRequest" Then
                                app.Context.User = New System.Security.Principal.GenericPrincipal(identity, New String() {identity.Name})
                            End If
                        End If
                    End Using
                End Using
                
                'Dim ObjUser As MsUser = DataRepository.MsUserProvider.GetByUserid(identity.Name)
                
                'If Not ObjUser Is Nothing Then
                '    Dim ObjGroup As MsGroup = DataRepository.MsGroupProvider.GetByPk_MsGroup_Id(ObjUser.Fk_MsGroup_id)
                '    If Not ObjGroup Is Nothing Then
                '        app.Context.User = New System.Security.Principal.GenericPrincipal(identity, New String() {ObjGroup.GroupName.ToLower})
                '    End If
                'Else
                '    If identity.Name = "ForceChangePassword" Then
                '        app.Context.User = New System.Security.Principal.GenericPrincipal(identity, New String() {identity.Name})
                '    End If
                'End If
            End If
        End If
    End Sub
</script>