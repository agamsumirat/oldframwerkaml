<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false"
    CodeFile="NewsManagementApprovalDetail.aspx.vb" Inherits="NewsManagementApprovalDetail"
    Title="News Approval Detail" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table cellspacing="4" cellpadding="0" width="100%">
        <tr>
            <td valign="bottom" align="left">
                <img height="15" src="images/dot_title.gif" width="15">
            </td>
            <td class="maintitle" valign="bottom" width="99%">
                <asp:Label ID="LabelTitle" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table id="TableDetail" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
        bgcolor="#dddddd" border="2" runat="server">
        <tr class="formText" id="UserAdd1">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
                &nbsp;
            </td>
            <td bgcolor="#ffffff">
                Title
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :
            </td>
            <td width="80%" bgcolor="#ffffff">
                <asp:Label ID="LabelNewsTitleAdd" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserAdd2">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                Start Date
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNewsStartDateAdd" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserAdd3">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                End Date
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNewsEndDateAdd" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserAdd4">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                News Summary<br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" width="80%">
                <asp:Label ID="TextNewsSummaryAdd" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserAdd5">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
                &nbsp;
            </td>
            <td bgcolor="#ffffff">
                News Content<br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="TextNewsContentAdd" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserAdd6">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
                &nbsp;
            </td>
            <td bgcolor="#ffffff">
                Attachment
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :
            </td>
            <td bgcolor="#ffffff">
            <table>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="GridAttach" runat="server" AutoGenerateColumns="False"
                            CellPadding="4" ForeColor="Black" GridLines="Vertical" DataKeyNames="PK_NewsAttachment"
                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" 
                            BorderWidth="1px" EnableModelValidation="True">
                            <FooterStyle BackColor="#CCCC99" />
                            <Columns>
                                <asp:BoundField HeaderText="PK_NewsAttachment" Visible="False"
                                    DataField="PK_NewsAttachment">
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Attach File">
                                    <ItemTemplate>
                                        <asp:DataList ID="DataList1" runat="server">
                                            <ItemTemplate>
                                                <%#GenerateLink(Eval("PK_NewsAttachment"), Eval("Filename"), Eval("Fk_NewsApprovalId"))%>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </td>
                </tr>
                </table>
            </td>
        </tr>
        <tr class="formText" id="UserEdi1">
            <td height="24" colspan="4" bgcolor="#ffffcc">
                &nbsp;Old Values
            </td>
            <td colspan="4" bgcolor="#ffffcc">
                &nbsp;New Values
            </td>
        </tr>
        <tr class="formText" id="UserEdi2">
            <td bgcolor="#ffffff" style="width: 22px; height: 32px">
                &nbsp;
            </td>
            <td bgcolor="#ffffff" style="height: 32px;">
                News ID
            </td>
            <td bgcolor="#ffffff" style="width: 44px; height: 32px;">
                :
            </td>
            <td width="40%" bgcolor="#ffffff" style="height: 32px">
                <asp:Label ID="LabelOldNewsID" runat="server"></asp:Label>
            </td>
            <td width="5" bgcolor="#ffffff" style="height: 32px">
                &nbsp;
            </td>
            <td width="10%" bgcolor="#ffffff" style="height: 32px">
                News ID
            </td>
            <td bgcolor="#ffffff" style="height: 32px; width: 12px;">
                :
            </td>
            <td width="40%" bgcolor="#ffffff" style="height: 32px">
                <asp:Label ID="LabelNewNewsID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserEdi3">
            <td bgcolor="#ffffff" style="width: 22px">
                &nbsp;
            </td>
            <td bgcolor="#ffffff">
                Title
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelOldNewsTitle" runat="server"></asp:Label>
            </td>
            <td bgcolor="#ffffff">
                &nbsp;
            </td>
            <td bgcolor="#ffffff">
                Title
            </td>
            <td bgcolor="#ffffff" style="width: 12px">
                :
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNewNewsTitle" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserEdi4">
            <td bgcolor="#ffffff" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                Start Date
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelOldNewsStartDate" runat="server"></asp:Label>
            </td>
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff">
                Start Date
            </td>
            <td bgcolor="#ffffff" style="width: 12px">
                :
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNewNewsStartDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserEdi5">
            <td bgcolor="#ffffff" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                End Date
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelOldNewsEndDate" runat="server"></asp:Label>
            </td>
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff">
                End Date
            </td>
            <td bgcolor="#ffffff" style="width: 12px">
                :
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNewNewsEndDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserEdi6">
            <td bgcolor="#ffffff" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                News Summary<br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="TextOldNewsSummary" runat="server" Text=""></asp:Label>
            </td>
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff">
                News Summary<br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="width: 12px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="TextNewNewsSummary" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserEdi7">
            <td bgcolor="#ffffff" style="width: 22px">
                &nbsp;
            </td>
            <td bgcolor="#ffffff">
                News Content<br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="TextOldNewsContent" runat="server" Text=""></asp:Label>
            </td>
            <td bgcolor="#ffffff">
                &nbsp;
            </td>
            <td bgcolor="#ffffff">
                News Content<br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="width: 12px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="TextNewNewsContent" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserEdi8">
            <td bgcolor="#ffffff" style="width: 22px">
                &nbsp;
            </td>
            <td bgcolor="#ffffff">
                Attachment
            <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td  bgcolor="#ffffff" >
                        <asp:GridView ID="GridOldAttach" runat="server" AutoGenerateColumns="False"
                            CellPadding="4" ForeColor="Black" GridLines="Vertical" DataKeyNames="PK_NewsAttachment"
                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" 
                            BorderWidth="1px" EnableModelValidation="True">
                            <FooterStyle BackColor="#CCCC99" />
                            <Columns>
                                <asp:BoundField HeaderText="PK_NewsAttachment" Visible="False"
                                    DataField="PK_NewsAttachment">
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Attach File">
                                    <ItemTemplate>
                                        <asp:DataList ID="DataList1" runat="server">
                                            <ItemTemplate>
                                                <%#GenerateLink(Eval("PK_NewsAttachment"), Eval("Filename"), Eval("Fk_NewsApprovalId"))%>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </td>
            <td bgcolor="#ffffff">
                &nbsp;
            </td>
            <td bgcolor="#ffffff">
                Attachment<br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="width: 12px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td  bgcolor="#ffffff" >
                        <asp:GridView ID="GridNewAttach" runat="server" AutoGenerateColumns="False"
                            CellPadding="4" ForeColor="Black" GridLines="Vertical" DataKeyNames="PK_NewsAttachment"
                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" 
                            BorderWidth="1px" EnableModelValidation="True">
                            <FooterStyle BackColor="#CCCC99" />
                            <Columns>
                                <asp:BoundField HeaderText="PK_NewsAttachment" Visible="False"
                                    DataField="PK_NewsAttachment">
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Attach File">
                                    <ItemTemplate>
                                        <asp:DataList ID="DataList1" runat="server">
                                            <ItemTemplate>
                                                <%#GenerateLink(Eval("PK_NewsAttachment"), Eval("Filename"), Eval("Fk_NewsApprovalId"))%>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </td>
        </tr>
        <tr class="formText" id="UserDel1">
            <td bgcolor="#ffffff" style="height: 23px; width: 22px;">
                &nbsp;
            </td>
            <td bgcolor="#ffffff" style="height: 23px;">
                News ID
            </td>
            <td bgcolor="#ffffff" style="height: 23px; width: 44px;">
                :
            </td>
            <td width="80%" bgcolor="#ffffff" style="height: 23px">
                <asp:Label ID="LabelNewsIDDelete" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserDel2">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
                &nbsp;
            </td>
            <td bgcolor="#ffffff">
                Title
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNewsTitleDelete" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserDel3">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                Start Date
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNewsStartDateDelete" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserDel4">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                End Date
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNewsEndDateDelete" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserDel5">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                News Summary<br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="TextNewsSummaryDelete" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserDel6">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
                &nbsp;
            </td>
            <td bgcolor="#ffffff">
                News Content<br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="TextNewsContentDelete" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr class="formText" id="UserDel7">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
                &nbsp;
            </td>
            <td bgcolor="#ffffff">
                Attachment
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :
            </td>
            <td bgcolor="#ffffff">
            <table>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="GridAttachDel" runat="server" AutoGenerateColumns="False"
                            CellPadding="4" ForeColor="Black" GridLines="Vertical" DataKeyNames="PK_NewsAttachment"
                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" 
                            BorderWidth="1px" EnableModelValidation="True">
                            <FooterStyle BackColor="#CCCC99" />
                            <Columns>
                                <asp:BoundField HeaderText="PK_NewsAttachment" Visible="False"
                                    DataField="PK_NewsAttachment">
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Attach File">
                                    <ItemTemplate>
                                        <asp:DataList ID="DataList1" runat="server">
                                            <ItemTemplate>
                                                <%#GenerateLink(Eval("PK_NewsAttachment"), Eval("Filename"), Eval("Fk_NewsApprovalId"))%>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </td>
                </tr>
                </table>
            </td>
        </tr>
        <tr class="formText" id="Button11" bgcolor="#dddddd" height="30">
            <td style="width: 22px">
                <img height="15" src="images/arrow.gif" width="15">
            </td>
            <td colspan="7">
                <table cellspacing="0" cellpadding="3" border="0">
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImageAccept" runat="server" CausesValidation="False" SkinID="AcceptButton">
                            </asp:ImageButton>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton">
                            </asp:ImageButton>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton">
                            </asp:ImageButton>
                        </td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="none"></asp:CustomValidator>
            </td>
        </tr>
    </table>
</asp:Content>
