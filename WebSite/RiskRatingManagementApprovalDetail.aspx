<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="RiskRatingManagementApprovalDetail.aspx.vb" Inherits="RiskRatingManagementApprovalDetail" title="Risk Rating Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <TABLE id="TableDetail" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="2" runat="server">
	    <TR class="formText" id="UserAdd1">
		    <TD bgColor="#ffffff" height="24" style="width: 22px">&nbsp;</TD>
		    <TD bgColor="#ffffff">
                Risk Rating Name</TD>
		    <TD bgColor="#ffffff" style="width: 44px">:</TD>
		    <TD width="80%" bgColor="#ffffff"><asp:label id="LabelRiskRatingNameAdd" runat="server"></asp:label></TD>
	    </TR>
        <tr class="formText" id="UserAdd2">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                Description<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" width="80%">
                <asp:TextBox ID="TextRiskRatingDescriptionAdd" runat="server" MaxLength="255" ReadOnly="True"
                    Rows="5" TextMode="MultiLine" Width="312px"></asp:TextBox></td>
        </tr>
	    <tr class="formText" id="UserAdd3">
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Risk Score</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td bgcolor="#ffffff">
                From
                <asp:Label ID="LblScoreFromAdd" runat="server"></asp:Label>
                to
                <asp:label id="LblScoreToAdd" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserEdi1">
		    <td height="24" colspan="4" bgcolor="#ffffcc">&nbsp;Old Values</td>
		    <td colspan="4" bgcolor="#ffffcc">&nbsp;New Values</td>
	    </tr>
	    <tr class="formText" id="UserEdi2">
		    <td bgcolor="#ffffff" style="width: 22px; height: 32px">&nbsp;</td>
		    <td bgcolor="#ffffff" style="height: 32px;">
                Risk Rating ID</td>
		    <td bgcolor="#ffffff" style="width: 44px; height: 32px;">:</td>
		    <td width="40%" bgcolor="#ffffff" style="height: 32px"><asp:label id="LabelRiskRatingIDEditOldRiskRatingID" runat="server"></asp:label></td>
		    <td width="5" bgcolor="#ffffff" style="height: 32px">&nbsp;</td>
		    <td width="10%" bgcolor="#ffffff" style="height: 32px">
                Risk Rating ID</td>
		    <td width="5" bgcolor="#ffffff" style="height: 32px">:</td>
		    <td width="40%" bgcolor="#ffffff" style="height: 32px"><asp:label id="LabelRiskRatingIDEditNewRiskRatingID" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserEdi3">
		    <td bgcolor="#ffffff" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Risk Rating Name</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelRiskRatingNameEditOldRiskRatingName" runat="server"></asp:label></td>
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Risk Rating Name</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelRiskRatingEditNewRiskRatingName" runat="server"></asp:label></td>
	    </tr>
        <tr class="formText" id="UserEdi4">
            <td bgcolor="#ffffff" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                Description<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff">
                <asp:TextBox ID="TextRiskRatingDescriptionEditOldRiskRatingDescription" runat="server"
                    MaxLength="255" ReadOnly="True" Rows="5" TextMode="MultiLine" Width="312px"></asp:TextBox></td>
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff">
                Description<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff">
                <asp:TextBox ID="TextRiskRatingEditNewRiskRatingDescription" runat="server" MaxLength="255"
                    ReadOnly="True" Rows="5" TextMode="MultiLine" Width="312px"></asp:TextBox></td>
        </tr>
	    <tr class="formText" id="UserEdi5">
		    <td bgcolor="#ffffff" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Risk Score</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td bgcolor="#ffffff">
                From
                <asp:Label ID="LblOldScoreFrom" runat="server"></asp:Label>
                to
                <asp:Label ID="LblOldScoreTo" runat="server"></asp:Label></td>
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Risk Score</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff">
                From
                <asp:Label ID="LblNewScoreFrom" runat="server"></asp:Label>
                to
                <asp:Label ID="LblNewScoreTo" runat="server"></asp:Label></td>
	    </tr>
	    <TR class="formText" id="UserDel1">
		    <TD bgColor="#ffffff" style="height: 23px; width: 22px;">&nbsp;</TD>
		    <TD bgColor="#ffffff" style="height: 23px;">
                Risk Rating ID</TD>
		    <TD bgColor="#ffffff" style="height: 23px; width: 44px;">:</TD>
		    <TD width="80%" bgColor="#ffffff" style="height: 23px"><asp:label id="LabelRiskRatingDeleteRiskRatingID" runat="server"></asp:label></TD>
	    </TR>
	    <tr class="formText" id="UserDel2">
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Risk Rating Name</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelRiskRatingDeleteRiskRatingName" runat="server"></asp:label></td>
	    </tr>
        <tr class="formText" id="UserDel3">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                Description<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff">
                <asp:TextBox ID="TextRiskRatingDeleteRiskRatingDescription" runat="server" MaxLength="255"
                    ReadOnly="True" Rows="5" TextMode="MultiLine" Width="312px"></asp:TextBox></td>
        </tr>
	    <tr class="formText" id="UserDel4">
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Risk Score</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td bgcolor="#ffffff">
                From
                <asp:Label ID="LblScoreFromDelete" runat="server"></asp:Label>
                to
                <asp:Label ID="LblScoreToDelete" runat="server"></asp:Label></td>
	    </tr>
	    <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="True" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="none"></asp:CustomValidator></TD>
	    </TR>
    </TABLE>
</asp:Content>