<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="ChangePassword.aspx.vb" Inherits="ChangePassword" title="Change Password" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="231" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4">
                <strong><span style="font-size: 18px">Change Password</span></strong><br />
                <hr />
            </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4"><span style="color: #ff0000">* Required</span></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" colspan="2">
                <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="99%">
                <table style="width: 100%; height: 81px" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 82px; height: 30px">
                        </td>
                        <td style="height: 30px; width: 20%;">
                    User ID</td>
                          <td style="width: 2%; height: 30px" align="center">
                            :
                        </td>
                        <td style="width: 75%; height: 30px">
                    &nbsp;<asp:Label ID="LabelUserId" runat="server" Width="174px" Font-Bold="True" ForeColor="#FF8000"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 82px; height: 30px">
                        </td>
                        <td style="width: 20%; height: 30px">
                User Name</td>
                        <td style="width: 2%; height: 30px" align="center">
                            :
                        </td>
                        <td style="width: 75%; height: 30px">
                        &nbsp;<asp:Label ID="LabelUserName" runat="server" Width="174px" Font-Bold="True" ForeColor="#FF8000"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 82px; height: 30px">
                <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ErrorMessage="Old Password is required" Display="Dynamic"
					ControlToValidate="TextOldPassword">*</asp:requiredfieldvalidator></td>
                        <td style="width: 20%; height: 30px">
                            Old Password</td>
                        <td align="center" style="width: 2%; height: 30px">
                            :</td>
                        <td style="width: 80%; height: 30px">
                            &nbsp;<asp:TextBox ID="TextOldPassword" runat="server" CssClass="textBox" MaxLength="50"
                                TextMode="Password"></asp:TextBox>
                            <strong><span style="color: #ff0000">*</span></strong></td>
                    </tr>
                    <tr>
                        <td style="width: 82px; height: 30px">
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="TextNewPassword"
                                Display="Dynamic" ErrorMessage="New Password is required">*</asp:RequiredFieldValidator></td>
                        <td style="width: 20%; height: 30px">
                            New
                            Password</td>
                        <td style="width: 2%; height: 30px" align="center">
                            :
                        </td>
                        <td style="width: 80%; height: 30px">
                            &nbsp;<asp:textbox id="TextNewPassword" runat="server" CssClass="textBox" MaxLength="50" TextMode="Password"></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong></td>
                    </tr>
                    <tr>
                        <td style="width: 82px; height: 30px">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxRetype"
                                Display="Dynamic" ErrorMessage="New Password Confirmation is required">*</asp:RequiredFieldValidator><br />
                <asp:comparevalidator id="CompareValidator1" runat="server" ErrorMessage="Your entry in the Confirm Password textbox must be the same as the entry in the Password textbox " Display="Dynamic"
					ControlToValidate="TextBoxRetype" ControlToCompare="TextNewPassword">*</asp:comparevalidator></td>
                        <td style="width: 20%; height: 30px">
                Confirm New Password</td>
                        <td style="width: 2%; height: 30px" align="center">
                            :
                        </td>
                        <td style="width: 80%; height: 30px">
                            &nbsp;<asp:textbox id="TextBoxRetype" runat="server" CssClass="textBox" MaxLength="50" TextMode="Password"></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong></td>
                    </tr>
                   <tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 82px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="saveButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>              
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
                </table><strong><span style="color: #ff0000">             
                </span></strong>              
            </ajax:AjaxPanel>
            </td>
        </tr>		
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="2" height="24">
            </td>
        </tr>
	</table>
	<script>
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>

