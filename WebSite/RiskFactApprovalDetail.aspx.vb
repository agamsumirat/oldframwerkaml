Imports System.Data.SqlClient
Partial Class RiskFactApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Integer
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get DataUpdatingParameterApprovalID
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    '''  [Hendra] 12:24 PM 10/7/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property RiskFactPendingApprovalID() As Int64
        Get
            Dim Temp As String = String.Empty
            Temp = Me.Request.Params("RiskFactPendingApprovalID")
            If Temp = "" OrElse Not IsNumeric(Temp) Then
                Throw New Exception("Pending Approval ID not valid or already deleted")
                'cek id yang direquest masih ada atau tidak di database kalau tidak ada maka throw error
            ElseIf IsNumeric(Temp) Then
                Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                    Dim orows() As AMLDAL.RiskFact.RiskFactPendingApprovalRow = adapter.GetRiskFactPendingApprovalByPK(Temp).Select
                    If orows.Length = 0 Then
                        Throw New Exception("Pending Approval ID not exist  or already deleted")
                    Else
                        Return Temp
                    End If
                End Using
            End If
        End Get
    End Property

    Private ReadOnly Property GetHeaderRiskFactPendingApproval() As AMLDAL.RiskFact.RiskFactPendingApprovalRow
        Get
            If Session("RiskFactApprovalHeaderRow") Is Nothing OrElse CType(Session("RiskFactApprovalHeaderRow"), AMLDAL.RiskFact.RiskFactPendingApprovalRow).PKRiskFactPendingApprovalID <> Me.RiskFactPendingApprovalID Then
                Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                    Dim objDatarow As AMLDAL.RiskFact.RiskFactPendingApprovalRow = adapter.GetRiskFactPendingApprovalByPK(Me.RiskFactPendingApprovalID).Rows(0)
                    If Not objDatarow Is Nothing Then
                        Using adapter1 As New AMLDAL.WorkingAssignmentTableAdapters.UserWorkingUnitAssignmentTableAdapter
                            If adapter1.GetUserWorkingUnitAssignmentByUserID(Sahassa.AML.Commonly.SessionUserId).Select("userid=" & objDatarow.RiskFactUserID).Length = 0 Then
                                'Ngak punya hak jadi harus di redirect balik
                                Response.Redirect("RiskFactApproval.aspx", False)
                            Else
                                Return objDatarow
                            End If
                        End Using
                    Else
                        Throw New Exception("Pending Approval ID not exist or already deleted")
                    End If
                End Using
            ElseIf CType(Session("RiskFactApprovalHeaderRow"), AMLDAL.RiskFact.RiskFactPendingApprovalRow).PKRiskFactPendingApprovalID = Me.RiskFactPendingApprovalID Then
                Return Session("RiskFactApprovalHeaderRow")


            End If
        End Get
    End Property
    Private Sub AbandonSession()
        Session("RiskFactApprovalHeaderRow") = Nothing
        Session("RiskFactApprovalDetailRow") = Nothing
    End Sub

    Private ReadOnly Property GetDetailRiskFact() As AMLDAL.RiskFact.RiskFactApprovalRow
        Get
            If Session("RiskFactApprovalDetailRow") Is Nothing OrElse CType(Session("RiskFactApprovalDetailRow"), AMLDAL.RiskFact.RiskFactApprovalRow).FKRiskFactPendingApprovalID <> Me.RiskFactPendingApprovalID Then

                Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactApprovalTableAdapter
                    Dim objDatarow As AMLDAL.RiskFact.RiskFactApprovalRow = adapter.GetRiskFactApprovalByFK(Me.RiskFactPendingApprovalID).Rows(0)
                    If Not objDatarow Is Nothing Then
                        Return objDatarow
                    Else
                        Throw New Exception("Pending Approval ID not exist or already deleted")
                    End If
                End Using
            ElseIf CType(Session("RiskFactApprovalDetailRow"), AMLDAL.RiskFact.RiskFactApprovalRow).FKRiskFactPendingApprovalID = Me.RiskFactPendingApprovalID Then
                Return Session("RiskFactApprovalDetailRow")
            End If
        End Get
    End Property
#End Region

#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HandleTampilan()

        Select Case Me.ParamType
            Case TypeConfirm.Add

                Me.TableDetailAdd.Visible = True
                Me.TableDetailEdit.Visible = False
                Me.TableDetailDelete.Visible = False
            Case TypeConfirm.Edit
                Me.TableDetailAdd.Visible = False
                Me.TableDetailEdit.Visible = True
                Me.TableDetailDelete.Visible = False
            Case TypeConfirm.Delete
                Me.TableDetailAdd.Visible = False
                Me.TableDetailEdit.Visible = False
                Me.TableDetailDelete.Visible = True
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select


    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load LoadDataAdvancedRulesAdd
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 1:04 PM 10/7/2007 created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadDataRiskFactAdd()
        Me.LabelTitle.Text = "Activity: Add Data Risk Fact"
        If Not GetDetailRiskFact Is Nothing Then
            Me.LabelRiskFactNameAdd.Text = GetDetailRiskFact.RiskFactName
            Me.LabelDescriptionAdd.Text = GetDetailRiskFact.RiskFactDescription
            Me.LabelEnabledAdd.Text = GetDetailRiskFact.Enable
            Me.LabelRiskScoreAdd.Text = GetDetailRiskFact.RiskScore
            Me.LabelSQLExpressionAdd.Text = GetDetailRiskFact.RiskFactExpression
        End If

    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load LoadDataAdvancedRulesEdit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 1:30 PM 10/7/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadDataRiskFactEdit()
        Me.LabelTitle.Text = "Activity: Edit Data Risk Fact"
        If Not GetDetailRiskFact Is Nothing Then
            Me.LabelRiskFactNameEditOld.Text = GetDetailRiskFact.RiskFactName_Old
            Me.LabelDescriptionEditOld.Text = GetDetailRiskFact.RiskFactDescription_Old
            Me.LabelEnabledEditOld.Text = GetDetailRiskFact.Enable_Old
            Me.LabelRiskScoreEditOld.Text = GetDetailRiskFact.RiskScore_Old
            Me.LabelSQLExpressionEditOld.Text = GetDetailRiskFact.RiskFactExpression_Old
            Me.LabelRiskFactNameEditNew.Text = GetDetailRiskFact.RiskFactName
            Me.LabelDescriptionEditNew.Text = GetDetailRiskFact.RiskFactDescription
            Me.LabelEnabledEditNew.Text = GetDetailRiskFact.Enable
            Me.LabelRiskScoreEditNew.Text = GetDetailRiskFact.RiskScore
            Me.LabelSQLExpressionEditNew.Text = GetDetailRiskFact.RiskFactExpression

        End If
    End Sub
    Private Sub LoadDataRiskFactDelete()
        Me.LabelTitle.Text = "Activity: Delete Data Risk Fact"
        If Not GetDetailRiskFact Is Nothing Then
            Me.LabelRiskFactNameDelete.Text = GetDetailRiskFact.RiskFactName
            Me.LabelDescriptionDelete.Text = GetDetailRiskFact.RiskFactDescription
            Me.LabelEnabledDelete.Text = GetDetailRiskFact.Enable
            Me.LabelRiskScoreDelete.Text = GetDetailRiskFact.RiskScore
            Me.LabelSQLExpressionDelete.Text = GetDetailRiskFact.RiskFactExpression
        End If
    End Sub
#End Region

#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' AcceptDataRiskFactAdd add accept
    ''' </summary>
    ''' <remarks>
    ''' 1.insert data Risk Fact ke table RiskFact
    ''' 2.insert data ke audittrail
    ''' 3.delete data dari table RiskFactpendingapproval dan RiskFactapproval
    ''' 
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 10:35 AM 10/10/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptDataRiskFactAdd()
        Dim oSQLTrans As System.Data.SqlClient.SqlTransaction = Nothing
        Try

            If Not Me.GetDetailRiskFact Is Nothing Then
                'Step 1
                Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactTableAdapter
                    oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                    adapter.Insert(GetDetailRiskFact.RiskFactName, GetDetailRiskFact.RiskFactDescription, GetDetailRiskFact.RiskFactExpression, GetDetailRiskFact.RiskScore, GetDetailRiskFact.Enable)

                    Dim strpreparer As String = ""
                    Using adapter3 As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                        Dim objdata As AMLDAL.RiskFact.RiskFactPendingApprovalDataTable = adapter3.GetRiskFactPendingApprovalByPK(Me.RiskFactPendingApprovalID)
                        If Not objdata Is Nothing Then
                            strpreparer = objdata.Rows(0).Item("RiskFactUserID").ToString
                        End If
                    End Using

                    'Step 2
                    InsertAuditTrailAdd(oSQLTrans, strpreparer)
                    'Step 3
                    Using adapter1 As New AMLDAL.RiskFactTableAdapters.RiskFactApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(adapter1, oSQLTrans)
                        adapter1.DeleteRiskFactApprovalByFK(Me.RiskFactPendingApprovalID)

                        Using adapter2 As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter

                            adapter2.DeleteRiskFactPendingApprovalByPK(Me.RiskFactPendingApprovalID)
                        End Using
                    End Using
                    oSQLTrans.Commit()
                    AbandonSession()
                End Using
            End If

        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If

            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    Private Sub InsertAuditTrailAdd(ByRef osqlTrans As SqlTransaction, preparer As String)
        Try

            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
            If Not Me.GetDetailRiskFact Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, osqlTrans)
                    AccessAudit.Insert(Now, preparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Fact Name", "Add", "", Me.GetDetailRiskFact.RiskFactName, "Accepted")
                    AccessAudit.Insert(Now, preparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Description", "Add", "", Me.GetDetailRiskFact.RiskFactDescription, "Accepted")
                    AccessAudit.Insert(Now, preparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Enabled", "Add", "", Me.GetDetailRiskFact.Enable, "Accepted")
                    AccessAudit.Insert(Now, preparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Score", "Add", "", Me.GetDetailRiskFact.RiskScore, "Accepted")
                    AccessAudit.Insert(Now, preparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Expression", "Add", "", Me.GetDetailRiskFact.RiskFactExpression, "Accepted")
                    AccessAudit.Insert(Now, preparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk FactCreated Date", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                End Using
            End If
        Catch
            Throw
        End Try
    End Sub
    Private Sub InsertAuditTrailEdit(ByRef osqlTrans As SqlTransaction, strpreparer As String)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
            If Not Me.GetDetailRiskFact Is Nothing Then

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, osqlTrans)
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Fact Name", "Edit", Me.GetDetailRiskFact.RiskFactName_Old, Me.GetDetailRiskFact.RiskFactName, "Accepted")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Description", "Edit", Me.GetDetailRiskFact.RiskFactDescription_Old, Me.GetDetailRiskFact.RiskFactDescription, "Accepted")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Enabled", "Edit", Me.GetDetailRiskFact.Enable_Old, Me.GetDetailRiskFact.Enable, "Accepted")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Score", "Edit", Me.GetDetailRiskFact.RiskScore_Old, Me.GetDetailRiskFact.RiskScore, "Accepted")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Expression", "Edit", Me.GetDetailRiskFact.RiskFactExpression_Old, Me.GetDetailRiskFact.RiskFactExpression, "Accepted")
                End Using
            End If
        Catch
            Throw
        End Try
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' DataUpdatingParameter edit accept
    ''' </summary>
    ''' <remarks>
    ''' Step
    ''' 1.Update Data RiskFact
    ''' 2.Insert Data ke AuditTrail
    ''' 3.Delete Data dari table RiskFactApproval dan RiskFactPendingApproval
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 10:22 PM 10/7/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptRiskFactRulesEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            'Step 1
            Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                adapter.UpdateRiskFactByRiskFactID(Me.GetDetailRiskFact.RiskFactName, Me.GetDetailRiskFact.RiskFactDescription, Me.GetDetailRiskFact.RiskFactExpression, Me.GetDetailRiskFact.RiskScore, Me.GetDetailRiskFact.Enable, Me.GetDetailRiskFact.Pk_RiskFact_ID)


                Dim strpreparer As String = ""
                Using adapter3 As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                    Dim objdata As AMLDAL.RiskFact.RiskFactPendingApprovalDataTable = adapter3.GetRiskFactPendingApprovalByPK(Me.RiskFactPendingApprovalID)
                    If Not objdata Is Nothing Then
                        strpreparer = objdata.Rows(0).Item("RiskFactUserID").ToString
                    End If
                End Using


                'Step2
                InsertAuditTrailEdit(oSQLTrans, strpreparer)
                'Step3
                Using adapter1 As New AMLDAL.RiskFactTableAdapters.RiskFactApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter1, oSQLTrans)
                    adapter1.DeleteRiskFactApprovalByFK(Me.RiskFactPendingApprovalID)

                    Using adapter2 As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(adapter2, oSQLTrans)
                        adapter2.DeleteRiskFactPendingApprovalByPK(Me.RiskFactPendingApprovalID)
                    End Using

                End Using
                oSQLTrans.Commit()
                AbandonSession()
            End Using

        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try

    End Sub
    ''' <summary>
    ''' Accept Delete riskFact
    ''' </summary>
    ''' <remarks>
    ''' Step 
    ''' 1.Delete Data riskFact
    ''' 2.Insert AuditTrail
    ''' 3.Delete Data dari table riskFactApproval dan riskFactPendingApproval
    ''' </remarks>
    ''' <history>
    ''' [Hendra] 11:22 AM 10/10/2007
    ''' </history>
    Private Sub AcceptRiskFactRulesDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            'Step 1
            Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                adapter.DeleteRiskFactByPK(Me.GetDetailRiskFact.Pk_RiskFact_ID)


                Dim strpreparer As String = ""
                Using adapter3 As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                    Dim objdata As AMLDAL.RiskFact.RiskFactPendingApprovalDataTable = adapter3.GetRiskFactPendingApprovalByPK(Me.RiskFactPendingApprovalID)
                    If Not objdata Is Nothing Then
                        strpreparer = objdata.Rows(0).Item("RiskFactUserID").ToString
                    End If
                End Using

                'Step 2
                InsertAuditTrailDelete(oSQLTrans, strpreparer)
                'Step3
                Using adapter1 As New AMLDAL.RiskFactTableAdapters.RiskFactApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter1, oSQLTrans)
                    adapter1.DeleteRiskFactApprovalByFK(Me.RiskFactPendingApprovalID)

                    Using adapter2 As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(adapter2, oSQLTrans)
                        adapter2.DeleteRiskFactPendingApprovalByPK(Me.RiskFactPendingApprovalID)
                    End Using
                End Using
                oSQLTrans.Commit()
                AbandonSession()
            End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            LogError(ex)
            oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If

        End Try
    End Sub
    Private Sub InsertAuditTrailDelete(ByRef oSqlTrans As SqlTransaction, strpreparer As String)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
            If Not Me.GetDetailRiskFact Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSqlTrans)
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Fact Name", "Delete", "", Me.GetDetailRiskFact.RiskFactName, "Accepted")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Description", "Delete", "", Me.GetDetailRiskFact.RiskFactDescription, "Accepted")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Enabled", "Delete", "", Me.GetDetailRiskFact.Enable, "Accepted")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Score", "Delete", "", Me.GetDetailRiskFact.RiskScore, "Accepted")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Expression", "Delete", "", Me.GetDetailRiskFact.RiskFactExpression, "Accepted")


                End Using
            End If
        Catch
            Throw
        End Try
    End Sub
#End Region

#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject Data Risk Fact add
    ''' </summary>
    ''' <remarks>
    ''' Step 
    '''
    '''1.Delete Data dari table Risk FactApproval dan Risk FactPendingApproval
    '''2.Insert Audit Trail
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 11:41 AM 10/10/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectDataRiskFactAdd()
        Dim oSQLTrans As System.Data.SqlClient.SqlTransaction = Nothing
        Try

            'Step 1
            Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                adapter.DeleteRiskFactApprovalByFK(Me.RiskFactPendingApprovalID)


                Dim strpreparer As String = ""
                Using adapter3 As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                    Dim objdata As AMLDAL.RiskFact.RiskFactPendingApprovalDataTable = adapter3.GetRiskFactPendingApprovalByPK(Me.RiskFactPendingApprovalID)
                    If Not objdata Is Nothing Then
                        strpreparer = objdata.Rows(0).Item("RiskFactUserID").ToString
                    End If
                End Using


                InsertAuditTrailRejectAdd(oSQLTrans, strpreparer)
                Using adapter1 As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter1, oSQLTrans)
                    adapter1.DeleteRiskFactPendingApprovalByPK(Me.RiskFactPendingApprovalID)
                End Using

                'Step 2

                oSQLTrans.Commit()
                AbandonSession()
            End Using

            Me.Response.Redirect("riskfactApproval.aspx", False)

        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If

        End Try
    End Sub
    ''' <summary>
    ''' insert audit trail reject add 
    ''' </summary>
    ''' <param name="oSqlTrans"></param>
    ''' <remarks></remarks>
    ''' <history>
    ''' [Hendra] 10:30 AM 10/11/2007
    ''' </history>
    Private Sub InsertAuditTrailRejectAdd(ByRef oSqlTrans As SqlTransaction, strpreparer As String)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
            If Not Me.GetDetailRiskFact Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSqlTrans)
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Fact Name", "Add", "", Me.GetDetailRiskFact.RiskFactName, "Rejected")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Description", "Add", "", Me.GetDetailRiskFact.RiskFactDescription, "Rejected")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Enabled", "Add", "", Me.GetDetailRiskFact.Enable, "Rejected")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Score", "Add", "", Me.GetDetailRiskFact.RiskScore, "Rejected")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Expression", "Add", "", Me.GetDetailRiskFact.RiskFactExpression, "Rejected")

                End Using
            End If
        Catch ex As Exception
            Throw
        End Try


    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' RejectDataRiskFactRejectEdit reject edit
    ''' </summary>
    ''' <remarks>
    ''' Step 
    ''' 
    ''' 1.Delete Data dari table RiskFactApproval dan RiskFactPendingApproval
    ''' 2.Insert AuditTrail
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 10:31 AM 10/11/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectDataRiskFactRejectEdit()
        Dim oSQLTrans As System.Data.SqlClient.SqlTransaction = Nothing
        Try
            'Step 1
            Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                adapter.DeleteRiskFactApprovalByFK(Me.RiskFactPendingApprovalID)

                Dim strpreparer As String = ""
                Using adapter3 As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                    Dim objdata As AMLDAL.RiskFact.RiskFactPendingApprovalDataTable = adapter3.GetRiskFactPendingApprovalByPK(Me.RiskFactPendingApprovalID)
                    If Not objdata Is Nothing Then
                        strpreparer = objdata.Rows(0).Item("RiskFactUserID").ToString
                    End If
                End Using


                InsertAuditTrailRejectEdit(oSQLTrans, strpreparer)
                Using adapter2 As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter2, oSQLTrans)
                    adapter2.DeleteRiskFactPendingApprovalByPK(Me.RiskFactPendingApprovalID)
                End Using
                'Step 2

                oSQLTrans.Commit()
                AbandonSession()
            End Using
            Me.Response.Redirect("RiskFactApproval.aspx", False)
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If


        End Try

    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' RejectDataRulesAdvancedRejectDelete reject Delete
    ''' </summary>
    ''' <remarks>
    ''' Step 
    ''' 1.Insert AuditTrail
    ''' 2.Delete Data dari table RulesAdvancedApproval dan RulesAdvancedPendingApproval
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 11:02 PM 10/7/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectDataRiskFactRejectDelete()
        Dim oSQLTrans As System.Data.SqlClient.SqlTransaction = Nothing
        Try
            Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                adapter.DeleteRiskFactApprovalByFK(Me.RiskFactPendingApprovalID)
                Dim strpreparer As String = ""
                Using adapter3 As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                    Dim objdata As AMLDAL.RiskFact.RiskFactPendingApprovalDataTable = adapter3.GetRiskFactPendingApprovalByPK(Me.RiskFactPendingApprovalID)
                    If Not objdata Is Nothing Then
                        strpreparer = objdata.Rows(0).Item("RiskFactUserID").ToString
                    End If
                End Using


                InsertAuditTrailRejectDelete(oSQLTrans, strpreparer)
                Using adapter2 As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter2, oSQLTrans)
                    adapter2.DeleteRiskFactPendingApprovalByPK(Me.RiskFactPendingApprovalID)
                End Using



                oSQLTrans.Commit()
                AbandonSession()
            End Using

            Me.Response.Redirect("RiskFactApproval.aspx", False)
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try

    End Sub

#End Region
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="oSQLTrans"></param>
    ''' <remarks></remarks>
    Private Sub InsertAuditTrailRejectEdit(ByRef oSQLTrans As SqlTransaction, strpreparer As String)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
            If Not Me.GetDetailRiskFact Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Fact Name", "Edit", Me.GetDetailRiskFact.RiskFactName_Old, Me.GetDetailRiskFact.RiskFactName, "Rejected")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Description", "Edit", Me.GetDetailRiskFact.RiskFactDescription_Old, Me.GetDetailRiskFact.RiskFactDescription, "Rejected")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Enabled", "Edit", Me.GetDetailRiskFact.Enable_Old, Me.GetDetailRiskFact.Enable, "Rejected")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Score", "Edit", Me.GetDetailRiskFact.RiskScore_Old, Me.GetDetailRiskFact.RiskScore, "Rejected")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Expression", "Edit", Me.GetDetailRiskFact.RiskFactExpression_Old, Me.GetDetailRiskFact.RiskFactExpression, "Rejected")
                End Using
            End If
        Catch ex As Exception
            Throw
        End Try


    End Sub
    ''' <summary>
    ''' InsertAuditTrailRejectDelete
    ''' </summary>
    ''' <param name="oSQLTrans"></param>
    ''' <remarks></remarks>
    ''' <history>
    ''' [Hendra]10:33 AM 10/11/2007
    ''' </history>
    Private Sub InsertAuditTrailRejectDelete(ByRef oSQLTrans As SqlTransaction, strpreparer As String)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
            If Not Me.GetDetailRiskFact Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Fact Name", "Delete", "", Me.GetDetailRiskFact.RiskFactName, "Rejected")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Description", "Delete", "", Me.GetDetailRiskFact.RiskFactDescription, "Rejected")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Enabled", "Delete", "", Me.GetDetailRiskFact.Enable, "Rejected")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Score", "Delete", "", Me.GetDetailRiskFact.RiskScore, "Rejected")
                    AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Expression", "Delete", "", Me.GetDetailRiskFact.RiskFactExpression, "Rejected")
                End Using
            End If
        Catch ex As Exception
            Throw
        End Try


    End Sub





    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HandleTampilan()
                Select Case Me.ParamType
                    Case TypeConfirm.Add
                        Me.LoadDataRiskFactAdd()
                    Case TypeConfirm.Edit
                        Me.LoadDataRiskFactEdit()
                    Case TypeConfirm.Delete
                        Me.LoadDataRiskFactDelete()
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case TypeConfirm.Add
                    Me.AcceptDataRiskFactAdd()
                Case TypeConfirm.Edit
                    Me.AcceptRiskFactRulesEdit()
                Case TypeConfirm.Delete
                    Me.AcceptRiskFactRulesDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "RiskFactApproval.aspx"

            Me.Response.Redirect("RiskFactApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case TypeConfirm.Add
                    Me.RejectDataRiskFactAdd()
                Case TypeConfirm.Edit
                    Me.RejectDataRiskFactRejectEdit()
                Case TypeConfirm.Delete
                    Me.RejectDataRiskFactRejectDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "RiskFactApproval.aspx"

            Me.Response.Redirect("RiskFactApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "RiskFactApproval.aspx"
        AbandonSession()

        Me.Response.Redirect("RiskFactApproval.aspx", False)
    End Sub
    Public Enum TypeConfirm
        Add = 1
        Edit
        Delete
    End Enum

End Class
