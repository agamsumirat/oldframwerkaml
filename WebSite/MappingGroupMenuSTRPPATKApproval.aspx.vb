Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Imports System.IO
Imports System.Collections.Generic

Partial Class MappingGroupMenuSTRPPATKApproval
    Inherits Parent

    Public Property PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID() As String
        Get
            Return CType(Session("MappingGroupMenuSTRPPATKApproval.PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID"), String)
        End Get
        Set(ByVal value As String)
            Session("MappingGroupMenuSTRPPATKApproval.PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID") = value
        End Set
    End Property

    Public Property SetnGetGroupName() As String
        Get
            If Not Session("MappingGroupMenuSTRPPATKApproval.SetnGetGroupName") Is Nothing Then
                Return CStr(Session("MappingGroupMenuSTRPPATKApproval.SetnGetGroupName"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingGroupMenuSTRPPATKApproval.SetnGetGroupName") = value
        End Set
    End Property

    Public Property SetnGetMode() As String
        Get
            If Not Session("MappingGroupMenuSTRPPATKApproval.SetnGetMode") Is Nothing Then
                Return CStr(Session("MappingGroupMenuSTRPPATKApproval.SetnGetMode"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingGroupMenuSTRPPATKApproval.SetnGetMode") = value
        End Set
    End Property

    Public Property SetnGetPreparer() As String
        Get
            If Not Session("MappingGroupMenuSTRPPATKApproval.SetnGetPreparer") Is Nothing Then
                Return CStr(Session("MappingGroupMenuSTRPPATKApproval.SetnGetPreparer"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingGroupMenuSTRPPATKApproval.SetnGetPreparer") = value
        End Set
    End Property

    Public Property SetnGetEntryDateFrom() As String
        Get
            If Not Session("MappingGroupMenuSTRPPATKApproval.CreatedDate") Is Nothing Then
                Return CStr(Session("MappingGroupMenuSTRPPATKApproval.CreatedDate"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingGroupMenuSTRPPATKApproval.CreatedDate") = value
        End Set
    End Property

    Public Property SetnGetEntryDateUntil() As String
        Get
            If Not Session("MappingGroupMenuSTRPPATKApproval.CreatedDateUntil") Is Nothing Then
                Return CStr(Session("MappingGroupMenuSTRPPATKApproval.CreatedDateUntil"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingGroupMenuSTRPPATKApproval.CreatedDateUntil") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("MappingGroupMenuSTRPPATKApproval.Sort") Is Nothing, "GroupName  asc", Session("MappingGroupMenuSTRPPATKApproval.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("MappingGroupMenuSTRPPATKApproval.Sort") = Value
        End Set
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("MappingGroupMenuSTRPPATKApproval.RowTotal") Is Nothing, 0, Session("MappingGroupMenuSTRPPATKApproval.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MappingGroupMenuSTRPPATKApproval.RowTotal") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("MappingGroupMenuSTRPPATKApproval.CurrentPage") Is Nothing, 0, Session("MappingGroupMenuSTRPPATKApproval.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MappingGroupMenuSTRPPATKApproval.CurrentPage") = Value
        End Set
    End Property

    Public Shared Function FORMAT_DATE_BEGIN_GREATER_THAN_DATE_END(ByVal strTanggal As String) As String
        If Not Sahassa.AML.Commonly.SessionLanguage Is Nothing Then
            If Sahassa.AML.Commonly.SessionLanguage.ToLower = "en-us" Then
                Return "Start Date must Earlier than End Date for " & strTanggal
            ElseIf Sahassa.AML.Commonly.SessionLanguage.ToLower = "id-id" Then
                Return "Tanggal Mulai harus lebih Muda dari pada Tanggal Akhir untuk " & strTanggal
            Else
                Return "Start Date must Earlier than End Date for " & strTanggal
            End If

        Else
            Return "Start Date must Earlier than End Date for " & strTanggal
        End If
    End Function

    Function Getvw_MappingGroupMenuSTRPPATKApproval(ByVal strWhereClause As String, ByVal strOrderBy As String, ByVal intStart As Integer, ByVal intPageLength As Integer, ByRef intCount As Integer) As VList(Of vw_MappingGroupMenuSTRPPATKApproval)
        Dim Objvw_MappingGroupMenuSTRPPATKApproval As VList(Of vw_MappingGroupMenuSTRPPATKApproval) = Nothing
        Objvw_MappingGroupMenuSTRPPATKApproval = DataRepository.vw_MappingGroupMenuSTRPPATKApprovalProvider.GetPaged(strWhereClause, strOrderBy, intStart, intPageLength, intCount)
        Return Objvw_MappingGroupMenuSTRPPATKApproval
    End Function

    Public Property SetnGetBindTable() As VList(Of vw_MappingGroupMenuSTRPPATKApproval)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            If SetnGetGroupName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "GroupName like '%" & SetnGetGroupName.Trim.Replace("'", "''") & "%'"
            End If

            If SetnGetMode.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Nama like '%" & SetnGetMode.Trim.Replace("'", "''") & "%'"
            End If

            If SetnGetPreparer.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Preparer like '%" & SetnGetPreparer.Trim.Replace("'", "''") & "%'"
            End If

            If SetnGetEntryDateFrom.Length > 0 AndAlso SetnGetEntryDateUntil.Length > 0 Then

                If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetEntryDateFrom) AndAlso Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetEntryDateFrom) Then

                    Dim tanggal As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateFrom).ToString("yyyy-MM-dd")
                    Dim tanggalAkhir As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateUntil).ToString("yyyy-MM-dd")
                    If DateDiff(DateInterval.Day, Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateFrom), Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateUntil)) > -1 Then

                        ReDim Preserve strWhereClause(strWhereClause.Length)

                        strWhereClause(strWhereClause.Length - 1) = vw_MappingGroupMenuSTRPPATKApprovalColumn.CreatedDate.ToString & " between '" & tanggal & " 00:00:00' and '" & tanggalAkhir & " 23:59:59'"
                    Else
                        Throw New SahassaException(FORMAT_DATE_BEGIN_GREATER_THAN_DATE_END("CreatedDate"))
                    End If
                End If
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            If strAllWhereClause.Trim.Length > 0 Then
                strAllWhereClause += " and FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            Else
                strAllWhereClause += " FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            End If
            Session("MappingGroupMenuSTRPPATKApproval.Table") = Getvw_MappingGroupMenuSTRPPATKApproval(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)


            Return CType(Session("MappingGroupMenuSTRPPATKApproval.Table"), VList(Of vw_MappingGroupMenuSTRPPATKApproval))


        End Get
        Set(ByVal value As VList(Of vw_MappingGroupMenuSTRPPATKApproval))
            Session("MappingGroupMenuSTRPPATKApproval.Table") = value
        End Set
    End Property



    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("MappingGroupMenuSTRPPATKApproval.PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID") = Nothing
        Session("MappingGroupMenuSTRPPATKApproval.SetnGetGroupName") = Nothing
        Session("MappingGroupMenuSTRPPATKApproval.SetnGetMode") = Nothing
        Session("MappingGroupMenuSTRPPATKApproval.SetnGetPreparer") = Nothing
        Session("MappingGroupMenuSTRPPATKApproval.CreatedDate") = Nothing
        Session("MappingGroupMenuSTRPPATKApproval.CreatedDateUntil") = Nothing

        SetnGetCurrentPage = Nothing
        Me.SetnGetRowTotal = Nothing
        Me.SetnGetSort = Nothing

    End Sub


    Private Sub bindgrid()
        SettingControlSearching()
        Me.GridGMSTRPPATK.DataSource = Me.SetnGetBindTable
        Me.GridGMSTRPPATK.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridGMSTRPPATK.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridGMSTRPPATK.DataBind()

        If SetnGetRowTotal > 0 Then
            LabelNoRecordFound.Visible = False
        Else
            LabelNoRecordFound.Visible = True
        End If
    End Sub

    Private Sub SettingPropertySearching()

        SetnGetGroupName = TxtGroupName.Text.Trim
        SetnGetMode = TxtNama.Text.Trim
        SetnGetPreparer = TxtPreparer.Text.Trim
        Me.SetnGetEntryDateFrom = TxtEntryDateFrom.Text.Trim
        Me.SetnGetEntryDateUntil = TxtEntryDateUntil.Text.Trim

    End Sub

    Private Sub SettingControlSearching()

        TxtGroupName.Text = SetnGetGroupName
        TxtNama.Text = SetnGetMode
        TxtPreparer.Text = SetnGetPreparer
        TxtEntryDateFrom.Text = SetnGetEntryDateFrom
        TxtEntryDateUntil.Text = SetnGetEntryDateUntil

      
    End Sub



    Private Sub SetInfoNavigate()

        Me.PageCurrentPage.Text = (Me.SetnGetCurrentPage + 1).ToString
        Me.PageTotalPages.Text = Me.GetPageTotal.ToString
        Me.PageTotalRows.Text = Me.SetnGetRowTotal.ToString
        Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
        Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Try
            Me.bindgrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub ImageButtonSearchCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearchCancel.Click
        Try

            SetnGetGroupName = Nothing
            SetnGetMode = Nothing
            SetnGetPreparer = Nothing
            Me.SetnGetEntryDateFrom = Nothing
            Me.SetnGetEntryDateUntil = Nothing


        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            SettingPropertySearching()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub





    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

                ClearThisPageSessions()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Me.popUpEntryDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtEntryDateFrom.ClientID & "'), 'dd-mm-yyyy')")
                Me.popUpEntryDateUntil.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtEntryDateUntil.ClientID & "'), 'dd-mm-yyyy')")

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                bindgrid()

            End If
            GridGMSTRPPATK.PageSize = CInt(Sahassa.AML.Commonly.GetDisplayedTotalRow)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridGMSTRPPATK_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridGMSTRPPATK.EditCommand
        Dim PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID As Integer 'primary key
        'Dim StrUserID As String  'unik
        Try
            PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID = CType(e.Item.Cells(1).Text, Integer)
            'StrUserID = e.Item.Cells(2).Text

            Response.Redirect("MappingGroupMenuSTRPPATKApprovalDetail.aspx?PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID=" & PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID, False)
            'Using ObjMsBranchBll As New SahassaBLL.MsHelpBLL
            '    If ObjMsBranchBll.IsNotExistInApproval(StrUserID) Then
            '        Response.Redirect("MsHelpEdit.aspx?PkMsHelpID=" & PkMsHelpID, False)
            '    End If
            'End Using

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridGMSTRPPATK_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridGMSTRPPATK.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                Dim EditButton As LinkButton = CType(e.Item.Cells(6).FindControl("LnkEdit"), LinkButton)

                e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try            
            If IsNumeric(Me.TextGoToPage.Text) AndAlso (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    Throw New Exception(Sahassa.AML.CommonlyEnum.EXECPTION_PAGENUMBER_GREATER_TOTALPAGENUMBER)
                End If
            Else
                Throw New Exception(Sahassa.AML.CommonlyEnum.EXECPTION_PAGENUMBER_NUMERICPOSITIF)
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
  

    Protected Sub GridGMSTRPPATK_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridGMSTRPPATK.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
