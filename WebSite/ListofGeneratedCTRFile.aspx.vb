Imports Sahassa.AML.Commonly
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports System.Globalization
Partial Class ListofGeneratedCTRFile
    Inherits Parent
    Private BindGridFromExcel As Boolean = False

#Region "Set Session"
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("MsListofGeneratedFileViewSelected") Is Nothing, New ArrayList, Session("MsListofGeneratedFileViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("MsListofGeneratedFileViewSelected") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("MsListofGeneratedFileViewSort") Is Nothing, "PK_ListOfGeneratedFileCTR_ID desc", Session("MsListofGeneratedFileViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("MsListofGeneratedFileViewSort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("MsListofGeneratedFileViewCurrentPage") Is Nothing, 0, Session("MsListofGeneratedFileViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsListofGeneratedFileViewCurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get

            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("MsListofGeneratedFileViewRowTotal") Is Nothing, 0, Session("MsListofGeneratedFileViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsListofGeneratedFileViewRowTotal") = Value
        End Set
    End Property
    Private Property SetAndGetSearchingCriteria() As String
        Get
            Return IIf(Session("MsListofGeneratedFileViewSearchCriteria") Is Nothing, "", Session("MsListofGeneratedFileViewSearchCriteria"))
        End Get
        Set(ByVal Value As String)
            Session("MsListofGeneratedFileViewSearchCriteria") = Value
        End Set
    End Property
    Private Function SetAllSearchingCriteria() As String
        Dim StrSearch As String = ""
        Try
            'Transaction Date
            If Me.txtFilterTransactionDate.Text <> "" Then
                If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", Me.txtFilterTransactionDate.Text) Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & "'" & DateTime.ParseExact(Me.txtFilterTransactionDate.Text, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal) & " 00:00' <= TransactionDate and '" & DateTime.ParseExact(Me.txtFilterTransactionDate.Text, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal) & " 23:59'  >= TransactionDate "
                    Else
                        StrSearch = StrSearch & " And '" & DateTime.ParseExact(Me.txtFilterTransactionDate.Text, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal) & " 00:00' <= TransactionDate and '" & DateTime.ParseExact(Me.txtFilterTransactionDate.Text, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal) & " 23:59'  >= TransactionDate "
                    End If
                Else
                    Throw New Exception("Transaction date is not valid.")
                End If
            End If
            'Customer Type
            If Not Me.RdbReportType.SelectedIndex = 0 Then
                If StrSearch = "" Then
                    StrSearch = StrSearch & " FK_ReportType_ID =" & Convert.ToInt16(Me.RdbReportType.SelectedValue)
                Else
                    StrSearch = StrSearch & " And FK_ReportType_ID =" & Convert.ToInt16(Me.RdbReportType.SelectedValue)
                End If
            End If
            'Status Uploaded to ppatk
            If Not Me.RdbStatusUploadToPPATK.SelectedIndex = 0 Then
                If StrSearch = "" Then
                    StrSearch = StrSearch & " FK_MsStatusUploadPPATK_ID =" & Convert.ToInt16(Me.RdbStatusUploadToPPATK.SelectedValue)
                Else
                    StrSearch = StrSearch & " And FK_MsStatusUploadPPATK_ID =" & Convert.ToInt16(Me.RdbStatusUploadToPPATK.SelectedValue)
                End If
            End If

            If StrSearch.Length = 0 Then
                StrSearch &= " UserIDProposed ='" & Sahassa.AML.Commonly.SessionUserId & "'"
            Else
            End If
            Return StrSearch
        Catch
            Throw
            Return ""
        End Try
    End Function
    Private Property SetnGetBindTable() As VList(Of vw_ListOfGeneratedFileCTR)
        Get
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            If Session("MsListofGeneratedFileViewData") Is Nothing Then
                Return DataRepository.vw_ListOfGeneratedFileCTRProvider.GetPaged(Me.SetAndGetSearchingCriteria, Me.SetnGetSort, CInt(Me.SetnGetCurrentPage), CInt(Sahassa.AML.Commonly.GetDisplayedTotalRow), Me.SetnGetRowTotal)
            Else
                Return Session("MsListofGeneratedFileViewData")
            End If
        End Get
        Set(ByVal value As VList(Of vw_ListOfGeneratedFileCTR))
            Session("MsListofGeneratedFileViewData") = value
        End Set
    End Property
#End Region
#Region "Paging Event"
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be in number format.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
#Region "Sorting Event"

    'Protected Sub DtGridListofGeneratedFileView_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtGridListofGeneratedFileView.ItemCommand
    '    Try
    '        'ListOfDetailTransaction
    '        If e.CommandName.ToLower() = "ListOfDetailTransaction".ToLower() Then
    '            Dim LinkSeeDetailTransaction As LinkButton = CType(e.Item.FindControl("LinkListofTransaction"), LinkButton)
    '            Dim Pk_CTRFile_Id As Int32
    '            Pk_CTRFile_Id = LinkSeeDetailTransaction.CommandArgument
    '            Session("View_PkCTRFileId") = Pk_CTRFile_Id
    '            Using objLstvwCTRFile As VList(Of vwCTRFile) = DataRepository.vwCTRFileProvider.GetPaged("PK_ListOfCTRFile_Id = " & Pk_CTRFile_Id, "", 0, 1, 0)
    '                If objLstvwCTRFile.Count > 0 Then
    '                    Dim objvwCTRFile As vwCTRFile = objLstvwCTRFile(0)
    '                    Session("View_CTRFileTransactionDate") = objvwCTRFile.CTRFileTransactionDate
    '                    Session("View_FkCustomerTypeId") = objvwCTRFile.Fk_MsCustomerType_Id
    '                End If
    '            End Using
    '            Sahassa.AML.Commonly.SessionIntendedPage = "CTRFileListDetailTransaction.aspx"
    '            Me.Response.Redirect("CTRFileListDetailTransaction.aspx", False)
    '        End If
    '    Catch ex As Exception
    '        Me.cvalPageError.IsValid = False
    '        Me.cvalPageError.ErrorMessage = ex.Message
    '        LogError(ex)
    '    End Try
    'End Sub

    Protected Sub DtGridListofGeneratedFileView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtGridListofGeneratedFileView.SortCommand
        Dim GridListofGeneratedFileView As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridListofGeneratedFileView.Columns(Sahassa.AML.Commonly.IndexSort(GridListofGeneratedFileView, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
#Region "Bind Grid View"
    Public Sub BindGrid()

        Me.DtGridListofGeneratedFileView.DataSource = Me.SetnGetBindTable
        Me.DtGridListofGeneratedFileView.VirtualItemCount = Me.SetnGetRowTotal
        Me.DtGridListofGeneratedFileView.DataBind()
    End Sub
#End Region
    Private Sub ClearThisPageSessions()
        Session("MsListofGeneratedFileViewSelected") = Nothing
        Session("MsListofGeneratedFileViewSort") = Nothing
        Session("MsListofGeneratedFileViewCurrentPage") = Nothing
        Session("MsListofGeneratedFileViewRowTotal") = Nothing
        Session("MsListofGeneratedFileViewSearchCriteria") = Nothing
        Session("MsListofGeneratedFileViewData") = Nothing
    End Sub
#Region "Searching Event"
    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSearch.Click
        Try
            Me.SetAndGetSearchingCriteria = Me.SetAllSearchingCriteria
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub ImageClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageClearSearch.Click
        Try
            Me.txtFilterTransactionDate.Text = ""
            Me.RdbReportType.SelectedIndex = 0
            Me.RdbStatusUploadToPPATK.SelectedIndex = 0
            Me.SetAndGetSearchingCriteria = Nothing
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
#Region "Excel Export Event"
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.DtGridListofGeneratedFileView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim PkCTRFileId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PkCTRFileId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PkCTRFileId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PkCTRFileId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls
    Private Sub BindSelected()
        Dim Rows As New ArrayList
        Dim sbBranchSearch As New StringBuilder
        Try
            For i As Integer = 0 To Me.SetnGetSelectedItem.Count - 1
                If i = 0 Then
                    sbBranchSearch.Append(Me.SetnGetSelectedItem.Item(i))
                Else
                    sbBranchSearch.Append("," & Me.SetnGetSelectedItem.Item(i))
                End If
            Next
            If sbBranchSearch.Length > 0 Then
                Using ObjListofGeneratedFile As VList(Of vw_ListOfGeneratedFileCTR) = DataRepository.vw_ListOfGeneratedFileCTRProvider.GetPaged("PK_ListOfGeneratedFileCTR_ID in (" & sbBranchSearch.ToString & ")", Me.SetnGetSort, 0, Integer.MaxValue, Me.SetnGetRowTotal)
                    If ObjListofGeneratedFile.Count > 0 Then
                        For x As Integer = 0 To ObjListofGeneratedFile.Count - 1
                            Rows.Add(ObjListofGeneratedFile(x))
                        Next
                    End If
                End Using
            End If

            Me.DtGridListofGeneratedFileView.DataSource = Rows
            Me.DtGridListofGeneratedFileView.AllowPaging = False
            Me.DtGridListofGeneratedFileView.DataBind()

            Me.DtGridListofGeneratedFileView.Columns(0).Visible = False
            Me.DtGridListofGeneratedFileView.Columns(1).Visible = False
            Me.DtGridListofGeneratedFileView.Columns(3).Visible = False

            Me.DtGridListofGeneratedFileView.Columns(12).Visible = False
            Me.DtGridListofGeneratedFileView.Columns(13).Visible = False
            Me.DtGridListofGeneratedFileView.Columns(14).Visible = False

        Catch
            Throw
        Finally
            If Not sbBranchSearch Is Nothing Then
                sbBranchSearch = Nothing
            End If
        End Try
    End Sub
    Private Sub BindSelectedAll()
        Try
            Me.DtGridListofGeneratedFileView.DataSource = DataRepository.vw_ListOfGeneratedFileCTRProvider.GetPaged(Me.SetAndGetSearchingCriteria, Me.SetnGetSort, 0, Integer.MaxValue, 0)
            Me.DtGridListofGeneratedFileView.AllowPaging = False
            Me.DtGridListofGeneratedFileView.DataBind()

            Me.DtGridListofGeneratedFileView.Columns(0).Visible = False
            Me.DtGridListofGeneratedFileView.Columns(1).Visible = False
            Me.DtGridListofGeneratedFileView.Columns(3).Visible = False

            Me.DtGridListofGeneratedFileView.Columns(12).Visible = False
            Me.DtGridListofGeneratedFileView.Columns(13).Visible = False
            Me.DtGridListofGeneratedFileView.Columns(14).Visible = False

        Catch
            Throw
        End Try

    End Sub
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            BindGridFromExcel = True
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=ListofGeneratedCTRFile.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(DtGridListofGeneratedFileView)
            DtGridListofGeneratedFileView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub lnkExportAllData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAllData.Click
        Try
            Me.SetAndGetSearchingCriteria = Me.SetAllSearchingCriteria
            BindGridFromExcel = True
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelectedAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.Charset = ""
            Response.AddHeader("content-disposition", "attachment;filename=ListofGeneratedCTRFileAll.xls")

            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(DtGridListofGeneratedFileView)
            DtGridListofGeneratedFileView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.DtGridListofGeneratedFileView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim PkId As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(PkId) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(PkId) Then
        '                    ArrTarget.Add(PkId)
        '                End If
        '            Else
        '                ArrTarget.Remove(PkId)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(PkId) Then
        '                    ArrTarget.Add(PkId)
        '                End If
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next

        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.DtGridListofGeneratedFileView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
        Me.BindGrid()
    End Sub
    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim totalrow As Int16 = 0
        For Each gridRow As DataGridItem In Me.DtGridListofGeneratedFileView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                totalrow = CType(totalrow + 1, Int16)
            End If
        Next
        If i = totalrow Then
            Me.CheckBoxSelectAll.Checked = True
        Else
            Me.CheckBoxSelectAll.Checked = False
        End If
    End Sub
#End Region
    Sub LoadReportType()
        RdbReportType.Items.Clear()
        RdbReportType.Items.Add(New ListItem("All", "0"))
        RdbReportType.AppendDataBoundItems = True
        RdbReportType.DataSource = AMLBLL.ReportControlGeneratorBLL.GetTlistReportType("", "", 0, Integer.MaxValue, 0)
        RdbReportType.DataTextField = ReportTypeColumn.ReportTypeDesc.ToString
        RdbReportType.DataValueField = ReportTypeColumn.ReportType.ToString
        RdbReportType.DataBind()
    End Sub
    Sub LoadStatusUploadPPATK()
        RdbStatusUploadToPPATK.Items.Clear()
        RdbStatusUploadToPPATK.Items.Add(New ListItem("All", "0"))
        RdbStatusUploadToPPATK.AppendDataBoundItems = True
        RdbStatusUploadToPPATK.DataSource = AMLBLL.ReportControlGeneratorBLL.GetTlistMsStatusUploadPPATK("", "", 0, Integer.MaxValue, 0)
        RdbStatusUploadToPPATK.DataTextField = MsStatusUploadPPATKColumn.MsStatusUploadPPATK_Name.ToString
        RdbStatusUploadToPPATK.DataValueField = MsStatusUploadPPATKColumn.Pk_MsStatusUploadPPATK_Id.ToString
        RdbStatusUploadToPPATK.DataBind()
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.popUpTransactionDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtFilterTransactionDate.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUpTransactionDate.Style.Add("display", "")

                Me.ClearThisPageSessions()

                LoadReportType()
                LoadStatusUploadPPATK()

                Me.RdbReportType.SelectedIndex = 0
                Me.RdbStatusUploadToPPATK.SelectedIndex = 0

                Me.DtGridListofGeneratedFileView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                            'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '    Transcope.Complete()
                End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub DtGridListofGeneratedFileView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtGridListofGeneratedFileView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                'e.Item.Cells(5).Text = Convert.ToDateTime(e.Item.Cells(5).Text).ToString("dd-MMM-yyyy")
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)

                Dim objtemp As vw_ListOfGeneratedFileCTR = e.Item.DataItem
                If objtemp.StatusReportApproval.GetValueOrDefault(0) = 2 Then
                    CType(e.Item.Cells(13).FindControl("LinkGenerate"), LinkButton).Visible = True
                    CType(e.Item.Cells(14).FindControl("LnkUploadRefNo"), LinkButton).Visible = True

                Else
                    CType(e.Item.Cells(13).FindControl("LinkGenerate"), LinkButton).Visible = False
                    CType(e.Item.Cells(14).FindControl("LnkUploadRefNo"), LinkButton).Visible = False
                End If
                If objtemp.FK_ReportFormat_ID.GetValueOrDefault(0) = 2 Then
                    CType(e.Item.FindControl("LnkUploadRefNo"), LinkButton).Visible = True
                    CType(e.Item.FindControl("LinkListofTransaction"), LinkButton).Visible = True
                Else
                    CType(e.Item.FindControl("LinkListofTransaction"), LinkButton).Visible = False
                    CType(e.Item.FindControl("LnkUploadRefNo"), LinkButton).Visible = False
                End If


                ''e.Item.Cells(7).Text = Convert.ToDecimal(e.Item.Cells(7).Text).ToString("###,###0.00")

                'If BindGridFromExcel = True Then
                '    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                'Else
                '    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow))

                '    Dim LinkGenerateFileCTR As LinkButton = e.Item.FindControl("LinkGenerate")
                '    Dim ButtonUploadReffNo As LinkButton = CType(e.Item.Cells(12).Controls(0), LinkButton)
                '    Dim ButtonConfirmed As LinkButton = CType(e.Item.Cells(14).Controls(0), LinkButton)
                '    Dim LinkTransactionDetail As LinkButton = e.Item.FindControl("LinkListofTransaction")
                '    LinkTransactionDetail.CommandArgument = e.Item.Cells(1).Text
                '    LinkTransactionDetail.CommandName = "ListOfDetailTransaction"


                'Bisa generate kalau filenya masih ada..
                'bisa generate kalau is confirmed sudah ok
                'confirmed disabled kalau isconfirmed sudah is confirmed
                'upload reff no disabled kalau sudah ada reference numbernya (status upload ppatk sudah ok)
                'bisa upload kalau in confirmed = true

                'If IsConfirmed Then
                '    LinkGenerateFileCTR.Enabled = True
                '    Select Case FkStatusUploadtoPPATK
                '        Case 1
                '            'Sudah diupload ke ppatk
                '            ButtonUploadReffNo.Enabled = False
                '        Case 2
                '            'pending
                '            ButtonUploadReffNo.Enabled = True
                '    End Select
                '    ButtonConfirmed.Enabled = False
                'Else
                '    LinkGenerateFileCTR.Enabled = False
                '    ButtonUploadReffNo.Enabled = False
                '    ButtonConfirmed.Enabled = True
                'End If
                'End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.SetAndGetSearchingCriteria = Me.SetAllSearchingCriteria
            Me.CollectSelected()
            Me.BindGrid()
            Me.SetInfoNavigate()
            SetCheckedAll()
            BindGridFromExcel = False
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub cvalPageError_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageError.PreRender
        If Me.cvalPageError.IsValid = False Then
            'Sahassa.AML.Commonly.SessionErrorMessageCount = Sahassa.AML.Commonly.SessionErrorMessageCount + 1

        End If
    End Sub
#Region "Event for All Link"
    Protected Sub DtGridListofGeneratedFileView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtGridListofGeneratedFileView.DeleteCommand
        'confirmed
        Dim PkCTRFileId As Integer = e.Item.Cells(1).Text
        Dim FkCustomerTypeId As Int16 = e.Item.Cells(18).Text
        Try
        
            Sahassa.AML.Commonly.SessionIntendedPage = "CTRFileApproval.aspx"
            Me.Response.Redirect("CTRFileApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub DtGridListofGeneratedFileView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtGridListofGeneratedFileView.EditCommand
        'upload reff number..
        Dim PkCTRFileId As Integer = e.Item.Cells(1).Text
        Dim FkCustomerTypeId As Int16 = e.Item.Cells(17).Text
        Try
            Session("View_PkCTRFileId") = PkCTRFileId
            Session("View_FkCustomerTypeId") = FkCustomerTypeId
            Session("View_CTRFileTransactionDate") = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", e.Item.Cells(5).Text)
            Sahassa.AML.Commonly.SessionIntendedPage = "CTRFileUploadReferenceNumber.aspx"
            Me.Response.Redirect("CTRFileUploadReferenceNumber.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub LinkGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'go to generate page
        Dim LinkGenerateCTRFile As LinkButton
        Dim Pk_CTRFile_Id As Int32
        Try
            LinkGenerateCTRFile = sender
            Pk_CTRFile_Id = LinkGenerateCTRFile.CommandArgument
            Session("View_PkCTRFileId") = Pk_CTRFile_Id

            Using objListOfgenratectr As ListOfGeneratedFileCTR = DataRepository.ListOfGeneratedFileCTRProvider.GetByPK_ListOfGeneratedFileCTR_ID(Pk_CTRFile_Id)
                If Not objListOfgenratectr Is Nothing Then
                    If objListOfgenratectr.FK_ReportFormat_ID.GetValueOrDefault(0) = 2 Then
                        objListOfgenratectr.FK_MsStatusUploadPPATK_ID = 3
                        DataRepository.ListOfGeneratedFileCTRProvider.Save(objListOfgenratectr)
                    End If
                    

                    Response.AddHeader("content-disposition", "attachment; filename=" & objListOfgenratectr.FileName)
                    Response.Charset = ""
                    Me.EnableViewState = False
                    Response.ContentType = ""
                    Response.BinaryWrite(objListOfgenratectr.IsiFile)
                    Response.End()
                End If
            End Using
          


        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    'Protected Sub LinkListofTransaction_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    'go to list transaction file
    '    Dim LinkSeeDetailTransaction As LinkButton
    '    Dim Pk_CTRFile_Id As Int32
    '    Try
    '        LinkSeeDetailTransaction = sender
    '        Pk_CTRFile_Id = LinkSeeDetailTransaction.CommandArgument
    '        Session("View_PkCTRFileId") = Pk_CTRFile_Id
    '        Sahassa.AML.Commonly.SessionIntendedPage = "CTRFileListDetailTransaction.aspx"
    '        Me.Response.Redirect("CTRFileListDetailTransaction.aspx", False)
    '    Catch ex As Exception
    '        Me.cvalPageError.IsValid = False
    '        Me.cvalPageError.ErrorMessage = ex.Message
    '        LogError(ex)
    '    End Try
    'End Sub
#End Region

    Protected Sub LnkUploadRefNo_Click(sender As Object, e As System.EventArgs)
        Dim LinkGenerateCTRFile As LinkButton
        Dim Pk_CTRFile_Id As Int32
        Try
            LinkGenerateCTRFile = sender
            Pk_CTRFile_Id = LinkGenerateCTRFile.CommandArgument
            Session("View_PkCTRFileId") = Pk_CTRFile_Id
            
            Using objListOfgenratectr As ListOfGeneratedFileCTR = DataRepository.ListOfGeneratedFileCTRProvider.GetByPK_ListOfGeneratedFileCTR_ID(Pk_CTRFile_Id)
                If Not objListOfgenratectr Is Nothing Then
                    Response.Redirect("CTRFileUploadReferenceNumber.aspx?PK=" & Pk_CTRFile_Id, False)
                End If
            End Using



        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LinkListofTransaction_Click(sender As Object, e As System.EventArgs)
        Dim LinkGenerateCTRFile As LinkButton
        Dim Pk_CTRFile_Id As Int32
        Try
            LinkGenerateCTRFile = sender
            Pk_CTRFile_Id = LinkGenerateCTRFile.CommandArgument
            Session("View_PkCTRFileId") = Pk_CTRFile_Id

            Using objListOfgenratectr As ListOfGeneratedFileCTR = DataRepository.ListOfGeneratedFileCTRProvider.GetByPK_ListOfGeneratedFileCTR_ID(Pk_CTRFile_Id)
                If Not objListOfgenratectr Is Nothing Then
                    Response.Redirect("CTRFileListDetailTransaction.aspx?PK=" & Pk_CTRFile_Id, False)
                End If
            End Using



        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

   
    
End Class
