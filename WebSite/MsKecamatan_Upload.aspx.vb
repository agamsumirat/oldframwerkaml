#Region "Imports..."
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Excel
Imports Sahassa.AML.Commonly
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports AMLBLL.DataType
#End Region

Partial Class MsKecamatan_upload
    Inherits Parent

#Region "Structure..."

    Structure GrouP_MsKecamatan_success
        Dim MsKecamatan As MsKecamatan_ApprovalDetail
        Dim L_MsKecamatanNCBS As TList(Of MsKecamatanNCBS_ApprovalDetail)

    End Structure

#End Region

#Region "Property umum"
    Private Property GroupTableSuccessData() As List(Of GrouP_MsKecamatan_success)
        Get
            Return Session("GroupTableSuccessData")
        End Get
        Set(ByVal value As List(Of GrouP_MsKecamatan_success))
            Session("GroupTableSuccessData") = value
        End Set
    End Property

    Private Property GroupTableAnomalyData() As List(Of AMLBLL.MsKecamatanBll.DT_Group_MsKecamatan)
        Get
            Return Session("GroupTableAnomalyData")
        End Get
        Set(ByVal value As List(Of AMLBLL.MsKecamatanBll.DT_Group_MsKecamatan))
            Session("GroupTableAnomalyData") = value
        End Set
    End Property

    Public ReadOnly Property GET_PKUserID() As Integer
        Get
            Return SessionPkUserId
        End Get
    End Property

    Public ReadOnly Property GET_UserName() As String
        Get
            'Return SessionNIK
            Return SessionUserId
        End Get
    End Property

    Private Property SetnGetDataTableForSuccessData() As TList(Of MsKecamatan_ApprovalDetail)
        Get
            Return CType(IIf(Session("MsKecamatan_upload.TableSuccessUpload") Is Nothing, Nothing, Session("MsKecamatan_upload.TableSuccessUpload")), TList(Of MsKecamatan_ApprovalDetail))
        End Get
        Set(ByVal value As TList(Of MsKecamatan_ApprovalDetail))
            Session("MsKecamatan_upload.TableSuccessUpload") = value
        End Set
    End Property

    Private Property SetnGetDataTableForAnomalyData() As Data.DataTable
        Get
            Return CType(IIf(Session("MsKecamatan_upload.TableAnomalyUpload") Is Nothing, Nothing, Session("MsKecamatan_upload.TableAnomalyUpload")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsKecamatan_upload.TableAnomalyUpload") = value
        End Set
    End Property

    Private Property DSexcelTemp() As Data.DataSet
        Get
            Return CType(IIf(Session("MsKecamatan_upload.DSexcelTemp") Is Nothing, Nothing, Session("MsKecamatan_upload.DSexcelTemp")), DataSet)
        End Get
        Set(ByVal value As Data.DataSet)
            Session("MsKecamatan_upload.DSexcelTemp") = value
        End Set
    End Property

#End Region

#Region "Property Tabel Temporary"
    Private Property DT_AllMsKecamatan() As Data.DataTable
        Get
            Return CType(IIf(Session("MsKecamatan_upload.DT_MsKecamatan") Is Nothing, Nothing, Session("MsKecamatan_upload.DT_MsKecamatan")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsKecamatan_upload.DT_MsKecamatan") = value
        End Set
    End Property

    Private Property DT_AllMsKecamatanNCBS() As Data.DataTable
        Get
            Return CType(IIf(Session("MsKecamatan_upload.DT_MsKecamatanNCBS") Is Nothing, Nothing, Session("MsKecamatan_upload.DT_MsKecamatanNCBS")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsKecamatan_upload.DT_MsKecamatanNCBS") = value
        End Set
    End Property

#End Region

#Region "Property Table Sukses Data"

    Private Property TableSuccessMsKecamatan() As TList(Of MsKecamatan_ApprovalDetail)
        Get
            Return Session("TableSuccessMsKecamatan")
        End Get
        Set(ByVal value As TList(Of MsKecamatan_ApprovalDetail))
            Session("TableSuccessMsKecamatan") = value
        End Set
    End Property

    Private Property TableSuccessMsKecamatanNCBS() As TList(Of MsKecamatanNCBS)
        Get
            Return Session("TableSuccessMsKecamatanNCBS")
        End Get
        Set(ByVal value As TList(Of MsKecamatanNCBS))
            Session("TableSuccessMsKecamatanNCBS") = value
        End Set
    End Property


#End Region

#Region "Property Table Anomaly Data"

    Private Property TableAnomalyMsKecamatan() As Data.DataTable
        Get
            Return Session("TableAnomalyMsKecamatan")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsKecamatan") = value
        End Set
    End Property

    Private Property TableAnomalyMsKecamatanNCBS() As Data.DataTable
        Get
            Return Session("TableAnomalyMsKecamatanNCBS")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsKecamatanNCBS") = value
        End Set
    End Property

#End Region

#Region "validasi.."

    Private Function ValidateInput(ByVal MsKecamatan As AMLBLL.MsKecamatanBll.DT_Group_MsKecamatan) As List(Of String)
        Dim temp As New List(Of String)
        Dim msgError As New StringBuilder
        Dim errorline As New StringBuilder
        Try
            'validasi
            ValidateMsKecamatan(MsKecamatan.MsKecamatan, msgError, errorline)
            If msgError.ToString = "" Then
                For Each ci As DataRow In MsKecamatan.L_MsKecamatanNCBS.Rows
                    'validasi NCBS
                    ValidateMsKecamatanNCBS(ci, msgError, errorline)
                Next
            End If
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        Catch ex As Exception
            msgError.Append(ex.Message)
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        End Try
        Return temp
    End Function

    Private Shared Sub ValidateMsKecamatanNCBS(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorLine As StringBuilder)
        Try

            '======================== Validasi textbox :  IDKecamatanNCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("IDKecamatanBank")) = False Then
                msgError.AppendLine(" &bull; IDKecamatanBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsKecamatanBank : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  NamaKecamatanNCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("NamaKecamatanBank")) = False Then
                msgError.AppendLine(" &bull; NamaKecamatanBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsKecamatanBank : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorLine.AppendLine(" &bull;MsKecamatanBank : Line " & DTR("NO") & "<BR/>")
        End Try
    End Sub

    Private Shared Sub ValidateMsKecamatan(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorline As StringBuilder)
        Try

            Using checkBlocking As TList(Of MsKecamatan_ApprovalDetail) = DataRepository.MsKecamatan_ApprovalDetailProvider.GetPaged("IDKecamatan=" & DTR("IDKecamatan"), "", 0, 1, Nothing)
                If checkBlocking.Count > 0 Then
                    msgError.AppendLine(" &bull;MsKecamatan is waiting for approval <BR/>")
                    errorline.AppendLine(" &bull;MsKecamatan : Line " & DTR("NO") & "<BR/>")
                End If
            End Using

            '======================== Validasi textbox :  IDKotaKabupaten   canot Null (#)====================================================
            If ObjectAntiNull(DTR("IDKotaKabupaten")) = False Then
                msgError.AppendLine(" &bull; ID must be filled <BR/>")
                errorline.AppendLine(" &bull; MsKecamatan : Line " & DTR("NO") & "<BR/>")
            End If
            If ObjectAntiNull(DTR("IDKotaKabupaten")) Then
                If objectNumOnly(DTR("IDKotaKabupaten")) = False Then
                    msgError.AppendLine(" &bull; ID must be Numeric <BR/>")
                    errorline.AppendLine(" &bull; MsKecamatan : Line " & DTR("NO") & "<BR/>")
                End If
            End If
            '======================== Validasi textbox :  NamaKecamatan   canot Null (#)====================================================
            If ObjectAntiNull(DTR("NamaKecamatan")) = False Then
                msgError.AppendLine(" &bull; Nama must be filled <BR/>")
                errorline.AppendLine(" &bull; MsKecamatan : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorline.AppendLine(" &bull;MsKecamatan  : Line " & DTR("NO") & "<BR/>")
            LogError(ex)
        End Try
    End Sub

#End Region

#Region "Upload File"

    Public Function SaveFileImportToSave() As String
        Dim filePath As String
        Dim FileNameToGetData As String = ""
        Try
            If FileUploadKPI.PostedFile.ContentLength = 0 Then
                Return FileNameToGetData
            End If

            If Not FileUploadKPI.HasFile Then
                Return FileNameToGetData
            End If

            filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName)

            FileNameToGetData = FileUploadKPI.FileName
            If System.IO.File.Exists(filePath) Then
                Dim bexist As Boolean = True
                Dim i As Integer = 1
                While bexist
                    filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & FileUploadKPI.FileName.Split(CChar("."))(1))
                    FileNameToGetData = FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & Me.FileUploadKPI.FileName.Split(CChar("."))(1)
                    If Not System.IO.File.Exists(filePath) Then
                        bexist = False
                        Exit While
                    End If
                    i = i + 1
                End While
            End If
            Me.FileUploadKPI.PostedFile.SaveAs(filePath)
            Session("Filepath") = filePath
            Return FileNameToGetData
        Catch
            Throw
        End Try
    End Function

    ReadOnly Property FilePath() As String
        Get
            Return Session("FilePath")
        End Get
    End Property

    Private Sub ProcessExcelFile(ByVal FullPathFileNameToExportExcel As String)

        Using stream As FileStream = File.Open(FullPathFileNameToExportExcel, FileMode.Open, FileAccess.Read)
            Using excelReader As IExcelDataReader = ExcelReaderFactory.CreateBinaryReader(stream)
                excelReader.IsFirstRowAsColumnNames = True
                DSexcelTemp = excelReader.AsDataSet()
                If DSexcelTemp.Tables("MsKecamatan").Columns(0).ColumnName Like "*Column*" Then
                    DSexcelTemp.Tables("MsKecamatan").Rows.Clear()
                End If

            End Using
        End Using

    End Sub


#End Region

#Region "Function..."

    Function getErrorNo(ByVal Id As String, ByVal nama As String) As String
        Dim temp As String = ""
        Try
            Dim DV As New DataView(DT_AllMsKecamatanNCBS)
            'jika Null maka harus is null bukan kosong disini belum di handle
            DV.RowFilter = "IDKecamatan = '" & Id & "' and NamaKecamatan = '" & nama & "'"

            Dim DT As Data.DataTable = DV.ToTable
            If DT.Rows.Count > 0 Then
                temp = Safe(DT.Rows(0)("NO"))
            End If
        Catch ex As Exception
            temp = ex.Message
        End Try

        Return temp
    End Function
    ''' <summary>
    ''' Merubah excel ke Object Databale sekaligus memecahnya
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ImportExcelTempToDTproperty()
        Dim DT As New Data.DataTable
        'DT_MsKecamatan = DSexcelTemp.Tables("MsKecamatan")
        DT = DSexcelTemp.Tables("MsKecamatan")
        Dim DV As New DataView(DT)
        DT_AllMsKecamatan = DV.ToTable(True, "IDKecamatan", "IDKotaKabupaten", "NamaKecamatan", "activation")
        DT_AllMsKecamatanNCBS = DSexcelTemp.Tables("MsKecamatan")
        DT_AllMsKecamatan.Columns.Add("NO")
        For Each i As DataRow In DT_AllMsKecamatan.Rows
            Dim PK As String = Safe(i("IDKecamatan"))
            Dim nama As String = Safe(i("NamaKecamatan"))
            i("NO") = getErrorNo(PK, nama)
        Next
    End Sub

    Private Sub ClearThisPageSessions()
        DT_AllMsKecamatan = Nothing
        DT_AllMsKecamatanNCBS = Nothing
        GroupTableSuccessData = Nothing
        GroupTableAnomalyData = Nothing
        DSexcelTemp = Nothing
        SetnGetDataTableForAnomalyData = Nothing
        SetnGetDataTableForSuccessData = Nothing
    End Sub


    Function ConvertDTtoMsKecamatanNCBS(ByVal DT As Data.DataTable) As TList(Of MsKecamatanNCBS_ApprovalDetail)
        Dim temp As New TList(Of MsKecamatanNCBS_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsKecamatanNCBS As New MsKecamatanNCBS_ApprovalDetail
                With MsKecamatanNCBS
                    FillOrNothing(.IDKecamatanNCBS, DTR("IDKecamatanBank"), True, oInt)
                    FillOrNothing(.NamaKecamatanNCBS, DTR("NamaKecamatanBank"), True, oString)
                    FillOrNothing(.Activation, 1, True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsKecamatanNCBS)
                End With
            End Using

        Next

        Return temp
    End Function



    Function ConvertDTtoMsKecamatan(ByVal DTR As DataRow) As MsKecamatan_ApprovalDetail
        Dim temp As New MsKecamatan_ApprovalDetail
        Using MsKecamatan As New MsKecamatan_ApprovalDetail()
            With MsKecamatan
                FillOrNothing(.IDKotaKabupaten, DTR("IDKotaKabupaten"), True, oInt)
                FillOrNothing(.IDKecamatan, DTR("IDKecamatan"), True, oInt)
                FillOrNothing(.NamaKecamatan, DTR("NamaKecamatan"), True, Ovarchar)

                FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                FillOrNothing(.CreatedDate, Now, True, oDate)
            End With
            temp = MsKecamatan
        End Using

        Return temp
    End Function

    Function ConvertDTtoMsKecamatan(ByVal DT As Data.DataTable) As TList(Of MsKecamatan_ApprovalDetail)
        Dim temp As New TList(Of MsKecamatan_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsKecamatan As New MsKecamatan_ApprovalDetail()
                With MsKecamatan
                    FillOrNothing(.IDKotaKabupaten, DTR("IDKotaKabupaten"), True, oInt)
                    FillOrNothing(.IDKecamatan, DTR("IDKecamatan"), True, oInt)
                    FillOrNothing(.NamaKecamatan, DTR("NamaKecamatan"), True, Ovarchar)

                    FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsKecamatan)
                End With
            End Using
        Next
        Return temp
    End Function

    Private Sub ChangeMultiView(ByVal index As Integer)
        MultiViewKPIOvertimeUpload.ActiveViewIndex = index
        If index = 0 Then
            Imgbtnsave.Visible = False
            Imgbtncancel.Visible = False
        ElseIf index = 1 Then
            Imgbtncancel.Visible = True
        ElseIf index = 2 Then
            Imgbtncancel.Visible = False
            Imgbtnsave.Visible = False
        End If
    End Sub

    Private Function GenerateGroupingMsKecamatan() As List(Of AMLBLL.MsKecamatanBll.DT_Group_MsKecamatan)
        Dim Ltemp As New List(Of AMLBLL.MsKecamatanBll.DT_Group_MsKecamatan)
        For i As Integer = 0 To DT_AllMsKecamatan.Rows.Count - 1
            Dim MsKecamatan As New AMLBLL.MsKecamatanBll.DT_Group_MsKecamatan
            With MsKecamatan
                .MsKecamatan = DT_AllMsKecamatan.Rows(i)
                .L_MsKecamatanNCBS = DT_AllMsKecamatanNCBS.Clone

                'Insert MsKecamatanNCBS
                Dim MsKecamatan_ID As String = Safe(.MsKecamatan("IDKecamatan"))
                Dim MsKecamatan_nama As String = Safe(.MsKecamatan("NamaKecamatan"))
                Dim MsKecamatan_Activation As String = Safe(.MsKecamatan("Activation"))
                If DT_AllMsKecamatanNCBS.Rows.Count > 0 Then
                    Using DV As New DataView(DT_AllMsKecamatanNCBS)
                        DV.RowFilter = "IDKecamatan='" & MsKecamatan_ID & "' and " & _
                                                  "NamaKecamatan='" & MsKecamatan_nama & "' and " & _
                                                  "activation='" & MsKecamatan_Activation & "'"
                        If DV.Count > 0 Then
                            Dim temp As Data.DataTable = DV.ToTable
                            For Each H As DataRow In temp.Rows
                                .L_MsKecamatanNCBS.ImportRow(H)
                            Next
                        End If
                    End Using
                End If




            End With
            Ltemp.Add(MsKecamatan)
        Next
        Return Ltemp
    End Function

    Public Sub BindGrid()
        lblValueFailedData.Text = SetnGetDataTableForAnomalyData.Rows.Count
        GridAnomalyList.DataSource = SetnGetDataTableForAnomalyData
        GridAnomalyList.DataBind()
        lblValueSuccessData.Text = SetnGetDataTableForSuccessData.Count
        GridSuccessList.DataSource = SetnGetDataTableForSuccessData
        lblValueuploadeddata.Text = DT_AllMsKecamatan.Rows.Count
        GridSuccessList.DataBind()
    End Sub

#End Region

#Region "events..."


    Protected Sub ImgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtncancel.Click
        ClearThisPageSessions()
        ChangeMultiView(0)
        LinkButtonExportExcel.Visible = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If IsPostBack Then
                Return
            End If

            ClearThisPageSessions()
            SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            ChangeMultiView(0)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        ChangeMultiView(0)
    End Sub

    Protected Sub GridAnomalyList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridAnomalyList.PageIndexChanged
        GridAnomalyList.CurrentPageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GridSuccessList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridSuccessList.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                'Show mapping item
                Dim StringMaping As New StringBuilder
                Dim DV As New DataView(DT_AllMsKecamatanNCBS)
                Dim DT As New Data.DataTable
                DV.RowFilter = "IDKecamatan='" & e.Item.Cells(1).Text & "' and " & "NamaKecamatan='" & e.Item.Cells(2).Text & "'"
                DT = DV.ToTable
                If DT.Rows.Count > 0 Then
                    Dim countField As Integer = 7
                    Dim lengthField As Integer = 0
                    For Each idx As DataRow In DT.Rows
                        With idx
                            If lengthField > countField Then
                                StringMaping.AppendLine(idx("NamaKecamatanBank") & "(" & idx("IDKecamatanBank") & ");")
                                lengthField = 0
                            Else
                                StringMaping.AppendLine(idx("NamaKecamatanBank") & "(" & idx("IDKecamatanBank") & ");")
                                lengthField += 1
                            End If

                        End With
                    Next
                Else
                    StringMaping.Append("-")
                End If
                e.Item.Cells(CollCount).Text = StringMaping.ToString

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridSuccessList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridSuccessList.PageIndexChanged
        Dim GridTarget As DataGrid = CType(source, DataGrid)
        Try
            GridSuccessList.CurrentPageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgSaveSuccessData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtnsave.Click
        Try
            'File belum diupload
            If DT_AllMsKecamatan Is Nothing Then
                Throw New Exception("Load data failed")
            End If

            'Save MsKecamatan Approval (Header)
            Dim KeyApp As String
            Using Aproval As New MsKecamatan_Approval()
                With Aproval
                    .RequestedBy = GET_PKUserID
                    .RequestedDate = Date.Now
                    .FK_MsMode_Id = 2 'Edit
                    .IsUpload = True
                End With
                DataRepository.MsKecamatan_ApprovalProvider.Save(Aproval)
                'ambil key approval
                KeyApp = Aproval.PK_MsKecamatan_Approval_Id
            End Using

            For k As Integer = 0 To GroupTableSuccessData.Count - 1

                Dim MsKecamatan As GrouP_MsKecamatan_success = GroupTableSuccessData(k)


                With MsKecamatan
                    'save MsKecamatan approval detil
                    .MsKecamatan.FK_MsKecamatan_Approval_Id = KeyApp
                    DataRepository.MsKecamatan_ApprovalDetailProvider.Save(.MsKecamatan)
                    'ambil key MsKecamatan approval detil
                    Dim IdMsKecamatan As String = .MsKecamatan.IDKecamatan
                    Dim L_MappingMsKecamatanNCBSPPATK_Approval_Detail As New TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail)
                    'save mapping dan NCBS
                    For Each OMsKecamatanNCBS_ApprovalDetail As MsKecamatanNCBS_ApprovalDetail In .L_MsKecamatanNCBS
                        OMsKecamatanNCBS_ApprovalDetail.PK_MsKecamatan_Approval_Id = KeyApp
                        Using OMappingMsKecamatanNCBSPPATK_Approval_Detail As New MappingMsKecamatanNCBSPPATK_Approval_Detail
                            With OMappingMsKecamatanNCBSPPATK_Approval_Detail
                                FillOrNothing(.IDKecamatanNCBS, OMsKecamatanNCBS_ApprovalDetail.IDKecamatanNCBS, True, oInt)
                                FillOrNothing(.IDKecamatan, IdMsKecamatan, True, oInt)
                                FillOrNothing(.PK_MsKecamatan_Approval_Id, KeyApp, True, oInt)
                                FillOrNothing(.Nama, OMsKecamatanNCBS_ApprovalDetail.NamaKecamatanNCBS, True, oString)
                            End With
                            L_MappingMsKecamatanNCBSPPATK_Approval_Detail.Add(OMappingMsKecamatanNCBSPPATK_Approval_Detail)
                        End Using
                    Next
                    'save maping dan NCBS
                    DataRepository.MappingMsKecamatanNCBSPPATK_Approval_DetailProvider.Save(L_MappingMsKecamatanNCBSPPATK_Approval_Detail)
                    DataRepository.MsKecamatanNCBS_ApprovalDetailProvider.Save(.L_MsKecamatanNCBS)

                End With
            Next

            LblConfirmation.Text = "MsKecamatan data has edited (" & SetnGetDataTableForSuccessData.Count & " Rows / MsKecamatan)"
            ChangeMultiView(2)

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgUpload.Click
        Dim CombinedFileName As String
        GroupTableSuccessData = New List(Of GrouP_MsKecamatan_success)
        GroupTableAnomalyData = New List(Of AMLBLL.MsKecamatanBll.DT_Group_MsKecamatan)
        Try
            'validasi File
            If FileUploadKPI.FileName = "" Then
                Throw New Exception("Data required")
            End If

            If Path.GetExtension(FileUploadKPI.FileName) <> ".xls" Then
                Throw New Exception("Invalid File Upload Extension")
            End If

            CombinedFileName = SaveFileImportToSave()
            If String.Compare(CombinedFileName, "", False) <> 0 Then
                Try
                    ProcessExcelFile(FilePath)
                Catch ex As Exception
                    Throw New Exception("Invalid File Format")
                End Try
            Else
                Throw New Exception("Invalid File Format")
            End If

            'Import from Excell to property Global
            ImportExcelTempToDTproperty()

            Dim LG_MsKecamatan As List(Of AMLBLL.MsKecamatanBll.DT_Group_MsKecamatan) = GenerateGroupingMsKecamatan()
            'tambahkan kolom untuk tampilan di grid anomaly
            DT_AllMsKecamatan.Columns.Add("Description")
            DT_AllMsKecamatan.Columns.Add("errorline")

            'Membuat tempory untuk yang anomali dan yang sukses
            Dim DTnormal As Data.DataTable = DT_AllMsKecamatan.Clone
            Dim DTAnomaly As Data.DataTable = DT_AllMsKecamatan.Clone

            'proses pemilahan data anomaly dan data sukses melalui validasi 
            For i As Integer = 0 To LG_MsKecamatan.Count - 1
                Dim Lerror As List(Of String) = ValidateInput(LG_MsKecamatan(i))
                Dim MSG As String = Lerror(0)
                Dim LINE As String = Lerror(1)
                If MSG = "" Then
                    DTnormal.ImportRow(LG_MsKecamatan(i).MsKecamatan)
                    Dim GroupMsKecamatan As New GrouP_MsKecamatan_success
                    With GroupMsKecamatan
                        'konversi dari datatable ke Object Nettier
                        .MsKecamatan = ConvertDTtoMsKecamatan(LG_MsKecamatan(i).MsKecamatan)
                        .L_MsKecamatanNCBS = ConvertDTtoMsKecamatanNCBS(LG_MsKecamatan(i).L_MsKecamatanNCBS)
                    End With
                    'masukan ke group sukses
                    GroupTableSuccessData.Add(GroupMsKecamatan)
                Else
                    DT_AllMsKecamatan.Rows(i)("Description") = MSG
                    DT_AllMsKecamatan.Rows(i)("errorline") = LINE
                    DTAnomaly.ImportRow(DT_AllMsKecamatan.Rows(i))
                    GroupTableAnomalyData.Add(LG_MsKecamatan(i))
                End If

            Next

            SetnGetDataTableForSuccessData = ConvertDTtoMsKecamatan(DTnormal)
            SetnGetDataTableForAnomalyData = DTAnomaly

            If DTAnomaly.Rows.Count > 0 Then
                Imgbtnsave.Visible = False
                Imgbtncancel.Visible = True
            Else
                Imgbtnsave.Visible = True
                Imgbtncancel.Visible = True
            End If

            BindGrid()
            If GridAnomalyList.Items.Count < 1 Then
                LinkButtonExportExcel.Visible = False
            Else
                LinkButtonExportExcel.Visible = True
            End If
            ChangeMultiView(1)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Dim Exp As New AMLBLL.MsKecamatanBll
            Dim FolderPath As String = Server.MapPath("FolderTemplate")

            Dim buffer As Byte() = Exp.ExportDTtoExcel(GroupTableAnomalyData, FolderPath)
            Response.AddHeader("Content-type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment; filename=MsKecamatanUpload.xls")
            Response.BinaryWrite(buffer)
            Response.Flush()
            Response.End()

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered 

    End Sub

    Protected Sub GridSuccessList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridSuccessList.SelectedIndexChanged

    End Sub
End Class





