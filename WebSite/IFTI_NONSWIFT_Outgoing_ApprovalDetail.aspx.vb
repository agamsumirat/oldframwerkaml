﻿#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports AMLBLL.AuditTrailBLL
Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
Imports System.Data.SqlClient
#End Region

Partial Class IFTI_NONSWIFT_Outgoing_ApprovalDetail
    Inherits Parent

#Region "Property"

    Public ReadOnly Property SetnGetUserID() As String
        Get
            Return SessionPkUserId
        End Get

    End Property

    Private Property GetTotalInsert() As Double
        Get
            Return Session("GetTotalInsert")
        End Get
        Set(ByVal value As Double)
            Session("GetTotalInsert") = value
        End Set
    End Property

    Private Property GetTotalUpdate() As Double
        Get
            Return Session("GetTotalUpdate")
        End Get
        Set(ByVal value As Double)
            Session("GetTotalUpdate") = value
        End Set
    End Property

    Public ReadOnly Property parID As String
        Get
            Return Request.Params("PK_IFTI_Approval_Id")
        End Get
    End Property

    Public Property SetIgnoreID() As String
        Get
            If Not Session("IFTI_Approval_Detail.setPK_MsIgnoreList_Approval_Id") Is Nothing Then
                Return CStr(Session("IFTI_Approval_Detail.setPK_MsIgnoreList_Approval_Id"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("IFTI_Approval_Detail.setPK_MsIgnoreList_Approval_Id") = value
        End Set
    End Property

    Public Property SetIgnoreKeyword() As String
        Get
            If Not Session("IFTI_Approval_Detail.SetIgnoreKeyword") Is Nothing Then
                Return CStr(Session("IFTI_Approval_Detail.SetIgnoreKeyword"))
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            Session("IFTI_Approval_Detail.SetIgnoreKeyword") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.Sort") Is Nothing, " FK_IFTI_Approval_Id  asc", Session("IFTI_Approval_Detail.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("IFTI_Approval_Detail.Sort") = Value
        End Set
    End Property

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.SelectedItem") Is Nothing, New ArrayList, Session("IFTI_Approval_Detail.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("IFTI_Approval_Detail.SelectedItem") = value
        End Set
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.RowTotal") Is Nothing, 0, Session("IFTI_Approval_Detail.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("IFTI_Approval_Detail.RowTotal") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return iTotalPages(SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.CurrentPage") Is Nothing, 0, Session("IFTI_Approval_Detail.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("IFTI_Approval_Detail.CurrentPage") = Value
        End Set
    End Property

    Private Sub LoadIDType()
        Me.CBOPenerimaNasabah_IND_JenisIdenOld.Items.Clear()
        Me.CBOPenerimaNasabah_IND_JenisIdenOld.Items.Add("-Select-")
        Me.CBOPenerimaNasabah_IND_JenisIdenOld.AppendDataBoundItems = True
        Me.CBOPenerimaNasabah_IND_JenisIdenOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBOPenerimaNasabah_IND_JenisIdenOld.DataTextField = "IDType"
        Me.CBOPenerimaNasabah_IND_JenisIdenOld.DataValueField = "PK_IFTI_IDType"
        Me.CBOPenerimaNasabah_IND_JenisIdenOld.DataBind()

        Me.CBOPenerimaNasabah_IND_JenisIdenNew.Items.Clear()
        Me.CBOPenerimaNasabah_IND_JenisIdenNew.Items.Add("-Select-")
        Me.CBOPenerimaNasabah_IND_JenisIdenNew.AppendDataBoundItems = True
        Me.CBOPenerimaNasabah_IND_JenisIdenNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBOPenerimaNasabah_IND_JenisIdenNew.DataTextField = "IDType"
        Me.CBOPenerimaNasabah_IND_JenisIdenNew.DataValueField = "PK_IFTI_IDType"
        Me.CBOPenerimaNasabah_IND_JenisIdenNew.DataBind()

        Me.CboPengirimNasInd_jenisidentitasNew.Items.Clear()
        Me.CboPengirimNasInd_jenisidentitasNew.Items.Add("-Select-")
        Me.CboPengirimNasInd_jenisidentitasNew.AppendDataBoundItems = True
        Me.CboPengirimNasInd_jenisidentitasNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CboPengirimNasInd_jenisidentitasNew.DataTextField = "IDType"
        Me.CboPengirimNasInd_jenisidentitasNew.DataValueField = "PK_IFTI_IDType"
        Me.CboPengirimNasInd_jenisidentitasNew.DataBind()

        Me.CboPengirimNasInd_jenisidentitasOld.Items.Clear()
        Me.CboPengirimNasInd_jenisidentitasOld.Items.Add("-Select-")
        Me.CboPengirimNasInd_jenisidentitasOld.AppendDataBoundItems = True
        Me.CboPengirimNasInd_jenisidentitasOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CboPengirimNasInd_jenisidentitasOld.DataTextField = "IDType"
        Me.CboPengirimNasInd_jenisidentitasOld.DataValueField = "PK_IFTI_IDType"
        Me.CboPengirimNasInd_jenisidentitasOld.DataBind()

        Me.CBOBenfOwnerNasabah_JenisDokumenNew.Items.Clear()
        Me.CBOBenfOwnerNasabah_JenisDokumenNew.Items.Add("-Select-")
        Me.CBOBenfOwnerNasabah_JenisDokumenNew.AppendDataBoundItems = True
        Me.CBOBenfOwnerNasabah_JenisDokumenNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBOBenfOwnerNasabah_JenisDokumenNew.DataTextField = "IDType"
        Me.CBOBenfOwnerNasabah_JenisDokumenNew.DataValueField = "PK_IFTI_IDType"
        Me.CBOBenfOwnerNasabah_JenisDokumenNew.DataBind()

        Me.CBOBenfOwnerNasabah_JenisDokumenOld.Items.Clear()
        Me.CBOBenfOwnerNasabah_JenisDokumenOld.Items.Add("-Select-")
        Me.CBOBenfOwnerNasabah_JenisDokumenOld.AppendDataBoundItems = True
        Me.CBOBenfOwnerNasabah_JenisDokumenOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBOBenfOwnerNasabah_JenisDokumenOld.DataTextField = "IDType"
        Me.CBOBenfOwnerNasabah_JenisDokumenOld.DataValueField = "PK_IFTI_IDType"
        Me.CBOBenfOwnerNasabah_JenisDokumenOld.DataBind()

        Me.CboPengirimNonNasabah_JenisDokumenNew.Items.Clear()
        Me.CboPengirimNonNasabah_JenisDokumenNew.Items.Add("-Select-")
        Me.CboPengirimNonNasabah_JenisDokumenNew.AppendDataBoundItems = True
        Me.CboPengirimNonNasabah_JenisDokumenNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CboPengirimNonNasabah_JenisDokumenNew.DataTextField = "IDType"
        Me.CboPengirimNonNasabah_JenisDokumenNew.DataValueField = "PK_IFTI_IDType"
        Me.CboPengirimNonNasabah_JenisDokumenNew.DataBind()

        Me.CboPengirimNonNasabah_JenisDokumenOld.Items.Clear()
        Me.CboPengirimNonNasabah_JenisDokumenOld.Items.Add("-Select-")
        Me.CboPengirimNonNasabah_JenisDokumenOld.AppendDataBoundItems = True
        Me.CboPengirimNonNasabah_JenisDokumenOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CboPengirimNonNasabah_JenisDokumenOld.DataTextField = "IDType"
        Me.CboPengirimNonNasabah_JenisDokumenOld.DataValueField = "PK_IFTI_IDType"
        Me.CboPengirimNonNasabah_JenisDokumenOld.DataBind()

    End Sub

    Public ReadOnly Property SetnGetBindTable(Optional ByVal AllRecord As Boolean = False) As TList(Of IFTI_Approval_Detail)
        Get
            Dim txt As TList(Of IFTI_Approval_Detail) = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_id = " & parID, "", 0, Integer.MaxValue, 0)
            Dim TotalDisplay As Integer = 0
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
                SetnGetCurrentPage = GetPageTotal - 1
            End If

            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = "FK_IFTI_Approval_id=" & parID

            If SetIgnoreID.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = IFTI_Approval_DetailColumn.PK_IFTI_Approval_Detail_Id.ToString & " like '%" & SetIgnoreID.Trim.Replace("'", "''") & "%'"
            End If

            If AllRecord = False Then
                TotalDisplay = GetDisplayedTotalRow
            Else
                TotalDisplay = Integer.MaxValue
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            Return DataRepository.IFTI_Approval_DetailProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, TotalDisplay, SetnGetRowTotal)

        End Get

    End Property
    Private _DataNonSwiftIn As IFTI_Approval_Detail
    Private ReadOnly Property DataNonSwiftIn() As IFTI_Approval_Detail
        Get
            Try
                If _DataNonSwiftIn Is Nothing Then
                    Dim otable As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)
                    If Not otable Is Nothing Then
                        _DataNonSwiftIn = otable
                        Return _DataNonSwiftIn
                    Else
                        Return Nothing
                    End If

                Else
                    Return _DataNonSwiftIn
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property
    Private _Databenefeciary As IFTI_Beneficiary
    Private ReadOnly Property Databenefeciary() As IFTI_Beneficiary
        Get
            Try
                If _Databenefeciary Is Nothing Then
                    Using otable As IFTI_Beneficiary = DataRepository.IFTI_BeneficiaryProvider.GetByPK_IFTI_Beneficiary_ID(Me.PKbeneficiary)
                        If Not otable Is Nothing Then
                            _Databenefeciary = otable
                            Return _Databenefeciary
                        Else
                            Return Nothing
                        End If
                    End Using
                Else
                    Return _Databenefeciary
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property
    Private _DatabenefeciaryApproval As IFTI_Approval_Beneficiary
    Private PKbeneficiary As Integer
    Private ReadOnly Property DatabenefeciaryApproval() As IFTI_Approval_Beneficiary
        Get
            Try
                If _DatabenefeciaryApproval Is Nothing Then
                    Dim testdata As TList(Of IFTI_Approval_Beneficiary) = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_Approval_Id =" & parID, "", 0, Integer.MaxValue, 0)
                    If testdata.Count <> 0 Then
                        Using otable As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_Approval_Id =" & parID, "", 0, Integer.MaxValue, 0)(0)
                            If Not otable Is Nothing Then
                                PKbeneficiary = otable.FK_IFTI_Beneficiary_ID
                                _DatabenefeciaryApproval = otable
                                Return _DatabenefeciaryApproval
                            Else
                                Return Nothing
                            End If
                        End Using
                    End If
                    
                Else
                    Return _DatabenefeciaryApproval
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property

    Private Sub bindgrid()
        Try
            
            If Not DataNonSwiftIn Is Nothing Then

                '++++++++++++++++++++++++++++++++++++++++UMUM+++++++++++++++++++++++++++++++++++++++++++'

                If Not DataNonSwiftIn.LTDLNNo = "" Then
                    NonSwiftOutUmum_LTDNNew.Text = DataNonSwiftIn.LTDLNNo
                End If
                If Not DataNonSwiftIn.LTDLNNo_Old = "" Then
                    NonSwiftOutUmum_LTDNOld.Text = DataNonSwiftIn.LTDLNNo_Old
                End If
                If Not DataNonSwiftIn.LTDLNNoKoreksi = "" Then
                    NonSwiftOutUmum_LtdnKoreksiNew.Text = DataNonSwiftIn.LTDLNNoKoreksi
                End If
                If Not DataNonSwiftIn.LTDLNNoKoreksi_Old = "" Then
                    NonSwiftOutUmum_LtdnKoreksiOld.Text = DataNonSwiftIn.LTDLNNoKoreksi_Old
                End If
                If Not DataNonSwiftIn.TanggalLaporan = "" Then
                    NonSwiftOutUmum_TanggalLaporanNew.Text = DataNonSwiftIn.TanggalLaporan
                End If
                If Not DataNonSwiftIn.TanggalLaporan_Old = "" Then
                    NonSwiftOutUmum_TanggalLaporanOld.Text = DataNonSwiftIn.TanggalLaporan_Old
                End If
                If Not DataNonSwiftIn.NamaPJKBankPelapor = "" Then
                    NonSwiftOutUmum_NamaPJKBankNew.Text = DataNonSwiftIn.NamaPJKBankPelapor
                End If
                If Not DataNonSwiftIn.NamaPJKBankPelapor_Old = "" Then
                    NonSwiftOutUmum_NamaPJKBankOld.Text = DataNonSwiftIn.NamaPJKBankPelapor_Old
                End If
                If Not DataNonSwiftIn.NamaPejabatPJKBankPelapor = "" Then
                    NonSwiftOutUmum_NamaPejabatPJKBankNew.Text = DataNonSwiftIn.NamaPejabatPJKBankPelapor
                End If
                If Not DataNonSwiftIn.NamaPejabatPJKBankPelapor_Old = "" Then
                    NonSwiftOutUmum_NamaPejabatPJKBankOld.Text = DataNonSwiftIn.NamaPejabatPJKBankPelapor_Old
                End If
                If Not DataNonSwiftIn.JenisLaporan = "" Then
                    RbNonSwiftOutUmum_JenisLaporanNew.SelectedValue = DataNonSwiftIn.JenisLaporan.GetValueOrDefault
                End If
                If Not DataNonSwiftIn.JenisLaporan_Old = "" Then
                    RbNonSwiftOutUmum_JenisLaporanOld.SelectedValue = DataNonSwiftIn.JenisLaporan_Old.GetValueOrDefault
                End If

                '++++++++++++++++++++++++++++++++++++++++PENGIRIM+++++++++++++++++++++++++++++++++++++++++++'

                If Not DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID = "" Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID.GetValueOrDefault
                        Case 1
                            Me.RbPengirimNasabah_TipePengirimOld.SelectedValue = 1
                            MultiViewPengirimNasabah.ActiveViewIndex = 0
                        Case 2
                            Me.RbPengirimNasabah_TipePengirimOld.SelectedValue = 1
                            MultiViewPengirimNasabah.ActiveViewIndex = 1
                        Case 3
                            Me.RbPengirimNasabah_TipePengirimOld.SelectedValue = 2
                            MultiViewNonSWIFTOutIdenPengirim.ActiveViewIndex = 1
                            PengirimNonNasabah()
                    End Select
                End If

                If Not DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID = "" Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID.GetValueOrDefault
                        Case 1
                            Me.RbPengirimNasabah_TipePengirimNew.SelectedValue = 1
                        Case 2
                            Me.RbPengirimNasabah_TipePengirimNew.SelectedValue = 1
                        Case 3
                            Me.RbPengirimNasabah_TipePengirimNew.SelectedValue = 2
                    End Select
                End If

                If Not DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old = "" Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old.GetValueOrDefault(0)
                        Case 1
                            Me.Rb_IdenPengirimNas_TipeNasabahOld.SelectedValue = 1
                            Me.MultiViewPengirimNasabah.ActiveViewIndex = 0
                            PengirimNasabah()
                        Case 2
                            Me.Rb_IdenPengirimNas_TipeNasabahOld.SelectedValue = 2
                            MultiViewPengirimNasabah.ActiveViewIndex = 1
                            PengirimKorporasi()

                    End Select
                End If

                If Not DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID = "" Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID.GetValueOrDefault(0)
                        Case 1
                            Me.Rb_IdenPengirimNas_TipeNasabahNew.SelectedValue = 1
                            Me.MultiViewPengirimNasabah_New.ActiveViewIndex = 0
                            PengirimNasabahnew()
                        Case 2
                            Me.Rb_IdenPengirimNas_TipeNasabahNew.SelectedValue = 2
                            MultiViewPengirimNasabah_New.ActiveViewIndex = 1
                            PengirimKorporasinew()

                    End Select
                End If

                '+++++++++++++++++++++++++++++++++++++++++++++BENEFICIARY OWNER+++++++++++++++++++++++++++++++++++++++++++++++'
                If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_Keterlibatan) = True Then
                    Select Case DataNonSwiftIn.BeneficialOwner_Keterlibatan.GetValueOrDefault
                        Case 1
                            BenfOwnerHubunganPemilikDanaNew.Text = DataNonSwiftIn.BeneficialOwner_HubunganDenganPemilikDana
                            BenfOwnerHubunganPemilikDanaOld.Text = DataNonSwiftIn.BeneficialOwner_HubunganDenganPemilikDana_Old

                            If ObjectAntiNull(DataNonSwiftIn.FK_IFTI_BeneficialOwnerType_ID) = True Then
                                Select Case DataNonSwiftIn.FK_IFTI_BeneficialOwnerType_ID.GetValueOrDefault
                                    Case 1
                                        Me.Rb_BOwnerNas_TipePengirimNew.SelectedValue = 1
                                        MultiViewNonSWIFTOutBOwner.ActiveViewIndex = 0
                                        BenefNasabah()
                                    Case 3
                                        Me.Rb_BOwnerNas_TipePengirimNew.SelectedValue = 2
                                        MultiViewNonSWIFTOutBOwner.ActiveViewIndex = 1
                                        BenefNonNasabah()
                                End Select
                            End If

                            If ObjectAntiNull(DataNonSwiftIn.FK_IFTI_BeneficialOwnerType_ID_Old) = True Then
                                Select Case DataNonSwiftIn.FK_IFTI_BeneficialOwnerType_ID_Old.GetValueOrDefault
                                    Case 1
                                        Me.Rb_BOwnerNas_TipePengirimOld.SelectedValue = 1
                                        MultiViewNonSWIFTOutBOwner.ActiveViewIndex = 0
                                        BenefNasabah()
                                    Case 3
                                        Me.Rb_BOwnerNas_TipePengirimOld.SelectedValue = 2
                                        MultiViewNonSWIFTOutBOwner.ActiveViewIndex = 1
                                        BenefNonNasabah()
                                End Select
                            End If


                        Case 2
                            Me.KeterlibatanBenefNew.SelectedValue = DataNonSwiftIn.BeneficialOwner_Keterlibatan.GetValueOrDefault
                            Me.KeterlibatanBenefOld.SelectedValue = DataNonSwiftIn.BeneficialOwner_Keterlibatan_Old.GetValueOrDefault
                            MultiViewNonSWIFTOutBOwner.Visible = False
                            row1a.Visible = False
                            row2a.Visible = False

                    End Select
                End If


                '+++++++++++++++++++++++++++++++++++++++++++++PENERIMA LUAR NEGERI+++++++++++++++++++++++++++++++++++++++++++++++'
                txtNonSwiftOutPenerimaNasabah_IND_RekeningNew.Text = getStringFieldValue(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoRekening)
                txtNonSwiftOutPenerimaNasabah_IND_RekeningOld.Text = getStringFieldValue(Databenefeciary.Beneficiary_Nasabah_INDV_NoRekening)

                If Not Databenefeciary.FK_IFTI_NasabahType_ID = "" Then
                    Select Case Databenefeciary.FK_IFTI_NasabahType_ID.GetValueOrDefault
                        Case 1
                            Me.RbNonSwiftOutPenerimaNonNasabah_TipePenerimaOld.SelectedValue = 1
                            MultiViewNonSwiftOutPenerima.ActiveViewIndex = 0
                        Case 2
                            Me.RbNonSwiftOutPenerimaNonNasabah_TipePenerimaOld.SelectedValue = 1
                            MultiViewNonSwiftOutPenerima.ActiveViewIndex = 0
                        Case 3
                            Me.RbNonSwiftOutPenerimaNonNasabah_TipePenerimaOld.SelectedValue = 2
                            MultiViewNonSwiftOutPenerima.ActiveViewIndex = 1
                            Penerima_LN_NonNasabah()
                    End Select
                End If

                If Not Databenefeciary.FK_IFTI_NasabahType_ID = "" Then
                    Select Case Databenefeciary.FK_IFTI_NasabahType_ID.GetValueOrDefault
                        Case 1
                            Me.RbNonSwiftOutPenerimaNonNasabah_TipeNasabahOld.SelectedValue = 1
                            MultiViewPenerimaAkhirNasabah.ActiveViewIndex = 0
                            Penerima_LN_Nasabah()
                        Case 2
                            Me.RbNonSwiftOutPenerimaNonNasabah_TipeNasabahOld.SelectedValue = 2
                            MultiViewPenerimaAkhirNasabah.ActiveViewIndex = 1
                            Penerima_LN_Korporasi()
                    End Select
                End If

                If Not DatabenefeciaryApproval.FK_IFTI_NasabahType_ID = "" Then
                    Select Case DatabenefeciaryApproval.FK_IFTI_NasabahType_ID.GetValueOrDefault
                        Case 1
                            Me.RbNonSwiftOutPenerimaNonNasabah_TipePenerimaNew.SelectedValue = 1
                            MultiViewPenerimaAkhirNasabah_New.ActiveViewIndex = 0
                            Penerima_LN_Nasabahnew()
                        Case 2
                            Me.RbNonSwiftOutPenerimaNonNasabah_TipePenerimaNew.SelectedValue = 1
                            MultiViewPenerimaAkhirNasabah_New.ActiveViewIndex = 1
                            Penerima_LN_KorporasiNew()
                        Case 3
                            Me.RbNonSwiftOutPenerimaNonNasabah_TipePenerimaNew.SelectedValue = 2

                    End Select
                End If

                'If Not DatabenefeciaryApproval.FK_IFTI_NasabahType_ID = "" Then
                '    Select Case DatabenefeciaryApproval.FK_IFTI_NasabahType_ID.GetValueOrDefault
                '        Case 1
                '            Me.RbNonSwiftOutPenerimaNonNasabah_TipeNasabahNew.SelectedValue = 1

                '        Case 2
                '            Me.RbNonSwiftOutPenerimaNonNasabah_TipeNasabahNew.SelectedValue = 2

                '    End Select
                'End If
                transaksi()
            End If

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub PengirimNasabah()

        '---------------------------------------------Negara-------------------------------------'
        ' Dim idnegara As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara.GetValueOrDefault(0)
        Dim idnegara_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old.GetValueOrDefault(0)
        'Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        'getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '------------------------------------------------pekerjaan-------------------------------------'
        'Dim idpekerjaan As String = DataNonSwiftIn.Sender_Nasabah_INDV_Pekerjaan.GetValueOrDefault(0)
        Dim idpekerjaan_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Pekerjaan_Old.GetValueOrDefault(0)
        'Dim getpekerjaantype As MsPekerjaan
        Dim getpekerjaantype_old As MsPekerjaan
        'getpekerjaantype = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan)
        getpekerjaantype_old = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        'Dim idKotakab As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab_Old.GetValueOrDefault(0)
        'Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        'getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        'Dim idPropinsi As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi.GetValueOrDefault(0)
        Dim idPropinsi_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi_Old.GetValueOrDefault(0)
        'Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        'getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '------------------------------------------------Kota kab Iden---------------------------------------------'
        'Dim idKotakabiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab.GetValueOrDefault(0)
        Dim idKotakabiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab_Old.GetValueOrDefault(0)
        'Dim getKotakabidentype As MsKotaKab
        Dim getKotakabidentype_old As MsKotaKab
        'getKotakabidentype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabidentype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakabiden_old)

        '-------------------------------------------------propinsi Iden------------------------------------------------'
        'Dim idPropinsiiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi.GetValueOrDefault(0)
        Dim idPropinsiiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi_Old.GetValueOrDefault(0)
        'Dim getPropinsiidentype As MsProvince
        Dim getPropinsiidentype_old As MsProvince
        'getPropinsiidentype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsiidentype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsiiden_old)
        '==================================NASABAH INDIVIDU========================================='

        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening = "" Then
        '    TxtNonSWIFTOutPengirim_rekeningNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening_Old = "" Then
            TxtNonSWIFTOutPengirim_rekeningOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening_Old
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap = "" Then
        '    TxtIdenPengirimNas_Ind_NamaLengkapNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old = "" Then
            TxtIdenPengirimNas_Ind_NamaLengkapOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir = "" Then
        '    TxtIdenPengirimNas_Ind_tanggalLahirNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir.GetValueOrDefault
        'End If
        If Not IsNothing(DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old.GetValueOrDefault) Then
            TxtIdenPengirimNas_Ind_tanggalLahirOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old.GetValueOrDefault
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan = "" Then
        '    Rb_IdenPengirimNas_Ind_kewarganegaraanNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan.GetValueOrDefault
        'End If
        If Not IsNothing(DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old.GetValueOrDefault) Then
            Rb_IdenPengirimNas_Ind_kewarganegaraanOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old.GetValueOrDefault
        End If
        'If Not IsNothing(getnegaratype) Then
        '    TxtIdenPengirimNas_Ind_negaraNew.Text = getStringFieldValue(getnegaratype.NamaNegara)
        'End If
        If Not IsNothing(getnegaratype_old) Then
            TxtIdenPengirimNas_Ind_negaraOld.Text = getStringFieldValue(getnegaratype_old.NamaNegara)
        End If
        'If Not IsNothing(DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya) Then
        '    TxtIdenPengirimNas_Ind_negaralainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya_Old = "" Then
            TxtIdenPengirimNas_Ind_negaralainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya_Old
        End If
        'If Not IsNothing(getpekerjaantype) Then
        '    TxtIdenPengirimNas_Ind_pekerjaanNew.Text = getpekerjaantype.NamaPekerjaan
        'End If
        If Not IsNothing(getpekerjaantype_old) Then

            TxtIdenPengirimNas_Ind_pekerjaanOld.Text = getpekerjaantype_old.NamaPekerjaan
        End If

        'TxtIdenPengirimNas_Ind_PekerjaanlainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_PekerjaanLainnya
        TxtIdenPengirimNas_Ind_PekerjaanlainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_PekerjaanLainnya_Old
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat = "" Then
        '    TxtIdenPengirimNas_Ind_AlamatNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Alamat
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat_Old = "" Then
            TxtIdenPengirimNas_Ind_AlamatOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Alamat_Old
        End If
        'If Not IsNothing(getKotakabtype) Then
        '    TxtIdenPengirimNas_Ind_KotaNew.Text = getStringFieldValue(getKotakabtype.NamaKotaKab)
        'End If
        If Not IsNothing(getKotakabtype_old) Then
            TxtIdenPengirimNas_Ind_KotaOld.Text = getStringFieldValue(getKotakabtype_old.NamaKotaKab)
        End If
        'TxtIdenPengirimNas_Ind_KotaLainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old) = True Then
            TxtIdenPengirimNas_Ind_kotalainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old
        End If
        'If Not IsNothing(getPropinsitype) Then
        '    TxtIdenPengirimNas_Ind_provinsiNew.Text = getStringFieldValue(getPropinsitype.Nama)
        'End If
        If Not IsNothing(getPropinsitype_old) Then
            TxtIdenPengirimNas_Ind_provinsiOld.Text = getStringFieldValue(getPropinsitype_old.Nama)
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya) = True Then
        '    TxtIdenPengirimNas_Ind_provinsiLainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya_Old) = True Then
            TxtIdenPengirimNas_Ind_provinsiLainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya_Old
        End If
        'TxtIdenPengirimNas_Ind_alamatIdentitasNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat
        TxtIdenPengirimNas_Ind_alamatIdentitasOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat_Old
        'If Not IsNothing(getKotakabidentype) Then
        '    TxtIdenPengirimNas_Ind_kotaIdentitasNew.Text = getStringFieldValue(getKotakabidentype.NamaKotaKab)
        'End If
        If Not IsNothing(getKotakabidentype_old) Then
            TxtIdenPengirimNas_Ind_kotaIdentitasOld.Text = getStringFieldValue(getKotakabidentype_old.NamaKotaKab)
        End If
        'TxtIdenPengirimNas_Ind_KotaLainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old) = True Then
            TxtIdenPengirimNas_Ind_KotaLainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old
        End If
        'If Not IsNothing(getPropinsiidentype) Then
        '    TxtIdenPengirimNas_Ind_ProvinsiIdenNew.Text = getStringFieldValue(getPropinsiidentype.Nama)
        'End If
        If Not IsNothing(getPropinsiidentype_old) Then
            TxtIdenPengirimNas_Ind_ProvinsiIdenOld.Text = getStringFieldValue(getPropinsiidentype_old.Nama)
        End If
        'TxtIdenPengirimNas_Ind_ProvinsilainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_PropinsiLainnya
        TxtIdenPengirimNas_Ind_ProvinsilainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_PropinsiLainnya_Old
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType) = True Then
        '    CboPengirimNasInd_jenisidentitasNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType.GetValueOrDefault
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old.GetValueOrDefault) = True Then
            CboPengirimNasInd_jenisidentitasOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old.GetValueOrDefault
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NomorID) = True Then
        '    TxtIdenPengirimNas_Ind_noIdentitasNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID
        'End If
        TxtIdenPengirimNas_Ind_noIdentitasOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID_Old
    End Sub
    Private Sub PengirimNasabahNew()

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara.GetValueOrDefault(0)
        'Dim idnegara_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        'Dim getnegaratype_old As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        'getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '------------------------------------------------pekerjaan-------------------------------------'
        Dim idpekerjaan As String = DataNonSwiftIn.Sender_Nasabah_INDV_Pekerjaan.GetValueOrDefault(0)
        'Dim idpekerjaan_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Pekerjaan_Old.GetValueOrDefault(0)
        Dim getpekerjaantype As MsPekerjaan
        'Dim getpekerjaantype_old As MsPekerjaan
        getpekerjaantype = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan)
        'getpekerjaantype_old = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab.GetValueOrDefault(0)
        'Dim idKotakab_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        'Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        'getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi.GetValueOrDefault(0)
        'Dim idPropinsi_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi_Old.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        'Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        'getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '------------------------------------------------Kota kab Iden---------------------------------------------'
        Dim idKotakabiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab.GetValueOrDefault(0)
        'Dim idKotakabiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabidentype As MsKotaKab
        'Dim getKotakabidentype_old As MsKotaKab
        getKotakabidentype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakabiden)
        'getKotakabidentype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi Iden------------------------------------------------'
        Dim idPropinsiiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi.GetValueOrDefault(0)
        'Dim idPropinsiiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi_Old.GetValueOrDefault(0)
        Dim getPropinsiidentype As MsProvince
        'Dim getPropinsiidentype_old As MsProvince
        getPropinsiidentype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsiiden)
        'getPropinsiidentype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)
        '==================================NASABAH INDIVIDU========================================='

        If Not DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening = "" Then
            TxtNonSWIFTOutPengirim_rekeningNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening_Old = "" Then
        '    TxtNonSWIFTOutPengirim_rekeningOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening_Old
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap = "" Then
            TxtIdenPengirimNas_Ind_NamaLengkapNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old = "" Then
        '    TxtIdenPengirimNas_Ind_NamaLengkapOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir = "" Then
            TxtIdenPengirimNas_Ind_tanggalLahirNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir.GetValueOrDefault
        End If
        'If Not IsNothing(DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old.GetValueOrDefault) Then
        '    TxtIdenPengirimNas_Ind_tanggalLahirOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old.GetValueOrDefault
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan = "" Then
            Rb_IdenPengirimNas_Ind_kewarganegaraanNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan.GetValueOrDefault
        End If
        'If Not IsNothing(DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old.GetValueOrDefault) Then
        '    Rb_IdenPengirimNas_Ind_kewarganegaraanOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old.GetValueOrDefault
        'End If
        If Not IsNothing(getnegaratype) Then
            TxtIdenPengirimNas_Ind_negaraNew.Text = getStringFieldValue(getnegaratype.NamaNegara)
        End If
        'If Not IsNothing(getnegaratype) Then
        '    TxtIdenPengirimNas_Ind_negaraOld.Text = getStringFieldValue(getnegaratype_old.NamaNegara)
        'End If
        If Not IsNothing(DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya) Then
            TxtIdenPengirimNas_Ind_negaralainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya_Old = "" Then
        '    TxtIdenPengirimNas_Ind_negaralainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya_Old
        'End If
        If Not IsNothing(getpekerjaantype) Then
            TxtIdenPengirimNas_Ind_pekerjaanNew.Text = getpekerjaantype.NamaPekerjaan
        End If
        'If Not IsNothing(getpekerjaantype_old) Then

        '    TxtIdenPengirimNas_Ind_pekerjaanOld.Text = getpekerjaantype_old.NamaPekerjaan
        'End If

        TxtIdenPengirimNas_Ind_PekerjaanlainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_PekerjaanLainnya
        'TxtIdenPengirimNas_Ind_PekerjaanlainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_PekerjaanLainnya_Old
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat = "" Then
            TxtIdenPengirimNas_Ind_AlamatNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Alamat
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat_Old = "" Then
        '    TxtIdenPengirimNas_Ind_AlamatOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Alamat_Old
        'End If
        If Not IsNothing(getKotakabtype) Then
            TxtIdenPengirimNas_Ind_KotaNew.Text = getStringFieldValue(getKotakabtype.NamaKotaKab)
        End If
        'If Not IsNothing(getKotakabtype_old) Then
        '    TxtIdenPengirimNas_Ind_KotaOld.Text = getStringFieldValue(getKotakabtype_old.NamaKotaKab)
        'End If
        TxtIdenPengirimNas_Ind_KotaLainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old) = True Then
        '    TxtIdenPengirimNas_Ind_kotalainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old
        'End If
        If Not IsNothing(getPropinsitype) Then
            TxtIdenPengirimNas_Ind_provinsiNew.Text = getStringFieldValue(getPropinsitype.Nama)
        End If
        'If Not IsNothing(getPropinsitype_old) Then
        '    TxtIdenPengirimNas_Ind_provinsiOld.Text = getStringFieldValue(getPropinsitype_old.Nama)
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya) = True Then
            TxtIdenPengirimNas_Ind_provinsiLainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya_Old) = True Then
        '    TxtIdenPengirimNas_Ind_provinsiLainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya_Old
        'End If
        TxtIdenPengirimNas_Ind_alamatIdentitasNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat
        'TxtIdenPengirimNas_Ind_alamatIdentitasOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat_Old
        If Not IsNothing(getKotakabidentype) Then
            TxtIdenPengirimNas_Ind_kotaIdentitasNew.Text = getStringFieldValue(getKotakabidentype.NamaKotaKab)
        End If
        'If Not IsNothing(getKotakabidentype_old) Then
        '    TxtIdenPengirimNas_Ind_kotaIdentitasOld.Text = getStringFieldValue(getKotakabidentype_old.NamaKotaKab)
        'End If
        TxtIdenPengirimNas_Ind_KotaLainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old) = True Then
        '    TxtIdenPengirimNas_Ind_KotaLainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old
        'End If
        If Not IsNothing(getPropinsiidentype) Then
            TxtIdenPengirimNas_Ind_ProvinsiIdenNew.Text = getStringFieldValue(getPropinsiidentype.Nama)
        End If
        'If Not IsNothing(getPropinsiidentype_old) Then
        '    TxtIdenPengirimNas_Ind_ProvinsiIdenOld.Text = getStringFieldValue(getPropinsiidentype_old.Nama)
        'End If
        TxtIdenPengirimNas_Ind_ProvinsilainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_PropinsiLainnya
        'TxtIdenPengirimNas_Ind_ProvinsilainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_PropinsiLainnya_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType) = True Then
            CboPengirimNasInd_jenisidentitasNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType.GetValueOrDefault
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old.GetValueOrDefault) = True Then
        '    CboPengirimNasInd_jenisidentitasOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old.GetValueOrDefault
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NomorID) = True Then
            TxtIdenPengirimNas_Ind_noIdentitasNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID
        End If
        'TxtIdenPengirimNas_Ind_noIdentitasOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID_Old
    End Sub
    Private Sub PengirimKorporasi()

        '---------------------------------------Bentuk Badan Usaha-------------------------------'
        'Dim idBentukBadan As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim idBentukBadan_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id_Old.GetValueOrDefault(0)
        'Dim getBentukbadantype As MsBentukBidangUsaha
        Dim getBentukbadantype_old As MsBentukBidangUsaha
        'getBentukbadantype = DataRepository.MsBentukBidangUsahaProvider.GetByIdBidangUsaha(idBentukBadan)
        getBentukbadantype_old = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentukBadan_old)

        '-------------------------------------------Badan Usaha----------------------------------'
        'Dim idBadan As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        Dim idBadan_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old.GetValueOrDefault(0)
        'Dim getbadantype As MsBidangUsaha
        Dim getbadantype_old As MsBidangUsaha
        'getbadantype = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)
        getbadantype_old = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        'Dim idKotakab As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab_Old.GetValueOrDefault(0)
        'Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        'getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        'Dim idPropinsi As String = DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi.GetValueOrDefault(0)
        Dim idPropinsi_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi_Old.GetValueOrDefault(0)
        'Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        'getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '=========================================KORPORASI============================================='

        'If Not IsNothing(getBentukbadantype) Then
        '    TxtIdenPengirimNas_Corp_BentukBadanUsahaNew.Text = getBentukbadantype.BentukBidangUsaha
        'End If
        If Not IsNothing(getBentukbadantype_old) Then
            TxtIdenPengirimNas_Corp_BentukBadanUsahaOld.Text = getBentukbadantype_old.BentukBidangUsaha
        End If
        'TxtIdenPengirimNas_Corp_BentukBadanUsahaLainNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old) = True Then
            TxtIdenPengirimNas_Corp_BentukBadanUsahaLainOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old
        End If
        'TxtIdenPengirimNas_Corp_NamaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi
        TxtIdenPengirimNas_Corp_NamaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi_Old

        'If Not IsNothing(getbadantype) Then
        '    TxtIdenPengirimNas_Corp_BidangUsahaKorpNew.Text = getbadantype.NamaBidangUsaha
        'End If

        If Not IsNothing(getbadantype_old) Then
            TxtIdenPengirimNas_Corp_BidangUsahaKorpOld.Text = getbadantype_old.NamaBidangUsaha
        End If

        'TxtIdenPengirimNas_Corp_BidangUsahaLainKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BidangUsahaLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_BidangUsahaLainnya_Old) = True Then
            TxtIdenPengirimNas_Corp_BidangUsahaLainKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BidangUsahaLainnya_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap) = True Then
        '    TxtIdenPengirimNas_Corp_alamatkorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap_Old) = True Then
            TxtIdenPengirimNas_Corp_alamatkorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap_Old
        End If
        'If Not IsNothing(getKotakabtype) Then
        '    TxtIdenPengirimNas_Corp_kotakorpNew.Text = getKotakabtype.NamaKotaKab
        'End If
        If Not IsNothing(getKotakabtype_old) Then
            TxtIdenPengirimNas_Corp_kotakorpOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        'TxtIdenPengirimNas_Corp_kotakorplainNew.Text = DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya_Old) = True Then
            TxtIdenPengirimNas_Corp_kotakorplainOld.Text = DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya_Old
        End If
        'If Not IsNothing(getPropinsitype) Then
        '    TxtIdenPengirimNas_Corp_provKorpNew.Text = getPropinsitype.Nama
        'End If
        If Not IsNothing(getPropinsitype_old) Then
            TxtIdenPengirimNas_Corp_provKorpOld.Text = getPropinsitype_old.Nama
        End If
        'TxtIdenPengirimNas_Corp_provKorpLainNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya_Old) = True Then
            TxtIdenPengirimNas_Corp_provKorpLainOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap) = True Then
        '    TxtIdenPengirimNas_Corp_AlamatLuarNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap_Old) = True Then
            TxtIdenPengirimNas_Corp_AlamatLuarOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim) = True Then
        '    TxtIdenPengirimNas_Corp_AlamatAsalNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
        'End If
        TxtIdenPengirimNas_Corp_AlamatAsalOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim_Old

    End Sub
    Private Sub PengirimKorporasiNew()

        '---------------------------------------Bentuk Badan Usaha-------------------------------'
        Dim idBentukBadan As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        'Dim idBentukBadan_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id_Old.GetValueOrDefault(0)
        Dim getBentukbadantype As MsBentukBidangUsaha
        'Dim getBentukbadantype_old As MsBentukBidangUsaha
        getBentukbadantype = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentukBadan)
        'getBentukbadantype_old = DataRepository.MsBentukBidangUsahaProvider.GetByIdBidangUsaha(idBentukBadan)

        '-------------------------------------------Badan Usaha----------------------------------'
        Dim idBadan As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        'Dim idBadan_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old.GetValueOrDefault(0)
        Dim getbadantype As MsBidangUsaha
        'Dim getbadantype_old As MsBidangUsaha
        getbadantype = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)
        'getbadantype_old = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab.GetValueOrDefault(0)
        'Dim idKotakab_old As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        'Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        'getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi.GetValueOrDefault(0)
        'Dim idPropinsi_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi_Old.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        'Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        'getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '=========================================KORPORASI============================================='

        If Not IsNothing(getBentukbadantype) Then
            TxtIdenPengirimNas_Corp_BentukBadanUsahaNew.Text = getBentukbadantype.BentukBidangUsaha
        End If
        'If Not IsNothing(getBentukbadantype_old) Then
        '    TxtIdenPengirimNas_Corp_BentukBadanUsahaOld.Text = getBentukbadantype_old.BentukBidangUsaha
        'End If
        TxtIdenPengirimNas_Corp_BentukBadanUsahaLainNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old) = True Then
        '    TxtIdenPengirimNas_Corp_BentukBadanUsahaLainOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old
        'End If
        TxtIdenPengirimNas_Corp_NamaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi
        'TxtIdenPengirimNas_Corp_NamaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi_Old

        If Not IsNothing(getbadantype) Then
            TxtIdenPengirimNas_Corp_BidangUsahaKorpNew.Text = getbadantype.NamaBidangUsaha
        End If

        'If Not IsNothing(getbadantype_old) Then
        '    TxtIdenPengirimNas_Corp_BidangUsahaKorpOld.Text = getbadantype_old.NamaBidangUsaha
        'End If

        TxtIdenPengirimNas_Corp_BidangUsahaLainKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BidangUsahaLainnya
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_BidangUsahaLainnya_Old) = True Then
        '    TxtIdenPengirimNas_Corp_BidangUsahaLainKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BidangUsahaLainnya_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap) = True Then
            TxtIdenPengirimNas_Corp_alamatkorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap_Old) = True Then
        '    TxtIdenPengirimNas_Corp_alamatkorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap_Old
        'End If
        If Not IsNothing(getKotakabtype) Then
            TxtIdenPengirimNas_Corp_kotakorpNew.Text = getKotakabtype.NamaKotaKab
        End If
        'If Not IsNothing(getKotakabtype_old) Then
        '    TxtIdenPengirimNas_Corp_kotakorpOld.Text = getKotakabtype_old.NamaKotaKab
        'End If
        TxtIdenPengirimNas_Corp_kotakorplainNew.Text = DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya_Old) = True Then
        '    TxtIdenPengirimNas_Corp_kotakorplainOld.Text = DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya_Old
        'End If
        If Not IsNothing(getPropinsitype) Then
            TxtIdenPengirimNas_Corp_provKorpNew.Text = getPropinsitype.Nama
        End If
        'If Not IsNothing(getPropinsitype_old) Then
        '    TxtIdenPengirimNas_Corp_provKorpOld.Text = getPropinsitype_old.Nama
        'End If
        TxtIdenPengirimNas_Corp_provKorpLainNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya_Old) = True Then
        '    TxtIdenPengirimNas_Corp_provKorpLainOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap) = True Then
            TxtIdenPengirimNas_Corp_AlamatLuarNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap_Old) = True Then
        '    TxtIdenPengirimNas_Corp_AlamatLuarOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim) = True Then
            TxtIdenPengirimNas_Corp_AlamatAsalNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
        End If
        'TxtIdenPengirimNas_Corp_AlamatAsalOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim_Old

    End Sub
    Private Sub PengirimNonNasabah()

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DataNonSwiftIn.Sender_NonNasabah_ID_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = DataNonSwiftIn.Sender_NonNasabah_ID_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DataNonSwiftIn.Sender_NonNasabah_ID_Propinsi.GetValueOrDefault(0)
        Dim idPropinsi_old As String = DataNonSwiftIn.Sender_NonNasabah_ID_Propinsi_Old.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '==============================================NON NASABAH==========================================='
        RbPengirimNonNasabah_100Juta.SelectedValue = DataNonSwiftIn.FK_IFTI_NonNasabahNominalType_ID
        TxtPengirimNonNasabah_namaNew.Text = DataNonSwiftIn.Sender_NonNasabah_NamaLengkap
        TxtPengirimNonNasabah_namaOld.Text = DataNonSwiftIn.Sender_NonNasabah_NamaLengkap_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_TanggalLahir) = True Then
            TxtPengirimNonNasabah_TanggalLahirNew.Text = DataNonSwiftIn.Sender_NonNasabah_TanggalLahir
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_TanggalLahir_Old) = True Then
            TxtPengirimNonNasabah_TanggalLahirOld.Text = DataNonSwiftIn.Sender_NonNasabah_TanggalLahir_Old.GetValueOrDefault
        End If
        TxtPengirimNonNasabah_alamatidenNew.Text = DataNonSwiftIn.Sender_NonNasabah_ID_Alamat
        TxtPengirimNonNasabah_alamatidenOld.Text = DataNonSwiftIn.Sender_NonNasabah_ID_Alamat_Old
        If Not IsNothing(getKotakabtype) Then
            TxtPengirimNonNasabah_kotaIdenNew.Text = getKotakabtype.NamaKotaKab
        End If
        If Not IsNothing(getKotakabtype_old) Then
            TxtPengirimNonNasabah_kotaIdenOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        TxtPengirimNonNasabah_KotaLainIdenNew.Text = DataNonSwiftIn.Sender_NonNasabah_ID_KotaKabLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_KotaKabLainnya_Old) = True Then
            TxtPengirimNonNasabah_KotaLainIdenOld.Text = DataNonSwiftIn.Sender_NonNasabah_ID_KotaKabLainnya_Old
        End If
        If Not IsNothing(getPropinsitype) Then
            TxtPengirimNonNasabah_ProvIdenNew.Text = getPropinsitype.Nama
        End If
        If Not IsNothing(getPropinsitype_old) Then
            TxtPengirimNonNasabah_ProvIdenOld.Text = getPropinsitype_old.Nama
        End If
        TxtPengirimNonNasabah_ProvLainIdenNew.Text = DataNonSwiftIn.Sender_NonNasabah_ID_PropinsiLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_PropinsiLainnya_Old) = True Then
            TxtPengirimNonNasabah_ProvLainIdenOld.Text = DataNonSwiftIn.Sender_NonNasabah_ID_PropinsiLainnya_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_FK_IFTI_IDType) = True Then
            CboPengirimNonNasabah_JenisDokumenNew.SelectedValue = DataNonSwiftIn.Sender_NonNasabah_FK_IFTI_IDType
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_FK_IFTI_IDType_Old) = True Then
            CboPengirimNonNasabah_JenisDokumenOld.SelectedValue = DataNonSwiftIn.Sender_NonNasabah_FK_IFTI_IDType_Old
        End If
        TxtPengirimNonNasabah_NomorIdenNew.Text = DataNonSwiftIn.Sender_NonNasabah_NomorID
        TxtPengirimNonNasabah_NomorIdenOld.Text = DataNonSwiftIn.Sender_NonNasabah_NomorID_Old
    End Sub
    Private Sub BenefNasabah()

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DataNonSwiftIn.BeneficialOwner_ID_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = DataNonSwiftIn.BeneficialOwner_ID_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DataNonSwiftIn.BeneficialOwner_ID_Propinsi.GetValueOrDefault(0)
        Dim idPropinsi_old As String = DataNonSwiftIn.BeneficialOwner_ID_Propinsi_Old.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '============================================NASABAH==========================================='
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_NoRekening) = True Then
            BenfOwnerNasabah_rekeningNew.Text = DataNonSwiftIn.BeneficialOwner_NoRekening
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_NoRekening_Old) = True Then
            BenfOwnerNasabah_rekeningOld.Text = DataNonSwiftIn.BeneficialOwner_NoRekening_Old
        End If
        BenfOwnerNasabah_NamaNew.Text = DataNonSwiftIn.BeneficialOwner_NamaLengkap
        BenfOwnerNasabah_NamaOld.Text = DataNonSwiftIn.BeneficialOwner_NamaLengkap_Old

        If Not IsNothing(DataNonSwiftIn.BeneficialOwner_TanggalLahir) Then
            BenfOwnerNasabah_tanggalLahirNew.Text = DataNonSwiftIn.BeneficialOwner_TanggalLahir.GetValueOrDefault
        End If
        If Not IsNothing(DataNonSwiftIn.BeneficialOwner_TanggalLahir_Old) Then
            BenfOwnerNasabah_tanggalLahirOld.Text = DataNonSwiftIn.BeneficialOwner_TanggalLahir_Old
        End If

        BenfOwnerNasabah_AlamatIdenNew.Text = DataNonSwiftIn.BeneficialOwner_ID_Alamat
        BenfOwnerNasabah_AlamatIdenOld.Text = DataNonSwiftIn.BeneficialOwner_ID_Alamat_Old
        If Not IsNothing(getKotakabtype) Then
            BenfOwnerNasabah_KotaIdenNew.Text = getKotakabtype.NamaKotaKab
        End If
        If Not IsNothing(getKotakabtype_old) Then
            BenfOwnerNasabah_KotaIdenOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        BenfOwnerNasabah_kotaLainIdenNew.Text = DataNonSwiftIn.BeneficialOwner_ID_KotaKabLainnya
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_ID_KotaKabLainnya_Old) = True Then
            BenfOwnerNasabah_kotaLainIdenOld.Text = DataNonSwiftIn.BeneficialOwner_ID_KotaKabLainnya_Old
        End If
        If Not IsNothing(getPropinsitype) Then
            BenfOwnerNasabah_ProvinsiIdenNew.Text = getPropinsitype.Nama
        End If
        If Not IsNothing(getPropinsitype_old) Then
            BenfOwnerNasabah_ProvinsiIdenOld.Text = getPropinsitype_old.Nama
        End If
        BenfOwnerNasabah_ProvinsiLainIdenNew.Text = DataNonSwiftIn.BeneficialOwner_ID_PropinsiLainnya
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_ID_PropinsiLainnya_Old) = True Then
            BenfOwnerNasabah_ProvinsiLainIdenOld.Text = DataNonSwiftIn.BeneficialOwner_ID_PropinsiLainnya_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType) = True Then
            CBOBenfOwnerNasabah_JenisDokumenNew.SelectedValue = DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType_Old) = True Then
            CBOBenfOwnerNasabah_JenisDokumenOld.SelectedValue = DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_NomorID) = True Then
            BenfOwnerNasabah_NomorIdenMew.Text = DataNonSwiftIn.BeneficialOwner_NomorID
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_NomorID_Old) = True Then
            BenfOwnerNasabah_NomorIdenOld.Text = DataNonSwiftIn.BeneficialOwner_NomorID_Old
        End If
    End Sub
    Private Sub BenefNonNasabah()

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DataNonSwiftIn.BeneficialOwner_ID_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = DataNonSwiftIn.BeneficialOwner_ID_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DataNonSwiftIn.BeneficialOwner_ID_Propinsi.GetValueOrDefault(0)
        Dim idPropinsi_old As String = DataNonSwiftIn.BeneficialOwner_ID_Propinsi_Old.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '==================================================NON NASABAH=========================================='

        BenfOwnerNonNasabah_NamaNew.Text = DataNonSwiftIn.BeneficialOwner_NamaLengkap
        BenfOwnerNonNasabah_NamaOld.Text = DataNonSwiftIn.BeneficialOwner_NamaLengkap_Old
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_TanggalLahir) = True Then
            BenfOwnerNonNasabah_TanggalLahirOld.Text = DataNonSwiftIn.BeneficialOwner_TanggalLahir
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_TanggalLahir_Old) = True Then
            BenfOwnerNonNasabah_TanggalLahirNew.Text = DataNonSwiftIn.BeneficialOwner_TanggalLahir_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_ID_Alamat) = True Then
            BenfOwnerNonNasabah_AlamatIdenOld.Text = DataNonSwiftIn.BeneficialOwner_ID_Alamat
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_ID_Alamat_Old) = True Then
            BenfOwnerNonNasabah_AlamatIdenNew.Text = DataNonSwiftIn.BeneficialOwner_ID_Alamat_Old
        End If
        BenfOwnerNonNasabah_KotaIdenOld.Text = getKotakabtype.NamaKotaKab
        BenfOwnerNonNasabah_KotaIdenNew.Text = getKotakabtype_old.NamaKotaKab
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_ID_KotaKabLainnya_Old) = True Then
            BenfOwnerNonNasabah_KotaLainIdenOld.Text = DataNonSwiftIn.BeneficialOwner_ID_KotaKabLainnya_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_ID_KotaKabLainnya) = True Then
            BenfOwnerNonNasabah_KotaLainIdenNew.Text = DataNonSwiftIn.BeneficialOwner_ID_KotaKabLainnya
        End If
        If Not IsNothing(getPropinsitype) Then
            BenfOwnerNonNasabah_ProvinsiIdennew.Text = getPropinsitype.Nama
        End If
        If Not IsNothing(getPropinsitype_old) Then
            BenfOwnerNonNasabah_ProvinsiIdenOld.Text = getPropinsitype_old.Nama
        End If


        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_ID_PropinsiLainnya) = True Then
            BenfOwnerNonNasabah_ProvinsiLainIdenNew.Text = DataNonSwiftIn.BeneficialOwner_ID_PropinsiLainnya
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_ID_PropinsiLainnya_Old) = True Then
            BenfOwnerNonNasabah_ProvinsiLainIdenOld.Text = DataNonSwiftIn.BeneficialOwner_ID_PropinsiLainnya_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType) = True Then
            CBOBenfOwnerNonNasabah_JenisDokumenOld.SelectedValue = DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType_Old) = True Then
            CBOBenfOwnerNonNasabah_JenisDokumenNew.SelectedValue = DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType_Old
        End If
        BenfOwnerNonNasabah_NomorIdentitasOld.Text = DataNonSwiftIn.BeneficialOwner_NomorID
        BenfOwnerNonNasabah_NomorIdentitasNew.Text = DataNonSwiftIn.BeneficialOwner_NomorID_Old
    End Sub
    Private Sub Penerima_LN_Nasabahnew()
        '============================================INDIVIDU==========================================='

        txtPenerimaNasabah_IND_namaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NamaLengkap
        'txtPenerimaNasabah_IND_namaOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NamaLengkap
        txtPenerimaNasabah_IND_TanggalLahirNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir.GetValueOrDefault
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_TanggalLahir.GetValueOrDefault) = True Then
        '    txtPenerimaNasabah_IND_TanggalLahirOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_TanggalLahir.GetValueOrDefault
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan) = True Then
            RbPenerimaNasabah_IND_WarganegaraNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan.GetValueOrDefault
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_KewargaNegaraan) = True Then
        '    RbPenerimaNasabah_IND_WarganegaraOld.SelectedValue = Databenefeciary.Beneficiary_Nasabah_INDV_KewargaNegaraan.GetValueOrDefault
        'End If
        If DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Negara.GetValueOrDefault Then
            txtPenerimaNasabah_IND_negaraNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Negara
        End If
        'If Databenefeciary.Beneficiary_Nasabah_INDV_Negara.GetValueOrDefault Then
        '    txtPenerimaNasabah_IND_negaraOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_Negara
        'End If

        txtPenerimaNasabah_IND_negaraLainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NegaraLainnya
        'txtPenerimaNasabah_IND_negaraLainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NegaraLainnya
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Dom) = True Then
            txtPenerimaNasabah_IND_alamatIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Dom) = True Then
        '    txtPenerimaNasabah_IND_alamatIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraBagian) = True Then
            txtPenerimaNasabah_IND_negaraBagianNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraBagian
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_NegaraBagian) = True Then
        '    txtPenerimaNasabah_IND_negaraBagianOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_NegaraBagian
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Negara.GetValueOrDefault) = True Then
            txtPenerimaNasabah_IND_negaraIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Negara.GetValueOrDefault
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_Negara.GetValueOrDefault) = True Then
        '    txtPenerimaNasabah_IND_negaraIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Negara.GetValueOrDefault
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType.GetValueOrDefault) = True Then
            CBOPenerimaNasabah_IND_JenisIdenNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType.GetValueOrDefault
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_FK_IFTI_IDType.GetValueOrDefault) = True Then
        '    CBOPenerimaNasabah_IND_JenisIdenOld.SelectedValue = Databenefeciary.Beneficiary_Nasabah_INDV_FK_IFTI_IDType.GetValueOrDefault
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID) = True Then
            txtPenerimaNasabah_IND_NomorIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_NomorID) = True Then
        '    txtPenerimaNasabah_IND_NomorIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NomorID
        'End If
    End Sub
    Private Sub Penerima_LN_Nasabah()
        '============================================INDIVIDU==========================================='

        'txtPenerimaNasabah_IND_namaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NamaLengkap
        txtPenerimaNasabah_IND_namaOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NamaLengkap
        'txtPenerimaNasabah_IND_TanggalLahirNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir.GetValueOrDefault
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_TanggalLahir.GetValueOrDefault) = True Then
            txtPenerimaNasabah_IND_TanggalLahirOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_TanggalLahir.GetValueOrDefault
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan) = True Then
        '    RbPenerimaNasabah_IND_WarganegaraNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan.GetValueOrDefault
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_KewargaNegaraan) = True Then
            RbPenerimaNasabah_IND_WarganegaraOld.SelectedValue = Databenefeciary.Beneficiary_Nasabah_INDV_KewargaNegaraan.GetValueOrDefault
        End If
        'If DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Negara.GetValueOrDefault Then
        '    txtPenerimaNasabah_IND_negaraNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Negara
        'End If
        If Databenefeciary.Beneficiary_Nasabah_INDV_Negara.GetValueOrDefault Then
            txtPenerimaNasabah_IND_negaraOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_Negara
        End If

        'txtPenerimaNasabah_IND_negaraLainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NegaraLainnya
        txtPenerimaNasabah_IND_negaraLainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NegaraLainnya
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Dom) = True Then
        '    txtPenerimaNasabah_IND_alamatIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Dom) = True Then
            txtPenerimaNasabah_IND_alamatIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraBagian) = True Then
        '    txtPenerimaNasabah_IND_negaraBagianNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraBagian
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_NegaraBagian) = True Then
            txtPenerimaNasabah_IND_negaraBagianOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_NegaraBagian
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Negara.GetValueOrDefault) = True Then
        '    txtPenerimaNasabah_IND_negaraIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Negara.GetValueOrDefault
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_Negara.GetValueOrDefault) = True Then
            txtPenerimaNasabah_IND_negaraIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Negara.GetValueOrDefault
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType.GetValueOrDefault) = True Then
        '    CBOPenerimaNasabah_IND_JenisIdenNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType.GetValueOrDefault
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_FK_IFTI_IDType.GetValueOrDefault) = True Then
            CBOPenerimaNasabah_IND_JenisIdenOld.SelectedValue = Databenefeciary.Beneficiary_Nasabah_INDV_FK_IFTI_IDType.GetValueOrDefault
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID) = True Then
        '    txtPenerimaNasabah_IND_NomorIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_NomorID) = True Then
            txtPenerimaNasabah_IND_NomorIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NomorID
        End If
    End Sub
    Private Sub Penerima_LN_Korporasi()

        '------------------------------------------------Bentuk Badan Usaha---------------------------------------'
        'Dim idBentukBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim idBentukBadan_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        'Dim getBentukbadantype As MsBentukBidangUsaha
        Dim getBentukbadantype_old As MsBentukBidangUsaha
        'getBentukbadantype = DataRepository.MsBentukBidangUsahaProvider.GetByIdBidangUsaha(idBentukBadan)
        getBentukbadantype_old = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentukBadan_old)

        '------------------------------------------------Badan Usaha---------------------------------------'
        'Dim idBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        Dim idBadan_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        'Dim getbadantype As MsBidangUsaha
        Dim getbadantype_old As MsBidangUsaha
        'getbadantype = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)
        getbadantype_old = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        'Dim idNegara As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Negara.GetValueOrDefault(0)
        Dim idNegara_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_ID_Negara.GetValueOrDefault(0)
        'Dim getNegaratype As MsNegara
        Dim getNegaratype_old As MsNegara
        'getNegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idNegara)
        getNegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idNegara_old)

        '====================================================KORPORASI============================================='
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id) = True Then
        '    txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaNew.Text = getBentukbadantype.BentukBidangUsaha
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id) = True Then
            txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaOld.Text = getBentukbadantype_old.BentukBidangUsaha
        End If

        'txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaLainnyaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya) = True Then
            txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaLainnyaOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
        '    txtNonSwiftOutPenerimaNasabah_Korp_namaKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
            txtNonSwiftOutPenerimaNasabah_Korp_namaKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NamaKorporasi
        End If

        'If Not IsNothing(getbadantype) Then
        '    txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaKorpNew.Text = getbadantype.NamaBidangUsaha
        'End If
        If Not IsNothing(getbadantype_old) Then
            txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaKorpOld.Text = getbadantype_old.NamaBidangUsaha
        End If

        'txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaLainKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_BidangUsahaLainnya) = True Then
            txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaLainKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        End If
        'txtNonSwiftOutPenerimaNasabah_Korp_AlamatKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_AlamatLengkap
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_AlamatLengkap) = True Then
            txtNonSwiftOutPenerimaNasabah_Korp_AlamatKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_AlamatLengkap
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_NegaraBagian) = True Then
        '    txtNonSwiftOutPenerimaNasabah_Korp_NegaraBagianNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_NegaraBagian
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_ID_NegaraBagian) = True Then
            txtNonSwiftOutPenerimaNasabah_Korp_NegaraBagianOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_ID_NegaraBagian
        End If
        'If Not IsNothing(getNegaratype) Then
        '    txtNonSwiftOutPenerimaNasabah_Korp_NegaraNew.Text = getNegaratype.NamaNegara
        'End If
        If Not IsNothing(getNegaratype_old) Then
            txtNonSwiftOutPenerimaNasabah_Korp_NegaraOld.Text = getNegaratype_old.NamaNegara
        End If

        'txtNonSwiftOutPenerimaNasabah_Korp_NegaralainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
        txtNonSwiftOutPenerimaNasabah_Korp_NegaralainOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
    End Sub
    Private Sub Penerima_LN_KorporasiNew()

        '------------------------------------------------Bentuk Badan Usaha---------------------------------------'
        Dim idBentukBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        'Dim idBentukBadan_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim getBentukbadantype As MsBentukBidangUsaha
        'Dim getBentukbadantype_old As MsBentukBidangUsaha
        getBentukbadantype = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentukBadan)
        'getBentukbadantype_old = DataRepository.MsBentukBidangUsahaProvider.GetByIdBidangUsaha(idBentukBadan)

        '------------------------------------------------Badan Usaha---------------------------------------'
        Dim idBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        'Dim idBadan_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        Dim getbadantype As MsBidangUsaha
        'Dim getbadantype_old As MsBidangUsaha
        getbadantype = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)
        'getbadantype_old = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idNegara As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Negara.GetValueOrDefault(0)
        'Dim idNegara_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_ID_Negara.GetValueOrDefault(0)
        Dim getNegaratype As MsNegara
        'Dim getNegaratype_old As MsNegara
        getNegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idNegara)
        'getNegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idNegara_old)

        '====================================================KORPORASI============================================='
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id) = True Then
            txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaNew.Text = getBentukbadantype.BentukBidangUsaha
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id) = True Then
        '    txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaOld.Text = getBentukbadantype_old.BentukBidangUsaha
        'End If

        txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaLainnyaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya) = True Then
        '    txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaLainnyaOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
            txtNonSwiftOutPenerimaNasabah_Korp_namaKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
        '    txtNonSwiftOutPenerimaNasabah_Korp_namaKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NamaKorporasi
        'End If

        If Not IsNothing(getbadantype) Then
            txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaKorpNew.Text = getbadantype.NamaBidangUsaha
        End If
        'If Not IsNothing(getbadantype_old) Then
        '    txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaKorpOld.Text = getbadantype_old.NamaBidangUsaha
        'End If

        txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaLainKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_BidangUsahaLainnya) = True Then
        '    txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaLainKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        'End If
        txtNonSwiftOutPenerimaNasabah_Korp_AlamatKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_AlamatLengkap
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_AlamatLengkap) = True Then
        '    txtNonSwiftOutPenerimaNasabah_Korp_AlamatKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_AlamatLengkap
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_NegaraBagian) = True Then
            txtNonSwiftOutPenerimaNasabah_Korp_NegaraBagianNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_NegaraBagian
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_ID_NegaraBagian) = True Then
        '    txtNonSwiftOutPenerimaNasabah_Korp_NegaraBagianOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_ID_NegaraBagian
        'End If
        If Not IsNothing(getNegaratype) Then
            txtNonSwiftOutPenerimaNasabah_Korp_NegaraNew.Text = getNegaratype.NamaNegara
        End If
        'If Not IsNothing(getNegaratype_old) Then
        '    txtNonSwiftOutPenerimaNasabah_Korp_NegaraOld.Text = getNegaratype_old.NamaNegara
        'End If

        txtNonSwiftOutPenerimaNasabah_Korp_NegaralainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
        'txtNonSwiftOutPenerimaNasabah_Korp_NegaralainOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
    End Sub
    Private Sub Penerima_LN_NonNasabah()

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_Negara.GetValueOrDefault(0)
        Dim idnegara_old As String = Databenefeciary.Beneficiary_NonNasabah_ID_Negara.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '==================================================NON NASABAH=========================================='
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_KodeRahasia) = True Then
            TxtNonSwiftOutPenerimaNonNasabah_KodeRahasaNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_KodeRahasia
        End If
        TxtNonSwiftOutPenerimaNonNasabah_KodeRahasaOld.Text = Databenefeciary.Beneficiary_NonNasabah_KodeRahasia
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NoRekening) = True Then
            TxtNonSwiftOutPenerimaNonNasabah_NoRekNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NoRekening
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_NoRekening) = True Then
            TxtNonSwiftOutPenerimaNonNasabah_NoRekOld.Text = Databenefeciary.Beneficiary_NonNasabah_NoRekening
        End If
        TxtNonSwiftOutPenerimaNonNasabah_namaBankNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaBank
        TxtNonSwiftOutPenerimaNonNasabah_namaBankOld.Text = Databenefeciary.Beneficiary_NonNasabah_NamaBank
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaLengkap) = True Then
            TxtNonSwiftOutPenerimaNonNasabah_namaNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaLengkap
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_NamaLengkap) = True Then
            TxtNonSwiftOutPenerimaNonNasabah_namaOld.Text = Databenefeciary.Beneficiary_NonNasabah_NamaLengkap
        End If
        TxtNonSwiftOutPenerimaNonNasabah_alamatidenNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_Alamat
        TxtNonSwiftOutPenerimaNonNasabah_alamatidenOld.Text = Databenefeciary.Beneficiary_NonNasabah_ID_Alamat
        If Not IsNothing(getnegaratype) Then
            txtPenerimaNonNasabah_NegaraNew.Text = getnegaratype.NamaNegara
        End If
        If Not IsNothing(getnegaratype_old) Then
            txtPenerimaNonNasabah_NegaraOld.Text = getnegaratype_old.NamaNegara
        End If

        txtPenerimaNonNasabah_NegaraLainNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_NegaraLainnya
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_ID_NegaraLainnya) = True Then
            txtPenerimaNonNasabah_NegaraLainOld.Text = Databenefeciary.Beneficiary_NonNasabah_ID_NegaraLainnya
        End If

    End Sub
    Private Sub transaksi()
        '+++++++++++++++++++++++++++++++++++++++++++++TRANSAKSI+++++++++++++++++++++++++++++++++++++++++++++'
        Dim idCurrent As String = DataNonSwiftIn.ValueDate_FK_Currency_ID.GetValueOrDefault(0)
        Dim idcurrent_old As String = DataNonSwiftIn.ValueDate_FK_Currency_ID_Old.GetValueOrDefault(0)
        Dim getbadantype As MsCurrency
        Dim getbadantype_old As MsCurrency
        getbadantype = DataRepository.MsCurrencyProvider.GetByIdCurrency(idCurrent)
        getbadantype_old = DataRepository.MsCurrencyProvider.GetByIdCurrency(idcurrent_old)

        Dim idCurrent1 As String = DataNonSwiftIn.Instructed_Currency.GetValueOrDefault(0)
        Dim idcurrent1_old As String = DataNonSwiftIn.Instructed_Currency_Old.GetValueOrDefault(0)
        Dim getbadantype1 As MsCurrency
        Dim getbadantype1_old As MsCurrency
        getbadantype1 = DataRepository.MsCurrencyProvider.GetByIdCurrency(idCurrent1)
        getbadantype1_old = DataRepository.MsCurrencyProvider.GetByIdCurrency(idcurrent1_old)

        Transaksi_NonSwiftOuttanggalNew.Text = getStringFieldValue(DataNonSwiftIn.TanggalTransaksi)
        Transaksi_NonSwiftOuttanggalOld.Text = getStringFieldValue(DataNonSwiftIn.TanggalTransaksi_Old)
        Transaksi_NonSwiftOutkantorCabangPengirimNew.Text = getStringFieldValue(DataNonSwiftIn.KantorCabangPenyelengaraPengirimAsal)
        Transaksi_NonSwiftOutkantorCabangPengirimOld.Text = getStringFieldValue(DataNonSwiftIn.KantorCabangPenyelengaraPengirimAsal_Old)
        'Transaksi_NonSwiftOutValueTanggalTransaksiNew.Text = getStringFieldValue(getbadantype.Name)
        'Transaksi_NonSwiftOutValueTanggalTransaksiOld.Text = getStringFieldValue(getbadantype_old.Name)
        Transaksi_NonSwiftOutValueTanggalTransaksiNew.Text = getStringFieldValue(DataNonSwiftIn.ValueDate_TanggalTransaksi)
        Transaksi_NonSwiftOutValueTanggalTransaksiOld.Text = getStringFieldValue(DataNonSwiftIn.ValueDate_TanggalTransaksi_Old)
        TxtnilaitransaksiNew.Text = getStringFieldValue(DataNonSwiftIn.ValueDate_NilaiTransaksi)
        TxtnilaitransaksiOld.Text = getStringFieldValue(DataNonSwiftIn.ValueDate_NilaiTransaksi_Old)

        If ObjectAntiNull(DataNonSwiftIn.ValueDate_FK_Currency_ID) = True Then
            Transaksi_NonSwiftOutMataUangTransaksiNew.Text = getStringFieldValue(getbadantype.Code)
        End If
        If ObjectAntiNull(DataNonSwiftIn.ValueDate_FK_Currency_ID_Old) = True Then
            Transaksi_NonSwiftOutMataUangTransaksiOld.Text = getStringFieldValue(getbadantype_old.Code)
        End If

        Transaksi_NonSwiftOutMataUangTransaksiLainnyaNew.Text = getStringFieldValue(DataNonSwiftIn.ValueDate_CurrencyLainnya)
        Transaksi_NonSwiftOutMataUangTransaksiLainnyaOld.Text = getStringFieldValue(DataNonSwiftIn.ValueDate_CurrencyLainnya_Old)
        Transaksi_NonSwiftOutAmountdalamRupiahNew.Text = getStringFieldValue(DataNonSwiftIn.ValueDate_NilaiTransaksi)
        Transaksi_NonSwiftOutAmountdalamRupiahOld.Text = getStringFieldValue(DataNonSwiftIn.ValueDate_NilaiTransaksi_Old)

        If ObjectAntiNull(DataNonSwiftIn.Instructed_Currency) = True Then
            Transaksi_NonSwiftOutcurrencyNew.Text = getStringFieldValue(getbadantype1.Code)
        End If
        If ObjectAntiNull(DataNonSwiftIn.Instructed_Currency_Old) = True Then
            Transaksi_NonSwiftOutcurrencyOld.Text = getStringFieldValue(getbadantype1_old.Code)
        End If

        Transaksi_NonSwiftOutcurrencyLainnyaNew.Text = getStringFieldValue(DataNonSwiftIn.Instructed_CurrencyLainnya)
        Transaksi_NonSwiftOutcurrencyLainnyaOld.Text = getStringFieldValue(DataNonSwiftIn.Instructed_CurrencyLainnya_Old)
        'If Not DataNonSwiftIn.Instructed_Amount = "" Then
        Transaksi_NonSwiftOutinstructedAmountNew.Text = getStringFieldValue(DataNonSwiftIn.Instructed_Amount)
        'End If
        'If Not DataNonSwiftIn.Instructed_Amount_Old = "" Then
        Transaksi_NonSwiftOutinstructedAmountOld.Text = getStringFieldValue(DataNonSwiftIn.Instructed_Amount_Old)
        'End If

        Transaksi_NonSwiftOutTujuanTransaksiNew.Text = getStringFieldValue(DataNonSwiftIn.TujuanTransaksi)
        Transaksi_NonSwiftOutTujuanTransaksiOld.Text = getStringFieldValue(DataNonSwiftIn.TujuanTransaksi_Old)
        Transaksi_NonSwiftOutSumberPenggunaanDanaNew.Text = getStringFieldValue(DataNonSwiftIn.SumberPenggunaanDana)
        Transaksi_NonSwiftOutSumberPenggunaanDanaOld.Text = getStringFieldValue(DataNonSwiftIn.SumberPenggunaanDana_Old)
    End Sub
#End Region

#Region "Function"
    Private Sub ClearThisPageSessions()

        SetnGetRowTotal = Nothing
        SetnGetSort = Nothing
        SetnGetCurrentPage = 0
    End Sub
    Sub HideControl()
        BtnReject.Visible = False
        BtnSave.Visible = False
        BtnCancel.Visible = False
    End Sub
    Private Sub LoadData()
        Dim PkObject As String
        Dim ObjIFTI_Approval As vw_IFTI_Approval = DataRepository.vw_IFTI_ApprovalProvider.GetPaged("PK_IFTI_Approval_id = " & parID, "", 0, Integer.MaxValue, 0)(0)
        With ObjIFTI_Approval
            SafeDefaultValue = "-"
            txtMsUser_StaffName.Text = .RequestedBy
            txtRequestedDate1.Text = .RequestedDate

            'other info
            Dim OUser As User
            OUser = DataRepository.UserProvider.GetBypkUserID(.RequestedBy)
            If OUser IsNot Nothing Then
                txtMsUser_StaffName.Text = OUser.UserName
            Else
                txtMsUser_StaffName.Text = SafeDefaultValue
            End If

            OUser = DataRepository.UserProvider.GetBypkUserID(.RequestedBy)
            If OUser IsNot Nothing Then
                txtMsUser_StaffName.Text = OUser.UserName
            Else
                txtMsUser_StaffName.Text = SafeDefaultValue
            End If
        End With
        PkObject = ObjIFTI_Approval.PK_IFTI_Approval_Id

    End Sub
#End Region

#Region "Event"

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("IFTI_Approvals.aspx")
    End Sub


    Protected Sub BtnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReject.Click
        Try
            Dim Key_ApprovalID As String = "0"
            Dim Idx As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_id = " & parID, "", 0, Integer.MaxValue, 0)(0)
            '----------------------------------------------------------------------------------------------------------------------------------------------------------------
            Dim IFTI_ApprovalDetail As IFTI_Approval_Detail
            Dim IFTI_Approval As IFTI_Approval
            Dim IFTI_AppBenef As ifti_approval_beneficiary

            'Get IFTI Approval detail
            IFTI_ApprovalDetail = DataRepository.IFTI_Approval_DetailProvider.GetPaged( _
                IFTI_Approval_DetailColumn.PK_IFTI_Approval_Detail_Id.ToString & "=" & Idx.PK_IFTI_Approval_Detail_Id.ToString, _
                "", 0, Integer.MaxValue, Nothing)(0)
            'Get IFTI Approval
            IFTI_Approval = DataRepository.IFTI_ApprovalProvider.GetPaged( _
                IFTI_ApprovalColumn.PK_IFTI_Approval_Id.ToString & "=" & Idx.FK_IFTI_Approval_Id.ToString, _
                "", 0, Integer.MaxValue, Nothing)(0)
            Key_ApprovalID = IFTI_ApprovalDetail.PK_IFTI_ID
            'get ifti app Beneficiary
            IFTI_AppBenef = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged( _
                            IFTI_Approval_BeneficiaryColumn.FK_IFTI_Approval_Id.ToString & "=" & Idx.FK_IFTI_Approval_Id.ToString, _
                            "", 0, Integer.MaxValue, Nothing)(0)
            'Get Key IFTI
            Dim IFTIID As String = IFTI_ApprovalDetail.PK_IFTI_ID

            'delete IFTI_approval and IFTI_ApprovalDetail

            DataRepository.IFTI_Approval_DetailProvider.Delete(IFTI_ApprovalDetail)
            DataRepository.IFTI_ApprovalProvider.Delete(IFTI_Approval)
            DataRepository.IFTI_Approval_BeneficiaryProvider.Delete(IFTI_AppBenef)
         
            'MtvMsUser.ActiveViewIndex = 1
            BtnCancel.Visible = False

            MultiView1.ActiveViewIndex = 1
            LblConfirmation.Text = "Rejected Success"

            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = "Rejected Success"
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Public ReadOnly Property SetnGetBindTableBenef(Optional ByVal AllRecord As Boolean = False) As TList(Of IFTI_Approval_Beneficiary)
        Get
            Dim TotalDisplay As Integer = 0
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
                SetnGetCurrentPage = GetPageTotal - 1
            End If

            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = "FK_IFTI_Approval_Id=" & parID

            If AllRecord = False Then
                TotalDisplay = GetDisplayedTotalRow
            Else
                TotalDisplay = Integer.MaxValue
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            Return DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, TotalDisplay, SetnGetRowTotal)

        End Get

    End Property

    Private Sub AcceptDeleteBeneficiary()
        Try
            'Dim test As Integer = Session("FK_IFTI_Beneficiary_ID")
            'Using RulesIFTIBeneveciaryApproval As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)
            '    Dim IFTIBeneveciaryApprovalTableRow As IFTI_Approval_Beneficiary
            '    IFTIBeneveciaryApprovalTableRow = RulesIFTIBeneveciaryApproval
            Dim otrans As TransactionManager = Nothing
            Try
                otrans = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
                otrans.BeginTransaction()

                For Each idx As IFTI_Approval_Beneficiary In SetnGetBindTableBenef
                    Dim objintFK_IFTI_Beneficiary_ID As Integer = 0
                    If idx.FK_IFTI_Beneficiary_ID.ToString = "" Then
                        objintFK_IFTI_Beneficiary_ID = 0
                    Else
                        objintFK_IFTI_Beneficiary_ID = idx.FK_IFTI_Beneficiary_ID.ToString
                    End If


                    Dim countID As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID = " & objintFK_IFTI_Beneficiary_ID, "", 0, Integer.MaxValue, 0)
                    If countID.Count = 1 Then
                        Dim benefapp As IFTI_Beneficiary = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID = " & objintFK_IFTI_Beneficiary_ID, "", 0, Integer.MaxValue, 0)(0)
                        With benefapp
                            .FK_IFTI_ID = idx.FK_IFTI_ID
                            .FK_IFTI_NasabahType_ID = idx.FK_IFTI_NasabahType_ID
                            .Beneficiary_Nasabah_INDV_NoRekening = idx.Beneficiary_Nasabah_INDV_NoRekening
                            .Beneficiary_Nasabah_INDV_NamaLengkap = idx.Beneficiary_Nasabah_INDV_NamaLengkap
                            .Beneficiary_Nasabah_INDV_TanggalLahir = idx.Beneficiary_Nasabah_INDV_TanggalLahir
                            .Beneficiary_Nasabah_INDV_KewargaNegaraan = idx.Beneficiary_Nasabah_INDV_KewargaNegaraan
                            .Beneficiary_Nasabah_INDV_Pekerjaan = idx.Beneficiary_Nasabah_INDV_Pekerjaan
                            .Beneficiary_Nasabah_INDV_PekerjaanLainnya = idx.Beneficiary_Nasabah_INDV_PekerjaanLainnya
                            .Beneficiary_Nasabah_INDV_Negara = idx.Beneficiary_Nasabah_INDV_Negara
                            .Beneficiary_Nasabah_INDV_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_NegaraLainnya
                            .Beneficiary_Nasabah_INDV_ID_Alamat_Dom = idx.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
                            .Beneficiary_Nasabah_INDV_ID_KotaKab_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom
                            .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
                            .Beneficiary_Nasabah_INDV_ID_Propinsi_Dom = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom
                            .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                            .Beneficiary_Nasabah_INDV_ID_Alamat_Iden = idx.Beneficiary_Nasabah_INDV_ID_Alamat_Iden
                            .Beneficiary_Nasabah_INDV_ID_KotaKab_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden
                            .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
                            .Beneficiary_Nasabah_INDV_ID_Propinsi_Iden = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden
                            .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
                            .Beneficiary_Nasabah_INDV_ID_NegaraBagian = idx.Beneficiary_Nasabah_INDV_ID_NegaraBagian
                            .Beneficiary_Nasabah_INDV_ID_Negara = idx.Beneficiary_Nasabah_INDV_ID_Negara
                            .Beneficiary_Nasabah_INDV_ID_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
                            .Beneficiary_Nasabah_INDV_NoTelp = idx.Beneficiary_Nasabah_INDV_NoTelp
                            .Beneficiary_Nasabah_INDV_FK_IFTI_IDType = idx.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
                            .Beneficiary_Nasabah_INDV_NomorID = idx.Beneficiary_Nasabah_INDV_NomorID
                            .Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
                            .Beneficiary_Nasabah_CORP_NoRekening = idx.Beneficiary_Nasabah_CORP_NoRekening
                            .Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
                            .Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
                            .Beneficiary_Nasabah_CORP_NamaKorporasi = idx.Beneficiary_Nasabah_CORP_NamaKorporasi
                            .Beneficiary_Nasabah_CORP_NoTelp = idx.Beneficiary_Nasabah_CORP_NoTelp
                            .Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
                            .Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id
                            .Beneficiary_Nasabah_CORP_BidangUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
                            .Beneficiary_Nasabah_CORP_AlamatLengkap = idx.Beneficiary_Nasabah_CORP_AlamatLengkap
                            .Beneficiary_Nasabah_CORP_ID_KotaKab = idx.Beneficiary_Nasabah_CORP_ID_KotaKab
                            .Beneficiary_Nasabah_CORP_ID_KotaKabLainnya = idx.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
                            .Beneficiary_Nasabah_CORP_ID_Propinsi = idx.Beneficiary_Nasabah_CORP_ID_Propinsi
                            .Beneficiary_Nasabah_CORP_ID_PropinsiLainnya = idx.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
                            .Beneficiary_Nasabah_CORP_ID_NegaraBagian = idx.Beneficiary_Nasabah_CORP_ID_NegaraBagian
                            .Beneficiary_Nasabah_CORP_ID_Negara = idx.Beneficiary_Nasabah_CORP_ID_Negara
                            .Beneficiary_Nasabah_CORP_ID_NegaraLainnya = idx.Beneficiary_Nasabah_CORP_ID_NegaraLainnya
                            .Beneficiary_NonNasabah_NoRekening = idx.Beneficiary_NonNasabah_NoRekening
                            .Beneficiary_NonNasabah_KodeRahasia = idx.Beneficiary_NonNasabah_KodeRahasia
                            .Beneficiary_NonNasabah_NamaLengkap = idx.Beneficiary_NonNasabah_NamaLengkap
                            .Beneficiary_NonNasabah_NamaBank = idx.Beneficiary_NonNasabah_NamaBank
                            .Beneficiary_NonNasabah_TanggalLahir = idx.Beneficiary_NonNasabah_TanggalLahir
                            .Beneficiary_NonNasabah_NoTelp = idx.Beneficiary_NonNasabah_NoTelp
                            .Beneficiary_NonNasabah_NilaiTransaksikeuangan = idx.Beneficiary_NonNasabah_NilaiTransaksikeuangan
                            .Beneficiary_NonNasabah_ID_Alamat = idx.Beneficiary_NonNasabah_ID_Alamat
                            .Beneficiary_NonNasabah_ID_NegaraBagian = idx.Beneficiary_NonNasabah_ID_NegaraBagian
                            .Beneficiary_NonNasabah_ID_Negara = idx.Beneficiary_NonNasabah_ID_Negara
                            .Beneficiary_NonNasabah_ID_NegaraLainnya = idx.Beneficiary_NonNasabah_ID_NegaraLainnya
                            .Beneficiary_NonNasabah_FK_IFTI_IDType = idx.Beneficiary_NonNasabah_FK_IFTI_IDType
                            .Beneficiary_NonNasabah_NomorID = idx.Beneficiary_NonNasabah_NomorID
                        End With
                        DataRepository.IFTI_BeneficiaryProvider.Save(otrans, benefapp)

                    Else
                        Dim newbenef As New IFTI_Beneficiary
                        With newbenef
                            .FK_IFTI_ID = idx.FK_IFTI_ID
                            .FK_IFTI_NasabahType_ID = idx.FK_IFTI_NasabahType_ID
                            .Beneficiary_Nasabah_INDV_NoRekening = idx.Beneficiary_Nasabah_INDV_NoRekening
                            .Beneficiary_Nasabah_INDV_NamaLengkap = idx.Beneficiary_Nasabah_INDV_NamaLengkap
                            .Beneficiary_Nasabah_INDV_TanggalLahir = idx.Beneficiary_Nasabah_INDV_TanggalLahir
                            .Beneficiary_Nasabah_INDV_KewargaNegaraan = idx.Beneficiary_Nasabah_INDV_KewargaNegaraan
                            .Beneficiary_Nasabah_INDV_Pekerjaan = idx.Beneficiary_Nasabah_INDV_Pekerjaan
                            .Beneficiary_Nasabah_INDV_PekerjaanLainnya = idx.Beneficiary_Nasabah_INDV_PekerjaanLainnya
                            .Beneficiary_Nasabah_INDV_Negara = idx.Beneficiary_Nasabah_INDV_Negara
                            .Beneficiary_Nasabah_INDV_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_NegaraLainnya
                            .Beneficiary_Nasabah_INDV_ID_Alamat_Dom = idx.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
                            .Beneficiary_Nasabah_INDV_ID_KotaKab_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom
                            .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
                            .Beneficiary_Nasabah_INDV_ID_Propinsi_Dom = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom
                            .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                            .Beneficiary_Nasabah_INDV_ID_Alamat_Iden = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                            .Beneficiary_Nasabah_INDV_ID_KotaKab_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden
                            .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
                            .Beneficiary_Nasabah_INDV_ID_Propinsi_Iden = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden
                            .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
                            .Beneficiary_Nasabah_INDV_ID_NegaraBagian = idx.Beneficiary_Nasabah_INDV_ID_NegaraBagian
                            .Beneficiary_Nasabah_INDV_ID_Negara = idx.Beneficiary_Nasabah_INDV_ID_Negara
                            .Beneficiary_Nasabah_INDV_ID_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
                            .Beneficiary_Nasabah_INDV_NoTelp = idx.Beneficiary_Nasabah_INDV_NoTelp
                            .Beneficiary_Nasabah_INDV_FK_IFTI_IDType = idx.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
                            .Beneficiary_Nasabah_INDV_NomorID = idx.Beneficiary_Nasabah_INDV_NomorID
                            .Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
                            .Beneficiary_Nasabah_CORP_NoRekening = idx.Beneficiary_Nasabah_CORP_NoRekening
                            .Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
                            .Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
                            .Beneficiary_Nasabah_CORP_NamaKorporasi = idx.Beneficiary_Nasabah_CORP_NamaKorporasi
                            .Beneficiary_Nasabah_CORP_NoTelp = idx.Beneficiary_Nasabah_CORP_NoTelp
                            .Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
                            .Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id
                            .Beneficiary_Nasabah_CORP_BidangUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
                            .Beneficiary_Nasabah_CORP_AlamatLengkap = idx.Beneficiary_Nasabah_CORP_AlamatLengkap
                            .Beneficiary_Nasabah_CORP_ID_KotaKab = idx.Beneficiary_Nasabah_CORP_ID_KotaKab
                            .Beneficiary_Nasabah_CORP_ID_KotaKabLainnya = idx.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
                            .Beneficiary_Nasabah_CORP_ID_Propinsi = idx.Beneficiary_Nasabah_CORP_ID_Propinsi
                            .Beneficiary_Nasabah_CORP_ID_PropinsiLainnya = idx.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
                            .Beneficiary_Nasabah_CORP_ID_NegaraBagian = idx.Beneficiary_Nasabah_CORP_ID_NegaraBagian
                            .Beneficiary_Nasabah_CORP_ID_Negara = idx.Beneficiary_Nasabah_CORP_ID_Negara
                            .Beneficiary_Nasabah_CORP_ID_NegaraLainnya = idx.Beneficiary_Nasabah_CORP_ID_NegaraLainnya
                            .Beneficiary_NonNasabah_NoRekening = idx.Beneficiary_NonNasabah_NoRekening
                            .Beneficiary_NonNasabah_KodeRahasia = idx.Beneficiary_NonNasabah_KodeRahasia
                            .Beneficiary_NonNasabah_NamaLengkap = idx.Beneficiary_NonNasabah_NamaLengkap
                            .Beneficiary_NonNasabah_NamaBank = idx.Beneficiary_NonNasabah_NamaBank
                            .Beneficiary_NonNasabah_TanggalLahir = idx.Beneficiary_NonNasabah_TanggalLahir
                            .Beneficiary_NonNasabah_NoTelp = idx.Beneficiary_NonNasabah_NoTelp
                            .Beneficiary_NonNasabah_NilaiTransaksikeuangan = idx.Beneficiary_NonNasabah_NilaiTransaksikeuangan
                            .Beneficiary_NonNasabah_ID_Alamat = idx.Beneficiary_NonNasabah_ID_Alamat
                            .Beneficiary_NonNasabah_ID_NegaraBagian = idx.Beneficiary_NonNasabah_ID_NegaraBagian
                            .Beneficiary_NonNasabah_ID_Negara = idx.Beneficiary_NonNasabah_ID_Negara
                            .Beneficiary_NonNasabah_ID_NegaraLainnya = idx.Beneficiary_NonNasabah_ID_NegaraLainnya
                            .Beneficiary_NonNasabah_FK_IFTI_IDType = idx.Beneficiary_NonNasabah_FK_IFTI_IDType
                            .Beneficiary_NonNasabah_NomorID = idx.Beneficiary_NonNasabah_NomorID
                        End With
                        DataRepository.IFTI_BeneficiaryProvider.Save(otrans, newbenef)
                    End If
                    Dim deleteAppBenef As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("PK_IFTI_Approval_Beneficiary_ID =" & idx.PK_IFTI_Approval_Beneficiary_ID.ToString, "", 0, Integer.MaxValue, 0)(0)

                    deleteAppBenef.FK_IFTI_Approval_Id = parID
                    DataRepository.IFTI_Approval_BeneficiaryProvider.Delete(otrans, deleteAppBenef)
                Next
                otrans.Commit()
            Catch ex As Exception
                If Not otrans Is Nothing Then
                    otrans.Rollback()
                End If
                LogError(ex)
                Throw
            Finally
                If Not otrans Is Nothing Then
                    otrans.Dispose()
                    otrans = Nothing
                End If
            End Try

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        'Dim oSQLTrans As SqlTransaction = Nothing
        Dim otrans As TransactionManager = Nothing
        Try
            otrans = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
            otrans.BeginTransaction()


            Using iftiApprovalDetailAdapter As New IFTI_Approval_Detail

                Using RulesiftiApprovalDetailAdapter As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)

                    Using IFTIAdapter As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(RulesiftiApprovalDetailAdapter.PK_IFTI_ID)
                        'Bila RulesBasic_ApprovalTable.Rows.Count > 0 berarti Basic Rule tsb masih ada dlm tabel RulesBasic_Approval
                        If Not RulesiftiApprovalDetailAdapter Is Nothing Then
                            Dim IFTIApprovalTableRow As IFTI_Approval_Detail
                            IFTIApprovalTableRow = RulesiftiApprovalDetailAdapter


                            Dim Counter As Integer
                            'Using IFTIQueryAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.QueriesTableAdapter
                            'Using IFTIQueryAdapter As TList(Of IFTI) = DataRepository.IFTIProvider.GetPaged("LTDLNNO='" & NonSwiftOutUmum_LTDNNew.Text & "'", "", 0, Integer.MaxValue, 0)
                            '    'Counter = RulesBasicQueryAdapter.Count.ToString(IFTIApprovalTableRow.KYE_RulesBasicName)
                            'End Using

                            AcceptDeleteBeneficiary()


                            'Using RulesIFTIBeneveciaryApproval As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)

                            '    Dim IFTIBeneveciaryApprovalTableRow As IFTI_Approval_Beneficiary
                            '    IFTIBeneveciaryApprovalTableRow = RulesIFTIBeneveciaryApproval

                            '    Using IFTI_BeneficiaryAdapter As IFTI_Beneficiary = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID = " & IFTIBeneveciaryApprovalTableRow.FK_IFTI_Beneficiary_ID.ToString, "", 0, Integer.MaxValue, 0)(0)
                            '        With IFTI_BeneficiaryAdapter
                            '            '.PK_IFTI_Beneficiary_ID = 0
                            '            .FK_IFTI_ID = IFTIBeneveciaryApprovalTableRow.FK_IFTI_ID
                            '            .FK_IFTI_NasabahType_ID = IFTIBeneveciaryApprovalTableRow.FK_IFTI_NasabahType_ID
                            '            .Beneficiary_Nasabah_INDV_NoRekening = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_NoRekening
                            '            .Beneficiary_Nasabah_INDV_NamaLengkap = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_NamaLengkap
                            '            .Beneficiary_Nasabah_INDV_TanggalLahir = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_TanggalLahir
                            '            .Beneficiary_Nasabah_INDV_KewargaNegaraan = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_KewargaNegaraan
                            '            .Beneficiary_Nasabah_INDV_Pekerjaan = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_Pekerjaan
                            '            .Beneficiary_Nasabah_INDV_PekerjaanLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_PekerjaanLainnya
                            '            .Beneficiary_Nasabah_INDV_Negara = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_Negara
                            '            .Beneficiary_Nasabah_INDV_NegaraLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_NegaraLainnya
                            '            .Beneficiary_Nasabah_INDV_ID_Alamat_Dom = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
                            '            .Beneficiary_Nasabah_INDV_ID_KotaKab_Dom = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom
                            '            .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
                            '            .Beneficiary_Nasabah_INDV_ID_Propinsi_Dom = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom
                            '            .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                            '            .Beneficiary_Nasabah_INDV_ID_Alamat_Iden = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                            '            .Beneficiary_Nasabah_INDV_ID_KotaKab_Iden = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden
                            '            .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
                            '            .Beneficiary_Nasabah_INDV_ID_Propinsi_Iden = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden
                            '            .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
                            '            .Beneficiary_Nasabah_INDV_ID_NegaraBagian = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_NegaraBagian
                            '            .Beneficiary_Nasabah_INDV_ID_Negara = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_Negara
                            '            .Beneficiary_Nasabah_INDV_ID_NegaraLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
                            '            .Beneficiary_Nasabah_INDV_NoTelp = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_NoTelp
                            '            .Beneficiary_Nasabah_INDV_FK_IFTI_IDType = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
                            '            .Beneficiary_Nasabah_INDV_NomorID = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_NomorID
                            '            .Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
                            '            .Beneficiary_Nasabah_CORP_NoRekening = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_NoRekening
                            '            .Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
                            '            .Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
                            '            .Beneficiary_Nasabah_CORP_NamaKorporasi = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_NamaKorporasi
                            '            .Beneficiary_Nasabah_CORP_NoTelp = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_NoTelp
                            '            .Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
                            '            .Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id
                            '            .Beneficiary_Nasabah_CORP_BidangUsahaLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
                            '            .Beneficiary_Nasabah_CORP_AlamatLengkap = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_AlamatLengkap
                            '            .Beneficiary_Nasabah_CORP_ID_KotaKab = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_ID_KotaKab
                            '            .Beneficiary_Nasabah_CORP_ID_KotaKabLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
                            '            .Beneficiary_Nasabah_CORP_ID_Propinsi = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_ID_Propinsi
                            '            .Beneficiary_Nasabah_CORP_ID_PropinsiLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
                            '            .Beneficiary_Nasabah_CORP_ID_NegaraBagian = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_ID_NegaraBagian
                            '            .Beneficiary_Nasabah_CORP_ID_Negara = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_ID_Negara
                            '            .Beneficiary_Nasabah_CORP_ID_NegaraLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_ID_NegaraLainnya
                            '            .Beneficiary_NonNasabah_NoRekening = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_NoRekening
                            '            .Beneficiary_NonNasabah_KodeRahasia = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_KodeRahasia
                            '            .Beneficiary_NonNasabah_NamaLengkap = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_NamaLengkap
                            '            .Beneficiary_NonNasabah_NamaBank = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_NamaBank
                            '            .Beneficiary_NonNasabah_TanggalLahir = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_TanggalLahir
                            '            .Beneficiary_NonNasabah_NoTelp = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_NoTelp
                            '            .Beneficiary_NonNasabah_NilaiTransaksikeuangan = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_NilaiTransaksikeuangan
                            '            .Beneficiary_NonNasabah_ID_Alamat = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_ID_Alamat
                            '            .Beneficiary_NonNasabah_ID_NegaraBagian = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_ID_NegaraBagian
                            '            .Beneficiary_NonNasabah_ID_Negara = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_ID_Negara
                            '            .Beneficiary_NonNasabah_ID_NegaraLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_ID_NegaraLainnya
                            '            .Beneficiary_NonNasabah_FK_IFTI_IDType = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_FK_IFTI_IDType
                            '            .Beneficiary_NonNasabah_NomorID = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_NomorID
                            '        End With
                            '        DataRepository.IFTI_BeneficiaryProvider.Save(otrans, IFTI_BeneficiaryAdapter)

                            '        ' Hapus Item tersebut dalam ifti_approval_beneficiary
                            '        Dim delete As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)
                            '        delete.FK_IFTI_Beneficiary_ID = parID
                            '        DataRepository.IFTI_Approval_BeneficiaryProvider.Delete(otrans, delete)
                            '    End Using
                            'End Using

                            'Bila counter = 0 berarti Data IFTI tsb belum ada dlm tabel IFTI, maka boleh ditambahkan
                            If Counter = 0 Then
                                With IFTIAdapter
                                    .PK_IFTI_ID = IFTIApprovalTableRow.PK_IFTI_ID
                                    '.ReportToPPATK = 
                                    '.Aging
                                    '.PaymentDetail
                                    '.AutoReplace
                                    '.IsDataValid
                                    .FK_IFTI_Type_ID = IFTIApprovalTableRow.FK_IFTI_Type_ID
                                    '.LocalID = IFTIApprovalTableRow.LocalID
                                    .LTDLNNo = IFTIApprovalTableRow.LTDLNNo
                                    .LTDLNNoKoreksi = IFTIApprovalTableRow.LTDLNNoKoreksi
                                    .TanggalLaporan = IFTIApprovalTableRow.TanggalLaporan
                                    .NamaPJKBankPelapor = IFTIApprovalTableRow.NamaPJKBankPelapor
                                    .NamaPejabatPJKBankPelapor = IFTIApprovalTableRow.NamaPejabatPJKBankPelapor
                                    .JenisLaporan = IFTIApprovalTableRow.JenisLaporan
                                    .Sender_FK_IFTI_NasabahType_ID = IFTIApprovalTableRow.Sender_FK_IFTI_NasabahType_ID
                                    .Sender_Nasabah_INDV_NoRekening = IFTIApprovalTableRow.Sender_Nasabah_INDV_NoRekening
                                    .Sender_Nasabah_INDV_NamaLengkap = IFTIApprovalTableRow.Sender_Nasabah_INDV_NamaLengkap
                                    .Sender_Nasabah_INDV_TanggalLahir = IFTIApprovalTableRow.Sender_Nasabah_INDV_TanggalLahir
                                    .Sender_Nasabah_INDV_KewargaNegaraan = IFTIApprovalTableRow.Sender_Nasabah_INDV_KewargaNegaraan
                                    .Sender_Nasabah_INDV_Negara = IFTIApprovalTableRow.Sender_Nasabah_INDV_KewargaNegaraan
                                    .Sender_Nasabah_INDV_NegaraLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_NegaraLainnya
                                    .Sender_Nasabah_INDV_Pekerjaan = IFTIApprovalTableRow.Sender_Nasabah_INDV_Pekerjaan
                                    .Sender_Nasabah_INDV_PekerjaanLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_PekerjaanLainnya
                                    .Sender_Nasabah_INDV_DOM_Alamat = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_Alamat
                                    .Sender_Nasabah_INDV_DOM_Propinsi = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_Propinsi
                                    .Sender_Nasabah_INDV_DOM_PropinsiLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_PropinsiLainnya
                                    .Sender_Nasabah_INDV_DOM_KotaKab = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_KotaKab
                                    .Sender_Nasabah_INDV_DOM_KotaKabLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_KotaKabLainnya
                                    .Sender_Nasabah_INDV_ID_Alamat = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_Alamat
                                    .Sender_Nasabah_INDV_ID_Propinsi = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_Propinsi
                                    .Sender_Nasabah_INDV_ID_PropinsiLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_PropinsiLainnya
                                    .Sender_Nasabah_INDV_ID_KotaKab = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_KotaKab
                                    .Sender_Nasabah_INDV_ID_KotaKabLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_KotaKabLainnya
                                    .Sender_Nasabah_INDV_FK_IFTI_IDType = IFTIApprovalTableRow.Sender_Nasabah_INDV_FK_IFTI_IDType
                                    .Sender_Nasabah_INDV_NomorID = IFTIApprovalTableRow.Sender_Nasabah_INDV_NomorID
                                    .Sender_Nasabah_CORP_NoRekening = IFTIApprovalTableRow.Sender_Nasabah_CORP_NoRekening
                                    .Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id = IFTIApprovalTableRow.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
                                    .Sender_Nasabah_CORP_BentukBadanUsahaLainnya = IFTIApprovalTableRow.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
                                    .Sender_Nasabah_CORP_NamaKorporasi = IFTIApprovalTableRow.Sender_Nasabah_CORP_NamaKorporasi
                                    .Sender_Nasabah_CORP_FK_MsBidangUsaha_Id = IFTIApprovalTableRow.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id
                                    .Sender_Nasabah_CORP_BidangUsahaLainnya = IFTIApprovalTableRow.Sender_Nasabah_CORP_BidangUsahaLainnya
                                    .Sender_Nasabah_CORP_AlamatLengkap = IFTIApprovalTableRow.Sender_Nasabah_CORP_AlamatLengkap
                                    .Sender_Nasabah_CORP_Propinsi = IFTIApprovalTableRow.Sender_Nasabah_CORP_Propinsi
                                    .Sender_Nasabah_CORP_PropinsiLainnya = IFTIApprovalTableRow.Sender_Nasabah_CORP_PropinsiLainnya
                                    .Sender_Nasabah_CORP_KotaKab = IFTIApprovalTableRow.Sender_Nasabah_COR_KotaKab
                                    .Sender_Nasabah_CORP_KotaKabLainnya = IFTIApprovalTableRow.Sender_Nasabah_COR_KotaKabLainnya
                                    .Sender_Nasabah_CORP_LN_AlamatLengkap = IFTIApprovalTableRow.Sender_Nasabah_CORP_LN_AlamatLengkap
                                    .Sender_Nasabah_CORP_Alamat_KantorCabangPengirim = IFTIApprovalTableRow.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
                                    .FK_IFTI_NonNasabahNominalType_ID = IFTIApprovalTableRow.FK_IFTI_NonNasabahNominalType_ID
                                    .Sender_NonNasabah_NamaLengkap = IFTIApprovalTableRow.Sender_NonNasabah_NamaLengkap
                                    .Sender_NonNasabah_TanggalLahir = IFTIApprovalTableRow.Sender_NonNasabah_TanggalLahir
                                    .Sender_NonNasabah_ID_Alamat = IFTIApprovalTableRow.Sender_NonNasabah_ID_Alamat
                                    .Sender_NonNasabah_ID_Propinsi = IFTIApprovalTableRow.Sender_NonNasabah_ID_Propinsi
                                    .Sender_NonNasabah_ID_PropinsiLainnya = IFTIApprovalTableRow.Sender_NonNasabah_ID_PropinsiLainnya
                                    .Sender_NonNasabah_ID_KotaKab = IFTIApprovalTableRow.Sender_NonNasabah_ID_KotaKab
                                    .Sender_NonNasabah_ID_KotaKabLainnya = IFTIApprovalTableRow.Sender_NonNasabah_ID_KotaKabLainnya
                                    .Sender_NonNasabah_FK_IFTI_IDType = IFTIApprovalTableRow.Sender_NonNasabah_FK_IFTI_IDType
                                    .Sender_NonNasabah_NomorID = IFTIApprovalTableRow.Sender_NonNasabah_NomorID
                                    .FK_IFTI_BeneficialOwnerType_ID = IFTIApprovalTableRow.FK_IFTI_BeneficialOwnerType_ID
                                    .BeneficialOwner_HubunganDenganPemilikDana = IFTIApprovalTableRow.BeneficialOwner_HubunganDenganPemilikDana
                                    .BeneficialOwner_NoRekening = IFTIApprovalTableRow.BeneficialOwner_NoRekening
                                    .BeneficialOwner_NamaLengkap = IFTIApprovalTableRow.BeneficialOwner_NamaLengkap
                                    .BeneficialOwner_TanggalLahir = IFTIApprovalTableRow.BeneficialOwner_TanggalLahir
                                    .BeneficialOwner_ID_Alamat = IFTIApprovalTableRow.BeneficialOwner_ID_Alamat
                                    .BeneficialOwner_ID_Propinsi = IFTIApprovalTableRow.BeneficialOwner_ID_Propinsi
                                    .BeneficialOwner_ID_PropinsiLainnya = IFTIApprovalTableRow.BeneficialOwner_ID_PropinsiLainnya
                                    .BeneficialOwner_ID_KotaKab = IFTIApprovalTableRow.BeneficialOwner_ID_KotaKab
                                    .BeneficialOwner_ID_KotaKabLainnya = IFTIApprovalTableRow.BeneficialOwner_ID_KotaKabLainnya
                                    .BeneficialOwner_FK_IFTI_IDType = IFTIApprovalTableRow.BeneficialOwner_FK_IFTI_IDType
                                    .BeneficialOwner_NomorID = IFTIApprovalTableRow.BeneficialOwner_NomorID
                                    .TanggalTransaksi = IFTIApprovalTableRow.TanggalTransaksi
                                    .TimeIndication = IFTIApprovalTableRow.TimeIndication
                                    .SenderReference = IFTIApprovalTableRow.SenderReference
                                    .BankOperationCode = IFTIApprovalTableRow.BankOperationCode
                                    .InstructionCode = IFTIApprovalTableRow.InstructionCode
                                    .KantorCabangPenyelengaraPengirimAsal = IFTIApprovalTableRow.KantorCabangPenyelengaraPengirimAsal
                                    .TransactionCode = IFTIApprovalTableRow.TransactionCode
                                    .ValueDate_TanggalTransaksi = IFTIApprovalTableRow.ValueDate_TanggalTransaksi
                                    .ValueDate_NilaiTransaksi = IFTIApprovalTableRow.ValueDate_NilaiTransaksi
                                    .ValueDate_FK_Currency_ID = IFTIApprovalTableRow.ValueDate_FK_Currency_ID
                                    .ValueDate_CurrencyLainnya = IFTIApprovalTableRow.ValueDate_CurrencyLainnya
                                    .ValueDate_NilaiTransaksiIDR = IFTIApprovalTableRow.ValueDate_NilaiTransaksiIDR
                                    .Instructed_Currency = IFTIApprovalTableRow.Instructed_Currency
                                    .Instructed_CurrencyLainnya = IFTIApprovalTableRow.Instructed_CurrencyLainnya
                                    .Instructed_Amount = IFTIApprovalTableRow.Instructed_Amount
                                    .ExchangeRate = IFTIApprovalTableRow.ExchangeRate
                                    .SendingInstitution = IFTIApprovalTableRow.SendingInstitution
                                    .TujuanTransaksi = IFTIApprovalTableRow.TujuanTransaksi
                                    .SumberPenggunaanDana = IFTIApprovalTableRow.SumberPenggunaanDana
                                    .InformationAbout_SenderCorrespondent = IFTIApprovalTableRow.InformationAbout_SenderCorrespondent
                                    .InformationAbout_ReceiverCorrespondent = IFTIApprovalTableRow.InformationAbout_ReceiverCorrespondent
                                    .InformationAbout_Thirdreimbursementinstitution = IFTIApprovalTableRow.InformationAbout_Thirdreimbursementinstitution
                                    .InformationAbout_IntermediaryInstitution = IFTIApprovalTableRow.InformationAbout_IntermediaryInstitution
                                    .RemittanceInformation = IFTIApprovalTableRow.RemittanceInformation
                                    .SendertoReceiverInformation = IFTIApprovalTableRow.SendertoReceiverInformation
                                    .RegulatoryReporting = IFTIApprovalTableRow.RegulatoryReporting
                                    .EnvelopeContents = IFTIApprovalTableRow.EnvelopeContents
                                End With
                                DataRepository.IFTIProvider.Save(otrans, IFTIAdapter)

                                'delete item tersebut dalam table ifti_approval & ifti_approval_detail

                                Dim delete As IFTI_Approval = DataRepository.IFTI_ApprovalProvider.GetByPK_IFTI_Approval_Id(parID)
                                'delete.PK_IFTI_Approval_Id = parID
                                DataRepository.IFTI_ApprovalProvider.Delete(otrans, delete)

                                Dim DeleteDetail As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_Id =" & parID, "", 0, Integer.MaxValue, 0)(0)
                                DataRepository.IFTI_Approval_DetailProvider.Delete(otrans, DeleteDetail)

                                'delete item error list dari ifti_errorlist
                                Dim DeleteError As TList(Of IFTI_ErrorDescription) = DataRepository.IFTI_ErrorDescriptionProvider.GetPaged("FK_IFTI_ID = " & IFTIAdapter.PK_IFTI_ID, "", 0, Integer.MaxValue, 0)

                                DataRepository.IFTI_ErrorDescriptionProvider.Delete(DeleteError)
                                GenerateListOfGeneratedIFTI(otrans)

                                otrans.Commit()
                            End If
                        End If
                    End Using
                End Using
            End Using

            MultiView1.ActiveViewIndex = 1
            LblConfirmation.Text = "Data Accept Success"

            'End Using
        Catch ex As Exception
            If Not otrans Is Nothing Then
                otrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not otrans Is Nothing Then
                otrans.Dispose()
                otrans = Nothing
            End If
        End Try
    End Sub

    Function GenerateListOfGeneratedIFTI(ByVal objtransaction As TransactionManager) As Boolean

        Using objCommand As SqlCommand = Commonly.GetSQLCommandStoreProcedure("usp_GenerateListOfGeneratedIfti")
            DataRepository.Provider.ExecuteNonQuery(objtransaction, objCommand)
        End Using


    End Function
    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Response.Redirect("IFTI_Approvals.aspx")
    End Sub

#End Region

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            LoadIDType()
            LoadData()
            bindgrid()
            If Not Page.IsPostBack Then
                ClearThisPageSessions()
                AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

End Class
