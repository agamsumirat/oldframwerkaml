Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper
Imports SahassaNettier.Data

Partial Class NewsManagementApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param pk News management
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamPkNewsManagement() As Int64
        Get
            Return Me.Request.Params("NewsID")
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserID() As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.News_PendingApprovalTableAdapter
                Return AccessPending.SelectNews_PendingApprovalUserID(Me.ParamPkNewsManagement)
            End Using
        End Get
    End Property
#End Region

#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HideAll()
        Dim StrId As String = ""
        Select Case Me.ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.NewsAdd
                StrId = "UserAdd"
            Case Sahassa.AML.Commonly.TypeConfirm.NewsEdit
                StrId = "UserEdi"
            Case Sahassa.AML.Commonly.TypeConfirm.NewsDelete
                StrId = "UserDel"
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select
        For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
            ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID.Substring(0, 7) = StrId
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load News add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadNewsAdd()
        Me.LabelTitle.Text = "Activity: Add News"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.News_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetNewsApprovalData(Me.ParamPkNewsManagement)
                'Bila ObjTable.Rows.Count > 0 berarti News tsb masih ada dlm tabel News_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.News_ApprovalRow = ObjTable.Rows(0)
                    LabelNewsTitleAdd.Text = rowData.NewsTitle
                    TextNewsSummaryAdd.Text = rowData.NewsSummary
                    TextNewsContentAdd.Text = rowData.NewsContent

                    LabelNewsStartDateAdd.Text = String.Format("{0:dd-MMM-yyyy}", CType(rowData.NewsStartDate, Date))
                    LabelNewsEndDateAdd.Text = String.Format("{0:dd-MMM-yyyy}", CType(rowData.NewsEndDate, Date))
                    GridAttach.DataSource = oRowNewsAttach
                    GridAttach.DataBind()
                Else 'Bila ObjTable.Rows.Count = 0 berarti News tsb sudah tidak lagi berada dlm tabel News_Approval
                    Throw New Exception("Cannot load data from the following News: " & Me.LabelNewsTitleAdd.Text & " because that News is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load News edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadNewsEdit()
        Me.LabelTitle.Text = "Activity: Edit News"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.News_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetNewsApprovalData(Me.ParamPkNewsManagement)
                'Bila ObjTable.Rows.Count > 0 berarti News tsb masih ada dlm tabel News_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.News_ApprovalRow = ObjTable.Rows(0)

                    LabelNewNewsID.Text = rowData.NewsID
                    LabelNewNewsTitle.Text = rowData.NewsTitle
                    TextNewNewsSummary.Text = rowData.NewsSummary
                    TextNewNewsContent.Text = rowData.NewsContent

                    LabelNewNewsStartDate.Text = String.Format("{0:dd-MMM-yyyy}", CType(rowData.NewsStartDate, Date))
                    LabelNewNewsEndDate.Text = String.Format("{0:dd-MMM-yyyy}", CType(rowData.NewsEndDate, Date))

                    LabelOldNewsID.Text = rowData.NewsID_Old
                    LabelOldNewsTitle.Text = rowData.NewsTitle_Old
                    TextOldNewsSummary.Text = rowData.NewsSummary_Old
                    TextOldNewsContent.Text = rowData.NewsContent_Old

                    LabelOldNewsStartDate.Text = String.Format("{0:dd-MMM-yyyy}", CType(rowData.NewsStartDate_Old, Date))
                    LabelOldNewsEndDate.Text = String.Format("{0:dd-MMM-yyyy}", CType(rowData.NewsEndDate_Old, Date))

                    GridNewAttach.DataSource = oRowNewsAttach
                    GridNewAttach.DataBind()

                    GridOldAttach.DataSource = oRowOldNewsAttach
                    GridOldAttach.DataBind()

                Else 'Bila ObjTable.Rows.Count = 0 berarti News tsb sudah tidak lagi berada dlm tabel News_Approval
                    Throw New Exception("Cannot load data from the following News: " & Me.LabelOldNewsTitle.Text & " because that News is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load News delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadNewsDelete()
        Me.LabelTitle.Text = "Activity: Delete News"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.News_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetNewsApprovalData(Me.ParamPkNewsManagement)
                'Bila ObjTable.Rows.Count > 0 berarti News tsb masih ada dlm tabel News_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.News_ApprovalRow = ObjTable.Rows(0)
                    LabelNewsIDDelete.Text = rowData.NewsID
                    LabelNewsTitleDelete.Text = rowData.NewsTitle
                    TextNewsSummaryDelete.Text = rowData.NewsSummary
                    TextNewsContentDelete.Text = rowData.NewsContent
                    LabelNewsStartDateDelete.Text = String.Format("{0:dd-MMM-yyyy}", CType(rowData.NewsStartDate, Date))
                    LabelNewsEndDateDelete.Text = String.Format("{0:dd-MMM-yyyy}", CType(rowData.NewsEndDate, Date))

                    GridAttachDel.DataSource = oRowNewsAttach
                    GridAttachDel.DataBind()

                Else 'Bila ObjTable.Rows.Count = 0 berarti News tsb sudah tidak lagi berada dlm tabel News_Approval
                    Throw New Exception("Cannot load data from the following News: " & Me.LabelNewsTitleDelete.Text & " because that News is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub
#End Region



#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' News add accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptNewsAdd()
        Dim oSQLTrans As SqlTransaction = Nothing

        'Using TranScope As New Transactions.TransactionScope
        Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessNews, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.News_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetNewsApprovalData(Me.ParamPkNewsManagement)

                'Bila ObjTable.Rows.Count > 0 berarti News tsb masih ada dlm tabel News_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.News_ApprovalRow = ObjTable.Rows(0)

                    'Periksa apakah NewsTitle yg baru tsb sudah ada dlm tabel News atau belum. 
                    Dim counter As Int32 = AccessNews.CountMatchingNews(rowData.NewsTitle)

                    'Bila counter = 0 berarti NewsTitle tsb belum ada dlm tabel News, maka boleh ditambahkan
                    If counter = 0 Then
                        'tambahkan item tersebut dalam tabel News
                        AccessNews.Insert(rowData.NewsTitle, rowData.NewsSummary, rowData.NewsContent, rowData.NewsStartDate, rowData.NewsEndDate, rowData.NewsCreatedDate)

                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
                        'catat aktifitas dalam tabel Audit Trail
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsTitle", "Add", "", rowData.NewsTitle, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsSummary", "Add", "", rowData.NewsSummary, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsContent", "Add", "", rowData.NewsContent, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsStartDate", "Add", "", rowData.NewsStartDate, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsEndDate", "Add", "", rowData.NewsEndDate, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsCreatedDate", "Add", "", rowData.NewsCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                            'hapus item tersebut dalam tabel News_Approval
                            AccessPending.DeleteNewsApproval(rowData.News_PendingApprovalID)

                            'hapus item tersebut dalam tabel News_PendingApproval
                            Using AccessPendingNews As New AMLDAL.AMLDataSetTableAdapters.News_PendingApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingNews, oSQLTrans)
                                AccessPendingNews.DeleteNewsPendingApproval(rowData.News_PendingApprovalID)
                            End Using

                            oSQLTrans.Commit()
                        End Using

                        'Add attach add approval reject
                        Using objcmd As New System.Data.SqlClient.SqlCommand("UpdateNewsAttachmentFK_NewsID")
                            objcmd.CommandType = System.Data.CommandType.StoredProcedure
                            objcmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings("SQLCommandTimeout")
                            objcmd.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fk_NewsApprovalId", ParamPkNewsManagement))
                            objcmd.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NewsTitle", rowData.NewsTitle))
                            DataRepository.Provider.ExecuteScalar(objcmd)
                        End Using


                    Else 'Bila counter != 0 berarti NewsTitle tsb sudah ada dlm tabel News, maka AcceptNewsAdd gagal
                        Throw New Exception("Cannot add the following News: " & rowData.NewsTitle & " because that News Title already exists in the database.")
                    End If
                Else 'Bila ObjTable.Rows.Count = 0 berarti News tsb sudah tidak lagi berada dlm tabel News_Approval
                    Throw New Exception("Cannot add the following News: " & Me.LabelNewsTitleAdd.Text & " because that News is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' News edit accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptNewsEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessNews, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.News_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetNewsApprovalData(Me.ParamPkNewsManagement)

                'Bila ObjTable.Rows.Count > 0 berarti News tsb masih ada dlm tabel News_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.News_ApprovalRow = ObjTable.Rows(0)

                    'Periksa apakah NewsTitle yg baru tsb sudah ada dlm tabel News atau belum. 
                    Dim counter As Int32 = AccessNews.CountMatchingNews(rowData.NewsTitle)

                    'Bila tidak ada perubahan NewsTitle
                    If rowData.NewsTitle = rowData.NewsTitle_Old Then
                        GoTo Edit
                    Else 'Bila ada perubahan NewsTitle
                        'Counter = 0 berarti NewsTitle tersebut tidak ada dalam tabel News
                        If counter = 0 Then
Edit:
                            'update item tersebut dalam tabel News
                            AccessNews.UpdateNews(rowData.NewsID, rowData.NewsTitle, rowData.NewsSummary, rowData.NewsContent, rowData.NewsStartDate, rowData.NewsEndDate)

                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(7)
                            'catat aktifitas dalam tabel Audit Trail
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsTitle", "Edit", rowData.NewsTitle_Old, rowData.NewsTitle, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsSummary", "Edit", rowData.NewsSummary_Old, rowData.NewsSummary, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsContent", "Edit", rowData.NewsContent_Old, rowData.NewsContent, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsStartDate", "Edit", rowData.NewsStartDate_Old, rowData.NewsStartDate, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsEndDate", "Edit", rowData.NewsEndDate_Old, rowData.NewsEndDate, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsID", "Edit", rowData.NewsID_Old, rowData.NewsID, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsCreatedDate", "Edit", rowData.NewsCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.NewsCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                                'hapus item tersebut dalam tabel News_Approval
                                AccessPending.DeleteNewsApproval(rowData.News_PendingApprovalID)

                                'hapus item tersebut dalam tabel News_PendingApproval
                                Using AccessPendingNews As New AMLDAL.AMLDataSetTableAdapters.News_PendingApprovalTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingNews, oSQLTrans)
                                    AccessPendingNews.DeleteNewsPendingApproval(rowData.News_PendingApprovalID)
                                End Using
                                oSQLTrans.Commit()
                            End Using

                            'hapus attach Edit approval reject
                            Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                                adapter.DeleteNewsAttachmentbyFkNewsId(rowData.NewsID)
                            End Using

                            'Add attach Edit approval reject
                            Using objcmd As New System.Data.SqlClient.SqlCommand("UpdateNewsAttachmentFK_NewsID")
                                objcmd.CommandType = System.Data.CommandType.StoredProcedure
                                objcmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings("SQLCommandTimeout")
                                objcmd.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Fk_NewsApprovalId", ParamPkNewsManagement))
                                objcmd.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NewsTitle", rowData.NewsTitle))
                                DataRepository.Provider.ExecuteScalar(objcmd)
                            End Using

                        Else 'Bila counter != 0 berarti NewsTitle tsb telah ada dlm tabel News, maka AcceptNewsEdit gagal
                            Throw New Exception("Cannot change to the following News Title: " & rowData.NewsTitle & " because that News Title already exists in the database.")
                        End If
                    End If
                Else 'Bila ObjTable.Rows.Count = 0 berarti News tsb sudah tidak lagi berada dlm tabel News_Approval
                    Throw New Exception("Cannot edit the following News: " & Me.LabelOldNewsTitle.Text & " because that News is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' News delete accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptNewsDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessNews, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.News_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetNewsApprovalData(Me.ParamPkNewsManagement)

                'Bila ObjTable.Rows.Count > 0 berarti News tsb masih ada dlm tabel News_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.News_ApprovalRow = ObjTable.Rows(0)

                    'Periksa apakah News yg hendak didelete tsb ada dlm tabelnya atau tidak. 
                    Dim counter As Int32 = AccessNews.CountMatchingNews(rowData.NewsTitle)

                    'Bila counter = 0 berarti News tsb tidak ada dlm tabel News, maka AcceptNewsDelete gagal
                    If counter = 0 Then
                        Throw New Exception("Cannot delete the following News: " & rowData.NewsTitle & " because that News does not exist in the database anymore.")
                    Else 'Bila counter != 0, maka News tsb bisa didelete
                        'hapus item tersebut dari tabel News
                        AccessNews.DeleteNews(rowData.NewsID)

                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
                        'catat aktifitas dalam tabel Audit Trail
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsTitle", "Delete", rowData.NewsTitle, "", "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsSummary", "Delete", rowData.NewsSummary, "", "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsContent", "Delete", rowData.NewsContent, "", "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsStartDate", "Delete", rowData.NewsStartDate, "", "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsEndDate", "Delete", rowData.NewsEndDate, "", "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsID", "Delete", rowData.NewsID, "", "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsCreatedDate", "Delete", rowData.NewsCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "", "Accepted")

                            'hapus item tersebut dalam tabel News_Approval
                            AccessPending.DeleteNewsApproval(rowData.News_PendingApprovalID)

                            'hapus item tersebut dalam tabel News_PendingApproval
                            Using AccessPendingNews As New AMLDAL.AMLDataSetTableAdapters.News_PendingApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingNews, oSQLTrans)
                                AccessPendingNews.DeleteNewsPendingApproval(rowData.News_PendingApprovalID)
                            End Using

                            oSQLTrans.Commit()
                        End Using

                        'hapus attach add approval reject
                        Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                            adapter.DeleteNewsAttachmentbyFkNewsApprovalId(ParamPkNewsManagement)
                        End Using

                        'hapus attach Delete approval
                        Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                            adapter.DeleteNewsAttachmentbyFkNewsId(rowData.NewsID)
                        End Using


                    End If
                Else 'Bila ObjTable.Rows.Count = 0 berarti News tsb sudah tidak lagi berada dlm tabel News_Approval
                    Throw New Exception("Cannot delete the following News: " & Me.LabelNewsTitleDelete.Text & " because that News is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub

#End Region
#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject News add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectNewsAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
            Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.News_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessNews, oSQLTrans)
                Using AccessNewsPending As New AMLDAL.AMLDataSetTableAdapters.News_PendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessNewsPending, oSQLTrans)
                    Using TableNews As Data.DataTable = AccessNews.GetNewsApprovalData(Me.ParamPkNewsManagement)

                        'Bila TableNews.Rows.Count > 0 berarti News tsb masih ada dlm tabel News_Approval
                        If TableNews.Rows.Count > 0 Then
                            Dim ObjRow As AMLDAL.AMLDataSet.News_ApprovalRow = TableNews.Rows(0)

                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
                            'catat aktifitas dalam tabel Audit Trail
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsTitle", "Add", "", ObjRow.NewsTitle, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsSummary", "Add", "", ObjRow.NewsSummary, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsContent", "Add", "", ObjRow.NewsContent, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsStartDate", "Add", "", ObjRow.NewsStartDate, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsEndDate", "Add", "", ObjRow.NewsEndDate, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsCreatedDate", "Add", "", ObjRow.NewsCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                            'hapus item tersebut dalam tabel News_Approval
                            AccessNews.DeleteNewsApproval(Me.ParamPkNewsManagement)

                            'hapus item tersebut dalam tabel News_PendingApproval
                            AccessNewsPending.DeleteNewsPendingApproval(Me.ParamPkNewsManagement)
                            oSQLTrans.Commit()

                            'hapus attach add approval reject
                            Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                                adapter.DeleteNewsAttachmentbyFkNewsApprovalId(ParamPkNewsManagement)
                            End Using


                        Else 'Bila TableNews.Rows.Count = 0 berarti News tsb sudah tidak lagi berada dlm tabel News_Approval
                            Throw New Exception("Operation failed. The following News: " & Me.LabelNewsTitleAdd.Text & " is no longer in the approval table.")
                        End If
                    End Using
                End Using
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject News edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectNewsEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
            Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.News_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessNews, oSQLTrans)
                Using AccessNewsPending As New AMLDAL.AMLDataSetTableAdapters.News_PendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessNewsPending, oSQLTrans)
                    Using TableNews As Data.DataTable = AccessNews.GetNewsApprovalData(Me.ParamPkNewsManagement)

                        'Bila TableNews.Rows.Count > 0 berarti News tsb masih ada dlm tabel News_Approval
                        If TableNews.Rows.Count > 0 Then
                            Dim ObjRow As AMLDAL.AMLDataSet.News_ApprovalRow = TableNews.Rows(0)

                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(7)
                            'catat aktifitas dalam tabel Audit Trail
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsTitle", "Edit", ObjRow.NewsTitle_Old, ObjRow.NewsTitle, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsSummary", "Edit", ObjRow.NewsSummary_Old, ObjRow.NewsSummary, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsContent", "Edit", ObjRow.NewsContent_Old, ObjRow.NewsContent, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsStartDate", "Edit", ObjRow.NewsStartDate_Old, ObjRow.NewsStartDate, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsEndDate", "Edit", ObjRow.NewsEndDate_Old, ObjRow.NewsEndDate, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsID", "Edit", ObjRow.NewsID_Old, ObjRow.NewsID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsCreatedDate", "Edit", ObjRow.NewsCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.NewsCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                            'hapus item tersebut dalam tabel News_Approval
                            AccessNews.DeleteNewsApproval(Me.ParamPkNewsManagement)

                            'hapus item tersebut dalam tabel News_PendingApproval
                            AccessNewsPending.DeleteNewsPendingApproval(Me.ParamPkNewsManagement)
                            oSQLTrans.Commit()

                            'hapus attach add approval reject
                            Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                                adapter.DeleteNewsAttachmentbyFkNewsApprovalId(ParamPkNewsManagement)
                            End Using

                        Else 'Bila TableNews.Rows.Count = 0 berarti News tsb sudah tidak lagi berada dlm tabel News_Approval
                            Throw New Exception("Operation failed. The following News: " & Me.LabelOldNewsTitle.Text & " is no longer in the approval table.")
                        End If
                    End Using
                End Using
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject News delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectNewsDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
            Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.News_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessNews, oSQLTrans)
                Using AccessNewsPending As New AMLDAL.AMLDataSetTableAdapters.News_PendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessNewsPending, oSQLTrans)
                    Using TableNews As Data.DataTable = AccessNews.GetNewsApprovalData(Me.ParamPkNewsManagement)

                        'Bila TableNews.Rows.Count > 0 berarti News tsb masih ada dlm tabel News_Approval
                        If TableNews.Rows.Count > 0 Then
                            Dim ObjRow As AMLDAL.AMLDataSet.News_ApprovalRow = TableNews.Rows(0)

                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(7)
                            'catat aktifitas dalam tabel Audit Trail
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsTitle", "Delete", ObjRow.NewsTitle_Old, ObjRow.NewsTitle, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsSummary", "Delete", ObjRow.NewsSummary_Old, ObjRow.NewsSummary, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsContent", "Delete", ObjRow.NewsContent_Old, ObjRow.NewsContent, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsStartDate", "Delete", ObjRow.NewsStartDate_Old, ObjRow.NewsStartDate, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsEndDate", "Delete", ObjRow.NewsEndDate_Old, ObjRow.NewsEndDate, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsID", "Delete", ObjRow.NewsID_Old, ObjRow.NewsID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "News", "NewsCreatedDate", "Delete", ObjRow.NewsCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.NewsCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                            'hapus item tersebut dalam tabel News_Approval
                            AccessNews.DeleteNewsApproval(Me.ParamPkNewsManagement)

                            'hapus item tersebut dalam tabel News_PendingApproval
                            AccessNewsPending.DeleteNewsPendingApproval(Me.ParamPkNewsManagement)
                            oSQLTrans.Commit()

                            'hapus attach add approval reject
                            Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                                adapter.DeleteNewsAttachmentbyFkNewsApprovalId(ParamPkNewsManagement)
                            End Using

                        Else 'Bila TableNews.Rows.Count = 0 berarti News tsb sudah tidak lagi berada dlm tabel News_Approval
                            Throw New Exception("Operation failed. The following News: " & Me.LabelNewsTitleDelete.Text & " is no longer in the approval table.")
                        End If
                    End Using
                End Using
            End Using
        End Using
        'End Using
    End Sub
#End Region

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HideAll()
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.NewsAdd
                        Me.LoadNewsAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.NewsEdit
                        Me.LoadNewsEdit()
                    Case Sahassa.AML.Commonly.TypeConfirm.NewsDelete
                        Me.LoadNewsDelete()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.NewsAdd
                    Me.AcceptNewsAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.NewsEdit
                    Me.AcceptNewsEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.NewsDelete
                    Me.AcceptNewsDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "NewsManagementApproval.aspx"

            Me.Response.Redirect("NewsManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.NewsAdd
                    Me.RejectNewsAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.NewsEdit
                    Me.RejectNewsEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.NewsDelete
                    Me.RejectNewsDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "NewsManagementApproval.aspx"

            Me.Response.Redirect("NewsManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "NewsManagementApproval.aspx"

        Me.Response.Redirect("NewsManagementApproval.aspx", False)
    End Sub

    Protected Sub GridAttach_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridAttach.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim PK As Integer = GridAttach.DataKeys(e.Row.RowIndex).Value
            Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                CType(e.Row.FindControl("Datalist1"), System.Web.UI.WebControls.DataList).DataSource = adapter.GetNewsAttachmentByPK(PK)
                CType(e.Row.FindControl("Datalist1"), System.Web.UI.WebControls.DataList).DataBind()
            End Using
        End If
    End Sub
    Protected Function GenerateLink(ByVal PK_NewsAttachment As Integer, ByVal Filename As String, ByVal Fk_NewsApprovalId As Integer) As String
        Return String.Format("<a href=""{0}"">{1}</a> ", ResolveUrl("~\NewsDownloadAttach.aspx?PK_NewsAttachment=" & PK_NewsAttachment), Filename)
    End Function
    Private _oRowNewsAttach As AMLDAL.AMLDataSet.NewsAttachmentDataTable
    Private _oRowOldNewsAttach As AMLDAL.AMLDataSet.NewsAttachmentDataTable
    Private ReadOnly Property oRowNewsAttach() As AMLDAL.AMLDataSet.NewsAttachmentDataTable
        Get
            If Not _oRowNewsAttach Is Nothing Then
                Return _oRowNewsAttach
            Else
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                    _oRowNewsAttach = adapter.GetNewsAttachmentByNewsIDApproval(Me.ParamPkNewsManagement)
                    Return _oRowNewsAttach
                End Using
            End If

        End Get
    End Property
    Private ReadOnly Property oRowOldNewsAttach() As AMLDAL.AMLDataSet.NewsAttachmentDataTable
        Get
            If Not _oRowOldNewsAttach Is Nothing Then
                Return _oRowOldNewsAttach
            Else
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                    _oRowOldNewsAttach = adapter.GetByFk_NewsID(LabelOldNewsID.Text)
                    Return _oRowOldNewsAttach
                End Using
            End If

        End Get
    End Property
    Protected Sub GridNewAttach_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridNewAttach.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim PK As Integer = GridNewAttach.DataKeys(e.Row.RowIndex).Value
            Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                CType(e.Row.FindControl("Datalist1"), System.Web.UI.WebControls.DataList).DataSource = adapter.GetNewsAttachmentByPK(PK)
                CType(e.Row.FindControl("Datalist1"), System.Web.UI.WebControls.DataList).DataBind()
            End Using
        End If
    End Sub
    Protected Sub GridOldAttach_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridOldAttach.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim PK As Integer = GridOldAttach.DataKeys(e.Row.RowIndex).Value
            Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                CType(e.Row.FindControl("Datalist1"), System.Web.UI.WebControls.DataList).DataSource = adapter.GetNewsAttachmentByPK(PK)
                CType(e.Row.FindControl("Datalist1"), System.Web.UI.WebControls.DataList).DataBind()
            End Using
        End If
    End Sub
    Protected Sub GridAttachDel_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridAttachDel.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim PK As Integer = GridAttachDel.DataKeys(e.Row.RowIndex).Value
            Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                CType(e.Row.FindControl("Datalist1"), System.Web.UI.WebControls.DataList).DataSource = adapter.GetNewsAttachmentByPK(PK)
                CType(e.Row.FindControl("Datalist1"), System.Web.UI.WebControls.DataList).DataBind()
            End Using
        End If
    End Sub

End Class