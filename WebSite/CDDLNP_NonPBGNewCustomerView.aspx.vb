Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports System.Collections.Generic
Imports System.IO

Partial Class CDDLNP_NonPBGNewCustomerView
    Inherits Parent
    Private BindGridFromExcel As Boolean = False

#Region " Property "
    Private ReadOnly Property GetModuleName() As String
        Get
            Return "CDDLNP_NonPBGNewCustomerView"
        End Get
    End Property

    Private Property SetnGetCDDType() As String
        Get
            Return CType(IIf(Session(Me.GetModuleName & ".CDDType") Is Nothing, 0, Session(Me.GetModuleName & ".CDDType")), String)
        End Get
        Set(ByVal Value As String)
            Session(Me.GetModuleName & ".CDDType") = Value
        End Set
    End Property
    Private Property SetnGetNama() As String
        Get
            Return CType(IIf(Session(Me.GetModuleName & ".Nama") Is Nothing, String.Empty, Session(Me.GetModuleName & ".Nama")), String)
        End Get
        Set(ByVal Value As String)
            Session(Me.GetModuleName & ".Nama") = Value
        End Set
    End Property
    Private Property SetnGetCDD_NasabahType_ID() As String
        Get
            Return CType(IIf(Session(Me.GetModuleName & ".CDD_NasabahType_ID") Is Nothing, 0, Session(Me.GetModuleName & ".CDD_NasabahType_ID")), String)
        End Get
        Set(ByVal Value As String)
            Session(Me.GetModuleName & ".CDD_NasabahType_ID") = Value
        End Set
    End Property
    Private Property SetnGetCIF() As String
        Get
            Return CType(IIf(Session(Me.GetModuleName & ".CIF") Is Nothing, String.Empty, Session(Me.GetModuleName & ".CIF")), String)
        End Get
        Set(ByVal Value As String)
            Session(Me.GetModuleName & ".CIF") = Value
        End Set
    End Property
    Private Property SetnGetAMLRiskRatingPersonal() As String
        Get
            Return CType(IIf(Session(Me.GetModuleName & ".AMLRiskRatingPersonal") Is Nothing, String.Empty, Session(Me.GetModuleName & ".AMLRiskRatingPersonal")), String)
        End Get
        Set(ByVal Value As String)
            Session(Me.GetModuleName & ".AMLRiskRatingPersonal") = Value
        End Set
    End Property
    Private Property SetnGetAMLRiskRatingGabungan() As String
        Get
            Return CType(IIf(Session(Me.GetModuleName & ".AMLRiskRatingGabungan") Is Nothing, String.Empty, Session(Me.GetModuleName & ".AMLRiskRatingGabungan")), String)
        End Get
        Set(ByVal Value As String)
            Session(Me.GetModuleName & ".AMLRiskRatingGabungan") = Value
        End Set
    End Property
    Private Property SetnGetIsPEP() As String
        Get
            Return CType(IIf(Session(Me.GetModuleName & ".IsPEP") Is Nothing, String.Empty, Session(Me.GetModuleName & ".IsPEP")), String)
        End Get
        Set(ByVal Value As String)
            Session(Me.GetModuleName & ".IsPEP") = Value
        End Set
    End Property
    Private Property SetnGetCreatedDateStart() As String
        Get
            Return CType(IIf(Session(Me.GetModuleName & ".CreatedDateStart") Is Nothing, String.Empty, Session(Me.GetModuleName & ".CreatedDateStart")), String)
        End Get
        Set(ByVal Value As String)
            Session(Me.GetModuleName & ".CreatedDateStart") = Value
        End Set
    End Property
    Private Property SetnGetCreatedDateEnd() As String
        Get
            Return CType(IIf(Session(Me.GetModuleName & ".CreatedDateEnd") Is Nothing, String.Empty, Session(Me.GetModuleName & ".CreatedDateEnd")), String)
        End Get
        Set(ByVal Value As String)
            Session(Me.GetModuleName & ".CreatedDateEnd") = Value
        End Set
    End Property
    Private Property SetnGetCreatedBy() As String
        Get
            Return CType(IIf(Session(Me.GetModuleName & ".CreatedBy") Is Nothing, String.Empty, Session(Me.GetModuleName & ".CreatedBy")), String)
        End Get
        Set(ByVal Value As String)
            Session(Me.GetModuleName & ".CreatedBy") = Value
        End Set
    End Property
    Private Property SetnGetParentBO() As String
        Get
            Return CType(IIf(Session(Me.GetModuleName & ".ParentBO") Is Nothing, String.Empty, Session(Me.GetModuleName & ".ParentBO")), String)
        End Get
        Set(ByVal Value As String)
            Session(Me.GetModuleName & ".ParentBO") = Value
        End Set
    End Property
    Private Property SetnGetLastApprovalGroup() As String
        Get
            Return CType(IIf(Session(Me.GetModuleName & ".LastApprovalGroup") Is Nothing, String.Empty, Session(Me.GetModuleName & ".LastApprovalGroup")), String)
        End Get
        Set(ByVal Value As String)
            Session(Me.GetModuleName & ".LastApprovalGroup") = Value
        End Set
    End Property
    Private Property SetnGetApprovalStatus() As String
        Get
            Return CType(IIf(Session(Me.GetModuleName & ".ApprovalStatus") Is Nothing, 0, Session(Me.GetModuleName & ".ApprovalStatus")), String)
        End Get
        Set(ByVal Value As String)
            Session(Me.GetModuleName & ".ApprovalStatus") = Value
        End Set
    End Property

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session(Me.GetModuleName & ".Selected") Is Nothing, New ArrayList, Session(Me.GetModuleName & ".Selected"))
        End Get
        Set(ByVal value As ArrayList)
            Session(Me.GetModuleName & ".Selected") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session(Me.GetModuleName & ".Sort") Is Nothing, "PK_CDDLNP_ID  desc", Session(Me.GetModuleName & ".Sort"))
        End Get
        Set(ByVal Value As String)
            Session(Me.GetModuleName & ".Sort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session(Me.GetModuleName & ".CurrentPage") Is Nothing, 0, Session(Me.GetModuleName & ".CurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session(Me.GetModuleName & ".CurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session(Me.GetModuleName & ".RowTotal") Is Nothing, 0, Session(Me.GetModuleName & ".RowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session(Me.GetModuleName & ".RowTotal") = Value
        End Set
    End Property
    Private Function SetnGetBindTable() As VList(Of vw_CDDLNP_View)
        Return DataRepository.vw_CDDLNP_ViewProvider.GetPaged(SearchFilter, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, SetnGetRowTotal)
    End Function
#End Region

    Private Sub ClearThisPageSessions()
        Me.SetnGetCDDType = String.Empty
        Me.SetnGetNama = String.Empty
        Me.SetnGetCDD_NasabahType_ID = String.Empty
        Me.SetnGetCIF = String.Empty
        Me.SetnGetAMLRiskRatingPersonal = String.Empty
        Me.SetnGetAMLRiskRatingGabungan = String.Empty
        Me.SetnGetCreatedDateStart = String.Empty
        Me.SetnGetCreatedDateEnd = String.Empty
        Me.SetnGetCreatedBy = String.Empty
        Me.SetnGetParentBO = String.Empty
        Me.SetnGetIsPEP = String.Empty
        Me.SetnGetLastApprovalGroup = String.Empty
        Me.SetnGetApprovalStatus = String.Empty

        Me.SetnGetSelectedItem = Nothing
        Me.SetnGetSort = Nothing
        Me.SetnGetCurrentPage = Nothing
        Me.SetnGetRowTotal = Nothing
    End Sub

    Private Sub BindComboBox()
        Try
            cboCDDType.Items.Clear()
            cboCDDType.Items.Add(New ListItem("[All]", String.Empty))
            For Each item As CDD_Type In DataRepository.CDD_TypeProvider.GetAll
                cboCDDType.Items.Add(New ListItem(item.CDD_Type, item.PK_CDD_Type_ID))
            Next

            cboNasabahType.Items.Clear()
            cboNasabahType.Items.Add(New ListItem("[All]", String.Empty))
            For Each item As CDD_NasabahType In DataRepository.CDD_NasabahTypeProvider.GetAll
                cboNasabahType.Items.Add(New ListItem(item.CDD_NasabahType_ID, item.PK_CDD_NasabahType_ID))
            Next

            cboIsPEP.Items.Clear()
            cboIsPEP.Items.Add(New ListItem("[All]", String.Empty))
            cboIsPEP.Items.Add(New ListItem("Yes", 1))
            cboIsPEP.Items.Add(New ListItem("No", 0))

            cboApprovalStatus.Items.Clear()
            cboApprovalStatus.Items.Add(New ListItem("[All]", String.Empty))
            For Each item As CDDLNP_Status In DataRepository.CDDLNP_StatusProvider.GetAll
                cboApprovalStatus.Items.Add(New ListItem(item.ApprovalStatus, item.PK_CDDLNP_Status_ID))
            Next
        Catch
            Throw
        End Try
    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknown Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridViewCDDLNP.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim PkId As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(PkId) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(PkId) Then
        '                    ArrTarget.Add(PkId)
        '                End If
        '            Else
        '                ArrTarget.Remove(PkId)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(PkId) Then
        '                    ArrTarget.Add(PkId)
        '                End If
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next

        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridViewCDDLNP.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
        Me.BindGrid()
    End Sub

    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim totalrow As Int16 = 0
        For Each gridRow As DataGridItem In Me.GridViewCDDLNP.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                totalrow = CType(totalrow + 1, Int16)
            End If
        Next
        If i = totalrow Then
            Me.CheckBoxSelectAll.Checked = True
        Else
            Me.CheckBoxSelectAll.Checked = False
        End If
    End Sub

    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridViewCDDLNP.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim PkId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PkId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PkId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PkId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    Protected Sub GridViewCDDLNP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewCDDLNP.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow))
                End If

                'Parent BO
                If e.Item.Cells(12).Text = "&nbsp;" Then
                    e.Item.Cells(12).Text = "N/A"
                End If

                'Approval next PIC
                Select Case e.Item.Cells(16).Text
                    Case 1, 3 'Jika Draft dan Request change maka PIC adalah CDD Initiator
                        e.Item.Cells(17).Text = "RM - " & e.Item.Cells(14).Text

                    Case 4, 5 'Jika Approve dan Declined maka PIC adalah "-"
                        e.Item.Cells(17).Text = "-"

                    Case 2
                        Dim strUser As String = e.Item.Cells(14).Text
                        Dim strWhereClause As String = "List_RM LIKE '%" & strUser & ";%'"
                        Using objWorkflow As TList(Of CDDLNP_WorkflowUpload) = DataRepository.CDDLNP_WorkflowUploadProvider.GetPaged(strWhereClause, String.Empty, 0, 1, 0)
                            If objWorkflow.Count > 0 Then
                                Select Case CInt(e.Item.Cells(17).Text) + 1
                                    Case 2
                                        e.Item.Cells(17).Text = "Team Head - " & objWorkflow(0).List_TeamHead.TrimEnd(";")
                                    Case 3
                                        e.Item.Cells(17).Text = "Group Head - " & objWorkflow(0).List_GroupHead.TrimEnd(";")
                                    Case 4
                                        e.Item.Cells(17).Text = "Head Off - " & objWorkflow(0).List_HeadOff.TrimEnd(";")

                                End Select

                            End If
                        End Using
                End Select

                'PEP
                If e.Item.Cells(11).Text = "True" Then
                    e.Item.Cells(11).Text = "Yes"
                Else
                    e.Item.Cells(11).Text = "No"
                End If

                If e.Item.Cells(14).Text = Sahassa.AML.Commonly.SessionUserId Then
                    e.Item.Cells(GridViewCDDLNP.Columns.Count - 2).Visible = True
                    e.Item.Cells(GridViewCDDLNP.Columns.Count - 1).Visible = True
                Else
                    e.Item.Cells(GridViewCDDLNP.Columns.Count - 2).Visible = False
                    e.Item.Cells(GridViewCDDLNP.Columns.Count - 1).Visible = False
                End If

                Dim EditButton As LinkButton = CType(e.Item.Cells(GridViewCDDLNP.Columns.Count - 2).FindControl("LnkEdit"), LinkButton)
                Dim DelButton As LinkButton = CType(e.Item.Cells(GridViewCDDLNP.Columns.Count - 1).FindControl("LnkDelete"), LinkButton)

                'Approval
                Select Case e.Item.Cells(16).Text
                    Case 2, 4, 5
                        EditButton.Enabled = False
                        DelButton.Enabled = False

                    Case 1, 3, "&nbsp;"
                        EditButton.Enabled = True
                        DelButton.Enabled = True
                        Dim StrDelMessage As String = "javascript:return confirm('You are about to delete CDD for Customer: " & e.Item.Cells(7).Text & ". This action cannot be undone. Are you sure want to proceed?')"
                        DelButton.Attributes.Add("onclick", StrDelMessage)

                End Select

            Else
                e.Item.Cells(18).Text = "Action"
                e.Item.Cells(18).ForeColor = Drawing.Color.White
                e.Item.Cells(18).ColumnSpan = 4
                e.Item.Cells(19).Visible = False
                e.Item.Cells(20).Visible = False
                e.Item.Cells(21).Visible = False
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewCDDLNP_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridViewCDDLNP.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewCDDLNP_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewCDDLNP.ItemCommand
        Try
            Select Case e.CommandName.ToLower
                Case "detail"
                    Session("CDDLNP.BackPage") = Sahassa.AML.Commonly.SessionCurrentPage
                    Response.Redirect("CDDLNPApprovalDetail.aspx?ID=" & e.Item.Cells(1).Text)

                Case "print"
                    Me.Controls.Add(New LiteralControl("<script>window.open('CDDLNPPrint.aspx?CID=" & e.Item.Cells(1).Text & "&TID=" & e.Item.Cells(3).Text & "&NTID=" & e.Item.Cells(5).Text & "','mustunique','scrollbars=1,directories=0,height=600,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0');</script> "))

                Case "edit"
                    Session("CDDLNPEdit.ReferenceID") = 0
                    Response.Redirect("CDDLNPEdit.aspx?CID=" & e.Item.Cells(1).Text & "&TID=" & e.Item.Cells(3).Text & "&NTID=" & e.Item.Cells(5).Text)

                Case "delete"
                    Using objCDDLNP As New CDDLNPBLL
                        objCDDLNP.CDDLNPDelete(e.Item.Cells(1).Text)
                        ClientScript.RegisterStartupScript(Page.GetType(), "DeleteSuccess" & e.Item.Cells(1).Text, "alert('CDD is deleted.');", False)
                    End Using
                    'Response.Redirect("CDDLNPDelete.aspx?CID=" & e.Item.Cells(1).Text & "&TID=" & e.Item.Cells(3).Text & "&NTID=" & e.Item.Cells(5).Text)

            End Select
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be less than or equal to the total page count.")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageClear.Click
        Try
            Me.ClearThisPageSessions()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Me.SetnGetCDDType = cboCDDType.SelectedValue
        Me.SetnGetNama = txtName.Text
        Me.SetnGetCDD_NasabahType_ID = cboNasabahType.SelectedValue
        Me.SetnGetCIF = txtCIF.Text
        Me.SetnGetAMLRiskRatingPersonal = txtRatingPersonal.Text
        Me.SetnGetAMLRiskRatingGabungan = txtRatingGabungan.Text
        Me.SetnGetCreatedDateStart = txtCreatedDateStart.Text
        Me.SetnGetCreatedDateEnd = txtCreatedDateEnd.Text
        Me.SetnGetCreatedBy = txtCreatedBy.Text
        Me.SetnGetParentBO = txtParentBO.Text
        Me.SetnGetIsPEP = cboIsPEP.SelectedValue
        Me.SetnGetLastApprovalGroup = txtLastApproval.Text
        Me.SetnGetApprovalStatus = cboApprovalStatus.SelectedValue
        Me.SetnGetCurrentPage = 0
        Me.CollectSelected()
    End Sub

    Private Sub BindGrid()
        Me.SettingControl()

        Me.GridViewCDDLNP.DataSource = Me.SetnGetBindTable
        Me.GridViewCDDLNP.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridViewCDDLNP.DataBind()
        Me.lblNoRecord.Visible = (Me.SetnGetRowTotal = 0)
    End Sub

    Private Function SearchFilter() As String
        Dim strWhereClause(-1) As String

        If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
            Me.SetnGetCurrentPage = Me.GetPageTotal - 1
        ElseIf Me.SetnGetCurrentPage = -1 Then
            Me.SetnGetCurrentPage = 0
        End If

        Try

            SetnGetCDDType = 3
            If SetnGetCDDType.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_CDDLNP_ViewColumn.FK_CDD_Type_ID.ToString & " = " & SetnGetCDDType
            End If

            If SetnGetNama.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_CDDLNP_ViewColumn.Nama.ToString & " LIKE '%" & SetnGetNama.Trim.Replace("'", "''") & "%'"
            End If

            If SetnGetCDD_NasabahType_ID.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_CDDLNP_ViewColumn.FK_CDD_NasabahType_ID.ToString & " = " & SetnGetCDD_NasabahType_ID
            End If

            If SetnGetCIF.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_CDDLNP_ViewColumn.CIF.ToString & " LIKE '%" & SetnGetCIF.Trim.Replace("'", "''") & "%'"
            End If

            If SetnGetAMLRiskRatingPersonal.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_CDDLNP_ViewColumn.AMLRiskRatingPersonal.ToString & " LIKE '%" & SetnGetAMLRiskRatingPersonal.Trim.Replace("'", "''") & "%'"
            End If

            If SetnGetAMLRiskRatingGabungan.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_CDDLNP_ViewColumn.AMLRiskRatingGabungan.ToString & " LIKE '%" & SetnGetAMLRiskRatingGabungan.Trim.Replace("'", "''") & "%'"
            End If

            'SetnGetCreatedBy = Sahassa.AML.Commonly.SessionUserId
            If SetnGetCreatedBy.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_CDDLNP_ViewColumn.CreatedBy.ToString & " = '" & SetnGetCreatedBy.Trim.Replace("'", "''") & "'"
            End If

            If Me.SetnGetCreatedDateStart.Length > 0 AndAlso Me.SetnGetCreatedDateEnd.Length > 0 Then
                If IsDate(SetnGetCreatedDateStart) AndAlso IsDate(SetnGetCreatedDateEnd) Then
                    If SetnGetCreatedDateStart <= SetnGetCreatedDateEnd Then
                        ReDim Preserve strWhereClause(strWhereClause.Length)
                        strWhereClause(strWhereClause.Length - 1) = "(CONVERT(DATETIME,CONVERT(VARCHAR(20),CreatedDate,112),112)  BETWEEN '" & SetnGetCreatedDateStart & "' AND '" & SetnGetCreatedDateEnd & "')"
                    Else
                        Throw New Exception("Created date searching is not valid")
                    End If
                Else
                    Throw New Exception("Created date format is not valid")
                End If
            End If

            If SetnGetParentBO.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_CDDLNP_ViewColumn.ParentBO.ToString & " LIKE '%" & SetnGetParentBO.Trim.Replace("'", "''") & "%'"
            End If

            If SetnGetIsPEP.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_CDDLNP_ViewColumn.IsPEP.ToString & " = " & SetnGetIsPEP
            End If

            If SetnGetLastApprovalGroup.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_CDDLNP_ViewColumn.LastWorkflowGroup.ToString & " LIKE '%" & SetnGetLastApprovalGroup & "%'"
            End If

            If SetnGetApprovalStatus.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_CDDLNP_ViewColumn.LastApprovalStatus.ToString & " = " & SetnGetApprovalStatus
            End If

            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = "FK_RefParent_ID IS NULL AND CreatedBy IN (" & GetRMUser() & ")"

            Return String.Join(" AND ", strWhereClause)
        Catch
            Throw
        End Try
    End Function

    Private Sub SettingControl()
        cboCDDType.SelectedValue = Me.SetnGetCDDType
        txtName.Text = Me.SetnGetNama
        cboNasabahType.SelectedValue = Me.SetnGetCDD_NasabahType_ID
        txtCIF.Text = Me.SetnGetCIF
        txtRatingPersonal.Text = Me.SetnGetAMLRiskRatingPersonal
        txtRatingGabungan.Text = Me.SetnGetAMLRiskRatingGabungan
        txtParentBO.Text = Me.SetnGetParentBO
        txtCreatedDateStart.Text = Me.SetnGetCreatedDateStart
        txtCreatedDateEnd.Text = Me.SetnGetCreatedDateEnd
        txtCreatedBy.Text = Me.SetnGetCreatedBy
        cboIsPEP.SelectedValue = Me.SetnGetIsPEP
        txtLastApproval.Text = Me.SetnGetLastApprovalGroup
        cboApprovalStatus.SelectedValue = Me.SetnGetApprovalStatus
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                Me.ClearThisPageSessions()

                Me.popupCreatedDateStart.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtCreatedDateStart.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popupCreatedDateStart.Style.Add("display", "")
                Me.popupCreatedDateEnd.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtCreatedDateEnd.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popupCreatedDateEnd.Style.Add("display", "")

                Me.GridViewCDDLNP.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                BindComboBox()
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
            Me.SetCheckedAll()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub BindSelected()
        Try
            Dim SbPk As New StringBuilder
            SbPk.Append("0, ")
            For Each IdPk As Int64 In Me.SetnGetSelectedItem
                SbPk.Append(IdPk.ToString & ", ")
            Next

            Me.GridViewCDDLNP.DataSource = DataRepository.vw_CDDLNP_ViewProvider.GetPaged("Pk_CDDLNP_Id in (" & SbPk.ToString.Substring(0, SbPk.Length - 2) & ") ", Me.SetnGetSort, 0, Integer.MaxValue, 0)
            Me.GridViewCDDLNP.AllowPaging = False
            Me.GridViewCDDLNP.DataBind()

            Me.GridViewCDDLNP.Columns(0).Visible = False
            Me.GridViewCDDLNP.Columns(1).Visible = False
            Me.GridViewCDDLNP.Columns(GridViewCDDLNP.Columns.Count - 4).Visible = False
            Me.GridViewCDDLNP.Columns(GridViewCDDLNP.Columns.Count - 3).Visible = False
            Me.GridViewCDDLNP.Columns(GridViewCDDLNP.Columns.Count - 2).Visible = False
            Me.GridViewCDDLNP.Columns(GridViewCDDLNP.Columns.Count - 1).Visible = False
        Catch
            Throw
        End Try
    End Sub

    Private Sub BindSelectedAll()
        Try
            Me.GridViewCDDLNP.DataSource = DataRepository.vw_CDDLNP_ViewProvider.GetPaged(SearchFilter, Me.SetnGetSort, 0, Int32.MaxValue, 0)

            Me.GridViewCDDLNP.AllowPaging = False
            Me.GridViewCDDLNP.DataBind()

            Me.GridViewCDDLNP.Columns(0).Visible = False
            Me.GridViewCDDLNP.Columns(1).Visible = False
            Me.GridViewCDDLNP.Columns(GridViewCDDLNP.Columns.Count - 4).Visible = False
            Me.GridViewCDDLNP.Columns(GridViewCDDLNP.Columns.Count - 3).Visible = False
            Me.GridViewCDDLNP.Columns(GridViewCDDLNP.Columns.Count - 2).Visible = False
            Me.GridViewCDDLNP.Columns(GridViewCDDLNP.Columns.Count - 1).Visible = False
        Catch
            Throw
        End Try
    End Sub

    Protected Sub lnkExportData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportData.Click
        Try
            BindGridFromExcel = True
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CDDLNP_Selected.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(Me.GridViewCDDLNP)
            Me.GridViewCDDLNP.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub lnkExportAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAll.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelectedAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CDDLNP_All.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(Me.GridViewCDDLNP)
            Me.GridViewCDDLNP.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LinkButtonAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddNew.Click
        CDDLNPBLL.SessionReferenceID = 0
        Me.Response.Redirect("CDDLNPAdd.aspx?TID=3&NTID=1")
    End Sub

    Private Function GetRMUser()
        Dim strUser As String = Sahassa.AML.Commonly.SessionUserId
        Dim strWhereClause As String = " List_TeamHead LIKE '%" & strUser & ";%' OR List_GroupHead LIKE '%" & strUser & ";%' OR List_HeadOff LIKE '%" & strUser & ";%'"
        Dim strChild As String = "'" & Sahassa.AML.Commonly.SessionUserId & "',"

        'Get RM under this user
        For Each objWorkflow As CDDLNP_WorkflowUpload In DataRepository.CDDLNP_WorkflowUploadProvider.GetPaged(strWhereClause, String.Empty, 0, Int32.MaxValue, 0)
            For Each user As String In objWorkflow.List_RM.Split(";")
                strChild &= "'" & user & "',"
            Next
        Next

        Return strChild.Remove(strChild.LastIndexOf(","))
    End Function
End Class