<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="LTKTView.aspx.vb" Inherits="LTKTView"  %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script src="Script/popcalendar.js"></script>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div >
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="10" height="100%" /></td>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <img src="Images/dot_title.gif" width="17" height="17">
                                                <strong>
                                                    <asp:Label ID="Label1" runat="server" Text="LTKT - View"></asp:Label>
                                                </strong>
                                                <hr />
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Label6" runat="server" Text="Search Criteria" Font-Bold="True"></asp:Label>
                                                            &nbsp;</td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('SearchCriteria','searchimage4','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="searchimage4" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px"></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="SearchCriteria">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%; height: 21px;">
                                                            <asp:Label ID="Label2" runat="server" Text="CIF No"></asp:Label></td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%; height: 21px;">
                                                            <asp:TextBox ID="TxtCIFNo" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="250px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap="nowrap" style="width: 15%; background-color: #fff7e6">
                                                            <asp:Label ID="Label7" runat="server" Text="Name"></asp:Label></td>
                                                        <td nowrap="nowrap" style="width: 35%; background-color: #ffffff">
                                                            <asp:TextBox ID="TxtFullName" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="250px" MaxLength="50"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label3" runat="server" Text="Tanggal Transaksi"></asp:Label></td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtTransactionDateStart" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="125px" MaxLength="50"></asp:TextBox>
                                                            <input id="popUpTransactionDate" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                                border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                                border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                                width: 16px;" type="button" onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')"
                                                                runat="server">
                                                            to
                                                            <asp:TextBox ID="txtTransactionDateEnd" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox><input id="PopTransactionEndDate" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                                border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                                border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                                width: 16px;" type="button" onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')"
                                                                runat="server"></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label4" runat="server" Text="Transaction Nominal CashIn"></asp:Label></td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtTransNominalBCashIn" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="125px" MaxLength="50"></asp:TextBox>
                                                            s/d
                                                            <asp:TextBox ID="txtTransNominalUCashIn" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label10" runat="server" Text="Transaction Nominal CashOut"></asp:Label></td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtTransNominalBCashOut" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="125px" MaxLength="50"></asp:TextBox>
                                                            s/d
                                                            <asp:TextBox ID="txtTransNominalUCashOut" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap="nowrap" style="width: 15%; background-color: #fff7e6">
                                                            Last Update Date</td>
                                                        <td nowrap="nowrap" style="width: 35%; background-color: #ffffff">
                                                            <asp:TextBox ID="TxtlastUpdateStart" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox>
                                                            <input id="btnupdateStart" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                                border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                                border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                                width: 16px;" type="button" onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')"
                                                                runat="server">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="10">
                                                            &nbsp;<asp:ImageButton ID="ImageButtonSearch" TabIndex="3" runat="server" SkinID="SearchButton"
                                                                CausesValidation="False" ImageUrl="~/Images/Button/Search.gif"></asp:ImageButton>&nbsp;<asp:ImageButton
                                                                    ID="Imagebutton1" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/clearSearch.gif" TabIndex="3" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr id="TR1">
                                            <td valign="top" width="98%" bgcolor="#ffffff">
                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="0" width="100%" bgcolor="#dddddd"
                                                    border="2">
                                                    <tr>
                                                        <td bgcolor="#ffffff" background="images/testbg.gif">
                                                            <asp:DataGrid ID="GridDataView" runat="server" AutoGenerateColumns="False" 
                                                                Font-Size="XX-Small" C SkinID="gridview" AllowPaging="True" Width="100%" AllowSorting="True" ForeColor="Black" 
                                                                Font-Bold="False" Font-Italic="False"
                                                                Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                                                                HorizontalAlign="Left" BackColor="White" BorderColor="#DEDFDE" 
                                                                BorderStyle="None" BorderWidth="1px">
                                                                <AlternatingItemStyle BackColor="White" />
                                                                <Columns>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="2%" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="Pk_LTKT_Id" Visible="False">
                                                                        <HeaderStyle Width="0%" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="No.">
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" Wrap="False" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" Wrap="False" HorizontalAlign="Center" Width="3%" ForeColor="White"
                                                                            VerticalAlign="Middle" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="CIFNo" HeaderText="CIF No" SortExpression="CIFNo  desc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="NamaLengkap" HeaderText="Nama" SortExpression="NamaLengkap  desc"></asp:BoundColumn>
                                                                    
                                                                    <asp:BoundColumn DataField="NoRekening" HeaderText="Account Number" SortExpression="NoRekening desc"
                                                                        Visible="False"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="TanggalTransaksi" HeaderText="Tanggal Transaksi" SortExpression="TanggalTransaksi  desc" DataFormatString="{0:dd-MMM-yyyy}">
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" Wrap="False" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="CashInNominal" HeaderText="CashIn" SortExpression="CashInNominal desc" DataFormatString="{0:###,###,###,###,###,##0.00}">
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Center" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="CashOutNominal" DataFormatString="{0:###,###,###,###,###,##0.00}"
                                                                        HeaderText="CashOut" SortExpression="CashOutNominal desc">
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="LastUpdateDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Last Update Date"
                                                                        SortExpression="LastUpdateDate  desc"></asp:BoundColumn>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="Btndetail" runat="server" CommandName="detail">Detail</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="btnEdit" runat="server" CommandName="edit">Edit</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn Visible="False">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="btnDelete" runat="server" CommandName="delete">Delete</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                                <FooterStyle BackColor="#CCCC99" />
                                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                <ItemStyle BackColor="#F7F7DE" />
                                                                <PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" 
                                                                    BackColor="#F7F7DE" Mode="NumericPages">
                                                                </PagerStyle>
                                                                <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #ffffff">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td nowrap style="height: 20px">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td nowrap style="height: 20px; width: 820px;">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td style="width: 87px">
                                                                                    <asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
                                                                                </td>
                                                                                <td style="width: 62px">
                                                                                    Export To:</td>
                                                                                <td style="width: 42px">
                                                                                    <asp:DropDownList runat="server" ID="cboFormatFile" CssClass="combobox">
                                                                                        <asp:ListItem>Excel</asp:ListItem>
                                                                                        <asp:ListItem>Xml</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td style="width: 160px">
                                                                                    <asp:RadioButtonList runat="server" ID="rblData" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Selected="True">Selected</asp:ListItem>
                                                                                        <asp:ListItem>All</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="lnkExportData" runat="server" Text="Export" ajaxcall="none"></asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="100%" style="height: 20px">
                                                                        &nbsp;&nbsp;</td>
                                                                    <td align="right" nowrap style="height: 20px">
                                                                        &nbsp;&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <tr>
                                                    <td bgcolor="#ffffff">
                                                        <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                            bgcolor="#ffffff" border="2">
                                                            <tr class="regtext" align="center" bgcolor="#dddddd">
                                                                <td valign="top" align="left" width="50%" bgcolor="#ffffff" style="height: 19px">
                                                                    Page&nbsp;<asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>&nbsp;of&nbsp;
                                                                    <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label></td>
                                                                <td valign="top" align="right" width="50%" bgcolor="#ffffff" style="height: 19px">
                                                                    Total Records&nbsp;
                                                                    <asp:Label ID="PageTotalRows" runat="server">0</asp:Label></td>
                                                            </tr>
                                                        </table>
                                                        <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                            bgcolor="#ffffff" border="2">
                                                            <tr bgcolor="#ffffff">
                                                                <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                                                                    <hr>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                                                                    Go to page</td>
                                                                <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                                                                    <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                                        <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox>
                                                                    </font>
                                                                </td>
                                                                <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                                                                    <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton" ImageUrl="~/Images/Button/Go.gif">
                                                                    </asp:ImageButton>
                                                                </td>
                                                                <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                                    <img height="5" src="images/first.gif" width="6">
                                                                </td>
                                                                <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                                                    <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                                                        OnCommand="PageNavigate">First</asp:LinkButton>
                                                                </td>
                                                                <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                                                    <img height="5" src="images/prev.gif" width="6"></td>
                                                                <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
                                                                    <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                                                        OnCommand="PageNavigate">Previous</asp:LinkButton>
                                                                </td>
                                                                <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
                                                                    <a class="pageNav" href="#">
                                                                        <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                                                            OnCommand="PageNavigate">Next</asp:LinkButton></a></td>
                                                                <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                                    <img height="5" src="images/next.gif" width="6"></td>
                                                                <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
                                                                    <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                                                        OnCommand="PageNavigate">Last</asp:LinkButton>
                                                                </td>
                                                                <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                                    <img height="5" src="images/last.gif" width="6"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                    </table>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="Images/blank.gif" width="5" height="1" /></td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="images/arrow.gif" width="15" height="15" /></td>
                        <td width="99%" background="Images/button-bground.gif">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
