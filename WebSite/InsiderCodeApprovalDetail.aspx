﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="InsiderCodeApprovalDetail.aspx.vb" Inherits="InsiderCodeApprovalDetailView" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <span defaultbutton="ImgBackAdd">
        <script src="Script/popcalendar.js"></script>
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td>
                </td>
                <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                    <div id="divcontent" class="divcontent">
                        <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td class="divcontentinside" bgcolor="#FFFFFF">
                                    <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%">
                                        <asp:MultiView ID="mtvApproval" runat="server">
                                            <asp:View ID="ViewApprovalDetail" runat="server">
                                                <table style="width: 100%" bgcolor="#dddddd">
                                                    <tr bgcolor="#ffffff">
                                                        <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                            border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                                                            <strong>
                                                                <img src="Images/dot_title.gif" width="17" height="17">
                                                                <asp:Label ID="Label1" runat="server" Text="InsiderCode Approval Detail"></asp:Label>
                                                                <hr />
                                                            </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table style="width: 100%" bgcolor="#dddddd">
                                                    <tr bgcolor="#ffffff">
                                                        <td style="width: 10%; height: 18px">
                                                            <asp:Label ID="Label2" runat="server" Text="Request By"></asp:Label>
                                                        </td>
                                                        <td style="width: 1px">
                                                            :
                                                        </td>
                                                        <td style="width: 90%">
                                                            <asp:Label ID="LblRequestBy" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#ffffff">
                                                        <td style="width: 10%; height: 18px">
                                                            <asp:Label ID="Label3" runat="server" Text="Request Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 1px">
                                                            :
                                                        </td>
                                                        <td style="width: 90%">
                                                            <asp:Label ID="LblRequestDate" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#ffffff">
                                                        <td style="width: 10%; height: 18px">
                                                            <asp:Label ID="LblMode" runat="server" Text="Mode"></asp:Label>
                                                        </td>
                                                        <td style="width: 1px">
                                                            :
                                                        </td>
                                                        <td style="width: 90%">
                                                            <asp:Label ID="LblAction" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#ffffff">
                                                        <td colspan="3">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td style="vertical-align: top;">
                                                                        <asp:Panel ID="PanelOld" runat="server" Width="100%" Visible="False">
                                                                            <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                                                                border="3" height="72" id="TABLE2">
                                                                                <tr class="formText">
                                                                                    <td colspan="4" bgcolor="#ffffff" style="border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; height: 24px; border-bottom-style: none">
                                                                                        <span defaultbutton="ImgBackAdd">
                                                                                            <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="Medium" Text="Old Value"></asp:Label>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="formText">
                                                                                    <td bgcolor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; text-align: center; border-bottom-style: none;">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;" width="23%">
                                                                                        InsiderCode Code
                                                                                    </td>
                                                                                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;">
                                                                                        :
                                                                                    </td>
                                                                                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;" width="80%">
                                                                                        <asp:Label ID="old_txtInsiderCodeID" runat="server" Width="190px"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="formText">
                                                                                    <td bgcolor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; text-align: center; border-bottom-style: none;">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td width="23%" bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;">
                                                                                        InsiderCode Description
                                                                                    </td>
                                                                                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;">
                                                                                        :
                                                                                    </td>
                                                                                    <td width="77%" bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;">
                                                                                        <asp:Label ID="old_txtInsiderCodeName" runat="server" Width="200px"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="formText">
                                                                                    <td bgcolor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; text-align: center; border-bottom-style: none;">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;" width="23%">
                                                                                        Activation
                                                                                    </td>
                                                                                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;">
                                                                                        :
                                                                                    </td>
                                                                                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;" width="77%">
                                                                                        <span defaultbutton="ImgBackAdd">
                                                                                            <asp:Label ID="old_txtActivation" runat="server" Width="200px"></asp:Label>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </td>
                                                                    <td style="vertical-align: top;">
                                                                        <asp:Panel ID="PanelNew" runat="server" Width="100%" Visible="False">
                                                                            <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                                                                border="3" height="72" id="TABLE1">
                                                                                <tr class="formText">
                                                                                    <td colspan="4" bgcolor="#ffffff" style="border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; height: 24px; border-bottom-style: none">
                                                                                        <span defaultbutton="ImgBackAdd">
                                                                                            <asp:Label ID="LblNewValue" runat="server" Font-Bold="True" Font-Size="Medium" Text="New Value"></asp:Label>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="formText">
                                                                                    <td bgcolor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; text-align: center; border-bottom-style: none;">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;" width="23%">
                                                                                        InsiderCode Code
                                                                                    </td>
                                                                                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;">
                                                                                        :
                                                                                    </td>
                                                                                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;" width="80%">
                                                                                        <asp:Label ID="txtInsiderCodeID" runat="server" Width="190px"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="formText">
                                                                                    <td bgcolor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; text-align: center; border-bottom-style: none;">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td width="23%" bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;">
                                                                                        InsiderCode Description
                                                                                    </td>
                                                                                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;">
                                                                                        :
                                                                                    </td>
                                                                                    <td width="77%" bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;">
                                                                                        <asp:Label ID="txtInsiderCodeName" runat="server" Width="200px"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="formText">
                                                                                    <td bgcolor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; text-align: center; border-bottom-style: none;">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;" width="23%">
                                                                                        Activation
                                                                                    </td>
                                                                                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;">
                                                                                        :
                                                                                    </td>
                                                                                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                                                                                        border-left-style: none; border-bottom-style: none;" width="77%">
                                                                                        <span defaultbutton="ImgBackAdd">
                                                                                            <asp:Label ID="txtActivation" runat="server" Width="200px"></asp:Label>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:View>
                                            <asp:View ID="dialog" runat="server">
                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#ffffff"
                                                    border="2" id="TABLE3">
                                                    <tr class="formText" align="center">
                                                        <td>
                                                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr class="formText" align="center">
                                                        <td>
                                                            <asp:ImageButton ID="btnOK" runat="server" ImageUrl="~/Images/button/ok.gif" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:View>
                                        </asp:MultiView>
                                    </ajax:AjaxPanel>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="left" background="Images/button-bground.gif" valign="middle">
                                    <img height="1" src="Images/blank.gif" width="5" />
                                </td>
                                <td align="left" background="Images/button-bground.gif" valign="middle">
                                    <img height="15" src="images/arrow.gif" width="15" />&nbsp;
                                </td>
                                <td background="Images/button-bground.gif">
                                    &nbsp;<asp:ImageButton ID="ImgBtnAccept" runat="server" ImageUrl="~/images/button/accept.gif" />
                                </td>
                                <td background="Images/button-bground.gif">
                                    &nbsp;<asp:ImageButton ID="ImgBtnReject" runat="server" ImageUrl="~/images/button/reject.gif" />
                                </td>
                                <td background="Images/button-bground.gif">
                                    &nbsp;<asp:ImageButton ID="ImgBackAdd" runat="server" ImageUrl="~/images/button/back.gif"
                                        CausesValidation="False" />
                                </td>
                                <td background="Images/button-bground.gif" width="99%">
                                    <img height="1" src="Images/blank.gif" width="1" />
                                </td>
                            </tr>
                        </table>
                        <asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" ValidationGroup="handle"></asp:CustomValidator><asp:CustomValidator
                            ID="CvalPageErr" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
                </td>
            </tr>
        </table>
</asp:Content>

