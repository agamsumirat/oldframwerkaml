
Partial Class CaseManagementIssueEdit
    Inherits Parent
    Private ReadOnly Property PKCaseManagementIssueID() As String
        Get
            Return Request.Params("PKCaseManagementIssueID")
        End Get
    End Property
    Private ReadOnly Property PKCaseManagementID() As String
        Get
            Return Request.Params("PKCaseManagementID")
        End Get
    End Property
    Private _oRowCaseManagementIssue As AMLDAL.CaseManagement.MapCaseManagementIssueRow
    Public ReadOnly Property oRowCaseManagementIssue() As AMLDAL.CaseManagement.MapCaseManagementIssueRow
        Get
            If Not _oRowCaseManagementIssue Is Nothing Then
                Return _oRowCaseManagementIssue
            Else
                Using adapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementIssueTableAdapter
                    Using otable As AMLDAL.CaseManagement.MapCaseManagementIssueDataTable = adapter.GetMapCaseManagementIssueByPK(Me.PKCaseManagementIssueID)
                        If otable.Rows.Count > 0 Then
                            Return otable.Rows(0)
                        Else
                            Return Nothing
                        End If
                    End Using
                End Using
            End If
        End Get
    End Property



    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If Page.IsValid Then
                'save status
                Using adapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementIssueTableAdapter
                    adapter.UpdateMapCaseManagementIssueByPK(CboIssueStatus.SelectedValue, Now, oRowCaseManagementIssue.PK_MapCaseManagementIssueID)
                End Using
                Using CaseManagementAdapter As New AMLDAL.CaseManagementTableAdapters.SelectCaseManagementTableAdapter
                    Select Case CboIssueStatus.SelectedValue
                        Case "1" ' open
                            CaseManagementAdapter.UpdateCaseManagementHasOpenIssueByPK(True, Me.PKCaseManagementID)
                        Case "2" ' closed
                            CaseManagementAdapter.UpdateCaseManagementHasOpenIssueByPK(False, Me.PKCaseManagementID)
                    End Select
                End Using
                Response.Redirect("CaseManagementViewDetail.aspx?PK_CaseManagementID=" & Me.PKCaseManagementID & "&SelectedIndex=2", False)
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub
    Protected Sub LoadData()
        Try
            If Not oRowCaseManagementIssue Is Nothing Then
                If Not oRowCaseManagementIssue.IsIssueTitleNull Then
                    LabelIssueTitle.Text = oRowCaseManagementIssue.IssueTitle
                End If
                If Not oRowCaseManagementIssue.IsDescriptionNull Then
                    LabelDescription.Text = oRowCaseManagementIssue.Description
                End If
                If Not oRowCaseManagementIssue.IsFK_IssueStatusIDNull Then
                    CboIssueStatus.SelectedValue = oRowCaseManagementIssue.FK_IssueStatusID
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
        

    End Sub
    Protected Sub FillIssueStatus()
        Try
            Using adapter As New AMLDAL.CaseManagementTableAdapters.MsIssueStatusTableAdapter

                CboIssueStatus.DataSource = adapter.GetData()
                CboIssueStatus.DataTextField = "IssueStatus"
                CboIssueStatus.DataValueField = "PK_IssueStatusId"
                CboIssueStatus.DataBind()
            End Using
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadData()
                FillIssueStatus()
            End If
            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)


            End Using
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

       

    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("CaseManagementViewDetail.aspx?PK_CaseManagementID=" & Me.PKCaseManagementID & "&SelectedIndex=2", False)
    End Sub
End Class
