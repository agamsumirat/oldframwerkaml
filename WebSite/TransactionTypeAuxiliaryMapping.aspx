<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TransactionTypeAuxiliaryMapping.aspx.vb" Inherits="TransactionTypeAuxiliaryMapping" MasterPageFile="~/MasterPage.master" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    &nbsp;<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/dot_title.gif" />
                <strong>Transaction Type Auxiliary Transaction Code Mapping - View&nbsp; </strong>
            </td>
        </tr>
    </table>
    <TABLE borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
        <TR>
            <TD align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></TD>
				<TD vAlign="top" width="98%" bgColor="#ffffff">
                    <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="1" cellspacing="1"
                        width="100%">
                        <tr style="background-color: #ffffff">
                            <td style="width: 113px">
                                Auxiliary Transaction Code</td>
                            <td style="width: 1%">
                                :</td>
                            <td style="width: 75%">
                                &nbsp;<asp:TextBox ID="TxtAuxTransCode" runat="server" CssClass="searcheditbox"></asp:TextBox></td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td style="width: 113px">
                                Auxiliary Transaction Description</td>
                            <td style="width: 1%">
                                :</td>
                            <td style="width: 75%">
                                &nbsp;<asp:TextBox ID="TxtAuxTransDescription" runat="server" CssClass="searcheditbox"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td style="width: 113px">
                                Transaction Type</td>
                            <td style="width: 1%">
                                :</td>
                            <td style="width: 75%">
                                &nbsp;<asp:TextBox ID="TxtTransType" runat="server" CssClass="searcheditbox"></asp:TextBox>
                            </td>
                        </tr>
                    </table>                				
				</TD>
			</TR>
			<TR>
                <TD align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></TD>
                <td>
                    <asp:ImageButton ID="ImageButtonSearch" runat="server" ImageUrl="~/Images/button/search.gif" />
                </td>
            </TR>
	</TABLE>
		<TABLE borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
			<TR>
				<TD align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></TD>
				<TD vAlign="top" width="98%" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel14" runat="server">
                    <asp:CustomValidator ID="CustomValidator1" runat="server" Display="None"></asp:CustomValidator></ajax:ajaxpanel></TD>
			</TR>
	</TABLE>
<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
  <tr> 
    <td bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel4" runat="server"> 
       <asp:DataGrid ID="GridView" runat="server" AllowCustomPaging="True" AutoGenerateColumns="False"
            BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
            CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="100%" AllowSorting="True">
            <FooterStyle BackColor="#CCCC99" />
            <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <AlternatingItemStyle BackColor="White" />
            <ItemStyle BackColor="#F7F7DE" />
            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
            <Columns>

        <asp:TemplateColumn>
          <ItemTemplate> 
            <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
          </ItemTemplate>
        </asp:TemplateColumn>
            <asp:BoundColumn DataField="TransactionCode" SortExpression="TransactionCode desc" HeaderText="Auxiliary Transaction Code"></asp:BoundColumn>
            <asp:BoundColumn DataField="TransactionDescription" HeaderText="Auxiliary Transaction Description" SortExpression="TransactionDescription desc">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="TransactionTypeID" SortExpression="TransactionTypeID"
                Visible="False"></asp:BoundColumn>
            <asp:BoundColumn DataField="TransactionTypeName" HeaderText="Transaction Type" SortExpression="TransactionTypeName desc">
            </asp:BoundColumn>
            <asp:EditCommandColumn CancelText="Edit" EditText="Edit" UpdateText="Update"></asp:EditCommandColumn>
                <asp:ButtonColumn CommandName="Delete" Text="Delete"></asp:ButtonColumn>
        </Columns>
        <PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages"></PagerStyle>
      </asp:DataGrid>
      </ajax:ajaxpanel> </td>
  </tr>
  <tr> 
    <td style="background-color:#ffffff"><table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td nowrap style="height: 18px"><ajax:ajaxpanel id="AjaxPanel5" runat="server">&nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
			  &nbsp; </ajax:ajaxpanel> </td>
			 <td width="99%" style="height: 18px">&nbsp;&nbsp;
                <asp:LinkButton ID="LinkButtonExportExcel" runat="server" CausesValidation="False">Export to Excel</asp:LinkButton>
            </td>
			<td align="right" nowrap style="height: 18px">&nbsp;&nbsp;</td>
		</tr>
      </table></td>
  </tr>
  <tr> 
    <td bgColor="#ffffff"> <TABLE id="Table3" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
        <TR class="regtext" align="center" bgColor="#dddddd"> 
          <TD vAlign="top" align="left" width="50%" bgColor="#ffffff">Page&nbsp;<ajax:ajaxpanel id="AjaxPanel6" runat="server"> 
            <asp:label id="PageCurrentPage" runat="server" CssClass="regtext">0</asp:label>
            &nbsp;of&nbsp; 
            <asp:label id="PageTotalPages" runat="server" CssClass="regtext">0</asp:label>
            </ajax:ajaxpanel></TD>
          <TD vAlign="top" align="right" width="50%" bgColor="#ffffff">Total Records&nbsp; 
            <ajax:ajaxpanel id="AjaxPanel7" runat="server"> 
            <asp:label id="PageTotalRows" runat="server">0</asp:label>
            </ajax:ajaxpanel></TD>
        </TR>
      </TABLE>
      <TABLE id="Table4" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
        <TR bgColor="#ffffff"> 
          <TD class="regtext" vAlign="middle" align="left" colSpan="11" height="7"> 
            <HR color="#f40101" noShade SIZE="1"> </TD>
        </TR>
        <TR> 
          <TD class="regtext" vAlign="middle" align="left" width="63" bgColor="#ffffff">Go 
            to page</TD>
          <TD class="regtext" vAlign="middle" align="left" width="5" bgColor="#ffffff"><FONT face="Verdana, Arial, Helvetica, sans-serif" size="1"> 
            <ajax:ajaxpanel id="AjaxPanel8" runat="server"> 
            <asp:textbox id="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:textbox>
            </ajax:ajaxpanel> </FONT></TD>
          <TD class="regtext" vAlign="middle" align="left" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel9" runat="server"> 
            <asp:imagebutton id="ImageButtonGo" runat="server" SkinID="GoButton"></asp:imagebutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/first.gif" width="6"> 
          </TD>
          <TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel10" runat="server"> 
            <asp:linkbutton id="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First" OnCommand="PageNavigate">First</asp:linkbutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><IMG height="5" src="images/prev.gif" width="6"></TD>
          <TD class="regtext" vAlign="middle" align="right" width="14" bgColor="#ffffff"> 
            <ajax:ajaxpanel id="AjaxPanel11" runat="server"> 
            <asp:linkbutton id="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev" OnCommand="PageNavigate">Previous</asp:linkbutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="right" width="60" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel12" runat="server"><A class="pageNav" href="#"> 
            <asp:linkbutton id="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next" OnCommand="PageNavigate">Next</asp:linkbutton>
            </A></ajax:ajaxpanel></TD>
          <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/next.gif" width="6"></TD>
          <TD class="regtext" vAlign="middle" align="left" width="25" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel13" runat="server"> 
            <asp:linkbutton id="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last" OnCommand="PageNavigate">Last</asp:linkbutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/last.gif" width="6"></TD>
        </TR>
      </TABLE></td>
  </tr>
    <ajax:ajaxpanel id="AjaxPanel3" runat="server">
        <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
    </ajax:ajaxpanel>  
</table>

</asp:Content>