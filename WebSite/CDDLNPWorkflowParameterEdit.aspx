﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CDDLNPWorkflowParameterEdit.aspx.vb" Inherits="CDDLNPWorkflowParameterEdit" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>
                    <img height="17" src="Images/dot_title.gif" width="17" />
                    <asp:Label ID="lblHeader" runat="server" Text="EDD Group Workflow Parameter - Edit"></asp:Label>&nbsp;
                    <hr />
                </strong>
            </td>
        </tr>
    </table>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none">
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff" width="5px" style="height: 20px">
                &nbsp;</td>
            <td bgcolor="#ffffff" width="20%" style="height: 20px">
                Level</td>
            <td bgcolor="#ffffff" width="5px" style="height: 20px">
                :</td>
            <td bgcolor="#ffffff" colspan="2" width="80%" style="height: 20px">
                <asp:Label ID="lblLevel" runat="server">
                </asp:Label></td>
        </tr>
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff" width="5px" style="height: 20px">
                &nbsp;</td>
            <td bgcolor="#ffffff" width="20%" style="height: 20px">
                Group Workflow</td>
            <td bgcolor="#ffffff" width="5px" style="height: 20px">
                :</td>
            <td bgcolor="#ffffff" colspan="2" width="80%" style="height: 20px">
                <asp:TextBox ID="txtGroupWorkflow" runat="server" CssClass="textbox" Width="200px"></asp:TextBox></td>
        </tr>
        <tr class="formText" bgcolor="#dddddd" height="30">
            <td style="width: 5px">
                <img height="15" src="images/arrow.gif" width="15"></td>
            <td colspan="6" style="height: 9px">
                <table cellspacing="0" cellpadding="3" border="0">
                    <tr>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Save.gif">
                                </asp:ImageButton></ajax:AjaxPanel></td>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                <asp:ImageButton ID="ImageCancel" runat="server" ImageUrl="~/Images/Button/Cancel.gif">
                                </asp:ImageButton></ajax:AjaxPanel></td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
        </tr>
    </table>
</asp:Content>
