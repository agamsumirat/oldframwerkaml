Imports CDDLNPNettier.Entities
Imports CDDLNPNettier.Data
Partial Class UploadUSRCustody_Approval
    Inherits Parent
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("UploadURSCustody_Approval.Selected") Is Nothing, New ArrayList, Session("UploadURSCustody_Approval.Selected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("UploadURSCustody_Approval.Selected") = value
        End Set
    End Property
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("UploadURSCustody_Approval.ValueSearch") Is Nothing, "", Session("UploadURSCustody_Approval.ValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("UploadURSCustody_Approval.ValueSearch") = Value
        End Set
    End Property

    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("UploadURSCustody_Approval.FieldSearch") Is Nothing, "", Session("UploadURSCustody_Approval.FieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("UploadURSCustody_Approval.FieldSearch") = Value
        End Set
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("UploadURSCustody_Approval.CurrentPage") Is Nothing, 0, Session("UploadURSCustody_Approval.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("UploadURSCustody_Approval.CurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("UploadURSCustody_Approval.RowTotal") Is Nothing, 0, Session("UploadURSCustody_Approval.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("UploadURSCustody_Approval.RowTotal") = Value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("UploadURSCustody_Approval.Sort") Is Nothing, "PK_AccountUSRCustody_ApprovalID  asc", Session("UploadURSCustody_Approval.Sort"))
        End Get
        Set(ByVal Value As String)
            Session("UploadURSCustody_Approval.Sort") = Value
        End Set
    End Property

    Sub ClearSession()
        SetnGetSelectedItem = Nothing
        SetnGetValueSearch = Nothing
        SetnGetFieldSearch = Nothing
        SetnGetCurrentPage = Nothing
        SetnGetRowTotal = Nothing
        SetnGetSort = Nothing
    End Sub
    Private Sub FillSearch()
        Me.ComboSearch.Items.Add(New ListItem("...", ""))
        Me.ComboSearch.Items.Add(New ListItem("Created By", "UserID Like '%-=Search=-%'"))
        Me.ComboSearch.Items.Add(New ListItem("Entry Date", "CreatedDate BETWEEN '-=Search=- 00:00' AND '-=Search=- 23:59'"))

    End Sub
    Public Property SetnGetBindTable() As VList(Of vw_AccountUSRCustody_Approval)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If

            If ComboSearch.SelectedValue <> "" Then
                If ComboSearch.SelectedItem.Text = "Created By" Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = Me.ComboSearch.SelectedValue.Replace("%-=Search=-%", TextSearch.Text)
                End If

                If ComboSearch.SelectedItem.Text = "Entry Date" Then
                    If TextSearch.Text <> "" Then
                        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TextSearch.Text.Trim) Then
                            ReDim Preserve strWhereClause(strWhereClause.Length)
                            strWhereClause(strWhereClause.Length - 1) = Me.ComboSearch.SelectedValue.Replace("-=Search=-", Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TextSearch.Text.Trim).ToString("yyyy-MM-dd"))

                        Else

                            Throw New Exception("Entry Date Must dd-MMM-yyyy")
                        End If
                    Else
                        Throw New Exception("Please Fill Entry Date.")
                    End If
                End If


            End If


            strAllWhereClause = String.Join(" and ", strWhereClause)
            If strAllWhereClause.Trim.Length > 0 Then
                strAllWhereClause += " and UserID <> '" & Sahassa.AML.Commonly.SessionUserId & "' AND UserID IN (SELECT u.UserID FROM [User] u WHERE u.UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            Else
                strAllWhereClause += " UserID <> '" & Sahassa.AML.Commonly.SessionUserId & "' AND UserID IN (SELECT u.UserID FROM [User] u WHERE u.UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            End If
            Session("UploadURSCustody_Approval.Table") = DataRepository.vw_AccountUSRCustody_ApprovalProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)

            Return CType(Session("UploadURSCustody_Approval.Table"), VList(Of vw_AccountUSRCustody_Approval))
        End Get
        Set(ByVal value As VList(Of vw_AccountUSRCustody_Approval))
            Session("UploadURSCustody_Approval.Table") = value
        End Set
    End Property
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim UserId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(UserId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(UserId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(UserId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                FillSearch()
                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check(this, 2, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "');")


                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Sub BindGrid()

        Me.GridMSUserView.DataSource = SetnGetBindTable

        'Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridMSUserView.DataBind()
    End Sub
    Sub SetinfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridMSUserView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim pkid As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(pkid) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            Else
        '                ArrTarget.Remove(pkid)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetinfoNavigate()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message

        End Try
    End Sub

    Protected Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Try
            Dim pk As Integer = e.Item.Cells(1).Text

            Response.Redirect("UploadURSCustodyApprovalDetail.aspx?PK_AccountUSRCustody_ApprovalID=" & pk, False)

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim pk As String = e.Item.Cells(1).Text
                Dim objcheckbox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                objcheckbox.Checked = Me.SetnGetSelectedItem.Contains(pk)

                'Dim IntUserId As Integer = CInt(e.Item.Cells(2).Text)
                'Using ObjUser As User = DataRepository.UserProvider.GetBypkUserID(IntUserId)
                '    If Not ObjUser Is Nothing Then
                '        e.Item.Cells(2).Text = ObjUser.UserID
                '    End If
                'End Using

                'If e.Item.Cells(4).Text = "2" Then
                '    e.Item.Cells(4).Text = "Edit"
                'End If

            End If

        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls
    Private Sub BindSelected()
        Dim Rows As New ArrayList

        For Each IdPk As Int32 In Me.SetnGetSelectedItem

            Dim objtlist As VList(Of vw_AccountUSRCustody_Approval) = DataRepository.vw_AccountUSRCustody_ApprovalProvider.GetPaged(vw_AccountUSRCustody_ApprovalColumn.PK_AccountUSRCustody_ApprovalID.ToString & "=" & IdPk, "", 0, Integer.MaxValue, 0)

            If objtlist.Count > 0 Then
                Rows.Add(objtlist(0))
            End If
            'Dim rowData() As AMLDAL.AMLDataSet.User_PendingApprovalRow = Me.SetnGetBindTable.Select("User_PendingApprovalID = " & IdPk)
            'If rowData.Length > 0 Then
            '    Rows.Add(rowData(0))
            'End If
        Next

        Me.GridMSUserView.DataSource = Rows
        Me.GridMSUserView.AllowPaging = False
        Me.GridMSUserView.DataBind()

        'Sembunyikan kolom ke 0,1  & 6 agar tidak ikut diekspor ke excel
        Me.GridMSUserView.Columns(0).Visible = False
        Me.GridMSUserView.Columns(1).Visible = False
        Me.GridMSUserView.Columns(4).Visible = False
    End Sub

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=urscustody_approval.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
            ElseIf Me.ComboSearch.SelectedIndex = 2 Then
                If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TextSearch.Text.Trim) Then
                    Throw New Exception("Birth Date must dd-MMM-yyyy")
                End If
            End If
            Me.SetnGetValueSearch = Me.TextSearch.Text
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

End Class
