<%@ Page Language="VB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Graphical Transaction Analysis Menu</title>
</head>
<body bgcolor="#33cc66">
    <table cellpadding="1" cellspacing="1" border="0">
        <tr>
            <td align=right style="height: 14px" bgcolor="#006600" ><a href="Javascript:parent.HideMenu();" style="font-size: 12px; color: white; font-family: Verdana; font-weight: bold;">Close [X]</a></td>
        </tr>
        <tr>
            <% If (Not Request.Params.Item("StartDate") Is Nothing) And (Not Request.Params.Item("EndDate") Is Nothing) Then%>
            <td style="height: 14px" bgcolor="#ffffcc"><a href="Javascript:parent.Expand();" id="aExpand" style="font-weight: bold; font-size: 12px; color: black; font-family: Verdana">Expand (<%=Request.Params.Item("StartDate")%> - <%=Request.Params.Item("EndDate")%>)</a></td>
            <% else %>
            <td style="height: 14px" bgcolor="#ffffcc"><a href="Javascript:parent.Expand();" id="a1" style="font-weight: bold; font-size: 12px; color: black; font-family: Verdana">Expand</a></td>
            <% end if %>
        </tr>
        <tr>
            <td bgcolor="#ffffff"><a href="Javascript:parent.ShowAccontInfo();" style="font-weight: bold; font-size: 12px; color: black; font-family: Verdana">Show Account Info</a></td>
        </tr> 
  </table>
</body>
</html>
