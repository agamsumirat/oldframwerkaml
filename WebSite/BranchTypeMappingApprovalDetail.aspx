<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="BranchTypeMappingApprovalDetail.aspx.vb" Inherits="BranchTypeMappingApprovalDetail" title="Branch Type Mapping - Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <table style="width: 100%">
        <tr>
            <td>
            </td>
            <td colspan="2">
                <asp:Panel ID="PanelAdd" runat="server" Height="100%" Width="100%">
                    <table style="width: 100%">
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                Branch Name</td>
                            <td width="1">
                                :</td>
                            <td>
                                <asp:Label ID="LabelBranchNameAdd" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                Branch Type</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelBranchTypeAdd" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="PanelEditNew" runat="server" Height="100%" Width="100%" GroupingText="NEW VALUE">
                    <table style="width: 100%">
                        <tr>
                            <td nowrap="noWrap" width="1%">
                                Branch Name</td>
                            <td width="1">
                                :</td>
                            <td>
                                <asp:Label ID="LabelBranchNameEditNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="noWrap" width="1%">
                                Branch Type</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelBranchTypeEditNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="noWrap" width="1%">
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
                <asp:Panel ID="PanelEditOld" runat="server" Height="100%" Width="100%" GroupingText="OLD VALUE">
                    <table style="width: 100%">
                        <tr>
                            <td nowrap="noWrap" width="1%">
                                Branch Name</td>
                            <td width="1">
                                :</td>
                            <td>
                                <asp:Label ID="LabelBranchNameEditOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="noWrap" width="1%">
                                Branch Type</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelBranchTypeEditOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="noWrap" width="1%">
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td colspan="2">
                <asp:Panel ID="PanelDelete" runat="server" Height="100%" Width="100%">
                    <table style="width: 100%">
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                Branch Name</td>
                            <td width="1">
                                :</td>
                            <td>
                                <asp:Label ID="LabelBranchnameDelete" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                Branch Type</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelBranchTypeDelete" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
         <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="True" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="none"></asp:CustomValidator></TD>
	    </TR>
    </table>
  
</asp:Content>
