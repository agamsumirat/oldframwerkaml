<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ResetPassword.aspx.vb" Inherits="ResetPassword" title="Reset Password" %>

<%@ Register Src="webcontrol/PopUpUser.ascx" TagName="PopUpUser" TagPrefix="uc1" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<div class="buttonwrapper">
<ajax:AjaxPanel ID="AjaxPanel2" runat="server" >
    <uc1:PopUpUser ID="PopUpUser1" runat="server" />
</ajax:AjaxPanel> 
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="231" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4">
                <strong><span style="font-size: 18px">Reset Password</span></strong><br />
                <hr />
            </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4"><span style="color: #ff0000">* Required</span></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
                <br />
                <br />
                <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ErrorMessage="Password is required" Display="Dynamic"
					ControlToValidate="TextPassword">*</asp:requiredfieldvalidator><br />
                <br />
                <asp:comparevalidator id="CompareValidator1" runat="server" ErrorMessage="Your entry in the Confirm Password textbox must be the same as the entry in the Password textbox " Display="Dynamic"
					ControlToValidate="TextPassword" ControlToCompare="TextBoxRetype">*</asp:comparevalidator></td>
            <td bgcolor="#ffffff" colspan="0"><ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="625px">
                <table style="width: 582px;" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 20%;height: 30px ">
                    User ID<asp:HiddenField ID="HUserID" runat="server" />
                        </td>
                          <td style="width: 15;height: 30px" align="center">
                            :
                        </td>
                        <td style="width: 80%; height: 30px">
                            &nbsp;&nbsp;
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="width: auto; height: 17px" valign="top">
                    <asp:Label ID="LblUserID" runat="server">Click Browse Button To Choose UserID</asp:Label></td>
                                    <td style="height: 17px" valign="top">
                                        <asp:LinkButton ID="btnBrowse" runat="server" CssClass="ovalbutton" CausesValidation="False"><span>Browse</span></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%; height: 30px">
                User Name</td>
                        <td style="width: 15; height: 30px" align="center">
                            :
                        </td>
                        <td style="width: 80%; height: 30px">
                        &nbsp;<asp:Label ID="LabelUserName" runat="server" Width="174px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 20%; height: 30px">
                            Password</td>
                        <td style="width: 15; height: 30px" align="center">
                            :
                        </td>
                        <td style="width: 80%; height: 30px">
                            &nbsp;<asp:textbox id="TextPassword" runat="server" CssClass="textBox" MaxLength="50" TextMode="Password"></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong></td>
                    </tr>
                    <tr>
                        <td style="width: 20%; height: 30px">
                Confirm Password</td>
                        <td style="width: 15; height: 30px" align="center">
                            :
                        </td>
                        <td style="width: 80%; height: 30px">
                            &nbsp;<asp:textbox id="TextBoxRetype" runat="server" CssClass="textBox" MaxLength="50" TextMode="Password"></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong></td>
                    </tr>
                </table><strong><span style="color: #ff0000">             
                </span></strong>              
            </ajax:AjaxPanel>
            </td>
        </tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="saveButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>              
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	
	</div> 
	
</asp:Content>

