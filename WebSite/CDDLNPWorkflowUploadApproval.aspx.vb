Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports System.Collections.Generic
Imports System.IO

Partial Class CDDLNPWorkflowUploadApproval
    Inherits Parent
    Private BindGridFromExcel As Boolean = False

#Region " Property "
    Private Property SetnGetFK_MsMode_Id() As Int32
        Get
            Return CType(IIf(Session("CDDLNPWorkflowUploadApproval.FK_MsMode_Id") Is Nothing, 0, Session("CDDLNPWorkflowUploadApproval.FK_MsMode_Id")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPWorkflowUploadApproval.FK_MsMode_Id") = Value
        End Set
    End Property
    Private Property SetnGetRequestedBy() As String
        Get
            Return CType(IIf(Session("CDDLNPWorkflowUploadApproval.RequestedBy") Is Nothing, String.Empty, Session("CDDLNPWorkflowUploadApproval.RequestedBy")), String)
        End Get
        Set(ByVal Value As String)
            Session("CDDLNPWorkflowUploadApproval.RequestedBy") = Value
        End Set
    End Property
    Private Property SetnGetRequestedDateFrom() As String
        Get
            Return CType(IIf(Session("CDDLNPWorkflowUploadApproval.RequestedDateFrom") Is Nothing, String.Empty, Session("CDDLNPWorkflowUploadApproval.RequestedDateFrom")), String)
        End Get
        Set(ByVal Value As String)
            Session("CDDLNPWorkflowUploadApproval.RequestedDateFrom") = Value
        End Set
    End Property
    Private Property SetnGetRequestedDateTo() As String
        Get
            Return CType(IIf(Session("CDDLNPWorkflowUploadApproval.RequestedDateTo") Is Nothing, String.Empty, Session("CDDLNPWorkflowUploadApproval.RequestedDateTo")), String)
        End Get
        Set(ByVal Value As String)
            Session("CDDLNPWorkflowUploadApproval.RequestedDateTo") = Value
        End Set
    End Property

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("CDDLNPWorkflowUploadApproval.Selected") Is Nothing, New ArrayList, Session("CDDLNPWorkflowUploadApproval.Selected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CDDLNPWorkflowUploadApproval.Selected") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CDDLNPWorkflowUploadApproval.Sort") Is Nothing, "PK_CDDLNP_WorkflowUpload_Approval_Id  desc", Session("CDDLNPWorkflowUploadApproval.Sort"))
        End Get
        Set(ByVal Value As String)
            Session("CDDLNPWorkflowUploadApproval.Sort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CDDLNPWorkflowUploadApproval.CurrentPage") Is Nothing, 0, Session("CDDLNPWorkflowUploadApproval.CurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPWorkflowUploadApproval.CurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CDDLNPWorkflowUploadApproval.RowTotal") Is Nothing, 0, Session("CDDLNPWorkflowUploadApproval.RowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPWorkflowUploadApproval.RowTotal") = Value
        End Set
    End Property
    Private Function SetnGetBindTable() As VList(Of vw_CDDLNP_WorkflowUpload_Approval)
        Return DataRepository.vw_CDDLNP_WorkflowUpload_ApprovalProvider.GetPaged(SearchFilter, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, SetnGetRowTotal)
    End Function
#End Region

    Private Sub ClearThisPageSessions()
        Me.SetnGetFK_MsMode_Id = 0
        Me.SetnGetRequestedBy = String.Empty
        Me.SetnGetRequestedDateFrom = String.Empty
        Me.SetnGetRequestedDateTo = String.Empty
        Me.SetnGetSelectedItem = Nothing
        Me.SetnGetSort = Nothing
        Me.SetnGetCurrentPage = Nothing
        Me.SetnGetRowTotal = Nothing
    End Sub

    Private Sub BindComboBox()
        Try
            ddlMode.Items.Clear()
            ddlMode.Items.Add(New ListItem("[All]", 0))
            For Each objMode As SahassaNettier.Entities.Mode In SahassaNettier.Data.DataRepository.ModeProvider.GetAll
                ddlMode.Items.Add(New ListItem(objMode.Nama, objMode.MsModeID))
            Next
        Catch
            Throw
        End Try
    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknown Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridViewCDDLNP.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim PkId As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(PkId) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(PkId) Then
        '                    ArrTarget.Add(PkId)
        '                End If
        '            Else
        '                ArrTarget.Remove(PkId)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(PkId) Then
        '                    ArrTarget.Add(PkId)
        '                End If
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridViewCDDLNP.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
        Me.BindGrid()
    End Sub

    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim totalrow As Int16 = 0
        For Each gridRow As DataGridItem In Me.GridViewCDDLNP.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                totalrow = CType(totalrow + 1, Int16)
            End If
        Next
        If i = totalrow Then
            Me.CheckBoxSelectAll.Checked = True
        Else
            Me.CheckBoxSelectAll.Checked = False
        End If
    End Sub

    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridViewCDDLNP.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim PkId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PkId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PkId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PkId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    Protected Sub GridViewCDDLNP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewCDDLNP.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow))
                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewCDDLNP_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridViewCDDLNP.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewCDDLNP_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewCDDLNP.ItemCommand
        Select Case e.CommandName.ToLower
            Case "detail"
                Response.Redirect("CDDLNPWorkflowUploadApprovalDetail.aspx?ID=" & e.Item.Cells(1).Text)

        End Select
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be less than or equal to the total page count.")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageClear.Click
        Try
            'Me.ClearThisPageSessions()
            Me.SetnGetFK_MsMode_Id = 0
            Me.SetnGetRequestedBy = String.Empty
            Me.SetnGetRequestedDateFrom = String.Empty
            Me.SetnGetRequestedDateTo = String.Empty            
            Me.SetnGetSort = Nothing
            Me.SetnGetCurrentPage = Nothing
            Me.SetnGetRowTotal = Nothing
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Me.SetnGetFK_MsMode_Id = ddlMode.SelectedValue
        Me.SetnGetRequestedBy = txtRequestedBy.Text
        Me.SetnGetRequestedDateFrom = txtRequestedDateFrom.Text
        Me.SetnGetRequestedDateTo = txtRequestedDateTo.Text
        Me.SetnGetCurrentPage = 0
        Me.CollectSelected()
    End Sub

    Private Sub BindGrid()
        Me.SettingControl()

        Me.GridViewCDDLNP.DataSource = Me.SetnGetBindTable
        Me.GridViewCDDLNP.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridViewCDDLNP.DataBind()
    End Sub

    Private Function SearchFilter() As String
        Dim strWhereClause(-1) As String

        If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
            Me.SetnGetCurrentPage = Me.GetPageTotal - 1
        ElseIf Me.SetnGetCurrentPage = -1 Then
            Me.SetnGetCurrentPage = 0
        End If

        Try

            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = "RequestedBy <> '" & Sahassa.AML.Commonly.SessionPkUserId & "' AND RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Sahassa.AML.Commonly.SessionUserId & "'))"

            If Me.SetnGetFK_MsMode_Id > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "FK_MsMode_Id = " & SetnGetFK_MsMode_Id
            End If

            If Me.SetnGetRequestedBy.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "UserName LIKE '%" & SetnGetRequestedBy & "%'"
            End If

            If Me.SetnGetRequestedDateFrom.Length > 0 AndAlso Me.SetnGetRequestedDateTo.Length > 0 Then
                If IsDate(SetnGetRequestedDateFrom) AndAlso IsDate(SetnGetRequestedDateTo) Then
                    If SetnGetRequestedDateFrom <= SetnGetRequestedDateTo Then
                        ReDim Preserve strWhereClause(strWhereClause.Length)
                        strWhereClause(strWhereClause.Length - 1) = "(CONVERT(DATETIME,CONVERT(VARCHAR(20),RequestedDate,112),112)  BETWEEN '" & SetnGetRequestedDateFrom & "' AND '" & SetnGetRequestedDateTo & "')"
                    Else
                        Throw New Exception("Requested date searching is not valid")
                    End If
                Else
                    Throw New Exception("Requested date format is not valid")
                End If
            End If

            Return String.Join(" AND ", strWhereClause)
        Catch
            Throw
        End Try
    End Function

    Private Sub SettingControl()
        txtRequestedBy.Text = Me.SetnGetRequestedBy
        txtRequestedDateFrom.Text = Me.SetnGetRequestedDateFrom
        txtRequestedDateTo.Text = Me.SetnGetRequestedDateTo
        ddlMode.SelectedValue = Me.SetnGetFK_MsMode_Id
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                Me.ClearThisPageSessions()
                PopRequestedDateFrom.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtRequestedDateFrom.ClientID & "'), 'dd-mmm-yyyy')")
                PopRequestedDateTo.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtRequestedDateTo.ClientID & "'), 'dd-mmm-yyyy')")

                Me.GridViewCDDLNP.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                BindComboBox()
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
            Me.SetCheckedAll()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub BindSelected()
        Try
            Dim Rows As New ArrayList

            Dim SbPk As New StringBuilder
            SbPk.Append("0, ")
            For Each IdPk As Int64 In Me.SetnGetSelectedItem
                SbPk.Append(IdPk.ToString & ", ")
            Next

            Me.GridViewCDDLNP.DataSource = DataRepository.vw_CDDLNP_WorkflowUpload_ApprovalProvider.GetPaged("PK_CDDLNP_WorkflowUpload_Approval_Id in (" & SbPk.ToString.Substring(0, SbPk.Length - 2) & ") ", Me.SetnGetSort, 0, Integer.MaxValue, 0)
            Me.GridViewCDDLNP.AllowPaging = False
            Me.GridViewCDDLNP.DataBind()

            Me.GridViewCDDLNP.Columns(0).Visible = False
            Me.GridViewCDDLNP.Columns(1).Visible = False
            Me.GridViewCDDLNP.Columns(GridViewCDDLNP.Columns.Count - 1).Visible = False
        Catch
            Throw
        End Try
    End Sub

    Private Sub BindSelectedAll()
        Try
            Me.GridViewCDDLNP.DataSource = DataRepository.vw_CDDLNP_WorkflowUpload_ApprovalProvider.GetPaged(SearchFilter, Me.SetnGetSort, 0, Int32.MaxValue, 0)

            Me.GridViewCDDLNP.AllowPaging = False
            Me.GridViewCDDLNP.DataBind()

            Me.GridViewCDDLNP.Columns(0).Visible = False
            Me.GridViewCDDLNP.Columns(1).Visible = False
            Me.GridViewCDDLNP.Columns(GridViewCDDLNP.Columns.Count - 1).Visible = False
        Catch
            Throw
        End Try
    End Sub

    Protected Sub lnkExportData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportData.Click
        Try
            BindGridFromExcel = True
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CDDLNPWorkflowUploadApproval_Selected.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(Me.GridViewCDDLNP)
            Me.GridViewCDDLNP.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub lnkExportAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAll.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelectedAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CDDLNPWorkflowUploadApproval_All.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(Me.GridViewCDDLNP)
            Me.GridViewCDDLNP.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class