Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class GroupDelete
    Inherits Parent

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetGroupId() As String
        Get
            Return Me.Request.Params("GroupID")
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "GroupView.aspx"

        Me.Response.Redirect("GroupView.aspx", False)
    End Sub
#Region "Delete Group"
    ''' <summary>
    ''' insert by su
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DeleteGroupBySU()
        Try
            Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                AccessGroup.DeleteGroup(CInt(Me.GetGroupId))
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' audit trail
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertAuditTrail()
        Try
            Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                Using dt As AMLDAL.AMLDataSet.GroupDataTable = AccessGroup.GetDataByGroupID(CInt(Me.GetGroupId))
                    Dim rowData As AMLDAL.AMLDataSet.GroupRow = dt.Rows(0)
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupID", "Delete", "", rowData.GroupID, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupName", "Delete", "", Me.TextGroupName.Text, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupDescription", "Delete", "", Me.TextGroupDescription.Text, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupCreatedDate", "Delete", rowData.GroupCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), rowData.GroupCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                    End Using
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageUpdate.Click
        Dim oSQLTrans As SQLTransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                'Buat variabel untuk menampung nilai-nilai baru
                Dim GroupID As String = Me.LabelGroupID.Text
                Dim GroupName As String = Me.TextGroupName.Text

                'Periksa apakah GroupName tsb sdh ada dalam tabel Group atau belum
                Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                    Dim counter As Int32 = AccessGroup.CountMatchingGroup(GroupName)

                    'Counter > 0 berarti GroupName tersebut masih ada dalam tabel Group dan bisa didelete
                    If counter > 0 Then
                        'Periksa apakah GroupName tsb dalam status pending approval atau tidak
                        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Group_ApprovalTableAdapter
                            oSQLTrans = BeginTransaction(AccessPending, IsolationLevel.ReadUncommitted)
                            counter = AccessPending.CountGroupApproval(GroupID)

                            'Counter = 0 berarti GroupName tersebut tidak dalam status pending approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertAuditTrail()
                                    Me.DeleteGroupBySU()
                                    Me.LblSuccess.Text = "Success to Delete Group."
                                    Me.LblSuccess.Visible = True
                                Else
                                    Dim GroupDescription As String = Me.TextGroupDescription.Text

                                    'Buat variabel untuk menampung nilai-nilai lama
                                    Dim GroupID_Old As Int32 = ViewState("GroupID_Old")
                                    Dim GroupName_Old As String = ViewState("GroupName_Old")
                                    Dim GroupDescription_Old As String = ViewState("GroupTypeDescription_Old")
                                    Dim GroupCreatedDate_Old As DateTime = ViewState("GroupCreatedDate_Old")
                                    Dim isCreatedBySU_Old As Boolean = ViewState("isCreatedBySU_Old")

                                    'variabel untuk menampung nilai identity dari tabel GroupPendingApprovalID
                                    Dim GroupPendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope()
                                    Using AccessGroupPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Group_PendingApprovalTableAdapter
                                        SetTransaction(AccessGroupPendingApproval, oSQLTrans)
                                        'Tambahkan ke dalam tabel Group_PendingApproval dengan ModeID = 3 (Delete) 
                                        GroupPendingApprovalID = AccessGroupPendingApproval.InsertGroup_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Group Delete", 3)

                                        'Tambahkan ke dalam tabel Group_Approval dengan ModeID = 3 (Delete) 
                                        AccessPending.Insert(GroupPendingApprovalID, 3, GroupID, GroupName, GroupDescription, Now, GroupID_Old, GroupName_Old, GroupDescription_Old, GroupCreatedDate_Old, isCreatedBySU_Old, isCreatedBySU_Old)

                                        oSQLTrans.Commit()
                                        'TransScope.Complete()
                                    End Using
                                    'End Using

                                    Dim MessagePendingID As Integer = 8203 'MessagePendingID 8203 = Group Delete 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & GroupName

                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & GroupName, False)
                                End If
                            Else
                                Throw New Exception("Cannot delete the following Group: '" & GroupName & "' because it is currently waiting for approval.")
                            End If
                        End Using
                    Else  'Counter = 0 berarti GroupName tersebut tidak ada dalam tabel Group
                        Throw New Exception("Cannot delete the following Group: '" & GroupName & "' because that Group Name does not exist in the database anymore.")
                    End If
                End Using
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
            Using TableGroup As AMLDAL.AMLDataSet.GroupDataTable = AccessGroup.GetDataByGroupID(Me.GetGroupId)
                If TableGroup.Rows.Count > 0 Then
                    Dim TableRowGroup As AMLDAL.AMLDataSet.GroupRow = TableGroup.Rows(0)

                    ViewState("GroupID_Old") = TableRowGroup.GroupID
                    Me.LabelGroupID.Text = ViewState("GroupID_Old")

                    ViewState("GroupName_Old") = TableRowGroup.GroupName
                    Me.TextGroupName.Text = ViewState("GroupName_Old")

                    ViewState("GroupTypeDescription_Old") = TableRowGroup.GroupDescription
                    Me.TextGroupDescription.Text = ViewState("GroupTypeDescription_Old")

                    ViewState("GroupCreatedDate_Old") = TableRowGroup.GroupCreatedDate
                    ViewState("isCreatedBySU_Old") = TableRowGroup.isCreatedBySU
                End If
            End Using
        End Using
    End Sub


    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillEditData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '    Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class