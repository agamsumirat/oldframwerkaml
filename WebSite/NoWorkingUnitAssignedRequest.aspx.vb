Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration

Partial Class NoWorkingUnitAssignedRequest
    Inherits System.Web.UI.Page

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPkUserID() As String
        Get
            Return Me.Request.Params("UId")
        End Get
    End Property


    Private Sub LogError(ByVal ex As Exception)
        Dim Logger As log4net.ILog = log4net.LogManager.GetLogger("AMLError")
        Logger.Error("Exception has occured", ex)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.LabelUserID.Text = Me.GetPkUserID

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "SignOut.aspx"
        Me.Response.Redirect("SignOut.aspx", False)
    End Sub
End Class
