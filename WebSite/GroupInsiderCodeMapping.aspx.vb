Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Public Class GroupInsiderCodeMapping
    Inherits Parent
#Region " Property "
    ''' <summary>
    ''' get focus id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.DropdownlistGroup.ClientID
        End Get
    End Property

    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(ViewState("MsGroupViewSelected") Is Nothing, New ArrayList, ViewState("MsGroupViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            ViewState("MsGroupViewSelected") = value
        End Set
    End Property

    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(ViewState("MsGroupViewData") Is Nothing, New AMLDAL.AMLDataSet.InsiderCodeDataTable, ViewState("MsGroupViewData"))
        End Get
        Set(ByVal value As Data.DataTable)
            ViewState("MsGroupViewData") = value
        End Set
    End Property

    Private Property OldInsiderCodes() As ArrayList
        Get
            Return IIf(ViewState("OldInsiderCodes") Is Nothing, New ArrayList, ViewState("OldInsiderCodes"))
        End Get
        Set(ByVal value As ArrayList)
            ViewState("OldInsiderCodes") = value
        End Set
    End Property

#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get data Potential Customer Verification Parameter
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	03/07/2006	Created
    '''     [Hendry] 15/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillGroupData()
        Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
            Me.DropdownlistGroup.DataSource = AccessGroup.GetData
            Me.DropdownlistGroup.DataTextField = "GroupName"
            Me.DropdownlistGroup.DataValueField = "GroupID"
            Me.DropdownlistGroup.DataBind()
        End Using

        Me.DropdownlistGroup.Items.Add("-- Choose a Group Name here --")
        Me.DropdownlistGroup.SelectedIndex = Me.DropdownlistGroup.Items.Count - 1
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cancel event handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) 'Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "Default.aspx"

        Me.Response.Redirect("Default.aspx", False)
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillGroupData()
                Using AccessInsiderCode As New AMLDAL.AMLDataSetTableAdapters.InsiderCodeTableAdapter
                    Dim objTable As Data.DataTable = AccessInsiderCode.GetInsiderCodeMappingDataView
                    Me.SetnGetBindTable = objTable

                    Dim ArrTarget As New ArrayList

                    For Each row As AMLDAL.AMLDataSet.InsiderCodeRow In objTable.Rows
                        ArrTarget.Add(row.InsiderCodeId)
                    Next

                    OldInsiderCodes = ArrTarget
                End Using

                Me.BindGrid()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            Me.GridMSUserView.DataSource = Me.SetnGetBindTable
            'Me.SetnGetRowTotal = Rows.Length
            'Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
            'Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridMSUserView.DataBind()
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' PickInsiderCode
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Hendry]	06/06/2007	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub PickInsiderCode()
        Try
            Using AccessGroupInsiderCode As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMappingTableAdapter
                Using GroupInsiderCodeTable As AMLDAL.AMLDataSet.GroupInsiderCodeMappingDataTable = AccessGroupInsiderCode.GetDataByGroupId(Me.DropdownlistGroup.SelectedValue)
                    'Periksa apakah sudah ada Group Insider Code dalam table
                    If GroupInsiderCodeTable.Rows.Count > 0 Then
                        Me.OldInsiderCodes = Nothing

                        Dim ArrTarget As New ArrayList

                        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
                            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                                Dim InsiderCodeId As String = gridRow.Cells(1).Text

                                'Aktifkan Checkbox tsb apabila ditemukan pasangan antara GroupId yg dipilih dan InsiderCodeId tsb
                                Dim count As Int32 = AccessGroupInsiderCode.CheckGroupInsiderCodeMapping(Me.DropdownlistGroup.SelectedValue, InsiderCodeId)

                                Dim chkbox1 As CheckBox = gridRow.Cells(0).FindControl("CheckBoxGroupInsiderCodeMapping")

                                If count > 0 Then
                                    chkbox1.Checked = True
                                    ArrTarget.Add(InsiderCodeId)
                                Else
                                    chkbox1.Checked = False
                                End If
                            End If
                        Next

                        'Set Old Insider Codes
                        Me.OldInsiderCodes = ArrTarget

                    Else 'Bila tidak ada maka Hilangkan tanda cek pd semua checkbox
                        UncheckAllCheckBox()
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub

    Private Sub UncheckAllCheckBox()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim InsiderCodeId As String = gridRow.Cells(1).Text

                Dim chkbox1 As CheckBox = gridRow.Cells(0).FindControl("CheckBoxGroupInsiderCodeMapping")

                chkbox1.Checked = False
            End If
        Next

        Me.CheckBoxSelectAll.Checked = False
        Me.OldInsiderCodes = Nothing
    End Sub


    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkbox1 As CheckBox = gridRow.Cells(0).FindControl("CheckBoxGroupInsiderCodeMapping")
                chkbox1.Checked = CheckBoxSelectAll.Checked
            End If
        Next
    End Sub

    Protected Sub DropdownlistGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropdownlistGroup.SelectedIndexChanged
        Try
            If Me.DropdownlistGroup.SelectedIndex <> (Me.DropdownlistGroup.Items.Count - 1) Then
                Me.PickInsiderCode()
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Function CompareArrayList(ByRef ArrayList1 As ArrayList, ByRef ArrayList2 As ArrayList) As Boolean
        Dim i As Integer

        If ArrayList1.Count = ArrayList2.Count Then
            For i = 0 To ArrayList1.Count - 1
                If ArrayList1(i) <> ArrayList2(i) Then
                    Return False
                End If
            Next
        Else
            Return False
        End If
        Return True
    End Function


    Protected Sub SaveButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles SaveButton.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        If Me.DropdownlistGroup.SelectedIndex <> (Me.DropdownlistGroup.Items.Count - 1) Then
            Try

                'Periksa apakah User ada memilih Insider Code untuk Group tsb atau tidak saat mengklik tombol Save
                Dim CheckBoxCheckedCounter As Int32 = 0

                Dim NewInsiderCodes As New ArrayList

                'Periksa semua Checkbox InsiderCode
                For Each gridRow As DataGridItem In Me.GridMSUserView.Items
                    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                        Dim InsiderCodeId As String = gridRow.Cells(1).Text

                        'Aktifkan Checkbox tsb apabila ditemukan pasangan antara GroupId yg dipilih dan InsiderCodeId tsb
                        Dim chkbox1 As CheckBox = gridRow.Cells(0).FindControl("CheckBoxGroupInsiderCodeMapping")

                        'Bila ada Checkbox InsiderCode yg dicek, tambahkan dalam CheckBoxCheckedCounter
                        If chkbox1.Checked Then
                            CheckBoxCheckedCounter += 1
                            NewInsiderCodes.Add(InsiderCodeId)
                        End If
                    End If
                Next
                GetCreatedDate()
                'Jika NewInsiderCodes.Equals(Me.OldInsiderCodes) berarti tdk ada perubahan
                If Me.CompareArrayList(NewInsiderCodes, Me.OldInsiderCodes) Then
                    'do nothing
                Else
                    'CheckBoxCheckedCounter = 0 berarti tidak ada InsiderCode yg dipilih
                    If CheckBoxCheckedCounter = 0 Then
                        'Jika yg melakukan transaksi bukan Super User maka transaksi tsb akan masuk dlm pending approval terlebih dahulu
                        If Sahassa.AML.Commonly.SessionPkUserId <> 1 Then
                            'Using TransScope As New Transactions.TransactionScope
                            Using AccessGroupInsiderCode As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMappingTableAdapter
                                Using GroupInsiderCodeTable As AMLDAL.AMLDataSet.GroupInsiderCodeMappingDataTable = AccessGroupInsiderCode.GetDataByGroupId(Me.DropdownlistGroup.SelectedValue)
                                    'Jika Count > 0 berarti dari sebelumnya ada beberapa InsiderCode menjadi tidak ada InsiderCode, maka ModeId = 3 (Delete)
                                    If GroupInsiderCodeTable.Rows.Count > 0 Then
                                        'Catat dlm tabel PendingApproval dengan ModeId = 3
                                        Using AccessPendingApproval As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMapping_PendingApprovalTableAdapter
                                            oSQLTrans = BeginTransaction(AccessPendingApproval, IsolationLevel.ReadUncommitted)
                                            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMapping_ApprovalTableAdapter
                                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                                                For Each Row As AMLDAL.AMLDataSet.GroupInsiderCodeMappingRow In GroupInsiderCodeTable.Rows
                                                    Dim PendingApprovalId As Int64 = AccessPendingApproval.InsertGroupInsiderCodeMapping_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Group Insider Code Mapping Delete", 3)
                                                    AccessPending.Insert(PendingApprovalId, 3, Row.GroupId, Row.InsiderCodeId, Nothing, Nothing, Now, Nothing)
                                                Next
                                            End Using

                                        End Using
                                        oSQLTrans.Commit()

                                        Dim MessagePendingID As Integer = 8633 'MessagePendingID 8633 = GroupInsiderCodeMapping Delete 

                                        Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & Me.DropdownlistGroup.SelectedItem.Text

                                        Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & Me.DropdownlistGroup.SelectedItem.Text, False)
                                    Else 'Jika Count = 0 berarti sebelumnya memang tidak ada Insider Code untuk Group tsb maka tdk perlu melakukan apa2
                                        Exit Sub
                                    End If
                                End Using
                            End Using
                            'End Using
                        Else 'Jika yg melakukan delete adalah SuperUser
                            'Using TransScope As New Transactions.TransactionScope
                            Using AccessGroupInsider As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMappingTableAdapter
                                oSQLTrans = BeginTransaction(AccessGroupInsider, IsolationLevel.ReadUncommitted)

                                Dim dt As Data.DataTable = AccessGroupInsider.GetDataByGroupId(CInt(Me.DropdownlistGroup.SelectedValue))
                                If dt.Rows.Count > 0 Then
                                    Dim InsiderName As String
                                    Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                        SetTransaction(AccessAuditTrail, oSQLTrans)
                                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3 * dt.Rows.Count)
                                        For Each row As AMLDAL.AMLDataSet.GroupInsiderCodeMappingRow In dt.Rows
                                            InsiderName = Me.GetInsiderName(row.InsiderCodeId)
                                            AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group Insider Code Mapping", "GroupId", "Delete", Me.DropdownlistGroup.SelectedItem.Text, Me.DropdownlistGroup.SelectedItem.Text, "")
                                            AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group Insider Code Mapping", "InsiderCodeId", "Delete", "", InsiderName, "")
                                            AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group Insider Code Mapping", "CreatedDate", "Delete", Session("CreatedDateGroupInsiderCodeMapping"), Session("CreatedDateGroupInsiderCodeMapping"), "")
                                            AccessGroupInsider.DeleteGroupInsiderCodeMapping(CInt(Me.DropdownlistGroup.SelectedValue), row.InsiderCodeId)
                                        Next
                                    End Using

                                End If
                                oSQLTrans.Commit()
                            End Using
                            'End Using
                        End If
                    Else  'CheckBoxCheckedCounter != 0 berarti tidak ada InsiderCode yg dipilih
                        'Using TransScope As New Transactions.TransactionScope
                        'Berarti ada InsiderCode yg dipilih dan berarti ada perubahan pada InsiderCode untuk Group tsb
                        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMapping_ApprovalTableAdapter
                            oSQLTrans = BeginTransaction(AccessPending)
                            'Periksa apakah Group tsb berada dalam tabel GroupInsiderCode_Approval atau tidak
                            Dim counter As Int32 = AccessPending.CountGroupInsiderCodeApproval(Me.DropdownlistGroup.SelectedValue)

                            'Counter = 0 berarti Group tsb tidak dalam status Pending Approval dan boleh ditambahkan
                            If counter = 0 Then
                                Dim AddedInsiderCodes As New ArrayList
                                Dim DeletedInsiderCodes As New ArrayList
                                Dim OldInsiderCodes_ As ArrayList = Me.OldInsiderCodes

                                Dim i As Integer

                                'Periksa tiap2 InsiderCode baru thd InsiderCode lama
                                For i = 0 To NewInsiderCodes.Count - 1
                                    If OldInsiderCodes_.Contains(NewInsiderCodes(i)) = False Then
                                        AddedInsiderCodes.Add(NewInsiderCodes(i))
                                    End If
                                Next

                                'Periksa tiap2 InsiderCode lama thd InsiderCode baru
                                For i = 0 To OldInsiderCodes_.Count - 1
                                    If NewInsiderCodes.Contains(OldInsiderCodes_(i)) = False Then
                                        DeletedInsiderCodes.Add(OldInsiderCodes_(i))
                                    End If
                                Next

                                'Jika yg melakukan transaksi bukan Super User maka transaksi tsb akan masuk dlm pending approval terlebih dahulu
                                If Sahassa.AML.Commonly.SessionPkUserId <> 1 Then
                                    'Using TransScope As New Transactions.TransactionScope
                                    Using AccessPendingApproval As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMapping_PendingApprovalTableAdapter
                                        SetTransaction(AccessPendingApproval, oSQLTrans)
                                        Dim PendingApprovalId As Int64

                                        'Catat dlm tabel Approval InsiderCode2 yg ditambahkan dgn mode = 1 (Add)
                                        For i = 0 To AddedInsiderCodes.Count - 1
                                            PendingApprovalId = AccessPendingApproval.InsertGroupInsiderCodeMapping_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Group Insider Code Mapping Add", 1)
                                            AccessPending.Insert(PendingApprovalId, 1, Me.DropdownlistGroup.SelectedValue, AddedInsiderCodes(i), Nothing, Nothing, Now, Nothing)
                                        Next

                                        'Catat dlm tabel Approval InsiderCode2 yg dihapus dgn mode = 3 (Delete)
                                        For i = 0 To DeletedInsiderCodes.Count - 1
                                            PendingApprovalId = AccessPendingApproval.InsertGroupInsiderCodeMapping_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Group Insider Code Mapping Delete", 3)
                                            AccessPending.Insert(PendingApprovalId, 3, Me.DropdownlistGroup.SelectedValue, DeletedInsiderCodes(i), Nothing, Nothing, Now, Nothing)
                                        Next
                                    End Using
                                    oSQLTrans.Commit()

                                    Dim MessagePendingID As Integer = 8632 'MessagePendingID 8632 = GroupInsiderCodeMapping Edit 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & Me.DropdownlistGroup.SelectedItem.Text

                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & Me.DropdownlistGroup.SelectedItem.Text, False)
                                    'End Using
                                Else 'Jika yg melakukan transaksi adalah Super User maka transaksi langsung dijalankan
                                    'Using TransScope As New Transactions.TransactionScope
                                    Using AccessGroupInsider As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMappingTableAdapter
                                        SetTransaction(AccessGroupInsider, oSQLTrans)
                                        Using dtGroup As Data.DataTable = AccessGroupInsider.GetDataByGroupId(CInt(Me.DropdownlistGroup.SelectedValue))
                                            Dim InsiderName As String = ""
                                            If dtGroup.Rows.Count > 0 Then ' edit
                                                Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrail, oSQLTrans)
                                                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3 * (DeletedInsiderCodes.Count + AddedInsiderCodes.Count))
                                                    ' delete group yg lama
                                                    For j As Integer = 0 To DeletedInsiderCodes.Count - 1
                                                        InsiderName = Me.GetInsiderName(DeletedInsiderCodes(j))
                                                        AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group Insider Code Mapping", "GroupId", "Delete", Me.DropdownlistGroup.SelectedItem.Text, Me.DropdownlistGroup.SelectedItem.Text, "")
                                                        AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group Insider Code Mapping", "InsiderCodeId", "Delete", "", InsiderName, "")
                                                        AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group Insider Code Mapping", "CreatedDate", "Delete", Session("CreatedDateGroupInsiderCodeMapping"), Session("CreatedDateGroupInsiderCodeMapping"), "")
                                                        AccessGroupInsider.DeleteGroupInsiderCodeMapping(CInt(Me.DropdownlistGroup.SelectedValue), DeletedInsiderCodes(j))
                                                    Next

                                                    ' insert yang baru dan audittrail
                                                    For h As Integer = 0 To AddedInsiderCodes.Count - 1
                                                        InsiderName = Me.GetInsiderName(AddedInsiderCodes(h))
                                                        AccessGroupInsider.Insert(CInt(Me.DropdownlistGroup.SelectedValue), AddedInsiderCodes(h), Now)
                                                        AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group Insider Code Mapping", "GroupId", "Add", Me.DropdownlistGroup.SelectedItem.Text, Me.DropdownlistGroup.SelectedItem.Text, "")
                                                        AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group Insider Code Mapping", "InsiderCodeId", "Add", "", InsiderName, "")
                                                        AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group Insider Code Mapping", "CreatedDate", "Add", Session("CreatedDateGroupInsiderCodeMapping"), Session("CreatedDateGroupInsiderCodeMapping"), "")
                                                    Next
                                                    oSQLTrans.Commit()
                                                    Me.LblSuccess.Visible = True
                                                    Me.LblSuccess.Text = "Success to update group insider code mapping."
                                                End Using
                                            Else ' add
                                                Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3 * NewInsiderCodes.Count)
                                                Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrail, oSQLTrans)
                                                    For j As Integer = 0 To NewInsiderCodes.Count - 1
                                                        InsiderName = Me.GetInsiderName(NewInsiderCodes(j))
                                                        AccessGroupInsider.Insert(CInt(Me.DropdownlistGroup.SelectedValue), NewInsiderCodes(j), Now)
                                                        AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group Insider Code Mapping", "GroupId", "Add", Me.DropdownlistGroup.SelectedItem.Text, Me.DropdownlistGroup.SelectedItem.Text, "")
                                                        AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group Insider Code Mapping", "InsiderCodeId", "Add", "", InsiderName, "")
                                                        AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group Insider Code Mapping", "CreatedDate", "Add", Now.ToString("dd MMMM yyyy"), Now.ToString("dd MMMM yyyy"), "")
                                                    Next
                                                End Using
                                                oSQLTrans.Commit()
                                                Me.LblSuccess.Visible = True
                                                Me.LblSuccess.Text = "Success to insert group insider code mapping."
                                            End If
                                        End Using
                                    End Using
                                    'End Using
                                End If
                            Else 'Counter != 0 berarti Group tsb dalam status Pending Approval
                                Throw New Exception("Cannot change the Insider Code Mapping for the following Group : " & Me.DropdownlistGroup.SelectedItem.Text & " because it is currently waiting for approval.")
                            End If

                        End Using
                        'End Using
                    End If
                End If
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try
        End If
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="insidercode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetInsiderName(ByVal insidercode As String) As String
        Try
            Dim InsiderName As String = ""
            Using AccessInsider As New AMLDAL.AMLDataSetTableAdapters.InsiderCodeTableAdapter
                Using dt As Data.DataTable = AccessInsider.GetData
                    Dim row As AMLDAL.AMLDataSet.InsiderCodeRow = dt.Select("InsiderCodeId='" & insidercode & "'")(0)
                    If Not row.IsInsiderCodeNameNull Then
                        InsiderName = row.InsiderCodeName
                        Return InsiderName
                    End If
                End Using
            End Using
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Sub GetCreatedDate()
        Try
            Using AccessGroupInsider As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMappingTableAdapter
                Using dt As Data.DataTable = AccessGroupInsider.GetDataByGroupId(CInt(Me.DropdownlistGroup.SelectedValue))
                    If dt.Rows.Count > 0 Then
                        Dim row As AMLDAL.AMLDataSet.GroupInsiderCodeMappingRow = dt.Rows(0)
                        Session("CreatedDateGroupInsiderCodeMapping") = row.CreatedDate.ToString("dd MMMM yyyy hh:mm:ss")
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub
End Class