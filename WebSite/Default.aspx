<%@ Page Language="VB" MasterPageFile="~/MasterPage_v2.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" title=":: Anti Money Laundering ::" %>




<asp:Content ID="Header" ContentPlaceHolderID="head" runat="server">

    <link href="theme/toastr.min.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="script/jquery-3.3.1.min.js" type="text/javascript"></script>
    <%--<script language="javascript" src="script/jquery-1.9.1.min.js" type="text/javascript"></script>--%>
    <script language="javascript" src="script/toastr.min.js" type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table style="height: 122px" width="99%">
        <tr>
            <td colspan="7" >&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7" >&nbsp;</td>
        </tr>              
        <tr>
            <td colspan="3" rowspan="2" align="center">
            
                <table width="100%" height="100%">
                    <tr>
                        <td colspan="7" align="center"><img src="images/hometop.gif" width="895" height="57"/></td>
                    </tr>
                    <tr align="center">
                        <td><img src="images/home1.gif" /></td>
                        <td>&nbsp;&nbsp;<img src="images/homearrow.gif" />&nbsp;&nbsp;</td>
                        <td><img src="images/home2.gif" /></td>
                        <td>&nbsp;&nbsp;<img src="images/homearrow.gif" />&nbsp;&nbsp;</td>
                        <td><img src="images/home3.gif" /></td>
                        <td>&nbsp;&nbsp;<img src="images/homearrow.gif" />&nbsp;&nbsp;</td>
                        <td><img src="images/home4.gif" /></td>                                                
                    </tr>                    
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="7">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7" style="border-bottom-color: darkgray; border-bottom-style: ridge;">&nbsp;</td>
        </tr>        
        <tr>
            <td colspan="7" >&nbsp;</td>
        </tr>        

    </table>
             <table style="height: 122px" width="99%" bgcolor="white">   
              <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Task List&nbsp;         
                </strong>
               </td>
        </tr>    
                 <tr>
                     <td>
                     <p>
                            <asp:Repeater ID="NewTaskListRepeater" runat="server">
                                <ItemTemplate>                                                             
                                   <%--<asp:LinkButton ID="ShowMoreTaskList" runat="server" CommandArgument='<%# Eval("TaskURL") %>'
                                        CommandName="ShowMoreTaskList"><strong><%#Eval("TaskName")%></strong></asp:LinkButton>--%>
                                    <asp:LinkButton ID="ShowMoreTaskList" href="#"  Font-Size="16px" ForeColor="Red" runat="server"><strong><%#Eval("TaskName")%></strong></asp:LinkButton>
                                    <br />
                                </ItemTemplate>                              
                            </asp:Repeater>
                            </p>
                     </td>
                 </tr>
                 <tr>
                     <td>
                     <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
                     </td>
                 </tr>
                 <tr>
                    <td><asp:LinkButton ID="LnkCustomerTicketTransaction" runat="server" 
                                Font-Size="16px" ForeColor="Red"></asp:LinkButton></td>
                 </tr>
        <tr>
						<td>
                            <p>
                            <asp:Repeater ID="TaskListRepeater" runat="server">
                                <ItemTemplate>                                                             
                                   <asp:LinkButton ID="ShowMoreTaskList" runat="server" CommandArgument='<%# Eval("TaskListID") %>'
                                        CommandName="ShowMoreTaskList"><strong><%#Eval("TaskListLabel")%></strong></asp:LinkButton>
                                        <br />
                                </ItemTemplate>                              
                            </asp:Repeater>
                            </p>
                    </td>
					</tr>
    </table>
    <script type="text/javascript">
            $(function () {
                var toast = '<%=toastrText %>';

            // Check whether a toast should be displayed
            if (toast) {
                var toasttype = '<%=toastrType %>';
                var toastposition = '<%=toastrPosition %>';
                    toastr.options = {
                        "positionClass": toastposition

                    }
                    toastr[toasttype](toast);
                }
            })
        </script>
</asp:Content>


