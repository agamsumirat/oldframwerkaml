Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class RiskRatingAdd
    Inherits System.Web.UI.Page

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("RiskRatingView.aspx", False)
    End Sub

    Private Sub InsertAuditTrail()
        Try
            Dim RiskRatingDescription As String
            If Me.TextRiskRatingDescription.Text.Length <= 255 Then
                RiskRatingDescription = Me.TextRiskRatingDescription.Text
            Else
                RiskRatingDescription = Me.TextRiskRatingDescription.Text.Substring(0, 255)
            End If
            Dim RiskScoreFrom As Int16 = Me.TextRiskScoreFrom.Text
            Dim RiskScoreTo As Int16 = Me.TextRiskScoreTo.Text

            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "CreatedDate", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskRatingName", "Add", "", Me.TextRiskRatingName.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "Description", "Add", "", RiskRatingDescription, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreFrom", "Add", "", RiskScoreFrom, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreTo", "Add", "", RiskScoreTo, "Accepted")
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub InsertRiskRatingBySU()
        Try
            Using AccessRiskRating As New AMLDAL.AMLDataSetTableAdapters.RiskRatingTableAdapter
                Dim RiskRatingDescription As String
                If Me.TextRiskRatingDescription.Text.Length <= 255 Then
                    RiskRatingDescription = Me.TextRiskRatingDescription.Text
                Else
                    RiskRatingDescription = Me.TextRiskRatingDescription.Text.Substring(0, 255)
                End If
                Dim RiskScoreFrom As Int16 = Me.TextRiskScoreFrom.Text
                Dim RiskScoreTo As Int16 = Me.TextRiskScoreTo.Text
                AccessRiskRating.Insert(Me.TextRiskRatingName.Text, RiskRatingDescription, RiskScoreFrom, RiskScoreTo, Now)
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                Dim RiskRatingName As String = Trim(Me.TextRiskRatingName.Text)

                'Periksa apakah RiskRating Name tersebut sudah ada dalam tabel RiskRating atau belum
                Using AccessRiskRating As New AMLDAL.AMLDataSetTableAdapters.RiskRatingTableAdapter
                    Dim counter As Int32 = AccessRiskRating.CountMatchingRiskRating(RiskRatingName)

                    'Counter = 0 berarti RiskRating tersebut belum pernah ada dalam tabel RiskRating
                    If counter = 0 Then
                        'Periksa apakah RiskRating Name tersebut sudah ada dalam tabel RiskRating_Approval atau belum
                        Using AccessRiskRatingApproval As New AMLDAL.AMLDataSetTableAdapters.RiskRating_ApprovalTableAdapter
                            oSQLTrans = BeginTransaction(AccessRiskRatingApproval, IsolationLevel.ReadUncommitted)
                            counter = AccessRiskRatingApproval.CountRiskRatingApprovalByRiskRatingName(RiskRatingName)

                            'Counter = 0 berarti RiskRating tersebut statusnya tidak dalam pending approval dan boleh ditambahkan dlm tabel RiskRating_Approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertRiskRatingBySU()
                                    Me.InsertAuditTrail()
                                    Me.LblSucces.Visible = True
                                    Me.LblSucces.Text = "Success to Insert Risk Rating."
                                Else
                                    Dim RiskRatingDescription As String
                                    If Me.TextRiskRatingDescription.Text.Length <= 255 Then
                                        RiskRatingDescription = Me.TextRiskRatingDescription.Text
                                    Else
                                        RiskRatingDescription = Me.TextRiskRatingDescription.Text.Substring(0, 255)
                                    End If

                                    Dim RiskScoreFrom As Int16 = Me.TextRiskScoreFrom.Text
                                    Dim RiskScoreTo As Int16 = Me.TextRiskScoreTo.Text

                                    Dim RiskRatingPendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope
                                    Using AccessRiskRatingPendingApproval As New AMLDAL.AMLDataSetTableAdapters.RiskRating_PendingApprovalTableAdapter
                                        SetTransaction(AccessRiskRatingPendingApproval, oSQLTrans)
                                        'Tambahkan ke dalam tabel RiskRating_PendingApproval dengan ModeID = 1 (Add) 
                                        RiskRatingPendingApprovalID = AccessRiskRatingPendingApproval.InsertRiskRating_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Risk Rating Add", 1)
                                    End Using

                                    'Tambahkan ke dalam tabel RiskRating_Approval dengan ModeID = 1 (Add) 
                                    AccessRiskRatingApproval.Insert(RiskRatingPendingApprovalID, 1, Nothing, RiskRatingName, RiskRatingDescription, RiskScoreFrom, RiskScoreTo, Now, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)

                                    oSQLTrans.Commit()
                                    'TransScope.Complete()
                                    'End Using

                                    Dim MessagePendingID As Integer = 81201 'MessagePendingID 81201 = RiskRating Add 
                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & RiskRatingName, False)

                                End If

                            Else
                                Throw New Exception("Cannot add the following Risk Rating: '" & RiskRatingName & "' because it is currently waiting for approval.")
                            End If
                        End Using
                    Else
                        Throw New Exception("Cannot add the following Risk Rating: '" & RiskRatingName & "' because that Risk Rating Name already exists in the database.")
                    End If

                End Using
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
            End Try
        End If
    End Sub
End Class