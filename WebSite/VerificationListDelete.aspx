<%@ Page Language="VB" MasterPageFile="~/MASTERPAGE.master" AutoEventWireup="false" CodeFile="VerificationListDelete.aspx.vb" Inherits="VerificationListDelete" title="Verification List Delete" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<ajax:AjaxPanel ID="lbl" runat="server" Width="100%">
		<table id="title"  border="2" bgcolor="#ffffff" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Verification List - Delete&nbsp;
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none" width="5">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/validationsign_animate.gif" /></td>
            <td bgcolor="#ffffff" colspan="3" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>The following Verification List will be deleted :</strong></td>
        </tr>
    </table>
    </ajax:AjaxPanel>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72">       
        <tr class="formText">
            <td bgcolor="#ffffff" style="height: 24px" colspan="4">
                <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 81px">
                        <tr>
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                                Verification List ID</td>
                            <td align="left" style="width: 45px; height: 35px">
                                :</td>
                            <td style="width: 2%; height: 35px">
                            </td>
                            <td style="width: 90%; height: 35px">
                                <asp:Label ID="LabelVerificationListID" runat="server"></asp:Label></td>
                        </tr>
                      
                        <tr id="GridViewAliasesRow" runat="server">
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                                Name/Alias</td>
                            <td align="left" style="width: 45px; height: 35px">                                :</td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                            <div style="overflow:auto;height:auto"><asp:DataGrid ID="GridViewAliases" runat="server" AutoGenerateColumns="False" BackColor="White"
                                    BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2" Font-Size="XX-Small"
                                    ForeColor="Black" HorizontalAlign="Left" Width="300px">
                                    <Columns>
                                        <asp:BoundColumn DataField="id" HeaderText="No.">
                                            <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Aliases" HeaderText="Alias ">
                                            <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                        </asp:BoundColumn>                                    
                                    </Columns>
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                                        Visible="False" />
                                    <AlternatingItemStyle BackColor="AliceBlue" />
                                </asp:DataGrid></div>                            
                                </td>
                        </tr>
                        <tr id="SpacerAliases" runat="server">
                            <td style="width: 24px; height: 15px">
                            </td>
                            <td style="width: 16%; height: 15px">
                            </td>
                             <td align="left" style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 80%; height: 15px">
                            </td>
                        </tr>
                         <tr>
                            <td style="width: 24px; height: 35px">
                                &nbsp;</td>
                            <td style="width: 16%; height: 35px">
                Date of Data</td>
                             <td align="left" style="width: 45px; height: 35px">
                                :
                            </td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <asp:Label ID="LabelDateOfData" runat="server"></asp:Label></td>
                        </tr>                          
                         <tr>
                            <td style="width: 24px; height: 35px">
                                &nbsp;</td>
                            <td style="width: 16%; height: 35px">
                Date of Birth</td>
                             <td align="left" style="width: 45px; height: 35px">
                                :
                            </td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <asp:Label ID="LabelDOB" runat="server"></asp:Label></td>
                        </tr>
                        <tr id="GridViewAddressesRow" runat="server">
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                                Address</td>
                             <td align="left" style="width: 45px; height: 35px">
                                :</td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                             <div style="overflow:auto;height:auto"> 
                                 <asp:DataGrid ID="GridViewAddresses" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                     BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                                     Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                                     <Columns>
                                         <asp:BoundColumn DataField="id" HeaderText="No.">
                                             <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                 Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                         </asp:BoundColumn>
                                         <asp:BoundColumn DataField="Addresses" HeaderText="Address">
                                             <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                 Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                         </asp:BoundColumn>
                                         <asp:BoundColumn DataField="AddressType" Visible="False"></asp:BoundColumn>
                                         <asp:BoundColumn DataField="AddressTypeLabel" HeaderText="Address Type">
                                             <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                 Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                         </asp:BoundColumn>
                                         <asp:BoundColumn DataField="IsLocalAddress" HeaderText="Local Address?">
                                             <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                 Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                             <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                 Font-Underline="False" HorizontalAlign="Center" />
                                         </asp:BoundColumn>                                       
                                     </Columns>
                                     <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                                         Visible="False" />
                                     <AlternatingItemStyle BackColor="AliceBlue" />
                                 </asp:DataGrid></div></td>
                        </tr>
                          <tr id="SpacerAddresses" runat="server">
                            <td style="width: 24px; height: 15px">
                            </td>
                            <td style="width: 16%; height: 15px">
                            </td>
                             <td align="left" style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 80%; height: 15px">
                            </td>
                        </tr>
                        <tr id="GridViewIDNosRow" runat="server">
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                                ID Number</td>
                             <td align="left" style="width: 45px; height: 35px">
                                :
                            </td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                             <div style="overflow:auto;height:auto">
                                 <asp:DataGrid ID="GridViewIDNos" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                     BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                                     Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                                     <Columns>
                                         <asp:BoundColumn DataField="id" HeaderText="No.">
                                             <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                 Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                         </asp:BoundColumn>
                                         <asp:BoundColumn DataField="IDNos" HeaderText="ID Number">
                                             <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                 Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                         </asp:BoundColumn>                                         
                                     </Columns>
                                     <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                                         Visible="False" />
                                     <AlternatingItemStyle BackColor="AliceBlue" />
                                 </asp:DataGrid></div>
                                </td>
                        </tr>
                         <tr id="SpacerIDNos" runat="server">
                            <td style="width: 24px; height: 15px">
                            </td>
                            <td style="width: 16%; height: 15px">
                            </td>
                             <td align="left" style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 80%; height: 15px">
                            </td>
                        </tr>                       
                        <tr>
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                Verification List Type</td>
                             <td align="left" style="width: 45px; height: 35px">
                                :
                            </td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <asp:Label ID="LabelVerificationListType" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                Category</td>
                             <td align="left" style="width: 45px; height: 35px">
                                :</td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <asp:Label ID="LabelCategory" runat="server"></asp:Label></td>
                        </tr>
                        <tr id="GridViewCustomRemarksRow" runat="server">
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                                Custom Remark(s)</td>
                            <td align="left" style="width: 45px; height: 35px">
                                :</td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                             <div style="overflow:auto;height:auto">
                                 <asp:DataGrid ID="GridViewCustomRemarks" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                     BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                                     Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                                     <Columns>
                                         <asp:BoundColumn DataField="id" HeaderText="No.">
                                             <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                 Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                         </asp:BoundColumn>
                                         <asp:BoundColumn DataField="CustomRemarks" HeaderText="Custom Remark">
                                             <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                 Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                         </asp:BoundColumn>                                       
                                     </Columns>
                                     <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                                         Visible="False" />
                                     <AlternatingItemStyle BackColor="AliceBlue" />
                                 </asp:DataGrid></div>
                                </td>
                        </tr>
                         <tr id="SpacerCustomRemarks" runat="server">
                            <td style="width: 24px; height: 15px">
                            </td>
                            <td style="width: 16%; height: 15px">
                            </td>
                             <td align="left" style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 80%; height: 15px">
                            </td>
                        </tr>
                        <tr class="formText" bgColor="#dddddd" height="30">
                            <td style="width: 24px">
                                <IMG height="15" src="images/arrow.gif" width="15"></td>
                            <td style="height: 35px" colspan="4">
                            <asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="DeleteButton"></asp:imagebutton>
                                <asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton><br />
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
                        </tr>
                    </table>
                    <strong><span style="color: #ff0000"></span></strong>
                </ajax:AjaxPanel>
            </td>
        </tr>
	</table>
	
</asp:Content>

