<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="YearIntervalCDDLowRiskEdit.aspx.vb" Inherits="YearIntervalCDDLowRiskEdit" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%">
        <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
            border="2">
            <tr>
                <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                    border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                    <strong>Year Interval CDD &nbsp;Low Risk
                        <hr />
                    </strong>
                    <asp:Label ID="LblSucces" CssClass="validationok" Width="94%" runat="server" Visible="False"></asp:Label></td>
            </tr>
            <tr class="formText">
                <td width="5" bgcolor="#ffffff" height="24">
                    &nbsp;</td>
                <td width="20%" bgcolor="#ffffff">
                    Year Interval CDD Low Risk</td>
                <td width="5" bgcolor="#ffffff">
                    :</td>
                <td width="80%" bgcolor="#ffffff">
                    &nbsp;<asp:TextBox ID="txtNTN" runat="server" CssClass="textbox"></asp:TextBox>
                    </td>
            </tr>
            <tr class="formText" bgcolor="#dddddd" height="30">
                <td width="15">
                    <img height="15" src="images/arrow.gif" width="15"></td>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="3" border="0">
                        <tr>
                            <td>
                                <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="True" SkinID="SaveButton">
                                </asp:ImageButton></td>
                            <td>
                                <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton">
                                </asp:ImageButton></td>
                        </tr>
                    </table>
                    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator></td>
            </tr>
        </table>
    </ajax:AjaxPanel>
</asp:Content>


