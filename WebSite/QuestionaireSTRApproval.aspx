<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="QuestionaireSTRApproval.aspx.vb" Inherits="QuestionaireSTRApproval"  %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
 <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Questionaire STR Approval&nbsp;
                    <hr />
                </strong>
            </td>
        </tr>
    </table>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2">
        <tr>
            <td align="left" bgcolor="#eeeeee">
                <img height="15" src="images/arrow.gif" width="15"></td>
            <td valign="top" width="98%" bgcolor="#ffffff">
                <table cellspacing="4" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="Regtext" nowrap>
                            Search By :
                        </td>
                        <td valign="middle" nowrap>
                            <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                <asp:DropDownList ID="ComboSearch" TabIndex="1" runat="server" Width="120px" CssClass="searcheditcbo">
                                </asp:DropDownList>&nbsp;
                                <asp:TextBox ID="TextSearch" TabIndex="2" runat="server" CssClass="searcheditbox"></asp:TextBox><input
                                    id="popUp" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                    border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                    border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                    width: 16px;" type="button" name="popUpCalc" runat="server"></ajax:AjaxPanel></td>
                        <td valign="middle" width="99%">
                            <ajax:AjaxPanel ID="AjaxPanel2" runat="server">
                                <asp:ImageButton ID="ImageButtonSearch" TabIndex="3" runat="server" SkinID="SearchButton">
                                </asp:ImageButton></ajax:AjaxPanel>
                        </td>
                    </tr>
                </table>
                <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel></td>
        </tr>
    </table>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2">
        <tr>
            <td bgcolor="#ffffff">
                &nbsp;<ajax:AjaxPanel ID="Ajaxpanel14" runat="server"><asp:DataGrid ID="GridView"
                    runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                    CellPadding="4" Font-Size="XX-Small" ForeColor="Black" GridLines="Vertical" Width="100%" AllowCustomPaging="True">
                    <FooterStyle BackColor="#CCCC99" />
                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <AlternatingItemStyle BackColor="White" />
                    <ItemStyle BackColor="#F7F7DE" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="XX-Small" ForeColor="White" />
                    <Columns>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="PK_MsQuestionaireSTR_ApprovalID" HeaderText="PK_MsQuestionaireSTR_ApprovalID"
                            Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="UserName" HeaderText="Preparer"
                            SortExpression="UserName Desc"></asp:BoundColumn>
                        <asp:BoundColumn DataField="DescriptionAlertSTR" HeaderText="Description Alert STR" SortExpression="DescriptionAlertSTR Desc">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="QuestionNo" HeaderText="Question No." SortExpression="QuestionNo Desc">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Nama" HeaderText="Mode" SortExpression="Nama Desc">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" SortExpression="CreatedDate Desc" DataFormatString="{0:dd-MM-yyyy}">
                        </asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton ID="LnkDetail" runat="server" CausesValidation="False" CommandName="Detail"
                                    meta:resourcekey="LnkDeleteResource1" Text="Detail"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                        Visible="False" />
                </asp:DataGrid></ajax:AjaxPanel>
            </td>
        </tr>
        <tr>
            <td style="background-color: #ffffff">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td nowrap>
                            <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                                &nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
                                &nbsp;
                            </ajax:AjaxPanel>
                        </td>
                        <td width="99%">
                            &nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" runat="server">Export 
		        to Excel</asp:LinkButton>
                            <asp:LinkButton ID="lnkExportAllData" runat="server">Export All
		        to Excel</asp:LinkButton></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                    bgcolor="#ffffff" border="2">
                    <tr class="regtext" align="center" bgcolor="#dddddd">
                        <td valign="top" align="left" width="50%" bgcolor="#ffffff">
                            Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel6" runat="server"><asp:Label ID="PageCurrentPage"
                                runat="server" CssClass="regtext">0</asp:Label>&nbsp;of&nbsp;
                                <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label></ajax:AjaxPanel></td>
                        <td valign="top" align="right" width="50%" bgcolor="#ffffff">
                            Total Records&nbsp;
                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                <asp:Label ID="PageTotalRows" runat="server">0</asp:Label></ajax:AjaxPanel></td>
                    </tr>
                </table>
                <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                    bgcolor="#ffffff" border="2">
                    <tr bgcolor="#ffffff">
                        <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                            <hr color="#f40101" noshade size="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                            Go to page</td>
                        <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                    <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox></ajax:AjaxPanel>
                            </font>
                        </td>
                        <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton"></asp:ImageButton></ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/first.gif" width="6">
                        </td>
                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                    OnCommand="PageNavigate">First</asp:LinkButton></ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/prev.gif" width="6"></td>
                        <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                    OnCommand="PageNavigate">Previous</asp:LinkButton></ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                <a class="pageNav" href="#">
                                    <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                        OnCommand="PageNavigate">Next</asp:LinkButton></a></ajax:AjaxPanel></td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/next.gif" width="6"></td>
                        <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                    OnCommand="PageNavigate">Last</asp:LinkButton></ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/last.gif" width="6"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

