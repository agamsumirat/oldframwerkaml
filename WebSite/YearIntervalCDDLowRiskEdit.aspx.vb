Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper
Imports SahassaNettier.Data
Imports SahassaNettier.Entities

Partial Class YearIntervalCDDLowRiskEdit
    Inherits Parent

    Private Sub GetData()
        'modus amount yang diambil
        Using ObjSystemParameter As SystemParameter = DataRepository.SystemParameterProvider.GetByPK_SystemParameter_ID(12)
            If Not ObjSystemParameter Is Nothing Then
                With ObjSystemParameter

                    txtNTN.Text = .ParameterValue
                End With
            End If
        End Using

        'Toleransi
        'Using ObjSystemParameter As SystemParameter = DataRepository.SystemParameterProvider.GetByPK_SystemParameter_ID(9)
        '    If Not ObjSystemParameter Is Nothing Then
        '        With ObjSystemParameter
        '            Me.TxtToleransi.Text = .ParameterValue
        '        End With
        '    End If
        'End Using
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using


                Me.GetData()
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ImageCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "Default.aspx"

        Me.Response.Redirect("Default.aspx", False)
    End Sub

    Private Function IsDataValid() As Boolean
        If txtNTN.Text.Trim = "" Then
            Throw New Exception("Please Fill Year Internal CDD Low Risk")
        End If
        If Not Double.TryParse(txtNTN.Text, 0) Then
            Throw New Exception("Year Internal CDD Low Risk must Numeric")
        End If

        'If Me.TxtToleransi.Text.Trim = "" Then
        '    Throw New Exception("Toleransi is required.")
        'ElseIf Not IsNumeric(Me.TxtToleransi.Text) Then
        '    Throw New Exception("Toleransi must be numeric.")
        'End If

        Return True
    End Function

    Private Sub ImageSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            Dim IntCount As Integer = 0
            DataRepository.SystemParameter_ApprovalDetailProvider.GetPaged("FK_SystemParameter_ID=12", "", 0, 0, IntCount)

            If IntCount = 0 Then
                If Me.IsDataValid Then
                    'Cek apakah Superuser atau bukan
                    If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                        Using ObjSystemParameterBLL As New AMLBLL.SystemParameterBLL

                            ObjSystemParameterBLL.SaveLowRiskDirect(Me.txtNTN.Text.ToString)

                            Me.LblSucces.Visible = True
                            Me.LblSucces.Text = "System Parameter Succeed to Save."
                        End Using
                    Else
                        Using ObjSystemParameterBLL As New AMLBLL.SystemParameterBLL
                            ObjSystemParameterBLL.SaveLowRiskToApproval(Me.txtNTN.Text.ToString)

                            Me.LblSucces.Visible = True
                            Me.LblSucces.Text = "System Parameter has been changed and it is currently waiting for approval."
                        End Using
                    End If

                End If

            Else
                Throw New Exception("Cannot update System Parameter because it is currently waiting for approval")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
