Imports System.Globalization
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports Sahassanettier.Services
Partial Class LTKTApproval
    Inherits Parent

#Region "Set Session"
    Private Sub ClearThisPageSessions()
        Session("LTKTApprovalList.ApprovalSort") = Nothing
        Session("LTKTApprovalList.ApprovalCurrentPage") = Nothing
        Session("LTKTApprovalList.ApprovalRowTotal") = Nothing
        Session("LTKTApprovalList.ApprovalSearchCriteria") = Nothing
        Session("LTKTApprovalList.ApprovalViewData") = Nothing
    End Sub
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("LTKTApprovalList.ApprovalSort") Is Nothing, "PK_LTKT_Approval_Id desc", Session("LTKTApprovalList.ApprovalSort"))
        End Get
        Set(ByVal Value As String)
            Session("LTKTApprovalList.ApprovalSort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("LTKTApprovalList.ApprovalCurrentPage") Is Nothing, 0, Session("LTKTApprovalList.ApprovalCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("LTKTApprovalList.ApprovalCurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("LTKTApprovalList.ApprovalRowTotal") Is Nothing, 0, Session("LTKTApprovalList.ApprovalRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("LTKTApprovalList.ApprovalRowTotal") = Value
        End Set
    End Property
    Private Property SetAndGetSearchingCriteria() As String
        Get
            Return IIf(Session("LTKTApprovalList.ApprovalSearchCriteria") Is Nothing, "", Session("LTKTApprovalList.ApprovalSearchCriteria"))
        End Get
        Set(ByVal Value As String)
            Session("LTKTApprovalList.ApprovalSearchCriteria") = Value
        End Set
    End Property
    Private Function SetAllSearchingCriteria() As String
        Dim StrSearch As String = ""
        Try
            If StrSearch = "" Then
                StrSearch = StrSearch & " UserId <> '" & Sahassa.AML.Commonly.SessionUserId & "' and UserId IN (SELECT UserId FROM   UserWorkingUnitAssignment WHERE  WorkingUnitId IN (SELECT WorkingUnitId FROM UserWorkingUnitAssignment WHERE UserId = '" & Sahassa.AML.Commonly.SessionUserId & "'))"
            Else
                StrSearch = StrSearch & " AND UserId <> '" & Sahassa.AML.Commonly.SessionUserId & "' and UserId IN (SELECT UserId FROM   UserWorkingUnitAssignment WHERE  WorkingUnitId IN (SELECT WorkingUnitId FROM UserWorkingUnitAssignment WHERE UserId = '" & Sahassa.AML.Commonly.SessionUserId & "'))"
            End If

            If Me.txtNIKStaff.Text <> "" Then
                If StrSearch = "" Then
                    StrSearch = StrSearch & " UserName like '%" & Me.txtNIKStaff.Text.Replace("'", "''") & "%' "
                Else
                    StrSearch = StrSearch & " And UserName like '%" & Me.txtNIKStaff.Text.Replace("'", "''") & "%' "
                End If
            End If
            If Me.CboAction.SelectedIndex > 0 Then
                If StrSearch = "" Then
                    StrSearch = StrSearch & " Nama like '%" & Me.CboAction.SelectedItem.Text & "%'"
                Else
                    StrSearch = StrSearch & " And Nama like'%" & Me.CboAction.SelectedItem.Text & "%'"
                End If
            End If

            If (Me.txtStartDate.Text <> "" And Me.txtEndDate.Text = "") Or (Me.txtStartDate.Text = "" And Me.txtEndDate.Text <> "") Then
                Throw New Exception("Date is not valid.")
            End If

            If Me.txtStartDate.Text <> "" And Me.txtEndDate.Text <> "" Then
                If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", Me.txtStartDate.Text) And Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", Me.txtEndDate.Text) Then
                    If DateDiff(DateInterval.Day, DateTime.ParseExact(Me.txtStartDate.Text, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal), DateTime.ParseExact(Me.txtEndDate.Text, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal)) < 0 Then
                        Throw New Exception("Date is not valid.")
                    End If
                    If StrSearch = "" Then
                        StrSearch = StrSearch & "'" & DateTime.ParseExact(Me.txtStartDate.Text, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal) & " 00:00' <= RequestedDate and '" & DateTime.ParseExact(Me.txtEndDate.Text, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal) & " 23:59'  >= RequestedDate "
                    Else
                        StrSearch = StrSearch & " And '" & DateTime.ParseExact(Me.txtStartDate.Text, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal) & " 00:00' <= RequestedDate and '" & DateTime.ParseExact(Me.txtEndDate.Text, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal) & " 23:59'  >= RequestedDate "
                    End If
                End If
            End If

            Return StrSearch
        Catch
            Throw
            Return ""
        End Try
    End Function
    Private Property SetnGetBindTable() As VList(Of Vw_LTKTApproval)
        Get

            Session("LTKTApprovalList.ApprovalViewData") = DataRepository.Vw_LTKTApprovalProvider.GetPaged(Me.SetAndGetSearchingCriteria, Me.SetnGetSort, (Me.SetnGetCurrentPage), Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)

            Return Session("LTKTApprovalList.ApprovalViewData")

        End Get
        Set(ByVal value As VList(Of Vw_LTKTApproval))
            Session("LTKTApprovalList.ApprovalViewData") = value
        End Set
    End Property
#End Region
#Region "Paging Event"
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try            
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be less than or equal to the total page count.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

#Region "Bind Grid View"
    Public Sub BindGrid()
        Me.GridLTKTApprovalList.DataSource = Me.SetnGetBindTable
        'Me.GridLTKTApprovalList.DataSource = DataRepository.Vw_LTKTApprovalProvider.GetAll
        Me.GridLTKTApprovalList.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridLTKTApprovalList.DataBind()
    End Sub
#End Region

#Region "Searching Event"
    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSearch.Click
        Try
            Me.SetAndGetSearchingCriteria = Me.SetAllSearchingCriteria
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub ImageClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageClearSearch.Click
        Try
            Me.txtStartDate.Text = ""
            Me.txtEndDate.Text = ""
            Me.txtNIKStaff.Text = ""
            Me.CboAction.SelectedIndex = 0
            Me.SetAndGetSearchingCriteria = SetAllSearchingCriteria()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
#Region "Bind Combo Box"
    Private Sub BindModeDescription()
        Dim oList As TList(Of Mode)
        Try
            oList = DataRepository.ModeProvider.GetAll()
            Me.CboAction.Items.Clear()
            Me.CboAction.DataSource = oList
            Me.CboAction.DataValueField = "MsModeID"
            Me.CboAction.DataTextField = "Nama"
            Me.CboAction.DataBind()
            Me.CboAction.Items.Insert(0, New ListItem("[All]", "0"))
            Me.CboAction.SelectedIndex = 0
        Catch
            Throw
        End Try
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()
                Me.BindModeDescription()

                Me.popUpStartDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtStartDate.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUpStartDate.Style.Add("display", "")
                Me.popUpEndDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtEndDate.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUpEndDate.Style.Add("display", "")

                Me.SetAndGetSearchingCriteria = Me.SetAllSearchingCriteria
                Me.GridLTKTApprovalList.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub GridLTKTApprovalList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridLTKTApprovalList.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                e.Item.Cells(4).Text = Convert.ToDateTime(e.Item.Cells(4).Text).ToString("dd-MMM-yyyy hh:mm:ss")
                e.Item.Cells(1).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub cvalPageError_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageError.PreRender
        'If Me.cvalPageError.IsValid = False Then
        '    Sahassa.AML.Commonly.SessionErrorMessageCount = Sahassa.AML.Commonly.SessionErrorMessageCount + 1
        '    ClientScript.RegisterStartupScript(Page.GetType, "ErrorMessage" & Sahassa.AML.Commonly.SessionErrorMessageCount.ToString, "alert('There was uncompleteness in this page: \n" & Me.cvalPageError.ErrorMessage.Replace("'", "\'").Replace(vbCrLf, "\n").Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">") & "');", True)
        'End If
    End Sub


    Protected Sub GridLTKTApprovalList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridLTKTApprovalList.ItemCommand
        If e.CommandName.ToLower = "detail" Then
            Response.Redirect("LTKTApprovalDetail.aspx?Approval_ID=" & e.Item.Cells(0).Text)
        End If
    End Sub
End Class
