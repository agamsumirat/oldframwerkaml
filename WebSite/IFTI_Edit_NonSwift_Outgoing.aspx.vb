#Region "Imports..."
Imports Sahassanettier.Data

Imports Sahassanettier.Entities
Imports System.Collections.Generic

Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports AMLBLL.DataType

Imports Sahassa.AML.Commonly
#End Region
Partial Class IFTI_Edit_NonSwift_Outgoing
    Inherits Parent
#Region "properties..."
    ReadOnly Property getIFTIPK() As Integer
        Get
            If Session("IFTIEdit.IFTIPK") = Nothing Then
                Session("IFTIEdit.IFTIPK") = CInt(Request.Params("ID"))
            End If
            Return Session("IFTIEdit.IFTIPK")
        End Get
    End Property
    Private Property SetnGetSenderAccount() As String
        Get
            Return IIf(Session("IFTIEdit.SenderAccount") Is Nothing, "", Session("IFTIEdit.SenderAccount"))
        End Get
        Set(ByVal Value As String)
            Session("IFTIEdit.SenderAccount") = Value
        End Set
    End Property

    'ReadOnly Property getSenderAccount() As Integer
    '    Get
    '        If Session("IFTIEdit.SenderAccount") = Nothing Then
    '            Session("IFTIEdit.SenderAccount") = CInt(Request.Params("ID"))
    '        End If
    '        Return Session("IFTIEdit.SenderAccount")
    '    End Get
    'End Property
#End Region
#Region "Validation"
#Region "Validation Sender"
    Public Function IsValidate() As Boolean
        Dim result As Boolean = True
        ValidasiControl()
        '============ Insert Detail Approval
        Using objIfti_ApprovalDetail As New IFTI_Approval_Detail()
            Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)

                Dim senderType As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID

                'cekPengirim
                Select Case (senderType)
                    Case 1
                        'pengirimNasabahIndividu
                        validasiSender1()
                    Case 2
                        'pengirimNasabahKorporasi
                        validasiSender2()
                    Case 3
                        'PengirimNonNasabah
                        validasiSender3()
                End Select
                'cekBOwner
                If Not IsNothing(objIfti.FK_IFTI_BeneficialOwnerType_ID) Then
                    Dim BOwnerID As Integer = objIfti.FK_IFTI_BeneficialOwnerType_ID
                    Select Case (BOwnerID)
                        Case 1
                            'Nasabah
                            ValidasiBOwnerNasabah()
                        Case 2
                            'NonNasabah
                            ValidasiBOwnerNonNasabah()
                    End Select
                End If
                'cekPenerima
                Using objReceiver As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
                    Dim TipePenerima As Integer = objReceiver(0).FK_IFTI_NasabahType_ID
                    Select Case (TipePenerima)
                        Case 1
                            validasiReceiverInd()
                        Case 2
                            validasiReceiverKorp()
                        Case 3
                            validasiReceiverNonNasabah()
                    End Select
                End Using
                'cekTransaksi
                validasiTransaksi()
            End Using
        End Using
        Return result
    End Function
    Sub ValidasiControl()
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        If Me.Rb_Umum_NonSwiftOut_jenislaporan.SelectedValue <> "1" And ObjectAntiNull(TxtUmum_NonSwiftOutLTDLNKoreksi.Text) = False Then Throw New Exception("No. LTDLN Koreksi harus diisi  ")
        If ObjectAntiNull(TxtUmum_NonSwiftOutTanggalLaporan.Text) = False Then Throw New Exception("Tanggal Laporan harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtUmum_NonSwiftOutTanggalLaporan.Text) = False Then
            Throw New Exception("Tanggal Laporan tidak valid")
        End If
        If ObjectAntiNull(TxtUmum_NonSwiftOutPJKBankPelapor.Text) = False Or TxtUmum_NonSwiftOutPJKBankPelapor.Text.Length < 3 Then Throw New Exception("Nama PJK Bank Pelapor harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtUmum_NonSwiftOutNamaPejabatPJKBankPelapor.Text) = False Or TxtUmum_NonSwiftOutNamaPejabatPJKBankPelapor.Text.Length < 3 Then Throw New Exception("Nama Pejabat PJK Bank Pelapor harus diisi lebih dari 2 karakter! ")
        If Rb_Umum_NonSwiftOut_jenislaporan.SelectedIndex = -1 Then Throw New Exception("Jenis Laporan harus dipilih  ")

    End Sub
    Sub validasiSender1()
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        'senderIndividu
        'If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Tipe PJK Bank harus diisi  ")
        If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Tipe Pengirim harus diisi  ")
        If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Tipe Nasabah harus diisi  ")

        If ObjectAntiNull(TxtIdenNonSwiftOutPengirim_Norekening.Text) = False Or TxtIdenNonSwiftOutPengirim_Norekening.Text.Length < 3 Then Throw New Exception("Pengirim Nasabah Perorangan: No Rekening harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_NamaLengkap.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Nama Lengkap harus diisi  ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_TanggalLahir.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Tanggal Lahir harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtIdenNonSwiftOutPengirimNas_Ind_TanggalLahir.Text) = False Then
            Throw New Exception("Pengirim Nasabah Perorangan :Tanggal Lahir tidak valid")
        End If
        If RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan.SelectedValue = "2" Then
            If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Negara.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Negara harus diisi  ")
            If Me.hfIdenNonSwiftOutPengirimNas_Ind_Negara.Value = ParameterNegara.MsSystemParameter_Value Then
                If ObjectAntiNull(Me.TxtIdenNonSwiftOutPengirimNas_Ind_NegaraLain.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Negara Lain harus diisi  ")
            End If
            'If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Negara.Text) = True And ObjectAntiNull(Me.TxtIdenNonSwiftOutPengirimNas_Ind_NegaraLain.Text) = True Then Throw New Exception("Pengirim Nasabah Perorangan: Negara harus diisi salah satu ")
        End If
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaan.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Pekerjaan harus diisi  ")
        If Me.hfIdenNonSwiftOutPengirimNas_Ind_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
            If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaanlain.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Pekerjaan Lain harus diisi  ")
        End If
        'If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaan.Text) = True And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaanlain.Text) = True Then Throw New Exception("Pengirim Nasabah Perorangan: Pekerjaan harus diisi salah satu ")
        'alamat
        'dom
        'If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Kota.Text) = True And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_kotalain.Text) = True Then Throw New Exception("Pengirim Nasabah Perorangan: Kota/Kabupaten harus diisi Salah satu  ")
        'If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_provinsi.Text) = True And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_provinsiLain.Text) = True Then Throw New Exception("Pengirim Nasabah Perorangan: Provinsi harus diisi salah satu ")
        'optional
        '--------------------------
        'If ObjectAntiNull(hfIdenNonSwiftOutPengirimNas_Ind_Kota_dom.Value) = True Then
        '    If Me.hfIdenNonSwiftOutPengirimNas_Ind_Kota_dom.Value = ParameterKota.MsSystemParameter_Value Then
        '        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_kotalain.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Kota/Kabupaten Domisili Lain harus diisi  ")
        '    End If
        'End If
        'If ObjectAntiNull(hfIdenNonSwiftOutPengirimNas_Ind_provinsi_dom) = True Then
        '    If Me.hfIdenNonSwiftOutPengirimNas_Ind_provinsi_dom.Value = ParameterProvinsi.MsSystemParameter_Value Then
        '        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_provinsi.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Provinsi Domisili Lain harus diisi  ")
        '    End If
        'End If
        '-------------------------------------------------

        'iden
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_alamatIdentitas.Text) = False Or TxtIdenNonSwiftOutPengirimNas_Ind_alamatIdentitas.Text.Length < 3 Then Throw New Exception("Pengirim Nasabah Perorangan: Alamat Identitas harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Kota/Kabupaten harus diisi  ")
        If Me.hfIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Value = ParameterKota.MsSystemParameter_Value Then
            If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Kota/Kabupaten Lain harus diisi  ")
        End If
        'If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Text) = True And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Text) = True Then Throw New Exception("Pengirim Nasabah Perorangan: Kota/Kabupaten harus diisi Salah satu  ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Provinsi harus diisi  ")
        If Me.hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
            If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Provinsi Lain harus diisi  ")
        End If
        'If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Text) = True And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Text) = True Then Throw New Exception("Pengirim Nasabah Perorangan: Provinsi harus diisi salah satu ")
        If cboNonSwiftOutPengirimNasInd_jenisidentitas.SelectedIndex = -1 Then Throw New Exception("Pengirim Nasabah Perorangan:Jenis Dokumen Identitas harus dipilih ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_noIdentitas.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Nomor Identitas harus diisi  ")
        If TxtIdenNonSwiftOutPengirimNas_Ind_noIdentitas.Text <> "" Then
            If Me.cboNonSwiftOutPengirimNasInd_jenisidentitas.SelectedIndex > 0 Then
                If Me.cboNonSwiftOutPengirimNasInd_jenisidentitas.SelectedValue = 1 Or Me.cboNonSwiftOutPengirimNasInd_jenisidentitas.SelectedValue = 2 Then
                    If Not Regex.Match(TxtIdenNonSwiftOutPengirimNas_Ind_noIdentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                        Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                    End If
                Else
                    If Not Regex.Match(TxtIdenNonSwiftOutPengirimNas_Ind_noIdentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                        Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                    End If
                End If

            End If
           

        End If
    End Sub
    Sub validasiSender2()
        'senderKorp
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        Dim ParameterBentukBadanUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BentukBadanUsaha)
        ' If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Tipe PJK Bank harus diisi  ")
        If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Tipe Pengirim harus diisi  ")
        If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Tipe Nasabah harus diisi  ")

        If ObjectAntiNull(TxtIdenNonSwiftOutPengirim_Norekening.Text) = False Then Throw New Exception("Pengirim Nasabah Korporasi: No Rekening harus diisi  ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_NamaKorp.Text) = False Or TxtIdenNonSwiftOutPengirimNas_Corp_NamaKorp.Text.Length < 3 Then Throw New Exception("Pengirim Nasabah Korporasi:Nama Korporasi harus diisi minimal 3 karakter! ")
        'If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Text) = False Then Throw New Exception("Pengirim Nasabah Korporasi: Bidang Usaha Korporasi harus diisi  ")
        'optional
        '-----------------------------------
        'If cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.SelectedIndex <> 0 Then
        '    If cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.SelectedValue = ParameterBentukBadanUsaha.MsSystemParameter_Value Then
        '        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsahaLain.Text) = False Then Throw New Exception("Pengirim Nasabah Korporasi: Bentuk Usaha Lain Korporasi diisi! ")
        '    End If
        'End If
        '----------------------------------------
        If ObjectAntiNull(hfIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Value) = False Then Throw New Exception("Pengirim Nasabah Korporasi: Bidang Usaha Korporasi harus diisi  ")
        If hfIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
            If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorpLainnya.Text) = False Then Throw New Exception("Pengirim Nasabah Korporasi: Bidang Usaha Korporasi Lain harus diisi  ")
        End If

        'alamat
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_alamatkorp.Text) = False Or TxtIdenNonSwiftOutPengirimNas_Corp_alamatkorp.Text.Length < 3 Then Throw New Exception("Pengirim Nasabah Korporasi: Alamat Identitas harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_kotakorp.Text) = False Then Throw New Exception("Pengirim Nasabah Korporasi: Kota/Kabupaten harus diisi  ")
        If Me.hfIdenNonSwiftOutPengirimNas_Corp_kotakorp.Value = ParameterKota.MsSystemParameter_Value Then
            If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_kotakorplain.Text) = False Then Throw New Exception("Pengirim Nasabah Korporasi: Kota/Kabupaten Lain harus diisi  ")
        End If
        'If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_kotakorp.Text) = True And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_kotakorplain.Text) = True Then Throw New Exception("Pengirim Nasabah Korporasi: Kota/Kabupaten harus diisi salah satu ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_provKorp.Text) = False Then Throw New Exception("Pengirim Nasabah Korporasi: Provinsi harus diisi  ")
        If Me.hfIdenNonSwiftOutPengirimNas_Corp_provKorp.Value = ParameterProvinsi.MsSystemParameter_Value Then
            If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_provKorpLain.Text) = False Then Throw New Exception("Pengirim Nasabah Korporasi: Provinsi Lain harus diisi  ")
        End If
        'If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_provKorp.Text) = True And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_provKorpLain.Text) = True Then Throw New Exception("Pengirim Nasabah Korporasi: Provinsi harus diisi salah satu ")
    End Sub
    Sub validasiSender3()
        'senderNOnNasabah
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        ' If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Tipe PJK Bank harus diisi  ")
        'If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Pengirim Nasabah :Tipe Nasabah harus diisi  ")

        If RbNonSwiftOutPengirimNonNasabah_100Juta.SelectedIndex = -1 Then Throw New Exception("Pengirim Non Nasabah: Transaksi harus diisi  ")
        If ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_nama.Text) = False Or TxtNonSwiftOutPengirimNonNasabah_nama.Text.Length < 3 Then Throw New Exception("Pengirim Non Nasabah: Nama Lengkap harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_TanggalLahir.Text) = True Then
            If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtNonSwiftOutPengirimNonNasabah_TanggalLahir.Text) = False Then
                Throw New Exception("Pengirim Non Nasabah :Tanggal Lahir tidak valid")
            End If
        End If

        If ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_alamatiden.Text) = False Or TxtNonSwiftOutPengirimNonNasabah_alamatiden.Text.Length < 3 Then Throw New Exception("Pengirim Non Nasabah: Alamat harus diisi lebih dari 2 karakter! ")
        'kota
        If ObjectAntiNull(hfNonSwiftOutPengirimNonNasabah_kotaIden.Value) = True Then
            If Me.hfNonSwiftOutPengirimNonNasabah_kotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                If ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_kotaIden.Text) = False Then Throw New Exception("Pengirim Non Nasabah : Kota/Kabupaten Lain harus diisi  ")
            End If
        End If

        'Provinsi
        If ObjectAntiNull(hfNonSwiftOutPengirimNonNasabah_ProvIden.Value) = True Then
            If Me.hfNonSwiftOutPengirimNonNasabah_ProvIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                If ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_ProvIden.Text) = False Then Throw New Exception("Pengirim Non Nasabah : Provinsi Lain harus diisi  ")
            End If
        End If

        'If ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_NomorIden.Text) = False Then Throw New Exception("Pengirim Non Nasabah:Nomor Identitas harus diisi  ")
        If TxtNonSwiftOutPengirimNonNasabah_NomorIden.Text <> "" Then
            If Me.CboNonSwiftOutPengirimNonNasabah_JenisDokumen.SelectedIndex > 0 Then
                If Me.CboNonSwiftOutPengirimNonNasabah_JenisDokumen.SelectedValue = 1 Or Me.CboNonSwiftOutPengirimNonNasabah_JenisDokumen.SelectedValue = 2 Then
                    If Not Regex.Match(TxtNonSwiftOutPengirimNonNasabah_NomorIden.Text.Trim(), "^[0-9]*$").Success Then
                        Throw New Exception("Nomor Identitas hanya boleh menggunakan angka!")
                    End If
                Else
                    If Not Regex.Match(TxtNonSwiftOutPengirimNonNasabah_NomorIden.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                        Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                    End If
                End If
            Else
                Throw New Exception("Pengirim Non Nasabah : Jenis Nomor Identitas harus diisi  ")
            End If
        End If
        If RbNonSwiftOutPengirimNonNasabah_100Juta.SelectedValue = 2 Then
            If CboNonSwiftOutPengirimNonNasabah_JenisDokumen.SelectedIndex = -1 Then Throw New Exception("Pengirim Non Nasabah: Jenis Dokumen Identitas harus dipilih! ")
            If TxtNonSwiftOutPengirimNonNasabah_NomorIden.Text = "" Then Throw New Exception("Pengirim Non Nasabah: Nomor Identitas harus diisi! ")
        End If
    End Sub
#End Region
#Region "Validation Receiver"
    Sub validasiReceiverInd()
        'penerimaIndividu
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        If RBNonSwiftOutPenerimaNasabah_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Penerima  : Tipe Pengirim harus diisi  ")
        If Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Penerima  :Tipe Nasabah harus diisi  ")

        If ObjectAntiNull(txtPenerimaNonSwiftOut_rekening.Text) = False Or txtPenerimaNonSwiftOut_rekening.Text.Length < 3 Then Throw New Exception("Penerima  : Nomor rekening harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_IND_nama.Text) = False Or txtPenerimaNonSwiftOutNasabah_IND_nama.Text.Length < 3 Then Throw New Exception("Penerima Nasabah Individu: Nama Lengkap harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_IND_TanggalLahir.Text) = True Then
            If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtPenerimaNonSwiftOutNasabah_IND_TanggalLahir.Text) = False Then
                Throw New Exception("Penerima Nasabah Individu: Tanggal Lahir tidak valid")
            End If
        End If
        If Me.RbNonSwiftOutPenerimaNasabah_IND_Warganegara.SelectedValue = "2" Then
            If ObjectAntiNull(Me.txtPenerimaNonSwiftOutNasabah_IND_negara.Text) = False Then Throw New Exception("Penerima Nasabah Individu: Negara harus diisi  ")
            If Me.hfNonSwiftOutPengirimNasabah_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                If ObjectAntiNull(Me.txtPenerimaNonSwiftOutNasabah_IND_negaraLain.Text) = False Then Throw New Exception("Penerima Nasabah Individu: Negara Lain harus diisi  ")
            End If
            'If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Negara.Text) = True And ObjectAntiNull(Me.TxtIdenNonSwiftOutPengirimNas_Ind_NegaraLain.Text) = True Then Throw New Exception("Pengirim Nasabah Perorangan: Negara harus diisi salah satu ")
        End If
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_IND_alamatIden.Text) = False Or txtPenerimaNonSwiftOutNasabah_IND_alamatIden.Text.Length < 3 Then Throw New Exception("Penerima Nasabah Individu: Alamat Identitas harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_IND_negaraIden.Text) = False Then Throw New Exception("Penerima Nasabah Individu: Negara harus diisi  ")
        If Me.hfNonSwiftOutPengirimNasabah_IND_negaraIden.Value = ParameterNegara.MsSystemParameter_Value Then
            If ObjectAntiNull(Me.txtPenerimaNonSwiftOutNasabah_IND_negaraLainIden.Text) = False Then Throw New Exception("Penerima Nasabah Individu: Negara Lain harus diisi  ")
        End If
        'If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_IND_negaraIden.Text) = True And ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_IND_negaraLainIden.Text) = True Then Throw New Exception("Penerima Nasabah Individu: Negara harus diisi salah satu ")
        If txtPenerimaNonSwiftOutNasabah_IND_NomorIden.Text <> "" Then
            If Me.CBONonSwiftOutPenerimaNasabah_IND_JenisIden.SelectedIndex > 0 Then
                If Me.CBONonSwiftOutPenerimaNasabah_IND_JenisIden.SelectedValue = 1 Or Me.CBONonSwiftOutPenerimaNasabah_IND_JenisIden.SelectedValue = 1 Then
                    If Not Regex.Match(txtPenerimaNonSwiftOutNasabah_IND_NomorIden.Text.Trim(), "^[0-9]*$").Success Then
                        Throw New Exception("Nomor Identitas hanya boleh menggunakan angka!")
                    End If
                Else
                    If Not Regex.Match(txtPenerimaNonSwiftOutNasabah_IND_NomorIden.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                        Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                    End If
                End If
            Else
                Throw New Exception("Jenis Identitas harus diisi!")
            End If
            
            
        End If
    End Sub
    Sub validasiReceiverKorp()
        'pennerimaKorporasi
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        Dim ParameterBentukBadanUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BentukBadanUsaha)

        If ObjectAntiNull(txtPenerimaNonSwiftOut_rekening.Text) = False Or txtPenerimaNonSwiftOut_rekening.Text.Length < 3 Then Throw New Exception(" Nomor rekening harus diisi lebih dari 2 karakter! ")
        If RBNonSwiftOutPenerimaNasabah_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Tipe Pengirim harus diisi  ")
        If Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Tipe Nasabah harus diisi  ")
        'optional
        '---------------
        'If cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain.SelectedIndex <> 0 Then
        '    If cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain.SelectedValue = ParameterBentukBadanUsaha.MsSystemParameter_Value Then
        '        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLainnya.Text) = False Then Throw New Exception("Penerima Nasabah Korporasi: Bentuk badan usaha Lain harus diisi! ")
        '    End If
        'End If

        'bidangusaha
        'If ObjectAntiNull(hfNonSwiftOutPengirimNasabah_Korp_BidangUsahaKorp) = True Then
        '    If Me.hfNonSwiftOutPengirimNasabah_Korp_BidangUsahaKorp.Value = ParameterNegara.MsSystemParameter_Value Then
        '        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_BidangUsahaKorp.Text) = False Then Throw New Exception("Penerima Nasabah Korporasi: Bidang usaha Lain harus diisi  ")
        '    End If
        'End If
        '-------------
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_namaKorp.Text) = False Or txtPenerimaNonSwiftOutNasabah_Korp_namaKorp.Text.Length < 3 Then Throw New Exception("Penerima Nasabah Korporasi: Nama Korporasi harus diisi lenih dari 2 karakter! ")
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_AlamatKorp.Text) = False Or txtPenerimaNonSwiftOutNasabah_Korp_AlamatKorp.Text.Length < 3 Then Throw New Exception("Penerima Nasabah Korporasi: Alamat Identitas harus diisi lebih dari 2 karakter! ")
        'If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_BidangUsahaKorp.Text) = True And ObjectAntiNull(txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text) = True Then Throw New Exception("Penerima Nasabah Korporasi: Bidang Usaha Lainnya harus diisi salah satu saja! ")

        If ObjectAntiNull(hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Value) = False Then Throw New Exception("Penerima Nasabah Korporasi: Negara harus diisi  ")
        If Me.hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Value = ParameterNegara.MsSystemParameter_Value Then
            If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_NegaraLainnyaKorp.Text) = False Then Throw New Exception("Penerima Nasabah Korporasi: Negara Lain harus diisi  ")
        End If
        'If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Text) = True And ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_NegaraLainnyaKorp.Text) = True Then Throw New Exception("Penerima Nasabah Korporasi: Negara harus diisi salah satu ")
    End Sub
    Sub validasiReceiverNonNasabah()
        'penerimaNOnNasabah
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        If RBNonSwiftOutPenerimaNasabah_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Penerima Non Nasabah: Tipe Pengirim harus diisi  ")
        If txtPenerimaNonSwiftOutNonNasabah_KodeRahasia.Text = "" And txtPenerimaNonSwiftOutNonNasabah_rekening.Text = "" Then Throw New Exception("Kode Rahasia atau No Rekening minimal harus diisi salah satu! ")
        If txtPenerimaNonSwiftOutNonNasabah_rekening.Text <> "" And txtPenerimaNonSwiftOutNonNasabah_namabank.Text = "" Then Throw New Exception("Nama Bank harus diisi apabila no Rekening diisi! ")

        If ObjectAntiNull(txtPenerimaNonSwiftOutNonNasabah_Nama.Text) = False Or txtPenerimaNonSwiftOutNonNasabah_Nama.Text.Length < 3 Then Throw New Exception("Penerima Non Nasabah: Nama Lengkap harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(hfSwiftInNonSwiftOutPengirimNonNasabah_Negara.Value) = False Then Throw New Exception("Penerima Non Nasabah: Negara harus diisi  ")
        If Me.hfSwiftInNonSwiftOutPengirimNonNasabah_Negara.Value = ParameterNegara.MsSystemParameter_Value Then
            If ObjectAntiNull(txtPenerimaNonSwiftOutNonNasabah_negaraLain.Text) = False Then Throw New Exception("Penerima Non Nasabah: Negara Lain harus diisi  ")
        End If
        'If ObjectAntiNull(txtPenerimaNonSwiftOutNonNasabah_Negara.Text) = True And ObjectAntiNull(txtPenerimaNonSwiftOutNonNasabah_negaraLain.Text) = True Then Throw New Exception("Penerima Non Nasabah: Negara harus diisi salah satu saja ")
    End Sub
#End Region
    Sub validasiTransaksi()
        Dim ParameterMataUang As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        If ObjectAntiNull(Transaksi_NonSwiftOut_tanggal.Text) = False Then Throw New Exception("Tanggal Transaksi harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", Transaksi_NonSwiftOut_tanggal.Text) = False Then
            Throw New Exception("Tanggal Transaksi tidak valid")
        End If
        If ObjectAntiNull(Transaksi_NonSwiftOut_ValueTanggalTransaksi.Text) = False Then Throw New Exception("Tanggal Transaksi(value date) harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", Transaksi_NonSwiftOut_ValueTanggalTransaksi.Text) = False Then
            Throw New Exception("Tanggal Transaksi(value date) tidak valid")
        End If
        If ObjectAntiNull(Transaksi_NonSwiftOut_nilaitransaksi.Text) = False Then Throw New Exception("Nilai Transaksi harus diisi  ")
        If ObjectAntiNull(Transaksi_NonSwiftOut_MataUangTransaksi.Text) = False Then Throw New Exception("Mata Uang Transaksi harus diisi! ")
        If Me.hfTransaksi_NonSwiftOut_MataUangTransaksi.Value = ParameterMataUang.MsSystemParameter_Value Then
            If ObjectAntiNull(Transaksi_NonSwiftOut_MataUangTransaksiLain.Text) = False Then Throw New Exception("Mata Uang Lain Transaksi harus diisi! ")
        End If
        'If ObjectAntiNull(Transaksi_NonSwiftOut_MataUangTransaksi.Text) = True And ObjectAntiNull(Transaksi_NonSwiftOut_MataUangTransaksiLain.Text) = True Then Throw New Exception("Mata Uang Transaksi harus diisi salah satu saja! ")
        If ObjectAntiNull(Transaksi_NonSwiftOut_AmountdalamRupiah.Text) = False Then Throw New Exception("Amount dalam rupiah harus diisi  ")
        If Not IsValidDecimal(15, 2, Transaksi_NonSwiftOut_AmountdalamRupiah.Text) Then Throw New Exception("Amount dalam rupiah harus diisi angka ")
        'If ObjectAntiNull(Transaksi_NonSwiftOut_currency.Text) = True And ObjectAntiNull(Transaksi_NonSwiftOut_currencyLain.Text) = True Then Throw New Exception("Mata Uang yang diinstruksikan harus diisi salah satu saja! ")
        If ObjectAntiNull(Transaksi_NonSwiftOut_instructedAmount.Text) = True Then
            'Dim objresult As Decimal
            If Not IsValidDecimal(15, 2, Transaksi_NonSwiftOut_instructedAmount.Text) Then Throw New Exception("Nilai Transaksi Keuangan yang diinstruksikan harus diisi angka ")
            'If Not IsNumeric(Transaksi_NonSwiftOut_instructedAmount.Text) Then Throw New Exception("Amount dalam rupiah harus diisi angka ")
        End If
        'If Me.Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedIndex > 0 Then

 
        If Me.Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1 And ObjectAntiNull(Me.Transaksi_NonSwiftOut_SumberPenggunaanDana.Text) = False Then Throw New Exception("Sumber Penggunaan Dana harus diisi! ")
        If Me.Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 2 And CInt(Me.Transaksi_NonSwiftOut_AmountdalamRupiah.Text) > 100000000 And ObjectAntiNull(Me.Transaksi_NonSwiftOut_SumberPenggunaanDana.Text) = False Then Throw New Exception("Sumber Penggunaan Dana harus diisi! ")


        'If ObjectAntiNull(Transaksi_NonSwiftOut_SumberPenggunaanDana.Text) = True And Not IsValidDecimal(15, 2, Transaksi_NonSwiftOut_SumberPenggunaanDana.Text) Then Throw New Exception("Sumber Penggunaan Dana tidak valid!")
        'If ObjectAntiNull(Transaksi_NonSwiftOut_SumberPenggunaanDana.Text) = False And Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1 And CInt(Transaksi_NonSwiftOut_AmountdalamRupiah.Text) > 100000000 Then Throw New Exception("Sumber Penggunaan Dana harus diisi  ")
        'End If
    End Sub
    Sub ValidasiBOwnerNasabah()
        'parameter value
        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_rekening.Text) = False Or BenfOwnerNonSwiftOutNasabah_rekening.Text.Length < 3 Then Throw New Exception("Identitas Beneficial Owner Nasabah: No Rekening harus diisi minimal 3 karakter!  ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_Nama.Text) = False Or BenfOwnerNonSwiftOutNasabah_Nama.Text.Length < 3 Then Throw New Exception("Identitas Beneficial Owner Nasabah: Nama Lengkap harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_tanggalLahir.Text) = False Then Throw New Exception("Beneficiary Owner: Tanggal Lahir harus diisi!")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_tanggalLahir.Text) = True Then
            If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", BenfOwnerNonSwiftOutNasabah_tanggalLahir.Text) = False Then
                Throw New Exception("Beneficiary Owner: Tanggal Lahir tidak valid")
            End If
        End If
        'If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_KotaIden.Text) = False And ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_kotaLainIden.Text) = False Then Throw New Exception("Beneficiary Owner: Kota/Kabupaten harus diisi! ")
        'If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_ProvinsiIden.Text) = False And ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_ProvinsiLainIden.Text) = False Then Throw New Exception("Beneficiary Owner: Provinsi harus diisi! ")



        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_AlamatIden.Text) = False Or BenfOwnerNonSwiftOutNasabah_AlamatIden.Text.Length < 3 Then Throw New Exception("Identitas Beneficial Owner Nasabah: Alamat harus diisi lebih dari 2 karakter!")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_KotaIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Kota/Kabupaten harus diisi! ")
        If Me.hfBenfOwnerNonSwiftOutNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
            If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_kotaLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Kota/Kabupaten Lain harus diisi! ")
        End If
        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_ProvinsiIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Provinsi harus diisi! ")
        If Me.hfBenfOwnerNonSwiftOutNasabah_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
            If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_ProvinsiLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Provinsi Lain harus diisi! ")
        End If
        'If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_KotaIden.Text) = True And ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_kotaLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Nasabah: Kota/Kabupaten harus diisi salah satu saja ")
        'If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_ProvinsiIden.Text) = True And ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_ProvinsiLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Nasabah: Provinsi harus diisi salah satu saja ")
        If CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.SelectedIndex = 0 Then Throw New Exception("Identitas Beneficial Owner Nasabah: Jenis Dokumen identitas harus diisi! ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_NomorIden.Text) = True And BenfOwnerNonSwiftOutNasabah_NomorIden.Text.Length < 3 Then Throw New Exception("Identitas Beneficial Owner Nasabah: Nomor Identitas harus diisi! ")
        If BenfOwnerNonSwiftOutNasabah_NomorIden.Text <> "" Then
            
            If Me.CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.SelectedValue = 1 Or Me.CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.SelectedValue = 2 Then
                If Not Regex.Match(BenfOwnerNonSwiftOutNasabah_NomorIden.Text.Trim(), "^[0-9]*$").Success Then
                    Throw New Exception("Nomor Identitas hanya boleh menggunakan angka!")
                End If
            Else
                If Not Regex.Match(BenfOwnerNonSwiftOutNasabah_NomorIden.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                    Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                End If
            End If
        End If
    End Sub
    Sub ValidasiBOwnerNonNasabah()
        'parameter value
        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_Nama.Text) = False Or BenfOwnerNonSwiftOutNonNasabah_Nama.Text.Length < 3 Then Throw New Exception("Identitas Beneficial Owner No Nasabah: Nama Lengkap harus diisi!")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_TanggalLahir.Text) = True Then
            If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", BenfOwnerNonSwiftOutNonNasabah_TanggalLahir.Text) = False Then
                Throw New Exception("Beneficiary Owner: Tanggal Transaksi tidak valid")
            End If
        End If
        'If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_KotaIden.Text) = False And ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_KotaLainIden.Text) = False Then Throw New Exception("Beneficiary Owner: Kota/Kabupaten harus diisi! ")
        'If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Text) = False And ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Text) = False Then Throw New Exception("Beneficiary Owner: Provinsi harus diisi! ")


        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_AlamatIden.Text) = False Or BenfOwnerNonSwiftOutNonNasabah_AlamatIden.Text.Length < 3 Then Throw New Exception("Identitas Beneficial Owner No Nasabah: Alamat harus diisi!")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_KotaIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Kota/Kabupaten harus diisi! ")
        If hfBenfOwnerNonSwiftOutNonNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
            If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_KotaLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Kota/Kabupaten Lain harus diisi! ")
        End If
        'If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_KotaIden.Text) = True And ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_KotaLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Kota/Kabupaten harus diisi salah satu saja ")
        'If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Text) = True And ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Provinsi harus diisi salah satu saja ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Provinsi harus diisi! ")
        If Me.hfBenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
            If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Provinsi Lain harus diisi! ")
        End If

        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_NomorIdentitas.Text) = True And BenfOwnerNonSwiftOutNonNasabah_NomorIdentitas.Text.Length < 3 Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Nomor Identitas harus diisi! ")
        If BenfOwnerNonSwiftOutNonNasabah_NomorIdentitas.Text <> "" Then
            If Me.CBOBenfOwnerNonSwiftOutNonNasabah_JenisDokumen.SelectedIndex > 0 Then
                If Me.CBOBenfOwnerNonSwiftOutNonNasabah_JenisDokumen.SelectedValue = 1 Or Me.CBOBenfOwnerNonSwiftOutNonNasabah_JenisDokumen.SelectedValue = 2 Then
                    If Not Regex.Match(BenfOwnerNonSwiftOutNonNasabah_NomorIdentitas.Text.Trim(), "^[0-9]*$").Success Then
                        Throw New Exception("Nomor Identitas hanya boleh menggunakan angka!")
                    End If
                Else
                    If Not Regex.Match(BenfOwnerNonSwiftOutNonNasabah_NomorIdentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                        Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                    End If
                End If
            Else
                Throw New Exception("Jenis Identitas harus diisi!")
            End If
        End If
    End Sub
#End Region
#Region "Save"
    Sub SaveNonSwiftOut()
        Try
            Using OTrans As TransactionManager = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)

                Try
                    OTrans.BeginTransaction()
                    Page.Validate("handle")
                    'If IsValidate() Then
                    ValidasiControl()
                    '============ Insert Detail Approval
                    If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Tipe Pengirim harus diisi  ")
                    Dim tipepengirim As Integer = Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue
                    'If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedIndex > 0 Then
                    Dim tipenasabah As Integer
                    'End If

                    Dim senderType As Integer

                    If tipepengirim = 1 Then
                        If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Tipe Nasabah harus diisi  ")
                        tipenasabah = Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue
                        If tipenasabah = 1 Then
                            senderType = 1
                        Else
                            senderType = 2
                        End If
                    Else
                        senderType = 3
                    End If

                    'cekPengirim
                    Select Case (senderType)
                        Case 1
                            'pengirimNasabahIndividu
                            validasiSender1()
                        Case 2
                            'pengirimNasabahKorporasi
                            validasiSender2()
                        Case 3
                            'PengirimNonNasabah
                            validasiSender3()
                    End Select
                    'cekBOwner
                    If Rb_BOwnerNas_ApaMelibatkan.SelectedValue = 1 Then
                        If BenfOwnerNonSwiftOutHubunganPemilikDana.Text = "" Then Throw New Exception("Hubungan Pemilik dana harus diisi!")
                        If Rb_NonSwiftOutBOwnerNas_TipePengirim.SelectedValue > 0 Then
                            Dim BOwnerID As Integer = Rb_NonSwiftOutBOwnerNas_TipePengirim.SelectedValue
                            Select Case (BOwnerID)
                                Case 1
                                    'Nasabah
                                    ValidasiBOwnerNasabah()
                                Case 2
                                    'NonNasabah
                                    ValidasiBOwnerNonNasabah()
                            End Select
                        End If
                    End If
                    'cekPenerima
                    Dim tipepenerima As Integer = RBNonSwiftOutPenerimaNasabah_TipePengirim.SelectedValue
                    Dim tipenasabahPenerima As Integer
                    Dim receiverType As Integer
                    'parameter value
                    Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                    Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                    Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                    Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
                    Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

                    If tipepenerima = 1 Then
                        tipenasabahPenerima = Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedValue
                        If tipenasabahPenerima = 1 Then
                            validasiReceiverInd()
                            receiverType = 1
                        Else
                            validasiReceiverKorp()
                            receiverType = 2
                        End If
                    Else
                        validasiReceiverNonNasabah()
                        receiverType = 3
                    End If

                    'cek transaksi
                    validasiTransaksi()
                    'ValidasiControl()

                    ' =========== Insert Header Approval
                    Dim KeyHeaderApproval As Integer
                    Using objIftiApproval As New IFTI_Approval
                        With objIftiApproval
                            FillOrNothing(.FK_MsMode_Id, 2, False, oInt)
                            FillOrNothing(.RequestedBy, SessionPkUserId)
                            FillOrNothing(.RequestedDate, Date.Now)
                            .Fk_IFTI_Type_ID = 3
                            'FillOrNothing(.IsUpload, False)
                        End With
                        DataRepository.IFTI_ApprovalProvider.Save(OTrans, objIftiApproval)
                        KeyHeaderApproval = objIftiApproval.PK_IFTI_Approval_Id
                    End Using

                    '============ Insert Detail Approval
                    Using objIfti_ApprovalDetail As New IFTI_Approval_Detail()
                        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
                            With objIfti_ApprovalDetail
                                'FK
                                .PK_IFTI_ID_Old = objIfti.PK_IFTI_ID
                                .PK_IFTI_ID = objIfti.PK_IFTI_ID
                                .FK_IFTI_Approval_Id = KeyHeaderApproval
                                .FK_IFTI_Type_ID_Old = objIfti.FK_IFTI_Type_ID
                                .FK_IFTI_Type_ID = 3
                                'umum
                                '--old--
                                FillOrNothing(.LTDLNNo_Old, objIfti.LTDLNNo)
                                FillOrNothing(.LTDLNNoKoreksi_Old, objIfti.LTDLNNoKoreksi)
                                FillOrNothing(.TanggalLaporan_Old, objIfti.TanggalLaporan)
                                FillOrNothing(.NamaPJKBankPelapor_Old, objIfti.NamaPJKBankPelapor)
                                FillOrNothing(.NamaPejabatPJKBankPelapor_Old, objIfti.NamaPejabatPJKBankPelapor)
                                FillOrNothing(.JenisLaporan_Old, objIfti.JenisLaporan)
                                '--new--
                                FillOrNothing(.LTDLNNo, Me.TxtUmum_NonSwiftOutLTDLN.Text, False, Ovarchar)
                                FillOrNothing(.LTDLNNoKoreksi, Me.TxtUmum_NonSwiftOutLTDLNKoreksi.Text, False, Ovarchar)
                                FillOrNothing(.TanggalLaporan, Me.TxtUmum_NonSwiftOutTanggalLaporan.Text, False, oDate)
                                FillOrNothing(.NamaPJKBankPelapor, Me.TxtUmum_NonSwiftOutPJKBankPelapor.Text, False, Ovarchar)
                                FillOrNothing(.NamaPejabatPJKBankPelapor, Me.TxtUmum_NonSwiftOutNamaPejabatPJKBankPelapor.Text, False, Ovarchar)
                                FillOrNothing(.JenisLaporan, Me.Rb_Umum_NonSwiftOut_jenislaporan.SelectedValue, False, oInt)

                                'cekPengirimOld
                                Dim senderOld As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID
                                Select Case (senderOld)
                                    Case 1
                                        'pengirimNasabahIndividu
                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                        FillOrNothing(.Sender_Nasabah_INDV_NoRekening_Old, objIfti.Sender_Nasabah_INDV_NoRekening)
                                        FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap_Old, objIfti.Sender_Nasabah_INDV_NamaLengkap)
                                        FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir_Old, objIfti.Sender_Nasabah_INDV_TanggalLahir)
                                        FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan_Old, objIfti.Sender_Nasabah_INDV_KewargaNegaraan)
                                        FillOrNothing(.Sender_Nasabah_INDV_Negara_Old, objIfti.Sender_Nasabah_INDV_Negara)
                                        FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya_Old, objIfti.Sender_Nasabah_INDV_NegaraLainnya)
                                        FillOrNothing(.Sender_Nasabah_INDV_Pekerjaan_Old, objIfti.Sender_Nasabah_INDV_Pekerjaan)
                                        FillOrNothing(.Sender_Nasabah_INDV_PekerjaanLainnya_Old, objIfti.Sender_Nasabah_INDV_PekerjaanLainnya)
                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_Alamat_Old, objIfti.Sender_Nasabah_INDV_ID_Alamat)
                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKab_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKab)
                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya)
                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_Propinsi_Old, objIfti.Sender_Nasabah_INDV_ID_Propinsi)
                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_PropinsiLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_PropinsiLainnya)
                                        FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat_Old, objIfti.Sender_Nasabah_INDV_ID_Alamat)
                                        FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKab_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKab)
                                        FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya)
                                        FillOrNothing(.Sender_Nasabah_INDV_ID_Propinsi_Old, objIfti.Sender_Nasabah_INDV_ID_Propinsi)
                                        FillOrNothing(.Sender_Nasabah_INDV_ID_PropinsiLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_PropinsiLainnya)
                                        FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType_Old, objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType)
                                        FillOrNothing(.Sender_Nasabah_INDV_NomorID_Old, objIfti.Sender_Nasabah_INDV_NomorID)

                                    Case 2
                                        'pengirimNasabahKorporasi
                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                        FillOrNothing(.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id_Old, objIfti.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id)
                                        FillOrNothing(.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old, objIfti.Sender_Nasabah_CORP_BentukBadanUsahaLainnya)
                                        FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi_Old, objIfti.Sender_Nasabah_CORP_NamaKorporasi)
                                        FillOrNothing(.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old, objIfti.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id)
                                        FillOrNothing(.Sender_Nasabah_CORP_BidangUsahaLainnya_Old, objIfti.Sender_Nasabah_CORP_BidangUsahaLainnya, False, oInt)
                                        FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap_Old, objIfti.Sender_Nasabah_CORP_AlamatLengkap)
                                        FillOrNothing(.Sender_Nasabah_COR_KotaKab_Old, objIfti.Sender_Nasabah_CORP_KotaKab)
                                        FillOrNothing(.Sender_Nasabah_COR_KotaKabLainnya_Old, objIfti.Sender_Nasabah_CORP_KotaKabLainnya)
                                        FillOrNothing(.Sender_Nasabah_CORP_Propinsi_Old, objIfti.Sender_Nasabah_CORP_Propinsi)
                                        FillOrNothing(.Sender_Nasabah_CORP_PropinsiLainnya_Old, objIfti.Sender_Nasabah_CORP_PropinsiLainnya)
                                        FillOrNothing(.Sender_Nasabah_CORP_LN_AlamatLengkap_Old, objIfti.Sender_Nasabah_CORP_LN_AlamatLengkap)
                                        FillOrNothing(.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim_Old, objIfti.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim)

                                    Case 3
                                        'PengirimNonNasabah

                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                        FillOrNothing(.FK_IFTI_NonNasabahNominalType_ID_Old, objIfti.FK_IFTI_NonNasabahNominalType_ID)
                                        FillOrNothing(.Sender_NonNasabah_NamaLengkap_Old, objIfti.Sender_NonNasabah_NamaLengkap)
                                        FillOrNothing(.Sender_NonNasabah_TanggalLahir_Old, objIfti.Sender_NonNasabah_TanggalLahir)
                                        FillOrNothing(.Sender_NonNasabah_ID_Alamat_Old, objIfti.Sender_NonNasabah_ID_Alamat)
                                        FillOrNothing(.Sender_NonNasabah_ID_KotaKab_Old, objIfti.Sender_NonNasabah_ID_KotaKab)
                                        FillOrNothing(.Sender_NonNasabah_ID_KotaKabLainnya_Old, objIfti.Sender_NonNasabah_ID_KotaKabLainnya)
                                        FillOrNothing(.Sender_NonNasabah_ID_Propinsi_Old, objIfti.Sender_NonNasabah_ID_Propinsi)
                                        FillOrNothing(.Sender_NonNasabah_ID_PropinsiLainnya_Old, objIfti.Sender_NonNasabah_ID_PropinsiLainnya)
                                        FillOrNothing(.Sender_NonNasabah_FK_IFTI_IDType_Old, objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType)
                                        FillOrNothing(.Sender_NonNasabah_NomorID_Old, objIfti.Sender_NonNasabah_NomorID)

                                End Select

                                'cekPengirimNew
                                Select Case (senderType)
                                    Case 1
                                        'pengirimNasabahIndividu
                                       
                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)
                                        FillOrNothing(.Sender_Nasabah_INDV_NoRekening, Me.TxtIdenNonSwiftOutPengirim_Norekening.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap, Me.TxtIdenNonSwiftOutPengirimNas_Ind_NamaLengkap.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir, Me.TxtIdenNonSwiftOutPengirimNas_Ind_TanggalLahir.Text, False, oDate)
                                        FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan, Me.RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan.SelectedValue, False, oInt)
                                        FillOrNothing(.Sender_Nasabah_INDV_Negara, Me.hfIdenNonSwiftOutPengirimNas_Ind_Negara.Value, False, oInt)
                                        If Me.RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan.SelectedValue = "2" Then
                                            If Me.hfIdenNonSwiftOutPengirimNas_Ind_Negara.Value = ParameterNegara.MsSystemParameter_Value Then
                                                FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_NegaraLain.Text, False, Ovarchar)
                                            Else
                                                FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Nothing)
                                            End If

                                        End If
                                        FillOrNothing(.Sender_Nasabah_INDV_Pekerjaan, Me.hfIdenNonSwiftOutPengirimNas_Ind_pekerjaan.Value, False, oInt)
                                        If Me.hfIdenNonSwiftOutPengirimNas_Ind_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
                                            FillOrNothing(.Sender_Nasabah_INDV_PekerjaanLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaanlain.Text, Ovarchar)
                                        Else
                                            FillOrNothing(.Sender_Nasabah_INDV_PekerjaanLainnya, Nothing)
                                        End If
                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_Alamat, Me.TxtIdenNonSwiftOutPengirimNas_Ind_Alamat.Text, False, Ovarchar)
                                        If ObjectAntiNull(Me.TxtIdenNonSwiftOutPengirimNas_Ind_Kota.Text) = True Then
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKab, Me.hfIdenNonSwiftOutPengirimNas_Ind_Kota_dom.Value, False, oInt)
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKabLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Text, False, Ovarchar)
                                        Else
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKab, Nothing)
                                            .Sender_Nasabah_INDV_DOM_KotaKabLainnya = ""
                                        End If
                                        If ObjectAntiNull(Me.TxtIdenNonSwiftOutPengirimNas_Ind_provinsi.Text) = True Then
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_Propinsi, Me.hfIdenNonSwiftOutPengirimNas_Ind_provinsi_dom.Value, False, oInt)
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_PropinsiLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_provinsiLain.Text, False, Ovarchar)
                                        Else
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_Propinsi, Nothing)
                                            .Sender_Nasabah_INDV_DOM_PropinsiLainnya = ""
                                        End If
                                        FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat, Me.TxtIdenNonSwiftOutPengirimNas_Ind_alamatIdentitas.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKab, Me.hfIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Value, False, oInt)
                                        If Me.hfIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Value = ParameterKota.MsSystemParameter_Value Then
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKabLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_kotalain.Text, False, Ovarchar)
                                        Else
                                            .Sender_Nasabah_INDV_ID_KotaKabLainnya = ""
                                        End If
                                        FillOrNothing(.Sender_Nasabah_INDV_ID_Propinsi, Me.hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Value, False, oInt)
                                        If Me.hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_PropinsiLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Text, False, Ovarchar)
                                        Else
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_PropinsiLainnya, Nothing)
                                        End If
                                        FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType, Me.cboNonSwiftOutPengirimNasInd_jenisidentitas.SelectedValue, False, oInt)
                                        FillOrNothing(.Sender_Nasabah_INDV_NomorID, Me.TxtIdenNonSwiftOutPengirimNas_Ind_noIdentitas.Text, False, Ovarchar)

                                    Case 2
                                        'pengirimNasabahKorporasi
                                       
                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)

                                        If cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.SelectedIndex > 0 Then
                                            FillOrNothing(.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, Me.cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.SelectedValue, False, Ovarchar)
                                        End If
                                        FillOrNothing(.Sender_Nasabah_CORP_BentukBadanUsahaLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsahaLain.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi, Me.TxtIdenNonSwiftOutPengirimNas_Corp_NamaKorp.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Value, False, oInt)
                                        If hfIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                                            FillOrNothing(.Sender_Nasabah_CORP_BidangUsahaLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorpLainnya.Text, False, oInt)
                                        Else
                                            .Sender_Nasabah_CORP_BidangUsahaLainnya = ""
                                        End If
                                        FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap, Me.TxtIdenNonSwiftOutPengirimNas_Corp_alamatkorp.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_COR_KotaKab, Me.hfIdenNonSwiftOutPengirimNas_Corp_kotakorp.Value, False, oInt)
                                        If Me.hfIdenNonSwiftOutPengirimNas_Corp_kotakorp.Value = ParameterKota.MsSystemParameter_Value Then
                                            FillOrNothing(.Sender_Nasabah_COR_KotaKabLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Text, False, Ovarchar)
                                        Else
                                            .Sender_Nasabah_COR_KotaKabLainnya = ""
                                        End If
                                        FillOrNothing(.Sender_Nasabah_CORP_Propinsi, Me.hfIdenNonSwiftOutPengirimNas_Corp_provKorp.Value, False, oInt)
                                        If hfIdenNonSwiftOutPengirimNas_Corp_provKorp.Value = ParameterProvinsi.MsSystemParameter_Value Then
                                            FillOrNothing(.Sender_Nasabah_CORP_PropinsiLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Corp_provKorpLain.Text, False, Ovarchar)
                                        Else
                                            .Sender_Nasabah_CORP_PropinsiLainnya = ""
                                        End If
                                        FillOrNothing(.Sender_Nasabah_CORP_LN_AlamatLengkap, Me.TxtIdenNonSwiftOutPengirimNas_Corp_AlamatLuar.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim, Me.TxtIdenNonSwiftOutPengirimNas_Corp_AlamatAsal.Text, False, Ovarchar)

                                    Case 3
                                        'PengirimNonNasabah
                                       
                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)
                                        FillOrNothing(.FK_IFTI_NonNasabahNominalType_ID, Me.RbNonSwiftOutPengirimNonNasabah_100Juta.SelectedValue, False, oInt)
                                        FillOrNothing(.Sender_NonNasabah_NamaLengkap, Me.TxtNonSwiftOutPengirimNonNasabah_nama.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_NonNasabah_TanggalLahir, Me.TxtNonSwiftOutPengirimNonNasabah_TanggalLahir.Text, False, oDate)
                                        FillOrNothing(.Sender_NonNasabah_ID_Alamat, Me.TxtNonSwiftOutPengirimNonNasabah_alamatiden.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_NonNasabah_ID_KotaKab, Me.hfNonSwiftOutPengirimNonNasabah_kotaIden.Value, False, oInt)
                                        If ObjectAntiNull(hfNonSwiftOutPengirimNonNasabah_kotaIden.Value) = True Then
                                            If hfNonSwiftOutPengirimNonNasabah_kotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                                                FillOrNothing(.Sender_NonNasabah_ID_KotaKabLainnya, Me.TxtNonSwiftOutPengirimNonNasabah_kotaIden.Text, False, Ovarchar)
                                            Else
                                                FillOrNothing(.Sender_NonNasabah_ID_KotaKabLainnya, Nothing)
                                            End If
                                        End If
                                        FillOrNothing(.Sender_NonNasabah_ID_Propinsi, Me.hfNonSwiftOutPengirimNonNasabah_ProvIden.Value, False, oInt)
                                        If ObjectAntiNull(hfNonSwiftOutPengirimNonNasabah_ProvIden.Value) = True Then
                                            If hfNonSwiftOutPengirimNonNasabah_ProvIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                                                FillOrNothing(.Sender_NonNasabah_ID_PropinsiLainnya, Me.hfNonSwiftOutPengirimNonNasabah_ProvIden.Value, False, oInt)
                                            Else
                                                .Sender_NonNasabah_ID_PropinsiLainnya = ""
                                            End If
                                        End If
                                        FillOrNothing(.Sender_NonNasabah_FK_IFTI_IDType, Me.CboNonSwiftOutPengirimNonNasabah_JenisDokumen.SelectedValue, False, oInt)
                                        FillOrNothing(.Sender_NonNasabah_NomorID, Me.TxtNonSwiftOutPengirimNonNasabah_NomorIden.Text, False, Ovarchar)

                                End Select
                                'cekBOwner
                                If Rb_BOwnerNas_ApaMelibatkan.SelectedValue = 1 Then
                                    FillOrNothing(.BeneficialOwner_Keterlibatan_Old, objIfti.FK_IFTI_KeterlibatanBeneficialOwner_ID)
                                    FillOrNothing(.BeneficialOwner_Keterlibatan, Me.Rb_BOwnerNas_ApaMelibatkan.SelectedValue, False, oInt)
                                    FillOrNothing(.BeneficialOwner_HubunganDenganPemilikDana_Old, objIfti.BeneficialOwner_HubunganDenganPemilikDana)
                                    FillOrNothing(.BeneficialOwner_HubunganDenganPemilikDana, Me.BenfOwnerNonSwiftOutHubunganPemilikDana.Text, False, Ovarchar)
                                    If Rb_NonSwiftOutBOwnerNas_TipePengirim.SelectedValue > 0 Then
                                        Dim BOwnerID As Integer = Rb_NonSwiftOutBOwnerNas_TipePengirim.SelectedValue
                                        FillOrNothing(.FK_IFTI_BeneficialOwnerType_ID_Old, objIfti.FK_IFTI_BeneficialOwnerType_ID)
                                        FillOrNothing(.FK_IFTI_BeneficialOwnerType_ID, BOwnerID)
                                        Select Case (BOwnerID)
                                            Case 1
                                                'Nasabah
                                                FillOrNothing(.BeneficialOwner_NoRekening_Old, objIfti.BeneficialOwner_NoRekening)
                                                FillOrNothing(.BeneficialOwner_NoRekening, Me.BenfOwnerNonSwiftOutNasabah_rekening.Text, False, Ovarchar)
                                                FillOrNothing(.BeneficialOwner_NamaLengkap_Old, objIfti.BeneficialOwner_NamaLengkap)
                                                FillOrNothing(.BeneficialOwner_NamaLengkap, Me.BenfOwnerNonSwiftOutNasabah_Nama.Text, False, Ovarchar)
                                                FillOrNothing(.BeneficialOwner_TanggalLahir_Old, objIfti.BeneficialOwner_TanggalLahir)
                                                FillOrNothing(.BeneficialOwner_TanggalLahir, Me.BenfOwnerNonSwiftOutNasabah_tanggalLahir, False, oDate)
                                                FillOrNothing(.BeneficialOwner_ID_Alamat_Old, objIfti.BeneficialOwner_ID_Alamat)
                                                FillOrNothing(.BeneficialOwner_ID_Alamat, Me.BenfOwnerNonSwiftOutNasabah_AlamatIden.Text, False, Ovarchar)
                                                FillOrNothing(.BeneficialOwner_ID_KotaKab, Me.hfBenfOwnerNonSwiftOutNasabah_KotaIden.Value, False, oInt)
                                                FillOrNothing(.BeneficialOwner_ID_KotaKab_Old, objIfti.BeneficialOwner_ID_KotaKab)
                                                FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya_Old, objIfti.BeneficialOwner_ID_KotaKabLainnya)
                                                If hfBenfOwnerNonSwiftOutNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                                                    FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya, Me.BenfOwnerNonSwiftOutNasabah_kotaLainIden.Text, False, Ovarchar)
                                                Else
                                                    .BeneficialOwner_ID_KotaKabLainnya = ""
                                                End If
                                                FillOrNothing(.BeneficialOwner_ID_Propinsi_Old, objIfti.BeneficialOwner_ID_Propinsi)
                                                FillOrNothing(.BeneficialOwner_ID_Propinsi, Me.hfBenfOwnerNonSwiftOutNasabah_ProvinsiIden.Value, False, oInt)
                                                FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya_Old, objIfti.BeneficialOwner_ID_PropinsiLainnya)
                                                If hfBenfOwnerNonSwiftOutNasabah_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                                                    FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya, Me.BenfOwnerNonSwiftOutNasabah_ProvinsiLainIden.Text, False, Ovarchar)
                                                Else
                                                    .BeneficialOwner_ID_PropinsiLainnya = ""
                                                End If
                                                FillOrNothing(.BeneficialOwner_FK_IFTI_IDType_Old, objIfti.BeneficialOwner_FK_IFTI_IDType)
                                                FillOrNothing(.BeneficialOwner_FK_IFTI_IDType, Me.CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.SelectedValue, False, oInt)
                                                FillOrNothing(.BeneficialOwner_NomorID_Old, objIfti.BeneficialOwner_NomorID)
                                                FillOrNothing(.BeneficialOwner_NomorID, Me.BenfOwnerNonSwiftOutNasabah_NomorIden.Text, False, Ovarchar)

                                            Case 2
                                                'NonNasabah
                                                FillOrNothing(.BeneficialOwner_NamaLengkap_Old, objIfti.BeneficialOwner_NamaLengkap)
                                                FillOrNothing(.BeneficialOwner_NamaLengkap, Me.BenfOwnerNonSwiftOutNonNasabah_Nama.Text, False, Ovarchar)
                                                FillOrNothing(.BeneficialOwner_TanggalLahir_Old, objIfti.BeneficialOwner_TanggalLahir)
                                                FillOrNothing(.BeneficialOwner_TanggalLahir, Me.BenfOwnerNonSwiftOutNonNasabah_TanggalLahir, False, oDate)
                                                FillOrNothing(.BeneficialOwner_ID_Alamat_Old, objIfti.BeneficialOwner_ID_Alamat)
                                                FillOrNothing(.BeneficialOwner_ID_Alamat, Me.BenfOwnerNonSwiftOutNonNasabah_AlamatIden.Text, False, Ovarchar)
                                                FillOrNothing(.BeneficialOwner_ID_KotaKab_Old, objIfti.BeneficialOwner_ID_KotaKab)
                                                FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya_Old, objIfti.BeneficialOwner_ID_KotaKabLainnya)
                                                FillOrNothing(.BeneficialOwner_ID_KotaKab, Me.hfBenfOwnerNonSwiftOutNonNasabah_KotaIden.Value, False, oInt)
                                                If hfBenfOwnerNonSwiftOutNonNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                                                    FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya, Me.BenfOwnerNonSwiftOutNonNasabah_KotaLainIden.Text, False, Ovarchar)
                                                Else
                                                    .BeneficialOwner_ID_KotaKabLainnya = ""
                                                End If
                                                FillOrNothing(.BeneficialOwner_ID_Propinsi_Old, objIfti.BeneficialOwner_ID_Propinsi)
                                                FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya_Old, objIfti.BeneficialOwner_ID_PropinsiLainnya)
                                                FillOrNothing(.BeneficialOwner_ID_Propinsi, Me.hfBenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Value, False, oInt)
                                                If hfBenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                                                    FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya, Me.BenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Text, False, Ovarchar)
                                                Else
                                                    .BeneficialOwner_ID_PropinsiLainnya = ""
                                                End If
                                                FillOrNothing(.BeneficialOwner_FK_IFTI_IDType_Old, objIfti.BeneficialOwner_FK_IFTI_IDType)
                                                FillOrNothing(.BeneficialOwner_FK_IFTI_IDType, Me.CBOBenfOwnerNonSwiftOutNonNasabah_JenisDokumen.SelectedValue, False, oInt)
                                                FillOrNothing(.BeneficialOwner_NomorID_Old, objIfti.BeneficialOwner_NomorID)
                                                FillOrNothing(.BeneficialOwner_NomorID, Me.BenfOwnerNonSwiftOutNonNasabah_NomorIdentitas.Text, False, Ovarchar)
                                        End Select

                                    Else
                                        Throw New Exception("Tipe Nasabah harus diisi!")

                                    End If


                                End If

                                'cekPenerima
                                Using objReceiver As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
                                    If objReceiver.Count > 0 Then
                                        Dim KeyBeneficiary As Integer = objReceiver(0).PK_IFTI_Beneficiary_ID
                                        'Dim TipePenerima As Integer = objReceiver(0).FK_IFTI_NasabahType_ID
                                        Using objReceiverAppDetail As New IFTI_Approval_Beneficiary ' = 'DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
                                            With objReceiverAppDetail

                                                Select Case (receiverType)
                                                    Case 1
                                                        'validasiReceiverInd()
                                                        .FK_IFTI_NasabahType_ID = 1
                                                        .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                        .FK_IFTI_Approval_Id = KeyHeaderApproval
                                                        .FK_IFTI_Beneficiary_ID = KeyBeneficiary

                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_NoRekening, Me.txtPenerimaNonSwiftOut_rekening.Text)

                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_NamaLengkap, Me.txtPenerimaNonSwiftOutNasabah_IND_nama.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_TanggalLahir, Me.txtPenerimaNonSwiftOutNasabah_IND_TanggalLahir.Text, False, oDate)
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_KewargaNegaraan, Me.RbNonSwiftOutPenerimaNasabah_IND_Warganegara.SelectedValue, False, oInt)
                                                        If RbNonSwiftOutPenerimaNasabah_IND_Warganegara.SelectedValue = "2" Then
                                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Me.hfNonSwiftOutPengirimNasabah_IND_negara.Value, False, oInt)
                                                            If hfNonSwiftOutPengirimNasabah_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                                                                FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.txtPenerimaNonSwiftOutNasabah_IND_negaraLain.Text, False, Ovarchar)
                                                            Else
                                                                .Beneficiary_Nasabah_INDV_NegaraLainnya = ""
                                                            End If
                                                        End If
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Iden, Me.txtPenerimaNonSwiftOutNasabah_IND_alamatIden.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_NegaraBagian, Me.txtPenerimaNonSwiftOutNasabah_IND_negaraBagian.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Me.hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Value, False, oInt)
                                                        If hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Value = ParameterNegara.MsSystemParameter_Value Then
                                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_NegaraLainnya, Me.txtPenerimaNonSwiftOutNasabah_IND_negaraLainIden.Text, False, Ovarchar)
                                                        Else
                                                            .Beneficiary_Nasabah_INDV_ID_NegaraLainnya = ""
                                                        End If

                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_FK_IFTI_IDType, Me.CBONonSwiftOutPenerimaNasabah_IND_JenisIden.SelectedValue, False, oInt)
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_NomorID, Me.txtPenerimaNonSwiftOutNasabah_IND_NomorIden.Text, False, Ovarchar)

                                                    Case 2
                                                        'validasiReceiverKorp()
                                                        .FK_IFTI_NasabahType_ID = 2
                                                        .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                        .FK_IFTI_Approval_Id = KeyHeaderApproval
                                                        .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_NoRekening, Me.txtPenerimaNonSwiftOut_rekening.Text)

                                                        If cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain.SelectedIndex > 0 Then
                                                            FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain.SelectedValue, False, oInt)
                                                        Else
                                                            FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, Nothing)
                                                        End If
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya, Me.txtPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLainnya.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_NamaKorporasi, Me.txtPenerimaNonSwiftOutNasabah_Korp_namaKorp.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfNonSwiftOutPengirimNasabah_Korp_BidangUsahaKorp.Value, False, oInt)
                                                        If ObjectAntiNull(hfNonSwiftOutPengirimNasabah_Korp_BidangUsahaKorp.Value) = True Then
                                                            If hfNonSwiftOutPengirimNasabah_Korp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                                                                FillOrNothing(.Beneficiary_Nasabah_CORP_BidangUsahaLainnya, Me.txtPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLainnya.Text, False, Ovarchar)
                                                            Else
                                                                .Beneficiary_Nasabah_CORP_BidangUsahaLainnya = ""
                                                            End If
                                                        End If

                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_AlamatLengkap, Me.txtPenerimaNonSwiftOutNasabah_Korp_AlamatKorp.Text, tipepenerima, Ovarchar)
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_NegaraBagian, Me.txtPenerimaNonSwiftOutNasabah_Korp_Kota.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Negara, Me.hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Value, False, oInt)
                                                        If hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Value = ParameterNegara.MsSystemParameter_Value Then
                                                            FillOrNothing(.Beneficiary_Nasabah_CORP_ID_NegaraLainnya, Me.txtPenerimaNonSwiftOutNasabah_Korp_NegaraLainnyaKorp.Text, False, Ovarchar)
                                                        Else
                                                            .Beneficiary_Nasabah_CORP_ID_NegaraLainnya = ""
                                                        End If


                                                    Case 3
                                                        'validasiReceiverNonNasabah()
                                                        .FK_IFTI_NasabahType_ID = 3
                                                        .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                        .FK_IFTI_Approval_Id = KeyHeaderApproval
                                                        .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                                        FillOrNothing(.Beneficiary_NonNasabah_KodeRahasia, Me.txtPenerimaNonSwiftOutNonNasabah_KodeRahasia.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_NonNasabah_NoRekening, Me.txtPenerimaNonSwiftOutNonNasabah_rekening.Text)

                                                        FillOrNothing(.Beneficiary_NonNasabah_NamaBank, Me.txtPenerimaNonSwiftOutNonNasabah_namabank.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_NonNasabah_NamaLengkap, Me.txtPenerimaNonSwiftOutNonNasabah_Nama.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_NonNasabah_ID_Alamat, Me.txtPenerimaNonSwiftOutNonNasabah_Alamat.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_NonNasabah_ID_Negara, Me.hfSwiftInNonSwiftOutPengirimNonNasabah_Negara.Value, False, oInt)
                                                        If Me.hfSwiftInNonSwiftOutPengirimNonNasabah_Negara.Value = ParameterNegara.MsSystemParameter_Value Then
                                                            FillOrNothing(.Beneficiary_NonNasabah_ID_NegaraLainnya, Me.txtPenerimaNonSwiftOutNonNasabah_negaraLain.Text, False, Ovarchar)
                                                        Else
                                                            .Beneficiary_NonNasabah_ID_NegaraLainnya = ""
                                                        End If

                                                End Select
                                            End With
                                            'save beneficiary
                                            DataRepository.IFTI_Approval_BeneficiaryProvider.Save(OTrans, objReceiverAppDetail)
                                        End Using
                                    End If
                                End Using

                                'cekTransaksi

                                '----old----
                                FillOrNothing(.TanggalTransaksi_Old, objIfti.TanggalTransaksi)
                                FillOrNothing(.TimeIndication_Old, objIfti.TimeIndication)
                                FillOrNothing(.SenderReference_Old, objIfti.SenderReference)
                                FillOrNothing(.BankOperationCode_Old, objIfti.BankOperationCode)
                                FillOrNothing(.InstructionCode_Old, objIfti.InstructionCode)
                                FillOrNothing(.KantorCabangPenyelengaraPengirimAsal_Old, objIfti.KantorCabangPenyelengaraPengirimAsal)
                                FillOrNothing(.TransactionCode, objIfti.TransactionCode)
                                FillOrNothing(.ValueDate_TanggalTransaksi_Old, objIfti.ValueDate_TanggalTransaksi)
                                FillOrNothing(.ValueDate_NilaiTransaksi_Old, objIfti.ValueDate_NilaiTransaksi)
                                FillOrNothing(.ValueDate_FK_Currency_ID_Old, objIfti.ValueDate_FK_Currency_ID)
                                FillOrNothing(.ValueDate_CurrencyLainnya_Old, objIfti.ValueDate_CurrencyLainnya)
                                FillOrNothing(.ValueDate_NilaiTransaksiIDR_Old, objIfti.ValueDate_NilaiTransaksiIDR)
                                FillOrNothing(.Instructed_Currency_Old, objIfti.Instructed_Currency)
                                FillOrNothing(.Instructed_CurrencyLainnya_Old, objIfti.Instructed_CurrencyLainnya)
                                FillOrNothing(.Instructed_Amount_Old, objIfti.Instructed_Amount)
                                FillOrNothing(.ExchangeRate_Old, objIfti.ExchangeRate)
                                FillOrNothing(.SendingInstitution_Old, objIfti.SendingInstitution)
                                FillOrNothing(.TujuanTransaksi_Old, objIfti.TujuanTransaksi)
                                FillOrNothing(.SumberPenggunaanDana_Old, objIfti.SumberPenggunaanDana)

                                '-----new---
                                FillOrNothing(.TanggalTransaksi, objIfti.TanggalTransaksi)
                                FillOrNothing(.TimeIndication, objIfti.TimeIndication)
                                FillOrNothing(.SenderReference, objIfti.SenderReference)
                                FillOrNothing(.BankOperationCode, objIfti.BankOperationCode)
                                FillOrNothing(.InstructionCode, objIfti.InstructionCode)
                                FillOrNothing(.ExchangeRate, objIfti.ExchangeRate)
                                FillOrNothing(.SendingInstitution, objIfti.SendingInstitution)



                                FillOrNothing(.TanggalTransaksi, Me.Transaksi_NonSwiftOut_tanggal.Text, False, oDate)
                                FillOrNothing(.KantorCabangPenyelengaraPengirimAsal, Me.Transaksi_NonSwiftOut_kantorCabangPengirim.Text, False, Ovarchar)
                                FillOrNothing(.ValueDate_TanggalTransaksi, Me.Transaksi_NonSwiftOut_ValueTanggalTransaksi.Text, False, oDate)
                                FillOrNothing(.ValueDate_NilaiTransaksi, Me.Transaksi_NonSwiftOut_nilaitransaksi.Text, False, oDecimal)
                                If Me.hfTransaksi_NonSwiftOut_MataUangTransaksi.Value <> "" Then
                                    Using objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & " like '%" & Me.Transaksi_NonSwiftOut_MataUangTransaksi.Text & "%'", "", 0, Integer.MaxValue, 0)
                                        If objCurrency.Count > 0 Then
                                            FillOrNothing(.ValueDate_FK_Currency_ID, objCurrency(0).IdCurrency, False, oInt)
                                        End If

                                    End Using
                                End If

                                FillOrNothing(.ValueDate_CurrencyLainnya, objIfti.ValueDate_CurrencyLainnya, False, Ovarchar)
                                FillOrNothing(.ValueDate_NilaiTransaksiIDR, Me.Transaksi_NonSwiftOut_AmountdalamRupiah.Text, False, oDecimal)
                                If Me.hfTransaksi_NonSwiftOut_currency.Value <> "" Then
                                    Using objCurrency2 As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & _
                                                                                                            " like '%" & Me.Transaksi_NonSwiftOut_currency.Text & "%'", "", 0, Integer.MaxValue, 0)
                                        If objCurrency2.Count > 0 Then
                                            FillOrNothing(.Instructed_Currency, objCurrency2(0).IdCurrency, False, oInt)
                                        End If
                                    End Using
                                End If

                                FillOrNothing(.Instructed_CurrencyLainnya, objIfti.Instructed_CurrencyLainnya, False, Ovarchar)
                                FillOrNothing(.Instructed_Amount, Me.Transaksi_NonSwiftOut_instructedAmount.Text, False, oDecimal)
                                FillOrNothing(.TujuanTransaksi, Me.Transaksi_NonSwiftOut_TujuanTransaksi.Text, False, Ovarchar)
                                FillOrNothing(.SumberPenggunaanDana, Me.Transaksi_NonSwiftOut_SumberPenggunaanDana.Text, False, Ovarchar)

                                'saving
                            End With

                            DataRepository.IFTI_Approval_DetailProvider.Save(objIfti_ApprovalDetail)
                        End Using
                        ''Send Email
                        'SendEmail("Ifti Edit (User Id : " & SessionNIK & "-" & SessionGroupMenuName & ")", _
                        '          "Ifti", SessionIsBankWide, SessionFkGroupApprovalId, "", "IFTI_Approvals.aspx")
                        ImageButtonSave.Visible = False
                        ImageButtonCancel.Visible = False
                        lblMsg.Text = "Data has been edited and waiting for approval"
                        MultiViewEditNonSwiftOut.ActiveViewIndex = 1
                    End Using
                    'End If
                    OTrans.Commit()
                Catch ex As Exception
                    OTrans.Rollback()
                    Throw
                End Try

            End Using
        Catch ex As Exception
            'LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
#End Region
#Region "Function..."
    Function IsValidDecimal(ByVal intPrecision As Integer, ByVal intScale As Integer, ByVal strDatatoValidate As String) As Boolean

        Return Regex.IsMatch(strDatatoValidate, "^[\d]{1," & intPrecision & "}(\.[\d]{1," & intScale & "})?$")

    End Function
    Private Sub SetCOntrolLoad()
        'bind JenisID
        Using objJenisId As TList(Of IFTI_IDType) = DataRepository.IFTI_IDTypeProvider.GetAll
            If objJenisId.Count > 0 Then
                Me.cboNonSwiftOutPengirimNasInd_jenisidentitas.Items.Clear()
                cboNonSwiftOutPengirimNasInd_jenisidentitas.Items.Add("-Select-")

                Me.CboNonSwiftOutPengirimNonNasabah_JenisDokumen.Items.Clear()
                CboNonSwiftOutPengirimNonNasabah_JenisDokumen.Items.Add("-Select-")

                Me.CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.Items.Clear()
                CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.Items.Add("-Select-")

                Me.CBOBenfOwnerNonSwiftOutNonNasabah_JenisDokumen.Items.Clear()
                CBOBenfOwnerNonSwiftOutNonNasabah_JenisDokumen.Items.Add("-Select-")

                Me.CBONonSwiftOutPenerimaNasabah_IND_JenisIden.Items.Clear()
                Me.CBONonSwiftOutPenerimaNasabah_IND_JenisIden.Items.Add("-Select-")

                For i As Integer = 0 To objJenisId.Count - 1
                    cboNonSwiftOutPengirimNasInd_jenisidentitas.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CboNonSwiftOutPengirimNonNasabah_JenisDokumen.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CBOBenfOwnerNonSwiftOutNonNasabah_JenisDokumen.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CBONonSwiftOutPenerimaNasabah_IND_JenisIden.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                Next
            End If


            'bindgrid 
            PopUpUmum_NonSwiftOut_TanggalLaporan.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtUmum_NonSwiftOutTanggalLaporan.ClientID & "'), 'dd-mmm-yyyy')")
            Me.PopUpUmum_NonSwiftOut_TanggalLaporan.Style.Add("display", "")
            Me.popUpTanggalLahirNonSwiftOutPengirimNasabah_Ind.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtIdenNonSwiftOutPengirimNas_Ind_TanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTanggalLahirNonSwiftOutPengirimNasabah_Ind.Style.Add("display", "")
            Me.popUpTanggalLahirNonSwiftOutPengirimNonNasabah.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtNonSwiftOutPengirimNonNasabah_TanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTanggalLahirNonSwiftOutPengirimNonNasabah.Style.Add("display", "")
            Me.popUpTanggalLahirNonSwiftOutBOwnerNasabah.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.BenfOwnerNonSwiftOutNasabah_tanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTanggalLahirNonSwiftOutBOwnerNasabah.Style.Add("display", "")
            Me.popUpTanggalLahirBownerNonNasabah.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.BenfOwnerNonSwiftOutNonNasabah_TanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTanggalLahirBownerNonNasabah.Style.Add("display", "")
            Me.popUpNonSwiftOutTanggalLahirPenerimaNasabah_Ind.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtPenerimaNonSwiftOutNasabah_IND_TanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpNonSwiftOutTanggalLahirPenerimaNasabah_Ind.Style.Add("display", "")
            Me.popUpTanggalTransaksi_NonSwiftOut_Transaksi.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.Transaksi_NonSwiftOut_tanggal.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTanggalTransaksi_NonSwiftOut_Transaksi.Style.Add("display", "")
            Me.popUpTanggalTransaksi_NonSwiftOut_ValueDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.Transaksi_NonSwiftOut_ValueTanggalTransaksi.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTanggalTransaksi_NonSwiftOut_ValueDate.Style.Add("display", "")

            'Bind MsBentukBadanUsaha
            Using objBentukBadanUsaha As TList(Of MsBentukBidangUsaha) = DataRepository.MsBentukBidangUsahaProvider.GetAll
                If objBentukBadanUsaha.Count > 0 Then
                    cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.Items.Clear()
                    cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.Items.Add("-Select-")

                    cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain.Items.Clear()
                    cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain.Items.Add("-Select-")

                    For i As Integer = 0 To objBentukBadanUsaha.Count - 1
                        cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                        cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    Next
                End If
            End Using

        End Using

    End Sub
    Sub clearSession()
        'Session("IFTIEdit.grvTRXKMDetilValutaAsingDATA") = Nothing
        'Session("WICEdit.grvDetilKasKeluarDATA") = Nothing
        'Session("WICEdit.ResumeKasMasukKasKeluar") = Nothing
        'Session("WICEdit.RowEdit") = Nothing
        Session("IFTIEdit.IFTIPK") = Nothing
        'Session("PickerProvinsi.Data") = Nothing
    End Sub
    Sub TransactionNonSwiftOut()
        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SafeDefaultValue = ""
            If Not IsNothing(objIfti) Then
                'umum
                Me.TxtUmum_NonSwiftOutLTDLN.Text = Safe(objIfti.LTDLNNo)
                Me.TxtUmum_NonSwiftOutLTDLNKoreksi.Text = Safe(objIfti.LTDLNNoKoreksi)
                Me.TxtUmum_NonSwiftOutTanggalLaporan.Text = FormatDate(Safe(objIfti.TanggalLaporan))
                Me.TxtUmum_NonSwiftOutPJKBankPelapor.Text = Safe(objIfti.NamaPJKBankPelapor)
                Me.TxtUmum_NonSwiftOutNamaPejabatPJKBankPelapor.Text = Safe(objIfti.NamaPejabatPJKBankPelapor)
                Me.Rb_Umum_NonSwiftOut_jenislaporan.SelectedValue = Safe(objIfti.JenisLaporan)

                'Identitas Pengirim

                'cekTipe Penyelenggara
                Dim senderType As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID.GetValueOrDefault(1)
                Select Case (senderType)
                    Case 1
                        'pengirimNasabahIndividu
                        ' Me.CboSwiftOutPengirim.SelectedValue = 0
                        Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1
                        Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue = 1
                        Me.trNonSwiftOutPengirimNas_TipePengirim.Visible = True
                        Me.trNonSwiftOutTipeNasabah.Visible = True
                        Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 0
                        Me.MultiViewNonSwiftOutPengirimNasabah.ActiveViewIndex = 0
                        FieldSwiftOutSenderCase1()
                    Case 2
                        'pengirimNasabahKorp
                        ' Me.CboSwiftOutPengirim.SelectedValue = 0
                        Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1
                        Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue = 2
                        Me.trNonSwiftOutPengirimNas_TipePengirim.Visible = True
                        Me.trNonSwiftOutTipeNasabah.Visible = True
                        Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 0
                        Me.MultiViewNonSwiftOutPengirimNasabah.ActiveViewIndex = 1
                        FieldSwiftOutSenderCase2()
                    Case 3
                        'PengirimNonNasabah
                        ' Me.CboSwiftOutPengirim.SelectedValue = 0
                        Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 2
                        Me.trNonSwiftOutTipeNasabah.Visible = False
                        Me.trNonSwiftOutPengirimNas_TipePengirim.Visible = True
                        Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 1
                        FieldSwiftOutSenderCase3()
                End Select

                'Beneficiary Owner
                'check if having Beneficiary Owner
                'check if FK_IFTI_BeneficialOwnerType_ID not nothing
                Rb_BOwnerNas_ApaMelibatkan.SelectedValue = objIfti.FK_IFTI_KeterlibatanBeneficialOwner_ID.GetValueOrDefault(2)
                If Not IsNothing(objIfti.FK_IFTI_BeneficialOwnerType_ID) Then
                    'Rb_BOwnerNas_ApaMelibatkan.SelectedValue = objIfti.FK_IFTI_KeterlibatanBeneficialOwner_ID.GetValueOrDefault(2)
                    Me.BenfOwnerTipePengirim.Visible = True
                    Me.BenfOwnerHubungan.Visible = True
                    Me.BenfOwnerNonSwiftOutHubunganPemilikDana.Text = objIfti.BeneficialOwner_HubunganDenganPemilikDana
                    Me.Rb_NonSwiftOutBOwnerNas_TipePengirim.SelectedValue = objIfti.FK_IFTI_BeneficialOwnerType_ID
                    Dim BOwnerID As Integer = objIfti.FK_IFTI_BeneficialOwnerType_ID
                    Select Case (BOwnerID)
                        Case 1
                            MultiViewNonSwiftOutBOwner.ActiveViewIndex = 0
                            FieldSwiftOutBOwnerNasabah()
                        Case 2
                            MultiViewNonSwiftOutBOwner.ActiveViewIndex = 1
                            FieldSwiftOutBOwnerNonNasabah()
                    End Select
                End If

                'Identitas Penerima
                Using objIftiBeneficiary As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID = " & getIFTIPK, "", 0, Integer.MaxValue, 0)
                    If objIftiBeneficiary.Count > 0 Then
                        ' cek ReceiverType
                        Dim receiverType As Integer = objIftiBeneficiary(0).FK_IFTI_NasabahType_ID
                        Select Case (receiverType)
                            Case 1
                                Me.RBNonSwiftOutPenerimaNasabah_TipePengirim.SelectedValue = 1
                                Me.trNonSwiftOutPenerimaTipePengirim.Visible = True
                                Me.trNonSwiftOutPenerimaTipeNasabah.Visible = True
                                Me.Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedValue = 1
                                Me.MultiViewNonSwiftOutIdenPenerima.ActiveViewIndex = 0
                                Me.MultiViewNonSwiftOutPenerimaNasabah.ActiveViewIndex = 0
                                FieldNonSwiftOutPenerimaNasabahPerorangan()
                            Case 2
                                Me.RBNonSwiftOutPenerimaNasabah_TipePengirim.SelectedValue = 1
                                Me.Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedValue = 2
                                Me.trNonSwiftOutPenerimaTipePengirim.Visible = True
                                Me.trNonSwiftOutPenerimaTipeNasabah.Visible = True
                                Me.MultiViewNonSwiftOutIdenPenerima.ActiveViewIndex = 0
                                Me.MultiViewNonSwiftOutPenerimaNasabah.ActiveViewIndex = 1
                                FieldNonSwiftOutPenerimaNasabahKorporasi()
                            Case 3
                                Me.RBNonSwiftOutPenerimaNasabah_TipePengirim.SelectedValue = 2
                                'Me.trNonSwiftOutPenerimaTipePengirim.Visible = true
                                Me.MultiViewNonSwiftOutIdenPenerima.ActiveViewIndex = 1
                                Me.trNonSwiftOutPenerimaTipeNasabah.Visible = False
                                FieldNonSwiftOutPenerimaNonNasabah()
                        End Select
                    End If

                End Using

                'Transaksi
                Me.Transaksi_NonSwiftOut_tanggal.Text = FormatDate(objIfti.TanggalTransaksi)
                Me.Transaksi_NonSwiftOut_kantorCabangPengirim.Text = objIfti.KantorCabangPenyelengaraPengirimAsal
                Me.Transaksi_NonSwiftOut_ValueTanggalTransaksi.Text = FormatDate(objIfti.ValueDate_TanggalTransaksi)
                Me.Transaksi_NonSwiftOut_nilaitransaksi.Text = objIfti.ValueDate_NilaiTransaksi
                Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.ValueDate_FK_Currency_ID.GetValueOrDefault(0))
                    If Not IsNothing(objCurrency) Then
                        Me.Transaksi_NonSwiftOut_MataUangTransaksi.Text = objCurrency.Code
                        hfTransaksi_NonSwiftOut_MataUangTransaksi.Value = objCurrency.Code
                    End If
                End Using
                Me.Transaksi_NonSwiftOut_MataUangTransaksiLain.Text = objIfti.ValueDate_CurrencyLainnya
                Me.Transaksi_NonSwiftOut_AmountdalamRupiah.Text = objIfti.ValueDate_NilaiTransaksiIDR
                Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.Instructed_Currency.GetValueOrDefault(0))
                    If Not IsNothing(objCurrency) Then
                        Me.Transaksi_NonSwiftOut_currency.Text = objCurrency.Code
                        hfTransaksi_NonSwiftOut_currency.Value = objCurrency.Code
                    End If
                End Using
                Me.Transaksi_NonSwiftOut_currencyLain.Text = objIfti.Instructed_CurrencyLainnya
                If Not IsNothing(objIfti.Instructed_Amount) Then
                    Me.Transaksi_NonSwiftOut_instructedAmount.Text = objIfti.Instructed_Amount
                End If
                Me.Transaksi_NonSwiftOut_TujuanTransaksi.Text = objIfti.TujuanTransaksi
                Me.Transaksi_NonSwiftOut_SumberPenggunaanDana.Text = objIfti.SumberPenggunaanDana
            End If
        End Using
    End Sub
#End Region
#Region "NonSwiftOutBOwner..."
    Sub FieldSwiftOutBOwnerNasabah()
        'BeneficialOwnerNasabah
        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SafeDefaultValue = ""
            If Not IsNothing(objIfti) Then
                Me.BenfOwnerNonSwiftOutNasabah_rekening.Text = objIfti.BeneficialOwner_NoRekening
                Me.BenfOwnerNonSwiftOutNasabah_Nama.Text = objIfti.BeneficialOwner_NamaLengkap
                If ObjectAntiNull(objIfti.BeneficialOwner_TanggalLahir) = True Then
                    Me.BenfOwnerNonSwiftOutNasabah_tanggalLahir.Text = FormatDate(objIfti.BeneficialOwner_TanggalLahir)
                End If

                Me.BenfOwnerNonSwiftOutNasabah_AlamatIden.Text = objIfti.BeneficialOwner_ID_Alamat
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.BeneficialOwner_ID_KotaKab)
                If Not IsNothing(objSkotaKab) Then
                    Me.BenfOwnerNonSwiftOutNasabah_KotaIden.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfBenfOwnerNonSwiftOutNasabah_KotaIden.Value = Safe(objSkotaKab.IDKotaKab)
                    If hfBenfOwnerNonSwiftOutNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                        Me.BenfOwnerNonSwiftOutNasabah_kotaLainIden.Text = objIfti.BeneficialOwner_ID_KotaKabLainnya
                    End If
                End If
                objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.BeneficialOwner_ID_Propinsi))
                If Not IsNothing(objSProvinsi) Then
                    Me.BenfOwnerNonSwiftOutNasabah_ProvinsiIden.Text = Safe(objSProvinsi.Nama)
                    hfBenfOwnerNonSwiftOutNasabah_ProvinsiIden.Value = Safe(objSProvinsi.IdProvince)
                    If hfBenfOwnerNonSwiftOutNasabah_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                        Me.BenfOwnerNonSwiftOutNasabah_ProvinsiLainIden.Text = objIfti.BeneficialOwner_ID_PropinsiLainnya
                    End If
                End If
                If ObjectAntiNull(objIfti.BeneficialOwner_FK_IFTI_IDType) = True Then
                    Me.CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.SelectedValue = objIfti.BeneficialOwner_FK_IFTI_IDType
                Else
                    Me.CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.SelectedIndex = 0
                End If

                Me.BenfOwnerNonSwiftOutNasabah_NomorIden.Text = objIfti.BeneficialOwner_NomorID
            End If
        End Using


    End Sub
    Sub FieldSwiftOutBOwnerNonNasabah()
        'BeneficialOwnerNonNasabah
        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.IdProvince.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SafeDefaultValue = ""
            If Not IsNothing(objIfti) Then
                Me.BenfOwnerNonSwiftOutNonNasabah_Nama.Text = objIfti.BeneficialOwner_NamaLengkap
                If Not IsNothing(objIfti.BeneficialOwner_TanggalLahir) Then
                    Me.BenfOwnerNonSwiftOutNonNasabah_TanggalLahir.Text = FormatDate(objIfti.BeneficialOwner_TanggalLahir)
                End If
                Me.BenfOwnerNonSwiftOutNonNasabah_AlamatIden.Text = objIfti.BeneficialOwner_ID_Alamat
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.BeneficialOwner_ID_KotaKab)
                If Not IsNothing(objSkotaKab) Then
                    Me.BenfOwnerNonSwiftOutNonNasabah_KotaIden.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfBenfOwnerNonSwiftOutNonNasabah_KotaIden.Value = Safe(objSkotaKab.IDKotaKab)
                    If hfBenfOwnerNonSwiftOutNonNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                        Me.BenfOwnerNonSwiftOutNonNasabah_KotaLainIden.Text = objIfti.BeneficialOwner_ID_KotaKabLainnya
                        Me.trBenfOwnerNonSwiftOutNonNasabah_KotaLainIden.Visible = True
                    End If
                End If
                objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.BeneficialOwner_ID_Propinsi))
                If Not IsNothing(objSProvinsi) Then
                    Me.BenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Text = Safe(objSProvinsi.Nama)
                    hfBenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Value = Safe(objSProvinsi.IdProvince)
                    If hfBenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                        Me.BenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Text = objIfti.BeneficialOwner_ID_PropinsiLainnya
                        Me.trBenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Visible = True
                    End If
                End If

                If Not IsNothing(objIfti.BeneficialOwner_FK_IFTI_IDType) Then
                    Me.CBOBenfOwnerNonSwiftOutNonNasabah_JenisDokumen.SelectedValue = objIfti.BeneficialOwner_FK_IFTI_IDType
                End If
                Me.BenfOwnerNonSwiftOutNonNasabah_NomorIdentitas.Text = objIfti.BeneficialOwner_NomorID
            End If
        End Using
    End Sub
#End Region
#Region "NonSwiftOutReceiver..."
    Sub FieldNonSwiftOutPenerimaNasabahPerorangan()
        'PenerimaNasabahPerorangan

        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        'parameter value

        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)

        Using objIfti As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, 1, 0)
            Me.txtPenerimaNonSwiftOut_rekening.Text = objIfti(0).Beneficiary_Nasabah_INDV_NoRekening
            Me.txtPenerimaNonSwiftOutNasabah_IND_nama.Text = objIfti(0).Beneficiary_Nasabah_INDV_NamaLengkap
            Me.txtPenerimaNonSwiftOutNasabah_IND_TanggalLahir.Text = FormatDate(objIfti(0).Beneficiary_Nasabah_INDV_TanggalLahir)
            If Not IsNothing(objIfti(0).Beneficiary_Nasabah_INDV_KewargaNegaraan) Then
                Me.RbNonSwiftOutPenerimaNasabah_IND_Warganegara.SelectedValue = objIfti(0).Beneficiary_Nasabah_INDV_KewargaNegaraan
                If objIfti(0).Beneficiary_Nasabah_INDV_KewargaNegaraan = "2" Then
                    Me.NegaraNonSwift_penerimaPerorangan.Visible = True

                    objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_Nasabah_INDV_Negara)
                    If Not IsNothing(objSnegara) Then
                        txtPenerimaNonSwiftOutNasabah_IND_negara.Text = Safe(objSnegara.NamaNegara)
                        hfNonSwiftOutPengirimNasabah_IND_negara.Value = Safe(objSnegara.IDNegara)
                        If hfNonSwiftOutPengirimNasabah_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                            Me.txtPenerimaNonSwiftOutNasabah_IND_negaraLain.Text = objIfti(0).Beneficiary_Nasabah_INDV_NegaraLainnya
                            Me.NegaraLainNonSwift_penerimaPerorangan.Visible = True
                        End If
                    End If
                End If
            End If
            Me.txtPenerimaNonSwiftOutNasabah_IND_alamatIden.Text = objIfti(0).Beneficiary_Nasabah_INDV_ID_Alamat_Iden
            Me.txtPenerimaNonSwiftOutNasabah_IND_negaraBagian.Text = objIfti(0).Beneficiary_Nasabah_INDV_ID_NegaraBagian
            objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_Nasabah_INDV_ID_Negara)
            If Not IsNothing(objSnegara) Then
                txtPenerimaNonSwiftOutNasabah_IND_negaraIden.Text = Safe(objSnegara.NamaNegara)
                hfNonSwiftOutPengirimNasabah_IND_negaraIden.Value = Safe(objSnegara.IDNegara)
                If hfNonSwiftOutPengirimNasabah_IND_negaraIden.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.txtPenerimaNonSwiftOutNasabah_IND_negaraLainIden.Text = objIfti(0).Beneficiary_Nasabah_INDV_ID_NegaraLainnya
                    Me.trPenerimaNonSwiftOutNasabah_IND_negaraLainIden.Visible = True
                End If
            End If
            Me.CBONonSwiftOutPenerimaNasabah_IND_JenisIden.SelectedValue = objIfti(0).Beneficiary_Nasabah_INDV_FK_IFTI_IDType
            Me.txtPenerimaNonSwiftOutNasabah_IND_NomorIden.Text = objIfti(0).Beneficiary_Nasabah_INDV_NomorID
        End Using
    End Sub
    Sub FieldNonSwiftOutPenerimaNasabahKorporasi()
        'PenerimaNasabahKorporasi
        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        'parameter value
        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        Using objIfti As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, 1, 0)
            Me.txtPenerimaNonSwiftOut_rekening.Text = objIfti(0).Beneficiary_Nasabah_CORP_NoRekening
            Using objBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(objIfti(0).Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0))
                If Not IsNothing(objBadanUsaha) Then
                    Me.cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain.SelectedValue = objBadanUsaha.BentukBidangUsaha
                End If
            End Using
            Me.txtPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLainnya.Text = objIfti(0).Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
            Me.txtPenerimaNonSwiftOutNasabah_Korp_namaKorp.Text = objIfti(0).Beneficiary_Nasabah_CORP_NamaKorporasi
            Using objBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(objIfti(0).Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0))
                If Not IsNothing(objBidangUsaha) Then
                    Me.txtPenerimaNonSwiftOutNasabah_Korp_BidangUsahaKorp.Text = objBidangUsaha.NamaBidangUsaha
                    Me.hfNonSwiftOutPengirimNasabah_Korp_BidangUsahaKorp.Value = objBidangUsaha.IdBidangUsaha
                    If hfNonSwiftOutPengirimNasabah_Korp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                        Me.txtPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLainnya.Text = objIfti(0).Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
                        Me.trPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Visible = True
                    End If
                End If
            End Using

            Me.txtPenerimaNonSwiftOutNasabah_Korp_Kota.Text = objIfti(0).Beneficiary_Nasabah_CORP_ID_NegaraBagian
            objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_Nasabah_CORP_ID_Negara)
            If Not IsNothing(objSnegara) Then
                txtPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Text = Safe(objSnegara.NamaNegara)
                hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Value = Safe(objSnegara.IDNegara)
                If hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.txtPenerimaNonSwiftOutNasabah_Korp_NegaraLainnyaKorp.Text = objIfti(0).Beneficiary_Nasabah_CORP_ID_NegaraLainnya
                    Me.trPenerimaNonSwiftOutNasabah_IND_negaraLainIden.Visible = True
                End If
            End If

        End Using
    End Sub
    Sub FieldNonSwiftOutPenerimaNonNasabah()
        'PenerimaNonNasabah

        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        Using objIfti As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, 1, 0)
            Me.txtPenerimaNonSwiftOutNonNasabah_KodeRahasia.Text = objIfti(0).Beneficiary_NonNasabah_KodeRahasia
            'If objIfti(0).Beneficiary_NonNasabah_ID_Alamat Is Nothing Then
            Me.txtPenerimaNonSwiftOutNonNasabah_rekening.Text = objIfti(0).Beneficiary_NonNasabah_NoRekening
            'End If
            Me.txtPenerimaNonSwiftOutNonNasabah_namabank.Text = objIfti(0).Beneficiary_NonNasabah_NamaBank
            Me.txtPenerimaNonSwiftOutNonNasabah_Nama.Text = objIfti(0).Beneficiary_NonNasabah_NamaLengkap
            Me.txtPenerimaNonSwiftOutNonNasabah_Alamat.Text = objIfti(0).Beneficiary_NonNasabah_ID_Alamat
            objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_NonNasabah_ID_Negara)
            If Not IsNothing(objSnegara) Then
                txtPenerimaNonSwiftOutNonNasabah_Negara.Text = Safe(objSnegara.NamaNegara)
                hfSwiftInNonSwiftOutPengirimNonNasabah_Negara.Value = Safe(objSnegara.IDNegara)
                If hfSwiftInNonSwiftOutPengirimNonNasabah_Negara.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.txtPenerimaNonSwiftOutNonNasabah_negaraLain.Text = objIfti(0).Beneficiary_NonNasabah_ID_NegaraLainnya
                    Me.trPenerimaNonSwiftOutNonNasabah_negaraLain.Visible = True
                End If
            End If
        End Using

    End Sub
#End Region
#Region "SwiftOutSenderType..."
    Sub FieldSwiftOutSenderCase1()
        'PengirimNasabahIndividu

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetPaged(MsPekerjaanColumn.Activation.ToString & "=1", MsPekerjaanColumn.NamaPekerjaan.ToString, 0, Integer.MaxValue, 0)
        Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_Nasabah_INDV_NoRekening
            Me.TxtIdenNonSwiftOutPengirim_Norekening.Text = SetnGetSenderAccount
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_NamaLengkap.Text = objIfti.Sender_Nasabah_INDV_NamaLengkap
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_TanggalLahir.Text = FormatDate(objIfti.Sender_Nasabah_INDV_TanggalLahir)
            Me.RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan.SelectedValue = objIfti.Sender_Nasabah_INDV_KewargaNegaraan
            If objIfti.Sender_Nasabah_INDV_KewargaNegaraan = "2" Then
                Me.NegaraNonSwift_pengirimPerorangan.Visible = True

                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti.Sender_Nasabah_INDV_Negara)
                If Not IsNothing(objSnegara) Then
                    TxtIdenNonSwiftOutPengirimNas_Ind_Negara.Text = Safe(objSnegara.NamaNegara)
                    hfIdenNonSwiftOutPengirimNas_Ind_Negara.Value = Safe(objSnegara.IDNegara)
                    If hfIdenNonSwiftOutPengirimNas_Ind_Negara.Value = ParameterNegara.MsSystemParameter_Value Then
                        Me.TxtIdenNonSwiftOutPengirimNas_Ind_NegaraLain.Text = objIfti.Sender_Nasabah_INDV_NegaraLainnya
                        Me.NegaraLainNonSwift_pengirimPerorangan.Visible = True
                    End If
                End If

            End If
            objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objIfti.Sender_Nasabah_INDV_Pekerjaan)
            If Not IsNothing(objSPekerjaan) Then
                TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                hfIdenNonSwiftOutPengirimNas_Ind_pekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                If hfIdenNonSwiftOutPengirimNas_Ind_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
                    Me.TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaanlain.Text = objIfti.Sender_Nasabah_INDV_PekerjaanLainnya
                    Me.trIdenNonSwiftOutPengirimNas_Ind_Pekerjaanlain.Visible = True
                End If
            End If


            Me.TxtIdenNonSwiftOutPengirimNas_Ind_Alamat.Text = objIfti.Sender_Nasabah_INDV_DOM_Alamat

            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_INDV_DOM_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtIdenNonSwiftOutPengirimNas_Ind_Kota.Text = Safe(objSkotaKab.NamaKotaKab)
                hfIdenNonSwiftOutPengirimNas_Ind_Kota_dom.Value = Safe(objSkotaKab.IDKotaKab)
                If hfIdenNonSwiftOutPengirimNas_Ind_Kota_dom.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TxtIdenNonSwiftOutPengirimNas_Ind_kotalain.Text = objIfti.Sender_Nasabah_INDV_DOM_KotaKabLainnya
                    Me.trIdenNonSwiftOutPengirimNas_Ind_kotalain.Visible = True
                End If
            End If


            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_INDV_DOM_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                TxtIdenNonSwiftOutPengirimNas_Ind_provinsi.Text = Safe(objSProvinsi.Nama)
                hfIdenNonSwiftOutPengirimNas_Ind_provinsi_dom.Value = Safe(objSProvinsi.IdProvince)
                If hfIdenNonSwiftOutPengirimNas_Ind_provinsi_dom.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TxtIdenNonSwiftOutPengirimNas_Ind_provinsiLain.Text = objIfti.Sender_Nasabah_INDV_DOM_PropinsiLainnya
                    Me.trIdenNonSwiftOutPengirimNas_Ind_provinsiLain.Visible = True
                End If
            End If

            Me.TxtIdenNonSwiftOutPengirimNas_Ind_alamatIdentitas.Text = objIfti.Sender_Nasabah_INDV_ID_Alamat

            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_INDV_ID_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Text = Safe(objSkotaKab.NamaKotaKab)
                hfIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Value = Safe(objSkotaKab.IDKotaKab)
                If hfIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TxtIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Text = objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya
                    Me.trIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Visible = True
                End If
            End If

            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_INDV_ID_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Text = Safe(objSProvinsi.Nama)
                hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Value = Safe(objSProvinsi.IdProvince)
                If hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Text = objIfti.Sender_Nasabah_INDV_ID_PropinsiLainnya
                    Me.trIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Visible = True
                End If
            End If
            Me.cboNonSwiftOutPengirimNasInd_jenisidentitas.SelectedValue = objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_noIdentitas.Text = objIfti.Sender_Nasabah_INDV_NomorID
        End Using
    End Sub
    Sub FieldSwiftOutSenderCase2()
        'PengirimNasabahKorporasi

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_Nasabah_INDV_NoRekening
            Me.TxtIdenNonSwiftOutPengirim_Norekening.Text = SetnGetSenderAccount

            Using objBentukBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(objIfti.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0))
                If Not IsNothing(objBentukBadanUsaha) Then
                    Me.cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.SelectedValue = objBentukBadanUsaha.IdBentukBidangUsaha
                End If
            End Using
            Me.TxtIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsahaLain.Text = objIfti.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
            Me.TxtIdenNonSwiftOutPengirimNas_Corp_NamaKorp.Text = objIfti.Sender_Nasabah_CORP_NamaKorporasi
            Using objBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(objIfti.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0))
                If Not IsNothing(objBidangUsaha) Then
                    Me.TxtIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Text = objBidangUsaha.NamaBidangUsaha
                    hfIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Value = objBidangUsaha.IdBidangUsaha
                    If hfIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                        Me.TxtIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorpLainnya.Text = objIfti.Sender_Nasabah_CORP_BidangUsahaLainnya
                        Me.trIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorpLainnya.Visible = True
                    End If
                End If

            End Using
            Me.TxtIdenNonSwiftOutPengirimNas_Corp_alamatkorp.Text = objIfti.Sender_Nasabah_CORP_AlamatLengkap
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_CORP_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtIdenNonSwiftOutPengirimNas_Corp_kotakorp.Text = Safe(objSkotaKab.NamaKotaKab)
                hfIdenNonSwiftOutPengirimNas_Corp_kotakorp.Value = Safe(objSkotaKab.IDKotaKab)
                If hfIdenNonSwiftOutPengirimNas_Corp_kotakorp.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TxtIdenNonSwiftOutPengirimNas_Corp_kotakorplain.Text = objIfti.Sender_Nasabah_CORP_KotaKabLainnya
                    Me.trIdenNonSwiftOutPengirimNas_Corp_kotakorplain.Visible = True
                End If
            End If
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_CORP_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtIdenNonSwiftOutPengirimNas_Corp_provKorp.Text = Safe(objSProvinsi.Nama)
                hfIdenNonSwiftOutPengirimNas_Corp_provKorp.Value = Safe(objSProvinsi.IdProvince)
                If hfIdenNonSwiftOutPengirimNas_Corp_provKorp.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TxtIdenNonSwiftOutPengirimNas_Corp_provKorpLain.Text = objIfti.Sender_Nasabah_CORP_PropinsiLainnya
                    Me.trIdenNonSwiftOutPengirimNas_Corp_provKorpLain.Visible = True
                End If
            End If
            Me.TxtIdenNonSwiftOutPengirimNas_Corp_AlamatLuar.Text = objIfti.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
            Me.TxtIdenNonSwiftOutPengirimNas_Corp_AlamatAsal.Text = objIfti.Sender_Nasabah_CORP_LN_AlamatLengkap
        End Using
    End Sub
    Sub FieldSwiftOutSenderCase3()
        'PengirimNonNasabah

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            Me.RbNonSwiftOutPengirimNonNasabah_100Juta.SelectedValue = objIfti.FK_IFTI_NonNasabahNominalType_ID
            Me.TxtNonSwiftOutPengirimNonNasabah_nama.Text = objIfti.Sender_NonNasabah_NamaLengkap
            Me.TxtNonSwiftOutPengirimNonNasabah_TanggalLahir.Text = FormatDate(objIfti.Sender_NonNasabah_TanggalLahir)
            Me.TxtNonSwiftOutPengirimNonNasabah_alamatiden.Text = objIfti.Sender_NonNasabah_ID_Alamat
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_NonNasabah_ID_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtNonSwiftOutPengirimNonNasabah_kotaIden.Text = Safe(objSkotaKab.NamaKotaKab)
                hfNonSwiftOutPengirimNonNasabah_kotaIden.Value = Safe(objSkotaKab.IDKotaKab)
                If hfNonSwiftOutPengirimNonNasabah_kotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TxtNonSwiftOutPengirimNonNasabah_KoaLainIden.Text = objIfti.Sender_NonNasabah_ID_KotaKabLainnya
                    Me.trNonSwiftOutPengirimNonNasabah_KoaLainIden.Visible = True
                End If
            End If
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_NonNasabah_ID_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtNonSwiftOutPengirimNonNasabah_ProvIden.Text = Safe(objSProvinsi.Nama)
                hfNonSwiftOutPengirimNonNasabah_ProvIden.Value = Safe(objSProvinsi.IdProvince)
                If hfNonSwiftOutPengirimNonNasabah_ProvIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TxtNonSwiftOutPengirimNonNasabah_ProvLainIden.Text = objIfti.Sender_NonNasabah_ID_PropinsiLainnya
                    Me.trNonSwiftOutPengirimNonNasabah_ProvLainIden.Visible = True
                End If
            End If

            Me.CboNonSwiftOutPengirimNonNasabah_JenisDokumen.SelectedValue = objIfti.Sender_NonNasabah_FK_IFTI_IDType.GetValueOrDefault(0)
            Me.TxtNonSwiftOutPengirimNonNasabah_NomorIden.Text = objIfti.Sender_NonNasabah_NomorID
        End Using
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                clearSession()
                'AuditTrailBLL.InsertAuditTrailUserAccess(Sahassa.AML.Commonly.SessionCurrentPage)
                SetCOntrolLoad()
                TransactionNonSwiftOut()
                'loadIFTIToField()
                'loadResume()

                'If MultiView1.ActiveViewIndex = 0 Then
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                'End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            'LogError(ex)
        End Try
    End Sub

    'Protected Sub RbNonSwiftOutPengirimNonNasabah_100Juta_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbNonSwiftOutPengirimNonNasabah_100Juta.SelectedIndexChanged
    '    If RbNonSwiftOutPengirimNonNasabah_100Juta.SelectedValue = 0 Then
    '        Me.Mand_lbh100jt.Visible = False
    '    Else
    '        Me.Mand_lbh100jt.Visible = True
    '    End If
    'End Sub

    Protected Sub ImageButton_IdenNonSwiftOutPengirimNas_Ind_Negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_IdenNonSwiftOutPengirimNas_Ind_Negara.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                TxtIdenNonSwiftOutPengirimNas_Ind_Negara.Text = strData(1)
                Me.hfIdenNonSwiftOutPengirimNas_Ind_Negara.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If hfIdenNonSwiftOutPengirimNas_Ind_Negara.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.NegaraLainNonSwift_pengirimPerorangan.Visible = True
                Else
                    Me.NegaraLainNonSwift_pengirimPerorangan.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_IdenNonSwiftOutPengirimNas_Ind_Pekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_IdenNonSwiftOutPengirimNas_Ind_Pekerjaan.Click
        Try
            If Session("PickerPekerjaan.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerPekerjaan.Data")).Split(";")
                TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaan.Text = strData(1)
                hfIdenNonSwiftOutPengirimNas_Ind_pekerjaan.Value = strData(0)
                Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
                If hfIdenNonSwiftOutPengirimNas_Ind_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
                    Me.trIdenNonSwiftOutPengirimNas_Ind_Pekerjaanlain.Visible = True
                Else
                    Me.trIdenNonSwiftOutPengirimNas_Ind_Pekerjaanlain.Visible = False
                End If
            End If
        Catch ex As Exception
            ' LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButtonNonSwiftOutPengirimNasKotaDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonNonSwiftOutPengirimNasKotaDom.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")

                TxtIdenNonSwiftOutPengirimNas_Ind_Kota.Text = strData(1)
                hfIdenNonSwiftOutPengirimNas_Ind_Kota_dom.Value = strData(0)


                hfIdenNonSwiftOutPengirimNas_Ind_provinsi_dom.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                TxtIdenNonSwiftOutPengirimNas_Ind_provinsi.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfIdenNonSwiftOutPengirimNas_Ind_Kota_dom.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trIdenNonSwiftOutPengirimNas_Ind_kotalain.Visible = True
                Else
                    Me.trIdenNonSwiftOutPengirimNas_Ind_kotalain.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButtonNonSwiftOutPengirimNasProvinsiDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonNonSwiftOutPengirimNasProvinsiDom.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                TxtIdenNonSwiftOutPengirimNas_Ind_provinsi.Text = strData(1)
                hfIdenNonSwiftOutPengirimNas_Ind_provinsi_dom.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfIdenNonSwiftOutPengirimNas_Ind_provinsi_dom.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trIdenNonSwiftOutPengirimNas_Ind_provinsi.Visible = True
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButtonNonSwiftOutPengirimNasKotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonNonSwiftOutPengirimNasKotaIden.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                TxtIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Text = strData(1)
                hfIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Value = strData(0)

                hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Visible = True
                Else
                    Me.trIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub imgButtonNonSwiftOutPengirimNasCorp_BidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonNonSwiftOutPengirimNasCorp_BidangUsaha.Click
        Try
            If Session("PickerBidangUsaha.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerBidangUsaha.Data")).Split(";")
                TxtIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Text = strData(1)
                hfIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Value = strData(0)
                Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)
                If hfIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                    Me.trIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorpLainnya.Visible = True
                Else
                    Me.trIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorpLainnya.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButtonNonSwiftOutPengirimNasKorp_kotaLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonNonSwiftOutPengirimNasKorp_kotaLengkap.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                TxtIdenNonSwiftOutPengirimNas_Corp_kotakorp.Text = strData(1)
                hfIdenNonSwiftOutPengirimNas_Corp_kotakorp.Value = strData(0)

                hfIdenNonSwiftOutPengirimNas_Corp_provKorp.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                TxtIdenNonSwiftOutPengirimNas_Corp_provKorp.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfIdenNonSwiftOutPengirimNas_Corp_kotakorp.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trIdenNonSwiftOutPengirimNas_Corp_kotakorplain.Visible = True
                Else
                    Me.trIdenNonSwiftOutPengirimNas_Corp_kotakorplain.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButtonNonSwiftOutPengirimNasKorp_ProvinsiLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonNonSwiftOutPengirimNasKorp_ProvinsiLengkap.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                TxtIdenNonSwiftOutPengirimNas_Corp_provKorp.Text = strData(1)
                hfIdenNonSwiftOutPengirimNas_Corp_provKorp.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfIdenNonSwiftOutPengirimNas_Corp_provKorp.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trIdenNonSwiftOutPengirimNas_Corp_provKorpLain.Visible = True
                Else
                    Me.trIdenNonSwiftOutPengirimNas_Corp_provKorpLain.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButtonNonSwiftOut_PengirimNonNas__Provinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonNonSwiftOut_PengirimNonNas__Provinsi.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                TxtNonSwiftOutPengirimNonNasabah_ProvIden.Text = strData(1)
                hfNonSwiftOutPengirimNonNasabah_ProvIden.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfNonSwiftOutPengirimNonNasabah_ProvIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trNonSwiftOutPengirimNonNasabah_ProvLainIden.Visible = True
                Else
                    Me.trNonSwiftOutPengirimNonNasabah_ProvLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButtonNonSwiftOut_PengirimNonNas_Kota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonNonSwiftOut_PengirimNonNas_Kota.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                TxtNonSwiftOutPengirimNonNasabah_kotaIden.Text = strData(1)
                hfNonSwiftOutPengirimNonNasabah_kotaIden.Value = strData(0)

                hfNonSwiftOutPengirimNonNasabah_ProvIden.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                TxtNonSwiftOutPengirimNonNasabah_ProvIden.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfNonSwiftOutPengirimNonNasabah_kotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trNonSwiftOutPengirimNonNasabah_KoaLainIden.Visible = True
                Else
                    Me.trNonSwiftOutPengirimNonNasabah_KoaLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_NonSwiftOutBOwnerNas_KotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_NonSwiftOutBOwnerNas_KotaIden.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                BenfOwnerNonSwiftOutNasabah_KotaIden.Text = strData(1)
                hfBenfOwnerNonSwiftOutNasabah_KotaIden.Value = strData(0)

                hfBenfOwnerNonSwiftOutNasabah_ProvinsiIden.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                BenfOwnerNonSwiftOutNasabah_ProvinsiIden.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfBenfOwnerNonSwiftOutNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trBenfOwnerNonSwiftOutNasabah_kotaLainIden.Visible = True
                Else
                    Me.trBenfOwnerNonSwiftOutNasabah_kotaLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_NonSwiftOutBOwnerNas_ProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_NonSwiftOutBOwnerNas_ProvinsiIden.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                BenfOwnerNonSwiftOutNasabah_ProvinsiIden.Text = strData(1)
                hfBenfOwnerNonSwiftOutNasabah_ProvinsiIden.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfBenfOwnerNonSwiftOutNasabah_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trBenfOwnerNonSwiftOutNasabah_ProvinsiLainIden.Visible = True
                Else
                    Me.trBenfOwnerNonSwiftOutNasabah_ProvinsiLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_NonSwiftOutBOwnerNonNas_kota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_NonSwiftOutBOwnerNonNas_kota.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                BenfOwnerNonSwiftOutNonNasabah_KotaIden.Text = strData(1)
                hfBenfOwnerNonSwiftOutNonNasabah_KotaIden.Value = strData(0)

                hfBenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                BenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfBenfOwnerNonSwiftOutNonNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trBenfOwnerNonSwiftOutNonNasabah_KotaLainIden.Visible = True
                Else
                    Me.trBenfOwnerNonSwiftOutNonNasabah_KotaLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_NonSwiftOutBOwnerNonNas_Provinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_NonSwiftOutBOwnerNonNas_Provinsi.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                BenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Text = strData(1)
                hfBenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfBenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trBenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Visible = True
                Else
                    Me.trBenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_NonSwiftOutPengirimNas_IND_negaraWarga_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_NonSwiftOutPengirimNas_IND_negaraWarga.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtPenerimaNonSwiftOutNasabah_IND_negara.Text = strData(1)
                hfNonSwiftOutPengirimNasabah_IND_negara.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If hfNonSwiftOutPengirimNasabah_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.trPenerimaNonSwiftOutNasabah_IND_negaraLainIden.Visible = True
                Else
                    Me.trPenerimaNonSwiftOutNasabah_IND_negaraLainIden.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub hfNonSwiftOutPengirimNasabah_IND_negaraIden_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hfNonSwiftOutPengirimNasabah_IND_negaraIden.ValueChanged
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtPenerimaNonSwiftOutNasabah_IND_negaraIden.Text = strData(1)
                Me.hfNonSwiftOutPengirimNasabah_IND_negaraIden.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If hfNonSwiftOutPengirimNasabah_IND_negaraIden.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.trPenerimaNonSwiftOutNasabah_IND_negaraLainIden.Visible = True
                Else
                    Me.trPenerimaNonSwiftOutNasabah_IND_negaraLainIden.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_NonSwiftOutPengirimNas_Korp_bidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_NonSwiftOutPengirimNas_Korp_bidangUsaha.Click
        Try
            If Session("PickerBidangUsaha.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerBidangUsaha.Data")).Split(";")
                txtPenerimaNonSwiftOutNasabah_Korp_BidangUsahaKorp.Text = strData(1)
                hfNonSwiftOutPengirimNasabah_Korp_BidangUsahaKorp.Value = strData(0)
                Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)
                If hfNonSwiftOutPengirimNasabah_Korp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                    Me.trPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Visible = True
                Else
                    Me.trPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_PenerimaNonSwiftOutNasabah_Korp_NegaraKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Text = strData(1)
                Me.hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If Me.hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.trPenerimaNonSwiftOutNasabah_Korp_NegaraLainnyaKorp.Visible = True
                Else
                    Me.trPenerimaNonSwiftOutNasabah_Korp_NegaraLainnyaKorp.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButton_PenerimaNonSwiftOutNonNasabah_Negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_PenerimaNonSwiftOutNonNasabah_Negara.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtPenerimaNonSwiftOutNonNasabah_Negara.Text = strData(1)
                hfSwiftInNonSwiftOutPengirimNonNasabah_Negara.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If hfSwiftInNonSwiftOutPengirimNonNasabah_Negara.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.trPenerimaNonSwiftOutNonNasabah_negaraLain.Visible = True
                Else
                    Me.trPenerimaNonSwiftOutNonNasabah_negaraLain.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButton_Transaksi_NonSwiftOut_currency_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_Transaksi_NonSwiftOut_currency.Click
        Try
            If Session("PickerMataUang.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerMataUang.Data")).Split(";")
                Dim ParameterMataUang As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.MataUang)
                'Transaksi_NonSwiftOut_currency.Text = strData(0) & " - " & strData(1)
                'hfTransaksi_NonSwiftOut_currency.Value = strData(0)
                Using objMatauang As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged("Code = '" & strData(0).Trim & "'", "", 0, 1, 0)
                    If objMatauang.Count > 0 Then
                        Transaksi_NonSwiftOut_currency.Text = objMatauang(0).Code & "-" & objMatauang(0).Code
                        hfTransaksi_NonSwiftOut_currency.Value = objMatauang(0).IdCurrency
                        If hfTransaksi_NonSwiftOut_currency.Value = ParameterMataUang.MsSystemParameter_Value Then
                            Me.trNonSwiftOut_currencyLain.Visible = True
                        Else
                            Me.trNonSwiftOut_currencyLain.Visible = False
                        End If
                    End If
                End Using
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButton_Transaksi_NonSwiftOut_MataUangTransaksi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_Transaksi_NonSwiftOut_MataUangTransaksi.Click
        Try
            If Session("PickerMataUang.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerMataUang.Data")).Split(";")
                Dim ParameterMataUang As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.MataUang)
                'Transaksi_NonSwiftOut_MataUangTransaksi.Text = strData(0) & " - " & strData(1)
                'hfTransaksi_NonSwiftOut_MataUangTransaksi.Value = strData(0)
                Using objMatauang As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged("Code = '" & strData(0).Trim & "'", "", 0, 1, 0)
                    If objMatauang.Count > 0 Then
                        Transaksi_NonSwiftOut_MataUangTransaksi.Text = objMatauang(0).Code
                        hfTransaksi_NonSwiftOut_MataUangTransaksi.Value = objMatauang(0).IdCurrency
                        If hfTransaksi_NonSwiftOut_MataUangTransaksi.Value = ParameterMataUang.MsSystemParameter_Value Then
                            Me.trNonSwiftOut_MataUangTransaksiLain.Visible = True
                        Else
                            Me.trNonSwiftOut_MataUangTransaksiLain.Visible = False
                        End If
                    End If
                End Using
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButtonCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCancel.Click
        Response.Redirect("IFTIView.aspx")
    End Sub

    Protected Sub ImageButtonSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSave.Click
        SaveNonSwiftOut()
    End Sub

    Protected Sub CheckNonSwiftOutPengirimNas_Ind_alamat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckNonSwiftOutPengirimNas_Ind_alamat.CheckedChanged
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)

        If CheckNonSwiftOutPengirimNas_Ind_alamat.Checked = True Then
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_alamatIdentitas.Text = TxtIdenNonSwiftOutPengirimNas_Ind_Alamat.Text
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Text = TxtIdenNonSwiftOutPengirimNas_Ind_Kota.Text
            hfIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Value = hfIdenNonSwiftOutPengirimNas_Ind_Kota_dom.Value
            If hfIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Value = ParameterKota.MsSystemParameter_Value Then
                Me.TxtIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Text = TxtIdenNonSwiftOutPengirimNas_Ind_kotalain.Text
                Me.trIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Visible = True
            Else
                Me.trIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Visible = False
            End If
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Text = TxtIdenNonSwiftOutPengirimNas_Ind_provinsi.Text
            hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Value = hfIdenNonSwiftOutPengirimNas_Ind_provinsi_dom.Value
            If hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                Me.TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Text = TxtIdenNonSwiftOutPengirimNas_Ind_provinsiLain.Text
                Me.trIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Visible = True
            Else
                Me.trIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Visible = False
            End If


        Else
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_alamatIdentitas.Text = ""
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Text = ""
            hfIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Value = ""
            Me.trIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Visible = False
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Text = ""
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Text = ""
            hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Value = ""
            Me.trIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Visible = False
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Text = ""

        End If


    End Sub

    Protected Sub RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan.SelectedIndexChanged
        If RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan.SelectedValue = 1 Then
            Me.NegaraNonSwift_pengirimPerorangan.Visible = False
            Me.NegaraLainNonSwift_pengirimPerorangan.Visible = False
        Else
            Me.NegaraNonSwift_pengirimPerorangan.Visible = True
            'Me.NegaraLainNonSwift_pengirimPerorangan.Visible = True

        End If
    End Sub

    Protected Sub RbNonSwiftOutPenerimaNasabah_IND_Warganegara_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbNonSwiftOutPenerimaNasabah_IND_Warganegara.SelectedIndexChanged
        If RbNonSwiftOutPenerimaNasabah_IND_Warganegara.SelectedValue = 1 Then
            Me.NegaraNonSwift_penerimaPerorangan.Visible = False
            Me.NegaraLainNonSwift_penerimaPerorangan.Visible = False
        Else
            Me.NegaraNonSwift_penerimaPerorangan.Visible = True
            'Me.NegaraLainNonSwift_penerimaPerorangan.Visible = True
        End If
    End Sub
    Protected Sub cvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageErr.PreRender
        If cvalPageErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Protected Sub imgButtonNonSwiftOutPengirimNasProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonNonSwiftOutPengirimNasProvinsiIden.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Text = strData(1)
                hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Visible = True
                Else
                    Me.trIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_NonSwiftOutPengirimNas_IND_negaraIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_NonSwiftOutPengirimNas_IND_negaraIden.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtPenerimaNonSwiftOutNasabah_IND_negaraIden.Text = strData(1)
                Me.hfNonSwiftOutPengirimNasabah_IND_negaraIden.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If Me.hfNonSwiftOutPengirimNasabah_IND_negaraIden.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.trPenerimaNonSwiftOutNasabah_IND_negaraLainIden.Visible = True
                Else
                    Me.trPenerimaNonSwiftOutNasabah_IND_negaraLainIden.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgOKMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOKMsg.Click
        Response.Redirect("Iftiview.aspx")
    End Sub

    Protected Sub Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedIndexChanged
        Me.trNonSwiftOutTipeNasabah.Visible = True

        'Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1
        'Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue = 1
        'Me.trNonSwiftOutTipeNasabah.Visible = True
        'Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 0
        'Me.MultiViewNonSwiftOutPengirimNasabah.ActiveViewIndex = 0

        If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1 Then
            Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1

            'If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue = 1 Then
            Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1
            Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue = 1
            Me.trNonSwiftOutTipeNasabah.Visible = True
            Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 0
            Me.MultiViewNonSwiftOutPengirimNasabah.ActiveViewIndex = 0
            'Else
            '    Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1
            '    Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue = 2
            '    Me.trNonSwiftOutTipeNasabah.Visible = True
            '    Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 0
            '    Me.MultiViewNonSwiftOutPengirimNasabah.ActiveViewIndex = 1
            'End If



        Else
            Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 2
            Me.trNonSwiftOutTipeNasabah.Visible = False
            Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 1
        End If
    End Sub


    Protected Sub Rb_BOwnerNas_ApaMelibatkan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_BOwnerNas_ApaMelibatkan.SelectedIndexChanged
        If Rb_BOwnerNas_ApaMelibatkan.SelectedValue = 1 Then
            Me.BenfOwnerTipePengirim.Visible = True
            Me.BenfOwnerHubungan.Visible = True
            MultiViewNonSwiftOutBOwner.Visible = True
        Else
            Me.BenfOwnerTipePengirim.Visible = False
            Me.BenfOwnerHubungan.Visible = False
            MultiViewNonSwiftOutBOwner.Visible = False
        End If
    End Sub

    Protected Sub Rb_NonSwiftOutBOwnerNas_TipePengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_NonSwiftOutBOwnerNas_TipePengirim.SelectedIndexChanged
        If Rb_NonSwiftOutBOwnerNas_TipePengirim.SelectedValue = 1 Then
            Me.MultiViewNonSwiftOutBOwner.ActiveViewIndex = 0
        Else
            Me.MultiViewNonSwiftOutBOwner.ActiveViewIndex = 1
        End If
    End Sub

    Protected Sub Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedIndexChanged
        'If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue = 1 Then
        '    MultiViewNonSwiftOutPengirimNasabah.ActiveViewIndex = 0
        'Else
        '    MultiViewNonSwiftOutPengirimNasabah.ActiveViewIndex = 1
        'End If
        If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue = 1 Then
            Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1
            Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue = 1
            Me.trNonSwiftOutTipeNasabah.Visible = True
            Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 0
            Me.MultiViewNonSwiftOutPengirimNasabah.ActiveViewIndex = 0
            RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan.SelectedIndex = 0
        Else
            Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1
            Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue = 2
            Me.trNonSwiftOutTipeNasabah.Visible = True
            Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 0
            Me.MultiViewNonSwiftOutPengirimNasabah.ActiveViewIndex = 1
        End If
    End Sub

    Protected Sub RBNonSwiftOutPenerimaNasabah_TipePengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBNonSwiftOutPenerimaNasabah_TipePengirim.SelectedIndexChanged
        If RBNonSwiftOutPenerimaNasabah_TipePengirim.SelectedValue = "1" Then
            Me.trNonSwiftOutPenerimaTipeNasabah.Visible = True
            Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedValue = 1
            'If Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedValue = "1" Then
            '    Me.MultiViewNonSwiftOutIdenPenerima.ActiveViewIndex = 0
            '    Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 0
            'ElseIf Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedValue = "2" Then
            '    Me.MultiViewNonSwiftOutIdenPenerima.ActiveViewIndex = 0
            '    Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 1
            'End If
            Me.MultiViewNonSwiftOutIdenPenerima.ActiveViewIndex = 0
            Me.MultiViewNonSwiftOutPenerimaNasabah.ActiveViewIndex = 0
            RbNonSwiftOutPenerimaNasabah_IND_Warganegara.SelectedIndex = 0
        ElseIf RBNonSwiftOutPenerimaNasabah_TipePengirim.SelectedValue = "2" Then
            Me.trNonSwiftOutPenerimaTipeNasabah.Visible = False
            Me.MultiViewNonSwiftOutIdenPenerima.ActiveViewIndex = 1
        End If
    End Sub

    Protected Sub Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedIndexChanged
        If Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedValue = 1 Then
            Me.MultiViewNonSwiftOutIdenPenerima.ActiveViewIndex = 0
            Me.MultiViewNonSwiftOutPenerimaNasabah.ActiveViewIndex = 0
            RbNonSwiftOutPenerimaNasabah_IND_Warganegara.SelectedIndex = 0
        ElseIf Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedValue = 2 Then
            Me.MultiViewNonSwiftOutIdenPenerima.ActiveViewIndex = 0
            Me.MultiViewNonSwiftOutPenerimaNasabah.ActiveViewIndex = 1

        End If
    End Sub

    Protected Sub RbNonSwiftOutPengirimNonNasabah_100Juta_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbNonSwiftOutPengirimNonNasabah_100Juta.SelectedIndexChanged
        If RbNonSwiftOutPengirimNonNasabah_100Juta.SelectedValue = 1 Then
            Me.Mand_Nomor.Visible = False
            Me.Mand_JenisDokumen.Visible = False
        ElseIf RbNonSwiftOutPengirimNonNasabah_100Juta.SelectedValue = 2 Then
            Me.Mand_Nomor.Visible = True
            Me.Mand_JenisDokumen.Visible = True
        End If
    End Sub


    Protected Sub RMIdenNonSwiftOutPengirimNas_Ind_Negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMIdenNonSwiftOutPengirimNas_Ind_Negara.Click
        Me.TxtIdenNonSwiftOutPengirimNas_Ind_Negara.Text = ""
        Me.hfIdenNonSwiftOutPengirimNas_Ind_Negara.Value = Nothing
    End Sub

    Protected Sub RMIdenNonSwiftOutPengirimNas_Ind_Pekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMIdenNonSwiftOutPengirimNas_Ind_Pekerjaan.Click
        Me.TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaan.Text = ""
        Me.hfIdenNonSwiftOutPengirimNas_Ind_pekerjaan.Value = Nothing
    End Sub

    Protected Sub RMNonSwiftOutPengirimNasKotaDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMNonSwiftOutPengirimNasKotaDom.Click
        Me.TxtIdenNonSwiftOutPengirimNas_Ind_Kota.Text = ""
        Me.hfIdenNonSwiftOutPengirimNas_Ind_Kota_dom.Value = Nothing
    End Sub

    Protected Sub RMNonSwiftOutPengirimNasProvinsiDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMNonSwiftOutPengirimNasProvinsiDom.Click
        Me.TxtIdenNonSwiftOutPengirimNas_Ind_provinsi.Text = ""
        Me.hfIdenNonSwiftOutPengirimNas_Ind_provinsi_dom.Value = Nothing
    End Sub

    Protected Sub RMNonSwiftOutPengirimNasKotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMNonSwiftOutPengirimNasKotaIden.Click
        Me.TxtIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Text = ""
        Me.hfIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Value = Nothing
    End Sub

    Protected Sub RMNonSwiftOutPengirimNasProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMNonSwiftOutPengirimNasProvinsiIden.Click
        Me.TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Text = ""
        Me.hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Value = Nothing
    End Sub

    Protected Sub RMNonSwiftOutPengirimNasCorp_BidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMNonSwiftOutPengirimNasCorp_BidangUsaha.Click
        Me.TxtIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Text = ""
        Me.hfIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Value = Nothing
    End Sub

    Protected Sub RMNonSwiftOutPengirimNasKorp_kotaLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMNonSwiftOutPengirimNasKorp_kotaLengkap.Click
        Me.TxtIdenNonSwiftOutPengirimNas_Corp_kotakorp.Text = ""
        Me.hfIdenNonSwiftOutPengirimNas_Corp_kotakorp.Value = Nothing
    End Sub

    Protected Sub RMNonSwiftOutPengirimNasKorp_ProvinsiLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMNonSwiftOutPengirimNasKorp_ProvinsiLengkap.Click
        Me.TxtIdenNonSwiftOutPengirimNas_Corp_provKorp.Text = ""
        Me.hfIdenNonSwiftOutPengirimNas_Corp_provKorp.Value = Nothing
    End Sub

    Protected Sub ImageButton10_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton10.Click
        Me.TxtNonSwiftOutPengirimNonNasabah_kotaIden.Text = ""
        Me.hfNonSwiftOutPengirimNonNasabah_kotaIden.Value = Nothing
    End Sub

    Protected Sub ImageButton11_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton11.Click
        Me.TxtNonSwiftOutPengirimNonNasabah_ProvIden.Text = ""
        Me.hfNonSwiftOutPengirimNonNasabah_ProvIden.Value = Nothing
    End Sub

    Protected Sub ImageButton12_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton12.Click
        Me.BenfOwnerNonSwiftOutNasabah_KotaIden.Text = ""
        Me.hfBenfOwnerNonSwiftOutNasabah_KotaIden.Value = Nothing
    End Sub

    Protected Sub ImageButton13_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton13.Click
        Me.BenfOwnerNonSwiftOutNasabah_ProvinsiIden.Text = ""
        Me.hfBenfOwnerNonSwiftOutNasabah_ProvinsiIden.Value = Nothing
    End Sub

    Protected Sub ImageButton14_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton14.Click
        Me.BenfOwnerNonSwiftOutNonNasabah_KotaIden.Text = ""
        Me.hfBenfOwnerNonSwiftOutNonNasabah_KotaIden.Value = Nothing
    End Sub

    Protected Sub ImageButton15_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton15.Click
        Me.BenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Text = ""
        Me.hfBenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Value = Nothing
    End Sub

    Protected Sub ImageButton16_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton16.Click
        Me.txtPenerimaNonSwiftOutNasabah_IND_negara.Text = ""
        Me.hfNonSwiftOutPengirimNasabah_IND_negara.Value = Nothing
    End Sub

    Protected Sub RMNonSwiftOutPengirimNas_IND_negaraIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMNonSwiftOutPengirimNas_IND_negaraIden.Click
        Me.txtPenerimaNonSwiftOutNasabah_IND_negaraIden.Text = ""
        Me.hfNonSwiftOutPengirimNasabah_IND_negaraIden.Value = Nothing
    End Sub

    Protected Sub RMNonSwiftOutPengirimNas_Korp_bidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMNonSwiftOutPengirimNas_Korp_bidangUsaha.Click
        Me.txtPenerimaNonSwiftOutNasabah_Korp_BidangUsahaKorp.Text = ""
        Me.hfNonSwiftOutPengirimNasabah_Korp_BidangUsahaKorp.Value = Nothing
    End Sub

    Protected Sub RMPenerimaNonSwiftOutNasabah_Korp_NegaraKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Click
        Me.txtPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Text = ""
        Me.hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Value = Nothing
    End Sub

    Protected Sub RMPenerimaNonSwiftOutNonNasabah_Negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPenerimaNonSwiftOutNonNasabah_Negara.Click
        Me.txtPenerimaNonSwiftOutNonNasabah_Negara.Text = ""
        Me.hfSwiftInNonSwiftOutPengirimNonNasabah_Negara.Value = Nothing
    End Sub

    Protected Sub RMTransaksi_NonSwiftOut_MataUangTransaksi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMTransaksi_NonSwiftOut_MataUangTransaksi.Click
        Me.Transaksi_NonSwiftOut_MataUangTransaksi.Text = ""
        Me.hfTransaksi_NonSwiftOut_MataUangTransaksi.Value = Nothing
    End Sub

    Protected Sub RMTransaksi_NonSwiftOut_currency_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMTransaksi_NonSwiftOut_currency.Click
        Me.Transaksi_NonSwiftOut_currency.Text = ""
        Me.hfTransaksi_NonSwiftOut_currency.Value = Nothing
    End Sub

    Protected Sub cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.SelectedIndexChanged
        Dim ParameterBentukUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BentukBadanUsaha)
        If cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.SelectedValue = ParameterBentukUsaha.MsSystemParameter_Value Then
            Me.trNonSwiftOutPengirimNas_Corp_BentukBadanUsahaLain.Visible = True
        Else
            Me.trNonSwiftOutPengirimNas_Corp_BentukBadanUsahaLain.Visible = False
        End If
    End Sub
End Class
