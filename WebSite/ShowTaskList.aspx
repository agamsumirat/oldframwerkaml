﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowTaskList.aspx.vb" Inherits="ShowTaskList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Theme/aml.css" rel="stylesheet" />
    </head>
<body>
    <form id="form1" runat="server">
    <div>
        <strong>Task List</strong>
        <br />
        <asp:GridView ID="grdTaskList" runat="server" AllowPaging="True" AllowSorting="True" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True" ForeColor="Black" GridLines="Vertical">
            <AlternatingRowStyle BackColor="White" />
            <FooterStyle BackColor="#CCCC99" />
            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <RowStyle BackColor="#F7F7DE" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
        </asp:GridView>
        <br />
        <asp:imagebutton id="ImageSearch" runat="server" CausesValidation="True" ImageUrl="~/Images/button/ok.gif" OnClientClick="window.close();"></asp:imagebutton>
    </div>
    </form>
</body>
</html>
