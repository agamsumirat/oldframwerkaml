<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="RiskFactEdit.aspx.vb" Inherits="RiskFactEdit" title="Risk Fact - Edit" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>


<%@ Register Src="webcontrol/HavingCondition.ascx" TagName="HavingCondition" TagPrefix="uc5" %>

<%@ Register Src="webcontrol/GroupBy.ascx" TagName="GroupBy" TagPrefix="uc4" %>

<%@ Register Src="webcontrol/WhereClauses.ascx" TagName="WhereClauses" TagPrefix="uc3" %>

<%@ Register Src="webcontrol/Fields.ascx" TagName="Fields" TagPrefix="uc2" %>

<%@ Register Src="webcontrol/Tables.ascx" TagName="Tables" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Risk Fact - Edit&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000">* Required</span></td>
        </tr>
    </table>	
    <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" width="100%">
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextRiskFactName"
                    Display="Dynamic" ErrorMessage="Risk Fact Name is required">*</asp:RequiredFieldValidator></td>
            <td bgcolor="#ffffff" style="color: #000000; height: 24px" width="20%">
                Risk Fact &nbsp;Name</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:TextBox ID="TextRiskFactName" runat="server" CssClass="textBox" MaxLength="100"></asp:TextBox><span><strong>
                    </strong></span></td>
        </tr>
        <tr class="formText" style="font-weight:  color: #000000">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <br />
                <asp:CustomValidator ID="CustomValidator1" runat="server" Display="Dynamic" EnableClientScript="False">*</asp:CustomValidator></td>
            <td bgcolor="#ffffff" rowspan="1" style="color: #000000; height: 24px">
                Risk Fact
                Description</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                <span style="color: #ff0000">:</span></td>
            <td bgcolor="#ffffff" rowspan="1" style="color: #ff0000; height: 24px">
                <span style="color: #ff0000"><asp:TextBox
                    ID="TextDescription" runat="server" CssClass="textBox" Height="63px" MaxLength="255"
                    TextMode="MultiLine" Width="289px"></asp:TextBox></span></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="color: #000000; height: 24px">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="color: #000000; height: 24px">
                <asp:CheckBox ID="chkEnabled" runat="server" Text="Enabled" /></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextRiskScore"
                    Display="Dynamic" ErrorMessage="Risk Fact  is required">*</asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="TextRiskScore"
                    Display="Dynamic" ErrorMessage="Risk Fact Score Must Numeric" MaximumValue="255"
                    MinimumValue="1" Type="Integer">*</asp:RangeValidator></td>
            <td bgcolor="#ffffff" rowspan="1" style="color: #000000; height: 24px">
                Risk Fact Score</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                :</td>
            <td bgcolor="#ffffff" rowspan="1" style="color: #000000; height: 24px">
                <asp:TextBox ID="TextRiskScore" runat="server" CssClass="textBox" MaxLength="100"></asp:TextBox></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" rowspan="1" style="color: #ff0000; height: 24px">
                &nbsp;
                <asp:Menu ID="Menu1" runat="server" Orientation="Horizontal"   CssClass="tabs" StaticEnableDefaultPopOutImage="False">
                    <Items>
                        <asp:MenuItem Text="Tables" Value="0"></asp:MenuItem>
                        <asp:MenuItem Text="Conditions" Value="1"></asp:MenuItem>
                        <asp:MenuItem Text="Group By" Value="2"></asp:MenuItem>
                        <asp:MenuItem Text="Having Condition" Value="3"></asp:MenuItem>
                      
                    </Items>
                    <StaticSelectedStyle CssClass="selectedTab" />
                        <StaticMenuItemStyle CssClass="tab" />
                </asp:Menu>
                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="TabTables" runat="server">
                        <uc1:Tables ID="Tablestest1" runat="server" StrSessionName="RiskFactEdit" />
                        &nbsp;
                       </asp:View>
                    &nbsp;
                    <asp:View ID="TabConditions" runat="server">
                        <uc3:whereclauses id="WhereClausesTest1" runat="server" StrSessionName="RiskFactEdit"></uc3:whereclauses>
                    </asp:View>
                    <asp:View ID="TabGroupBy" runat="server">
                        <uc4:GroupBy ID="GroupBy1" runat="server" StrSessionName="RiskFactEdit" />
                    </asp:View>
                    <asp:View ID="TabHavingCondition" runat="server">
                        <uc5:HavingCondition id="HavingCondition1" runat="server" StrSessionName="RiskFactEdit">
                        </uc5:HavingCondition>
                    </asp:View>
                  
                </asp:MultiView></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="color: #000000; height: 24px">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="color: #ff0000; height: 24px">
            </td>
        </tr>
      
        <tr class="formText">
        </tr>
        <tr class="formText">
        </tr>
        <tr bgcolor="#dddddd" class="formText" height="30">
            <td style="width: 24px">
                <img height="15" src="images/arrow.gif" width="15" /></td>
            <td colspan="3" style="height: 9px">
                <table border="0" cellpadding="3" cellspacing="0">
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="True" SkinID="SaveButton" /></td>
                        <td>
                            <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton" /></td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="none"></asp:CustomValidator></td>
        </tr>
    </table>
</asp:Content>

