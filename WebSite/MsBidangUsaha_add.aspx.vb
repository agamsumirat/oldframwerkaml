#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
#End Region

Partial Class MsBidangUsaha_add
    Inherits Parent



#Region "Function"

    ''' <summary>
    ''' Validasi men-handle seluruh syarat2 data valid
    ''' data yang lolos dari validasi dipastikan lolos ke database 
    ''' karena menggunakan FillorNothing
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ValidasiControl()

        '======================== Validasi textbox :  ID canot Null ====================================================
        If ObjectAntiNull(txtIdBidangUsaha.Text) = False Then Throw New Exception("ID  must be filled  ")
        If Not IsNumeric(Me.txtIdBidangUsaha.Text) Then
            Throw New Exception("ID must be in number format")
        End If
        '======================== Validasi textbox :  Nama canot Null ====================================================
        If ObjectAntiNull(txtNamaBidangUsaha.Text) = False Then Throw New Exception("Nama  must be filled  ")


        If Not DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(txtIdBidangUsaha.Text) Is Nothing Then
            Throw New Exception("ID already exist in the database")
        End If

        If DataRepository.MsBidangUsaha_ApprovalDetailProvider.GetPaged(MsBidangUsaha_ApprovalDetailColumn.IdBidangUsaha.ToString & "=" & txtIdBidangUsaha.Text, "", 0, 1, Nothing).Count > 0 Then
            Throw New Exception("Data exist in List Waiting Approval")
        End If


        'If LBMapping.Items.Count = 0 Then Throw New Exception("data doesnt have list Mapping")
    End Sub

#End Region

#Region "events..."
    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Response.Redirect("MsBidangUsaha_View.aspx")
    End Sub
    Protected Sub imgBrowse_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBrowse.Click
        Try
            If Not Get_DataFromPicker Is Nothing Then
                Listmaping.AddRange(Get_DataFromPicker)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgDeleteItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDeleteItem.Click
        Try
            If LBMapping.SelectedIndex < 0 Then
                Throw New Exception("No data Selected")
            End If
            Listmaping.RemoveAt(LBMapping.SelectedIndex)

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                Listmaping = New TList(Of MappingBidangUsahaNCBS)
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub



    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then
                ValidasiControl()
                'create audittrailtype
                'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsNamaBidangUsaha_Approval As New MsBidangUsaha_Approval
                    With ObjMsNamaBidangUsaha_Approval
                        FillOrNothing(.FK_MsMode_Id, 1, True, oInt)
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsBidangUsaha_ApprovalProvider.Save(ObjMsNamaBidangUsaha_Approval)
                    KeyHeaderApproval = ObjMsNamaBidangUsaha_Approval.PK_MsBidangUsaha_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsBidangUsaha_ApprovalDetail As New MsBidangUsaha_ApprovalDetail()
                    With objMsBidangUsaha_ApprovalDetail
                        FillOrNothing(.IdBidangUsaha, txtIdBidangUsaha.Text, True, oInt)
                        FillOrNothing(.NamaBidangUsaha, txtNamaBidangUsaha.Text, True, Ovarchar)

                        FillOrNothing(.Activation, 1, True, oBit)
                        FillOrNothing(.CreatedDate, Date.Now)
                        FillOrNothing(.CreatedBy, SessionUserId)
                        FillOrNothing(.FK_MsBidangUsaha_Approval_Id, KeyHeaderApproval)
                    End With
                    DataRepository.MsBidangUsaha_ApprovalDetailProvider.Save(objMsBidangUsaha_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsBidangUsaha_ApprovalDetail.PK_MsBidangUsaha_ApprovalDetail_Id
                    'Insert auditrail
                    InsertAuditTrail()
                    '========= Insert mapping item 
                    Dim LobjMappingMsNamaBidangUsahaNCBSPPATK_Approval_Detail As New TList(Of MappingBidangUsahaNCBS_Approval_Detail)
                    For Each objMappingMsNamaBidangUsahaNCBSPPATK As MappingBidangUsahaNCBS In Listmaping
                        Dim Mapping As New MappingBidangUsahaNCBS_Approval_Detail
                        With Mapping
                            FillOrNothing(.BidangUsahaId, objMsBidangUsaha_ApprovalDetail.IdBidangUsaha)
                            FillOrNothing(.IDBidangUsahaNCBS, objMappingMsNamaBidangUsahaNCBSPPATK.IDBidangUsahaNCBS)
                            FillOrNothing(.PK_MsBidangUsaha_Approval_id, KeyHeaderApproval)
                            FillOrNothing(.PK_MappingBidangUsahaNCBS_Approval_detail_id, keyHeaderApprovalDetail)
                            FillOrNothing(.NAMA, objMappingMsNamaBidangUsahaNCBSPPATK.Nama)
                            LobjMappingMsNamaBidangUsahaNCBSPPATK_Approval_Detail.Add(Mapping)
                        End With
                    Next
                    DataRepository.MappingBidangUsahaNCBS_Approval_DetailProvider.Save(LobjMappingMsNamaBidangUsahaNCBSPPATK_Approval_Detail)
                    'session Maping item Clear
                    Listmaping.Clear()
                    LblConfirmation.Text = "MsBidangUsaha data is waiting approval for Insert"
                    MtvMsUser.ActiveViewIndex = 1
                    ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Try
            ClearAllControl(VwAdd)
            MtvMsUser.ActiveViewIndex = 0
            ImgBtnSave.Visible = True
            ImgBackAdd.Visible = True
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Response.Redirect("MsBidangUsaha_View.aspx")
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub

#End Region

#Region "Property..."
    ''' <summary>
    ''' Mengambil Data dari Picker
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Get_DataFromPicker() As TList(Of MappingBidangUsahaNCBS)
        Get
            Dim Temp As New TList(Of MappingBidangUsahaNCBS)
            Dim L_MsBidangUsahaNCBS As TList(Of MsBidangUsahaNCBS)
            Try
                If Session("PickerMSBIDANGUSAHANCBS.Data") Is Nothing Then
                    Throw New Exception
                End If
                L_MsBidangUsahaNCBS = Session("PickerMSBIDANGUSAHANCBS.Data")
                For Each i As MsBidangUsahaNCBS In L_MsBidangUsahaNCBS
                    Dim Tempmapping As New MappingBidangUsahaNCBS
                    With Tempmapping
                        .IDBidangUsahaNCBS = i.IDBidangUsahaNCBS
                        .Nama = i.NamaBidangUsahaNCBS
                    End With
                    Temp.Add(Tempmapping)
                Next
            Catch ex As Exception
                L_MsBidangUsahaNCBS = Nothing
            End Try
            Return Temp
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingBidangUsahaNCBS)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingBidangUsahaNCBS))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingBidangUsahaNCBS In Listmaping.FindAllDistinct("IDBidangUsahaNCBS")
                Temp.Add(i.IDBidangUsahaNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBidangUsaha", "ID", "Add", "", txtIdBidangUsaha.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBidangUsaha", "Nama", "Add", "", txtNamaBidangUsaha.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBidangUsaha", "Mapping Item", "Add", "", LBMapping.Text, "Accepted")
            End Using
        Catch
            Throw
        End Try
    End Sub

End Class



