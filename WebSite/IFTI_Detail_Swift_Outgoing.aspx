<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" 
CodeFile="IFTI_Detail_Swift_Outgoing.aspx.vb" Inherits="IFTI_Detail_Swift_Outgoing" title="Untitled Page" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<script src="Script/popcalendar.js"></script>
	

	<script language="javascript" type="text/javascript">
	  function hidePanel(objhide,objpanel,imgmin,imgmax)
	  {
		document.getElementById(objhide).style.display='none';
		document.getElementById(objpanel).src=imgmax;
	  }
	  // JScript File

	 
	   function popWinNegara()
	  {
		var height = '600px';
		var width = '550px';
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

		window.showModalDialog("PickerNegara.aspx", "#3", winSetting);
	  }
	 
	  function popWinProvinsi()
	  {
		var height = '600px';
		var width = '550px';
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

		window.showModalDialog("PickerProvinsi.aspx", "#5", winSetting);
	  }
	  function popWinPekerjaan()
	  {
		var height = '600px';
		var width = '550px';
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

		window.showModalDialog("PickerPekerjaan.aspx", "#6", winSetting);
	  }
	  function popWinKotaKab()
	  {
		var height = '600px';
		var width = '550px';
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

		window.showModalDialog("PickerKotaKab.aspx", "#7", winSetting);
	  }
	  function popWinBidangUsaha()
	  {
		var height = '600px';
		var width = '550px';
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

		window.showModalDialog("PickerBidangUsaha.aspx", "#8", winSetting);
	  }
    function popWinMataUang() {
            var height = '600px';
            var width = '550px';
            var left = (screen.availWidth - width) / 2;
            var top = (screen.availHeight - height) / 2;
            var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

            window.showModalDialog("PickerMataUang.aspx", "#4", winSetting);

        }

	</script>

    <table cellpadding="0" cellspacing="0" border="0" style="height: 100%; width: 100%;">
        <tr>
			<td style="width: 8px; height: 19px">
				<img src="Images/blank.gif" width="5" height="1" /></td>
			<td style="height: 19px">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td style="height: 15px">
							</td>
						<td width="99%" bgcolor="#FFFFFF" style="height: 100%">
							<img src="Images/blank.gif" width="1" height="1" /></td>
						<td bgcolor="#FFFFFF" style="height: 15px">
							<img src="Images/blank.gif" width="1" height="1" /></td>
					</tr>
				</table>
			</td>
			<td style="height: 19px">
				<img src="Images/blank.gif" width="5" height="1" /></td>
		</tr>
        <tr>
            <td style="height: 100%; width: 8px;">
            </td>
            <td id="tdcontent" valign="top" bgcolor="#FFFFFF">
               <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">                                             
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="10" height="100%" /></td>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                             <ajax:AjaxPanel ID="a" runat="server">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="There were uncompleteness on the page:"
                                        Width="95%" CssClass="validation"></asp:ValidationSummary>
                                </ajax:AjaxPanel>
                                 <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">
                                    <asp:MultiView ID="MultiViewNasabahTypeAll" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="ViewSwiftOut" runat="server">
                                            <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                                style="border-top-style: none; border-right-style: none; border-left-style: none;
                                                border-bottom-style: none" width="100%">
                                                <tr>
                                                    <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                        border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                        <img src="Images/dot_title.gif" width="17" height="17" />
                                                        <strong>
                                                            <asp:Label ID="Labels1" runat="server" Text="IFTI Swift Outgoing - Detail "></asp:Label>
                                                        </strong>
                                                        <hr />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                                                        border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                        <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                                                            <asp:Label ID="LblSucces" CssClass="validationok" Width="94%" runat="server" Visible="False"></asp:Label>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                bgcolor="#dddddd" border="2">
                                                <tr>
                                                    <td background="Images/search-bar-background.gif" valign="middle" align="left" style="height: 6px;
                                                        width: 100%;">
                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td class="formtext">
                                                                    <asp:Label ID="Labela6" runat="server" Text="A. Umum" Font-Bold="True"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <a href="#" onclick="javascript:ShowHidePanel('Umum','searchimage4','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                        title="click to minimize or maximize">
                                                                        <img id="searchimage4" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                            width="12px" /></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr id="Umum">
                                                    <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;">
                                                        <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                            <tr>
                                                                <td style="width: 257px; background-color: #fff7e6; height: 22px;">
                                                                    <asp:Label ID="Labele16" runat="server" Text="a. No. LTDLN "></asp:Label></td>
                                                                <td style="height: 22px">
                                                                    <asp:TextBox ID="TxtUmum_LTDLN" runat="server" CssClass="textbox" MaxLength="30"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 257px; background-color: #fff7e6">
                                                                    <asp:Label ID="Labelj15" runat="server" Text="b. No. LTDLN Koreksi " Width="132px"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="TxtUmum_LTDLNKoreksi" runat="server" CssClass="textbox" MaxLength="30"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 257px; background-color: #fff7e6">
                                                                    <asp:Label ID="Labelas17" runat="server" Text="c. Tanggal Laporan "></asp:Label><asp:Label
                                                                        ID="Labelj23" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="TxtUmum_TanggalLaporan" runat="server" CssClass="textbox" TabIndex="2"
                                                                        Width="122px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 257px; background-color: #fff7e6">
                                                                    <asp:Label ID="Labele18" runat="server" Text="d. Nama PJK Bank Pelapor " Width="162px"></asp:Label>
                                                                    <asp:Label ID="Label260" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="TxtUmum_PJKBankPelapor" runat="server" CssClass="textbox" MaxLength="100"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 257px; background-color: #fff7e6; height: 22px;">
                                                                    <asp:Label ID="Labele19" runat="server" Text="e. Nama Pejabat PJK Bank Pelapor" Width="204px"></asp:Label>
                                                                    <asp:Label ID="Label261" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                <td style="height: 22px">
                                                                    <asp:TextBox ID="TxtUmum_NamaPejabatPJKBankPelapor" runat="server" CssClass="textbox"
                                                                        MaxLength="100" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 257px; background-color: #fff7e6">
                                                                    <asp:Label ID="Labele20" runat="server" Text="f. Jenis Laporan"></asp:Label>
                                                                    <asp:Label ID="Label262" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                <td>
                                                                    <asp:RadioButtonList ID="RbUmum_JenisLaporan" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" Enabled="False">
                                                                        <asp:ListItem Value="1">Baru</asp:ListItem>
                                                                        <asp:ListItem Value="2">Recall</asp:ListItem>
                                                                        <asp:ListItem Value="3">Koreksi</asp:ListItem>
                                                                        <asp:ListItem Value="4">Reject</asp:ListItem>
                                                                    </asp:RadioButtonList></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 257px">
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <table id="IdentitasPengirimNasabah" align="center" bordercolor="#ffffff" cellspacing="1"
                                                            cellpadding="2" width="99%" bgcolor="#dddddd" border="2">
                                                            <tr>
                                                                <td background="Images/search-bar-background.gif" valign="middle" align="left" style="height: 11px;
                                                                    width: 100%;">
                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td class="formtext">
                                                                                <asp:Label ID="Labelb2" runat="server" Text="B.  Identitas Pengirim" Font-Bold="True"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <a href="#" onclick="javascript:ShowHidePanel('B1identitasPENGIRIM','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                    title="click to minimize or maximize">
                                                                                    <img id="Img1" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="B1identitasPENGIRIM">
                                                                <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;">
                                                                    <table style="width: 100%">
                                                                        <tr>
                                                                            <td>
                                                                            </td>
                                                                            <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                                                border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                                                &nbsp;<asp:Label ID="Label36" runat="server" Text="Tipe PJK Bank" Width="96px"></asp:Label>
                                                                                <asp:Label ID="Label9270" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                                                border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                                                <ajax:AjaxPanel ID="AjaxPanel777" runat="server" Width="100%">
                                                                                    <asp:DropDownList ID="CboSwiftOutPengirim" runat="server" AutoPostBack="True" Enabled="False">
                                                                                        <asp:ListItem Value="1">Penyelenggara Pengirim Asal</asp:ListItem>
                                                                                        <asp:ListItem Value="2">Penyelenggara Penerus</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </ajax:AjaxPanel>
                                                                            </td>
                                                                            <td style="width: 25%">
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trTipePengirim" runat="server" visible="false">
                                                                            <td style="width: 25%">
                                                                            </td>
                                                                            <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                                                border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                                                <asp:Label ID="Label" runat="server" Text="       Tipe Pengirim "></asp:Label>
                                                                                <asp:Label ID="Label9271" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                                                border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                                                <ajax:AjaxPanel ID="AjaxPanel" runat="server">
                                                                                    <asp:RadioButtonList ID="Rb_IdenPengirimNas_TipePengirim" runat="server" RepeatDirection="Horizontal"
                                                                                        AutoPostBack="True" Width="189px" Enabled="False">
                                                                                        <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                                        <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </ajax:AjaxPanel>
                                                                            </td>
                                                                            <td style="width: 25%">
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trTipeNasabah" runat="server" visible="false">
                                                                            <td style="width: 25%">
                                                                            </td>
                                                                            <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                                                border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                                                <asp:Label ID="Label284" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                                                <asp:Label ID="Label9272" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                                                border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                                                <ajax:AjaxPanel ID="AjaxPanels3" runat="server">
                                                                                    <asp:RadioButtonList ID="Rb_IdenPengirimNas_TipeNasabah" runat="server" RepeatDirection="Horizontal"
                                                                                        AutoPostBack="True" Enabled="False">
                                                                                        <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                                        <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </ajax:AjaxPanel>
                                                                            </td>
                                                                            <td style="width: 25%">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <ajax:AjaxPanel ID="ajxPnlIdenTerlapor" runat="server" Width="100%">
                                                                        <asp:MultiView ID="MultiViewSwiftOUtIdentitasPengirim" runat="server">
                                                                            <asp:View ID="ViewSwiftOutPengirimAsal" runat="server">
                                                                                <asp:MultiView ID="MultiViewSwiftOutJenisNasabah" runat="server">
                                                                                    <asp:View ID="ViewSwiftOutPengirimNasabah" runat="server">
                                                                                        <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                                                            bgcolor="#dddddd" border="2" id="TABLESwiftOutNasabah" >
                                                                                            <tr>
                                                                                                <td>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                                                    style="height: 6px">
                                                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                                                        <tr>
                                                                                                            <td class="formtext" style="height: 14px">
                                                                                                                <asp:Label ID="Labeln3" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="height: 14px">
                                                                                                                <a href="#" onclick="javascript:ShowHidePanel('B1identitasPENGIRIM1nasabah','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                                                    title="click to minimize or maximize">
                                                                                                                    <img id="Img2" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="B1identitasPENGIRIM1nasabah">
                                                                                                <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                                                    <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                                        border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                                        <tr>
                                                                                                            <td style="width: 100%">
                                                                                                                <table>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 59%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label24" runat="server" Text="No. Rek " Width="84px"></asp:Label>
                                                                                                                            <asp:Label ID="Label26" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                        <td style="width: 124px">
                                                                                                                            <asp:TextBox ID="TxtIdenPengirimNas_account" runat="server" CssClass="textbox" MaxLength="50"
                                                                                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 53px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 610px">
                                                                                                                <asp:MultiView ID="MultiViewPengirimNasabah" runat="server" ActiveViewIndex="0">
                                                                                                                    <asp:View ID="Vw_PNG_Ind" runat="server">
                                                                                                                        <ajax:AjaxPanel ID="AjaxPanel113" runat="server">
                                                                                                                            <table style="width: 98%; height: 49px">
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; height: 15px; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Label30" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td style="height: 15px; width: 222px;">
                                                                                                                                    </td>
                                                                                                                                    <td style="height: 15px; width: 35px;">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px; height: 15px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Label29" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                                                        <asp:Label ID="Label263" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                    <td style="width: 222px">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_NamaLengkap" runat="server" CssClass="textbox"
                                                                                                                                            MaxLength="255" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                    <td style="width: 35px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Label32" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label>
                                                                                                                                        <asp:Label ID="Label264" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                    <td style="width: 222px">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_tanggalLahir" runat="server" CssClass="textbox"
                                                                                                                                            TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                                                                        </td>
                                                                                                                                    <td style="width: 35px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Label33" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                                                            Width="223px"></asp:Label></td>
                                                                                                                                    <td style="width: 222px">
                                                                                                                                        <asp:RadioButtonList ID="Rb_IdenPengirimNas_Ind_kewarganegaraan" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" Enabled="False">
                                                                                                                                            <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                                                            <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                                                        </asp:RadioButtonList></td>
                                                                                                                                    <td style="width: 35px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr id="PengirimIndNegara" runat="server" visible="false">
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Label34" runat="server" Text=" Negara "></asp:Label>
                                                                                                                                        <br />
                                                                                                                                        <asp:Label ID="Label40" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                                                            Width="258px"></asp:Label></td>
                                                                                                                                    <td style="width: 222px">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_negara" runat="server" CssClass="textbox"
                                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                        <asp:HiddenField ID="hfIdenPengirimNas_Ind_negara" runat="server" />
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 35px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr id="PengirimIndNegaraLain" runat="server" visible="false">
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Label47" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                                                    <td style="width: 222px">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_negaralain" runat="server" CssClass="textbox"
                                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                    <td style="width: 35px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Label35" runat="server" Text="d. Pekerjaan"></asp:Label>
                                                                                                                                        <asp:Label ID="Label266" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                    <td style="width: 222px">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_pekerjaan" runat="server" CssClass="textbox"
                                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                        <asp:HiddenField ID="hfIdenPengirimNas_Ind_pekerjaan" runat="server" />
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 35px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Labelu45" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                                                                                    <td style="width: 222px">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_Pekerjaanlain" runat="server" CssClass="textbox"
                                                                                                                                            MaxLength="100" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                    <td style="width: 35px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Labely37" runat="server" Text="e. Alamat Domisili"></asp:Label></td>
                                                                                                                                    <td style="width: 222px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 35px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6; height: 20px;">
                                                                                                                                        <asp:Label ID="Labely38" runat="server" Text="Alamat"></asp:Label>
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 222px; height: 20px;">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_Alamat" runat="server" CssClass="textbox"
                                                                                                                                            MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                                                    <td style="width: 35px; height: 20px;">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px; height: 20px;">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Labelo39" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                                                                                    <td style="height: 18px; width: 222px;">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_Kota" runat="server" CssClass="textbox" MaxLength="50"
                                                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                        <asp:HiddenField ID="hfIdenPengirimNas_Ind_Kota_dom" runat="server" />
                                                                                                                                    </td>
                                                                                                                                    <td style="height: 18px; width: 35px;">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px; height: 18px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Labelk57" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                                                    <td style="width: 222px; height: 18px">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_kotalain" runat="server" CssClass="textbox"
                                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                    <td style="width: 35px; height: 18px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px; height: 18px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Labelj41" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                                                                    <td style="width: 222px">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_provinsi" runat="server" CssClass="textbox"
                                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                        <asp:HiddenField ID="hfIdenPengirimNas_Ind_provinsi_dom" runat="server" />
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        &nbsp;</td>
                                                                                                                                    <td style="width: 47px">
                                                                                                                                        &nbsp;</td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Labeli186" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                                                    <td style="width: 222px">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_provinsiLain" runat="server" CssClass="textbox"
                                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                    <td style="width: 35px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6; height: 22px;">
                                                                                                                                        <asp:Label ID="Labelu42" runat="server" Text="f. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                                                                    <td style="width: 222px; height: 22px;">
                                                                                                                                        <%-- <ajax:AjaxPanel ID="AjaxPanelCheckPengirimNas_Ind_alamat" runat="server">--%>
                                                                                                                                        &nbsp;<%--</ajax:AjaxPanel>--%></td>
                                                                                                                                    <td style="width: 35px; height: 22px;">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px; height: 22px;">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Labelj43" runat="server" Text="Alamat"></asp:Label>
                                                                                                                                        <asp:Label ID="Labelj267" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                    <td style="width: 222px">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_alamatIdentitas" runat="server" CssClass="textbox"
                                                                                                                                            MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                                                    <td style="width: 35px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Labelj44" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                                                                        <asp:Label ID="Labelj268" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                    <td style="width: 222px">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_kotaIdentitas" runat="server" CssClass="textbox"
                                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                        <asp:HiddenField ID="hfIdenPengirimNas_Ind_kotaIdentitas" runat="server" />
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 35px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Labeli83" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                                                    <td style="width: 222px; height: 18px">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_KotaLainIden" runat="server" CssClass="textbox"
                                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                    <td style="width: 35px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6; height: 18px;">
                                                                                                                                        <asp:Label ID="Labeol46" runat="server" Text="Provinsi"></asp:Label>
                                                                                                                                        <asp:Label ID="Labeli269" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                    <td style="height: 18px; width: 222px;">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_ProvinsiIden" runat="server" CssClass="textbox"
                                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                        <asp:HiddenField ID="hfIdenPengirimNas_Ind_ProvinsiIden" runat="server" />
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        &nbsp;</td>
                                                                                                                                    <td style="width: 47px; height: 18px">
                                                                                                                                        &nbsp;</td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Labeli62" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                                                    <td style="width: 222px; height: 18px">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_ProvinsilainIden" runat="server" CssClass="textbox"
                                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                    <td style="width: 35px; height: 18px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px; height: 18px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                        <asp:Label ID="Labeli48" runat="server" Text="h. Jenis Dokument Identitas"></asp:Label>
                                                                                                                                        <asp:Label ID="Labeli270" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                    <td style="width: 222px">
                                                                                                                                        <asp:DropDownList ID="cboPengirimNasInd_jenisidentitas" runat="server" CssClass="combobox" Enabled="False">
                                                                                                                                        </asp:DropDownList></td>
                                                                                                                                    <td style="width: 35px">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6; height: 18px;">
                                                                                                                                        <asp:Label ID="LabeliI49" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                                                                        <asp:Label ID="LabeliI271" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                    <td style="height: 18px; width: 222px;">
                                                                                                                                        <asp:TextBox ID="TxtIdenPengirimNas_Ind_noIdentitas" runat="server" CssClass="textbox"
                                                                                                                                            MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                    <td style="width: 35px; height: 18px;">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px; height: 18px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 50%; background-color: #fff7e6; height: 15px;">
                                                                                                                                    </td>
                                                                                                                                    <td style="height: 15px; width: 222px;">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 35px; height: 15px;">
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 47px; height: 15px">
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </ajax:AjaxPanel>
                                                                                                                    </asp:View>
                                                                                                                    <asp:View ID="Vw_PNG_Corp" runat="server">
                                                                                                                        <table style="width: 288px; height: 31px">
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; height: 15px; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="LabelIu51" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="height: 15px; color: #000000; width: 240px;">
                                                                                                                                </td>
                                                                                                                                <td style="height: 15px; width: 2977px; color: #000000;">
                                                                                                                                </td>
                                                                                                                                <td style="color: #000000; height: 15px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labeu9306" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 240px">
                                                                                                                                    <asp:DropDownList ID="CBOIdenPengirimNas_Corp_BentukBadanUsaha" runat="server" CssClass="combobox" Enabled="False">
                                                                                                                                    </asp:DropDownList>
                                                                                                                                </td>
                                                                                                                                <td style="width: 2977px">
                                                                                                                                    &nbsp;</td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;</td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelb9307" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="214px"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 240px">
                                                                                                                                    <asp:TextBox ID="TxtIdenPengirimNas_Corp_BentukBadanUsahaLain" runat="server" CssClass="textbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td style="width: 2977px">
                                                                                                                                    &nbsp;</td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;</td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Label5i5" runat="server" Text="b. Nama Korporasi" Width="112px"></asp:Label>
                                                                                                                                    <asp:Label ID="Labeli2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 240px">
                                                                                                                                    <asp:TextBox ID="TxtIdenPengirimNas_Corp_NamaKorp" runat="server" CssClass="textbox"
                                                                                                                                        MaxLength="100" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td style="width: 2977px">
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelr4" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label>
                                                                                                                                    <asp:Label ID="Labelo273" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 240px">
                                                                                                                                    <asp:TextBox ID="TxtIdenPengirimNas_Corp_BidangUsahaKorp" runat="server" CssClass="textbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                    <asp:HiddenField ID="hfIdenPengirimNas_Corp_BidangUsahaKorp" runat="server" />
                                                                                                                                </td>
                                                                                                                                <td style="width: 2977px">
                                                                                                                                    &nbsp;</td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;</td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelr234" runat="server" Text="Bidang Usaha Korporasi Lainnya"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 240px">
                                                                                                                                    <asp:TextBox ID="TxtIdenPengirimNas_Corp_BidangUsahaKorpLainnya" runat="server" CssClass="textbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td style="width: 2977px">
                                                                                                                                    &nbsp;</td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;</td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelk58" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                                                                <td style="width: 240px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 2977px">
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelk59" runat="server" Text="Alamat"></asp:Label>
                                                                                                                                    <asp:Label ID="Labelk274" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                <td style="width: 240px">
                                                                                                                                    <asp:TextBox ID="TxtIdenPengirimNas_Corp_alamatkorp" runat="server" CssClass="textbox"
                                                                                                                                        MaxLength="100" TabIndex="2" Width="342px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="width: 2977px">
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelk61" runat="server" Text="Kota / Kabupaten "></asp:Label>
                                                                                                                                    <asp:Label ID="Labelk275" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                <td style="width: 240px">
                                                                                                                                    <asp:TextBox ID="TxtIdenPengirimNas_Corp_kotakorp" runat="server" CssClass="textbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                    <asp:HiddenField ID="hfIdenPengirimNas_Corp_kotakorp" runat="server" />
                                                                                                                                </td>
                                                                                                                                <td style="width: 2977px">
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labeli69" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                                                <td style="width: 240px">
                                                                                                                                    <asp:TextBox ID="TxtIdenPengirimNas_Corp_kotakorplain" runat="server" CssClass="textbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="width: 2977px">
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelu63" runat="server" Text="Provinsi"></asp:Label>
                                                                                                                                    <asp:Label ID="Label2u76" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                <td style="width: 240px">
                                                                                                                                    <asp:TextBox ID="TxtIdenPengirimNas_Corp_provKorp" runat="server" CssClass="textbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                    <asp:HiddenField ID="hfIdenPengirimNas_Corp_provKorp" runat="server" />
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;</td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;</td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelu78" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                                                <td style="width: 222px">
                                                                                                                                    <asp:TextBox ID="TxtIdenPengirimNas_Corp_provKorpLain" runat="server" CssClass="textbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="width: 2977px; height: 15px">
                                                                                                                                </td>
                                                                                                                                <td style="height: 15px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelu64" runat="server" Text="e. Alamat Korporasi di Luar Negeri"></asp:Label></td>
                                                                                                                                <td style="width: 240px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 2977px">
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelu9258" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                                                <td style="width: 240px">
                                                                                                                                    <asp:TextBox ID="TxtIdenPengirimNas_Corp_AlamatLuar" runat="server" CssClass="textbox"
                                                                                                                                        MaxLength="100" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="width: 2977px">
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelu60" runat="server" Text="f. Alamat Cabang Pengirim Asal"></asp:Label></td>
                                                                                                                                <td style="width: 240px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 2977px">
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 60%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelu9259" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                                                <td style="width: 240px">
                                                                                                                                    <asp:TextBox ID="TxtIdenPengirimNas_Corp_AlamatAsal" runat="server" CssClass="textbox"
                                                                                                                                        MaxLength="100" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="width: 2977px">
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </asp:View>
                                                                                                                </asp:MultiView></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:View>
                                                                                    <asp:View ID="ViewSwiftOutPengirimNonNasabah" runat="server">
                                                                                        <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                                            bgcolor="#dddddd" border="2">
                                                                                            <tr>
                                                                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                                                    style="height: 7px">
                                                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                                                        <tr>
                                                                                                            <td class="formtext" style="height: 14px">
                                                                                                                <asp:Label ID="Labelb4" runat="server" Text="Non Nasabah (Pengiriman Tunai)" Font-Bold="True"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="height: 14px">
                                                                                                                <a href="#" onclick="javascript:ShowHidePanel('B1identitasNonSwiftOutPengirimNonNasabah','Img3','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                                                    title="click to minimize or maximize">
                                                                                                                    <img id="Img3" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr id="B1identitasNonSwiftOutPengirimNonNasabah">
                                                                                                <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                                                    <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                                        border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                                    </table>
                                                                                                    <table style="width: 100%; height: 49px">
                                                                                                        <tr>
                                                                                                            <td style="background-color: #fff7e6" colspan="2">
                                                                                                                <asp:RadioButtonList ID="RbPengirimNonNasabah_100Juta" runat="server" RepeatDirection="Horizontal" Enabled="False">
                                                                                                                    <asp:ListItem Value="1">&lt; 100 Juta (Kurs yang digunakan</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2">&gt; 100 Juta (Kurs yang digunakan</asp:ListItem>
                                                                                                                </asp:RadioButtonList>
                                                                                                                <asp:Label ID="Labely240" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 5%;">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 20%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labely81" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                                <asp:Label ID="Labely85" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <td style="width: 35%">
                                                                                                                <asp:TextBox ID="TxtPengirimNonNasabah_nama" runat="server" CssClass="textbox" MaxLength="100"
                                                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td style="width: 5%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 20%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labele82" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 35%">
                                                                                                                <asp:TextBox ID="TxtPengirimNonNasabah_TanggalLahir" runat="server" CssClass="textbox"
                                                                                                                    TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            <td style="width: 5%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 20%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labeil95" runat="server" Text="c. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                                            <td style="width: 35%">
                                                                                                            </td>
                                                                                                            <td style="width: 5%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 20%; background-color: #fff7e6; height: 20px;">
                                                                                                                <asp:Label ID="Labelr96" runat="server" Text="Alamat"></asp:Label>
                                                                                                                <asp:Label ID="Label87" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 35%; height: 20px;">
                                                                                                                <asp:TextBox ID="TxtPengirimNonNasabah_alamatiden" runat="server" CssClass="textbox"
                                                                                                                    MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td style="width: 5%; height: 20px;">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 20%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelr97" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 35%">
                                                                                                                <asp:TextBox ID="TxtPengirimNonNasabah_kotaIden" runat="server" CssClass="textbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                <asp:HiddenField ID="hfPengirimNonNasabah_kotaIden" runat="server" />
                                                                                                            </td>
                                                                                                            <td style="width: 5%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 20%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labeli88" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                            <td style="width: 35%">
                                                                                                                <asp:TextBox ID="TxtPengirimNonNasabah_KoaLainIden" runat="server" CssClass="textbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td style="width: 5%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 20%; background-color: #fff7e6;">
                                                                                                                <asp:Label ID="Labeli98" runat="server" Text="Provinsi"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="TxtPengirimNonNasabah_ProvIden" runat="server" CssClass="textbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                <asp:HiddenField ID="hfPengirimNonNasabah_ProvIden" runat="server" />
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                &nbsp;</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 20%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelo99" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                            <td style="width: 35%;">
                                                                                                                <asp:TextBox ID="TxtPengirimNonNasabah_ProvLainIden" runat="server" CssClass="textbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td style="width: 5%;">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 20%; background-color: #fff7e6; height: 24px;">
                                                                                                                <asp:Label ID="Label1o00" runat="server" Text="d. Jenis Dokument Identitas"></asp:Label>
                                                                                                                <asp:Label ID="jenisdokumen_nonNasabah_mand" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 35%; height: 24px;">
                                                                                                                <asp:DropDownList ID="CboPengirimNonNasabah_JenisDokumen" runat="server" CssClass="combobox" Enabled="False">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                            <td style="width: 5%; height: 24px;">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 20%; background-color: #fff7e6;">
                                                                                                                <asp:Label ID="Labelp101" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 35%;">
                                                                                                                <asp:TextBox ID="TxtPengirimNonNasabah_NomorIden" runat="server" CssClass="textbox"
                                                                                                                    MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td style="width: 5%;">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 20%; background-color: #fff7e6;">
                                                                                                            </td>
                                                                                                            <td style="width: 35%;">
                                                                                                            </td>
                                                                                                            <td style="width: 5%;">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:View>
                                                                                </asp:MultiView>
                                                                            </asp:View>
                                                                            <asp:View ID="ViewSwiftOutPengirimPenerus" runat="server">
                                                                                <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                                                    bgcolor="#dddddd" border="2" id="TablePengirimPenerus">
                                                                                    <tr>
                                                                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                                            style="height: 6px">
                                                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                                                <tr>
                                                                                                    <td class="formtext">
                                                                                                        &nbsp;</td>
                                                                                                    <td>
                                                                                                        <a href="#" onclick="javascript:ShowHidePanel('B2identitaspengirim','Img4','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                                            title="click to minimize or maximize">
                                                                                                            <img id="Img4" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr id="B2identitaspengirim">
                                                                                        <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                            </table>
                                                                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                                <tr>
                                                                                                    <td style="width: 100%">
                                                                                                        <table style="width: 725px; height: 60px">
                                                                                                            <tr>
                                                                                                                <td style="width: 33%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label70" runat="server" Text="No. Rek " Width="84px"></asp:Label>
                                                                                                                    <asp:Label ID="Label71" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                <td style="width: 124px">
                                                                                                                    <asp:TextBox ID="TxtPengirimPenerus_Rekening" runat="server" CssClass="textbox" MaxLength="50"
                                                                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                <td style="width: 53px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 610px">
                                                                                                        <asp:MultiView ID="MultiViewPengirimPenerus" runat="server" ActiveViewIndex="0">
                                                                                                            <asp:View ID="ViewPengirimPenerus_IND" runat="server">
                                                                                                                <table style="width: 98%; height: 49px">
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; height: 15px; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label91" runat="server" Text="Perorangan " Font-Bold="true"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="height: 15px; width: 222px;">
                                                                                                                        </td>
                                                                                                                        <td style="height: 15px; width: 36px;">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px; height: 15px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label9268" runat="server" Text="a. Nama Bank"></asp:Label>
                                                                                                                            <asp:Label ID="Label92" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                        <td style="width: 222px">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_Ind_namaBank" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 36px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label9267" runat="server" Text="b. Nama Lengkap"></asp:Label>
                                                                                                                            <asp:Label ID="Label9263" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="width: 222px">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_Ind_nama" runat="server" CssClass="textbox" MaxLength="100"
                                                                                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 36px">
                                                                                                                            &nbsp;</td>
                                                                                                                        <td style="width: 47px">
                                                                                                                            &nbsp;</td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6; height: 20px;">
                                                                                                                            <asp:Label ID="Label94" runat="server" Text="c. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label></td>
                                                                                                                        <td style="width: 222px; height: 20px;">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_Ind_TanggalLahir" runat="server" CssClass="textbox"
                                                                                                                                TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                        <td style="width: 36px; height: 20px;">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px; height: 20px;">
                                                                                                                            <asp:Label ID="Labelwe103" runat="server" Text="Label" Visible="False" Width="128px"></asp:Label></td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Labelwe104" runat="server" Text="d. Kewarganegaraan (Pilih salah satu) "
                                                                                                                                Width="223px"></asp:Label></td>
                                                                                                                        <td style="width: 222px">
                                                                                                                            <asp:RadioButtonList ID="RBPengirimPenerus_Ind_Kewarganegaraan" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" Enabled="False">
                                                                                                                                <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                                                <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                                            </asp:RadioButtonList></td>
                                                                                                                        <td style="width: 36px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="PengirimPenerusIndNegara" runat="server" visible="false">
                                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label105" runat="server" Text=" Negara "></asp:Label>
                                                                                                                            <br />
                                                                                                                            <asp:Label ID="Label106" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                                                Width="258px"></asp:Label></td>
                                                                                                                        <td style="width: 222px">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_IND_negara" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                            <asp:HiddenField ID="hfPengirimPenerus_IND_negara" runat="server" />
                                                                                                                        </td>
                                                                                                                        <td style="width: 36px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="PengirimPenerusIndNegaraLain" runat="server" visible="false">
                                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label107" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                                        <td style="width: 222px">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_IND_negaralain" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 36px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label108" runat="server" Text="e. Pekerjaan"></asp:Label></td>
                                                                                                                        <td style="width: 222px">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_IND_pekerjaan" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                            <asp:HiddenField ID="hfPengirimPenerus_IND_pekerjaan" runat="server" />
                                                                                                                        </td>
                                                                                                                        <td style="width: 36px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label109" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                                                                        <td style="width: 222px">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_IND_pekerjaanLain" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 36px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6; height: 15px;">
                                                                                                                            <asp:Label ID="Label110" runat="server" Text="f. Alamat Domisili"></asp:Label></td>
                                                                                                                        <td style="width: 222px; height: 15px;">
                                                                                                                        </td>
                                                                                                                        <td style="width: 36px; height: 15px;">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px; height: 15px;">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label111" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                                        <td style="width: 222px">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_IND_alamatDom" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 36px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; height: 18px; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label112" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                                                                        <td style="height: 18px; width: 222px;">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_IND_kotaDom" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                            <asp:HiddenField ID="hfPengirimPenerus_IND_kotaDom" runat="server" />
                                                                                                                        </td>
                                                                                                                        <td style="height: 18px; width: 36px;">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px; height: 18px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; height: 18px; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label113" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                                        <td style="width: 222px; height: 18px">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_IND_kotaLainDom" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 36px; height: 18px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px; height: 18px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label114" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                                                        <td style="width: 222px">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_IND_ProvDom" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                            <asp:HiddenField ID="hfPengirimPenerus_IND_ProvDom" runat="server" />
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            &nbsp;</td>
                                                                                                                        <td style="width: 47px">
                                                                                                                            &nbsp;</td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label175" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                                        <td style="width: 222px">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_IND_ProvLainDom" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 36px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6; height: 22px;">
                                                                                                                            <asp:Label ID="Label115" runat="server" Text="g. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                                                        <td style="width: 222px; height: 22px;">
                                                                                                                            </td>
                                                                                                                        <td style="width: 36px; height: 22px;">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px; height: 22px;">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label116" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                                        <td style="width: 222px">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_IND_alamatIden" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 36px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label117" runat="server" Text="Kota / Kabupaten"></asp:Label></td>
                                                                                                                        <td style="width: 222px">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_IND_kotaIden" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                            <asp:HiddenField ID="hfPengirimPenerus_IND_kotaIden" runat="server" />
                                                                                                                        </td>
                                                                                                                        <td style="width: 36px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; height: 18px; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label118" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                                        <td style="width: 222px; height: 18px">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_IND_KotaLainIden" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 36px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6; height: 18px;">
                                                                                                                            <asp:Label ID="Label119" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                                                        <td style="height: 18px; width: 222px;">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_IND_ProvIden" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                            <asp:HiddenField ID="hfPengirimPenerus_IND_ProvIden" runat="server" />
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            &nbsp;</td>
                                                                                                                        <td style="width: 47px; height: 18px">
                                                                                                                            &nbsp;</td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; height: 18px; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label120" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                                        <td style="width: 222px; height: 18px">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_IND_ProvLainIden" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 36px; height: 18px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px; height: 18px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label121" runat="server" Text="h. Jenis Dokument Identitas"></asp:Label></td>
                                                                                                                        <td style="width: 222px">
                                                                                                                            <asp:DropDownList ID="CBoPengirimPenerus_IND_jenisIdentitas" runat="server" CssClass="combobox" Enabled="False">
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                        <td style="width: 36px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6; height: 18px;">
                                                                                                                            <asp:Label ID="Label122" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                                                                        <td style="height: 18px; width: 222px;">
                                                                                                                            <asp:TextBox ID="TxtPengirimPenerus_IND_nomoridentitas" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 36px; height: 18px;">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px; height: 18px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                                            &nbsp;</td>
                                                                                                                        <td style="width: 222px">
                                                                                                                            &nbsp;</td>
                                                                                                                        <td style="width: 36px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 48%; background-color: #fff7e6; height: 15px;">
                                                                                                                        </td>
                                                                                                                        <td style="height: 15px; width: 222px;">
                                                                                                                        </td>
                                                                                                                        <td style="width: 36px; height: 15px;">
                                                                                                                        </td>
                                                                                                                        <td style="width: 47px; height: 15px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </asp:View>
                                                                                                            <asp:View ID="ViewPengirimPenerus_Korp" runat="server">
                                                                                                                <table style="width: 288px; height: 31px">
                                                                                                                    <tr>
                                                                                                                        <td style="width: 60%; height: 15px; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label124" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="height: 15px; color: #000000; width: 240px;">
                                                                                                                        </td>
                                                                                                                        <td style="width: 240px; color: #000000; height: 15px">
                                                                                                                        </td>
                                                                                                                        <td style="height: 15px; width: 2977px; color: #000000;">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr style="color: #000000">
                                                                                                                        <td style="width: 60%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label9264" runat="server" Text="a. Nama Bank"></asp:Label>
                                                                                                                            <asp:Label ID="Label9265" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="width: 240px">
                                                                                                                            <asp:TextBox ID="txtPengirimPenerus_Korp_namabank" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 240px">
                                                                                                                            &nbsp;</td>
                                                                                                                        <td style="width: 2977px">
                                                                                                                            &nbsp;</td>
                                                                                                                    </tr>
                                                                                                                    <tr style="color: #000000">
                                                                                                                        <td style="width: 60%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label126" runat="server" Text="b. Bentuk Badan Usaha "></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="width: 240px">
                                                                                                                            <asp:DropDownList ID="CBOPengirimPenerus_Korp_BentukBadanUsaha" runat="server" CssClass="combobox" Enabled="False">
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                        <td style="width: 240px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 2977px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 60%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label127" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="257px"></asp:Label></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                            <asp:TextBox ID="txtPengirimPenerus_Korp_BentukBadanUsahaLain" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 2977px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 60%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label128" runat="server" Text="c. Nama Korporasi" Width="119px"></asp:Label>
                                                                                                                            <asp:Label ID="Label9266" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="width: 240px">
                                                                                                                            <asp:TextBox ID="txtPengirimPenerus_Korp_NamaKorp" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="100" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 2977px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 60%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label129" runat="server" Text="d. Bidang Usaha Korporasi"></asp:Label></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                            <asp:TextBox ID="txtPengirimPenerus_Korp_BidangUsahaKorp" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                            <asp:HiddenField ID="hfPengirimPenerus_Korp_BidangUsahaKorp" runat="server" />
                                                                                                                        </td>
                                                                                                                        <td style="width: 240px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 2977px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 60%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label9269" runat="server" Text="Bidang Usaha Korporasi Lainnya"></asp:Label>
                                                                                                                        </td>
                                                                                                                        <td style="width: 240px">
                                                                                                                            <asp:TextBox ID="txtPengirimPenerus_Korp_BidangUsahaKorplain" runat="server" CssClass="textbox" MaxLength="50" TabIndex="2"
                                                                                                                                Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 240px">
                                                                                                                            &nbsp;</td>
                                                                                                                        <td style="width: 2977px">
                                                                                                                            &nbsp;</td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 60%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label130" runat="server" Text="e. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 240px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 2977px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 60%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label131" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                            <asp:TextBox ID="txtPengirimPenerus_Korp_alamatkorp" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="100" TabIndex="2" Width="342px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 2977px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 60%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label132" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                            <asp:TextBox ID="txtPengirimPenerus_Korp_kotakorp" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                            <asp:HiddenField ID="hfPengirimPenerus_Korp_kotakorp" runat="server" />
                                                                                                                        </td>
                                                                                                                        <td style="width: 240px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 2977px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 60%; background-color: #fff7e6; height: 20px;">
                                                                                                                            <asp:Label ID="Label133" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                                        <td style="width: 240px; height: 20px;">
                                                                                                                            <asp:TextBox ID="txtPengirimPenerus_Korp_kotalainnyakorp" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 240px; height: 20px;">
                                                                                                                        </td>
                                                                                                                        <td style="width: 2977px; height: 20px;">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 60%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label134" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                            <asp:TextBox ID="txtPengirimPenerus_Korp_ProvinsiKorp" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                            <asp:HiddenField ID="hfPengirimPenerus_Korp_ProvinsiKorp" runat="server" />
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            &nbsp;</td>
                                                                                                                        <td style="width: 2977px">
                                                                                                                            &nbsp;</td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 60%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label135" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                            <asp:TextBox ID="txtPengirimPenerus_Korp_provinsiLainnyaKorp" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 2977px; height: 15px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 60%; background-color: #fff7e6; height: 17px;">
                                                                                                                            <asp:Label ID="Label136" runat="server" Text="f. Alamat Korporasi di Luar Negeri"></asp:Label></td>
                                                                                                                        <td style="width: 240px; height: 17px;">
                                                                                                                        </td>
                                                                                                                        <td style="width: 240px; height: 17px;">
                                                                                                                        </td>
                                                                                                                        <td style="width: 2977px; height: 17px;">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 60%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label9260" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                            <asp:TextBox ID="txtPengirimPenerus_Korp_AlamatLuar" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="100" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 2977px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 60%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label141" runat="server" Text="g. Alamat Cabang Pengirim Asal"></asp:Label></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 240px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 2977px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 60%; background-color: #fff7e6">
                                                                                                                            <asp:Label ID="Label9261" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                            <asp:TextBox ID="txtPengirimPenerus_Korp_AlamatAsal" runat="server" CssClass="textbox"
                                                                                                                                MaxLength="100" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td style="width: 240px">
                                                                                                                        </td>
                                                                                                                        <td style="width: 2977px">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </asp:View>
                                                                                                        </asp:MultiView>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:View>
                                                                        </asp:MultiView>
                                                                    </ajax:AjaxPanel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                bgcolor="#dddddd" border="2">
                                                <tr>
                                                    <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                        style="height: 6px">
                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td class="formtext">
                                                                    <asp:Label ID="Labelr7" runat="server" Text="C. Beneficiary Owner " Font-Bold="True"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <a href="#" onclick="javascript:ShowHidePanel('CBeneficiaryOwner','Img5','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                        title="click to minimize or maximize">
                                                                        <img id="Img5" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr id="CBeneficiaryOwner">
                                                    <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                        <table style="width: 725px; height: 60px">
                                                            <tr>
                                                                <td colspan="" style="width: 33%; background-color: #fff7e6; height: 24px;">
                                                                    <asp:Label ID="Label3" runat="server" Text="Apakah transaksi melibatkan Beneficial Owner?"
                                                                        Width="298px"></asp:Label>
                                                                    <asp:Label ID="Label22" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                <td colspan="" style="width: 124px; height: 24px;">
                                                                    <asp:RadioButtonList ID="Rb_BOwnerNas_ApaMelibatkan" runat="server" RepeatDirection="Horizontal"
                                                                            AutoPostBack="True" Width="189px" Enabled="False">
                                                                        <asp:ListItem Value="1">Ya</asp:ListItem>
                                                                        <asp:ListItem Value="2">Tidak</asp:ListItem>
                                                                    </asp:RadioButtonList></td>
                                                            </tr>
                                                            <tr id="trBenfOwnerHubungan" runat="server" visible="false">
                                                                <td style="width: 33%; background-color: #fff7e6">
                                                                    <asp:Label ID="Labelt162" runat="server" Text="Hubungan dengan Pemilik Dana (Beneficial Owner)"
                                                                        Width="323px"></asp:Label>
                                                                    <asp:Label ID="Label23" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                </td>
                                                                <td style="width: 124px">
                                                                    <asp:TextBox ID="BenfOwnerHubunganPemilikDana" runat="server" CssClass="textbox"
                                                                        MaxLength="50" TabIndex="2" Width="343px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr id="trBenfOwnerTipePengirim" runat="server" visible="false">
                                                                <td style="width: 33%; background-color: #fff7e6; height: 82px;">
                                                                    <asp:Label ID="Label9t255" runat="server" Text="       Tipe Pengirim " Width="87px"></asp:Label>
                                                                    <asp:Label ID="Label25" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                <td style="width: 124px; height: 82px;">
                                                                    <ajax:AjaxPanel ID="AjaxPanely6" runat="server">
                                                                        <asp:RadioButtonList ID="Rb_BOwnerNas_TipePengirim" runat="server" RepeatDirection="Horizontal"
                                                                            AutoPostBack="True" Width="189px" Enabled="False">
                                                                            <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                            <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </ajax:AjaxPanel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:MultiView ID="MultiViewSwiftOutBOwner" runat="server">
                                                            <asp:View ID="ViewSwiftOutBOwnerNasabah" runat="server">
                                                                <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                                    bgcolor="#dddddd" border="2" id="TableSwiftOutBOwnerNas">
                                                                    <tr>
                                                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                            style="height: 6px">
                                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        <asp:Label ID="Label8" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <a href="#" onclick="javascript:ShowHidePanel('BOwner1nasabah','Img6','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                            title="click to minimize or maximize">
                                                                                            <img id="Img6" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="BOwner1nasabah">
                                                                        <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                            </table>
                                                                            <table style="width: 98%; height: 49px">
                                                                                <tr>
                                                                                    <td style="width: 25%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelf1163" runat="server" Text="a. No. Rek "></asp:Label>
                                                                                        <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                    <td style="width: 204px">
                                                                                        <asp:TextBox ID="BenfOwnerNasabah_rekening" runat="server" CssClass="textbox" MaxLength="50"
                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 35px">
                                                                                    </td>
                                                                                    <td style="width: 47px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 25%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labele146" runat="server" Text="b. Nama Lengkap"></asp:Label>
                                                                                        <asp:Label ID="Label6" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                    <td style="width: 204px">
                                                                                        <asp:TextBox ID="BenfOwnerNasabah_Nama" runat="server" CssClass="textbox" MaxLength="100"
                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 35px">
                                                                                    </td>
                                                                                    <td style="width: 47px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 25%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Label148" runat="server" Text="c. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label>
                                                                                        <asp:Label ID="Label15" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                    <td style="width: 204px">
                                                                                        <asp:TextBox ID="BenfOwnerNasabah_tanggalLahir" runat="server" CssClass="textbox"
                                                                                            TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                        </td>
                                                                                    <td style="width: 35px">
                                                                                    </td>
                                                                                    <td style="width: 47px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 25%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labeld150" runat="server" Text="d. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                    <td style="width: 204px">
                                                                                    </td>
                                                                                    <td style="width: 35px">
                                                                                    </td>
                                                                                    <td style="width: 47px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 25%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelw151" runat="server" Text="Alamat"></asp:Label>
                                                                                        <asp:Label ID="Label7" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                    <td style="width: 204px">
                                                                                        <asp:TextBox ID="BenfOwnerNasabah_AlamatIden" runat="server" CssClass="textbox" MaxLength="100"
                                                                                            TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 35px">
                                                                                    </td>
                                                                                    <td style="width: 47px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 25%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelt152" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                        <asp:Label ID="Label10" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                    <td style="width: 204px">
                                                                                        <asp:TextBox ID="BenfOwnerNasabah_KotaIden" runat="server" CssClass="textbox" MaxLength="50"
                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                        <asp:HiddenField ID="hfBenfOwnerNasabah_KotaIden" runat="server" />
                                                                                    </td>
                                                                                    <td style="width: 35px">
                                                                                    </td>
                                                                                    <td style="width: 47px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 25%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelt154" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                    <td style="width: 204px">
                                                                                        <asp:TextBox ID="BenfOwnerNasabah_kotaLainIden" runat="server" CssClass="textbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 35px">
                                                                                    </td>
                                                                                    <td style="width: 47px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 25%; background-color: #fff7e6; height: 18px;">
                                                                                        <asp:Label ID="Labelt155" runat="server" Text="Provinsi"></asp:Label>
                                                                                        <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                    <td style="height: 18px; width: 204px;">
                                                                                        <asp:TextBox ID="BenfOwnerNasabah_ProvinsiIden" runat="server" CssClass="textbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                        <asp:HiddenField ID="hfBenfOwnerNasabah_ProvinsiIden" runat="server" />
                                                                                    </td>
                                                                                    <td>
                                                                                        &nbsp;</td>
                                                                                    <td style="width: 47px; height: 18px">
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 25%; height: 18px; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelt156" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                    <td style="width: 204px; height: 18px">
                                                                                        <asp:TextBox ID="BenfOwnerNasabah_ProvinsiLainIden" runat="server" CssClass="textbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 35px; height: 18px">
                                                                                    </td>
                                                                                    <td style="width: 47px; height: 18px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 25%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelt158" runat="server" Text="e. Jenis Dokument Identitas"></asp:Label>
                                                                                        <asp:Label ID="Label14" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                    <td style="width: 204px">
                                                                                        <asp:DropDownList ID="CBOBenfOwnerNasabah_JenisDokumen" runat="server" CssClass="combobox" Enabled="False">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                    <td style="width: 35px">
                                                                                    </td>
                                                                                    <td style="width: 47px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 25%; background-color: #fff7e6; height: 18px;">
                                                                                        <asp:Label ID="Labelt160" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                                    <td style="height: 18px; width: 204px;">
                                                                                        <asp:TextBox ID="BenfOwnerNasabah_NomorIden" runat="server" CssClass="textbox" MaxLength="30"
                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 35px; height: 18px;">
                                                                                    </td>
                                                                                    <td style="width: 47px; height: 18px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 25%; background-color: #fff7e6; height: 15px;">
                                                                                    </td>
                                                                                    <td style="height: 15px; width: 204px;">
                                                                                    </td>
                                                                                    <td style="width: 35px; height: 15px;">
                                                                                    </td>
                                                                                    <td style="width: 47px; height: 15px">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:View>
                                                            <asp:View ID="ViewSwiftOutBOwnerNonNasabah" runat="server">
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none"
                                                                    id="TableSwiftOutBOwnerNonNas">
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                                                bgcolor="#dddddd" border="2">
                                                                                <tr>
                                                                                    <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                                        style="height: 6px">
                                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                                            <tr>
                                                                                                <td class="formtext" style="height: 14px">
                                                                                                    <asp:Label ID="Labelt9" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                                                                </td>
                                                                                                <td style="height: 14px">
                                                                                                    <a href="#" onclick="javascript:ShowHidePanel('BOwnerNonNasabah','Img7','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                                        title="click to minimize or maximize">
                                                                                                        <img id="Img7" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="BOwnerNonNasabah">
                                                                                    <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                                        <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                        </table>
                                                                                        <table style="width: 98%; height: 49px">
                                                                                            <tr>
                                                                                                <td style="width: 25%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Labelu157" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                    <asp:Label ID="Label16" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                <td style="width: 222px">
                                                                                                    <asp:TextBox ID="BenfOwnerNonNasabah_Nama" runat="server" CssClass="textbox" MaxLength="100"
                                                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                <td style="width: 35px">
                                                                                                </td>
                                                                                                <td style="width: 47px">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 25%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label1t66" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 222px">
                                                                                                    <asp:TextBox ID="BenfOwnerNonNasabah_TanggalLahir" runat="server" CssClass="textbox"
                                                                                                        TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                <td style="width: 35px">
                                                                                                </td>
                                                                                                <td style="width: 47px">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 25%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Labelt168" runat="server" Text="c. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                                <td style="width: 222px">
                                                                                                </td>
                                                                                                <td style="width: 35px">
                                                                                                </td>
                                                                                                <td style="width: 47px">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 25%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Labele169" runat="server" Text="Alamat"></asp:Label>
                                                                                                    <asp:Label ID="Label17" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                <td style="width: 222px">
                                                                                                    <asp:TextBox ID="BenfOwnerNonNasabah_AlamatIden" runat="server" CssClass="textbox"
                                                                                                        MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                <td style="width: 35px">
                                                                                                </td>
                                                                                                <td style="width: 47px">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 25%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label1t70" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                                    <asp:Label ID="Label18" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                <td style="width: 222px">
                                                                                                    <asp:TextBox ID="BenfOwnerNonNasabah_KotaIden" runat="server" CssClass="textbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                    <asp:HiddenField ID="hfBenfOwnerNonNasabah_KotaIden" runat="server" />
                                                                                                </td>
                                                                                                <td style="width: 35px">
                                                                                                </td>
                                                                                                <td style="width: 47px">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 25%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Labelo172" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                <td style="width: 222px">
                                                                                                    <asp:TextBox ID="BenfOwnerNonNasabah_KotaLainIden" runat="server" CssClass="textbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                <td style="width: 35px">
                                                                                                </td>
                                                                                                <td style="width: 47px">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 25%; background-color: #fff7e6; height: 18px;">
                                                                                                    <asp:Label ID="Labelo173" runat="server" Text="Provinsi"></asp:Label>
                                                                                                    <asp:Label ID="Label20" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                <td style="height: 18px; width: 222px;">
                                                                                                    <asp:TextBox ID="BenfOwnerNonNasabah_ProvinsiIden" runat="server" CssClass="textbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                    <asp:HiddenField ID="hfBenfOwnerNonNasabah_ProvinsiIden" runat="server" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    &nbsp;</td>
                                                                                                <td style="width: 47px; height: 18px">
                                                                                                    &nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 25%; height: 18px; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Labelo174" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                <td style="width: 222px; height: 18px">
                                                                                                    <asp:TextBox ID="BenfOwnerNonNasabah_ProvinsiLainIden" runat="server" CssClass="textbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                <td style="width: 35px; height: 18px">
                                                                                                </td>
                                                                                                <td style="width: 47px; height: 18px">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 25%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Labelo176" runat="server" Text="d. Jenis Dokument Identitas"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 222px">
                                                                                                    <asp:DropDownList ID="CBOBenfOwnerNonNasabah_JenisDokumen" runat="server" CssClass="combobox" Enabled="False">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                                <td style="width: 35px">
                                                                                                </td>
                                                                                                <td style="width: 47px">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 25%; background-color: #fff7e6; height: 18px;">
                                                                                                    <asp:Label ID="Labelo178" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                                                <td style="height: 18px; width: 222px;">
                                                                                                    <asp:TextBox ID="BenfOwnerNonNasabah_NomorIdentitas" runat="server" CssClass="textbox"
                                                                                                        MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                <td style="width: 35px; height: 18px;">
                                                                                                </td>
                                                                                                <td style="width: 47px; height: 18px">
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:View>
                                                        </asp:MultiView>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                bgcolor="#dddddd" border="2">
                                                <tr>
                                                    <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                        style="height: 6px">
                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td class="formtext">
                                                                    <asp:Label ID="Labelo10" runat="server" Text="D. Identitas Penerima" Font-Bold="True"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <a href="#" onclick="javascript:ShowHidePanel('DIdentitasPenerima','Img8','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                        title="click to minimize or maximize">
                                                                        <img id="Img8" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr id="DIdentitasPenerima">
                                                    <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                        <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                        </table>
                                                        <table bordercolor="#ffffff" cellspacing="1" cellpadding="0" width="100%" bgcolor="#dddddd"
                                                    border="2">
                                                    <tr>
                                                        <td bgcolor="#ffffff" background="images/testbg.gif">
                                                            <asp:DataGrid ID="GridDataView" runat="server" Font-Size="XX-Small" CellSpacing="1"
                                                                CellPadding="4" SkinID="gridview" AllowPaging="True" Width="100%" GridLines="None"
                                                                AllowSorting="True" ForeColor="#333333" Font-Bold="False" Font-Italic="False"
                                                                Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" CssClass="gridview">
                                                                <Columns>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="2%" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="PK_IFTI_Beneficiary_ID" Visible="False">
                                                                        <HeaderStyle Width="0%" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="FK_IFTI_ID" Visible="False" DataField="FK_IFTI_ID"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="FK_IFTI_NasabahType_ID" HeaderText="FK_IFTI_NasabahType_ID" Visible="False">
                                                                    </asp:BoundColumn>
                                                                    <asp:TemplateColumn HeaderText="No.">
                                                                        <HeaderStyle ForeColor="White" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_NoRekening" HeaderText="No Rekening (Perorangan)" SortExpression="Beneficiary_Nasabah_INDV_NoRekening desc">
                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" />
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_NamaBank" HeaderText="Nama Bank (Perorangan)" SortExpression="Beneficiary_Nasabah_INDV_NamaBank desc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_NamaLengkap" HeaderText="Nama Lengkap (Perorangan)" SortExpression="Beneficiary_Nasabah_INDV_NamaLengkap desc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_TanggalLahir" HeaderText="Tanggal Lahir (Perorangan)" SortExpression="Beneficiary_Nasabah_INDV_TanggalLahir desc" DataFormatString="{0:dd-MMM-yyyy}">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_NoRekening" HeaderText="No Rekening (Korporasi)" SortExpression="Beneficiary_Nasabah_CORP_NoRekening desc">
                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" />
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_NamaKorporasi" HeaderText="Nama Korporasi (Korporasi)" SortExpression="Beneficiary_Nasabah_CORP_NamaKorporasi desc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_AlamatLengkap" HeaderText="Alamat Lengkap (Korporasi)" SortExpression="Beneficiary_Nasabah_CORP_AlamatLengkap desc">
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" Wrap="False" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_NoTelp" HeaderText="No Telp (Korporasi)" SortExpression="Beneficiary_Nasabah_CORP_NoTelp desc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_NoRekening" HeaderText="No Rekening (Non Nasabah)" SortExpression="Beneficiary_NonNasabah_NoRekening desc">
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" Wrap="False" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_KodeRahasia" HeaderText="Kode Rahasia (Non Nasabah)" SortExpression="Beneficiary_NonNasabah_KodeRahasia desc">
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" Wrap="False" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_NamaBank" HeaderText="Nama Bank (Non Nasabah)" SortExpression="Beneficiary_NonNasabah_NamaBank desc">
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_NamaLengkap" HeaderText="Nama Lengkap (Non Nasabah)" SortExpression="Beneficiary_NonNasabah_NamaLengkap desc">
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_TanggalLahir" HeaderText="Tanggal Lahir (Non Nasabah)" SortExpression="Beneficiary_NonNasabah_TanggalLahir desc" DataFormatString="{0:dd-MMM-yyyy}">
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" Wrap="False" />
                                                                    </asp:BoundColumn>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="btnDetail" runat="server" CommandName="edit">Detail</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                                <PagerStyle Visible="False" HorizontalAlign="Center" ForeColor="White" BackColor="#666666">
                                                                </PagerStyle>
                                                                <EditItemStyle BackColor="#7C6F57" />
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                                        <table style="width: 725px; height: 60px" id="tableTipePenerima" runat="server" visible="true">
                                                            <tr>
                                                                <td style="width: 33%; background-color: #fff7e6; height: 20px;">
                                                                    <asp:Label ID="Labelo190" runat="server" Text="No. Rek " Width="84px"></asp:Label>
                                                                    <asp:Label ID="Labelo191" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                <td style="width: 124px; height: 20px;">
                                                                    <asp:TextBox ID="txtPenerima_rekening" runat="server" CssClass="textbox" MaxLength="50"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                <td style="width: 53px; height: 20px;">
                                                                </td>
                                                            </tr>
                                                            <tr id="trSwiftOutPenerimaTipePengirim" runat="server">
                                                                <td style="width: 33%; background-color: #fff7e6; height: 34px;">
                                                                    <asp:Label ID="Labelo192" runat="server" Text="       "></asp:Label>
                                                                    <asp:Label ID="Labelo193" runat="server" Text="       Tipe Penerima "></asp:Label>
                                                                    <asp:Label ID="Labelo194" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                </td>
                                                                <td style="width: 124px; height: 34px;">
                                                                    <ajax:AjaxPanel ID="AjaxPaneli5" runat="server" Width="93px">
                                                                        &nbsp;<asp:RadioButtonList ID="RBPenerimaNasabah_TipePengirim" runat="server" RepeatDirection="Horizontal"
                                                                            AutoPostBack="True" Width="201px" Enabled="False">
                                                                            <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                            <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                        </asp:RadioButtonList></ajax:AjaxPanel></td>
                                                                <td style="width: 53px; height: 34px;">
                                                                </td>
                                                            </tr>
                                                            <tr id="trSwiftOutPenerimaTipeNasabah" runat="server" visible="False">
                                                                <td style="width: 33%; height: 34px; background-color: #fff7e6">
                                                                    <asp:Label ID="Labelo9256" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                                    <asp:Label ID="Labelo9257" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                </td>
                                                                <td style="width: 124px; height: 34px">
                                                                    <ajax:AjaxPanel ID="AjaxPanelo9" runat="server">
                                                                        <asp:RadioButtonList ID="Rb_IdenPenerimaNas_TipeNasabah" runat="server" AutoPostBack="True"
                                                                            RepeatDirection="Horizontal" Enabled="False">
                                                                            <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                            <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </ajax:AjaxPanel>
                                                                </td>
                                                                <td style="width: 53px; height: 34px">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:MultiView ID="MultiViewSwiftOutIdenPenerima" runat="server">
                                                            <asp:View ID="ViewSwiftOutIdenPenerimaNasabah" runat="server">
                                                                <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                                    bgcolor="#dddddd" border="2">
                                                                    <tr>
                                                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                            style="height: 6px">
                                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        <asp:Label ID="Label11e" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <a href="#" onclick="javascript:ShowHidePanel('DIdentitasPenerima1nasabah','Img9','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                            title="click to minimize or maximize">
                                                                                            <img id="Img9" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="DIdentitasPenerima1nasabah">
                                                                        <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                            </table>
                                                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                <tr>
                                                                                    <td style="width: 100%">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 610px">
                                                                                        <asp:MultiView ID="MultiViewPenerimaNasabah" runat="server" ActiveViewIndex="0">
                                                                                            <asp:View ID="ViewPenerimaNasabah_IND" runat="server">
                                                                                                <table style="width: 98%; height: 49px">
                                                                                                    <tr>
                                                                                                        <td style="width: 48%; height: 15px; background-color: #fff7e6">
                                                                                                            <asp:Label ID="Label1o95" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="height: 15px; width: 222px;">
                                                                                                        </td>
                                                                                                        <td style="height: 15px; width: 36px;">
                                                                                                        </td>
                                                                                                        <td style="width: 47px; height: 15px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                            <asp:Label ID="Label19o7" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                            <asp:Label ID="Label12o5" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                        <td style="width: 222px">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_IND_nama" runat="server" CssClass="textbox" MaxLength="100"
                                                                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                        <td style="width: 36px">
                                                                                                        </td>
                                                                                                        <td style="width: 47px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                            <asp:Label ID="Label19o8" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label></td>
                                                                                                        <td style="width: 222px">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_IND_TanggalLahir" runat="server" CssClass="textbox"
                                                                                                                TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        <td style="width: 36px">
                                                                                                        </td>
                                                                                                        <td style="width: 47px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                            <asp:Label ID="Labelo200" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                                Width="223px"></asp:Label></td>
                                                                                                        <td style="width: 222px">
                                                                                                            <asp:RadioButtonList ID="RbPenerimaNasabah_IND_Warganegara" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" Enabled="False">
                                                                                                                <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                                <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                            </asp:RadioButtonList></td>
                                                                                                        <td style="width: 36px">
                                                                                                        </td>
                                                                                                        <td style="width: 47px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr id="PenerimaIndNegara" runat="server" visible="false">
                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                            <asp:Label ID="Label201" runat="server" Text="Negara "></asp:Label>
                                                                                                            <br />
                                                                                                            <asp:Label ID="Label202" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                                Width="258px"></asp:Label></td>
                                                                                                        <td style="width: 222px">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_IND_negara" runat="server" CssClass="textbox"
                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                            <asp:HiddenField ID="hfPenerimaNasabah_IND_negara" runat="server" />
                                                                                                        </td>
                                                                                                        <td style="width: 36px">
                                                                                                        </td>
                                                                                                        <td style="width: 47px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr id="PenerimaIndNegaraLain" runat="server" visible="false">
                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                            <asp:Label ID="Label203" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                        <td style="width: 222px">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_IND_negaraLain" runat="server" CssClass="textbox"
                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                        <td style="width: 36px">
                                                                                                        </td>
                                                                                                        <td style="width: 47px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 48%; background-color: #fff7e6; height: 22px;">
                                                                                                            <asp:Label ID="Label213" runat="server" Text="d. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                                        <td style="width: 222px; height: 22px;">
                                                                                                            &nbsp;</td>
                                                                                                        <td style="width: 36px; height: 22px;">
                                                                                                        </td>
                                                                                                        <td style="width: 47px; height: 22px;">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 48%; background-color: #fff7e6; height: 20px;">
                                                                                                            <asp:Label ID="Label214" runat="server" Text="Alamat"></asp:Label>
                                                                                                            <asp:Label ID="Label12o6" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="width: 222px; height: 20px;">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_IND_alamatIden" runat="server" CssClass="textbox"
                                                                                                                MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                        <td style="width: 36px; height: 20px;">
                                                                                                        </td>
                                                                                                        <td style="width: 47px; height: 20px;">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 48%; background-color: #fff7e6; height: 20px;">
                                                                                                            <asp:Label ID="Label205" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                                                        <td style="width: 222px; height: 20px;">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_IND_negaraBagian" runat="server" CssClass="textbox"
                                                                                                                MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td style="width: 36px; height: 20px;">
                                                                                                        </td>
                                                                                                        <td style="width: 47px; height: 20px;">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 48%; height: 18px; background-color: #fff7e6">
                                                                                                            <asp:Label ID="Label204" runat="server" Text="Negara"></asp:Label>
                                                                                                            <asp:Label ID="Label12o7" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="width: 222px; height: 18px">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_IND_negaraIden" runat="server" CssClass="textbox"
                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                            <asp:HiddenField ID="hfPenerimaNasabah_IND_negaraIden" runat="server" />
                                                                                                        </td>
                                                                                                        <td style="width: 36px">
                                                                                                        </td>
                                                                                                        <td style="width: 47px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 48%; background-color: #fff7e6; height: 18px;">
                                                                                                            <asp:Label ID="Label206" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                        <td style="height: 18px; width: 222px;">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_IND_negaraLainIden" runat="server" CssClass="textbox"
                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                        </td>
                                                                                                        <td style="width: 47px; height: 18px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                                                            <asp:Label ID="Label220" runat="server" Text="e. Jenis Dokument Identitas"></asp:Label></td>
                                                                                                        <td style="width: 222px">
                                                                                                            <asp:DropDownList ID="CBOPenerimaNasabah_IND_JenisIden" runat="server" CssClass="combobox" Enabled="False">
                                                                                                            </asp:DropDownList>
                                                                                                        </td>
                                                                                                        <td style="width: 36px">
                                                                                                        </td>
                                                                                                        <td style="width: 47px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 48%; background-color: #fff7e6; height: 18px;">
                                                                                                            <asp:Label ID="Label221" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                                                        <td style="height: 18px; width: 222px;">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_IND_NomorIden" runat="server" CssClass="textbox"
                                                                                                                MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                        <td style="width: 36px; height: 18px;">
                                                                                                        </td>
                                                                                                        <td style="width: 47px; height: 18px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 48%; background-color: #fff7e6; height: 18px;">
                                                                                                            <asp:Label ID="Labelew9270" runat="server" Text="f. Nilai Transaksi Keuangan (dalam Rupiah)"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="height: 18px; width: 222px;">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_IND_NilaiTransaksiKeuangan" runat="server" CssClass="textbox"
                                                                                                                MaxLength="15" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td style="width: 36px; height: 18px;">
                                                                                                            &nbsp;</td>
                                                                                                        <td style="width: 47px; height: 18px">
                                                                                                            &nbsp;</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 48%; background-color: #fff7e6; height: 15px;">
                                                                                                        </td>
                                                                                                        <td style="height: 15px; width: 222px;">
                                                                                                            &nbsp;
                                                                                                            <asp:ImageButton ID="ImageButton_CancelPenerimaInd" runat="server"  ImageUrl="~/Images/button/Ok.gif" /></td>
                                                                                                        <td style="width: 36px; height: 15px;">
                                                                                                        </td>
                                                                                                        <td style="width: 47px; height: 15px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </asp:View>
                                                                                            <asp:View ID="ViewPenerimaNasabah_Korp" runat="server">
                                                                                                <table style="width: 288px; height: 31px">
                                                                                                    <tr>
                                                                                                        <td style="width: 55%; background-color: #fff7e6;">
                                                                                                            <asp:Label ID="Labelko223" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="height: 15px; color: #000000; width: 240px;">
                                                                                                        </td>
                                                                                                        <td style="height: 15px; width: 2977px; color: #000000;">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr style="color: #000000">
                                                                                                        <td style="width: 55%; background-color: #fff7e6;">
                                                                                                            <asp:Label ID="Labela225" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label></td>
                                                                                                        <td style="width: 240px">
                                                                                                            <asp:DropDownList ID="CBOPenerimaNasabah_Korp_BentukBadanUsahaLain" runat="server"
                                                                                                                CssClass="combobox" Enabled="False">
                                                                                                            </asp:DropDownList>
                                                                                                        </td>
                                                                                                        <td style="width: 240px">
                                                                                                        </td>
                                                                                                        <td style="width: 2977px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 55%; background-color: #fff7e6;">
                                                                                                            <asp:Label ID="Labelb226" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="167px"></asp:Label></td>
                                                                                                        <td style="width: 240px">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya" runat="server"
                                                                                                                CssClass="textbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                        <td style="width: 240px">
                                                                                                        </td>
                                                                                                        <td style="width: 2977px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 55%; background-color: #fff7e6;">
                                                                                                            <asp:Label ID="Labelb227" runat="server" Text="b. Nama Korporasi" Width="110px"></asp:Label>
                                                                                                            <asp:Label ID="Labelb149" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                        <td style="width: 240px">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_Korp_namaKorp" runat="server" CssClass="textbox"
                                                                                                                MaxLength="100" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                        <td style="width: 240px">
                                                                                                        </td>
                                                                                                        <td style="width: 2977px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 55%; background-color: #fff7e6;">
                                                                                                            <asp:Label ID="Labelb228" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label></td>
                                                                                                        <td style="width: 240px">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_Korp_BidangUsahaKorp" runat="server" CssClass="textbox"
                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                            <asp:HiddenField ID="hfPenerimaNasabah_Korp_BidangUsahaKorp" runat="server" />
                                                                                                        </td>
                                                                                                        <td style="width: 240px">
                                                                                                        </td>
                                                                                                        <td style="width: 2977px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 55%; background-color: #fff7e6;">
                                                                                                            <asp:Label ID="Labelb9308" runat="server" Text="Bidang Usaha Korporasi Lainnya"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="width: 240px">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya" runat="server" CssClass="textbox"
                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td style="width: 240px">
                                                                                                            &nbsp;</td>
                                                                                                        <td style="width: 2977px">
                                                                                                            &nbsp;</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 55%; background-color: #fff7e6;">
                                                                                                            <asp:Label ID="Labelb229" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                                        <td style="width: 240px">
                                                                                                        </td>
                                                                                                        <td style="width: 240px">
                                                                                                        </td>
                                                                                                        <td style="width: 2977px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 55%; background-color: #fff7e6;">
                                                                                                            <asp:Label ID="Labelb199" runat="server" Text="Alamat"></asp:Label>
                                                                                                            <asp:Label ID="Labelb153" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                        <td style="width: 222px">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_Korp_AlamatKorp" runat="server" CssClass="textbox"
                                                                                                                MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                        <td style="width: 36px">
                                                                                                        </td>
                                                                                                        <td style="width: 47px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 55%; background-color: #fff7e6;">
                                                                                                            <asp:Label ID="Labelh207" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                                                        <td style="width: 222px">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_Korp_negaraKota" runat="server" CssClass="textbox"
                                                                                                                MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td style="width: 36px">
                                                                                                        </td>
                                                                                                        <td style="width: 47px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 55%; background-color: #fff7e6;">
                                                                                                            <asp:Label ID="Labelh208" runat="server" Text="Negara"></asp:Label>
                                                                                                            <asp:Label ID="Labelh159" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                        <td style="width: 222px; height: 18px">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_Korp_NegaraKorp" runat="server" CssClass="textbox"
                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                            <asp:HiddenField ID="hfPenerimaNasabah_Korp_NegaraKorp" runat="server" />
                                                                                                        </td>
                                                                                                        <td style="width: 36px">
                                                                                                        </td>
                                                                                                        <td style="width: 47px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 55%; background-color: #fff7e6;">
                                                                                                            <asp:Label ID="Labelh209" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                        <td style="height: 18px; width: 222px;">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_Korp_NegaraLainnyaKorp" runat="server" CssClass="textbox"
                                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                        </td>
                                                                                                        <td style="width: 47px; height: 18px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 55%; background-color: #fff7e6;">
                                                                                                            <asp:Label ID="Labelse9271" runat="server" Text="e. Nilai Transaksi Keuangan (dalam Rupiah )"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="height: 18px; width: 222px;">
                                                                                                            <asp:TextBox ID="txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan" runat="server" CssClass="textbox"
                                                                                                                MaxLength="15" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            &nbsp;</td>
                                                                                                        <td style="width: 47px; height: 18px">
                                                                                                            &nbsp;</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 55%; background-color: #fff7e6">
                                                                                                        </td>
                                                                                                        <td style="width: 222px; height: 18px">
                                                                                                            &nbsp;
                                                                                                            <asp:ImageButton ID="ImageButton_CancelPenerimaKorp" runat="server"  ImageUrl="~/Images/button/Ok.gif" /></td>
                                                                                                        <td>
                                                                                                        </td>
                                                                                                        <td style="width: 47px; height: 18px">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </asp:View>
                                                                                        </asp:MultiView>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:View>
                                                            <asp:View ID="ViewSwiftOutIdenPenerimaNonNasabah" runat="server">
                                                                <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                                    bgcolor="#dddddd" border="2">
                                                                    <tr>
                                                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                            style="height: 6px">
                                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        <asp:Label ID="Labelh12" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <a href="#" onclick="javascript:ShowHidePanel('DIdentitasPenerimaNonNasabah','Img10','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                            title="click to minimize or maximize">
                                                                                            <img id="Img10" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="DIdentitasPenerimaNonNasabah">
                                                                        <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                            </table>
                                                                            <table style="width: 98%; height: 49px">
                                                                                <tr>
                                                                                    <td style="width: 30%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelh9272" runat="server" Text="a. Nama Bank"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 204px">
                                                                                        <asp:TextBox ID="txtPenerimaNonNasabah_namabank" runat="server" CssClass="textbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 35px">
                                                                                        &nbsp;</td>
                                                                                    <td style="width: 47px">
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 30%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelh1146" runat="server" Text="b. Nama Lengkap"></asp:Label>
                                                                                        <asp:Label ID="Labelh1147" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                    <td style="width: 204px">
                                                                                        <asp:TextBox ID="txtPenerimaNonNasabah_Nama" runat="server" CssClass="textbox" MaxLength="100"
                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 35px">
                                                                                    </td>
                                                                                    <td style="width: 47px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 30%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelh210" runat="server" Text="c. Alamat Sesuai Bukti Identitas/ Voucher"
                                                                                            Height="14px" Width="242px"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 204px">
                                                                                        &nbsp;</td>
                                                                                    <td style="width: 35px">
                                                                                    </td>
                                                                                    <td style="width: 47px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 30%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelalam1150" runat="server" Text="Alamat"></asp:Label></td>
                                                                                    <td style="width: 204px">
                                                                                        <asp:TextBox ID="txtPenerimaNonNasabah_Alamat" runat="server" CssClass="textbox"
                                                                                            MaxLength="100" TabIndex="2" Width="421px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 35px">
                                                                                    </td>
                                                                                    <td style="width: 47px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 30%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelneg211" runat="server" Text="Negara"></asp:Label>
                                                                                        <asp:Label ID="Labelneg165" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                    <td style="width: 204px">
                                                                                        <asp:TextBox ID="txtPenerimaNonNasabah_Negara" runat="server" CssClass="textbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hfPenerimaNonNasabah_Negara" runat="server" />
                                                                                    </td>
                                                                                    <td style="width: 35px">
                                                                                    </td>
                                                                                    <td style="width: 47px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 30%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labellain212" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                    <td style="width: 204px">
                                                                                        <asp:TextBox ID="txtPenerimaNonNasabah_negaraLain" runat="server" CssClass="textbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 35px">
                                                                                    </td>
                                                                                    <td style="width: 47px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 30%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelnilai74" runat="server" Text="d. Nilai Transaksi Keuangan (dalam Rupiah) "></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 204px">
                                                                                        <asp:TextBox ID="txtPenerimaNonNasabah_nilaiTransaksiKeuangan" runat="server" CssClass="textbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 35px">
                                                                                        &nbsp;</td>
                                                                                    <td style="width: 47px">
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 30%; background-color: #fff7e6">
                                                                                    </td>
                                                                                    <td style="width: 204px">
                                                                                        &nbsp;
                                                                                        <asp:ImageButton ID="ImageButton_CancelPenerimaNonNasabah" runat="server"  ImageUrl="~/Images/button/Ok.gif" /></td>
                                                                                    <td style="width: 35px">
                                                                                    </td>
                                                                                    <td style="width: 47px">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:View>
                                                        </asp:MultiView></td>
                                                </tr>
                                            </table>
                                            <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                bgcolor="#dddddd" border="2">
                                                <tr>
                                                    <td background="Images/search-bar-background.gif" valign="middle" align="left" style="width: 100%;">
                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td class="formtext">
                                                                    <asp:Label ID="Labelj13" runat="server" Text="E. Transaksi" Font-Bold="True"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <a href="#" onclick="javascript:ShowHidePanel('Etransaksi','Img11','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                        title="click to minimize or maximize">
                                                                        <img id="Img11" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:Label ID="Labelj215" runat="server" Font-Italic="True" Text="(Diisi oleh PJK Bank  yang bertindak sebagai Penyelenggara Pengirim Asal atau sebagai Penyelenggara Penerus)"></asp:Label></td>
                                                </tr>
                                                <tr id="Etransaksi">
                                                    <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;">
                                                        <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                        </table>
                                                        <table style="width: 100%; height: 49px">
                                                            <tr>
                                                                <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                                    <asp:Label ID="Labelg216" runat="server" Text="a. Tanggal Transaksi (tgl/bln/thn) "></asp:Label>
                                                                    <asp:Label ID="Labelg217" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_tanggal" runat="server" CssClass="textbox" TabIndex="2"
                                                                        Width="122px" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                    <asp:Label ID="Label218" runat="server" Text="b. Waktu Transaksi diproses (Time Indication) "></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_waktutransaksi" runat="server" CssClass="textbox" MaxLength="20"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                    <asp:Label ID="Label9280" runat="server" Text="c. Referensi Pengirim" Width="129px"></asp:Label>&nbsp;
                                                                    <asp:Label ID="Label167" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_Sender" runat="server" CssClass="textbox" MaxLength="20"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                    <asp:Label ID="Label9284" runat="server" Text="d. Kode Operasi Bank"></asp:Label>
                                                                    <asp:Label ID="Label177" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_BankOperationCode" runat="server" CssClass="textbox" MaxLength="30"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                    <asp:Label ID="Label9286" runat="server" Text="e.  Kode Instruksi"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_InstructionCode" runat="server" CssClass="textbox" MaxLength="50"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                    <asp:Label ID="Labelg235" runat="server" Text="f. Kantor Cabang Penyelenggara Pengirim Asal"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_kantorCabangPengirim" runat="server" CssClass="textbox"
                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                    <asp:Label ID="Label236" runat="server" Text="g. Kode Tipe Transaksi "></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_kodeTipeTransaksi" runat="server" CssClass="textbox" MaxLength="50"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                    <asp:Label ID="Labelg237" runat="server" Text=" h.  Value Date/Currency/Interbank Settled Amount"></asp:Label></td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6; height: 20px;">
                                                                    <asp:Label ID="Labelg238" runat="server" Text="Tanggal Transaksi (Value Date)"></asp:Label>
                                                                    <asp:Label ID="Labelg280" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                <td style="height: 20px">
                                                                    <asp:TextBox ID="Transaksi_ValueTanggalTransaksi" runat="server" CssClass="textbox"
                                                                        TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                                    <asp:Label ID="Labely239" runat="server" Text="Nilai Transaksi (Amount of The EFT)"></asp:Label>
                                                                    <asp:Label ID="Labelg281" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_nilaitransaksi" runat="server" CssClass="textbox" MaxLength="50"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; height: 90px; background-color: #fff7e6">
                                                                    <asp:Label ID="Labele240" runat="server" Text="Mata Uang Transaksi (currency of the EFT)"></asp:Label>
                                                                    <asp:Label ID="Labele281" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                <td style="height: 90px">
                                                                    <asp:TextBox ID="Transaksi_MataUangTransaksi" runat="server" CssClass="textbox" MaxLength="50"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                    <asp:HiddenField ID="hfTransaksi_MataUangTransaksi" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                                    <asp:Label ID="Label1" runat="server" Text="Mata Uang Transaksi Lainnya"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_MataUangTransaksiLain" runat="server" CssClass="textbox"
                                                                        MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                    <asp:Label ID="Labele243" runat="server" Text="Nilai Transaksi Keuangan yang dikirim dalam rupiah"></asp:Label>
                                                                    <asp:Label ID="Labelg282" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_AmountdalamRupiah" runat="server" CssClass="textbox" MaxLength="50"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                    <asp:Label ID="Labels244" runat="server" Text="i. Currency/Instructed Amount"></asp:Label></td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                    <asp:Label ID="Label9298" runat="server" Text="Mata Uang yang diinstruksikan"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_currency" runat="server" CssClass="textbox" MaxLength="50"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                    <asp:HiddenField ID="hfTransaksi_currency" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                                    <asp:Label ID="Label2" runat="server" Text="Mata Uang Lainnya"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_currencyLain" runat="server" CssClass="textbox" MaxLength="30"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                    <asp:Label ID="Labele46" runat="server" Text="Instructed Amount "></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_instructedAmount" runat="server" CssClass="textbox" MaxLength="50"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                                    <asp:Label ID="Label2e47" runat="server" Text="j. Nilai Tukar (Exchange Rate) "></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_nilaiTukar" runat="server" CssClass="textbox" MaxLength="50"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                    <asp:Label ID="Labele248" runat="server" Text="k. Sending Instituion"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_sendingInstitution" runat="server" CssClass="textbox"
                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                    <asp:Label ID="Label2e51" runat="server" Text="l. Tujuan Transaksi "></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_TujuanTransaksi" runat="server" CssClass="textbox" MaxLength="100"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6;">
                                                                    <asp:Label ID="Labele252" runat="server" Text="m. Sumber Penggunaan Dana "></asp:Label>
                                                                    <asp:Label ID="Label21" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="Transaksi_SumberPenggunaanDana" runat="server" CssClass="textbox"
                                                                        MaxLength="100" TabIndex="2" Width="296px" Enabled="False"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                bgcolor="#dddddd" border="2">
                                                <tr>
                                                    <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                        style="height: 6px">
                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td class="formtext">
                                                                    <asp:Label ID="Labelr14" runat="server" Text="F. Informasi Lainnya" Font-Bold="True"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <a href="#" onclick="javascript:ShowHidePanel('FInformasiLainnya','Img12','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                        title="click to minimize or maximize">
                                                                        <img id="Img12" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:Label ID="Labelr230" runat="server" Font-Italic="True" Text="(Diisi oleh PJK Bank  yang bertindak sebagai Penyelenggara Pengirim Asal atau sebagai Penyelenggara Penerus)"></asp:Label></td>
                                                </tr>
                                                <tr id="FInformasiLainnya">
                                                    <td valign="top" bgcolor="#ffffff" style="height: 124px">
                                                        <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                        </table>
                                                        <table style="width: 98%; height: 49px">
                                                            <tr>
                                                                <td style="width: 48%; height: 15px; background-color: #fff7e6">
                                                                    <asp:Label ID="Labelr233" runat="server" Text="a. Information about the sender's
    correspondent"></asp:Label>
                                                                </td>
                                                                <td style="height: 15px; width: 222px; color: #000000;">
                                                                    <asp:TextBox ID="InformasiLainnya_Sender" runat="server" CssClass="textbox" MaxLength="140"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                <td style="height: 15px; width: 36px; color: #000000;">
                                                                </td>
                                                                <td style="width: 47px; height: 15px">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 48%; background-color: #fff7e6; height: 20px;">
                                                                    <asp:Label ID="Labele249" runat="server" Text="b. Information about the receiver's correspondent "></asp:Label></td>
                                                                <td style="width: 222px; height: 20px;">
                                                                    <asp:TextBox ID="InformasiLainnya_receiver" runat="server" CssClass="textbox" MaxLength="140"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                <td style="width: 36px; height: 20px;">
                                                                </td>
                                                                <td style="width: 47px; height: 20px;">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 48%; background-color: #fff7e6">
                                                                    <asp:Label ID="Label253" runat="server" Text="c. Information about the third reimbursement
    institution" Width="369px"></asp:Label></td>
                                                                <td style="width: 222px">
                                                                    <asp:TextBox ID="InformasiLainnya_thirdReimbursement" runat="server" CssClass="textbox"
                                                                        MaxLength="140" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                <td style="width: 36px">
                                                                </td>
                                                                <td style="width: 47px">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 48%; background-color: #fff7e6">
                                                                    <asp:Label ID="Label254" runat="server" Text="d. Information about the intermediary institution "
                                                                        Width="290px"></asp:Label></td>
                                                                <td style="width: 222px">
                                                                    <asp:TextBox ID="InformasiLainnya_intermediary" runat="server" CssClass="textbox"
                                                                        MaxLength="140" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                <td style="width: 36px">
                                                                </td>
                                                                <td style="width: 47px">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 48%; background-color: #fff7e6;">
                                                                    <asp:Label ID="Label256" runat="server" Text="e.   Remittance Information"></asp:Label>
                                                                </td>
                                                                <td style="width: 222px;">
                                                                    <asp:TextBox ID="InformasiLainnya_Remittance" runat="server" CssClass="textbox" MaxLength="140"
                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 36px;">
                                                                </td>
                                                                <td style="width: 47px;">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 48%; background-color: #fff7e6">
                                                                    <asp:Label ID="Labelf257" runat="server" Text="f. Sender to receiver information"></asp:Label></td>
                                                                <td style="width: 222px">
                                                                    <asp:TextBox ID="InformasiLainnya_SenderToReceiver" runat="server" CssClass="textbox"
                                                                        MaxLength="210" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 36px">
                                                                </td>
                                                                <td style="width: 47px">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 48%; background-color: #fff7e6; height: 20px;">
                                                                    <asp:Label ID="Label258" runat="server" Text="g. Regulatory Reporting"></asp:Label></td>
                                                                <td style="width: 222px; height: 20px;">
                                                                    <asp:TextBox ID="InformasiLainnya_RegulatoryReport" runat="server" CssClass="textbox"
                                                                        MaxLength="105" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                <td style="width: 36px; height: 20px;">
                                                                </td>
                                                                <td style="width: 47px; height: 20px;">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 48%; background-color: #fff7e6">
                                                                    <asp:Label ID="Label259" runat="server" Text="h. Envelope contents"></asp:Label></td>
                                                                <td style="width: 222px">
                                                                    <asp:TextBox ID="InformasiLainnya_EnvelopeContents" runat="server" CssClass="textbox"
                                                                        MaxLength="100" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                <td style="width: 36px">
                                                                </td>
                                                                <td style="width: 47px">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 48%; background-color: #fff7e6; height: 15px;">
                                                                </td>
                                                                <td style="height: 15px; width: 222px;">
                                                                </td>
                                                                <td style="width: 36px; height: 15px;">
                                                                </td>
                                                                <td style="width: 47px; height: 15px">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                        <asp:View ID="vwMessage" runat="server">
                                            <table width="100%">
                                                <tr>
                                                    <td class="formtext" align="center" style="height: 15px">
                                                        <asp:Label runat="server" ID="lblMsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="formtext" align="center">
                                                        <asp:ImageButton ID="imgOKMsg" runat="server"  ImageUrl="~/Images/button/Ok.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </asp:MultiView>
                                </ajax:AjaxPanel>
                             </td>
                        </tr>
             <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator><ajax:AjaxPanel ID="AjaxPanel2" runat="server"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" background="Images/button-bground.gif" valign="middle">
                                <img height="1" src="Images/blank.gif" width="5" /></td>
                            <td align="left" background="Images/button-bground.gif" valign="middle">
                                <img height="15" src="images/arrow.gif" width="15" />
                            </td>
                            <td background="Images/button-bground.gif" style="width: 5px">
                                </td>
                           <%-- <td background="Images/button-bground.gif">
                                <asp:ImageButton ID="ImageButtonSave" runat="server" ImageUrl="~/Images/Button/Save.gif"
                                    SkinID="AddButton" />
                            </td>--%>
                            <td background="Images/button-bground.gif" style="width: 62px">
                                <asp:ImageButton ID="ImageButtonCancel" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/Back.gif"
                                     />
                            </td>
                            <td background="Images/button-bground.gif" width="99%">
                                <img height="1" src="Images/blank.gif" width="1" /></td>
                            <td>
                                </td>
                        </tr>
                    </table>
                </ajax:AjaxPanel>
                    </table>
                    </div>                                
            </td>
        </tr>
	</table>
                
            </asp:Content>
                       

