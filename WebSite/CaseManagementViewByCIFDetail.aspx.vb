﻿Imports System.Data.SqlClient
Imports Sahassa.AML.TableAdapterHelper
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports AMLBLL
Imports System.Data
Imports System.Web.Configuration

Partial Class CaseManagementViewByCIFDetail
    Inherits Parent
#Region " Property "
    Private ReadOnly Property GetCIFNo() As String
        Get
            Return Me.Request.Params.Item("CIFNo").Trim
        End Get
    End Property
    Private Property SearchCaseID() As String
        Get
            If Not Session("CaseManagementViewCaseID") Is Nothing Then
                Return Session("CaseManagementViewCaseID")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewCaseID") = value
        End Set
    End Property
    Private Property SearchCaseDescription() As String
        Get
            If Not Session("CaseManagementViewCaseDescription") Is Nothing Then
                Return Session("CaseManagementViewCaseDescription")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewCaseDescription") = value
        End Set
    End Property
    Private Property SearchCaseStatus() As String
        Get
            If Not Session("CaseManagementViewCaseStatus") Is Nothing Then
                Return Session("CaseManagementViewCaseStatus")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewCaseStatus") = value
        End Set
    End Property
    Private Property SearchWorkFlowStep() As String
        Get
            If Not Session("CaseManagementViewWorkFlowStep") Is Nothing Then
                Return Session("CaseManagementViewWorkFlowStep")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewWorkFlowStep") = value
        End Set
    End Property
    Private Property SearchAccountOwner() As String
        Get
            If Not Session("CaseManagementViewAccountOwner") Is Nothing Then
                Return Session("CaseManagementViewAccountOwner")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewAccountOwner") = value
        End Set
    End Property
    Private Property SearchLastProposedAction() As String
        Get
            If Not Session("CaseManagementViewLastProposedAction") Is Nothing Then
                Return Session("CaseManagementViewLastProposedAction")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewLastProposedAction") = value
        End Set
    End Property
    Private Property SearchCreateDateFirst() As String
        Get
            If Not Session("CaseManagementViewCreatedDateFirst") Is Nothing Then
                Return Session("CaseManagementViewCreatedDateFirst")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewCreatedDateFirst") = value
        End Set
    End Property
    Private Property SearchCreateDateLast() As String
        Get
            If Not Session("CaseManagementViewCreatedDateLast") Is Nothing Then
                Return Session("CaseManagementViewCreatedDateLast")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewCreatedDateLast") = value
        End Set
    End Property
    Private Property SearchLastUpdatedDateFirst() As String
        Get
            If Not Session("CaseManagementViewLastUpdatedDateFirst") Is Nothing Then
                Return Session("CaseManagementViewLastUpdatedDateFirst")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewLastUpdatedDateFirst") = value
        End Set
    End Property
    Private Property SearchLastUpdatedDateLast() As String
        Get
            If Not Session("CaseManagementViewLastUpdatedDateLast") Is Nothing Then
                Return Session("CaseManagementViewLastUpdatedDateLast")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewLastUpdatedDateLast") = value
        End Set
    End Property
    Private Property SearchPIC() As String
        Get
            If Not Session("CaseManagementViewPIC") Is Nothing Then
                Return Session("CaseManagementViewPIC")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewPIC") = value
        End Set
    End Property
    Private Property SearchHighRiskCustomer() As String
        Get
            If Not Session("CaseManagementViewHighRiskCustomer") Is Nothing Then
                Return Session("CaseManagementViewHighRiskCustomer")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewHighRiskCustomer") = value
        End Set
    End Property
    Private Property SearchSameCaseCount() As String
        Get
            If Not Session("CaseManagementSearchSameCaseCount") Is Nothing Then
                Return Session("CaseManagementSearchSameCaseCount")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementSearchSameCaseCount") = value
        End Set
    End Property
    Private Property SearchAging() As String
        Get
            If Not Session("CaseManagementViewAging") Is Nothing Then
                Return Session("CaseManagementViewAging")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewAging") = value
        End Set
    End Property
    Private Property SearchCustomerName() As String
        Get
            If Not Session("CaseManagementViewCustomerName") Is Nothing Then
                Return Session("CaseManagementViewCustomerName")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewCustomerName") = value
        End Set
    End Property
    Private Property SearchRMCode() As String
        Get
            If Not Session("CaseManagementViewRMCode") Is Nothing Then
                Return Session("CaseManagementViewRMCode")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewRMCode") = value
        End Set
    End Property

    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("CaseManagementViewSelected") Is Nothing, New ArrayList, Session("CaseManagementViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CaseManagementViewSelected") = value
        End Set
    End Property


    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CaseManagementViewSort") Is Nothing, "Aging desc", Session("CaseManagementViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("CaseManagementViewSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CaseManagementViewCurrentPage") Is Nothing, 0, Session("CaseManagementViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CaseManagementViewCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CaseManagementViewRowTotal") Is Nothing, 0, Session("CaseManagementViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CaseManagementViewRowTotal") = Value
        End Set
    End Property
    Private ReadOnly Property Tables() As String
        Get
            Dim StrQuery As New StringBuilder
            StrQuery.Append(" CaseManagement INNER JOIN CaseStatus ON CaseManagement.FK_CaseStatusID = CaseStatus.CaseStatusId INNER JOIN AccountOwner ON CaseManagement.FK_AccountOwnerID = AccountOwner.AccountOwnerId LEFT OUTER JOIN CaseManagementProposedAction ON CaseManagement.FK_LastProposedStatusID = CaseManagementProposedAction.PK_CaseManagementProposedActionID ")
            StrQuery.Append(" INNER JOIN CaseManagementWorkflow ON CaseManagement.FK_AccountOwnerId=CaseManagementWorkflow.AccountOwnerId ")
            StrQuery.Append(" INNER JOIN CaseManagementWorkflowDetail ON CaseManagementWorkflow.PK_CMW_ID=CaseManagementWorkflowDetail.FK_CMW_ID ")
            StrQuery.Append(" INNER JOIN MapCaseManagementWorkflowHistory mcmwh ON mcmwh.FK_CaseManagementId=casemanagement.PK_CaseManagementID ")
            Return StrQuery.ToString()
        End Get
    End Property
    Private ReadOnly Property PK() As String
        Get
            Return "CaseManagement.PK_CaseManagementID"
        End Get
    End Property
    Private ReadOnly Property Fields() As String

        Get
            Dim strField As New StringBuilder
            strField.Append("CaseManagement.CustomerName,CaseManagement.RMCode,CaseManagement.PK_CaseManagementID, CaseManagement.CaseDescription, CaseManagement.FK_CaseStatusID,")
            strField.Append("CaseStatus.CaseStatusDescription, CaseManagement.WorkflowStep, CaseManagement.CreatedDate, CaseManagement.LastUpdated,")

            'update PIC
            'strField.Append("CaseManagement.FK_AccountOwnerID, AccountOwner.AccountOwnerName, CAST(CaseManagement.FK_AccountOwnerID AS VARCHAR) + ' - ' + AccountOwner.AccountOwnerName AS AccountOwnerIDName, CaseManagement.PIC, CaseManagement.HasOpenIssue,")
            strField.Append("CaseManagement.FK_AccountOwnerID, AccountOwner.AccountOwnerName, CAST(CaseManagement.FK_AccountOwnerID AS VARCHAR) + ' - ' + AccountOwner.AccountOwnerName AS AccountOwnerIDName, CaseManagement.HasOpenIssue,")

            strField.Append("CaseManagement.Aging, CaseManagement.IsReportedtoRegulator, CaseManagement.PPATKConfirmationno,")
            'strField.Append("CaseManagementProposedAction.ProposedAction,SameCaseCount=(SELECT COUNT(1) FROM CaseManagementSameCaseHistory cmsch WHERE cmsch.FK_CaseManagement_ID=CaseManagement.PK_CaseManagementID)")
            'strField.Append("CaseManagementProposedAction.ProposedAction,SameCaseCount=(SELECT COUNT(1) FROM CaseManagementSameCaseHistory cmsch WHERE cmsch.FK_CaseManagement_ID=CaseManagement.PK_CaseManagementID),HighRiskCustomer=dbo.ufn_GetHighRiskCustomerByPkCaseManagementID(casemanagement.PK_CaseManagementID)  ")

            strField.Append("CaseManagementProposedAction.ProposedAction,SameCaseCount=(SELECT COUNT(1) FROM CaseManagementSameCaseHistory cmsch WHERE cmsch.FK_CaseManagement_ID=CaseManagement.PK_CaseManagementID),HighRiskCustomer =ISNULL((SELECT cmr.Risk FROM CaseManagementRisk cmr WHERE cmr.FK_CaseManagement_ID=casemanagement.PK_CaseManagementID),'Low'),casemanagement.cifno ")
            'strField.Append("CaseManagementProposedAction.ProposedAction,SameCaseCount=(SELECT COUNT(1) FROM CaseManagementSameCaseHistory cmsch WHERE cmsch.FK_CaseManagement_ID=CaseManagement.PK_CaseManagementID),HighRiskCustomer=( SELECT CASE  when COUNT(1) >0 THEN 'High Risk Customer' ELSE 'Non High Risk Customer' end  FROM CaseManagementProfileTopology cmpt INNER JOIN AllAccount_AllInfo aaai ON aaai.AccountNo=cmpt.AccountNo INNER JOIN CustomerInList cil ON cil.CIFNo=aaai.CIFNO WHERE cmpt.FK_CaseManagement_ID=casemanagement.PK_CaseManagementID)")
            Return strField.ToString
        End Get
    End Property

    Private ReadOnly Property GroupBy() As String
        Get
            Dim strField As New StringBuilder
            strField.Append("CaseManagement.CustomerName,CaseManagement.RMCode,CaseManagement.PK_CaseManagementID, CaseManagement.CaseDescription, CaseManagement.FK_CaseStatusID,")
            strField.Append("CaseStatus.CaseStatusDescription, CaseManagement.WorkflowStep, CaseManagement.CreatedDate, CaseManagement.LastUpdated,")

            'update PIC
            'strField.Append("CaseManagement.FK_AccountOwnerID, AccountOwner.AccountOwnerName, CAST(CaseManagement.FK_AccountOwnerID AS VARCHAR) + ' - ' + AccountOwner.AccountOwnerName, CaseManagement.PIC, CaseManagement.HasOpenIssue,")
            strField.Append("CaseManagement.FK_AccountOwnerID, AccountOwner.AccountOwnerName, CAST(CaseManagement.FK_AccountOwnerID AS VARCHAR) + ' - ' + AccountOwner.AccountOwnerName, CaseManagement.HasOpenIssue,")

            strField.Append("CaseManagement.Aging, CaseManagement.IsReportedtoRegulator, CaseManagement.PPATKConfirmationno,")
            strField.Append("CaseManagementProposedAction.ProposedAction,CaseManagement.cifno")
            Return strField.ToString
        End Get
    End Property


    Private ReadOnly Property SetnGetBindTableAll() As Data.DataTable
        Get
            ' Return IIf(Session("CaseManagementViewData") Is Nothing, New AMLDAL.CaseManagement.CaseManagementDataTable, Session("CaseManagementViewData"))

            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            strAllWhereClause = String.Join(" and ", strWhereClause)
            'If strAllWhereClause.Trim = "" Then
            '    strAllWhereClause += " CaseManagement.fk_accountownerid in (select accountownerid from AccountOwnerUserMapping where workingunitid in( select * from ufn_GetHierarkiWorkingUnitByUserID('" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "'))) "
            'Else
            '    strAllWhereClause += " and  CaseManagement.fk_accountownerid in (select accountownerid from AccountOwnerUserMapping where workingunitid in( select * from ufn_GetHierarkiWorkingUnitByUserID('" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "'))) "
            'End If
            If strAllWhereClause.Trim = "" Then
                strAllWhereClause += " ("
                strAllWhereClause += " '|' + CaseManagementWorkflowDetail.Approvers + '|'  like '%|" & Sahassa.AML.Commonly.SessionPkUserId & "|%'"
                strAllWhereClause += " OR '|' + CaseManagementWorkflowDetail.NotifyOthers + '|'  like '%|" & Sahassa.AML.Commonly.SessionPkUserId & "|%'"
                strAllWhereClause += " )"
                'strAllWhereClause += "     and mcmwh.userid like '%" & Sahassa.AML.Commonly.SessionUserId & "%' and mcmwh.fk_eventtypeid IN (2, 8)  and mcmwh.bcomplate=0 and CaseManagement.CIFNo = '" & GetCIFNo & "'"
                'strAllWhereClause += "     and mcmwh.fk_eventtypeid IN (2, 8)  and mcmwh.bcomplate=0 and CaseManagement.CIFNo = '" & GetCIFNo & "'"
                strAllWhereClause += "  and dbo.ufn_GetSerialNoLastWorkflow(casemanagement.PK_CaseManagementID)<= dbo.ufn_GetSerialNoLastWorkflowByUserID(casemanagement.PK_CaseManagementID,'" & Sahassa.AML.Commonly.SessionPkUserId & "')   and mcmwh.fk_eventtypeid IN (2, 8)  and mcmwh.bcomplate=0 and CaseManagement.CIFNo = '" & GetCIFNo & "'"

            Else
                strAllWhereClause += " AND ("
                strAllWhereClause += " '|' + CaseManagementWorkflowDetail.Approvers + '|'  like '%|" & Sahassa.AML.Commonly.SessionPkUserId & "|%'"
                strAllWhereClause += " OR '|' + CaseManagementWorkflowDetail.NotifyOthers + '|'  like '%|" & Sahassa.AML.Commonly.SessionPkUserId & "|%'"
                strAllWhereClause += " )"
                'strAllWhereClause += " and mcmwh.userid like '%" & Sahassa.AML.Commonly.SessionUserId & "%' and mcmwh.fk_eventtypeid IN (2, 8)  and mcmwh.bcomplate=0 and CaseManagement.CIFNo = '" & GetCIFNo & "'"
                'strAllWhereClause += "     and mcmwh.fk_eventtypeid IN (2, 8)  and mcmwh.bcomplate=0 and CaseManagement.CIFNo = '" & GetCIFNo & "'"
                strAllWhereClause += "  and dbo.ufn_GetSerialNoLastWorkflow(casemanagement.PK_CaseManagementID)<= dbo.ufn_GetSerialNoLastWorkflowByUserID(casemanagement.PK_CaseManagementID,'" & Sahassa.AML.Commonly.SessionPkUserId & "')   and mcmwh.fk_eventtypeid IN (2, 8)  and mcmwh.bcomplate=0 and CaseManagement.CIFNo = '" & GetCIFNo & "'"
            End If
            Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCountDistinctByColumn(Me.Tables, strAllWhereClause, Me.PK)
            Dim pageSize As Long = Integer.MaxValue
            'If Sahassa.AML.Commonly.SessionPagingLimit = "" Then
            '    pageSize = 10
            'Else
            '    pageSize = Sahassa.AML.Commonly.SessionPagingLimit
            'End If
            'Return Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, pageSize, Me.Fields, strAllWhereClause, Me.GroupBy)
            Dim strsql As String = ""
            strsql = "select " & Me.Fields & " from " & Me.Tables & " where " & strAllWhereClause & "  group by " & GroupBy & " order by " & Me.SetnGetSort


            Return SahassaNettier.Data.DataRepository.Provider.ExecuteDataSet(Sahassa.AML.Commonly.GetSQLCommand(strsql)).Tables(0)
        End Get

    End Property

    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property SetnGetBindTable() As Data.DataTable
        Get
            ' Return IIf(Session("CaseManagementViewData") Is Nothing, New AMLDAL.CaseManagement.CaseManagementDataTable, Session("CaseManagementViewData"))

            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            strAllWhereClause = String.Join(" and ", strWhereClause)

            strAllWhereClause += " CaseManagement.CIFNo = '" & GetCIFNo & "'"

            Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCountDistinctByColumn(Me.Tables, strAllWhereClause, Me.PK)
            Dim pageSize As Long
            If Sahassa.AML.Commonly.SessionPagingLimit = "" Then
                pageSize = 10
            Else
                pageSize = Sahassa.AML.Commonly.SessionPagingLimit
            End If
            Session("strAllWhereClause") = strAllWhereClause
            Return Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, pageSize, Me.Fields, strAllWhereClause, Me.GroupBy)
        End Get

    End Property
#End Region




    Private Sub ClearThisPageSessions()
        Session("CaseManagementSearchSameCaseCount") = Nothing
        Session("CaseManagementViewSelected") = Nothing
        Session("CaseManagementViewFieldSearch") = Nothing
        Session("CaseManagementViewValueSearch") = Nothing
        Session("CaseManagementViewSort") = Nothing
        Session("CaseManagementViewCurrentPage") = Nothing
        Session("CaseManagementViewRowTotal") = Nothing
        Session("CaseManagementViewData") = Nothing
        Session("CaseManagementViewRMCode") = Nothing
        Session("CaseManagementViewCustomerName") = Nothing
        SetnGetSelectedItemForAttach = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Panel1.Visible = False
                Me.ClearThisPageSessions()
                FillProposedAction()

                'CboProposedAction.Attributes.Add("onchange", "cekproposedaction('" & CboProposedAction.ClientID & "')")

                If Request.Params("CaseDescription") <> "" Then

                    SearchCaseDescription = Request.Params("CaseDescription")
                End If
                Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    If Request.Params("msg") <> "" Then
                        Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, "  window.alert(' " & Request.Params("msg") & ".')", True)
                    End If

                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            Me.GridMSUserView.DataSource = Me.SetnGetBindTable
            'Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
            Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridMSUserView.DataBind()
        Catch
            Throw
        End Try
    End Sub
    Public Sub BindGridSum()
        Try
          


            Dim connectionString As String = WebConfigurationManager.ConnectionStrings("AMLConnectionString").ConnectionString
            Dim con As New SqlConnection(connectionString)
            'Dim QueryString As String

            'QueryString = String.Format("SELECT cm.CIFNo,CaseStatus.CaseStatusDescription CaseStatusDescription,COUNT(1) AS [COUNT],")
            'QueryString = QueryString + (" dbo.ufn_GetCaseManagementSumCaseDescription (CIFNo, cm.FK_CaseStatusID) AS AlertType,dbo.ufn_GetCaseManagementSumAging (CIFNo, cm.FK_CaseStatusID) AS Aging")
            'QueryString += " from CaseManagement cm INNER JOIN CaseStatus ON cm.FK_CaseStatusID = CaseStatus.CaseStatusId INNER JOIN AccountOwner ON cm.FK_AccountOwnerID = AccountOwner.AccountOwnerId LEFT OUTER JOIN CaseManagementProposedAction ON cm.FK_LastProposedStatusID = CaseManagementProposedAction.PK_CaseManagementProposedActionID "
            'QueryString += " INNER JOIN CaseManagementWorkflow ON cm.FK_AccountOwnerId=CaseManagementWorkflow.AccountOwnerId "
            'QueryString += " INNER JOIN CaseManagementWorkflowDetail ON CaseManagementWorkflow.PK_CMW_ID=CaseManagementWorkflowDetail.FK_CMW_ID "
            'QueryString += " INNER JOIN MapCaseManagementWorkflowHistory mcmwh ON mcmwh.FK_CaseManagementId=cm.PK_CaseManagementID "
            'QueryString += " where  ("
            'QueryString += " '|' + CaseManagementWorkflowDetail.Approvers + '|'  like '%|" & Sahassa.AML.Commonly.SessionPkUserId & "|%'"
            'QueryString += " OR '|' + CaseManagementWorkflowDetail.NotifyOthers + '|'  like '%|" & Sahassa.AML.Commonly.SessionPkUserId & "|%'"
            'QueryString += " )"
            'QueryString += "  and dbo.ufn_GetSerialNoLastWorkflow(cm.PK_CaseManagementID)<= dbo.ufn_GetSerialNoLastWorkflowByUserID(cm.PK_CaseManagementID,'" & Sahassa.AML.Commonly.SessionPkUserId & "')    and mcmwh.fk_eventtypeid IN (2, 8)  and mcmwh.bcomplate=0 and cm.CIFNo = '" & GetCIFNo & "'"
            'QueryString += "     group by cm.CIFNo,CaseStatusDescription ,cm.FK_CaseStatusID"



            Dim sql As String
            sql = "SELECT CIFNo,cs.CaseStatusDescription,COUNT(1) AS COUNT,  dbo.ufn_GetCaseManagementSumCaseDescription (CIFNo, cm.FK_CaseStatusID) AS AlertType,  " & vbCrLf _
                & " dbo.ufn_GetCaseManagementSumAging (CIFNo, cm.FK_CaseStatusID) AS Aging " & vbCrLf _
                & "FROM casemanagement cm " & vbCrLf _
                & "INNER JOIN CaseStatus cs ON cs.CaseStatusId=cm.FK_CaseStatusID " & vbCrLf _
                & "WHERE CIFNo= '" & GetCIFNo & "'       " & vbCrLf _
                & "GROUP BY CIFNo,CaseStatusDescription,cm.FK_CaseStatusID"


            Dim cmd As New SqlCommand()

            cmd.Connection = con
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings("SQLCommandTimeout")
            cmd.CommandText = sql
            Using ds As DataSet = DataRepository.Provider.ExecuteDataSet(cmd)
                Me.GrdSum.DataSource = ds.Tables(0)
                Me.GrdSum.DataBind()
            End Using

            con.Open()
            con.Close()
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Dim strCaseManagementID As String = e.Item.Cells(1).Text
        Dim strcifno As String = e.Item.Cells(2).Text
        Try

            Sahassa.AML.Commonly.SessionIntendedPage = "CaseManagementViewDetail.aspx?PK_CaseManagementID=" & strCaseManagementID
            Me.Response.Redirect("CaseManagementViewDetail.aspx?PK_CaseManagementID=" & strCaseManagementID & "&source=CIFNo&cifno=" & strcifno, False)
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 14/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
            CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim PKID As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PKID) Then
                    If Not chkBox.Checked Then

                        ArrTarget.Remove(PKID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGridSum()
            Me.BindGrid()
            Me.SetInfoNavigate()
            If SetnGetSelectedItem.Count > 0 Then
                Panel1.Visible = True
            Else
                Panel1.Visible = False
                Using objMapCaseManagementWorkflowHistoryAttachment As TList(Of MapCaseManagementWorkflowHistoryAttachment) = DataRepository.MapCaseManagementWorkflowHistoryAttachmentProvider.GetPaged("FK_UserID = " & Sahassa.AML.Commonly.SessionPkUserId, "", 0, Integer.MaxValue, 0)
                    If objMapCaseManagementWorkflowHistoryAttachment.Count > 0 Then
                        For i As Integer = 0 To objMapCaseManagementWorkflowHistoryAttachment.Count - 1
                            DataRepository.MapCaseManagementWorkflowHistoryAttachmentProvider.Delete(objMapCaseManagementWorkflowHistoryAttachment)
                        Next
                    End If
                End Using
                BindListAttachment()
            End If
            If CboProposedAction.SelectedValue = "5" Then
                trAccountOwner.Visible = True
            Else
                trAccountOwner.Visible = False
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()
        Try

            Dim listPK As New System.Collections.Generic.List(Of String)
            listPK.Add("'0'")
            For Each IdPk As String In Me.SetnGetSelectedItem
                listPK.Add("'" & IdPk.ToString & "'")
            Next

            Me.GridMSUserView.DataSource = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, 0, Int32.MaxValue, Me.Fields, Session("strAllWhereClause") + " and PK_CaseManagementID IN (" & String.Join(",", listPK.ToArray) & ")", Me.GroupBy)
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0,1,4 & 5 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(15).Visible = False
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    ''' <summary>
    ''' export button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=CaseManagementView.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            Dim ArrTarget As ArrayList = SetnGetSelectedItemForAttach
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)

                Dim pk As Long = CType(e.Item.DataItem, System.Data.DataRowView).Item(2)

                Dim strProposedAction As String = CType(e.Item.DataItem, System.Data.DataRowView).Item(16).ToString
                Dim casestatusid As Integer = CType(e.Item.DataItem, System.Data.DataRowView).Item(4)
                Dim currentworkflow As Integer
                Dim currentuserworkflow As Integer

                If strProposedAction = "Confirm As STR" Then
                    For Each Item As TableCell In e.Item.Cells
                        Item.ForeColor = Drawing.Color.Red
                    Next
                End If
                Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommand(" Select  dbo.ufn_GetSerialNoLastWorkflow(" & pk & ")")
                    currentworkflow = DataRepository.Provider.ExecuteScalar(objCommand)
                End Using


                Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommand(" select dbo.ufn_GetSerialNoLastWorkflowByUserID(" & pk & "," & Sahassa.AML.Commonly.SessionPkUserId & ") ")
                    currentuserworkflow = DataRepository.Provider.ExecuteScalar(objCommand)
                End Using

                'cek workflow status masih jalan dan case status=new atau underinvestigate
                If currentuserworkflow >= currentworkflow And (casestatusid = 1 Or casestatusid = 2) Then
                    chkBox.Enabled = True

                Else
                    chkBox.Enabled = False
                End If



                'chkBox.Enabled = True
                If chkBox.Checked = True Then
                    Dim ObjTrack As Boolean = True
                    For i As Integer = 0 To ArrTarget.Count - 1
                        If e.Item.Cells(1).Text = ArrTarget.Item(i) Then
                            ObjTrack = False
                            Exit For
                        Else
                            ObjTrack = True
                        End If
                    Next
                    If ObjTrack = True Then
                        ArrTarget.Add(e.Item.Cells(1).Text)
                    End If
                Else
                    ArrTarget.Remove(e.Item.Cells(1).Text)
                End If

                Using ObjPic As TList(Of MapCaseManagementWorkflowHistory) = DataRepository.MapCaseManagementWorkflowHistoryProvider.GetPaged("FK_EventTypeID in (2,3) and bComplate = 0 and FK_CaseManagementId = " & e.Item.Cells(1).Text, "", 0, Integer.MaxValue, 0)
                    If ObjPic.Count > 0 Then
                        For i As Integer = 0 To ObjPic.Count - 1
                            If i = 0 Then
                                e.Item.Cells(11).Text = ObjPic(i).UserID
                            Else
                                e.Item.Cells(11).Text = e.Item.Cells(10).Text + ";" + ObjPic(i).UserID
                            End If
                        Next
                    End If
                End Using

            ElseIf e.Item.ItemType = ListItemType.Footer Then
                SetnGetSelectedItemForAttach = ArrTarget
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                If chkBox.Enabled Then
                    If Me.CheckBoxSelectAll.Checked Then
                        If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                            ArrTarget.Add(StrKey)
                        End If
                    Else
                        If Me.SetnGetSelectedItem.Contains(StrKey) Then
                            ArrTarget.Remove(StrKey)
                        End If
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub


    ''' <summary>
    ''' Setting Property Searching
    ''' </summary>
    ''' <remarks></remarks>


    Sub BindExportAll()
        GridMSUserView.PageSize = Integer.MaxValue
        Me.GridMSUserView.DataSource = Me.SetnGetBindTableAll
        Me.GridMSUserView.DataBind()
        Me.GridMSUserView.Columns(0).Visible = False
        Me.GridMSUserView.Columns(15).Visible = False
    End Sub

    Protected Sub LnkBtnExportAllToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkBtnExportAllToExcel.Click

        Try

            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindExportAll()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=CaseManagementALL.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try


    End Sub

    Sub LoadDraftWorklfow()
        If ObjTMapCaseManagementWorkflowHistoryDraft.Count > 0 Then
            TextInvestigationNotes.Text = ObjTMapCaseManagementWorkflowHistoryDraft(0).InvestigationNotes
            Try
                CboProposedAction.SelectedValue = ObjTMapCaseManagementWorkflowHistoryDraft(0).FK_CaseManagementProposedActionID.GetValueOrDefault(0)
            Catch ex As Exception

            End Try

            Try
                CboAccountOwner.SelectedValue = ObjTMapCaseManagementWorkflowHistoryDraft(0).FK_AccountOwnerID
            Catch ex As Exception

            End Try
        End If
    End Sub
    Protected Sub DoneProposed()
        Dim oSQLTrans As SqlTransaction = Nothing
        Dim oCurrentWorkflowhistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim ProposedStatusFinal As Integer
        Dim strDescription As String = ""
        Dim StrNextWorkflowStep As String = ""
        Try
            If Page.IsValid Then
                Using adapter As New AMLDAL.CaseManagementTableAdapters.SelectMapCaseManagementWorkflowHistoryTableAdapter
                    oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                    'Selesaikan satu workflow

                    oCurrentWorkflowhistory = GetCurrentWorkflowHistory()
                    If Not oCurrentWorkflowhistory Is Nothing Then
                        strDescription = GetDescriptionByStep(oCurrentWorkflowhistory.workflowstep)
                        adapter.UpdateMapCaseManagementWorkfloHistoryBComplate(True, oCurrentWorkflowhistory.PK_MapCaseManagementWorkflowHistoryID)
                        Dim intAccountownerid As Integer = 0
                        If CboProposedAction.SelectedValue = ProposedAction.ReAssign Then
                            intAccountownerid = CboAccountOwner.SelectedValue
                        End If
                        adapter.usp_DeleteTaskDraftMapCaseManagementWorkFlowHistory(Me.PK_CaseManagementID)
                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskComplated, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, Me.TextInvestigationNotes.Text, CboProposedAction.SelectedValue, True, strDescription, intAccountownerid, True)
                        'MaxPK_MapCaseManagementWorkflowHistoryID()
                        'UpdateMapCaseManagementWorkflowHistoryAttachment(oCurrentWorkflowhistory.PK_MapCaseManagementWorkflowHistoryID, oCurrentWorkflowhistory.FK_CaseManagementID)
                    End If


                    Using adapterCaseManagement As New AMLDAL.CaseManagementTableAdapters.SelectCaseManagementTableAdapter
                        SetTransaction(adapterCaseManagement, oSQLTrans)
                        adapterCaseManagement.UpdateCaseManagementLastProposedAction(CboProposedAction.SelectedValue, Me.PK_CaseManagementID)
                        'Kalau level teratas dan sudah ngak ada lagi untuk current workflow step yang belum di approve
                        If Me.IsLevelWorkFlowTeratas(oSQLTrans) Then
                            If Not IsHaveOtherWorkFlowThatNotComplate(oCurrentWorkflowhistory.workflowstep, oSQLTrans) Then


                                'Keputusan status yang dipilih berdasarkan level prioritas paling tinggi(paling kecil dianggap paling tinggi 1-5) dari semua approver
                                ProposedStatusFinal = GetProposedStatusFinal(oCurrentWorkflowhistory.workflowstep, oSQLTrans)
                                Select Case ProposedStatusFinal
                                    Case ProposedAction.Dropped
                                        'insert ke mapcasemanagementworkflowhistory dgn eventtype workflowfinish
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.WorkFlowFinish, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, "", Nothing, True, strDescription, 0, 0)

                                        'update casemanagement set fk_casestastusid=3 (droped)
                                        adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.Dropped, Me.PK_CaseManagementID)


                                    Case ProposedAction.CaseMonitoring
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.CaseMonitoring, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, "", Nothing, False, strDescription, 0, 0)
                                        adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.UnderInvestigate, Me.PK_CaseManagementID)
                                        StrNextWorkflowStep = oCurrentWorkflowhistory.workflowstep


                                    Case ProposedAction.ConfirmAsSTR
                                        'insert ke mapcasemanagementworkflowhistory dgn eventtype workflowfinish
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.WorkFlowFinish, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, "", Nothing, True, strDescription, 0, 0)
                                        'update casemanagement set fk_casestastusid=4 (approved)
                                        adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.Approved, Me.PK_CaseManagementID)

                                    Case ProposedAction.ReAssign
                                        Dim intAccountownerpilihan As Integer = 0
                                        'TODO: Confirm ambil datanya sesuai ngak?
                                        'intAccountownerpilihan = GetPilihanAccontOwner(oCurrentWorkflowhistory.workflowstep, oSQLTrans)
                                        intAccountownerpilihan = CboAccountOwner.SelectedValue
                                        If intAccountownerpilihan > 0 Then
                                            ':Update accountownerid dengan accountowner, pic yang diselect
                                            Dim struserid As String = ""
                                            'Using adapteraccount As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.AccountOwnerUserMappingTableAdapter
                                            '    struserid = CType(adapteraccount.GetUserIDLowestLevelByAccountOwnerID(intAccountownerpilihan).Rows(0), AMLDAL.SettingWorkFlowCaseManagement.AccountOwnerUserMappingRow).UserID
                                            'End Using
                                            Using AdapterCaseManagementWorkflow As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter
                                                Dim CaseManagementWorkflowTable As New AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable
                                                CaseManagementWorkflowTable = AdapterCaseManagementWorkflow.GetSettingWorkFlowCaseManagementByAccountOwnerID(intAccountownerpilihan, Me.oRowCaseManagement.VIPCode, Me.oRowCaseManagement.InsiderCode, Me.oRowCaseManagement.Segment, Me.oRowCaseManagement.CaseDescription, oRowCaseManagement.SBU, oRowCaseManagement.SUBSBU, oRowCaseManagement.RM)
                                                If Not CaseManagementWorkflowTable Is Nothing Then
                                                    If CaseManagementWorkflowTable.Rows.Count > 0 Then
                                                        Dim CaseManagementWorkflowTableRows() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
                                                        CaseManagementWorkflowTableRows = CaseManagementWorkflowTable.Select("SerialNo=1 AND Paralel=1")
                                                        If Not CaseManagementWorkflowTableRows Is Nothing Then
                                                            If CaseManagementWorkflowTableRows.Length > 0 Then
                                                                Dim CaseManagementWorkflowTableRow As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
                                                                CaseManagementWorkflowTableRow = CaseManagementWorkflowTableRows(0)
                                                                If Not CaseManagementWorkflowTableRow.IsWorkflowStepNull Then
                                                                    StrNextWorkflowStep = CaseManagementWorkflowTableRow.WorkflowStep
                                                                End If
                                                                If Not CaseManagementWorkflowTableRow.IsApproversNull Then
                                                                    Dim ArrApprovers() As String = Nothing
                                                                    ArrApprovers = CaseManagementWorkflowTableRow.Approvers.Split("|"c)
                                                                    If Not ArrApprovers Is Nothing Then
                                                                        If ArrApprovers.Length > 0 Then
                                                                            struserid = Me.GetUserIDbyPK(ArrApprovers(0))
                                                                        End If
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End Using

                                            adapterCaseManagement.UpdateCaseManagementAccountOwner(CboAccountOwner.SelectedValue, struserid, Me.PK_CaseManagementID)
                                            adapterCaseManagement.UpdateCaseManagementWorkflowStepByPK(StrNextWorkflowStep, Me.PK_CaseManagementID)
                                            adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.Reassign, "", "", "", Nothing, True, "", Nothing, False)
                                            ':Insert ke mapcasemanagementworkflowhistory dengan eventtype=2 (task started) untuk accountowner yang baru.
                                        End If
                                        Dim oworkflowBaruSerial1() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkFlowStepBaruSerial1(CboAccountOwner.SelectedValue, oRowCaseManagement.VIPCode, oRowCaseManagement.InsiderCode, oRowCaseManagement.Segment, oRowCaseManagement.CaseDescription, oRowCaseManagement.SBU, oRowCaseManagement.SUBSBU, oRowCaseManagement.RM)
                                        If Not oworkflowBaruSerial1 Is Nothing AndAlso oworkflowBaruSerial1.Length > 0 Then
                                            For i As Integer = 0 To oworkflowBaruSerial1.Length - 1
                                                Dim strPKUserBaruSerial1 As String = oworkflowBaruSerial1(i).Approvers
                                                Dim arrPKUserBaruSerial1() As String = strPKUserBaruSerial1.Split("|")

                                                Dim strPKNotifyUser As String = oworkflowBaruSerial1(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")
                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""


                                                Dim strListUserBaruSerial1 As String = ""
                                                Dim strDescriptionBaruSerial1 As String = ""
                                                For j As Integer = 0 To arrPKUserBaruSerial1.Length - 1
                                                    strListUserBaruSerial1 += GetUserIDbyPK(arrPKUserBaruSerial1(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserBaruSerial1(j))
                                                    If j <> arrPKUserBaruSerial1.Length - 1 Then
                                                        strListUserBaruSerial1 += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oworkflowBaruSerial1(i).Pk_CMW_Id, oworkflowBaruSerial1(i).SerialNo)
                                                strEmailBody = GetEmailBody(oworkflowBaruSerial1(i).Pk_CMW_Id, oworkflowBaruSerial1(i).SerialNo)

                                                If oworkflowBaruSerial1.Length > 1 Then
                                                    strDescriptionBaruSerial1 = "Parallel " & i + 1
                                                Else
                                                    strDescriptionBaruSerial1 = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oworkflowBaruSerial1(i).WorkflowStep, strListUserBaruSerial1, "", Nothing, 0, strDescriptionBaruSerial1, 0, 0)
                                                SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next

                                        End If



                                    Case ProposedAction.RequestChange
                                        Dim oWorkFlowStepSebelumnya() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkflowStepSebelumnya(oCurrentWorkflowhistory.workflowstep)
                                        If Not oWorkFlowStepSebelumnya Is Nothing AndAlso oWorkFlowStepSebelumnya.Length > 0 Then
                                            If Not oWorkFlowStepSebelumnya(0).IsWorkflowStepNull Then
                                                StrNextWorkflowStep = oWorkFlowStepSebelumnya(0).WorkflowStep
                                            End If
                                            For i As Integer = 0 To oWorkFlowStepSebelumnya.Length - 1
                                                Dim strPKUserSebelumnya As String = oWorkFlowStepSebelumnya(i).Approvers
                                                Dim arrPKUserSebelumnya() As String = strPKUserSebelumnya.Split("|")

                                                Dim strPKNotifyUser As String = oWorkFlowStepSebelumnya(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")


                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""

                                                Dim strListUserStepSebelumnya As String = ""
                                                Dim strDescriptionStepSebelumnya As String = ""
                                                For j As Integer = 0 To arrPKUserSebelumnya.Length - 1
                                                    strListUserStepSebelumnya += GetUserIDbyPK(arrPKUserSebelumnya(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserSebelumnya(j))
                                                    If j <> arrPKUserSebelumnya.Length - 1 Then
                                                        strListUserStepSebelumnya += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)
                                                strEmailBody = GetEmailBody(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)

                                                Dim strDescriptioinStepSebelumnya As String
                                                If oWorkFlowStepSebelumnya.Length > 1 Then
                                                    strDescriptioinStepSebelumnya = "Parallel " & i + 1
                                                Else
                                                    strDescriptioinStepSebelumnya = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oWorkFlowStepSebelumnya(i).WorkflowStep, strListUserStepSebelumnya, "", Nothing, 0, strDescriptioinStepSebelumnya, 0, 0)
                                                SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next
                                            adapterCaseManagement.UpdateCaseManagementWorkflowStepByPK(StrNextWorkflowStep, Me.PK_CaseManagementID)
                                        End If
                                End Select
                                ':update mapcasemanagementworkflowhistory set baktiveworkflow menjadi false untuk record yang eventtypenya complate untuk workflow step parameter

                                adapter.UpdateMapCaseManagementWorkflowHistoryBAktifWorkflow(Eventtype.TaskComplated, oCurrentWorkflowhistory.workflowstep, Me.PK_CaseManagementID)
                            End If
                        Else
                            If Not IsHaveOtherWorkFlowThatNotComplate(oCurrentWorkflowhistory.workflowstep, oSQLTrans) Then

                                'Bukan level teratas maka buat task baru untuk level berikutnya.
                                ProposedStatusFinal = GetProposedStatusFinal(oCurrentWorkflowhistory.workflowstep, oSQLTrans)
                                Select Case ProposedStatusFinal
                                    Case ProposedAction.ConfirmAsSTR, ProposedAction.Dropped, ProposedAction.RequestAssignment
                                        ': insert ke mapcasemanagementworkflowhistory dengan eventtype =2(taskstarted) untuk user yang terdaftar dalam setting level berikutnya

                                        Dim oWorkFlowStepBerikutnya() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkflowStepBerikutnya(oCurrentWorkflowhistory.workflowstep)
                                        If Not oWorkFlowStepBerikutnya Is Nothing AndAlso oWorkFlowStepBerikutnya.Length > 0 Then

                                            If Not oWorkFlowStepBerikutnya(0).IsWorkflowStepNull Then
                                                StrNextWorkflowStep = oWorkFlowStepBerikutnya(0).WorkflowStep
                                            End If
                                            For i As Integer = 0 To oWorkFlowStepBerikutnya.Length - 1
                                                Dim strPKUserStepBerikutnya As String = oWorkFlowStepBerikutnya(i).Approvers
                                                Dim arrPKUserBerikutnya() As String = strPKUserStepBerikutnya.Split("|")

                                                Dim strPKNotifyUser As String = oWorkFlowStepBerikutnya(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")

                                                Dim strListuserStepBerikutnya As String = ""
                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""

                                                For j As Integer = 0 To arrPKUserBerikutnya.Length - 1
                                                    strListuserStepBerikutnya += GetUserIDbyPK(arrPKUserBerikutnya(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserBerikutnya(j))
                                                    If j <> arrPKUserBerikutnya.Length - 1 Then
                                                        strListuserStepBerikutnya += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oWorkFlowStepBerikutnya(i).Pk_CMW_Id, oWorkFlowStepBerikutnya(i).SerialNo)
                                                strEmailBody = GetEmailBody(oWorkFlowStepBerikutnya(i).Pk_CMW_Id, oWorkFlowStepBerikutnya(i).SerialNo)

                                                Dim strDescriptionStepBerikutnya As String
                                                If oWorkFlowStepBerikutnya.Length > 1 Then
                                                    strDescriptionStepBerikutnya = "Parallel " & i + 1
                                                Else
                                                    strDescriptionStepBerikutnya = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oWorkFlowStepBerikutnya(i).WorkflowStep, strListuserStepBerikutnya, "", Nothing, 0, strDescriptionStepBerikutnya, 0, 0)
                                                SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next

                                        End If

                                    Case ProposedAction.CaseMonitoring
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oCurrentWorkflowhistory.workflowstep, oCurrentWorkflowhistory.UserID, "", Nothing, 0, "", 0, 0)
                                        StrNextWorkflowStep = oCurrentWorkflowhistory.workflowstep

                                    Case ProposedAction.RequestChange

                                        ':insert ke mapcasemanagementworkflowhistory dengan eventype=2(taskstarted) untuk user yang terdaftar dalam setting level sebelumnnya
                                        Dim oWorkFlowStepSebelumnya() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkflowStepSebelumnya(oCurrentWorkflowhistory.workflowstep)

                                        If Not oWorkFlowStepSebelumnya Is Nothing AndAlso oWorkFlowStepSebelumnya.Length > 0 Then
                                            If Not oWorkFlowStepSebelumnya(0).IsWorkflowStepNull Then
                                                StrNextWorkflowStep = oWorkFlowStepSebelumnya(0).WorkflowStep
                                            End If

                                            For i As Integer = 0 To oWorkFlowStepSebelumnya.Length - 1
                                                Dim strPKUserSebelumnya As String = oWorkFlowStepSebelumnya(i).Approvers
                                                Dim arrPKUserSebelumnya() As String = strPKUserSebelumnya.Split("|")

                                                Dim strPKNotifyUser As String = oWorkFlowStepSebelumnya(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")


                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""

                                                Dim strListUserStepSebelumnya As String = ""
                                                Dim strDescriptionStepSebelumnya As String = ""
                                                For j As Integer = 0 To arrPKUserSebelumnya.Length - 1
                                                    strListUserStepSebelumnya += GetUserIDbyPK(arrPKUserSebelumnya(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserSebelumnya(j))
                                                    If j <> arrPKUserSebelumnya.Length - 1 Then
                                                        strListUserStepSebelumnya += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)
                                                strEmailBody = GetEmailBody(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)

                                                Dim strDescriptioinStepSebelumnya As String
                                                If oWorkFlowStepSebelumnya.Length > 1 Then
                                                    strDescriptioinStepSebelumnya = "Parallel " & i + 1
                                                Else
                                                    strDescriptioinStepSebelumnya = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oWorkFlowStepSebelumnya(i).WorkflowStep, strListUserStepSebelumnya, "", Nothing, 0, strDescriptioinStepSebelumnya, 0, 0)
                                                SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next
                                        End If
                                End Select
                                adapter.UpdateMapCaseManagementWorkflowHistoryBAktifWorkflow(Eventtype.TaskComplated, oCurrentWorkflowhistory.workflowstep, Me.PK_CaseManagementID)
                                'Else
                                adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.UnderInvestigate, Me.PK_CaseManagementID)
                                adapterCaseManagement.UpdateCaseManagementWorkflowStepByPK(StrNextWorkflowStep, Me.PK_CaseManagementID)
                            End If
                        End If
                        adapterCaseManagement.UpdateCaseManagementLastUpdatedByPK(DateTime.Now(), Me.PK_CaseManagementID)
                    End Using

                    'update lastrun dari workflow
                    Using adapterCase As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementWorkflowTableAdapter
                        SetTransaction(adapterCase, oSQLTrans)
                        adapterCase.UpdateMapCaseManagementWorkflowLastRun(Now, Me.PK_CaseManagementID)
                    End Using
                    oSQLTrans.Commit()

                    AMLBLL.CaseManagementBLL.UpdateAging(Me.PK_CaseManagementID)



                    UnloadSession()
                    If Me.IsHaveRighttoInputInvestigationNotoesResponse Then
                        Me.Panel1.Visible = True
                        BindListAttachment()
                    Else
                        Me.Panel1.Visible = False
                    End If
                End Using
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
            End If

        End Try
    End Sub
    Protected Sub MaxPK_MapCaseManagementWorkflowHistoryID()
        Session("ObjPK_MapCaseManagementWorkflowHistoryID") = Nothing
        Dim connectionString As String = WebConfigurationManager.ConnectionStrings("AMLConnectionString").ConnectionString
        Dim con As New SqlConnection(connectionString)
        Try
            Dim QueryString As String
            QueryString = ("SELECT MAX(PK_MapCaseManagementWorkflowHistoryID) FROM MapCaseManagementWorkflowHistory ")
            QueryString = QueryString + (" WHERE FK_CaseManagementID = " & PK_CaseManagementID)
            Dim cmd As New SqlCommand()

            cmd.Connection = con
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings("SQLCommandTimeout")
            cmd.CommandText = QueryString
            Using ds As DataSet = DataRepository.Provider.ExecuteDataSet(CommandType.Text, cmd.CommandText)
                Session("ObjPK_MapCaseManagementWorkflowHistoryID") = ds.Tables(0).Rows(0)(0)
            End Using
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        Finally
            con.Open()
            con.Close()
        End Try
    End Sub
    Protected Sub DoneProposedFlowBeforeRequestChange()
        Dim oSQLTrans As SqlTransaction = Nothing
        Dim oCurrentWorkflowhistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim ProposedStatusFinal As Integer
        Dim strDescription As String = ""
        Dim StrNextWorkflowStep As String = ""

        Try
            If Page.IsValid Then
                Using adapter As New AMLDAL.CaseManagementTableAdapters.SelectMapCaseManagementWorkflowHistoryTableAdapter
                    oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                    'Selesaikan satu workflow yang sedang berjalan

                    MaxPK_MapCaseManagementWorkflowHistoryID()
                    adapter.UpdateMapCaseManagementWorkfloHistoryBComplate(True, Session("ObjPK_MapCaseManagementWorkflowHistoryID"))
                    adapter.usp_DeleteTaskDraftMapCaseManagementWorkFlowHistory(Me.PK_CaseManagementID)

                    Dim oRowWorkflowNotComplete() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
                    oRowWorkflowNotComplete = oRowCasemanagementWorkFlowHistory.Select("PK_MapCaseManagementWorkflowHistoryID = " & Session("ObjPK_MapCaseManagementWorkflowHistoryID"))
                    oCurrentWorkflowhistory = oRowWorkflowNotComplete(0)
                    adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskComplated, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, Me.TextInvestigationNotes.Text, CboProposedAction.SelectedValue, True, strDescription, oCurrentWorkflowhistory.FK_AccountOwnerID, True)


                    Using adapterCaseManagement As New AMLDAL.CaseManagementTableAdapters.SelectCaseManagementTableAdapter
                        SetTransaction(adapterCaseManagement, oSQLTrans)
                        adapterCaseManagement.UpdateCaseManagementLastProposedAction(CboProposedAction.SelectedValue, Me.PK_CaseManagementID)
                        'Kalau level teratas dan sudah ngak ada lagi untuk current workflow step yang belum di approve
                        If Me.IsLevelWorkFlowTeratas(oSQLTrans) Then
                            If Not IsHaveOtherWorkFlowThatNotComplate(oCurrentWorkflowhistory.workflowstep, oSQLTrans) Then


                                'Keputusan status yang dipilih berdasarkan level prioritas paling tinggi(paling kecil dianggap paling tinggi 1-5) dari semua approver
                                ProposedStatusFinal = GetProposedStatusFinal(oCurrentWorkflowhistory.workflowstep, oSQLTrans)
                                Select Case ProposedStatusFinal
                                    Case ProposedAction.Dropped
                                        'insert ke mapcasemanagementworkflowhistory dgn eventtype workflowfinish
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.WorkFlowFinish, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, "", Nothing, True, strDescription, 0, 0)

                                        'update casemanagement set fk_casestastusid=3 (droped)
                                        adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.Dropped, Me.PK_CaseManagementID)


                                    Case ProposedAction.CaseMonitoring
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.CaseMonitoring, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, "", Nothing, False, strDescription, 0, 0)
                                        adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.UnderInvestigate, Me.PK_CaseManagementID)
                                        StrNextWorkflowStep = oCurrentWorkflowhistory.workflowstep


                                    Case ProposedAction.ConfirmAsSTR
                                        'insert ke mapcasemanagementworkflowhistory dgn eventtype workflowfinish
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.WorkFlowFinish, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, "", Nothing, True, strDescription, 0, 0)
                                        'update casemanagement set fk_casestastusid=4 (approved)
                                        adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.Approved, Me.PK_CaseManagementID)

                                    Case ProposedAction.ReAssign
                                        Dim intAccountownerpilihan As Integer = 0
                                        'TODO: Confirm ambil datanya sesuai ngak?
                                        'intAccountownerpilihan = GetPilihanAccontOwner(oCurrentWorkflowhistory.workflowstep, oSQLTrans)
                                        intAccountownerpilihan = CboAccountOwner.SelectedValue
                                        If intAccountownerpilihan > 0 Then
                                            ':Update accountownerid dengan accountowner, pic yang diselect
                                            Dim struserid As String = ""
                                            'Using adapteraccount As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.AccountOwnerUserMappingTableAdapter
                                            '    struserid = CType(adapteraccount.GetUserIDLowestLevelByAccountOwnerID(intAccountownerpilihan).Rows(0), AMLDAL.SettingWorkFlowCaseManagement.AccountOwnerUserMappingRow).UserID
                                            'End Using
                                            Using AdapterCaseManagementWorkflow As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter
                                                Dim CaseManagementWorkflowTable As New AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable
                                                CaseManagementWorkflowTable = AdapterCaseManagementWorkflow.GetSettingWorkFlowCaseManagementByAccountOwnerID(intAccountownerpilihan, Me.oRowCaseManagement.VIPCode, Me.oRowCaseManagement.InsiderCode, Me.oRowCaseManagement.Segment, Me.oRowCaseManagement.CaseDescription, Me.oRowCaseManagement.SBU, Me.oRowCaseManagement.SUBSBU, Me.oRowCaseManagement.RM)
                                                If Not CaseManagementWorkflowTable Is Nothing Then
                                                    If CaseManagementWorkflowTable.Rows.Count > 0 Then
                                                        Dim CaseManagementWorkflowTableRows() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
                                                        CaseManagementWorkflowTableRows = CaseManagementWorkflowTable.Select("SerialNo=1 AND Paralel=1")
                                                        If Not CaseManagementWorkflowTableRows Is Nothing Then
                                                            If CaseManagementWorkflowTableRows.Length > 0 Then
                                                                Dim CaseManagementWorkflowTableRow As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
                                                                CaseManagementWorkflowTableRow = CaseManagementWorkflowTableRows(0)
                                                                If Not CaseManagementWorkflowTableRow.IsWorkflowStepNull Then
                                                                    StrNextWorkflowStep = CaseManagementWorkflowTableRow.WorkflowStep
                                                                End If
                                                                If Not CaseManagementWorkflowTableRow.IsApproversNull Then
                                                                    Dim ArrApprovers() As String = Nothing
                                                                    ArrApprovers = CaseManagementWorkflowTableRow.Approvers.Split("|"c)
                                                                    If Not ArrApprovers Is Nothing Then
                                                                        If ArrApprovers.Length > 0 Then
                                                                            struserid = Me.GetUserIDbyPK(ArrApprovers(0))
                                                                        End If
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End Using

                                            adapterCaseManagement.UpdateCaseManagementAccountOwner(CboAccountOwner.SelectedValue, struserid, Me.PK_CaseManagementID)
                                            adapterCaseManagement.UpdateCaseManagementWorkflowStepByPK(StrNextWorkflowStep, Me.PK_CaseManagementID)
                                            adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.Reassign, "", "", "", Nothing, True, "", Nothing, False)
                                            ':Insert ke mapcasemanagementworkflowhistory dengan eventtype=2 (task started) untuk accountowner yang baru.
                                        End If
                                        Dim oworkflowBaruSerial1() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkFlowStepBaruSerial1(CboAccountOwner.SelectedValue, oRowCaseManagement.VIPCode, oRowCaseManagement.InsiderCode, oRowCaseManagement.Segment, oRowCaseManagement.CaseDescription, oRowCaseManagement.SBU, oRowCaseManagement.SUBSBU, oRowCaseManagement.RM)
                                        If Not oworkflowBaruSerial1 Is Nothing AndAlso oworkflowBaruSerial1.Length > 0 Then
                                            For i As Integer = 0 To oworkflowBaruSerial1.Length - 1
                                                Dim strPKUserBaruSerial1 As String = oworkflowBaruSerial1(i).Approvers
                                                Dim arrPKUserBaruSerial1() As String = strPKUserBaruSerial1.Split("|")

                                                Dim strPKNotifyUser As String = oworkflowBaruSerial1(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")
                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""


                                                Dim strListUserBaruSerial1 As String = ""
                                                Dim strDescriptionBaruSerial1 As String = ""
                                                For j As Integer = 0 To arrPKUserBaruSerial1.Length - 1
                                                    strListUserBaruSerial1 += GetUserIDbyPK(arrPKUserBaruSerial1(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserBaruSerial1(j))
                                                    If j <> arrPKUserBaruSerial1.Length - 1 Then
                                                        strListUserBaruSerial1 += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oworkflowBaruSerial1(i).Pk_CMW_Id, oworkflowBaruSerial1(i).SerialNo)
                                                strEmailBody = GetEmailBody(oworkflowBaruSerial1(i).Pk_CMW_Id, oworkflowBaruSerial1(i).SerialNo)

                                                If oworkflowBaruSerial1.Length > 1 Then
                                                    strDescriptionBaruSerial1 = "Parallel " & i + 1
                                                Else
                                                    strDescriptionBaruSerial1 = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oworkflowBaruSerial1(i).WorkflowStep, strListUserBaruSerial1, "", Nothing, 0, strDescriptionBaruSerial1, 0, 0)
                                                SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next

                                        End If



                                    Case ProposedAction.RequestChange
                                        Dim oWorkFlowStepSebelumnya() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkflowStepSebelumnya(oCurrentWorkflowhistory.workflowstep)
                                        If Not oWorkFlowStepSebelumnya Is Nothing AndAlso oWorkFlowStepSebelumnya.Length > 0 Then
                                            If Not oWorkFlowStepSebelumnya(0).IsWorkflowStepNull Then
                                                StrNextWorkflowStep = oWorkFlowStepSebelumnya(0).WorkflowStep
                                            End If
                                            For i As Integer = 0 To oWorkFlowStepSebelumnya.Length - 1
                                                Dim strPKUserSebelumnya As String = oWorkFlowStepSebelumnya(i).Approvers
                                                Dim arrPKUserSebelumnya() As String = strPKUserSebelumnya.Split("|")

                                                Dim strPKNotifyUser As String = oWorkFlowStepSebelumnya(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")


                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""

                                                Dim strListUserStepSebelumnya As String = ""
                                                Dim strDescriptionStepSebelumnya As String = ""
                                                For j As Integer = 0 To arrPKUserSebelumnya.Length - 1
                                                    strListUserStepSebelumnya += GetUserIDbyPK(arrPKUserSebelumnya(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserSebelumnya(j))
                                                    If j <> arrPKUserSebelumnya.Length - 1 Then
                                                        strListUserStepSebelumnya += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)
                                                strEmailBody = GetEmailBody(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)

                                                Dim strDescriptioinStepSebelumnya As String
                                                If oWorkFlowStepSebelumnya.Length > 1 Then
                                                    strDescriptioinStepSebelumnya = "Parallel " & i + 1
                                                Else
                                                    strDescriptioinStepSebelumnya = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oWorkFlowStepSebelumnya(i).WorkflowStep, strListUserStepSebelumnya, "", Nothing, 0, strDescriptioinStepSebelumnya, 0, 0)
                                                SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next
                                            adapterCaseManagement.UpdateCaseManagementWorkflowStepByPK(StrNextWorkflowStep, Me.PK_CaseManagementID)
                                        End If
                                End Select
                                ':update mapcasemanagementworkflowhistory set baktiveworkflow menjadi false untuk record yang eventtypenya complate untuk workflow step parameter

                                adapter.UpdateMapCaseManagementWorkflowHistoryBAktifWorkflow(Eventtype.TaskComplated, oCurrentWorkflowhistory.workflowstep, Me.PK_CaseManagementID)
                            End If
                        Else
                            If Not IsHaveOtherWorkFlowThatNotComplate(oCurrentWorkflowhistory.workflowstep, oSQLTrans) Then

                                'Bukan level teratas maka buat task baru untuk level berikutnya.
                                ProposedStatusFinal = GetProposedStatusFinal(oCurrentWorkflowhistory.workflowstep, oSQLTrans)
                                Select Case ProposedStatusFinal
                                    Case ProposedAction.ConfirmAsSTR, ProposedAction.Dropped, ProposedAction.RequestAssignment
                                        ': insert ke mapcasemanagementworkflowhistory dengan eventtype =2(taskstarted) untuk user yang terdaftar dalam setting level berikutnya

                                        Dim oWorkFlowStepBerikutnya() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkflowStepBerikutnya(oCurrentWorkflowhistory.workflowstep)
                                        If Not oWorkFlowStepBerikutnya Is Nothing AndAlso oWorkFlowStepBerikutnya.Length > 0 Then

                                            If Not oWorkFlowStepBerikutnya(0).IsWorkflowStepNull Then
                                                StrNextWorkflowStep = oWorkFlowStepBerikutnya(0).WorkflowStep
                                            End If
                                            For i As Integer = 0 To oWorkFlowStepBerikutnya.Length - 1
                                                Dim strPKUserStepBerikutnya As String = oWorkFlowStepBerikutnya(i).Approvers
                                                Dim arrPKUserBerikutnya() As String = strPKUserStepBerikutnya.Split("|")

                                                Dim strPKNotifyUser As String = oWorkFlowStepBerikutnya(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")

                                                Dim strListuserStepBerikutnya As String = ""
                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""

                                                For j As Integer = 0 To arrPKUserBerikutnya.Length - 1
                                                    strListuserStepBerikutnya += GetUserIDbyPK(arrPKUserBerikutnya(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserBerikutnya(j))
                                                    If j <> arrPKUserBerikutnya.Length - 1 Then
                                                        strListuserStepBerikutnya += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oWorkFlowStepBerikutnya(i).Pk_CMW_Id, oWorkFlowStepBerikutnya(i).SerialNo)
                                                strEmailBody = GetEmailBody(oWorkFlowStepBerikutnya(i).Pk_CMW_Id, oWorkFlowStepBerikutnya(i).SerialNo)

                                                Dim strDescriptionStepBerikutnya As String
                                                If oWorkFlowStepBerikutnya.Length > 1 Then
                                                    strDescriptionStepBerikutnya = "Parallel " & i + 1
                                                Else
                                                    strDescriptionStepBerikutnya = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oWorkFlowStepBerikutnya(i).WorkflowStep, strListuserStepBerikutnya, "", Nothing, 0, strDescriptionStepBerikutnya, 0, 0)
                                                'SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next

                                        End If

                                    Case ProposedAction.CaseMonitoring
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oCurrentWorkflowhistory.workflowstep, oCurrentWorkflowhistory.UserID, "", Nothing, 0, "", 0, 0)
                                        StrNextWorkflowStep = oCurrentWorkflowhistory.workflowstep

                                    Case ProposedAction.RequestChange

                                        ':insert ke mapcasemanagementworkflowhistory dengan eventype=2(taskstarted) untuk user yang terdaftar dalam setting level sebelumnnya
                                        Dim oWorkFlowStepSebelumnya() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkflowStepSebelumnya(oCurrentWorkflowhistory.workflowstep)

                                        If Not oWorkFlowStepSebelumnya Is Nothing AndAlso oWorkFlowStepSebelumnya.Length > 0 Then
                                            If Not oWorkFlowStepSebelumnya(0).IsWorkflowStepNull Then
                                                StrNextWorkflowStep = oWorkFlowStepSebelumnya(0).WorkflowStep
                                            End If

                                            For i As Integer = 0 To oWorkFlowStepSebelumnya.Length - 1
                                                Dim strPKUserSebelumnya As String = oWorkFlowStepSebelumnya(i).Approvers
                                                Dim arrPKUserSebelumnya() As String = strPKUserSebelumnya.Split("|")

                                                Dim strPKNotifyUser As String = oWorkFlowStepSebelumnya(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")


                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""

                                                Dim strListUserStepSebelumnya As String = ""
                                                Dim strDescriptionStepSebelumnya As String = ""
                                                For j As Integer = 0 To arrPKUserSebelumnya.Length - 1
                                                    strListUserStepSebelumnya += GetUserIDbyPK(arrPKUserSebelumnya(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserSebelumnya(j))
                                                    If j <> arrPKUserSebelumnya.Length - 1 Then
                                                        strListUserStepSebelumnya += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)
                                                strEmailBody = GetEmailBody(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)

                                                Dim strDescriptioinStepSebelumnya As String
                                                If oWorkFlowStepSebelumnya.Length > 1 Then
                                                    strDescriptioinStepSebelumnya = "Parallel " & i + 1
                                                Else
                                                    strDescriptioinStepSebelumnya = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oWorkFlowStepSebelumnya(i).WorkflowStep, strListUserStepSebelumnya, "", Nothing, 0, strDescriptioinStepSebelumnya, 0, 0)
                                                SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next
                                        End If
                                End Select
                                adapter.UpdateMapCaseManagementWorkflowHistoryBAktifWorkflow(Eventtype.TaskComplated, oCurrentWorkflowhistory.workflowstep, Me.PK_CaseManagementID)
                                'Else
                                adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.UnderInvestigate, Me.PK_CaseManagementID)
                                adapterCaseManagement.UpdateCaseManagementWorkflowStepByPK(StrNextWorkflowStep, Me.PK_CaseManagementID)
                            End If
                        End If
                        adapterCaseManagement.UpdateCaseManagementLastUpdatedByPK(DateTime.Now(), Me.PK_CaseManagementID)
                    End Using

                    'update lastrun dari workflow
                    Using adapterCase As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementWorkflowTableAdapter
                        SetTransaction(adapterCase, oSQLTrans)
                        adapterCase.UpdateMapCaseManagementWorkflowLastRun(Now, Me.PK_CaseManagementID)
                    End Using
                    oSQLTrans.Commit()

                    AMLBLL.CaseManagementBLL.UpdateAging(Me.PK_CaseManagementID)



                    UnloadSession()
                    If Me.IsHaveRighttoInputInvestigationNotoesResponse Then
                        Me.Panel1.Visible = True
                        BindListAttachment()
                    Else
                        Me.Panel1.Visible = False
                    End If
                End Using
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
            End If

        End Try
    End Sub
    Protected Sub DoneProposedFlowBefore()
        Dim oSQLTrans As SqlTransaction = Nothing
        Dim oCurrentWorkflowhistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim ProposedStatusFinal As Integer
        Dim strDescription As String = ""
        Dim StrNextWorkflowStep As String = ""
        
        Try
            If Page.IsValid Then
                Using adapter As New AMLDAL.CaseManagementTableAdapters.SelectMapCaseManagementWorkflowHistoryTableAdapter
                    oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                    'Selesaikan satu workflow yang sedang berjalan

                    MaxPK_MapCaseManagementWorkflowHistoryID()
                    adapter.UpdateMapCaseManagementWorkfloHistoryBComplate(True, Session("ObjPK_MapCaseManagementWorkflowHistoryID"))
                    adapter.usp_DeleteTaskDraftMapCaseManagementWorkFlowHistory(Me.PK_CaseManagementID)
                    
                    Dim oRowWorkflowNotComplete() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
                    oRowWorkflowNotComplete = oRowCasemanagementWorkFlowHistory.Select("PK_MapCaseManagementWorkflowHistoryID = " & Session("ObjPK_MapCaseManagementWorkflowHistoryID"))
                    oCurrentWorkflowhistory = oRowWorkflowNotComplete(0)
                    adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskComplated, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, "By System", CboProposedAction.SelectedValue, True, strDescription, oCurrentWorkflowhistory.FK_AccountOwnerID, True)


                    Using adapterCaseManagement As New AMLDAL.CaseManagementTableAdapters.SelectCaseManagementTableAdapter
                        SetTransaction(adapterCaseManagement, oSQLTrans)
                        adapterCaseManagement.UpdateCaseManagementLastProposedAction(CboProposedAction.SelectedValue, Me.PK_CaseManagementID)
                        'Kalau level teratas dan sudah ngak ada lagi untuk current workflow step yang belum di approve
                        If Me.IsLevelWorkFlowTeratas(oSQLTrans) Then
                            If Not IsHaveOtherWorkFlowThatNotComplate(oCurrentWorkflowhistory.workflowstep, oSQLTrans) Then


                                'Keputusan status yang dipilih berdasarkan level prioritas paling tinggi(paling kecil dianggap paling tinggi 1-5) dari semua approver
                                ProposedStatusFinal = GetProposedStatusFinal(oCurrentWorkflowhistory.workflowstep, oSQLTrans)
                                Select Case ProposedStatusFinal
                                    Case ProposedAction.Dropped
                                        'insert ke mapcasemanagementworkflowhistory dgn eventtype workflowfinish
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.WorkFlowFinish, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, "", Nothing, True, strDescription, 0, 0)

                                        'update casemanagement set fk_casestastusid=3 (droped)
                                        adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.Dropped, Me.PK_CaseManagementID)


                                    Case ProposedAction.CaseMonitoring
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.CaseMonitoring, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, "", Nothing, False, strDescription, 0, 0)
                                        adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.UnderInvestigate, Me.PK_CaseManagementID)
                                        StrNextWorkflowStep = oCurrentWorkflowhistory.workflowstep

                                    Case ProposedAction.ConfirmAsSTR
                                        'insert ke mapcasemanagementworkflowhistory dgn eventtype workflowfinish
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.WorkFlowFinish, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, "", Nothing, True, strDescription, 0, 0)
                                        'update casemanagement set fk_casestastusid=4 (approved)
                                        adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.Approved, Me.PK_CaseManagementID)

                                    Case ProposedAction.ReAssign
                                        Dim intAccountownerpilihan As Integer = 0
                                        'TODO: Confirm ambil datanya sesuai ngak?
                                        'intAccountownerpilihan = GetPilihanAccontOwner(oCurrentWorkflowhistory.workflowstep, oSQLTrans)
                                        intAccountownerpilihan = CboAccountOwner.SelectedValue
                                        If intAccountownerpilihan > 0 Then
                                            ':Update accountownerid dengan accountowner, pic yang diselect
                                            Dim struserid As String = ""
                                            'Using adapteraccount As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.AccountOwnerUserMappingTableAdapter
                                            '    struserid = CType(adapteraccount.GetUserIDLowestLevelByAccountOwnerID(intAccountownerpilihan).Rows(0), AMLDAL.SettingWorkFlowCaseManagement.AccountOwnerUserMappingRow).UserID
                                            'End Using
                                            Using AdapterCaseManagementWorkflow As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter
                                                Dim CaseManagementWorkflowTable As New AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable
                                                CaseManagementWorkflowTable = AdapterCaseManagementWorkflow.GetSettingWorkFlowCaseManagementByAccountOwnerID(intAccountownerpilihan, Me.oRowCaseManagement.VIPCode, Me.oRowCaseManagement.InsiderCode, Me.oRowCaseManagement.Segment, Me.oRowCaseManagement.CaseDescription, Me.oRowCaseManagement.SBU, Me.oRowCaseManagement.SUBSBU, Me.oRowCaseManagement.RM)
                                                If Not CaseManagementWorkflowTable Is Nothing Then
                                                    If CaseManagementWorkflowTable.Rows.Count > 0 Then
                                                        Dim CaseManagementWorkflowTableRows() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
                                                        CaseManagementWorkflowTableRows = CaseManagementWorkflowTable.Select("SerialNo=1 AND Paralel=1")
                                                        If Not CaseManagementWorkflowTableRows Is Nothing Then
                                                            If CaseManagementWorkflowTableRows.Length > 0 Then
                                                                Dim CaseManagementWorkflowTableRow As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
                                                                CaseManagementWorkflowTableRow = CaseManagementWorkflowTableRows(0)
                                                                If Not CaseManagementWorkflowTableRow.IsWorkflowStepNull Then
                                                                    StrNextWorkflowStep = CaseManagementWorkflowTableRow.WorkflowStep
                                                                End If
                                                                If Not CaseManagementWorkflowTableRow.IsApproversNull Then
                                                                    Dim ArrApprovers() As String = Nothing
                                                                    ArrApprovers = CaseManagementWorkflowTableRow.Approvers.Split("|"c)
                                                                    If Not ArrApprovers Is Nothing Then
                                                                        If ArrApprovers.Length > 0 Then
                                                                            struserid = Me.GetUserIDbyPK(ArrApprovers(0))
                                                                        End If
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End Using

                                            adapterCaseManagement.UpdateCaseManagementAccountOwner(CboAccountOwner.SelectedValue, struserid, Me.PK_CaseManagementID)
                                            adapterCaseManagement.UpdateCaseManagementWorkflowStepByPK(StrNextWorkflowStep, Me.PK_CaseManagementID)
                                            adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.Reassign, "", "", "", Nothing, True, "", Nothing, False)
                                            ':Insert ke mapcasemanagementworkflowhistory dengan eventtype=2 (task started) untuk accountowner yang baru.
                                        End If
                                        Dim oworkflowBaruSerial1() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkFlowStepBaruSerial1(CboAccountOwner.SelectedValue, oRowCaseManagement.VIPCode, oRowCaseManagement.InsiderCode, oRowCaseManagement.Segment, oRowCaseManagement.CaseDescription, oRowCaseManagement.SBU, oRowCaseManagement.SUBSBU, oRowCaseManagement.RM)
                                        If Not oworkflowBaruSerial1 Is Nothing AndAlso oworkflowBaruSerial1.Length > 0 Then
                                            For i As Integer = 0 To oworkflowBaruSerial1.Length - 1
                                                Dim strPKUserBaruSerial1 As String = oworkflowBaruSerial1(i).Approvers
                                                Dim arrPKUserBaruSerial1() As String = strPKUserBaruSerial1.Split("|")

                                                Dim strPKNotifyUser As String = oworkflowBaruSerial1(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")
                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""


                                                Dim strListUserBaruSerial1 As String = ""
                                                Dim strDescriptionBaruSerial1 As String = ""
                                                For j As Integer = 0 To arrPKUserBaruSerial1.Length - 1
                                                    strListUserBaruSerial1 += GetUserIDbyPK(arrPKUserBaruSerial1(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserBaruSerial1(j))
                                                    If j <> arrPKUserBaruSerial1.Length - 1 Then
                                                        strListUserBaruSerial1 += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oworkflowBaruSerial1(i).Pk_CMW_Id, oworkflowBaruSerial1(i).SerialNo)
                                                strEmailBody = GetEmailBody(oworkflowBaruSerial1(i).Pk_CMW_Id, oworkflowBaruSerial1(i).SerialNo)

                                                If oworkflowBaruSerial1.Length > 1 Then
                                                    strDescriptionBaruSerial1 = "Parallel " & i + 1
                                                Else
                                                    strDescriptionBaruSerial1 = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oworkflowBaruSerial1(i).WorkflowStep, strListUserBaruSerial1, "", Nothing, 0, strDescriptionBaruSerial1, 0, 0)
                                                SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next

                                        End If



                                    Case ProposedAction.RequestChange
                                        Dim oWorkFlowStepSebelumnya() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkflowStepSebelumnya(oCurrentWorkflowhistory.workflowstep)
                                        If Not oWorkFlowStepSebelumnya Is Nothing AndAlso oWorkFlowStepSebelumnya.Length > 0 Then
                                            If Not oWorkFlowStepSebelumnya(0).IsWorkflowStepNull Then
                                                StrNextWorkflowStep = oWorkFlowStepSebelumnya(0).WorkflowStep
                                            End If
                                            For i As Integer = 0 To oWorkFlowStepSebelumnya.Length - 1
                                                Dim strPKUserSebelumnya As String = oWorkFlowStepSebelumnya(i).Approvers
                                                Dim arrPKUserSebelumnya() As String = strPKUserSebelumnya.Split("|")

                                                Dim strPKNotifyUser As String = oWorkFlowStepSebelumnya(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")


                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""

                                                Dim strListUserStepSebelumnya As String = ""
                                                Dim strDescriptionStepSebelumnya As String = ""
                                                For j As Integer = 0 To arrPKUserSebelumnya.Length - 1
                                                    strListUserStepSebelumnya += GetUserIDbyPK(arrPKUserSebelumnya(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserSebelumnya(j))
                                                    If j <> arrPKUserSebelumnya.Length - 1 Then
                                                        strListUserStepSebelumnya += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)
                                                strEmailBody = GetEmailBody(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)

                                                Dim strDescriptioinStepSebelumnya As String
                                                If oWorkFlowStepSebelumnya.Length > 1 Then
                                                    strDescriptioinStepSebelumnya = "Parallel " & i + 1
                                                Else
                                                    strDescriptioinStepSebelumnya = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oWorkFlowStepSebelumnya(i).WorkflowStep, strListUserStepSebelumnya, "", Nothing, 0, strDescriptioinStepSebelumnya, 0, 0)
                                                SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next
                                            adapterCaseManagement.UpdateCaseManagementWorkflowStepByPK(StrNextWorkflowStep, Me.PK_CaseManagementID)
                                        End If
                                End Select
                                ':update mapcasemanagementworkflowhistory set baktiveworkflow menjadi false untuk record yang eventtypenya complate untuk workflow step parameter

                                adapter.UpdateMapCaseManagementWorkflowHistoryBAktifWorkflow(Eventtype.TaskComplated, oCurrentWorkflowhistory.workflowstep, Me.PK_CaseManagementID)
                            End If
                        Else
                            If Not IsHaveOtherWorkFlowThatNotComplate(oCurrentWorkflowhistory.workflowstep, oSQLTrans) Then

                                'Bukan level teratas maka buat task baru untuk level berikutnya.
                                ProposedStatusFinal = GetProposedStatusFinal(oCurrentWorkflowhistory.workflowstep, oSQLTrans)
                                Select Case ProposedStatusFinal
                                    Case ProposedAction.ConfirmAsSTR, ProposedAction.Dropped, ProposedAction.RequestAssignment
                                        ': insert ke mapcasemanagementworkflowhistory dengan eventtype =2(taskstarted) untuk user yang terdaftar dalam setting level berikutnya

                                        Dim oWorkFlowStepBerikutnya() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkflowStepBerikutnya(oCurrentWorkflowhistory.workflowstep)
                                        If Not oWorkFlowStepBerikutnya Is Nothing AndAlso oWorkFlowStepBerikutnya.Length > 0 Then

                                            If Not oWorkFlowStepBerikutnya(0).IsWorkflowStepNull Then
                                                StrNextWorkflowStep = oWorkFlowStepBerikutnya(0).WorkflowStep
                                            End If
                                            For i As Integer = 0 To oWorkFlowStepBerikutnya.Length - 1
                                                Dim strPKUserStepBerikutnya As String = oWorkFlowStepBerikutnya(i).Approvers
                                                Dim arrPKUserBerikutnya() As String = strPKUserStepBerikutnya.Split("|")

                                                Dim strPKNotifyUser As String = oWorkFlowStepBerikutnya(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")

                                                Dim strListuserStepBerikutnya As String = ""
                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""

                                                For j As Integer = 0 To arrPKUserBerikutnya.Length - 1
                                                    strListuserStepBerikutnya += GetUserIDbyPK(arrPKUserBerikutnya(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserBerikutnya(j))
                                                    If j <> arrPKUserBerikutnya.Length - 1 Then
                                                        strListuserStepBerikutnya += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oWorkFlowStepBerikutnya(i).Pk_CMW_Id, oWorkFlowStepBerikutnya(i).SerialNo)
                                                strEmailBody = GetEmailBody(oWorkFlowStepBerikutnya(i).Pk_CMW_Id, oWorkFlowStepBerikutnya(i).SerialNo)

                                                Dim strDescriptionStepBerikutnya As String
                                                If oWorkFlowStepBerikutnya.Length > 1 Then
                                                    strDescriptionStepBerikutnya = "Parallel " & i + 1
                                                Else
                                                    strDescriptionStepBerikutnya = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oWorkFlowStepBerikutnya(i).WorkflowStep, strListuserStepBerikutnya, "", Nothing, 0, strDescriptionStepBerikutnya, 0, 0)
                                                'SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next

                                        End If

                                    Case ProposedAction.CaseMonitoring
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oCurrentWorkflowhistory.workflowstep, oCurrentWorkflowhistory.UserID, "", Nothing, 0, "", 0, 0)
                                        StrNextWorkflowStep = oCurrentWorkflowhistory.workflowstep


                                    Case ProposedAction.RequestChange

                                        ':insert ke mapcasemanagementworkflowhistory dengan eventype=2(taskstarted) untuk user yang terdaftar dalam setting level sebelumnnya
                                        Dim oWorkFlowStepSebelumnya() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkflowStepSebelumnya(oCurrentWorkflowhistory.workflowstep)

                                        If Not oWorkFlowStepSebelumnya Is Nothing AndAlso oWorkFlowStepSebelumnya.Length > 0 Then
                                            If Not oWorkFlowStepSebelumnya(0).IsWorkflowStepNull Then
                                                StrNextWorkflowStep = oWorkFlowStepSebelumnya(0).WorkflowStep
                                            End If

                                            For i As Integer = 0 To oWorkFlowStepSebelumnya.Length - 1
                                                Dim strPKUserSebelumnya As String = oWorkFlowStepSebelumnya(i).Approvers
                                                Dim arrPKUserSebelumnya() As String = strPKUserSebelumnya.Split("|")

                                                Dim strPKNotifyUser As String = oWorkFlowStepSebelumnya(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")


                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""

                                                Dim strListUserStepSebelumnya As String = ""
                                                Dim strDescriptionStepSebelumnya As String = ""
                                                For j As Integer = 0 To arrPKUserSebelumnya.Length - 1
                                                    strListUserStepSebelumnya += GetUserIDbyPK(arrPKUserSebelumnya(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserSebelumnya(j))
                                                    If j <> arrPKUserSebelumnya.Length - 1 Then
                                                        strListUserStepSebelumnya += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)
                                                strEmailBody = GetEmailBody(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)

                                                Dim strDescriptioinStepSebelumnya As String
                                                If oWorkFlowStepSebelumnya.Length > 1 Then
                                                    strDescriptioinStepSebelumnya = "Parallel " & i + 1
                                                Else
                                                    strDescriptioinStepSebelumnya = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oWorkFlowStepSebelumnya(i).WorkflowStep, strListUserStepSebelumnya, "", Nothing, 0, strDescriptioinStepSebelumnya, 0, 0)
                                                SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next
                                        End If
                                End Select
                                adapter.UpdateMapCaseManagementWorkflowHistoryBAktifWorkflow(Eventtype.TaskComplated, oCurrentWorkflowhistory.workflowstep, Me.PK_CaseManagementID)
                                'Else
                                adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.UnderInvestigate, Me.PK_CaseManagementID)
                                adapterCaseManagement.UpdateCaseManagementWorkflowStepByPK(StrNextWorkflowStep, Me.PK_CaseManagementID)
                            End If
                        End If
                        adapterCaseManagement.UpdateCaseManagementLastUpdatedByPK(DateTime.Now(), Me.PK_CaseManagementID)
                    End Using

                    'update lastrun dari workflow
                    Using adapterCase As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementWorkflowTableAdapter
                        SetTransaction(adapterCase, oSQLTrans)
                        adapterCase.UpdateMapCaseManagementWorkflowLastRun(Now, Me.PK_CaseManagementID)
                    End Using
                    oSQLTrans.Commit()

                    AMLBLL.CaseManagementBLL.UpdateAging(Me.PK_CaseManagementID)



                    UnloadSession()
                    If Me.IsHaveRighttoInputInvestigationNotoesResponse Then
                        Me.Panel1.Visible = True
                        BindListAttachment()
                    Else
                        Me.Panel1.Visible = False
                    End If
                End Using
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
            End If
            
        End Try
    End Sub

    Protected Sub BtnDoneProposed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDoneProposed.Click
        Try
            For Each IdPk As String In Me.SetnGetSelectedItem
                'cek jika currentworkflow adalah level terendah,maka ngak boleh jalan.
                Dim serialno As Integer = 0
                Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_GetSerialNoLastWorkflow")
                    objCommand.Parameters.Add(New SqlParameter("@PK_CaseManagementID", IdPk))
                    serialno = DataRepository.Provider.ExecuteScalar(objCommand)
                End Using

                If serialno = 1 And CboProposedAction.SelectedValue = 2 Then
                    Throw New Exception("Current Workflow Step for Case ID " & IdPk & " is the lowest step, Can't Select Proposed Action Request Change")
                End If
            Next

            If SetnGetSelectedItem.Count > 0 Then
                For Each IdPk As String In Me.SetnGetSelectedItem
                    Me.PK_CaseManagementID = IdPk
                    Dim oCurrentWorkflowhistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
                    Me._oRowCasemanagementWorkFlowHistory = Nothing
                    oCurrentWorkflowhistory = GetCurrentWorkflowHistory()
                    If Not oCurrentWorkflowhistory Is Nothing Then
                        DoneProposed()
                    Else
                        If CboProposedAction.SelectedValue = 2 Then 'kalau request change
                            DoneProposedFlowBeforeRequestChange()
                        Else
                            DoneProposedFlowBefore()
                            UpdateMapCaseManagementWorkflowHistoryBComplate(Me.PK_CaseManagementID)
                            oCurrentWorkflowhistory = GetCurrentWorkflowHistory()
                            If Not oCurrentWorkflowhistory Is Nothing Then
                                DoneProposed()
                            Else
                                DoneProposedFlowBefore()
                                UpdateMapCaseManagementWorkflowHistoryBComplate(Me.PK_CaseManagementID)
                                oCurrentWorkflowhistory = GetCurrentWorkflowHistory()
                                If Not oCurrentWorkflowhistory Is Nothing Then
                                    DoneProposed()
                                Else
                                    DoneProposedFlowBefore()
                                    UpdateMapCaseManagementWorkflowHistoryBComplate(Me.PK_CaseManagementID)
                                    oCurrentWorkflowhistory = GetCurrentWorkflowHistory()
                                    If Not oCurrentWorkflowhistory Is Nothing Then
                                        DoneProposed()
                                    End If
                                End If
                            End If
                        End If
                    End If
                Next
                Response.Redirect("CaseManagementViewByCIF.aspx?clear=no", False)

            Else
                Throw New Exception("There is no data selected.")
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
            Me.BtnDoneProposed.Enabled = True
        End Try
    End Sub

    'Protected Sub BtnDoneProposed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDoneProposed.Click
    '    Try
    '        If SetnGetSelectedItem.Count > 0 Then
    '            For Each IdPk As String In Me.SetnGetSelectedItem
    '                Me.PK_CaseManagementID = IdPk
    '                Dim oCurrentWorkflowhistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
    '                Me._oRowCasemanagementWorkFlowHistory = Nothing
    '                oCurrentWorkflowhistory = GetCurrentWorkflowHistory()
    '                If Not oCurrentWorkflowhistory Is Nothing Then
    '                    DoneProposed()
    '                Else
    '                    'cek jika currentworkflow adalah level terendah,maka ngak boleh jalan.
    '                    Dim serialno As Integer = 0
    '                    Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_GetSerialNoLastWorkflow")
    '                        objCommand.Parameters.Add(New SqlParameter("@PK_CaseManagementID", Me.PK_CaseManagementID))
    '                        serialno = DataRepository.Provider.ExecuteScalar(objCommand)
    '                    End Using

    '                    If serialno = 1 And CboProposedAction.SelectedValue = 2 Then
    '                        Throw New Exception("Current Workflow Step for Case ID " & Me.PK_CaseManagementID & " is the lowest step, Can't Select Proposed Action Request Change")
    '                    End If

    '                    If CboProposedAction.SelectedValue = 2 Then 'kalau request change
    '                        DoneProposedFlowBeforeRequestChange()
    '                    Else
    '                        DoneProposedFlowBefore()
    '                        UpdateMapCaseManagementWorkflowHistoryBComplate(Me.PK_CaseManagementID)
    '                        oCurrentWorkflowhistory = GetCurrentWorkflowHistory()
    '                        If Not oCurrentWorkflowhistory Is Nothing Then
    '                            DoneProposed()
    '                        Else
    '                            DoneProposedFlowBefore()
    '                            UpdateMapCaseManagementWorkflowHistoryBComplate(Me.PK_CaseManagementID)
    '                            oCurrentWorkflowhistory = GetCurrentWorkflowHistory()
    '                            If Not oCurrentWorkflowhistory Is Nothing Then
    '                                DoneProposed()
    '                            Else
    '                                DoneProposedFlowBefore()
    '                                UpdateMapCaseManagementWorkflowHistoryBComplate(Me.PK_CaseManagementID)
    '                                oCurrentWorkflowhistory = GetCurrentWorkflowHistory()
    '                                If Not oCurrentWorkflowhistory Is Nothing Then
    '                                    DoneProposed()
    '                                End If
    '                            End If
    '                        End If
    '                    End If
    '                End If
    '            Next
    '            Response.Redirect("CaseManagementViewByCIF.aspx?clear=no", False)

    '        Else
    '            Throw New Exception("There is no data selected.")
    '        End If

    '    Catch ex As Exception
    '        Me.cvalPageError.IsValid = False
    '        Me.cvalPageError.ErrorMessage = ex.Message
    '        LogError(ex)
    '    End Try
    'End Sub

    Protected Sub BtnSaveAsDraftWorklfow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSaveAsDraftWorklfow.Click
        Dim oSQLTrans As SqlTransaction = Nothing

        Dim ProposedStatusFinal As Integer
        Dim strDescription As String = ""
        Dim StrNextWorkflowStep As String = ""
        Dim intAccountownerid As Integer
        Try
            If Page.IsValid Then
                If SetnGetSelectedItem.Count > 0 Then
                    For Each IdPk As String In Me.SetnGetSelectedItem
                        Me.PK_CaseManagementID = IdPk
                        Dim oCurrentWorkflowhistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing

                        oCurrentWorkflowhistory = GetCurrentWorkflowHistory()


                        If Not oCurrentWorkflowhistory Is Nothing Then
                            If CboProposedAction.SelectedValue = ProposedAction.ReAssign Then
                                intAccountownerid = CboAccountOwner.SelectedValue
                            End If
                            CaseManagementBLL.SaveNotesDraft(oCurrentWorkflowhistory.workflowstep, TextInvestigationNotes.Text.Trim, CboProposedAction.SelectedValue, intAccountownerid, Me.PK_CaseManagementID)
                        End If
                    Next
                End If
            End If

            Response.Redirect("CaseManagementViewByCIF.aspx", False)

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            BtnDoneProposed.Enabled = True
        End Try

    End Sub
    Protected Sub ButtonAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        Dim WorkFlowHistoryID As System.Nullable(Of Long)
        Try

            If FileUpload1.HasFile Then
                For Each IdPk As String In Me.SetnGetSelectedItemForAttach 'Me.SetnGetSelectedItem
                    WorkFlowHistoryID = IdPk
                    Using adapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementWorkflowHistoryAttachmentTableAdapter
                        adapter.Insert(WorkFlowHistoryID, FileUpload1.FileName, FileUpload1.FileBytes, Sahassa.AML.Commonly.SessionPkUserId, FileUpload1.PostedFile.ContentType)
                    End Using
                Next
                BindListAttachment()
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ButtonDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonDelete.Click
        Try
            If ListAttachment.SelectedIndex <> -1 Then
                Using adapter As New AMLDAL.CaseManagementTableAdapters.GetMapCaseManagementWorkflowAttachmentByUserIDTableAdapter
                    adapter.DeleteMapCaseManagementWorkflowAttachmentByPK(ListAttachment.SelectedValue)
                End Using
                BindListAttachment()
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub CboProposedAction_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CboProposedAction.SelectedIndexChanged
        Try
            If SetnGetSelectedItem.Count > 0 Then
                If CboProposedAction.SelectedValue = "5" Then
                    FillAccountOwner()
                    trAccountOwner.Visible = True
                Else
                    trAccountOwner.Visible = False
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub FillProposedAction()
        Try
            Using adapter As New AMLDAL.CaseManagementTableAdapters.CaseManagementProposedActionTableAdapter
                CboProposedAction.DataSource = adapter.GetData.Select("PK_CaseManagementProposedActionID in (2,3,4,7)")
                CboProposedAction.DataTextField = "ProposedAction"
                CboProposedAction.DataValueField = "PK_CaseManagementProposedActionID"
                CboProposedAction.DataBind()
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Private Sub FillAccountOwner()
        Using adapter As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
            CboAccountOwner.Items.Clear()

            For Each orows As AMLDAL.AMLDataSet.AccountOwnerRow In adapter.GetData.Select("", "AccountOwnerId  asc")
                'If IsBolehMendapatReAssign(orows.AccountOwnerId, Me.oRowCaseManagement.VIPCode, Me.oRowCaseManagement.InsiderCode, Me.oRowCaseManagement.Segment, Me.oRowCaseManagement.CaseDescription) Then
                CboAccountOwner.Items.Add(New ListItem(orows.AccountOwnerId & " - " & orows.AccountOwnerName, orows.AccountOwnerId))

                'End If
            Next
        End Using
    End Sub
    Public Property ObjTMapCaseManagementWorkflowHistoryDraft() As TList(Of MapCaseManagementWorkflowHistory)
        Get
            If Session("CaseManagementViewDetail.ObjTMapCaseManagementWorkflowHistory") Is Nothing Then
                Session("CaseManagementViewDetail.ObjTMapCaseManagementWorkflowHistory") = CaseManagementBLL.GetTlistMapCaseManagementWorkflowHistory(MapCaseManagementWorkflowHistoryColumn.FK_EventTypeID.ToString & "=" & Eventtype.TaskDraft & " and " & MapCaseManagementWorkflowHistoryColumn.FK_CaseManagementID.ToString & "=" & Me.PK_CaseManagementID & " and " & MapCaseManagementWorkflowHistoryColumn.UserID.ToString & "='" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "'", "", 0, Integer.MaxValue, 0)
                Return CType(Session("CaseManagementViewDetail.ObjTMapCaseManagementWorkflowHistory"), TList(Of MapCaseManagementWorkflowHistory))
            Else
                Return CType(Session("CaseManagementViewDetail.ObjTMapCaseManagementWorkflowHistory"), TList(Of MapCaseManagementWorkflowHistory))
            End If
        End Get
        Set(ByVal value As TList(Of MapCaseManagementWorkflowHistory))
            Session("CaseManagementViewDetail.ObjTMapCaseManagementWorkflowHistory") = value
        End Set
    End Property
    Private Function GetCurrentWorkflowHistory() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow
        Dim oRowWorkflowNotComplete() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim ArrUserid() As String = Nothing
        Try
            'cari yang status stated dan untuk user yang sedang login
            oRowWorkflowNotComplete = oRowCasemanagementWorkFlowHistory.Select("fk_eventtypeid IN (" & Eventtype.TaskStarted & "," & Eventtype.CaseMonitoring & ") and bcomplate=0")
            For i As Integer = 0 To oRowWorkflowNotComplete.Length - 1
                ArrUserid = oRowWorkflowNotComplete(i).UserID.Split(";")
                For j As Integer = 0 To ArrUserid.Length - 1
                    If ArrUserid(j).ToLower = Sahassa.AML.Commonly.SessionUserId.ToLower Then
                        Return oRowWorkflowNotComplete(i)
                    End If
                Next
            Next
            Return Nothing
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Private Function GetDescriptionByStep(ByVal strWorkFlowStep) As String
        Dim orowSettingworkflow() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = Nothing
        Dim ArrApprover() As String = Nothing
        Try
            orowSettingworkflow = oSettingWorkFlow.Select("workflowstep='" & strWorkFlowStep & "'")
            If orowSettingworkflow.Length > 0 Then
                For i As Integer = 0 To orowSettingworkflow.Length - 1

                    If orowSettingworkflow(i).Paralel > 1 Then
                        ArrApprover = orowSettingworkflow(i).Approvers.Split("|")
                        For j As Integer = 0 To ArrApprover.Length - 1
                            If ArrApprover(j) = Sahassa.AML.Commonly.SessionPkUserId Then
                                Return "Parallel " & i + 1
                            End If
                        Next
                    ElseIf orowSettingworkflow(i).Paralel = 1 Then
                        Return ""
                    End If

                Next
            Else
                Return ""

            End If
        Catch ex As Exception
            LogError(ex)
            Return ""
        Finally
            If Not orowSettingworkflow Is Nothing Then
                orowSettingworkflow = Nothing
            End If
            If Not ArrApprover Is Nothing Then
                ArrApprover = Nothing
            End If
        End Try
    End Function

    Public Enum Eventtype
        WorkFlowStarted = 1
        TaskStarted
        TaskComplated
        TaskCanceledBySystem
        Reassign
        WorkFlowFinish
        TaskDraft
        CaseMonitoring
    End Enum
    Public Enum ProposedAction
        RequestAssignment = 1
        RequestChange
        Dropped
        ConfirmAsSTR
        ReAssign
        UnderlyingTransactionConfirmation
        CaseMonitoring
    End Enum

    Public Enum CaseManagementStatus
        bNew = 1
        UnderInvestigate
        Dropped
        Approved
    End Enum
    Private Property PK_CaseManagementID() As String
        Get
            Return Session("CaseManagementByCIFDetail.PK_CaseManagementID")
        End Get
        Set(value As String)
            Session("CaseManagementByCIFDetail.PK_CaseManagementID") = value
        End Set
    End Property
    Private Function IsLevelWorkFlowTeratas() As Boolean
        Dim oRowWorkflowNotComplete() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim strWorkflowstep As String
        Dim ArrSettingWorkflow() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = Nothing
        Dim intCurrentSerial As Integer = 0
        Dim intMaxSerial As Integer = 0
        Try
            oRowWorkflowNotComplete = oRowCasemanagementWorkFlowHistory.Select("fk_eventtypeid IN (" & Eventtype.TaskStarted & "," & Eventtype.CaseMonitoring & ") and bcomplate=0")
            For i As Integer = 0 To oRowWorkflowNotComplete.Length - 1
                strWorkflowstep = oRowWorkflowNotComplete(i).workflowstep
                ArrSettingWorkflow = oSettingWorkFlow.Select("workflowstep='" & strWorkflowstep.Replace("'", "''") & "'")
                For j As Integer = 0 To ArrSettingWorkflow.Length - 1
                    intCurrentSerial = ArrSettingWorkflow(j).SerialNo
                Next
            Next
            ArrSettingWorkflow = oSettingWorkFlow.Select("", "serialno desc")
            If ArrSettingWorkflow.Length > 0 Then
                intMaxSerial = ArrSettingWorkflow(0).SerialNo
            End If
            If intCurrentSerial = intMaxSerial AndAlso intMaxSerial <> 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        Finally
            If Not oRowWorkflowNotComplete Is Nothing Then
                oRowWorkflowNotComplete = Nothing
            End If
            If Not ArrSettingWorkflow Is Nothing Then
                ArrSettingWorkflow = Nothing
            End If
        End Try
    End Function
    Private Function IsLevelWorkFlowTeratas(ByVal osqltrans As SqlTransaction) As Boolean
        Dim oRowWorkflowNotComplete() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim strWorkflowstep As String
        Dim ArrSettingWorkflow() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = Nothing
        Dim intCurrentSerial As Integer = 0
        Dim intMaxSerial As Integer = 0
        Try
            oRowWorkflowNotComplete = oRowCasemanagementWorkFlowHistory.Select("fk_eventtypeid IN (" & Eventtype.TaskStarted & "," & Eventtype.CaseMonitoring & ") and bcomplate=0")
            For i As Integer = 0 To oRowWorkflowNotComplete.Length - 1
                strWorkflowstep = oRowWorkflowNotComplete(i).workflowstep
                ArrSettingWorkflow = oSettingWorkFlow(osqltrans).Select("workflowstep='" & strWorkflowstep.Replace("'", "''") & "'")
                For j As Integer = 0 To ArrSettingWorkflow.Length - 1
                    intCurrentSerial = ArrSettingWorkflow(j).SerialNo
                Next
            Next
            ArrSettingWorkflow = oSettingWorkFlow(osqltrans).Select("", "serialno desc")
            If ArrSettingWorkflow.Length > 0 Then
                intMaxSerial = ArrSettingWorkflow(0).SerialNo
            End If
            If intCurrentSerial = intMaxSerial AndAlso intMaxSerial <> 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        Finally
            If Not oRowWorkflowNotComplete Is Nothing Then
                oRowWorkflowNotComplete = Nothing
            End If
            If Not ArrSettingWorkflow Is Nothing Then
                ArrSettingWorkflow = Nothing
            End If
        End Try
    End Function
    Private _oRowCasemanagementWorkFlowHistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryDataTable
    Private ReadOnly Property oRowCasemanagementWorkFlowHistory() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryDataTable
        Get
            If Not _oRowCasemanagementWorkFlowHistory Is Nothing Then
                Return _oRowCasemanagementWorkFlowHistory
            Else
                Using adapter As New AMLDAL.CaseManagementTableAdapters.SelectMapCaseManagementWorkflowHistoryTableAdapter
                    _oRowCasemanagementWorkFlowHistory = adapter.SelectMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID)
                    Return _oRowCasemanagementWorkFlowHistory
                End Using
            End If

        End Get
    End Property
    Private _oSettiingWorkFlow As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable
    Private ReadOnly Property oSettingWorkFlow() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable
        Get
            Try
                If _oSettiingWorkFlow Is Nothing Then
                    Using adapter As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter
                        If Not Me.oRowCaseManagement.IsFK_AccountOwnerIDNull Then
                            _oSettiingWorkFlow = adapter.GetSettingWorkFlowCaseManagementByAccountOwnerID(Me.oRowCaseManagement.FK_AccountOwnerID, Me.oRowCaseManagement.VIPCode, Me.oRowCaseManagement.InsiderCode, Me.oRowCaseManagement.Segment, oRowCaseManagement.CaseDescription, oRowCaseManagement.SBU, oRowCaseManagement.SUBSBU, oRowCaseManagement.RM)
                            Return _oSettiingWorkFlow
                        Else
                            Throw New Exception("Account Owner Not Valid")
                            Return Nothing
                        End If

                    End Using
                Else
                    Return _oSettiingWorkFlow
                End If
            Catch ex As Exception
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End Get
    End Property
    Private ReadOnly Property oSettingWorkFlow(ByVal osqltrans As SqlTransaction) As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable
        Get
            Try
                If _oSettiingWorkFlow Is Nothing Then
                    Using adapter As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter
                        If Not Me.oRowCaseManagement(osqltrans).IsFK_AccountOwnerIDNull Then
                            _oSettiingWorkFlow = adapter.GetSettingWorkFlowCaseManagementByAccountOwnerID(Me.oRowCaseManagement.FK_AccountOwnerID, Me.oRowCaseManagement.VIPCode, Me.oRowCaseManagement.InsiderCode, Me.oRowCaseManagement.Segment, oRowCaseManagement.CaseDescription, oRowCaseManagement.SBU, oRowCaseManagement.SUBSBU, oRowCaseManagement.RM)
                            Return _oSettiingWorkFlow
                        Else
                            Throw New Exception("Account Owner Not Valid")
                            Return Nothing
                        End If

                    End Using
                Else
                    Return _oSettiingWorkFlow
                End If
            Catch ex As Exception
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End Get
    End Property

    Private _oRowCaseManagement As AMLDAL.CaseManagement.SelectCaseManagementRow
    Private ReadOnly Property oRowCaseManagement() As AMLDAL.CaseManagement.SelectCaseManagementRow
        Get
            Try
                If Not _oRowCaseManagement Is Nothing Then
                    Return _oRowCaseManagement
                Else
                    Using adapter As New AMLDAL.CaseManagementTableAdapters.SelectCaseManagementTableAdapter
                        Using oTable As AMLDAL.CaseManagement.SelectCaseManagementDataTable = adapter.GetVw_CaseManagementByPK(Me.PK_CaseManagementID)
                            If oTable.Rows.Count > 0 Then
                                _oRowCaseManagement = oTable.Rows(0)
                                Return _oRowCaseManagement
                            Else
                                Throw New Exception("There is no data for PKManagementID " & Me.PK_CaseManagementID)
                            End If
                        End Using
                    End Using
                End If

            Catch ex As Exception
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End Get

    End Property
    Private ReadOnly Property oRowCaseManagement(ByVal osqltrans As SqlTransaction) As AMLDAL.CaseManagement.SelectCaseManagementRow
        Get
            Try
                If Not _oRowCaseManagement Is Nothing Then
                    Return _oRowCaseManagement
                Else
                    Using adapter As New AMLDAL.CaseManagementTableAdapters.SelectCaseManagementTableAdapter
                        SetTransaction(adapter, osqltrans)
                        Using oTable As AMLDAL.CaseManagement.SelectCaseManagementDataTable = adapter.GetVw_CaseManagementByPK(Me.PK_CaseManagementID)
                            If oTable.Rows.Count > 0 Then
                                _oRowCaseManagement = oTable.Rows(0)
                                Return _oRowCaseManagement
                            Else
                                Throw New Exception("There is no data for PKManagementID " & Me.PK_CaseManagementID)
                            End If
                        End Using
                    End Using
                End If

            Catch ex As Exception
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End Get

    End Property

    Private Function IsHaveOtherWorkFlowThatNotComplate(ByVal strworkflowStep As String, ByRef osqltrans As SqlTransaction) As Boolean
        Dim oTableWorkflowHistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryDataTable = Nothing
        Dim ArrWorkflow() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Try
            Using adapterwp As New AMLDAL.CaseManagementTableAdapters.SelectMapCaseManagementWorkflowHistoryTableAdapter
                SetTransaction(adapterwp, osqltrans)
                oTableWorkflowHistory = adapterwp.SelectMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID)
                ArrWorkflow = oTableWorkflowHistory.Select("fk_eventtypeid IN (" & Eventtype.TaskStarted & "," & Eventtype.CaseMonitoring & ") and bcomplate=0 and workflowstep='" & strworkflowStep.Replace("'", "''") & "'")
                If ArrWorkflow.Length = 0 Then
                    Return False
                Else
                    Return True
                End If
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        Finally
            If Not oTableWorkflowHistory Is Nothing Then
                oTableWorkflowHistory = Nothing
            End If
            If Not ArrWorkflow Is Nothing Then
                ArrWorkflow = Nothing
            End If
        End Try

    End Function

    Private Function GetProposedStatusFinal(ByVal StepWorkflow As String, ByRef osqltrans As SqlTransaction) As Integer
        'Ambil workflowhistory yang final
        Dim arrHasil() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow
        Dim orowProposedAction() As AMLDAL.CaseManagement.CaseManagementProposedActionRow
        Dim arrProposedAction(-1) As Integer
        Try
            'harus query lagi kalau ngak bakal ambil yang belum terupdate
            Using adapter As New AMLDAL.CaseManagementTableAdapters.SelectMapCaseManagementWorkflowHistoryTableAdapter
                SetTransaction(adapter, osqltrans)
                Using oTable As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryDataTable = adapter.SelectMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID)
                    If oTable.Rows.Count > 0 Then
                        'Cari yang eventtype=taskcomplate(fk_eventtypeid=3) dan workflowste=parameter dan 
                        arrHasil = oTable.Select("fk_eventtypeid=" & Eventtype.TaskComplated & " and workflowstep='" & StepWorkflow.Replace("'", "''") & "' and baktifworkflow=1")
                        If arrHasil.Length > 0 Then
                            For i As Integer = 0 To arrHasil.Length - 1
                                ReDim Preserve arrProposedAction(arrProposedAction.Length)
                                If Not arrHasil(i).IsFK_CaseManagementProposedActionIDNull Then
                                    arrProposedAction(arrProposedAction.Length - 1) = arrHasil(i).FK_CaseManagementProposedActionID
                                End If
                            Next

                            Dim intMaxPriority As Integer = Integer.MaxValue
                            Dim PKPriority As Integer = 0
                            For i As Integer = 0 To arrProposedAction.Length - 1
                                Using adapterProposedAction As New AMLDAL.CaseManagementTableAdapters.CaseManagementProposedActionTableAdapter
                                    orowProposedAction = adapterProposedAction.GetData.Select("PK_CaseManagementProposedActionID=" & arrProposedAction(i))
                                    If orowProposedAction.Length > 0 Then
                                        If orowProposedAction(0).LevelPriority < intMaxPriority Then
                                            'Cari priority paling rendah
                                            intMaxPriority = orowProposedAction(0).LevelPriority
                                            PKPriority = orowProposedAction(0).PK_CaseManagementProposedActionID
                                        End If
                                        'if intMaxPriority
                                    End If
                                End Using
                            Next


                            Return PKPriority
                        End If
                    End If
                End Using

            End Using


        Catch ex As Exception
            LogError(ex)
            Throw
        End Try

    End Function
    Private Function GetUserIDbyPK(ByVal PKUSERID As String) As String
        Try
            If PKUSERID <> "" Then
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Using oTable As AMLDAL.AMLDataSet.UserDataTable = adapter.GetDataByUserID(PKUSERID)
                        If oTable.Rows.Count > 0 Then
                            Return CType(oTable.Rows(0), AMLDAL.AMLDataSet.UserRow).UserID
                        End If
                    End Using
                End Using
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub SendEmail(ByVal StrRecipientTo As String, ByVal StrRecipientCC As String, ByVal StrSubject As String, ByVal strbody As String)
        Dim oEmail As Sahassa.AML.EMail
        Try
            oEmail = New Sahassa.AML.EMail
            oEmail.Sender = System.Configuration.ConfigurationManager.AppSettings("FromMailAddress")
            oEmail.Recipient = StrRecipientTo
            oEmail.RecipientCC = StrRecipientCC
            oEmail.Subject = StrSubject
            oEmail.Body = strbody.Replace(vbLf, "<br>")
            'oEmail.SendEmail()
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub
    Private Function GetWorkFlowStepBaruSerial1(ByVal AccountOwnerID As Integer, ByVal strViPcode As String, ByVal strInsiderCode As String, ByVal strSegment As String, ByVal strdescriptioncasealert As String, strsbu As String, strsubsbu As String, strrm As String) As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow()

        Try
            Using adapter As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter

                Return adapter.GetSettingWorkFlowCaseManagementByAccountOwnerID(AccountOwnerID, strViPcode, strInsiderCode, strSegment, strdescriptioncasealert, strsbu, strsubsbu, strrm).Select("serialno=1")

            End Using
        Catch ex As Exception
            LogError(ex)
        End Try
    End Function
    Private Function GetEmailAddress(ByVal PKUserID As String) As String
        Try
            If PKUserID <> "" Then
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Using otable As AMLDAL.AMLDataSet.UserDataTable = adapter.GetDataByUserID(PKUserID)
                        If otable.Rows.Count > 0 Then
                            If CType(otable.Rows(0), AMLDAL.AMLDataSet.UserRow).IsUserEmailAddressNull Then
                                Return ""
                            Else
                                Return CType(otable.Rows(0), AMLDAL.AMLDataSet.UserRow).UserEmailAddress
                            End If
                        Else
                            Return ""
                        End If
                    End Using
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Function GetEmailSubject(ByVal PKCMWID As Long, ByVal intSerialNo As Integer) As String
        Try
            Using adapter As New AMLDAL.CaseManagementTableAdapters.CaseManagementWorkflowEmailTemplateTableAdapter

                Using otable As AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateDataTable = adapter.GetEmailTemplate(PKCMWID, intSerialNo)
                    If otable.Rows.Count > 0 Then
                        If Not CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).IsSubject_EmailTemplateNull Then
                            Return CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).Subject_EmailTemplate
                        Else
                            Return ""
                        End If
                    Else
                        Return ""
                    End If

                End Using
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Function GetEmailBody(ByVal PKCMWID As Long, ByVal intSerialNo As Integer) As String
        Try
            Using adapter As New AMLDAL.CaseManagementTableAdapters.CaseManagementWorkflowEmailTemplateTableAdapter

                Using otable As AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateDataTable = adapter.GetEmailTemplate(PKCMWID, intSerialNo)
                    If otable.Rows.Count > 0 Then
                        If Not CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).IsBody_EmailTemplateNull Then
                            Return CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).Body_EmailTemplate
                        Else
                            Return ""
                        End If
                    Else
                        Return ""
                    End If

                End Using
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Function GetWorkflowStepSebelumnya(ByVal strcurrentWorkflow As String) As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow()
        Dim orowStepCurrent() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
        Dim SerialNo As Integer
        Dim orowStepSebelumnya() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
        Try
            orowStepCurrent = oSettingWorkFlow.Select("workflowstep='" & strcurrentWorkflow.Replace("'", "''") & "'")
            If orowStepCurrent.Length > 0 Then
                SerialNo = orowStepCurrent(0).SerialNo
                If SerialNo > 1 Then
                    orowStepSebelumnya = oSettingWorkFlow.Select("Serialno=" & SerialNo - 1 & "")
                Else
                    orowStepSebelumnya = oSettingWorkFlow.Select("Serialno=" & SerialNo & "")
                End If
                Return orowStepSebelumnya
            End If
            Return Nothing
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function GetWorkflowStepBerikutnya(ByVal strcurrentWorkflow As String) As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow()
        Dim orowStepCurrent() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
        Dim SerialNo As Integer
        Dim orowStepBerikut() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
        Try
            orowStepCurrent = oSettingWorkFlow.Select("workflowstep='" & strcurrentWorkflow.Replace("'", "''") & "'")
            If orowStepCurrent.Length > 0 Then
                SerialNo = orowStepCurrent(0).SerialNo
                orowStepBerikut = oSettingWorkFlow.Select("Serialno=" & SerialNo + 1 & "")
                Return orowStepBerikut
            End If
            Return Nothing
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub UnloadSession()
        _oRowCaseManagement = Nothing
        _oRowCaseManagementWorkFlow = Nothing
        _oRowCasemanagementWorkFlowHistory = Nothing

    End Sub
    Private _oRowCaseManagementWorkFlow As AMLDAL.CaseManagement.MapCaseManagementWorkflowRow
    Private ReadOnly Property oRowCaseManagementWorkFlow() As AMLDAL.CaseManagement.MapCaseManagementWorkflowRow
        Get
            Try
                If Not _oRowCaseManagementWorkFlow Is Nothing Then
                    Return _oRowCaseManagementWorkFlow
                Else
                    Using adapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementWorkflowTableAdapter
                        Using oTable As AMLDAL.CaseManagement.MapCaseManagementWorkflowDataTable = adapter.GetCaseManagementWorkFlowByFK(Me.PK_CaseManagementID)
                            If oTable.Rows.Count > 0 Then
                                _oRowCaseManagementWorkFlow = oTable.Rows(0)
                                Return _oRowCaseManagementWorkFlow
                            Else
                                Throw New Exception("There is no data workflow for PKManagementID " & Me.PK_CaseManagementID)
                            End If
                        End Using
                    End Using
                End If

            Catch ex As Exception
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End Get
    End Property
    Private Function IsHaveRighttoInputInvestigationNotoesResponse()
        Dim oRowWorkflowNotComplete() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim ArrUserid() As String = Nothing
        Try
            'cari yang workflowstep=task started dan belum complete
            'fk_eventtypeid=2 and bcomplete=0
            oRowWorkflowNotComplete = oRowCasemanagementWorkFlowHistory.Select("fk_eventtypeid IN (" & Eventtype.TaskStarted & ", " & Eventtype.CaseMonitoring & ") and bcomplate=0")
            For i As Integer = 0 To oRowWorkflowNotComplete.Length - 1
                ArrUserid = oRowWorkflowNotComplete(i).UserID.Split(";")
                For j As Integer = 0 To ArrUserid.Length - 1
                    If ArrUserid(j).ToLower = Sahassa.AML.Commonly.SessionUserId.ToLower Then
                        'cek jika workflowstep itu serial yang pertama ngak boleh input investigation notes. tapi input casemanagementpenjelasan
                        If AMLBLL.CaseManagementBLL.GetSerialNo(oRowWorkflowNotComplete(i).workflowstep, ObjCaseManagement.FK_AccountOwnerID) = 1 Then
                            Return False
                        Else
                            Return True
                        End If


                    End If
                Next
            Next
            Return False
        Catch ex As Exception
            LogError(ex)
            Throw
        Finally
            If Not oRowWorkflowNotComplete Is Nothing Then
                oRowWorkflowNotComplete = Nothing
            End If
            If Not ArrUserid Is Nothing Then
                ArrUserid = Nothing
            End If
        End Try

    End Function
    Private Sub BindListAttachment()
        Try
            Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_GetAttachmentSTRByUserID")
                objCommand.Parameters.Add(New SqlParameter("@fk_userId", Sahassa.AML.Commonly.SessionPkUserId))
                Me.ListAttachment.DataSource = DataRepository.Provider.ExecuteDataSet(objCommand)
                Me.ListAttachment.DataTextField = "filename"
                Me.ListAttachment.DataValueField = "PK_MapCaseManagementWorkflowHistoryAttachmentID"
                Me.ListAttachment.DataBind()
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Public Property ObjCaseManagement() As CaseManagement
        Get
            If Session("CaseManagementViewDetail.ObjCaseManagement") Is Nothing Then
                Session("CaseManagementViewDetail.ObjCaseManagement") = CaseManagementBLL.GetCaseManagementByPk(Me.PK_CaseManagementID)
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagement"), CaseManagement)
            Else
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagement"), CaseManagement)
            End If
        End Get
        Set(ByVal value As CaseManagement)
            Session("CaseManagementViewDetail.ObjCaseManagement") = value
        End Set
    End Property
    Private Function IsLevelWorkFlowTerendah() As Boolean
        Dim oRowWorkflowNotComplete() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim strWorkflowstep As String
        Dim ArrSettingWorkflow() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = Nothing
        Dim strCurrentSerial As Integer = 0
        Try
            oRowWorkflowNotComplete = oRowCasemanagementWorkFlowHistory.Select("fk_eventtypeid IN (" & Eventtype.TaskStarted & "," & Eventtype.CaseMonitoring & ") and bcomplate=0")
            For i As Integer = 0 To oRowWorkflowNotComplete.Length - 1
                strWorkflowstep = oRowWorkflowNotComplete(i).workflowstep
                ArrSettingWorkflow = oSettingWorkFlow.Select("workflowstep='" & strWorkflowstep.Replace("'", "''") & "'")
                For j As Integer = 0 To ArrSettingWorkflow.Length - 1
                    strCurrentSerial = ArrSettingWorkflow(j).SerialNo
                Next
            Next
            If strCurrentSerial = 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        Finally
            If Not oRowWorkflowNotComplete Is Nothing Then
                oRowWorkflowNotComplete = Nothing
            End If
            If Not ArrSettingWorkflow Is Nothing Then
                ArrSettingWorkflow = Nothing
            End If
        End Try
    End Function
    'Private Function IsBolehMendapatReAssign(ByVal strAccontOwnerID As Integer, ByVal strVIPCode As String, ByVal strInsiderCode As String, ByVal strSegment As String, ByVal strdescriptioncasealert As String) As Boolean
    '    Try
    '        Using adapter As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter
    '            If adapter.GetSettingWorkFlowCaseManagementByAccountOwnerID(strAccontOwnerID, strVIPCode, strInsiderCode, strSegment, strdescriptioncasealert).Rows.Count = 0 Then
    '                Return False
    '            Else
    '                Return True
    '            End If
    '        End Using
    '    Catch ex As Exception
    '        Throw

    '    End Try
    'End Function

    Protected Sub CheckBoxExporttoExcel_CheckedChanged(sender As Object, e As System.EventArgs)
        CollectSelected()
    End Sub
    Private Property SetnGetSelectedItemForAttach() As ArrayList
        Get
            Return IIf(Session("CaseManagementView.SetnGetSelectedItemForAttach") Is Nothing, New ArrayList, Session("CaseManagementView.SetnGetSelectedItemForAttach"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CaseManagementView.SetnGetSelectedItemForAttach") = value
        End Set
    End Property
    Private Sub UpdateMapCaseManagementWorkflowHistoryBComplate(ByVal ObjFK_CaseManagementID As Integer)
        Using objcmd As New System.Data.SqlClient.SqlCommand("usp_UpdateMapCaseManagementWorkflowHistoryBComplate")
            objcmd.CommandType = System.Data.CommandType.StoredProcedure
            objcmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings("SQLCommandTimeout")
            objcmd.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PK_CaseManagementID", ObjFK_CaseManagementID))
            DataRepository.Provider.ExecuteScalar(objcmd)
        End Using
    End Sub

    Protected Sub ImageButton1_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Try
            Response.Redirect("CaseManagementViewByCIF.aspx", False)
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class '3537