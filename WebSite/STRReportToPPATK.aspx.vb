﻿Imports EkaDataNettier.Entities
Imports EkaDataNettier.Services
Imports AMLBLL

Partial Class STRReportToPPATK
    Inherits Parent
#Region " Property "
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("STRReportToPPATKViewSelected") Is Nothing, New ArrayList, Session("STRReportToPPATKViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("STRReportToPPATKViewSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("STRReportToPPATKViewFieldSearch") Is Nothing, "", Session("STRReportToPPATKViewFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("STRReportToPPATKViewFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("STRReportToPPATKViewValueSearch") Is Nothing, "", Session("STRReportToPPATKViewValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("STRReportToPPATKViewValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("STRReportToPPATKViewSort") Is Nothing, "PK_CaseManagementID  asc", Session("STRReportToPPATKViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("STRReportToPPATKViewSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("STRReportToPPATKViewCurrentPage") Is Nothing, 0, Session("STRReportToPPATKViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("STRReportToPPATKViewCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("STRReportToPPATKViewRowTotal") Is Nothing, 0, Session("STRReportToPPATKViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("STRReportToPPATKViewRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable() As VList(Of vw_STRReportToPPATK)
        Get
            Session("STRReportToPPATKViewData") = STRReportToPPATKBLL.GetVw_STRReportedToPPATK(SetnGetValueSearch, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, SetnGetRowTotal)
            Return CType(Session("STRReportToPPATKViewData"), VList(Of vw_STRReportToPPATK))
        End Get
        Set(ByVal value As VList(Of vw_STRReportToPPATK))
            Session("STRReportToPPATKViewData") = value
        End Set
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' fill search
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Case ID", "PK_CaseManagementID = -=Search=-"))
            Me.ComboSearch.Items.Add(New ListItem("Customer Name", "CustomerName Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Case Description", "CaseDescription Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Status", "ProposedAction Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Created Date", "CreatedDate Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Last Updated Date", "LastUpdated Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Assigned Branch", "assignedbranch Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("PIC", "PIC Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Is Reported To PPATK", "IsReportedtoRegulator Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Aging", "Aging Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("No STR PPPATK", "PPATKConfirmationno Like '%-=Search=-%'"))

        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("STRReportToPPATKViewSelected") = Nothing
        Session("STRReportToPPATKViewFieldSearch") = Nothing
        Session("STRReportToPPATKViewValueSearch") = Nothing
        Session("STRReportToPPATKViewSort") = Nothing
        Session("STRReportToPPATKViewCurrentPage") = Nothing
        Session("STRReportToPPATKViewRowTotal") = Nothing
        Session("STRReportToPPATKViewData") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()
                Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                Me.ComboSearch.Attributes.Add("onchange", "javascript:CheckNoPopUp('" & Me.TextSearch.ClientID & "');")

                'Using AccessSTRReportToPPATK As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedTableAdapter
                '    'Me.SetnGetBindTable = AccessSTRReportToPPATK.GetData
                'End Using
                Me.FillSearch()
                Me.BindGrid()
                'Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)


                End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            'Dim Rows() As AMLDAL.RulesAdvanced.RulesAdvancedRow = Me.SetnGetBindTable.Select(Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), Me.SetnGetSort)
            Me.GridMSUserView.DataSource = SetnGetBindTable()
            'Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
            'Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridMSUserView.DataBind()
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            'Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.CollectSelected()
            SetnGetValueSearch = ""
            Dim DateSearch As DateTime
            If ComboSearch.SelectedIndex <> 0 And TextSearch.Text <> "" Then
                If ComboSearch.SelectedIndex = 5 Or ComboSearch.SelectedIndex = 6 Then

                    Try
                        DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
                    Catch ex As ArgumentOutOfRangeException
                        Throw New Exception("Input character cannot be convert to datetime.")
                    Catch ex As FormatException
                        Throw New Exception("Unknown format date")
                    End Try
                    Me.SetnGetValueSearch = ComboSearch.SelectedValue.ToString.Replace("-=Search=-", DateSearch.ToString("dd-MMM-yyyy"))

                    If ComboSearch.SelectedIndex = 5 Then

                        SetnGetValueSearch = "convert(datetime,convert(varchar(8),CreatedDate,112)) = '" + DateSearch.ToString("yyyyMMdd") + "'"
                    End If
                    If ComboSearch.SelectedIndex = 6 Then

                        SetnGetValueSearch = "convert(datetime,convert(varchar(8),LastUpdated,112)) = '" + DateSearch.ToString("yyyyMMdd") + "'"
                    End If
                Else
                    SetnGetValueSearch = ComboSearch.SelectedValue.ToString.Replace("-=Search=-", TextSearch.Text)
                End If
                Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
                If Me.ComboSearch.SelectedIndex = 0 Then
                    Me.TextSearch.Text = ""
                End If
            End If
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub



    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim groupID As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(groupID) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(groupID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(groupID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()
        Try
            Dim listPK As New System.Collections.Generic.List(Of String)
            For Each IdPk As Int32 In Me.SetnGetSelectedItem
                listPK.Add(IdPk.ToString)
            Next
            If listPK.Count < 1 Then
                listPK.Add("0")
            End If
            Me.GridMSUserView.DataSource = STRReportToPPATKBLL.GetVw_STRReportedToPPATK("PK_CaseManagementID in (" & String.Join(",", listPK.ToArray) & ")", SetnGetSort, 0, Int32.MaxValue, SetnGetRowTotal)


            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()
            'Sembunyikan kolom ke 0,1,6 & 7 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(13).Visible = False
            'Me.GridMSUserView.Columns(7).Visible = False
            'Me.GridMSUserView.Columns(8).Visible = False
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' export button 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=STRReportToPPATKView.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)

            End If
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                If e.Item.Cells(5).Text <> "&nbsp;" And e.Item.Cells(5).Text <> "" Then
                    e.Item.Cells(5).Text = CDate(e.Item.Cells(5).Text).ToString("dd-MM-yyyy")
                End If
                If e.Item.Cells(6).Text <> "&nbsp;" And e.Item.Cells(6).Text <> "" Then
                    e.Item.Cells(6).Text = CDate(e.Item.Cells(6).Text).ToString("dd-MM-yyyy")
                End If
                If e.Item.Cells(10).Text <> "&nbsp;" And e.Item.Cells(10).Text <> "" Then
                    e.Item.Cells(10).Text = CDate(e.Item.Cells(10).Text).ToString("dd-MM-yyyy")
                End If
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub



    Protected Sub GridMSUserView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.ItemCommand
        If e.CommandName.ToLower = "detail" Then
            Dim strSTRReportToPPATKID As String = e.Item.Cells(1).Text

            Try
                Sahassa.AML.Commonly.SessionIntendedPage = "STRReportToPPATKDetail.aspx?STRReportToPPATKID=" & strSTRReportToPPATKID

                Me.Response.Redirect("STRReportToPPATKDetail.aspx?STRReportToPPATKID=" & strSTRReportToPPATKID, False)

            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub

    Private Sub BindExportAll()
        Try
            Me.GridMSUserView.DataSource = STRReportToPPATKBLL.GetVw_STRReportedToPPATK(SetnGetValueSearch, SetnGetSort, 0, Int32.MaxValue, SetnGetRowTotal)
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(13).Visible = False
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Protected Sub LnkBtnExportAllToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkBtnExportAllToExcel.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindExportAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=STRReportToPPATKView.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
