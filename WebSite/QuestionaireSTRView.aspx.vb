Option Explicit On
Option Strict On
Imports SahassaNettier.Entities
Imports SahassaNettier.Data

Imports AMLBLL

Partial Class QuestionaireSTRView
    Inherits Parent

#Region "SetnGet Searching"
    Public Property SetnGetDescriptionAlertSTR() As String
        Get
            If Not Session("QuestionaireSTRView.SetnGetDescriptionAlertSTR") Is Nothing Then
                Return CStr(Session("QuestionaireSTRView.SetnGetDescriptionAlertSTR"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("QuestionaireSTRView.SetnGetDescriptionAlertSTR") = value
        End Set
    End Property
    Public Property SetnGetQuestionNo() As String
        Get
            If Not Session("QuestionaireSTRView.SetnGetQuestionNo") Is Nothing Then
                Return CStr(Session("QuestionaireSTRView.SetnGetQuestionNo"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("QuestionaireSTRView.SetnGetQuestionNo") = value
        End Set
    End Property
    Public Property SetnGetQuestionType() As String
        Get
            If Not Session("QuestionaireSTRView.SetnGetQuestionType") Is Nothing Then
                Return CStr(Session("QuestionaireSTRView.SetnGetQuestionType"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("QuestionaireSTRView.SetnGetQuestionType") = value
        End Set
    End Property
    Public Property SetnGetQuestion() As String
        Get
            If Not Session("QuestionaireSTRView.SetnGetQuestion") Is Nothing Then
                Return CStr(Session("QuestionaireSTRView.SetnGetQuestion"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("QuestionaireSTRView.SetnGetQuestion") = value
        End Set
    End Property
    Public Property SetnGetMultipleChoice() As String
        Get
            If Not Session("QuestionaireSTRView.SetnGetMultipleChoice") Is Nothing Then
                Return CStr(Session("QuestionaireSTRView.SetnGetMultipleChoice"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("QuestionaireSTRView.SetnGetMultipleChoice") = value
        End Set
    End Property
    Public Property SetnGetActivation() As String
        Get
            If Not Session("QuestionaireSTRView.SetnGetActivation") Is Nothing Then
                Return CStr(Session("QuestionaireSTRView.SetnGetActivation"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("QuestionaireSTRView.SetnGetActivation") = value
        End Set
    End Property
    Private Property SetnGetFieldSearch() As String
        Get
            Return CStr(IIf(Session("QuestionaireSTRViewFieldSearch") Is Nothing, "", Session("QuestionaireSTRViewFieldSearch")))
        End Get
        Set(ByVal Value As String)
            Session("QuestionaireSTRViewFieldSearch") = Value
        End Set
    End Property
    Private Property SetnGetValueSearch() As String
        Get
            Return CStr(IIf(Session("QuestionaireSTRViewValueSearch") Is Nothing, "", Session("QuestionaireSTRViewValueSearch")))
        End Get
        Set(ByVal Value As String)
            Session("QuestionaireSTRViewValueSearch") = Value
        End Set
    End Property
#End Region

#Region "SetGrid"
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("QuestionaireSTRView.SelectedItem") Is Nothing, New ArrayList, Session("QuestionaireSTRView.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("QuestionaireSTRView.SelectedItem") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("QuestionaireSTRView.Sort") Is Nothing, "DescriptionAlertSTR asc", Session("QuestionaireSTRView.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("QuestionaireSTRView.Sort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("QuestionaireSTRView.CurrentPage") Is Nothing, 0, Session("QuestionaireSTRView.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("QuestionaireSTRView.CurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("QuestionaireSTRView.RowTotal") Is Nothing, 0, Session("QuestionaireSTRView.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("QuestionaireSTRView.RowTotal") = Value
        End Set
    End Property
    Public Property SetAndGetSearchingCriteria() As String
        Get
            Return CStr(IIf(Session("MsUserViewAppPageSearchCriteria") Is Nothing, "", Session("MsUserViewAppPageSearchCriteria")))
        End Get
        Set(ByVal value As String)
            Session("MsUserViewAppPageSearchCriteria") = value
        End Set
    End Property

    Public Property SetnGetBindTable() As VList(Of vw_MsQuestionaireSTR)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If

            If ComboSearch.Text = "DescriptionAlertSTR Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "DescriptionAlertSTR like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "QuestionNo Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "QuestionNo like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "QuestionType Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "QuestionType like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "Question Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Question like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "MultipleChoice Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "MultipleChoice like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "Activation Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Activation like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            End If


            strAllWhereClause = String.Join(" and ", strWhereClause)
            'If strAllWhereClause.Trim.Length > 0 Then
            '    strAllWhereClause += " UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "'))"
            'Else
            '    strAllWhereClause += " UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "'))"
            'End If
            Session("QuestionaireSTRView.Table") = DataRepository.vw_MsQuestionaireSTRProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)

            Return CType(Session("QuestionaireSTRView.Table"), VList(Of vw_MsQuestionaireSTR))
        End Get
        Set(ByVal value As VList(Of vw_MsQuestionaireSTR))
            Session("QuestionaireSTRView.Table") = value
        End Set
    End Property

    Public Property SetnGetBindTableAll() As VList(Of vw_MsQuestionaireSTR)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If

            If ComboSearch.Text = "DescriptionAlertSTR Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "DescriptionAlertSTR like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "QuestionNo Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "QuestionNo like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "QuestionType Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "QuestionType like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "Question Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Question like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "MultipleChoice Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "MultipleChoice like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "Activation Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Activation like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            End If


            strAllWhereClause = String.Join(" and ", strWhereClause)
            'If strAllWhereClause.Trim.Length > 0 Then
            '    strAllWhereClause += " and FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            'Else
            '    strAllWhereClause += " FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            'End If
            Session("QuestionaireSTRView.TableALL") = DataRepository.vw_MsQuestionaireSTRProvider.GetPaged(strAllWhereClause, SetnGetSort, 0, Integer.MaxValue, 0)

            Return CType(Session("QuestionaireSTRView.TableALL"), VList(Of vw_MsQuestionaireSTR))
        End Get
        Set(ByVal value As VList(Of vw_MsQuestionaireSTR))
            Session("QuestionaireSTRView.Table") = value
        End Set
    End Property

#End Region

    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Description Alert STR", "DescriptionAlertSTR Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Question No", "QuestionNo Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Question Type", "QuestionType Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Question", "Question Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Multiple Choice", "MultipleChoice Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Activation", "Activation Like '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub

    Private Sub BindGrid()

        Me.GridMSUserView.DataSource = Me.SetnGetBindTable
        Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridMSUserView.DataBind()

    End Sub

    Public Sub BindGridView()
        GridMSUserView.DataSource = Me.SetnGetBindTable
        GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
        GridMSUserView.DataBind()
    End Sub

    Private Sub ClearThisPageSessions()
        SetnGetDescriptionAlertSTR = Nothing
        SetnGetQuestionNo = Nothing
        SetnGetQuestionType = Nothing
        SetnGetQuestion = Nothing
        SetnGetMultipleChoice = Nothing
        SetnGetActivation = Nothing
        SetnGetValueSearch = Nothing
        SetnGetSelectedItem = Nothing

        SetnGetCurrentPage = Nothing
        SetnGetSelectedItem = Nothing
        Me.SetnGetRowTotal = Nothing
        Me.SetnGetSort = Nothing
    End Sub

    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
                Me.SetnGetValueSearch = Me.TextSearch.Text
            Else
                Me.SetnGetValueSearch = Me.TextSearch.Text
            End If
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()

                Me.FillSearch()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '        Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub SetInfoNavigate()
        Me.PageCurrentPage.Text = (Me.SetnGetCurrentPage + 1).ToString
        Me.PageTotalPages.Text = Me.GetPageTotal.ToString
        Me.PageTotalRows.Text = Me.SetnGetRowTotal.ToString
        Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
        Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LinkButtonAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddNew.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "QuestionaireSTRAdd.aspx"

        Me.Response.Redirect("QuestionaireSTRAdd.aspx", False)
    End Sub


#Region "export"
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                Dim PKMsQuestionaireSTRID As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PKMsQuestionaireSTRID) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKMsQuestionaireSTRID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKMsQuestionaireSTRID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridMSUserView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim pkid As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(pkid) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            Else
        '                ArrTarget.Remove(pkid)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Private Sub BindSelectedAll()
        Try
            Me.GridMSUserView.DataSource = SetnGetBindTableAll
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            For i As Integer = 0 To GridMSUserView.Items.Count - 1
                For y As Integer = 0 To GridMSUserView.Columns.Count - 1
                    GridMSUserView.Items(i).Cells(y).Attributes.Add("class", "text")
                Next
            Next
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(1).Visible = False
            Me.GridMSUserView.Columns(7).Visible = False
            Me.GridMSUserView.Columns(9).Visible = False
            Me.GridMSUserView.Columns(10).Visible = False
        Catch
            Throw
        End Try
    End Sub

    Private Sub BindSelected()
        Dim Rows As New ArrayList
        'Dim AllList As VList(Of vw_MsQuestionaireSTR) = DataRepository.vw_MsQuestionaireSTRProvider.GetPaged("", "", 0, Integer.MaxValue, 0)

        For Each IdPk As Long In Me.SetnGetSelectedItem
            'Dim oList As VList(Of vw_MsQuestionaireSTR) = AllList.FindAll(vw_MsQuestionaireSTRColumn.PK_MsQuestionaireSTR_ID, IdPk)
            Dim AllList As VList(Of vw_MsQuestionaireSTR) = DataRepository.vw_MsQuestionaireSTRProvider.GetPaged("PK_MsQuestionaireSTR_ID = '" & IdPk & "'", "", 0, Integer.MaxValue, 0)
            If AllList.Count > 0 Then
                Rows.Add(AllList(0))
            End If
        Next
        Me.GridMSUserView.DataSource = Rows
        Me.GridMSUserView.AllowPaging = False
        Me.GridMSUserView.DataBind()
        For i As Integer = 0 To GridMSUserView.Items.Count - 1
            For y As Integer = 0 To GridMSUserView.Columns.Count - 1
                GridMSUserView.Items(i).Cells(y).Attributes.Add("class", "text")
            Next
        Next

        Me.GridMSUserView.Columns(0).Visible = False
        Me.GridMSUserView.Columns(1).Visible = False
        Me.GridMSUserView.Columns(7).Visible = False
        Me.GridMSUserView.Columns(9).Visible = False
        Me.GridMSUserView.Columns(10).Visible = False
    End Sub

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=QuestionaireSTR.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub lnkExportAllData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAllData.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelectedAll()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=QuestionaireSTRAll.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

#Region "grid"
    
    Protected Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Dim PKMsQuestionaireSTRID As Integer
        Try
            PKMsQuestionaireSTRID = CInt(e.Item.Cells(1).Text)
            Response.Redirect("QuestionaireSTREdit.aspx?PKMsQuestionaireSTRID=" & PKMsQuestionaireSTRID, False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridGMHIPORTJIVE_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.DeleteCommand
        Dim PKMsQuestionaireSTRID As Integer
        Try
            PKMsQuestionaireSTRID = CType(e.Item.Cells(1).Text, Integer)

            Response.Redirect("QuestionaireSTRDelete.aspx?PKMsQuestionaireSTRID=" & PKMsQuestionaireSTRID, False)

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub GridMSUserView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.ItemCommand
        Dim BActivationNeedApproval As Boolean = False
        Dim PKMsQuestionaireSTRID As Integer
        Dim strUserID As String 'uniq
        Try
            If e.CommandName.ToLower = "active" Or e.CommandName.ToLower = "notactive" Then
                PKMsQuestionaireSTRID = CInt(e.Item.Cells(1).Text)
                strUserID = e.Item.Cells(2).Text
                'Using ObjMsHelp As New SahassaBLL.MsHelpBLL
                'If ObjMsHelp.IsNotExistInApproval(strUserID) Then
                Response.Redirect("QuestionaireSTRActivation.aspx?PKMsQuestionaireSTRID=" & PKMsQuestionaireSTRID, False)
                ' End If
                'End Using
            ElseIf e.CommandName.ToLower = "detail" Then
                PKMsQuestionaireSTRID = CInt(e.Item.Cells(1).Text)
                strUserID = e.Item.Cells(2).Text
                'Using ObjMsHelp As New SahassaBLL.MsHelpBLL
                'If ObjMsHelp.IsNotExistInApproval(strUserID) Then
                Response.Redirect("QuestionaireSTRDetail.aspx?PKMsQuestionaireSTRID=" & PKMsQuestionaireSTRID, False)
                ' End If
                'End Using
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            Dim Subject As String
            If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
                Dim PKMsQuestionaireSTRID As String = e.Item.Cells(1).Text
                Dim objcheckbox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                objcheckbox.Checked = Me.SetnGetSelectedItem.Contains(PKMsQuestionaireSTRID)


                Subject = e.Item.Cells(2).Text & " no. " & e.Item.Cells(3).Text

                Dim DetailButton As LinkButton = CType(e.Item.Cells(8).FindControl("LnkDetail"), LinkButton)
                Dim EditButton As LinkButton = CType(e.Item.Cells(10).FindControl("LnkEdit"), LinkButton)
                Dim DelButton As LinkButton = CType(e.Item.Cells(11).FindControl("LnkDelete"), LinkButton)
                Dim Activebutton As LinkButton = CType(e.Item.Cells(9).FindControl("LnkActive"), LinkButton)
                Dim notActiveButton As LinkButton = CType(e.Item.Cells(9).FindControl("lnkNotActive"), LinkButton)
                Dim strActivation As String = e.Item.Cells(7).Text

                If strActivation.ToLower = "true" Then
                    e.Item.Cells(7).Text = "Active"
                    EditButton.Enabled = True
                    DelButton.Enabled = True
                    Activebutton.Enabled = False
                    notActiveButton.Enabled = True
                    DelButton.OnClientClick = CONFIRMATION_BEFOREDELETE(Subject)
                ElseIf strActivation.ToLower = "false" Then
                    e.Item.Cells(7).Text = "Non Active"
                    EditButton.Enabled = False
                    DelButton.Enabled = False
                    Activebutton.Enabled = True
                    notActiveButton.Enabled = False
                End If

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Shared Function CONFIRMATION_BEFOREDELETE(ByVal strKeyUnik As String) As String
        If Not Sahassa.AML.Commonly.SessionLanguage Is Nothing Then
            If Sahassa.AML.Commonly.SessionLanguage.ToLower = "en-us" Then
                Return "javascript:return window.confirm('Are You Sure to delete : " & strKeyUnik & " ? ');"
            ElseIf Sahassa.AML.Commonly.SessionLanguage.ToLower = "id-id" Then
                Return "javascript:return window.confirm('Apakah anda yakin untuk menghapus : " & strKeyUnik & " ? ');"
            Else
                Return "javascript:return window.confirm('Are You Sure to delete : " & strKeyUnik & " ? ');"
            End If

        Else
            Return "javascript:return window.confirm('Are You Sure to delete : " & strKeyUnik & " ? ');"
        End If
    End Function
#End Region


End Class
