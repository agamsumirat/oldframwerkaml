<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LinkAnalysisView.aspx.vb" Inherits="LinkAnalysisView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Link Analysis</title>
    <script src="script/x_core.js"></script>
    <script language=javascript>               
        if (document.layers) { // Netscape
            document.captureEvents(Event.MOUSEMOVE);
            document.onmousemove = captureMousePosition;
        } else if (document.all) { // Internet Explorer
            document.onmousemove = captureMousePosition;
        } else if (document.getElementById) { // Netcsape 6
            document.onmousemove = captureMousePosition;
        }
        // Global variables
        xMousePos = 0; // Horizontal position of the mouse on the screen
        yMousePos = 0; // Vertical position of the mouse on the screen
        xMousePosMax = 0; // Width of the page
        yMousePosMax = 0; // Height of the page

        function captureMousePosition(e) {
            if (document.layers) {

                xMousePos = e.pageX;
                yMousePos = e.pageY;
                xMousePosMax = window.innerWidth+window.pageXOffset;
                yMousePosMax = window.innerHeight+window.pageYOffset;
            } else if (document.all) {
                xMousePos = document.body.scrollLeft + (window.event?window.event.x:0);
                yMousePos = document.body.scrollTop + (window.event?window.event.y:0);
                //window.status ="xMousePos="+xMousePos+";"+"yMousePos="+yMousePos;
                xMousePosMax = document.body.clientWidth+document.body.scrollLeft;
                yMousePosMax = document.body.clientHeight+document.body.scrollTop;
            } else if (document.getElementById) {
                // Netscape 6 behaves the same as Netscape 4 in this regard
                xMousePos = e.pageX;
                yMousePos = e.pageY;
                xMousePosMax = window.innerWidth+window.pageXOffset;
                yMousePosMax = window.innerHeight+window.pageYOffset;
            }
           //window.status = "xMousePos=" + xMousePos + ", yMousePos=" + yMousePos + ", xMousePosMax=" + xMousePosMax + ", yMousePosMax=" + yMousePosMax + " - " + document.documentElement.scrollTop ;
        }
        
        var SelectedCIFNo;
        function ShowMenu(StrCIFNo)
        {
        
              var scrOfX = 0, scrOfY = 0;

              if( typeof( window.pageYOffset ) == 'number' ) {
                //Netscape compliant
                scrOfY = window.pageYOffset;
                scrOfX = window.pageXOffset;
              } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
                //IE6 standards compliant mode
                scrOfY = document.documentElement.scrollTop;
                scrOfX = document.documentElement.scrollLeft;                
              } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
                //DOM compliant
                scrOfY = document.body.scrollTop;
                scrOfX = document.body.scrollLeft;
              }
        
            SelectedCIFNo=StrCIFNo;
            var ele = xGetElementById("popupmenu");
            ele.src="LinkAnalysisViewMenu.aspx"    //kalo yg graphical, diparse nilai str cif no nya.
            xShow(ele);
            xMoveTo(ele,xMousePos+scrOfX,yMousePos+scrOfY);
          }
        
        function HideMenu()
        {
            xHide("popupmenu");
        }
        
        function Expand()
        {
            window.location="LinkAnalysisView.aspx?CIFNo=" + SelectedCIFNo + "&ExpandLink=true";
        }
        
        function ShowCustomerCustomerProfile()
        {
            window.top.location.href="CustomerInformationDetail.aspx?CIFNo=" + SelectedCIFNo;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:CustomValidator ID="cvalPageError" runat="server" Display="Dynamic"></asp:CustomValidator>
        <IMG id="LinkAnalysisImage"  src="~/Images/nodata.gif" border="0"  runat=server visible="false">
        <embed src="./LinkAnalysis.aspx?.svg"  type="image/svg+xml" width="1000" height="1000"  />
        <iframe id="popupmenu" src="LinkAnalysisViewMenu.aspx" name="popupmenu" style="visibility:hidden; z-index:100;position:absolute;" width="150" height="90" scrolling="no" frameborder=no></iframe>        
<%--        <div id="divContext" style="border: 1px solid blue; visibility: hidden;background-color:White; position: absolute;z-index: 100;" >
          <table>
            <tr bgcolor="#ffffff">
                <td align=right><a href="Javascript:HideMenu();">Close</a></td>
            </tr>
            <tr  bgcolor="#ffffff">
                <td><a href="Javascript:Expand();">Expand</a></td>
            </tr>
            <tr  bgcolor="#ffffff">
                <td><a href="Javascript:ShowCustomerCustomerProfile();">Show Customer Profile</a></td>
            </tr>                
          </table>
        </div>--%>
    </form>
</body>
</html>
