Imports System.Data
Imports System.Data.SqlClient
Imports AMLBLL
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Partial Class CreateNewCase 
    Inherits Parent

#Region " Property "
    Private m_AccountOwnerId As Integer
    'sessionnya jangan diganti
    Private Property SetnGetSelectedItemFilter() As ArrayList
        Get
            Return IIf(Session("CustomTransactionAnalysisDetailSelected") Is Nothing, New ArrayList, Session("CustomTransactionAnalysisDetailSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CustomTransactionAnalysisDetailSelected") = value
        End Set
    End Property
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("CustomTransactionAnalysisDetailValueSearch") Is Nothing, "", Session("CustomTransactionAnalysisDetailValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("CustomTransactionAnalysisDetailValueSearch") = Value
        End Set
    End Property

    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("CreateNewCaseDetailFieldSearch") Is Nothing, "", Session("CreateNewCaseDetailFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("CreateNewCaseDetailFieldSearch") = Value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CreateNewCaseDetailSort") Is Nothing, "TransactionDetail.TransactionDetailId  asc", Session("CreateNewCaseDetailSort"))
        End Get
        Set(ByVal Value As String)
            Session("CreateNewCaseDetailSort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CreateNewCaseDetailCurrentPage") Is Nothing, 0, Session("CreateNewCaseDetailCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CreateNewCaseDetailCurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CreateNewCaseDetailRowTotal") Is Nothing, 0, Session("CreateNewCaseDetailRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CreateNewCaseDetailRowTotal") = Value
        End Set
    End Property
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("CreateNewCaseDetailData") Is Nothing, Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Me.PageSize, Me.Fields, Me.SetnGetValueSearch, ""), Session("CreateNewCaseDetailData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CreateNewCaseDetailData") = value
        End Set
    End Property

    Private ReadOnly Property Tables() As String
        Get
            'Return "TransactionDetail "
            'Return "TransactionDetail inner join CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF# " & _
            '"INNER JOIN AccountOwner ON TransactionDetail.AccountOwnerId=AccountOwner.AccountOwnerId"
            Dim StrQuery As New StringBuilder
            StrQuery.Append("TransactionDetail INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF# ")
            StrQuery.Append("INNER JOIN TransactionChannelType ON TransactionDetail.TransactionChannelType = TransactionChannelType.PK_TransactionChannelType ")
            StrQuery.Append("LEFT JOIN JHCOUN ON TransactionDetail.CountryCode=JHCOUN.JHCCOC ")
            StrQuery.Append("INNER JOIN vw_AuxTransactionCode ON TransactionDetail.AuxiliaryTransactionCode=vw_AuxTransactionCode.TransactionCode ")
            StrQuery.Append("INNER JOIN AccountOwner ON TransactionDetail.AccountOwnerId=AccountOwner.AccountOwnerId ")
            Return StrQuery.ToString()
        End Get
    End Property

    Private ReadOnly Property PK() As String
        Get
            Return "TransactionDetail.TransactionDetailId"
        End Get
    End Property

    Private ReadOnly Property Fields() As String
        Get
            'Return "TransactionDetailId, TransactionDate, TransactionChannelType, CountryCode, BankCode, BankName, CIFNo, TransactionDetail.AccountOwnerId, AccountOwner.AccountOwnerName, AccountNo, AccountName, TransactionCode, AuxiliaryTransactionCode, TransactionAmount, CurrencyType, DebitOrCredit, MemoRemark, TransactionRemark, TransactionExchangeRate, TransactionLocalEquivalent, TransactionTicketNo"
            Dim StrQuery As New StringBuilder
            StrQuery.Append("TransactionDetail.TransactionDetailId, TransactionDetail.TransactionDate, TransactionDetail.AccountOwnerId, ")
            StrQuery.Append("CAST(TransactionDetail.AccountOwnerId AS VARCHAR)  + ' - ' + AccountOwner.AccountOwnerName AS AccountOwner, ")
            StrQuery.Append("TransactionChannelType.TransactionChannelTypeName, TransactionDetail.CountryCode + ' - ' + ISNULL(JHCOUN.JHCNAM,'') AS Country, ")
            StrQuery.Append("TransactionDetail.BankCode, TransactionDetail.BankName, ")
            StrQuery.Append("TransactionDetail.CIFNo, TransactionDetail.AccountNo, TransactionDetail.AccountName, ")
            StrQuery.Append("TransactionDetail.AuxiliaryTransactionCode + ' - ' + vw_AuxTransactionCode.TransactionDescription AS AuxiliaryTransactionCode, ")
            StrQuery.Append("TransactionDetail.TransactionAmount, TransactionDetail.CurrencyType, ")
            StrQuery.Append("TransactionDetail.DebitORCredit, TransactionDetail.MemoRemark, ")
            StrQuery.Append("TransactionDetail.TransactionRemark, TransactionDetail.TransactionExchangeRate, ")
            StrQuery.Append("TransactionDetail.TransactionLocalEquivalent, TransactionDetail.TransactionTicketNo, ")
            StrQuery.Append("TransactionDetail.UserId")
            Return StrQuery.ToString()
        End Get
    End Property

    Public Property PageSize() As Int16
        Get
            Return IIf(Session("PageSize") IsNot Nothing, Session("PageSize"), 10)
        End Get
        Set(ByVal value As Int16)
            Session("PageSize") = value
        End Set
    End Property

    Private _RM As String
    Public Property RM() As String
        Get
            Return _RM
        End Get
        Set(ByVal value As String)
            _RM = value
        End Set
    End Property


    Private _SUBSBU As String
    Public Property SUBSBU() As String
        Get
            Return _SUBSBU
        End Get
        Set(ByVal value As String)
            _SUBSBU = value
        End Set
    End Property

    Private _SBU As String
    Public Property SBU() As String
        Get
            Return _SBU
        End Get
        Set(ByVal value As String)
            _SBU = value
        End Set
    End Property
    Private _InsiderCode As String
    Public Property InsiderCode() As String
        Get
            Return _InsiderCode
        End Get
        Set(ByVal value As String)
            _InsiderCode = value
        End Set
    End Property

    Private _VIPCode As String
    Public Property VIPCode() As String
        Get
            Return _VIPCode
        End Get
        Set(ByVal value As String)
            _VIPCode = value
        End Set
    End Property

    Private _Segment As String
    Public Property Segment() As String
        Get
            Return _Segment
        End Get
        Set(ByVal value As String)
            _Segment = value
        End Set
    End Property


    Private Property AccountOwnerId() As Integer
        Get
            Return m_AccountOwnerId
        End Get
        Set(ByVal value As Integer)
            m_AccountOwnerId = value
        End Set
    End Property
#End Region

    Private Sub ClearThisPageSessions()
        Session("CreateNewCaseDetailSelected") = Nothing
        Session("CreateNewCaseDetailFieldSearch") = Nothing
        Session("CreateNewCaseDetailValueSearch") = Nothing
        Session("CreateNewCaseDetailSort") = Nothing
        Session("CreateNewCaseDetailCurrentPage") = Nothing
        Session("CreateNewCaseDetailRowTotal") = Nothing
        Session("CreateNewCaseDetailData") = Nothing
    End Sub

    Private Function FillMsAccountAccountDropDownList() As Boolean
        Using AccountOwnerAdapter As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
            Dim AccountOwnerTable As New AMLDAL.AMLDataSet.AccountOwnerDataTable
            AccountOwnerTable = AccountOwnerAdapter.GetAccountOwnerDataView()
            If Not AccountOwnerTable Is Nothing Then
                Me.AccountOwnerDropdownlist.DataSource = AccountOwnerTable
                Me.AccountOwnerDropdownlist.DataTextField = "AccountOwnerIdName"
                Me.AccountOwnerDropdownlist.DataValueField = "AccountOwnerId"
                Me.AccountOwnerDropdownlist.DataBind()
            End If
        End Using
        Return True
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Me.ClearThisPageSessions()
                Me.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                Me.GridMSUserView.PageSize = Me.PageSize

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                Me.AccountOwnerDropdownlist.Visible = False
                Me.FillMsAccountAccountDropDownList()
                Me.BindGrid()
                Me.IsiListAccountOwner()
                Me.IsiPIC()
                LoadTopology()

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Sub LoadTopology()
        Using ObjQuestionaireSTRBLL As New AMLBLL.QuestionaireSTRBLL
            CboAlertSTR.AppendDataBoundItems = True
            CboAlertSTR.DataSource = ObjQuestionaireSTRBLL.GetLoadCboAlertSTR
            CboAlertSTR.DataTextField = RulesAdvancedColumn.RulesAdvancedName.ToString
            CboAlertSTR.DataValueField = RulesAdvancedColumn.RulesAdvancedName.ToString
            CboAlertSTR.DataBind()
            CboAlertSTR.Items.Insert(0, New ListItem("Please Select Description Alert", ""))
            CboAlertSTR.Items.Insert(1, New ListItem("Financial Profile Abnormal", "Financial Profile Abnormal"))
            CboAlertSTR.Items.Insert(2, New ListItem("Identity Profile", "Identity Profile"))
            CboAlertSTR.Items.Insert(3, New ListItem("Artificial STR", "Artificial STR"))

            Using ObjRulesBasic As TList(Of RulesBasic) = DataRepository.RulesBasicProvider.GetPaged("IsEnabled=1", "", 0, Integer.MaxValue, 0)
                Dim n As Integer = 4
                For Each ObjRulesBasicBindTable As RulesBasic In ObjRulesBasic
                    CboAlertSTR.Items.Insert(n, New ListItem(ObjRulesBasicBindTable.RulesBasicDescription, ObjRulesBasicBindTable.RulesBasicDescription))
                    n = n + 1
                Next
            End Using
        End Using
    End Sub


    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknown Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Sub BindGrid()
        Try
            'Me.SetnGetValueSearch = ""
            Dim ArrTarget As ArrayList = Me.SetnGetSelectedItemFilter
            Dim PkId As String = ""
            If ArrTarget.Count > 0 Then
                For x As Integer = 0 To ArrTarget.Count - 1
                    If x <> 0 Then PkId = PkId & ", "
                    PkId = PkId & ArrTarget(x) & " "
                Next

                Me.SetnGetBindTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Me.PageSize, Me.Fields, Me.SetnGetValueSearch & " And TransactionDetailId in ( " & PkId & ") ", "")
                Me.GridMSUserView.DataSource = Me.SetnGetBindTable
                Dim TotalRow As Int64

                TotalRow = Sahassa.AML.Commonly.GetTableCount(Me.Tables, Me.SetnGetValueSearch & " And TransactionDetailId in ( " & PkId & ") ")
                Me.SetnGetRowTotal = TotalRow
                Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Else
                Me.GridMSUserView.DataSource = Nothing
            End If

            Me.GridMSUserView.DataBind()
        Catch
            Throw
        End Try
    End Sub

    Private Sub GridA_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub IsiListAccountOwner()
        Dim ulang As Integer
        Dim AccountOwnerId As String
        Me.ListAccountOwner.Items.Clear()
        For x As Integer = 0 To Me.GridMSUserView.Items.Count - 1
            ulang = 0
            AccountOwnerId = Me.GridMSUserView.Items(x).Cells(2).Text
            For y As Integer = 0 To x - 1
                If AccountOwnerId = Me.GridMSUserView.Items(y).Cells(2).Text Then
                    ulang = 1
                End If
            Next
            If ulang = 0 Then
                Me.ListAccountOwner.Items.Add(New ListItem(Me.GridMSUserView.Items(x).Cells(3).Text, Me.GridMSUserView.Items(x).Cells(2).Text))
            End If
        Next
        Me.ListAccountOwner.Items.Add(New ListItem("Choose Account Owner", 0))
    End Sub

    Protected Sub IsiPIC()
        Me.AccountOwnerId = Me.ListAccountOwner.SelectedValue
        Me.VIPCode = CboVIPCode.SelectedValue
        Me.InsiderCode = CboInsidercode.SelectedValue
        Me.Segment = CboSegment.SelectedValue
        Me.SBU = cbosbu.SelectedValue
        Me.SUBSBU = cbosubsbu.SelectedValue
        Me.RM = CboRM.SelectedValue

        Me.LabelPIC.Text = Me.GetPICByAccountOwnerId(Me.AccountOwnerId, Me.VIPCode, Me.InsiderCode, Me.Segment, Me.SBU, Me.SUBSBU, Me.RM)
    End Sub

    Protected Sub ImageButtonCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "CustomTransactionAnalysisDetail.aspx"
        Response.Redirect("CustomTransactionAnalysisDetail.aspx", False)
    End Sub

    Private Sub SendEmail(ByVal StrRecipientTo As String, ByVal StrRecipientCC As String, ByVal StrSubject As String, ByVal strbody As String)
        Dim oEmail As Sahassa.AML.EMail
        Try
            oEmail = New Sahassa.AML.EMail
            oEmail.Sender = System.Configuration.ConfigurationManager.AppSettings("FromMailAddress")
            oEmail.Recipient = StrRecipientTo
            oEmail.RecipientCC = StrRecipientCC
            oEmail.Subject = StrSubject
            oEmail.Body = strbody.Replace(vbLf, "<br>")
            oEmail.SendEmail()
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Private Function GetEmailAddress(ByVal PKUserID As String) As String
        Try
            If PKUserID <> "" Then
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Using otable As AMLDAL.AMLDataSet.UserDataTable = adapter.GetDataByUserID(PKUserID)
                        If otable.Rows.Count > 0 Then
                            If CType(otable.Rows(0), AMLDAL.AMLDataSet.UserRow).IsUserEmailAddressNull Then
                                Return ""
                            Else
                                Return CType(otable.Rows(0), AMLDAL.AMLDataSet.UserRow).UserEmailAddress
                            End If
                        Else
                            Return ""
                        End If
                    End Using
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Private Function GetEmailSubject(ByVal PKCMWID As Long, ByVal intSerialNo As Integer) As String
        Try
            Using adapter As New AMLDAL.CaseManagementTableAdapters.CaseManagementWorkflowEmailTemplateTableAdapter

                Using otable As AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateDataTable = adapter.GetEmailTemplate(PKCMWID, intSerialNo)
                    If otable.Rows.Count > 0 Then
                        If Not CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).IsSubject_EmailTemplateNull Then
                            Return CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).Subject_EmailTemplate
                        Else
                            Return ""
                        End If
                    Else
                        Return ""
                    End If

                End Using
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Private Function GetEmailBody(ByVal PKCMWID As Long, ByVal intSerialNo As Integer) As String
        Try
            Using adapter As New AMLDAL.CaseManagementTableAdapters.CaseManagementWorkflowEmailTemplateTableAdapter

                Using otable As AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateDataTable = adapter.GetEmailTemplate(PKCMWID, intSerialNo)
                    If otable.Rows.Count > 0 Then
                        If Not CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).IsBody_EmailTemplateNull Then
                            Return CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).Body_EmailTemplate
                        Else
                            Return ""
                        End If
                    Else
                        Return ""
                    End If

                End Using
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Function GetAccountno() As String
        Dim straccount As String
        For Each item As DataGridItem In GridMSUserView.Items
            If item.ItemType = ListItemType.Item Or item.ItemType = ListItemType.AlternatingItem Then
                straccount = item.Cells(9).Text
                Return straccount
            End If
        Next

    End Function
    Protected Sub ImageButtonSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSave.Click
        Dim Trans As SqlTransaction = Nothing
        Try
            Dim StrWorkflowstep As String = ""
            Dim StrPIC As String = ""
            Dim PK_CaseManagementID As String
            Dim DateCase As DateTime
            Dim CaseManagementWorkflowTable As New AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable
            Dim CaseManagementWorkflowTableRows() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = Nothing
            Dim CaseManagementWorkflowTableRow As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = Nothing

            If Me.ListAccountOwner.SelectedValue = "0" Then
                Me.AccountOwnerId = Me.AccountOwnerDropdownlist.SelectedValue
            Else
                Me.AccountOwnerId = Me.ListAccountOwner.SelectedValue
            End If

            Me.VIPCode = CboVIPCode.SelectedValue
            Me.InsiderCode = CboInsidercode.SelectedValue
            Me.Segment = CboSegment.SelectedValue

            Using AccessTable As New AMLDAL.NewCaseTableAdapters.CaseManagementTableAdapter
                Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessTable)
                Using AdapterCaseManagementWorkflow As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter

                    CaseManagementWorkflowTable = AdapterCaseManagementWorkflow.GetSettingWorkFlowCaseManagementByAccountOwnerID(Me.AccountOwnerId, Me.VIPCode, Me.InsiderCode, Me.Segment, CboAlertSTR.SelectedValue, Me.SBU, Me.SUBSBU, Me.RM)
                    If Not CaseManagementWorkflowTable Is Nothing Then
                        If CaseManagementWorkflowTable.Rows.Count > 0 Then

                            'CaseManagementWorkflowTableRows = CaseManagementWorkflowTable.Select("SerialNo=1 AND Paralel=1")
                            CaseManagementWorkflowTableRows = CaseManagementWorkflowTable.Select("SerialNo=1")
                            If Not CaseManagementWorkflowTableRows Is Nothing Then
                                If CaseManagementWorkflowTableRows.Length > 0 Then

                                    CaseManagementWorkflowTableRow = CaseManagementWorkflowTableRows(0)
                                    If Not CaseManagementWorkflowTableRow.IsApproversNull Then
                                        Dim ArrApprovers() As String = Nothing
                                        ArrApprovers = CaseManagementWorkflowTableRow.Approvers.Split("|"c)
                                        If Not ArrApprovers Is Nothing Then
                                            If ArrApprovers.Length > 0 Then
                                                StrPIC = Me.GetUserIDbyPK(ArrApprovers(0))
                                            End If
                                        End If
                                    End If
                                    If Not CaseManagementWorkflowTableRow.IsWorkflowStepNull Then
                                        StrWorkflowstep = CaseManagementWorkflowTableRow.WorkflowStep
                                    End If
                                End If
                            End If
                        End If
                    End If
                End Using
                DateCase = DateTime.Now()

                Dim dttrans As DataTable = SetnGetBindTable
                Dim strcifno As String = ""
                Dim strcustname As String = ""
                Dim straccountno As String = ""

                If Not dttrans Is Nothing Then
                    If dttrans.Rows.Count > 0 Then
                        strcifno = dttrans.Rows(0).Item("CIFNo").ToString
                        straccountno = dttrans.Rows(0).Item("AccountNo").ToString
                        strcustname = dttrans.Rows(0).Item("AccountName").ToString
                    End If
                End If

                PK_CaseManagementID = AccessTable.InsertCaseManagement(CboAlertSTR.SelectedValue, 1, StrWorkflowstep, DateCase, DateCase, Me.AccountOwnerId, StrPIC, False, 0, False, "", Nothing, Sahassa.AML.Commonly.SessionUserId, "", Nothing, "", strcustname, strcifno, straccountno, Me.VIPCode, Me.InsiderCode, Me.Segment)
                If PK_CaseManagementID > 0 Then
                    ' Insert ke MapCaseManagementTransaction
                    Dim ArrTarget As ArrayList = Me.SetnGetSelectedItemFilter
                    Dim PkId As String = ""
                    If ArrTarget.Count > 0 Then
                        For x As Integer = 0 To ArrTarget.Count - 1
                            If x <> 0 Then PkId = PkId & ", "
                            PkId = PkId & ArrTarget(x) & " "
                        Next
                    End If

                    If PkId <> "" Then
                        Using MapCaseManagementTransactionAdapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementTransactionTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(MapCaseManagementTransactionAdapter, Trans)
                            MapCaseManagementTransactionAdapter.MapCaseManagementTransactionInsertByListOfTransactionId(PK_CaseManagementID, PkId)
                        End Using
                    End If

                    'insert casemanagementprofiletypologi
                    Using objnewtypology As New SahassaNettier.Entities.CaseManagementProfileTopology
                        objnewtypology.FK_CaseManagement_ID = PK_CaseManagementID
                        objnewtypology.ResikoTopology = "H"
                        objnewtypology.AccountNo = getAccountno()
                        objnewtypology.CaseDescription = CboAlertSTR.SelectedValue

                        SahassaNettier.Data.DataRepository.CaseManagementProfileTopologyProvider.Save(objnewtypology)
                    End Using



                    ' Insert ke MapCaseManagementWorkflow
                    Using MapCaseManagementAdapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementWorkflowTableAdapter
                        MapCaseManagementAdapter.Insert(PK_CaseManagementID, "SYSTEM", DateCase, DateCase)
                    End Using



                    Using cmdQUESTION As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_GenerateQuestionCaseManagement")
                        cmdQUESTION.Parameters.Add(New SqlParameter("@PKCaseManagementID", PK_CaseManagementID))
                        cmdQUESTION.Parameters.Add(New SqlParameter("@DescriptioinCaseAlert", CboAlertSTR.SelectedValue))
                        SahassaNettier.Data.DataRepository.Provider.ExecuteNonQuery(cmdQUESTION)
                    End Using

                    ' Insert ke MapCaseManagementWorkflowHistory
                    Using MapCaseManagementHistoryAdapter As New AMLDAL.CaseManagementTableAdapters.SelectMapCaseManagementWorkflowHistoryTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(MapCaseManagementHistoryAdapter, Trans)
                        MapCaseManagementHistoryAdapter.InsertMapCaseManagementWorkflowHistory(PK_CaseManagementID, DateCase, 1, StrWorkflowstep, "", "", Nothing, True, "", Me.AccountOwnerId, Nothing)
                        'MapCaseManagementHistoryAdapter.InsertMapCaseManagementWorkflowHistory(PK_CaseManagementID, DateCase, 2, StrWorkflowstep, StrPIC, "", Nothing, False, "", Me.AccountOwnerId, Nothing)
                        ' Insert to Workflow
                        Dim Counter As Integer
                        Dim ListOfUser As String
                        Dim ApproverCounter As Integer
                        Dim StrEmailRecepients As String
                        Dim StrEmailCC As String
                        Dim StrEmailSubject As String
                        Dim StrEmailBody As String
                        If Not CaseManagementWorkflowTableRows Is Nothing Then
                            If CaseManagementWorkflowTableRows.Length > 0 Then
                                For Counter = 0 To CaseManagementWorkflowTableRows.Length - 1
                                    CaseManagementWorkflowTableRow = CaseManagementWorkflowTableRows(Counter)
                                    ListOfUser = StrPIC
                                    StrEmailRecepients = ""
                                    StrEmailCC = ""
                                    If Not CaseManagementWorkflowTableRow.IsApproversNull Then
                                        Dim ArrApprovers() As String = Nothing
                                        ArrApprovers = CaseManagementWorkflowTableRow.Approvers.Split("|"c)
                                        If Not ArrApprovers Is Nothing Then
                                            If ArrApprovers.Length > 0 Then
                                                ListOfUser = ""

                                                For ApproverCounter = 0 To ArrApprovers.Length - 1
                                                    ListOfUser = ListOfUser & Me.GetUserIDbyPK(ArrApprovers(ApproverCounter)) & ";"
                                                    strEmailRecepients += GetEmailAddress(ArrApprovers(ApproverCounter)) & ";"
                                                Next
                                                If ListOfUser.Length > 0 Then
                                                    ListOfUser = ListOfUser.Remove(ListOfUser.Length - 1)
                                                End If
                                                If strEmailRecepients.Length > 0 Then
                                                    strEmailRecepients = strEmailRecepients.Remove(strEmailRecepients.Length - 1)
                                                End If
                                            End If
                                        End If
                                    End If
                                    If Not CaseManagementWorkflowTableRow.IsNotifyOthersNull Then
                                        Dim ArrOtherApprovers() As String = Nothing
                                        ArrOtherApprovers = CaseManagementWorkflowTableRow.NotifyOthers.Split("|"c)
                                        If Not ArrOtherApprovers Is Nothing Then
                                            If ArrOtherApprovers.Length > 0 Then

                                                For ApproverCounter = 0 To ArrOtherApprovers.Length - 1
                                                    StrEmailCC += GetEmailAddress(ArrOtherApprovers(ApproverCounter)) & ";"
                                                Next
                                                If StrEmailCC.Length > 0 Then
                                                    StrEmailCC = StrEmailCC.Remove(StrEmailCC.Length - 1)
                                                End If
                                            End If
                                        End If
                                    End If

                                    MapCaseManagementHistoryAdapter.InsertMapCaseManagementWorkflowHistory(PK_CaseManagementID, DateCase, 2, StrWorkflowstep, ListOfUser, "", Nothing, False, "", Me.AccountOwnerId, Nothing)
                                    If StrEmailRecepients <> "" Then
                                        StrEmailSubject = GetEmailSubject(CaseManagementWorkflowTableRow.Pk_CMW_Id, CaseManagementWorkflowTableRow.SerialNo)
                                        StrEmailBody = GetEmailBody(CaseManagementWorkflowTableRow.Pk_CMW_Id, CaseManagementWorkflowTableRow.SerialNo)
                                        SendEmail(StrEmailRecepients, StrEmailCC, StrEmailSubject, StrEmailBody)
                                    End If

                                Next
                            End If
                        End If
                    End Using
                End If
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, Trans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CreateNewCase", "CaseDescription", "Add", "", CboAlertSTR.SelectedValue, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CreateNewCase", "Workflowstep", "Add", "", StrWorkflowstep, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CreateNewCase", "CreatedDate", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CreateNewCase", "LastUpdated", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CreateNewCase", "Fk_AccountOwnerId", "Add", "", AccountOwnerId, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CreateNewCase", "PIC", "Add", "", StrPIC, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CreateNewCase", "ProposedBy", "Add", "", Sahassa.AML.Commonly.SessionUserId, "Accepted")
                End Using

                Trans.Commit()
            End Using
            Sahassa.AML.Commonly.SessionIntendedPage = "CustomTransactionAnalysis.aspx"
            Response.Redirect("CustomTransactionAnalysis.aspx", False)
        Catch tex As Threading.ThreadAbortException
            ' ignore
        Catch ex As Exception
            If Not Trans Is Nothing Then
                Trans.Rollback()
            End If
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
            End If
        End Try
    End Sub

    Protected Sub ListAccountOwner_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListAccountOwner.SelectedIndexChanged
        If ListAccountOwner.SelectedValue = "0" Then
            Me.AccountOwnerDropdownlist.Visible = True
        Else
            Me.AccountOwnerDropdownlist.Visible = False

            IsiVIPCode(ListAccountOwner.SelectedValue)
            IsiInsidercode(ListAccountOwner.SelectedValue)
            IsiSegment(ListAccountOwner.SelectedValue)
            IsiPIC()
        End If

    End Sub

    Sub IsiInsidercode(strAccountOwner As String)
        CboInsidercode.Items.Clear()
        For Each item As DataRow In AMLBLL.CreateNewCaseBLL.GetListInsiderCodeWorkflowByAccountOwnerId(strAccountOwner).Rows
            CboInsidercode.Items.Add(New ListItem(item(0).ToString & " - " & item(1).ToString, item(0).ToString))
        Next

    End Sub
    Sub IsiSBU(strAccountOwner As String)
        cbosbu.Items.Clear()
        For Each item As DataRow In AMLBLL.CreateNewCaseBLL.GetListSBUWorkflowByAccountOwnerId(strAccountOwner).Rows
            cbosbu.Items.Add(New ListItem(item(0).ToString & " - " & item(1).ToString, item(0).ToString))
        Next

    End Sub



    Sub IsiSubSBU(strAccountOwner As String)
        cbosubsbu.Items.Clear()
        For Each item As DataRow In AMLBLL.CreateNewCaseBLL.GetListsubSBUWorkflowByAccountOwnerId(strAccountOwner).Rows
            cbosubsbu.Items.Add(New ListItem(item(0).ToString & " - " & item(1).ToString, item(0).ToString))
        Next

    End Sub


    Sub IsiRM(strAccountOwner As String)
        CboRM.Items.Clear()
        For Each item As DataRow In AMLBLL.CreateNewCaseBLL.GetListRMWorkflowByAccountOwnerId(strAccountOwner).Rows
            CboRM.Items.Add(New ListItem(item(0).ToString & " - " & item(1).ToString, item(0).ToString))
        Next

    End Sub
    Sub IsiSegment(strAccountOwner As String)
        CboSegment.Items.Clear()
        For Each item As DataRow In AMLBLL.CreateNewCaseBLL.GetListSegmentWorkflowByAccountOwnerId(strAccountOwner).Rows
            CboSegment.Items.Add(New ListItem(item(0).ToString & " - " & item(1).ToString, item(0).ToString))
        Next

    End Sub
    Sub IsiVIPCode(strAccountOwner As String)
        CboVIPCode.Items.Clear()
        For Each item As DataRow In AMLBLL.CreateNewCaseBLL.GetListVIPCodeWorkflowByAccountOwnerId(strAccountOwner).Rows
            CboVIPCode.Items.Add(New ListItem(item(0).ToString & " - " & item(1).ToString, item(0).ToString))
        Next

    End Sub

    Protected Sub AccountOwnerDropdownlist_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AccountOwnerDropdownlist.SelectedIndexChanged
        Me.AccountOwnerId = Me.AccountOwnerDropdownlist.SelectedValue

        IsiVIPCode(ListAccountOwner.SelectedValue)
        IsiInsidercode(ListAccountOwner.SelectedValue)
        IsiSegment(ListAccountOwner.SelectedValue)
        IsiSBU(ListAccountOwner.SelectedValue)
        IsiSubSBU(ListAccountOwner.SelectedValue)
        IsiRM(ListAccountOwner.SelectedValue)

        Me.VIPCode = CboVIPCode.SelectedValue
        Me.Segment = CboSegment.SelectedValue
        Me.InsiderCode = CboInsidercode.SelectedValue
        Me.SBU = cbosbu.SelectedValue
        Me.SUBSBU = cbosubsbu.SelectedValue
        Me.RM = CboRM.SelectedValue

        Me.LabelPIC.Text = Me.GetPICByAccountOwnerId(Me.AccountOwnerId, Me.VIPCode, Me.InsiderCode, Me.Segment, Me.SBU, Me.SUBSBU, Me.RM)
    End Sub

    Private Function GetUserIDbyPK(ByVal PKUserId As String) As String
        Try
            Dim StrUserId As String = ""
            If PKUserId <> "" Then
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Using oTable As AMLDAL.AMLDataSet.UserDataTable = adapter.GetDataByUserID(PKUserId)
                        If oTable.Rows.Count > 0 Then
                            StrUserId = CType(oTable.Rows(0), AMLDAL.AMLDataSet.UserRow).UserID
                        End If
                    End Using
                End Using
            End If
            Return StrUserId
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function GetPICByAccountOwnerId(ByVal StrAccountOwnerId As String, strVipcode As String, strInsidercode As String, strSegment As String, strsbu As String, strsubsbu As String, strrm As String) As String
        Dim StrUserId As String = ""
        Using AdapterCaseManagementWorkflow As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter
            Dim CaseManagementWorkflowTable As New AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable
            CaseManagementWorkflowTable = AdapterCaseManagementWorkflow.GetSettingWorkFlowCaseManagementByAccountOwnerID(StrAccountOwnerId, strVipcode, strInsidercode, strSegment, CboAlertSTR.SelectedValue, strsbu, strsubsbu, strrm)
            If Not CaseManagementWorkflowTable Is Nothing Then
                If CaseManagementWorkflowTable.Rows.Count > 0 Then
                    Dim CaseManagementWorkflowTableRows() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
                    CaseManagementWorkflowTableRows = CaseManagementWorkflowTable.Select("SerialNo=1 AND Paralel=1")
                    If Not CaseManagementWorkflowTableRows Is Nothing Then
                        If CaseManagementWorkflowTableRows.Length > 0 Then
                            Dim CaseManagementWorkflowTableRow As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
                            CaseManagementWorkflowTableRow = CaseManagementWorkflowTableRows(0)
                            If Not CaseManagementWorkflowTableRow.IsApproversNull Then
                                Dim ArrApprovers() As String = Nothing
                                ArrApprovers = CaseManagementWorkflowTableRow.Approvers.Split("|"c)
                                If Not ArrApprovers Is Nothing Then
                                    If ArrApprovers.Length > 0 Then
                                        StrUserId = Me.GetUserIDbyPK(ArrApprovers(0))
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End Using
        Return StrUserId
    End Function

    Protected Sub CboVIPCode_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CboVIPCode.SelectedIndexChanged
        Try
            IsiPIC()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub CboInsidercode_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CboInsidercode.SelectedIndexChanged
        Try
            IsiPIC()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub CboSegment_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CboSegment.SelectedIndexChanged
        Try
            IsiPIC()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class