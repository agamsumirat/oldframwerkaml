﻿Imports System.Data

Partial Class ShowTaskList
    Inherits System.Web.UI.Page

    Public Property UID As String
        Get
            Return Session("ShowTaskList.uid")
        End Get
        Set
            Session("ShowTaskList.uid") = Value
        End Set
    End Property


    Private Property Sort() As String
        Get
            Return IIf(Session("ShowTaskList.Sort") Is Nothing, "", Session("ShowTaskList.Sort"))
        End Get
        Set(ByVal value As String)
            Session("ShowTaskList.Sort") = value
        End Set
    End Property
    Private Sub ShowTaskList_Load(sender As Object, e As EventArgs) Handles Me.Load

        Try
            If Not IsPostBack Then

                UID = Request.Params("uid")
                If UID IsNot Nothing Then
                    'LoadDataWorldcheck(UID)
                    RebindDataSourceGrid()
                End If

            End If


        Catch ex As Exception

        End Try
    End Sub

    'Sub LoadDataWorldcheck(struid As String)
    '    Using objdt As Data.DataTable = EKABLL.ScreeningBLL.GetWorldcheckData(struid)
    '        If objdt.Rows.Count > 0 Then

    '            LblData0.Text = objdt.Rows(0).Item(0)
    '            LblData1.Text = objdt.Rows(0).Item(1)
    '            LblData2.Text = objdt.Rows(0).Item(2)
    '            LblData3.Text = objdt.Rows(0).Item(3)
    '            LblData4.Text = objdt.Rows(0).Item(4)
    '            LblData5.Text = objdt.Rows(0).Item(5)
    '            LblData6.Text = objdt.Rows(0).Item(6)
    '            LblData7.Text = objdt.Rows(0).Item(7)
    '            LblData8.Text = objdt.Rows(0).Item(8)
    '            LblData9.Text = objdt.Rows(0).Item(9)
    '            LblData10.Text = objdt.Rows(0).Item(10)


    '            Dim strdob As String
    '            Try
    '                strdob = Convert.ToDateTime(objdt.Rows(0).Item(11)).ToString("dd-MMM-yyyy")
    '            Catch ex As Exception
    '                strdob = objdt.Rows(0).Item(11)
    '            End Try

    '            LblData11.Text = strdob
    '            LblData12.Text = objdt.Rows(0).Item(12)
    '            LblData13.Text = objdt.Rows(0).Item(13)
    '            LblData14.Text = objdt.Rows(0).Item(14)
    '            LblData15.Text = objdt.Rows(0).Item(15)
    '            LblData16.Text = objdt.Rows(0).Item(16)
    '            LblData17.Text = objdt.Rows(0).Item(17)
    '            LblData18.Text = objdt.Rows(0).Item(18)
    '            LblData19.Text = objdt.Rows(0).Item(19)
    '            LblData20.Text = objdt.Rows(0).Item(20)
    '            LblData21.Text = objdt.Rows(0).Item(21)
    '            LblData22.Text = objdt.Rows(0).Item(22)
    '            LblData23.Text = objdt.Rows(0).Item(23)
    '            LblData24.Text = objdt.Rows(0).Item(24)

    '            Dim strentered As String
    '            Try
    '                strentered = Convert.ToDateTime(objdt.Rows(0).Item(25)).ToString("dd-MMM-yyyy")
    '            Catch ex As Exception
    '                strentered = objdt.Rows(0).Item(25)
    '            End Try
    '            LblData25.Text = strentered

    '            Dim strupdate As String
    '            Try
    '                strupdate = Convert.ToDateTime(objdt.Rows(0).Item(26)).ToString("dd-MMM-yyyy")
    '            Catch ex As Exception
    '                strupdate = objdt.Rows(0).Item(26)
    '            End Try

    '            LblData26.Text = strupdate
    '            LblData27.Text = objdt.Rows(0).Item(27)
    '            LblData28.Text = objdt.Rows(0).Item(28)



    '        End If
    '    End Using

    'End Sub

    'Sub LoadTaskList(struid As String)

    '    EKABLL.CustomTaskListBLL.GetAllCustomTaskListModule(Sahassa.AML.Commonly.SessionUserId)

    '    Using objdt As Data.DataTable = EKABLL.ScreeningBLL.GetWorldcheckData(struid)
    '        If objdt.Rows.Count > 0 Then

    '            LblData0.Text = objdt.Rows(0).Item(0)
    '            LblData1.Text = objdt.Rows(0).Item(1)
    '            LblData2.Text = objdt.Rows(0).Item(2)
    '            LblData3.Text = objdt.Rows(0).Item(3)
    '            LblData4.Text = objdt.Rows(0).Item(4)
    '            LblData5.Text = objdt.Rows(0).Item(5)
    '            LblData6.Text = objdt.Rows(0).Item(6)
    '            LblData7.Text = objdt.Rows(0).Item(7)
    '            LblData8.Text = objdt.Rows(0).Item(8)
    '            LblData9.Text = objdt.Rows(0).Item(9)
    '            LblData10.Text = objdt.Rows(0).Item(10)


    '            Dim strdob As String
    '            Try
    '                strdob = Convert.ToDateTime(objdt.Rows(0).Item(11)).ToString("dd-MMM-yyyy")
    '            Catch ex As Exception
    '                strdob = objdt.Rows(0).Item(11)
    '            End Try

    '            LblData11.Text = strdob
    '            LblData12.Text = objdt.Rows(0).Item(12)
    '            LblData13.Text = objdt.Rows(0).Item(13)
    '            LblData14.Text = objdt.Rows(0).Item(14)
    '            LblData15.Text = objdt.Rows(0).Item(15)
    '            LblData16.Text = objdt.Rows(0).Item(16)
    '            LblData17.Text = objdt.Rows(0).Item(17)
    '            LblData18.Text = objdt.Rows(0).Item(18)
    '            LblData19.Text = objdt.Rows(0).Item(19)
    '            LblData20.Text = objdt.Rows(0).Item(20)
    '            LblData21.Text = objdt.Rows(0).Item(21)
    '            LblData22.Text = objdt.Rows(0).Item(22)
    '            LblData23.Text = objdt.Rows(0).Item(23)
    '            LblData24.Text = objdt.Rows(0).Item(24)

    '            Dim strentered As String
    '            Try
    '                strentered = Convert.ToDateTime(objdt.Rows(0).Item(25)).ToString("dd-MMM-yyyy")
    '            Catch ex As Exception
    '                strentered = objdt.Rows(0).Item(25)
    '            End Try
    '            LblData25.Text = strentered

    '            Dim strupdate As String
    '            Try
    '                strupdate = Convert.ToDateTime(objdt.Rows(0).Item(26)).ToString("dd-MMM-yyyy")
    '            Catch ex As Exception
    '                strupdate = objdt.Rows(0).Item(26)
    '            End Try

    '            LblData26.Text = strupdate
    '            LblData27.Text = objdt.Rows(0).Item(27)
    '            LblData28.Text = objdt.Rows(0).Item(28)



    '        End If
    '    End Using

    'End Sub

    Protected Sub grdTaskList_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdTaskList.PageIndexChanging
        Try
            grdTaskList.PageIndex = e.NewPageIndex
            RebindDataSourceGrid()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub RebindDataSourceGrid()

        'Ambil semuanya dulu berdasarkan PK
        Dim allDataSet As DataSet = Sahassa.AML.Commonly.ExecuteDataset("SELECT * FROM CustomTaskList WHERE PK_CustomTaskList_ID = " & UID)

        'Ambil kode SQL nya dan jalankan

        Dim strsql As String = allDataSet.Tables(0).Rows(0).Item("QuerySQL").ToString()
        strsql = strsql.ToLower.Replace("@userid", Sahassa.AML.Commonly.SessionUserId)


        Dim currentDataSet As DataSet = Sahassa.AML.Commonly.ExecuteDataset(strsql)

        'Tampilkan
        grdTaskList.DataSource = currentDataSet.Tables(0)
        grdTaskList.DataBind()
    End Sub

    Private Sub grdTaskList_Sorting(sender As Object, e As GridViewSortEventArgs) Handles grdTaskList.Sorting
        Try
            Dim strsort As String
            If e.SortDirection = SortDirection.Ascending Then
                strsort = "  asc"
            Else
                strsort = " desc"
            End If
            If Me.Sort.ToLower.Length > 5 Then
                If Left(Me.Sort.ToLower, Me.Sort.Length - 5).Trim = "[" & e.SortExpression.ToLower & "]" Then
                    Me.Sort = Sahassa.AML.Commonly.ChangeSortCommand(Me.Sort)
                Else
                    Me.Sort = "[" & e.SortExpression & "]" & strsort
                End If
            Else
                Me.Sort = "[" & e.SortExpression & "]" & strsort
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class
