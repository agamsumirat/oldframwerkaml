﻿#Region "Imports"
Imports System.IO
Imports System.Data
'Imports Sahassanettier.Data
'Imports SahassaNettier.Entities
Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports CDDLNPNettier.Services
Imports AMLBLL
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
Imports System
#End Region

Partial Class Bancassurance_Upload
    Inherits Parent
    Private ReadOnly BindGridFromExcel As Boolean

    Structure GrouP_BA_success
        Dim WU As Bancassurance_Approval_Detail
        Dim WU_ad As TList(Of Bancassurance_Approval_Detail)
        Dim Dt_WU As DataTable
    End Structure

#Region "Property"
    Private Property GroupTableSuccessData() As List(Of GrouP_BA_success)
        Get
            Return Session("Bancassurance_Upload.ObjDataValid")
        End Get
        Set(ByVal value As List(Of GrouP_BA_success))
            Session("Bancassurance_Upload.ObjDataValid") = value
        End Set
    End Property
    Public Property ObjDataValid() As Data.DataTable
        Get
            Return CType(Session("Bancassurance_Upload.ObjDataValid"), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("Bancassurance_Upload.ObjDataValid") = value
        End Set
    End Property

    Public Property ObjDataInValid() As Data.DataTable
        Get
            Return CType(Session("Bancassurance_Upload.ObjDataInValid"), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("Bancassurance_Upload.ObjDataInValid") = value
        End Set
    End Property
    Public Property ObjDataApprovalDetail() As Bancassurance_Approval_Detail
        Get
            Return CType(Session("Bancassurance_Upload.ObjDataValid"), Bancassurance_Approval_Detail)
        End Get
        Set(ByVal value As Bancassurance_Approval_Detail)
            Session("Bancassurance_Upload.ObjDataValid") = value
        End Set
    End Property
    Public Property ObjDatavalidate() As Bancassurance_Valid_Upload
        Get
            Return CType(Session("Bancassurance_Upload.ObjDataValid"), Bancassurance_Valid_Upload)
        End Get
        Set(ByVal value As Bancassurance_Valid_Upload)
            Session("Bancassurance_Upload.ObjDataValid") = value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("WUUpload.CurrentPage") Is Nothing, 0, Session("WUUpload.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("WUUpload.CurrentPage") = Value
        End Set
    End Property
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("WUUpload.SelectedItem") Is Nothing, New ArrayList, Session("WUUpload.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("WUUpload.SelectedItem") = value
        End Set
    End Property

#End Region

    Sub ClearSession()
        Session("Bancassurance_Upload.ObjDataValid") = Nothing
        Session("Bancassurance_Upload.ObjDataInValid") = Nothing
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        Try
            If Not Me.IsPostBack Then
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                Using ObjAuditTrailUserAccess As SahassaNettier.Entities.AuditTrail_UserAccess = New SahassaNettier.Entities.AuditTrail_UserAccess
                    With ObjAuditTrailUserAccess
                        .AuditTrail_UserAccessUserid = Sahassa.AML.Commonly.SessionUserId
                        .AuditTrail_UserAccessActionDate = DateTime.Now
                        .AuditTrail_UserAccessAction = UserAccessAction
                    End With
                    SahassaNettier.Data.DataRepository.AuditTrail_UserAccessProvider.Save(ObjAuditTrailUserAccess)
                End Using

            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridSuccessList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridSuccessList.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                If BindGridFromExcel = True Then
                    e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1) + (GridSuccessList.CurrentPageIndex * GetDisplayedTotalRow))
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridSuccessList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridSuccessList.PageIndexChanged
        Try
            GridSuccessList.CurrentPageIndex = e.NewPageIndex
            GridSuccessList.DataSource = ObjDataValid
            GridSuccessList.DataBind()
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridAnomalyList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridAnomalyList.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                'chkBox.Checked = SetnGetSelectedItem.Contains(e.Item.Cells(2).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(1).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(1).Text = CStr((e.Item.ItemIndex + 1) + (GridAnomalyList.CurrentPageIndex * GetDisplayedTotalRow))
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridAnomalyList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridAnomalyList.PageIndexChanged
        Try
            GridAnomalyList.CurrentPageIndex = e.NewPageIndex
            GridAnomalyList.DataSource = ObjDataInValid
            GridAnomalyList.DataBind()
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgUpload.Click
        Try
            If FileUploadAttachment.HasFile Then
                ClearSession()

                Dim lengthFileBtyes As Long = FileUploadAttachment.PostedFile.ContentLength
                Dim lengthFileKB As Long = CLng(lengthFileBtyes / 1024)
                Dim strfilename As String
                'If lengthFileKB > CDbl(System.Configuration.ConfigurationManager.AppSettings("MaxFileSize")) Then
                '    Throw New Exception("File Size Must lower than " & System.Configuration.ConfigurationManager.AppSettings("MaxFileSize") & " KB .Your File Size are " & lengthFileKB & " KB")
                'End If
                Dim strext As String = System.IO.Path.GetExtension(FileUploadAttachment.FileName)
                If Not (strext.ToString.ToLower <> ".xls" Or strext.ToString.ToLower <> ".xlsx") Then
                    Throw New Exception("Invalid File Upload Extension")
                End If

                Dim strTempfile As String = System.IO.Path.GetTempFileName
                Dim strxlsfile As String = IO.Path.ChangeExtension(strTempfile, strext)

                FileUploadAttachment.SaveAs(strxlsfile)

                Using objwt As New Bancassurance_UploadBLL

                    Dim strError As String = ""
                    objwt.ProcessExcelbaru(strxlsfile, ObjDataValid, ObjDataInValid, strError)
                    'objwt.ProcessExcel(strxlsfile, ObjDataValid, ObjDataInValid, strError)

                    GridAnomalyList.DataSource = ObjDataInValid
                    GridAnomalyList.PageSize = GetDisplayedTotalRow
                    GridAnomalyList.DataBind()

                    GridSuccessList.DataSource = ObjDataValid
                    GridSuccessList.PageSize = GetDisplayedTotalRow
                    GridSuccessList.DataBind()
                    If strError.Trim.Length > 0 Then
                        BtnSave.Visible = False
                        BtnCancel.Visible = True
                        MultiViewKPIOvertimeUpload.ActiveViewIndex = 1
                    Else
                        BtnSave.Visible = True
                        BtnCancel.Visible = True
                        MultiViewKPIOvertimeUpload.ActiveViewIndex = 1
                    End If
                    'txtError.Text = strError
                    'PanelResult.Visible = True
                    lblValueFailedData.Text = ObjDataInValid.Rows.Count.ToString & " record"
                    lblValueSuccessData.Text = ObjDataValid.Rows.Count.ToString & " record"
                    lblValueuploadeddata.Text = (ObjDataInValid.Rows.Count + ObjDataValid.Rows.Count).ToString & " record"

                    'If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    '    LblMessage.Text = "Data Workflow Saved."
                    'Else
                    '    LblMessage.Text = "Data Workflow Saved into Pending Approval."
                    'End If
                End Using
            Else
                Throw New Exception("Pilih file untuk di upload terlebih dahulu kemudian klik tombol upload.")
            End If
            'End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            'Session("uploadexcel") = Nothing
        End Try
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Try
            ClearSession()
            lblValueFailedData.Text = ""
            lblValueSuccessData.Text = ""
            lblValueuploadeddata.Text = ""
            MultiViewKPIOvertimeUpload.ActiveViewIndex = 0

            Dim deleteValidate As TList(Of Bancassurance_Valid_Upload) = DataRepository.Bancassurance_Valid_UploadProvider.GetPaged(Bancassurance_Valid_UploadColumn.User_ID.ToString & "=" & SessionPkUserId.ToString, "", 0, Integer.MaxValue, 0)
            DataRepository.Bancassurance_Valid_UploadProvider.Delete(deleteValidate)

        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Try

            If ObjDataValid Is Nothing OrElse ObjDataValid.Rows.Count = 0 Then
                Throw New Exception("Tidak Ada data valid untuk di simpan.")
            End If

            Using insertapproval As New Bancassurance_Approval


                With insertapproval
                    '.PK_WO_Approval_Id = 
                    .RequestedBy = Sahassa.AML.Commonly.SessionPkUserId
                    .RequestedDate = Now
                    .FK_MsMode_Id = 1
                End With
                DataRepository.Bancassurance_ApprovalProvider.Save(insertapproval)


                Bancassurance_UploadBLL.SaveAdd(CStr(Sahassa.AML.Commonly.SessionPkUserId), insertapproval.PK_Bancassurance_Approval_Id)
            End Using
            LblConfirmation.Text = "Bancassurance data has been add to list approval (" & ObjDataValid.Rows.Count & " Rows / Bancassurance)"
            MultiViewKPIOvertimeUpload.ActiveViewIndex = 2

            'Bancassurance_Upload.SaveAdd(CStr(Sahassa.CommonCTRWeb.Commonly.SessionPkUserId))
            'RegisterClientScriptBlock(System.Guid.NewGuid.ToString, "  window.alert('WU Data Berhasil Di Simpan !')")
            'BtnSave_Click(Nothing, Nothing)

        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        MultiViewKPIOvertimeUpload.ActiveViewIndex = 0
    End Sub

    Protected Sub BtnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportExcel.Click
        Try
            Const strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelectedAll()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=WUUploadlAll.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            EnableViewState = False
            Using stringWrite As System.IO.StringWriter = New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                ClearControls(GridAnomalyList)
                GridAnomalyList.RenderControl(htmlWrite)
                Response.Write(strStyle)
                Response.Write(stringWrite.ToString())
            End Using
            Response.End()

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                    'Skip
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub
    Private Sub BindSelectedAll()
        Try

            Dim getdatabind As TList(Of Bancassurance_Valid_Upload) = DataRepository.Bancassurance_Valid_UploadProvider.GetPaged("user_id = " & Trim(SessionPkUserId.ToString), "", 0, Integer.MaxValue, 0)

            GridAnomalyList.DataSource = getdatabind
            GridAnomalyList.AllowPaging = False
            GridAnomalyList.DataBind()

            For i As Integer = 0 To GridAnomalyList.Items.Count - 1
                For y As Integer = 0 To GridAnomalyList.Columns.Count - 1
                    GridAnomalyList.Items(i).Cells(y).Attributes.Add("class", "text")
                Next
            Next
            HideButtonGrid()
        Catch
            Throw
        End Try

    End Sub
    Private Sub HideButtonGrid()
        For Each objColumn As DataGridColumn In GridAnomalyList.Columns
            objColumn.Visible = True
        Next
        With GridAnomalyList
            .Columns(1).Visible = False
        End With
    End Sub
    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim TotalRow As Int16 = 0
        For Each gridRow As DataGridItem In GridAnomalyList.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                TotalRow = CType(TotalRow + 1, Int16)
            End If
        Next

    End Sub
End Class
