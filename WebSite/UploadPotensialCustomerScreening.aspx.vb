﻿Partial Class UploadPotensialCustomerScreening
    Inherits Parent

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered 

    End Sub
    Protected Sub LnkDownloadTemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkDownloadTemplate.Click
        Try

            Dim fileContents As Byte()
            fileContents = My.Computer.FileSystem.ReadAllBytes(Server.MapPath("~\template\PotensialCustomerTemplate.xls"))




            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=Potensial Customer Screening Template.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            Response.ContentType = "application/vnd.xls"
            Response.BinaryWrite(fileContents)
            Response.End()

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub ImgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgSearch.Click
        Try

            If FileUpload1.HasFile Then
                Dim DescPath As String = Server.MapPath("~\FolderExport\" & Guid.NewGuid.ToString & ".xls")
                FileUpload1.SaveAs(DescPath)
                EKABLL.ScreeningBLL.ProcessExcelBaru(DescPath, "")

                BindGrid()




            End If

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        Finally
            MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(10000)
        End Try
    End Sub


    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not Page.IsPostBack Then
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using Transcope As New Transactions.TransactionScope

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(10000)

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub


    Public Function GetProgressSearching(ByVal struserid As String) As Data.DataTable
        Using objCommand As System.Data.SqlClient.SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_GetTableProgressBulkSearching")
            objCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@userid", struserid))
            Return SahassaNettier.Data.DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using
    End Function

    Sub BindGrid()
        GrdViewResult.DataSource = EKABLL.ScreeningBLL.GetBulkSearchingData(Sahassa.AML.Commonly.SessionUserId)
        GrdViewResult.DataBind()
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindGrid()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GrdViewResult_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GrdViewResult.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim objrow As Data.DataRowView = e.Row.DataItem
                Dim status As String = Sahassa.AML.Commonly.getStringFieldValue(objrow.Item(4))

                Dim objlnkdownload As LinkButton = e.Row.FindControl("LnkDownloadFile")
                objlnkdownload.ToolTip = objrow(0)
                If status = "Finish" Then
                    objlnkdownload.Visible = True
                Else
                    objlnkdownload.Visible = False
                End If


            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub LnkDownloadFile_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Try
                Dim objgridviewrow As GridViewRow = CType(sender, LinkButton).NamingContainer
                Dim objlinkbutton As LinkButton = CType(sender, LinkButton)
                Dim pk As Long = GrdViewResult.DataKeys(objgridviewrow.DataItemIndex).Value

                Dim strfile As Byte() = EKABLL.ScreeningBLL.GenerateFileExcel(pk)


                Response.Clear()
                Response.ClearHeaders()
                Response.AddHeader("content-disposition", "attachment;filename=Potensial Customer Screening Result.xls")
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "ContentType"
                Response.BinaryWrite(strfile)
                Response.End()



            Catch ex As Exception

            End Try
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class