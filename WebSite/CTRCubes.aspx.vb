Imports OWC11
Imports System.Configuration
Partial Class CTRCubes
    Inherits Parent

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Dim strXMLData As String
                strXMLData = GetDataCube()
                ClientScript.RegisterHiddenField("txtXMLData", strXMLData)

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Private Function GetDataCube() As String
        Dim m_XML As String
        Try
            Dim objPivot As PivotTableClass = New PivotTableClass
            Dim objPivotView As PivotView
            Dim FSetDimensionDate As PivotFieldSet
            Dim FSetDimensionWorkingUnit As PivotFieldSet

            'Dim objPT As PivotTableClass = New PivotTableClass
            objPivot.ConnectionString = System.Configuration.ConfigurationManager.AppSettings.Item("OLAPConnectionString")
            objPivot.DataMember = "CTRCubes"
            objPivotView = objPivot.ActiveView
            objPivotView.TitleBar.Visible = False

            FSetDimensionDate = objPivotView.FieldSets("Time")
            FSetDimensionWorkingUnit = objPivotView.FieldSets("Working Unit")

            objPivotView.DataAxis.InsertTotal(objPivotView.Totals("Total Case"))
            objPivotView.DataAxis.InsertTotal(objPivotView.Totals("Total New Case"))

            objPivotView.RowAxis.InsertFieldSet(FSetDimensionWorkingUnit)
            objPivotView.ColumnAxis.InsertFieldSet(FSetDimensionDate)
            'objPivotView.ColumnAxis.InsertFieldSet(objPivotView.FieldSets("Data Updating Month"))
            'objPivotView.ColumnAxis.InsertFieldSet(objPivotView.FieldSets("Data Updating Day"))

            'RemoveAlltotals(objPivotView.FieldSets("Data Updating Year"))
            'RemoveAlltotals(objPivotView.FieldSets("Data Updating Month"))
            'RemoveAlltotals(objPivotView.FieldSets("Data Updating Day"))
            'RemoveAlltotals(objPivotView.FieldSets("Risk Level"))
            objPivot.ActiveData.HideDetails()

            objPivotView.ExpandMembers = PivotTableExpandEnum.plExpandNever
            m_XML = objPivot.XMLData

            objPivot = Nothing

        Catch err As Exception
            m_XML = "<err>" & err.Source & " - " & err.Message & "</err>"
        Finally

        End Try
        Return (m_XML)
    End Function

    Private Sub RemoveAlltotals(ByVal varFset As PivotFieldSet)
        Dim i As Integer
        For i = 0 To varFset.Fields.Count - 1
            varFset.Fields(i).Subtotals(i) = False
        Next
    End Sub
End Class
