<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="VerificationListManagementApprovalDetail.aspx.vb" Inherits="VerificationListManagementApprovalDetail" title="Verification List Management Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <TABLE id="TableDetail" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="2" runat="server" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
   
        <tr class="formText" id="UserAdd1">
            <td bgcolor="#ffffff" height="24" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Name/Alias
            </td>
            <td bgcolor="#ffffff" style="width: 15px">
                :
            </td>
            <td bgcolor="#ffffff" width="80%">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewAliasesAdd" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2" Font-Size="XX-Small"
                        ForeColor="Black" HorizontalAlign="Left" Width="300px">
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Aliases" HeaderText="Alias ">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                    </asp:DataGrid></div>
            </td>
        </tr>
        <tr class="formText" height="15" id="UserAdd2">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" width="80%" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
        </tr>
          <tr class="formText" id="UserAddDateOfData">
            <td bgcolor="#ffffff" height="24" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Date of Data</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff" width="80%">
                <asp:Label ID="LabelDateOfData" runat="server"></asp:Label></td>
        </tr>	        
          <tr class="formText" id="UserAdd3">
            <td bgcolor="#ffffff" height="24" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Date of Birth</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff" width="80%">
                <asp:Label ID="LabelDOBAdd" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserAdd4">
            <td bgcolor="#ffffff" height="24" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                Birth Place</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff" width="80%">
                <asp:Label Text="" runat="server" ID="LblBirthPlace" /></td>
        </tr>
        <tr class="formText" id="UserAdd12">
            <td bgcolor="#ffffff" height="24" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Address
            </td>
            <td bgcolor="#ffffff" style="width: 15px">
                :
            </td>
            <td bgcolor="#ffffff" width="80%">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewAddressesAdd" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                        Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Addresses" HeaderText="Address">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AddressType" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AddressTypeLabel" HeaderText="Address Type">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IsLocalAddress" HeaderText="Local Address?">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" />
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                    </asp:DataGrid></div>
            </td>
        </tr>
        <tr class="formText" height="15" id="UserAdd5">
            <td bgcolor="#ffffff"  style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" width="80%" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
        </tr>
        <tr class="formText" id="UserAdd6">
            <td bgcolor="#ffffff" height="24" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                ID Number</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff" width="80%">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewIDNosAdd" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                        Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IDNos" HeaderText="ID Number">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                    </asp:DataGrid></div>
            </td>
        </tr>
        <tr class="formText" height="15" id="UserAdd7">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" width="80%" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
        </tr>      
        <tr class="formText" id="UserAdd8">
            <td bgcolor="#ffffff" height="24" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Verification List Type</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelListTypeAdd" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserAdd9">
            <td bgcolor="#ffffff" height="24" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Category</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelCategoryAdd" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserAdd10">
            <td bgcolor="#ffffff" height="24" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Custom Remark(s)</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewCustomRemarksAdd" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                        Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CustomRemarks" HeaderText="Custom Remark">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid></div>
            </td>
        </tr>
        <tr class="formText" id="UserAdd11">
            <td bgcolor="#ffffff" height="24" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                Nationality</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNationalityAdd" runat="server"></asp:Label></td>
        </tr>
	    <tr class="formText" id="UserEdi1">
		    <td height="24" colspan="4" bgcolor="#ffffcc">&nbsp;Old Values</td>
		    <td colspan="4" bgcolor="#ffffcc">&nbsp;New Values</td>
	    </tr>
	    <tr class="formText" id="UserEdi2">
		    <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">&nbsp;</td>
		    <td bgcolor="#ffffff" style="height: 32px;">
                Verification
                List ID</td>
		    <td bgcolor="#ffffff" style="width: 44px; height: 32px;">:</td>
		    <td width="40%" bgcolor="#ffffff" style="height: 32px"><asp:label id="LabelOldListID" runat="server"></asp:label></td>
		    <td width="5" bgcolor="#ffffff" style="height: 32px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">&nbsp;</td>
		    <td width="10%" bgcolor="#ffffff" style="height: 32px">
                Verification
                List ID</td>
		    <td width="5" bgcolor="#ffffff" style="height: 32px">:</td>
		    <td width="40%" bgcolor="#ffffff" style="height: 32px"><asp:label id="LabelNewListID" runat="server"></asp:label></td>
	    </tr>
   
        <tr class="formText" id="UserEdi3">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Name/Alias
            </td>
            <td bgcolor="#ffffff" style="width: 15px">
                :
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 32px; border-bottom-style: none">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewAliasesOld" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2" Font-Size="XX-Small"
                        ForeColor="Black" HorizontalAlign="Left" Width="300px">
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Aliases" HeaderText="Alias ">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                    </asp:DataGrid></div>
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 32px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff">
                Name/Alias
            </td>
            <td bgcolor="#ffffff">
                :
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 32px; border-bottom-style: none">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewAliasesNew" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2" Font-Size="XX-Small"
                        ForeColor="Black" HorizontalAlign="Left" Width="300px">
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Aliases" HeaderText="Alias ">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                    </asp:DataGrid></div>
            </td>
        </tr>
        <tr class="formText" height="15" id="UserEdi4">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none; height: 15px;">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
        </tr>
        <tr class="formText" id="UserEdiDateOfData">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff">
                Date of Data</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelDateOfDataOld" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 32px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff">
                Date of Data</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelDateOfDataNew" runat="server"></asp:Label></td>
        </tr>	        
        <tr class="formText" id="UserEdi5">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff">
                Date of Birth</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelOldDOB" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 32px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff">
                Date of Birth</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNewDOB" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserEdi6">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                Birth Place</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <asp:Label Text="" runat="server" ID="LblBirthPlaceOld" /></td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                &nbsp;</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                Birth Place</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                :</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <asp:Label Text="" runat="server" ID="LblBirthPlaceNew" /></td>
        </tr>
        <tr class="formText" id="UserEdi7">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff">
                Address
            </td>
            <td bgcolor="#ffffff" style="width: 15px">
                :
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewAddressesOld" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                        Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Addresses" HeaderText="Address">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AddressType" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AddressTypeLabel" HeaderText="Address Type">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IsLocalAddress" HeaderText="Local Address?">
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" />
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid></div>
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                Address
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                :
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewAddressesNew" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                        Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Addresses" HeaderText="Address">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AddressType" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AddressTypeLabel" HeaderText="Address Type">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IsLocalAddress" HeaderText="Local Address?">
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" />
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid></div>
            </td>
        </tr>
        <tr class="formText" height="15" id="UserEdi8">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none; height: 15px;">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
        </tr>
        <tr class="formText" id="UserEdi9">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff">
                ID Number</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewIDNosOld" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                        Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IDNos" HeaderText="ID Number">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid></div>
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 32px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff">
                ID Number</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewIDNosNew" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                        Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IDNos" HeaderText="ID Number">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid></div>
            </td>
        </tr>
        <tr class="formText" height="15" id="UserEdi10">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none; height: 15px;">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 15px; border-bottom-style: none">
            </td>
        </tr>        
        <tr class="formText" id="UserEdi11">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Verification List Type</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelOldListType" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 32px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff">
                Verification List Type</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNewListType" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserEdi12">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Category</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelOldCategory" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 32px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff">
                Category</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNewCategory" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserEdi13">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Custom Remark(s)</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewCustomRemarksOld" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                        Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CustomRemarks" HeaderText="Custom Remark">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid></div>
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 32px; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff">
                Custosm Remark(s)</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewCustomRemarksNew" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                        Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CustomRemarks" HeaderText="Custom Remark">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid></div>
            </td>
        </tr>
        <tr class="formText" id="UserEdi14">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                Nationality</td>
            <td bgcolor="#ffffff" style="width: 15px">
                :</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <asp:Label ID="LabelOldNationality" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 32px; border-bottom-style: none">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                Nationality</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <asp:Label ID="LabelNewNationality" runat="server"></asp:Label></td>
        </tr>
	    <TR class="formText" id="UserDel1">
		    <TD bgColor="#ffffff" style="height: 23px; width: 22px;">&nbsp;</TD>
		    <TD bgColor="#ffffff" style="height: 23px;">
                Verification
                List ID</TD>
		    <TD bgColor="#ffffff" style="height: 23px; width: 44px;">:</TD>
		    <TD width="80%" bgColor="#ffffff" style="height: 23px"><asp:label id="LabelListIDDelete" runat="server"></asp:label></TD>
	    </TR>
    
        <tr class="formText" id="UserDel2">
            <td bgcolor="#ffffff" height="24" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Name/Alias
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :
            </td>
            <td bgcolor="#ffffff">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewAliasesDelete" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2" Font-Size="XX-Small"
                        ForeColor="Black" HorizontalAlign="Left" Width="300px">
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Aliases" HeaderText="Alias ">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid></div>
            </td>
        </tr>
        <tr class="formText" height="15" id="UserDel3">
            <td bgcolor="#ffffff"  style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
        </tr>
         <tr class="formText" id="UserDelDateOfData">
            <td bgcolor="#ffffff" height="24" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Date of Data</td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelDateOfDataDeleted" runat="server"></asp:Label></td>
        </tr>        
         <tr class="formText" id="UserDel4">
            <td bgcolor="#ffffff" height="24" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Date of Birth</td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelDOBDelete" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserDel5">
            <td bgcolor="#ffffff" height="24" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                Birth Place</td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label Text="" runat="server" id="LblBirthPlaceDelete"/></td>
        </tr>
        <tr class="formText" id="UserDel6">
            <td bgcolor="#ffffff" height="24" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Address
            </td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewAddressesDelete" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                        Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Addresses" HeaderText="Address">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AddressType" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AddressTypeLabel" HeaderText="Address Type">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IsLocalAddress" HeaderText="Local Address?">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" />
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                    </asp:DataGrid></div>
            </td>
        </tr>
        <tr class="formText" id="UserDel12">
            <td bgcolor="#ffffff" height="24" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                ID Number</td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewIDNosDelete" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                        Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IDNos" HeaderText="ID Number">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                    </asp:DataGrid></div>
            </td>
        </tr>
        <tr class="formText" height="15" id="UserDel8">
            <td bgcolor="#ffffff"  style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
        </tr>       
        <tr class="formText" id="UserDel9">
            <td bgcolor="#ffffff" height="24" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Verification List Type</td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelListTypeDelete" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserDel10">
            <td bgcolor="#ffffff" height="24" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Category</td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelCategoryDelete" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserDel11">
            <td bgcolor="#ffffff" height="24" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Custom Remark(s)</td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff">
                <div style="overflow: auto; height: auto">
                    <asp:DataGrid ID="GridViewCustomRemarksDelete" runat="server" AllowPaging="True"
                        AutoGenerateColumns="False" BackColor="White" BorderColor="White" BorderStyle="None"
                        BorderWidth="1px" CellPadding="2" Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                            Visible="False" />
                        <AlternatingItemStyle BackColor="AliceBlue" />
                        <Columns>
                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CustomRemarks" HeaderText="Custom Remark">
                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid></div>
            </td>
        </tr>
        <tr class="formText" id="UserDel13">
            <td bgcolor="#ffffff" height="24" style="width: 44px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                Nationality</td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNationalityDel" runat="server"></asp:Label>
            </td>
        </tr>
        <tr  bgcolor="#ffffff" class="formText" height="30" id="Button12">
            <td style="width: 22px">
            </td>
            <td >
            Status Approval</td>
              <td >
                  :</td>
            <td >
                <asp:DropDownList ID="cboStatusApproval" runat="server" CssClass="combobox" 
                    AutoPostBack="True">
                    <asp:ListItem Value="0">None</asp:ListItem>
                    <asp:ListItem Value="1">Accept</asp:ListItem>
                    <asp:ListItem Value="2">Reject</asp:ListItem>
                    <asp:ListItem Value="3">Correction</asp:ListItem>
                </asp:DropDownList></td>
            
        </tr>
        <tr  bgcolor="#ffffff" class="formText" height="30" id="Button13">
            <td style="width: 22px">
                &nbsp;</td>
            <td >
                &nbsp;</td>
              <td >
                  &nbsp;</td>
            <td >
                <asp:Panel runat="server" ID="PanelReviewNotes" Visible="false">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td> Review Notes
                            </td>
                            <td style="width: 90%">
                                <asp:TextBox ID="txtReviewNotes" runat="server" Rows="8" TextMode="MultiLine" 
                                    Width="100%" ></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            
        </tr>
	    <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD>
                            <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="False" SkinID="SaveButton" /></TD>
					    <TD></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="none"></asp:CustomValidator></TD>
	    </TR>
    </TABLE>
</asp:Content>