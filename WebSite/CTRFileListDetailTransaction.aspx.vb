Imports SahassaNettier.Data

Imports SahassaNettier.Entities
Imports Sahassa.AML.Commonly

Partial Class CTRFileListDetailTransaction
    Inherits Parent


    Public ReadOnly Property PK() As Long
        Get
            Dim strtemp As String = Request.Params("PK")
            Dim lngreturn As Long = 0
            If Long.TryParse(strtemp, lngreturn) Then
                Return lngreturn
            Else
                Throw New Exception("PK not valid.")
            End If
        End Get
        
    End Property


    Public Property ObjListOfGeneratedFileCTR() As ListOfGeneratedFileCTR
        Get
            If Session("CTRFileListDetailTransaction.ObjListOfGeneratedFileCTR ") Is Nothing Then
                Session("CTRFileListDetailTransaction.ObjListOfGeneratedFileCTR ") = AMLBLL.ReportControlGeneratorBLL.GetListOfGeneratedFileCTRByPk(Me.PK)
                Return CType(Session("CTRFileListDetailTransaction.ObjListOfGeneratedFileCTR "), ListOfGeneratedFileCTR)
            Else
                Return CType(Session("CTRFileListDetailTransaction.ObjListOfGeneratedFileCTR "), ListOfGeneratedFileCTR)
            End If

        End Get
        Set(value As ListOfGeneratedFileCTR)
            Session("CTRFileListDetailTransaction.ObjListOfGeneratedFileCTR ") = value
        End Set
    End Property


    Public Property ObjTblDetail() As Data.DataTable
        Get
            If Session("CTRFileListDetailTransaction.ObjTblDetail ") Is Nothing Then
                Session("CTRFileListDetailTransaction.ObjTblDetail ") = AMLBLL.ReportControlGeneratorBLL.GetDetailTransByPKGeneratedID(Me.PK)
                Return CType(Session("CTRFileListDetailTransaction.ObjTblDetail "), Data.DataTable)
            Else
                Return CType(Session("CTRFileListDetailTransaction.ObjTblDetail "), Data.DataTable)
            End If
        End Get
        Set(value As Data.DataTable)
            Session("CTRFileListDetailTransaction.ObjListOfGeneratedFileCTR ") = value
        End Set
    End Property


    '#Region "Property"
    '    Public ReadOnly Property View_PkCTRFileId() As Int32
    '        Get
    '            Return Convert.ToInt32(Session("View_PkCTRFileId"))
    '        End Get
    '    End Property
    '    Public Property MappingFkCustomerTypeId() As Int16
    '        Get
    '            Return Session("View_MappingCTRFileFkCustomerTypeId")
    '        End Get
    '        Set(ByVal value As Int16)
    '            Session("View_MappingCTRFileFkCustomerTypeId") = value
    '        End Set
    '    End Property
    '    Public ReadOnly Property View_FkCustomerId() As Int16
    '        Get
    '            Return Convert.ToInt16(Session("View_FkCustomerTypeId"))
    '        End Get
    '    End Property
    '    Public ReadOnly Property View_CTRFileTransactionDate() As DateTime
    '        Get
    '            Return Convert.ToDateTime(Session("View_CTRFileTransactionDate"))
    '        End Get
    '    End Property
    '#End Region
    '#Region "Set Session"
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("CTRFileListTransactionViewSelected") Is Nothing, New ArrayList, Session("CTRFileListTransactionViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CTRFileListTransactionViewSelected") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CTRFileListTransactionViewSort") Is Nothing, "PK_LTKT_Id desc", Session("CTRFileListTransactionViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("CTRFileListTransactionViewSort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CTRFileListTransactionViewCurrentPage") Is Nothing, 0, Session("CTRFileListTransactionViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CTRFileListTransactionViewCurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CTRFileListTransactionViewRowTotal") Is Nothing, 0, Session("CTRFileListTransactionViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CTRFileListTransactionViewRowTotal") = Value
        End Set
    End Property
    '    Private Property SetAndGetSearchingCriteria() As String
    '        Get
    '            Return IIf(Session("CTRFileListTransactionViewSearchCriteria") Is Nothing, "", Session("CTRFileListTransactionViewSearchCriteria"))
    '        End Get
    '        Set(ByVal Value As String)
    '            Session("CTRFileListTransactionViewSearchCriteria") = Value
    '        End Set
    '    End Property
    '    Private Property SetnGetBindTableCIF() As VList(Of Vw_MappingCIFTransactionCTRFile)
    '        Get
    '            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
    '                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
    '            End If
    '            If Session("CTRFileListTransactionViewData") Is Nothing Then
    '                Return DataRepository.Vw_MappingCIFTransactionCTRFileProvider.GetPaged(Me.SetAndGetSearchingCriteria, Me.SetnGetSort, (Me.SetnGetCurrentPage), Sahassa.CommonCTRWeb.Commonly.SessionPagingLimit, Me.SetnGetRowTotal)
    '            Else
    '                Return Session("CTRFileListTransactionViewData")
    '            End If
    '        End Get
    '        Set(ByVal value As VList(Of Vw_MappingCIFTransactionCTRFile))
    '            Session("CTRFileListTransactionViewData") = value
    '        End Set
    '    End Property
    '    Private Property SetnGetBindTableWIC() As VList(Of Vw_MappingWICTransactionCTRFile)
    '        Get
    '            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
    '                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
    '            End If
    '            If Session("CTRFileListTransactionViewData") Is Nothing Then
    '                Return DataRepository.Vw_MappingWICTransactionCTRFileProvider.GetPaged(Me.SetAndGetSearchingCriteria, Me.SetnGetSort, (Me.SetnGetCurrentPage), Sahassa.CommonCTRWeb.Commonly.SessionPagingLimit, Me.SetnGetRowTotal)
    '            Else
    '                Return Session("CTRFileListTransactionViewData")
    '            End If
    '        End Get
    '        Set(ByVal value As VList(Of Vw_MappingWICTransactionCTRFile))
    '            Session("CTRFileListTransactionViewData") = value
    '        End Set
    '    End Property
    '#End Region
    '#Region "Paging Event"
    '    Private Sub SetInfoNavigate()
    '        Try
    '            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
    '            Me.PageTotalPages.Text = Me.GetPageTotal
    '            Me.PageTotalRows.Text = Me.SetnGetRowTotal
    '            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
    '            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
    '            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
    '            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
    '        Catch
    '            Throw
    '        End Try
    '    End Sub
    '    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
    '        Try
    '            Select Case e.CommandName
    '                Case "First" : Me.SetnGetCurrentPage = 0
    '                Case "Prev" : Me.SetnGetCurrentPage -= 1
    '                Case "Next" : Me.SetnGetCurrentPage += 1
    '                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
    '                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
    '            End Select
    '        Catch ex As Exception
    '            Me.CValPageErr.IsValid = False
    '            Me.CValPageErr.ErrorMessage = ex.Message
    '            LogError(ex)
    '        End Try
    '    End Sub
    '    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
    '        Try
    '            If IsNumeric(Me.TextGoToPage.Text) Then
    '                If (CInt(Me.TextGoToPage.Text) > 0) Then
    '                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
    '                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
    '                    Else
    '                        Throw New Exception("Page number must be less than or equal to the total page count.")
    '                    End If
    '                Else
    '                    Throw New Exception("Page number must be less than or equal to the total page count.")
    '                End If
    '            Else
    '                Throw New Exception("Page number must be in number format.")
    '            End If
    '        Catch ex As Exception
    '            Me.CValPageErr.IsValid = False
    '            Me.CValPageErr.ErrorMessage = ex.Message
    '            LogError(ex)
    '        End Try
    '    End Sub
    '#End Region
    '#Region "Sorting Event"
    '    Protected Sub DtGridTransactionsList_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtGridTransactionsList.SortCommand
    '        Dim GridListofGeneratedFileView As DataGrid = source
    '        Try
    '            Me.SetnGetSort = Sahassa.CommonCTRWeb.Commonly.ChangeSortCommand(e.SortExpression)
    '            GridListofGeneratedFileView.Columns(Sahassa.CommonCTRWeb.Commonly.IndexSort(GridListofGeneratedFileView, e.SortExpression)).SortExpression = Me.SetnGetSort
    '        Catch ex As Exception
    '            Me.CValPageErr.IsValid = False
    '            Me.CValPageErr.ErrorMessage = ex.Message
    '            LogError(ex)
    '        End Try
    '    End Sub
    '#End Region
    '#Region "Bind Grid View"
    '    Public Sub BindGrid()
    '        Select Case Me.View_FkCustomerId
    '            Case 1
    '                'CIF
    '                Me.SetAndGetSearchingCriteria = " DATEDIFF(day,TanggalTransaksi,'" & Me.View_CTRFileTransactionDate.ToString("yyyy-MM-dd") & "') = 0 "
    '                Me.DtGridTransactionsList.DataSource = Me.SetnGetBindTableCIF
    '            Case 2
    '                'WIC
    '                Me.SetAndGetSearchingCriteria = " DATEDIFF(day,TanggalTransaksi,'" & Me.View_CTRFileTransactionDate.ToString("yyyy-MM-dd") & "') = 0 "
    '                Me.DtGridTransactionsList.DataSource = Me.SetnGetBindTableWIC
    '        End Select
    '        Me.DtGridTransactionsList.VirtualItemCount = Me.SetnGetRowTotal
    '        Me.DtGridTransactionsList.DataBind()
    '    End Sub
    '#End Region
    '    Private Sub ClearThisPageSessions()
    '        Session("CTRFileListTransactionViewSelected") = Nothing
    '        Session("CTRFileListTransactionViewSort") = Nothing
    '        Session("CTRFileListTransactionViewCurrentPage") = Nothing
    '        Session("CTRFileListTransactionViewRowTotal") = Nothing
    '        Session("CTRFileListTransactionViewSearchCriteria") = Nothing
    '        Session("CTRFileListTransactionViewData") = Nothing
    '    End Sub
    '    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '        Try
    '            If Not Me.IsPostBack Then
    '                Sahassa.CommonCTRWeb.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
    '                Dim UserAccessAction As String = "Accesssing page " & Sahassa.CommonCTRWeb.Commonly.SessionCurrentPage & " succeeded"
    '                Using ObjAuditTrailUserAccess As AuditTrailUserAccess = New AuditTrailUserAccess
    '                    With ObjAuditTrailUserAccess
    '                        .AuditTrailUserAccess_NIK = Sahassa.CommonCTRWeb.Commonly.SessionNIK
    '                        .AuditTrailUserAccess_StaffName = Sahassa.CommonCTRWeb.Commonly.SessionStaffName
    '                        .AuditTrailUserAccess_ActionDate = DateTime.Now
    '                        .AuditTrailUserAccess_Description = UserAccessAction
    '                    End With
    '                    DataRepository.AuditTrailUserAccessProvider.Save(ObjAuditTrailUserAccess)
    '                End Using
    '                Me.LoadData()
    '            End If
    '        Catch ex As Exception
    '            Me.CValPageErr.IsValid = False
    '            Me.CValPageErr.ErrorMessage = ex.Message
    '            LogError(ex)
    '        End Try
    '    End Sub
    '    Protected Sub CValPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CValPageErr.PreRender
    '        If Me.CValPageErr.IsValid = False Then
    '            Sahassa.CommonCTRWeb.Commonly.SessionErrorMessageCount = Sahassa.CommonCTRWeb.Commonly.SessionErrorMessageCount + 1
    '            ClientScript.RegisterStartupScript(Page.GetType, "ErrorMessage" & Sahassa.CommonCTRWeb.Commonly.SessionErrorMessageCount.ToString, "alert('There was uncompleteness in this page: \n" & Me.CValPageErr.ErrorMessage.Replace("'", "\'").Replace(vbCrLf, "\n").Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">") & "');", True)
    '        End If
    '    End Sub
    '    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
    '        Try
    '            Session("View_PkCTRFileId") = Nothing
    '            Session("DetailTransactionPage") = "CTRFileListDetailTransaction.aspx"
    '            Sahassa.CommonCTRWeb.Commonly.SessionIntendedPage = "ListofGeneratedCTRFile.aspx"
    '            Me.Response.Redirect("ListofGeneratedCTRFile.aspx", False)
    '        Catch ex As Exception
    '            Me.CValPageErr.IsValid = False
    '            Me.CValPageErr.ErrorMessage = ex.Message
    '            LogError(ex)
    '        End Try
    '    End Sub
    '    Private Sub LoadData()
    '        'kalau wic load dari wic kalau cif load dari cif
    '        Try
    '            Using ObjListvwCTRFile As VList(Of vwCTRFile) = DataRepository.vwCTRFileProvider.GetPaged("Pk_ListOfCTRFile_Id =" & Me.View_PkCTRFileId, "", 0, 1, 0)
    '                If Not ObjListvwCTRFile Is Nothing Then
    '                    Dim ObjvwCTRFile As vwCTRFile = ObjListvwCTRFile.Item(0)
    '                    Me.LblTextFileName.Text = ObjvwCTRFile.CTRFileName
    '                    If CInt(ObjvwCTRFile.Fk_MsCustomerType_Id) = 1 Then
    '                        Me.LblTotalNumberofCIF.Text = ObjvwCTRFile.CTRFileTotalNumberOfCIF.ToString()
    '                    Else
    '                        Me.LblTotalNumberofCIF.Text = ObjvwCTRFile.CTRFileTotalNumberOfWIC.ToString()
    '                    End If
    '                    LblTotalAmountCashIn.Text = GetTotalCashIn(ObjvwCTRFile)
    '                    LblTotalAmountCashOut.Text = GetTotalCashOut(ObjvwCTRFile)

    '                    Me.LblCustomerType.Text = ObjvwCTRFile.MsCustomerType_Name
    '                    Me.LblReportType.Text = ObjvwCTRFile.MsCTRReportTypeFile_Name

    '                    'Me.LblTotalNumberofCIF.Text = ObjvwCTRFile.CTRFile_TotalNumberofCIF.ToString
    '                    'Me.LblTotalAmountCashIn.Text = "(IDR) " & ObjvwCTRFile.CTRFile_TotalAmount.ToString("###,###0.00")
    '                    'Me.LblCustomerType.Text = ObjvwCTRFile.MsCustomerType_Name
    '                    'Me.LblReportType.Text = ObjvwCTRFile.MsCTRReportTypeFile_Name
    '                    'Me.MappingFkCustomerTypeId = ObjvwCTRFile.Fk_MsCustomerType_Id
    '                End If
    '            End Using
    '            BindGrid()
    '        Catch
    '            Throw
    '        End Try
    '    End Sub

    '    Private Function GetTotalCashIn(ByVal objvwCtrFile As vwCTRFile) As String
    '        Dim decTotalCash As Decimal = 0D
    '        Dim criteria As String = "DateDiff(day,TanggalTransaksi,'" & CDate(objvwCtrFile.CTRFileTransactionDate).ToString("yyyy-MM-dd") & "') = 0 AND DetailId = 1"
    '        If objvwCtrFile.Fk_MsCustomerType_Id.GetValueOrDefault() = 1 Then
    '            'CIF
    '            Using objVw As VList(Of vw_MappingCIFTransactionCTRFile) = DataRepository.vw_MappingCIFTransactionCTRFileProvider.GetPaged(criteria, "DetailId desc", 0, Integer.MaxValue, 0)
    '                If objVw.Count > 0 Then
    '                    For i As Integer = 0 To objVw.Count - 1
    '                        decTotalCash += objVw(i).LCENominal
    '                    Next
    '                End If
    '            End Using
    '        Else
    '            'WIC
    '            Using objVw As VList(Of vw_MappingWICTransactionCTRFile) = DataRepository.vw_MappingWICTransactionCTRFileProvider.GetPaged(criteria, "DetailId desc", 0, Integer.MaxValue, 0)
    '                If objVw.Count > 0 Then
    '                    For i As Integer = 0 To objVw.Count - 1
    '                        decTotalCash += objVw(i).LCENominal
    '                    Next
    '                End If
    '            End Using
    '        End If
    '        Return decTotalCash.ToString("#,###.#0")
    '    End Function

    '    Private Function GetTotalCashOut(ByVal objvwCtrFile As vwCTRFile) As String
    '        Dim decTotalCash As Decimal = 0D
    '        Dim criteria As String = "DateDiff(day,TanggalTransaksi,'" & CDate(objvwCtrFile.CTRFileTransactionDate).ToString("yyyy-MM-dd") & "') = 0 AND DetailId = 2"
    '        If objvwCtrFile.Fk_MsCustomerType_Id.GetValueOrDefault() = 1 Then
    '            'CIF
    '            Using objVw As VList(Of vw_MappingCIFTransactionCTRFile) = DataRepository.vw_MappingCIFTransactionCTRFileProvider.GetPaged(criteria, "DetailId desc", 0, Integer.MaxValue, 0)
    '                If objVw.Count > 0 Then
    '                    For i As Integer = 0 To objVw.Count - 1
    '                        decTotalCash += objVw(i).LCENominal
    '                    Next
    '                End If
    '            End Using
    '        Else
    '            'WIC
    '            Using objVw As VList(Of vw_MappingWICTransactionCTRFile) = DataRepository.vw_MappingWICTransactionCTRFileProvider.GetPaged(criteria, "DetailId desc", 0, Integer.MaxValue, 0)
    '                If objVw.Count > 0 Then
    '                    For i As Integer = 0 To objVw.Count - 1
    '                        decTotalCash += objVw(i).LCENominal
    '                    Next
    '                End If
    '            End Using
    '        End If
    '        Return decTotalCash.ToString("#,###.#0")
    '    End Function

    '    Protected Sub DtGridTransactionsList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtGridTransactionsList.ItemDataBound
    '        Try
    '            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
    '                If Me.View_FkCustomerId = 1 Then
    '                    'cif
    '                    e.Item.Cells(0).Text = CType(e.Item.DataItem, vw_MappingCIFTransactionCTRFile).PK_LTKT_Id
    '                Else
    '                    'wic
    '                    e.Item.Cells(0).Text = CType(e.Item.DataItem, vw_MappingWICTransactionCTRFile).PK_WIC_Id
    '                End If

    '                'e.Item.Cells(8).Text = Convert.ToDecimal(e.Item.Cells(8).Text).ToString("###,###0.00")
    '                'e.Item.Cells(9).Text = Convert.ToDecimal(e.Item.Cells(9).Text).ToString("###,###0.00")
    '                'e.Item.Cells(10).Text = Convert.ToDecimal(e.Item.Cells(10).Text).ToString("###,###0.00")

    '                e.Item.Cells(1).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.CommonCTRWeb.Commonly.GetDisplayedTotalRow))
    '            End If
    '        Catch ex As Exception
    '            Me.CValPageErr.IsValid = False
    '            Me.CValPageErr.ErrorMessage = ex.Message
    '            LogError(ex)
    '        End Try
    '    End Sub
    '    Protected Sub DtGridTransactionsList_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtGridTransactionsList.DeleteCommand
    '        Dim PkMappingCTRFile As Int64
    '        Try
    '            'Show Detail Transaction
    '            PkMappingCTRFile = e.Item.Cells(0).Text
    '            Session("View_PkMappingCTRFile") = PkMappingCTRFile
    '            Session("DetailTransactionPage") = "CTRFileListDetailTransaction.aspx"
    '            Sahassa.CommonCTRWeb.Commonly.SessionIntendedPage = "CTRFileTransactionDetail.aspx"
    '            Me.Response.Redirect("CTRFileTransactionDetail.aspx", False)
    '        Catch ex As Exception
    '            Me.CValPageErr.IsValid = False
    '            Me.CValPageErr.ErrorMessage = ex.Message
    '            LogError(ex)
    '        End Try
    '    End Sub
    '    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '        Try
    '            Me.SetAndGetSearchingCriteria = ""
    '            Me.BindGrid()
    '            Me.SetInfoNavigate()
    '        Catch ex As Exception
    '            Me.CValPageErr.IsValid = False
    '            Me.CValPageErr.ErrorMessage = ex.Message
    '            LogError(ex)
    '        End Try
    '    End Sub

    '    'Protected Sub lnkBackToGeneratedCTRFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBackToGeneratedCTRFile.Click
    '    '    Try
    '    '        Session("View_PkCTRFileId") = Nothing
    '    '        Session("DetailTransactionPage") = "CTRFileListDetailTransaction.aspx"
    '    '        Sahassa.CommonCTRWeb.Commonly.SessionIntendedPage = "ListofGeneratedCTRFile.aspx"
    '    '        Me.Response.Redirect("ListofGeneratedCTRFile.aspx", False)
    '    '    Catch ex As Exception
    '    '        Me.CValPageErr.IsValid = False
    '    '        Me.CValPageErr.ErrorMessage = ex.Message
    '    '        LogError(ex)
    '    '    End Try
    '    'End Sub
#Region "Paging Event"

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.CValPageErr.IsValid = False
            Me.CValPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

#End Region
#Region "Sorting Event"
    Protected Sub DtGridTransactionsList_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtGridTransactionsList.SortCommand
        Dim GridListofGeneratedFileView As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridListofGeneratedFileView.Columns(Sahassa.AML.Commonly.IndexSort(GridListofGeneratedFileView, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.CValPageErr.IsValid = False
            Me.CValPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
    Sub ClearSession()
        ObjListOfGeneratedFileCTR = Nothing
        ObjTblDetail = Nothing
    End Sub

    Sub LoadData()
        If Not ObjListOfGeneratedFileCTR Is Nothing Then
            LblTextFileName.Text = ObjListOfGeneratedFileCTR.FileName
            If ObjListOfGeneratedFileCTR.FK_ReportType_ID.GetValueOrDefault(0) = 1 Then
                LblTotalNumberofCIF.Text = ObjListOfGeneratedFileCTR.TotalCIF
            Else
                LblTotalNumberofCIF.Text = ObjListOfGeneratedFileCTR.TotalWIC
            End If
            Using objReporttype As ReportType = DataRepository.ReportTypeProvider.GetByReportType(ObjListOfGeneratedFileCTR.FK_ReportType_ID.GetValueOrDefault(0))
                If Not objReporttype Is Nothing Then
                    LblCustomerType.Text = objReporttype.ReportTypeDesc
                End If
            End Using

            Using objreportformat As ReportFormat = DataRepository.ReportFormatProvider.GetByReportFormat(ObjListOfGeneratedFileCTR.FK_ReportFormat_ID.GetValueOrDefault(0))
                If Not objreportformat Is Nothing Then
                    LblReportFormat.Text = objreportformat.ReportFormatName
                End If
            End Using

            LblTotalAmountCashIn.Text = CType(AMLBLL.ReportControlGeneratorBLL.GetTotalCashInByPKGeneratedID(Me.PK), Double).ToString("#,###.##")
            LblTotalAmountCashOut.Text = CType(AMLBLL.ReportControlGeneratorBLL.GetTotalCashoutByPKGeneratedID(Me.PK), Double).ToString("#,###.##")
            DtGridTransactionsList.DataSource = ObjTblDetail
            DtGridTransactionsList.DataBind()

        End If

    End Sub

    Protected Sub Page_Load1(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '    Transcope.Complete()
                End Using
                LoadData()
            End If
        Catch ex As Exception
            Me.CValPageErr.IsValid = False
            Me.CValPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub DtGridTransactionsList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtGridTransactionsList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim objlblno As Label = CType(e.Item.FindControl("lblno"), Label)
                objlblno.Text = e.Item.ItemIndex + 1
            End If
        Catch ex As Exception
            Me.CValPageErr.IsValid = False
            Me.CValPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    
    Protected Sub ImgBack_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Try
            Response.Redirect("ListofGeneratedCTRFile.aspx", False)
        Catch ex As Exception
            Me.CValPageErr.IsValid = False
            Me.CValPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub DtGridTransactionsList_PageIndexChanged(source As Object, e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles DtGridTransactionsList.PageIndexChanged
        Try
            DtGridTransactionsList.CurrentPageIndex = e.NewPageIndex
            DtGridTransactionsList.DataSource = ObjTblDetail
            DtGridTransactionsList.DataBind()
        Catch ex As Exception
            Me.CValPageErr.IsValid = False
            Me.CValPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
