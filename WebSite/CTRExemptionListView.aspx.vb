Imports System.Data
Imports System.Data.SqlClient
Imports AML2015Nettier.Data
Imports AML2015Nettier.Entities
Partial Class CTRExemptionListView
    Inherits Parent

#Region " Property "
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("MsCTRExemptionListSelected") Is Nothing, New ArrayList, Session("MsCTRExemptionListSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("MsCTRExemptionListSelected") = value
        End Set
    End Property
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("MsCTRExemptionListFieldSearch") Is Nothing, "", Session("MsCTRExemptionListFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsCTRExemptionListFieldSearch") = Value
        End Set
    End Property
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("MsCTRExemptionListValueSearch") Is Nothing, "", Session("MsCTRExemptionListValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsCTRExemptionListValueSearch") = Value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("MsCTRExemptionListSort") Is Nothing, "CTRExemptionListId  asc", Session("MsCTRExemptionListSort"))
        End Get
        Set(ByVal Value As String)
            Session("MsCTRExemptionListSort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("MsCTRExemptionListCurrentPage") Is Nothing, 0, Session("MsCTRExemptionListCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsCTRExemptionListCurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("MsCTRExemptionListRowTotal") Is Nothing, 0, Session("MsCTRExemptionListRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsCTRExemptionListRowTotal") = Value
        End Set
    End Property
    'Private Property SetnGetBindTable() As Data.DataTable
    '    Get
    '        Return IIf(Session("MsCTRExemptionListData") Is Nothing, New AMLDAL.CTRExemptionList.CTRExemptionListDataTable, Session("MsCTRExemptionListData"))
    '    End Get
    '    Set(ByVal value As Data.DataTable)
    '        Session("MsCTRExemptionListData") = value
    '    End Set
    'End Property
    Private Property SetnGetBindTable() As VList(Of vw_CTRExemptionList)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If


            If SetnGetFieldSearch.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''"))
            End If
            strAllWhereClause = String.Join(" and ", strWhereClause)

            'If strAllWhereClause = "" Then
            '    strAllWhereClause += UserColumn.pkUserID.ToString & "<> 1"
            'Else
            '    strAllWhereClause += " and " & UserColumn.pkUserID.ToString & "<> 1"
            'End If
            'Session("CTRExemptionListView.Table") = AMLBLL.UserBLL.GetTMsUser(strAllWhereClause, SetnGetSort, SetnGetCurrentPageUser, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotalUser)
            'Return CType(Session("CTRExemptionListView.Table"), TList(Of User))

            Session("CTRExemptionListView.Table") = AMLBLL.CTRExemptionListBLL.GetTlistvw_CTRExemptionList(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)
            Return CType(Session("CTRExemptionListView.Table"), VList(Of vw_CTRExemptionList))


        End Get
        Set(ByVal value As VList(Of vw_CTRExemptionList))
            Session("CTRExemptionListView.Table") = value
        End Set
    End Property


#End Region

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Created Date", "CreatedDate BETWEEN '-=Search=- 00:00' AND '-=Search=- 23:59'"))
            Me.ComboSearch.Items.Add(New ListItem("Account Number", "AccountNo Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("CIF No", "CIFNo Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Name", "CustomerName Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Exemption Type", "CtrExemptionTypeName Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Description", "Description Like '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("MsCTRExemptionListSelected") = Nothing
        Session("MsCTRExemptionListFieldSearch") = Nothing
        Session("MsCTRExemptionListValueSearch") = Nothing
        Session("MsCTRExemptionListSort") = Nothing
        Session("MsCTRExemptionListCurrentPage") = Nothing
        Session("MsCTRExemptionListRowTotal") = Nothing
        Session("MsCTRExemptionListData") = Nothing
        Session("CTRExemptionListDeleteID") = Nothing
        Session("CTRExemptionListView.Table") = Nothing
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Trans As SqlTransaction
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()

                'Using AccessCTRExemptionList As New AMLDAL.CTRExemptionListTableAdapters.CTRExemptionListTableAdapter
                '    Me.SetnGetBindTable = AccessCTRExemptionList.GetData
                'End Using



                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check(this, 1, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "');")
                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")

                Me.FillSearch()
                Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit)
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    Trans.Commit()
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            Trans.Rollback()
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
            End If
        End Try
    End Sub

    Public Sub BindGrid()
        Dim strWhereClause As String = Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''"))
        'If strWhereClause.Contains("CreatedDate BETWEEN") Then
        '    strWhereClause = strWhereClause.Replace(" BETWEEN ", " >= ").Replace(" AND ", " AND CreatedDate <= ")
        'End If
        'Dim Rows() As AMLDAL.CTRExemptionList.CTRExemptionListRow = Me.SetnGetBindTable.Select(strWhereClause, Me.SetnGetSort)

        Me.GridMSUserView.DataSource = SetnGetBindTable
        Me.SetnGetRowTotal = Me.SetnGetRowTotal
        Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridMSUserView.AllowCustomPaging = True
        Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridMSUserView.DataBind()
    End Sub

    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim groupID As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(groupID) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(groupID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(groupID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    Private Sub BindSelected()
        Try
            Dim Rows As New ArrayList
            Dim strfilter As String = "0,"
            For Each IdPk As Int32 In Me.SetnGetSelectedItem
                strfilter += IdPk & ","
            Next
            strfilter = strfilter.Substring(0, strfilter.Length - 1)
            'For Each IdPk As Int32 In Me.SetnGetSelectedItem
            '    'Dim rowData() As AMLDAL.CTRExemptionList.CTRExemptionListRow = Me.SetnGetBindTable.Select("CTRExemptionListId = '" & IdPk & "'")
            '    'If rowData.Length > 0 Then
            '    '    Rows.Add(rowData(0))
            '    'End If
            '    Dim rowdata As VList(Of vw_CTRExemptionList) = AMLBLL.CTRExemptionListBLL.GetTlistvw_CTRExemptionList("CTRExemptionListId = '" & IdPk & "'", SetnGetSort, 0, Integer.MaxValue, Me.SetnGetRowTotal)
            '    If rowdata.Count > 0 Then
            '        Rows.Add(rowdata(0))
            '    End If
            'Next


            Dim rowdata As VList(Of vw_CTRExemptionList) = AMLBLL.CTRExemptionListBLL.GetTlistvw_CTRExemptionList("CTRExemptionListId in (" & strfilter & ")", SetnGetSort, 0, Integer.MaxValue, Me.SetnGetRowTotal)

            Me.GridMSUserView.DataSource = rowdata
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0,1,4 & 5 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(8).Visible = False
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CTRExemptionView.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged        
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            Else
                Throw New Exception("Cannot delete the following CTR Exemption: '")
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget

    End Sub

    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Public Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
                Me.SetnGetValueSearch = Me.TextSearch.Text
            ElseIf Me.ComboSearch.SelectedIndex = 1 Then
                Dim DateSearch As DateTime
                Try
                    DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
                Catch ex As ArgumentOutOfRangeException
                    Throw New Exception("Input character cannot be convert to datetime.")
                Catch ex As FormatException
                    Throw New Exception("Unknown format date")
                End Try
                Me.SetnGetValueSearch = DateSearch.ToString("dd-MMM-yyyy")
            Else
                Me.SetnGetValueSearch = Me.TextSearch.Text
            End If

            Me.SetnGetCurrentPage = 0

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Function IsDeleted(ByVal FK_CTRExemptionType_ID As Integer, ByVal strAccountNo As String, ByVal cifno As String) As Boolean

        If FK_CTRExemptionType_ID = 1 Then 'cif
            Using objtlist As TList(Of CTRExemptionList_PendingApproval) = AMLBLL.CTRExemptionListBLL.GetTlistCTRExemptionList_PendingApproval(CTRExemptionList_PendingApprovalColumn.CTRPendingApproval_CIFNo.ToString & "='" & cifno & "'", "", 0, Integer.MaxValue, 0)
                If objtlist.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            End Using
        ElseIf FK_CTRExemptionType_ID = 2 Then 'accountno
            Using objtlist As TList(Of CTRExemptionList_PendingApproval) = AMLBLL.CTRExemptionListBLL.GetTlistCTRExemptionList_PendingApproval(CTRExemptionList_PendingApprovalColumn.CTRPendingApproval_AccountNo.ToString & "='" & strAccountNo & "'", "", 0, Integer.MaxValue, 0)
                If objtlist.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            End Using
        End If

        'Using AccessPending As New AMLDAL.CTRExemptionListTableAdapters.CTRApprovalTableAdapter
        '    Dim count As Int32 = AccessPending.CTRApproval_Count(strAccountNo)
        '    If count > 0 Then
        '        Return True
        '    Else
        '        Return False
        '    End If
        'End Using
    End Function

    Private Sub GridMSUserView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.DeleteCommand
        Dim pk As String = e.Item.Cells(1).Text

        Try
            If e.Item.Cells(2).Text.ToLower = "superuser" Then
                Throw New Exception("Super User Can't be Deleted")
            Else

                If Me.IsDeleted(e.Item.Cells(9).Text, e.Item.Cells(3).Text, e.Item.Cells(4).Text) = False Then
                    Session("CTRExemptionListDeleteID") = pk
                    Sahassa.AML.Commonly.SessionIntendedPage = "CTRExemptionListDelete.aspx"
                    Me.Response.Redirect("CTRExemptionListDelete.aspx", False)
                Else
                    If e.Item.Cells(9).Text = "1" Then
                        Throw New Exception("Cannot delete the following cif : '" & e.Item.Cells(4).Text & "' because it is currently waiting for approval")
                    ElseIf e.Item.Cells(9).Text = "2" Then
                        Throw New Exception("Cannot delete the following account: '" & e.Item.Cells(3).Text & "' because it is currently waiting for approval")
                    End If

                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub LinkButtonAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddNew.Click
        Me.Response.Redirect("CTRExemptionListAdd.aspx", False)
    End Sub

End Class