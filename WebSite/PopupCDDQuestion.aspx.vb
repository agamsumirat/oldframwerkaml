Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities

Partial Class PopupCDDQuestion
    Inherits Parent

#Region "Set Session"
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("PopUpCDDQuestionPageSort") Is Nothing, "PK_CDD_Question_ID  asc", Session("PopUpCDDQuestionPageSort"))
        End Get
        Set(ByVal Value As String)
            Session("PopUpCDDQuestionPageSort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("PopUpCDDQuestionCurrentPage") Is Nothing, 0, Session("PopUpCDDQuestionCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("PopUpCDDQuestionCurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("PopUpCDDQuestionRowTotal") Is Nothing, 0, Session("PopUpCDDQuestionRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("PopUpCDDQuestionRowTotal") = Value
        End Set
    End Property
    Private Property SetAndGetSearchingCriteria() As String
        Get
            Return IIf(Session("PopUpCDDQuestionSearchCriteria") Is Nothing, "", Session("PopUpCDDQuestionSearchCriteria"))
        End Get
        Set(ByVal Value As String)
            Session("PopUpCDDQuestionSearchCriteria") = Value
        End Set
    End Property

    Private Function SetAllSearchingCriteria() As String
        Dim StrSearch As String = ""
        Try
            If TextSearch.Text <> "" Then
                If ComboSearch.SelectedValue = "CDD_Question" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " CDD_Question LIKE '%" & Me.TextSearch.Text & "%'"
                    End If
                End If
                If ComboSearch.SelectedValue = "QuestionTypeName" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " QuestionTypeName LIKE '%" & Me.TextSearch.Text & "%'"
                    End If
                End If
            End If

            StrSearch = StrSearch & " PK_CDD_Question_ID NOT IN(" & Session("strPK_CDD_Question_ID") & ")"

            Return StrSearch
        Catch
            Throw
            Return ""
        End Try
    End Function

    Private Property SetnGetBindTable() As VList(Of vw_CDD_Question)
        Get
            Return IIf(Session("PopUpCDDLNPQuestionViewData") Is Nothing, New VList(Of vw_CDD_Question), Session("PopUpCDDLNPQuestionViewData"))
        End Get
        Set(ByVal value As VList(Of vw_CDD_Question))
            Session("PopUpCDDLNPQuestionViewData") = value
        End Set
    End Property

    Private Sub ClearThisPageSessions()
        Session("PopUpCDDQuestionPageSelected") = Nothing
        Session("PopUpCDDQuestionPageSort") = Nothing
        Session("PopUpCDDQuestionCurrentPage") = Nothing
        Session("PopUpCDDQuestionRowTotal") = Nothing
        Session("PopUpCDDQuestionSearchCriteria") = Nothing
        Session("PopUpCDDLNPQuestionViewData") = Nothing
    End Sub

    Private Sub FillSearchField()
        ComboSearch.Items.Add(New ListItem("Question", "CDD_Question"))
        ComboSearch.Items.Add(New ListItem("Question Type", "QuestionTypeName"))
    End Sub
#End Region

    Public Sub BindGrid()
        Me.SetnGetBindTable = DataRepository.vw_CDD_QuestionProvider.GetPaged(Me.SetAllSearchingCriteria, Me.SetnGetSort, Me.SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)
        Me.GridView.DataSource = Me.SetnGetBindTable
        Me.GridView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridView.DataBind()
    End Sub

#Region "Paging Event"
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try            
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
#End Region

    Protected Sub GridView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridView.ItemCommand
        Try
            Select Case e.CommandName.ToLower
                Case "select"
                    Session("PopupCDDQuestion.PK_CDD_Question_ID") = e.Item.Cells(0).Text
                    Session("PopupCDDQuestion.CDD_Question") = e.Item.Cells(1).Text

                    ClientScript.RegisterClientScriptBlock(Page.GetType, "PopupCDDQuestion", "javascript:window.close();;")

            End Select
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridView.SortCommand
        Dim GridRETBaselViewPage As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridRETBaselViewPage.Columns(Sahassa.AML.Commonly.IndexSort(GridView, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearThisPageSessions()
                Me.GridView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                Me.FillSearchField()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageSearchButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSearchButton.Click
        Try
            Me.SetAndGetSearchingCriteria = Me.SetAllSearchingCriteria
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
