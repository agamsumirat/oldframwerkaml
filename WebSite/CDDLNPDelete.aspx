﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CDDLNPDelete.aspx.vb" Inherits="CDDLNPDelete" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script language="javascript">
        function OpenCustomerVerification() {
            window.open('PopUpPotentialCustomerVerification.aspx?Name=' + document.getElementById('ctl00$cpContent$ans_2_1_True').value + '','mustunique','scrollbars=1,directories=0,height=600,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0');
        }

        function OpenCustomerVerificationResult() {
            window.open('PopUpPotentialCustomerVerificationResult.aspx','mustunique','scrollbars=1,directories=0,height=600,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0');
        }

        function PostBackOnMainPage(){
          <%=GetPostBackScript()%>
        }
    </script>

    <script runat="server" language="VB">
        'Create the postback script
        Private Function GetPostBackScript() As String
        
            Dim options As New PostBackOptions(lblHeader)
            Page.ClientScript.RegisterForEventValidation(options)
            Return Page.ClientScript.GetPostBackEventReference(options)
        
        End Function
    </script>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div>
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <img src="Images/dot_title.gif" width="17" height="17">
                                                <strong>
                                                    <asp:Label ID="lblHeader" runat="server" Text="EDD - Delete"></asp:Label>
                                                </strong>
                                                <hr />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Table ID="TableCategory" runat="server" Width="100%" CellSpacing="0" CellPadding="4"
                                        bgColor="#dddddd">
                                    </asp:Table>
                                </ajax:AjaxPanel>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td align="left" valign="middle">
                                <img src="Images/blank.gif" width="5" height="1" /></td>
                            <td align="left" valign="middle">
                                <img src="images/arrow.gif" width="15" height="15" />&nbsp;</td>
                            <td>
                            </td>
                            <td>
                                <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/Delete.gif"
                                    OnClientClick="javascript:return window.confirm('Are you sure want to delete this data?')">
                                </asp:ImageButton>&nbsp;
                            </td>
                            <td>
                                <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/Cancel.gif">
                                </asp:ImageButton>&nbsp;
                            </td>
                            <td width="99%">
                                <img src="Images/blank.gif" width="1" height="1" /></td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </ajax:AjaxPanel>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
