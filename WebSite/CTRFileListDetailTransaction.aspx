<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CTRFileListDetailTransaction.aspx.vb" Inherits="CTRFileListDetailTransaction" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                            
                        </td>
                        <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="20" height="100%" />
                            </td>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%">
                                    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none;
                                                font-family: Tahoma">
                                                <img src="Images/dot_title.gif" width="17" height="17">&nbsp;<b><asp:Label ID="Label1"
                                                    runat="server" Text="CTR Report File - List of Transactions"></asp:Label></b><hr />
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" style="background-color: White;
                                        border-color: White;" width="100%">
                                        <tr>
                                            <td>
                                                <table align="left" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                    bgcolor="#dddddd" border="2">
                                                    <tr>
                                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                            style="height: 6px">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="formtext">
                                                                        <asp:Label ID="Label9" runat="server" Text="CTR File Information" Font-Bold="True"></asp:Label>&nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <a href="#" onclick="javascript:ShowHidePanel('TrCTRFileInfo','ImgCTRFileInfo','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                            title="click to minimize or maximize">
                                                                            <img id="ImgCTRFileInfo" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                                width="12px"></a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="TrCTRFileInfo">
                                                        <td valign="top" bgcolor="#ffffff">
                                                            <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                    <tr class="formText">
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="Label3" runat="server" Text="File Name"></asp:Label>
                                                                        </td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :
                                                                        </td>
                                                                        <td width="30%" bgcolor="#ffffff">
                                                                            <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                                                                                <asp:Label ID="LblTextFileName" runat="server"></asp:Label></ajax:AjaxPanel>
                                                                        </td>
                                                                        <td width="3%" bgcolor="#ffffff">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="Label5" runat="server" Text="Report Type"></asp:Label>
                                                                        </td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :
                                                                        </td>
                                                                        <td width="30%" bgcolor="#ffffff">
                                                                            <ajax:AjaxPanel ID="AjaxPanel15" runat="server">
                                                                                <asp:Label ID="LblCustomerType" runat="server"></asp:Label></ajax:AjaxPanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="formText">
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="Label7" runat="server" Text="Total # CIF/# WIC"></asp:Label>
                                                                        </td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :
                                                                        </td>
                                                                        <td width="30%" bgcolor="#ffffff">
                                                                            <ajax:AjaxPanel ID="AjaxPanel17" runat="server">
                                                                                <asp:Label ID="LblTotalNumberofCIF" runat="server"></asp:Label></ajax:AjaxPanel>
                                                                        </td>
                                                                        <td width="3%" bgcolor="#ffffff">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="Label11" runat="server" Text="Report Format"></asp:Label>
                                                                        </td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :
                                                                        </td>
                                                                        <td width="30%" bgcolor="#ffffff">
                                                                            <ajax:AjaxPanel ID="AjaxPanel19" runat="server">
                                                                                <asp:Label ID="LblReportFormat" runat="server"></asp:Label></ajax:AjaxPanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="formText">
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="Label10" runat="server" Text="Total Amount"></asp:Label>
                                                                        </td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :
                                                                        </td>
                                                                        <td width="30%" bgcolor="#ffffff">
                                                                            <ajax:AjaxPanel ID="AjaxPanel20" runat="server">
                                                                                (IDR-Cash In&nbsp; )&nbsp;
                                                                                <asp:Label ID="LblTotalAmountCashIn" runat="server"></asp:Label>
                                                                                <br />
                                                                                (IDR-Cash Out)&nbsp;
                                                                                <asp:Label ID="LblTotalAmountCashOut" runat="server"></asp:Label>
                                                                            </ajax:AjaxPanel>
                                                                        </td>
                                                                        <td width="3%" bgcolor="#ffffff">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td width="15%" bgcolor="#ffffff">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td width="30%" bgcolor="#ffffff">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table align="left" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                    bgcolor="#dddddd" border="2">
                                                    <tr>
                                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                            style="height: 6px">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="formtext" style="height: 14px">
                                                                        <asp:Label ID="Label8" runat="server" Text="List of Transactions" Font-Bold="True"></asp:Label>&nbsp;
                                                                    </td>
                                                                    <td style="height: 14px">
                                                                        <a href="#" onclick="javascript:ShowHidePanel('TrGridData','ImgGriddData','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                            title="click to minimize or maximize">
                                                                            <img id="ImgGriddData" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                                width="12px"></a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="TrGridData">
                                                        <td valign="top" width="100%" bgcolor="#ffffff">
                                                            <table cellspacing="4" cellpadding="0" width="100%" border="0">
                                                                <tr>
                                                                    <td valign="middle" width="100%" nowrap>
                                                                        <table style="width: 100%; height: 100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <ajax:AjaxPanel ID="AjaxPanel4" runat="server" Width="1300px">
                                                                                        <asp:DataGrid ID="DtGridTransactionsList" runat="server" SkinID="gridview" AutoGenerateColumns="False"
                                                                                            Font-Size="XX-Small" CellPadding="4" AllowPaging="True" Width="100%"
                                                                                            GridLines="Vertical" AllowSorting="True" ForeColor="Black" 
                                                                                            Font-Bold="False" Font-Italic="False"
                                                                                            Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left"
                                                                                            BorderWidth="1px" BackColor="White" BorderColor="#DEDFDE" 
                                                                                            BorderStyle="None">
                                                                                            <AlternatingItemStyle BackColor="White" />
                                                                                            <Columns>
                                                                                                <%--<asp:BoundColumn DataField="MsAccountType_Name" HeaderText="Account Type" SortExpression="MsAccountType_Name desc">
                                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Wrap="False" />
                                                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Width="4%"
                                                                                                        Wrap="False" />
                                                                                                </asp:BoundColumn>--%>
                                                                                               <%-- <asp:BoundColumn DataField="MsTransactionCode_Code" HeaderText="TC" SortExpression="MsTransactionCode_Code desc">
                                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                        Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" Wrap="False" />
                                                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                        Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" Width="3%"
                                                                                                        Wrap="False" />
                                                                                                </asp:BoundColumn>--%>
                                                                                                <asp:BoundColumn HeaderText="ID" Visible="False">
                                                                                                    <HeaderStyle Width="2px" />
                                                                                                </asp:BoundColumn>
                                                                                                <asp:TemplateColumn HeaderText="No.">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="LblNo" runat="server"></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                    <EditItemTemplate>
                                                                                                        <asp:TextBox runat="server"></asp:TextBox>
                                                                                                    </EditItemTemplate>
                                                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                                        Font-Strikeout="False" Font-Underline="False" ForeColor="White" Width="2px" />
                                                                                                </asp:TemplateColumn>
                                                                                                <asp:BoundColumn DataField="NamaNAsabah" HeaderText="Customer Full Name" 
                                                                                                    SortExpression="NamaNAsabah  desc">
                                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" 
                                                                                                        Wrap="False" />

                                                                                                </asp:BoundColumn>
                                                                                                <asp:BoundColumn DataField="TransactionDate" HeaderText="TransactionDate"
                                                                                                    SortExpression="TransactionDate  desc"  DataFormatString="{0:dd-MMM-yyyy}">
                                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                        Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" 
                                                                                                        Wrap="False" />
                                                                                                    
                                                                                                </asp:BoundColumn>
                                                                                                <asp:BoundColumn DataField="CTRXmlFileName" HeaderText="CTRFileName" 
                                                                                                    SortExpression="CTRXmlFileName  desc">
                                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Right" 
                                                                                                        VerticalAlign="Top" Wrap="False" />
                                                                                                    
                                                                                                </asp:BoundColumn>
                                                                                                <asp:BoundColumn DataField="TotalCashIn" DataFormatString="{0:#,##0.#0}" 
                                                                                                    HeaderText="TotalCashIn" SortExpression="TotalCashIn  desc">
                                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Right" 
                                                                                                        VerticalAlign="Top" Wrap="False" />
                                                                                                    
                                                                                                </asp:BoundColumn>
                                                                                                <asp:BoundColumn DataField="TotalCashOut" DataFormatString="{0:#,##0.#0}" 
                                                                                                    HeaderText="TotalCashOut" SortExpression="TotalCashOut  desc">
                                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Right" 
                                                                                                        VerticalAlign="Top" Wrap="False" />
                                                                                                    
                                                                                                </asp:BoundColumn>
                                                                                            </Columns>
                                                                                            <PagerStyle HorizontalAlign="Right" ForeColor="Black" 
                                                                                                BackColor="#F7F7DE" Mode="NumericPages">
                                                                                            </PagerStyle>
                                                                                            <ItemStyle BorderColor="#CC9966" BorderStyle="Solid" BackColor="#F7F7DE" />
                                                                                            <FooterStyle BackColor="#CCCC99" />
                                                                                            <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B" />
                                                                                            <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                                        </asp:DataGrid>
                                                                                        </ajax:AjaxPanel>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="background-color: #ffffff; height: 5px;">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="background-color: #ffffff">
                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                        <tr>
                                                                                            <td nowrap>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                            <td width="100%">
                                                                                                &nbsp;&nbsp;
                                                                                            </td>
                                                                                            <td align="right" nowrap>
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td bgcolor="#ffffff" colspan="3">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="Images/blank.gif" width="5" height="1" />
                        </td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="images/arrow.gif" width="15" height="15" />
                        </td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            &nbsp;<ajax:AjaxPanel ID="AjaxPanel22" runat="server"><asp:ImageButton ID="ImgBack"
                                runat="server" ImageUrl="~/Images/button/back.gif"></asp:ImageButton></ajax:AjaxPanel>
                        </td>
                        <td width="99%" background="Images/button-bground.gif">
                            <img src="Images/blank.gif" width="1" height="1" />
                            <asp:CustomValidator ID="CValPageErr" runat="server" Display="Dynamic"></asp:CustomValidator>
                        </td>
                        <td>
                           
                        </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
