<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="WorkingUnitManagementApprovalDetail.aspx.vb" Inherits="WorkingUnitManagementApprovalDetail" title="Working Unit Management Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <TABLE id="TableDetail" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="2" runat="server">
	    <TR class="formText" id="UserAdd1">
		    <TD bgColor="#ffffff" height="24" style="width: 22px">&nbsp;</TD>
		    <TD bgColor="#ffffff">
                Working Unit Name</TD>
		    <TD bgColor="#ffffff" style="width: 44px">:</TD>
		    <TD width="80%" bgColor="#ffffff"><asp:label id="LabelWorkingUnitNameAdd" runat="server"></asp:label></TD>
	    </TR>
        <tr class="formText" id="UserAdd2">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                Level Type Name</td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff" width="80%">
                <asp:Label ID="LabelLevelTypeNameAdd" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserAdd3">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                Working Unit Parent</td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff" width="80%">
                <asp:Label ID="LabelWorkingUnitParentNameAdd" runat="server"></asp:Label></td>
        </tr>
	    <tr class="formText" id="UserAdd4">
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Description<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff" style="width: 44px">:<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff">
                <asp:TextBox ID="TextWorkingUnitDescriptionAdd" runat="server" MaxLength="255" ReadOnly="True"
                    Rows="5" TextMode="MultiLine" Width="312px"></asp:TextBox></td>
	    </tr>
	    <tr class="formText" id="UserEdi1">
		    <td height="24" colspan="4" bgcolor="#ffffcc">&nbsp;Old Values</td>
		    <td colspan="4" bgcolor="#ffffcc">&nbsp;New Values</td>
	    </tr>
	    <tr class="formText" id="UserEdi2">
		    <td bgcolor="#ffffff" style="width: 22px; height: 32px">&nbsp;</td>
		    <td bgcolor="#ffffff" style="height: 32px;">
                Working Unit ID</td>
		    <td bgcolor="#ffffff" style="width: 44px; height: 32px;">:</td>
		    <td width="40%" bgcolor="#ffffff" style="height: 32px"><asp:label id="LabelOldWorkingUnitID" runat="server"></asp:label></td>
		    <td width="5" bgcolor="#ffffff" style="height: 32px">&nbsp;</td>
		    <td width="10%" bgcolor="#ffffff" style="height: 32px">
                Working Unit ID</td>
		    <td width="5" bgcolor="#ffffff" style="height: 32px">:</td>
		    <td width="40%" bgcolor="#ffffff" style="height: 32px"><asp:label id="LabelNewWorkingUnitID" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserEdi3">
		    <td bgcolor="#ffffff" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Working Unit Name</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelOldWorkingUnitName" runat="server"></asp:label></td>
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Working Unit Name</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelNewWorkingUnitName" runat="server"></asp:label></td>
	    </tr>
        <tr class="formText" id="UserEdi4">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Level Type Name</td>
            <td bgcolor="#ffffff" style="width: 44px">
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelOldLevelTypeName" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff">
                Level Type Name</td>
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNewLevelTypeName" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserEdi5">
            <td bgcolor="#ffffff" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Working Unit Parent</td>
            <td bgcolor="#ffffff" style="width: 44px">
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelOldWorkingUnitParentName" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff">
                Working Unit Parent</td>
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNewWorkingUnitParentName" runat="server"></asp:Label></td>
        </tr>
	    <tr class="formText" id="UserEdi6">
		    <td bgcolor="#ffffff" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Description<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff" style="width: 44px">:<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff">
                <asp:TextBox ID="TextOldWorkingUnitDescription" runat="server"
                    MaxLength="255" ReadOnly="True" Rows="5" TextMode="MultiLine" Width="312px"></asp:TextBox></td>
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Description<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff">:<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff">
                <asp:TextBox ID="TextNewWorkingUnitDescription" runat="server" MaxLength="255"
                    ReadOnly="True" Rows="5" TextMode="MultiLine" Width="312px"></asp:TextBox></td>
	    </tr>
	    <TR class="formText" id="UserDel1">
		    <TD bgColor="#ffffff" style="height: 23px; width: 22px;">&nbsp;</TD>
		    <TD bgColor="#ffffff" style="height: 23px;">
                Working Unit ID</TD>
		    <TD bgColor="#ffffff" style="height: 23px; width: 44px;">:</TD>
		    <TD width="80%" bgColor="#ffffff" style="height: 23px"><asp:label id="LabelWorkingUnitIDDelete" runat="server"></asp:label></TD>
	    </TR>
	    <tr class="formText" id="UserDel2">
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Working Unit Name</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelWorkingUnitNameDelete" runat="server"></asp:label></td>
	    </tr>
        <tr class="formText" id="UserDel3">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                Level Type Name</td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelLevelTypeNameDelete" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserDel4">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                Working Unit Parent</td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelWorkingUnitParentNameDelete" runat="server"></asp:Label></td>
        </tr>
	    <tr class="formText" id="UserDel5">
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Description<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff" style="width: 44px">:<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff">
                <asp:TextBox ID="TextWorkingUnitDescriptionDelete" runat="server" MaxLength="255" ReadOnly="True" Rows="5"
                    TextMode="MultiLine" Width="312px"></asp:TextBox></td>
	    </tr>
	    <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="False" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="none"></asp:CustomValidator></TD>
	    </TR>
    </TABLE>
</asp:Content>