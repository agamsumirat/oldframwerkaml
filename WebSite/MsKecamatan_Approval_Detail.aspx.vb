#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsKecamatan_APPROVAL_Detail
    Inherits Parent

#Region "Function"

    Sub HideControl()
        imgReject.Visible = False
        imgApprove.Visible = False
        ImageCancel.Visible = False
    End Sub

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsKecamatan_Approval_View.aspx")
    End Sub

    Protected Sub imgReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgReject.Click
        Try
            Rejected()
            ChangeMultiView(1)
            LblConfirmation.Text = "Data has been rejected"
            HideControl()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgApprove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgApprove.Click
        Try
            Using ObjMsKecamatan_Approval As MsKecamatan_Approval = DataRepository.MsKecamatan_ApprovalProvider.GetByPK_MsKecamatan_Approval_Id(parID)
                If ObjMsKecamatan_Approval.FK_MsMode_Id.GetValueOrDefault = 1 Then
                    Inserdata(ObjMsKecamatan_Approval)
                ElseIf ObjMsKecamatan_Approval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                    UpdateData(ObjMsKecamatan_Approval)
                ElseIf ObjMsKecamatan_Approval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                    DeleteData(ObjMsKecamatan_Approval)
                End If
            End Using
            ChangeMultiView(1)
            LblConfirmation.Text = "Data approved successful"
            HideControl()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
                ListmapingNew = New TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail)
                ListmapingOld = New TList(Of MappingMsKecamatanNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim PkObject As String
        'Load new data
        Using ObjMsKecamatan_ApprovalDetail As MsKecamatan_ApprovalDetail = DataRepository.MsKecamatan_ApprovalDetailProvider.GetPaged(MsKecamatan_ApprovalDetailColumn.FK_MsKecamatan_Approval_Id.ToString & _
         "=" & _
         parID, "", 0, 1, Nothing)(0)
            Dim CekMode As MsKecamatan_Approval = DataRepository.MsKecamatan_ApprovalProvider.GetByPK_MsKecamatan_Approval_Id(ObjMsKecamatan_ApprovalDetail.FK_MsKecamatan_Approval_Id)
            Dim nama As String = CekMode.FK_MsMode_Id
            If nama = 1 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 2 Then
                baru.Visible = True
                lama.Visible = True
                baruNew.Visible = True
                lamaOld.Visible = True
            ElseIf nama = 3 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 4 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            End If
            With ObjMsKecamatan_ApprovalDetail
                SafeDefaultValue = "-"
                HFKotaKabNew.Value = Safe(.IDKotaKabupaten)
                Dim OKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(.IDKotaKabupaten.GetValueOrDefault)
                If OKotaKab IsNot Nothing Then LBSearchNamaKotaKabNew.Text = Safe(OKotaKab.NamaKotaKab)

                txtIDKecamatanNew.Text = Safe(.IDKecamatan)
                txtNamaKecamatannew.Text = Safe(.NamaKecamatan)

                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByNew.Text = Omsuser(0).UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyNew.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
                lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
                txtActivationNew.Text = SafeActiveInactive(.Activation)
                Dim L_objMappingMsKecamatanNCBSPPATK_Approval_Detail As TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail)
                L_objMappingMsKecamatanNCBSPPATK_Approval_Detail = DataRepository.MappingMsKecamatanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKecamatanNCBSPPATK_Approval_DetailColumn.PK_MsKecamatan_Approval_Id.ToString & _
                 "=" & _
                 parID, "", 0, Integer.MaxValue, Nothing)
                'Add mapping
                ListmapingNew.AddRange(L_objMappingMsKecamatanNCBSPPATK_Approval_Detail)

            End With
            PkObject = ObjMsKecamatan_ApprovalDetail.IDKecamatan
        End Using

        'Load Old Data
        Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(PkObject)
            If ObjMsKecamatan Is Nothing Then Return
            With ObjMsKecamatan
                SafeDefaultValue = "-"
                HFKotaKabOld.Value = Safe(.IDKotaKabupaten)
                Dim OKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(.IDKotaKabupaten.GetValueOrDefault)
                If OKotaKab IsNot Nothing Then LBSearchNamaKotaKabOld.Text = Safe(OKotaKab.NamaKotaKab)

                txtIDKecamatanOld.Text = Safe(.IDKecamatan)
                txtNamaKecamatanOld.Text = Safe(.NamaKecamatan)

                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByOld.Text = Omsuser(0).UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyOld.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
                lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
                Dim L_objMappingMsKecamatanNCBSPPATK As TList(Of MappingMsKecamatanNCBSPPATK)
                txtActivationOld.Text = SafeActiveInactive(.Activation)
                L_objMappingMsKecamatanNCBSPPATK = DataRepository.MappingMsKecamatanNCBSPPATKProvider.GetPaged(MappingMsKecamatanNCBSPPATKColumn.IDKecamatan.ToString & _
                   "=" & _
                   PkObject, "", 0, Integer.MaxValue, Nothing)
                ListmapingOld.AddRange(L_objMappingMsKecamatanNCBSPPATK)
            End With
        End Using
    End Sub



    Protected Sub imgBtnConfirmation_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnConfirmation.Click
        Try
            Response.Redirect("MsKecamatan_approval_view.aspx")
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping New sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingNew() As TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail)
        Get
            Return Session("ListmapingNew.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail))
            Session("ListmapingNew.data") = value
        End Set
    End Property

    ''' <summary>
    ''' Menyimpan Item untuk mapping Old sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingOld() As TList(Of MappingMsKecamatanNCBSPPATK)
        Get
            Return Session("ListmapingOld.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKecamatanNCBSPPATK))
            Session("ListmapingOld.data") = value
        End Set
    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKecamatanNCBSPPATK_Approval_Detail In ListmapingNew.FindAllDistinct("IDKecamatanNCBS")
                Temp.Add(i.IDKecamatanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKecamatanNCBSPPATK In ListmapingOld.FindAllDistinct("IDKecamatanNCBS")
                Temp.Add(i.IDKecamatanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

#Region "Action Approval..."

    Sub Rejected()
        Dim ObjMsKecamatan_Approval As MsKecamatan_Approval = DataRepository.MsKecamatan_ApprovalProvider.GetByPK_MsKecamatan_Approval_Id(parID)
        Dim ObjMsKecamatan_ApprovalDetail As MsKecamatan_ApprovalDetail = DataRepository.MsKecamatan_ApprovalDetailProvider.GetPaged(MsKecamatan_ApprovalDetailColumn.FK_MsKecamatan_Approval_Id.ToString & "=" & ObjMsKecamatan_Approval.PK_MsKecamatan_Approval_Id, "", 0, 1, Nothing)(0)
        Dim L_ObjMappingMsKecamatanNCBSPPATK_Approval_Detail As TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKecamatanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKecamatanNCBSPPATK_Approval_DetailColumn.PK_MsKecamatan_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)
        DeleteApproval(ObjMsKecamatan_Approval, ObjMsKecamatan_ApprovalDetail, L_ObjMappingMsKecamatanNCBSPPATK_Approval_Detail)
    End Sub

    Sub DeleteApproval(ByRef objMsKecamatan_Approval As MsKecamatan_Approval, ByRef ObjMsKecamatan_ApprovalDetail As MsKecamatan_ApprovalDetail, ByRef L_ObjMappingMsKecamatanNCBSPPATK_Approval_Detail As TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail))
        DataRepository.MsKecamatan_ApprovalProvider.Delete(objMsKecamatan_Approval)
        DataRepository.MsKecamatan_ApprovalDetailProvider.Delete(ObjMsKecamatan_ApprovalDetail)
        DataRepository.MappingMsKecamatanNCBSPPATK_Approval_DetailProvider.Delete(L_ObjMappingMsKecamatanNCBSPPATK_Approval_Detail)
    End Sub

    ''' <summary>
    ''' Delete Data dari  asli dari approval
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DeleteData(ByVal objMsKecamatan_Approval As MsKecamatan_Approval)
        '   '================= Delete Header ===========================================================
        Dim LObjMsKecamatan_ApprovalDetail As TList(Of MsKecamatan_ApprovalDetail) = DataRepository.MsKecamatan_ApprovalDetailProvider.GetPaged(MsKecamatan_ApprovalDetailColumn.FK_MsKecamatan_Approval_Id.ToString & "=" & objMsKecamatan_Approval.PK_MsKecamatan_Approval_Id, "", 0, 1, Nothing)
        If LObjMsKecamatan_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        Dim ObjMsKecamatan_ApprovalDetail As MsKecamatan_ApprovalDetail = LObjMsKecamatan_ApprovalDetail(0)
        Using ObjNewMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(ObjMsKecamatan_ApprovalDetail.IDKecamatan)
            DataRepository.MsKecamatanProvider.Delete(ObjNewMsKecamatan)
        End Using

        '======== Delete MAPPING =========================================
        Dim L_ObjMappingMsKecamatanNCBSPPATK_Approval_Detail As TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKecamatanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKecamatanNCBSPPATK_Approval_DetailColumn.PK_MsKecamatan_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


        For Each c As MappingMsKecamatanNCBSPPATK_Approval_Detail In L_ObjMappingMsKecamatanNCBSPPATK_Approval_Detail
            Using ObjNewMappingMsKecamatanNCBSPPATK As MappingMsKecamatanNCBSPPATK = DataRepository.MappingMsKecamatanNCBSPPATKProvider.GetByPK_MappingMsKecamatanNCBSPPATK_Id(c.PK_MappingMsKecamatanNCBSPPATK_Id)
                DataRepository.MappingMsKecamatanNCBSPPATKProvider.Delete(ObjNewMappingMsKecamatanNCBSPPATK)
            End Using
        Next

        '======== Delete Approval data ======================================
        DeleteApproval(objMsKecamatan_Approval, ObjMsKecamatan_ApprovalDetail, L_ObjMappingMsKecamatanNCBSPPATK_Approval_Detail)
    End Sub

    ''' <summary>
    ''' Update Data dari approval ke table asli 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateData(ByVal objMsKecamatan_Approval As MsKecamatan_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
        '   '================= Update Header ===========================================================
        Dim LObjMsKecamatan_ApprovalDetail As TList(Of MsKecamatan_ApprovalDetail) = DataRepository.MsKecamatan_ApprovalDetailProvider.GetPaged(MsKecamatan_ApprovalDetailColumn.FK_MsKecamatan_Approval_Id.ToString & "=" & objMsKecamatan_Approval.PK_MsKecamatan_Approval_Id, "", 0, 1, Nothing)
        If LObjMsKecamatan_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        Dim ObjMsKecamatan_ApprovalDetail As MsKecamatan_ApprovalDetail = LObjMsKecamatan_ApprovalDetail(0)
        Dim ObjMsKecamatanNew As MsKecamatan
        Using ObjMsKecamatanOld As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(ObjMsKecamatan_ApprovalDetail.IDKecamatan)
            If ObjMsKecamatanOld Is Nothing Then Throw New Exception("Data Not Found")
            ObjMsKecamatanNew = ObjMsKecamatanOld.Clone
            With ObjMsKecamatanNew
                FillOrNothing(.NamaKecamatan, ObjMsKecamatan_ApprovalDetail.NamaKecamatan)
                FillOrNothing(.CreatedBy, ObjMsKecamatan_ApprovalDetail.CreatedBy)
                FillOrNothing(.CreatedDate, ObjMsKecamatan_ApprovalDetail.CreatedDate)
                FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
                FillOrNothing(.ApprovedDate, Date.Now)
                .Activation = True
                .IDKotaKabupaten = ObjMsKecamatan_ApprovalDetail.IDKotaKabupaten
            End With
            DataRepository.MsKecamatanProvider.Save(ObjMsKecamatanNew)
            'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "MsKecamatan has Approved to Update data", ObjMsKecamatanNew, ObjMsKecamatanOld)
            '======== Update MAPPING =========================================
            'delete mapping item
            Using L_ObjMappingMsKecamatanNCBSPPATK As TList(Of MappingMsKecamatanNCBSPPATK) = DataRepository.MappingMsKecamatanNCBSPPATKProvider.GetPaged(MappingMsKecamatanNCBSPPATKColumn.IDKecamatan.ToString & _
             "=" & _
             ObjMsKecamatanNew.IDKecamatan, "", 0, Integer.MaxValue, Nothing)
                DataRepository.MappingMsKecamatanNCBSPPATKProvider.Delete(L_ObjMappingMsKecamatanNCBSPPATK)
            End Using
            'Insert mapping item
            Dim L_ObjMappingMsKecamatanNCBSPPATK_Approval_Detail As TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKecamatanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKecamatanNCBSPPATK_Approval_DetailColumn.PK_MsKecamatan_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            Dim L_ObjNewMappingMsKecamatanNCBSPPATK As New TList(Of MappingMsKecamatanNCBSPPATK)
            For Each c As MappingMsKecamatanNCBSPPATK_Approval_Detail In L_ObjMappingMsKecamatanNCBSPPATK_Approval_Detail
                Dim ObjNewMappingMsKecamatanNCBSPPATK As New MappingMsKecamatanNCBSPPATK '= DataRepository.MappingMsKecamatanNCBSPPATKProvider.GetByPK_MappingMsKecamatanNCBSPPATK_Id(c.PK_MappingMsKecamatanNCBSPPATK_Id.GetValueOrDefault)
                With ObjNewMappingMsKecamatanNCBSPPATK
                    FillOrNothing(.IDKecamatan, ObjMsKecamatanNew.IDKecamatan)
                    FillOrNothing(.Nama, c.Nama)
                    FillOrNothing(.IDKecamatanNCBS, c.IDKecamatanNCBS)
                    L_ObjNewMappingMsKecamatanNCBSPPATK.Add(ObjNewMappingMsKecamatanNCBSPPATK)
                End With
            Next
            DataRepository.MappingMsKecamatanNCBSPPATKProvider.Save(L_ObjNewMappingMsKecamatanNCBSPPATK)
            '======== Delete Approval data ======================================
            DeleteApproval(objMsKecamatan_Approval, ObjMsKecamatan_ApprovalDetail, L_ObjMappingMsKecamatanNCBSPPATK_Approval_Detail)
        End Using
    End Sub
    ''' <summary>
    ''' Insert Data dari approval ke table asli 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Inserdata(ByVal objMsKecamatan_Approval As MsKecamatan_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
        '   '================= Save Header ===========================================================
        Dim LObjMsKecamatan_ApprovalDetail As TList(Of MsKecamatan_ApprovalDetail) = DataRepository.MsKecamatan_ApprovalDetailProvider.GetPaged(MsKecamatan_ApprovalDetailColumn.FK_MsKecamatan_Approval_Id.ToString & "=" & objMsKecamatan_Approval.PK_MsKecamatan_Approval_Id, "", 0, 1, Nothing)
        'jika data tidak ada di database
        If LObjMsKecamatan_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        'binding approval ke table asli
        Dim ObjMsKecamatan_ApprovalDetail As MsKecamatan_ApprovalDetail = LObjMsKecamatan_ApprovalDetail(0)
        Using ObjNewMsKecamatan As New MsKecamatan()
            With ObjNewMsKecamatan
                FillOrNothing(.IDKecamatan, ObjMsKecamatan_ApprovalDetail.IDKecamatan)
                FillOrNothing(.NamaKecamatan, ObjMsKecamatan_ApprovalDetail.NamaKecamatan)
                FillOrNothing(.CreatedBy, ObjMsKecamatan_ApprovalDetail.CreatedBy)
                FillOrNothing(.CreatedDate, ObjMsKecamatan_ApprovalDetail.CreatedDate)
                FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
                FillOrNothing(.ApprovedDate, Date.Now)
                .Activation = True
                .IDKotaKabupaten = ObjMsKecamatan_ApprovalDetail.IDKotaKabupaten
            End With
            DataRepository.MsKecamatanProvider.Save(ObjNewMsKecamatan)
            'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "MsKecamatan has Approved to Insert data", ObjNewMsKecamatan, Nothing)
            '======== Insert MAPPING =========================================
            Dim L_ObjMappingMsKecamatanNCBSPPATK_Approval_Detail As TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKecamatanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKecamatanNCBSPPATK_Approval_DetailColumn.PK_MsKecamatan_Approval_Id.ToString & _
             "=" & _
             parID, "", 0, Integer.MaxValue, Nothing)
            Dim L_ObjNewMappingMsKecamatanNCBSPPATK As New TList(Of MappingMsKecamatanNCBSPPATK)()
            For Each c As MappingMsKecamatanNCBSPPATK_Approval_Detail In L_ObjMappingMsKecamatanNCBSPPATK_Approval_Detail
                Dim ObjNewMappingMsKecamatanNCBSPPATK As New MappingMsKecamatanNCBSPPATK()
                With ObjNewMappingMsKecamatanNCBSPPATK
                    FillOrNothing(.IDKecamatan, ObjNewMsKecamatan.IDKecamatan)
                    FillOrNothing(.Nama, c.Nama)
                    FillOrNothing(.IDKecamatanNCBS, c.IDKecamatanNCBS)
                    L_ObjNewMappingMsKecamatanNCBSPPATK.Add(ObjNewMappingMsKecamatanNCBSPPATK)
                End With
            Next
            DataRepository.MappingMsKecamatanNCBSPPATKProvider.Save(L_ObjNewMappingMsKecamatanNCBSPPATK)
            '======== Delete Approval data ======================================
            DeleteApproval(objMsKecamatan_Approval, ObjMsKecamatan_ApprovalDetail, L_ObjMappingMsKecamatanNCBSPPATK_Approval_Detail)
        End Using
    End Sub

#End Region

End Class


