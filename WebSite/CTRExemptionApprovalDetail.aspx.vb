Imports System.Data
Imports System.Data.SqlClient
Imports AML2015Nettier.Data
Imports AML2015Nettier.Entities
Partial Class CTRExemptionApprovalDetail
    Inherits Parent

#Region " Property "
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property
    Private ReadOnly Property ParamPkCTR() As Int64
        Get
            Return Me.Request.Params("PendingID")
        End Get
    End Property
    Private ReadOnly Property GetUserID() As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Group_PendingApprovalTableAdapter
                Return AccessPending.SelectGroup_PendingApprovalUserID(Me.ParamPkCTR)
            End Using
        End Get
    End Property


    Public Property ObjCTRExemptionList_PendingApproval() As CTRExemptionList_PendingApproval
        Get
            If Session("CTRExemptionApprovalDetail.ObjCTRExemptionList_PendingApproval") Is Nothing Then
                Session("CTRExemptionApprovalDetail.ObjCTRExemptionList_PendingApproval") = AMLBLL.CTRExemptionListBLL.GetCTRExemptionList_PendingApprovalByPk(ParamPkCTR)
                Return CType(Session("CTRExemptionApprovalDetail.ObjCTRExemptionList_PendingApproval"), CTRExemptionList_PendingApproval)
            Else
                Return CType(Session("CTRExemptionApprovalDetail.ObjCTRExemptionList_PendingApproval"), CTRExemptionList_PendingApproval)
            End If
        End Get
        Set(ByVal value As CTRExemptionList_PendingApproval)
            Session("CTRExemptionApprovalDetail.ObjCTRExemptionList_PendingApproval") = value
        End Set
    End Property

    Public Property ObjTCTRExemptionList_Approval() As TList(Of CTRExemptionList_Approval)
        Get
            If Session("CTRExemptionApprovalDetail.ObjCTRExemptionList_Approval") Is Nothing Then
                Session("CTRExemptionApprovalDetail.ObjCTRExemptionList_Approval") = AMLBLL.CTRExemptionListBLL.GetTlistCTRExemptionList_Approval("CTRPendingApproval_ID=" & Me.ParamPkCTR, "", 0, Integer.MaxValue, 0)
                Return CType(Session("CTRExemptionApprovalDetail.ObjCTRExemptionList_Approval"), TList(Of CTRExemptionList_Approval))
            Else
                Return CType(Session("CTRExemptionApprovalDetail.ObjCTRExemptionList_Approval"), TList(Of CTRExemptionList_Approval))
            End If
        End Get
        Set(ByVal value As TList(Of CTRExemptionList_Approval))
            Session("CTRExemptionApprovalDetail.ObjCTRExemptionList_Approval") = value
        End Set
    End Property

   

#End Region

#Region " Load "
    Private Sub HideAll()
        Dim StrId As String = ""
        Select Case Me.ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.GroupAdd
                StrId = "UserAdd"
            Case Sahassa.AML.Commonly.TypeConfirm.GroupEdit
                StrId = "UserEdi"
            Case Sahassa.AML.Commonly.TypeConfirm.GroupDelete
                StrId = "UserDel"
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select
        For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
            ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID.Substring(0, 7) = StrId
        Next
    End Sub
    Private Sub LoadGroupAdd()
        Me.LabelTitle.Text = "Activity: Add New CTR Exemption"

        If Me.ObjTCTRExemptionList_Approval.Count > 0 Then

            If ObjTCTRExemptionList_Approval(0).FK_CTRExemptionType_ID.GetValueOrDefault(0) = 1 Then
                Me.LabelCIFNo.Text = ObjTCTRExemptionList_Approval(0).CIFNo
            Else
                Me.LabelCIFNo.Text = ObjTCTRExemptionList_Approval(0).AccountNo
            End If

            Me.LabelAccountNo.Text = ObjTCTRExemptionList_Approval(0).AccountNo
            Me.LabelCustomerName.Text = ObjTCTRExemptionList_Approval(0).CustomerName
            Me.TextGroupDescriptionAdd.Text = ObjTCTRExemptionList_Approval(0).Description

        End If

        'Using AccessPending As New AMLDAL.CTRExemptionListTableAdapters.CTRApprovalTableAdapter
        '    Using ObjTable As Data.DataTable = AccessPending.CTRApproval_GetData(Me.ParamPkCTR)
        '        'Bila ObjTable.Rows.Count > 0 berarti Group tsb masih ada dlm tabel Group_Approval
        '        If ObjTable.Rows.Count > 0 Then
        '            Dim rowData As AMLDAL.CTRExemptionList.CTRApprovalRow = ObjTable.Rows(0)
        '            Me.LabelAccountNo.Text = rowData.AccountNo
        '            Me.LabelGroupNameAdd.Text = rowData.CustomerName
        '            Me.TextGroupDescriptionAdd.Text = rowData.Description
        '        Else
        '            'Bila ObjTable.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
        '            Throw New Exception("Cannot load data from the following CTRPending_ApprovalID: " & Me.LabelGroupNameAdd.Text & " because that CTRPendingApproval_ID is no longer in the approval table.")
        '        End If
        '    End Using
        'End Using
    End Sub
    Private Sub LoadGroupEdit()
        Me.LabelTitle.Text = "Activity: Edit Group"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Group_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetGroupApprovalData(Me.ParamPkCTR)
                'Bila ObjTable.Rows.Count > 0 berarti Group tsb masih ada dlm tabel Group_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.Group_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelGroupIDEditNewGroupID.Text = rowData.GroupID
                    Me.LabelGroupEditNewGroupName.Text = rowData.GroupName
                    Me.TextGroupEditNewGroupDescription.Text = rowData.GroupDescription
                    Me.LabelGroupIDEditOldGroupID.Text = rowData.GroupID_Old
                    Me.LabelGroupNameEditOldGroupName.Text = rowData.GroupName_Old
                    Me.TextGroupDescriptionEditOldGroupDescription.Text = rowData.GroupDescription_Old
                Else 'Bila ObjTable.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
                    Throw New Exception("Cannot load data from the following Group: " & Me.LabelGroupNameEditOldGroupName.Text & " because that Group is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub
    Private Sub LoadGroupDelete()
        Me.LabelTitle.Text = "Activity: Delete"

        If Me.ObjTCTRExemptionList_Approval.Count > 0 Then

            If ObjTCTRExemptionList_Approval(0).FK_CTRExemptionType_ID.GetValueOrDefault(0) = 1 Then
                Me.LabelCIFNoDeleted.Text = ObjTCTRExemptionList_Approval(0).CIFNo
            Else
                Me.LabelCIFNoDeleted.Text = ObjTCTRExemptionList_Approval(0).AccountNo
            End If

            Me.LabelAccountNoDeleted.Text = ObjTCTRExemptionList_Approval(0).AccountNo
            Me.LabelCustomerNameDeleted.Text = ObjTCTRExemptionList_Approval(0).CustomerName
            Me.TxtDescriptionDeleted.Text = ObjTCTRExemptionList_Approval(0).Description

        End If

        'Using AccessPending As New AMLDAL.CTRExemptionListTableAdapters.CTRApprovalTableAdapter
        '    '.AMLDataSetTableAdapters.Group_ApprovalTableAdapter()
        '    Using ObjTable As Data.DataTable = AccessPending.CTRApproval_GetData(Me.ParamPkCTR)
        '        'Bila ObjTable.Rows.Count > 0 berarti Group tsb masih ada dlm tabel Group_Approval
        '        If ObjTable.Rows.Count > 0 Then
        '            Dim rowData As AMLDAL.CTRExemptionList.CTRApprovalRow = ObjTable.Rows(0)
        '            '.AMLDataSet.Group_ApprovalRow = ObjTable.Rows(0)
        '            Me.LabelGroupDeleteGroupID.Text = rowData.AccountNo
        '            Me.LabelGroupDeleteGroupName.Text = rowData.CustomerName
        '            Me.TextGroupDeleteGroupDescription.Text = rowData.Description
        '        Else 'Bila ObjTable.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
        '            Throw New Exception("Cannot load data from the following Account Number: " & Me.LabelGroupDeleteGroupID.Text & " because that Account is no longer in the approval table.")
        '        End If
        '    End Using
        'End Using
    End Sub
#End Region

#Region " Accept "
    Private Sub AcceptGroupAdd()



        Try
            AMLBLL.CTRExemptionListBLL.AcceptAddCtrExemptionList(Me.ParamPkCTR)
        Catch ex As Exception
            Throw
        End Try

        'Dim Trans As SqlTransaction = Nothing
        'Try
        '    Using AccessGroup As New AMLDAL.CTRExemptionListTableAdapters.CTRExemptionListTableAdapter
        '        Using AccessPending As New AMLDAL.CTRExemptionListTableAdapters.CTRApprovalTableAdapter
        '            Dim ObjTable As Data.DataTable = AccessPending.CTRApproval_GetData(Me.ParamPkCTR)

        '            'Bila ObjTable.Rows.Count > 0 berarti Group tsb masih ada dlm tabel Group_Approval
        '            If ObjTable.Rows.Count > 0 Then
        '                Dim rowData As AMLDAL.CTRExemptionList.CTRApprovalRow = ObjTable.Rows(0)

        '                'Periksa apakah GroupName yg baru tsb sudah ada dlm tabel Group atau belum. 
        '                Dim counter As Int32 = AccessGroup.GetCTRExemptionCount(rowData.AccountNo)

        '                'Bila counter = 0 berarti GroupName tsb belum ada dlm tabel Group, maka boleh ditambahkan
        '                If counter = 0 Then
        '                    'tambahkan item tersebut dalam tabel Group
        '                    Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessGroup)
        '                    AccessGroup.Insert(rowData.AccountNo, rowData.CustomerName, rowData.Description, rowData.CreatedDate, rowData.AddedBy)
        '                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
        '                    'catat aktifitas dalam tabel Audit Trail
        '                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
        '                        AccessAudit.Insert(Now, rowData.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList", "AccountNo", "Add", "", rowData.AccountNo, "Accepted")
        '                        AccessAudit.Insert(Now, rowData.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList", "CustomerName", "Add", "", rowData.CustomerName, "Accepted")
        '                        AccessAudit.Insert(Now, rowData.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList", "Description", "Add", "", rowData.Description, "Accepted")
        '                        AccessAudit.Insert(Now, rowData.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList", "CreatedDate", "Add", "", rowData.CreatedDate, "Accepted")
        '                        AccessAudit.Insert(Now, rowData.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList", "AddedBy", "Add", "", rowData.AddedBy, "Accepted")

        '                        'hapus item tersebut dalam tabel Group_Approval
        '                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, Trans)
        '                        AccessPending.CTRApproval_DeleteByPendingID(rowData.CTRPendingApproval_ID)

        '                        'hapus item tersebut dalam tabel Group_PendingApproval
        '                        Using AccessPendingGroup As New AMLDAL.CTRExemptionListTableAdapters.CTRPending_SelectCommandTableAdapter
        '                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingGroup, Trans)
        '                            AccessPendingGroup.CTRPending_DeleteByPendingID(rowData.CTRPendingApproval_ID)
        '                        End Using
        '                        Trans.Commit()
        '                    End Using
        '                Else 'Bila counter != 0 berarti GroupName tsb sudah ada dlm tabel Group, maka AcceptGroupAdd gagal
        '                    Throw New Exception("Cannot add the following CTR Exemption: " & rowData.AccountNo & " because that Account already exists in the database.")
        '                End If
        '            Else 'Bila ObjTable.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
        '                Throw New Exception("Cannot add the following CTR Exemption: " & Me.LabelCustomerName.Text & " because that Account is no longer in the approval table.")
        '            End If
        '        End Using
        '    End Using
        'Catch
        '    If Not Trans Is Nothing Then
        '        Trans.Rollback()
        '    End If
        '    Throw
        'Finally
        '    If Not Trans Is Nothing Then
        '        Trans.Dispose()
        '    End If
        'End Try
    End Sub

    Private Sub AcceptGroupDelete()

        AMLBLL.CTRExemptionListBLL.AcceptDeleteCtrExemptionList(Me.ParamPkCTR)

        'Dim Trans As SqlTransaction
        'Try
        '    Using AccessGroup As New AMLDAL.CTRExemptionListTableAdapters.CTRExemptionListTableAdapter()
        '        '.CTRExemptionListTableAdapters.CTRExemptionListTableAdapter()
        '        '.AMLDataSetTableAdapters.GroupTableAdapter()
        '        Using AccessPending As New AMLDAL.CTRExemptionListTableAdapters.CTRApprovalTableAdapter
        '            '.AMLDataSetTableAdapters.Group_ApprovalTableAdapter()
        '            Dim ObjTable As Data.DataTable = AccessPending.CTRApproval_GetData(Me.ParamPkCTR)

        '            'Bila ObjTable.Rows.Count > 0 berarti Group tsb masih ada dlm tabel Group_Approval
        '            If ObjTable.Rows.Count > 0 Then
        '                Dim rowData As AMLDAL.CTRExemptionList.CTRApprovalRow = ObjTable.Rows(0)
        '                '.AMLDataSet.Group_ApprovalRow = ObjTable.Rows(0)

        '                'Periksa apakah Group yg hendak didelete tsb ada dlm tabelnya atau tidak. 
        '                Dim counter As Int32 = AccessGroup.GetCTRExemptionCount(rowData.AccountNo)

        '                'Bila counter = 0 berarti Group tsb tidak ada dlm tabel Group, maka AcceptGroupDelete gagal
        '                If counter = 0 Then
        '                    Throw New Exception("Cannot delete the following CTR Exemption: " & rowData.AccountNo & " because that Account does not exist in the database anymore.")
        '                Else 'Bila counter != 0, maka Group tsb bisa didelete
        '                    'hapus item tersebut dari tabel Group
        '                    Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessGroup)
        '                    AccessGroup.CTRExemption_DeleteByAccountNo(rowData.AccountNo)
        '                    '.DeleteGroup(rowData.GroupID)

        '                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
        '                    'catat aktifitas dalam tabel Audit Trail
        '                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
        '                        AccessAudit.Insert(Now, rowData.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList", "AccountNo", "Delete", "", rowData.AccountNo, "Accepted")
        '                        AccessAudit.Insert(Now, rowData.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList", "CustomerName", "Delete", "", rowData.CustomerName, "Accepted")
        '                        AccessAudit.Insert(Now, rowData.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList", "Description", "Delete", "", rowData.Description, "Accepted")
        '                        AccessAudit.Insert(Now, rowData.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList", "CreatedDate", "Delete", "", rowData.CreatedDate, "Accepted")
        '                        AccessAudit.Insert(Now, rowData.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList", "AddedBy", "Delete", "", rowData.AddedBy, "Accepted")

        '                        'hapus item tersebut dalam tabel Group_Approval
        '                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, Trans)
        '                        AccessPending.CTRApproval_DeleteByPendingID(rowData.CTRPendingApproval_ID)

        '                        'hapus item tersebut dalam tabel Group_PendingApproval
        '                        Using AccessPendingGroup As New AMLDAL.CTRExemptionListTableAdapters.CTRPending_SelectCommandTableAdapter
        '                            '.AMLDataSetTableAdapters.Group_PendingApprovalTableAdapter()
        '                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingGroup, Trans)
        '                            AccessPendingGroup.CTRPending_DeleteByPendingID(rowData.CTRPendingApproval_ID)
        '                        End Using
        '                        Trans.Commit()
        '                    End Using
        '                End If
        '            Else 'Bila ObjTable.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
        '                Throw New Exception("Cannot delete the following CTR Exemption: " & Me.LabelCustomerName.Text & " because that Account is no longer in the approval table.")
        '            End If
        '        End Using
        '    End Using
        'Catch
        '    Throw
        '    If Not Trans Is Nothing Then
        '        Trans.Rollback()
        '    End If
        'Finally
        '    If Not Trans Is Nothing Then
        '        Trans.Dispose()
        '    End If
        'End Try
    End Sub

#End Region

#Region " Reject "
    Private Sub RejectGroupAdd()

        AMLBLL.CTRExemptionListBLL.RejectAddCtrExemptionList(Me.ParamPkCTR)
        'Dim Trans As SqlTransaction
        'Try
        '    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
        '        Using AccessGroup As New AMLDAL.CTRExemptionListTableAdapters.CTRApprovalTableAdapter
        '            '.AMLDataSetTableAdapters.Group_ApprovalTableAdapter()
        '            Using AccessGroupPending As New AMLDAL.CTRExemptionListTableAdapters.CTRPending_SelectCommandTableAdapter
        '                '.AMLDataSetTableAdapters.Group_PendingApprovalTableAdapter()
        '                Using TableGroup As Data.DataTable = AccessGroup.CTRApproval_GetData(Me.ParamPkCTR)

        '                    'Bila TableGroup.Rows.Count > 0 berarti Group tsb masih ada dlm tabel Group_Approval
        '                    If TableGroup.Rows.Count > 0 Then
        '                        Dim ObjRow As AMLDAL.CTRExemptionList.CTRApprovalRow = TableGroup.Rows(0)
        '                        '.AMLDataSet.Group_ApprovalRow = TableGroup.Rows(0)

        '                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
        '                        'catat aktifitas dalam tabel Audit Trail
        '                        AccessAudit.Insert(Now, ObjRow.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList_Approval", "AccountNo", "Add", "", ObjRow.AccountNo, "Rejected")
        '                        AccessAudit.Insert(Now, ObjRow.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList_Approval", "CustomerName", "Add", "", ObjRow.CustomerName, "Rejected")
        '                        AccessAudit.Insert(Now, ObjRow.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList_Approval", "Description", "Add", "", ObjRow.Description, "Rejected")
        '                        AccessAudit.Insert(Now, ObjRow.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList_Approval", "CreatedDate", "Add", "", ObjRow.CreatedDate, "Rejected")
        '                        AccessAudit.Insert(Now, ObjRow.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList_Approval", "AddedBy", "Add", "", ObjRow.AddedBy, "Rejected")

        '                        'hapus item tersebut dalam tabel Group_Approval
        '                        Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessGroup)
        '                        AccessGroup.CTRApproval_DeleteByPendingID(ObjRow.CTRPendingApproval_ID)

        '                        'hapus item tersebut dalam tabel Group_PendingApproval
        '                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessGroupPending, Trans)
        '                        AccessGroupPending.CTRPending_DeleteByPendingID(ObjRow.CTRPendingApproval_ID)
        '                        Trans.Commit()
        '                    Else 'Bila TableGroup.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
        '                        Throw New Exception("Operation failed. The following CTR Exemption: " & Me.LabelCustomerName.Text & " is no longer in the approval table.")
        '                    End If
        '                End Using
        '            End Using
        '        End Using
        '    End Using
        'Catch
        '    If Not Trans Is Nothing Then
        '        Trans.Rollback()
        '    End If
        '    Throw
        'Finally
        '    If Not Trans Is Nothing Then
        '        Trans.Dispose()
        '    End If
        'End Try
    End Sub

    Private Sub RejectGroupDelete()

        AMLBLL.CTRExemptionListBLL.RejectDeleteCtrExemptionList(Me.ParamPkCTR)
        'Dim Trans As SqlTransaction
        'Try
        '    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter()
        '        Using AccessGroup As New AMLDAL.CTRExemptionListTableAdapters.CTRApprovalTableAdapter
        '            '.AMLDataSetTableAdapters.Group_ApprovalTableAdapter()
        '            Using AccessGroupPending As New AMLDAL.CTRExemptionListTableAdapters.CTRPending_SelectCommandTableAdapter
        '                '.AMLDataSetTableAdapters.Group_PendingApprovalTableAdapter()
        '                Using TableGroup As Data.DataTable = AccessGroup.CTRApproval_GetData(Me.ParamPkCTR)
        '                    'Bila TableGroup.Rows.Count > 0 berarti Group tsb masih ada dlm tabel Group_Approval
        '                    If TableGroup.Rows.Count > 0 Then
        '                        Dim ObjRow As AMLDAL.CTRExemptionList.CTRApprovalRow = TableGroup.Rows(0)
        '                        '.AMLDataSet.Group_ApprovalRow = TableGroup.Rows(0)

        '                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
        '                        'catat aktifitas dalam tabel Audit Trail
        '                        AccessAudit.Insert(Now, ObjRow.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList_Approval", "AccountNo", "Delete", "", ObjRow.AccountNo, "Rejected")
        '                        AccessAudit.Insert(Now, ObjRow.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList_Approval", "CustomerName", "Delete", "", ObjRow.CustomerName, "Rejected")
        '                        AccessAudit.Insert(Now, ObjRow.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList_Approval", "Description", "Delete", "", ObjRow.Description, "Rejected")
        '                        AccessAudit.Insert(Now, ObjRow.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList_Approval", "CreatedDate", "Delete", "", ObjRow.CreatedDate, "Rejected")
        '                        AccessAudit.Insert(Now, ObjRow.AddedBy, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionList_Approval", "AddedBy", "Delete", "", ObjRow.AddedBy, "Rejected")

        '                        'hapus item tersebut dalam tabel Group_Approval
        '                        Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessGroup)
        '                        AccessGroup.CTRApproval_DeleteByPendingID(ObjRow.CTRPendingApproval_ID)

        '                        'hapus item tersebut dalam tabel Group_PendingApproval
        '                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessGroupPending, Trans)
        '                        AccessGroupPending.CTRPending_DeleteByPendingID(ObjRow.CTRPendingApproval_ID)
        '                        Trans.Commit()
        '                    Else 'Bila TableGroup.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
        '                        Throw New Exception("Operation failed. The following CTR Exemption: " & Me.LabelCustomerNameDeleted.Text & " is no longer in the approval table.")
        '                    End If
        '                End Using
        '            End Using
        '        End Using
        '    End Using
        'Catch
        '    If Not Trans Is Nothing Then
        '        Trans.Rollback()
        '    End If
        '    Throw
        'Finally
        '    If Not Trans Is Nothing Then
        '        Trans.Dispose()
        '    End If
        'End Try
    End Sub
#End Region

    Sub ClearSession()
        Session("CTRExemptionApprovalDetail.ObjCTRExemptionList_Approval") = Nothing
        Session("CTRExemptionApprovalDetail.ObjCTRExemptionList_PendingApproval") = Nothing
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                ClearSession()
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HideAll()
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.GroupAdd
                        Me.LoadGroupAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.GroupEdit
                        Me.LoadGroupEdit()
                    Case Sahassa.AML.Commonly.TypeConfirm.GroupDelete
                        Me.LoadGroupDelete()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.GroupAdd
                    Me.AcceptGroupAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.GroupDelete
                    Me.AcceptGroupDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "CTRExemptionApproval.aspx"
            Me.Response.Redirect("CTRExemptionApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.GroupAdd
                    Me.RejectGroupAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.GroupDelete
                    Me.RejectGroupDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "CTRExemptionApproval.aspx"
            Me.Response.Redirect("CTRExemptionApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "CTRExemptionApproval.aspx"
        Me.Response.Redirect("CTRExemptionApproval.aspx", False)
    End Sub
End Class