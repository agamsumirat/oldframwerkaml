#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports AMLBLL.AuditTrailBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsKecamatan_UploadApprovalDetail_Detail
    Inherits Parent

#Region "Function"

    Sub HideControl()
        ImageCancel.Visible = False
    End Sub

    Sub ChangeMultiView(index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Dim LngId As Long = 0
        Using ObjMsKecamatan_ApprovalDetail As TList(Of MsKecamatan_ApprovalDetail) = DataRepository.MsKecamatan_ApprovalDetailProvider.GetPaged("PK_MsKecamatan_ApprovalDetail_Id = " & parID, "", 0, Integer.MaxValue, 0)
            If ObjMsKecamatan_ApprovalDetail.Count > 0 Then
                LngId = ObjMsKecamatan_ApprovalDetail(0).FK_MsKecamatan_Approval_Id
            End If
        End Using
        Response.Redirect("MsKecamatan_UploadApprovalDetail_View.aspx?ID=" & LngId)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
                ListmapingNew = New TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail)
                ListmapingOld = New TList(Of MappingMsKecamatanNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim PkObject As String
        'Load new data
        Using ObjMsKecamatan_ApprovalDetail As MsKecamatan_ApprovalDetail = DataRepository.MsKecamatan_ApprovalDetailProvider.GetPaged(MsKecamatan_ApprovalDetailColumn.PK_MsKecamatan_ApprovalDetail_Id.ToString & _
            "=" & _
            parID, "", 0, 1, Nothing)(0)
            With ObjMsKecamatan_ApprovalDetail
                SafeDefaultValue = "-"
               HFKotaKabNew.value = Safe(.IDKotaKabupaten)
dim OKotaKab as  MsKotaKab = Datarepository.MsKotaKabProvider.GetByIDKotaKab(.IDKotaKabupaten.GetValueOrDefault)
if OKotaKab isnot nothing then LBSearchNamaKotaKabNew.text = safe(OKotaKab.NamaKotaKab)

txtIDKecamatanNew.Text = Safe(.IDKecamatan)
txtNamaKecamatanNew.Text = Safe(.NamaKecamatan)

                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByNew.Text = Omsuser(0).UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyNew.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
                lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
                'txtActivationNew.Text = SafeActiveInactive(.Activation)
                Dim L_objMappingMsKecamatanNCBSPPATK_Approval_Detail As TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail)
                L_objMappingMsKecamatanNCBSPPATK_Approval_Detail = DataRepository.MappingMsKecamatanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKecamatanNCBSPPATK_Approval_DetailColumn.IDKecamatan.ToString & _
                 "=" & _
                 ObjMsKecamatan_ApprovalDetail.IDKecamatan, "", 0, Integer.MaxValue, Nothing)
              
                'Add mapping
                ListmapingNew.AddRange(L_objMappingMsKecamatanNCBSPPATK_Approval_Detail)
               
            End With
            PkObject = ObjMsKecamatan_ApprovalDetail.IDKecamatan
        End Using



        'Load Old Data
        Using objMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(PkObject)
            If objMsKecamatan Is Nothing Then
                lamaOld.Visible = False
                lama.Visible = False
                Return
            End If

            With objMsKecamatan
                SafeDefaultValue = "-"
               HFKotaKabOld.value = Safe(.IDKotaKabupaten)
dim OKotaKab as  MsKotaKab = Datarepository.MsKotaKabProvider.GetByIDKotaKab(.IDKotaKabupaten.GetValueOrDefault)
if OKotaKab isnot nothing then LBSearchNamaKotaKabOld.text = safe(OKotaKab.NamaKotaKab)

txtIDKecamatanOld.Text = Safe(.IDKecamatan)
txtNamaKecamatanOld.Text = Safe(.NamaKecamatan)

                
                'other info
                Dim omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If omsuser.Count > 0 Then
                    lblCreatedByOld.Text = omsuser(0).UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateOld.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If omsuser.Count > 0 Then
                    lblUpdatedbyOld.Text = omsuser(0).UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
                lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
                Dim l_objMappingMsKecamatanNCBSPPATK As TList(Of MappingMsKecamatanNCBSPPATK)
                'txtNamaKecamatanOld.Text = Safe(.NamaKecamatan)
                l_objMappingMsKecamatanNCBSPPATK = DataRepository.MappingMsKecamatanNCBSPPATKProvider.GetPaged(MappingMsKecamatanNCBSPPATKColumn.IDKecamatan.ToString & _
                   "=" & _
                   PkObject, "", 0, Integer.MaxValue, Nothing)
               
                ListmapingOld.AddRange(l_objMappingMsKecamatanNCBSPPATK)
            End With
        End Using
    End Sub






    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew '(0).PK_MappingMsBentukBidangUsahaNCBSPPATK_Id
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

    Public ReadOnly Property parID As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping New sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingNew As TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail)
        Get
            Return Session("ListmapingNew.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail))
            Session("ListmapingNew.data") = value
        End Set
    End Property

    ''' <summary>
    ''' Menyimpan Item untuk mapping Old sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingOld As TList(Of MappingMsKecamatanNCBSPPATK)
        Get
            Return Session("ListmapingOld.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKecamatanNCBSPPATK))
            Session("ListmapingOld.data") = value
        End Set
    End Property

    ''' <summary>
    ''' List yang tampil di ListBox New
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKecamatanNCBSPPATK_Approval_Detail In ListmapingNew.FindAllDistinct("IDKecamatanNCBS")
                Temp.Add(i.IDKecamatanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox Old
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKecamatanNCBSPPATK In ListmapingOld.FindAllDistinct("IDKecamatanNCBS")
                Temp.Add(i.IDKecamatanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property
#End Region



End Class



