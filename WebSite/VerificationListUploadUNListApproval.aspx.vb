Imports System.Data.SqlClient
Imports SahassaNettier.Data
Imports SahassaNettier.Entities

Partial Class VerificationListUploadUNListApproval
    Inherits Parent

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")

                Using ObjUNList_PendingApproval As TList(Of UNList_PendingApproval) = DataRepository.UNList_PendingApprovalProvider.GetPaged("", "", 0, Integer.MaxValue, 0)
                    If ObjUNList_PendingApproval.Count > 0 Then
                        Me.TableUploadData.Visible = True
                        Me.TableNoPendingApproval.Visible = False
                        Me.ImageAccept.Visible = True
                        Me.ImageReject.Visible = True
                        Me.ImageBack.Visible = True

                        Using ObjUNList As TList(Of UNList) = DataRepository.UNListProvider.GetPaged("", "RefId", 0, Integer.MaxValue, 0)
                            Me.GridPendingApprovalUNList.DataSource = ObjUNList
                            Me.GridPendingApprovalUNList.DataBind()
                        End Using

                        Dim StrQuery As String = ""

                        'Alias
                        StrQuery = "SELECT u.RefId, ua.[Name] FROM UNListAlias ua INNER JOIN UNList u ON u.Pk_UNList_Id = ua.Fk_UNList_Id ORDER BY u.RefId"
                        Using DtSet As Data.DataSet = DataRepository.Provider.ExecuteDataSet(Data.CommandType.Text, StrQuery)
                            If DtSet.Tables.Count > 0 Then
                                Using DtTable As Data.DataTable = DtSet.Tables(0)
                                    Me.GridPendingApprovalUNAlias.DataSource = DtTable
                                    Me.GridPendingApprovalUNAlias.DataBind()
                                End Using
                            End If
                        End Using

                        'Address
                        StrQuery = "SELECT u.RefId, ua.[Address], ISNULL(ua.PostalCode, '') AS PostalCode, at.AddressTypeDescription, CASE WHEN ua.IsLocalAddress = 0 THEN 'No' ELSE 'Yes' END AS LocalAddress FROM UNListAddress ua INNER JOIN UNList u ON ua.Fk_UNList_Id = u.Pk_UNList_Id INNER JOIN AddressType at ON ua.AddressTypeId = at.AddressTypeId ORDER BY u.RefId"
                        Using DtSet As Data.DataSet = DataRepository.Provider.ExecuteDataSet(Data.CommandType.Text, StrQuery)
                            If DtSet.Tables.Count > 0 Then
                                Using DtTable As Data.DataTable = DtSet.Tables(0)
                                    Me.GridPendingApprovalUNAddress.DataSource = DtTable
                                    Me.GridPendingApprovalUNAddress.DataBind()
                                End Using
                            End If
                        End Using

                        'ID Number
                        StrQuery = "SELECT u.RefId, un.IDNumber FROM UNListNumber un INNER JOIN UNList u ON un.Fk_UNList_Id = u.Pk_UNList_Id ORDER BY u.RefId"
                        Using DtSet As Data.DataSet = DataRepository.Provider.ExecuteDataSet(Data.CommandType.Text, StrQuery)
                            If DtSet.Tables.Count > 0 Then
                                Using DtTable As Data.DataTable = DtSet.Tables(0)
                                    Me.GridPendingApprovalUNIDNumber.DataSource = DtTable
                                    Me.GridPendingApprovalUNIDNumber.DataBind()
                                End Using
                            End If
                        End Using
                    Else
                        Me.TableUploadData.Visible = False
                        Me.TableNoPendingApproval.Visible = True
                        Me.ImageAccept.Visible = False
                        Me.ImageReject.Visible = False
                        Me.ImageBack.Visible = True
                    End If
                End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Private Function ExecNonQuery(ByVal SQLQuery As String) As Integer
        Dim SqlConn As SqlConnection = Nothing
        Dim SqlCmd As SqlCommand = Nothing
        Try
            SqlConn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("AMLConnectionString").ToString)
            SqlConn.Open()
            SqlCmd = New SqlCommand
            SqlCmd.Connection = SqlConn
            SqlCmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings.Get("SQLCommandTimeout")
            SqlCmd.CommandText = SQLQuery
            Return SqlCmd.ExecuteNonQuery()
        Catch tex As Threading.ThreadAbortException
            Throw tex
        Catch ex As Exception
            Throw ex
        Finally
            If Not SqlConn Is Nothing Then
                SqlConn.Close()
                SqlConn.Dispose()
                SqlConn = Nothing
            End If
            If Not SqlCmd Is Nothing Then
                SqlCmd.Dispose()
                SqlCmd = Nothing
            End If
        End Try
    End Function

    Private Sub InsertAuditTrail(ByVal ApprovalStatusDescription As String, userpreparer As String)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(1)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, userpreparer, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "ALL", "Update", "", "", "Upload UN List Approval - " & ApprovalStatusDescription)
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Dim strpreparer As String = ""
            Using ObjUNListBLL As New AMLBLL.UNListBLL
                Using ObjUNList_PendingApproval As TList(Of UNList_PendingApproval) = DataRepository.UNList_PendingApprovalProvider.GetPaged("", "", 0, Integer.MaxValue, 0)
                    If ObjUNList_PendingApproval.Count > 0 Then
                        With ObjUNList_PendingApproval(0)
                            strpreparer = .UNList_PendingApprovalUserID
                        End With
                    End If
                End Using

                InsertAuditTrail("Accepted", strpreparer)
            ObjUNListBLL.AcceptApproval()

            ' insert to audit trail

            End Using

            Dim MessagePendingID As Integer = 9003 'MessagePendingID 9003 = UN List Approval approved saved successfully
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID, False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try

            Dim strpreparer As String = ""
            Using ObjUNListBLL As New AMLBLL.UNListBLL

                Using ObjUNList_PendingApproval As TList(Of UNList_PendingApproval) = DataRepository.UNList_PendingApprovalProvider.GetPaged("", "", 0, Integer.MaxValue, 0)
                    If ObjUNList_PendingApproval.Count > 0 Then
                        With ObjUNList_PendingApproval(0)
                            strpreparer = .UNList_PendingApprovalUserID
                        End With
                    End If
                End Using
                InsertAuditTrail("Rejected", strpreparer)

                ObjUNListBLL.RejectApproval()

                ' insert to audit trail

            End Using

            Dim MessagePendingID As Integer = 9004 'MessagePendingID 9004 = UN List Approval rejected saved successfully
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID, False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Me.Response.Redirect("Default.aspx", False)
    End Sub
End Class
