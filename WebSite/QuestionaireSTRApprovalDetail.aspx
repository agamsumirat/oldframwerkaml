<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="QuestionaireSTRApprovalDetail.aspx.vb" Inherits="QuestionaireSTRApprovalDetail"
     %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table cellspacing="4" cellpadding="0" width="100%">
        <tr>
            <td valign="bottom" align="left" style="height: 16px">
                <img height="15" src="images/dot_title.gif" width="15"></td>
            <td class="maintitle" valign="bottom" width="99%" style="height: 16px">
                <asp:Label ID="LabelTitle" runat="server"></asp:Label></td>
        </tr>
    </table>
    <table style="width: 100%">
        <tr id="ViewEditNew" runat="server">
            <td>
            </td>
            <td>
                <asp:Panel ID="PanelEditNew" runat="server" Height="100%" Width="100%" GroupingText="NEW VALUE">
                    <table style="width: 100%">
                        <tr>
                            <td nowrap="noWrap" width="1%" style="height: 15px">
                                Mode</td>
                            <td width="1" style="height: 15px">
                                :</td>
                            <td style="height: 15px">
                                <asp:Label ID="LblModeEditNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="noWrap" width="1%" style="height: 15px">
                                Description Alert STR</td>
                            <td style="height: 15px">
                                :</td>
                            <td style="height: 15px">
                                <asp:Label ID="LblDescriptionAlertSTREditNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="noWrap" width="1%" style="height: 15px">
                                QuestionType</td>
                            <td style="height: 15px">
                                :</td>
                            <td style="height: 15px">
                                <asp:Label ID="LblQuestionTypeEditNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="noWrap" width="1%">
                                QuestionNo</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LblQuestionNoEditNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                Question</td>
                            <td>
                                :</td>
                            <td>
                                <asp:TextBox ID="TxtQuestionEditNew" runat="server" Height="112px" ReadOnly="True"
                                    TextMode="MultiLine" Width="481px"></asp:TextBox></td>
                        </tr>
                        <tr id="ViewNew" runat="server">
                            <td nowrap="nowrap" width="1%" style="height: 15px">
                                MultipleChoice</td>
                            <td style="height: 15px">
                                :</td>
                            <td style="height: 15px">
                                <asp:DataGrid ID="gridViewEditNew" runat="server" AllowPaging="True" AllowSorting="True"
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None"
                                    BorderWidth="1px" CellPadding="4" Font-Size="XX-Small" ForeColor="Black" GridLines="Vertical"
                                    Width="100%">
                                    <FooterStyle BackColor="#CCCC99" />
                                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                    <AlternatingItemStyle BackColor="White" />
                                    <ItemStyle BackColor="#F7F7DE" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="No." Visible="False"></asp:TemplateColumn>
                                        <asp:BoundColumn DataField="MultipleChoice" HeaderText="Choice" SortExpression="MultipleChoice Desc">
                                        </asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="XX-Small" ForeColor="White" />
                                    <PagerStyle Mode="NumericPages" />
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%" style="height: 15px">
                                CreatedBy / date</td>
                            <td style="height: 15px">
                                :</td>
                            <td style="height: 15px">
                                <asp:Label ID="LblCreatedEditNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                LastUpdateBy / date</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LblLastUpDateEditNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                ApprovedBy / date</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LblApprovedByEditNew" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td id="ViewEditOld" runat="server">
                <asp:Panel ID="PanelEditOld" runat="server" Height="100%" Width="100%" GroupingText="OLD VALUE">
                    <table style="width: 100%">
                        <tr>
                            <td nowrap="noWrap" width="1%">
                                Mode</td>
                            <td width="1">
                                :</td>
                            <td>
                                <asp:Label ID="LblModeEditOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="noWrap" width="1%">
                                Description Alert STR</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LblDescriptionAlertSTREditOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="noWrap" width="1%">
                                QuestionType</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LblQuestionTypeEditOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="noWrap" width="1%" style="height: 15px">
                                QuestionNo</td>
                            <td style="height: 15px">
                                :</td>
                            <td style="height: 15px">
                                <asp:Label ID="LblQuestionNoEditOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                Question</td>
                            <td>
                                :</td>
                            <td>
                                <asp:TextBox ID="TxtQuestionEditOld" runat="server" Height="112px" ReadOnly="True"
                                    TextMode="MultiLine" Width="481px"></asp:TextBox></td>
                        </tr>
                        <tr id="ViewOld" runat="server">
                            <td nowrap="nowrap" width="1%">
                                MultipleChoice</td>
                            <td>
                                :</td>
                            <td>
                                <asp:DataGrid ID="gridViewEditOld" runat="server" AllowPaging="True" AllowSorting="True"
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None"
                                    BorderWidth="1px" CellPadding="4" Font-Size="XX-Small" ForeColor="Black" GridLines="Vertical"
                                    Width="100%">
                                    <FooterStyle BackColor="#CCCC99" />
                                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                    <AlternatingItemStyle BackColor="White" />
                                    <ItemStyle BackColor="#F7F7DE" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="No." Visible="False"></asp:TemplateColumn>
                                        <asp:BoundColumn DataField="MultipleChoice" HeaderText="Choice" SortExpression="MultipleChoice Desc">
                                        </asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="XX-Small" ForeColor="White" />
                                    <PagerStyle Mode="NumericPages" />
                                </asp:DataGrid></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                CreatedBy / date</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LblCreatedEditOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                LastUpdateBy / date</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LblLastUpDateEditOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%" style="height: 15px">
                                ApprovedBy / date</td>
                            <td style="height: 15px">
                                :</td>
                            <td style="height: 15px">
                                <asp:Label ID="LblApprovedByEditOld" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr class="formText" id="Button11" bgcolor="#dddddd" height="30">
            <td style="width: 22px; height: 30px;">
                <img height="15" src="images/arrow.gif" width="15"></td>
            <td colspan="7" style="height: 30px">
                <table cellspacing="0" cellpadding="3" border="0">
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImageAccept" runat="server" CausesValidation="True" SkinID="AcceptButton">
                            </asp:ImageButton></td>
                        <td>
                            <asp:ImageButton ID="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton">
                            </asp:ImageButton></td>
                        <td>
                            <asp:ImageButton ID="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton">
                            </asp:ImageButton></td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
        </tr>
    </table>
</asp:Content>
