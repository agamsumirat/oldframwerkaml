﻿Option Explicit On
Option Strict On

Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports AMLBLL

Partial Class TransactionAmountApprovalDetail
    Inherits Parent

    Public ReadOnly Property PKTransactionAmountApprovalID() As Integer
        Get
            If Session("TransactionAmountApprovalDetail.PK") Is Nothing Then
                Dim StrTmppkTransactionAmountApprovalId As String

                StrTmppkTransactionAmountApprovalId = Request.Params("PKTransactionAmountApprovalID")
                If Integer.TryParse(StrTmppkTransactionAmountApprovalId, 0) Then
                    Session("TransactionAmountApprovalDetail.PK") = StrTmppkTransactionAmountApprovalId
                Else
                    Throw New SahassaException("Parameter PKTransactionAmountApprovalID is not valid.")
                End If

                Return CInt(Session("TransactionAmountApprovalDetail.PK"))
            Else
                Return CInt(Session("TransactionAmountApprovalDetail.PK"))
            End If
        End Get
    End Property

    Public ReadOnly Property ObjTransactionAmountApproval() As TList(Of TransactionAmount_Approval)
        Get
            Dim PkApprovalid As Integer
            Try
                If Session("TransactionAmountApprovalDetail.ObjTransactionAmountApproval") Is Nothing Then
                    PkApprovalid = PKTransactionAmountApprovalID
                    Session("TransactionAmountApprovalDetail.ObjTransactionAmountApproval") = DataRepository.TransactionAmount_ApprovalProvider.GetPaged("PK_TransactionAmount_ApprovalID = " & PkApprovalid, "", 0, Integer.MaxValue, 0)
                End If
                Return CType(Session("TransactionAmountApprovalDetail.ObjTransactionAmountApproval"), TList(Of TransactionAmount_Approval))
            Finally
            End Try
        End Get
    End Property

    Private Sub LoadDataApproval()
        If ObjTransactionAmountApproval.Count > 0 Then
            If ObjTransactionAmountApproval(0).FK_ModeID.GetValueOrDefault(0) = 2 Then
                Me.LblAction.Text = "Edit"
            End If

            Dim strRequestBy As String = ""
            Using objRequestBy As User = SahassaNettier.Data.DataRepository.UserProvider.GetByPkUserID(ObjTransactionAmountApproval(0).FK_MsUserID.GetValueOrDefault(0))
                If Not objRequestBy Is Nothing Then
                    strRequestBy = objRequestBy.UserID & " ( " & objRequestBy.UserName & " ) "
                End If
            End Using

            LblRequestBy.Text = strRequestBy
            LblRequestDate.Text = ObjTransactionAmountApproval(0).CreatedDate.GetValueOrDefault().ToString("dd-MM-yyyy")

            If ObjTransactionAmountApproval(0).FK_ModeID.GetValueOrDefault(0) = 2 Then
                'Old
                
                Me.GrdVwTransactionAmountOld.DataSource = DataRepository.TransactionAmountProvider.GetPaged("", "Tier", 0, Integer.MaxValue, 0)
                Me.GrdVwTransactionAmountOld.DataBind()

                'New
                Me.GrdVwTransactionAmountNew.DataSource = DataRepository.TransactionAmount_ApprovalDetailProvider.GetPaged("", "Tier", 0, Integer.MaxValue, 0)
                Me.GrdVwTransactionAmountNew.DataBind()
            End If
        Else
            'kalau ternyata sudah di approve /reject dan memakai tombol back, maka redirect ke viewapproval
            Response.Redirect("TransactionAmountApproval.aspx", False)
        End If
    End Sub

    Private Sub ClearSession()
        Session("TransactionAmountApprovalDetail.PK") = Nothing
        Session("TransactionAmountApprovalDetail.ObjTransactionAmountApproval") = Nothing
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                LoadDataApproval()
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAccept.Click
        Try
            If ObjTransactionAmountApproval.Count > 0 Then
                Using ObjTransactionAmountBll As New TransactionAmountBLL
                    If ObjTransactionAmountBll.AcceptEdit Then
                        Response.Redirect("TransactionAmountApproval.aspx", False)
                    End If
                End Using
            Else
                Response.Redirect("TransactionAmountApproval.aspx", False)
            End If

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnReject.Click
        Try
            If ObjTransactionAmountApproval.Count > 0 Then
                Using ObjTransactionAmountBll As New TransactionAmountBLL
                    If ObjTransactionAmountBll.RejectEdit Then
                        Response.Redirect("TransactionAmountApproval.aspx", False)
                    End If
                End Using
            Else
                Response.Redirect("TransactionAmountApproval.aspx", False)
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("TransactionAmountApproval.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
    '    If CvalHandleErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    'Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalPageErr.PreRender
    '    If CvalPageErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub
End Class