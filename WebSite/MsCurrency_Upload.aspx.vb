#Region "Imports..."
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Excel
Imports Sahassa.AML.Commonly
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports Sahassanettier.Data.SqlClient
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports AMLBLL.DataType
#End Region

Partial Class MsCurrency_upload
    Inherits Parent

#Region "Structure..."

    Structure GrouP_MsCurrency_success
        Dim MsCurrency As MsCurrency_ApprovalDetail
        Dim L_MsCurrencyNCBS As TList(Of MsCurrencyNCBS_ApprovalDetail)

    End Structure

#End Region

#Region "Property umum"
    Private Property GroupTableSuccessData() As List(Of GrouP_MsCurrency_success)
        Get
            Return Session("GroupTableSuccessData")
        End Get
        Set(ByVal value As List(Of GrouP_MsCurrency_success))
            Session("GroupTableSuccessData") = value
        End Set
    End Property

    Private Property GroupTableAnomalyData() As List(Of AMLBLL.MsCurrencyBll.DT_Group_MsCurrency)
        Get
            Return Session("GroupTableAnomalyData")
        End Get
        Set(ByVal value As List(Of AMLBLL.MsCurrencyBll.DT_Group_MsCurrency))
            Session("GroupTableAnomalyData") = value
        End Set
    End Property

    Public ReadOnly Property GET_PKUserID() As Integer
        Get
            Return SessionPkUserId
        End Get
    End Property

    Public ReadOnly Property GET_UserName() As String
        Get
            Return SessionUserId
            'Return SessionNIK
        End Get
    End Property

    Private Property SetnGetDataTableForSuccessData() As TList(Of MsCurrency_ApprovalDetail)
        Get
            Return CType(IIf(Session("MsCurrency_upload.TableSuccessUpload") Is Nothing, Nothing, Session("MsCurrency_upload.TableSuccessUpload")), TList(Of MsCurrency_ApprovalDetail))
        End Get
        Set(ByVal value As TList(Of MsCurrency_ApprovalDetail))
            Session("MsCurrency_upload.TableSuccessUpload") = value
        End Set
    End Property

    Private Property SetnGetDataTableForAnomalyData() As Data.DataTable
        Get
            Return CType(IIf(Session("MsCurrency_upload.TableAnomalyUpload") Is Nothing, Nothing, Session("MsCurrency_upload.TableAnomalyUpload")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsCurrency_upload.TableAnomalyUpload") = value
        End Set
    End Property

    Private Property DSexcelTemp() As Data.DataSet
        Get
            Return CType(IIf(Session("MsCurrency_upload.DSexcelTemp") Is Nothing, Nothing, Session("MsCurrency_upload.DSexcelTemp")), DataSet)
        End Get
        Set(ByVal value As Data.DataSet)
            Session("MsCurrency_upload.DSexcelTemp") = value
        End Set
    End Property

#End Region

#Region "Property Tabel Temporary"
    Private Property DT_AllMsCurrency() As Data.DataTable
        Get
            Return CType(IIf(Session("MsCurrency_upload.DT_MsCurrency") Is Nothing, Nothing, Session("MsCurrency_upload.DT_MsCurrency")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsCurrency_upload.DT_MsCurrency") = value
        End Set
    End Property

    Private Property DT_AllMsCurrencyNCBS() As Data.DataTable
        Get
            Return CType(IIf(Session("MsCurrency_upload.DT_MsCurrencyNCBS") Is Nothing, Nothing, Session("MsCurrency_upload.DT_MsCurrencyNCBS")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsCurrency_upload.DT_MsCurrencyNCBS") = value
        End Set
    End Property

#End Region

#Region "Property Table Sukses Data"

    Private Property TableSuccessMsCurrency() As TList(Of MsCurrency_ApprovalDetail)
        Get
            Return Session("TableSuccessMsCurrency")
        End Get
        Set(ByVal value As TList(Of MsCurrency_ApprovalDetail))
            Session("TableSuccessMsCurrency") = value
        End Set
    End Property

    Private Property TableSuccessMsCurrencyNCBS() As TList(Of MsCurrencyNCBS)
        Get
            Return Session("TableSuccessMsCurrencyNCBS")
        End Get
        Set(ByVal value As TList(Of MsCurrencyNCBS))
            Session("TableSuccessMsCurrencyNCBS") = value
        End Set
    End Property


#End Region

#Region "Property Table Anomaly Data"

    Private Property TableAnomalyMsCurrency() As Data.DataTable
        Get
            Return Session("TableAnomalyMsCurrency")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsCurrency") = value
        End Set
    End Property

    Private Property TableAnomalyMsCurrencyNCBS() As Data.DataTable
        Get
            Return Session("TableAnomalyMsCurrencyNCBS")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsCurrencyNCBS") = value
        End Set
    End Property

#End Region

#Region "validasi.."

    Private Function ValidateInput(ByVal MsCurrency As AMLBLL.MsCurrencyBll.DT_Group_MsCurrency) As List(Of String)
        Dim temp As New List(Of String)
        Dim msgError As New StringBuilder
        Dim errorline As New StringBuilder
        Try
            'validasi
            ValidateMsCurrency(MsCurrency.MsCurrency, msgError, errorline)
            If msgError.ToString = "" Then
                For Each ci As DataRow In MsCurrency.L_MsCurrencyNCBS.Rows
                    'validasi NCBS
                    ValidateMsCurrencyNCBS(ci, msgError, errorline)
                Next
            End If
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        Catch ex As Exception
            msgError.Append(ex.Message)
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        End Try
        Return temp
    End Function

    Private Shared Sub ValidateMsCurrencyNCBS(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorLine As StringBuilder)
        Try

            '======================== Validasi textbox :  IdCurrencyNCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("IdCurrencyBank")) = False Then
                msgError.AppendLine(" &bull; IdCurrencyBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsCurrencyBank : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  Description canot Null (#)====================================================
            If ObjectAntiNull(DTR("CurrencyCodeBank")) = False Then
                msgError.AppendLine(" &bull; CurrencyCodeBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsCurrencyBank : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  Description canot Null (#)====================================================
            If ObjectAntiNull(DTR("CurrencyNameBank")) = False Then
                msgError.AppendLine(" &bull; CurrencyNameBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsCurrencyBank : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorLine.AppendLine(" &bull;MsCurrencyBank : Line " & DTR("NO") & "<BR/>")
        End Try
    End Sub

    Private Shared Sub ValidateMsCurrency(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorline As StringBuilder)
        Try

            Using checkBlocking As TList(Of MsCurrency_ApprovalDetail) = DataRepository.MsCurrency_ApprovalDetailProvider.GetPaged("IdCurrency=" & DTR("IdCurrency"), "", 0, 1, Nothing)
                If checkBlocking.Count > 0 Then
                    msgError.AppendLine(" &bull;MsCurrency is waiting for approval <BR/>")
                    errorline.AppendLine(" &bull;MsCurrency : Line " & DTR("NO") & "<BR/>")
                End If
            End Using

            '======================== Validasi textbox :  IdCurrency   canot Null (#)====================================================
            If ObjectAntiNull(DTR("IdCurrency")) = False Then
                msgError.AppendLine(" &bull; ID must be filled <BR/>")
                errorline.AppendLine(" &bull; MsCurrency : Line " & DTR("NO") & "<BR/>")
            End If
            If ObjectAntiNull(DTR("IdCurrency")) Then
                If objectNumOnly(DTR("IdCurrency")) = False Then
                    msgError.AppendLine(" &bull; ID must be Numeric <BR/>")
                    errorline.AppendLine(" &bull; MsCurrency : Line " & DTR("NO") & "<BR/>")
                End If
            End If
            '======================== Validasi textbox :  Code   canot Null (#)====================================================
            If ObjectAntiNull(DTR("CurrencyCode")) = False Then
                msgError.AppendLine(" &bull; CurrencyCode must be filled <BR/>")
                errorline.AppendLine(" &bull; MsCurrency : Line " & DTR("NO") & "<BR/>")
            End If
            '======================== Validasi textbox :  Name   canot Null (#)====================================================
            If ObjectAntiNull(DTR("CurrencyName")) = False Then
                msgError.AppendLine(" &bull; CurrencyName must be filled <BR/>")
                errorline.AppendLine(" &bull; MsCurrency : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorline.AppendLine(" &bull;MsCurrency  : Line " & DTR("NO") & "<BR/>")
            LogError(ex)
        End Try
    End Sub

#End Region

#Region "Upload File"

    Public Function SaveFileImportToSave() As String
        Dim filePath As String
        Dim FileNameToGetData As String = ""
        Try
            If FileUploadKPI.PostedFile.ContentLength = 0 Then
                Return FileNameToGetData
            End If

            If Not FileUploadKPI.HasFile Then
                Return FileNameToGetData
            End If

            filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName)

            FileNameToGetData = FileUploadKPI.FileName
            If System.IO.File.Exists(filePath) Then
                Dim bexist As Boolean = True
                Dim i As Integer = 1
                While bexist
                    filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & FileUploadKPI.FileName.Split(CChar("."))(1))
                    FileNameToGetData = FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & Me.FileUploadKPI.FileName.Split(CChar("."))(1)
                    If Not System.IO.File.Exists(filePath) Then
                        bexist = False
                        Exit While
                    End If
                    i = i + 1
                End While
            End If
            Me.FileUploadKPI.PostedFile.SaveAs(filePath)
            Session("Filepath") = filePath
            Return FileNameToGetData
        Catch
            Throw
        End Try
    End Function

    ReadOnly Property FilePath() As String
        Get
            Return Session("FilePath")
        End Get
    End Property

    Private Sub ProcessExcelFile(ByVal FullPathFileNameToExportExcel As String)

        Using stream As FileStream = File.Open(FullPathFileNameToExportExcel, FileMode.Open, FileAccess.Read)
            Using excelReader As IExcelDataReader = ExcelReaderFactory.CreateBinaryReader(stream)
                excelReader.IsFirstRowAsColumnNames = True
                DSexcelTemp = excelReader.AsDataSet()
                If DSexcelTemp.Tables("MsCurrency").Columns(0).ColumnName Like "*Column*" Then
                    DSexcelTemp.Tables("MsCurrency").Rows.Clear()
                End If

            End Using
        End Using

    End Sub


#End Region

#Region "Function..."

    Function getErrorNo(ByVal Id As String, ByVal nama As String) As String
        Dim temp As String = ""
        Try
            Dim DV As New DataView(DT_AllMsCurrencyNCBS)
            'jika Null maka harus is null bukan kosong disini belum di handle
            DV.RowFilter = "IdCurrency = '" & Id & "' and CurrencyName = '" & nama & "'"

            Dim DT As Data.DataTable = DV.ToTable
            If DT.Rows.Count > 0 Then
                temp = Safe(DT.Rows(0)("NO"))
            End If
        Catch ex As Exception
            temp = ex.Message
        End Try

        Return temp
    End Function
    ''' <summary>
    ''' Merubah excel ke Object Databale sekaligus memecahnya
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ImportExcelTempToDTproperty()
        Dim DT As New Data.DataTable
        'DT_MsCurrency = DSexcelTemp.Tables("MsCurrency")
        DT = DSexcelTemp.Tables("MsCurrency")
        Dim DV As New DataView(DT)
        DT_AllMsCurrency = DV.ToTable(True, "IdCurrency", "CurrencyCode", "CurrencyName", "activation")
        DT_AllMsCurrencyNCBS = DSexcelTemp.Tables("MsCurrency")
        DT_AllMsCurrency.Columns.Add("NO")
        For Each i As DataRow In DT_AllMsCurrency.Rows
            Dim PK As String = Safe(i("IdCurrency"))
            Dim nama As String = Safe(i("CurrencyName"))
            i("NO") = getErrorNo(PK, nama)
        Next
    End Sub

    Private Sub ClearThisPageSessions()
        DT_AllMsCurrency = Nothing
        DT_AllMsCurrencyNCBS = Nothing
        GroupTableSuccessData = Nothing
        GroupTableAnomalyData = Nothing
        DSexcelTemp = Nothing
        SetnGetDataTableForAnomalyData = Nothing
        SetnGetDataTableForSuccessData = Nothing
    End Sub


    Function ConvertDTtoMsCurrencyNCBS(ByVal DT As Data.DataTable) As TList(Of MsCurrencyNCBS_ApprovalDetail)
        Dim temp As New TList(Of MsCurrencyNCBS_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsCurrencyNCBS As New MsCurrencyNCBS_ApprovalDetail
                With MsCurrencyNCBS
                    FillOrNothing(.IdCurrencyNCBS, DTR("IdCurrencyBank"), True, oInt)
                    FillOrNothing(.Description, DTR("CurrencyNameBank"), True, oString)
                    FillOrNothing(.Activation, 1, True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    .Code = DTR("CurrencyCodeBank")
                    temp.Add(MsCurrencyNCBS)
                End With
            End Using

        Next

        Return temp
    End Function



    Function ConvertDTtoMsCurrency(ByVal DTR As DataRow) As MsCurrency_ApprovalDetail
        Dim temp As New MsCurrency_ApprovalDetail
        Using MsCurrency As New MsCurrency_ApprovalDetail()
            With MsCurrency
                FillOrNothing(.IdCurrency, DTR("IdCurrency"), True, oInt)
                FillOrNothing(.Code, DTR("CurrencyCode"), True, Ovarchar)
                FillOrNothing(.Name, DTR("CurrencyName"), True, Ovarchar)

                FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                FillOrNothing(.CreatedDate, Now, True, oDate)
            End With
            temp = MsCurrency
        End Using

        Return temp
    End Function

    Function ConvertDTtoMsCurrency(ByVal DT As Data.DataTable) As TList(Of MsCurrency_ApprovalDetail)
        Dim temp As New TList(Of MsCurrency_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsCurrency As New MsCurrency_ApprovalDetail()
                With MsCurrency
                    FillOrNothing(.IdCurrency, DTR("IdCurrency"), True, oInt)
                    FillOrNothing(.Code, DTR("CurrencyCode"), True, Ovarchar)
                    FillOrNothing(.Name, DTR("CurrencyName"), True, Ovarchar)

                    FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsCurrency)
                End With
            End Using
        Next
        Return temp
    End Function

    Private Sub ChangeMultiView(ByVal index As Integer)
        MultiViewKPIOvertimeUpload.ActiveViewIndex = index
        If index = 0 Then
            Imgbtnsave.Visible = False
            Imgbtncancel.Visible = False
        ElseIf index = 1 Then
            Imgbtncancel.Visible = True
        ElseIf index = 2 Then
            Imgbtncancel.Visible = False
            Imgbtnsave.Visible = False
        End If
    End Sub

    Private Function GenerateGroupingMsCurrency() As List(Of AMLBLL.MsCurrencyBll.DT_Group_MsCurrency)
        Dim Ltemp As New List(Of AMLBLL.MsCurrencyBll.DT_Group_MsCurrency)
        For i As Integer = 0 To DT_AllMsCurrency.Rows.Count - 1
            Dim MsCurrency As New AMLBLL.MsCurrencyBll.DT_Group_MsCurrency
            With MsCurrency
                .MsCurrency = DT_AllMsCurrency.Rows(i)
                .L_MsCurrencyNCBS = DT_AllMsCurrencyNCBS.Clone

                'Insert MsCurrencyNCBS
                Dim MsCurrency_ID As String = Safe(.MsCurrency("IdCurrency"))
                Dim MsCurrency_nama As String = Safe(.MsCurrency("CurrencyName"))
                Dim MsCurrency_Activation As String = Safe(.MsCurrency("Activation"))
                If DT_AllMsCurrencyNCBS.Rows.Count > 0 Then
                    Using DV As New DataView(DT_AllMsCurrencyNCBS)
                        DV.RowFilter = "IdCurrency='" & MsCurrency_ID & "' and " & _
                                                  "CurrencyName='" & MsCurrency_nama & "' and " & _
                                                  "activation='" & MsCurrency_Activation & "'"
                        If DV.Count > 0 Then
                            Dim temp As Data.DataTable = DV.ToTable
                            For Each H As DataRow In temp.Rows
                                .L_MsCurrencyNCBS.ImportRow(H)
                            Next
                        End If
                    End Using
                End If




            End With
            Ltemp.Add(MsCurrency)
        Next
        Return Ltemp
    End Function

    Public Sub BindGrid()
        lblValueFailedData.Text = SetnGetDataTableForAnomalyData.Rows.Count
        GridAnomalyList.DataSource = SetnGetDataTableForAnomalyData
        GridAnomalyList.DataBind()
        lblValueSuccessData.Text = SetnGetDataTableForSuccessData.Count
        GridSuccessList.DataSource = SetnGetDataTableForSuccessData
        lblValueuploadeddata.Text = DT_AllMsCurrency.Rows.Count
        GridSuccessList.DataBind()
    End Sub

#End Region

#Region "events..."


    Protected Sub ImgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtncancel.Click
        ClearThisPageSessions()
        ChangeMultiView(0)
        LinkButtonExportExcel.Visible = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If IsPostBack Then
                Return
            End If

            ClearThisPageSessions()
            SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            ChangeMultiView(0)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        ChangeMultiView(0)
    End Sub

    Protected Sub GridAnomalyList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridAnomalyList.PageIndexChanged
        GridAnomalyList.CurrentPageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GridSuccessList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridSuccessList.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                'Show mapping item
                Dim StringMaping As New StringBuilder
                Dim DV As New DataView(DT_AllMsCurrencyNCBS)
                Dim DT As New Data.DataTable
                DV.RowFilter = "IdCurrency='" & e.Item.Cells(0).Text & "' and " & "CurrencyName='" & e.Item.Cells(2).Text & "'"
                DT = DV.ToTable
                If DT.Rows.Count > 0 Then
                    Dim countField As Integer = 7
                    Dim lengthField As Integer = 0
                    For Each idx As DataRow In DT.Rows
                        With idx
                            If lengthField > countField Then
                                StringMaping.AppendLine(idx("CurrencyNameBank") & "(" & idx("IdCurrencyBank") & ");")
                                lengthField = 0
                            Else
                                StringMaping.AppendLine(idx("CurrencyNameBank") & "(" & idx("IdCurrencyBank") & ");")
                                lengthField += 1
                            End If

                        End With
                    Next
                Else
                    StringMaping.Append("-")
                End If
                e.Item.Cells(CollCount).Text = StringMaping.ToString

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridSuccessList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridSuccessList.PageIndexChanged
        Dim GridTarget As DataGrid = CType(source, DataGrid)
        Try
            GridSuccessList.CurrentPageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgSaveSuccessData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtnsave.Click
        Try
            'File belum diupload
            If DT_AllMsCurrency Is Nothing Then
                Throw New Exception("Load data failed")
            End If

            'Save MsCurrency Approval (Header)
            Dim KeyApp As String
            Using Aproval As New MsCurrency_Approval()
                With Aproval
                    .RequestedBy = GET_PKUserID
                    .RequestedDate = Date.Now
                    .FK_MsMode_Id = 2 'Edit
                    .IsUpload = True
                End With
                DataRepository.MsCurrency_ApprovalProvider.Save(Aproval)
                'ambil key approval
                KeyApp = Aproval.PK_MsCurrencyPPATK_Approval_Id
            End Using

            For k As Integer = 0 To GroupTableSuccessData.Count - 1

                Dim MsCurrency As GrouP_MsCurrency_success = GroupTableSuccessData(k)


                With MsCurrency
                    'save MsCurrency approval detil
                    .MsCurrency.PK_MsCurrency_Approval_id = KeyApp
                    DataRepository.MsCurrency_ApprovalDetailProvider.Save(.MsCurrency)
                    'ambil key MsCurrency approval detil
                    Dim IdMsCurrency As String = .MsCurrency.IdCurrency
                    Dim L_MappingMsCurrencyNCBSPPATK_Approval_Detail As New TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail)
                    'save mapping dan NCBS
                    For Each OMsCurrencyNCBS_ApprovalDetail As MsCurrencyNCBS_ApprovalDetail In .L_MsCurrencyNCBS
                        OMsCurrencyNCBS_ApprovalDetail.PK_MsCurrency_Approval_Id = KeyApp
                        Using OMappingMsCurrencyNCBSPPATK_Approval_Detail As New MappingMsCurrencyNCBSPPATK_Approval_Detail
                            With OMappingMsCurrencyNCBSPPATK_Approval_Detail
                                FillOrNothing(.Pk_MsCurrencyPPATK_Id, OMsCurrencyNCBS_ApprovalDetail.IdCurrencyNCBS, True, oInt)
                                FillOrNothing(.Pk_MsCurrency_Id, IdMsCurrency, True, oInt)
                                FillOrNothing(.PK_MappingMsCurrencyNCBSPPATK_Approval_Id, KeyApp, True, oInt)
                                FillOrNothing(.Nama, OMsCurrencyNCBS_ApprovalDetail.Description, True, oString)
                                .CurCodeDanamon = OMsCurrencyNCBS_ApprovalDetail.Code
                            End With
                            L_MappingMsCurrencyNCBSPPATK_Approval_Detail.Add(OMappingMsCurrencyNCBSPPATK_Approval_Detail)
                        End Using
                    Next
                    'save maping dan NCBS
                    DataRepository.MappingMsCurrencyNCBSPPATK_Approval_DetailProvider.Save(L_MappingMsCurrencyNCBSPPATK_Approval_Detail)
                    DataRepository.MsCurrencyNCBS_ApprovalDetailProvider.Save(.L_MsCurrencyNCBS)

                End With
            Next

            LblConfirmation.Text = "MsCurrency data has edited (" & SetnGetDataTableForSuccessData.Count & " Rows / MsCurrency)"
            ChangeMultiView(2)

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgUpload.Click
        Dim CombinedFileName As String
        GroupTableSuccessData = New List(Of GrouP_MsCurrency_success)
        GroupTableAnomalyData = New List(Of AMLBLL.MsCurrencyBll.DT_Group_MsCurrency)
        Try
            'validasi File
            If FileUploadKPI.FileName = "" Then
                Throw New Exception("Data required")
            End If

            If Path.GetExtension(FileUploadKPI.FileName) <> ".xls" Then
                Throw New Exception("Invalid File Upload Extension")
            End If

            CombinedFileName = SaveFileImportToSave()
            If String.Compare(CombinedFileName, "", False) <> 0 Then
                Try
                    ProcessExcelFile(FilePath)
                Catch ex As Exception
                    Throw New Exception("Invalid File Format")
                End Try
            Else
                Throw New Exception("Invalid File Format")
            End If

            'Import from Excell to property Global
            ImportExcelTempToDTproperty()

            Dim LG_MsCurrency As List(Of AMLBLL.MsCurrencyBll.DT_Group_MsCurrency) = GenerateGroupingMsCurrency()
            'tambahkan kolom untuk tampilan di grid anomaly
            DT_AllMsCurrency.Columns.Add("Description")
            DT_AllMsCurrency.Columns.Add("errorline")

            'Membuat tempory untuk yang anomali dan yang sukses
            Dim DTnormal As Data.DataTable = DT_AllMsCurrency.Clone
            Dim DTAnomaly As Data.DataTable = DT_AllMsCurrency.Clone

            'proses pemilahan data anomaly dan data sukses melalui validasi 
            For i As Integer = 0 To LG_MsCurrency.Count - 1
                Dim Lerror As List(Of String) = ValidateInput(LG_MsCurrency(i))
                Dim MSG As String = Lerror(0)
                Dim LINE As String = Lerror(1)
                If MSG = "" Then
                    DTnormal.ImportRow(LG_MsCurrency(i).MsCurrency)
                    Dim GroupMsCurrency As New GrouP_MsCurrency_success
                    With GroupMsCurrency
                        'konversi dari datatable ke Object Nettier
                        .MsCurrency = ConvertDTtoMsCurrency(LG_MsCurrency(i).MsCurrency)
                        .L_MsCurrencyNCBS = ConvertDTtoMsCurrencyNCBS(LG_MsCurrency(i).L_MsCurrencyNCBS)
                    End With
                    'masukan ke group sukses
                    GroupTableSuccessData.Add(GroupMsCurrency)
                Else
                    DT_AllMsCurrency.Rows(i)("Description") = MSG
                    DT_AllMsCurrency.Rows(i)("errorline") = LINE
                    DTAnomaly.ImportRow(DT_AllMsCurrency.Rows(i))
                    GroupTableAnomalyData.Add(LG_MsCurrency(i))
                End If

            Next

            SetnGetDataTableForSuccessData = ConvertDTtoMsCurrency(DTnormal)
            SetnGetDataTableForAnomalyData = DTAnomaly

            If DTAnomaly.Rows.Count > 0 Then
                Imgbtnsave.Visible = False
                Imgbtncancel.Visible = True
            Else
                Imgbtnsave.Visible = True
                Imgbtncancel.Visible = True
            End If

            BindGrid()
            If GridAnomalyList.Items.Count < 1 Then
                LinkButtonExportExcel.Visible = False
            Else
                LinkButtonExportExcel.Visible = True
            End If
            ChangeMultiView(1)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Dim Exp As New AMLBLL.MsCurrencyBll
            Dim FolderPath As String = Server.MapPath("FolderTemplate")

            Dim buffer As Byte() = Exp.ExportDTtoExcel(GroupTableAnomalyData, FolderPath)
            Response.AddHeader("Content-type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment; filename=MsCurrencyUpload.xls")
            Response.BinaryWrite(buffer)
            Response.Flush()
            Response.End()

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered 

    End Sub

    Protected Sub GridSuccessList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridSuccessList.SelectedIndexChanged

    End Sub
End Class






