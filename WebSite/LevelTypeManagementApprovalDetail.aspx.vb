Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class LevelTypeManagementApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param pk user management
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamPkLevelTypeManagement() As Int64
        Get
            Return Me.Request.Params("LevelTypeID")
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserID() As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.LevelType_PendingApprovalTableAdapter
                Return AccessPending.SelectLevelType_PendingApprovalUserID(Me.ParamPkLevelTypeManagement)
            End Using
        End Get
    End Property
#End Region

#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HideAll()
        Dim StrId As String = ""
        Select Case Me.ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.LevelTypeAdd
                StrId = "UserAdd"
            Case Sahassa.AML.Commonly.TypeConfirm.LevelTypeEdit
                StrId = "UserEdi"
            Case Sahassa.AML.Commonly.TypeConfirm.LevelTypeDelete
                StrId = "UserDel"
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select
        For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
            ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID.Substring(0, 7) = StrId
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load user add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadLevelTypeAdd()
        Me.LabelTitle.Text = "Activity: Add Level Type"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.LevelType_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetLevelTypeApprovalData(Me.ParamPkLevelTypeManagement)
                'Bila ObjTable.Rows.Count > 0 berarti LevelType tsb masih ada dlm tabel LevelType_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.LevelType_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelLevelTypeNameAdd.Text = rowData.LevelTypeName
                    Me.TextLevelTypeDescriptionAdd.Text = rowData.LevelTypeDesc
                Else 'Bila ObjTable.Rows.Count = 0 berarti LevelType tsb sudah tidak lagi berada dlm tabel LevelType_Approval
                    Throw New Exception("Cannot load data from the following Level Type: " & Me.LabelLevelTypeNameAdd.Text & " because that Level Type is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load user edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadLevelTypeEdit()
        Me.LabelTitle.Text = "Activity: Edit Level Type"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.LevelType_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetLevelTypeApprovalData(Me.ParamPkLevelTypeManagement)
                'Bila ObjTable.Rows.Count > 0 berarti LevelType tsb masih ada dlm tabel LevelType_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.LevelType_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelLevelTypeIDEditNewLevelTypeID.Text = rowData.LevelTypeID
                    Me.LabelLevelTypeEditNewLevelTypeName.Text = rowData.LevelTypeName
                    Me.TextLevelTypeEditNewDescription.Text = rowData.LevelTypeDesc
                    Me.LabelLevelTypeIDEditOldLevelTypeID.Text = rowData.LevelTypeID_Old
                    Me.LabelLevelTypeNameEditOldLevelTypeName.Text = rowData.LevelTypeName_Old
                    Me.TextLevelTypeDescriptionEditOldLevelTypeDescription.Text = rowData.LevelTypeDesc_Old
                Else 'Bila ObjTable.Rows.Count = 0 berarti LevelType tsb sudah tidak lagi berada dlm tabel LevelType_Approval
                    Throw New Exception("Cannot load data from the following Level Type: " & Me.LabelLevelTypeNameEditOldLevelTypeName.Text & " because that Level Type is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load user delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadLevelTypeDelete()
        Me.LabelTitle.Text = "Activity: Delete Level Type"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.LevelType_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetLevelTypeApprovalData(Me.ParamPkLevelTypeManagement)
                'Bila ObjTable.Rows.Count > 0 berarti LevelType tsb masih ada dlm tabel LevelType_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.LevelType_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelLevelTypeDeleteLevelTypeID.Text = rowData.LevelTypeID
                    Me.LabelLevelTypeDeleteLevelTypeName.Text = rowData.LevelTypeName
                    Me.TextLevelTypeDeleteDescription.Text = rowData.LevelTypeDesc
                Else 'Bila ObjTable.Rows.Count = 0 berarti LevelType tsb sudah tidak lagi berada dlm tabel LevelType_Approval
                    Throw New Exception("Cannot load data from the following Level Type: " & Me.LabelLevelTypeDeleteLevelTypeName.Text & " because that Level Type is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub
#End Region

#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' user add accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptLevelTypeAdd()

        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessLevelType, Data.IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.LevelType_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Dim ObjTable As Data.DataTable = AccessPending.GetLevelTypeApprovalData(Me.ParamPkLevelTypeManagement)

                    'Bila ObjTable.Rows.Count > 0 berarti User tsb masih ada dlm tabel LevelType_Approval
                    If ObjTable.Rows.Count > 0 Then
                        Dim rowData As AMLDAL.AMLDataSet.LevelType_ApprovalRow = ObjTable.Rows(0)

                        'Periksa apakah LevelTypeName yg baru tsb sudah ada dlm tabel LevelType atau belum. 
                        Dim counter As Int32 = AccessLevelType.CountMatchingLevelType(rowData.LevelTypeName)

                        'Bila counter = 0 berarti LevelTypeName tsb belum ada dlm tabel LevelType, maka boleh ditambahkan
                        If counter = 0 Then
                            'tambahkan item tersebut dalam tabel LevelType
                            AccessLevelType.Insert(rowData.LevelTypeName, rowData.LevelTypeDesc, rowData.LevelTypeCreatedDate)

                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)
                            'catat aktifitas dalam tabel Audit Trail
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)

                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeName", "Add", "", rowData.LevelTypeName, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeDesc", "Add", "", rowData.LevelTypeDesc, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeCreatedDate", "Add", "", rowData.LevelTypeCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                                'hapus item tersebut dalam tabel LevelType_Approval
                                AccessPending.DeleteLevelTypeApproval(rowData.LevelType_PendingApprovalID)

                                'hapus item tersebut dalam tabel LevelType_PendingApproval
                                Using AccessPendingUser As New AMLDAL.AMLDataSetTableAdapters.LevelType_PendingApprovalTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingUser, oSQLTrans)

                                    AccessPendingUser.DeleteLevelTypePendingApproval(rowData.LevelType_PendingApprovalID)
                                End Using

                                oSQLTrans.Commit()
                            End Using
                        Else 'Bila counter != 0 berarti LevelTypeName tsb sudah ada dlm tabel LevelType, maka AcceptLevelTypeAdd gagal
                            Throw New Exception("Cannot add the following Level Type: " & rowData.LevelTypeName & " because that Level Type Name already exists in the database.")
                        End If
                    Else 'Bila ObjTable.Rows.Count = 0 berarti LevelType tsb sudah tidak lagi berada dlm tabel LevelType_Approval
                        Throw New Exception("Cannot add the following Level Type: " & Me.LabelLevelTypeNameAdd.Text & " because that Level Type is no longer in the approval table.")
                    End If
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' LevelType edit accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptLevelTypeEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            'Using TranScope As New Transactions.TransactionScope
            Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessLevelType, Data.IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.LevelType_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Dim ObjTable As Data.DataTable = AccessPending.GetLevelTypeApprovalData(Me.ParamPkLevelTypeManagement)

                    'Bila ObjTable.Rows.Count > 0 berarti User tsb masih ada dlm tabel LevelType_Approval
                    If ObjTable.Rows.Count > 0 Then
                        Dim rowData As AMLDAL.AMLDataSet.LevelType_ApprovalRow = ObjTable.Rows(0)

                        'Periksa apakah LevelTypeName yg baru tsb sudah ada dlm tabel LevelType atau belum. 
                        Dim counter As Int32 = AccessLevelType.CountMatchingLevelType(rowData.LevelTypeName)

                        'Bila tidak ada perubahan LevelTypeName
                        If rowData.LevelTypeName = rowData.LevelTypeName_Old Then
                            GoTo Edit
                        Else 'Bila ada perubahan LevelTypeName
                            'Counter = 0 berarti LevelTypeName tersebut tidak ada dalam tabel LevelType
                            If counter = 0 Then
Edit:
                                'update item tersebut dalam tabel LevelType
                                AccessLevelType.UpdateLevelType(rowData.LevelTypeID, rowData.LevelTypeName, rowData.LevelTypeDesc)

                                Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
                                'catat aktifitas dalam tabel Audit Trail
                                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeName", "Edit", rowData.LevelTypeName_Old, rowData.LevelTypeName, "Accepted")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeDesc", "Edit", rowData.LevelTypeDesc_Old, rowData.LevelTypeDesc, "Accepted")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeID", "Edit", rowData.LevelTypeID_Old, rowData.LevelTypeID, "Accepted")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeCreatedDate", "Edit", rowData.LevelTypeCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.LevelTypeCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                                    'hapus item tersebut dalam tabel LevelType_Approval
                                    AccessPending.DeleteLevelTypeApproval(rowData.LevelType_PendingApprovalID)

                                    'hapus item tersebut dalam tabel LevelType_PendingApproval
                                    Using AccessPendingLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelType_PendingApprovalTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingLevelType, oSQLTrans)
                                        AccessPendingLevelType.DeleteLevelTypePendingApproval(rowData.LevelType_PendingApprovalID)
                                    End Using
                                    oSQLTrans.Commit()
                                End Using
                            Else 'Bila counter != 0 berarti LevelTypeName tsb telah ada dlm tabel LevelType, maka AcceptLevelTypeEdit gagal
                                Throw New Exception("Cannot change to the following Level Type Name: " & rowData.LevelTypeName & " because that Level Type Name already exists in the database.")
                            End If
                        End If
                    Else 'Bila ObjTable.Rows.Count = 0 berarti LevelType tsb sudah tidak lagi berada dlm tabel LevelType_PendingApproval
                        Throw New Exception("Cannot edit the following Level Type: " & Me.LabelLevelTypeNameEditOldLevelTypeName.Text & " because that Level Type is no longer in the approval table.")
                    End If
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' LevelType delete accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptLevelTypeDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            'Using TranScope As New Transactions.TransactionScope
            Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessLevelType, Data.IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.LevelType_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Dim ObjTable As Data.DataTable = AccessPending.GetLevelTypeApprovalData(Me.ParamPkLevelTypeManagement)

                    'Bila ObjTable.Rows.Count > 0 berarti LevelType tsb masih ada dlm tabel LevelType_Approval
                    If ObjTable.Rows.Count > 0 Then
                        Dim rowData As AMLDAL.AMLDataSet.LevelType_ApprovalRow = ObjTable.Rows(0)

                        'Periksa apakah LevelType yg hendak didelete tsb ada dlm tabelnya atau tidak. 
                        Dim counter As Int32 = AccessLevelType.CountMatchingLevelType(rowData.LevelTypeName)

                        'Bila counter = 0 berarti LevelType tsb tidak ada dlm tabel LevelType, maka AcceptGroupDelete gagal
                        If counter = 0 Then
                            Throw New Exception("Cannot delete the following Level Type: " & rowData.LevelTypeName & " because that Level Type does not exist in the database anymore.")
                        Else 'Bila counter != 0, maka LevelType tsb bisa didelete
                            'hapus item tersebut dari tabel LevelType
                            'hapus item tersebut dari tabel LevelType
                            AccessLevelType.DeleteLevelType(rowData.LevelTypeID)

                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
                            'catat aktifitas dalam tabel Audit Trail
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeName", "Delete", rowData.LevelTypeName_Old, rowData.LevelTypeName, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeDesc", "Delete", rowData.LevelTypeDesc_Old, rowData.LevelTypeDesc, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeID", "Delete", rowData.LevelTypeID_Old, rowData.LevelTypeID, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeCreatedDate", "Delete", rowData.LevelTypeCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.LevelTypeCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                                'hapus item tersebut dalam tabel LevelType_Approval
                                AccessPending.DeleteLevelTypeApproval(rowData.LevelType_PendingApprovalID)

                                'hapus item tersebut dalam tabel LevelType_PendingApproval
                                Using AccessPendingLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelType_PendingApprovalTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingLevelType, oSQLTrans)
                                    AccessPendingLevelType.DeleteLevelTypePendingApproval(rowData.LevelType_PendingApprovalID)
                                End Using

                                oSQLTrans.Commit()
                            End Using
                        End If
                    Else 'Bila ObjTable.Rows.Count = 0 berarti LevelType tsb sudah tidak lagi berada dlm tabel LevelType_Approval
                        Throw New Exception("Cannot delete the following Level Type: " & Me.LabelLevelTypeDeleteLevelTypeName.Text & " because that Level Type is no longer in the approval table.")
                    End If
                End Using
            End Using
            'End Using

        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

#End Region
#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject user add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectLevelTypeAdd()

        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(Adapter, Data.IsolationLevel.ReadUncommitted)
                Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelType_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessLevelType, oSQLTrans)
                    Using AccessLevelTypePending As New AMLDAL.AMLDataSetTableAdapters.LevelType_PendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessLevelTypePending, oSQLTrans)
                        Using TableLevelType As Data.DataTable = AccessLevelType.GetLevelTypeApprovalData(Me.ParamPkLevelTypeManagement)

                            'Bila ObjTable.Rows.Count > 0 berarti LevelType tsb masih ada dlm tabel LevelType_Approval
                            If TableLevelType.Rows.Count > 0 Then
                                Dim ObjRow As AMLDAL.AMLDataSet.LevelType_ApprovalRow = TableLevelType.Rows(0)

                                Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)
                                'catat aktifitas dalam tabel Audit Trail
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeName", "Add", "", ObjRow.LevelTypeName, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeDesc", "Add", "", ObjRow.LevelTypeDesc, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeCreatedDate", "Add", "", ObjRow.LevelTypeCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                                'hapus item tersebut dalam tabel LevelType_Approval
                                AccessLevelType.DeleteLevelTypeApproval(Me.ParamPkLevelTypeManagement)

                                'hapus item tersebut dalam tabel LevelType_PendingApproval
                                AccessLevelTypePending.DeleteLevelTypePendingApproval(Me.ParamPkLevelTypeManagement)
                                oSQLTrans.Commit()
                            Else 'Bila ObjTable.Rows.Count = 0 berarti LevelType tsb sudah tidak lagi berada dlm tabel LevelType_Approval
                                Throw New Exception("Operation failed. The following Level Type: " & Me.LabelLevelTypeNameAdd.Text & " is no longer in the approval table.")
                            End If
                        End Using
                    End Using
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()

            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject user edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectLevelTypeEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
                Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelType_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessLevelType, oSQLTrans)
                    Using AccessLevelTypePending As New AMLDAL.AMLDataSetTableAdapters.LevelType_PendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessLevelTypePending, oSQLTrans)
                        Using TableLevelType As Data.DataTable = AccessLevelType.GetLevelTypeApprovalData(Me.ParamPkLevelTypeManagement)

                            'Bila ObjTable.Rows.Count > 0 berarti LevelType tsb masih ada dlm tabel LevelType_Approval
                            If TableLevelType.Rows.Count > 0 Then
                                Dim ObjRow As AMLDAL.AMLDataSet.LevelType_ApprovalRow = TableLevelType.Rows(0)

                                Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
                                'catat aktifitas dalam tabel Audit Trail
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeName", "Edit", ObjRow.LevelTypeName_Old, ObjRow.LevelTypeName, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeDesc", "Edit", ObjRow.LevelTypeDesc_Old, ObjRow.LevelTypeDesc, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeID", "Edit", ObjRow.LevelTypeID_Old, ObjRow.LevelTypeID, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeCreatedDate", "Edit", ObjRow.LevelTypeCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.LevelTypeCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                                'hapus item tersebut dalam tabel LevelType_Approval
                                AccessLevelType.DeleteLevelTypeApproval(Me.ParamPkLevelTypeManagement)

                                'hapus item tersebut dalam tabel LevelType_PendingApproval
                                AccessLevelTypePending.DeleteLevelTypePendingApproval(Me.ParamPkLevelTypeManagement)
                                oSQLTrans.Commit()
                            Else 'Bila ObjTable.Rows.Count = 0 berarti LevelType tsb sudah tidak lagi berada dlm tabel LevelType_Approval
                                Throw New Exception("Operation failed. The following Level Type: " & Me.LabelLevelTypeNameEditOldLevelTypeName.Text & " is no longer in the approval table.")
                            End If
                        End Using
                    End Using
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject user delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectLevelTypeDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
                Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelType_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessLevelType, oSQLTrans)
                    Using AccessLevelTypePending As New AMLDAL.AMLDataSetTableAdapters.LevelType_PendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessLevelTypePending, oSQLTrans)
                        Using TableLevelType As Data.DataTable = AccessLevelType.GetLevelTypeApprovalData(Me.ParamPkLevelTypeManagement)

                            'Bila ObjTable.Rows.Count > 0 berarti LevelType tsb masih ada dlm tabel LevelType_Approval
                            If TableLevelType.Rows.Count > 0 Then
                                Dim ObjRow As AMLDAL.AMLDataSet.LevelType_ApprovalRow = TableLevelType.Rows(0)

                                Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
                                'catat aktifitas dalam tabel Audit Trail
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeName", "Delete", ObjRow.LevelTypeName_Old, ObjRow.LevelTypeName, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeDesc", "Delete", ObjRow.LevelTypeDesc_Old, ObjRow.LevelTypeDesc, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeID", "Delete", ObjRow.LevelTypeID_Old, ObjRow.LevelTypeID, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeCreatedDate", "Delete", ObjRow.LevelTypeCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.LevelTypeCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                                'hapus item tersebut dalam tabel LevelType_Approval
                                AccessLevelType.DeleteLevelTypeApproval(Me.ParamPkLevelTypeManagement)

                                'hapus item tersebut dalam tabel LevelType_PendingApproval
                                AccessLevelTypePending.DeleteLevelTypePendingApproval(Me.ParamPkLevelTypeManagement)
                                oSQLTrans.Commit()
                            Else 'Bila ObjTable.Rows.Count = 0 berarti LevelType tsb sudah tidak lagi berada dlm tabel LevelType_Approval
                                Throw New Exception("Operation failed. The following Level Type: " & Me.LabelLevelTypeDeleteLevelTypeName.Text & " is no longer in the approval table.")
                            End If
                        End Using
                    End Using
                End Using
            End Using
            'End Using
        Catch
            Throw
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HideAll()
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.LevelTypeAdd
                        Me.LoadLevelTypeAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.LevelTypeEdit
                        Me.LoadLevelTypeEdit()
                    Case Sahassa.AML.Commonly.TypeConfirm.LevelTypeDelete
                        Me.LoadLevelTypeDelete()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.LevelTypeAdd
                    Me.AcceptLevelTypeAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.LevelTypeEdit
                    Me.AcceptLevelTypeEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.LevelTypeDelete
                    Me.AcceptLevelTypeDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "LevelTypeManagementApproval.aspx"

            Me.Response.Redirect("LevelTypeManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.LevelTypeAdd
                    Me.RejectLevelTypeAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.LevelTypeEdit
                    Me.RejectLevelTypeEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.LevelTypeDelete
                    Me.RejectLevelTypeDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "LevelTypeManagementApproval.aspx"

            Me.Response.Redirect("LevelTypeManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "LevelTypeManagementApproval.aspx"

        Me.Response.Redirect("LevelTypeManagementApproval.aspx", False)
    End Sub
End Class