#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsKepemilikan_APPROVAL_Detail
	Inherits Parent

#Region "Function"

	Sub HideControl()
		imgReject.Visible = False
		imgApprove.Visible = False
		ImageCancel.Visible = False
	End Sub

	Sub ChangeMultiView(index As Integer)
		MtvMsUser.ActiveViewIndex = index
	End Sub

#End Region

#Region "events..."

	Protected Sub ImageCancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
		Response.Redirect("MsKepemilikan_Approval_View.aspx")
	End Sub

	Protected Sub imgReject_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgReject.Click
		Try
            Rejected()
            ChangeMultiView(1)
            LblConfirmation.Text = "Data has been rejected"
            HideControl()
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

	Protected Sub imgApprove_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgApprove.Click
		Try
			Using ObjMsKepemilikan_Approval As MsKepemilikan_Approval = DataRepository.MsKepemilikan_ApprovalProvider.GetByPK_MsKepemilikan_Approval_Id(parID)
				If ObjMsKepemilikan_Approval.FK_MsMode_Id.GetValueOrDefault = 1 Then
					Inserdata(ObjMsKepemilikan_Approval)
				ElseIf ObjMsKepemilikan_Approval.FK_MsMode_Id.GetValueOrDefault = 2 Then
					UpdateData(ObjMsKepemilikan_Approval)
				ElseIf ObjMsKepemilikan_Approval.FK_MsMode_Id.GetValueOrDefault = 3 Then
					DeleteData(ObjMsKepemilikan_Approval)
				End If
			End Using
			ChangeMultiView(1)
			LblConfirmation.Text = "Data approved successful"
            HideControl()
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try

	End Sub


	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
				ListmapingNew = New TList(Of MappingMsKepemilikanNCBSPPATK_Approval_Detail)
				ListmapingOld = New TList(Of MappingMsKepemilikanNCBSPPATK)
				LoadData()
			Catch ex As Exception
				LogError(ex)
				CvalPageErr.IsValid = False
				CvalPageErr.ErrorMessage = ex.Message
			End Try
		End If

	End Sub

	Private Sub LoadData()
		Dim PkObject As String
		'Load new data
		Using ObjMsKepemilikan_ApprovalDetail As MsKepemilikan_ApprovalDetail = DataRepository.MsKepemilikan_ApprovalDetailProvider.GetPaged(MsKepemilikan_ApprovalDetailColumn.FK_MsKepemilikan_Approval_Id.ToString & _
			"=" & _
			parID, "", 0, 1, Nothing)(0)
            Dim CekMode As MsKepemilikan_Approval = DataRepository.MsKepemilikan_ApprovalProvider.GetByPK_MsKepemilikan_Approval_Id(ObjMsKepemilikan_ApprovalDetail.FK_MsKepemilikan_Approval_Id)
            Dim nama As String = CekMode.FK_MsMode_Id
            If nama = 1 Then
                Baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 2 Then
                Baru.Visible = True
                lama.Visible = True
                baruNew.Visible = True
                lamaOld.Visible = True
            ElseIf nama = 3 Then
                Baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 4 Then
                Baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            End If
			With ObjMsKepemilikan_ApprovalDetail
				SafeDefaultValue = "-"
			   txtIDKepemilikanNew.Text = Safe(.IDKepemilikan)
txtNamaKepemilikanNew.Text = Safe(.NamaKepemilikan)


			       'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByNew.Text = Omsuser(0).UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyNew.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
			  lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
               txtActivationNew.Text = SafeActiveInactive(.activation)
				Dim L_objMappingMsKepemilikanNCBSPPATK_Approval_Detail As TList(Of MappingMsKepemilikanNCBSPPATK_Approval_Detail)
				L_objMappingMsKepemilikanNCBSPPATK_Approval_Detail = DataRepository.MappingMsKepemilikanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKepemilikanNCBSPPATK_Approval_DetailColumn.PK_MsKepemilikan_Approval_Id.ToString & _
					"=" & _
					parID, "", 0, Integer.MaxValue, Nothing)
                'Add mapping
                ListmapingNew.AddRange(L_oBJMappingMsKepemilikanNCBSPPATK_Approval_Detail)
			
			End With
			PkObject = ObjMsKepemilikan_ApprovalDetail.IDKepemilikan
		End Using

		'Load Old Data
		Using ObjMsKepemilikan As MsKepemilikan = DataRepository.MsKepemilikanProvider.GetByIDKepemilikan(PkObject)
			If ObjMsKepemilikan Is Nothing Then Return
			With ObjMsKepemilikan
				SafeDefaultValue = "-"
			    txtIDKepemilikanOld.Text = Safe(.IDKepemilikan)
txtNamaKepemilikanOld.Text = Safe(.NamaKepemilikan)

                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByOld.Text = Omsuser(0).UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyOld.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
			  lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
				Dim L_objMappingMsKepemilikanNCBSPPATK As TList(Of MappingMsKepemilikanNCBSPPATK)
                 txtActivationoLD.Text = SafeActiveInactive(.activation)
              L_ObjMappingMsKepemilikanNCBSPPATK = DataRepository.MappingMsKepemilikanNCBSPPATKProvider.GetPaged(MappingMsKepemilikanNCBSPPATKColumn.IDKepemilikan.ToString & _
                 "=" & _
                 PkObject, "", 0, Integer.MaxValue, Nothing)
                ListmapingOld.AddRange(L_ObjMappingMsKepemilikanNCBSPPATK)
            End With
        End Using
    End Sub



	Protected Sub imgBtnConfirmation_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnConfirmation.Click
		Try
			Response.Redirect("MsKepemilikan_approval_view.aspx")
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub


	Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
		Try
			BindListMapping()
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

	Public ReadOnly Property parID As String
		Get
			Return Request.Item("ID")
		End Get
	End Property
	''' <summary>
	''' Menyimpan Item untuk mapping New sementara
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property ListmapingNew As TList(Of MappingMsKepemilikanNCBSPPATK_Approval_Detail)
		Get
			Return Session("ListmapingNew.data")
		End Get
		Set(value As TList(Of MappingMsKepemilikanNCBSPPATK_Approval_Detail))
			Session("ListmapingNew.data") = value
		End Set
	End Property

	''' <summary>
	''' Menyimpan Item untuk mapping Old sementara
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property ListmapingOld As TList(Of MappingMsKepemilikanNCBSPPATK)
		Get
			Return Session("ListmapingOld.data")
		End Get
		Set(value As TList(Of MappingMsKepemilikanNCBSPPATK))
			Session("ListmapingOld.data") = value
		End Set
	End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKepemilikanNCBSPPATK_Approval_Detail In ListmapingNew.FindAllDistinct("IDKepemilikanNCBS")
                Temp.Add(i.IDKepemilikanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKepemilikanNCBSPPATK In ListmapingOld.FindAllDistinct("IDKepemilikanNCBS")
                Temp.Add(i.IDKepemilikanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

#Region "Action Approval..."

	Sub Rejected()
		Dim ObjMsKepemilikan_Approval As MsKepemilikan_Approval = DataRepository.MsKepemilikan_ApprovalProvider.GetByPK_MsKepemilikan_Approval_Id(parID)
		Dim ObjMsKepemilikan_ApprovalDetail As MsKepemilikan_ApprovalDetail = DataRepository.MsKepemilikan_ApprovalDetailProvider.GetPaged(MsKepemilikan_ApprovalDetailColumn.FK_MsKepemilikan_Approval_Id.ToString & "=" & ObjMsKepemilikan_Approval.PK_MsKepemilikan_Approval_Id, "", 0, 1, Nothing)(0)
		Dim L_ObjMappingMsKepemilikanNCBSPPATK_Approval_Detail As TList(Of MappingMsKepemilikanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKepemilikanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKepemilikanNCBSPPATK_Approval_DetailColumn.PK_MsKepemilikan_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)
		DeleteApproval(ObjMsKepemilikan_Approval, ObjMsKepemilikan_ApprovalDetail, L_ObjMappingMsKepemilikanNCBSPPATK_Approval_Detail)
	End Sub

	Sub DeleteApproval(ByRef objMsKepemilikan_Approval As MsKepemilikan_Approval, ByRef ObjMsKepemilikan_ApprovalDetail As MsKepemilikan_ApprovalDetail, ByRef L_ObjMappingMsKepemilikanNCBSPPATK_Approval_Detail As TList(Of MappingMsKepemilikanNCBSPPATK_Approval_Detail))
		DataRepository.MsKepemilikan_ApprovalProvider.Delete(objMsKepemilikan_Approval)
		DataRepository.MsKepemilikan_ApprovalDetailProvider.Delete(ObjMsKepemilikan_ApprovalDetail)
		DataRepository.MappingMsKepemilikanNCBSPPATK_Approval_DetailProvider.Delete(L_ObjMappingMsKepemilikanNCBSPPATK_Approval_Detail)
	End Sub

	''' <summary>
	''' Delete Data dari  asli dari approval
	''' </summary>
	''' <remarks></remarks>
	Private Sub DeleteData(objMsKepemilikan_Approval As MsKepemilikan_Approval)
		'   '================= Delete Header ===========================================================
		Dim LObjMsKepemilikan_ApprovalDetail As TList(Of MsKepemilikan_ApprovalDetail) = DataRepository.MsKepemilikan_ApprovalDetailProvider.GetPaged(MsKepemilikan_ApprovalDetailColumn.FK_MsKepemilikan_Approval_Id.ToString & "=" & objMsKepemilikan_Approval.PK_MsKepemilikan_Approval_Id, "", 0, 1, Nothing)
		If LObjMsKepemilikan_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
		Dim ObjMsKepemilikan_ApprovalDetail As MsKepemilikan_ApprovalDetail = LObjMsKepemilikan_ApprovalDetail(0)
		Using ObjNewMsKepemilikan As MsKepemilikan = DataRepository.MsKepemilikanProvider.GetByIDKepemilikan(ObjMsKepemilikan_ApprovalDetail.IDKepemilikan)
			DataRepository.MsKepemilikanProvider.Delete(ObjNewMsKepemilikan)
		End Using

		'======== Delete MAPPING =========================================
		Dim L_ObjMappingMsKepemilikanNCBSPPATK_Approval_Detail As TList(Of MappingMsKepemilikanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKepemilikanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKepemilikanNCBSPPATK_Approval_DetailColumn.PK_MsKepemilikan_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


		For Each c As MappingMsKepemilikanNCBSPPATK_Approval_Detail In L_ObjMappingMsKepemilikanNCBSPPATK_Approval_Detail
			Using ObjNewMappingMsKepemilikanNCBSPPATK As MappingMsKepemilikanNCBSPPATK = DataRepository.MappingMsKepemilikanNCBSPPATKProvider.GetByPK_MappingMsKepemilikanNCBSPPATK_Id(c.PK_MappingMsKepemilikanNCBSPPATK_Id)
				DataRepository.MappingMsKepemilikanNCBSPPATKProvider.Delete(ObjNewMappingMsKepemilikanNCBSPPATK)
			End Using
		Next

		'======== Delete Approval data ======================================
		DeleteApproval(objMsKepemilikan_Approval, ObjMsKepemilikan_ApprovalDetail, L_ObjMappingMsKepemilikanNCBSPPATK_Approval_Detail)
	End Sub

	''' <summary>
	''' Update Data dari approval ke table asli 
	''' </summary>
	''' <remarks></remarks>
	Private Sub UpdateData(objMsKepemilikan_Approval As MsKepemilikan_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
		'   '================= Update Header ===========================================================
		Dim LObjMsKepemilikan_ApprovalDetail As TList(Of MsKepemilikan_ApprovalDetail) = DataRepository.MsKepemilikan_ApprovalDetailProvider.GetPaged(MsKepemilikan_ApprovalDetailColumn.FK_MsKepemilikan_Approval_Id.ToString & "=" & objMsKepemilikan_Approval.PK_MsKepemilikan_Approval_Id, "", 0, 1, Nothing)
		If LObjMsKepemilikan_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
		Dim ObjMsKepemilikan_ApprovalDetail As MsKepemilikan_ApprovalDetail = LObjMsKepemilikan_ApprovalDetail(0)
		Dim ObjMsKepemilikanNew As MsKepemilikan
		Using ObjMsKepemilikanOld As MsKepemilikan = DataRepository.MsKepemilikanProvider.GetByIDKepemilikan(ObjMsKepemilikan_ApprovalDetail.IDKepemilikan)
			If ObjMsKepemilikanOld Is Nothing Then Throw New Exception("Data Not Found")
			ObjMsKepemilikanNew = ObjMsKepemilikanOld.Clone
			With ObjMsKepemilikanNew
				FillOrNothing(.NamaKepemilikan, ObjMsKepemilikan_ApprovalDetail.NamaKepemilikan)
				FillOrNothing(.CreatedBy, ObjMsKepemilikan_ApprovalDetail.CreatedBy)
				FillOrNothing(.CreatedDate, ObjMsKepemilikan_ApprovalDetail.CreatedDate)
				FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
				FillOrNothing(.ApprovedDate, Date.Now)
			End With
			DataRepository.MsKepemilikanProvider.Save(ObjMsKepemilikanNew)
			'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.AML.Commonly.SessionNIK, Sahassa.AML.Commonly.SessionStaffName, "MsBentukBidangUsaha has Approved to Update data", ObjMsBentukBidangUsahaNew, ObjMsBentukBidangUsahaOld)
			'======== Update MAPPING =========================================
			'delete mapping item
			Using L_ObjMappingMsKepemilikanNCBSPPATK As TList(Of MappingMsKepemilikanNCBSPPATK) = DataRepository.MappingMsKepemilikanNCBSPPATKProvider.GetPaged(MappingMsKepemilikanNCBSPPATKColumn.IDKepemilikan.ToString & _
				"=" & _
				ObjMsKepemilikanNew.IDKepemilikan, "", 0, Integer.MaxValue, Nothing)
				DataRepository.MappingMsKepemilikanNCBSPPATKProvider.Delete(L_ObjMappingMsKepemilikanNCBSPPATK)
			End Using
			'Insert mapping item
			Dim L_ObjMappingMsKepemilikanNCBSPPATK_Approval_Detail As TList(Of MappingMsKepemilikanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKepemilikanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKepemilikanNCBSPPATK_Approval_DetailColumn.PK_MsKepemilikan_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

			Dim L_ObjNewMappingMsKepemilikanNCBSPPATK As New TList(Of MappingMsKepemilikanNCBSPPATK)
			For Each c As MappingMsKepemilikanNCBSPPATK_Approval_Detail In L_ObjMappingMsKepemilikanNCBSPPATK_Approval_Detail
				Dim ObjNewMappingMsKepemilikanNCBSPPATK As New MappingMsKepemilikanNCBSPPATK '= DataRepository.MappingMsKepemilikanNCBSPPATKProvider.GetByPK_MappingMsKepemilikanNCBSPPATK_Id(c.PK_MappingMsKepemilikanNCBSPPATK_Id.GetValueOrDefault)
				With ObjNewMappingMsKepemilikanNCBSPPATK
					FillOrNothing(.IDKepemilikan, ObjMsKepemilikanNew.IDKepemilikan)
					FillOrNothing(.Nama, c.Nama)
					FillOrNothing(.IDKepemilikanNCBS, c.IDKepemilikanNCBS)
					L_ObjNewMappingMsKepemilikanNCBSPPATK.Add(ObjNewMappingMsKepemilikanNCBSPPATK)
				End With
			Next
			DataRepository.MappingMsKepemilikanNCBSPPATKProvider.Save(L_ObjNewMappingMsKepemilikanNCBSPPATK)
			'======== Delete Approval data ======================================
			DeleteApproval(objMsKepemilikan_Approval, ObjMsKepemilikan_ApprovalDetail, L_ObjMappingMsKepemilikanNCBSPPATK_Approval_Detail)
		End Using
	End Sub
	''' <summary>
	''' Insert Data dari approval ke table asli 
	''' </summary>
	''' <remarks></remarks>
	Private Sub Inserdata(objMsKepemilikan_Approval As MsKepemilikan_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
		'   '================= Save Header ===========================================================
		Dim LObjMsKepemilikan_ApprovalDetail As TList(Of MsKepemilikan_ApprovalDetail) = DataRepository.MsKepemilikan_ApprovalDetailProvider.GetPaged(MsKepemilikan_ApprovalDetailColumn.FK_MsKepemilikan_Approval_Id.ToString & "=" & objMsKepemilikan_Approval.PK_MsKepemilikan_Approval_Id, "", 0, 1, Nothing)
		'jika data tidak ada di database
		If LObjMsKepemilikan_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
		'binding approval ke table asli
		Dim ObjMsKepemilikan_ApprovalDetail As MsKepemilikan_ApprovalDetail = LObjMsKepemilikan_ApprovalDetail(0)
		Using ObjNewMsKepemilikan As New MsKepemilikan()
			With ObjNewMsKepemilikan
				FillOrNothing(.IDKepemilikan, ObjMsKepemilikan_ApprovalDetail.IDKepemilikan)
				FillOrNothing(.NamaKepemilikan, ObjMsKepemilikan_ApprovalDetail.NamaKepemilikan)
				FillOrNothing(.CreatedBy, ObjMsKepemilikan_ApprovalDetail.CreatedBy)
				FillOrNothing(.CreatedDate, ObjMsKepemilikan_ApprovalDetail.CreatedDate)
				FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
				FillOrNothing(.ApprovedDate, Date.Now)
                .activation = True
			End With
			DataRepository.MsKepemilikanProvider.Save(ObjNewMsKepemilikan)
			'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.AML.Commonly.SessionNIK, Sahassa.AML.Commonly.SessionStaffName, "MsBentukBidangUsaha has Approved to Insert data", ObjNewMsBentukBidangUsaha, Nothing)
			'======== Insert MAPPING =========================================
			Dim L_ObjMappingMsKepemilikanNCBSPPATK_Approval_Detail As TList(Of MappingMsKepemilikanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKepemilikanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKepemilikanNCBSPPATK_Approval_DetailColumn.PK_MsKepemilikan_Approval_Id.ToString & _
				"=" & _
				parID, "", 0, Integer.MaxValue, Nothing)
			Dim L_ObjNewMappingMsKepemilikanNCBSPPATK As New TList(Of MappingMsKepemilikanNCBSPPATK)()
			For Each c As MappingMsKepemilikanNCBSPPATK_Approval_Detail In L_ObjMappingMsKepemilikanNCBSPPATK_Approval_Detail
				Dim ObjNewMappingMsKepemilikanNCBSPPATK As New MappingMsKepemilikanNCBSPPATK()
				With ObjNewMappingMsKepemilikanNCBSPPATK
					FillOrNothing(.IDKepemilikan, ObjNewMsKepemilikan.IDKepemilikan)
 					FillOrNothing(.Nama, c.Nama)
					FillOrNothing(.IDKepemilikanNCBS, c.IDKepemilikanNCBS)
					L_ObjNewMappingMsKepemilikanNCBSPPATK.Add(ObjNewMappingMsKepemilikanNCBSPPATK)
				End With
			Next
			DataRepository.MappingMsKepemilikanNCBSPPATKProvider.Save(L_ObjNewMappingMsKepemilikanNCBSPPATK)
			'======== Delete Approval data ======================================
			DeleteApproval(objMsKepemilikan_Approval, ObjMsKepemilikan_ApprovalDetail, L_ObjMappingMsKepemilikanNCBSPPATK_Approval_Detail)
		End Using
	End Sub

#End Region

End Class


