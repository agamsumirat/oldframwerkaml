Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Imports System.IO
Imports System.Collections.Generic

Partial Class MappingGroupMenuSTRPPATKEdit
    Inherits Parent

    Public ReadOnly Property PK_MappingGroupMenuSTRPPATK_ID() As Integer
        Get
            Dim strTemp As String = Request.Params("PK_MappingGroupMenuSTRPPATK_ID")
            Dim intResult As Integer
            If Integer.TryParse(strTemp, intResult) Then
                Return intResult
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Parameter PK_MappingGroupMenuSTRPPATK_ID is invalid."

            End If
        End Get
    End Property

    Public ReadOnly Property Objvw_MappingGroupMenuSTRPPATK() As VList(Of vw_MappingGroupMenuSTRPPATK)
        Get
            If Session("Objvw_MappingGroupMenuSTRPPATK.MappingGroupMenuSTRPPATKEdit") Is Nothing Then

                Session("Objvw_MappingGroupMenuSTRPPATK.MappingGroupMenuSTRPPATKEdit") = DataRepository.vw_MappingGroupMenuSTRPPATKProvider.GetPaged("PK_MappingGroupMenuSTRPPATK_ID = '" & PK_MappingGroupMenuSTRPPATK_ID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("Objvw_MappingGroupMenuSTRPPATK.MappingGroupMenuSTRPPATKEdit"), VList(Of vw_MappingGroupMenuSTRPPATK))
        End Get
    End Property

    Public Property SetnGetMode() As String
        Get
            If Not Session("MappingGroupMenuSTRPPATKEdit.Mode") Is Nothing Then
                Return CStr(Session("MappingGroupMenuSTRPPATKEdit.Mode"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingGroupMenuSTRPPATKEdit.Mode") = value
        End Set
    End Property

    Private Sub LoadMode()
        Session("MappingGroupMenuSTRPPATKEdit.Mode") = Nothing
        If Objvw_MappingGroupMenuSTRPPATK.Count > 0 Then

            CboGroupMenu.Items.Clear()
            Using ObjSTRPPATKBll As New AMLBLL.MappingGroupMenuSTRPPATKBLL
                CboGroupMenu.AppendDataBoundItems = True
                CboGroupMenu.DataSource = ObjSTRPPATKBll.GeModeDataEdit
                CboGroupMenu.DataTextField = GroupColumn.GroupName.ToString
                CboGroupMenu.DataValueField = GroupColumn.GroupID.ToString
                CboGroupMenu.DataBind()
                CboGroupMenu.Items.Insert(0, New ListItem("...", ""))
                'CboGroupMenu.Items.Insert(1, New ListItem(Objvw_MappingGroupMenuSTRPPATK(0).GroupName, CStr(Objvw_MappingGroupMenuSTRPPATK(0).FK_Group_ID)))
            End Using
        Else
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = MappingGroupMenuSTRPPATKBLL.DATA_NOTVALID
        End If

    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oTrans As SqlTransaction = Nothing
        Try

            Using ObjList As MappingGroupMenuSTRPPATK = DataRepository.MappingGroupMenuSTRPPATKProvider.GetByPK_MappingGroupMenuSTRPPATK_ID(PK_MappingGroupMenuSTRPPATK_ID)
                If ObjList Is Nothing Then
                    Throw New AMLBLL.SahassaException(MappingGroupMenuSTRPPATKEnum.DATA_NOTVALID)
                End If
            End Using

            Dim GroupID As Integer
            If CboGroupMenu.Text = "" Then
                GroupID = 0
            Else
                GroupID = CInt(CboGroupMenu.Text)
            End If

            Dim GroupName As String = ""
            If GroupID > 0 Then

                AMLBLL.MappingGroupMenuSTRPPATKBLL.IsDataValidEditApproval(CStr(GroupID))

                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    Using ObjMappingGroupMenuSTRPPATKNEW As New MappingGroupMenuSTRPPATK
                        Using ObjMappingGroupMenuSTRPPATK As MappingGroupMenuSTRPPATK = DataRepository.MappingGroupMenuSTRPPATKProvider.GetByPK_MappingGroupMenuSTRPPATK_ID(PK_MappingGroupMenuSTRPPATK_ID)
                            Using ObjGroup As Group = DataRepository.GroupProvider.GetByGroupID(CInt(ObjMappingGroupMenuSTRPPATK.FK_Group_ID))
                                AMLBLL.MappingGroupMenuSTRPPATKBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingGroupMenuSTRPPATK", "GroupName", "Edit", ObjGroup.GroupName, CStr(GroupID), "Acc")
                            End Using
                            DataRepository.MappingGroupMenuSTRPPATKProvider.Delete(ObjMappingGroupMenuSTRPPATK)
                        End Using
                        ObjMappingGroupMenuSTRPPATKNEW.FK_Group_ID = GroupID

                        Using Objlist As TList(Of MappingGroupMenuSTRPPATK) = DataRepository.MappingGroupMenuSTRPPATKProvider.GetPaged("FK_Group_ID = '" & GroupID & "'", "", 0, Integer.MaxValue, 0)
                            If Objlist.Count > 0 Then
                                Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKView.aspx"
                                Me.Response.Redirect("MappingGroupMenuSTRPPATKView.aspx", False)
                            Else

                                DataRepository.MappingGroupMenuSTRPPATKProvider.Save(ObjMappingGroupMenuSTRPPATKNEW)
                                Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKView.aspx"
                                Me.Response.Redirect("MappingGroupMenuSTRPPATKView.aspx", False)
                            End If
                        End Using

                    End Using
                Else
                    Using ObjMappingGroupMenuSTRPPATK_Approval As New MappingGroupMenuSTRPPATK_Approval

                        Using objgroup As TList(Of Group) = DataRepository.GroupProvider.GetPaged("GroupID = '" & GroupID & "'", "", 0, Integer.MaxValue, 0)
                            GroupName = objgroup(0).GroupName

                            ObjMappingGroupMenuSTRPPATK_Approval.FK_MsUserID = Sahassa.AML.Commonly.SessionPkUserId
                            ObjMappingGroupMenuSTRPPATK_Approval.GroupMenu = GroupName
                            ObjMappingGroupMenuSTRPPATK_Approval.FK_ModeID = CInt(Sahassa.AML.Commonly.TypeMode.Edit)
                            ObjMappingGroupMenuSTRPPATK_Approval.CreatedDate = Now
                            DataRepository.MappingGroupMenuSTRPPATK_ApprovalProvider.Save(ObjMappingGroupMenuSTRPPATK_Approval)

                            Using ObjMappingGroupMenuSTRPPATK As MappingGroupMenuSTRPPATK = DataRepository.MappingGroupMenuSTRPPATKProvider.GetByPK_MappingGroupMenuSTRPPATK_ID(PK_MappingGroupMenuSTRPPATK_ID)
                                Using ObjMappingGroupMenuSTRPPATK_approvalDetail As New MappingGroupMenuSTRPPATK_ApprovalDetail
                                    ObjMappingGroupMenuSTRPPATK_approvalDetail.FK_MappingGroupMenuSTRPPATK_Approval_ID = ObjMappingGroupMenuSTRPPATK_Approval.PK_MappingGroupMenuSTRPPATK_Approval_ID
                                    ObjMappingGroupMenuSTRPPATK_approvalDetail.PK_MappingGroupMenuSTRPPATK_ID = PK_MappingGroupMenuSTRPPATK_ID
                                    ObjMappingGroupMenuSTRPPATK_approvalDetail.FK_Group_ID = GroupID
                                    ObjMappingGroupMenuSTRPPATK_approvalDetail.PK_MappingGroupMenuSTRPPATK_ID_Old = PK_MappingGroupMenuSTRPPATK_ID
                                    ObjMappingGroupMenuSTRPPATK_approvalDetail.FK_Group_ID_Old = ObjMappingGroupMenuSTRPPATK.FK_Group_ID
                                    'ObjMappingGroupMenuSTRPPATK_approvalDetail.PK_MappingGroupMenuSTRPPATK_ID_Old = ObjMappingGroupMenuSTRPPATK.PK_MappingGroupMenuSTRPPATK_ID
                                    AMLBLL.MappingGroupMenuSTRPPATKBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingGroupMenuSTRPPATK", "GroupName", "Edit", GroupName, CStr(GroupID), "PendingApproval")
                                    DataRepository.MappingGroupMenuSTRPPATK_ApprovalDetailProvider.Save(ObjMappingGroupMenuSTRPPATK_approvalDetail)
                                End Using
                            End Using
                        End Using
                        'Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKView.aspx"
                        'Me.Response.Redirect("MappingGroupMenuSTRPPATKView.aspx", False)
                        Using ObjGroup As TList(Of Group) = DataRepository.GroupProvider.GetPaged("GroupID = '" & GroupID & "'", "", 0, Integer.MaxValue, 0)
                            Me.LblSuccess.Text = "Data saved successfully and waiting for Approval."
                            Me.LblSuccess.Visible = True
                            LoadMode()
                        End Using

                    End Using
                End If
                MultiView1.ActiveViewIndex = 1
            Else
                Me.LblSuccess.Text = "Please select Group Menu"
                Me.LblSuccess.Visible = True
            End If
        Catch ex As Exception
            If Not oTrans Is Nothing Then oTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
                oTrans = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKView.aspx"
            Me.Response.Redirect("MappingGroupMenuSTRPPATKView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Session("Objvw_MappingGroupMenuSTRPPATK.MappingGroupMenuSTRPPATKEdit") = Nothing
            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                '    Transcope.Complete()
            End Using
            If Not Page.IsPostBack Then
                LoadMode()
                MultiView1.ActiveViewIndex = 0
            End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKView.aspx"
            Me.Response.Redirect("MappingGroupMenuSTRPPATKView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
