<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="CaseManagementIssueEdit.aspx.vb" Inherits="CaseManagementIssueEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
 <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Case Management Issue - Edit 
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <span style="color: #000000"></span></td>
        </tr>
    </table>
    <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="color: #000000; border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                <br />
            </td>
            <td bgcolor="#ffffff" style="color: #000000; height: 24px" width="20%">
                Issue Title</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%" valign="top">
                <asp:Label ID="LabelIssueTitle" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="6" style="width: 24px; height: 24px">
                <br />
                <br />
                <br />
                <br />
                &nbsp;</td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px">
                Description<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px">
                :<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px" valign="top">
                <asp:Label ID="LabelDescription" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
        </tr>
        <tr class="formText">
        </tr>
        <tr class="formText">
        </tr>
        <tr class="formText">
        </tr>
        <tr class="formText">
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                Issue Status</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                :</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                <asp:DropDownList ID="CboIssueStatus" runat="server" CssClass="combobox">
                </asp:DropDownList></td>
        </tr>
        <tr bgcolor="#dddddd" class="formText" height="30">
            <td style="width: 24px">
                <img height="15" src="images/arrow.gif" width="15" /></td>
            <td colspan="3" style="height: 9px">
                <table border="0" cellpadding="3" cellspacing="0">
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="True" SkinID="SaveButton" /></td>
                        <td>
                            <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton" /></td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
        </tr>
    </table>

   

</asp:Content>

