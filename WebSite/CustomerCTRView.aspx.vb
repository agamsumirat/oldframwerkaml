Imports System.Collections.Generic
Imports System.Data
Imports Sahassa.AML
Imports System.IO
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services


Partial Class CustomerCTRView
    Inherits Parent
    Private BindGridFromExcel As Boolean = False

#Region " Property "



    ReadOnly Property GetTanggalTransaksi() As String
        Get
            If Not Request.Params("TanggalTransaksi") Is Nothing Then
                Return CStr(Request.Params("TanggalTransaksi")).Replace("'", "''")
            Else
                Return ""
            End If

        End Get
    End Property

    ReadOnly Property GetTipeTransaksi() As String
        Get
            If Not Request.Params("TipeTransaksi") Is Nothing Then
                Return CStr(Request.Params("TipeTransaksi")).Replace("'", "''")
            Else
                Return ""
            End If

        End Get
    End Property

    ReadOnly Property GetNomorTiketTransaksi() As String
        Get
            If Not Request.Params("NomorTiketTransaksi") Is Nothing Then
                Return CStr(Request.Params("NomorTiketTransaksi")).Replace("'", "''")
            Else
                Return ""
            End If
        End Get
    End Property

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("CustomerCTRViewSelected") Is Nothing, New ArrayList, Session("CustomerCTRViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CustomerCTRViewSelected") = value
        End Set
    End Property

    Private Property IsSearch() As Boolean
        Get
            Return IIf(Session("CustomerCTRView.IsSearch") Is Nothing, False, Session("CustomerCTRView.IsSearch"))
        End Get
        Set(ByVal Value As Boolean)
            Session("CustomerCTRView.IsSearch") = Value
        End Set
    End Property

    'Private Property SetnGetFieldSearch() As String
    '    Get
    '        Return IIf(Session("CustomerCTRViewFieldSearch") Is Nothing, "", Session("CustomerCTRViewFieldSearch"))
    '    End Get
    '    Set(ByVal Value As String)
    '        Session("CustomerCTRViewFieldSearch") = Value
    '    End Set
    'End Property

    'Private Property SetnGetValueSearch() As String
    '    Get
    '        Return IIf(Session("CustomerCTRViewValueSearch") Is Nothing, "", Session("CustomerCTRViewValueSearch"))
    '    End Get
    '    Set(ByVal Value As String)
    '        Session("CustomerCTRViewValueSearch") = Value
    '    End Set
    'End Property

    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CustomerCTRViewSort") Is Nothing, "Pk_CustomerCTR_Id  asc", Session("CustomerCTRViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("CustomerCTRViewSort") = Value
        End Set
    End Property

    Private Property SetnGetSortCustomer() As String
        Get
            'Return IIf(Session("CustomerCTRView.SortCustomer") Is Nothing, "CustomerInformationDetail_WebTempTable.CIFNo  asc", Session("CustomerCTRView.SortCustomer"))
            Return IIf(Session("CustomerCTRView.SortCustomer") Is Nothing, "CIFNo  asc", Session("CustomerCTRView.SortCustomer"))
        End Get
        Set(ByVal Value As String)
            Session("CustomerCTRView.SortCustomer") = Value
        End Set
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CustomerCTRViewCurrentPage") Is Nothing, 0, Session("CustomerCTRViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CustomerCTRViewCurrentPage") = Value
        End Set
    End Property

    Private Property SetnGetCurrentPageCustomer() As Int32
        Get
            Return IIf(Session("CustomerCTRView.CurrentPageCustomer") Is Nothing, 0, Session("CustomerCTRView.CurrentPageCustomer"))
        End Get
        Set(ByVal Value As Int32)
            Session("CustomerCTRView.CurrentPageCustomer") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property

    Private ReadOnly Property GetPageTotalCustomer() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotalCustomer)
        End Get
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CustomerCTRViewRowTotal") Is Nothing, 0, Session("CustomerCTRViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CustomerCTRViewRowTotal") = Value
        End Set
    End Property

    Private Property SetnGetRowTotalCustomer() As Int32
        Get
            Return IIf(Session("CustomerCTRView.RowTotalCustomer") Is Nothing, 0, Session("CustomerCTRView.RowTotalCustomer"))
        End Get
        Set(ByVal Value As Int32)
            Session("CustomerCTRView.RowTotalCustomer") = Value
        End Set
    End Property

    Private Function SetnGetBindTable() As TList(Of CustomerCTR)
        Return DataRepository.CustomerCTRProvider.GetPaged(SetAndGetSearchingCriteria, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, SetnGetRowTotal)
    End Function

    Private Function SetnGetBindTableCustomer() As Data.DataTable
        Dim StrTable As String
        Dim StrPK As String
        Dim StrFields As String
        'TODO: sementara join AllAccount_WebTempTable karena datanya ngak ada, harusnya AllAccount_WebbTempTableUnionJ
        'StrTable = "CustomerInformationDetail_WebTempTable INNER JOIN AllAccount_WebbTempTableUnionJ ON CustomerInformationDetail_WebTempTable.CIFNo=AllAccount_WebbTempTableUnionJ.CIFNo"
        'StrPK = "AllAccount_WebbTempTableUnionJ.AccountNo"
        'StrFields = "CustomerInformationDetail_WebTempTable.CIFNo, AllAccount_WebbTempTableUnionJ.AccountNo, CustomerInformationDetail_WebTempTable.CustomerName, CustomerInformationDetail_WebTempTable.DateOfBirth, CustomerInformationDetail_WebTempTable.BirthPlace, CustomerInformationDetail_WebTempTable.IdType, CustomerInformationDetail_WebTempTable.IdNumber"
        'temporary karena data kosong, jadi pake AllAccount_WebTempTable dulu
        'StrTable = "CustomerInformationDetail_WebTempTable INNER JOIN AllAccount_WebTempTable ON CustomerInformationDetail_WebTempTable.CIFNo=AllAccount_WebTempTable.CIFNo"
        'StrPK = "AllAccount_WebTempTable.AccountNo"
        'StrFields = "CustomerInformationDetail_WebTempTable.CIFNo, AllAccount_WebTempTable.AccountNo, CustomerInformationDetail_WebTempTable.CustomerName, CustomerInformationDetail_WebTempTable.DateOfBirth, CustomerInformationDetail_WebTempTable.BirthPlace, CustomerInformationDetail_WebTempTable.IdType, CustomerInformationDetail_WebTempTable.IdNumber"



        StrTable = "vw_CustomerCTRView"
        StrPK = "AccountNo"

        Me.SetnGetRowTotalCustomer = Sahassa.AML.Commonly.GetTableCount(StrTable, SetAndGetSearchingCriteriaCustomer)

        'dirubah
        'Return Sahassa.AML.Commonly.GetDatasetPaging(StrTable, StrPK, Me.SetnGetSortCustomer, Me.SetnGetCurrentPageCustomer + 1, Sahassa.AML.Commonly.GetDisplayedTotalRow, StrFields, SetAndGetSearchingCriteriaCustomer, "")
        Return Sahassa.AML.Commonly.GetDatasetPaging(StrTable, "*", SetAndGetSearchingCriteriaCustomer, Me.SetnGetSortCustomer, Me.SetnGetCurrentPageCustomer, Sahassa.AML.Commonly.GetDisplayedTotalRow, SetnGetRowTotalCustomer)



        'Return DataRepository.CustomerInformationDetail_WebTempTableProvider.GetPaged(SetAndGetSearchingCriteriaCustomer, SetnGetSortCustomer, SetnGetCurrentPageCustomer, Sahassa.AML.Commonly.GetDisplayedTotalRow, SetnGetRowTotalCustomer)
    End Function

    Private Function SetnGetBindTableAll() As TList(Of CustomerCTR)
        Return DataRepository.CustomerCTRProvider.GetPaged(SetAndGetSearchingCriteria, SetnGetSort, 0, Integer.MaxValue, SetnGetRowTotal)
    End Function

    Private Property SetAndGetSearchingCriteria() As String
        Get
            Return IIf(Session("CustomerCTRViewSearchCriteria") Is Nothing, "", Session("CustomerCTRViewSearchCriteria"))
        End Get
        Set(ByVal Value As String)
            Session("CustomerCTRViewSearchCriteria") = Value
        End Set
    End Property

    Private Property SetAndGetSearchingCriteriaCustomer() As String
        Get
            Return IIf(Session("CustomerCTRViewSearchCriteriaCustomer") Is Nothing, "", Session("CustomerCTRViewSearchCriteriaCustomer"))
        End Get
        Set(ByVal Value As String)
            Session("CustomerCTRViewSearchCriteriaCustomer") = Value
        End Set
    End Property
#End Region 'done

#Region "Bind Combo Box"
    Private Sub BindIdType()
        Try

            Me.CboIDType.Items.Clear()
            Me.CboIDType.DataSource = AMLBLL.CustomerCTRBLL.GetAllMsIDType()
            Me.CboIDType.DataValueField = "Pk_MsIDType_Id"
            Me.CboIDType.DataTextField = "MsIDType_Name"
            Me.CboIDType.DataBind()
            Me.CboIDType.Items.Insert(0, New ListItem("[All]", "0"))
            Me.CboIDType.SelectedIndex = 0

        Catch
            Throw
        End Try
    End Sub
#End Region 'done

#Region "Function..."

    Private Function GetAllIDSelected(ByVal arrayList As ArrayList) As String
        Dim strVal As String = ""
        For Each pkid As String In arrayList
            strVal = strVal & pkid & ","
        Next
        Dim strRetVal As String = strVal.Substring(0, strVal.LastIndexOf(","))
        Return strRetVal
    End Function

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Function isValidSearch() As Boolean
        Try
            If TxtDateOfBirth.Text <> "" Then
                If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtDateOfBirth.Text) = False Then
                    Throw New Exception("Date of Birth tidak valid, gunakan format dd-MM-yyyy")
                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
        Return True
    End Function

    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub

    Private Sub SetInfoNavigateCustomer()
        Try
            Me.PageCurrentPageCustomer.Text = Me.SetnGetCurrentPageCustomer + 1
            Me.PageTotalPagesCustomer.Text = Me.GetPageTotalCustomer
            Me.PageTotalRowsCustomer.Text = Me.SetnGetRowTotalCustomer
            Me.LinkButtonNextCustomer.Enabled = (Not Me.SetnGetCurrentPageCustomer + 1 = Me.GetPageTotalCustomer) AndAlso Me.GetPageTotalCustomer <> 0
            Me.LinkButtonLastCustomer.Enabled = (Not Me.SetnGetCurrentPageCustomer + 1 = Me.GetPageTotalCustomer) AndAlso Me.GetPageTotalCustomer <> 0
            Me.LinkButtonFirstCustomer.Enabled = Not Me.SetnGetCurrentPageCustomer = 0
            Me.LinkButtonPreviousCustomer.Enabled = Not Me.SetnGetCurrentPageCustomer = 0
        Catch
            Throw
        End Try
    End Sub

    Public Sub BindGrid()
        If Me.IsSearch Then
            If RblCustomerNonCustomer.SelectedValue = "1" Then
                MvCustomerCTR.ActiveViewIndex = 0
                Me.GridDataView.DataSource = Me.SetnGetBindTable
                Me.GridDataView.VirtualItemCount = Me.SetnGetRowTotal
                Me.GridDataView.DataBind()
                If Me.SetnGetRowTotal = 0 Then
                    Me.TblData.Visible = False
                    Me.TblDataNotFound.Visible = True
                Else
                    Me.TblData.Visible = True
                    Me.TblDataNotFound.Visible = False
                End If
            Else
                MvCustomerCTR.ActiveViewIndex = 1
                Me.GridDataViewCustomer.DataSource = Me.SetnGetBindTableCustomer
                Me.GridDataViewCustomer.VirtualItemCount = Me.SetnGetRowTotalCustomer
                Me.GridDataViewCustomer.DataBind()
                If Me.SetnGetRowTotalCustomer = 0 Then
                    Me.TblDataCustomer.Visible = False
                    Me.TblDataNotFound.Visible = True
                Else
                    Me.TblDataCustomer.Visible = True
                    Me.TblDataNotFound.Visible = False
                End If
            End If

        End If
    End Sub

    Private Sub ClearThisPageSessions()
        Session("CustomerCTRViewSelected") = Nothing
        Session("CustomerCTRView.IsSearch") = Nothing

        Session("CustomerCTRViewSort") = Nothing
        Session("CustomerCTRView.SortCustomer") = Nothing
        Session("CustomerCTRViewCurrentPage") = Nothing
        Session("CustomerCTRView.CurrentPageCustomer") = Nothing
        Session("CustomerCTRViewRowTotal") = Nothing
        Session("CustomerCTRView.RowTotalCustomer") = Nothing
        Session("CustomerCTRViewSearchCriteria") = Nothing
        Session("CustomerCTRViewSearchCriteriaCustomer") = Nothing

    End Sub

    Private Function SearchFilter() As String
        Dim StrSearch As String = ""
        Try
            If isValidSearch() Then


                If Me.TxtFullName.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " CustomerName like '%" & Me.TxtFullName.Text.Replace("'", "''") & "%'"
                    Else
                        StrSearch = StrSearch & " And CustomerName like '%" & Me.TxtFullName.Text.Replace("'", "''") & "%' "
                    End If
                End If

                If Me.TxtDateOfBirth.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " INDV_TanggalLahir = '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", Me.TxtDateOfBirth.Text.Replace("'", "''")).ToString("yyyy-MM-dd") & "' "
                    Else
                        StrSearch = StrSearch & " And INDV_TanggalLahir = '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", Me.TxtDateOfBirth.Text.Replace("'", "''")).ToString("yyyy-MM-dd") & "' "
                    End If
                End If

                If Me.TxtBirthPlace.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " INDV_TempatLahir like '%" & Me.TxtBirthPlace.Text.Replace("'", "''") & "%'"
                    Else
                        StrSearch = StrSearch & " And INDV_TempatLahir like '%" & Me.TxtBirthPlace.Text.Replace("'", "''") & "%' "
                    End If
                End If

                If Me.CboIDType.SelectedIndex <> 0 Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " INDV_FK_MsIDType_Id = '" & Me.CboIDType.SelectedValue & "' "
                    Else
                        StrSearch = StrSearch & " And INDV_FK_MsIDType_Id = '" & Me.CboIDType.SelectedValue & "' "
                    End If
                End If

                If Me.TxtIDNo.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " INDV_NomorId like '%" & Me.TxtIDNo.Text.Replace("'", "''") & "%' "
                    Else
                        StrSearch = StrSearch & " and INDV_NomorId like '%" & Me.TxtIDNo.Text.Replace("'", "''") & "%' "
                    End If
                End If

                If Me.txtNPWP.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " NPWP like '%" & Me.txtNPWP.Text.Replace("'", "''") & "%' "
                    Else
                        StrSearch = StrSearch & " and NPWP like '%" & Me.txtNPWP.Text.Replace("'", "''") & "%'"
                    End If
                End If

                If StrSearch = "" Then
                    StrSearch = StrSearch & " IsExistingCustomer = '0' "
                Else
                    StrSearch = StrSearch & " And IsExistingCustomer = '0' "
                End If

                Return StrSearch
            End If
        Catch
            Throw
            Return ""
        End Try
    End Function

    Private Function SearchFilterCustomer() As String
        Dim StrSearch As String = ""
        Try
            If isValidSearch() Then
                'If Me.TxtCIFNumber.Text <> "" Then
                '    If StrSearch = "" Then
                '        StrSearch = StrSearch & " CustomerInformationDetail_WebTempTable.CIFNo like '%" & Me.TxtCIFNumber.Text.Replace("'", "''") & "%'"
                '    Else
                '        StrSearch = StrSearch & " And CustomerInformationDetail_WebTempTable.CIFNo like '%" & Me.TxtCIFNumber.Text.Replace("'", "''") & "%' "
                '    End If
                'End If

                If Me.TxtCIFNumber.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " CIFNo like '%" & Me.TxtCIFNumber.Text.Replace("'", "''") & "%'"
                    Else
                        StrSearch = StrSearch & " And CIFNo like '%" & Me.TxtCIFNumber.Text.Replace("'", "''") & "%' "
                    End If
                End If

                If Me.TxtAccountNumber.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " AccountNo like '%" & Me.TxtAccountNumber.Text.Replace("'", "''") & "%'"
                    Else
                        StrSearch = StrSearch & " And AccountNo like '%" & Me.TxtAccountNumber.Text.Replace("'", "''") & "%' "
                    End If
                End If

                If Me.TxtFullName.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " CustomerName like '%" & Me.TxtFullName.Text.Replace("'", "''") & "%'"
                    Else
                        StrSearch = StrSearch & " And CustomerName like '%" & Me.TxtFullName.Text.Replace("'", "''") & "%' "
                    End If
                End If

                If Me.TxtDateOfBirth.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " DateOfBirth = '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", Me.TxtDateOfBirth.Text.Replace("'", "''")).ToString("yyyy-MM-dd") & "' "
                    Else
                        StrSearch = StrSearch & " And DateOfBirth = '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", Me.TxtDateOfBirth.Text.Replace("'", "''")).ToString("yyyy-MM-dd") & "' "
                    End If
                End If

                If Me.TxtBirthPlace.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " BirthPlace like '%" & Me.TxtBirthPlace.Text.Replace("'", "''") & "%'"
                    Else
                        StrSearch = StrSearch & " And BirthPlace like '%" & Me.TxtBirthPlace.Text.Replace("'", "''") & "%' "
                    End If
                End If

                If Me.TxtIDTypeCustomer.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " IdType like '%" & Me.TxtIDTypeCustomer.Text.Replace("'", "''") & "%' "
                    Else
                        StrSearch = StrSearch & " and IdType like '%" & Me.TxtIDTypeCustomer.Text.Replace("'", "''") & "%' "
                    End If
                End If

                If Me.TxtIDNo.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " IdNumber like '%" & Me.TxtIDNo.Text.Replace("'", "''") & "%' "
                    Else
                        StrSearch = StrSearch & " and IdNumber like '%" & Me.TxtIDNo.Text.Replace("'", "''") & "%' "
                    End If
                End If

                Return StrSearch
            End If
        Catch
            Throw
            Return ""
        End Try
    End Function

#End Region 'done

#Region "Excel Export Event"

    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim PkId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PkId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PkId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PkId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Private Sub BindSelected()
        Dim Rows As New ArrayList

        Dim SbPk As New StringBuilder
        For Each IdPk As Int64 In Me.SetnGetSelectedItem
            SbPk.Append(IdPk.ToString & ", ")
        Next
        If SbPk.ToString <> "" Then
            Dim rowData As TList(Of CustomerCTR) = DataRepository.CustomerCTRProvider.GetPaged("Pk_CustomerCTR_Id in (" & SbPk.ToString.Substring(0, SbPk.Length - 2) & ") ", "", 0, Sahassa.AML.Commonly.GetDisplayedTotalRow, 0)
            For Each rowList As CustomerCTR In rowData
                Rows.Add(rowList)
            Next
            SbPk = Nothing
        End If

        Me.GridDataView.DataSource = Rows
        Me.GridDataView.AllowPaging = False
        Me.GridDataView.DataBind()

        Me.GridDataView.Columns(0).Visible = False
        Me.GridDataView.Columns(10).Visible = False
        'Me.GridDataView.Columns(11).Visible = False
    End Sub

    Private Sub BindSelectedAll()
        Try

            Me.GridDataView.DataSource = DataRepository.CustomerCTRProvider.GetPaged(SetAndGetSearchingCriteria, Me.SetnGetSort, 0, Sahassa.AML.Commonly.SessionPagingLimit, 0)

            Me.GridDataView.AllowPaging = False
            Me.GridDataView.DataBind()

            Me.GridDataView.Columns(0).Visible = False
            Me.GridDataView.Columns(10).Visible = False
            Me.GridDataView.Columns(11).Visible = False
        Catch
            Throw
        End Try

    End Sub

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridDataView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim PkId As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(PkId) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(PkId) Then
        '                    ArrTarget.Add(PkId)
        '                End If
        '            Else
        '                ArrTarget.Remove(PkId)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(PkId) Then
        '                    ArrTarget.Add(PkId)
        '                End If
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
        Me.BindGrid()
    End Sub

    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim totalrow As Int16 = 0
        For Each gridRow As DataGridItem In Me.GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                totalrow = CType(totalrow + 1, Int16)
            End If
        Next
        If i = totalrow Then
            Me.CheckBoxSelectAll.Checked = True
        Else
            Me.CheckBoxSelectAll.Checked = False
        End If
    End Sub

#End Region

#Region "events..."

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                Me.ClearThisPageSessions()

                Me.popUpDateOfBirth.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtDateOfBirth.ClientID & "'), 'dd-mm-yyyy')")
                Me.popUpDateOfBirth.Style.Add("display", "")
                'Me.popUpTransactionDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTransactionDate.ClientID & "'), 'dd-mmm-yyyy')")
                'Me.popUpTransactionDate.Style.Add("display", "")

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using AccessAudit As New AMLBLL.AuditTrailBLL
                '    AccessAudit.InsertAuditTrailUserAccess(Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "Accesssing page " & Sahassa.CommonCTRWeb.Commonly.SessionCurrentPage & " succeeded")
                'End Using

                'Using ControlerGetUserAccess As New Sahassa.AML.BLL.AccessAuthorityBLL
                '    If ControlerGetUserAccess.BolIsUserHasAccess(Sahassa.AML.Commonly.SessionGroupId, "CustomerCTRView.aspx") = True Then
                '        Me.LinkButtonAddNew.Visible = True
                '    Else
                '        Me.LinkButtonAddNew.Visible = False
                '    End If
                'End Using
                TrCIFNumber.Visible = False
                TrAccountNumber.Visible = False
                TxtIDTypeCustomer.Visible = False
                CboIDType.Visible = True
                TrNPWP.Visible = True
                If Me.GetTanggalTransaksi <> "" Then
                    ImgBack.Visible = True
                Else
                    ImgBack.Visible = False
                End If

                TblData.Visible = False
                TblDataCustomer.Visible = False
                TblDataNotFound.Visible = False
                Me.GridDataView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                BindIdType()
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.CollectSelected()
            Me.BindGrid()
            SetCheckedAll()
            Me.SetInfoNavigate()
            Me.SetInfoNavigateCustomer()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ClearSearch()
        Me.IsSearch = False
        Me.SetAndGetSearchingCriteria = Nothing
        Me.SetAndGetSearchingCriteriaCustomer = Nothing
        Me.TxtFullName.Text = ""
        Me.TxtDateOfBirth.Text = ""
        Me.CboIDType.SelectedIndex = 0
        Me.TxtIDTypeCustomer.Text = ""
        Me.TxtIDNo.Text = ""
        Me.txtNPWP.Text = ""
        Me.TxtCIFNumber.Text = ""
        Me.TxtAccountNumber.Text = ""
        Me.TxtBirthPlace.Text = ""

        TblData.Visible = False
        TblDataCustomer.Visible = False
        TblDataNotFound.Visible = False
        MvCustomerCTR.ActiveViewIndex = -1
    End Sub
    Protected Sub BtnClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnClearSearch.Click
        Try
            Me.ClearSearch()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridDataView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridDataView.ItemDataBound
        Try


            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                e.Item.Cells(3).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow))


                If e.Item.Cells(2).Text <> "&nbsp;" Then
                    Using objLblIDType As Label = CType(e.Item.FindControl("LblIDType"), Label)
                        Using ObjIDType As MsIDType = AMLBLL.CustomerCTRBLL.GetMsIDTypeTypeByPK(CInt(e.Item.Cells(2).Text))
                            If Not IsNothing(ObjIDType) Then
                                objLblIDType.Text = ObjIDType.MsIDType_Name
                            End If
                        End Using
                    End Using
                End If


                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)


            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridDataView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridDataView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be less than or equal to the total page count.")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub GridDataView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridDataView.ItemCommand
        If e.CommandName.ToLower = "edit" Then
            Response.Redirect("CustomerCTREdit.aspx?PK_CustomerCTR_ID=" & e.Item.Cells(1).Text & "&TanggalTransaksi=" & Me.GetTanggalTransaksi & "&TipeTransaksi=" & Me.GetTipeTransaksi & "&NomorTiketTransaksi=" & Me.GetNomorTiketTransaksi)
        ElseIf e.CommandName.ToLower = "delete" Then
            Response.Redirect("CustomerCTRDelete.aspx?PK_CustomerCTR_ID=" & e.Item.Cells(1).Text & "&TanggalTransaksi=" & Me.GetTanggalTransaksi & "&TipeTransaksi=" & Me.GetTipeTransaksi & "&NomorTiketTransaksi=" & Me.GetNomorTiketTransaksi)
        End If
    End Sub


#End Region

    Protected Sub ImageButtonSearch_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            If isValidSearch() Then
                IsSearch = True
                If RblCustomerNonCustomer.SelectedValue = "1" Then
                    SetAndGetSearchingCriteria = SearchFilter()
                Else ' Customer
                    SetAndGetSearchingCriteriaCustomer = SearchFilterCustomer()
                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub BtnNewData_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles BtnNewData.Click
        Try
            Me.Response.Redirect("CustomerCTRAdd.aspx?TanggalTransaksi=" & Me.GetTanggalTransaksi & "&TipeTransaksi=" & Me.GetTipeTransaksi & "&NomorTiketTransaksi=" & Me.GetNomorTiketTransaksi, False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub RblCustomerNonCustomer_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles RblCustomerNonCustomer.SelectedIndexChanged
        Me.ClearSearch()
        If RblCustomerNonCustomer.SelectedValue = "1" Then
            TrCIFNumber.Visible = False
            TrAccountNumber.Visible = False
            TxtIDTypeCustomer.Visible = False
            CboIDType.Visible = True
            TrNPWP.Visible = True
            MvCustomerCTR.ActiveViewIndex = 0
        Else
            TrCIFNumber.Visible = True
            TrAccountNumber.Visible = True
            TxtIDTypeCustomer.Visible = True
            CboIDType.Visible = False
            TrNPWP.Visible = False
            MvCustomerCTR.ActiveViewIndex = 1
        End If
    End Sub

    Protected Sub GridDataViewCustomer_SortCommand(source As Object, e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridDataViewCustomer.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSortCustomer = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSortCustomer
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridDataViewCustomer_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridDataViewCustomer.ItemDataBound

        If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

            e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPageCustomer * Sahassa.AML.Commonly.GetDisplayedTotalRow))

        End If
    End Sub

    Protected Sub GridDataViewCustomer_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridDataViewCustomer.ItemCommand
        If e.CommandName.ToLower = "transaction" Then
            Response.Redirect("CustomerCTRViewCustomer.aspx?CIFNo=" & e.Item.Cells(1).Text & "&TanggalTransaksi=" & Me.GetTanggalTransaksi & "&TipeTransaksi=" & Me.GetTipeTransaksi & "&NomorTiketTransaksi=" & Me.GetNomorTiketTransaksi)
        End If
    End Sub

    Protected Sub ImageButtonGoCustomer_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGoCustomer.Click
        Try
            If IsNumeric(Me.TextGoToPageCustomer.Text) Then
                If (CInt(Me.TextGoToPageCustomer.Text) > 0) Then
                    If (CInt(Me.TextGoToPageCustomer.Text) <= CInt(PageTotalPagesCustomer.Text)) Then
                        Me.SetnGetCurrentPageCustomer = Int32.Parse(Me.TextGoToPageCustomer.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be less than or equal to the total page count.")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Sub PageNavigateCustomer(ByVal sender As Object, ByVal e As CommandEventArgs) Handles LinkButtonLastCustomer.Command, LinkButtonNextCustomer.Command, LinkButtonPreviousCustomer.Command, LinkButtonFirstCustomer.Command
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPageCustomer = 0
                Case "Prev" : Me.SetnGetCurrentPageCustomer -= 1
                Case "Next" : Me.SetnGetCurrentPageCustomer += 1
                Case "Last" : Me.SetnGetCurrentPageCustomer = Me.GetPageTotalCustomer - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LnkExportToExcel_Click(sender As Object, e As System.EventArgs) Handles LnkExportToExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CustomerCTR.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridDataView)
            GridDataView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgBack_Click(sender As Object, e As ImageClickEventArgs) Handles ImgBack.Click
        Try
            Response.Redirect("CustomerTicketNumberView.aspx", False)

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class