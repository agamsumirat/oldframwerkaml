<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="IFTI_Detail_Swift_Incoming.aspx.vb" Inherits="IFTI_Detail_Swift_Incoming"
    Title="Untitled Page" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script src="Script/popcalendar.js"> </script>

    <script language="javascript" type="text/javascript">
     function hidePanel(objhide, objpanel, imgmin, imgmax) {
         document.getElementById(objhide).style.display = 'none';
         document.getElementById(objpanel).src = imgmax;
     }
     // JScript File


     function popWinNegara() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerNegara.aspx", "#3", winSetting);
     }

     function popWinProvinsi() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerProvinsi.aspx", "#5", winSetting);
     }
     function popWinPekerjaan() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerPekerjaitemtan.aspx", "#6", winSetting);
     }
     function popWinKotaKab() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerKotaKab.aspx", "#7", winSetting);
     }
     function popWinBidangUsaha() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerBidangUsaha.aspx", "#8", winSetting);
     }
     function popWinMataUang() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerMataUang.aspx", "#4", winSetting);

     }

    </script>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td width="99%" bgcolor="#FFFFFF" style="height: 20px">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td bgcolor="#FFFFFF" style="height: 20px">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="10" height="100%" /></td>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="a" runat="server">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="There were uncompleteness on the page:"
                                        Width="95%" CssClass="validation"></asp:ValidationSummary>
                                </ajax:AjaxPanel>
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <asp:MultiView ID="MultiViewEditSwiftIn" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="ViewEdit" runat="server">
                                            <asp:MultiView ID="MultiViewNasabahTypeAll" runat="server" ActiveViewIndex="0">
                                                <asp:View ID="ViewSwiftIn" runat="server">
                                                    <table id="Table1" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                                        border-bottom-style: none" width="100%">
                                                        <tr>
                                                            <td bgcolor="#ffffff" style="font-size: 18px; border-top-style: none; border-right-style: none;
                                                                border-left-style: none; border-bottom-style: none">
                                                                <img src="Images/dot_title.gif" width="17" height="17" />
                                                                <strong>
                                                                    <asp:Label ID="Labelo5" runat="server" Text="IFTI Swift Incoming - Detail"></asp:Label>
                                                                </strong>
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#ffffff" style="font-size: 18px; border-top-style: none; border-right-style: none;
                                                                border-left-style: none; border-bottom-style: none">
                                                                &nbsp;<ajax:AjaxPanel ID="AjaxPanel3" runat="server"><asp:Label ID="LblSucces" CssClass="validationok"
                                                                    Width="94%" runat="server" Visible="False"></asp:Label>
                                                                </ajax:AjaxPanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                        bgcolor="#dddddd" border="2">
                                                        <tr>
                                                            <td background="Images/search-bar-background.gif" valign="middle" align="left" style="height: 6px;
                                                                width: 100%;">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Labelsi25" runat="server" Text="A. Umum" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <a href="#" onclick="javascript:ShowHidePanel('SwiftIN_Umum','Img13','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                title="click to minimize or maximize">
                                                                                <img id="Img13" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="SwiftIN_Umum">
                                                            <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;">
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                    <tr>
                                                                        <td style="width: 257px; background-color: #fff7e6; height: 22px;">
                                                                            <asp:Label ID="Labele27" runat="server" Text="a. No. LTDLN "></asp:Label></td>
                                                                        <td style="height: 22px">
                                                                            <asp:TextBox ID="SwiftInUmum_LTDN" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 257px; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelk50" runat="server" Text="b. No. LTDLN Koreksi " Width="132px"></asp:Label></td>
                                                                        <td>
                                                                            <asp:TextBox ID="SwiftInUmum_LtdnKoreksi" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 257px; background-color: #fff7e6">
                                                                            <asp:Label ID="Label66" runat="server" Text="c. Tanggal Laporan "></asp:Label><asp:Label
                                                                                ID="Labeltang67" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                        <td>
                                                                            <asp:TextBox ID="SwiftInUmum_TanggalLaporan" runat="server" CssClass="searcheditbox"
                                                                                TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 257px; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelna68" runat="server" Text="d. Nama PJK Bank Pelapor " Width="162px"></asp:Label>
                                                                            <asp:Label ID="Labelr72" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                        <td>
                                                                            <asp:TextBox ID="SwiftInUmum_NamaPJKBank" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="100" TabIndex="2" Width="125px" Height="22px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 257px; background-color: #fff7e6; height: 22px;">
                                                                            <asp:Label ID="Labelnama73" runat="server" Text="e. Nama Pejabat PJK Bank Pelapor"
                                                                                Width="204px"></asp:Label>
                                                                            <asp:Label ID="Labelnama74" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                        <td style="height: 22px">
                                                                            <asp:TextBox ID="SwiftInUmum_NamaPejabatPJKBank" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="100" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 257px; background-color: #fff7e6">
                                                                            <asp:Label ID="Labeljen75" runat="server" Text="f. Jenis Laporan"></asp:Label>
                                                                            <asp:Label ID="Labeljen76" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                        <td>
                                                                            <asp:RadioButtonList ID="RbSwiftInUmum_JenisLaporan" runat="server" RepeatColumns="2"
                                                                                RepeatDirection="Horizontal" Enabled="False">
                                                                                <asp:ListItem Value="1">Baru</asp:ListItem>
                                                                                <asp:ListItem Value="2">Recall</asp:ListItem>
                                                                                <asp:ListItem Value="3">Koreksi</asp:ListItem>
                                                                                <asp:ListItem Value="4">Reject</asp:ListItem>
                                                                            </asp:RadioButtonList></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 257px">
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                        bgcolor="#dddddd" border="2">
                                                        <tr>
                                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                style="height: 6px">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Label5" runat="server" Text="B. Identitas Pengirim" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <a href="#" onclick="javascript:ShowHidePanel('BSwiftInIdentitasPengirim','Imgr13','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                title="click to minimize or maximize">
                                                                                <img id="Imgr13" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="BSwiftInIdentitasPengirim">
                                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                </table>
                                                                <table style="width: 725px; height: 60px">
                                                                    <tr id="trSwiftInPengirim_rekeningtext" runat="server">
                                                                        <td style="width: 33%; background-color: #fff7e6; height: 20px;">
                                                                            <asp:Label ID="Label25" runat="server" Text="No. Rek " Width="84px"></asp:Label>
                                                                            <asp:Label ID="Label27" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                        <td style="width: 124px; height: 20px;">
                                                                            <asp:TextBox ID="txtSwiftInPengirim_rekening" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 53px; height: 20px;">
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="tr1" runat="server">
                                                                        <td style="width: 33%; background-color: #fff7e6; height: 34px;">
                                                                            <asp:Label ID="Label28" runat="server" Text="       "></asp:Label>
                                                                            <asp:Label ID="Labelr49" runat="server" Text="       Tipe Pengirim"></asp:Label>
                                                                            <asp:Label ID="Label50" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 124px; height: 34px;">
                                                                            <ajax:AjaxPanel ID="AjaxPanelS14" runat="server" Width="93px">
                                                                                &nbsp;<asp:RadioButtonList ID="rbrbSwiftIn_TipePengirim" runat="server" RepeatDirection="Horizontal"
                                                                                    AutoPostBack="True" Width="201px" Enabled="False">
                                                                                    <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                                    <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                                </asp:RadioButtonList></ajax:AjaxPanel></td>
                                                                        <td style="width: 53px; height: 34px;">
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="trSwiftInTipeNasabah" runat="server" visible="False">
                                                                        <td style="width: 33%; height: 34px; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelsw51" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                                            <asp:Label ID="Labelsw53" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 124px; height: 34px">
                                                                            <ajax:AjaxPanel ID="AjaxPanelsw7" runat="server">
                                                                                <asp:RadioButtonList ID="rbSwiftIn_TipeNasabah" runat="server" AutoPostBack="True"
                                                                                    RepeatDirection="Horizontal" Enabled="False">
                                                                                    <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                                    <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </ajax:AjaxPanel>
                                                                        </td>
                                                                        <td style="width: 53px; height: 34px">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <asp:MultiView ID="MultiViewSwiftInIdenPengirim" runat="server">
                                                                    <asp:View ID="ViewSwiftInIdenPengirimNasabah" runat="server">
                                                                        <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                                            bgcolor="#dddddd" border="2">
                                                                            <tr>
                                                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                                    style="height: 6px">
                                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                                        <tr>
                                                                                            <td class="formtext">
                                                                                                <asp:Label ID="Labelnas54" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <a href="#" onclick="javascript:ShowHidePanel('BSwiftInIdentitasPengirim1nasabah','Imgs9','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                                    title="click to minimize or maximize">
                                                                                                    <img id="Imgs9" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="BSwiftInIdentitasPengirim1nasabah">
                                                                                <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                                    <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                        border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                    </table>
                                                                                    <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                        border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                        <tr>
                                                                                            <td style="width: 100%">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 610px">
                                                                                                <asp:MultiView ID="MultiViewSwiftInNasabahType" runat="server" ActiveViewIndex="0">
                                                                                                    <asp:View ID="ViewPengirimNasabah_IND" runat="server">
                                                                                                        <table style="width: 98%; height: 49px">
                                                                                                            <tr>
                                                                                                                <td style="width: 48%; height: 15px; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Labelind56" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="height: 15px; width: 222px;">
                                                                                                                </td>
                                                                                                                <td style="height: 15px; width: 36px;">
                                                                                                                </td>
                                                                                                                <td style="width: 47px; height: 15px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 48%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label65" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                                    <asp:Label ID="Labelna66" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                <td style="width: 222px">
                                                                                                                    <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_nama" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="255" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                <td style="width: 36px">
                                                                                                                </td>
                                                                                                                <td style="width: 47px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 48%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label67" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label></td>
                                                                                                                <td style="width: 222px">
                                                                                                                    <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_TanggalLahir" runat="server" CssClass="searcheditbox"
                                                                                                                        TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                <td style="width: 36px">
                                                                                                                </td>
                                                                                                                <td style="width: 47px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 48%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label68" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                                        Width="223px"></asp:Label></td>
                                                                                                                <td style="width: 222px">
                                                                                                                    <asp:RadioButtonList ID="RbSwiftInPengirimNasabah_IND_Warganegara" runat="server"
                                                                                                                        RepeatDirection="Horizontal" AutoPostBack="True" Enabled="False">
                                                                                                                        <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                                        <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                                    </asp:RadioButtonList></td>
                                                                                                                <td style="width: 36px">
                                                                                                                </td>
                                                                                                                <td style="width: 47px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr id="negaraPengirimInd" runat="server" visible="false">
                                                                                                                <td style="width: 48%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label72" runat="server" Text="Negara "></asp:Label>
                                                                                                                    (Wajib diisi apabila kewarganegaraan WNA)</td>
                                                                                                                <td style="width: 222px">
                                                                                                                    <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negara" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                    <asp:HiddenField ID="hfSwiftInPengirimNasabah_IND_negara" runat="server" />
                                                                                                                </td>
                                                                                                                <td style="width: 36px">
                                                                                                                </td>
                                                                                                                <td style="width: 47px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr id="negaraLainPengirimInd" runat="server" visible="false">
                                                                                                                <td style="width: 48%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label75" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                                <td style="width: 222px">
                                                                                                                    <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraLain" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                <td style="width: 36px">
                                                                                                                </td>
                                                                                                                <td style="width: 47px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 48%; background-color: #fff7e6; height: 22px;">
                                                                                                                    <asp:Label ID="Label76" runat="server" Text="d. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                                                <td style="width: 222px; height: 22px;">
                                                                                                                    &nbsp;</td>
                                                                                                                <td style="width: 36px; height: 22px;">
                                                                                                                </td>
                                                                                                                <td style="width: 47px; height: 22px;">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 48%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Labelal77" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                                <td style="width: 222px">
                                                                                                                    <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_alamatIden" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                                <td style="width: 36px">
                                                                                                                </td>
                                                                                                                <td style="width: 47px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 48%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Labelkota79" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                                                                <td style="width: 222px">
                                                                                                                    <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraBagian" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td style="width: 36px">
                                                                                                                </td>
                                                                                                                <td style="width: 47px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 48%; height: 18px; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label9318" runat="server" Text="Negara"></asp:Label>
                                                                                                                    <asp:Label ID="Label73" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                    <td style="width: 222px; height: 18px">
                                                                                                                        <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraIden" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                        <asp:HiddenField ID="hfSwiftInPengirimNasabah_IND_negaraIden" runat="server" />
                                                                                                                    </td>
                                                                                                                    <td style="width: 36px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                    </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 48%; background-color: #fff7e6; height: 18px;">
                                                                                                                    <asp:Label ID="Labellai90" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                                <td style="height: 18px; width: 222px;">
                                                                                                                    <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraLainIden" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                </td>
                                                                                                                <td style="width: 47px; height: 18px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 48%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Labelf93" runat="server" Text="e. No Telp"></asp:Label></td>
                                                                                                                <td style="width: 222px">
                                                                                                                    <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_NoTelp" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td style="width: 36px">
                                                                                                                    &nbsp;</td>
                                                                                                                <td style="width: 47px">
                                                                                                                    &nbsp;</td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 48%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Labelq4e10" runat="server" Text="f. Jenis Dokument Identitas"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 222px">
                                                                                                                    <asp:DropDownList ID="cbo_SwiftInpengirimNas_Ind_jenisidentitas" runat="server" Enabled="False">
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                                <td style="width: 36px">
                                                                                                                    &nbsp;</td>
                                                                                                                <td style="width: 47px">
                                                                                                                    &nbsp;</td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 48%; background-color: #fff7e6; height: 15px;">
                                                                                                                    <asp:Label ID="Labelq4e11" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="height: 15px; width: 222px;">
                                                                                                                    <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_NomorIdentitas" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td style="width: 36px; height: 15px;">
                                                                                                                </td>
                                                                                                                <td style="width: 47px; height: 15px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </asp:View>
                                                                                                    <asp:View ID="ViewPengirimNasabah_Korp" runat="server">
                                                                                                        <table style="width: 288px; height: 31px">
                                                                                                            <tr>
                                                                                                                <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Labelko137" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="height: 15px; color: #000000; width: 240px;">
                                                                                                                </td>
                                                                                                                <td style="width: 240px; color: #000000; height: 15px">
                                                                                                                </td>
                                                                                                                <td style="height: 15px; width: 2977px; color: #000000;">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Labelnam139" runat="server" Text="a. Nama Korporasi" Width="110px"></asp:Label>
                                                                                                                    <asp:Label ID="Labelko226" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 240px">
                                                                                                                    <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_namaKorp" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="255" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                <td style="width: 240px">
                                                                                                                </td>
                                                                                                                <td style="width: 2977px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Labelb140" runat="server" Text="b. Alamat Lengkap"></asp:Label></td>
                                                                                                                <td style="width: 240px">
                                                                                                                </td>
                                                                                                                <td style="width: 240px">
                                                                                                                </td>
                                                                                                                <td style="width: 2977px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Labela142" runat="server" Text="Alamat"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 222px">
                                                                                                                    <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_AlamatKorp" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                                <td style="width: 36px">
                                                                                                                </td>
                                                                                                                <td style="width: 47px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Labelne143" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                                                                <td style="width: 222px">
                                                                                                                    <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_Kota" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td style="width: 36px">
                                                                                                                </td>
                                                                                                                <td style="width: 47px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Label144" runat="server" Text="Negara"></asp:Label>
                                                                                                                    <asp:Label ID="Labelko138" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 222px; height: 18px">
                                                                                                                    <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NegaraKorp" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                    <asp:HiddenField ID="hfSwiftInPengirimNasabah_Korp_NegaraKorp" runat="server" />
                                                                                                                </td>
                                                                                                                <td style="width: 36px">
                                                                                                                </td>
                                                                                                                <td style="width: 47px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Label145" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                                <td style="height: 18px; width: 222px;">
                                                                                                                    <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorp" runat="server"
                                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                </td>
                                                                                                                <td style="width: 47px; height: 18px">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Label161" runat="server" Text="c. No Telp"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="height: 18px; width: 222px;">
                                                                                                                    <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NoTelp" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    &nbsp;</td>
                                                                                                                <td style="width: 47px; height: 18px">
                                                                                                                    &nbsp;</td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </asp:View>
                                                                                                </asp:MultiView></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:View>
                                                                    <asp:View ID="ViewSwiftInIdenPengirimNonNasabah" runat="server">
                                                                        <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                                            bgcolor="#dddddd" border="2">
                                                                            <tr>
                                                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                                    style="height: 6px">
                                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                                        <tr>
                                                                                            <td class="formtext">
                                                                                                <asp:Label ID="Label179" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <a href="#" onclick="javascript:ShowHidePanel('BSwiftInIdentitasPengirimNonNasabah','Imgs10','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                                    title="click to minimize or maximize">
                                                                                                    <img id="Imgs10" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="BSwiftInIdentitasPengirimNonNasabah">
                                                                                <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                                    <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                        border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                    </table>
                                                                                    <table style="width: 98%; height: 49px">
                                                                                        <tr>
                                                                                            <td style="width: 30%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Label180" runat="server" Text="a. No. Rek "></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 204px">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNonNasabah_rekening" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <td style="width: 35px">
                                                                                            </td>
                                                                                            <td style="width: 47px">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 30%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Label181" runat="server" Text="b. Nama Bank"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 204px">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNonNasabah_namabank" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                            <td style="width: 35px">
                                                                                                &nbsp;</td>
                                                                                            <td style="width: 47px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 30%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Label182" runat="server" Text="c. Nama Lengkap"></asp:Label>
                                                                                                <asp:Label ID="Label183" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="width: 204px">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNonNasabah_Nama" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="255" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <td style="width: 35px">
                                                                                            </td>
                                                                                            <td style="width: 47px">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 30%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Label184" runat="server" Text="d. TanggalLahir" Height="14px" Width="242px"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 204px">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNonNasabah_TanggalLahir" runat="server" CssClass="searcheditbox"
                                                                                                    TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                            </td>
                                                                                            <td style="width: 35px">
                                                                                            </td>
                                                                                            <td style="width: 47px">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 30%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labelt1150" runat="server" Text="e. Alamat"></asp:Label></td>
                                                                                            <td style="width: 204px">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNonNasabah_Alamat" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="100" TabIndex="2" Width="421px" Enabled="False"></asp:TextBox></td>
                                                                                            <td style="width: 35px">
                                                                                            </td>
                                                                                            <td style="width: 47px">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 30%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labelkl16" runat="server" Text="Negara Bagian/ Kota"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 204px">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNonNasabah_Kota" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                            <td style="width: 35px">
                                                                                                &nbsp;</td>
                                                                                            <td style="width: 47px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 30%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labelr211" runat="server" Text="Negara"></asp:Label>
                                                                                                <asp:Label ID="Labelko227" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 204px">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNonNasabah_Negara" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNonNasabah_Negara" runat="server" />
                                                                                            </td>
                                                                                            <td style="width: 35px">
                                                                                            </td>
                                                                                            <td style="width: 47px">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 30%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Label212" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                            <td style="width: 204px">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNonNasabah_negaraLain" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <td style="width: 35px">
                                                                                            </td>
                                                                                            <td style="width: 47px">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 30%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Label9274" runat="server" Text="f. No Telp"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 204px">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNonNasabah_NoTelp" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                            <td style="width: 35px">
                                                                                                &nbsp;</td>
                                                                                            <td style="width: 47px">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:View>
                                                                </asp:MultiView></td>
                                                        </tr>
                                                    </table>
                                                    <table id="Table2" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                                        width="99%" bgcolor="#dddddd" border="2">
                                                        <tr>
                                                            <td background="Images/search-bar-background.gif" valign="middle" align="left" style="height: 11px;
                                                                width: 100%;">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Label49" runat="server" Text="C.  Identitas Penerima" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <a href="#" onclick="javascript:ShowHidePanel('C1SwiftInidentitasPENERIMA','Img1E4','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                title="click to minimize or maximize">
                                                                                <img id="Img1E4" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="C1SwiftInidentitasPENERIMA">
                                                            <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;">
                                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="0" width="100%" bgcolor="#dddddd"
                                                                    border="2">
                                                                    <tr>
                                                                        <td bgcolor="#ffffff" background="images/testbg.gif">
                                                                            <asp:DataGrid ID="GridDataView" runat="server" Font-Size="XX-Small" CellSpacing="1"
                                                                                CellPadding="4" SkinID="gridview" AllowPaging="True" Width="100%" GridLines="None"
                                                                                AllowSorting="True" ForeColor="#333333" Font-Bold="False" Font-Italic="False"
                                                                                Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left"
                                                                                CssClass="gridview">
                                                                                <Columns>
                                                                                    <asp:TemplateColumn>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                                                                        </ItemTemplate>
                                                                                        <HeaderStyle Width="2%" />
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="PK_IFTI_Beneficiary_ID" Visible="False">
                                                                                        <HeaderStyle Width="0%" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn HeaderText="FK_IFTI_ID" Visible="False" DataField="FK_IFTI_ID"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="FK_IFTI_NasabahType_ID" HeaderText="FK_IFTI_NasabahType_ID"
                                                                                        Visible="False"></asp:BoundColumn>
                                                                                    <asp:TemplateColumn HeaderText="No.">
                                                                                        <HeaderStyle ForeColor="White" />
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_NoRekening" HeaderText="No Rekening (Perorangan)"
                                                                                        SortExpression="Beneficiary_Nasabah_INDV_NoRekening desc">
                                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" />
                                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_NamaBank" HeaderText="Nama Bank (Perorangan)"
                                                                                        SortExpression="Beneficiary_Nasabah_INDV_NamaBank desc"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_NamaLengkap" HeaderText="Nama Lengkap (Perorangan)"
                                                                                        SortExpression="Beneficiary_Nasabah_INDV_NamaLengkap desc"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_TanggalLahir" HeaderText="Tanggal Lahir (Perorangan)"
                                                                                        SortExpression="Beneficiary_Nasabah_INDV_TanggalLahir desc" DataFormatString="{0:dd-MMM-yyyy}">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_NoRekening" HeaderText="No Rekening (Korporasi)"
                                                                                        SortExpression="Beneficiary_Nasabah_CORP_NoRekening desc">
                                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" />
                                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_NamaKorporasi" HeaderText="Nama Korporasi (Korporasi)"
                                                                                        SortExpression="Beneficiary_Nasabah_CORP_NamaKorporasi desc"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_AlamatLengkap" HeaderText="Alamat Lengkap (Korporasi)"
                                                                                        SortExpression="Beneficiary_Nasabah_CORP_AlamatLengkap desc">
                                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" Wrap="False" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_NoTelp" HeaderText="No Telp (Korporasi)"
                                                                                        SortExpression="Beneficiary_Nasabah_CORP_NoTelp desc"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_NoRekening" HeaderText="No Rekening (Non Nasabah)"
                                                                                        SortExpression="Beneficiary_NonNasabah_NoRekening desc">
                                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" Wrap="False" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_KodeRahasia" HeaderText="Kode Rahasia (Non Nasabah)"
                                                                                        SortExpression="Beneficiary_NonNasabah_KodeRahasia desc">
                                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" Wrap="False" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_NamaBank" HeaderText="Nama Bank (Non Nasabah)"
                                                                                        SortExpression="Beneficiary_NonNasabah_NamaBank desc">
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_NamaLengkap" HeaderText="Nama Lengkap (Non Nasabah)"
                                                                                        SortExpression="Beneficiary_NonNasabah_NamaLengkap desc">
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_TanggalLahir" HeaderText="Tanggal Lahir (Non Nasabah)"
                                                                                        SortExpression="Beneficiary_NonNasabah_TanggalLahir desc" DataFormatString="{0:dd-MMM-yyyy}">
                                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" Wrap="False" />
                                                                                    </asp:BoundColumn>                                                                                    
                                                                                    <asp:TemplateColumn>
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="btnDetail" runat="server" CommandName="edit">Detail</asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                </Columns>
                                                                                <PagerStyle Visible="False" HorizontalAlign="Center" ForeColor="White" BackColor="#666666">
                                                                                </PagerStyle>
                                                                                <EditItemStyle BackColor="#7C6F57" />
                                                                            </asp:DataGrid>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table style="width: 100%">
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                                            border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                                            &nbsp;<asp:Label ID="Label5s1" runat="server" Text="Tipe PJK Bank" Width="96px"></asp:Label></td>
                                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                                            border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                                            <ajax:AjaxPanel ID="AjaxPanelSwiftIN4" runat="server" Width="100%">
                                                                                <asp:DropDownList ID="cboSwiftInPenerima_TipePJKBank" runat="server" AutoPostBack="True" Enabled="False">
                                                                                    <asp:ListItem Value="0">-Select-</asp:ListItem>
                                                                                    <asp:ListItem Value="1">Penyelenggara Penerima Akhir</asp:ListItem>
                                                                                    <asp:ListItem Value="2">Penyelenggara Penerus</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </ajax:AjaxPanel>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="trSwiftInTipepengirim" runat="server" visible="false">
                                                                        <td style="width: 25%">
                                                                        </td>
                                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                                            border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                                            <asp:Label ID="Labell53" runat="server" Text="       Tipe Penerima"></asp:Label></td>
                                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                                            border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                                            <ajax:AjaxPanel ID="AjaxPanelw7" runat="server">
                                                                                <asp:RadioButtonList ID="RbSwiftInPenerima_TipePenerima" runat="server"
                                                                                    RepeatDirection="Horizontal" AutoPostBack="True" Width="189px" Enabled="False">
                                                                                    <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                                    <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </ajax:AjaxPanel>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="trSwiftInPenerimaTipeNasabah" runat="server" visible="false">
                                                                        <td style="width: 25%">
                                                                        </td>
                                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                                            border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                                            <asp:Label ID="Label54" runat="server" Text="Tipe Nasabah "></asp:Label></td>
                                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                                            border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                                            <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                                                                <asp:RadioButtonList ID="RbSwiftInPenerima_TipeNasabah" runat="server"
                                                                                    RepeatDirection="Horizontal" AutoPostBack="True" Enabled="False">
                                                                                    <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                                    <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </ajax:AjaxPanel>
                                                                        </td>
                                                                        <td style="width: 25%">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <ajax:AjaxPanel ID="AjaxPanel9" runat="server" Width="100%">
                                                                    <asp:MultiView ID="MultiViewSwiftInPenerima" runat="server">
                                                                        <asp:View ID="ViewSwiftInPenerimaAkhir" runat="server">
                                                                            <asp:MultiView ID="MultiViewSwiftInPenAkhirJenis" runat="server">
                                                                                <asp:View ID="ViewSwiftInPenerimaNasabah" runat="server">
                                                                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                                                        bgcolor="#dddddd" border="2" id="TABLE3">
                                                                                        <tr>
                                                                                            <td>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                                                style="height: 6px">
                                                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                                                    <tr>
                                                                                                        <td class="formtext" style="height: 14px">
                                                                                                            <asp:Label ID="Label56" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="height: 14px">
                                                                                                            <a href="#" onclick="javascript:ShowHidePanel('B1sWIFTiNidentitasPENerima1nasabah','Img1e5','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                                                title="click to minimize or maximize">
                                                                                                                <img id="Img1e5" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr id="B1sWIFTiNidentitasPENerima1nasabah">
                                                                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                                    <tr>
                                                                                                        <td style="width: 100%">
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td style="width: 59%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="Label77" runat="server" Text="No. Rek " Width="84px"></asp:Label>
                                                                                                                        <asp:Label ID="Label79" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                    <td style="width: 124px">
                                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_Rekening" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td style="width: 53px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 610px">
                                                                                                            <asp:MultiView ID="MultiViewSwiftINPenerimaAkhirNasabah" runat="server" ActiveViewIndex="0">
                                                                                                                <asp:View ID="VwSwiftIn_PEN_IND" runat="server">
                                                                                                                    <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                                                                                                        <table style="width: 98%; height: 49px">
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; height: 15px; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Label80" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="height: 15px; width: 222px;">
                                                                                                                                </td>
                                                                                                                                <td style="height: 15px; width: 35px;">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px; height: 15px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Label93" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                                                    <asp:Label ID="Labeler102" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                <td style="width: 222px">
                                                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_nama" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="255" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="width: 35px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labeler123" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label>
                                                                                                                                    <asp:Label ID="Labeler137" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                <td style="width: 222px">
                                                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_TanggalLahir" runat="server" CssClass="searcheditbox"
                                                                                                                                        TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                <td style="width: 35px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labeler138" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                                                        Width="223px"></asp:Label></td>
                                                                                                                                <td style="width: 222px">
                                                                                                                                    <asp:RadioButtonList ID="RbSwiftInPenerimaNasabah_IND_Kewarganegaraan" runat="server"
                                                                                                                                        RepeatDirection="Horizontal" AutoPostBack="True" Enabled="False">
                                                                                                                                        <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                                                        <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                                                    </asp:RadioButtonList></td>
                                                                                                                                <td style="width: 35px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr id="negaraPenerimaInd" runat="server" visible="false">
                                                                                                                                <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labeler139" runat="server" Text="Negara "></asp:Label>
                                                                                                                                    <br />
                                                                                                                                    <asp:Label ID="Labeler142" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                                                        Width="258px"></asp:Label></td>
                                                                                                                                <td style="width: 222px">
                                                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_negara" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                    <asp:HiddenField ID="hfSwiftInPenerimaNasabah_IND_negara" runat="server" />
                                                                                                                                </td>
                                                                                                                                <td style="width: 35px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr id="negaraLainPenerimaInd" runat="server" visible="false">
                                                                                                                                <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labell143" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                                                <td style="width: 222px">
                                                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_negaralain" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="width: 35px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelql144" runat="server" Text="d. Pekerjaan"></asp:Label>
                                                                                                                                    <asp:Label ID="Labelql145" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                <td style="width: 222px">
                                                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_pekerjaan" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                    <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_pekerjaan" runat="server" />
                                                                                                                                </td>
                                                                                                                                <td style="width: 35px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelql45" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                                                                                <td style="width: 222px">
                                                                                                                                    <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="width: 35px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelql37" runat="server" Text="e. Alamat Domisili"></asp:Label></td>
                                                                                                                                <td style="width: 222px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 35px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6; height: 20px;">
                                                                                                                                    <asp:Label ID="Labelql38" runat="server" Text="Alamat"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 222px; height: 20px;">
                                                                                                                                    <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_Alamat" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="width: 35px; height: 20px;">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px; height: 20px;">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelql39" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                                                                                <td style="height: 18px; width: 222px;">
                                                                                                                                    <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_Kota" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                    <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_Kota_dom" runat="server" />
                                                                                                                                </td>
                                                                                                                                <td style="height: 18px; width: 35px;">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px; height: 18px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labellq57" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                                                <td style="width: 222px; height: 18px">
                                                                                                                                    <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_kotalain" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="width: 35px; height: 18px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px; height: 18px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6; height: 66px;">
                                                                                                                                    <asp:Label ID="Labelql41" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                                                                <td style="width: 222px; height: 66px;">
                                                                                                                                    <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_provinsi" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                    <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_provinsi_dom" runat="server" />
                                                                                                                                </td>
                                                                                                                                <td style="height: 66px">
                                                                                                                                    &nbsp;</td>
                                                                                                                                <td style="width: 47px; height: 66px;">
                                                                                                                                    &nbsp;</td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelql186" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                                                <td style="width: 222px">
                                                                                                                                    <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_provinsiLain" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="width: 35px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6; height: 22px;">
                                                                                                                                    <asp:Label ID="Labelql42" runat="server" Text="f. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                                                                <td style="width: 222px; height: 22px;">
                                                                                                                                    <%-- <ajax:AjaxPanel ID="AjaxPanelCheckPengirimNas_Ind_alamat" runat="server">--%>
                                                                                                                                    &nbsp;<%--</ajax:AjaxPanel>--%></td>
                                                                                                                                <td style="width: 35px; height: 22px;">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px; height: 22px;">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelql43" runat="server" Text="Alamat"></asp:Label>
                                                                                                                                    <asp:Label ID="Labelql267" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                <td style="width: 222px">
                                                                                                                                    <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitas" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="width: 35px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelqo44" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                                                                    <asp:Label ID="Labelqo268" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                <td style="width: 222px">
                                                                                                                                    <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                    <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas" runat="server" />
                                                                                                                                </td>
                                                                                                                                <td style="width: 35px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labeqel83" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                                                <td style="width: 222px; height: 18px">
                                                                                                                                    <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="width: 35px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6; height: 18px;">
                                                                                                                                    <asp:Label ID="Labeqle46" runat="server" Text="Provinsi"></asp:Label>
                                                                                                                                    <asp:Label ID="Labelqe269" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                <td style="height: 18px; width: 222px;">
                                                                                                                                    <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                    <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden" runat="server" />
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    &nbsp;</td>
                                                                                                                                <td style="width: 47px; height: 18px">
                                                                                                                                    &nbsp;</td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; height: 8px; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelqe62" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                                                <td style="width: 222px; height: 8px">
                                                                                                                                    <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Style="height: 22px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="width: 35px; height: 8px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px; height: 8px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelsa9307" runat="server" Text="g. No Telp"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="width: 222px; height: 18px">
                                                                                                                                    <asp:TextBox ID="TxtISwiftIndenPenerimaNas_Ind_noTelp" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td style="width: 35px; height: 18px">
                                                                                                                                    &nbsp;</td>
                                                                                                                                <td style="width: 47px; height: 18px">
                                                                                                                                    &nbsp;</td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                    <asp:Label ID="Labelq4e8" runat="server" Text="h. Jenis Dokument Identitas"></asp:Label>
                                                                                                                                    <asp:Label ID="Labelq27e0" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                <td style="width: 222px">
                                                                                                                                    <asp:DropDownList ID="cbo_SwiftInpenerimaNas_Ind_jenisidentitas" runat="server" Enabled="False">
                                                                                                                                    </asp:DropDownList></td>
                                                                                                                                <td style="width: 35px">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6; height: 41px;">
                                                                                                                                    <asp:Label ID="Labelq4e9" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                                                                    <asp:Label ID="LabelIqe71" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                                <td style="height: 41px; width: 222px;">
                                                                                                                                    <asp:TextBox ID="TxtISwiftIndenPenerimaNas_Ind_noIdentitas" runat="server" CssClass="searcheditbox"
                                                                                                                                        MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="width: 35px; height: 41px;">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px; height: 41px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6; height: 18px;">
                                                                                                                                    <asp:Label ID="Label9270" runat="server" Text="i. Nilai Transaksi Keuangan (dalam Rupiah)"></asp:Label>
                                                                                                                                </td>
                                                                                                                                <td style="height: 18px; width: 222px;">
                                                                                                                                    <asp:TextBox ID="txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan" runat="server"
                                                                                                                                        CssClass="searcheditbox" MaxLength="15" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td style="width: 35px; height: 18px;">
                                                                                                                                    &nbsp;</td>
                                                                                                                                <td style="width: 47px; height: 18px">
                                                                                                                                    &nbsp;</td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td style="width: 50%; background-color: #fff7e6; height: 15px;">
                                                                                                                                </td>
                                                                                                                                <td style="height: 15px; width: 222px;">
                                                                                                                                    &nbsp;
                                                                                                                                    <asp:ImageButton ID="ImageButton_CancelPenerimaInd" runat="server" ImageUrl="~/Images/button/Ok.gif"
                                                                                                                                        /></td>
                                                                                                                                <td style="width: 35px; height: 15px;">
                                                                                                                                </td>
                                                                                                                                <td style="width: 47px; height: 15px">
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </ajax:AjaxPanel>
                                                                                                                </asp:View>
                                                                                                                <asp:View ID="VwSwiftIn_PEN_Corp" runat="server">
                                                                                                                    <table style="width: 288px; height: 31px">
                                                                                                                        <tr>
                                                                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                                <asp:Label ID="Labele223" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="height: 15px; color: #000000; width: 240px;">
                                                                                                                            </td>
                                                                                                                            <td style="width: 240px; color: #000000; height: 15px">
                                                                                                                            </td>
                                                                                                                            <td style="height: 15px; width: 2977px; color: #000000;">
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr style="color: #000000">
                                                                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                                <asp:Label ID="Labeel225" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label></td>
                                                                                                                            <td style="width: 240px">
                                                                                                                                <asp:DropDownList ID="cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha" runat="server" Enabled="False">
                                                                                                                                </asp:DropDownList>
                                                                                                                            </td>
                                                                                                                            <td style="width: 240px">
                                                                                                                            </td>
                                                                                                                            <td style="width: 2977px">
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                                <asp:Label ID="Labelt1151" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="167px"></asp:Label></td>
                                                                                                                            <td style="width: 240px">
                                                                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya" runat="server"
                                                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                            <td style="width: 240px">
                                                                                                                            </td>
                                                                                                                            <td style="width: 2977px">
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                                <asp:Label ID="Labelt227" runat="server" Text="b. Nama Korporasi" Width="110px"></asp:Label>
                                                                                                                                <asp:Label ID="Labelt149" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                            <td style="width: 240px">
                                                                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_namaKorp" runat="server" CssClass="searcheditbox"
                                                                                                                                    MaxLength="255" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                            <td style="width: 240px">
                                                                                                                            </td>
                                                                                                                            <td style="width: 2977px">
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                                <asp:Label ID="Labele228" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label>
                                                                                                                                <asp:Label ID="Labely08" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="width: 240px">
                                                                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorp" runat="server" CssClass="searcheditbox"
                                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp" runat="server" />
                                                                                                                            </td>
                                                                                                                            <td style="width: 240px">
                                                                                                                            </td>
                                                                                                                            <td style="width: 2977px">
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                                <asp:Label ID="Label9316" runat="server" Text="Bidang Usaha Korporasi Lainnya"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="width: 240px">
                                                                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya" runat="server"
                                                                                                                                    CssClass="textbox" Enabled="False"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td style="width: 240px">
                                                                                                                                &nbsp;</td>
                                                                                                                            <td style="width: 2977px">
                                                                                                                                &nbsp;</td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                                <asp:Label ID="Labelqw229" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="width: 240px">
                                                                                                                            </td>
                                                                                                                            <td style="width: 240px">
                                                                                                                            </td>
                                                                                                                            <td style="width: 2977px">
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                                <asp:Label ID="Labele1" runat="server" Text="Alamat"></asp:Label>
                                                                                                                                <asp:Label ID="Labelko228" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="width: 222px; height: 20px;">
                                                                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_AlamatKorp" runat="server" CssClass="searcheditbox"
                                                                                                                                    MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td style="width: 35px; height: 20px;">
                                                                                                                            </td>
                                                                                                                            <td style="width: 47px; height: 20px;">
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                <asp:Label ID="Labelsd2" runat="server" Text="Kota / Kabupaten "></asp:Label>
                                                                                                                                <asp:Label ID="Labelko229" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="height: 18px; width: 222px;">
                                                                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_Kotakab" runat="server" CssClass="searcheditbox"
                                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_kotakab" runat="server" />
                                                                                                                            </td>
                                                                                                                            <td style="height: 18px; width: 35px;">
                                                                                                                            </td>
                                                                                                                            <td style="width: 47px; height: 18px">
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                <asp:Label ID="Labelwe3" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="width: 222px; height: 18px">
                                                                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_kotaLain" runat="server" CssClass="searcheditbox"
                                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td style="width: 35px; height: 18px">
                                                                                                                            </td>
                                                                                                                            <td style="width: 47px; height: 18px">
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                <asp:Label ID="Labewl4" runat="server" Text="Provinsi"></asp:Label>
                                                                                                                                <asp:Label ID="Labelko230" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="width: 222px">
                                                                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_propinsi" runat="server" CssClass="searcheditbox"
                                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                                <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_propinsi" runat="server" />
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                &nbsp;</td>
                                                                                                                            <td style="width: 47px">
                                                                                                                                &nbsp;</td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                <asp:Label ID="Label6" runat="server" Text="Provinsi Lainnya"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="width: 222px">
                                                                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_propinsilain" runat="server" CssClass="searcheditbox"
                                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td style="width: 35px">
                                                                                                                            </td>
                                                                                                                            <td style="width: 47px">
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                                                                <asp:Label ID="Labelw0" runat="server" Text="e. No. Telp"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="width: 222px">
                                                                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_NoTelp" runat="server" CssClass="searcheditbox"
                                                                                                                                    MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td style="width: 35px">
                                                                                                                                &nbsp;</td>
                                                                                                                            <td style="width: 47px">
                                                                                                                                &nbsp;</td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                                                                <asp:Label ID="Label9271" runat="server" Text="f. Nilai Transaksi Keuangan (dalam Rupiah )"></asp:Label>
                                                                                                                            </td>
                                                                                                                            <td style="height: 18px; width: 222px;">
                                                                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan" runat="server"
                                                                                                                                    CssClass="searcheditbox" MaxLength="15" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                &nbsp;</td>
                                                                                                                            <td style="width: 47px; height: 18px">
                                                                                                                                &nbsp;</td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                                                            </td>
                                                                                                                            <td style="width: 222px; height: 18px">
                                                                                                                                &nbsp;
                                                                                                                                <asp:ImageButton ID="ImageButton_CancelPenerimaKorp" runat="server" ImageUrl="~/Images/button/Ok.gif"
                                                                                                                                     /></td>
                                                                                                                            <td>
                                                                                                                            </td>
                                                                                                                            <td style="width: 47px; height: 18px">
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </asp:View>
                                                                                                            </asp:MultiView></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:View>
                                                                                <asp:View ID="ViewSwiftInPenerimaNonNasabah" runat="server">
                                                                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                                        bgcolor="#dddddd" border="2">
                                                                                        <tr>
                                                                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                                                style="height: 27px">
                                                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                                                    <tr>
                                                                                                        <td class="formtext" style="height: 14px">
                                                                                                            <asp:Label ID="LabelSwiftIn4" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="height: 14px">
                                                                                                            <a href="#" onclick="javascript:ShowHidePanel('B1SwiftInidentitasPENerimaonNasabah','Imgt23','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                                                title="click to minimize or maximize">
                                                                                                                <img id="Imgt23" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr id="B1SwiftInidentitasPENerimaonNasabah">
                                                                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                                </table>
                                                                                                <table style="width: 100%; height: 49px">
                                                                                                    <tr>
                                                                                                        <td style="width: 20%; background-color: #fff7e6">
                                                                                                            <asp:Label ID="Labelqw81" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                            <asp:Label ID="Labelqw85" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                        <td style="width: 35%">
                                                                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_nama" runat="server" CssClass="searcheditbox"
                                                                                                                MaxLength="255" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 20%; background-color: #fff7e6">
                                                                                                            <asp:Label ID="LabelSwiftwIn82" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)"
                                                                                                                Width="183px"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="width: 35%">
                                                                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_TanggalLahir" runat="server" CssClass="searcheditbox"
                                                                                                                TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        <td style="width: 5%">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 20%; background-color: #fff7e6">
                                                                                                            <asp:Label ID="LabelSwiftIwn95" runat="server" Text="c. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                                        <td style="width: 35%">
                                                                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_alamatiden" runat="server" CssClass="searcheditbox"
                                                                                                                MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td style="width: 5%">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 20%; background-color: #fff7e6">
                                                                                                            <asp:Label ID="LabelSwiftIwn84" runat="server" Text="d. No Telepon"></asp:Label></td>
                                                                                                        <td style="width: 35%;">
                                                                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_NoTelepon" runat="server" CssClass="searcheditbox"
                                                                                                                MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td style="width: 5%;">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 20%; background-color: #fff7e6; height: 24px;">
                                                                                                            <asp:Label ID="LabelSwiftInw100" runat="server" Text="e. Jenis Dokument Identitas"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="width: 35%; height: 24px;">
                                                                                                            <asp:DropDownList ID="CboSwiftInPenerimaNonNasabah_JenisDokumen" runat="server" Enabled="False">
                                                                                                            </asp:DropDownList></td>
                                                                                                        <td style="width: 5%; height: 24px;">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 20%; background-color: #fff7e6;">
                                                                                                            <asp:Label ID="LabelSwiftIn101" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="width: 35%;">
                                                                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_NomorIden" runat="server" CssClass="searcheditbox"
                                                                                                                MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                        <td style="width: 5%;">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 20%; background-color: #fff7e6;">
                                                                                                            <asp:Label ID="Label9311" runat="server" Text="f. Nilai Transaksi Keuangan (dalam Rupiah )"></asp:Label>
                                                                                                        </td>
                                                                                                        <td style="width: 35%;">
                                                                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan" runat="server"
                                                                                                                CssClass="searcheditbox" MaxLength="15" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        </td>
                                                                                                        <td style="width: 5%;">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 20%; background-color: #fff7e6">
                                                                                                        </td>
                                                                                                        <td style="width: 35%">
                                                                                                            &nbsp;
                                                                                                            <asp:ImageButton ID="ImageButton_CancelPenerimaNonNasabah" runat="server" ImageUrl="~/Images/button/Ok.gif"
                                                                                                                 /></td>
                                                                                                        <td style="width: 5%">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:View>
                                                                            </asp:MultiView></asp:View>
                                                                        <asp:View ID="ViewSwiftInPenerimaPenerus" runat="server">
                                                                            <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                                                bgcolor="#dddddd" border="2" id="TableSwiftInPenerimaPenerus">
                                                                                <tr>
                                                                                    <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                                        style="height: 6px">
                                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp;</td>
                                                                                                <td>
                                                                                                    <a href="#" onclick="javascript:ShowHidePanel('B2SwiftInidentitaspenerima','Imge24','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                                        title="click to minimize or maximize">
                                                                                                        <img id="Imge24" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="B2SwiftInidentitaspenerima">
                                                                                    <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                                        <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                        </table>
                                                                                        <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                            <tr>
                                                                                                <td style="width: 100%">
                                                                                                    <table style="width: 725px; height: 60px">
                                                                                                        <tr>
                                                                                                            <td style="width: 33%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="LabelSwiftIn70" runat="server" Text="No. Rek " Width="84px"></asp:Label>
                                                                                                                <asp:Label ID="LabelSwiftIn71" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <td style="width: 124px">
                                                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_Rekening" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td style="width: 53px">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 610px">
                                                                                                    <asp:MultiView ID="MultiViewSwiftInPenerimaPenerus" runat="server" ActiveViewIndex="0">
                                                                                                        <asp:View ID="ViewSwiftInPenerimaPenerus_IND" runat="server">
                                                                                                            <table style="width: 98%; height: 49px">
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; height: 15px; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="Labelw91" runat="server" Text="Perorangan " Font-Bold="true"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="height: 15px; width: 222px;">
                                                                                                                    </td>
                                                                                                                    <td style="height: 15px; width: 36px;">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px; height: 15px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn9268" runat="server" Text="a. Nama Bank"></asp:Label>
                                                                                                                        <asp:Label ID="LabelSwiftIn92" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                    <td style="width: 222px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_Ind_namaBank" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td style="width: 36px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn9267" runat="server" Text="b. Nama Lengkap"></asp:Label>
                                                                                                                        <asp:Label ID="LabelSwiftIn9263" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 222px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_Ind_nama" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="255" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td style="width: 36px">
                                                                                                                        &nbsp;</td>
                                                                                                                    <td style="width: 47px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6; height: 20px;">
                                                                                                                        <asp:Label ID="LabelSwiftIn94" runat="server" Text="c. Tanggal lahir (tgl/bln/thn)"
                                                                                                                            Width="183px"></asp:Label></td>
                                                                                                                    <td style="width: 222px; height: 20px;">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_Ind_TanggalLahir" runat="server" CssClass="searcheditbox"
                                                                                                                            TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                                                        <input id="popUpSwiftInTangglaLahirPengirimPenerus_Ind" runat="server" onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')"
                                                                                                                            style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                                                                                            background-image: url(Script/Calendar/cal.gif); border-left: #ffffff 0px solid;
                                                                                                                            width: 16px; border-bottom: #ffffff 0px solid; height: 17px" title="Click to show calendar"
                                                                                                                            type="button" /></td>
                                                                                                                    <td style="width: 36px; height: 20px;">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px; height: 20px;">
                                                                                                                        <asp:Label ID="LabelSwiftIn103" runat="server" Text="Label" Visible="False" Width="128px"></asp:Label></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn104" runat="server" Text="d. Kewarganegaraan (Pilih salah satu) "
                                                                                                                            Width="223px"></asp:Label></td>
                                                                                                                    <td style="width: 222px">
                                                                                                                        <asp:RadioButtonList ID="RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan" runat="server"
                                                                                                                            RepeatDirection="Horizontal" AutoPostBack="True" Enabled="False">
                                                                                                                            <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                                            <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                                        </asp:RadioButtonList></td>
                                                                                                                    <td style="width: 36px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr id="negaraPenerimaPenerusInd" runat="server" visible="false">
                                                                                                                    <td style="width: 48%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn105" runat="server" Text="e. Negara "></asp:Label>
                                                                                                                        <br />
                                                                                                                        <asp:Label ID="LabelSwiftIn106" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                                            Width="258px"></asp:Label></td>
                                                                                                                    <td style="width: 222px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_negara" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                        <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_negara" runat="server" />
                                                                                                                    </td>
                                                                                                                    <td style="width: 36px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr id="negaraLainPenerimaPenerusInd" runat="server" visible="false">
                                                                                                                    <td style="width: 48%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn107" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                                    <td style="width: 222px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_negaralain" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td style="width: 36px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn108" runat="server" Text="f. Pekerjaan"></asp:Label></td>
                                                                                                                    <td style="width: 222px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_pekerjaan" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                        <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_pekerjaan" runat="server" />
                                                                                                                    </td>
                                                                                                                    <td style="width: 36px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn109" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                                                                    <td style="width: 222px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_pekerjaanLain" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td style="width: 36px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn110" runat="server" Text="g. Alamat Domisili"></asp:Label></td>
                                                                                                                    <td style="width: 222px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 36px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn111" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                                    <td style="width: 222px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_alamatDom" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td style="width: 36px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; height: 18px; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn112" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                                                                    <td style="height: 18px; width: 222px;">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_kotaDom" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                        <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_kotaDom" runat="server" />
                                                                                                                    </td>
                                                                                                                    <td style="height: 18px; width: 36px;">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px; height: 18px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; height: 18px; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn113" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                                    <td style="width: 222px; height: 18px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_kotaLainDom" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td style="width: 36px; height: 18px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px; height: 18px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn114" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                                                    <td style="width: 222px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_ProvDom" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                        <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_ProvDom" runat="server" />
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        &nbsp;</td>
                                                                                                                    <td style="width: 47px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn175" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                                    <td style="width: 222px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_ProvLainDom" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td style="width: 36px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6; height: 22px;">
                                                                                                                        <asp:Label ID="LabelSwiftIn115" runat="server" Text="h. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                                                    <td style="width: 222px; height: 22px;">
                                                                                                                        </td>
                                                                                                                    <td style="width: 36px; height: 22px;">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px; height: 22px;">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn116" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                                    <td style="width: 222px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_alamatIden" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td style="width: 36px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn117" runat="server" Text="Kota / Kabupaten"></asp:Label></td>
                                                                                                                    <td style="width: 222px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_kotaIden" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                        <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_kotaIden" runat="server" />
                                                                                                                    </td>
                                                                                                                    <td style="width: 36px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; height: 18px; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn118" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                                    <td style="width: 222px; height: 18px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_KotaLainIden" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td style="width: 36px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6; height: 18px;">
                                                                                                                        <asp:Label ID="Labelv119" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                                                    <td style="height: 18px; width: 222px;">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_ProvIden" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                        <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_ProvIden" runat="server" />
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        &nbsp;</td>
                                                                                                                    <td style="width: 47px; height: 18px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; height: 18px; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn120" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                                    <td style="width: 222px; height: 18px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_ProvLainIden" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td style="width: 36px; height: 18px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px; height: 18px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; height: 18px; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="Label9312" runat="server" Text="i. No. Telp"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 222px; height: 18px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_NoTelp" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td style="width: 36px; height: 18px">
                                                                                                                        &nbsp;</td>
                                                                                                                    <td style="width: 47px; height: 18px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn121" runat="server" Text="j. Jenis Dokument Identitas"></asp:Label></td>
                                                                                                                    <td style="width: 222px">
                                                                                                                        <asp:DropDownList ID="CBoSwiftInPenerimaPenerus_IND_jenisIdentitas" runat="server" Enabled="False">
                                                                                                                        </asp:DropDownList></td>
                                                                                                                    <td style="width: 36px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6; height: 18px;">
                                                                                                                        <asp:Label ID="LabelSwiftIn122" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                                                                    <td style="height: 18px; width: 222px;">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_nomoridentitas" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td style="width: 36px; height: 18px;">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px; height: 18px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="Label9313" runat="server" Text="k. Nilai Transaksi Keuangan (dalam Rupiah )"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 222px">
                                                                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="15" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td style="width: 36px">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 48%; background-color: #fff7e6; height: 15px;">
                                                                                                                    </td>
                                                                                                                    <td style="height: 15px; width: 222px;">
                                                                                                                        &nbsp;
                                                                                                                        <asp:ImageButton ID="ImageButton_CancelPenerimaPenerusInd" runat="server" ImageUrl="~/Images/button/Ok.gif"
                                                                                                                             /></td>
                                                                                                                    <td style="width: 36px; height: 15px;">
                                                                                                                    </td>
                                                                                                                    <td style="width: 47px; height: 15px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </asp:View>
                                                                                                        <asp:View ID="ViewSwiftInPenerimaPenerus_Korp" runat="server">
                                                                                                            <table style="width: 288px; height: 31px">
                                                                                                                <tr>
                                                                                                                    <td style="width: 60%; height: 15px; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn124" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="height: 15px; color: #000000; width: 240px;">
                                                                                                                    </td>
                                                                                                                    <td style="color: #000000; height: 15px">
                                                                                                                    </td>
                                                                                                                    <td style="height: 15px; width: 2977px; color: #000000;">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr style="color: #000000">
                                                                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn9264" runat="server" Text="a. Nama Bank"></asp:Label>
                                                                                                                        <asp:Label ID="LabelSwiftIn9265" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 240px">
                                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_namabank" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        &nbsp;</td>
                                                                                                                    <td style="width: 2977px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                                <tr style="color: #000000">
                                                                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn126" runat="server" Text="b. Bentuk Badan Usaha "></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 240px">
                                                                                                                        <asp:DropDownList ID="cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha" runat="server" Enabled="False">
                                                                                                                        </asp:DropDownList>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                    </td>
                                                                                                                    <td style="width: 2977px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn127" runat="server" Text="Bentuk Badan Usaha Lainnya "
                                                                                                                            Width="257px"></asp:Label></td>
                                                                                                                    <td style="width: 240px">
                                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLain" runat="server"
                                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td>
                                                                                                                    </td>
                                                                                                                    <td style="width: 2977px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn128" runat="server" Text="c. Nama Korporasi" Width="119px"></asp:Label>
                                                                                                                        <asp:Label ID="LabelSwiftIn9266" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 240px">
                                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_NamaKorp" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="255" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td>
                                                                                                                    </td>
                                                                                                                    <td style="width: 2977px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn129" runat="server" Text="d. Bidang Usaha Korporasi"></asp:Label></td>
                                                                                                                    <td style="width: 240px">
                                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorp" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                        <asp:HiddenField ID="hfSwiftInPenerimaPenerus_Korp_BidangUsahaKorp" runat="server" />
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                    </td>
                                                                                                                    <td style="width: 2977px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="Label9317" runat="server" Text="Bidang Usaha Korporasi Lainnya"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <asp:TextBox ID="TextBox2" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                                        TabIndex="2" Width="125px"></asp:TextBox><td style="width: 240px">
                                                                                                                            <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorpLainnya" runat="server"
                                                                                                                                CssClass="textbox" Enabled="False"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    <td>
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn130" runat="server" Text="e. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                                                    <td style="width: 240px">
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                    </td>
                                                                                                                    <td style="width: 2977px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn131" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                                    <td style="width: 240px">
                                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_alamatkorp" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="100" TabIndex="2" Width="342px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td>
                                                                                                                    </td>
                                                                                                                    <td style="width: 2977px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn132" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                                                                    <td style="width: 240px">
                                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_kotakorp" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                        <asp:HiddenField ID="hfSwiftInPenerimaPenerus_Korp_kotakorp" runat="server" />
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                    </td>
                                                                                                                    <td style="width: 2977px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 60%; background-color: #fff7e6; height: 20px;">
                                                                                                                        <asp:Label ID="LabelSwiftIn133" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                                    <td style="width: 240px; height: 20px;">
                                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_kotalainnyakorp" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td style="height: 20px;">
                                                                                                                    </td>
                                                                                                                    <td style="width: 2977px; height: 20px;">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn134" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                                                    <td style="width: 240px">
                                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_ProvinsiKorp" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                        <asp:HiddenField ID="hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp" runat="server" />
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        &nbsp;</td>
                                                                                                                    <td style="width: 2977px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn135" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                                    <td style="width: 240px">
                                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp" runat="server"
                                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td>
                                                                                                                    </td>
                                                                                                                    <td style="width: 2977px; height: 15px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="LabelSwiftIn141" runat="server" Text="f. No Telp"></asp:Label></td>
                                                                                                                    <td style="width: 240px">
                                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_NoTelp" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                    </td>
                                                                                                                    <td style="width: 2977px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                                                                        <asp:Label ID="Label9314" runat="server" Text="g. Nilai Transaksi Keuangan (dalam Rupiah )"></asp:Label>
                                                                                                                    </td>
                                                                                                                    <td style="width: 240px">
                                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi" runat="server" CssClass="searcheditbox"
                                                                                                                            MaxLength="15" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td>
                                                                                                                    </td>
                                                                                                                    <td style="width: 2977px">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                                                                        &nbsp;</td>
                                                                                                                    <td style="width: 240px">
                                                                                                                        &nbsp;&nbsp;
                                                                                                                        <asp:ImageButton ID="ImageButton_CancelPenerimaPenerusKorp" runat="server" ImageUrl="~/Images/button/Ok.gif"
                                                                                                                             /></td>
                                                                                                                    <td>
                                                                                                                        &nbsp;</td>
                                                                                                                    <td style="width: 2977px">
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </asp:View>
                                                                                                    </asp:MultiView></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:View>
                                                                    </asp:MultiView>
                                                                </ajax:AjaxPanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table align="center" bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="2"
                                                        cellspacing="1" width="99%">
                                                        <tr>
                                                            <td align="left" background="Images/search-bar-background.gif" style="height: 6px;
                                                                width: 100%;" valign="middle">
                                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Label9275" runat="server" Font-Bold="True" Text="E. Transaksi"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <a href="#" onclick="javascript:ShowHidePanel('ESwiftIntransaksi','Imgrr25','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                title="click to minimize or maximize">
                                                                                <img id="Imgrr25" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" />
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <asp:Label ID="Label9276" runat="server" Font-Italic="True" Text="(Diisi oleh PJK Bank  yang bertindak sebagai Penyelenggara Pengirim Asal atau sebagai Penyelenggara Penerus)"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr id="ESwiftIntransaksi">
                                                            <td bgcolor="#ffffff" style="height: 125px; width: 100%;" valign="top">
                                                                <table border="0" cellpadding="2" cellspacing="1" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none"
                                                                    width="100%">
                                                                </table>
                                                                <table style="width: 100%; height: 49px">
                                                                    <tr>
                                                                        <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                                            <asp:Label ID="Label9277" runat="server" Text="a. Tanggal Transaksi (tgl/bln/thn) "></asp:Label>
                                                                            <asp:Label ID="Label9278" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftIntanggal0" runat="server" CssClass="searcheditbox"
                                                                                TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                            <asp:Label ID="Label9279" runat="server" Text="b. Waktu Transaksi diproses (Time Indication) "></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftInwaktutransaksi0" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="20" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                            <asp:Label ID="Label9280" runat="server" Text="c. Referensi Pengirim" Width="129px"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftInSender0" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="10" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                            <asp:Label ID="Label9284" runat="server" Text="d. Kode Operasi Bank"></asp:Label>
                                                                            <asp:Label ID="Label9285" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftInBankOperationCode0" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                            <asp:Label ID="Label9286" runat="server" Text="e.  Kode Instruksi"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftInInstructionCode0" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                            <asp:Label ID="Label9287" runat="server" Text="f. Kantor Cabang Penyelenggara Pengirim Asal"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftInkantorCabangPengirim0" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                            <asp:Label ID="Label9289" runat="server" Text="g. Kode Tipe Transaksi "></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftInkodeTipeTransaksi0" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                            <asp:Label ID="Label9290" runat="server" Text=" h.  Value Date/Currency/Interbank Settled Amount"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                            <asp:Label ID="Label9291" runat="server" Text="Tanggal Transaksi (Value Date)"></asp:Label>
                                                                            <asp:Label ID="Label9292" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftInValueTanggalTransaksi" runat="server" CssClass="searcheditbox"
                                                                                TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                                            <asp:Label ID="Label9293" runat="server" Text="Nilai Transaksi (Amount of The EFT)"></asp:Label>
                                                                            <asp:Label ID="Label9315" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftInnilaitransaksi" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="15" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                                            <asp:Label ID="Label9294" runat="server" Text="Mata Uang Transaksi (currency of the EFT)"></asp:Label>
                                                                            <asp:Label ID="Label9295" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftInMataUangTransaksi0" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                            <asp:HiddenField ID="hfTransaksi_SwiftInMataUangTransaksi0" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; height: 20px; background-color: #fff7e6">
                                                                            <asp:Label ID="Label1" runat="server" Text="Mata Uang Transaksi Lainnya"></asp:Label></td>
                                                                        <td style="height: 20px">
                                                                            <asp:TextBox ID="Transaksi_SwiftInMataUangTransaksi0Lainnya" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                            <asp:Label ID="Label9296" runat="server" Text="Nilai Transaksi Keuangan yang diterima dalam rupiah"></asp:Label>
                                                                            <asp:Label ID="Labelko231" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftInAmountdalamRupiah0" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                            <asp:Label ID="Label9297" runat="server" Text="i. Currency/Instructed Amount"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                            <asp:Label ID="Label9298" runat="server" Text="Mata Uang yang diinstruksikan"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftIncurrency0" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                            <asp:HiddenField ID="hfTransaksi_SwiftIncurrency" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                                            <asp:Label ID="Label2" runat="server" Text="Mata Uang Lainnya"></asp:Label></td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftIncurrencyLainnya" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                            <asp:Label ID="Label9df299" runat="server" Text="Nilai Transaksi Keuangan yang diinstruksikan"></asp:Label></td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftIninstructedAmount0" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="15" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                                            <asp:Label ID="Label9300" runat="server" Text="j. Nilai Tukar (Exchange Rate) "></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftInnilaiTukar0" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="15" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                            <asp:Label ID="Label93w1" runat="server" Text="k. Sending Instituion"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftInsendingInstitution0" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                            <asp:Label ID="Label9we304" runat="server" Text="l. Tujuan Transaksi "></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="Transaksi_SwiftInTujuanTransaksi0" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="100" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 19px;">
                                                                            <asp:Label ID="Labelwe9305" runat="server" Text="m. Sumber Penggunaan Dana "></asp:Label>
                                                                        </td>
                                                                        <td style="height: 19px">
                                                                            <asp:TextBox ID="Transaksi_SwiftInSumberPenggunaanDana0" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="100" TabIndex="2" Width="296px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                        bgcolor="#dddddd" border="2">
                                                        <tr>
                                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                style="height: 6px">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Labelwe14" runat="server" Text="F. Informasi Lainnya" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <a href="#" onclick="javascript:ShowHidePanel('FSwiftInInformasiLainnya','Imgw12','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                title="click to minimize or maximize">
                                                                                <img id="Imgw12" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <asp:Label ID="Label230" runat="server" Font-Italic="True" Text="(Diisi oleh PJK Bank  yang bertindak sebagai Penyelenggara Pengirim Asal atau sebagai Penyelenggara Penerus)"></asp:Label></td>
                                                        </tr>
                                                        <tr id="FSwiftInInformasiLainnya">
                                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                </table>
                                                                <table style="width: 98%; height: 49px">
                                                                    <tr>
                                                                        <td style="width: 48%; height: 15px; background-color: #fff7e6">
                                                                            <asp:Label ID="Labeler233" runat="server" Text="a. Information about the sender's
    correspondent"></asp:Label>
                                                                        </td>
                                                                        <td style="height: 15px; width: 222px; color: #000000;">
                                                                            <asp:TextBox ID="InformasiLainnya_SwiftInSender" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="140" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="height: 15px; width: 36px; color: #000000;">
                                                                        </td>
                                                                        <td style="width: 47px; height: 15px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 48%; background-color: #fff7e6; height: 20px;">
                                                                            <asp:Label ID="Label249" runat="server" Text="b. Information about the receiver's correspondent "></asp:Label></td>
                                                                        <td style="width: 222px; height: 20px;">
                                                                            <asp:TextBox ID="InformasiLainnya_SwiftInreceiver" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="140" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 36px; height: 20px;">
                                                                        </td>
                                                                        <td style="width: 47px; height: 20px;">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelr253" runat="server" Text="c. Information about the third reimbursement
    institution" Width="369px"></asp:Label></td>
                                                                        <td style="width: 222px">
                                                                            <asp:TextBox ID="InformasiLainnya_SwiftInthirdReimbursement" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="140" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 36px">
                                                                        </td>
                                                                        <td style="width: 47px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelr254" runat="server" Text="d. Information about the intermediary institution "
                                                                                Width="290px"></asp:Label></td>
                                                                        <td style="width: 222px">
                                                                            <asp:TextBox ID="InformasiLainnya_SwiftInintermediary" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="140" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 36px">
                                                                        </td>
                                                                        <td style="width: 47px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 48%; background-color: #fff7e6;">
                                                                            <asp:Label ID="Labelr256" runat="server" Text="e.   Remittance Information"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 222px;">
                                                                            <asp:TextBox ID="InformasiLainnya_SwiftInRemittance" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="140" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 36px;">
                                                                        </td>
                                                                        <td style="width: 47px;">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                            <asp:Label ID="Label257" runat="server" Text="f. Sender to receiver information"></asp:Label></td>
                                                                        <td style="width: 222px">
                                                                            <asp:TextBox ID="InformasiLainnya_SwiftInSenderToReceiver" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="210" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 36px">
                                                                        </td>
                                                                        <td style="width: 47px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 48%; background-color: #fff7e6; height: 20px;">
                                                                            <asp:Label ID="Labelr258" runat="server" Text="g. Regulatory Reporting"></asp:Label></td>
                                                                        <td style="width: 222px; height: 20px;">
                                                                            <asp:TextBox ID="InformasiLainnya_SwiftInRegulatoryReport" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="105" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 36px; height: 20px;">
                                                                        </td>
                                                                        <td style="width: 47px; height: 20px;">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 48%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelr259" runat="server" Text="h. Envelope contents"></asp:Label></td>
                                                                        <td style="width: 222px">
                                                                            <asp:TextBox ID="InformasiLainnya_SwiftInEnvelopeContents" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="100" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 36px">
                                                                        </td>
                                                                        <td style="width: 47px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 48%; background-color: #fff7e6; height: 15px;">
                                                                        </td>
                                                                        <td style="height: 15px; width: 222px;">
                                                                        </td>
                                                                        <td style="width: 36px; height: 15px;">
                                                                        </td>
                                                                        <td style="width: 47px; height: 15px">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                            </asp:MultiView>
                                        </asp:View>
                                        <asp:View ID="vwMessage" runat="server">
                                            <table width="100%">
                                                <tr>
                                                    <td class="formtext" align="center" style="height: 15px">
                                                        <asp:Label runat="server" ID="lblMsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="formtext" align="center">
                                                        <asp:ImageButton ID="imgOKMsg" runat="server"  ImageUrl="~/Images/button/Ok.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </asp:MultiView>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator><ajax:AjaxPanel
                        ID="AjaxPanel2" runat="server"><table border="0" cellpadding="0" cellspacing="0"
                            width="100%">
                            <tr>
                                <td align="left" background="Images/button-bground.gif" valign="middle">
                                    <img height="1" src="Images/blank.gif" width="5" /></td>
                                <td align="left" background="Images/button-bground.gif" valign="middle">
                                    <img height="15" src="images/arrow.gif" width="15" />
                                </td>
                                <td background="Images/button-bground.gif" style="width: 5px">
                                </td>
                                
                                <td background="Images/button-bground.gif" style="width: 62px">
                                    <asp:ImageButton ID="ImageButtonCancel" runat="server" CausesValidation="False" ImageUrl="~/Images/button/back.gif"
                                         />
                                </td>
                                <td background="Images/button-bground.gif" width="99%">
                                    <img height="1" src="Images/blank.gif" width="1" /></td>
                            </tr>
                    </ajax:AjaxPanel>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
