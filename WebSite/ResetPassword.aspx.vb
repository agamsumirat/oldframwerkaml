Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper
Imports AMLBLL
Partial Class ResetPassword
    Inherits Parent



    Public ReadOnly Property SaltUser() As String
        Get
            Return Session("ResetPassword.SaltUser")
        End Get
        
    End Property

    Function IsDataValid() As Boolean
        If HUserID.Value = "" Then
            Throw New Exception("Click Browse Button To Choose UserID")
        End If
        Return True
    End Function


    ''' <summary>
    ''' save button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Page.Validate()
        Try

            If Page.IsValid AndAlso IsDataValid() Then

                Try
                    Dim MinimumPasswordLength As Int32 = Me.GetMinimumPasswordLength
                    If Me.TextPassword.Text.Length < MinimumPasswordLength Then
                        Throw New Exception("Cannot reset password for the following User : " & LblUserID.Text & " because the minimum password length is " & MinimumPasswordLength & " characters")
                    Else
                        Dim PasswordRecycleCount As Int32 = 3

                        LoginParameterBLL.CekPasswordChar(Me.TextPassword.Text.Trim)
                        LoginParameterBLL.CekPasswordCombination(Me.TextPassword.Text.Trim)
                        'Encrypt password baru
                        Dim salt As String = Me.SaltUser
                        Dim NewPassword As String = Sahassa.AML.Commonly.Encrypt(Me.TextPassword.Text, salt)

                        'Periksa apakah password itu melanggar aturan Password Recycle Count atau tidak
                        Using AccessLoginParameter As New AMLDAL.AMLDataSetTableAdapters.LoginParameterTableAdapter
                            Using LoginParamterTable As AMLDAL.AMLDataSet.LoginParameterDataTable = AccessLoginParameter.GetData
                                If LoginParamterTable.Rows.Count <> 0 Then
                                    Dim LoginParameterRow As AMLDAL.AMLDataSet.LoginParameterRow = LoginParamterTable.Rows(0)
                                    PasswordRecycleCount = LoginParameterRow.PasswordRecycleCount
                                End If
                            End Using
                        End Using

                        If Me.CheckIfPasswordIsOK(LblUserID.Text.Trim, NewPassword, PasswordRecycleCount) Then
                            'Using TranScope As New Transactions.TransactionScope
                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(1)
                            ' masukkan ke audittrail 
                            Dim oldPassword As String
                            Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessUser, Data.IsolationLevel.ReadUncommitted)

                                Using dt As Data.DataTable = AccessUser.GetDataByUserID(CInt(HUserID.Value))
                                    Dim row As AMLDAL.AMLDataSet.UserRow = dt.Rows(0)
                                    oldPassword = row.UserPassword
                                End Using
                            End Using
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "Password", "Reset Password", oldPassword, NewPassword, "True")
                            End Using
                            Using AccessHistoryPassword As New AMLDAL.AMLDataSetTableAdapters.HistoryPasswordTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessHistoryPassword, oSQLTrans)
                                AccessHistoryPassword.Insert(LblUserID.Text.Trim, NewPassword, Now)
                            End Using

                            Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessUser, oSQLTrans)
                                AccessUser.ChangeUserPassword(LblUserID.Text.Trim, NewPassword, "1900-01-01")
                            End Using

                            oSQLTrans.Commit()
                            'End Using

                            Dim MessagePendingID As Integer = 8622 'MessagePendingID 8622 = ResetPassword  

                            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & LblUserID.Text.Trim

                            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & LblUserID.Text.Trim, False)
                        Else
                            Throw New Exception("Cannot reset password for the following User : " & LblUserID.Text.Trim & " because it violates the Password Recycle Count rule.")
                        End If
                    End If
                Catch ex As Exception
                    If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                    Me.cvalPageError.IsValid = False
                    Me.cvalPageError.ErrorMessage = ex.Message
                    LogError(ex)
                Finally
                    If Not oSQLTrans Is Nothing Then
                        oSQLTrans.Dispose()
                        oSQLTrans = Nothing
                    End If

                End Try
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally

        End Try

    End Sub

    Sub SelectUser(ByVal IntUserid As Long)
        HUserID.Value = IntUserid
        Using objUser As SahassaNettier.Entities.User = UserBLL.GetUserByPkUserID(IntUserid)
            If Not objUser Is Nothing Then
                LblUserID.Text = objUser.UserID
                LabelUserName.Text = objUser.UserName
                Session("ResetPassword.SaltUser") = objUser.UserPasswordSalt

            
            Else
                LblUserID.Text = ""
                LabelUserName.Text = ""
                Session("ResetPassword.SaltUser") = ""
            End If
        End Using

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            AddHandler PopUpUser1.SelectUser, AddressOf SelectUser
            If Not Me.IsPostBack Then


                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '        Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' cancel event handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "Default.aspx"

        Me.Response.Redirect("Default.aspx", False)
    End Sub

    Private Sub RefreshUserName()
        'Tampilkan UserName pd LabelUserName
        Dim pkUserId As Int64 = HUserID.Value
        Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
            Dim objTable As Data.DataTable = AccessUser.GetDataByUserID(pkUserId)
            Dim objRow As AMLDAL.AMLDataSet.UserRow = objTable.Rows(0)

            Me.LabelUserName.Text = objRow.UserName

            ViewState("Salt") = CType(objRow.UserPasswordSalt, String)
        End Using
    End Sub


    ''' <summary>
    ''' Get Minimum Password Length
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetMinimumPasswordLength() As Int32
        Using AccessLoginParameter As New AMLDAL.AMLDataSetTableAdapters.LoginParameterTableAdapter
            Using LoginParameterTable As AMLDAL.AMLDataSet.LoginParameterDataTable = AccessLoginParameter.GetData

                If LoginParameterTable.Rows.Count = 0 Then
                    Return 6 'Default minimum password length = 6
                Else
                    Dim LoginParameterRow As AMLDAL.AMLDataSet.LoginParameterRow = LoginParameterTable.Rows(0)
                    Return LoginParameterRow.MinimumPasswordLength
                End If
            End Using
        End Using
    End Function

    Private Function CheckIfPasswordIsOK(ByVal UserId As String, ByVal EncryptedPassword As String, ByVal PasswordRecycleCount As Int32) As Boolean
        Try
            Dim connectionString As String = WebConfigurationManager.ConnectionStrings("AMLConnectionString").ConnectionString
            Dim con As New SqlConnection(connectionString)
            Dim QueryString As String
            QueryString = String.Format("select count(*) from (select top {0} HistoryPasswordUserPassword from HistoryPassword where HistoryPasswordUserId='{1}' order by HistoryPasswordEntryDate desc)a where HistoryPasswordUserPassword = '{2}'", PasswordRecycleCount, UserId, EncryptedPassword)

            Dim cmd As New SqlCommand()
            Dim count As Int32

            cmd.Connection = con
            cmd.CommandType = CommandType.Text
            cmd.CommandText = QueryString

            con.Open()
            count = CType(cmd.ExecuteScalar(), Int32)
            con.Close()

            If count > 0 Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Function

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Try

            If Not Page.ClientScript.IsClientScriptIncludeRegistered("idpopup1") Then
                Page.ClientScript.RegisterClientScriptInclude(Me.GetType(), "idpopup1", ResolveClientUrl("script/popupdrag.js"))
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub btnBrowse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBrowse.Click

        Try

            Page.ClientScript.RegisterStartupScript(Me.GetType(), "idpopup2", "popUpDrag('" & CType(sender, LinkButton).ClientID & "','divBrowseUser');", True)
            PopUpUser1.initData()

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub
End Class