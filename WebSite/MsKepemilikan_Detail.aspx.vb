#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
Imports Sahassa.AML
#End Region

Partial Class MsKepemilikan_Detail
    Inherits Parent

#Region "Function"

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub



    Private Sub LoadData()
        Dim ObjMsKepemilikan As MsKepemilikan = DataRepository.MsKepemilikanProvider.GetPaged(MsKepemilikanColumn.IDKepemilikan.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsKepemilikan Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsKepemilikan
            SafeDefaultValue = "-"
            lblIDKepemilikan.Text = .IDKepemilikan
            lblNamaKepemilikan.Text = Safe(.NamaKepemilikan)


            'other info
            Dim Omsuser As User
            Omsuser = DataRepository.UserProvider.GetPaged(UserColumn.UserID.ToString & "=" & .CreatedBy, "", 0, Integer.MaxValue, 0)(0)
            If Omsuser IsNot Nothing Then
                lblCreatedBy.Text = Omsuser.UserName
            End If
            lblCreatedDate.Text = FormatDate(.CreatedDate)
            Omsuser = DataRepository.UserProvider.GetPaged(UserColumn.UserID.ToString & "=" & .LastUpdatedBy, "", 0, Integer.MaxValue, 0)(0)
            If Omsuser IsNot Nothing Then
                lblUpdatedby.Text = Omsuser.UserName
            End If
            lblUpdatedDate.Text = FormatDate(.LastUpdatedDate)
            lblActivation.Text = SafeActiveInactive(.Activation)
            Dim L_objMappingMsKepemilikanNCBSPPATK As TList(Of MappingMsKepemilikanNCBSPPATK)
            L_objMappingMsKepemilikanNCBSPPATK = DataRepository.MappingMsKepemilikanNCBSPPATKProvider.GetPaged(MappingMsKepemilikanNCBSPPATKColumn.IDKepemilikan.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            Listmaping.AddRange(L_objMappingMsKepemilikanNCBSPPATK)

        End With
    End Sub


#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsKepemilikan_View.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
                Listmaping = New TList(Of MappingMsKepemilikanNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub
#End Region

#Region "Property..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsKepemilikanNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKepemilikanNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKepemilikanNCBSPPATK In Listmaping.FindAllDistinct("PK_MappingMsKepemilikanNCBSPPATK_Id")
                Temp.Add(i.IDKepemilikanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property
#End Region

End Class



