Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Excel
Imports Sahassa.AML.Commonly
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.IO

Partial Class LTKT_upload
    Inherits Parent

#Region "Structure..."
    'Structure AMLBLL.LTKTBLL.DT_GROUP_LTKT
    '    Dim LTKT As DataRow
    '    Dim L_LTKTTransactionCashIn As DataTable
    '    Dim L_LTKTTransactionCashOut As DataTable
    '    Dim L_LTKTDetailCashInTransaction As DataTable
    '    Dim L_LTKTDetailCashOutTransaction As DataTable
    'End Structure
    Structure GrouP_LTKT_success
        Dim LTKT As LTKT_ApprovalDetail
        Dim L_LTKTTransactionCashIn As TList(Of LTKTTransactionCashIn_ApprovalDetail)
        Dim L_LTKTTransactionCashOut As TList(Of LTKTTransactionCashOut_ApprovalDetail)
        Dim L_LTKTDetailCashInTransaction As TList(Of LTKTDetailCashInTransaction_ApprovalDetail)
        Dim L_LTKTDetailCashOutTransaction As TList(Of LTKTDetailCashOutTransaction_ApprovalDetail)
    End Structure

#End Region

#Region "Property umum"
    Private Property GroupTableSuccessData() As List(Of GrouP_LTKT_success)
        Get
            Return Session("GroupTableSuccessData")
        End Get
        Set(ByVal value As List(Of GrouP_LTKT_success))
            Session("GroupTableSuccessData") = value
        End Set
    End Property

    Private Property GroupTableAnomalyData() As List(Of AMLBLL.LTKTBLL.DT_GrouP_LTKT)
        Get
            Return Session("GroupTableAnomalyData")
        End Get
        Set(ByVal value As List(Of AMLBLL.LTKTBLL.DT_GrouP_LTKT))
            Session("GroupTableAnomalyData") = value
        End Set
    End Property
    Public ReadOnly Property GET_PKUserID() As Integer
        Get
            'Return SessionPkUserId
            Return SessionPkUserId
        End Get
    End Property

    Public ReadOnly Property GET_UserName() As String
        Get
            'Return SessionNIK
            Return SessionUserId
        End Get
    End Property

    Private Property SetnGetDataTableForSuccessData() As TList(Of LTKT_ApprovalDetail)
        Get
            Return CType(IIf(Session("LTKT_upload.TableSuccessUpload") Is Nothing, Nothing, Session("LTKT_upload.TableSuccessUpload")), TList(Of LTKT_ApprovalDetail))
        End Get
        Set(ByVal value As TList(Of LTKT_ApprovalDetail))
            Session("LTKT_upload.TableSuccessUpload") = value
        End Set
    End Property

    Private Property SetnGetDataTableForAnomalyData() As Data.DataTable
        Get
            Return CType(IIf(Session("LTKT_upload.TableAnomalyUpload") Is Nothing, Nothing, Session("LTKT_upload.TableAnomalyUpload")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("LTKT_upload.TableAnomalyUpload") = value
        End Set
    End Property

    Private Property DSexcelTemp() As Data.DataSet
        Get
            Return CType(IIf(Session("LTKT_upload.DSexcelTemp") Is Nothing, Nothing, Session("LTKT_upload.DSexcelTemp")), DataSet)
        End Get
        Set(ByVal value As Data.DataSet)
            Session("LTKT_upload.DSexcelTemp") = value
        End Set
    End Property

#End Region

#Region "Property Tabel Temporary"
    Private Property DT_LTKT() As Data.DataTable
        Get
            Return CType(IIf(Session("LTKT_upload.DT_LTKT") Is Nothing, Nothing, Session("LTKT_upload.DT_LTKT")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("LTKT_upload.DT_LTKT") = value
        End Set
    End Property

    Private Property DT_LTKTTransactionCashIn() As Data.DataTable
        Get
            Return CType(IIf(Session("LTKT_upload.DT_LTKTTransactionCashIn") Is Nothing, Nothing, Session("LTKT_upload.DT_LTKTTransactionCashIn")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("LTKT_upload.DT_LTKTTransactionCashIn") = value
        End Set
    End Property

    Private Property DT_LTKTDetailCashInTransaction() As Data.DataTable
        Get
            Return CType(IIf(Session("LTKT_upload.DT_LTKTDetailCashInTransaction") Is Nothing, Nothing, Session("LTKT_upload.DT_LTKTDetailCashInTransaction")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("LTKT_upload.DT_LTKTDetailCashInTransaction") = value
        End Set
    End Property

    Private Property DT_LTKTTransactionCashOut() As Data.DataTable
        Get
            Return CType(IIf(Session("LTKT_upload.DT_LTKTTransactionCashOut") Is Nothing, Nothing, Session("LTKT_upload.DT_LTKTTransactionCashOut")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("LTKT_upload.DT_LTKTTransactionCashOut") = value
        End Set
    End Property

    Private Property DT_LTKTDetailCashOutTransaction() As Data.DataTable
        Get
            Return CType(IIf(Session("LTKT_upload.DT_LTKTDetailCashOutTransaction") Is Nothing, Nothing, Session("LTKT_upload.DT_LTKTDetailCashOutTransaction")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("LTKT_upload.DT_LTKTDetailCashOutTransaction") = value
        End Set
    End Property
#End Region

#Region "Property Table Sukses Data"
    Private Property TableSuccessLTKT As TList(Of LTKT_ApprovalDetail)
        Get
            Return Session("TableSuccessLTKT")
        End Get
        Set(ByVal value As TList(Of LTKT_ApprovalDetail))
            Session("TableSuccessLTKT") = value
        End Set
    End Property

    Private Property TableSuccessLTKTTransactionCashIn As TList(Of LTKTTransactionCashIn_ApprovalDetail)
        Get
            Return Session("TableSuccessLTKTTransactionCashIn")
        End Get
        Set(ByVal value As TList(Of LTKTTransactionCashIn_ApprovalDetail))
            Session("TableSuccessLTKTTransactionCashIn") = value
        End Set
    End Property

    Private Property TableSuccessLTKTDetailCashInTransaction As TList(Of LTKTDetailCashInTransaction_ApprovalDetail)
        Get
            Return Session("TableSuccessLTKTDetailCashInTransaction")
        End Get
        Set(ByVal value As TList(Of LTKTDetailCashInTransaction_ApprovalDetail))
            Session("TableSuccessLTKTDetailCashInTransaction") = value
        End Set
    End Property

    Private Property TableSuccessLTKTTransactionCashOut As TList(Of LTKTTransactionCashOut)
        Get
            Return Session("TableSuccessLTKTTransactionCashOut")
        End Get
        Set(ByVal value As TList(Of LTKTTransactionCashOut))
            Session("TableSuccessLTKTTransactionCashIn") = value
        End Set
    End Property

    Private Property TableSuccessLTKTDetailCashOutTransaction As TList(Of LTKTDetailCashOutTransaction)
        Get
            Return Session("TableSuccessLTKTDetailCashOutTransaction")
        End Get
        Set(ByVal value As TList(Of LTKTDetailCashOutTransaction))
            Session("TableSuccessLTKTDetailCashOutTransaction") = value
        End Set
    End Property
#End Region

#Region "Property Table Anomaly Data"

    Private Property TableAnomalyLTKT As Data.DataTable
        Get
            Return Session("TableAnomalyLTKT")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyLTKT") = value
        End Set
    End Property

    Private Property TableAnomalyLTKTTransactionCashIn As Data.DataTable
        Get
            Return Session("TableAnomalyLTKTTransactionCashIn")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyLTKTTransactionCashIn") = value
        End Set
    End Property

    Private Property TableAnomalyLTKTDetailCashInTransaction As Data.DataTable
        Get
            Return Session("TableAnomalyLTKTDetailCashInTransaction")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyLTKTDetailCashInTransaction") = value
        End Set
    End Property

    Private Property TableAnomalyLTKTTransactionCashOut As Data.DataTable
        Get
            Return Session("TableAnomalyLTKTTransactionCashOut")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyLTKTTransactionCashIn") = value
        End Set
    End Property

    Private Property TableAnomalyLTKTDetailCashOutTransaction As Data.DataTable
        Get
            Return Session("TableAnomalyLTKTDetailCashOutTransaction")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyLTKTDetailCashOutTransaction") = value
        End Set
    End Property

#End Region

#Region "validasi.."

    Private Function ValidateInput(ByVal LTKT As AMLBLL.LTKTBLL.DT_GrouP_LTKT) As List(Of String)
        Dim temp As New List(Of String)
        Dim msgError As New StringBuilder
        Dim errorline As New StringBuilder
        Try
            ValidateLTKT(LTKT.LTKT, msgError, errorline)
            For Each ci As DataRow In LTKT.L_LTKTTransactionCashIn.Rows
                ValidateLTKTTransactionCashIn(ci, msgError, errorline)
            Next
            For Each ci As DataRow In LTKT.L_LTKTDetailCashInTransaction.Rows
                ValidateLTKTDetailCashInTransaction(ci, msgError, errorline)
            Next
            For Each ci As DataRow In LTKT.L_LTKTTransactionCashOut.Rows
                ValidateLTKTTransactionCashOut(ci, msgError, errorline)
            Next
            For Each ci As DataRow In LTKT.L_LTKTDetailCashOutTransaction.Rows
                ValidateLTKTDetailCashOutTransaction(ci, msgError, errorline)
            Next
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        Catch ex As Exception
            msgError.Append(ex.Message)
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        End Try
        Return temp
    End Function
    Private Shared Sub ValidateLTKTDetailCashOutTransaction(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorLine As StringBuilder)
        Try

            If DataRepository.LTKTDetailCashOutTransactionProvider.GetByPK_LTKTDetailCashOutTransaction_Id(DTR("PK_LTKTDetailCashOutTransaction_Id")) Is Nothing Then
                msgError.AppendLine(" &bull;Detail Cashout tidak eksis <BR/>")
                errorLine.AppendLine(" &bull;Detaial CashOut : Line " & DTR("NO") & "<BR/>")
                Exit Sub
            End If

            '===================== Validasi numeric : Kas Keluar input must numeric ==============================================================================
            If ObjectAntiNull(DTR("KasKeluar")) Then
                If objectNumOnly(DTR("KasKeluar")) = False Then
                    msgError.AppendLine("&bull;Kas Keluar must be filled numeric only<BR/>")
                    errorLine.AppendLine(" &bull;Detail CashOut : Line " & DTR("NO") & "<BR/>")
                End If

            End If

            ''======================== Validasi textbox :  Kas Keluar canot Null ====================================================
            'If ObjectAntiNull(DTR("KasKeluar")) = False Then
            '    If ObjectAntiNull(DTR("Asing_FK_MsCurrency_Id")) = False Then
            '        msgError.AppendLine("&bull;Asing  FK  Ms Currency  Id must be filled<BR/>")
            '        errorLine.AppendLine(" &bull;Detail CashOut : Line " & DTR("NO") & "<BR/>")
            '    End If
            'End If

            If CDec(DTR("KasKeluar")) > 0D Then
            Else
                '======================== Validasi textbox :  Asing  Kurs Transaksi canot Null ====================================================
                If ObjectAntiNull(DTR("Asing_KursTransaksi")) = False Then
                    msgError.AppendLine("&bull;Asing  Kurs Transaksi must be filled<BR/>")
                    errorLine.AppendLine(" &bull;Detail CashOut : Line " & DTR("NO") & "<BR/>")
                End If

                '===================== Validasi numeric : Asing  Kurs Transaksi input must numeric ==============================================================================
                If ObjectAntiNull(DTR("Asing_KursTransaksi")) Then
                    If objectNumOnly(DTR("Asing_KursTransaksi")) = False Then
                        msgError.AppendLine("&bull;Asing  Kurs Transaksi must be filled numeric only<BR/>")
                        errorLine.AppendLine(" &bull;Detail CashOut : Line " & DTR("NO") & "<BR/>")
                    End If

                End If
                '======================== Validasi textbox :  Asing  Total Kas Keluar Dalam Rupiah canot Null ====================================================
                If ObjectAntiNull(DTR("Asing_TotalKasKeluarDalamRupiah")) = False Then
                    msgError.AppendLine("&bull;Asing  Total Kas Keluar Dalam Rupiah must be filled<BR/>")
                    errorLine.AppendLine(" &bull;Detail CashOut : Line " & DTR("NO") & "<BR/>")
                End If

                '===================== Validasi numeric : Asing  Total Kas Keluar Dalam Rupiah input must numeric ==============================================================================
                If ObjectAntiNull(DTR("Asing_TotalKasKeluarDalamRupiah")) Then
                    If objectNumOnly(DTR("Asing_TotalKasKeluarDalamRupiah")) = False Then
                        msgError.AppendLine("&bull;Asing  Total Kas Keluar Dalam Rupiah must be filled numeric only<BR/>")
                        errorLine.AppendLine(" &bull;Detail CashOut : Line " & DTR("NO") & "<BR/>")
                    End If

                End If
            End If

           ''===================== Validasi numeric : Total Kas Keluar input must numeric ==============================================================================
            'If ObjectAntiNull(DTR("TotalKasKeluar")) Then
            '    If objectNumOnly(DTR("TotalKasKeluar")) = False Then
            '        msgError.AppendLine("&bull;Total Kas Keluar must be filled numeric only<BR/>")
            '        errorLine.AppendLine(" &bull;Detail CashOut : Line " & DTR("NO") & "<BR/>")
            '    End If

            'End If
            
            '======================== Validasi textbox :  PK LTKTDetail Cash Out Transaction  Id canot Null ====================================================
            If ObjectAntiNull(DTR("PK_LTKTDetailCashOutTransaction_Id")) = False Then
                msgError.AppendLine("&bull;PK LTKT Detail Cash Out Transaction  Id must be filled<BR/>")
                errorLine.AppendLine(" &bull;Detail CashOut : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  FK LTKTTransaction Cash Out  Id canot Null ====================================================
            If ObjectAntiNull(DTR("FK_LTKTTransactionCashOut_Id")) = False Then
                msgError.AppendLine("&bull;FK  LTKT Transaction Cash Out  Id must be filled<BR/>")
                errorLine.AppendLine(" &bull;Detail CashOut : Line " & DTR("NO") & "<BR/>")
            End If
        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorLine.AppendLine(" &bull;CashOut  : Line " & DTR("NO") & "<BR/>")
        End Try
    End Sub
    Private Shared Sub ValidateLTKTDetailCashInTransaction(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorLine As StringBuilder)
        Try
            If DataRepository.LTKTDetailCashInTransactionProvider.GetByPK_LTKTDetailCashInTransaction_Id(DTR("PK_LTKTDetailCashInTransaction_Id")) Is Nothing Then
                msgError.AppendLine(" &bull;Detail Cashin tidak eksis <BR/>")
                errorLine.AppendLine(" &bull;Detail Cashin : Line " & DTR("NO") & "<BR/>")
                Exit Sub
            End If

            '===================== Validasi numeric : Kas Masuk input must numeric ==============================================================================
            If objectNumOnly(DTR("KasMasuk")) Then
                If objectNumOnly(DTR("KasMasuk")) = False Then
                    msgError.AppendLine(" &bull;Kas Masuk must be filled numeric only<BR/> ")
                    errorLine.AppendLine(" &bull;Detail Cashin : Line " & DTR("NO") & "<BR/>")
                End If

            End If

            If CDec(DTR("KasMasuk")) > 0D Then
                '======================== Validasi textbox :  Kas Masuk canot Null ====================================================
                If objectNumOnly(DTR("KasMasuk")) = False Then
                    msgError.AppendLine(" &bull;Kas Masuk must be filled<BR/> ")
                    errorLine.AppendLine(" &bull;Detail Cashin : Line " & DTR("NO") & "<BR/>")
                End If
            Else
                If objectNumOnly(DTR("Asing_FK_MsCurrency_Id")) = False Then
                    msgError.AppendLine("&bull;Asing  FK  Ms Currency  Id must be filled<BR/> ")
                    errorLine.AppendLine(" &bull;Detail Cashin : Line " & DTR("NO") & "<BR/>")
                End If

                '======================== Validasi textbox :  Asing  Kurs Transaksi canot Null ====================================================
                If objectNumOnly(DTR("Asing_KursTransaksi")) = False Then
                    msgError.AppendLine("&bull;Asing  Kurs Transaksi must be filled<BR/> ")
                    errorLine.AppendLine(" &bull;Detail Cashin : Line " & DTR("NO") & "<BR/>")
                End If

                '===================== Validasi numeric : Asing  Kurs Transaksi input must numeric ==============================================================================
                If objectNumOnly(DTR("Asing_KursTransaksi")) Then
                    If objectNumOnly(DTR("Asing_KursTransaksi")) = False Then
                        msgError.AppendLine(" &bull;Asing  Kurs Transaksi must be filled numeric only<BR/> ")
                        errorLine.AppendLine(" &bull;Detail Cashin : Line " & DTR("NO") & "<BR/>")
                    End If

                End If
                '======================== Validasi textbox :  Asing  Total Kas Masuk Dalam Rupiah canot Null ====================================================
                If objectNumOnly(DTR("Asing_TotalKasMasukDalamRupiah")) = False Then
                    msgError.AppendLine(" &bull;Asing  Total Kas Masuk Dalam Rupiah must be filled<BR/> ")
                    errorLine.AppendLine(" &bull;Detail Cashin : Line " & DTR("NO") & "<BR/>")
                End If

                '===================== Validasi numeric : Asing  Total Kas Masuk Dalam Rupiah input must numeric ==============================================================================
                If objectNumOnly(DTR("Asing_TotalKasMasukDalamRupiah")) Then
                    If objectNumOnly(DTR("Asing_TotalKasMasukDalamRupiah")) = False Then
                        msgError.AppendLine(" &bull;Asing  Total Kas Masuk Dalam Rupiah must be filled numeric only<BR/> ")
                        errorLine.AppendLine(" &bull;Detail Cashin : Line " & DTR("NO") & "<BR/>")
                    End If

                End If
            End If



            ''======================== Validasi textbox :  Total Kas Masuk canot Null ====================================================
            'If objectNumOnly(DTR("TotalKasMasuk")) = False Then
            '    msgError.AppendLine(" &bull;Total Kas Masuk must be filled<BR/> ")
            '    errorLine.AppendLine(" &bull;Detail Cashin : Line " & DTR("NO") & "<BR/>")
            'End If

            ''===================== Validasi numeric : Total Kas Masuk input must numeric ==============================================================================
            'If objectNumOnly(DTR("TotalKasMasuk")) Then
            '    If objectNumOnly(DTR("TotalKasMasuk")) = False Then
            '        msgError.AppendLine(" &bull;Total Kas Masuk must be filled numeric only<BR/> ")
            '        errorLine.AppendLine(" &bull;Detail Cashin : Line " & DTR("NO") & "<BR/>")
            '    End If
            'End If



            '======================== Validasi textbox :  PK  LTKT Detail Cash In Transaction  Id canot Null ====================================================
            If objectNumOnly(DTR("PK_LTKTDetailCashInTransaction_Id")) = False Then
                msgError.AppendLine(" &bull;PK  LTKT Detail Cash In Transaction  Id must be filled<BR/> ")
                errorLine.AppendLine(" &bull;Detail Cashin : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  FK  LTKT Transaction Cash In  Id canot Null ====================================================
            If objectNumOnly(DTR("FK_LTKTTransactionCashIn_Id")) = False Then
                msgError.AppendLine(" &bull;FK  LTKT Transaction Cash In  Id must be filled<BR/> ")
                errorLine.AppendLine(" &bull;Detail Cashin : Line " & DTR("NO") & "<BR/>")
            End If
        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorLine.AppendLine(" &bull;CashOut  : Line " & DTR("NO") & "<BR/>")
        End Try
    End Sub
    Private Shared Sub ValidateLTKTTransactionCashOut(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorLine As StringBuilder)
        Try

            If DataRepository.LTKTTransactionCashOutProvider.GetByPK_LTKTTransactionCashOut_Id(DTR("PK_LTKTTransactionCashOut_Id")) Is Nothing Then
                msgError.AppendLine(" &bull;CashOut tidak eksis <BR/>")
                errorLine.AppendLine(" &bull;Cashout : Line " & DTR("NO") & "<BR/>")
                Exit Sub
            End If
            '======================== Validasi textbox :  FK  LTKT  Id canot Null ====================================================
            If ObjectAntiNull(DTR("FK_LTKT_Id")) = False Then
                msgError.AppendLine(" &bull;FK  LTKT  Id must be filled<BR/>")
                errorLine.AppendLine(" &bull;CashOut  : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  FK  Ms Kota Kab  Id canot Null ====================================================
            If ObjectAntiNull(DTR("FK_MsKotaKab_Id")) = False Then
                msgError.AppendLine(" &bull;FK  Ms Kota Kab  Id must be filled<BR/>")
                errorLine.AppendLine(" &bull;CashOut  : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  FK  Ms Province  Id canot Null ====================================================
            If ObjectAntiNull(DTR("FK_MsProvince_Id")) = False Then
                msgError.AppendLine(" &bull;FK  Ms Province  Id must be filled<BR/>")
                errorLine.AppendLine(" &bull;CashOut  : Line " & DTR("NO") & "<BR/>")
            End If



            '======================== Validasi textbox :  Tanggal Transaksi canot Null ====================================================
            If ObjectAntiNull(DTR("TanggalTransaksi")) = False Then
                msgError.AppendLine(" &bull;Tanggal Transaksi must be filled<BR/>")
                errorLine.AppendLine(" &bull;CashOut  : Line " & DTR("NO") & "<BR/>")
            End If

            '========================== validasi tanggal : Tanggal Transaksi must format date correct ==================================================
            If ObjectAntiNull(DTR("TanggalTransaksi")) Then
                If ObjectIsDates(DTR("TanggalTransaksi")) = False Then
                    msgError.AppendLine(" &bull;format Tanggal Transaksi tidak valid<BR/>")
                    errorLine.AppendLine(" &bull;CashOut  : Line " & DTR("NO") & "<BR/>")
                End If

            End If

            '======================== Validasi textbox :  Total canot Null ====================================================
            If ObjectAntiNull(DTR("Total")) = False Then
                msgError.AppendLine(" &bull;Total must be filled<BR/>")
                errorLine.AppendLine(" &bull;CashOut  : Line " & DTR("NO") & "<BR/>")
            End If

            '===================== Validasi numeric : Total input must numeric ==============================================================================
            If ObjectAntiNull(DTR("Total")) Then
                If objectNumOnly(DTR("Total")) = False Then
                    msgError.AppendLine(" &bull;Total must be filled numeric only<BR/>")
                    errorLine.AppendLine(" &bull;CashOut  : Line " & DTR("NO") & "<BR/>")
                End If

            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorLine.AppendLine(" &bull;CashOut  : Line " & DTR("NO") & "<BR/>")
        End Try
    End Sub
    Private Shared Sub ValidateLTKTTransactionCashIn(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorLine As StringBuilder)
        Try
            If DataRepository.LTKTTransactionCashInProvider.GetByPK_LTKTTransactionCashIn_Id(DTR("PK_LTKTTransactionCashIn_Id")) Is Nothing Then
                msgError.AppendLine(" &bull;Cashin tidak eksis <BR/>")
                errorLine.AppendLine(" &bull;Cashin : Line " & DTR("NO") & "<BR/>")
                Exit Sub
            End If
            '======================== Validasi textbox :  FK  LTKT  Id canot Null (#)====================================================
            If ObjectAntiNull(DTR("FK_LTKT_Id")) = False Then
                msgError.AppendLine(" &bull;FK  LTKT  Id must be filled <BR/>")
                errorLine.AppendLine(" &bull;Cashin : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  FK  Ms Kota Kab  Id canot Null  (#)====================================================
            'If ObjectAntiNull(DTR("FK_MsKotaKab_Id")) = False Then
            '    msgError.AppendLine(" &bull;FK  Ms Kota Kab  Id must be filled <BR/>")
            '    errorLine.AppendLine(" &bull;Cashin : Line " & DTR("NO") & "<BR/>")
            'End If

            '======================== Validasi textbox :  FK  Ms Province  Id canot Null  (#)====================================================
            'If ObjectAntiNull(DTR("FK_MsProvince_Id")) = False Then
            '    msgError.AppendLine(" &bull;FK  Ms Province  Id must be filled <BR/>")
            '    errorLine.AppendLine(" &bull;Cashin : Line " & DTR("NO") & "<BR/>")
            'End If

            '======================== Validasi textbox :  Tanggal Transaksi canot Null ====================================================
            If ObjectAntiNull(DTR("TanggalTransaksi")) = False Then
                msgError.AppendLine(" &bull;Tanggal Transaksi must be filled <BR/>")
                errorLine.AppendLine(" &bull;Cashin : Line " & DTR("NO") & "<BR/>")
            End If

            '========================== validasi tanggal : Tanggal Transaksi must format date correct ==================================================
            If ObjectAntiNull(DTR("TanggalTransaksi")) Then
                If ObjectIsDates(DTR("TanggalTransaksi")) = False Then
                    msgError.AppendLine(" &bull;format Tanggal Transaksi tidak valid <BR/>")
                    errorLine.AppendLine(" &bull;Cashin : Line " & DTR("NO") & "<BR/>")
                End If

            End If

            '======================== Validasi textbox :  Total canot Null (#)====================================================
            If ObjectAntiNull(DTR("Total")) = False Then
                msgError.AppendLine(" &bull;Total must be filled <BR/>")
                errorLine.AppendLine(" &bull;Cashin : Line " & DTR("NO") & "<BR/>")
            End If

            '===================== Validasi numeric : Total input must numeric ==============================================================================
            If ObjectAntiNull(DTR("Total")) = False Then
                If objectNumOnly(DTR("Total")) = False Then
                    msgError.AppendLine(" &bull;Total must be filled numeric only <BR/>")
                    errorLine.AppendLine(" &bull;Cashin : Line " & DTR("NO") & "<BR/>")
                End If

            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorLine.AppendLine(" &bull;CashOut  : Line " & DTR("NO") & "<BR/>")
        End Try
    End Sub
    Private Shared Sub ValidateLTKT(ByVal DTR As DataRow, ByVal temp As StringBuilder, ByVal errorline As StringBuilder)
        Try
            Using checkBlocking As TList(Of LTKT_ApprovalDetail) = DataRepository.LTKT_ApprovalDetailProvider.GetPaged("Pk_LTKT_id=" & DTR("PK_LTKT_Id"), "", 0, 1, Nothing)
                If checkBlocking.Count > 0 Then
                    temp.AppendLine(" &bull;PK  LTKT  Id is waiting for approval <BR/>")
                    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                End If
            End Using


            If DataRepository.LTKTProvider.GetByPK_LTKT_Id(DTR("PK_LTKT_Id")) Is Nothing Then
                temp.AppendLine(" &bull;LTKT tidak eksis <BR/>")
                errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                Exit Sub
            End If
            '======================== Validasi textbox :  PK  LTKT  Id canot Null ====================================================
            If ObjectAntiNull(DTR("PK_LTKT_Id")) = False Then
                temp.AppendLine(" &bull;PK  LTKT  Id must be filled <BR/>")
                errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
            End If
            '======================== Validasi textbox :  I N D V  Penghasilan Rata Rata canot Null (#) ====================================================
            'If ObjectAntiNull(DTR("INDV_PenghasilanRataRata")) = False Then
            '    temp.AppendLine(" &bull;I N D V  Penghasilan Rata Rata must be filled <BR/>")
            '    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
            'End If

            'If ObjectAntiNull(DTR("INDV_PenghasilanRataRata")) = False Then
            '    temp.AppendLine(" &bull;I N D V  Penghasilan Rata Rata must be filled <BR/>")
            '    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
            'End If
            '======================== Validasi textbox :  Tanggal Laporan canot Null (#) ====================================================
            If ObjectAntiNull(DTR("TanggalLaporan")) = False Then
                temp.AppendLine(" &bull;Tanggal Laporan must be filled <BR/>")
                errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
            Else
                If CToDate2(DTR("TanggalLaporan")) > Now Then
                    Throw New Exception("Tanggal Pelaporan should be not greater than today")
                End If
            End If
            '======================== Validasi textbox :  Nama P J K Pelapor canot Null ====================================================
            If ObjectAntiNull(DTR("NamaPJKPelapor")) = False Then
                temp.AppendLine(" &bull;Nama P J K Pelapor must be filled <BR/>")
                errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  Nama Pejabat P J K Pelapor canot Null  (#) ====================================================
            If ObjectAntiNull(DTR("NamaPejabatPJKPelapor")) = False Then
                temp.AppendLine(" &bull;Nama Pejabat P J K Pelapor must be filled <BR/>")
                errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  No Rekening canot Null (#) ====================================================
            If ObjectAntiNull(DTR("NoRekening")) = False Then
                temp.AppendLine(" &bull;No Rekening must be filled <BR/>")
                errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  Tipe Terlapor canot Null (#)====================================================
            If ObjectAntiNull(DTR("TipeTerlapor")) = False Then
                temp.AppendLine(" &bull;Tipe Terlapor must be filled <BR/>")
                errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  I N D V  FK  Ms Pekerjaan  Id canot Null (#) ====================================================
            If DTR("TipeTerlapor") = "1" Then
                If ObjectAntiNull(DTR("INDV_FK_MsPekerjaan_Id")) = False Then
                    temp.AppendLine(" &bull;I N D V  FK  Ms Pekerjaan  Id must be filled <BR/>")
                    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                End If
            End If

            If DTR("jenislaporan") = "2" Then
                If ObjectAntiNull(DTR("NoLTKTKoreksi")) = False Then
                    temp.AppendLine(" &bull;No LTKT Koreksi must be filled <BR/>")
                    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                End If
            End If


            '======================== Validasi textbox :  FK  Ms Kepemilikan  Id canot Null (#)====================================================
            If ObjectAntiNull(DTR("FK_MsKepemilikan_Id")) = False Then
                temp.AppendLine(" &bull;FK  Ms Kepemilikan  Id must be filled <BR/>")
                errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
            End If


            If DTR("TipeTerlapor") = "1" Then
                '======================== Validasi textbox :  I N D V  Nama Lengkap canot Null ====================================================
                If ObjectAntiNull(DTR("INDV_NamaLengkap")) = False Then
                    temp.AppendLine(" &bull;I N D V  Nama Lengkap must be filled <BR/>")
                    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                End If

                '======================== Validasi textbox :  I N D V  Tempat Lahir canot Null ====================================================
                If ObjectAntiNull(DTR("INDV_TempatLahir")) = False Then
                    temp.AppendLine(" &bull;I N D V  Tempat Lahir must be filled <BR/>")
                    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                End If

                '======================== Validasi textbox :  I N D V  Tanggal Lahir canot Null ====================================================
                If ObjectAntiNull(DTR("INDV_TanggalLahir")) = False Then
                    temp.AppendLine(" &bull;I N D V  Tanggal Lahir must be filled <BR/>")
                    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                End If

                '========================== validasi tanggal : I N D V  Tanggal Lahir must format date correct ==================================================
                If ObjectAntiNull(DTR("INDV_TanggalLahir")) Then
                    If ObjectIsDates(DTR("INDV_TanggalLahir")) = False Then
                        temp.AppendLine(" &bull;format I N D V  Tanggal Lahir tidak valid <BR/>")
                        errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                    End If
                End If
                '======================== Validasi textbox :  I N D V  Kewarganegaraan canot Null ====================================================
                If ObjectAntiNull(DTR("INDV_Kewarganegaraan")) = False Then
                    temp.AppendLine(" &bull;I N D V  Kewarganegaraan must be filled <BR/>")
                    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                End If

                If DTR("INDV_Kewarganegaraan") = "1" Then
                    '======================== Validasi textbox :  I N D V  I D  Kode Pos canot Null ====================================================
                    If ObjectAntiNull(DTR("INDV_ID_KodePos")) = False Then
                        temp.AppendLine(" &bull;I N D V  I D  Kode Pos must be filled <BR/>")
                        errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                    End If

                    '======================== Validasi textbox :  I N D V  FK  Ms Negara  Id canot Null (#) ====================================================
                    If ObjectAntiNull(DTR("INDV_FK_MsNegara_Id")) = False Then
                        temp.AppendLine(" &bull;I N D V  FK  Ms Negara  Id must be filled <BR/>")
                        errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                    End If
                End If

                '======================== Validasi textbox :  I N D V  D O M  FK  Ms Province  Id canot Null (#)====================================================
                If ObjectAntiNull(DTR("INDV_DOM_FK_MsProvince_Id")) = False Then
                    temp.AppendLine(" &bull;I N D V  D O M  FK  Ms Province  Id must be filled <BR/>")
                    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                End If

                '======================== Validasi textbox :  I N D V  I D  FK  Ms Kota Kab  Id canot Null (#)====================================================
                If ObjectAntiNull(DTR("INDV_DOM_FK_MsKotaKab_Id")) = False Then
                    temp.AppendLine(" &bull;I N D V  I D  FK  Ms Kota Kab  Id must be filled <BR/>")
                    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                End If


                '======================== Validasi textbox :  I N D V  I D  FK  Ms Province  Id canot Null ====================================================
                If ObjectAntiNull(DTR("INDV_ID_FK_MsProvince_Id")) = False Then
                    temp.AppendLine(" &bull;I N D V  I D  FK  Ms Province  Id must be filled <BR/>")
                    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                End If

            End If

            If DTR("TipeTerlapor") = "2" Then ' Tipe korporasi
                '============= Validasi textbox :  CORP  FK  Ms Bentuk Badan Usaha  Id canot Null ====================================================
                If ObjectAntiNull(DTR("CORP_FK_MsBentukBadanUsaha_Id")) = False Then
                    temp.AppendLine(" &bull;CORP  FK  Ms Bentuk Badan Usaha  Id must be filled <BR/>")
                    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                End If

                '======================== Validasi textbox :  CORP  Nama canot Null ====================================================
                If ObjectAntiNull(DTR("CORP_Nama")) = False Then
                    temp.AppendLine(" &bull;CORP  Nama must be filled <BR/>")
                    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                End If

                If ObjectAntiNull(DTR("CORP_FK_MsBentukBadanUsaha_Id")) Then
                    If DTR("CORP_FK_MsBentukBadanUsaha_Id") <> "3" Then
                        If ObjectAntiNull(DTR("CORP_FK_MsBidangUsaha_Id")) = False Then
                            temp.AppendLine(" &bull;CORP  FK  Ms Bidang Usaha  Id must be filled <BR/>")
                            errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                        End If
                    End If
                End If

                '======================== Validasi textbox :  CORP  Tipe Alamat canot Null ====================================================
                If ObjectAntiNull(DTR("CORP_TipeAlamat")) = False Then
                    temp.AppendLine(" &bull;CORP  Tipe Alamat must be filled <BR/>")
                    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                Else
                    If DTR("CORP_TipeAlamat") = "0" Then
                        '======================== Validasi textbox :  CORP  FK  Ms Kota Kab  Id canot Null ====================================================
                        If ObjectAntiNull(DTR("CORP_FK_MsKotaKab_Id")) = False Then
                            temp.AppendLine(" &bull;CORP  FK  Ms Kota Kab  Id must be filled <BR/>")
                            errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                        End If

                        '======================== Validasi textbox :  CORP  FK  Ms Province  Id canot Null ====================================================
                        If ObjectAntiNull(DTR("CORP_FK_MsProvince_Id")) = False Then
                            temp.AppendLine(" &bull;CORP  FK  Ms Province  Id must be filled <BR/>")
                            errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                        End If

                        '======================== Validasi textbox :  CORP  FK  Ms Negara  Id canot Null ====================================================
                        If ObjectAntiNull(DTR("CORP_FK_MsNegara_Id")) = False Then
                            temp.AppendLine(" &bull;CORP  FK  Ms Negara  Id must be filled <BR/>")
                            errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                        End If

                    ElseIf DTR("CORP_TipeAlamat") = "1" Then
                        '======================== Validasi textbox :  CORP  Kode Pos canot Null ====================================================
                        If ObjectAntiNull(DTR("CORP_KodePos")) = False Then
                            temp.AppendLine(" &bull;CORP  Kode Pos must be filled <BR/>")
                            errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                        End If

                        If ObjectAntiNull(DTR("CORP_FK_MsNegara_Id")) = False Then
                            temp.AppendLine(" &bull;CORP_FK_MsNegara_Id must be filled <BR/>")
                            errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                        End If

                    End If
                End If

            Else ' Tipe individu
                If DTR("INDV_Kewarganegaraan") = "2" Then ' kewarganegaraan asing 
                    '======================== Validasi textbox :  INDIV NA   FK  Ms Negara  Id canot Null (#)====================================================
                    If ObjectAntiNull(DTR("INDV_NA_FK_MsNegara_Id")) = False Then
                        temp.AppendLine(" &bull;INDIV NA   FK  Ms Negara  Id must be filled <BR/>")
                        errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                    End If

                    '======================== Validasi textbox :  INDIV NA   FK  Ms Province  Id canot Null (#)====================================================
                    If ObjectAntiNull(DTR("INDV_NA_FK_MsProvince_Id")) = False Then
                        temp.AppendLine(" &bull;INDIV NA   FK  Ms Province  Id must be filled <BR/>")
                        errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                    End If

                    '======================== Validasi textbox :  INDIV NA   FK  Ms Kota Kab  Id canot Null (#)====================================================
                    If ObjectAntiNull(DTR("INDV_NA_FK_MsKotaKab_Id")) = False Then
                        temp.AppendLine(" &bull;INDIV NA   FK  Ms Kota Kab  Id must be filled <BR/>")
                        errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                    End If
                End If
                '======================== Validasi textbox :  I N D V  FK  Ms I D Type  Id canot Null ====================================================
                If ObjectAntiNull(DTR("INDV_FK_MsIDType_Id")) = False Then
                    temp.AppendLine(" &bull;I N D V  FK  Ms I D Type  Id must be filled <BR/>")
                    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                End If


            End If



            '========================== validasi tanggal : Tanggal Laporan must format date correct (#)==================================================
            If ObjectAntiNull(DTR("TanggalLaporan")) Then
                If ObjectIsDates(DTR("TanggalLaporan")) = False Then
                    temp.AppendLine(" &bull;format Tanggal Laporan tidak valid <BR/>")
                    errorline.AppendLine(" &bull;LTKT : Line " & DTR("NO") & "<BR/>")
                End If

            End If


        Catch ex As Exception
            temp.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorline.AppendLine(" &bull;CashOut  : Line " & DTR("NO") & "<BR/>")
        End Try
    End Sub

#End Region

#Region "Upload File"

    Public Function SaveFileImportToSave() As String
        Dim filePath As String
        Dim FileNameToGetData As String = ""
        Try
            If FileUploadKPI.PostedFile.ContentLength = 0 Then
                Return FileNameToGetData
            End If

            If Not FileUploadKPI.HasFile Then
                Return FileNameToGetData
            End If

            filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName)

            FileNameToGetData = FileUploadKPI.FileName
            If System.IO.File.Exists(filePath) Then
                Dim bexist As Boolean = True
                Dim i As Integer = 1
                While bexist
                    filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & FileUploadKPI.FileName.Split(CChar("."))(1))
                    FileNameToGetData = FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & Me.FileUploadKPI.FileName.Split(CChar("."))(1)
                    If Not System.IO.File.Exists(filePath) Then
                        bexist = False
                        Exit While
                    End If
                    i = i + 1
                End While
            End If
            Me.FileUploadKPI.PostedFile.SaveAs(filePath)
            Session("Filepath") = filePath
            Return FileNameToGetData
        Catch
            Throw
        End Try
    End Function
    ReadOnly Property FilePath As String
        Get
            Return Session("FilePath")
        End Get
    End Property
    Private Sub ProcessExcelFile(ByVal FullPathFileNameToExportExcel As String)

        Using stream As FileStream = File.Open(FullPathFileNameToExportExcel, FileMode.Open, FileAccess.Read)
            '1. Reading from a binary Excel file ('97-2003 format; *.xls)
            Using excelReader As IExcelDataReader = ExcelReaderFactory.CreateBinaryReader(stream)
                '...
                '2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                'Dim excelReader As IExcelDataReader = ExcelReaderFactory.CreateOpenXmlReader(stream)
                '...
                '3. DataSet - The result of each spreadsheet will be created in the result.Tables
                'Dim result As DataSet = excelReader.AsDataSet()
                '...
                '4. DataSet - Create column names from first row
                excelReader.IsFirstRowAsColumnNames = True
                DSexcelTemp = excelReader.AsDataSet()
                If DSexcelTemp.Tables("LTKT").Columns(0).ColumnName Like "*Column*" Then
                    DSexcelTemp.Tables("LTKTTransactionCashIn").Rows.Clear()
                End If
                If DSexcelTemp.Tables("LTKTTransactionCashIn").Columns(0).ColumnName Like "*Column*" Then
                    DSexcelTemp.Tables("LTKTTransactionCashIn").Rows.Clear()
                End If
                If DSexcelTemp.Tables("LTKTDetailCashInTransaction").Columns(1).ColumnName Like "*Column*" Then
                    DSexcelTemp.Tables("LTKTDetailCashInTransaction").Rows.Clear()
                End If
                If DSexcelTemp.Tables("LTKTTransactionCashOut").Columns(0).ColumnName Like "*Column*" Then
                    DSexcelTemp.Tables("LTKTTransactionCashOut").Rows.Clear()
                End If
                If DSexcelTemp.Tables("LTKTDetailCashOutTransaction").Columns(0).ColumnName Like "*Column*" Then
                    DSexcelTemp.Tables("LTKTDetailCashOutTransaction").Rows.Clear()
                End If
                'DTexcelTemp = result
            End Using
        End Using

    End Sub


#End Region

#Region "Function..."

    Private Sub ImportExcelTempToDTproperty()
        DT_LTKT = DSexcelTemp.Tables("LTKT")
        DT_LTKTTransactionCashIn = DSexcelTemp.Tables("LTKTTransactionCashIn")
        DT_LTKTDetailCashInTransaction = DSexcelTemp.Tables("LTKTDetailCashInTransaction")
        DT_LTKTTransactionCashOut = DSexcelTemp.Tables("LTKTTransactionCashOut")
        DT_LTKTDetailCashOutTransaction = DSexcelTemp.Tables("LTKTDetailCashOutTransaction")
    End Sub

    Private Sub ClearThisPageSessions()
        DT_LTKT = Nothing
        DT_LTKTDetailCashInTransaction = Nothing
        DT_LTKTDetailCashOutTransaction = Nothing
        DT_LTKTTransactionCashIn = Nothing
        DT_LTKTTransactionCashOut = Nothing
        GroupTableSuccessData = Nothing
        GroupTableAnomalyData = Nothing
        DSexcelTemp = Nothing
        SetnGetDataTableForAnomalyData = Nothing
        SetnGetDataTableForSuccessData = Nothing
    End Sub

    Function ConvertDTtoLTKTTRANSACTIONCASHOUT(ByVal DT As Data.DataTable) As TList(Of LTKTTransactionCashOut_ApprovalDetail)
        Dim temp As New TList(Of LTKTTransactionCashOut_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using LTKTTRANSACTIONCASHOUT As New LTKTTransactionCashOut_ApprovalDetail()
                With LTKTTRANSACTIONCASHOUT
                    FillOrNothing(.CORP_TipeAlamat, DTR("CORP_TipeAlamat"), True, AMLBLL.DataType.oByte)
                    FillOrNothing(.CORP_FK_MsKecamatan_Id, DTR("CORP_FK_MsKecamatan_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_FK_MsKotaKab_Id, DTR("CORP_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_FK_MsProvince_Id, DTR("CORP_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_FK_MsNegara_Id, DTR("CORP_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_FK_MsIDType_Id, DTR("INDV_FK_MsIDType_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_FK_MsBentukBadanUsaha_Id, DTR("CORP_FK_MsBentukBadanUsaha_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_FK_MsBidangUsaha_Id, DTR("CORP_FK_MsBidangUsaha_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_LN_FK_MsNegara_Id, DTR("CORP_LN_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_LN_FK_MsProvince_Id, DTR("CORP_LN_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_LN_MsKotaKab_Id, DTR("CORP_LN_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.FK_LTKT_Id, DTR("FK_LTKT_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.FK_MsKotaKab_Id, DTR("FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.FK_MsProvince_Id, DTR("FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_FK_MsNegara_Id, DTR("INDV_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_DOM_FK_MsKecamatan_Id, DTR("INDV_DOM_FK_MsKecamatan_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_DOM_FK_MsKotaKab_Id, DTR("INDV_DOM_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_DOM_FK_MsProvince_Id, DTR("INDV_DOM_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_DOM_FK_MsNegara_Id, DTR("INDV_DOM_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_ID_FK_MsKecamatan_Id, DTR("INDV_ID_FK_MsKecamatan_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_ID_FK_MsKotaKab_Id, DTR("INDV_ID_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_ID_FK_MsProvince_Id, DTR("INDV_ID_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_ID_FK_MsNegara_Id, DTR("INDV_ID_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_NA_FK_MsNegara_Id, DTR("INDV_NA_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_NA_FK_MsProvince_Id, DTR("INDV_NA_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_NA_FK_MsKotaKab_Id, DTR("INDV_NA_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_FK_MsPekerjaan_Id, DTR("INDV_FK_MsPekerjaan_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_TanggalLahir, DTR("INDV_TanggalLahir"), True, AMLBLL.DataType.oDate)
                    FillOrNothing(.TanggalTransaksi, DTR("TanggalTransaksi"), True, AMLBLL.DataType.oDate)
                    FillOrNothing(.Total, DTR("Total"), True, AMLBLL.DataType.oDecimal)
                    FillOrNothing(.CORP_FK_MsKelurahan_Id, DTR("CORP_FK_MsKelurahan_Id"), True, AMLBLL.DataType.OLong)
                    FillOrNothing(.INDV_ID_FK_MsKelurahan_Id, DTR("INDV_ID_FK_MsKelurahan_Id"), True, AMLBLL.DataType.OLong)
                    FillOrNothing(.PK_LTKTTransactionCashOut_Id, DTR("PK_LTKTTransactionCashOut_Id"), True, AMLBLL.DataType.OLong)
                    FillOrNothing(.INDV_DOM_FK_MsKelurahan_Id, DTR("INDV_DOM_FK_MsKelurahan_Id"), True, AMLBLL.DataType.OLong)
                    FillOrNothing(.INDV_NomorId, DTR("INDV_NomorId"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NPWP, DTR("INDV_NPWP"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_Nama, DTR("CORP_Nama"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_LN_KodePos, DTR("CORP_LN_KodePos"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_NPWP, DTR("CORP_NPWP"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_TujuanTransaksi, DTR("CORP_TujuanTransaksi"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_SumberDana, DTR("CORP_SumberDana"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_NamaBankLain, DTR("CORP_NamaBankLain"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_NomorRekeningTujuan, DTR("CORP_NomorRekeningTujuan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_NamaJalan, DTR("CORP_NamaJalan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_RTRW, DTR("CORP_RTRW"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_KodePos, DTR("CORP_KodePos"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_LN_NamaJalan, DTR("CORP_LN_NamaJalan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.NamaKantorPJK, DTR("NamaKantorPJK"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.NomorRekening, DTR("NomorRekening"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_Gelar, DTR("INDV_Gelar"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NamaLengkap, DTR("INDV_NamaLengkap"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_TempatLahir, DTR("INDV_TempatLahir"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_DOM_NamaJalan, DTR("INDV_DOM_NamaJalan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_DOM_RTRW, DTR("INDV_DOM_RTRW"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_DOM_KodePos, DTR("INDV_DOM_KodePos"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_ID_NamaJalan, DTR("INDV_ID_NamaJalan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_ID_RTRW, DTR("INDV_ID_RTRW"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_Jabatan, DTR("INDV_Jabatan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_PenghasilanRataRata, DTR("INDV_PenghasilanRataRata"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_TempatBekerja, DTR("INDV_TempatBekerja"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_TujuanTransaksi, DTR("INDV_TujuanTransaksi"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_SumberDana, DTR("INDV_SumberDana"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NamaBankLain, DTR("INDV_NamaBankLain"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NomorRekeningTujuan, DTR("INDV_NomorRekeningTujuan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NA_KodePos, DTR("INDV_NA_KodePos"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_ID_KodePos, DTR("INDV_ID_KodePos"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NA_NamaJalan, DTR("INDV_NA_NamaJalan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_Kewarganegaraan, DTR("INDV_Kewarganegaraan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.LastUpdateDate, Date.Now, True, AMLBLL.DataType.oDate)
                    FillOrNothing(.LastUpdateBy, GET_UserName, True, AMLBLL.DataType.oString)
                    temp.Add(LTKTTRANSACTIONCASHOUT)
                End With
            End Using
        Next

        Return temp
    End Function
    Function ConvertDTtoLTKTDETAILCASHINTRANSACTION(ByVal DT As Data.DataTable) As TList(Of LTKTDetailCashInTransaction_ApprovalDetail)
        Dim temp As New TList(Of LTKTDetailCashInTransaction_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using LTKTDETAILCASHINTRANSACTION As New LTKTDetailCashInTransaction_ApprovalDetail()
                With LTKTDETAILCASHINTRANSACTION
                    FillOrNothing(.Asing_FK_MsCurrency_Id, DTR("Asing_FK_MsCurrency_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.Asing_KursTransaksi, DTR("Asing_KursTransaksi"), True, AMLBLL.DataType.oDecimal)
                    FillOrNothing(.Asing_TotalKasMasukDalamRupiah, DTR("Asing_TotalKasMasukDalamRupiah"), True, AMLBLL.DataType.oDecimal)
                    FillOrNothing(.TotalKasMasuk, DTR("TotalKasMasuk"), True, AMLBLL.DataType.oDecimal)
                    FillOrNothing(.KasMasuk, DTR("KasMasuk"), True, AMLBLL.DataType.oDecimal)
                    FillOrNothing(.PK_LTKTDetailCashInTransaction_Id, DTR("PK_LTKTDetailCashInTransaction_Id"), True, AMLBLL.DataType.OLong)
                    FillOrNothing(.FK_LTKTTransactionCashIn_Id, DTR("FK_LTKTTransactionCashIn_Id"), True, AMLBLL.DataType.OLong)
                    temp.Add(LTKTDETAILCASHINTRANSACTION)
                End With
            End Using

        Next


        Return temp
    End Function

    Function ConvertDTtoLTKTDETAILCASHOUTTRANSACTION(ByVal DT As Data.DataTable) As TList(Of LTKTDetailCashOutTransaction_ApprovalDetail)
        Dim temp As New TList(Of LTKTDetailCashOutTransaction_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using LTKTDETAILCASHOUTTRANSACTION As New LTKTDetailCashOutTransaction_ApprovalDetail()
                With LTKTDETAILCASHOUTTRANSACTION
                    FillOrNothing(.Asing_FK_MsCurrency_Id, DTR("Asing_FK_MsCurrency_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.Asing_KursTransaksi, DTR("Asing_KursTransaksi"), True, AMLBLL.DataType.oDecimal)
                    FillOrNothing(.Asing_TotalKasKeluarDalamRupiah, DTR("Asing_TotalKasKeluarDalamRupiah"), True, AMLBLL.DataType.oDecimal)
                    FillOrNothing(.TotalKasKeluar, DTR("TotalKasKeluar"), True, AMLBLL.DataType.oDecimal)
                    FillOrNothing(.KasKeluar, DTR("KasKeluar"), True, AMLBLL.DataType.oDecimal)
                    FillOrNothing(.PK_LTKTDetailCashOutTransaction_Id, DTR("PK_LTKTDetailCashOutTransaction_Id"), True, AMLBLL.DataType.OLong)
                    FillOrNothing(.FK_LTKTTransactionCashOut_Id, DTR("FK_LTKTTransactionCashOut_Id"), True, AMLBLL.DataType.OLong)
                    temp.Add(LTKTDETAILCASHOUTTRANSACTION)
                End With
            End Using

        Next

        Return temp
    End Function
    Function ConvertDTtoLTKTTRANSACTIONCASHIN(ByVal DT As Data.DataTable) As TList(Of LTKTTransactionCashIn_ApprovalDetail)
        Dim temp As New TList(Of LTKTTransactionCashIn_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using LTKTTRANSACTIONCASHIN As New LTKTTransactionCashIn_ApprovalDetail()
                With LTKTTRANSACTIONCASHIN
                    FillOrNothing(.CORP_TipeAlamat, DTR("CORP_TipeAlamat"), True, AMLBLL.DataType.oByte)
                    FillOrNothing(.CORP_FK_MsKecamatan_Id, DTR("CORP_FK_MsKecamatan_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_FK_MsKotaKab_Id, DTR("CORP_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_FK_MsProvince_Id, DTR("CORP_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_FK_MsNegara_Id, DTR("CORP_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_FK_MsIDType_Id, DTR("INDV_FK_MsIDType_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_FK_MsBentukBadanUsaha_Id, DTR("CORP_FK_MsBentukBadanUsaha_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_FK_MsBidangUsaha_Id, DTR("CORP_FK_MsBidangUsaha_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_LN_FK_MsNegara_Id, DTR("CORP_LN_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_LN_FK_MsProvince_Id, DTR("CORP_LN_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_LN_MsKotaKab_Id, DTR("CORP_LN_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.FK_LTKT_Id, DTR("FK_LTKT_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.FK_MsKotaKab_Id, DTR("FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.FK_MsProvince_Id, DTR("FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_FK_MsNegara_Id, DTR("INDV_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_DOM_FK_MsKecamatan_Id, DTR("INDV_DOM_FK_MsKecamatan_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_DOM_FK_MsKotaKab_Id, DTR("INDV_DOM_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_DOM_FK_MsProvince_Id, DTR("INDV_DOM_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_DOM_FK_MsNegara_Id, DTR("INDV_DOM_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_ID_FK_MsKecamatan_Id, DTR("INDV_ID_FK_MsKecamatan_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_ID_FK_MsKotaKab_Id, DTR("INDV_ID_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_ID_FK_MsProvince_Id, DTR("INDV_ID_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_ID_FK_MsNegara_Id, DTR("INDV_ID_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_NA_FK_MsNegara_Id, DTR("INDV_NA_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_NA_FK_MsProvince_Id, DTR("INDV_NA_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_NA_FK_MsKotaKab_Id, DTR("INDV_NA_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_FK_MsPekerjaan_Id, DTR("INDV_FK_MsPekerjaan_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_TanggalLahir, DTR("INDV_TanggalLahir"), True, AMLBLL.DataType.oDate)
                    FillOrNothing(.TanggalTransaksi, DTR("TanggalTransaksi"), True, AMLBLL.DataType.oDate)
                    FillOrNothing(.Total, DTR("Total"), True, AMLBLL.DataType.oDecimal)
                    FillOrNothing(.CORP_FK_MsKelurahan_Id, DTR("CORP_FK_MsKelurahan_Id"), True, AMLBLL.DataType.OLong)
                    FillOrNothing(.INDV_ID_FK_MsKelurahan_Id, DTR("INDV_ID_FK_MsKelurahan_Id"), True, AMLBLL.DataType.OLong)
                    FillOrNothing(.PK_LTKTTransactionCashIn_Id, DTR("PK_LTKTTransactionCashIn_Id"), True, AMLBLL.DataType.OLong)
                    FillOrNothing(.INDV_DOM_FK_MsKelurahan_Id, DTR("INDV_DOM_FK_MsKelurahan_Id"), True, AMLBLL.DataType.OLong)
                    FillOrNothing(.INDV_NomorId, DTR("INDV_NomorId"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NPWP, DTR("INDV_NPWP"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_Nama, DTR("CORP_Nama"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_LN_KodePos, DTR("CORP_LN_KodePos"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_NPWP, DTR("CORP_NPWP"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_TujuanTransaksi, DTR("CORP_TujuanTransaksi"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_SumberDana, DTR("CORP_SumberDana"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_NamaBankLain, DTR("CORP_NamaBankLain"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_NomorRekeningTujuan, DTR("CORP_NomorRekeningTujuan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_NamaJalan, DTR("CORP_NamaJalan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_RTRW, DTR("CORP_RTRW"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_KodePos, DTR("CORP_KodePos"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_LN_NamaJalan, DTR("CORP_LN_NamaJalan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.NamaKantorPJK, DTR("NamaKantorPJK"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.NomorRekening, DTR("NomorRekening"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_Gelar, DTR("INDV_Gelar"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NamaLengkap, DTR("INDV_NamaLengkap"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_TempatLahir, DTR("INDV_TempatLahir"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_DOM_NamaJalan, DTR("INDV_DOM_NamaJalan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_DOM_RTRW, DTR("INDV_DOM_RTRW"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_DOM_KodePos, DTR("INDV_DOM_KodePos"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_ID_NamaJalan, DTR("INDV_ID_NamaJalan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_ID_RTRW, DTR("INDV_ID_RTRW"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_Jabatan, DTR("INDV_Jabatan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_PenghasilanRataRata, DTR("INDV_PenghasilanRataRata"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_TempatBekerja, DTR("INDV_TempatBekerja"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_TujuanTransaksi, DTR("INDV_TujuanTransaksi"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_SumberDana, DTR("INDV_SumberDana"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NamaBankLain, DTR("INDV_NamaBankLain"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NomorRekeningTujuan, DTR("INDV_NomorRekeningTujuan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NA_KodePos, DTR("INDV_NA_KodePos"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_ID_KodePos, DTR("INDV_ID_KodePos"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NA_NamaJalan, DTR("INDV_NA_NamaJalan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_Kewarganegaraan, DTR("INDV_Kewarganegaraan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.LastUpdateDate, Date.Now, True, AMLBLL.DataType.oDate)
                    FillOrNothing(.LastUpdateBy, GET_UserName, True, AMLBLL.DataType.oString)
                    temp.Add(LTKTTRANSACTIONCASHIN)
                End With
            End Using

        Next

        Return temp
    End Function



    Function ConvertDTtoLTKT(ByVal DTR As DataRow) As LTKT_ApprovalDetail
        Dim temp As New LTKT_ApprovalDetail
        Using LTKT As New LTKT_ApprovalDetail()
            With LTKT
                FillOrNothing(.PK_LTKT_Id, DTR("PK_LTKT_Id"), True, AMLBLL.DataType.OLong)
                FillOrNothing(.TipeTerlapor, DTR("TipeTerlapor"), True, AMLBLL.DataType.oByte)
                FillOrNothing(.CORP_TipeAlamat, DTR("CORP_TipeAlamat"), True, AMLBLL.DataType.oByte)
                FillOrNothing(.CORP_FK_MsBidangUsaha_Id, DTR("CORP_FK_MsBidangUsaha_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.INDV_FK_MsIDType_Id, DTR("INDV_FK_MsIDType_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.CORP_FK_MsBentukBadanUsaha_Id, DTR("CORP_FK_MsBentukBadanUsaha_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.CORP_FK_MsKecamatan_Id, DTR("CORP_FK_MsKecamatan_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.CORP_FK_MsKotaKab_Id, DTR("CORP_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.CORP_FK_MsProvince_Id, DTR("CORP_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.CORP_FK_MsNegara_Id, DTR("CORP_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.CORP_LN_FK_MsNegara_Id, DTR("CORP_LN_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.CORP_LN_FK_MsProvince_Id, DTR("CORP_LN_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.CORP_LN_MsKotaKab_Id, DTR("CORP_LN_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.FK_MsKepemilikan_ID, DTR("FK_MsKepemilikan_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.INDV_FK_MsNegara_Id, DTR("INDV_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.INDV_DOM_FK_MsKecamatan_Id, DTR("INDV_DOM_FK_MsKecamatan_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.INDV_DOM_FK_MsKotaKab_Id, DTR("INDV_DOM_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.INDV_DOM_FK_MsProvince_Id, DTR("INDV_DOM_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.INDV_DOM_FK_MsNegara_Id, DTR("INDV_DOM_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.INDV_ID_FK_MsKecamatan_Id, DTR("INDV_ID_FK_MsKecamatan_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.INDV_ID_FK_MsKotaKab_Id, DTR("INDV_ID_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.INDV_ID_FK_MsProvince_Id, DTR("INDV_ID_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.INDV_ID_FK_MsNegara_Id, DTR("INDV_ID_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.INDV_NA_FK_MsNegara_Id, DTR("INDV_NA_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.INDV_NA_FK_MsProvince_Id, DTR("INDV_NA_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.INDV_NA_FK_MsKotaKab_Id, DTR("INDV_NA_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.INDV_FK_MsPekerjaan_Id, DTR("INDV_FK_MsPekerjaan_Id"), True, AMLBLL.DataType.oInt)
                FillOrNothing(.INDV_TanggalLahir, DTR("INDV_TanggalLahir"), True, AMLBLL.DataType.oDate)
                FillOrNothing(.TanggalLaporan, DTR("TanggalLaporan"), True, AMLBLL.DataType.oDate)
                FillOrNothing(.CIFNo, DTR("CIFNo"), True, AMLBLL.DataType.oDecimal)
                FillOrNothing(.CORP_FK_MsKelurahan_Id, DTR("CORP_FK_MsKelurahan_Id"), True, AMLBLL.DataType.OLong)
                FillOrNothing(.INDV_ID_FK_MsKelurahan_Id, DTR("INDV_ID_FK_MsKelurahan_Id"), True, AMLBLL.DataType.OLong)
                FillOrNothing(.INDV_DOM_FK_MsKelurahan_Id, DTR("INDV_DOM_FK_MsKelurahan_Id"), True, AMLBLL.DataType.OLong)
                FillOrNothing(.NoLTKTKoreksi, DTR("NoLTKTKoreksi"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_DOM_NamaJalan, DTR("INDV_DOM_NamaJalan"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_DOM_RTRW, DTR("INDV_DOM_RTRW"), True, AMLBLL.DataType.oString)
                FillOrNothing(.NoRekening, DTR("NoRekening"), True, AMLBLL.DataType.oString)
                FillOrNothing(.NamaPJKPelapor, DTR("NamaPJKPelapor"), True, AMLBLL.DataType.oString)
                FillOrNothing(.NamaPejabatPJKPelapor, DTR("NamaPejabatPJKPelapor"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_Gelar, DTR("INDV_Gelar"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_NamaLengkap, DTR("INDV_NamaLengkap"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_TempatLahir, DTR("INDV_TempatLahir"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_Jabatan, DTR("INDV_Jabatan"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_PenghasilanRataRata, DTR("INDV_PenghasilanRataRata"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_TempatBekerja, DTR("INDV_TempatBekerja"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_NA_KodePos, DTR("INDV_NA_KodePos"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_ID_KodePos, DTR("INDV_ID_KodePos"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_NA_NamaJalan, DTR("INDV_NA_NamaJalan"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_DOM_KodePos, DTR("INDV_DOM_KodePos"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_ID_NamaJalan, DTR("INDV_ID_NamaJalan"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_ID_RTRW, DTR("INDV_ID_RTRW"), True, AMLBLL.DataType.oString)
                FillOrNothing(.CORP_NamaJalan, DTR("CORP_NamaJalan"), True, AMLBLL.DataType.oString)
                FillOrNothing(.CORP_RTRW, DTR("CORP_RTRW"), True, AMLBLL.DataType.oString)
                FillOrNothing(.CORP_Nama, DTR("CORP_Nama"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_NomorId, DTR("INDV_NomorId"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_NPWP, DTR("INDV_NPWP"), True, AMLBLL.DataType.oString)
                FillOrNothing(.CORP_LN_KodePos, DTR("CORP_LN_KodePos"), True, AMLBLL.DataType.oString)
                FillOrNothing(.CORP_NPWP, DTR("CORP_NPWP"), True, AMLBLL.DataType.oString)
                FillOrNothing(.CORP_KodePos, DTR("CORP_KodePos"), True, AMLBLL.DataType.oString)
                FillOrNothing(.CORP_LN_NamaJalan, DTR("CORP_LN_NamaJalan"), True, AMLBLL.DataType.oString)
                FillOrNothing(.JenisLaporan, DTR("JenisLaporan"), True, AMLBLL.DataType.oString)
                FillOrNothing(.INDV_Kewarganegaraan, DTR("INDV_Kewarganegaraan"), True, AMLBLL.DataType.oString)
                FillOrNothing(.CreatedDate, Date.Now, True, AMLBLL.DataType.oDate)
                FillOrNothing(.CreatedBy, GET_UserName, True, AMLBLL.DataType.oString)
                FillOrNothing(.LastUpdateDate, Date.Now, True, AMLBLL.DataType.oDate)
            End With
            temp = LTKT
        End Using

        Return temp
    End Function
    Function ConvertDTtoLTKT(ByVal DT As Data.DataTable) As TList(Of LTKT_ApprovalDetail)
        Dim temp As New TList(Of LTKT_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using LTKT As New LTKT_ApprovalDetail()
                With LTKT
                    FillOrNothing(.PK_LTKT_Id, DTR("PK_LTKT_Id"), True, AMLBLL.DataType.OLong)
                    FillOrNothing(.TipeTerlapor, DTR("TipeTerlapor"), True, AMLBLL.DataType.oByte)
                    FillOrNothing(.CORP_TipeAlamat, DTR("CORP_TipeAlamat"), True, AMLBLL.DataType.oByte)
                    FillOrNothing(.CORP_FK_MsBidangUsaha_Id, DTR("CORP_FK_MsBidangUsaha_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_FK_MsIDType_Id, DTR("INDV_FK_MsIDType_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_FK_MsBentukBadanUsaha_Id, DTR("CORP_FK_MsBentukBadanUsaha_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_FK_MsKecamatan_Id, DTR("CORP_FK_MsKecamatan_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_FK_MsKotaKab_Id, DTR("CORP_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_FK_MsProvince_Id, DTR("CORP_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_FK_MsNegara_Id, DTR("CORP_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_LN_FK_MsNegara_Id, DTR("CORP_LN_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_LN_FK_MsProvince_Id, DTR("CORP_LN_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.CORP_LN_MsKotaKab_Id, DTR("CORP_LN_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.FK_MsKepemilikan_ID, DTR("FK_MsKepemilikan_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_FK_MsNegara_Id, DTR("INDV_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_DOM_FK_MsKecamatan_Id, DTR("INDV_DOM_FK_MsKecamatan_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_DOM_FK_MsKotaKab_Id, DTR("INDV_DOM_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_DOM_FK_MsProvince_Id, DTR("INDV_DOM_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_DOM_FK_MsNegara_Id, DTR("INDV_DOM_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_ID_FK_MsKecamatan_Id, DTR("INDV_ID_FK_MsKecamatan_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_ID_FK_MsKotaKab_Id, DTR("INDV_ID_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_ID_FK_MsProvince_Id, DTR("INDV_ID_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_ID_FK_MsNegara_Id, DTR("INDV_ID_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_NA_FK_MsNegara_Id, DTR("INDV_NA_FK_MsNegara_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_NA_FK_MsProvince_Id, DTR("INDV_NA_FK_MsProvince_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_NA_FK_MsKotaKab_Id, DTR("INDV_NA_FK_MsKotaKab_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_FK_MsPekerjaan_Id, DTR("INDV_FK_MsPekerjaan_Id"), True, AMLBLL.DataType.oInt)
                    FillOrNothing(.INDV_TanggalLahir, DTR("INDV_TanggalLahir"), True, AMLBLL.DataType.oDate)
                    FillOrNothing(.TanggalLaporan, DTR("TanggalLaporan"), True, AMLBLL.DataType.oDate)
                    FillOrNothing(.CIFNo, DTR("CIFNo"), True, AMLBLL.DataType.oDecimal)
                    FillOrNothing(.CORP_FK_MsKelurahan_Id, DTR("CORP_FK_MsKelurahan_Id"), True, AMLBLL.DataType.OLong)
                    FillOrNothing(.INDV_ID_FK_MsKelurahan_Id, DTR("INDV_ID_FK_MsKelurahan_Id"), True, AMLBLL.DataType.OLong)
                    FillOrNothing(.INDV_DOM_FK_MsKelurahan_Id, DTR("INDV_DOM_FK_MsKelurahan_Id"), True, AMLBLL.DataType.OLong)
                    FillOrNothing(.NoLTKTKoreksi, DTR("NoLTKTKoreksi"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_DOM_NamaJalan, DTR("INDV_DOM_NamaJalan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_DOM_RTRW, DTR("INDV_DOM_RTRW"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.NoRekening, DTR("NoRekening"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.NamaPJKPelapor, DTR("NamaPJKPelapor"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.NamaPejabatPJKPelapor, DTR("NamaPejabatPJKPelapor"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_Gelar, DTR("INDV_Gelar"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NamaLengkap, DTR("INDV_NamaLengkap"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_TempatLahir, DTR("INDV_TempatLahir"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_Jabatan, DTR("INDV_Jabatan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_PenghasilanRataRata, DTR("INDV_PenghasilanRataRata"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_TempatBekerja, DTR("INDV_TempatBekerja"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NA_KodePos, DTR("INDV_NA_KodePos"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_ID_KodePos, DTR("INDV_ID_KodePos"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NA_NamaJalan, DTR("INDV_NA_NamaJalan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_DOM_KodePos, DTR("INDV_DOM_KodePos"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_ID_NamaJalan, DTR("INDV_ID_NamaJalan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_ID_RTRW, DTR("INDV_ID_RTRW"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_NamaJalan, DTR("CORP_NamaJalan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_RTRW, DTR("CORP_RTRW"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_Nama, DTR("CORP_Nama"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NomorId, DTR("INDV_NomorId"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_NPWP, DTR("INDV_NPWP"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_LN_KodePos, DTR("CORP_LN_KodePos"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_NPWP, DTR("CORP_NPWP"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_KodePos, DTR("CORP_KodePos"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CORP_LN_NamaJalan, DTR("CORP_LN_NamaJalan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.JenisLaporan, DTR("JenisLaporan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.INDV_Kewarganegaraan, DTR("INDV_Kewarganegaraan"), True, AMLBLL.DataType.oString)
                    FillOrNothing(.CreatedDate, Date.Now, True, AMLBLL.DataType.oDate)
                    FillOrNothing(.CreatedBy, GET_UserName, True, AMLBLL.DataType.oString)
                    FillOrNothing(.LastUpdateDate, Date.Now, True, AMLBLL.DataType.oDate)
                    temp.Add(LTKT)
                End With
            End Using


        Next


        Return temp
    End Function

    Private Sub ChangeMultiView(ByVal index As Integer)
        MultiViewKPIOvertimeUpload.ActiveViewIndex = index
        If index = 0 Then
            Imgbtnsave.Visible = False
            Imgbtncancel.Visible = False
        ElseIf index = 1 Then
            Imgbtncancel.Visible = True
        ElseIf index = 2 Then
            Imgbtncancel.Visible = False
            Imgbtnsave.Visible = False
        End If
    End Sub



    Private Function GenerateGroupingLTKT() As List(Of AMLBLL.LTKTBLL.DT_GrouP_LTKT)
        Dim Ltemp As New List(Of AMLBLL.LTKTBLL.DT_GrouP_LTKT)
        For i As Integer = 0 To DT_LTKT.Rows.Count - 1
            Dim LTKT As New AMLBLL.LTKTBLL.DT_GrouP_LTKT
            With LTKT
                .LTKT = DT_LTKT.Rows(i)
                .L_LTKTTransactionCashIn = DT_LTKTTransactionCashIn.Clone
                .L_LTKTTransactionCashOut = DT_LTKTTransactionCashOut.Clone
                .L_LTKTDetailCashInTransaction = DT_LTKTDetailCashInTransaction.Clone
                .L_LTKTDetailCashOutTransaction = DT_LTKTDetailCashOutTransaction.Clone

                'Insert Cashin
                Dim LTKT_ID As String = .LTKT("PK_LTKT_ID")
                If DT_LTKTTransactionCashIn.Rows.Count > 0 Then
                    Using DV As New DataView(DT_LTKTTransactionCashIn)
                        DV.RowFilter = "FK_LTKT_ID='" & LTKT_ID & "'"
                        If DV.Count > 0 Then
                            Dim temp As Data.DataTable = DV.ToTable
                            For Each H As DataRow In temp.Rows
                                .L_LTKTTransactionCashIn.ImportRow(H)
                            Next
                        End If
                    End Using



                End If
                'Insert Cashout
                If DT_LTKTTransactionCashOut.Rows.Count > 0 Then
                    Using DV As New DataView(DT_LTKTTransactionCashOut)
                        DV.RowFilter = "FK_LTKT_ID='" & LTKT_ID & "'"
                        Dim temp As Data.DataTable = DV.ToTable
                        If DV.Count > 0 Then
                            For Each H As DataRow In temp.Rows
                                .L_LTKTTransactionCashOut.ImportRow(H)
                            Next
                        End If
                    End Using
                End If

                'Insert Detail Cashin
                If DT_LTKTDetailCashInTransaction.Rows.Count > 0 Then
                    For Each n As DataRow In .L_LTKTTransactionCashIn.Rows
                        Dim LTKTDetailCashInTransaction As String = n("PK_LTKTTransactionCashIn_ID")
                        Using DV As New DataView(DT_LTKTDetailCashInTransaction)
                            DV.RowFilter = "FK_LTKTTransactionCashIn_ID='" & LTKTDetailCashInTransaction & "'"
                            If DV.Count > 0 Then
                                Dim temp As Data.DataTable = DV.ToTable
                                For Each H As DataRow In temp.Rows
                                    .L_LTKTDetailCashInTransaction.ImportRow(H)
                                Next
                            End If
                        End Using
                    Next
                End If

                'Insert Detail CashOut
                If DT_LTKTDetailCashOutTransaction.Rows.Count > 0 Then
                    For Each n As DataRow In .L_LTKTTransactionCashOut.Rows
                        Dim LTKTDetailCashOutTransaction As String = n("PK_LTKTTransactionCashOut_ID")
                        Using DV As New DataView(DT_LTKTDetailCashOutTransaction)
                            DV.RowFilter = "FK_LTKTTransactionCashOut_ID='" & LTKTDetailCashOutTransaction & "'"
                            If DV.Count > 0 Then
                                Dim temp As Data.DataTable = DV.ToTable
                                For Each H As DataRow In temp.Rows
                                    .L_LTKTDetailCashOutTransaction.ImportRow(H)
                                Next
                            End If
                        End Using
                    Next
                End If

            End With
            Ltemp.Add(LTKT)
        Next
        Return Ltemp
    End Function

    Public Sub BindGrid()
        lblValueFailedData.Text = SetnGetDataTableForAnomalyData.Rows.Count
        GridAnomalyList.DataSource = SetnGetDataTableForAnomalyData
        GridAnomalyList.DataBind()
        lblValueSuccessData.Text = SetnGetDataTableForSuccessData.Count
        GridSuccessList.DataSource = SetnGetDataTableForSuccessData
        lblValueuploadeddata.Text = DT_LTKT.Rows.Count
        GridSuccessList.DataBind()
    End Sub

#End Region

#Region "events..."


    Protected Sub ImgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtncancel.Click
        ClearThisPageSessions()
        ChangeMultiView(0)
        LinkButtonExportExcel.Visible = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If IsPostBack Then
                Return
            End If

            ClearThisPageSessions()
            SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            'AuditTrailAccess(Me.Page)
            ChangeMultiView(0)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        ChangeMultiView(0)
    End Sub

    Protected Sub GridAnomalyList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridAnomalyList.PageIndexChanged
        GridAnomalyList.CurrentPageIndex = e.NewPageIndex
        BindGrid()
    End Sub


    Protected Sub GridSuccessList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridSuccessList.PageIndexChanged
        Dim GridTarget As DataGrid = CType(source, DataGrid)
        Try
            GridSuccessList.CurrentPageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageErr.PreRender
        If cvalPageErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Protected Sub imgSaveSuccessData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtnsave.Click
        Try

            If DT_LTKT Is Nothing Then
                Throw New Exception("Tidak data untuk diproses")
            End If
            Dim KeyApp As String
            Using Aproval As New LTKT_Approval()
                With Aproval
                    .RequestedBy = GET_PKUserID
                    .RequestedDate = Date.Now
                    .FK_MsMode_Id = 2
                    .IsUpload = True
                End With
                DataRepository.LTKT_ApprovalProvider.Save(Aproval)
                KeyApp = Aproval.PK_LTKT_Approval_Id
            End Using

            For k As Integer = 0 To GroupTableSuccessData.Count - 1
                'For Each LTKT As GrouP_LTKT_success In GroupTableSuccessData

                Dim LTKT As GrouP_LTKT_success = GroupTableSuccessData(k)


                With LTKT
                    .LTKT.FK_LTKT_Approval_Id = KeyApp
                    For Each obj As LTKTTransactionCashIn_ApprovalDetail In .L_LTKTTransactionCashIn
                        obj.FK_LTKT_ApprovalDetail_Id = KeyApp
                    Next

                    For Each obj As LTKTTransactionCashOut_ApprovalDetail In .L_LTKTTransactionCashOut
                        obj.FK_LTKT_ApprovalDetail_Id = KeyApp
                    Next

                    For Each obj As LTKTDetailCashInTransaction_ApprovalDetail In .L_LTKTDetailCashInTransaction
                        obj.FK_LTKT_Approval_Id = KeyApp
                    Next

                    For Each obj As LTKTDetailCashOutTransaction_ApprovalDetail In .L_LTKTDetailCashOutTransaction
                        obj.FK_LTKT_Approval_Id = KeyApp
                    Next

                    DataRepository.LTKT_ApprovalDetailProvider.Save(.LTKT)
                    DataRepository.LTKTTransactionCashIn_ApprovalDetailProvider.Save(.L_LTKTTransactionCashIn)
                    DataRepository.LTKTTransactionCashOut_ApprovalDetailProvider.Save(.L_LTKTTransactionCashOut)
                    DataRepository.LTKTDetailCashInTransaction_ApprovalDetailProvider.Save(.L_LTKTDetailCashInTransaction)
                    DataRepository.LTKTDetailCashOutTransaction_ApprovalDetailProvider.Save(.L_LTKTDetailCashOutTransaction)
                End With
            Next

            'AuditTrail(Nothing, "LTKT Upload-" & SetnGetDataTableForSuccessData.Count, SahassaNettier.Entities.MsOperationAuditTrailList.Add, Nothing, Nothing)
            LblConfirmation.Text = "LTKT data has edited (" & SetnGetDataTableForSuccessData.Count & " Rows)"
            ChangeMultiView(2)
            ''Kirim Email ke semua orang yang 1 group sama pembuat/pengedit/pendelete data

            Dim ApproverUser As String = vbNullString
            Dim EmailBodyData As String = vbNullString

            'EmailBodyData = EmailBLL.getEmailBodyTemplate("LTKT Upload", "LTKT")  '"Approval WIC Add (User Id : " & Sahassa.AML.Commonly.SessionNIK & "-" & Sahassa.AML.Commonly.SessionStaffName & ")<br>Is need your approval"

            'Using objMsUser As TList(Of User) = DataRepository.UserProvider.GetPaged(UserColumn.UserGroupId.ToString & " = " & Sahassa.AML.Commonly.SessionGroupId & "'", "", 0, Integer.MaxValue, 0)
            '    'Using objMsUser As TList(Of User) = DataRepository.UserProvider.GetPaged(UserColumn.UserGroupId.ToString & " = " & Sahassa.AML.Commonly.SessionGroupId & " and MsUser_IsBankWide =  '" & Sahassa.AML.Commonly.SessionIsBankWide & "'", "", 0, Integer.MaxValue, 0)
            '    If objMsUser.Count > 0 Then
            '        For Each singleObjMsUser As User In objMsUser
            '            If singleObjMsUser.pkUserID <> Sahassa.AML.Commonly.SessionPkUserId Then
            '                ApproverUser += singleObjMsUser.UserEmailAddress & ";"
            '            End If
            '        Next

            '        'Using CommonEmail As New Sahassa.AML.EmailSend
            '        '    CommonEmail.Subject = "LTKT Upload" '"Approval LTKT Upload (User Id : " & Sahassa.AML.Commonly.SessionNIK & "-" & Sahassa.AML.Commonly.SessionStaffName & ")"
            '        '    CommonEmail.Sender = "Cash Transaction Report Application"
            '        '    CommonEmail.Recipient = ApproverUser
            '        '    CommonEmail.Body = EmailBodyData
            '        '    CommonEmail.SendEmail()
            '        'End Using
            '    End If
            'End Using

            'End If
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub imgUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgUpload.Click
        Dim CombinedFileName As String
        GroupTableSuccessData = New List(Of GrouP_LTKT_success)
        GroupTableAnomalyData = New List(Of AMLBLL.LTKTBLL.DT_GrouP_LTKT)
        Try
            If FileUploadKPI.FileName = "" Then
                Throw New Exception("Tidak ada data untuk diproses")
            End If

            If Path.GetExtension(FileUploadKPI.FileName) <> ".xls" Then
                Throw New Exception("Invalid File Upload Extension")
            End If

            CombinedFileName = SaveFileImportToSave()
            If String.Compare(CombinedFileName, "", False) <> 0 Then
                Try
                    ProcessExcelFile(FilePath)
                Catch ex As Exception
                    Throw New Exception(CStr(GetLocalResourceObject("FileInvalid")))
                End Try
            Else
                Throw New Exception(CStr(GetLocalResourceObject("FileInvalid")))
            End If

            'Import from Excell to property Global
            ImportExcelTempToDTproperty()

            Dim LG_LTKT As List(Of AMLBLL.LTKTBLL.DT_GrouP_LTKT) = GenerateGroupingLTKT()
            DT_LTKT.Columns.Add("Description")
            DT_LTKT.Columns.Add("errorline")

            'Membuat tempory untuk yang anomali dan yang sukses
            Dim DTnormal As Data.DataTable = DT_LTKT.Clone
            Dim DTAnomaly As Data.DataTable = DT_LTKT.Clone

            'disini proses datanya masing - masing group LTKT 
            For i As Integer = 0 To LG_LTKT.Count - 1
                Dim Lerror As List(Of String) = ValidateInput(LG_LTKT(i))
                Dim MSG As String = Lerror(0)
                Dim LINE As String = Lerror(1)
                If MSG = "" Then
                    DTnormal.ImportRow(LG_LTKT(i).LTKT)
                    Dim GroupLTKT As New GrouP_LTKT_success
                    With GroupLTKT
                        .LTKT = ConvertDTtoLTKT(LG_LTKT(i).LTKT)
                        .L_LTKTTransactionCashIn = ConvertDTtoLTKTTRANSACTIONCASHIN(LG_LTKT(i).L_LTKTTransactionCashIn)
                        .L_LTKTTransactionCashOut = ConvertDTtoLTKTTRANSACTIONCASHOUT(LG_LTKT(i).L_LTKTTransactionCashOut)
                        .L_LTKTDetailCashInTransaction = ConvertDTtoLTKTDETAILCASHINTRANSACTION(LG_LTKT(i).L_LTKTDetailCashInTransaction)
                        .L_LTKTDetailCashOutTransaction = ConvertDTtoLTKTDETAILCASHOUTTRANSACTION(LG_LTKT(i).L_LTKTDetailCashOutTransaction)
                    End With
                    GroupTableSuccessData.Add(GroupLTKT)
                Else
                    DT_LTKT.Rows(i)("Description") = MSG
                    DT_LTKT.Rows(i)("errorline") = LINE
                    DTAnomaly.ImportRow(DT_LTKT.Rows(i))
                    GroupTableAnomalyData.Add(LG_LTKT(i))
                End If

            Next

            SetnGetDataTableForSuccessData = ConvertDTtoLTKT(DTnormal)
            SetnGetDataTableForAnomalyData = DTAnomaly

            'lbljumlahsukses.Text = DTnormal.Rows.Count & "/" & DT_LTKT.Rows.Count
            'lbljumlahanomalydata.Text = DTAnomaly.Rows.Count & "/" & DT_LTKT.Rows.Count

            If DTAnomaly.Rows.Count > 0 Then
                Imgbtnsave.Visible = False
                Imgbtncancel.Visible = True
            Else
                Imgbtnsave.Visible = True
                Imgbtncancel.Visible = True
            End If

            BindGrid()
            If GridAnomalyList.Items.Count < 1 Then
                LinkButtonExportExcel.Visible = False
            Else
                LinkButtonExportExcel.Visible = True
            End If
            ChangeMultiView(1)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Dim Exp As New AMLBLL.LTKTBLL
            Dim FolderPath As String = Server.MapPath("FolderTemplate")
            'Dim FolderHasil As String = Server.MapPath("FolderExport")
            'Exp.ExportDTtoExcel(GroupTableAnomalyData, FolderPath, FolderHasil)
            Dim buffer As Byte() = Exp.ExportDTtoExcel(GroupTableAnomalyData, FolderPath)
            'Response.Redirect("DownloadAttachment.aspx?IsFile=Y&FileName=" & FolderHasil & "\LTKTReportTemplateDownload.xls")
            Response.AddHeader("Content-type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment; filename=WICReportTemplateDownload.xls")
            Response.BinaryWrite(buffer)
            Response.Flush()
            Response.End()

        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered 

    End Sub

End Class


