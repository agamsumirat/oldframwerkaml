<%@ Page Language="VB" AutoEventWireup="false" ValidateRequest="false" CodeFile="LTKT_APPROVALDETAIL_View.aspx.vb"
    Inherits="LTKT_APPROVALDETAIL_View" MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script src="Script/Calendar/CalendarPopup.js"></script>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                            
                        </td>
                        <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <ajax:AjaxPanel ID="AjaxPanel15" runat="server">
                    <div id="divcontent" class="divcontent">
                        <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td>
                                    <img src="Images/blank.gif" width="20" height="100%" />
                                </td>
                                <td class="divcontentinside" bgcolor="#FFFFFF">
                                    &nbsp;<ajax:AjaxPanel ID="a" runat="server"><asp:ValidationSummary ID="ValidationSummaryunhandle"
                                        runat="server" CssClass="validation" />
                                        <asp:ValidationSummary ID="ValidationSummaryhandle" runat="server" CssClass="validationok"
                                            ForeColor="Black" ValidationGroup="handle" />
                                    </ajax:AjaxPanel>
                                    <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%">
                                        <table id="title" border="0" width="100%">
                                            <tr bgcolor="#ffffff">
                                                <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                    <strong>
                                                        <img src="Images/dot_title.gif" width="17" height="17">
                                                        <asp:Label ID="Label7" runat="server" Text="CIF APROVAL DETAIL"></asp:Label>
                                                        <hr />
                                                    </strong>&nbsp;&nbsp;&nbsp;&nbsp;<ajax:AjaxPanel ID="AjxMessage" runat="server">
                                                        <asp:Label ID="LblMessage" background="images/validationbground.gif" runat="server"
                                                            CssClass="validationok" Width="100%"></asp:Label></ajax:AjaxPanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </ajax:AjaxPanel>
                                    <asp:MultiView ID="MultiViewUtama" runat="server" Visible>
                                        <asp:View ID="View1" runat="server">
                                            <asp:Panel ID="Panel1" runat="server" Width="100%">
                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                                    border="2">
                                                    <tr>
                                                        <td colspan="2" background="images/search-bar-background.gif" height="18" valign="middle"
                                                            width="100%" align="right" style="text-align: left">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td style="height: 16px">
                                                                        &nbsp;Header&nbsp;&nbsp;
                                                                    </td>
                                                                    <td style="height: 16px">
                                                                        <a href="#" onclick="javascript:UpdateSearchBox()" title="click to minimize or maximize">
                                                                            <img id="searchimage" src="images/search-bar-minimize.gif" border="0" style="width: 19px;
                                                                                height: 14px"></a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="searchbox">
                                                        <td colspan="2" valign="top" width="98%" bgcolor="#ffffff">
                                                            <table cellpadding="0" width="100%" border="0">
                                                                <tr>
                                                                    <td valign="middle" nowrap>
                                                                        <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">
                                                                            <table style="height: 100%">
                                                                                <tr>
                                                                                    <td style="height: 7px">
                                                                                    </td>
                                                                                    <td style="height: 7px">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td style="height: 7px">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Request Date
                                                                                    </td>
                                                                                    <td>
                                                                                        :
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="lblRequestDate" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Request By
                                                                                    </td>
                                                                                    <td>
                                                                                        :
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="lblRequestBy" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            &nbsp;&nbsp;&nbsp;
                                                                        </ajax:AjaxPanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                                                                <asp:CustomValidator ID="CvalPageErr" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
                                                            <ajax:AjaxPanel ID="Ajaxpanel2" runat="server">
                                                                <asp:CustomValidator ID="CvalHandleErr" runat="server" ValidationGroup="handle" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                                border="2">
                                                <tr>
                                                    <td bgcolor="#ffffff">
                                                        <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                                                            <asp:DataGrid ID="GridMsUserView" runat="server" AllowCustomPaging="True" AutoGenerateColumns="False"
                                                                Font-Size="XX-Small" CellSpacing="1" CellPadding="4" SkinID="gridview" AllowPaging="True"
                                                                Width="100%" GridLines="None" AllowSorting="True" ForeColor="#333333" Font-Bold="False"
                                                                Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
                                                                HorizontalAlign="Left">
                                                                <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                                <SelectedItemStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedItemStyle>
                                                                <PagerStyle Mode="NumericPages" BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"
                                                                    Visible="False"></PagerStyle>
                                                                <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
                                                                <HeaderStyle Width="200px" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                    Font-Strikeout="False" Font-Underline="False" Wrap="False" HorizontalAlign="Left"
                                                                    VerticalAlign="Middle"></HeaderStyle>
                                                                <Columns>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server"></asp:CheckBox>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="10px" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="PK_LTKT_Approval_Id" Visible="False"></asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="No">
                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" ForeColor="White" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="NamaPJKPelapor" HeaderText="Name" SortExpression="NamaPJKPelapor asc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="INDV_TanggalLahir" HeaderText="Date of Birth" SortExpression="INDV_TanggalLahir desc"
                                                                        DataFormatString="{0:dd-MMM-yyyy}"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="MsIDType_Name" HeaderText="ID Type" SortExpression="MsIDType_Name asc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="INDV_NomorId" HeaderText="ID Number" SortExpression="INDV_NomorId asc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="INDV_NPWP" HeaderText="NPWP" SortExpression="INDV_NPWP asc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="DebetCredit" HeaderText="DebetCredit" SortExpression="DebetCredit asc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="brsnchIn" HeaderText="Cash In" SortExpression="brsnchIn asc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="BranchOut" HeaderText="Cash Out" SortExpression="BranchOut asc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="nominalIn" HeaderText="Cash in" SortExpression="nominalIn desc"
                                                                        DataFormatString="{0:##,0}"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="nominalOut" HeaderText="Cash Out" SortExpression="nominalOut desc"
                                                                        DataFormatString="{0:##,0}"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="jumlahin" HeaderText="Cash In" SortExpression="jumlahin desc"
                                                                        DataFormatString="{0:##,0}"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Jumlahout" HeaderText="Cash Out" SortExpression="jumlahout desc"
                                                                        DataFormatString="{0:##,0}"></asp:BoundColumn>
                                                                    <asp:HyperLinkColumn Text="Detail" DataNavigateUrlField="PK_LTKT_ApprovalDetail_Id"
                                                                        DataNavigateUrlFormatString="LTKT_UploadApprovalDetail.aspx?Approval_ID={0}"></asp:HyperLinkColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                            <br />
                                                            <br />
                                                            <asp:Label ID="LabelNoRecordFound" runat="server" Text="No record match with your criteria"
                                                                CssClass="text" Visible="False"></asp:Label></ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="background-color: #ffffff">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td nowrap>
                                                                    <ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                                                        &nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
                                                                        &nbsp;
                                                                    </ajax:AjaxPanel>
                                                                </td>
                                                                <td style="width: 92%">
                                                                    &nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" ajaxcall="none" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                        runat="server">Export 
				to Excel</asp:LinkButton>&nbsp;
                                                                    <asp:LinkButton ID="lnkExportAllData" runat="server" ajaxcall="none" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                        Text="Export All to Excel"></asp:LinkButton>
                                                                </td>
                                                                <td width="99%" style="text-align: right">
                                                                    &nbsp;
                                                                </td>
                                                                <td align="right" nowrap>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#ffffff">
                                                        <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                            bgcolor="#ffffff" border="2">
                                                            <tr class="regtext" align="center" bgcolor="#dddddd">
                                                                <td valign="top" align="left" width="50%" bgcolor="#ffffff" style="height: 99px">
                                                                    Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel7" runat="server"><asp:Label ID="PageCurrentPage"
                                                                        runat="server" CssClass="regtextbold">0</asp:Label>&nbsp;of&nbsp;
                                                                        <asp:Label ID="PageTotalPages" runat="server" CssClass="regtextbold">0</asp:Label></ajax:AjaxPanel>
                                                                </td>
                                                                <td valign="top" align="right" width="50%" bgcolor="#ffffff" style="height: 99px">
                                                                    Total Records&nbsp;
                                                                    <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                                                        <asp:Label ID="PageTotalRows" runat="server">0</asp:Label></ajax:AjaxPanel>
                                                                </td>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                bgcolor="#ffffff" border="2">
                                                <tr bgcolor="#ffffff">
                                                    <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                                                        <hr color="#f40101" noshade size="1">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                                                        Go to page
                                                    </td>
                                                    <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                                                        <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                            <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                                                <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox></ajax:AjaxPanel>
                                                        </font>
                                                    </td>
                                                    <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                                                        <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                                            <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton" ImageUrl="~/Images/button/go.gif">
                                                            </asp:ImageButton></ajax:AjaxPanel>
                                                    </td>
                                                    <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                        <img height="5" src="images/first.gif" width="6">
                                                    </td>
                                                    <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                                        <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                                            <asp:LinkButton ID="First" runat="server" CssClass="regtext" CommandName="First"
                                                                OnCommand="PageNavigate">First</asp:LinkButton></ajax:AjaxPanel>
                                                    </td>
                                                    <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                                        <img height="5" src="images/prev.gif" width="6">
                                                    </td>
                                                    <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
                                                        <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                                            <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                                                OnCommand="PageNavigate">Previous</asp:LinkButton></ajax:AjaxPanel>
                                                    </td>
                                                    <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
                                                        <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                                            <a class="pageNav" href="#">
                                                                <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                                                    OnCommand="PageNavigate">Next</asp:LinkButton></a></ajax:AjaxPanel>
                                                    </td>
                                                    <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                        <img height="5" src="images/next.gif" width="6">
                                                    </td>
                                                    <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
                                                        <ajax:AjaxPanel ID="AjaxPanel16" runat="server">
                                                            <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                                                OnCommand="PageNavigate">Last</asp:LinkButton></ajax:AjaxPanel>
                                                    </td>
                                                    <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                        <img height="5" src="images/last.gif" width="6">
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                        <asp:View ID="RejectReason" runat="server">
                                            <table width="100%" style="horiz-align: center;">
                                                <tr>
                                                    <td>
                                                        <table border="0" align="center">
                                                            <tr>
                                                                <td class="formtext" align="left" style="height: 15px">
                                                                    <asp:Label runat="server" ID="lblReject" Text="Reject Reason"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtReasonReject" runat="server" Columns="100" Rows="12" TextMode="MultiLine"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="formtext" align="center" style="height: 27px">
                                                        <asp:ImageButton ID="imgOkRejectReason" runat="server" SkinID="AddButton" ImageUrl="~/Images/button/Ok.gif"
                                                            Style="width: 40px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </asp:MultiView>
                                </td>
                            </tr>
                        </table>
                    </div>
                </ajax:AjaxPanel>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <ajax:AjaxPanel ID="AjaxPanel14" runat="server" Width="100%">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td background="Images/button-bground.gif" align="left" valign="middle">
                                <img src="Images/blank.gif" width="5" height="1" /></td>
                            <td background="Images/button-bground.gif" align="left" valign="middle">
                                <img src="images/arrow.gif" width="15" height="15" /></td>
                            <td background="Images/button-bground.gif" style="width: 5px">
                                &nbsp;</td>
                            <td background="Images/button-bground.gif">
                                <asp:ImageButton ID="imgApprove" runat="server" ImageUrl="~/Images/button/accept.gif" />&nbsp;</td>
                            <td background="Images/button-bground.gif">
                                &nbsp;<asp:ImageButton ID="imgReject" runat="server" CausesValidation="False" SkinID="CancelButton"
                                    ImageUrl="~/Images/button/reject.gif"></asp:ImageButton></td>
                            <td background="Images/button-bground.gif">
                                &nbsp;<asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"
                                    ImageUrl="~/Images/button/back.gif"></asp:ImageButton></td>
                            <td width="99%" background="Images/button-bground.gif">
                                <img src="Images/blank.gif" width="1" height="1" /></td>
                            <td>
                                </td>
                        </tr>
                    </table>
                    <asp:CustomValidator ID="CustomValidator1" runat="server" Display="None" ValidationGroup="handle"></asp:CustomValidator><asp:CustomValidator
                        ID="CustomValidator2" runat="server" Display="None"></asp:CustomValidator>
                </ajax:AjaxPanel>
            </td>
        </tr>
    </table>
</asp:Content>
