
Partial Class PopupWindow
    Inherits System.Web.UI.Page

#Region "Property"
    ''' <summary>
    ''' Property SetnGetSelectedUserId
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedUserId() As ArrayList
        Get
            Return Session("SelectedUserId")
        End Get
        Set(ByVal value As ArrayList)
            Session("SelectedUserId") = value
        End Set
    End Property
#End Region

#Region "Init and Load "
    ''' <summary>
    ''' Change Id TxtTampung
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub TxtTampung_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtTampung.Init
        Me.TxtTampung.ID = Me.Page.Request.Params.Get("clientid")
    End Sub

    ''' <summary>
    ''' Load Data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Session("SelectedUserId") = Nothing
                BindGrid()
                'Dim StrClientID As String = Me.Page.Request.Params.Get("clientid")
                'ImageButtonSend.Attributes.Add("onClick", "return mainValues('" + StrClientID + "');")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

#Region "BindGrid"
    ''' <summary>
    ''' Bind grid
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindGrid()
        Try
            Using oAccessUserID As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectAllUserIDTableAdapter
                Me.GridViewUserID.DataSource = oAccessUserID.GetData
                Me.GridViewUserID.DataBind()
            End Using
        Catch
            Throw
        End Try
    End Sub

#End Region

    '#Region "Selected Checkbox"

    '    ''' <summary>
    '    ''' Tampung UserId yang di check
    '    ''' </summary>
    '    ''' <param name="sender"></param>
    '    ''' <param name="e"></param>
    '    ''' <remarks></remarks>

    '    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '        Try
    '            Dim Arruserid As New ArrayList
    '            For Each Rows As GridViewRow In Me.GridViewUserID.Rows
    '                Dim CkhBox As CheckBox = CType(Rows.Cells(0).Controls(1), CheckBox)
    '                If CkhBox.Checked = True Then
    '                    Arruserid.Add(Rows.Cells(1).Text)
    '                End If
    '            Next
    '            Me.SetnGetSelectedUserId = Arruserid
    '            Me.TxtTampung.Text = ""
    '            For i As Integer = 0 To SetnGetSelectedUserId.Count - 1
    '                If i = SetnGetSelectedUserId.Count - 1 Then
    '                    Me.TxtTampung.Text &= SetnGetSelectedUserId(i).ToString
    '                Else
    '                    Me.TxtTampung.Text &= SetnGetSelectedUserId(i).ToString & ";"
    '                End If
    '            Next
    '        Catch ex As Exception
    '            Me.cvalPageError.IsValid = False
    '            Me.cvalPageError.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '#End Region

    
    
    Protected Sub ImageButtonSend_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSend.Click
        Try
            Dim Arruserid As New ArrayList
            For Each Rows As GridViewRow In Me.GridViewUserID.Rows
                Dim CkhBox As CheckBox = CType(Rows.Cells(0).Controls(1), CheckBox)
                If CkhBox.Checked = True Then
                    Arruserid.Add(Rows.Cells(1).Text)
                End If
            Next
            Me.SetnGetSelectedUserId = Arruserid
            Me.TxtTampung.Text = ""
            For i As Integer = 0 To Arruserid.Count - 1
                If i = Arruserid.Count - 1 Then
                    Me.TxtTampung.Text &= Arruserid(i).ToString
                Else
                    Me.TxtTampung.Text &= Arruserid(i).ToString & ";"
                End If
            Next
            Dim StrClientID As String = Me.Page.Request.Params.Get("clientid")
            Me.Page.ClientScript.RegisterStartupScript(Me.GetType, "CallmainValues ", "mainValues('" & StrClientID & "')", True)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub
End Class
