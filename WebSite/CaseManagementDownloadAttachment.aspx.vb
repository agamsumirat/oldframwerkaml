
Partial Class CaseManagementDownloadAttachment
    Inherits System.Web.UI.Page
    Private ReadOnly Property PKWorkflowhistoryAttachmentID() As String
        Get
            Dim temp As String = Request.Params("PKWorkflowhistoryAttachmentID")
            If temp.Trim.Length = 0 OrElse Not IsNumeric(temp) Then
                Throw New Exception("AttachmentID is invalid")
            Else
                Return temp
            End If

        End Get
    End Property
    Private _oRowHistoryAttachment As AMLDAL.CaseManagement.MapCaseManagementWorkflowHistoryAttachmentRow
    Public ReadOnly Property oRowHistoryAttachment() As AMLDAL.CaseManagement.MapCaseManagementWorkflowHistoryAttachmentRow
        Get
            If Not _oRowHistoryAttachment Is Nothing Then
                Return _oRowHistoryAttachment
            Else
                Using adapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementWorkflowHistoryAttachmentTableAdapter
                    Using otable As AMLDAL.CaseManagement.MapCaseManagementWorkflowHistoryAttachmentDataTable = adapter.GetMapCaseManagementWorkflowHistoryAttachmentByPK(Me.PKWorkflowhistoryAttachmentID)
                        If otable.Rows.Count > 0 Then
                            _oRowHistoryAttachment = otable.Rows(0)
                            Return _oRowHistoryAttachment
                        Else
                            Throw New Exception("File requested is already deleted. ")
                        End If
                    End Using
                End Using
            End If
            
        End Get
    End Property

    Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        Try
            If Not _oRowHistoryAttachment Is Nothing Then
                _oRowHistoryAttachment = Nothing
            End If
        Catch ex As Exception

        End Try
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=" & oRowHistoryAttachment.FileName)
        Response.Charset = ""
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Me.EnableViewState = False
        Response.ContentType = oRowHistoryAttachment.ContentType
        Response.BinaryWrite(oRowHistoryAttachment.FileSteam)
        Response.End()


    End Sub
End Class
