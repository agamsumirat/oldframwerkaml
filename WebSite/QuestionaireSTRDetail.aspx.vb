Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Imports System.IO
Imports System.Collections.Generic

Partial Class QuestionaireSTRDetail
    Inherits Parent

#Region "Sesion"
    Public ReadOnly Property PKMsQuestionaireSTRID() As Integer
        Get
            Dim strTemp As String = Request.Params("PKMsQuestionaireSTRID")
            Dim intResult As Integer
            If Integer.TryParse(strTemp, intResult) Then
                Return intResult
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Parameter PKMsQuestionaireSTRID is invalid."

            End If
        End Get
    End Property
    Public ReadOnly Property ObjMsQuestionaireSTR() As MsQuestionaireSTR
        Get
            If Session("QuestionaireSTRDetail.ObjMsQuestionaireSTR") Is Nothing Then

                Session("QuestionaireSTRDetail.ObjMsQuestionaireSTR") = DataRepository.MsQuestionaireSTRProvider.GetByPK_MsQuestionaireSTR_ID(PKMsQuestionaireSTRID)

            End If
            Return CType(Session("QuestionaireSTRDetail.ObjMsQuestionaireSTR"), MsQuestionaireSTR)
        End Get
    End Property
    Public ReadOnly Property ObjMsQuestionaireSTRMultipleChoice() As TList(Of MsQuestionaireSTRMultipleChoice)
        Get
            If Session("QuestionaireSTRDetail.ObjMsQuestionaireSTRMultipleChoice") Is Nothing Then

                Session("QuestionaireSTRDetail.ObjMsQuestionaireSTRMultipleChoice") = DataRepository.MsQuestionaireSTRMultipleChoiceProvider.GetPaged("FK_MsQuestionaireSTR_ID = '" & PKMsQuestionaireSTRID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("QuestionaireSTRDetail.ObjMsQuestionaireSTRMultipleChoice"), TList(Of MsQuestionaireSTRMultipleChoice))
        End Get
    End Property
    Private Property SetnGetCurrentPage() As Integer
        Get
            Return CInt(IIf(Session("SetnGetCurrentPage") Is Nothing, "", Session("SetnGetCurrentPage")))
        End Get
        Set(ByVal value As Integer)
            Session("SetnGetCurrentPage") = value
        End Set
    End Property

#End Region

#Region "ClearSession"
    Sub ClearSession()
        Session("QuestionaireSTRDetail.ObjMsQuestionaireSTR") = Nothing
        Session("QuestionaireSTRDetail.ObjMsQuestionaireSTRMultipleChoice") = Nothing
        Session("SetnGetCurrentPage") = Nothing
    End Sub
#End Region

#Region "load mode"
    Private Sub loaddata()

        If Not ObjMsQuestionaireSTR Is Nothing Then
            If ObjMsQuestionaireSTR.FK_QuestionType_ID = 1 Then

                cde.Visible = False

                LblDescriptionAlertSTR.Text = ObjMsQuestionaireSTR.DescriptionAlertSTR
                Using ObjQuestionType As QuestionType = DataRepository.QuestionTypeProvider.GetByPK_QuestionType_ID(ObjMsQuestionaireSTR.FK_QuestionType_ID)
                    LblQuestiontype.Text = ObjQuestionType.QuestionType
                End Using
                LblQuestionNo.Text = CStr(ObjMsQuestionaireSTR.QuestionNo)
                TxtQuestion.Text = ObjMsQuestionaireSTR.Question
                LblActivationValue.Text = CStr(ObjMsQuestionaireSTR.Activation)
                If LblActivationValue.Text = "True" Then
                    LblActivationValue.Text = "Active"
                Else
                    LblActivationValue.Text = "Not Active"
                End If
                LblCreatedBy.Text = ObjMsQuestionaireSTR.CreatedBy & " / " & CStr(ObjMsQuestionaireSTR.CreatedDate)
                LblUpdatedBy.Text = ObjMsQuestionaireSTR.LastUpdateBy & " / " & CStr(ObjMsQuestionaireSTR.LastUpdateDate)
                If ObjMsQuestionaireSTR.ApprovedBy = "" Then
                    LblApprovedBy.Text = ""
                Else
                    LblApprovedBy.Text = ObjMsQuestionaireSTR.ApprovedBy & " / " & CStr(ObjMsQuestionaireSTR.ApprovedDate)
                End If

            ElseIf ObjMsQuestionaireSTR.FK_QuestionType_ID = 2 Then

                cde.Visible = True

                LblDescriptionAlertSTR.Text = ObjMsQuestionaireSTR.DescriptionAlertSTR
                Using ObjQuestionType As QuestionType = DataRepository.QuestionTypeProvider.GetByPK_QuestionType_ID(ObjMsQuestionaireSTR.FK_QuestionType_ID)
                    LblQuestiontype.Text = ObjQuestionType.QuestionType
                End Using
                LblQuestionNo.Text = CStr(ObjMsQuestionaireSTR.QuestionNo)
                TxtQuestion.Text = ObjMsQuestionaireSTR.Question
                gridView.DataSource = ObjMsQuestionaireSTRMultipleChoice
                gridView.DataBind()
                LblActivationValue.Text = CStr(ObjMsQuestionaireSTR.Activation)
                If LblActivationValue.Text = "True" Then
                    LblActivationValue.Text = "Active"
                Else
                    LblActivationValue.Text = "Not Active"
                End If
                LblCreatedBy.Text = ObjMsQuestionaireSTR.CreatedBy & " / " & CStr(ObjMsQuestionaireSTR.CreatedDate)
                LblUpdatedBy.Text = ObjMsQuestionaireSTR.LastUpdateBy & " / " & CStr(ObjMsQuestionaireSTR.LastUpdateDate)
                If ObjMsQuestionaireSTR.ApprovedBy = "" Then
                    LblApprovedBy.Text = ""
                Else
                    LblApprovedBy.Text = ObjMsQuestionaireSTR.ApprovedBy & " / " & CStr(ObjMsQuestionaireSTR.ApprovedDate)
                End If
            End If

        Else
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = QuestionaireSTRBLL.DATA_NOTVALID
        End If
    End Sub
#End Region

#Region "IMGCANCEL"
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "QuestionaireSTRView.aspx"
            Me.Response.Redirect("QuestionaireSTRView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                '    Transcope.Complete()
            End Using

            If Not Page.IsPostBack Then
                ClearSession()
                loaddata()
                'abc.Visible = False
                'cde.Visible = False
            End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

#Region "Grid"
    Protected Sub gridview_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                Dim attachment As String = e.Item.Cells(1).Text

                e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub gridView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gridView.PageIndexChanged
        gridView.DataSource = ObjMsQuestionaireSTRMultipleChoice
        SetnGetCurrentPage = e.NewPageIndex
        gridView.CurrentPageIndex = SetnGetCurrentPage
        gridView.DataBind()
    End Sub
#End Region


End Class
