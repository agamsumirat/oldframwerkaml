
Partial Class AccountInformationDetailTransactionExcel
    Inherits Parent

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Session("AccountInformationDetailTransactionExcel") Is Nothing Then
                Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
                Response.Clear()
                Response.AddHeader("content-disposition", "attachment;filename=AccountInformationTransaction.xls")
                Response.Charset = ""
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Response.ContentType = "application/vnd.xls"
                Me.EnableViewState = False
                Response.Write(strStyle)
                Response.Write(Session("AccountInformationDetailTransactionExcel"))
                Session("AccountInformationDetailTransactionExcel") = Nothing
                Response.End()
            End If
        Catch tex As Threading.ThreadAbortException
            ' ignore
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub
End Class
