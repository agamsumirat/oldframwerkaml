<%--<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DataUpdatingView.aspx.vb" Inherits="DataUpdatingView" title="Data Updating View" %>--%>
<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DataUpdatingView.aspx.vb" Inherits="DataUpdatingView" title="Data Updating View" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">--%>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>List of CDD to be updated
                    <hr />
                </strong>
            </td>
        </tr>
    </table>
	<TABLE borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
			<TR>
				<TD align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></TD>
				<TD vAlign="top" width="98%" bgColor="#ffffff"><TABLE cellSpacing="4" cellPadding="0" width="100%" border="0">
						<TR>
							<TD class="Regtext" noWrap>Search By :
							</TD>
							<TD vAlign="middle" noWrap>
							<%--<ajax:ajaxpanel id="AjaxPanel1" runat="server">--%>
                                <asp:dropdownlist id="ComboSearch" tabIndex="1" runat="server" Width="148px" CssClass="searcheditcbo"></asp:dropdownlist>&nbsp; 
                                <asp:textbox id="TextSearch" tabIndex="2" runat="server" CssClass="searcheditbox" Width="143px"></asp:textbox>
                                <input id="popUp" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                    border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                    border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif); width: 16px;" title="Click to show calendar"
                                    type="button" />
                                    <%--</ajax:ajaxpanel>--%>
                                    
                                    </TD>
							<TD vAlign="middle" width="99%"><ajax:ajaxpanel id="AjaxPanel2" runat="server">
									<asp:imagebutton id="ImageButtonSearch" tabIndex="3" runat="server" SkinID="SearchButton"></asp:imagebutton></ajax:ajaxpanel>
								</TD>
						</TR>
					</TABLE><ajax:ajaxpanel id="AjaxPanel3" runat="server">
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:ajaxpanel></TD>
			</TR>
	</TABLE>
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
		<tr>
			<td bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel4" runat="server">
					<asp:datagrid id="GridMSUserView" runat="server"
						Font-Size="XX-Small" BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None"
						AllowPaging="True" width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE"
						ForeColor="Black" AutoGenerateColumns="False">
						<FooterStyle BackColor="#CCCC99"></FooterStyle>
						<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
						<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
						<ItemStyle BackColor="#F7F7DE" Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" ></ItemStyle>
						<HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B"></HeaderStyle>
						<PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages"></PagerStyle>
                       				<Columns>
                            <asp:TemplateColumn SortExpression="CIFNo  desc">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="DataUpdatingId" Visible="False"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="DataUpdatingDate" HeaderText="Created Date" SortExpression="DataUpdatingDate  desc" DataFormatString="{0:dd-MMM-yyyy}" ></asp:BoundColumn>
                            <asp:BoundColumn DataField="WorkingUnitParent" SortExpression="WorkingUnitParent desc" HeaderText="WorkingUnit Parent"></asp:BoundColumn>
                            <asp:BoundColumn DataField="WorkingUnit" HeaderText="Evaluating Branch" SortExpression="WorkingUnit desc"></asp:BoundColumn>                            
                     <asp:TemplateColumn HeaderText="CIF No" SortExpression="CIFNo desc">
                                               <EditItemTemplate>
                                                   <asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CIFNo") %>'></asp:TextBox>
                                               </EditItemTemplate>
                                               <ItemTemplate>
                                                   &nbsp;<asp:LinkButton ID="LnkCIF" runat="server" OnClick="LnkCIF_Click"></asp:LinkButton>
                                               </ItemTemplate>
                                               <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                   Font-Underline="False" ForeColor="White" />
                                           </asp:TemplateColumn>
                            <asp:BoundColumn DataField="CIFName" HeaderText="CIF Name" SortExpression="CIFName desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="DataUpdatingReason" HeaderText="AML/CFT Risk Rating" SortExpression="DataUpdatingReason desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="LastModified" HeaderText="Last CDD Updated" SortExpression="LastModified desc" DataFormatString="{0:dd-MMM-yyyy}">
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" />
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Left" />
                            </asp:BoundColumn>                                                       
                                           <asp:BoundColumn DataField="SalesOfficercode" HeaderText="SalesOfficer" SortExpression="SalesOfficercode  desc">
                                           </asp:BoundColumn>
                            <asp:EditCommandColumn CancelText="Cancel" EditText="Detail" UpdateText="Update" Visible="False"></asp:EditCommandColumn>
                                           <asp:TemplateColumn>
                                               <EditItemTemplate>
                                                   <asp:TextBox runat="server"></asp:TextBox>
                                               </EditItemTemplate>
                                               <ItemTemplate>
                                                   <asp:LinkButton ID="Lnkcifdetail" ToolTip='<%# DataBinder.Eval(Container, "DataItem.CIFNo") %>' Text="Update CDD Review" runat="server" OnClick="Lnkcifdetail_Click"></asp:LinkButton>
                                               </ItemTemplate>
                                           </asp:TemplateColumn>
						</Columns>
					</asp:datagrid></ajax:ajaxpanel>
				</td>
		</tr>
		<tr>
		    <td style="background-color:#ffffff"><table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td nowrap><ajax:ajaxpanel id="AjaxPanel5" runat="server">&nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
			  &nbsp; </ajax:ajaxpanel> </td>
			 <td width="99%">&nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" runat="server">Export 
		        to Excel</asp:LinkButton>
                 <asp:LinkButton ID="LnkBtnExportAllToExcel" runat="server">Export All to Excel</asp:LinkButton></td>
			<td align="right" nowrap>&nbsp;&nbsp;</td>
		</tr>
      </table></td>
		</tr>
		<tr>
			<td bgColor="#ffffff">
				<TABLE id="Table3" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
					<TR class="regtext" align="center" bgColor="#dddddd">
						<TD vAlign="top" align="left" width="50%" bgColor="#ffffff">Page&nbsp;<ajax:ajaxpanel id="AjaxPanel6" runat="server"><asp:label id="PageCurrentPage" runat="server" CssClass="regtext">0</asp:label>&nbsp;of&nbsp;
							<asp:label id="PageTotalPages" runat="server" CssClass="regtext">0</asp:label></ajax:ajaxpanel></TD>
						<TD vAlign="top" align="right" width="50%" bgColor="#ffffff">Total Records&nbsp;
							<ajax:ajaxpanel id="AjaxPanel7" runat="server"><asp:label id="PageTotalRows" runat="server">0</asp:label></ajax:ajaxpanel></TD>
					</TR>
				</TABLE>
				<TABLE id="Table4" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
					<TR bgColor="#ffffff">
						<TD class="regtext" vAlign="middle" align="left" colSpan="11" height="7">
							<HR color="#f40101" noShade SIZE="1">
						</TD>
					</TR>
					<TR>
						<TD class="regtext" vAlign="middle" align="left" width="63" bgColor="#ffffff">Go to 
							page</TD>
						<TD class="regtext" vAlign="middle" align="left" width="5" bgColor="#ffffff"><FONT face="Verdana, Arial, Helvetica, sans-serif" size="1">
									<ajax:ajaxpanel id="AjaxPanel8" runat="server"><asp:textbox id="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:textbox></ajax:ajaxpanel>
								</FONT></TD>
						<TD class="regtext" vAlign="middle" align="left" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel9" runat="server">
								<asp:imagebutton id="ImageButtonGo" runat="server" SkinID="GoButton"></asp:imagebutton></ajax:ajaxpanel>
							</TD>
						<TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/first.gif" width="6">
						</TD>
						<TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel10" runat="server">
								<asp:linkbutton id="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First" OnCommand="PageNavigate">First</asp:linkbutton></ajax:ajaxpanel>
							</TD>
						<TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><IMG height="5" src="images/prev.gif" width="6"></TD>
						<TD class="regtext" vAlign="middle" align="right" width="14" bgColor="#ffffff">
						<ajax:ajaxpanel id="AjaxPanel11" runat="server">
								<asp:linkbutton id="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev" OnCommand="PageNavigate">Previous</asp:linkbutton></ajax:ajaxpanel>
							</TD>
						<TD class="regtext" vAlign="middle" align="right" width="60" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel12" runat="server"><A class="pageNav" href="#">
									<asp:linkbutton id="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next" OnCommand="PageNavigate">Next</asp:linkbutton></A></ajax:ajaxpanel></TD>
						<TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/next.gif" width="6"></TD>
						<TD class="regtext" vAlign="middle" align="left" width="25" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel13" runat="server">
								<asp:linkbutton id="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last" OnCommand="PageNavigate">Last</asp:linkbutton></ajax:ajaxpanel>
							</TD>
						<TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/last.gif" width="6"></TD>
					</TR>
				</TABLE>
			</td>
		</tr>
	</table>
</asp:Content>