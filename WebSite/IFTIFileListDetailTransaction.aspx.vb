Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports Sahassa.aml.Commonly
Imports AMLBLL
Partial Class IFTIFileListDetailTransaction
    Inherits Parent


    Public Property ObjListOfGeneratedIFTI() As ListOfGeneratedIFTI
        Get
            If Session("IFTIFileListDetailTransaction.ObjListOfGeneratedIFTI") Is Nothing Then
                Session("IFTIFileListDetailTransaction.ObjListOfGeneratedIFTI") = ListOfGeneratedIFTIBLL.GetListOfGeneratedIFTIByPk(Me.PKListOfTransactionIFTI)
                Return CType(Session("IFTIFileListDetailTransaction.ObjListOfGeneratedIFTI"), ListOfGeneratedIFTI)
            Else
                Return CType(Session("IFTIFileListDetailTransaction.ObjListOfGeneratedIFTI"), ListOfGeneratedIFTI)
            End If
        End Get
        Set(ByVal value As ListOfGeneratedIFTI)
            Session("IFTIFileListDetailTransaction.ObjListOfGeneratedIFTI") = value
        End Set
    End Property

    Public ReadOnly Property PKListOfTransactionIFTI() As Long
        Get
            Dim temp As String
            Dim retvalue As Long
            temp = Request.Params("PkListOfGeneratedIFTIID")
            If Not Long.TryParse(temp, retvalue) Then
                Throw New Exception("PkListOfGeneratedIFTIID is not Vaid")
            End If
            Return retvalue
        End Get
    End Property


    Sub ClearSession()
        ObjListOfGeneratedIFTI = Nothing
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                Using ObjAuditTrailUserAccess As AuditTrail_UserAccess = New AuditTrail_UserAccess
                    With ObjAuditTrailUserAccess
                        .AuditTrail_UserAccessUserid = Sahassa.AML.Commonly.SessionUserId
                        .AuditTrail_UserAccessActionDate = DateTime.Now
                        .AuditTrail_UserAccessAction = UserAccessAction
                    End With
                    DataRepository.AuditTrail_UserAccessProvider.Save(ObjAuditTrailUserAccess)
                End Using
                Me.LoadData()
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Sub LoadData()
        If Not ObjListOfGeneratedIFTI Is Nothing Then

            LblLastConfirmBy.Text = ObjListOfGeneratedIFTI.LastConfirmedBy

            Using objMsCTRReportTypeFile As MsCTRReportTypeFile = DataRepository.MsCTRReportTypeFileProvider.GetByPk_MsCTRReportTypeFile_Id(ObjListOfGeneratedIFTI.Fk_MsCTRReportTypeFile_Id)
                If Not objMsCTRReportTypeFile Is Nothing Then
                    LblReportType.Text = objMsCTRReportTypeFile.MsCTRReportTypeFile_Name
                End If
            End Using


            Using objMsStatusUploadPPATK As MsStatusUploadPPATK = DataRepository.MsStatusUploadPPATKProvider.GetByPk_MsStatusUploadPPATK_Id(ObjListOfGeneratedIFTI.Fk_MsStatusUploadPPATK_Id)
                If Not objMsStatusUploadPPATK Is Nothing Then
                    LblStatusUploadPPATK.Text = objMsStatusUploadPPATK.MsStatusUploadPPATK_Name
                End If
            End Using



            LblTotalInvalid.Text = ObjListOfGeneratedIFTI.TotalInvalid.GetValueOrDefault(0)
            LblTransactionType.Text = ObjListOfGeneratedIFTI.SwiftType
            LblTotalTransaction.text = ObjListOfGeneratedIFTI.TotalTransaksi
            LblTotalValid.Text = ObjListOfGeneratedIFTI.TotalValid
            LblTransactionDate.Text = ObjListOfGeneratedIFTI.TransactionDate.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy").Replace("01-Jan-1900", "")

            Dim intIFTItype As Integer
            Using objIFTI_Type As TList(Of IFTI_Type) = DataRepository.IFTI_TypeProvider.GetPaged(IFTI_TypeColumn.IFTIType.ToString & "='" & ObjListOfGeneratedIFTI.SwiftType & "'", "", 0, Integer.MaxValue, 0)
                If objIFTI_Type.Count > 0 Then
                    intIFTItype = objIFTI_Type(0).PK_IFTI_Type_ID
                End If
            End Using

            Dim strfilter As New StringBuilder
            strfilter.Append(" IsDataValid=1 ")
            strfilter.Append(" and  TanggalTransaksi ='" & ObjListOfGeneratedIFTI.TransactionDate.GetValueOrDefault.ToString("yyyy-MM-dd") & "'")
            strfilter.Append(" and LastUpdateDate ='" & ObjListOfGeneratedIFTI.LastUpdateIFTIDate.GetValueOrDefault.ToString("yyyy-MM-dd") & "'")
            strfilter.Append(" and FK_IFTI_Type_ID='" & intIFTItype & "'")

            Dim objparameter As New System.Web.UI.WebControls.Parameter
            objparameter.Name = "WhereClause"
            objparameter.DefaultValue = strfilter.ToString()
            iftids.Parameters.Add(objparameter)

        End If


    End Sub
  


    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Try
            Response.Redirect("ListOfGeneratedIFTIFile.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
