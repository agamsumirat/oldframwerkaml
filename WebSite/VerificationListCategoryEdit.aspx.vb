Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class VerificationListCategoryEdit
    Inherits Parent

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetCategoryId() As String
        Get
            Return Me.Request.Params("CategoryID")
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListCategoryView.aspx"

        Me.Response.Redirect("VerificationListCategoryView.aspx", False)
    End Sub

    Private Sub InsertAuditTrail()
        Try
            Dim CategoryID_Old As Int32 = ViewState("CategoryID_Old")
            Dim CategoryName_Old As String = ViewState("CategoryName_Old")
            Dim CategoryDescription_Old As String = ViewState("CategoryDescription_Old")
            Dim CategoryCreatedDate_Old As DateTime = ViewState("CategoryCreatedDate_Old")
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)

            Dim CategoryDescription As String
            If Me.TextCategoryDescription.Text.Length <= 255 Then
                CategoryDescription = Me.TextCategoryDescription.Text
            Else
                CategoryDescription = Me.TextCategoryDescription.Text.Substring(0, 255)
            End If

            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryID", "Edit", CategoryID_Old, CategoryID_Old, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryName", "Edit", CategoryName_Old, Me.TextCategoryName.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryDescription", "Edit", CategoryDescription_Old, CategoryDescription, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryCreatedDate", "Edit", CategoryCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub UpdateVerificationListCategory()
        Try
            Dim CategoryDescription As String
            If Me.TextCategoryDescription.Text.Length <= 255 Then
                CategoryDescription = Me.TextCategoryDescription.Text
            Else
                CategoryDescription = Me.TextCategoryDescription.Text.Substring(0, 255)
            End If

            Using AccessUpdateVerificationListCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
                AccessUpdateVerificationListCategory.UpdateVerificationListCategory(CInt(Me.GetCategoryId), Me.TextCategoryName.Text, CategoryDescription)
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As sqltransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                'Buat variabel untuk menampung nilai-nilai baru
                Dim CategoryID As String = Me.LabelCategoryID.Text
                Dim CategoryName As String = Trim(Me.TextCategoryName.Text)


                'Jika tidak ada perubahan CategoryName
                If CategoryName = ViewState("CategoryName_Old") Then

                    GoTo Add
                Else 'Jika ada perubahan CategoryName 

                    Dim counter As Int32
                    'Periksa apakah CategoryName yg baru tsb sdh ada dalam tabel VerificationListCategory atau belum
                    Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
                        counter = AccessCategory.CountMatchingCategory(CategoryName)
                    End Using

                    'Counter = 0 berarti CategoryName tersebut belum pernah ada dalam tabel VerificationListCategory
                    If counter = 0 Then
Add:
                        'Periksa apakah CategoryName tsb dalam status pending approval atau tidak
                        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_ApprovalTableAdapter
                            oSQLTrans = BeginTransaction(AccessPending, IsolationLevel.ReadUncommitted)
                            counter = AccessPending.CountVerificationListCategoryApprovalByCategoryName(CategoryName)

                            'Counter = 0 berarti CategoryName yg baru tsb tidak dalam status pending approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertAuditTrail()
                                    Me.UpdateVerificationListCategory()
                                    Me.LblSuccess.Visible = True
                                    Me.LblSuccess.Text = "Success to Update Verification List Category."
                                Else
                                    Dim CategoryDescription As String
                                    If Me.TextCategoryDescription.Text.Length <= 255 Then
                                        CategoryDescription = Me.TextCategoryDescription.Text
                                    Else
                                        CategoryDescription = Me.TextCategoryDescription.Text.Substring(0, 255)
                                    End If

                                    'Buat variabel untuk menampung nilai-nilai lama
                                    Dim CategoryID_Old As Int32 = ViewState("CategoryID_Old")
                                    Dim CategoryName_Old As String = ViewState("CategoryName_Old")
                                    Dim CategoryDescription_Old As String = ViewState("CategoryDescription_Old")
                                    Dim CategoryCreatedDate_Old As DateTime = ViewState("CategoryCreatedDate_Old")

                                    'variabel untuk menampung nilai identity dari tabel VerificationListCategory_PendingApprovalID
                                    Dim CategoryPendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope
                                    Using AccessCategoryPendingApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_PendingApprovalTableAdapter
                                        SetTransaction(AccessCategoryPendingApproval, oSQLTrans)
                                        'Tambahkan ke dalam tabel VerificationListCategory_PendingApproval dengan ModeID = 2 (Edit)
                                        CategoryPendingApprovalID = AccessCategoryPendingApproval.InsertVerificationListCategory_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Verification List Category Edit", 2)

                                        'Tambahkan ke dalam tabel VerificationListCategory_Approval dengan ModeID = 2 (Edit) 
                                        AccessPending.Insert(CategoryPendingApprovalID, 2, CategoryID, CategoryName, CategoryDescription, Now, CategoryID_Old, CategoryName_Old, CategoryDescription_Old, CategoryCreatedDate_Old)

                                        oSQLTrans.Commit()
                                        'TransScope.Complete()
                                    End Using
                                    'End Using

                                    Dim MessagePendingID As Integer = 8902 'MessagePendingID 8902 = VerificationListCategory Edit 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & CategoryName_Old

                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & CategoryName_Old, False)
                                End If
                            Else 'Counter != 0 berarti CategoryName tersebut dalam status pending approval
                                Throw New Exception("Cannot edit the following Verification List Category: '" & CategoryName & "' because it is currently waiting for approval")
                            End If
                        End Using
                    Else 'Counter != 0 berarti CategoryName tersebut sudah ada dalam tabel Verification List Category
                        Throw New Exception("Cannot change to the following Category Name: '" & CategoryName & "' because that Category Name already exists in the database.")
                    End If
                End If
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try

        End If
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
            Using TableCategory As AMLDAL.AMLDataSet.VerificationListCategoryDataTable = AccessCategory.GetDataByCategoryID(Me.GetCategoryId)
                If TableCategory.Rows.Count > 0 Then
                    Dim TableRowCategory As AMLDAL.AMLDataSet.VerificationListCategoryRow = TableCategory.Rows(0)

                    Dim CategoryName = TableRowCategory.CategoryName

                    If (Me.GetCategoryId = 1) OrElse (Me.GetCategoryId = 2) OrElse (Me.GetCategoryId = 3) OrElse (Me.GetCategoryId = 4) OrElse (Me.GetCategoryId = 5) Then
                        Throw New Exception("The following Category : '" & CategoryName & "' cannot be edited.")
                    Else
                        ViewState("CategoryID_Old") = TableRowCategory.CategoryID
                        Me.LabelCategoryID.Text = ViewState("CategoryID_Old")

                        ViewState("CategoryName_Old") = CategoryName
                        Me.TextCategoryName.Text = ViewState("CategoryName_Old")

                        ViewState("CategoryDescription_Old") = TableRowCategory.CategoryDescription
                        Me.TextCategoryDescription.Text = ViewState("CategoryDescription_Old")

                        ViewState("CategoryCreatedDate_Old") = TableRowCategory.CategoryCreatedDate
                    End If
                End If
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillEditData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

End Class