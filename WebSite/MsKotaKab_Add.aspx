<%@ page language="VB" autoeventwireup="false" codefile="MsKotaKab_add.aspx.vb"
    inherits="MsKotaKab_Add" masterpagefile="~/MasterPage.master" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:content id="Content1" contentplaceholderid="cpContent" runat="Server">
	
	 <script language="javascript" type="text/javascript">
	     function hidePanel(objhide, objpanel, imgmin, imgmax) {
	         document.getElementById(objhide).style.display = 'none';
	         document.getElementById(objpanel).src = imgmax;
	     }
	     // JScript File
	     //Call picker master 
	     function popWinMsKotaKab_NCBS() {
	         var height = '600px';
	         var width = '550px';
	         var left = (screen.availWidth - width) / 2;
	         var top = (screen.availHeight - height) / 2;
	         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

	         window.showModalDialog("PickerMsKotaKab_NCBS.aspx", "#1", winSetting);

         
	     }

                  function popWinProvince() {
            var height = '600px';
            var width = '550px';
            var left = (screen.availWidth - width) / 2;
            var top = (screen.availHeight - height) / 2;
            var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

            window.showModalDialog("PickerProvinsi.aspx", "#7", winSetting);
        }

	  
	  </script>

	<table cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>
				<img src="Images/blank.gif" width="5" height="1" />
			</td>
			<td>
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td>
							&nbsp;
						</td>
						<td width="99%" bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" />
						</td>
						<td bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" />
						</td>
					</tr>
				</table>
			</td>
			<td>
				<img src="Images/blank.gif" width="5" height="1" />
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td id="tdcontent" valign="top" bgcolor="#FFFFFF">
				
					<table id="tblpenampung" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td style="width: 14px">
								<img src="Images/blank.gif" width="20" height="100%" />
							</td>
							<td class="divcontentinside" bgcolor="#FFFFFF" style="width: 100%">
								&nbsp;
								<ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%" meta:resourcekey="AjaxPanel5Resource1">
									<asp:MultiView ID="MtvMsUser" runat="server" ActiveViewIndex="0">
										<asp:View ID="VwAdd" runat="server">
											<table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
												width="100%" bgcolor="#dddddd" border="0">
												<tr>
													<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
														border-right-style: none; border-left-style: none; border-bottom-style: none"
														valign="top">
														<img src="Images/dot_title.gif" width="17" height="17">
														<asp:Label ID="LblValue" runat="server" Font-Bold="True" Font-Size="Medium" Text="Master Kota Kab ADD"></asp:Label>
														<hr />
													</td>
												</tr>
												<tr>
													<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
														border-right-style: none; border-left-style: none; border-bottom-style: none;
														height: 26px;" valign="top">
													</td>
												</tr>
												                 <tr bgcolor="#ffffff">
                                                    <td style="width: 22px; height: 24px">
                                                    </td>
                                                    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
                                                        <asp:Label ID="LabelProvince" runat="server" Text="Province"></asp:Label>
                                                        <span style="color: #ff0000">*</span>
                                                    </td>
                                                    <td style="width: 6px; height: 24px" valign="top">
                                                        :
                                                    </td>
                                                    <td colspan="7" style="height: 24px" valign="top">
                                                        <asp:TextBox ID="LBSearchNama" runat="server" CssClass="textBox" 
                                                            ReadOnly="True"></asp:TextBox>
                                                        <asp:ImageButton ID="imgBrowseNameProvince" runat="server" CausesValidation="False" ImageUrl="~/Images/button/browse.gif"
                                                            OnClientClick="javascript:popWinProvince();"  />
                                                        <br />
                                                        <asp:HiddenField ID="HFProvince" runat="server" />
                                                    </td>
                                                </tr>
<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="LabelIDKotaKab" runat="server" 
            text="ID"></asp:label>
        <span style="color: #ff0000">*</span>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td colspan="7" style="height: 24px" valign="top">
        <asp:textbox id="txtIDKotaKab" runat="server" cssclass="textBox" 
            tabindex="2" width="400px" tooltip="IDKotaKab" MaxLength="4"></asp:textbox>
    </td>
</tr>

<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="LabelNamaKotaKab" runat="server" 
            text="Nama"></asp:label>
        <span style="color: #ff0000">*</span>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td colspan="7" style="height: 24px" valign="top">
        <asp:textbox id="txtNamaKotaKab" runat="server" cssclass="textBox" 
            tabindex="2" width="400px" tooltip="NamaKotaKab" MaxLength="50"></asp:textbox>
    </td>
</tr>


												<tr bgcolor="#ffffff">
													<td style="width: 22px; height: 24px">
														&nbsp;</td>
													<td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
														Mapping Item
													</td>
													<td style="width: 6px; height: 24px" valign="top">
														:</td>
													<td style="height: 24px" valign="top">
														<asp:ImageButton ID="imgBrowse" runat="server" 
															CausesValidation="False" ImageUrl="~/Images/button/browse.gif" 
															OnClientClick="javascript:popWinMsKotaKab_NCBS();"  />
													</td>
												</tr>
												<tr bgcolor="#ffffff">
													<td style="width: 22px; height: 24px">
														&nbsp;</td>
													<td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
														&nbsp;</td>
													<td style="width: 6px; height: 24px" valign="top">
														&nbsp;</td>
													<td style="height: 24px" valign="top">
														<asp:ListBox ID="LBMapping" runat="server" Height="204px" Width="170px" 
															CssClass="textbox">
														</asp:ListBox>
														<asp:ImageButton ID="imgDeleteItem" runat="server" 
															CausesValidation="False" ImageUrl="~/Images/button/remove.gif" 
															 />
													</td>
												</tr>
												<tr bgcolor="#ffffff">
													<td style="width: 22px; height: 24px">
														&nbsp;</td>
													<td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
														&nbsp;</td>
													<td style="width: 6px; height: 24px" valign="top">
														&nbsp;</td>
													<td style="height: 24px" valign="top">
														&nbsp;</td>
												</tr>
											</table>
										</asp:View>
										<asp:View ID="VwConfirmation" runat="server">
											<table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
												width="100%" bgcolor="#dddddd" border="0">
												<tr bgcolor="#ffffff">
													<td colspan="2" align="center" style="height: 17px">
														<asp:Label ID="LblConfirmation" runat="server" meta:resourcekey="LblConfirmationResource1"></asp:Label>
													</td>
												</tr>
												<tr bgcolor="#ffffff">
													<td align="center" colspan="2">
														<asp:ImageButton ID="ImgBtnAdd" runat="server" ImageUrl="~/images/button/Ok.gif"
															CausesValidation="False" meta:resourcekey="ImgBtnAddResource1" />
														<asp:ImageButton ID="ImgBack" runat="server" ImageUrl="~/images/button/back.gif"
															CausesValidation="False" meta:resourcekey="ImgBackResource1" />
													</td>
												</tr>
											</table>
										</asp:View>
									</asp:MultiView>
								</ajax:AjaxPanel>
							</td>
						</tr>
					</table>
				
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
				<ajax:AjaxPanel ID="AjaxPanel1" runat="server" meta:resourcekey="AjaxPanel1Resource1">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td align="left" background="Images/button-bground.gif" valign="middle">
								<img height="1" src="Images/blank.gif" width="5" />
							</td>
							<td align="left" background="Images/button-bground.gif" valign="middle">
								&nbsp;
							</td>
							<td background="Images/button-bground.gif">
								&nbsp;<asp:ImageButton ID="ImgBtnSave" runat="server" ImageUrl="~/images/button/save.gif"
									meta:resourcekey="ImgBtnSaveResource1" />
							</td>
							<td background="Images/button-bground.gif">
								&nbsp;
							</td>
							<td background="Images/button-bground.gif">
								&nbsp;<asp:ImageButton ID="ImgBackAdd" runat="server" ImageUrl="~/images/button/back.gif"
									CausesValidation="False" meta:resourcekey="ImgBackAddResource1" />
							</td>
							<td background="Images/button-bground.gif" width="99%">
								<img height="1" src="Images/blank.gif" width="1" />
							</td>
							<td>
								
							</td>
						</tr>
					</table>
					<asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" ValidationGroup="handle"
						meta:resourcekey="CvalHandleErrResource1"></asp:CustomValidator><asp:CustomValidator
							ID="CvalPageErr" runat="server" Display="None" meta:resourcekey="CvalPageErrResource1"></asp:CustomValidator></ajax:AjaxPanel>
			</td>
		</tr>
	</table>
</asp:content>

