<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CTRExemptionListAdd.aspx.vb" Inherits="CTRExemptionListAdd" title="CTR Exemption List - Add" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
   <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>CTR Exemption List - Add&nbsp;
                    <hr />
                </strong>
            </td>
        </tr>
    </table>
    <TABLE id="SearchBar" runat="server" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
        <tr style="background-color: #ffffff">
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Ctr Exemption Type</td>
            <td bgcolor="#ffffff" style="height: 24px" width="1%">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="79%">
                &nbsp;<ajax:ajaxpanel id="Ajaxpanel1" runat="server"><asp:DropDownList ID="CboExemptionType" runat="server" CssClass="combobox" AutoPostBack="True">
                </asp:DropDownList></ajax:AjaxPanel></td>
        </tr>
                    <tr style="background-color: #ffffff">
                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                            <ajax:ajaxpanel id="Ajaxpanel2" runat="server">
                                <asp:Label ID="Lblcifaccount" runat="server"></asp:Label></ajax:AjaxPanel></td>
                        <td bgcolor="#ffffff" style="height: 24px" width="1%">
                            :</td>
                        <td bgcolor="#ffffff" style="height: 24px" width="79%">                            
                            <ajax:ajaxpanel id="AjaxPanel6" runat="server"><asp:TextBox ID="TextSearch" runat="server" CssClass="searcheditbox" MaxLength="20"></asp:TextBox>
							    <asp:imagebutton id="ImageButtonSearch" tabIndex="3" runat="server" SkinID="SearchButton">
							    </asp:imagebutton></ajax:ajaxpanel>
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                            Name </td>
                        <td bgcolor="#ffffff" style="height: 24px" width="1%">
                            :
                        </td>
						<td bgcolor="#ffffff" style="height: 24px" width="79%">
						    <ajax:ajaxpanel id="AjaxPanel5" runat="server">                                
                            <asp:textbox id="TextName" tabIndex="2" runat="server" CssClass="searcheditbox" BorderColor="Transparent" ReadOnly="True" BackColor="Transparent" BorderStyle="None" Width="300px">
                            </asp:textbox>
                            </ajax:ajaxpanel>
                        </TD>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                            Description</td>
                        <td bgcolor="#ffffff" style="height: 24px" width="1%">
                            :
                        </td>
                        <td bgcolor="#ffffff" style="height: 24px" width="79%">
                            <asp:textbox id="TxtDescription" runat="server" CssClass="textBox" MaxLength="255" TextMode="MultiLine" Width="477px" Rows="5"></asp:textbox>
				            <ajax:ajaxpanel id="AjaxPanel3" runat="server">
                                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
                            </ajax:ajaxpanel>
                        </td>
                    </tr>
	</TABLE>
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"	border="2">
		<tr class="formText" bgColor="#dddddd" height="20">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="SaveButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table><br/>
            </td>
		</tr>
	</table>

</asp:Content>