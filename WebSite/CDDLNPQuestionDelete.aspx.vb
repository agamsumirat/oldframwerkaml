﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports System.Data

Partial Class CDDLNPQuestionDelete
    Inherits Parent

    Private ReadOnly Property GetPK_CDDQuestion_ID() As Int32
        Get
            Return Request.QueryString("ID")
        End Get
    End Property

    Private Sub LoadQuestionDetail()
        Me.GridViewCDDQuestionDetail.DataSource = DataRepository.CDD_QuestionDetailProvider.GetPaged(CDD_QuestionDetailColumn.FK_CDD_Question_ID.ToString & " = " & GetPK_CDDQuestion_ID, "", 0, Integer.MaxValue, 0)
        GridViewCDDQuestionDetail.DataBind()
    End Sub

    Private Sub LoadData()
        Using objCDDQuestion As VList(Of vw_CDD_Question) = DataRepository.vw_CDD_QuestionProvider.GetPaged("PK_CDD_Question_ID=" & GetPK_CDDQuestion_ID, "", 0, 1, 0)
            If objCDDQuestion.Count > 0 Then
                Me.txtQuestion.Text = objCDDQuestion(0).CDD_Question.ToString.Replace("</br>", vbCrLf)
                Me.txtQuestionType.Text = objCDDQuestion(0).QuestionTypeName
                Select Case objCDDQuestion(0).FK_CDD_QuestionType_ID.GetValueOrDefault
                    Case 3, 4, 7
                        RowQuestionDetail.Visible = True
                        LoadQuestionDetail()
                    Case Else
                        RowQuestionDetail.Visible = False
                End Select
                Me.txtIsRequired.Text = objCDDQuestion(0).IsRequired.GetValueOrDefault(0)
                Using objCDDQuestionParent As CDD_Question = DataRepository.CDD_QuestionProvider.GetByPK_CDD_Question_ID(objCDDQuestion(0).FK_Parent_Question_ID.GetValueOrDefault(0))
                    If objCDDQuestionParent Is Nothing Then
                        Me.txtParentQuestion.Text = "-"
                    Else
                        Me.txtParentQuestion.Text = objCDDQuestionParent.CDD_Question
                    End If
                End Using
            End If
        End Using
    End Sub

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageError.PreRender
        If cvalPageError.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                LoadData()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                'HardCoded Question can not be delete
                ' 1 - Nama Nasabah (Nama nasabah merupakan data mandatory untuk table CDDLNP)
                ' 2 - Jenis Nasabah (Jenis nasabah merupakan data mandatory untuk table CDDLNP)
                ' 3 - Sumber dana nasabah (Jika dari orang lain harus mengisi lembar beneficial owner)
                ' 4 - Apakah terkait nasabah existing? (Untuk jawaban ya menampilkan pertanyaan CIF, untuk jawaban tidak meng-hide pertanyaan CIF)
                ' 5 - CIF# (Terkait fungsi dari pertanyaan nomor 4)
                ' 7 - Apakah termasuk PEP? (PEP merupakan data mandatory untuk table CDDLNP, Jika jawaban ya AML rating High)
                ' 8 - Apakah termasuk mandatory EDD? (Jika jawaban ya AML rating High)
                '11 - Customer screening di AML Solution (Ada link yang dihardcode di pertanyaan ini untuk redirect ke customer screening)
                '86 - CIF# untuk nasabah existing? (CIF# merupakan data mandatory untuk table CDDLNP) 
                
                Select Case GetPK_CDDQuestion_ID
                    Case 1, 2, 3, 4, 5, 7, 8, 11, 86
                        ImageDelete.Visible = False

                    Case Else
                        ImageDelete.Visible = True

                End Select
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("CDDLNPQuestionView.aspx")
    End Sub

    Protected Sub GridViewCDDQuestionDetail_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewCDDQuestionDetail.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Using objValidation As CDD_VAlidation = DataRepository.CDD_VAlidationProvider.GetByPK_CDD_VAlidation_ID(e.Item.Cells(2).Text)
                    If objValidation IsNot Nothing Then
                        e.Item.Cells(3).Text = objValidation.CDD_VAlidationName
                    End If
                End Using
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageDelete.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim objApproval As New CDD_Question_Approval
        Dim objCDDQuestion As New CDD_Question_ApprovalDetail
        Dim objCDDQuestionDetail As New CDD_QuestionDetail_ApprovalDetail

        Try
            objTransManager.BeginTransaction()

            If DataRepository.CDD_Question_ApprovalDetailProvider.GetTotalItems("PK_CDD_Question_ID=" & Me.GetPK_CDDQuestion_ID, 0) > 0 Then
                Throw New Exception("Question masih dalam pending approval")
            End If

            objApproval.RequestedBy = Sahassa.AML.Commonly.SessionPkUserId
            objApproval.RequestedDate = Date.Now
            objApproval.FK_MsMode_Id = 3
            objApproval.IsUpload = False
            DataRepository.CDD_Question_ApprovalProvider.Save(objTransManager, objApproval)


            objCDDQuestion.FK_CDD_Question_Approval_Id = objApproval.PK_CDD_Question_Approval_Id
            objCDDQuestion.PK_CDD_Question_ID = GetPK_CDDQuestion_ID
            DataRepository.CDD_Question_ApprovalDetailProvider.Save(objTransManager, objCDDQuestion) 'Save Question

            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10131", False)

        Catch ex As Exception
            objTransManager.Rollback()
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
            If Not objCDDQuestion Is Nothing Then
                objCDDQuestion.Dispose()
                objCDDQuestion = Nothing
            End If
            If Not objCDDQuestionDetail Is Nothing Then
                objCDDQuestionDetail.Dispose()
                objCDDQuestionDetail = Nothing
            End If
        End Try
    End Sub
End Class '425 / 173