Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class WorkingUnitDelete
    Inherits Parent

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetWorkingUnitId() As String
        Get
            Return Me.Request.Params("WorkingUnitID")
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "WorkingUnitView.aspx"

        Me.Response.Redirect("WorkingUnitView.aspx", False)
    End Sub

    ''' <summary>
    ''' Fill Initial data to be editted
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
            Using TableWorkingUnit As AMLDAL.AMLDataSet.WorkingUnitDataTable = AccessWorkingUnit.GetDataByWorkingUnitID(Me.GetWorkingUnitId)
                If TableWorkingUnit.Rows.Count > 0 Then
                    Dim TableRowWorkingUnit As AMLDAL.AMLDataSet.WorkingUnitRow = TableWorkingUnit.Rows(0)

                    ViewState("WorkingUnitID_Old") = TableRowWorkingUnit.WorkingUnitID
                    Me.LabelWorkingUnitID.Text = ViewState("WorkingUnitID_Old")

                    ViewState("WorkingUnitName_Old") = TableRowWorkingUnit.WorkingUnitName
                    Me.TextWorkingUnitName.Text = ViewState("WorkingUnitName_Old")

                    ViewState("LevelTypeID_Old") = TableRowWorkingUnit.LevelTypeID
                    Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                        Using TableLevelType As AMLDAL.AMLDataSet.LevelTypeDataTable = AccessLevelType.GetDataByLevelTypeID(TableRowWorkingUnit.LevelTypeID)
                            Dim TableRowLevelType As AMLDAL.AMLDataSet.LevelTypeRow = TableLevelType.Rows(0)
                            Me.TextLevelTypeName.Text = TableRowLevelType.LevelTypeName
                        End Using
                    End Using

                    ViewState("WorkingUnitParentID_Old") = TableRowWorkingUnit.WorkingUnitParentID
                    Using TableWorkingUnit2 As AMLDAL.AMLDataSet.WorkingUnitDataTable = AccessWorkingUnit.GetDataByWorkingUnitID(TableRowWorkingUnit.WorkingUnitParentID)
                        Dim TableRowWorkingUnit2 As AMLDAL.AMLDataSet.WorkingUnitRow = TableWorkingUnit2.Rows(0)
                        Me.TextWorkingUnitParentName.Text = ViewState("WorkingUnitParentID_Old") & " - " & TableRowWorkingUnit2.WorkingUnitName
                    End Using

                    ViewState("WorkingUnitDesc_Old") = TableRowWorkingUnit.WorkingUnitDesc
                    Me.TextWorkingUnitDescription.Text = ViewState("WorkingUnitDesc_Old")

                    ViewState("WorkingUnitCreatedDate_Old") = TableRowWorkingUnit.WorkingUnitCreatedDate

                End If
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' InsertWorkingUnitBySU
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DeleteWorkingUnitBySU()
        Try
            Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                AccessWorkingUnit.DeleteWorkingUnit(CInt(Me.GetWorkingUnitId))
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' InsertAuditTrail
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
            Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                Using dt As Data.DataTable = AccessWorkingUnit.GetDataByWorkingUnitID(CInt(Me.GetWorkingUnitId))
                    Dim row As AMLDAL.AMLDataSet.WorkingUnitRow = dt.Rows(0)
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitName", "Edit", "", row.WorkingUnitName, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "LevelTypeID", "Edit", "", row.LevelTypeID, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitParentID", "Edit", "", row.WorkingUnitParentID, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitDesc", "Edit", "", row.WorkingUnitDesc, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitCreatedDate", "Edit", "", row.WorkingUnitCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageUpdate.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                'Buat variabel untuk menampung nilai-nilai baru
                Dim WorkingUnitID As Int64 = Me.LabelWorkingUnitID.Text
                Dim WorkingUnitName As String = Me.TextWorkingUnitName.Text
                Dim LevelTypeID As Int16 = ViewState("LevelTypeID_Old")
                Dim WorkingUnitParentID As Int16 = ViewState("WorkingUnitParentID_Old")
                Dim WorkingUnitDescription As String = Me.TextWorkingUnitDescription.Text

                'Periksa apakah WorkingUnitName tsb sdh ada dalam tabel WorkingUnit atau belum
                Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                    Dim counter As Int32 = AccessWorkingUnit.CountMatchingWorkingUnit(WorkingUnitName)

                    'Counter > 0 berarti WorkingUnitName tersebut masih ada dalam tabel WorkingUnit dan bisa didelete
                    If counter > 0 Then
                        'Periksa apakah WorkingUnitName tsb dalam status pending approval atau tidak
                        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_ApprovalTableAdapter
                            oSQLTrans = BeginTransaction(AccessPending, IsolationLevel.ReadUncommitted)
                            counter = AccessPending.CountWorkingUnitApproval(WorkingUnitID)

                            'Counter = 0 berarti WorkingUnitName tersebut tidak dalam status pending approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertAuditTrail()
                                    Me.DeleteWorkingUnitBySU()
                                    Me.LblSuccess.Text = "Success to Delete Working Unit."
                                    Me.LblSuccess.Visible = True
                                Else

                                    'Buat variabel untuk menampung nilai-nilai lama
                                    Dim WorkingUnitID_Old As Int64 = ViewState("WorkingUnitID_Old")
                                    Dim WorkingUnitName_Old As String = ViewState("WorkingUnitName_Old")
                                    Dim LevelTypeID_Old As Int16 = ViewState("LevelTypeID_Old")
                                    Dim WorkingUnitParentID_Old As Int16 = ViewState("WorkingUnitParentID_Old")
                                    Dim WorkingUnitDesc_Old As String = ViewState("WorkingUnitDesc_Old")
                                    Dim WorkingUnitCreatedDate_Old As DateTime = ViewState("WorkingUnitCreatedDate_Old")

                                    'variabel untuk menampung nilai identity dari tabel WorkingUnitPendingApprovalID
                                    Dim WorkingUnitPendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope
                                    Using AccessWorkingUnitPendingApproval As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_PendingApprovalTableAdapter
                                        SetTransaction(AccessWorkingUnitPendingApproval, oSQLTrans)
                                        'Tambahkan ke dalam tabel WorkingUnit_PendingApproval dengan ModeID = 3 (Delete) 
                                        WorkingUnitPendingApprovalID = AccessWorkingUnitPendingApproval.InsertWorkingUnit_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "WorkingUnit Delete", 3)

                                        'Tambahkan ke dalam tabel WorkingUnit_Approval dengan ModeID = 3 (Delete) 
                                        AccessPending.Insert(WorkingUnitPendingApprovalID, 3, WorkingUnitID, WorkingUnitName, LevelTypeID, WorkingUnitParentID, WorkingUnitDescription, Now, WorkingUnitID_Old, WorkingUnitName_Old, LevelTypeID_Old, WorkingUnitParentID_Old, WorkingUnitDesc_Old, WorkingUnitCreatedDate_Old)

                                        oSQLTrans.Commit()
                                        '    TransScope.Complete()
                                    End Using
                                    'End Using

                                    Dim MessagePendingID As Integer = 8503 'MessagePendingID 8503 = WorkingUnit Delete 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & WorkingUnitName

                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & WorkingUnitName, False)
                                End If
                            Else
                                Throw New Exception("Cannot delete the following Working Unit: '" & WorkingUnitName & "' because it is currently waiting for approval.")
                            End If
                        End Using
                    Else 'Counter = 0 berarti WorkingUnitName tersebut tidak ada dalam tabel WorkingUnit
                        Throw New Exception("Cannot delete the following Working Unit: '" & WorkingUnitName & "' because that Working Unit Name does not exist in the database anymore.")
                    End If
                End Using
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillEditData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                ' Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

End Class