Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports Sahassanettier.Services

Partial Class PickerKecamatan
    Inherits Parent

#Region "Set Session"
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("PickerKecamatan.Sort") Is Nothing, "IDKecamatan asc", Session("PickerKecamatan.Sort"))
        End Get
        Set(ByVal Value As String)
            Session("PickerKecamatan.Sort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("PickerKecamatan.CurrentPage") Is Nothing, 0, Session("PickerKecamatan.CurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("PickerKecamatan.CurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("PickerKecamatan.RowTotal") Is Nothing, 0, Session("PickerKecamatan.RowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("PickerKecamatan.RowTotal") = Value
        End Set
    End Property
    Private Property SetAndGetSearchingCriteria() As String
        Get
            Return IIf(Session("PickerKecamatan.SearchCriteria") Is Nothing, "", Session("PickerKecamatan.SearchCriteria"))
        End Get
        Set(ByVal Value As String)
            Session("PickerKecamatan.SearchCriteria") = Value
        End Set
    End Property
    Private Function SearchFilter() As String
        Dim StrSearch As String = ""
        Try
            StrSearch = " Activation = 1 "

            If Me.txtIDKecamatan.Text <> "" Then
                If StrSearch = "" Then
                    StrSearch = StrSearch & " IDKecamatan like '%" & Me.txtIDKecamatan.Text.Replace("'", "''") & "%' "
                Else
                    StrSearch = StrSearch & " And IDKecamatan like '%" & Me.txtIDKecamatan.Text.Replace("'", "''") & "%' "
                End If
            End If

            If Me.txtNamaKecamatan.Text <> "" Then
                If StrSearch = "" Then
                    StrSearch = StrSearch & " NamaKecamatan like '%" & Me.txtNamaKecamatan.Text.Replace("'", "''") & "%' "
                Else
                    StrSearch = StrSearch & " And NamaKecamatan like '%" & Me.txtNamaKecamatan.Text.Replace("'", "''") & "%' "
                End If
            End If

            Return StrSearch
        Catch
            Throw
            Return ""
        End Try
    End Function
    Private Function SetnGetBindTable() As TList(Of MsKecamatan)
        Return DataRepository.MsKecamatanProvider.GetPaged(SearchFilter, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, SetnGetRowTotal)
    End Function
#End Region
    Private Property StrKecamatanFilter() As String
        Get
            Return IIf(Session("PickerKecamatan.StrKecamatanFilter") Is Nothing, "", Session("PickerKecamatan.StrKecamatanFilter"))
        End Get
        Set(ByVal Value As String)
            Session("PickerKecamatan.StrKecamatanFilter") = Value
        End Set
    End Property

#Region " Searching Box"
    Protected Sub ImageClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageClearSearch.Click
        Try
            Me.txtIDKecamatan.Text = ""
            Me.txtNamaKecamatan.Text = ""
        Catch
            Throw
        End Try
    End Sub
    Protected Sub ImgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgSearch.Click
        Try
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
#Region "Sorting Event"
    Protected Sub GridDataView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridDataView.SortCommand
        Dim GridPickerProductTier2Sort As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridPickerProductTier2Sort.Columns(Sahassa.AML.Commonly.IndexSort(GridDataView, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
#Region "Paging Event"
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be in number format.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

    Private Sub ClearThisPageSessions()
        Session("PickerKecamatan.Sort") = Nothing
        Session("PickerKecamatan.CurrentPage") = Nothing
        Session("PickerKecamatan.RowTotal") = Nothing
        Session("PickerKecamatan.SearchCriteria") = Nothing
        Session("PickerKecamatan.StrKecamatanFilter") = Nothing
        Session("PickerKecamatan.Data") = Nothing

    End Sub

    Public Sub BindGrid()
        Me.GridDataView.DataSource = Me.SetnGetBindTable
        Me.GridDataView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridDataView.DataBind()
    End Sub
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()

                Me.GridDataView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using AccessAudit As New CTRWebBLL.AuditTrailBLL
                '    AccessAudit.InsertAuditTrailUserAccess(Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "Accesssing page " & Sahassa.CommonCTRWeb.Commonly.SessionCurrentPage & " succeeded")
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    'Private Property SetnGetSelectedItem() As ArrayList
    '    Get
    '        Return IIf(Session("PickerProductTier2Selected") Is Nothing, New ArrayList, Session("PickerProductTier2Selected"))
    '    End Get
    '    Set(ByVal value As ArrayList)
    '        Session("PickerProductTier2Selected") = value
    '    End Set
    'End Property

    Protected Sub GridDataView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridDataView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                'Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                'chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgSelected_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgSelected.Click
        Dim IDKecamatan As String = ""
        Dim NamaKecamatan As String = ""
        Dim intSelected As Integer = 0

        For i As Integer = 0 To GridDataView.Items.Count - 1
            Dim rbSelected As RadioButton = CType(GridDataView.Items(i).FindControl("rbSelected"), RadioButton)
            If rbSelected.Checked Then
                IDKecamatan = GridDataView.Items(i).Cells(1).Text
                NamaKecamatan = GridDataView.Items(i).Cells(4).Text

                intSelected = 1
                Exit For
            End If
        Next

        Dim sbScript As New StringBuilder
        If intSelected = 0 Then
            sbScript.Append("<script>alert('Please select data');</script>")
            ClientScript.RegisterClientScriptBlock(Page.GetType, "alterting", sbScript.ToString())
        Else
            If Session("PickerKecamatan.Data") Is Nothing Then
                Session.Add("PickerKecamatan.Data", IDKecamatan & ";" & NamaKecamatan)
            Else
                Session("PickerKecamatan.Data") = IDKecamatan & ";" & NamaKecamatan
            End If

            'Dim strStartUpScript As String = "javascript:CloseDialog();"
            'ClientScript.RegisterStartupScript(Page.GetType, "Script", strStartUpScript, True)
            ClientScript.RegisterClientScriptBlock(Page.GetType, "closing", "javascript:window.close()", True)

            'ClientScript.RegisterStartupScript(Page.GetType, "PopupScript", strStartUpScript)
            'ClientScript.RegisterClientScriptBlock(Page.GetType, "PopupScript", strStartUpScript)
            'Session("PickerKelurahan.Data") = idKelurahan & ";" & namaKelurahan

            'sbScript.Append("<script language='JavaScript'>")
            'sbScript.Append("SetDataForSent('" & Request.Params("ButtonId") & "');")
            'sbScript.Append("</script>")
            'ClientScript.RegisterStartupScript(Page.GetType, "Script", sbScript.ToString)
        End If
    End Sub


End Class


