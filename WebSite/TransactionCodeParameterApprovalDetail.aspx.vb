Partial Class TransactionCodeParameterApprovalDetail
    Inherits Parent

    Private InMenuManagementId As Integer
    Private InGroupId As String
    Public js As String = ""
    Public jsold As String = ""
    Public jsplain As String = ""
    Public jsplainold As String = ""
    Public sothink As String = ""
    Public sothinkold As String = ""
    Public ObjMenu As Sahassa.AML.AMLMenu
    Public BufferLevelPattern, BufferLevelMenuItem, BufferLevelMenuItemWithLink As StringBuilder

    Public ReadOnly Property GetPkMenuPendingApproval() As Integer
        Get
            Return CInt(Me.Request.Params("MenuManagementId"))
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            GetDataAccessAuthority()

            If Not Page.IsPostBack Then
                Me.ImageButtonAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageButtonReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using Transcope As New Transactions.TransactionScope
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                        Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                        AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                        Transcope.Complete()
                    End Using
                End Using
            End If
        Catch ex As Exception
            ValidCustomPageError.IsValid = False
            ValidCustomPageError.ErrorMessage += ex.Message & "<br>"
            LogError(ex)
        Finally
            'ObjGroup = Nothing
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Untuk mengambil data dari AccessAuthority
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Administrator]	6/13/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GetDataAccessAuthority()
        Try
            ' New Value
            ListFile.Visible = True
            Dim x As Integer
            ListFile.Items.Clear()
            Using AccessTransactionCode As New AMLDAL.CTRExemptionListTableAdapters.TransactionCodeApprovalTableAdapter
                Using TableTransactionCode As AMLDAL.CTRExemptionList.TransactionCodeApprovalDataTable = AccessTransactionCode.GetDataByPendingID(Session("TransactionCodeApprovalID"))
                    If TableTransactionCode.Rows.Count > 0 Then
                        Dim TransactionCodeRow As AMLDAL.CTRExemptionList.TransactionCodeApprovalRow
                        For x = 0 To TableTransactionCode.Rows.Count - 1
                            TransactionCodeRow = TableTransactionCode.Rows(x)
                            ListFile.Items.Add( _
                            TransactionCodeRow.TransactionCode & "-" & _
                            TransactionCodeRow.DebitORCredit & "-" & _
                            TransactionCodeRow.TransactionDescription)
                        Next
                    Else
                    End If
                End Using
            End Using

            ' Old
            ListFileOld.Visible = True
            ListFileOld.Items.Clear()
            Using AccessTransactionCode As New AMLDAL.CTRExemptionListTableAdapters.TransactionCodeMasterTableAdapter
                Using TableTransactionCode As AMLDAL.CTRExemptionList.TransactionCodeMasterDataTable = AccessTransactionCode.GetData
                    If TableTransactionCode.Rows.Count > 0 Then
                        Dim TransactionCodeRow As AMLDAL.CTRExemptionList.TransactionCodeMasterRow
                        For x = 0 To TableTransactionCode.Rows.Count - 1
                            TransactionCodeRow = TableTransactionCode.Rows(x)
                            ListFileOld.Items.Add( _
                            TransactionCodeRow.TransactionCode & "-" & _
                            TransactionCodeRow.DebitORCredit & "-" & _
                            TransactionCodeRow.TransactionDescription)
                        Next
                    Else
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Untuk menerima perubahan
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Administrator]	6/13/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonAccept_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAccept.Click
        Try
            AccessAuthorityApprove()

        Catch ex As Exception
            ValidCustomPageError.IsValid = False
            ValidCustomPageError.ErrorMessage += ex.Message & "<br>"
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Untuk menolak perubahan
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Administrator]	6/13/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonReject_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReject.Click
        Try
            AccessAuthorityReject()

            Sahassa.AML.Commonly.SessionIntendedPage = "MenuApproval.aspx"
            Response.Redirect("MenuApproval.aspx", False)
        Catch ex As Exception
            ValidCustomPageError.IsValid = False
            ValidCustomPageError.ErrorMessage += ex.Message & "<br>"
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' Cek Existing FileSecurity Approval
    ''' </summary>
    ''' <param name="pk_menu_pending_id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CekExistingFileSecurityApproval(ByVal pk_menu_pending_id As Integer) As Boolean
        Try
            Using accessFileSecurity As New AMLDAL.AMLDataSetTableAdapters.Menu_PendingApprovalByIDTableAdapter
                Using dt As AMLDAL.AMLDataSet.Menu_PendingApprovalByIDDataTable = accessFileSecurity.GetData(pk_menu_pending_id)
                    If dt.Rows.Count > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Untuk menerima perubahan terhadap AccessAuthority dan memasukkan ke 
    '''     Audit_MenuManagement
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Administrator]	6/13/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AccessAuthorityApprove()
        Dim i As Integer
        Try

            'Tambahkan ke dalam tabel TransactionCode_PendingApproval dengan ModeID = 3 (Delete) 
            Using TransactionCodePendingApproval As New AMLDAL.CTRExemptionListTableAdapters.TransactionCodeMasterTableAdapter
                Dim x As Integer

                Using TransScope As New Transactions.TransactionScope()
                    Using TableCodeMaster As AMLDAL.CTRExemptionList.TransactionCodeMasterDataTable = TransactionCodePendingApproval.GetData

                        'update data pada tabel transaction code master
                        'delete data pada tabel transaction code master
                        For Each RowFileSecurity As AMLDAL.CTRExemptionList.TransactionCodeMasterRow In TransactionCodePendingApproval.GetData
                            TransactionCodePendingApproval.DeleteByTransactionCode(RowFileSecurity.TransactionCodeParameterId)
                        Next

                        'insert data baru pada tabel transaction code master & audit trail
                        Dim transactionCode As String = ""
                        Dim DebitOrCredit As String = ""
                        Dim Description As String = ""
                        Dim myString As String = ""
                        Dim abc As String = ""
                        Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            For x = 0 To ListFile.Items.Count - 1
                                ListFile.SelectedIndex = x
                                myString = ListFile.SelectedValue
                                Dim y As Integer = 0
                                myString = ""
                                While ListFile.SelectedValue.Substring(y, 1) <> "-"
                                    myString = myString & ListFile.SelectedValue.Substring(y, 1)
                                    y = y + 1
                                End While
                                transactionCode = myString
                                y = y + 1
                                DebitOrCredit = ListFile.SelectedValue.Substring(y, 1)
                                y = y + 2
                                Description = ListFile.SelectedValue.Substring(y)
                                abc = abc & transactionCode & DebitOrCredit & Description
                                TransactionCodePendingApproval.Insert("DD", transactionCode, DebitOrCredit, Now, Description)
                                AccessAuditTrail.Insert(Now, Nothing, Sahassa.AML.Commonly.SessionUserId, "TransactionCodeParameter", "TransactionCode", "Add", "", transactionCode, "Accepted")
                                AccessAuditTrail.Insert(Now, Nothing, Sahassa.AML.Commonly.SessionUserId, "TransactionCodeParameter", "DebitOrCredit", "Add", "", DebitOrCredit, "Accepted")
                                AccessAuditTrail.Insert(Now, Nothing, Sahassa.AML.Commonly.SessionUserId, "TransactionCodeParameter", "Description", "Add", "", Description, "Accepted")
                            Next
                        End Using
                        'delete data pada table TransactionCodePendingApproval
                        Using asd As New AMLDAL.CTRExemptionListTableAdapters.TransactionCodePendingApprovalTableAdapter
                            asd.DeleteByPendingID(Session("TransactionCodeApprovalID"))
                        End Using
                        'delete data pada table TransactionCodeApproval
                        Using asd As New AMLDAL.CTRExemptionListTableAdapters.TransactionCodeApprovalTableAdapter
                            asd.DeleteByPendingID(Session("TransactionCodeApprovalID"))
                        End Using

                    End Using
                    TransScope.Complete()
                End Using
            End Using

            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=81304"
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=81304", False)
        Catch
            Throw
        Finally
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Untuk menolak perubahan terhadap AccessAuthority dan memasukkan ke
    '''     Audit_MenuManagement
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Administrator]	6/13/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AccessAuthorityReject()
        Try
            'Tambahkan ke dalam tabel TransactionCode_PendingApproval dengan ModeID = 3 (Delete) 
            Using TransactionCodePendingApproval As New AMLDAL.CTRExemptionListTableAdapters.TransactionCodeMasterTableAdapter

                Using TransScope As New Transactions.TransactionScope()
                    Using TableCodeMaster As AMLDAL.CTRExemptionList.TransactionCodeMasterDataTable = TransactionCodePendingApproval.GetData

                        'delete data pada table TransactionCodePendingApproval
                        Using asd As New AMLDAL.CTRExemptionListTableAdapters.TransactionCodePendingApprovalTableAdapter
                            asd.DeleteByPendingID(Session("TransactionCodeApprovalID"))
                        End Using
                        'delete data pada table TransactionCodeApproval
                        Using asd As New AMLDAL.CTRExemptionListTableAdapters.TransactionCodeApprovalTableAdapter
                            asd.DeleteByPendingID(Session("TransactionCodeApprovalID"))
                        End Using

                    End Using
                    TransScope.Complete()
                End Using
            End Using

            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=81304"
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=81304", False)
        Catch
            Throw
        Finally
        End Try
    End Sub

End Class
