
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports System.Data
Imports System.Collections.Generic
Imports AMLBLL



Partial Class LTKTTransactionEdit
    Inherits System.Web.UI.Page

    ReadOnly Property getLTKTPK() As Integer
        Get
            If Session("LTKTEdit.LTKTPK") = Nothing Then
                Session("LTKTEdit.LTKTPK") = CInt(Request.Params("LTKT_ID"))
            End If
            Return Session("LTKTEdit.LTKTPK")
        End Get
    End Property

    Public Property SetnGetResumeKasMasukKasKeluar() As List(Of ResumeKasMasukKeluar)
        Get
            If Session("LTKTEdit.ResumeKasMasukKasKeluar") Is Nothing Then
                Session("LTKTEdit.ResumeKasMasukKasKeluar") = New List(Of ResumeKasMasukKeluar)
            End If
            Return Session("LTKTEdit.ResumeKasMasukKasKeluar")
        End Get
        Set(ByVal value As List(Of ResumeKasMasukKeluar))
            Session("LTKTEdit.ResumeKasMasukKasKeluar") = value
        End Set
    End Property

    Public Property SetnGetgrvTRXKMDetilValutaAsing() As List(Of LTKTDetailCashInEdit)
        Get
            If Session("LTKTEdit.grvTRXKMDetilValutaAsingDATA") Is Nothing Then
                Session("LTKTEdit.grvTRXKMDetilValutaAsingDATA") = New List(Of LTKTDetailCashInEdit)
            End If
            Return Session("LTKTEdit.grvTRXKMDetilValutaAsingDATA")
        End Get
        Set(ByVal value As List(Of LTKTDetailCashInEdit))
            Session("LTKTEdit.grvTRXKMDetilValutaAsing") = value
        End Set
    End Property

    Public Property SetnGetgrvDetilKasKeluar() As List(Of LTKTDetailCashInEdit)
        Get
            If Session("LTKTEdit.grvDetilKasKeluarDATA") Is Nothing Then
                Session("LTKTEdit.grvDetilKasKeluarDATA") = New List(Of LTKTDetailCashInEdit)
            End If
            Return Session("LTKTEdit.grvDetilKasKeluarDATA")
        End Get
        Set(ByVal value As List(Of LTKTDetailCashInEdit))
            Session("LTKTEdit.grvDetilKasKeluarDATA") = value
        End Set
    End Property

    Public Property SetnGetRowEdit() As Integer
        Get
            If Session("LTKTEdit.RowEdit") Is Nothing Then
                Session("LTKTEdit.RowEdit") = -1
            End If
            Return Session("LTKTEdit.RowEdit")
        End Get
        Set(ByVal value As Integer)
            Session("LTKTEdit.RowEdit") = value
        End Set
    End Property

    Sub tipeLaporanCheck()
        If cboTipeLaporan.SelectedIndex = 0 Then
            tblLTKTKoreksi.Visible = False
        Else
            tblLTKTKoreksi.Visible = True
        End If
    End Sub

    Protected Sub cboTipeLaporan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipeLaporan.SelectedIndexChanged
        tipeLaporanCheck()
    End Sub

    Sub LTKTTipeTerlaporChange()
        If rblTerlaporTipePelapor.SelectedValue = "Perorangan" Then
            divPerorangan.Visible = True
            divKorporasi.Visible = False
        Else
            divPerorangan.Visible = False
            divKorporasi.Visible = True
        End If
    End Sub

    Protected Sub rblTerlaporTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTerlaporTipePelapor.SelectedIndexChanged
        LTKTTipeTerlaporChange()
    End Sub

    Sub clearSession()
        Session("LTKTEdit.grvTRXKMDetilValutaAsingDATA") = Nothing
        Session("LTKTEdit.grvDetilKasKeluarDATA") = Nothing
        Session("LTKTEdit.ResumeKasMasukKasKeluar") = Nothing
        Session("LTKTEdit.RowEdit") = Nothing
        Session("LTKTEdit.LTKTPK") = Nothing

    End Sub

    Sub loadResume()
        Dim objResume As List(Of ResumeKasMasukKeluar) = SetnGetResumeKasMasukKasKeluar

        'load Kas Masuk ke objResume
        Using objListTransactionCashIn As TList(Of LTKTTransactionCashIn) = DataRepository.LTKTTransactionCashInProvider.GetPaged(LTKTTransactionCashInColumn.FK_LTKT_Id.ToString & " = " & getLTKTPK.ToString, "", 0, Integer.MaxValue, 0)
            For Each objSingleTransactinCashIn As LTKTTransactionCashIn In objListTransactionCashIn


                Dim objKas As ResumeKasMasukKeluar = New ResumeKasMasukKeluar
                'Insert untuk tampilan Grid
                objKas.TransactionDate = objSingleTransactinCashIn.TanggalTransaksi
                objKas.Branch = objSingleTransactinCashIn.NamaKantorPJK
                objKas.TransactionNominal = objSingleTransactinCashIn.Total.ToString
                objKas.AccountNumber = objSingleTransactinCashIn.NomorRekening
                objKas.Kas = "Kas Masuk"
                objKas.Type = "LTKT"

                'Insert Data Ke ObjectTransaction
                objKas.TransactionCashIn = New LTKTTransactionCashIn
                objKas.TransactionCashIn.PK_LTKTTransactionCashIn_Id = objSingleTransactinCashIn.PK_LTKTTransactionCashIn_Id
                objKas.TransactionCashIn.TanggalTransaksi = objSingleTransactinCashIn.TanggalTransaksi.GetValueOrDefault
                objKas.TransactionCashIn.NamaKantorPJK = objSingleTransactinCashIn.NamaKantorPJK
                objKas.TransactionCashIn.FK_MsKotaKab_Id = objSingleTransactinCashIn.FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashIn.FK_MsProvince_Id = objSingleTransactinCashIn.FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashIn.NomorRekening = objSingleTransactinCashIn.NomorRekening
                objKas.TransactionCashIn.INDV_Gelar = objSingleTransactinCashIn.INDV_Gelar
                objKas.TransactionCashIn.INDV_NamaLengkap = objSingleTransactinCashIn.INDV_NamaLengkap
                objKas.TransactionCashIn.INDV_TempatLahir = objSingleTransactinCashIn.INDV_TempatLahir
                objKas.TransactionCashIn.INDV_TanggalLahir = objSingleTransactinCashIn.INDV_TanggalLahir.GetValueOrDefault
                objKas.TransactionCashIn.INDV_Kewarganegaraan = objSingleTransactinCashIn.INDV_Kewarganegaraan
                objKas.TransactionCashIn.INDV_ID_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsNegara_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_DOM_NamaJalan = objSingleTransactinCashIn.INDV_DOM_NamaJalan
                objKas.TransactionCashIn.INDV_DOM_RTRW = objSingleTransactinCashIn.INDV_DOM_RTRW
                objKas.TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsKelurahan_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsKecamatan_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_DOM_KodePos = objSingleTransactinCashIn.INDV_DOM_KodePos
                objKas.TransactionCashIn.INDV_DOM_FK_MsProvince_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_ID_NamaJalan = objSingleTransactinCashIn.INDV_ID_NamaJalan
                objKas.TransactionCashIn.INDV_ID_RTRW = objSingleTransactinCashIn.INDV_ID_RTRW
                objKas.TransactionCashIn.INDV_ID_FK_MsKelurahan_Id = objSingleTransactinCashIn.INDV_ID_FK_MsKelurahan_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_ID_FK_MsKecamatan_Id = objSingleTransactinCashIn.INDV_ID_FK_MsKecamatan_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_ID_FK_MsKotaKab_Id = objSingleTransactinCashIn.INDV_ID_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_ID_KodePos = objSingleTransactinCashIn.INDV_ID_KodePos
                objKas.TransactionCashIn.INDV_ID_FK_MsProvince_Id = objSingleTransactinCashIn.INDV_ID_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_NA_NamaJalan = objSingleTransactinCashIn.INDV_NA_NamaJalan
                objKas.TransactionCashIn.INDV_NA_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_NA_FK_MsNegara_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_NA_FK_MsProvince_Id = objSingleTransactinCashIn.INDV_NA_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_NA_FK_MsKotaKab_Id = objSingleTransactinCashIn.INDV_NA_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_NA_KodePos = objSingleTransactinCashIn.INDV_NA_KodePos
                objKas.TransactionCashIn.INDV_FK_MsIDType_Id = objSingleTransactinCashIn.INDV_FK_MsIDType_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_NomorId = objSingleTransactinCashIn.INDV_NomorId
                objKas.TransactionCashIn.INDV_NPWP = objSingleTransactinCashIn.INDV_NPWP
                objKas.TransactionCashIn.INDV_FK_MsPekerjaan_Id = objSingleTransactinCashIn.INDV_FK_MsPekerjaan_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_Jabatan = objSingleTransactinCashIn.INDV_Jabatan
                objKas.TransactionCashIn.INDV_PenghasilanRataRata = objSingleTransactinCashIn.INDV_PenghasilanRataRata
                objKas.TransactionCashIn.INDV_TempatBekerja = objSingleTransactinCashIn.INDV_TempatBekerja
                objKas.TransactionCashIn.INDV_TujuanTransaksi = objSingleTransactinCashIn.INDV_TujuanTransaksi
                objKas.TransactionCashIn.INDV_SumberDana = objSingleTransactinCashIn.INDV_SumberDana
                objKas.TransactionCashIn.INDV_NamaBankLain = objSingleTransactinCashIn.INDV_NamaBankLain
                objKas.TransactionCashIn.INDV_NomorRekeningTujuan = objSingleTransactinCashIn.INDV_NomorRekeningTujuan

                objKas.TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id = objSingleTransactinCashIn.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_Nama = objSingleTransactinCashIn.CORP_Nama
                objKas.TransactionCashIn.CORP_FK_MsBidangUsaha_Id = objSingleTransactinCashIn.CORP_FK_MsBidangUsaha_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_TipeAlamat = objSingleTransactinCashIn.CORP_TipeAlamat.GetValueOrDefault
                objKas.TransactionCashIn.CORP_NamaJalan = objSingleTransactinCashIn.CORP_NamaJalan
                objKas.TransactionCashIn.CORP_RTRW = objSingleTransactinCashIn.CORP_RTRW
                objKas.TransactionCashIn.CORP_FK_MsKelurahan_Id = objSingleTransactinCashIn.CORP_FK_MsKelurahan_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_FK_MsKecamatan_Id = objSingleTransactinCashIn.CORP_FK_MsKecamatan_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_FK_MsKotaKab_Id = objSingleTransactinCashIn.CORP_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_KodePos = objSingleTransactinCashIn.CORP_KodePos
                objKas.TransactionCashIn.CORP_FK_MsProvince_Id = objSingleTransactinCashIn.CORP_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_FK_MsNegara_Id = objSingleTransactinCashIn.CORP_FK_MsNegara_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_LN_NamaJalan = objSingleTransactinCashIn.CORP_LN_NamaJalan
                objKas.TransactionCashIn.CORP_LN_FK_MsNegara_Id = objSingleTransactinCashIn.CORP_LN_FK_MsNegara_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_LN_FK_MsProvince_Id = objSingleTransactinCashIn.CORP_LN_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_LN_MsKotaKab_Id = objSingleTransactinCashIn.CORP_LN_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_LN_KodePos = objSingleTransactinCashIn.CORP_LN_KodePos
                objKas.TransactionCashIn.CORP_NPWP = objSingleTransactinCashIn.CORP_NPWP
                objKas.TransactionCashIn.CORP_TujuanTransaksi = objSingleTransactinCashIn.CORP_TujuanTransaksi
                objKas.TransactionCashIn.CORP_SumberDana = objSingleTransactinCashIn.CORP_SumberDana
                objKas.TransactionCashIn.CORP_NamaBankLain = objSingleTransactinCashIn.CORP_NamaBankLain
                objKas.TransactionCashIn.CORP_NomorRekeningTujuan = objSingleTransactinCashIn.CORP_NomorRekeningTujuan

                'Insert Data Ke Detail Transaction
                objKas.DetailTransactionCashIn = New List(Of LTKTDetailCashInTransaction)

                Dim objListDetailTransactionCashIn As TList(Of LTKTDetailCashInTransaction) = DataRepository.LTKTDetailCashInTransactionProvider.GetPaged(LTKTDetailCashInTransactionColumn.FK_LTKTTransactionCashIn_Id.ToString & " = '" & objSingleTransactinCashIn.PK_LTKTTransactionCashIn_Id.ToString & "'", "", 0, Integer.MaxValue, 0)
                For Each objSingleDetailTransactionCashIn As LTKTDetailCashInTransaction In objListDetailTransactionCashIn
                    Dim objCalonInsert As New LTKTDetailCashInTransaction
                    objCalonInsert.PK_LTKTDetailCashInTransaction_Id = objSingleDetailTransactionCashIn.PK_LTKTDetailCashInTransaction_Id
                    objCalonInsert.Asing_KursTransaksi = objSingleDetailTransactionCashIn.Asing_KursTransaksi
                    objCalonInsert.Asing_TotalKasMasukDalamRupiah = objSingleDetailTransactionCashIn.Asing_TotalKasMasukDalamRupiah
                    objCalonInsert.TotalKasMasuk = objSingleDetailTransactionCashIn.TotalKasMasuk
                    objCalonInsert.KasMasuk = objSingleDetailTransactionCashIn.KasMasuk
                    objCalonInsert.Asing_FK_MsCurrency_Id = objSingleDetailTransactionCashIn.Asing_FK_MsCurrency_Id

                    objKas.DetailTransactionCashIn.Add(objCalonInsert)
                Next

                objResume.Add(objKas)

            Next
        End Using

        Using objListTransactionCashOut As TList(Of LTKTTransactionCashOut) = DataRepository.LTKTTransactionCashOutProvider.GetPaged(LTKTTransactionCashOutColumn.FK_LTKT_Id.ToString & " = " & getLTKTPK.ToString, "", 0, Integer.MaxValue, 0)
            For Each objSingleTransactinCashOut As LTKTTransactionCashOut In objListTransactionCashOut


                Dim objKas As ResumeKasMasukKeluar = New ResumeKasMasukKeluar
                'Insert untuk tampilan Grid
                objKas.TransactionDate = objSingleTransactinCashOut.TanggalTransaksi
                objKas.Branch = objSingleTransactinCashOut.NamaKantorPJK
                objKas.TransactionNominal = objSingleTransactinCashOut.Total.ToString
                objKas.AccountNumber = objSingleTransactinCashOut.NomorRekening
                objKas.Kas = "Kas Keluar"
                objKas.Type = "LTKT"

                'Insert Data Ke ObjectTransaction
                objKas.TransactionCashOut = New LTKTTransactionCashOut
                objKas.TransactionCashOut.PK_LTKTTransactionCashOut_Id = objSingleTransactinCashOut.PK_LTKTTransactionCashOut_Id

                objKas.TransactionCashOut.TanggalTransaksi = objSingleTransactinCashOut.TanggalTransaksi.GetValueOrDefault
                objKas.TransactionCashOut.NamaKantorPJK = objSingleTransactinCashOut.NamaKantorPJK
                objKas.TransactionCashOut.FK_MsKotaKab_Id = objSingleTransactinCashOut.FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashOut.FK_MsProvince_Id = objSingleTransactinCashOut.FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashOut.NomorRekening = objSingleTransactinCashOut.NomorRekening
                objKas.TransactionCashOut.INDV_Gelar = objSingleTransactinCashOut.INDV_Gelar
                objKas.TransactionCashOut.INDV_NamaLengkap = objSingleTransactinCashOut.INDV_NamaLengkap
                objKas.TransactionCashOut.INDV_TempatLahir = objSingleTransactinCashOut.INDV_TempatLahir
                objKas.TransactionCashOut.INDV_TanggalLahir = objSingleTransactinCashOut.INDV_TanggalLahir.GetValueOrDefault
                objKas.TransactionCashOut.INDV_Kewarganegaraan = objSingleTransactinCashOut.INDV_Kewarganegaraan
                objKas.TransactionCashOut.INDV_ID_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsNegara_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_DOM_NamaJalan = objSingleTransactinCashOut.INDV_DOM_NamaJalan
                objKas.TransactionCashOut.INDV_DOM_RTRW = objSingleTransactinCashOut.INDV_DOM_RTRW
                objKas.TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsKelurahan_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsKecamatan_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_DOM_KodePos = objSingleTransactinCashOut.INDV_DOM_KodePos
                objKas.TransactionCashOut.INDV_DOM_FK_MsProvince_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_ID_NamaJalan = objSingleTransactinCashOut.INDV_ID_NamaJalan
                objKas.TransactionCashOut.INDV_ID_RTRW = objSingleTransactinCashOut.INDV_ID_RTRW
                objKas.TransactionCashOut.INDV_ID_FK_MsKelurahan_Id = objSingleTransactinCashOut.INDV_ID_FK_MsKelurahan_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_ID_FK_MsKecamatan_Id = objSingleTransactinCashOut.INDV_ID_FK_MsKecamatan_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_ID_FK_MsKotaKab_Id = objSingleTransactinCashOut.INDV_ID_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_ID_KodePos = objSingleTransactinCashOut.INDV_ID_KodePos
                objKas.TransactionCashOut.INDV_ID_FK_MsProvince_Id = objSingleTransactinCashOut.INDV_ID_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_NA_NamaJalan = objSingleTransactinCashOut.INDV_NA_NamaJalan
                objKas.TransactionCashOut.INDV_NA_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_NA_FK_MsNegara_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_NA_FK_MsProvince_Id = objSingleTransactinCashOut.INDV_NA_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_NA_FK_MsKotaKab_Id = objSingleTransactinCashOut.INDV_NA_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_NA_KodePos = objSingleTransactinCashOut.INDV_NA_KodePos
                objKas.TransactionCashOut.INDV_FK_MsIDType_Id = objSingleTransactinCashOut.INDV_FK_MsIDType_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_NomorId = objSingleTransactinCashOut.INDV_NomorId
                objKas.TransactionCashOut.INDV_NPWP = objSingleTransactinCashOut.INDV_NPWP
                objKas.TransactionCashOut.INDV_FK_MsPekerjaan_Id = objSingleTransactinCashOut.INDV_FK_MsPekerjaan_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_Jabatan = objSingleTransactinCashOut.INDV_Jabatan
                objKas.TransactionCashOut.INDV_PenghasilanRataRata = objSingleTransactinCashOut.INDV_PenghasilanRataRata
                objKas.TransactionCashOut.INDV_TempatBekerja = objSingleTransactinCashOut.INDV_TempatBekerja
                objKas.TransactionCashOut.INDV_TujuanTransaksi = objSingleTransactinCashOut.INDV_TujuanTransaksi
                objKas.TransactionCashOut.INDV_SumberDana = objSingleTransactinCashOut.INDV_SumberDana
                objKas.TransactionCashOut.INDV_NamaBankLain = objSingleTransactinCashOut.INDV_NamaBankLain
                objKas.TransactionCashOut.INDV_NomorRekeningTujuan = objSingleTransactinCashOut.INDV_NomorRekeningTujuan

                objKas.TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id = objSingleTransactinCashOut.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_Nama = objSingleTransactinCashOut.CORP_Nama
                objKas.TransactionCashOut.CORP_FK_MsBidangUsaha_Id = objSingleTransactinCashOut.CORP_FK_MsBidangUsaha_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_TipeAlamat = objSingleTransactinCashOut.CORP_TipeAlamat.GetValueOrDefault
                objKas.TransactionCashOut.CORP_NamaJalan = objSingleTransactinCashOut.CORP_NamaJalan
                objKas.TransactionCashOut.CORP_RTRW = objSingleTransactinCashOut.CORP_RTRW
                objKas.TransactionCashOut.CORP_FK_MsKelurahan_Id = objSingleTransactinCashOut.CORP_FK_MsKelurahan_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_FK_MsKecamatan_Id = objSingleTransactinCashOut.CORP_FK_MsKecamatan_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_FK_MsKotaKab_Id = objSingleTransactinCashOut.CORP_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_KodePos = objSingleTransactinCashOut.CORP_KodePos
                objKas.TransactionCashOut.CORP_FK_MsProvince_Id = objSingleTransactinCashOut.CORP_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_FK_MsNegara_Id = objSingleTransactinCashOut.CORP_FK_MsNegara_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_LN_NamaJalan = objSingleTransactinCashOut.CORP_LN_NamaJalan
                objKas.TransactionCashOut.CORP_LN_FK_MsNegara_Id = objSingleTransactinCashOut.CORP_LN_FK_MsNegara_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_LN_FK_MsProvince_Id = objSingleTransactinCashOut.CORP_LN_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_LN_MsKotaKab_Id = objSingleTransactinCashOut.CORP_LN_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_LN_KodePos = objSingleTransactinCashOut.CORP_LN_KodePos
                objKas.TransactionCashOut.CORP_NPWP = objSingleTransactinCashOut.CORP_NPWP
                objKas.TransactionCashOut.CORP_TujuanTransaksi = objSingleTransactinCashOut.CORP_TujuanTransaksi
                objKas.TransactionCashOut.CORP_SumberDana = objSingleTransactinCashOut.CORP_SumberDana
                objKas.TransactionCashOut.CORP_NamaBankLain = objSingleTransactinCashOut.CORP_NamaBankLain
                objKas.TransactionCashOut.CORP_NomorRekeningTujuan = objSingleTransactinCashOut.CORP_NomorRekeningTujuan


                'Insert Data Ke Detail Transaction
                objKas.DetailTranscationCashOut = New List(Of LTKTDetailCashOutTransaction)

                Dim objListDetailTransactionCashOut As TList(Of LTKTDetailCashOutTransaction) = DataRepository.LTKTDetailCashOutTransactionProvider.GetPaged(LTKTDetailCashOutTransactionColumn.FK_LTKTTransactionCashOut_Id.ToString & " = '" & objSingleTransactinCashOut.PK_LTKTTransactionCashOut_Id.ToString & "'", "", 0, Integer.MaxValue, 0)
                For Each objSingleDetailTransactionCashOut As LTKTDetailCashOutTransaction In objListDetailTransactionCashOut
                    Dim objCalonInsert As New LTKTDetailCashOutTransaction
                    objCalonInsert.PK_LTKTDetailCashOutTransaction_Id = objSingleDetailTransactionCashOut.PK_LTKTDetailCashOutTransaction_Id
                    objCalonInsert.Asing_KursTransaksi = objSingleDetailTransactionCashOut.Asing_KursTransaksi
                    objCalonInsert.Asing_TotalKasKeluarDalamRupiah = objSingleDetailTransactionCashOut.Asing_TotalKasKeluarDalamRupiah
                    objCalonInsert.TotalKasKeluar = objSingleDetailTransactionCashOut.TotalKasKeluar
                    objCalonInsert.KasKeluar = objSingleDetailTransactionCashOut.KasKeluar
                    objCalonInsert.Asing_FK_MsCurrency_Id = objSingleDetailTransactionCashOut.Asing_FK_MsCurrency_Id

                    objKas.DetailTranscationCashOut.Add(objCalonInsert)
                Next

                objResume.Add(objKas)

            Next
        End Using
        SetnGetResumeKasMasukKasKeluar = objResume
        grvTransaksi.DataSource = objResume
        grvTransaksi.DataBind()

        Dim totalKasMasuk As Integer = 0
        Dim totalKasKeluar As Integer = 0
        For Each kas As ResumeKasMasukKeluar In objResume
            If kas.Kas = "Kas Masuk" Then
                totalKasMasuk += CInt(kas.TransactionNominal)
            Else
                totalKasKeluar += CInt(kas.TransactionNominal)
            End If
        Next

        lblTotalKasMasuk.Text = totalKasMasuk.ToString
        lblTotalKasKeluar.Text = totalKasKeluar.ToString

    End Sub

    Sub loadLTKTToField()
        Using objLTKT As LTKT = DataRepository.LTKTProvider.GetByPK_LTKT_Id(getLTKTPK)

            If Not IsNothing(objLTKT) Then

                'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
                Dim i As Integer = 0 'buat iterasi
                Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetAll
                Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
                Dim objKelurahan As TList(Of MsKelurahan) = DataRepository.MsKelurahanProvider.GetAll
                Dim objSKelurahan As MsKelurahan 'Penampung hasil search kelurahan
                Dim objKecamatan As TList(Of MsKecamatan) = DataRepository.MsKecamatanProvider.GetAll
                Dim objSkecamatan As MsKecamatan 'Penampung hasil search kecamatan
                Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
                Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
                Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetAll
                Dim objSMapNegara As MappingMsNegaraNCBSPPATK 'Penampung hasil search mappingnegara yang pke picker
                Dim objNegara As TList(Of MsNegaraNCBS) = DataRepository.MsNegaraNCBSProvider.GetAll
                Dim objSnegara As MsNegaraNCBS 'Penampung hasil search negara yang pke picker
                Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetAll
                Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
                Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
                Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha

                txtUmumPJKPelapor.Text = objLTKT.NamaPJKPelapor
                txtTglLaporan.Text = Convert.ToDateTime(objLTKT.TanggalLaporan).ToString("dd-MMM-yyyy")
                txtPejabatPelapor.Text = objLTKT.NamaPejabatPJKPelapor
                If objLTKT.NoLTKTKoreksi <> "" Then
                    cboTipeLaporan.SelectedIndex = 1
                    txtNoLTKTKoreksi.Text = objLTKT.NoLTKTKoreksi
                    tipeLaporanCheck()
                End If
                i = 0
                For Each listKepemilikan As ListItem In cboTerlaporKepemilikan.Items
                    If objLTKT.FK_MsKepemilikan_ID.ToString = listKepemilikan.Value Then
                        cboTerlaporKepemilikan.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtLTKTInformasiLainnya.Text = objLTKT.InformasiLainnya
                txtTerlaporNoRekening.Text = objLTKT.NoRekening
                txtCIFNo.Text = objLTKT.CIFNo.ToString
                rblTerlaporTipePelapor.SelectedIndex = CInt(objLTKT.TipeTerlapor)
                LTKTTipeTerlaporChange()
                txtTerlaporGelar.Text = objLTKT.INDV_Gelar
                txtTerlaporNamaLengkap.Text = objLTKT.INDV_NamaLengkap
                txtTerlaporTempatLahir.Text = objLTKT.INDV_TempatLahir
                txtTerlaporTglLahir.Text = Convert.ToDateTime(objLTKT.INDV_TanggalLahir).ToString("dd-MMM-yyyy")
                rblTerlaporKewarganegaraan.SelectedIndex = CInt(objLTKT.INDV_Kewarganegaraan)
                i = 0
                For Each listNegara As ListItem In cboTerlaporNegara.Items
                    If objLTKT.INDV_FK_MsNegara_Id.GetValueOrDefault.ToString = listNegara.Value Then
                        cboTerlaporNegara.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtTerlaporDOMNamaJalan.Text = objLTKT.INDV_DOM_NamaJalan
                txtTerlaporDOMRTRW.Text = objLTKT.INDV_DOM_RTRW
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objLTKT.INDV_DOM_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTerlaporDOMKelurahan.Text = objSKelurahan.NamaKelurahan
                    hfTerlaporDOMKelurahan.Value = objSKelurahan.IDKelurahan
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objLTKT.INDV_DOM_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTerlaporDOMKecamatan.Text = objSkecamatan.NamaKecamatan
                    hfTerlaporDOMKecamatan.Value = objSkecamatan.IDKecamatan
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.INDV_DOM_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTerlaporDOMKotaKab.Text = objSkotaKab.NamaKotaKab
                    hfTerlaporDOMKotaKab.Value = objSkotaKab.IDKotaKab
                End If
                txtTerlaporDOMKodePos.Text = objLTKT.INDV_DOM_KodePos
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.INDV_DOM_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTerlaporDOMProvinsi.Text = objSProvinsi.Nama
                    hfTerlaporDOMProvinsi.Value = objSProvinsi.IdProvince
                End If
                txtTerlaporIDNamaJalan.Text = objLTKT.INDV_ID_NamaJalan
                txtTerlaporIDRTRW.Text = objLTKT.INDV_ID_RTRW
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objLTKT.INDV_ID_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTerlaporIDKelurahan.Text = objSKelurahan.NamaKelurahan
                    hfTerlaporIDKelurahan.Value = objSKelurahan.IDKelurahan
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objLTKT.INDV_ID_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTerlaporIDKecamatan.Text = objSkecamatan.NamaKecamatan
                    hfTerlaporIDKecamatan.Value = objSkecamatan.IDKecamatan
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.INDV_ID_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTerlaporIDKotaKab.Text = objSkotaKab.NamaKotaKab
                    hfTerlaporIDKotaKab.Value = objSkotaKab.IDKotaKab
                End If
                txtTerlaporIDKodePos.Text = objLTKT.INDV_ID_KodePos
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.INDV_ID_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTerlaporIDProvinsi.Text = objSProvinsi.Nama
                    hfTerlaporIDProvinsi.Value = objSProvinsi.IdProvince
                End If
                txtTerlaporNANamaJalan.Text = objLTKT.INDV_NA_NamaJalan
                objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objLTKT.INDV_NA_FK_MsNegara_Id)
                If Not IsNothing(objSMapNegara) Then
                    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                    If Not IsNothing(objSnegara) Then
                        txtTerlaporNANegara.Text = objSnegara.NamaNegara
                        hfTerlaporIDNegara.Value = objSnegara.IDNegaraNCBS
                    End If
                End If
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.INDV_NA_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTerlaporNAProvinsi.Text = objSProvinsi.Nama
                    hfTerlaporNAProvinsi.Value = objSProvinsi.IdProvince
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.INDV_NA_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTerlaporNAKota.Text = objSkotaKab.NamaKotaKab
                    hfTerlaporIDKota.Value = objSkotaKab.IDKotaKab
                End If
                txtTerlaporNAKodePos.Text = objLTKT.INDV_NA_KodePos
                i = 0
                For Each jenisDoc As ListItem In cboTerlaporJenisDocID.Items
                    If objLTKT.INDV_FK_MsIDType_Id.GetValueOrDefault.ToString = jenisDoc.Value Then
                        cboTerlaporJenisDocID.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtTerlaporNomorID.Text = objLTKT.INDV_NomorId
                txtTerlaporNPWP.Text = objLTKT.INDV_NPWP
                objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objLTKT.INDV_FK_MsPekerjaan_Id)
                If Not IsNothing(objSPekerjaan) Then
                    txtTerlaporPekerjaan.Text = objSPekerjaan.NamaPekerjaan
                    hfTerlaporPekerjaan.Value = objSPekerjaan.IDPekerjaan
                End If
                txtTerlaporJabatan.Text = objLTKT.INDV_Jabatan
                txtTerlaporPenghasilanRataRata.Text = objLTKT.INDV_PenghasilanRataRata
                txtTerlaporTempatKerja.Text = objLTKT.INDV_TempatBekerja
                i = 0
                For Each listBentukBadanUsaha As ListItem In cboTerlaporCORPBentukBadanUsaha.Items
                    If objLTKT.CORP_FK_MsBentukBadanUsaha_Id.ToString = listBentukBadanUsaha.Value Then
                        cboTerlaporCORPBentukBadanUsaha.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtTerlaporCORPNama.Text = objLTKT.CORP_Nama
                objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objLTKT.CORP_FK_MsBidangUsaha_Id)
                If Not IsNothing(objSBidangUsaha) Then
                    txtTerlaporCORPBidangUsaha.Text = objSBidangUsaha.NamaBidangUsaha
                    hfTerlaporCORPBidangUsaha.Value = objSBidangUsaha.IdBidangUsaha
                End If
                If objLTKT.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                    rblTerlaporCORPTipeAlamat.SelectedIndex = CInt(objLTKT.CORP_TipeAlamat)
                End If
                txtTerlaporCORPDLNamaJalan.Text = objLTKT.CORP_NamaJalan
                txtTerlaporCORPDLRTRW.Text = objLTKT.CORP_RTRW

                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objLTKT.CORP_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTerlaporCORPDLKelurahan.Text = objSKelurahan.NamaKelurahan
                    hfTerlaporCORPDLKelurahan.Value = objSKelurahan.IDKelurahan
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objLTKT.CORP_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTerlaporCORPDLKecamatan.Text = objSkecamatan.NamaKecamatan
                    hfTerlaporCORPDLKecamatan.Value = objSkecamatan.IDKecamatan
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.CORP_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTerlaporCORPDLKotaKab.Text = objSkotaKab.NamaKotaKab
                    hfTerlaporCORPDLKotaKab.Value = objSkotaKab.IDKotaKab
                End If
                txtTerlaporCORPDLKodePos.Text = objLTKT.CORP_KodePos
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.CORP_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTerlaporCORPDLProvinsi.Text = objSProvinsi.Nama
                    hfTerlaporCORPDLProvinsi.Value = objSProvinsi.IdProvince
                End If
                objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objLTKT.CORP_FK_MsNegara_Id)
                If Not IsNothing(objSMapNegara) Then
                    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                    If Not IsNothing(objSnegara) Then
                        txtTerlaporCORPDLNegara.Text = objSnegara.NamaNegara
                        hfTerlaporCORPDLNegara.Value = objSnegara.IDNegaraNCBS
                    End If
                End If
                txtTerlaporCORPLNNamaJalan.Text = objLTKT.CORP_LN_NamaJalan
                objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objLTKT.CORP_LN_FK_MsNegara_Id)
                If Not IsNothing(objSMapNegara) Then
                    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                    If Not IsNothing(objSnegara) Then
                        txtTerlaporCORPLNNegara.Text = objSnegara.NamaNegara
                        hfTerlaporCORPLNNegara.Value = objSnegara.IDNegaraNCBS
                    End If
                End If
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.CORP_LN_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTerlaporCORPLNProvinsi.Text = objSProvinsi.Nama
                    hfTerlaporCORPLNProvinsi.Value = objSProvinsi.IdProvince
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.CORP_LN_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTerlaporCORPLNKota.Text = objSkotaKab.NamaKotaKab
                    hfTerlaporCORPLNKota.Value = objSkotaKab.IDKotaKab
                End If
                txtTerlaporCORPLNKodePos.Text = objLTKT.CORP_LN_KodePos
                txtTerlaporCORPNPWP.Text = objLTKT.CORP_NPWP

            End If





        End Using
    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            SetControlLoad()
            clearSession()
            loadLTKTToField()
            loadResume()
            If MultiView1.ActiveViewIndex = 0 Then
                ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            End If
        End If
    End Sub

    Private Sub SetControlLoad()
        rblTerlaporTipePelapor.SelectedValue = "Perorangan"
        divPerorangan.Visible = True
        divKorporasi.Visible = False

        rblTRXKMTipePelapor.SelectedValue = "Perorangan"
        tblTRXKMTipePelapor.Visible = True
        tblTRXKMTipePelaporKorporasi.Visible = False

        rblTRXKKTipePelapor.SelectedValue = "Perorangan"
        tblTRXKKTipePelaporPerorangan.Visible = True
        tblTRXKKTipePelaporKorporasi.Visible = False

        'bind MsKepemilikan
        Using objPemilik As TList(Of MsKepemilikan) = DataRepository.MsKepemilikanProvider.GetAll
            If objPemilik.Count > 0 Then
                cboTerlaporKepemilikan.Items.Clear()
                cboTerlaporKepemilikan.Items.Add("-Select-")
                For i As Integer = 0 To objPemilik.Count - 1
                    cboTerlaporKepemilikan.Items.Add(New ListItem(objPemilik(i).NamaKepemilikan, objPemilik(i).IDKepemilikan.ToString))
                Next
            End If
        End Using

        'bind MsNegara
        Using objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetAll
            If objNegara.Count > 0 Then
                cboTerlaporNegara.Items.Clear()
                cboTerlaporNegara.Items.Add("-Select-")

                cboTRXKMINDVNegara.Items.Clear()
                cboTRXKMINDVNegara.Items.Add("-Select-")

                cboTRXKKINDVNegara.Items.Clear()
                cboTRXKKINDVNegara.Items.Add("-Select-")
                For i As Integer = 0 To objNegara.Count - 1
                    cboTerlaporNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                    cboTRXKMINDVNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                    cboTRXKKINDVNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                Next
            End If
        End Using

        'Bind MsBentukBadanUsaha
        Using objBentukBadanUsaha As TList(Of MsBentukBidangUsaha) = DataRepository.MsBentukBidangUsahaProvider.GetAll
            If objBentukBadanUsaha.Count > 0 Then
                cboTerlaporCORPBentukBadanUsaha.Items.Clear()
                cboTerlaporCORPBentukBadanUsaha.Items.Add("-Select-")

                cboTRXKMCORPBentukBadanUsaha.Items.Clear()
                cboTRXKMCORPBentukBadanUsaha.Items.Add("-Select-")

                cboTRXKKCORPBentukBadanUsaha.Items.Clear()
                cboTRXKKCORPBentukBadanUsaha.Items.Add("-Select-")

                For i As Integer = 0 To objBentukBadanUsaha.Count - 1
                    cboTerlaporCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    cboTRXKMCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    cboTRXKKCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                Next
            End If
        End Using

        'bind ID type
        Using objJenisID As TList(Of MsIDType) = DataRepository.MsIDTypeProvider.GetAll
            cboTerlaporJenisDocID.Items.Clear()
            cboTerlaporJenisDocID.Items.Add("-Select-")

            cboTRXKMINDVJenisID.Items.Clear()
            cboTRXKMINDVJenisID.Items.Add("-Select-")

            cboTRXKKINDVJenisID.Items.Clear()
            cboTRXKKINDVJenisID.Items.Add("-Select-")

            For i As Integer = 0 To objJenisID.Count - 1
                cboTerlaporJenisDocID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
                cboTRXKMINDVJenisID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
                cboTRXKKINDVJenisID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
            Next




        End Using

        'bind calendar
        Me.popUpTglLaporan.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTglLaporan.ClientID & "'), 'dd-mmm-yyyy')")
        Me.popUpTglLaporan.Style.Add("display", "")
        Me.popTglLahirTerlapor.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTerlaporTglLahir.ClientID & "'), 'dd-mmm-yyyy')")
        Me.popTglLahirTerlapor.Style.Add("display", "")
        Me.popTRXKMTanggalTrx.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTRXKMTanggalTrx.ClientID & "'), 'dd-mmm-yyyy')")
        Me.popTRXKMTanggalTrx.Style.Add("display", "")
        Me.popTRXKMINDVTanggalLahir.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTRXKMINDVTanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
        Me.popTRXKMINDVTanggalLahir.Style.Add("display", "")
        Me.popTRXKKTanggalTransaksi.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTRXKKTanggalTransaksi.ClientID & "'), 'dd-mmm-yyyy')")
        Me.popTRXKKTanggalTransaksi.Style.Add("display", "")
        Me.popTRXKKINDVTglLahir.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTRXKKINDVTglLahir.ClientID & "'), 'dd-mmm-yyyy')")
        Me.popTRXKKINDVTglLahir.Style.Add("display", "")

        'bind popup
        'kelurahan
        Me.imgTerlaporDOMKelurahan.Attributes.Add("onClick", "javascript:popWin2(" & Me.txtTerlaporDOMKelurahan.ClientID & ", " & Me.hfTerlaporDOMKelurahan.ClientID & ");")
        Me.imgTerlaporIDKelurahan.Attributes.Add("onClick", "javascript:popWin2(" & Me.txtTerlaporIDKelurahan.ClientID & ", " & Me.hfTerlaporIDKelurahan.ClientID & ");")
        Me.imgTRXKMINDVDOMKelurahan.Attributes.Add("onClick", "javascript:popWin2(" & Me.txtTRXKMINDVDOMKelurahan.ClientID & ", " & Me.hfTRXKMINDVDOMKelurahan.ClientID & ");")
        Me.imgTRXKMINDVIDKelurahan.Attributes.Add("onClick", "javascript:popWin2(" & Me.txtTRXKMINDVIDKelurahan.ClientID & ", " & Me.hfTRXKMINDVIDKelurahan.ClientID & ");")
        Me.imgTerlaporCORPDLKelurahan.Attributes.Add("onClick", "javascript:popWin2(" & Me.txtTerlaporCORPDLKelurahan.ClientID & ", " & Me.hfTerlaporCORPDLKelurahan.ClientID & ");")
        Me.imgTRXKMCORPDLKelurahan.Attributes.Add("onClick", "javascript:popWin2(" & Me.txtTRXKMCORPDLKelurahan.ClientID & ", " & Me.hfTRXKMCORPDLKelurahan.ClientID & ");")
        Me.imgTRXKKINDVDOMKelurahan.Attributes.Add("onClick", "javascript:popWin2(" & Me.txtTRXKKINDVDOMKelurahan.ClientID & ", " & Me.hfTRXKKINDVDOMKelurahan.ClientID & ");")
        Me.imgTRXKKINDVIDKelurahan.Attributes.Add("onClick", "javascript:popWin2(" & Me.txtTRXKKINDVIDKelurahan.ClientID & ", " & Me.hfTRXKKINDVIDKelurahan.ClientID & ");")
        Me.imgTRXKKCORPDLKelurahan.Attributes.Add("onClick", "javascript:popWin2(" & Me.txtTRXKKCORPDLKelurahan.ClientID & ", " & Me.hfTRXKKCORPDLKelurahan.ClientID & ");")

        'kecamatan
        Me.imgTerlaporDOMKecamatan.Attributes.Add("onClick", "javascript:popWinKecamatan(" & Me.txtTerlaporDOMKecamatan.ClientID & ", " & Me.hfTerlaporDOMKecamatan.ClientID & ");")
        Me.imgTerlaporIDKecamatan.Attributes.Add("onClick", "javascript:popWinKecamatan(" & Me.txtTerlaporIDKecamatan.ClientID & ", " & Me.hfTerlaporIDKecamatan.ClientID & ");")
        Me.imgTerlaporCORPDLKecamatan.Attributes.Add("onClick", "javascript:popWinKecamatan(" & Me.txtTerlaporCORPDLKecamatan.ClientID & ", " & Me.hfTerlaporCORPDLKecamatan.ClientID & ");")
        Me.imgTRXKMINDVDOMKecamatan.Attributes.Add("onClick", "javascript:popWinKecamatan(" & Me.txtTRXKMINDVDOMKecamatan.ClientID & ", " & Me.hfTRXKMINDVDOMKecamatan.ClientID & ");")
        Me.imgTRXKMINDVIDKecamatan.Attributes.Add("onClick", "javascript:popWinKecamatan(" & Me.txtTRXKMINDVIDKecamatan.ClientID & ", " & Me.hfTRXKMINDVIDKecamatan.ClientID & ");")
        Me.imgTRXKMCORPDLKecamatan.Attributes.Add("onClick", "javascript:popWinKecamatan(" & Me.txtTRXKMCORPDLKecamatan.ClientID & ", " & Me.hfTRXKMCORPDLKecamatan.ClientID & ");")
        Me.imgTRXKKINDVDOMKecamatan.Attributes.Add("onClick", "javascript:popWinKecamatan(" & Me.txtTRXKKINDVDOMKecamatan.ClientID & ", " & Me.hfTRXKKINDVDOMKecamatan.ClientID & ");")
        Me.imgTRXKKINDVIDKecamatan.Attributes.Add("onClick", "javascript:popWinKecamatan(" & Me.txtTRXKKINDVIDKecamatan.ClientID & ", " & Me.hfTRXKKINDVIDKecamatan.ClientID & ");")
        Me.imgTRXKKCORPDLKecamatan.Attributes.Add("onClick", "javascript:popWinKecamatan(" & Me.txtTRXKKCORPDLKecamatan.ClientID & ", " & Me.hfTRXKKCORPDLKecamatan.ClientID & ");")

        'kota kabupaten
        Me.imgTerlaporDOMKotaKab.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTerlaporDOMKotaKab.ClientID & ", " & Me.hfTerlaporDOMKotaKab.ClientID & ");")
        Me.imgTerlaporIDKotaKab.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTerlaporIDKotaKab.ClientID & ", " & Me.hfTerlaporIDKotaKab.ClientID & ");")
        Me.imgTerlaporCORPDLKotaKab.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTerlaporCORPDLKotaKab.ClientID & ", " & Me.hfTerlaporCORPDLKotaKab.ClientID & ");")
        Me.imgTRXKMKotaKab.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTRXKMKotaKab.ClientID & ", " & Me.hfTRXKMKotaKab.ClientID & ");")
        Me.imgTRXKMINDVDOMKotaKab.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTRXKMINDVDOMKotaKab.ClientID & ", " & Me.hfTRXKMINDVDOMKotaKab.ClientID & ");")
        Me.imgTRXKMINDVIDKotaKab.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTRXKMINDVIDKotaKab.ClientID & ", " & Me.hfTRXKMINDVIDKotaKab.ClientID & ");")
        Me.imgTRXKMCORPDLKotaKab.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTRXKMCORPDLKotaKab.ClientID & ", " & Me.hfTRXKMCORPDLKotaKab.ClientID & ");")
        Me.imgTRXKKKotaKab.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTRXKKKotaKab.ClientID & ", " & Me.hfTRXKKKotaKab.ClientID & ");")
        Me.imgTRXKKINDVDOMKotaKab.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTRXKKINDVDOMKotaKab.ClientID & ", " & Me.hfTRXKKINDVDOMKotaKab.ClientID & ");")
        Me.imgTRXKKINDVIDKotaKab.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTRXKKINDVIDKotaKab.ClientID & ", " & Me.hfTRXKKINDVIDKotaKab.ClientID & ");")
        Me.imgTRXKKCORPDLKotaKab.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTRXKKCORPDLKotaKab.ClientID & ", " & Me.hfTRXKKCORPDLKotaKab.ClientID & ");")
        Me.imgTerlaporIDKota.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTerlaporNAKota.ClientID & ", " & Me.hfTerlaporIDKota.ClientID & ");")
        Me.imgTRXKMINDVNAKota.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTRXKMINDVNAKota.ClientID & ", " & Me.hfTRXKMINDVNAKota.ClientID & ");")
        Me.imgTRXKMCORPLNKota.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTRXKMCORPLNKota.ClientID & ", " & Me.hfTRXKMCORPLNKota.ClientID & ");")
        Me.imgTRXKKCORPLNKota.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTRXKKCORPLNKota.ClientID & ", " & Me.hfTRXKKCORPLNKota.ClientID & ");")
        Me.imgTerlaporCORPLNKota.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTerlaporCORPLNKota.ClientID & ", " & Me.hfTerlaporCORPLNKota.ClientID & ");")
        Me.imgTRXKKINDVNAKota.Attributes.Add("onClick", "javascript:popWinKotaKab(" & Me.txtTRXKKINDVNAKota.ClientID & ", " & Me.hfTRXKKINDVNAKota.ClientID & ");")

        'provinsi
        Me.imgTerlaporIDProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTerlaporIDProvinsi.ClientID & ", " & Me.hfTerlaporIDProvinsi.ClientID & ");")
        Me.imgTerlaporDOMProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTerlaporDOMProvinsi.ClientID & ", " & Me.hfTerlaporDOMProvinsi.ClientID & ");")
        Me.imgTerlaporNAProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTerlaporNAProvinsi.ClientID & ", " & Me.hfTerlaporNAProvinsi.ClientID & ");")
        Me.imgTerlaporCORPDLProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTerlaporCORPDLProvinsi.ClientID & ", " & Me.hfTerlaporCORPDLProvinsi.ClientID & ");")
        Me.imgTerlaporCORPLNProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTerlaporCORPLNProvinsi.ClientID & ", " & Me.hfTerlaporCORPLNProvinsi.ClientID & ");")
        Me.imgTRXKMINDVDOMProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTRXKMINDVDOMProvinsi.ClientID & ", " & Me.hfTRXKMINDVDOMProvinsi.ClientID & ");")
        Me.imgTRXKMINDVIDProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTRXKMINDVIDProvinsi.ClientID & ", " & Me.hfTRXKMINDVIDProvinsi.ClientID & ");")
        Me.imgTRXKMINDVNAProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTRXKMINDVNAProvinsi.ClientID & ", " & Me.hfTRXKMINDVNAProvinsi.ClientID & ");")
        Me.imgTRXKMCORPDLProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTRXKMCORPDLProvinsi.ClientID & ", " & Me.hfTRXKMCORPDLProvinsi.ClientID & ");")
        Me.imgTRXKMCORPLNProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTRXKMCORPLNProvinsi.ClientID & ", " & Me.hfTRXKMCORPLNProvinsi.ClientID & ");")
        Me.imgTRXKKINDVDOMProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTRXKKINDVDOMProvinsi.ClientID & ", " & Me.hfTRXKKINDVDOMProvinsi.ClientID & ");")
        Me.imgTRXKKINDVIDProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTRXKKINDVIDProvinsi.ClientID & ", " & Me.hfTRXKKINDVIDProvinsi.ClientID & ");")
        Me.imgTRXKKINDVNAProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTRXKKINDVNAProvinsi.ClientID & ", " & Me.hfTRXKKINDVNAProvinsi.ClientID & ");")
        Me.imgTRXKKCORPDLProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTRXKKCORPDLProvinsi.ClientID & ", " & Me.hfTRXKKCORPDLProvinsi.ClientID & ");")
        Me.imgTRXKKCORPLNProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTRXKKCORPLNProvinsi.ClientID & ", " & Me.hfTRXKKCORPLNProvinsi.ClientID & ");")
        Me.imgTRXKMProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTRXKMProvinsi.ClientID & ", " & Me.hfTRXKMProvinsi.ClientID & ");")
        Me.imgTRXKKProvinsi.Attributes.Add("onClick", "javascript:popWinProvinsi(" & Me.txtTRXKKProvinsi.ClientID & ", " & Me.hfTRXKKProvinsi.ClientID & ");")


        'Negara
        Me.imgTerlaporIDNegara.Attributes.Add("onClick", "javascript:popWinNegara(" & Me.txtTerlaporNANegara.ClientID & ", " & Me.hfTerlaporIDNegara.ClientID & ");")
        Me.imgTerlaporCORPDLNegara.Attributes.Add("onClick", "javascript:popWinNegara(" & Me.txtTerlaporCORPDLNegara.ClientID & ", " & Me.hfTerlaporCORPDLNegara.ClientID & ");")
        Me.imgTerlaporCORPLNNegara.Attributes.Add("onClick", "javascript:popWinNegara(" & Me.txtTerlaporCORPLNNegara.ClientID & ", " & Me.hfTerlaporCORPLNNegara.ClientID & ");")
        Me.imgTRXKMINDVNANegara.Attributes.Add("onClick", "javascript:popWinNegara(" & Me.txtTRXKMINDVNANegara.ClientID & ", " & Me.hfTRXKMINDVNANegara.ClientID & ");")
        Me.imgTRXKMCORPDLNegara.Attributes.Add("onClick", "javascript:popWinNegara(" & Me.txtTRXKMCORPDLNegara.ClientID & ", " & Me.hfTRXKMCORPDLNegara.ClientID & ");")
        Me.imgTRXKMCORPLNNegara.Attributes.Add("onClick", "javascript:popWinNegara(" & Me.txtTRXKMCORPLNNegara.ClientID & ", " & Me.hfTRXKMCORPLNNegara.ClientID & ");")
        Me.imgTRXKKINDVNANegara.Attributes.Add("onClick", "javascript:popWinNegara(" & Me.txtTRXKKINDVNANegara.ClientID & ", " & Me.hfTRXKKINDVNANegara.ClientID & ");")
        Me.imgTRXKKCORPDLNegara.Attributes.Add("onClick", "javascript:popWinNegara(" & Me.txtTRXKKCORPDLNegara.ClientID & ", " & Me.hfTRXKKCORPDLNegara.ClientID & ");")
        Me.imgTRXKKCORPLNNegara.Attributes.Add("onClick", "javascript:popWinNegara(" & Me.txtTRXKKCORPLNNegara.ClientID & ", " & Me.hfTRXKKCORPLNNegara.ClientID & ");")

        'Mata Uang
        Me.imgTRXKMDetilMataUang.Attributes.Add("onClick", "javascript:popWinMataUang(" & Me.txtTRXKMDetilMataUang.ClientID & ", " & Me.hfTRXKMDetilMataUang.ClientID & ");")
        Me.imgTRXKKDetilMataUang.Attributes.Add("onClick", "javascript:popWinMataUang(" & Me.txtTRXKKDetilMataUang.ClientID & ", " & Me.hfTRXKKDetilMataUang.ClientID & ");")


        'Bidang Usaha imgTRXKMCORPBidangUsaha
        Me.imgTerlaporCORPBidangUsaha.Attributes.Add("onClick", "javascript:popWinBidangUsaha(" & Me.txtTerlaporCORPBidangUsaha.ClientID & ", " & Me.hfTerlaporCORPBidangUsaha.ClientID & ");")
        Me.imgTRXKMCORPBidangUsaha.Attributes.Add("onClick", "javascript:popWinBidangUsaha(" & Me.txtTRXKMCORPBidangUsaha.ClientID & ", " & Me.hfTRXKMCORPBidangUsaha.ClientID & ");")
        Me.imgTRXKKCORPBidangUsaha.Attributes.Add("onClick", "javascript:popWinBidangUsaha(" & Me.txtTRXKKCORPBidangUsaha.ClientID & ", " & Me.hfTRXKKCORPBidangUsaha.ClientID & ");")

        'Pekerjaan
        Me.imgTRXKMINDVPekerjaan.Attributes.Add("onClick", "javascript:popWinPekerjaan(" & Me.txtTRXKMINDVPekerjaan.ClientID & ", " & Me.hfTRXKMINDVPekerjaan.ClientID & ");")
        Me.imgTRXKKINDVPekerjaan.Attributes.Add("onClick", "javascript:popWinPekerjaan(" & Me.txtTRXKKINDVPekerjaan.ClientID & ", " & Me.hfTRXKKINDVPekerjaan.ClientID & ");")
        Me.imgTerlaporPekerjaan.Attributes.Add("onClick", "javascript:popWinPekerjaan(" & Me.txtTerlaporPekerjaan.ClientID & ", " & Me.hfTerlaporPekerjaan.ClientID & ");")
        '

    End Sub

    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu1.MenuItemClick
        MultiView1.ActiveViewIndex = CInt(Menu1.SelectedValue)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub rblTRXKMTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTRXKMTipePelapor.SelectedIndexChanged
        If rblTRXKMTipePelapor.SelectedValue = "Perorangan" Then
            tblTRXKMTipePelapor.Visible = True
            tblTRXKMTipePelaporKorporasi.Visible = False
        Else
            tblTRXKMTipePelapor.Visible = False
            tblTRXKMTipePelaporKorporasi.Visible = True
        End If
    End Sub

    Protected Sub rblTRXKKTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTRXKKTipePelapor.SelectedIndexChanged
        If rblTRXKKTipePelapor.SelectedValue = "Perorangan" Then
            tblTRXKKTipePelaporPerorangan.Visible = True
            tblTRXKKTipePelaporKorporasi.Visible = False
        Else
            tblTRXKKTipePelaporPerorangan.Visible = False
            tblTRXKKTipePelaporKorporasi.Visible = True
        End If
    End Sub

    Function JumlahKeseluruhanRp(ByVal objKas As List(Of LTKTDetailCashInEdit)) As Integer
        Dim total As Integer = 0
        For Each obj As LTKTDetailCashInEdit In objKas
            total = total + CInt(obj.JumlahRp)
        Next
        Return total
    End Function


    Function isvalidAddKassMasuk() As Boolean
        Try
            If txtTRXKMDetilKasMasuk.Text <> vbNullString And txtTRXKMDetilKasMasuk.Text <> "" Then
                If IsNumeric(txtTRXKMDetilKasMasuk.Text) = False Then
                    Throw New Exception("Kas Masuk must be filled by number")
                    Return False
                End If
            End If
            If txtTRXKMDetailKursTrx.Text = vbNullString OrElse txtTRXKMDetailKursTrx.Text = "" Then
                Throw New Exception("Kurs Transaksi (Kas Masuk) must be filled")
                Return False
            End If
            If IsNumeric(txtTRXKMDetailKursTrx.Text) = False Then
                Throw New Exception("Kurs Transaksi (Kas Masuk) must be filled by number")
                Return False
            End If
            If txtTRXKMDetilJumlah.Text = vbNullString OrElse txtTRXKMDetilJumlah.Text = "" Then
                Throw New Exception("Jumlah (Kas Masuk) must be filled")
                Return False
            End If
            If IsNumeric(txtTRXKMDetilJumlah.Text) = False Then
                Throw New Exception("Jumlah (Kas Masuk) must be filled by number")
                Return False
            End If
            Return True

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Function


    Protected Sub txtTRXKMDetilKasMasuk_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTRXKMDetilKasMasuk.TextChanged
        Try
            If IsNumeric(txtTRXKMDetilKasMasuk.Text) = False Then
                lblTRXKMDetilValutaAsingJumlahRp.Text = JumlahKeseluruhanRp(SetnGetgrvTRXKMDetilValutaAsing).ToString
                Throw New Exception("Kas Masuk must be filled by number")
            End If

            lblTRXKMDetilValutaAsingJumlahRp.Text = (CInt(txtTRXKMDetilKasMasuk.Text) + JumlahKeseluruhanRp(SetnGetgrvTRXKMDetilValutaAsing)).ToString

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub

    Protected Sub imgTRXKMDetilAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMDetilAdd.Click
        If isvalidAddKassMasuk() Then

            Dim objTemp As List(Of LTKTDetailCashInEdit)

            objTemp = SetnGetgrvTRXKMDetilValutaAsing

            Dim obj As New LTKTDetailCashInEdit
            obj.MataUang = txtTRXKMDetilMataUang.Text
            obj.KursTransaksi = txtTRXKMDetailKursTrx.Text
            obj.Jumlah = txtTRXKMDetilJumlah.Text
            obj.JumlahRp = (CInt(obj.KursTransaksi) * CInt(obj.Jumlah)).ToString

            objTemp.Add(obj)

            SetnGetgrvTRXKMDetilValutaAsing = objTemp

            grvTRXKMDetilValutaAsing.DataSource = objTemp
            grvTRXKMDetilValutaAsing.DataBind()

            If txtTRXKMDetilKasMasuk.Text = vbNullString Then
                lblTRXKMDetilValutaAsingJumlahRp.Text = JumlahKeseluruhanRp(SetnGetgrvTRXKMDetilValutaAsing).ToString
            Else
                lblTRXKMDetilValutaAsingJumlahRp.Text = (CInt(txtTRXKMDetilKasMasuk.Text) + JumlahKeseluruhanRp(SetnGetgrvTRXKMDetilValutaAsing)).ToString
            End If

            txtTRXKMDetailKursTrx.Text = vbNullString
            txtTRXKMDetilJumlah.Text = vbNullString


        End If
    End Sub


    Function isvalidAddKassKeluar() As Boolean
        Try
            If txtTRXKKDetailKasKeluar.Text <> vbNullString And txtTRXKKDetailKasKeluar.Text <> "" Then
                If IsNumeric(txtTRXKKDetailKasKeluar.Text) = False Then
                    Throw New Exception("Kas Masuk must be filled by number")
                    Return False
                End If
            End If
            If txtTRXKKDetilKursTrx.Text = vbNullString OrElse txtTRXKKDetilKursTrx.Text = "" Then
                Throw New Exception("Kurs Transaksi (Kas Keluar) must be filled")
                Return False
            End If
            If IsNumeric(txtTRXKKDetilKursTrx.Text) = False Then
                Throw New Exception("Kurs Transaksi (Kas Keluar) must be filled by number")
                Return False
            End If
            If txtTRXKKDetilJumlah.Text = vbNullString OrElse txtTRXKKDetilJumlah.Text = "" Then
                Throw New Exception("Jumlah (Kas Keluar) must be filled")
                Return False
            End If
            If IsNumeric(txtTRXKKDetilJumlah.Text) = False Then
                Throw New Exception("Jumlah (Kas Keluar) must be filled by number")
                Return False
            End If
            Return True

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Function

    Protected Sub txtTRXKKDetailKasKeluar_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTRXKKDetailKasKeluar.TextChanged
        Try
            If IsNumeric(txtTRXKKDetailKasKeluar.Text) = False Then
                lblDetilKasKeluarJumlahRp.Text = JumlahKeseluruhanRp(SetnGetgrvDetilKasKeluar).ToString
                Throw New Exception("Kas Keluar must be filled by number")
            End If

            lblDetilKasKeluarJumlahRp.Text = (CInt(txtTRXKKDetailKasKeluar.Text) + JumlahKeseluruhanRp(SetnGetgrvDetilKasKeluar)).ToString

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub



    Protected Sub imgTRXKKDetilAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKDetilAdd.Click
        If isvalidAddKassKeluar() Then

            Dim objTemp As List(Of LTKTDetailCashInEdit)

            objTemp = SetnGetgrvDetilKasKeluar

            Dim obj As New LTKTDetailCashInEdit
            obj.MataUang = txtTRXKKDetilMataUang.Text
            obj.KursTransaksi = txtTRXKKDetilKursTrx.Text
            obj.Jumlah = txtTRXKKDetilJumlah.Text
            obj.JumlahRp = (CInt(obj.KursTransaksi) * CInt(obj.Jumlah)).ToString

            objTemp.Add(obj)

            SetnGetgrvDetilKasKeluar = objTemp

            grvDetilKasKeluar.DataSource = objTemp
            grvDetilKasKeluar.DataBind()

            If txtTRXKKDetailKasKeluar.Text = vbNullString Then
                lblDetilKasKeluarJumlahRp.Text = JumlahKeseluruhanRp(SetnGetgrvDetilKasKeluar).ToString
            Else
                lblDetilKasKeluarJumlahRp.Text = (CInt(txtTRXKKDetailKasKeluar.Text) + JumlahKeseluruhanRp(SetnGetgrvDetilKasKeluar)).ToString
            End If
        End If

        txtTRXKKDetilKursTrx.Text = vbNullString
        txtTRXKKDetilJumlah.Text = vbNullString

    End Sub

    Protected Sub grvDetilKasKeluar_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvDetilKasKeluar.RowDataBound
        If e.Row.RowType <> ListItemType.Footer And e.Row.RowType <> ListItemType.Header Then
            e.Row.Cells(0).Text = e.Row.RowIndex + 1
        End If
    End Sub

    Protected Sub BtnDeletegrvDetilKasKeluar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objgridviewrow As GridViewRow = CType(CType(sender, LinkButton).NamingContainer, GridViewRow)
        Dim objTemp As List(Of LTKTDetailCashInEdit) = SetnGetgrvDetilKasKeluar
        objTemp.RemoveAt(objgridviewrow.RowIndex)
        grvDetilKasKeluar.DataSource = objTemp
        grvDetilKasKeluar.DataBind()
    End Sub

    Protected Sub BtngrvDetailValutaAsingDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objgridviewrow As GridViewRow = CType(CType(sender, LinkButton).NamingContainer, GridViewRow)
        Dim objTemp As List(Of LTKTDetailCashInEdit) = SetnGetgrvTRXKMDetilValutaAsing
        objTemp.RemoveAt(objgridviewrow.RowIndex)
        grvTRXKMDetilValutaAsing.DataSource = objTemp
        grvTRXKMDetilValutaAsing.DataBind()
    End Sub

    Protected Sub grvTRXKMDetilValutaAsing_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvTRXKMDetilValutaAsing.RowDataBound
        If e.Row.RowType <> ListItemType.Footer And e.Row.RowType <> ListItemType.Header Then
            e.Row.Cells(0).Text = e.Row.RowIndex + 1
        End If
    End Sub

    Function isvalidData() As Boolean
        Try
            'If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtTglLaporan.Text) = False Then
            ' Throw New Exception("Format Tanggal Pelaporan not valid")
            'End If

            If cboTipeLaporan.SelectedValue = 1 Then
                If txtNoLTKTKoreksi.Text = vbNullString OrElse txtNoLTKTKoreksi.Text = "" Then
                    Throw New Exception("No. LTKT yang dikoreksi must be filled")

                End If
            End If
            If cboTerlaporKepemilikan.SelectedIndex = 0 Then
                Throw New Exception("Kepemilikan must be choosen")

            End If

            If rblTerlaporTipePelapor.SelectedIndex = 0 Then
                If txtTerlaporNamaLengkap.Text = vbNullString OrElse txtTerlaporNamaLengkap.Text = "" Then
                    Throw New Exception("Nama Lengkap must be filled")

                End If
                If txtTerlaporTglLahir.Text = vbNullString OrElse txtTerlaporTglLahir.Text = "" Then
                    Throw New Exception("Tanggal Lahir must be filled")

                End If
                'If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtTerlaporTglLahir.Text) = False Then
                ' Throw New Exception("Format Tanggal Lahir not valid")
                'End If
                If rblTerlaporKewarganegaraan.SelectedIndex = -1 Then
                    Throw New Exception("Kewarganegaraan must be choosen")

                End If
                If rblTerlaporKewarganegaraan.SelectedIndex = 1 Then
                    If cboTerlaporNegara.SelectedIndex = 0 Then
                        Throw New Exception("Negara must be choosen")

                    End If
                    If txtTerlaporNAKodePos.Text = vbNullString OrElse txtTerlaporNAKodePos.Text = "" Then
                        Throw New Exception("Kode Pos must be filled (Alamat Sesuai Negara Asal)")

                    End If
                End If
                If txtTerlaporDOMKotaKab.Text = vbNullString OrElse txtTerlaporDOMKotaKab.Text = "" Then
                    Throw New Exception("Kota / Kabupaten Lahir must be filled (Alamat Domisili)")

                End If
                If txtTerlaporIDKotaKab.Text = vbNullString OrElse txtTerlaporIDKotaKab.Text = "" Then
                    Throw New Exception("Kota Kabupaten must be filled (Alamat Sesuai Bukti Identitas)")

                End If
                If cboTerlaporJenisDocID.SelectedIndex = 0 Then
                    Throw New Exception("Jenis Dokumen Identitas must be choosen")

                End If
                If txtTerlaporPekerjaan.Text = vbNullString OrElse txtTerlaporPekerjaan.Text = "" Then
                    Throw New Exception("Pekerjaan must be filled")

                End If
            End If

            If rblTerlaporTipePelapor.SelectedIndex = 1 Then
                If cboTerlaporCORPBentukBadanUsaha.SelectedIndex = 0 Then
                    Throw New Exception("Bentuk Badan Usaha must be filled")

                End If
                If txtTerlaporCORPNama.Text = vbNullString OrElse txtTerlaporCORPNama.Text = "" Then
                    Throw New Exception("Nama Korporasi must be filled")

                End If
                If txtTerlaporCORPBidangUsaha.Text = vbNullString OrElse txtTerlaporCORPBidangUsaha.Text = "" Then
                    Throw New Exception("Bidang Usaha Korporasi must be filled")

                End If
                If rblTerlaporCORPTipeAlamat.SelectedIndex = -1 Then
                    Throw New Exception("Alamat Korporasi must be choosen")

                End If
                If rblTerlaporCORPTipeAlamat.SelectedIndex = 0 Then
                    If txtTerlaporCORPDLKotaKab.Text = vbNullString OrElse txtTerlaporCORPDLKotaKab.Text = "" Then
                        Throw New Exception("Kota / Kabupaten (Alamat Lengkap Korporasi) must be filled")

                    End If
                End If
                If rblTerlaporCORPTipeAlamat.SelectedIndex = 1 Then
                    If txtTerlaporCORPLNKodePos.Text = vbNullString OrElse txtTerlaporCORPLNKodePos.Text = "" Then
                        Throw New Exception("Kode Pos (Alamat Korporasi Luar Negeri) must be filled")

                    End If
                End If
            End If

            Dim objResume As List(Of ResumeKasMasukKeluar) = SetnGetResumeKasMasukKasKeluar
            If objResume.Count < 1 Then
                Throw New Exception("Transaction must be added at least once")

            End If

            Return True

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
            Return False
        End Try

    End Function

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If isvalidData() Then
                Dim defaultDate As New Date(1900, 1, 1)
                Dim objResume As List(Of ResumeKasMasukKeluar) = SetnGetResumeKasMasukKasKeluar
                'Buat LTKT Approval
                Using objLTKTApproval As New LTKT_Approval
                    objLTKTApproval.RequestedBy = Sahassa.AML.Commonly.SessionPkUserId
                    objLTKTApproval.RequestedDate = Now()
                    objLTKTApproval.FK_MsMode_Id = 2

                    DataRepository.LTKT_ApprovalProvider.Save(objLTKTApproval)
                    'Ini dpake terus
                    'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
                    AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Request for approval add new data LTKT", objLTKTApproval, Nothing)


                    'LTKT Detil Approval
                    Using objLTKTDetilApproval As New LTKT_ApprovalDetail
                        objLTKTDetilApproval.FK_LTKT_Approval_Id = objLTKTApproval.PK_LTKT_Approval_Id
                        objLTKTDetilApproval.PK_LTKT_Id = getLTKTPK

                        objLTKTDetilApproval.NamaPJKPelapor = txtUmumPJKPelapor.Text
                        objLTKTDetilApproval.TanggalLaporan = txtTglLaporan.Text
                        objLTKTDetilApproval.NamaPejabatPJKPelapor = txtPejabatPelapor.Text
                        objLTKTDetilApproval.JenisLaporan = cboTipeLaporan.SelectedIndex.ToString
                        objLTKTDetilApproval.NoLTKTKoreksi = txtNoLTKTKoreksi.Text
                        objLTKTDetilApproval.InformasiLainnya = txtLTKTInformasiLainnya.Text
                        objLTKTDetilApproval.FK_MsKepemilikan_ID = CInt(cboTerlaporKepemilikan.SelectedValue)
                        objLTKTDetilApproval.NoRekening = txtTerlaporNoRekening.Text
                        If txtCIFNo.Text <> "" Then
                            objLTKTDetilApproval.CIFNo = txtCIFNo.Text
                        End If
                        objLTKTDetilApproval.TipeTerlapor = rblTerlaporTipePelapor.SelectedIndex
                        objLTKTDetilApproval.INDV_Gelar = txtTerlaporGelar.Text
                        objLTKTDetilApproval.INDV_NamaLengkap = txtTerlaporNamaLengkap.Text
                        objLTKTDetilApproval.INDV_TempatLahir = txtTerlaporTempatLahir.Text
                        If txtTerlaporTglLahir.Text <> "" Then
                            objLTKTDetilApproval.INDV_TanggalLahir = txtTerlaporTglLahir.Text
                        Else
                            objLTKTDetilApproval.INDV_TanggalLahir = defaultDate
                        End If
                        objLTKTDetilApproval.INDV_Kewarganegaraan = rblTerlaporKewarganegaraan.SelectedIndex.ToString
                        If cboTerlaporNegara.SelectedIndex <> 0 Then
                            objLTKTDetilApproval.INDV_FK_MsNegara_Id = CInt(cboTerlaporNegara.SelectedValue)
                        End If
                        objLTKTDetilApproval.INDV_DOM_NamaJalan = txtTerlaporDOMNamaJalan.Text
                        objLTKTDetilApproval.INDV_DOM_RTRW = txtTerlaporDOMRTRW.Text
                        If hfTerlaporDOMKelurahan.Value <> "" Then
                            objLTKTDetilApproval.INDV_DOM_FK_MsKelurahan_Id = CInt(hfTerlaporDOMKelurahan.Value)
                        End If
                        If hfTerlaporDOMKecamatan.Value <> "" Then
                            objLTKTDetilApproval.INDV_DOM_FK_MsKecamatan_Id = CInt(hfTerlaporDOMKecamatan.Value)
                        End If
                        If hfTerlaporDOMKotaKab.Value <> "" Then
                            objLTKTDetilApproval.INDV_DOM_FK_MsKotaKab_Id = CInt(hfTerlaporDOMKotaKab.Value)
                        End If
                        objLTKTDetilApproval.INDV_DOM_KodePos = txtTerlaporDOMKodePos.Text
                        If hfTerlaporDOMProvinsi.Value <> "" Then
                            objLTKTDetilApproval.INDV_DOM_FK_MsProvince_Id = CInt(hfTerlaporDOMProvinsi.Value)
                        End If
                        objLTKTDetilApproval.INDV_ID_NamaJalan = txtTerlaporIDNamaJalan.Text
                        objLTKTDetilApproval.INDV_ID_RTRW = txtTerlaporIDRTRW.Text
                        If hfTerlaporIDKelurahan.Value <> "" Then
                            objLTKTDetilApproval.INDV_ID_FK_MsKelurahan_Id = CInt(hfTerlaporIDKelurahan.Value)
                        End If
                        If hfTerlaporIDKecamatan.Value <> "" Then
                            objLTKTDetilApproval.INDV_ID_FK_MsKecamatan_Id = CInt(hfTerlaporIDKecamatan.Value)
                        End If
                        If hfTerlaporIDKotaKab.Value <> "" Then
                            objLTKTDetilApproval.INDV_ID_FK_MsKotaKab_Id = CInt(hfTerlaporIDKotaKab.Value)
                        End If
                        objLTKTDetilApproval.INDV_ID_KodePos = txtTerlaporIDKodePos.Text
                        If hfTerlaporIDProvinsi.Value <> "" Then
                            objLTKTDetilApproval.INDV_ID_FK_MsProvince_Id = CInt(hfTerlaporIDProvinsi.Value)
                        End If
                        objLTKTDetilApproval.INDV_NA_NamaJalan = txtTerlaporNANamaJalan.Text
                        If hfTerlaporIDNegara.Value <> "" Then
                            Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegaraNCBS.ToString & " like '%" & hfTerlaporIDNegara.Value.ToString & "%' ", "", 0, Integer.MaxValue, 0)
                            If objMapNegara.Count > 0 Then
                                objLTKTDetilApproval.INDV_NA_FK_MsNegara_Id = objMapNegara(0).IDNegara
                            End If
                        End If
                        If hfTerlaporNAProvinsi.Value <> "" Then
                            objLTKTDetilApproval.INDV_NA_FK_MsProvince_Id = CInt(hfTerlaporNAProvinsi.Value)
                        End If
                        If hfTerlaporIDKota.Value <> "" Then
                            objLTKTDetilApproval.INDV_NA_FK_MsKotaKab_Id = CInt(hfTerlaporIDKota.Value)
                        End If
                        objLTKTDetilApproval.INDV_NA_KodePos = txtTerlaporNAKodePos.Text
                        If cboTerlaporJenisDocID.SelectedIndex <> 0 Then
                            objLTKTDetilApproval.INDV_FK_MsIDType_Id = CInt(cboTerlaporJenisDocID.SelectedValue)
                        End If
                        objLTKTDetilApproval.INDV_NomorId = txtTerlaporNomorID.Text
                        objLTKTDetilApproval.INDV_NPWP = txtTerlaporNPWP.Text
                        If hfTerlaporPekerjaan.Value <> "" Then
                            objLTKTDetilApproval.INDV_FK_MsPekerjaan_Id = CInt(hfTerlaporPekerjaan.Value)
                        End If
                        objLTKTDetilApproval.INDV_Jabatan = txtTerlaporJabatan.Text
                        objLTKTDetilApproval.INDV_PenghasilanRataRata = txtTerlaporPenghasilanRataRata.Text
                        objLTKTDetilApproval.INDV_TempatBekerja = txtTerlaporTempatKerja.Text

                        'corp
                        If cboTerlaporCORPBentukBadanUsaha.SelectedIndex <> 0 Then
                            objLTKTDetilApproval.CORP_FK_MsBentukBadanUsaha_Id = CInt(cboTerlaporCORPBentukBadanUsaha.SelectedValue)
                        End If
                        objLTKTDetilApproval.CORP_Nama = txtTerlaporCORPNama.Text
                        If hfTerlaporCORPBidangUsaha.Value <> "" Then
                            objLTKTDetilApproval.CORP_FK_MsBidangUsaha_Id = CInt(hfTerlaporCORPBidangUsaha.Value)
                        End If
                        If rblTerlaporCORPTipeAlamat.SelectedIndex <> -1 Then
                            objLTKTDetilApproval.CORP_TipeAlamat = rblTerlaporCORPTipeAlamat.SelectedIndex
                        End If
                        objLTKTDetilApproval.CORP_NamaJalan = txtTerlaporCORPDLNamaJalan.Text
                        objLTKTDetilApproval.CORP_RTRW = txtTerlaporCORPDLRTRW.Text
                        If hfTerlaporCORPDLKelurahan.Value <> "" Then
                            objLTKTDetilApproval.CORP_FK_MsKelurahan_Id = CInt(hfTerlaporCORPDLKelurahan.Value)
                        End If
                        If hfTerlaporCORPDLKecamatan.Value <> "" Then
                            objLTKTDetilApproval.CORP_FK_MsKecamatan_Id = CInt(hfTerlaporCORPDLKecamatan.Value)
                        End If
                        If hfTerlaporCORPDLKotaKab.Value <> "" Then
                            objLTKTDetilApproval.CORP_FK_MsKotaKab_Id = CInt(hfTerlaporCORPDLKotaKab.Value)
                        End If
                        objLTKTDetilApproval.CORP_KodePos = txtTerlaporCORPDLKodePos.Text
                        If hfTerlaporCORPDLProvinsi.Value <> "" Then
                            objLTKTDetilApproval.CORP_FK_MsProvince_Id = CInt(hfTerlaporCORPDLProvinsi.Value)
                        End If

                        If hfTerlaporCORPDLNegara.Value <> "" Then
                            Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegaraNCBS.ToString & " like '%" & hfTerlaporCORPDLNegara.Value.ToString & "%' ", "", 0, Integer.MaxValue, 0)
                            If objMapNegara.Count > 0 Then
                                objLTKTDetilApproval.CORP_FK_MsNegara_Id = objMapNegara(0).IDNegara
                            End If
                        End If
                        objLTKTDetilApproval.CORP_LN_NamaJalan = txtTerlaporCORPLNNamaJalan.Text
                        If hfTerlaporCORPLNNegara.Value <> "" Then
                            Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegaraNCBS.ToString & " like '%" & hfTerlaporCORPLNNegara.Value.ToString & "%' ", "", 0, Integer.MaxValue, 0)
                            If objMapNegara.Count > 0 Then
                                objLTKTDetilApproval.CORP_LN_FK_MsNegara_Id = objMapNegara(0).IDNegara
                            End If
                        End If
                        If hfTerlaporCORPLNProvinsi.Value <> "" Then
                            objLTKTDetilApproval.CORP_LN_FK_MsProvince_Id = CInt(hfTerlaporCORPLNProvinsi.Value)
                        End If
                        If hfTerlaporCORPLNKota.Value <> "" Then
                            objLTKTDetilApproval.CORP_LN_MsKotaKab_Id = CInt(hfTerlaporCORPLNKota.Value)
                        End If
                        objLTKTDetilApproval.CORP_LN_KodePos = txtTerlaporCORPLNKodePos.Text
                        objLTKTDetilApproval.CORP_NPWP = txtTerlaporCORPNPWP.Text

                        objLTKTDetilApproval.CreatedBy = Sahassa.AML.Commonly.SessionUserId
                        objLTKTDetilApproval.CreatedDate = Now()

                        DataRepository.LTKT_ApprovalDetailProvider.Save(objLTKTDetilApproval)
                        AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Request for approval add new data LTKT Detail", objLTKTDetilApproval, Nothing)
                        For Each objSingleResume As ResumeKasMasukKeluar In objResume


                            'transaction approval detail
                            If objSingleResume.Kas = "Kas Masuk" Then
                                Using objLTKTTransactionCashInApproval As New LTKTTransactionCashIn_ApprovalDetail
                                    objLTKTTransactionCashInApproval.FK_LTKT_ApprovalDetail_Id = objLTKTDetilApproval.PK_LTKT_ApprovalDetail_Id
                                    objLTKTTransactionCashInApproval.PK_LTKTTransactionCashIn_Id = objSingleResume.TransactionCashIn.PK_LTKTTransactionCashIn_Id
                                    objLTKTTransactionCashInApproval.FK_LTKT_Id = getLTKTPK

                                    objLTKTTransactionCashInApproval.TanggalTransaksi = objSingleResume.TransactionCashIn.TanggalTransaksi
                                    objLTKTTransactionCashInApproval.NamaKantorPJK = objSingleResume.TransactionCashIn.NamaKantorPJK
                                    objLTKTTransactionCashInApproval.FK_MsKotaKab_Id = objSingleResume.TransactionCashIn.FK_MsKotaKab_Id
                                    objLTKTTransactionCashInApproval.FK_MsProvince_Id = objSingleResume.TransactionCashIn.FK_MsProvince_Id
                                    objLTKTTransactionCashInApproval.NomorRekening = objSingleResume.TransactionCashIn.NomorRekening
                                    objLTKTTransactionCashInApproval.INDV_Gelar = objSingleResume.TransactionCashIn.INDV_Gelar
                                    objLTKTTransactionCashInApproval.INDV_NamaLengkap = objSingleResume.TransactionCashIn.INDV_NamaLengkap
                                    objLTKTTransactionCashInApproval.INDV_TempatLahir = objSingleResume.TransactionCashIn.INDV_TempatLahir
                                    objLTKTTransactionCashInApproval.INDV_TanggalLahir = objSingleResume.TransactionCashIn.INDV_TanggalLahir
                                    objLTKTTransactionCashInApproval.INDV_Kewarganegaraan = objSingleResume.TransactionCashIn.INDV_Kewarganegaraan
                                    objLTKTTransactionCashInApproval.INDV_FK_MsNegara_Id = objSingleResume.TransactionCashIn.INDV_FK_MsNegara_Id
                                    objLTKTTransactionCashInApproval.INDV_DOM_NamaJalan = objSingleResume.TransactionCashIn.INDV_DOM_NamaJalan
                                    objLTKTTransactionCashInApproval.INDV_DOM_RTRW = objSingleResume.TransactionCashIn.INDV_DOM_RTRW
                                    objLTKTTransactionCashInApproval.INDV_DOM_FK_MsKelurahan_Id = objSingleResume.TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id
                                    objLTKTTransactionCashInApproval.INDV_DOM_FK_MsKecamatan_Id = objSingleResume.TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id
                                    objLTKTTransactionCashInApproval.INDV_DOM_FK_MsKotaKab_Id = objSingleResume.TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id
                                    objLTKTTransactionCashInApproval.INDV_DOM_KodePos = objSingleResume.TransactionCashIn.INDV_DOM_KodePos
                                    objLTKTTransactionCashInApproval.INDV_DOM_FK_MsProvince_Id = objSingleResume.TransactionCashIn.INDV_DOM_FK_MsProvince_Id
                                    objLTKTTransactionCashInApproval.INDV_ID_NamaJalan = objSingleResume.TransactionCashIn.INDV_ID_NamaJalan
                                    objLTKTTransactionCashInApproval.INDV_ID_RTRW = objSingleResume.TransactionCashIn.INDV_ID_RTRW
                                    objLTKTTransactionCashInApproval.INDV_ID_FK_MsKelurahan_Id = objSingleResume.TransactionCashIn.INDV_ID_FK_MsKelurahan_Id
                                    objLTKTTransactionCashInApproval.INDV_ID_FK_MsKecamatan_Id = objSingleResume.TransactionCashIn.INDV_ID_FK_MsKecamatan_Id
                                    objLTKTTransactionCashInApproval.INDV_ID_FK_MsKotaKab_Id = objSingleResume.TransactionCashIn.INDV_ID_FK_MsKotaKab_Id
                                    objLTKTTransactionCashInApproval.INDV_ID_KodePos = objSingleResume.TransactionCashIn.INDV_ID_KodePos
                                    objLTKTTransactionCashInApproval.INDV_ID_FK_MsProvince_Id = objSingleResume.TransactionCashIn.INDV_ID_FK_MsProvince_Id
                                    objLTKTTransactionCashInApproval.INDV_NA_NamaJalan = objSingleResume.TransactionCashIn.INDV_NA_NamaJalan
                                    objLTKTTransactionCashInApproval.INDV_NA_FK_MsNegara_Id = objSingleResume.TransactionCashIn.INDV_NA_FK_MsNegara_Id
                                    objLTKTTransactionCashInApproval.INDV_NA_FK_MsProvince_Id = objSingleResume.TransactionCashIn.INDV_NA_FK_MsProvince_Id
                                    objLTKTTransactionCashInApproval.INDV_NA_FK_MsKotaKab_Id = objSingleResume.TransactionCashIn.INDV_NA_FK_MsKotaKab_Id
                                    objLTKTTransactionCashInApproval.INDV_NA_KodePos = objSingleResume.TransactionCashIn.INDV_NA_KodePos
                                    objLTKTTransactionCashInApproval.INDV_FK_MsIDType_Id = objSingleResume.TransactionCashIn.INDV_FK_MsIDType_Id
                                    objLTKTTransactionCashInApproval.INDV_NomorId = objSingleResume.TransactionCashIn.INDV_NomorId
                                    objLTKTTransactionCashInApproval.INDV_NPWP = objSingleResume.TransactionCashIn.INDV_NPWP
                                    objLTKTTransactionCashInApproval.INDV_FK_MsPekerjaan_Id = objSingleResume.TransactionCashIn.INDV_FK_MsPekerjaan_Id
                                    objLTKTTransactionCashInApproval.INDV_Jabatan = objSingleResume.TransactionCashIn.INDV_Jabatan
                                    objLTKTTransactionCashInApproval.INDV_PenghasilanRataRata = objSingleResume.TransactionCashIn.INDV_PenghasilanRataRata
                                    objLTKTTransactionCashInApproval.INDV_TempatBekerja = objSingleResume.TransactionCashIn.INDV_TempatBekerja
                                    objLTKTTransactionCashInApproval.INDV_TujuanTransaksi = objSingleResume.TransactionCashIn.INDV_TujuanTransaksi
                                    objLTKTTransactionCashInApproval.INDV_SumberDana = objSingleResume.TransactionCashIn.INDV_SumberDana
                                    objLTKTTransactionCashInApproval.INDV_NamaBankLain = objSingleResume.TransactionCashIn.INDV_NamaBankLain
                                    objLTKTTransactionCashInApproval.INDV_NomorRekeningTujuan = objSingleResume.TransactionCashIn.INDV_NomorRekeningTujuan

                                    'corp
                                    objLTKTTransactionCashInApproval.CORP_FK_MsBentukBadanUsaha_Id = objSingleResume.TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id
                                    objLTKTTransactionCashInApproval.CORP_Nama = objSingleResume.TransactionCashIn.CORP_Nama
                                    objLTKTTransactionCashInApproval.CORP_FK_MsBidangUsaha_Id = objSingleResume.TransactionCashIn.CORP_FK_MsBidangUsaha_Id
                                    objLTKTTransactionCashInApproval.CORP_TipeAlamat = objSingleResume.TransactionCashIn.CORP_TipeAlamat
                                    objLTKTTransactionCashInApproval.CORP_NamaJalan = objSingleResume.TransactionCashIn.CORP_NamaJalan
                                    objLTKTTransactionCashInApproval.CORP_RTRW = objSingleResume.TransactionCashIn.CORP_RTRW
                                    objLTKTTransactionCashInApproval.CORP_FK_MsKelurahan_Id = objSingleResume.TransactionCashIn.CORP_FK_MsKelurahan_Id
                                    objLTKTTransactionCashInApproval.CORP_FK_MsKecamatan_Id = objSingleResume.TransactionCashIn.CORP_FK_MsKecamatan_Id
                                    objLTKTTransactionCashInApproval.CORP_FK_MsKotaKab_Id = objSingleResume.TransactionCashIn.CORP_FK_MsKotaKab_Id
                                    objLTKTTransactionCashInApproval.CORP_KodePos = objSingleResume.TransactionCashIn.CORP_KodePos
                                    objLTKTTransactionCashInApproval.CORP_FK_MsProvince_Id = objSingleResume.TransactionCashIn.CORP_FK_MsProvince_Id
                                    objLTKTTransactionCashInApproval.CORP_FK_MsNegara_Id = objSingleResume.TransactionCashIn.CORP_FK_MsNegara_Id
                                    objLTKTTransactionCashInApproval.CORP_LN_NamaJalan = objSingleResume.TransactionCashIn.CORP_LN_NamaJalan
                                    objLTKTTransactionCashInApproval.CORP_LN_FK_MsNegara_Id = objSingleResume.TransactionCashIn.CORP_LN_FK_MsNegara_Id
                                    objLTKTTransactionCashInApproval.CORP_LN_FK_MsProvince_Id = objSingleResume.TransactionCashIn.CORP_LN_FK_MsProvince_Id
                                    objLTKTTransactionCashInApproval.CORP_LN_MsKotaKab_Id = objSingleResume.TransactionCashIn.CORP_LN_MsKotaKab_Id
                                    objLTKTTransactionCashInApproval.CORP_LN_KodePos = objSingleResume.TransactionCashIn.CORP_LN_KodePos
                                    objLTKTTransactionCashInApproval.CORP_NPWP = objSingleResume.TransactionCashIn.CORP_NPWP
                                    objLTKTTransactionCashInApproval.CORP_TujuanTransaksi = objSingleResume.TransactionCashIn.CORP_TujuanTransaksi
                                    objLTKTTransactionCashInApproval.CORP_SumberDana = objSingleResume.TransactionCashIn.CORP_SumberDana
                                    objLTKTTransactionCashInApproval.CORP_NamaBankLain = objSingleResume.TransactionCashIn.CORP_NamaBankLain
                                    objLTKTTransactionCashInApproval.CORP_NomorRekeningTujuan = objSingleResume.TransactionCashIn.CORP_NomorRekeningTujuan

                                    objLTKTTransactionCashInApproval.Total = objSingleResume.TransactionNominal
                                    objLTKTTransactionCashInApproval.CreatedBy = Sahassa.AML.Commonly.SessionUserId
                                    objLTKTTransactionCashInApproval.CreatedDate = Now()
                                    DataRepository.LTKTTransactionCashIn_ApprovalDetailProvider.Save(objLTKTTransactionCashInApproval)
                                    AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Request for approval add new data LTKT Transaction CashIn", objLTKTTransactionCashInApproval, Nothing)

                                    'Insert Detail Transaction Approval
                                    For Each objSingleDetail As LTKTDetailCashInTransaction In objSingleResume.DetailTransactionCashIn
                                        Using objLTKTDetailTransactionCashInApproval As New LTKTDetailCashInTransaction_ApprovalDetail
                                            objLTKTDetailTransactionCashInApproval.FK_LTKTTransactionCashIn_ApprovalDetail_Id = objLTKTTransactionCashInApproval.PK_LTKTTransactionCashIn_ApprovalDetail_Id
                                            objLTKTDetailTransactionCashInApproval.PK_LTKTDetailCashInTransaction_Id = objSingleDetail.PK_LTKTDetailCashInTransaction_Id

                                            objLTKTDetailTransactionCashInApproval.KasMasuk = objSingleDetail.KasMasuk
                                            objLTKTDetailTransactionCashInApproval.Asing_FK_MsCurrency_Id = objSingleDetail.Asing_FK_MsCurrency_Id
                                            objLTKTDetailTransactionCashInApproval.Asing_KursTransaksi = objSingleDetail.Asing_KursTransaksi
                                            objLTKTDetailTransactionCashInApproval.Asing_TotalKasMasukDalamRupiah = objSingleDetail.Asing_TotalKasMasukDalamRupiah
                                            objLTKTDetailTransactionCashInApproval.TotalKasMasuk = objSingleDetail.TotalKasMasuk

                                            DataRepository.LTKTDetailCashInTransaction_ApprovalDetailProvider.Save(objLTKTDetailTransactionCashInApproval)
                                            AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Request for approval add new data LTKT Transaction CashIn Detail ", objLTKTDetailTransactionCashInApproval, Nothing)
                                        End Using
                                    Next


                                End Using
                            Else
                                Using objLTKTTransactionCashOutApproval As New LTKTTransactionCashOut_ApprovalDetail
                                    objLTKTTransactionCashOutApproval.FK_LTKT_ApprovalDetail_Id = objLTKTDetilApproval.PK_LTKT_ApprovalDetail_Id
                                    objLTKTTransactionCashOutApproval.PK_LTKTTransactionCashOut_Id = objSingleResume.TransactionCashOut.PK_LTKTTransactionCashOut_Id
                                    objLTKTTransactionCashOutApproval.FK_LTKT_Id = getLTKTPK

                                    objLTKTTransactionCashOutApproval.TanggalTransaksi = objSingleResume.TransactionCashOut.TanggalTransaksi
                                    objLTKTTransactionCashOutApproval.NamaKantorPJK = objSingleResume.TransactionCashOut.NamaKantorPJK
                                    objLTKTTransactionCashOutApproval.FK_MsKotaKab_Id = objSingleResume.TransactionCashOut.FK_MsKotaKab_Id
                                    objLTKTTransactionCashOutApproval.FK_MsProvince_Id = objSingleResume.TransactionCashOut.FK_MsProvince_Id
                                    objLTKTTransactionCashOutApproval.NomorRekening = objSingleResume.TransactionCashOut.NomorRekening
                                    objLTKTTransactionCashOutApproval.INDV_Gelar = objSingleResume.TransactionCashOut.INDV_Gelar
                                    objLTKTTransactionCashOutApproval.INDV_NamaLengkap = objSingleResume.TransactionCashOut.INDV_NamaLengkap
                                    objLTKTTransactionCashOutApproval.INDV_TempatLahir = objSingleResume.TransactionCashOut.INDV_TempatLahir
                                    objLTKTTransactionCashOutApproval.INDV_TanggalLahir = objSingleResume.TransactionCashOut.INDV_TanggalLahir
                                    objLTKTTransactionCashOutApproval.INDV_Kewarganegaraan = objSingleResume.TransactionCashOut.INDV_Kewarganegaraan
                                    objLTKTTransactionCashOutApproval.INDV_FK_MsNegara_Id = objSingleResume.TransactionCashOut.INDV_FK_MsNegara_Id
                                    objLTKTTransactionCashOutApproval.INDV_DOM_NamaJalan = objSingleResume.TransactionCashOut.INDV_DOM_NamaJalan
                                    objLTKTTransactionCashOutApproval.INDV_DOM_RTRW = objSingleResume.TransactionCashOut.INDV_DOM_RTRW
                                    objLTKTTransactionCashOutApproval.INDV_DOM_FK_MsKelurahan_Id = objSingleResume.TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id
                                    objLTKTTransactionCashOutApproval.INDV_DOM_FK_MsKecamatan_Id = objSingleResume.TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id
                                    objLTKTTransactionCashOutApproval.INDV_DOM_FK_MsKotaKab_Id = objSingleResume.TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id
                                    objLTKTTransactionCashOutApproval.INDV_DOM_KodePos = objSingleResume.TransactionCashOut.INDV_DOM_KodePos
                                    objLTKTTransactionCashOutApproval.INDV_DOM_FK_MsProvince_Id = objSingleResume.TransactionCashOut.INDV_DOM_FK_MsProvince_Id
                                    objLTKTTransactionCashOutApproval.INDV_ID_NamaJalan = objSingleResume.TransactionCashOut.INDV_ID_NamaJalan
                                    objLTKTTransactionCashOutApproval.INDV_ID_RTRW = objSingleResume.TransactionCashOut.INDV_ID_RTRW
                                    objLTKTTransactionCashOutApproval.INDV_ID_FK_MsKelurahan_Id = objSingleResume.TransactionCashOut.INDV_ID_FK_MsKelurahan_Id
                                    objLTKTTransactionCashOutApproval.INDV_ID_FK_MsKecamatan_Id = objSingleResume.TransactionCashOut.INDV_ID_FK_MsKecamatan_Id
                                    objLTKTTransactionCashOutApproval.INDV_ID_FK_MsKotaKab_Id = objSingleResume.TransactionCashOut.INDV_ID_FK_MsKotaKab_Id
                                    objLTKTTransactionCashOutApproval.INDV_ID_KodePos = objSingleResume.TransactionCashOut.INDV_ID_KodePos
                                    objLTKTTransactionCashOutApproval.INDV_ID_FK_MsProvince_Id = objSingleResume.TransactionCashOut.INDV_ID_FK_MsProvince_Id
                                    objLTKTTransactionCashOutApproval.INDV_NA_NamaJalan = objSingleResume.TransactionCashOut.INDV_NA_NamaJalan
                                    objLTKTTransactionCashOutApproval.INDV_NA_FK_MsNegara_Id = objSingleResume.TransactionCashOut.INDV_NA_FK_MsNegara_Id
                                    objLTKTTransactionCashOutApproval.INDV_NA_FK_MsProvince_Id = objSingleResume.TransactionCashOut.INDV_NA_FK_MsProvince_Id
                                    objLTKTTransactionCashOutApproval.INDV_NA_FK_MsKotaKab_Id = objSingleResume.TransactionCashOut.INDV_NA_FK_MsKotaKab_Id
                                    objLTKTTransactionCashOutApproval.INDV_NA_KodePos = objSingleResume.TransactionCashOut.INDV_NA_KodePos
                                    objLTKTTransactionCashOutApproval.INDV_FK_MsIDType_Id = objSingleResume.TransactionCashOut.INDV_FK_MsIDType_Id
                                    objLTKTTransactionCashOutApproval.INDV_NomorId = objSingleResume.TransactionCashOut.INDV_NomorId
                                    objLTKTTransactionCashOutApproval.INDV_NPWP = objSingleResume.TransactionCashOut.INDV_NPWP
                                    objLTKTTransactionCashOutApproval.INDV_FK_MsPekerjaan_Id = objSingleResume.TransactionCashOut.INDV_FK_MsPekerjaan_Id
                                    objLTKTTransactionCashOutApproval.INDV_Jabatan = objSingleResume.TransactionCashOut.INDV_Jabatan
                                    objLTKTTransactionCashOutApproval.INDV_PenghasilanRataRata = objSingleResume.TransactionCashOut.INDV_PenghasilanRataRata
                                    objLTKTTransactionCashOutApproval.INDV_TempatBekerja = objSingleResume.TransactionCashOut.INDV_TempatBekerja
                                    objLTKTTransactionCashOutApproval.INDV_TujuanTransaksi = objSingleResume.TransactionCashOut.INDV_TujuanTransaksi
                                    objLTKTTransactionCashOutApproval.INDV_SumberDana = objSingleResume.TransactionCashOut.INDV_SumberDana
                                    objLTKTTransactionCashOutApproval.INDV_NamaBankLain = objSingleResume.TransactionCashOut.INDV_NamaBankLain
                                    objLTKTTransactionCashOutApproval.INDV_NomorRekeningTujuan = objSingleResume.TransactionCashOut.INDV_NomorRekeningTujuan

                                    'corp
                                    objLTKTTransactionCashOutApproval.CORP_FK_MsBentukBadanUsaha_Id = objSingleResume.TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id
                                    objLTKTTransactionCashOutApproval.CORP_Nama = objSingleResume.TransactionCashOut.CORP_Nama
                                    objLTKTTransactionCashOutApproval.CORP_FK_MsBidangUsaha_Id = objSingleResume.TransactionCashOut.CORP_FK_MsBidangUsaha_Id
                                    objLTKTTransactionCashOutApproval.CORP_TipeAlamat = objSingleResume.TransactionCashOut.CORP_TipeAlamat
                                    objLTKTTransactionCashOutApproval.CORP_NamaJalan = objSingleResume.TransactionCashOut.CORP_NamaJalan
                                    objLTKTTransactionCashOutApproval.CORP_RTRW = objSingleResume.TransactionCashOut.CORP_RTRW
                                    objLTKTTransactionCashOutApproval.CORP_FK_MsKelurahan_Id = objSingleResume.TransactionCashOut.CORP_FK_MsKelurahan_Id
                                    objLTKTTransactionCashOutApproval.CORP_FK_MsKecamatan_Id = objSingleResume.TransactionCashOut.CORP_FK_MsKecamatan_Id
                                    objLTKTTransactionCashOutApproval.CORP_FK_MsKotaKab_Id = objSingleResume.TransactionCashOut.CORP_FK_MsKotaKab_Id
                                    objLTKTTransactionCashOutApproval.CORP_KodePos = objSingleResume.TransactionCashOut.CORP_KodePos
                                    objLTKTTransactionCashOutApproval.CORP_FK_MsProvince_Id = objSingleResume.TransactionCashOut.CORP_FK_MsProvince_Id
                                    objLTKTTransactionCashOutApproval.CORP_FK_MsNegara_Id = objSingleResume.TransactionCashOut.CORP_FK_MsNegara_Id
                                    objLTKTTransactionCashOutApproval.CORP_LN_NamaJalan = objSingleResume.TransactionCashOut.CORP_LN_NamaJalan
                                    objLTKTTransactionCashOutApproval.CORP_LN_FK_MsNegara_Id = objSingleResume.TransactionCashOut.CORP_LN_FK_MsNegara_Id
                                    objLTKTTransactionCashOutApproval.CORP_LN_FK_MsProvince_Id = objSingleResume.TransactionCashOut.CORP_LN_FK_MsProvince_Id
                                    objLTKTTransactionCashOutApproval.CORP_LN_MsKotaKab_Id = objSingleResume.TransactionCashOut.CORP_LN_MsKotaKab_Id
                                    objLTKTTransactionCashOutApproval.CORP_LN_KodePos = objSingleResume.TransactionCashOut.CORP_LN_KodePos
                                    objLTKTTransactionCashOutApproval.CORP_NPWP = objSingleResume.TransactionCashOut.CORP_NPWP
                                    objLTKTTransactionCashOutApproval.CORP_TujuanTransaksi = objSingleResume.TransactionCashOut.CORP_TujuanTransaksi
                                    objLTKTTransactionCashOutApproval.CORP_SumberDana = objSingleResume.TransactionCashOut.CORP_SumberDana
                                    objLTKTTransactionCashOutApproval.CORP_NamaBankLain = objSingleResume.TransactionCashOut.CORP_NamaBankLain
                                    objLTKTTransactionCashOutApproval.CORP_NomorRekeningTujuan = objSingleResume.TransactionCashOut.CORP_NomorRekeningTujuan


                                    objLTKTTransactionCashOutApproval.Total = objSingleResume.TransactionNominal
                                    objLTKTTransactionCashOutApproval.CreatedBy = Sahassa.AML.Commonly.SessionUserId
                                    objLTKTTransactionCashOutApproval.CreatedDate = Now()

                                    DataRepository.LTKTTransactionCashOut_ApprovalDetailProvider.Save(objLTKTTransactionCashOutApproval)
                                    AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Request for approval add new data LTKT Transaction CashOut ", objLTKTTransactionCashOutApproval, Nothing)
                                    'Insert Detail Transaction Approval
                                    For Each objSingleDetail As LTKTDetailCashOutTransaction In objSingleResume.DetailTranscationCashOut
                                        Using objLTKTDetailTransactionCashOutApproval As New LTKTDetailCashOutTransaction_ApprovalDetail
                                            objLTKTDetailTransactionCashOutApproval.FK_LTKTTransactionCashOut_ApprovalDetail_Id = objLTKTTransactionCashOutApproval.PK_LTKTTransactionCashOut_ApprovalDetail_Id
                                            objLTKTDetailTransactionCashOutApproval.PK_LTKTDetailCashOutTransaction_Id = objSingleDetail.PK_LTKTDetailCashOutTransaction_Id

                                            objLTKTDetailTransactionCashOutApproval.KasKeluar = objSingleDetail.KasKeluar
                                            objLTKTDetailTransactionCashOutApproval.Asing_FK_MsCurrency_Id = objSingleDetail.Asing_FK_MsCurrency_Id
                                            objLTKTDetailTransactionCashOutApproval.Asing_KursTransaksi = objSingleDetail.Asing_KursTransaksi
                                            objLTKTDetailTransactionCashOutApproval.Asing_TotalKasKeluarDalamRupiah = objSingleDetail.Asing_TotalKasKeluarDalamRupiah
                                            objLTKTDetailTransactionCashOutApproval.TotalKasKeluar = objSingleDetail.TotalKasKeluar

                                            DataRepository.LTKTDetailCashOutTransaction_ApprovalDetailProvider.Save(objLTKTDetailTransactionCashOutApproval)
                                            AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Request for approval add new data LTKT Transaction CashOut Detail", objLTKTDetailTransactionCashOutApproval, Nothing)
                                        End Using
                                    Next

                                End Using
                            End If
                        Next
                    End Using
                End Using
                mtvPage.ActiveViewIndex = 1
                lblMsg.Text = "LTKT data has edited and wait for approval"
            End If
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub

    Protected Sub chkCopyAlamatDOM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCopyAlamatDOM.CheckedChanged

        If chkCopyAlamatDOM.Checked = True Then
            txtTerlaporIDNamaJalan.Text = txtTerlaporDOMNamaJalan.Text
            txtTerlaporIDRTRW.Text = txtTerlaporDOMRTRW.Text
            txtTerlaporIDKelurahan.Text = txtTerlaporDOMKelurahan.Text
            txtTerlaporIDKecamatan.Text = txtTerlaporDOMKecamatan.Text
            txtTerlaporIDKotaKab.Text = txtTerlaporDOMKotaKab.Text
            txtTerlaporIDKodePos.Text = txtTerlaporDOMKodePos.Text
            txtTerlaporIDProvinsi.Text = txtTerlaporDOMProvinsi.Text


            hfTerlaporIDKelurahan.Value = hfTerlaporDOMKelurahan.Value
            hfTerlaporIDKecamatan.Value = hfTerlaporDOMKecamatan.Value
            hfTerlaporIDKotaKab.Value = hfTerlaporDOMKotaKab.Value
            hfTerlaporIDProvinsi.Value = hfTerlaporDOMProvinsi.Value
        Else
            txtTerlaporIDNamaJalan.Text = ""
            txtTerlaporIDRTRW.Text = ""
            txtTerlaporIDKelurahan.Text = ""
            txtTerlaporIDKecamatan.Text = ""
            txtTerlaporIDKotaKab.Text = ""
            txtTerlaporIDKodePos.Text = ""
            txtTerlaporIDProvinsi.Text = ""

            hfTerlaporIDKelurahan.Value = ""
            hfTerlaporIDKecamatan.Value = ""
            hfTerlaporIDKotaKab.Value = ""
            hfTerlaporIDProvinsi.Value = ""
        End If
    End Sub
    Protected Sub chkTRXKMINDVCopyDOM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTRXKMINDVCopyDOM.CheckedChanged

        If chkTRXKMINDVCopyDOM.Checked = True Then
            txtTRXKMINDVIDNamaJalan.Text = txtTRXKMINDVDOMNamaJalan.Text
            txtTRXKMINDVIDRTRW.Text = txtTRXKMINDVDOMRTRW.Text
            txtTRXKMINDVIDKelurahan.Text = txtTRXKMINDVDOMKelurahan.Text
            txtTRXKMINDVIDKecamatan.Text = txtTRXKMINDVDOMKecamatan.Text
            txtTRXKMINDVIDKotaKab.Text = txtTRXKMINDVDOMKotaKab.Text
            txtTRXKMINDVIDKodePos.Text = txtTRXKMINDVDOMKodePos.Text
            txtTRXKMINDVIDProvinsi.Text = txtTRXKMINDVDOMProvinsi.Text


            hfTRXKMINDVIDKelurahan.Value = hfTRXKMINDVDOMKelurahan.Value
            hfTRXKMINDVIDKecamatan.Value = hfTRXKMINDVDOMKecamatan.Value
            hfTRXKMINDVIDKotaKab.Value = hfTRXKMINDVDOMKotaKab.Value
            hfTRXKMINDVIDProvinsi.Value = hfTRXKMINDVDOMProvinsi.Value
        Else
            txtTRXKMINDVIDNamaJalan.Text = ""
            txtTRXKMINDVIDRTRW.Text = ""
            txtTRXKMINDVIDKelurahan.Text = ""
            txtTRXKMINDVIDKecamatan.Text = ""
            txtTRXKMINDVIDKotaKab.Text = ""
            txtTRXKMINDVIDKodePos.Text = ""
            txtTRXKMINDVIDProvinsi.Text = ""

            hfTRXKMINDVIDKelurahan.Value = ""
            hfTRXKMINDVIDKecamatan.Value = ""
            hfTRXKMINDVIDKotaKab.Value = ""
            hfTRXKMINDVIDProvinsi.Value = ""
        End If
    End Sub


    Protected Sub chkTRXKKINDVIDCopyDOM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTRXKKINDVIDCopyDOM.CheckedChanged

        If chkTRXKKINDVIDCopyDOM.Checked = True Then
            txtTRXKKINDVIDNamaJalan.Text = txtTRXKKINDVDOMNamaJalan.Text
            txtTRXKKINDVIDRTRW.Text = txtTRXKKINDVDOMRTRW.Text
            txtTRXKKINDVIDKelurahan.Text = txtTRXKKINDVDOMKelurahan.Text
            txtTRXKKINDVIDKecamatan.Text = txtTRXKKINDVDOMKecamatan.Text
            txtTRXKKINDVIDKotaKab.Text = txtTRXKKINDVDOMKotaKab.Text
            txtTRXKKINDVIDKodePos.Text = txtTRXKKINDVDOMKodePos.Text
            txtTRXKKINDVIDProvinsi.Text = txtTRXKKINDVDOMProvinsi.Text


            hfTRXKKINDVIDKelurahan.Value = hfTRXKKINDVDOMKelurahan.Value
            hfTRXKKINDVIDKecamatan.Value = hfTRXKKINDVDOMKecamatan.Value
            hfTRXKKINDVIDKotaKab.Value = hfTRXKKINDVDOMKotaKab.Value
            hfTRXKKINDVIDProvinsi.Value = hfTRXKKINDVDOMProvinsi.Value
        Else
            txtTRXKKINDVIDNamaJalan.Text = ""
            txtTRXKKINDVIDRTRW.Text = ""
            txtTRXKKINDVIDKelurahan.Text = ""
            txtTRXKKINDVIDKecamatan.Text = ""
            txtTRXKKINDVIDKotaKab.Text = ""
            txtTRXKKINDVIDKodePos.Text = ""
            txtTRXKKINDVIDProvinsi.Text = ""

            hfTRXKKINDVIDKelurahan.Value = ""
            hfTRXKKINDVIDKecamatan.Value = ""
            hfTRXKKINDVIDKotaKab.Value = ""
            hfTRXKKINDVIDProvinsi.Value = ""
        End If
    End Sub

    Function isValidAddKas() As Boolean
        Try

            If (lblDetilKasKeluarJumlahRp.Text = "0" OrElse lblDetilKasKeluarJumlahRp.Text = "") AndAlso (lblTRXKMDetilValutaAsingJumlahRp.Text = "0" OrElse lblTRXKMDetilValutaAsingJumlahRp.Text = "") Then
                Throw New Exception("Kas Masuk or Kas Keluar must be filled")

            End If


            If lblTRXKMDetilValutaAsingJumlahRp.Text <> vbNullString OrElse lblTRXKMDetilValutaAsingJumlahRp.Text <> "" Then
                If txtTRXKMTanggalTrx.Text = vbNullString OrElse txtTRXKMTanggalTrx.Text = "" Then
                    Throw New Exception("Tanggal Transaksi (Kas Masuk) must be filled")

                End If
                'If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtTRXKMTanggalTrx.Text) = False Then
                ' Throw New Exception("Tanggal Transaksi (Kas Masuk) format is not valid")
                ' 
                'End If
                If txtTRXKMNamaKantor.Text = vbNullString OrElse txtTRXKMNamaKantor.Text = "" Then
                    Throw New Exception("Nama Kantor (Kas Masuk) must be filled")

                End If
                If txtTRXKMKotaKab.Text = vbNullString OrElse txtTRXKMKotaKab.Text = "" Then
                    Throw New Exception("Kota Kabupaten (Kas Masuk) must be filled")

                End If
                If txtTRXKMProvinsi.Text = vbNullString OrElse txtTRXKMProvinsi.Text = "" Then
                    Throw New Exception("Provinsi (Kas Masuk) must be filled")
                End If
            End If

            If lblDetilKasKeluarJumlahRp.Text <> vbNullString OrElse lblDetilKasKeluarJumlahRp.Text <> "" Then
                If txtTRXKKTanggalTransaksi.Text = vbNullString OrElse txtTRXKKTanggalTransaksi.Text = "" Then
                    Throw New Exception("Tanggal Transaksi (Kas Keluar) must be filled")

                End If
                'If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtTRXKKTanggalTransaksi.Text) = False Then
                ' Throw New Exception("Tanggal Transaksi (Kas Keluar) format is not valid")
                ' 
                'End If
                If txtTRXKKNamaKantor.Text = vbNullString OrElse txtTRXKKNamaKantor.Text = "" Then
                    Throw New Exception("Nama Kantor (Kas Keluar) must be filled")

                End If
                If txtTRXKKKotaKab.Text = vbNullString OrElse txtTRXKKKotaKab.Text = "" Then
                    Throw New Exception("Kota Kabupaten (Kas Keluar) must be filled")

                End If
                If txtTRXKKProvinsi.Text = vbNullString OrElse txtTRXKKProvinsi.Text = "" Then
                    Throw New Exception("Provinsi (Kas Keluar) must be filled")
                End If

            End If

            Return True


        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
            Return False
        End Try
    End Function

    Protected Sub imgAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAdd.Click
        Try
            If isValidAddKas() Then
                Dim defaultDate As New Date(1900, 1, 1)
                Dim objColKas As List(Of ResumeKasMasukKeluar) = SetnGetResumeKasMasukKasKeluar
                If lblTRXKMDetilValutaAsingJumlahRp.Text <> vbNullString OrElse lblTRXKMDetilValutaAsingJumlahRp.Text <> "" Then
                    Dim objKas As ResumeKasMasukKeluar = New ResumeKasMasukKeluar
                    'Insert untuk tampilan Grid
                    objKas.TransactionDate = txtTRXKMTanggalTrx.Text
                    objKas.Branch = txtTRXKMNamaKantor.Text
                    objKas.TransactionNominal = lblTRXKMDetilValutaAsingJumlahRp.Text
                    objKas.AccountNumber = txtTRXKMNoRekening.Text
                    objKas.Kas = "Kas Masuk"
                    objKas.Type = "LTKT"

                    'Insert Data Ke ObjectTransaction
                    objKas.TransactionCashIn = New LTKTTransactionCashIn
                    objKas.TransactionCashIn.TanggalTransaksi = txtTRXKMTanggalTrx.Text
                    objKas.TransactionCashIn.NamaKantorPJK = txtTRXKMNamaKantor.Text
                    objKas.TransactionCashIn.FK_MsKotaKab_Id = CInt(hfTRXKMKotaKab.Value)
                    objKas.TransactionCashIn.FK_MsProvince_Id = CInt(hfTRXKMProvinsi.Value)
                    objKas.TransactionCashIn.NomorRekening = txtTRXKMNoRekening.Text
                    objKas.TransactionCashIn.INDV_Gelar = txtTRXKMINDVGelar.Text
                    objKas.TransactionCashIn.INDV_NamaLengkap = txtTRXKMINDVNamaLengkap.Text
                    objKas.TransactionCashIn.INDV_TempatLahir = txtTRXKMINDVTempatLahir.Text
                    If txtTRXKMINDVTanggalLahir.Text <> "" Then
                        objKas.TransactionCashIn.INDV_TanggalLahir = txtTRXKMINDVTanggalLahir.Text
                    Else
                        objKas.TransactionCashIn.INDV_TanggalLahir = defaultDate
                    End If
                    objKas.TransactionCashIn.INDV_Kewarganegaraan = rblTRXKMINDVKewarganegaraan.SelectedIndex.ToString
                    If cboTRXKMINDVNegara.SelectedIndex <> 0 Then
                        objKas.TransactionCashIn.INDV_ID_FK_MsNegara_Id = CInt(cboTRXKMINDVNegara.SelectedValue)
                    End If
                    objKas.TransactionCashIn.INDV_DOM_NamaJalan = txtTRXKMINDVDOMNamaJalan.Text
                    objKas.TransactionCashIn.INDV_DOM_RTRW = txtTRXKMINDVDOMRTRW.Text
                    If hfTRXKMINDVDOMKelurahan.Value <> "" Then
                        objKas.TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id = CInt(hfTRXKMINDVDOMKelurahan.Value)
                    End If
                    If hfTRXKMINDVDOMKecamatan.Value <> "" Then
                        objKas.TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id = CInt(hfTRXKMINDVDOMKecamatan.Value)
                    End If
                    If hfTRXKMINDVDOMKotaKab.Value <> "" Then
                        objKas.TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id = CInt(hfTRXKMINDVDOMKotaKab.Value)
                    End If
                    objKas.TransactionCashIn.INDV_DOM_KodePos = txtTRXKMINDVDOMKodePos.Text
                    If hfTRXKMINDVDOMProvinsi.Value <> "" Then
                        objKas.TransactionCashIn.INDV_DOM_FK_MsProvince_Id = CInt(hfTRXKMINDVDOMProvinsi.Value)
                    End If
                    objKas.TransactionCashIn.INDV_ID_NamaJalan = txtTRXKMINDVIDNamaJalan.Text
                    objKas.TransactionCashIn.INDV_ID_RTRW = txtTRXKMINDVIDRTRW.Text
                    If hfTRXKMINDVIDKelurahan.Value <> "" Then
                        objKas.TransactionCashIn.INDV_ID_FK_MsKelurahan_Id = CInt(hfTRXKMINDVIDKelurahan.Value)
                    End If
                    If hfTRXKMINDVIDKecamatan.Value <> "" Then
                        objKas.TransactionCashIn.INDV_ID_FK_MsKecamatan_Id = CInt(hfTRXKMINDVIDKecamatan.Value)
                    End If
                    If hfTRXKMINDVIDKotaKab.Value <> "" Then
                        objKas.TransactionCashIn.INDV_ID_FK_MsKotaKab_Id = CInt(hfTRXKMINDVIDKotaKab.Value)
                    End If
                    objKas.TransactionCashIn.INDV_ID_KodePos = txtTRXKMINDVIDKodePos.Text
                    If hfTRXKMINDVIDProvinsi.Value <> "" Then
                        objKas.TransactionCashIn.INDV_ID_FK_MsProvince_Id = CInt(hfTRXKMINDVIDProvinsi.Value)
                    End If
                    objKas.TransactionCashIn.INDV_NA_NamaJalan = txtTRXKMINDVNANamaJalan.Text
                    If hfTRXKMINDVNANegara.Value <> "" Then
                        Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegaraNCBS.ToString & " like '%" & hfTRXKMINDVNANegara.Value.ToString & "%' ", "", 0, Integer.MaxValue, 0)
                        If objMapNegara.Count > 0 Then
                            objKas.TransactionCashIn.INDV_NA_FK_MsNegara_Id = objMapNegara(0).IDNegara
                        End If
                    End If
                    If hfTRXKMINDVNAProvinsi.Value <> "" Then
                        objKas.TransactionCashIn.INDV_NA_FK_MsProvince_Id = CInt(hfTRXKMINDVNAProvinsi.Value)
                    End If
                    If hfTRXKMINDVNAKota.Value <> "" Then
                        objKas.TransactionCashIn.INDV_NA_FK_MsKotaKab_Id = CInt(hfTRXKMINDVNAKota.Value)
                    End If
                    objKas.TransactionCashIn.INDV_NA_KodePos = txtTRXKMINDVNAKodePos.Text
                    If cboTRXKMINDVJenisID.SelectedIndex <> 0 Then
                        objKas.TransactionCashIn.INDV_FK_MsIDType_Id = CInt(cboTRXKMINDVJenisID.SelectedValue)
                    End If
                    objKas.TransactionCashIn.INDV_NomorId = txtTRXKMINDVNomorID.Text
                    objKas.TransactionCashIn.INDV_NPWP = txtTRXKMINDVNPWP.Text
                    If hfTRXKMINDVPekerjaan.Value <> "" Then
                        objKas.TransactionCashIn.INDV_FK_MsPekerjaan_Id = CInt(hfTRXKMINDVPekerjaan.Value)
                    End If
                    objKas.TransactionCashIn.INDV_Jabatan = txtTRXKMINDVJabatan.Text
                    objKas.TransactionCashIn.INDV_PenghasilanRataRata = txtTRXKMINDVPenghasilanRataRata.Text
                    objKas.TransactionCashIn.INDV_TempatBekerja = txtTRXKMINDVTempatKerja.Text
                    objKas.TransactionCashIn.INDV_TujuanTransaksi = txtTRXKMINDVTujuanTrx.Text
                    objKas.TransactionCashIn.INDV_SumberDana = txtTRXKMINDVSumberDana.Text
                    objKas.TransactionCashIn.INDV_NamaBankLain = txtTRXKMINDVNamaBankLain.Text
                    objKas.TransactionCashIn.INDV_NomorRekeningTujuan = txtTRXKMINDVNoRekeningTujuan.Text

                    If cboTRXKMCORPBentukBadanUsaha.SelectedIndex <> 0 Then
                        objKas.TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id = CInt(cboTRXKMCORPBentukBadanUsaha.SelectedValue)
                    End If
                    objKas.TransactionCashIn.CORP_Nama = txtTRXKMCORPNama.Text
                    If hfTRXKMCORPBidangUsaha.Value <> "" Then
                        objKas.TransactionCashIn.CORP_FK_MsBidangUsaha_Id = hfTRXKMCORPBidangUsaha.Value
                    End If
                    objKas.TransactionCashIn.CORP_TipeAlamat = CByte(rblTRXKMCORPTipeAlamat.SelectedIndex)
                    objKas.TransactionCashIn.CORP_NamaJalan = txtTRXKMCORPDLNamaJalan.Text
                    objKas.TransactionCashIn.CORP_RTRW = txtTRXKMCORPDLRTRW.Text
                    If hfTRXKMCORPDLKelurahan.Value <> "" Then
                        objKas.TransactionCashIn.CORP_FK_MsKelurahan_Id = CInt(hfTRXKMCORPDLKelurahan.Value)
                    End If
                    If hfTRXKMCORPDLKecamatan.Value <> "" Then
                        objKas.TransactionCashIn.CORP_FK_MsKecamatan_Id = CInt(hfTRXKMCORPDLKecamatan.Value)
                    End If
                    If hfTRXKMCORPDLKotaKab.Value <> "" Then
                        objKas.TransactionCashIn.CORP_FK_MsKotaKab_Id = CInt(hfTRXKMCORPDLKotaKab.Value)
                    End If
                    objKas.TransactionCashIn.CORP_KodePos = txtTRXKMCORPDLKodePos.Text
                    If hfTRXKMCORPDLProvinsi.Value <> "" Then
                        objKas.TransactionCashIn.CORP_FK_MsProvince_Id = CInt(hfTRXKMCORPDLProvinsi.Value)
                    End If
                    If hfTRXKMCORPDLNegara.Value <> "" Then
                        Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegaraNCBS.ToString & " like '%" & hfTRXKMCORPDLNegara.Value.ToString & "%' ", "", 0, Integer.MaxValue, 0)
                        If objMapNegara.Count > 0 Then
                            objKas.TransactionCashIn.CORP_FK_MsNegara_Id = objMapNegara(0).IDNegara
                        End If
                    End If
                    objKas.TransactionCashIn.CORP_LN_NamaJalan = txtTRXKMCORPLNNamaJalan.Text
                    If hfTRXKMCORPLNNegara.Value <> "" Then
                        Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegaraNCBS.ToString & " like '%" & hfTRXKMCORPLNNegara.Value.ToString & "%' ", "", 0, Integer.MaxValue, 0)
                        If objMapNegara.Count > 0 Then
                            objKas.TransactionCashIn.CORP_LN_FK_MsNegara_Id = objMapNegara(0).IDNegara
                        End If
                    End If
                    If hfTRXKMCORPLNProvinsi.Value <> "" Then
                        objKas.TransactionCashIn.CORP_LN_FK_MsProvince_Id = CInt(hfTRXKMCORPLNProvinsi.Value)
                    End If
                    If hfTRXKMCORPLNKota.Value <> "" Then
                        objKas.TransactionCashIn.CORP_LN_MsKotaKab_Id = CInt(hfTRXKMCORPLNKota.Value)
                    End If
                    objKas.TransactionCashIn.CORP_LN_KodePos = txtTRXKMCORPLNKodePos.Text
                    objKas.TransactionCashIn.CORP_NPWP = txtTRXKMCORPNPWP.Text
                    objKas.TransactionCashIn.CORP_TujuanTransaksi = txtTRXKMCORPTujuanTrx.Text
                    objKas.TransactionCashIn.CORP_SumberDana = txtTRXKMCORPSumberDana.Text
                    objKas.TransactionCashIn.CORP_NamaBankLain = txtTRXKMCORPNamaBankLain.Text
                    objKas.TransactionCashIn.CORP_NomorRekeningTujuan = txtTRXKMCORPNoRekeningTujuan.Text

                    'Insert Data Ke Detail Transaction
                    objKas.DetailTransactionCashIn = New List(Of LTKTDetailCashInTransaction)
                    Dim DetilCol As List(Of LTKTDetailCashInEdit) = SetnGetgrvTRXKMDetilValutaAsing

                    If DetilCol.Count > 0 Then
                        For Each singleObj As LTKTDetailCashInEdit In DetilCol
                            Dim objDetilKasMasuk As New LTKTDetailCashInTransaction
                            If txtTRXKMDetilKasMasuk.Text <> "" Then
                                objDetilKasMasuk.KasMasuk = txtTRXKMDetilKasMasuk.Text
                            End If
                            Dim matauangKM As String = singleObj.MataUang.Substring(0, singleObj.MataUang.IndexOf("-")).Trim
                            Dim objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & " like '%" & matauangKM.ToString & "%'", "", 0, Integer.MaxValue, 0)
                            If objCurrency.Count > 0 Then
                                objDetilKasMasuk.Asing_FK_MsCurrency_Id = objCurrency(0).IdCurrency
                            End If
                            objDetilKasMasuk.Asing_KursTransaksi = singleObj.KursTransaksi
                            objDetilKasMasuk.Asing_TotalKasMasukDalamRupiah = singleObj.JumlahRp
                            objDetilKasMasuk.TotalKasMasuk = lblTRXKMDetilValutaAsingJumlahRp.Text

                            'Masukin ke detail
                            objKas.DetailTransactionCashIn.Add(objDetilKasMasuk)
                        Next
                    Else
                        Dim objDetilKasMasuk As New LTKTDetailCashInTransaction
                        objDetilKasMasuk.KasMasuk = txtTRXKMDetilKasMasuk.Text
                        objDetilKasMasuk.TotalKasMasuk = lblTRXKMDetilValutaAsingJumlahRp.Text
                        objKas.DetailTransactionCashIn.Add(objDetilKasMasuk)
                    End If



                    objColKas.Add(objKas)
                End If
                If lblDetilKasKeluarJumlahRp.Text <> vbNullString OrElse lblDetilKasKeluarJumlahRp.Text <> "" Then
                    Dim objKas As ResumeKasMasukKeluar = New ResumeKasMasukKeluar
                    'Insert untuk tampilan Grid
                    objKas.TransactionDate = txtTRXKKTanggalTransaksi.Text
                    objKas.Branch = txtTRXKKNamaKantor.Text
                    objKas.TransactionNominal = lblDetilKasKeluarJumlahRp.Text
                    objKas.AccountNumber = txtTRXKKRekeningKK.Text
                    objKas.Kas = "Kas Keluar"
                    objKas.Type = "LTKT"

                    'Insert Data Ke ObjectTransction
                    objKas.TransactionCashOut = New LTKTTransactionCashOut
                    objKas.TransactionCashOut.TanggalTransaksi = txtTRXKKTanggalTransaksi.Text
                    objKas.TransactionCashOut.NamaKantorPJK = txtTRXKKNamaKantor.Text
                    objKas.TransactionCashOut.FK_MsKotaKab_Id = CInt(hfTRXKKKotaKab.Value)
                    objKas.TransactionCashOut.FK_MsProvince_Id = CInt(hfTRXKKProvinsi.Value)
                    objKas.TransactionCashOut.NomorRekening = txtTRXKKRekeningKK.Text
                    objKas.TransactionCashOut.INDV_Gelar = txtTRXKKINDVGelar.Text
                    objKas.TransactionCashOut.INDV_NamaLengkap = txtTRXKKINDVNamaLengkap.Text
                    objKas.TransactionCashOut.INDV_TempatLahir = txtTRXKKINDVTempatLahir.Text
                    If txtTRXKKINDVTglLahir.Text <> "" Then
                        objKas.TransactionCashOut.INDV_TanggalLahir = txtTRXKKINDVTglLahir.Text
                    Else
                        objKas.TransactionCashOut.INDV_TanggalLahir = defaultDate
                    End If
                    objKas.TransactionCashOut.INDV_Kewarganegaraan = rblTRXKKINDVKewarganegaraan.SelectedIndex.ToString
                    If cboTRXKKINDVNegara.SelectedIndex <> 0 Then
                        objKas.TransactionCashOut.INDV_ID_FK_MsNegara_Id = CInt(cboTRXKKINDVNegara.SelectedValue)
                    End If
                    objKas.TransactionCashOut.INDV_DOM_NamaJalan = txtTRXKKINDVDOMNamaJalan.Text
                    objKas.TransactionCashOut.INDV_DOM_RTRW = txtTRXKKINDVDOMRTRW.Text
                    If hfTRXKKINDVDOMKelurahan.Value <> "" Then
                        objKas.TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id = CInt(hfTRXKKINDVDOMKelurahan.Value)
                    End If
                    If hfTRXKKINDVDOMKecamatan.Value <> "" Then
                        objKas.TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id = CInt(hfTRXKKINDVDOMKecamatan.Value)
                    End If
                    If hfTRXKKINDVDOMKotaKab.Value <> "" Then
                        objKas.TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id = CInt(hfTRXKKINDVDOMKotaKab.Value)
                    End If
                    objKas.TransactionCashOut.INDV_DOM_KodePos = txtTRXKKINDVDOMKodePos.Text
                    If hfTRXKKINDVDOMProvinsi.Value <> "" Then
                        objKas.TransactionCashOut.INDV_DOM_FK_MsProvince_Id = CInt(hfTRXKKINDVDOMProvinsi.Value)
                    End If
                    objKas.TransactionCashOut.INDV_ID_NamaJalan = txtTRXKKINDVIDNamaJalan.Text
                    objKas.TransactionCashOut.INDV_ID_RTRW = txtTRXKKINDVIDRTRW.Text
                    If hfTRXKKINDVIDKelurahan.Value <> "" Then
                        objKas.TransactionCashOut.INDV_ID_FK_MsKelurahan_Id = CInt(hfTRXKKINDVIDKelurahan.Value)
                    End If
                    If hfTRXKKINDVIDKecamatan.Value <> "" Then
                        objKas.TransactionCashOut.INDV_ID_FK_MsKecamatan_Id = CInt(hfTRXKKINDVIDKecamatan.Value)
                    End If
                    If hfTRXKKINDVIDKotaKab.Value <> "" Then
                        objKas.TransactionCashOut.INDV_ID_FK_MsKotaKab_Id = CInt(hfTRXKKINDVIDKotaKab.Value)
                    End If
                    objKas.TransactionCashOut.INDV_ID_KodePos = txtTRXKKINDVIDKodePos.Text
                    If hfTRXKKINDVIDProvinsi.Value <> "" Then
                        objKas.TransactionCashOut.INDV_ID_FK_MsProvince_Id = CInt(hfTRXKKINDVIDProvinsi.Value)
                    End If
                    objKas.TransactionCashOut.INDV_NA_NamaJalan = txtTRXKKINDVNANamaJalan.Text
                    If hfTRXKKINDVNANegara.Value <> "" Then
                        Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegaraNCBS.ToString & " like '%" & hfTRXKMINDVNANegara.Value.ToString & "%' ", "", 0, Integer.MaxValue, 0)
                        If objMapNegara.Count > 0 Then
                            objKas.TransactionCashOut.INDV_NA_FK_MsNegara_Id = objMapNegara(0).IDNegara
                        End If
                    End If
                    If hfTRXKKINDVNAProvinsi.Value <> "" Then
                        objKas.TransactionCashOut.INDV_NA_FK_MsProvince_Id = CInt(hfTRXKKINDVNAProvinsi.Value)
                    End If
                    If hfTRXKKINDVNAKota.Value <> "" Then
                        objKas.TransactionCashOut.INDV_NA_FK_MsKotaKab_Id = CInt(hfTRXKKINDVNAKota.Value)
                    End If
                    objKas.TransactionCashOut.INDV_NA_KodePos = txtTRXKKINDVNAKodePos.Text
                    If cboTRXKKINDVJenisID.SelectedIndex <> 0 Then
                        objKas.TransactionCashOut.INDV_FK_MsIDType_Id = CInt(cboTRXKKINDVJenisID.SelectedValue)
                    End If
                    objKas.TransactionCashOut.INDV_NomorId = txtTRXKKINDVNomorId.Text
                    objKas.TransactionCashOut.INDV_NPWP = txtTRXKKINDVNPWP.Text
                    If hfTRXKKINDVPekerjaan.Value <> "" Then
                        objKas.TransactionCashOut.INDV_FK_MsPekerjaan_Id = CInt(hfTRXKKINDVPekerjaan.Value)
                    End If
                    objKas.TransactionCashOut.INDV_Jabatan = txtTRXKKINDVJabatan.Text
                    objKas.TransactionCashOut.INDV_PenghasilanRataRata = txtTRXKKINDVPenghasilanRataRata.Text
                    objKas.TransactionCashOut.INDV_TempatBekerja = txtTRXKKINDVTempatKerja.Text
                    objKas.TransactionCashOut.INDV_TujuanTransaksi = txtTRXKKINDVTujuanTrx.Text
                    objKas.TransactionCashOut.INDV_SumberDana = txtTRXKKINDVSumberDana.Text
                    objKas.TransactionCashOut.INDV_NamaBankLain = txtTRXKKINDVNamaBankLain.Text
                    objKas.TransactionCashOut.INDV_NomorRekeningTujuan = txtTRXKKINDVNoRekTujuan.Text
                    objKas.TransactionCashOut.CORP_NamaBankLain = txtTRXKKCORPNamaBankLain.Text
                    objKas.TransactionCashOut.CORP_NomorRekeningTujuan = txtTRXKKCORPNoRekeningTujuan.Text

                    If cboTRXKKCORPBentukBadanUsaha.SelectedIndex <> 0 Then
                        objKas.TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id = CInt(cboTRXKKCORPBentukBadanUsaha.SelectedValue)
                    End If
                    objKas.TransactionCashOut.CORP_Nama = txtTRXKKCORPNama.Text
                    If hfTRXKKCORPBidangUsaha.Value <> "" Then
                        objKas.TransactionCashOut.CORP_FK_MsBidangUsaha_Id = hfTRXKKCORPBidangUsaha.Value
                    End If
                    objKas.TransactionCashOut.CORP_TipeAlamat = CByte(rblTRXKKCORPTipeAlamat.SelectedIndex)
                    objKas.TransactionCashOut.CORP_NamaJalan = txtTRXKKCORPDLNamaJalan.Text
                    objKas.TransactionCashOut.CORP_RTRW = txtTRXKKCORPDLRTRW.Text
                    If hfTRXKKCORPDLKelurahan.Value <> "" Then
                        objKas.TransactionCashOut.CORP_FK_MsKelurahan_Id = CInt(hfTRXKKCORPDLKelurahan.Value)
                    End If
                    If hfTRXKKCORPDLKecamatan.Value <> "" Then
                        objKas.TransactionCashOut.CORP_FK_MsKecamatan_Id = CInt(hfTRXKKCORPDLKecamatan.Value)
                    End If
                    If hfTRXKKCORPDLKotaKab.Value <> "" Then
                        objKas.TransactionCashOut.CORP_FK_MsKotaKab_Id = CInt(hfTRXKKCORPDLKotaKab.Value)
                    End If
                    objKas.TransactionCashOut.CORP_KodePos = txtTRXKKCORPDLKodePos.Text
                    If hfTRXKKCORPDLProvinsi.Value <> "" Then
                        objKas.TransactionCashOut.CORP_FK_MsProvince_Id = CInt(hfTRXKKCORPDLProvinsi.Value)
                    End If
                    If hfTRXKKCORPDLNegara.Value <> "" Then
                        Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegaraNCBS.ToString & " like '%" & hfTRXKKCORPDLNegara.Value.ToString & "%' ", "", 0, Integer.MaxValue, 0)
                        If objMapNegara.Count > 0 Then
                            objKas.TransactionCashOut.CORP_FK_MsNegara_Id = objMapNegara(0).IDNegara
                        End If
                    End If
                    objKas.TransactionCashOut.CORP_LN_NamaJalan = txtTRXKKCORPLNNamaJalan.Text
                    If hfTRXKKCORPLNNegara.Value <> "" Then
                        Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegaraNCBS.ToString & " like '%" & hfTRXKKCORPLNNegara.Value.ToString & "%' ", "", 0, Integer.MaxValue, 0)
                        If objMapNegara.Count > 0 Then
                            objKas.TransactionCashOut.CORP_LN_FK_MsNegara_Id = objMapNegara(0).IDNegara
                        End If
                    End If
                    If hfTRXKKCORPLNProvinsi.Value <> "" Then
                        objKas.TransactionCashOut.CORP_LN_FK_MsProvince_Id = CInt(hfTRXKKCORPLNProvinsi.Value)
                    End If
                    If hfTRXKKCORPLNKota.Value <> "" Then
                        objKas.TransactionCashOut.CORP_LN_MsKotaKab_Id = CInt(hfTRXKKCORPLNKota.Value)
                    End If
                    objKas.TransactionCashOut.CORP_LN_KodePos = txtTRXKKCORPLNKodePos.Text
                    objKas.TransactionCashOut.CORP_NPWP = txtTRXKKCORPNPWP.Text
                    objKas.TransactionCashOut.CORP_TujuanTransaksi = txtTRXKKCORPTujuanTrx.Text
                    objKas.TransactionCashOut.CORP_SumberDana = txtTRXKKCORPSumberDana.Text




                    'Insert Date Ke DetailTransaction
                    objKas.DetailTranscationCashOut = New List(Of LTKTDetailCashOutTransaction)
                    Dim DetilCol As List(Of LTKTDetailCashInEdit) = SetnGetgrvDetilKasKeluar

                    If DetilCol.Count > 0 Then
                        For Each singleObj As LTKTDetailCashInEdit In DetilCol
                            Dim objDetilKasKeluar As New LTKTDetailCashOutTransaction
                            If txtTRXKKDetailKasKeluar.Text <> "" Then
                                objDetilKasKeluar.KasKeluar = txtTRXKKDetailKasKeluar.Text
                            End If
                            Dim matauangKK As String = singleObj.MataUang.Substring(0, singleObj.MataUang.IndexOf("-")).Trim
                            Dim objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & " like '%" & matauangKK.ToString & "%'", "", 0, Integer.MaxValue, 0)
                            If objCurrency.Count > 0 Then
                                objDetilKasKeluar.Asing_FK_MsCurrency_Id = objCurrency(0).IdCurrency
                            End If
                            objDetilKasKeluar.Asing_FK_MsCurrency_Id = objCurrency(0).IdCurrency
                            objDetilKasKeluar.Asing_KursTransaksi = singleObj.KursTransaksi
                            objDetilKasKeluar.Asing_TotalKasKeluarDalamRupiah = singleObj.JumlahRp
                            objDetilKasKeluar.TotalKasKeluar = lblDetilKasKeluarJumlahRp.Text

                            'Masukin ke detail
                            objKas.DetailTranscationCashOut.Add(objDetilKasKeluar)
                        Next
                    Else
                        Dim objDetilKasKeluar As New LTKTDetailCashOutTransaction
                        objDetilKasKeluar.TotalKasKeluar = lblDetilKasKeluarJumlahRp.Text
                        objDetilKasKeluar.KasKeluar = txtTRXKKDetailKasKeluar.Text
                        objKas.DetailTranscationCashOut.Add(objDetilKasKeluar)
                    End If


                    objColKas.Add(objKas)
                End If

                Dim totalKasMasuk As Integer = 0
                Dim totalKasKeluar As Integer = 0
                For Each kas As ResumeKasMasukKeluar In objColKas
                    If kas.Kas = "Kas Masuk" Then
                        totalKasMasuk += CInt(kas.TransactionNominal)
                    Else
                        totalKasKeluar += CInt(kas.TransactionNominal)
                    End If
                Next

                lblTotalKasMasuk.Text = totalKasMasuk.ToString
                lblTotalKasKeluar.Text = totalKasKeluar.ToString

                grvTransaksi.DataSource = objColKas
                grvTransaksi.DataBind()

                clearKasMasukKasKeluar()
            End If


        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub


    Protected Sub grvTransaksi_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvTransaksi.RowDataBound
        If e.Row.RowType <> ListItemType.Footer And e.Row.RowType <> ListItemType.Header Then
            e.Row.Cells(0).Text = e.Row.RowIndex + 1
        End If
    End Sub

    Sub clearKasMasukKasKeluar()
        'bersihin grid
        Session("LTKTEdit.grvTRXKMDetilValutaAsingDATA") = Nothing
        Session("LTKTEdit.grvDetilKasKeluarDATA") = Nothing
        grvDetilKasKeluar.DataSource = SetnGetgrvDetilKasKeluar
        grvDetilKasKeluar.DataBind()
        grvTRXKMDetilValutaAsing.DataSource = SetnGetgrvTRXKMDetilValutaAsing
        grvTRXKMDetilValutaAsing.DataBind()

        'bersihin sisa Kas Masuk
        txtTRXKMTanggalTrx.Text = ""
        txtTRXKMNamaKantor.Text = ""
        txtTRXKMKotaKab.Text = ""
        hfTRXKMKotaKab.Value = ""
        txtTRXKMProvinsi.Text = ""
        hfTRXKMProvinsi.Value = ""
        txtTRXKMDetilKasMasuk.Text = ""
        txtTRXKMDetilMataUang.Text = ""
        hfTRXKMDetilMataUang.Value = ""
        txtTRXKMDetailKursTrx.Text = ""
        txtTRXKMDetilJumlah.Text = ""
        lblTRXKMDetilValutaAsingJumlahRp.Text = ""
        txtTRXKMNoRekening.Text = ""
        txtTRXKMINDVGelar.Text = ""
        txtTRXKMINDVNamaLengkap.Text = ""
        txtTRXKMINDVTempatLahir.Text = ""
        txtTRXKMINDVTanggalLahir.Text = ""
        rblTRXKMINDVKewarganegaraan.SelectedIndex = 0
        cboTRXKMINDVNegara.SelectedIndex = 0
        txtTRXKMINDVDOMNamaJalan.Text = ""
        txtTRXKMINDVDOMRTRW.Text = ""
        txtTRXKMINDVDOMKelurahan.Text = ""
        hfTRXKMINDVDOMKelurahan.Value = ""
        txtTRXKMINDVDOMKecamatan.Text = ""
        hfTRXKMINDVDOMKecamatan.Value = ""
        txtTRXKMINDVDOMKotaKab.Text = ""
        hfTRXKMINDVDOMKotaKab.Value = ""
        txtTRXKMINDVDOMKodePos.Text = ""
        txtTRXKMINDVDOMProvinsi.Text = ""
        hfTRXKMINDVDOMProvinsi.Value = ""
        chkTRXKMINDVCopyDOM.Checked = False
        txtTRXKMINDVIDNamaJalan.Text = ""
        txtTRXKMINDVIDRTRW.Text = ""
        txtTRXKMINDVIDKelurahan.Text = ""
        hfTRXKMINDVIDKelurahan.Value = ""
        txtTRXKMINDVIDKecamatan.Text = ""
        hfTRXKMINDVIDKecamatan.Value = ""
        txtTRXKMINDVIDKotaKab.Text = ""
        hfTRXKMINDVIDKotaKab.Value = ""
        txtTRXKMINDVIDKodePos.Text = ""
        txtTRXKMINDVIDProvinsi.Text = ""
        hfTRXKMINDVIDProvinsi.Value = ""
        txtTRXKMINDVNANamaJalan.Text = ""
        txtTRXKMINDVNANegara.Text = ""
        hfTRXKMINDVNANegara.Value = ""
        txtTRXKMINDVNAProvinsi.Text = ""
        hfTRXKMINDVNAProvinsi.Value = ""
        txtTRXKMINDVNAKota.Text = ""
        hfTRXKMINDVNAKota.Value = ""
        txtTRXKMINDVNAKodePos.Text = ""
        cboTRXKMINDVJenisID.SelectedIndex = 0
        txtTRXKMINDVNomorID.Text = ""
        txtTRXKMINDVNPWP.Text = ""
        txtTRXKMINDVPekerjaan.Text = ""
        hfTRXKMINDVPekerjaan.Value = ""
        txtTRXKMINDVJabatan.Text = ""
        txtTRXKMINDVPenghasilanRataRata.Text = ""
        txtTRXKMINDVTempatKerja.Text = ""
        txtTRXKMINDVTujuanTrx.Text = ""
        txtTRXKMINDVSumberDana.Text = ""
        txtTRXKMINDVNamaBankLain.Text = ""
        txtTRXKMINDVNoRekeningTujuan.Text = ""

        cboTRXKMCORPBentukBadanUsaha.SelectedIndex = 0
        txtTRXKMCORPNama.Text = ""
        txtTRXKMCORPBidangUsaha.Text = ""
        hfTRXKMCORPBidangUsaha.Value = ""
        txtTRXKMCORPDLNamaJalan.Text = ""
        txtTRXKMCORPDLRTRW.Text = ""
        txtTRXKMCORPDLKelurahan.Text = ""
        hfTRXKMCORPDLKelurahan.Value = ""
        txtTRXKMCORPDLKecamatan.Text = ""
        hfTRXKMCORPDLKecamatan.Value = ""
        txtTRXKMCORPDLKotaKab.Text = ""
        hfTRXKMCORPDLKotaKab.Value = ""
        txtTRXKMCORPDLKodePos.Text = ""
        txtTRXKMCORPDLProvinsi.Text = ""
        hfTRXKMCORPDLProvinsi.Value = ""
        txtTRXKMCORPDLNegara.Text = ""
        hfTRXKMCORPDLNegara.Value = ""
        txtTRXKMCORPLNNamaJalan.Text = ""
        txtTRXKMCORPLNNegara.Text = ""
        hfTRXKMCORPLNNegara.Value = ""
        txtTRXKMCORPLNProvinsi.Text = ""
        hfTRXKMCORPLNProvinsi.Value = ""
        txtTRXKMCORPLNKota.Text = ""
        hfTRXKMCORPLNKota.Value = ""
        txtTRXKMCORPLNKodePos.Text = ""
        txtTRXKMCORPNPWP.Text = ""
        txtTRXKMCORPTujuanTrx.Text = ""
        txtTRXKMCORPSumberDana.Text = ""
        txtTRXKMCORPNamaBankLain.Text = ""
        txtTRXKMCORPNoRekeningTujuan.Text = ""


        'Bersihin sisa Kas Keluar
        txtTRXKKTanggalTransaksi.Text = ""
        txtTRXKKNamaKantor.Text = ""
        txtTRXKKKotaKab.Text = ""
        hfTRXKKKotaKab.Value = ""
        txtTRXKKProvinsi.Text = ""
        hfTRXKKProvinsi.Value = ""
        txtTRXKKDetailKasKeluar.Text = ""
        txtTRXKKDetilMataUang.Text = ""
        hfTRXKKDetilMataUang.Value = ""
        txtTRXKKDetilKursTrx.Text = ""
        txtTRXKKDetilJumlah.Text = ""
        lblDetilKasKeluarJumlahRp.Text = ""
        'txtTRXKKNoRekening.Text = ""
        txtTRXKKINDVGelar.Text = ""
        txtTRXKKINDVNamaLengkap.Text = ""
        txtTRXKKINDVTempatLahir.Text = ""
        txtTRXKKINDVTglLahir.Text = ""
        rblTRXKKINDVKewarganegaraan.SelectedIndex = 0
        cboTRXKKINDVNegara.SelectedIndex = 0
        txtTRXKKINDVDOMNamaJalan.Text = ""
        txtTRXKKINDVDOMRTRW.Text = ""
        txtTRXKKINDVDOMKelurahan.Text = ""
        hfTRXKKINDVDOMKelurahan.Value = ""
        txtTRXKKINDVDOMKecamatan.Text = ""
        hfTRXKKINDVDOMKecamatan.Value = ""
        txtTRXKKINDVDOMKotaKab.Text = ""
        hfTRXKKINDVDOMKotaKab.Value = ""
        txtTRXKKINDVDOMKodePos.Text = ""
        txtTRXKKINDVDOMProvinsi.Text = ""
        hfTRXKKINDVDOMProvinsi.Value = ""
        chkTRXKKINDVIDCopyDOM.Checked = False
        txtTRXKKINDVIDNamaJalan.Text = ""
        txtTRXKKINDVIDRTRW.Text = ""
        txtTRXKKINDVIDKelurahan.Text = ""
        hfTRXKKINDVIDKelurahan.Value = ""
        txtTRXKKINDVIDKecamatan.Text = ""
        hfTRXKKINDVIDKecamatan.Value = ""
        txtTRXKKINDVIDKotaKab.Text = ""
        hfTRXKKINDVIDKotaKab.Value = ""
        txtTRXKKINDVIDKodePos.Text = ""
        txtTRXKKINDVIDProvinsi.Text = ""
        hfTRXKKINDVIDProvinsi.Value = ""
        txtTRXKKINDVNANamaJalan.Text = ""
        txtTRXKKINDVNANegara.Text = ""
        hfTRXKKINDVNANegara.Value = ""
        txtTRXKKINDVNAProvinsi.Text = ""
        hfTRXKKINDVNAProvinsi.Value = ""
        txtTRXKKINDVNAKota.Text = ""
        hfTRXKKINDVNAKota.Value = ""
        txtTRXKKINDVNAKodePos.Text = ""
        cboTRXKKINDVJenisID.SelectedIndex = 0
        txtTRXKKINDVNomorId.Text = ""
        txtTRXKKINDVNPWP.Text = ""
        txtTRXKKINDVPekerjaan.Text = ""
        hfTRXKKINDVPekerjaan.Value = ""
        txtTRXKKINDVJabatan.Text = ""
        txtTRXKKINDVPenghasilanRataRata.Text = ""
        txtTRXKKINDVTempatKerja.Text = ""
        txtTRXKKINDVTujuanTrx.Text = ""
        txtTRXKKINDVSumberDana.Text = ""
        txtTRXKKINDVNamaBankLain.Text = ""
        txtTRXKKINDVNoRekTujuan.Text = ""

        cboTRXKKCORPBentukBadanUsaha.SelectedIndex = 0
        txtTRXKKCORPNama.Text = ""
        txtTRXKKCORPBidangUsaha.Text = ""
        hfTRXKKCORPBidangUsaha.Value = ""
        txtTRXKKCORPDLNamaJalan.Text = ""
        txtTRXKKCORPDLRTRW.Text = ""
        txtTRXKKCORPDLKelurahan.Text = ""
        hfTRXKKCORPDLKelurahan.Value = ""
        txtTRXKKCORPDLKecamatan.Text = ""
        hfTRXKKCORPDLKecamatan.Value = ""
        txtTRXKKCORPDLKotaKab.Text = ""
        hfTRXKKCORPDLKotaKab.Value = ""
        txtTRXKKCORPDLKodePos.Text = ""
        txtTRXKKCORPDLProvinsi.Text = ""
        hfTRXKKCORPDLProvinsi.Value = ""
        txtTRXKKCORPDLNegara.Text = ""
        hfTRXKKCORPDLNegara.Value = ""
        txtTRXKKCORPLNNamaJalan.Text = ""
        txtTRXKKCORPLNNegara.Text = ""
        hfTRXKKCORPLNNegara.Value = ""
        txtTRXKKCORPLNProvinsi.Text = ""
        hfTRXKKCORPLNProvinsi.Value = ""
        txtTRXKKCORPLNKota.Text = ""
        hfTRXKKCORPLNKota.Value = ""
        txtTRXKKCORPLNKodePos.Text = ""
        txtTRXKKCORPNPWP.Text = ""
        txtTRXKKCORPTujuanTrx.Text = ""
        txtTRXKKCORPSumberDana.Text = ""
        txtTRXKKCORPNamaBankLain.Text = ""
        txtTRXKKCORPNoRekeningTujuan.Text = ""
        txtTRXKKRekeningKK.Text = ""


    End Sub

    Protected Sub imgClearAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClearAll.Click
        clearKasMasukKasKeluar()
    End Sub

    Protected Sub DeleteResume_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objgridviewrow As GridViewRow = CType(CType(sender, LinkButton).NamingContainer, GridViewRow)
        Dim objResume As List(Of ResumeKasMasukKeluar) = SetnGetResumeKasMasukKasKeluar
        objResume.RemoveAt(objgridviewrow.RowIndex)
        grvTransaksi.DataSource = objResume
        grvTransaksi.DataBind()

    End Sub

    Protected Sub EditResume_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clearKasMasukKasKeluar()
        Dim objgridviewrow As GridViewRow = CType(CType(sender, LinkButton).NamingContainer, GridViewRow)
        SetnGetRowEdit = objgridviewrow.RowIndex

        'LoadData ke KasMasuk dan Keluar
        Dim objResume As List(Of ResumeKasMasukKeluar) = SetnGetResumeKasMasukKasKeluar

        'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
        Dim i As Integer = 0 'buat iterasi
        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetAll
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objKelurahan As TList(Of MsKelurahan) = DataRepository.MsKelurahanProvider.GetAll
        Dim objSKelurahan As MsKelurahan 'Penampung hasil search kelurahan
        Dim objKecamatan As TList(Of MsKecamatan) = DataRepository.MsKecamatanProvider.GetAll
        Dim objSkecamatan As MsKecamatan 'Penampung hasil search kecamatan
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetAll
        Dim objSMapNegara As MappingMsNegaraNCBSPPATK 'Penampung hasil search mappingnegara yang pke picker
        Dim objNegara As TList(Of MsNegaraNCBS) = DataRepository.MsNegaraNCBSProvider.GetAll
        Dim objSnegara As MsNegaraNCBS 'Penampung hasil search negara yang pke picker
        Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetAll
        Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
        Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
        Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha

        If objResume(SetnGetRowEdit).Kas = "Kas Masuk" Then

            MultiView1.ActiveViewIndex = 0

            'LoadData ke DetailKasMasuk
            Dim objDetailKasMasuk As List(Of LTKTDetailCashInEdit) = SetnGetgrvTRXKMDetilValutaAsing
            For Each obj As LTKTDetailCashInTransaction In objResume(SetnGetRowEdit).DetailTransactionCashIn
                Dim singleDetailKasMasuk As New LTKTDetailCashInEdit

                If obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault <> Nothing Then
                    singleDetailKasMasuk.Jumlah = (CInt(obj.Asing_TotalKasMasukDalamRupiah) / CInt(obj.Asing_KursTransaksi)).ToString
                    singleDetailKasMasuk.JumlahRp = obj.Asing_TotalKasMasukDalamRupiah.ToString
                    singleDetailKasMasuk.KursTransaksi = obj.Asing_KursTransaksi.ToString
                    Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(obj.Asing_FK_MsCurrency_Id)
                    If Not IsNothing(objKurs) Then
                        singleDetailKasMasuk.MataUang = objKurs.Code & " - " & objKurs.Name
                    End If
                    objDetailKasMasuk.Add(singleDetailKasMasuk)
                End If

                txtTRXKMDetilKasMasuk.Text = obj.KasMasuk.ToString
                lblTRXKMDetilValutaAsingJumlahRp.Text = obj.TotalKasMasuk.ToString
            Next
            grvTRXKMDetilValutaAsing.DataSource = objDetailKasMasuk
            grvTRXKMDetilValutaAsing.DataBind()



            'LoadData ke field Kas Masuk
            txtTRXKMTanggalTrx.Text = Convert.ToDateTime(objResume(SetnGetRowEdit).TransactionCashIn.TanggalTransaksi).ToString("dd-MMM-yyyy")
            txtTRXKMNamaKantor.Text = objResume(SetnGetRowEdit).TransactionCashIn.NamaKantorPJK.ToString

            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.FK_MsKotaKab_Id)
            txtTRXKMKotaKab.Text = objSkotaKab.NamaKotaKab
            hfTRXKMKotaKab.Value = objSkotaKab.IDKotaKab
            objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.FK_MsProvince_Id)
            txtTRXKMProvinsi.Text = objSProvinsi.Nama
            hfTRXKMProvinsi.Value = objSProvinsi.IdProvince.ToString
            txtTRXKMNoRekening.Text = objResume(SetnGetRowEdit).TransactionCashIn.NomorRekening
            txtTRXKMINDVGelar.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_Gelar
            txtTRXKMINDVNamaLengkap.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_NamaLengkap
            txtTRXKMINDVTempatLahir.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_TempatLahir
            txtTRXKMINDVTanggalLahir.Text = Convert.ToDateTime(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TanggalLahir).ToString("dd-MMM-yyyy")
            rblTRXKMINDVKewarganegaraan.SelectedIndex = CInt(objResume(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan)
            If rblTRXKMINDVKewarganegaraan.SelectedIndex = 1 Then
                i = 0
                For Each itemCboNegara As ListItem In cboTRXKMINDVNegara.Items
                    If objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsNegara_Id.ToString = itemCboNegara.Value.ToString Then
                        cboTRXKMINDVNegara.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
            End If
            txtTRXKMINDVDOMNamaJalan.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_NamaJalan
            txtTRXKMINDVDOMRTRW.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_RTRW
            objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id)
            If Not IsNothing(objSKelurahan) Then
                txtTRXKMINDVDOMKelurahan.Text = objSKelurahan.NamaKelurahan
                hfTRXKMINDVDOMKelurahan.Value = objSKelurahan.IDKelurahan
            End If
            objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id)
            If Not IsNothing(objSkecamatan) Then
                txtTRXKMINDVDOMKecamatan.Text = objSkecamatan.NamaKecamatan
                hfTRXKMINDVDOMKecamatan.Value = objSkecamatan.IDKecamatan
            End If
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id)
            If Not IsNothing(objSkotaKab) Then
                txtTRXKMINDVDOMKotaKab.Text = objSkotaKab.NamaKotaKab
                hfTRXKMINDVDOMKotaKab.Value = objSkotaKab.IDKotaKab
            End If
            txtTRXKMINDVDOMKodePos.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_KodePos
            objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsProvince_Id)
            If Not IsNothing(objSProvinsi) Then
                txtTRXKMINDVDOMProvinsi.Text = objSProvinsi.Nama
                hfTRXKMINDVDOMProvinsi.Value = objSProvinsi.IdProvince
            End If
            'chkTRXKMINDVCopyDOM.Checked = False
            txtTRXKMINDVIDNamaJalan.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_NamaJalan
            txtTRXKMINDVIDRTRW.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_RTRW
            objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKelurahan_Id)
            If Not IsNothing(objSKelurahan) Then
                txtTRXKMINDVIDKelurahan.Text = objSKelurahan.NamaKelurahan
                hfTRXKMINDVIDKelurahan.Value = objSKelurahan.IDKelurahan
            End If
            objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKecamatan_Id)
            If Not IsNothing(objSkecamatan) Then
                txtTRXKMINDVIDKecamatan.Text = objSkecamatan.NamaKecamatan
                hfTRXKMINDVIDKecamatan.Value = objSkecamatan.IDKecamatan
            End If
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKotaKab_Id)
            If Not IsNothing(objSkotaKab) Then
                txtTRXKMINDVIDKotaKab.Text = objSkotaKab.NamaKotaKab
                hfTRXKMINDVIDKotaKab.Value = objSkotaKab.IDKotaKab
            End If
            txtTRXKMINDVIDKodePos.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_KodePos
            objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsProvince_Id)
            If Not IsNothing(objSProvinsi) Then
                txtTRXKMINDVIDProvinsi.Text = objSProvinsi.Nama
                hfTRXKMINDVIDProvinsi.Value = objSProvinsi.IdProvince
            End If
            txtTRXKMINDVNANamaJalan.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_NamaJalan
            objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsNegara_Id)
            If Not IsNothing(objSMapNegara) Then
                objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                If Not IsNothing(objSnegara) Then
                    txtTRXKMINDVNANegara.Text = objSnegara.NamaNegara
                    hfTRXKMINDVNANegara.Value = objSnegara.IDNegaraNCBS
                End If
            End If
            objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsProvince_Id)
            If Not IsNothing(objSProvinsi) Then
                txtTRXKMINDVNAProvinsi.Text = objSProvinsi.Nama
                hfTRXKMINDVNAProvinsi.Value = objSProvinsi.IdProvince
            End If
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsKotaKab_Id)
            If Not IsNothing(objSkotaKab) Then
                txtTRXKMINDVNAKota.Text = objSkotaKab.NamaKotaKab
                hfTRXKMINDVNAKota.Value = objSkotaKab.IDKotaKab
            End If
            txtTRXKMINDVNAKodePos.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_KodePos
            If objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsIDType_Id.GetValueOrDefault <> Nothing Then
                i = 0
                For Each itemJenisID As ListItem In cboTRXKMINDVJenisID.Items
                    If itemJenisID.Value = objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsIDType_Id.ToString Then
                        cboTRXKMINDVJenisID.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
            End If
            txtTRXKMINDVNomorID.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_NomorId
            txtTRXKMINDVNPWP.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_NPWP
            objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsPekerjaan_Id.GetValueOrDefault)
            If Not IsNothing(objSPekerjaan) Then
                txtTRXKMINDVPekerjaan.Text = objSPekerjaan.NamaPekerjaan
                hfTRXKMINDVPekerjaan.Value = objSPekerjaan.IDPekerjaan
            End If
            txtTRXKMINDVJabatan.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_Jabatan
            txtTRXKMINDVPenghasilanRataRata.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_PenghasilanRataRata
            txtTRXKMINDVTempatKerja.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_TempatBekerja
            txtTRXKMINDVTujuanTrx.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_TujuanTransaksi
            txtTRXKMINDVSumberDana.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_SumberDana
            txtTRXKMINDVNamaBankLain.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_NamaBankLain
            txtTRXKMINDVNoRekeningTujuan.Text = objResume(SetnGetRowEdit).TransactionCashIn.INDV_NomorRekeningTujuan

            'CORP
            If objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault <> Nothing Then
                i = 0
                For Each itemBentukBadangUsaha As ListItem In cboTRXKMCORPBentukBadanUsaha.Items
                    If itemBentukBadangUsaha.Value.ToString = objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id Then
                        cboTRXKMCORPBentukBadanUsaha.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
            End If

            txtTRXKMCORPNama.Text = objResume(SetnGetRowEdit).TransactionCashIn.CORP_Nama
            objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBidangUsaha_Id)
            If Not IsNothing(objSBidangUsaha) Then
                txtTRXKMCORPBidangUsaha.Text = objSBidangUsaha.NamaBidangUsaha
                hfTRXKMCORPBidangUsaha.Value = objSBidangUsaha.IdBidangUsaha
            End If
            If objResume(SetnGetRowEdit).TransactionCashIn.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                rblTRXKMCORPTipeAlamat.SelectedIndex = CInt(objResume(SetnGetRowEdit).TransactionCashIn.CORP_TipeAlamat)
            End If
            txtTRXKMCORPDLNamaJalan.Text = objResume(SetnGetRowEdit).TransactionCashIn.CORP_NamaJalan
            txtTRXKMCORPDLRTRW.Text = objResume(SetnGetRowEdit).TransactionCashIn.CORP_RTRW
            objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKelurahan_Id)
            If Not IsNothing(objSKelurahan) Then
                txtTRXKMCORPDLKelurahan.Text = objSKelurahan.NamaKelurahan
                hfTRXKMCORPDLKelurahan.Value = objSKelurahan.IDKelurahan
            End If
            objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKecamatan_Id)
            If Not IsNothing(objSkecamatan) Then
                txtTRXKMCORPDLKecamatan.Text = objSkecamatan.NamaKecamatan
                hfTRXKMCORPDLKecamatan.Value = objSkecamatan.IDKecamatan
            End If
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKotaKab_Id)
            If Not IsNothing(objSkotaKab) Then
                txtTRXKMCORPDLKotaKab.Text = objSkotaKab.NamaKotaKab
                hfTRXKMCORPDLKotaKab.Value = objSkotaKab.IDKotaKab
            End If
            txtTRXKMCORPDLKodePos.Text = objResume(SetnGetRowEdit).TransactionCashIn.CORP_KodePos
            objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsProvince_Id)
            If Not IsNothing(objSProvinsi) Then
                txtTRXKMCORPDLProvinsi.Text = objSProvinsi.Nama
                hfTRXKMCORPDLProvinsi.Value = objSProvinsi.IdProvince
            End If
            objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsNegara_Id)
            If Not IsNothing(objSMapNegara) Then
                objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                If Not IsNothing(objSnegara) Then
                    txtTRXKMCORPDLNegara.Text = objSnegara.NamaNegara
                    hfTRXKMCORPDLNegara.Value = objSnegara.IDNegaraNCBS
                End If
            End If
            txtTRXKMCORPLNNamaJalan.Text = objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_NamaJalan
            objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsNegara_Id)
            If Not IsNothing(objSMapNegara) Then
                objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                If Not IsNothing(objSnegara) Then
                    txtTRXKMCORPLNNegara.Text = objSnegara.NamaNegara
                    hfTRXKMCORPLNNegara.Value = objSnegara.IDNegaraNCBS
                End If
            End If
            objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsProvince_Id)
            If Not IsNothing(objSProvinsi) Then
                txtTRXKMCORPLNProvinsi.Text = objSProvinsi.Nama
                hfTRXKMCORPLNProvinsi.Value = objSProvinsi.IdProvince
            End If
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_MsKotaKab_Id)
            If Not IsNothing(objSkotaKab) Then
                txtTRXKMCORPLNKota.Text = objSkotaKab.NamaKotaKab
                hfTRXKMCORPLNKota.Value = objSkotaKab.IDKotaKab
            End If
            txtTRXKMCORPLNKodePos.Text = objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_KodePos
            txtTRXKMCORPNPWP.Text = objResume(SetnGetRowEdit).TransactionCashIn.CORP_NPWP
            txtTRXKMCORPTujuanTrx.Text = objResume(SetnGetRowEdit).TransactionCashIn.CORP_TujuanTransaksi
            txtTRXKMCORPSumberDana.Text = objResume(SetnGetRowEdit).TransactionCashIn.CORP_SumberDana
            txtTRXKMCORPNamaBankLain.Text = objResume(SetnGetRowEdit).TransactionCashIn.CORP_NamaBankLain
            txtTRXKMCORPNoRekeningTujuan.Text = objResume(SetnGetRowEdit).TransactionCashIn.CORP_NomorRekeningTujuan


        ElseIf objResume(SetnGetRowEdit).Kas = "Kas Keluar" Then

            MultiView1.ActiveViewIndex = 1

            'LoadData ke DetailKasKeluar
            Dim objDetailKasKeluar As List(Of LTKTDetailCashInEdit) = SetnGetgrvDetilKasKeluar
            For Each obj As LTKTDetailCashOutTransaction In objResume(SetnGetRowEdit).DetailTranscationCashOut
                Dim singleDetailKasKeluar As New LTKTDetailCashInEdit

                If obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault <> Nothing Then
                    singleDetailKasKeluar.Jumlah = (CInt(obj.Asing_TotalKasKeluarDalamRupiah) / CInt(obj.Asing_KursTransaksi)).ToString
                    singleDetailKasKeluar.JumlahRp = obj.Asing_TotalKasKeluarDalamRupiah.ToString
                    singleDetailKasKeluar.KursTransaksi = obj.Asing_KursTransaksi.ToString
                    Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(obj.Asing_FK_MsCurrency_Id)
                    If Not IsNothing(objKurs) Then
                        singleDetailKasKeluar.MataUang = objKurs.Code & " - " & objKurs.Name
                    End If
                    objDetailKasKeluar.Add(singleDetailKasKeluar)
                End If

                txtTRXKKDetailKasKeluar.Text = obj.KasKeluar.ToString
                lblDetilKasKeluarJumlahRp.Text = obj.TotalKasKeluar.ToString
            Next
            grvDetilKasKeluar.DataSource = objDetailKasKeluar
            grvDetilKasKeluar.DataBind()


            'LoadData ke field Kas Keluar
            txtTRXKKTanggalTransaksi.Text = Convert.ToDateTime(objResume(SetnGetRowEdit).TransactionCashOut.TanggalTransaksi).ToString("dd-MMM-yyyy")
            txtTRXKKNamaKantor.Text = objResume(SetnGetRowEdit).TransactionCashOut.NamaKantorPJK.ToString

            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.FK_MsKotaKab_Id)
            txtTRXKKKotaKab.Text = objSkotaKab.NamaKotaKab
            hfTRXKKKotaKab.Value = objSkotaKab.IDKotaKab
            objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.FK_MsProvince_Id)
            txtTRXKKProvinsi.Text = objSProvinsi.Nama
            hfTRXKKProvinsi.Value = objSProvinsi.IdProvince.ToString
            txtTRXKKRekeningKK.Text = objResume(SetnGetRowEdit).TransactionCashOut.NomorRekening
            txtTRXKKINDVGelar.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_Gelar
            txtTRXKKINDVNamaLengkap.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_NamaLengkap
            txtTRXKKINDVTempatLahir.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_TempatLahir
            txtTRXKKINDVTglLahir.Text = Convert.ToDateTime(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TanggalLahir).ToString("dd-MMMM-yyyy")
            rblTRXKKINDVKewarganegaraan.SelectedIndex = CInt(objResume(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan)
            If rblTRXKKINDVKewarganegaraan.SelectedIndex = 1 Then
                i = 0
                For Each itemCboNegara As ListItem In cboTRXKKINDVNegara.Items
                    If objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsNegara_Id.ToString = itemCboNegara.Value.ToString Then
                        cboTRXKKINDVNegara.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
            End If
            If objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                rblTRXKKCORPTipeAlamat.SelectedIndex = CInt(objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat)
            End If
            txtTRXKKINDVDOMNamaJalan.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_NamaJalan
            txtTRXKKINDVDOMRTRW.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_RTRW
            objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id)
            If Not IsNothing(objSKelurahan) Then
                txtTRXKKINDVDOMKelurahan.Text = objSKelurahan.NamaKelurahan
                hfTRXKKINDVDOMKelurahan.Value = objSKelurahan.IDKelurahan
            End If
            objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id)
            If Not IsNothing(objSkecamatan) Then
                txtTRXKKINDVDOMKecamatan.Text = objSkecamatan.NamaKecamatan
                hfTRXKKINDVDOMKecamatan.Value = objSkecamatan.IDKecamatan
            End If
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id)
            If Not IsNothing(objSkotaKab) Then
                txtTRXKKINDVDOMKotaKab.Text = objSkotaKab.NamaKotaKab
                hfTRXKKINDVDOMKotaKab.Value = objSkotaKab.IDKotaKab
            End If
            txtTRXKKINDVDOMKodePos.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_KodePos
            objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsProvince_Id)
            If Not IsNothing(objSProvinsi) Then
                txtTRXKKINDVDOMProvinsi.Text = objSProvinsi.Nama
                hfTRXKKINDVDOMProvinsi.Value = objSProvinsi.IdProvince
            End If
            'chkTRXKKINDVCopyDOM.Checked = False
            txtTRXKKINDVIDNamaJalan.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_NamaJalan
            txtTRXKKINDVIDRTRW.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_RTRW
            objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKelurahan_Id)
            If Not IsNothing(objSKelurahan) Then
                txtTRXKKINDVIDKelurahan.Text = objSKelurahan.NamaKelurahan
                hfTRXKKINDVIDKelurahan.Value = objSKelurahan.IDKelurahan
            End If
            objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKecamatan_Id)
            If Not IsNothing(objSkecamatan) Then
                txtTRXKKINDVIDKecamatan.Text = objSkecamatan.NamaKecamatan
                hfTRXKKINDVIDKecamatan.Value = objSkecamatan.IDKecamatan
            End If
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKotaKab_Id)
            If Not IsNothing(objSkotaKab) Then
                txtTRXKKINDVIDKotaKab.Text = objSkotaKab.NamaKotaKab
                hfTRXKKINDVIDKotaKab.Value = objSkotaKab.IDKotaKab
            End If
            txtTRXKKINDVIDKodePos.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_KodePos
            objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsProvince_Id)
            If Not IsNothing(objSProvinsi) Then
                txtTRXKKINDVIDProvinsi.Text = objSProvinsi.Nama
                hfTRXKKINDVIDProvinsi.Value = objSProvinsi.IdProvince
            End If
            txtTRXKKINDVNANamaJalan.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_NamaJalan
            objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsNegara_Id)
            If Not IsNothing(objSMapNegara) Then
                objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                If Not IsNothing(objSnegara) Then
                    txtTRXKKINDVNANegara.Text = objSnegara.NamaNegara
                    hfTRXKKINDVNANegara.Value = objSnegara.IDNegaraNCBS
                End If
            End If
            objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsProvince_Id)
            If Not IsNothing(objSProvinsi) Then
                txtTRXKKINDVNAProvinsi.Text = objSProvinsi.Nama
                hfTRXKKINDVNAProvinsi.Value = objSProvinsi.IdProvince
            End If
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsKotaKab_Id)
            If Not IsNothing(objSkotaKab) Then
                txtTRXKKINDVNAKota.Text = objSkotaKab.NamaKotaKab
                hfTRXKKINDVNAKota.Value = objSkotaKab.IDKotaKab
            End If
            txtTRXKKINDVNAKodePos.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_KodePos
            If objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsIDType_Id.GetValueOrDefault <> Nothing Then
                i = 0
                For Each itemJenisID As ListItem In cboTRXKKINDVJenisID.Items
                    If itemJenisID.Value = objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsIDType_Id.ToString Then
                        cboTRXKKINDVJenisID.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
            End If
            txtTRXKKINDVNomorId.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_NomorId
            txtTRXKKINDVNPWP.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_NPWP
            objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsPekerjaan_Id.GetValueOrDefault)
            If Not IsNothing(objSPekerjaan) Then
                txtTRXKKINDVPekerjaan.Text = objSPekerjaan.NamaPekerjaan
                hfTRXKKINDVPekerjaan.Value = objSPekerjaan.IDPekerjaan
            End If
            txtTRXKKINDVJabatan.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_Jabatan
            txtTRXKKINDVPenghasilanRataRata.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_PenghasilanRataRata
            txtTRXKKINDVTempatKerja.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_TempatBekerja
            txtTRXKKINDVTujuanTrx.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_TujuanTransaksi
            txtTRXKKINDVSumberDana.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_SumberDana
            txtTRXKKINDVNamaBankLain.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_NamaBankLain
            txtTRXKKINDVNoRekTujuan.Text = objResume(SetnGetRowEdit).TransactionCashOut.INDV_NomorRekeningTujuan

            'CORP
            If objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault <> Nothing Then
                i = 0
                For Each itemBentukBadangUsaha As ListItem In cboTRXKKCORPBentukBadanUsaha.Items
                    If itemBentukBadangUsaha.Value.ToString = objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id Then
                        cboTRXKKCORPBentukBadanUsaha.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
            End If

            txtTRXKKCORPNama.Text = objResume(SetnGetRowEdit).TransactionCashOut.CORP_Nama
            objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBidangUsaha_Id)
            If Not IsNothing(objSBidangUsaha) Then
                txtTRXKKCORPBidangUsaha.Text = objSBidangUsaha.NamaBidangUsaha
                hfTRXKKCORPBidangUsaha.Value = objSBidangUsaha.IdBidangUsaha
            End If
            If objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                rblTRXKKCORPTipeAlamat.SelectedIndex = CInt(objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat)
            End If
            txtTRXKKCORPDLNamaJalan.Text = objResume(SetnGetRowEdit).TransactionCashOut.CORP_NamaJalan
            txtTRXKKCORPDLRTRW.Text = objResume(SetnGetRowEdit).TransactionCashOut.CORP_RTRW
            objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKelurahan_Id)
            If Not IsNothing(objSKelurahan) Then
                txtTRXKKCORPDLKelurahan.Text = objSKelurahan.NamaKelurahan
                hfTRXKKCORPDLKelurahan.Value = objSKelurahan.IDKelurahan
            End If
            objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKecamatan_Id)
            If Not IsNothing(objSkecamatan) Then
                txtTRXKKCORPDLKecamatan.Text = objSkecamatan.NamaKecamatan
                hfTRXKKCORPDLKecamatan.Value = objSkecamatan.IDKecamatan
            End If
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKotaKab_Id)
            If Not IsNothing(objSkotaKab) Then
                txtTRXKKCORPDLKotaKab.Text = objSkotaKab.NamaKotaKab
                hfTRXKKCORPDLKotaKab.Value = objSkotaKab.IDKotaKab
            End If
            txtTRXKKCORPDLKodePos.Text = objResume(SetnGetRowEdit).TransactionCashOut.CORP_KodePos
            objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsProvince_Id)
            If Not IsNothing(objSProvinsi) Then
                txtTRXKKCORPDLProvinsi.Text = objSProvinsi.Nama
                hfTRXKKCORPDLProvinsi.Value = objSProvinsi.IdProvince
            End If
            objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsNegara_Id)
            If Not IsNothing(objSMapNegara) Then
                objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                If Not IsNothing(objSnegara) Then
                    txtTRXKKCORPDLNegara.Text = objSnegara.NamaNegara
                    hfTRXKKCORPDLNegara.Value = objSnegara.IDNegaraNCBS
                End If
            End If
            txtTRXKKCORPLNNamaJalan.Text = objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_NamaJalan
            objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsNegara_Id)
            If Not IsNothing(objSMapNegara) Then
                objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                If Not IsNothing(objSnegara) Then
                    txtTRXKKCORPLNNegara.Text = objSnegara.NamaNegara
                    hfTRXKKCORPLNNegara.Value = objSnegara.IDNegaraNCBS
                End If
            End If
            objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsProvince_Id)
            If Not IsNothing(objSProvinsi) Then
                txtTRXKKCORPLNProvinsi.Text = objSProvinsi.Nama
                hfTRXKKCORPLNProvinsi.Value = objSProvinsi.IdProvince
            End If
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_MsKotaKab_Id)
            If Not IsNothing(objSkotaKab) Then
                txtTRXKKCORPLNKota.Text = objSkotaKab.NamaKotaKab
                hfTRXKKCORPLNKota.Value = objSkotaKab.IDKotaKab
            End If
            txtTRXKKCORPLNKodePos.Text = objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_KodePos
            txtTRXKKCORPNPWP.Text = objResume(SetnGetRowEdit).TransactionCashOut.CORP_NPWP
            txtTRXKKCORPTujuanTrx.Text = objResume(SetnGetRowEdit).TransactionCashOut.CORP_TujuanTransaksi
            txtTRXKKCORPSumberDana.Text = objResume(SetnGetRowEdit).TransactionCashOut.CORP_SumberDana
            txtTRXKKCORPNamaBankLain.Text = objResume(SetnGetRowEdit).TransactionCashOut.CORP_NamaBankLain
            txtTRXKKCORPNoRekeningTujuan.Text = objResume(SetnGetRowEdit).TransactionCashOut.CORP_NomorRekeningTujuan


        End If
        imgAdd.Visible = False
        imgClearAll.Visible = False
        imgSaveEdit.Visible = True
        imgCancelEdit.Visible = True


    End Sub

    Protected Sub imgCancelEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCancelEdit.Click
        imgAdd.Visible = True
        imgClearAll.Visible = True
        imgSaveEdit.Visible = False
        imgCancelEdit.Visible = False
        SetnGetRowEdit = -1
        clearKasMasukKasKeluar()
    End Sub

    Protected Sub imgSaveEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSaveEdit.Click
        Try
            If isValidAddKas() Then
                Dim objColKas As List(Of ResumeKasMasukKeluar) = SetnGetResumeKasMasukKasKeluar
                If lblTRXKMDetilValutaAsingJumlahRp.Text <> vbNullString OrElse lblTRXKMDetilValutaAsingJumlahRp.Text <> "" Then

                    'Insert untuk tampilan Grid
                    objColKas(SetnGetRowEdit).TransactionDate = txtTRXKMTanggalTrx.Text
                    objColKas(SetnGetRowEdit).Branch = txtTRXKMNamaKantor.Text
                    objColKas(SetnGetRowEdit).TransactionNominal = lblTRXKMDetilValutaAsingJumlahRp.Text
                    objColKas(SetnGetRowEdit).AccountNumber = txtTRXKMNoRekening.Text
                    objColKas(SetnGetRowEdit).Kas = "Kas Masuk"
                    objColKas(SetnGetRowEdit).Type = "LTKT"

                    'Insert Data Ke ObjectTransaction
                    objColKas(SetnGetRowEdit).TransactionCashIn = New LTKTTransactionCashIn
                    objColKas(SetnGetRowEdit).TransactionCashIn.TanggalTransaksi = txtTRXKMTanggalTrx.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.NamaKantorPJK = txtTRXKMNamaKantor.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.FK_MsKotaKab_Id = CInt(hfTRXKMKotaKab.Value)
                    objColKas(SetnGetRowEdit).TransactionCashIn.FK_MsProvince_Id = CInt(hfTRXKMProvinsi.Value)
                    objColKas(SetnGetRowEdit).TransactionCashIn.NomorRekening = txtTRXKMNoRekening.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_Gelar = txtTRXKMINDVGelar.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NamaLengkap = txtTRXKMINDVNamaLengkap.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_TempatLahir = txtTRXKMINDVTempatLahir.Text
                    If txtTRXKMINDVTanggalLahir.Text <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_TanggalLahir = txtTRXKMINDVTanggalLahir.Text
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan = rblTRXKMINDVKewarganegaraan.SelectedIndex.ToString
                    If cboTRXKMINDVNegara.SelectedIndex <> 0 Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsNegara_Id = CInt(cboTRXKMINDVNegara.SelectedValue)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_DOM_NamaJalan = txtTRXKMINDVDOMNamaJalan.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_DOM_RTRW = txtTRXKMINDVDOMRTRW.Text
                    If hfTRXKMINDVDOMKelurahan.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id = CInt(hfTRXKMINDVDOMKelurahan.Value)
                    End If
                    If hfTRXKMINDVDOMKecamatan.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id = CInt(hfTRXKMINDVDOMKecamatan.Value)
                    End If
                    If hfTRXKMINDVDOMKotaKab.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id = CInt(hfTRXKMINDVDOMKotaKab.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_DOM_KodePos = txtTRXKMINDVDOMKodePos.Text
                    If hfTRXKMINDVDOMProvinsi.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsProvince_Id = CInt(hfTRXKMINDVDOMProvinsi.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_NamaJalan = txtTRXKMINDVIDNamaJalan.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_RTRW = txtTRXKMINDVIDRTRW.Text
                    If hfTRXKMINDVIDKelurahan.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKelurahan_Id = CInt(hfTRXKMINDVIDKelurahan.Value)
                    End If
                    If hfTRXKMINDVIDKecamatan.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKecamatan_Id = CInt(hfTRXKMINDVIDKecamatan.Value)
                    End If
                    If hfTRXKMINDVIDKotaKab.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKotaKab_Id = CInt(hfTRXKMINDVIDKotaKab.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_KodePos = txtTRXKMINDVIDKodePos.Text
                    If hfTRXKMINDVIDProvinsi.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsProvince_Id = CInt(hfTRXKMINDVIDProvinsi.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NA_NamaJalan = txtTRXKMINDVNANamaJalan.Text
                    If hfTRXKMINDVNANegara.Value <> "" Then
                        Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegaraNCBS.ToString & " like '%" & hfTRXKMINDVNANegara.Value.ToString & "%' ", "", 0, Integer.MaxValue, 0)
                        If objMapNegara.Count > 0 Then
                            objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsNegara_Id = objMapNegara(0).IDNegara
                        End If
                    End If
                    If hfTRXKMINDVNAProvinsi.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsProvince_Id = CInt(hfTRXKMINDVNAProvinsi.Value)
                    End If
                    If hfTRXKMINDVNAKota.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsKotaKab_Id = CInt(hfTRXKMINDVNAKota.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NA_KodePos = txtTRXKMINDVNAKodePos.Text
                    If cboTRXKMINDVJenisID.SelectedIndex <> 0 Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsIDType_Id = CInt(cboTRXKMINDVJenisID.SelectedValue)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NomorId = txtTRXKMINDVNomorID.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NPWP = txtTRXKMINDVNPWP.Text
                    If hfTRXKMINDVPekerjaan.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsPekerjaan_Id = CInt(hfTRXKMINDVPekerjaan.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_Jabatan = txtTRXKMINDVJabatan.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_PenghasilanRataRata = txtTRXKMINDVPenghasilanRataRata.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_TempatBekerja = txtTRXKMINDVTempatKerja.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_TujuanTransaksi = txtTRXKMINDVTujuanTrx.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_SumberDana = txtTRXKMINDVSumberDana.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NamaBankLain = txtTRXKMINDVNamaBankLain.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NomorRekeningTujuan = txtTRXKMINDVNoRekeningTujuan.Text

                    If cboTRXKMCORPBentukBadanUsaha.SelectedIndex <> 0 Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id = CInt(cboTRXKMCORPBentukBadanUsaha.SelectedValue)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashIn.CORP_Nama = txtTRXKMCORPNama.Text
                    If hfTRXKMCORPBidangUsaha.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBidangUsaha_Id = hfTRXKMCORPBidangUsaha.Value
                    End If
                    If rblTRXKMCORPTipeAlamat.SelectedIndex <> -1 Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.CORP_TipeAlamat = rblTRXKMCORPTipeAlamat.SelectedIndex
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashIn.CORP_NamaJalan = txtTRXKMCORPDLNamaJalan.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.CORP_RTRW = txtTRXKMCORPDLRTRW.Text
                    If hfTRXKMCORPDLKelurahan.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKelurahan_Id = CInt(hfTRXKMCORPDLKelurahan.Value)
                    End If
                    If hfTRXKMCORPDLKecamatan.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKecamatan_Id = CInt(hfTRXKMCORPDLKecamatan.Value)
                    End If
                    If hfTRXKMCORPDLKotaKab.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKotaKab_Id = CInt(hfTRXKMCORPDLKotaKab.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashIn.CORP_KodePos = txtTRXKMCORPDLKodePos.Text
                    If hfTRXKMCORPDLProvinsi.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsProvince_Id = CInt(hfTRXKMCORPDLProvinsi.Value)
                    End If
                    If hfTRXKMCORPDLNegara.Value <> "" Then
                        Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegaraNCBS.ToString & " like '%" & hfTRXKMCORPDLNegara.Value.ToString & "%' ", "", 0, Integer.MaxValue, 0)
                        If objMapNegara.Count > 0 Then
                            objColKas(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsNegara_Id = objMapNegara(0).IDNegara
                        End If
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashIn.CORP_LN_NamaJalan = txtTRXKMCORPLNNamaJalan.Text
                    If hfTRXKMCORPLNNegara.Value <> "" Then
                        Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegaraNCBS.ToString & " like '%" & hfTRXKMCORPLNNegara.Value.ToString & "%' ", "", 0, Integer.MaxValue, 0)
                        If objMapNegara.Count > 0 Then
                            objColKas(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsNegara_Id = objMapNegara(0).IDNegara
                        End If
                    End If
                    If hfTRXKMCORPLNProvinsi.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsProvince_Id = CInt(hfTRXKMCORPLNProvinsi.Value)
                    End If
                    If hfTRXKMCORPLNKota.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.CORP_LN_MsKotaKab_Id = CInt(hfTRXKMCORPLNKota.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashIn.CORP_LN_KodePos = txtTRXKMCORPLNKodePos.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.CORP_NPWP = txtTRXKMCORPNPWP.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.CORP_TujuanTransaksi = txtTRXKMCORPTujuanTrx.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.CORP_SumberDana = txtTRXKMCORPSumberDana.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.CORP_NamaBankLain = txtTRXKMCORPNamaBankLain.Text
                    objColKas(SetnGetRowEdit).TransactionCashIn.CORP_NomorRekeningTujuan = txtTRXKMCORPNoRekeningTujuan.Text

                    'Insert Data Ke Detail Transaction
                    objColKas(SetnGetRowEdit).DetailTransactionCashIn = New List(Of LTKTDetailCashInTransaction)
                    Dim DetilCol As List(Of LTKTDetailCashInEdit) = SetnGetgrvTRXKMDetilValutaAsing

                    'hapus detil
                    If objColKas(SetnGetRowEdit).DetailTransactionCashIn.Count > 0 Then
                        For Each singleObj As LTKTDetailCashInTransaction In objColKas(SetnGetRowEdit).DetailTransactionCashIn
                            objColKas(SetnGetRowEdit).DetailTransactionCashIn.Remove(singleObj)
                        Next
                    End If

                    'add ulang detil
                    If DetilCol.Count > 0 Then

                        For Each singleObj As LTKTDetailCashInEdit In DetilCol
                            Dim objDetilKasMasuk As New LTKTDetailCashInTransaction
                            If txtTRXKMDetilKasMasuk.Text <> "" Then
                                objDetilKasMasuk.KasMasuk = txtTRXKMDetilKasMasuk.Text
                            End If
                            Dim matauangKM As String = singleObj.MataUang.Substring(0, singleObj.MataUang.IndexOf("-")).Trim
                            Dim objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & " like '%" & matauangKM.ToString & "%'", "", 0, Integer.MaxValue, 0)
                            If objCurrency.Count > 0 Then
                                objDetilKasMasuk.Asing_FK_MsCurrency_Id = objCurrency(0).IdCurrency
                            End If
                            objDetilKasMasuk.Asing_KursTransaksi = singleObj.KursTransaksi
                            objDetilKasMasuk.Asing_TotalKasMasukDalamRupiah = singleObj.JumlahRp
                            objDetilKasMasuk.TotalKasMasuk = lblTRXKMDetilValutaAsingJumlahRp.Text

                            'Masukin ke detail
                            objColKas(SetnGetRowEdit).DetailTransactionCashIn.Add(objDetilKasMasuk)
                        Next
                    Else
                        Dim objDetilKasMasuk As New LTKTDetailCashInTransaction
                        objDetilKasMasuk.KasMasuk = txtTRXKMDetilKasMasuk.Text
                        objDetilKasMasuk.TotalKasMasuk = lblTRXKMDetilValutaAsingJumlahRp.Text
                        objColKas(SetnGetRowEdit).DetailTransactionCashIn.Add(objDetilKasMasuk)
                    End If



                End If
                If lblDetilKasKeluarJumlahRp.Text <> vbNullString OrElse lblDetilKasKeluarJumlahRp.Text <> "" Then

                    'Insert untuk tampilan Grid
                    objColKas(SetnGetRowEdit).TransactionDate = txtTRXKKTanggalTransaksi.Text
                    objColKas(SetnGetRowEdit).Branch = txtTRXKKNamaKantor.Text
                    objColKas(SetnGetRowEdit).TransactionNominal = lblDetilKasKeluarJumlahRp.Text
                    objColKas(SetnGetRowEdit).AccountNumber = txtTRXKKRekeningKK.Text
                    objColKas(SetnGetRowEdit).Kas = "Kas Keluar"
                    objColKas(SetnGetRowEdit).Type = "LTKT"

                    'Insert Data Ke ObjectTransction
                    objColKas(SetnGetRowEdit).TransactionCashOut = New LTKTTransactionCashOut
                    objColKas(SetnGetRowEdit).TransactionCashOut.TanggalTransaksi = txtTRXKKTanggalTransaksi.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.NamaKantorPJK = txtTRXKKNamaKantor.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.FK_MsKotaKab_Id = CInt(hfTRXKKKotaKab.Value)
                    objColKas(SetnGetRowEdit).TransactionCashOut.FK_MsProvince_Id = CInt(hfTRXKKProvinsi.Value)
                    objColKas(SetnGetRowEdit).TransactionCashOut.NomorRekening = txtTRXKKRekeningKK.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_Gelar = txtTRXKKINDVGelar.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NamaLengkap = txtTRXKKINDVNamaLengkap.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_TempatLahir = txtTRXKKINDVTempatLahir.Text
                    If txtTRXKKINDVTglLahir.Text <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_TanggalLahir = txtTRXKKINDVTglLahir.Text
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan = rblTRXKKINDVKewarganegaraan.SelectedIndex.ToString
                    If cboTRXKKINDVNegara.SelectedIndex <> 0 Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsNegara_Id = CInt(cboTRXKKINDVNegara.SelectedValue)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_DOM_NamaJalan = txtTRXKKINDVDOMNamaJalan.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_DOM_RTRW = txtTRXKKINDVDOMRTRW.Text
                    If hfTRXKKINDVDOMKelurahan.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id = CInt(hfTRXKKINDVDOMKelurahan.Value)
                    End If
                    If hfTRXKKINDVDOMKecamatan.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id = CInt(hfTRXKKINDVDOMKecamatan.Value)
                    End If
                    If hfTRXKKINDVDOMKotaKab.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id = CInt(hfTRXKKINDVDOMKotaKab.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_DOM_KodePos = txtTRXKKINDVDOMKodePos.Text
                    If hfTRXKKINDVDOMProvinsi.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsProvince_Id = CInt(hfTRXKKINDVDOMProvinsi.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_NamaJalan = txtTRXKKINDVIDNamaJalan.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_RTRW = txtTRXKKINDVIDRTRW.Text
                    If hfTRXKKINDVIDKelurahan.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKelurahan_Id = CInt(hfTRXKKINDVIDKelurahan.Value)
                    End If
                    If hfTRXKKINDVIDKecamatan.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKecamatan_Id = CInt(hfTRXKKINDVIDKecamatan.Value)
                    End If
                    If hfTRXKKINDVIDKotaKab.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKotaKab_Id = CInt(hfTRXKKINDVIDKotaKab.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_KodePos = txtTRXKKINDVIDKodePos.Text
                    If hfTRXKKINDVIDProvinsi.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsProvince_Id = CInt(hfTRXKKINDVIDProvinsi.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NA_NamaJalan = txtTRXKKINDVNANamaJalan.Text
                    If hfTRXKKINDVNANegara.Value <> "" Then
                        Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegaraNCBS.ToString & " like '%" & hfTRXKMINDVNANegara.Value.ToString & "%' ", "", 0, Integer.MaxValue, 0)
                        If objMapNegara.Count > 0 Then
                            objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsNegara_Id = objMapNegara(0).IDNegara
                        End If
                    End If
                    If hfTRXKKINDVNAProvinsi.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsProvince_Id = CInt(hfTRXKKINDVNAProvinsi.Value)
                    End If
                    If hfTRXKKINDVNAKota.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsKotaKab_Id = CInt(hfTRXKKINDVNAKota.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NA_KodePos = txtTRXKKINDVNAKodePos.Text
                    If cboTRXKKINDVJenisID.SelectedIndex <> 0 Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsIDType_Id = CInt(cboTRXKKINDVJenisID.SelectedValue)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NomorId = txtTRXKKINDVNomorId.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NPWP = txtTRXKKINDVNPWP.Text
                    If hfTRXKKINDVPekerjaan.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsPekerjaan_Id = CInt(hfTRXKKINDVPekerjaan.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_Jabatan = txtTRXKKINDVJabatan.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_PenghasilanRataRata = txtTRXKKINDVPenghasilanRataRata.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_TempatBekerja = txtTRXKKINDVTempatKerja.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_TujuanTransaksi = txtTRXKKINDVTujuanTrx.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_SumberDana = txtTRXKKINDVSumberDana.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NamaBankLain = txtTRXKKINDVNamaBankLain.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NomorRekeningTujuan = txtTRXKKINDVNoRekTujuan.Text


                    If cboTRXKKCORPBentukBadanUsaha.SelectedIndex <> 0 Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id = CInt(cboTRXKKCORPBentukBadanUsaha.SelectedValue)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashOut.CORP_Nama = txtTRXKKCORPNama.Text
                    If hfTRXKKCORPBidangUsaha.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBidangUsaha_Id = hfTRXKKCORPBidangUsaha.Value
                    End If
                    If rblTRXKKCORPTipeAlamat.SelectedIndex <> -1 Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat = rblTRXKKCORPTipeAlamat.SelectedIndex
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashOut.CORP_NamaJalan = txtTRXKKCORPDLNamaJalan.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.CORP_RTRW = txtTRXKKCORPDLRTRW.Text
                    If hfTRXKKCORPDLKelurahan.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKelurahan_Id = CInt(hfTRXKKCORPDLKelurahan.Value)
                    End If
                    If hfTRXKKCORPDLKecamatan.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKecamatan_Id = CInt(hfTRXKKCORPDLKecamatan.Value)
                    End If
                    If hfTRXKKCORPDLKotaKab.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKotaKab_Id = CInt(hfTRXKKCORPDLKotaKab.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashOut.CORP_KodePos = txtTRXKKCORPDLKodePos.Text
                    If hfTRXKKCORPDLProvinsi.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsProvince_Id = CInt(hfTRXKKCORPDLProvinsi.Value)
                    End If
                    If hfTRXKKCORPDLNegara.Value <> "" Then
                        Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegaraNCBS.ToString & " like '%" & hfTRXKKCORPDLNegara.Value.ToString & "%' ", "", 0, Integer.MaxValue, 0)
                        If objMapNegara.Count > 0 Then
                            objColKas(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsNegara_Id = objMapNegara(0).IDNegara
                        End If
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashOut.CORP_LN_NamaJalan = txtTRXKKCORPLNNamaJalan.Text
                    If hfTRXKKCORPLNNegara.Value <> "" Then
                        Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegaraNCBS.ToString & " like '%" & hfTRXKKCORPLNNegara.Value.ToString & "%' ", "", 0, Integer.MaxValue, 0)
                        If objMapNegara.Count > 0 Then
                            objColKas(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsNegara_Id = objMapNegara(0).IDNegara
                        End If
                    End If
                    If hfTRXKKCORPLNProvinsi.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsProvince_Id = CInt(hfTRXKKCORPLNProvinsi.Value)
                    End If
                    If hfTRXKKCORPLNKota.Value <> "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.CORP_LN_MsKotaKab_Id = CInt(hfTRXKKCORPLNKota.Value)
                    End If
                    objColKas(SetnGetRowEdit).TransactionCashOut.CORP_LN_KodePos = txtTRXKKCORPLNKodePos.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.CORP_NPWP = txtTRXKKCORPNPWP.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.CORP_TujuanTransaksi = txtTRXKKCORPTujuanTrx.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.CORP_SumberDana = txtTRXKKCORPSumberDana.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.CORP_NamaBankLain = txtTRXKKCORPNamaBankLain.Text
                    objColKas(SetnGetRowEdit).TransactionCashOut.CORP_NomorRekeningTujuan = txtTRXKKCORPNoRekeningTujuan.Text


                    'Insert Date Ke DetailTransaction
                    objColKas(SetnGetRowEdit).DetailTranscationCashOut = New List(Of LTKTDetailCashOutTransaction)
                    Dim DetilCol As List(Of LTKTDetailCashInEdit) = SetnGetgrvDetilKasKeluar

                    'hapus detil
                    If objColKas(SetnGetRowEdit).DetailTranscationCashOut.Count > 0 Then
                        For Each singleObj As LTKTDetailCashOutTransaction In objColKas(SetnGetRowEdit).DetailTranscationCashOut
                            objColKas(SetnGetRowEdit).DetailTranscationCashOut.Remove(singleObj)
                        Next
                    End If

                    'insert ulang detil
                    If DetilCol.Count > 0 Then
                        For Each singleObj As LTKTDetailCashInEdit In DetilCol
                            Dim objDetilKasKeluar As New LTKTDetailCashOutTransaction
                            If txtTRXKKDetailKasKeluar.Text <> "" Then
                                objDetilKasKeluar.KasKeluar = txtTRXKKDetailKasKeluar.Text
                            End If
                            Dim matauangKK As String = singleObj.MataUang.Substring(0, singleObj.MataUang.IndexOf("-")).Trim
                            Dim objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & " like '%" & matauangKK.ToString & "%'", "", 0, Integer.MaxValue, 0)
                            If objCurrency.Count > 0 Then
                                objDetilKasKeluar.Asing_FK_MsCurrency_Id = objCurrency(0).IdCurrency
                            End If
                            objDetilKasKeluar.Asing_FK_MsCurrency_Id = objCurrency(0).IdCurrency
                            objDetilKasKeluar.Asing_KursTransaksi = singleObj.KursTransaksi
                            objDetilKasKeluar.Asing_TotalKasKeluarDalamRupiah = singleObj.JumlahRp
                            objDetilKasKeluar.TotalKasKeluar = lblDetilKasKeluarJumlahRp.Text

                            'Masukin ke detail
                            objColKas(SetnGetRowEdit).DetailTranscationCashOut.Add(objDetilKasKeluar)
                        Next
                    Else
                        Dim objDetilKasKeluar As New LTKTDetailCashOutTransaction
                        objDetilKasKeluar.TotalKasKeluar = lblDetilKasKeluarJumlahRp.Text
                        objDetilKasKeluar.KasKeluar = txtTRXKKDetailKasKeluar.Text
                        objColKas(SetnGetRowEdit).DetailTranscationCashOut.Add(objDetilKasKeluar)
                    End If



                End If

                Dim totalKasMasuk As Integer = 0
                Dim totalKasKeluar As Integer = 0
                For Each kas As ResumeKasMasukKeluar In objColKas
                    If kas.Kas = "Kas Masuk" Then
                        totalKasMasuk += CInt(kas.TransactionNominal)
                    Else
                        totalKasKeluar += CInt(kas.TransactionNominal)
                    End If
                Next

                lblTotalKasMasuk.Text = totalKasMasuk.ToString
                lblTotalKasKeluar.Text = totalKasKeluar.ToString

                grvTransaksi.DataSource = objColKas
                grvTransaksi.DataBind()

                clearKasMasukKasKeluar()
                imgAdd.Visible = True
                imgClearAll.Visible = True
                imgSaveEdit.Visible = False
                imgCancelEdit.Visible = False
            End If


        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub

    Protected Sub imgOKMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOKMsg.Click
        Response.Redirect("LTKTView.aspx")
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("LTKTView.aspx")
    End Sub
End Class

#Region "Class LTKTDetailCashIn"
Public Class LTKTDetailCashInEdit

    Private _mataUang As String
    Public Property MataUang() As String
        Get
            Return _mataUang
        End Get
        Set(ByVal value As String)
            _mataUang = value
        End Set
    End Property

    Private _kursTransaksi As String
    Public Property KursTransaksi() As String
        Get
            Return _kursTransaksi
        End Get
        Set(ByVal value As String)
            _kursTransaksi = value
        End Set
    End Property

    Private _jumlah As String
    Public Property Jumlah() As String
        Get
            Return _jumlah
        End Get
        Set(ByVal value As String)
            _jumlah = value
        End Set
    End Property

    Private _jumlahRp As String
    Public Property JumlahRp() As String
        Get
            Return _jumlahRp
        End Get
        Set(ByVal value As String)
            _jumlahRp = value
        End Set
    End Property

End Class
#End Region

Public Class ResumeKasMasukKeluar

    Private _kas As String
    Public Property Kas() As String
        Get
            Return _kas
        End Get
        Set(ByVal value As String)
            _kas = value
        End Set
    End Property

    Private _transactionDate As String
    Public Property TransactionDate() As String
        Get
            Return _transactionDate
        End Get
        Set(ByVal value As String)
            _transactionDate = value
        End Set
    End Property

    Private _branch As String
    Public Property Branch() As String
        Get
            Return _branch
        End Get
        Set(ByVal value As String)
            _branch = value
        End Set
    End Property

    Private _accountNumber As String
    Public Property AccountNumber() As String
        Get
            Return _accountNumber
        End Get
        Set(ByVal value As String)
            _accountNumber = value
        End Set
    End Property

    Private _Type As String
    Public Property Type() As String
        Get
            Return _Type
        End Get
        Set(ByVal value As String)
            _Type = value
        End Set
    End Property

    Private _transactionNominal As String
    Public Property TransactionNominal() As String
        Get
            Return _transactionNominal
        End Get
        Set(ByVal value As String)
            _transactionNominal = value
        End Set
    End Property


    Private _transactionCashIn As LTKTTransactionCashIn
    Public Property TransactionCashIn() As LTKTTransactionCashIn
        Get
            Return _transactionCashIn
        End Get
        Set(ByVal value As LTKTTransactionCashIn)
            _transactionCashIn = value
        End Set
    End Property


    Private _transactionCashOut As LTKTTransactionCashOut
    Public Property TransactionCashOut() As LTKTTransactionCashOut
        Get
            Return _transactionCashOut
        End Get
        Set(ByVal value As LTKTTransactionCashOut)
            _transactionCashOut = value
        End Set
    End Property


    Private _detailTransactionCashIn As List(Of LTKTDetailCashInTransaction)
    Public Property DetailTransactionCashIn() As List(Of LTKTDetailCashInTransaction)
        Get
            Return _detailTransactionCashIn
        End Get
        Set(ByVal value As List(Of LTKTDetailCashInTransaction))
            _detailTransactionCashIn = value
        End Set
    End Property


    Private _detailTransactionCashOut As List(Of LTKTDetailCashOutTransaction)
    Public Property DetailTranscationCashOut() As List(Of LTKTDetailCashOutTransaction)
        Get
            Return _detailTransactionCashOut
        End Get
        Set(ByVal value As List(Of LTKTDetailCashOutTransaction))
            _detailTransactionCashOut = value
        End Set
    End Property



End Class

