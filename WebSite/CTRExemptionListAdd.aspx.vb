Imports System.Data
Imports System.Data.SqlClient
Imports AML2015Nettier.Data
Imports AML2015Nettier.Entities
Partial Class CTRExemptionListAdd
    Inherits Parent

    Private Sub ClearThisPageSessions()
        Session("CTRExemptionListAccountNo") = Nothing
        Session("CTRExemptionListAccountDataFound") = Nothing
    End Sub

    '    Dim AccountNumber As String
    Private Property GetAccountNumber() As String
        Get
            Return Session("CTRExemptionListAccountNo")
        End Get
        Set(ByVal value As String)
            Session("CTRExemptionListAccountNo") = value
        End Set
    End Property

    Sub LoadExemptiontype()
        CboExemptionType.Items.Clear()
        CboExemptionType.AppendDataBoundItems = True
        CboExemptionType.Items.Add(New ListItem("---", ""))
        CboExemptionType.DataSource = AMLBLL.CTRExemptionListBLL.GetTlistCTRExemptionType("", "", 0, Integer.MaxValue, 0)
        CboExemptionType.DataTextField = CTRExemptionTypeColumn.CtrExemptionTypeName.ToString
        CboExemptionType.DataValueField = CTRExemptionTypeColumn.PK_CTRExemptionType_ID.ToString
        CboExemptionType.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                LoadExemptiontype()

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "CTRExemptionListView.aspx"
        Me.Response.Redirect("CTRExemptionListView.aspx", False)
    End Sub

    Private Sub InsertGroupBySU(ByRef trans2 As SqlTransaction)
        Try
            Dim StrDescription As String
            If Me.TxtDescription.Text.Length <= 255 Then
                StrDescription = Me.TxtDescription.Text
            Else
                StrDescription = Me.TxtDescription.Text.Substring(0, 255)
            End If
            Using AccessGroup As New AMLDAL.CTRExemptionListTableAdapters.CTRExemptionListTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessGroup, trans2)
                AccessGroup.Insert(Me.GetAccountNumber, TextName.Text, _
                TxtDescription.Text, Now, Sahassa.AML.Commonly.SessionUserId)
            End Using
        Catch
            Throw
        End Try
    End Sub

    Private Sub InsertAuditTrail(ByRef trans2 As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
            Dim StrDescription As String
            If Me.TxtDescription.Text.Length <= 255 Then
                StrDescription = Me.TxtDescription.Text
            Else
                StrDescription = Me.TxtDescription.Text.Substring(0, 255)
            End If
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, trans2)
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionListAdd", "AccountNo", "Add", "", Me.GetAccountNumber, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionListAdd", "Description", "Add", "", StrDescription, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionListAdd", "CreatedDate", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CTRExemptionListAdd", "AddedBy", "Add", "", Sahassa.AML.Commonly.SessionUserId, "Accepted")
            End Using
        Catch
            Throw
        End Try
    End Sub


    Enum Mode
        Add = 1        
        Edit
        Delete
        Activation
    End Enum

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        'Page.Validate()
        'If Page.IsValid Then
        '    Try
        '        If Session("CTRExemptionListAccountDataFound") Is Nothing Then
        '            Throw New Exception("Cannot add because data not found.")
        '        Else
        '            If Not IsNumeric(Me.TextSearch.Text) Then Throw New Exception("Account Number must be numeric")
        '            'Using AccessTable As New AMLDAL.CTRExemptionListTableAdapters.QueriesTableAdapter
        '            '    If Me.TextName.Text <> AccessTable.GetAccountOwnerName(Me.TextSearch.Text) Then Throw New Exception("Account Number does not match with Account owner name")
        '            'End Using
        '            Me.SaveCTRExemptionList()
        '        End If
        '    Catch ex As Exception
        '        Me.cvalPageError.IsValid = False
        '        Me.cvalPageError.ErrorMessage = ex.Message
        '    End Try
        'End If


        Try
            ImageButtonSearch_Click(Nothing, Nothing)
            If IsDataValid Then
                'jika superuser siman ke db
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then

                    Using objCtrExemptionList As New CTRExemptionList
                        With objCtrExemptionList
                            .FK_CTRExemptionType_ID = CboExemptionType.SelectedValue
                            If CboExemptionType.SelectedValue = Exemptiontype.CIF Then
                                .CIFNo = TextSearch.Text
                                .AccountNo = ""
                            Else
                                .CIFNo = ""
                                .AccountNo = TextSearch.Text

                            End If
                            .CustomerName = TextName.Text
                            .Description = TxtDescription.Text
                            .CreatedDate = Now
                            .AddedBy = Sahassa.AML.Commonly.SessionUserId
                        End With
                        AMLBLL.CTRExemptionListBLL.SaveCtrExemptionList(objCtrExemptionList)
                    End Using
                    Sahassa.AML.Commonly.SessionIntendedPage = "CTRExemptionListView.aspx"
                    Me.Response.Redirect("CTRExemptionListView.aspx", False)
                Else
                    'jika bukan simpan ke approval
                    Using objCTRExemptionList_PendingApproval As New CTRExemptionList_PendingApproval
                        With objCTRExemptionList_PendingApproval

                            If CboExemptionType.SelectedValue = Exemptiontype.CIF Then
                                .CTRPendingApproval_CIFNo = TextSearch.Text
                                .CTRPendingApproval_AccountNo = ""
                            Else
                                .CTRPendingApproval_CIFNo = ""
                                .CTRPendingApproval_AccountNo = TextSearch.Text

                            End If

                            .CTRPendingApproval_CreatedDate = Now
                            .CTRPendingApproval_ModeID = Mode.Add
                            .CTRPendingApproval_Category = "Add"
                            .CTRPendingApproval_CreatedBy = Sahassa.AML.Commonly.SessionUserId
                        End With



                        Using objCTRExemptionList_Approval As New CTRExemptionList_Approval
                            With objCTRExemptionList_Approval

                                '.CTRPendingApproval_ID = item.CTRPendingApproval_ID
                                .ModeID = Mode.Add
                                .FK_CTRExemptionType_ID = CboExemptionType.SelectedValue
                                If CboExemptionType.SelectedValue = Exemptiontype.CIF Then
                                    
                                    .CIFNo = TextSearch.Text
                                    .AccountNo = ""
                                Else
                                    
                                    .CIFNo = ""
                                    .AccountNo = TextSearch.Text

                                End If

                                .CustomerName = TextName.Text
                                .[Description] = TxtDescription.Text.Trim
                                .CreatedDate = Now
                                .AddedBy = Sahassa.AML.Commonly.SessionUserId
                            End With





                            AMLBLL.CTRExemptionListBLL.SaveCtrExemptionListPendingApproval(objCTRExemptionList_PendingApproval, objCTRExemptionList_Approval, 1)
                        End Using
                    End Using

                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=81301&Identifier=" & Me.TextSearch.Text
                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=81301&Identifier=" & Me.TextSearch.Text, False)

                End If
                
            End If


        Catch ex As Exception
            Me.LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub




    Function IsDataValid() As Boolean
        'cek di CTRExemptionList ada atau ngak
        'cek di CTRExemptionList_Approval ada atau ngak
        Dim strErrorMessage As New StringBuilder

        If TextSearch.Text.Trim = "" Then
            strErrorMessage.Append("Please Add CIf or Account No.</br>")
        End If

        If CboExemptionType.SelectedValue = "" Then
            strErrorMessage.Append("Please Select CtrExemption Type.</br>")
        End If
        If CboExemptionType.SelectedValue <> "" Then
            If AMLBLL.CTRExemptionListBLL.IsExistinctrexemptionList(CboExemptionType.SelectedValue, TextSearch.Text) Then
                strErrorMessage.Append(Lblcifaccount.Text & "  already exist in CTR Exemption List.Please add another.</br>")
            End If
            If AMLBLL.CTRExemptionListBLL.IsExistinctrexemptionListApproval(CboExemptionType.SelectedValue, TextSearch.Text) Then
                strErrorMessage.Append(Lblcifaccount.Text & " already exist in CTR Exemption List Approval and waiting confirmation.</br>")
            End If
        End If
        If Session("CTRExemptionListAccountDataFound") Is Nothing Then
            strErrorMessage.Append("Please Select CIF or Account No.</br>")
        End If


        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New Exception(strErrorMessage.ToString)
        Else
            Return True
        End If
    End Function


    'Private Sub SaveCTRExemptionList()
    '    Dim Trans As SqlTransaction = Nothing
    '    Try

    '        'Periksa apakah Account tersebut sudah ada dalam tabel CTRExemptionList atau belum
    '        Using AccessCTR As New AMLDAL.CTRExemptionListTableAdapters.CTRExemptionListTableAdapter
    '            Dim Counter As Integer = AccessCTR.GetCTRExemptionCount(Me.GetAccountNumber)

    '            'Counter = 0 berarti Account Number tersebut belum pernah ada dalam tabel CTRExemptionList
    '            If Counter = 0 Then
    '                'Periksa apakah Account Number tersebut sudah ada dalam tabel CTR_approval atau belum
    '                Using AccessCTRApproval As New AMLDAL.CTRExemptionListTableAdapters.CTRApprovalTableAdapter
    '                    Using AccessCTRExemptionAdd As New AMLDAL.CTRExemptionListTableAdapters.CTRPending_SelectCommandTableAdapter

    '                        Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessCTRExemptionAdd)

    '                        Counter = AccessCTRExemptionAdd.GetCTRPendingApproval_Count(GetAccountNumber)
    '                        'Counter = 0 berarti Account Number tersebut statusnya tidak dalam pending approval dan boleh ditambahkan dlm tabel CTR_Approval
    '                        If Counter = 0 Then
    '                            If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
    '                                Me.InsertGroupBySU(Trans)
    '                                Me.InsertAuditTrail(Trans)
    '                                Trans.Commit()

    '                                Sahassa.AML.Commonly.SessionIntendedPage = "CTRExemptionListView.aspx"
    '                                Me.Response.Redirect("CTRExemptionListView.aspx", False)
    '                            Else
    '                                Dim CTRExemptionDescription As String = TxtDescription.Text
    '                                Dim AccountName As String = TextName.Text
    '                                Dim CTRAddedBy As String = Sahassa.AML.Commonly.SessionUserId
    '                                Dim CTRPending_Id As Integer

    '                                'Tambahkan ke dalam tabel Group_PendingApproval dengan ModeID = 1 (Add) 
    '                                CTRPending_Id = AccessCTRExemptionAdd.CTRExemptionList_PendingApprovalInsert(Me.GetAccountNumber, Now(), 1, "Add", CTRAddedBy)

    '                                'AccessCTRExemptionAdd.Insert(GetAccountNumber, Now, 1, "Add", CTRAddedBy)
    '                                'Using TableCTRExemption As AMLDAL.CTRExemptionList.CTRPending_SelectCommandDataTable = AccessCTRExemptionAdd.GetData
    '                                '    Dim TableRowCTRExemption As AMLDAL.CTRExemptionList.CTRPending_SelectCommandRow = TableCTRExemption.Rows(TableCTRExemption.Rows.Count - 1)
    '                                '    CTRPending_Id = TableRowCTRExemption.CTRPendingApproval_Id
    '                                'End Using

    '                                'Tambahkan ke dalam tabel Group_Approval dengan ModeID = 1 (Add) 
    '                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessCTRApproval, Trans)
    '                                AccessCTRApproval.Insert(CTRPending_Id, 1, GetAccountNumber, AccountName, CTRExemptionDescription, Now, CTRAddedBy)

    '                                Trans.Commit()
    '                                'MessagePendingID 81301 = CTR Exemption Add 
    '                                Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=81301&Identifier=" & Me.TextSearch.Text
    '                                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=81301&Identifier=" & Me.TextSearch.Text, False)
    '                            End If
    '                        Else
    '                            Throw New Exception("Cannot add the following Account: '" & GetAccountNumber & "' because it is currently waiting for approval.")
    '                        End If
    '                    End Using
    '                End Using
    '            Else
    '                Throw New Exception("Cannot add the following Account Number: '" & GetAccountNumber & "' because that Account Number already exists in the database.")
    '            End If
    '        End Using
    '    Catch ex As Exception
    '        If Not Trans Is Nothing Then
    '            Trans.Rollback()
    '        End If
    '        Me.cvalPageError.IsValid = False
    '        Me.cvalPageError.ErrorMessage = ex.Message
    '    Finally
    '        If Not Trans Is Nothing Then
    '            Trans.Dispose()
    '        End If
    '    End Try
    'End Sub

    Public Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try


            If CboExemptionType.SelectedValue = "" Then
                Throw New Exception("Please Select Ctr Exemption Type")
            End If

            If TextSearch.Text = "" Then
                Throw New Exception("Please Enter " & Lblcifaccount.Text & " No")
            End If
            Dim strnama As String = ""
            If CboExemptionType.SelectedValue = Exemptiontype.CIF Then
                strnama = AMLBLL.CTRExemptionListBLL.ValidateCIF(TextSearch.Text.Trim)

            ElseIf CboExemptionType.SelectedValue = Exemptiontype.Account Then
                strnama = AMLBLL.CTRExemptionListBLL.ValidateAccount(TextSearch.Text.Trim)
            End If
            TextName.Text = strnama
            If strnama.Trim <> "" Then
                Session("CTRExemptionListAccountDataFound") = 1
            Else
                Session("CTRExemptionListAccountDataFound") = Nothing
                Throw New Exception("Data Cif Or Account Not Found")
            End If

            'If Not IsNumeric(Me.TextSearch.Text) Then
            '    Throw New Exception("Account Number must be numeric")
            'Else
            '    'Me.SetnGetValueSearch = Me.TextSearch.Text
            '    'Me.SetnGetCurrentPage = 0
            '    Using AccessCTRExemption As New AMLDAL.CTRExemptionListTableAdapters.CTRAccountTableAdapter
            '        Using TableCTRExemption As AMLDAL.CTRExemptionList.CTRAccountDataTable = AccessCTRExemption.GetDataByAccount(Me.TextSearch.Text)
            '            If TableCTRExemption.Rows.Count > 0 Then
            '                Dim TableRowCTRExemption As AMLDAL.CTRExemptionList.CTRAccountRow = TableCTRExemption.Rows(0)
            '                If Not TableRowCTRExemption Is Nothing Then
            '                    If Not TableRowCTRExemption.IsAccountNameNull Then
            '                        TextName.Text = TableRowCTRExemption.AccountName
            '                    End If
            '                End If

            '                Me.GetAccountNumber = TextSearch.Text
            '                Session("CTRExemptionListAccountDataFound") = 1
            '            Else
            '                TextName.Text = "Data Tidak Ditemukan"
            '            End If
            '        End Using
            '    End Using
            'End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Enum Exemptiontype
        CIF = 1
        Account
    End Enum


    Protected Sub CboExemptionType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboExemptionType.SelectedIndexChanged
        Try
            TextName.Text = ""
            TextSearch.Text = ""
            Session("CTRExemptionListAccountDataFound") = Nothing
            '1=cif
            '2=account
            If CboExemptionType.SelectedValue <> "" Then
                If CboExemptionType.SelectedValue = Exemptiontype.CIF Then
                    Lblcifaccount.Text = "CIF"
                ElseIf CboExemptionType.SelectedValue = Exemptiontype.Account Then
                    Lblcifaccount.Text = "Account No"
                Else
                    CboExemptionType.SelectedValue = ""
                End If

            End If
            
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class