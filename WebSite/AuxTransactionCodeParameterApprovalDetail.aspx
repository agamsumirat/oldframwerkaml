<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AuxTransactionCodeParameterApprovalDetail.aspx.vb" Inherits="AuxTransactionCodeParameterApprovalDetail" title="Auxiliary Transaction Code Parameter Approval Detail" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<table>           
	<tr>
		<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
		<td class="maintitle" vAlign="bottom" width="99%" bgColor="#ffffff"><asp:label id="lblTitle" runat="server">Excluded Transaction Code Parameter - Approval Detail</asp:label></td>
	</tr>
	<tr>
		<td background="images/validationbground.gif" colSpan="2"><ajax:ajaxpanel id="AjaxpanelValidSumm" runat="server" Width="688px">
            &nbsp;<asp:customvalidator id="cvalPageErr" runat="server" Display="Dynamic">
					<b></b>
				</asp:customvalidator>
				<asp:label id="lblSuccess" runat="server" CssClass="validationok" Visible="False" Width="488px"></asp:label>
			</ajax:ajaxpanel></td>
	</tr>

	<tr>
		<td bgColor="#ffffff" colSpan="2">
			<table id="Table1" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
				border="2">
				<tr class="formText">
					<td width="5" bgColor="#ffffff" height="146">&nbsp;</td>
					<TD width="220" bgColor="#ffffff" height="146">
						<P>
                            Old Value</P>
						<P><ajax:ajaxpanel id="AjaxPanelListAvailableFile" runat="server">
								<asp:listbox id="ListOldValue" tabIndex="2" runat="server" Width="360px" CssClass="searcheditcbo"
									SelectionMode="Multiple" Height="200px"></asp:listbox>
							</ajax:ajaxpanel></P>
					</TD>
					<td width="12" bgColor="#ffffff" height="146">

					</td>
					<TD width="99%" bgColor="#ffffff" height="146"><P>
                        New Value</P>
						<P><ajax:ajaxpanel id="AjaxPanelListSelectedFile" runat="server">
								<asp:listbox id="ListNewValue" tabIndex="3" runat="server" Width="360px" CssClass="searcheditcbo"
									SelectionMode="Multiple" Height="200px"></asp:listbox>
							</ajax:ajaxpanel></P>
					</TD>
				</tr>
				<tr class="formText" bgColor="#dddddd" height="20">
					<td width="15"><IMG height="15" src="images/arrow.gif" width="15"></td>
					<td colSpan="3">
						<table cellSpacing="0" cellPadding="3" border="0">
							<tr>
								<td><asp:imagebutton id="ImageButtonAccept" runat="server" ImageUrl="images/button/Accept.gif" CausesValidation="True"></asp:imagebutton></td>
								<td><asp:imagebutton id="ImageButtonReject" runat="server" ImageUrl="images/button/Reject.gif" CausesValidation="True"></asp:imagebutton></td>
								<td><asp:imagebutton id="ImageCancel" runat="server" ImageUrl="images/button/back.gif" CausesValidation="False"></asp:imagebutton></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</table>
</asp:Content>