﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class InsiderCodeDelete
    Inherits Parent


    ReadOnly Property GetPk_InsiderCode As Long
        Get
            If IsNumeric(Request.Params("InsiderCodeID")) Then
                If Not IsNothing(Session("InsiderCodeDelete.PK")) Then
                    Return CLng(Session("InsiderCodeDelete.PK"))
                Else
                    Session("InsiderCodeDelete.PK") = Request.Params("InsiderCodeID")
                    Return CLng(Session("InsiderCodeDelete.PK"))
                End If
            End If
            Return 0
        End Get
    End Property


    Sub clearSession()
        Session("InsiderCodeDelete.PK") = Nothing
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "InsiderCodeView.aspx"

            Me.Response.Redirect("InsiderCodeView.aspx", False)
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImageDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageDelete.Click
        Try




            If Sahassa.AML.Commonly.SessionGroupName.ToLower = "superuser" Then
                If InsiderCodeBLL.Delete(GetPk_InsiderCode) Then
                    lblMessage.Text = "Delete Data Success"
                    mtvInsiderCodeAdd.ActiveViewIndex = 1
                End If
            Else
                If InsiderCodeBLL.DeleteApproval(GetPk_InsiderCode) Then
                    lblMessage.Text = "Data has been inserted to Approval"
                    mtvInsiderCodeAdd.ActiveViewIndex = 1
                End If
            End If


        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub LoadData()
        Using objInsiderCode As InsiderCode = InsiderCodeBLL.getInsiderCodeByPk(GetPk_InsiderCode)
            If Not IsNothing(objInsiderCode) Then
                txtInsiderCodeName.Text = objInsiderCode.InsiderCodeName.ToString
                txtInsiderCodeID.Text = objInsiderCode.InsiderCodeId.ToString
                txtActivation.Text = objInsiderCode.Activation.GetValueOrDefault(False)
            Else
                ImageDelete.Visible = False
            End If
        End Using
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                mtvInsiderCodeAdd.ActiveViewIndex = 0
                clearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                LoadData()
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Response.Redirect("InsiderCodeView.aspx")
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class


