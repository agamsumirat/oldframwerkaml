Imports System.Data
Imports System.Data.SqlClient

Partial Class AuxTransactionCodeParameterApproval
    Inherits Parent

#Region " Property "
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("AuxTransactionCodeApprovalSelected") Is Nothing, New ArrayList, Session("AuxTransactionCodeApprovalSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("AuxTransactionCodeApprovalSelected") = value
        End Set
    End Property
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("AuxTransactionCodeApprovalFieldSearch") Is Nothing, "", Session("AuxTransactionCodeApprovalFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("AuxTransactionCodeApprovalFieldSearch") = Value
        End Set
    End Property
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("AuxTransactionCodeApprovalValueSearch") Is Nothing, "", Session("AuxTransactionCodeApprovalValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("AuxTransactionCodeApprovalValueSearch") = Value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("AuxTransactionCodeApprovalSort") Is Nothing, "Fk_AuxTransactionCodeParameter_PendingApprovalUserID asc", Session("AuxTransactionCodeApprovalSort"))
        End Get
        Set(ByVal Value As String)
            Session("AuxTransactionCodeApprovalSort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("AuxTransactionCodeApprovalCurrentPage") Is Nothing, 0, Session("AuxTransactionCodeApprovalCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("AuxTransactionCodeApprovalCurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("AuxTransactionCodeApprovalRowTotal") Is Nothing, 0, Session("AuxTransactionCodeApprovalRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("AuxTransactionCodeApprovalRowTotal") = Value
        End Set
    End Property
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("AuxTransactionCodeApprovalData") Is Nothing, New AMLDAL.AuxTransactionCodeTableAdapters.ViewTransactionPendingApprovalTableAdapter, Session("AuxTransactionCodeApprovalData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("AuxTransactionCodeApprovalData") = value
        End Set
    End Property
#End Region

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Created By", "Fk_AuxTransactionCodeParameter_PendingApprovalUserID Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Entry Date", " TransactionCodeParameter_PendingApprovalEntryDate >= '-=Search=- 00:00' AND TransactionCodeParameter_PendingApprovalEntryDate <= '-=Search=- 23:59'"))
            Me.ComboSearch.Items.Add(New ListItem("Category", "TransactionCodeParameter_PendingApprovalCategory Like '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("AuxTransactionCodeApprovalSelected") = Nothing
        Session("AuxTransactionCodeApprovalFieldSearch") = Nothing
        Session("AuxTransactionCodeApprovalValueSearch") = Nothing
        Session("AuxTransactionCodeApprovalSort") = Nothing
        Session("AuxTransactionCodeApprovalCurrentPage") = Nothing
        Session("AuxTransactionCodeApprovalRowTotal") = Nothing
        Session("AuxTransactionCodeApprovalData") = Nothing
        Session("AuxTransactionCodeApprovalApprovalID") = Nothing
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()
                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check(this, 2, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "');")
                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")

                Using AccessCTRExemptionList As New AMLDAL.AuxTransactionCodeTableAdapters.ViewTransactionPendingApprovalTableAdapter
                    Me.SetnGetBindTable = AccessCTRExemptionList.ViewTransactionPendingApproval(Sahassa.AML.Commonly.SessionUserId)
                End Using
                Me.FillSearch()
                Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Public Sub BindGrid()
        Dim Rows() As AMLDAL.AuxTransactionCode.ViewTransactionPendingApprovalRow = Me.SetnGetBindTable.Select(Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), Me.SetnGetSort)
        Me.GridMSUserView.DataSource = Rows
        Me.SetnGetRowTotal = Rows.Length
        Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridMSUserView.DataBind()
    End Sub

    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Dim TypeConfirm As Sahassa.AML.Commonly.TypeConfirm
        Dim PendingID As Long
        Try
            PendingID = e.Item.Cells(1).Text

            If e.Item.Cells(4).Text.IndexOf("Add") > -1 Then
                TypeConfirm = Sahassa.AML.Commonly.TypeConfirm.GroupAdd
            End If
            If e.Item.Cells(4).Text.IndexOf("Edit") > -1 Then
                TypeConfirm = Sahassa.AML.Commonly.TypeConfirm.GroupEdit
            End If
            If e.Item.Cells(4).Text.IndexOf("Delete") > -1 Then
                TypeConfirm = Sahassa.AML.Commonly.TypeConfirm.GroupDelete
            End If
            Session("AuxTransactionCodeApprovalApprovalID") = PendingID
            Sahassa.AML.Commonly.SessionIntendedPage = "AuxTransactionCodeParameterApprovalDetail.aspx"
            MagicAjax.AjaxCallHelper.Redirect("AuxTransactionCodeParameterApprovalDetail.aspx")
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
            ElseIf Me.ComboSearch.SelectedIndex = 2 Then
                Dim DateSearch As DateTime
                Try
                    DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
                Catch ex As ArgumentOutOfRangeException
                    Throw New Exception("Input character cannot be convert to datetime.")
                Catch ex As FormatException
                    Throw New Exception("Unknown format date")
                End Try
                Me.SetnGetValueSearch = DateSearch.ToString("dd-MMM-yyyy")
            Else
                Me.SetnGetValueSearch = Me.TextSearch.Text
            End If
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Function IsDeleted(ByVal strRiskRatingID As String) As Boolean
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.RiskRating_ApprovalTableAdapter
            Dim count As Int32 = AccessPending.CountRiskRatingApproval(strRiskRatingID)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridMSUserView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim PK As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(PK) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(PK)
        '            Else
        '                ArrTarget.Remove(PK)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(PK)
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub


    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()
        Try
            Dim Rows As New ArrayList
            For Each IdPk As Int64 In Me.SetnGetSelectedItem
                Dim rowData() As AMLDAL.AuxTransactionCode.ViewTransactionPendingApprovalRow = Me.SetnGetBindTable.Select("Pk_AuxTransactionCodeParameter_PendingApprovalID = " & IdPk)
                If rowData.Length > 0 Then
                    Rows.Add(rowData(0))
                End If
            Next
            Me.GridMSUserView.DataSource = Rows
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0, 1, 6 & 7 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(1).Visible = False
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls


    Protected Sub LinkButtonExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=AuxTransactionCodeParmaeterApproval.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim UserId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(UserId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(UserId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(UserId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub
End Class