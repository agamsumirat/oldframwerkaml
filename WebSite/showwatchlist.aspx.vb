﻿
Partial Class showwatchlist
    Inherits System.Web.UI.Page


    Public Property UID() As String
        Get
            Return Session("showwatchlist.uid")
        End Get
        Set(ByVal value As String)
            Session("showwatchlist.uid") = value
        End Set
    End Property
    Private Sub showwatchlist_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                UID = Request.Params("uid")
                If Not UID Is Nothing Then
                    LoadDataWorldcheck(UID)
                End If

            End If


        Catch ex As Exception

        End Try
    End Sub

    Sub LoadDataWorldcheck(struid As String)
        Using objdt As Data.DataTable = EKABLL.ScreeningBLL.GetWorldcheckData(struid)
            If objdt.Rows.Count > 0 Then

                LblData0.Text = objdt.Rows(0).Item(0)
                LblData1.Text = objdt.Rows(0).Item(1)
                LblData2.Text = objdt.Rows(0).Item(2)
                LblData3.Text = objdt.Rows(0).Item(3)
                LblData4.Text = objdt.Rows(0).Item(4)
                LblData5.Text = objdt.Rows(0).Item(5)
                LblData6.Text = objdt.Rows(0).Item(6)
                LblData7.Text = objdt.Rows(0).Item(7)
                LblData8.Text = objdt.Rows(0).Item(8)
                LblData9.Text = objdt.Rows(0).Item(9)
                LblData10.Text = objdt.Rows(0).Item(10)


                Dim strdob As String
                Try
                    strdob = Convert.ToDateTime(objdt.Rows(0).Item(11)).ToString("dd-MMM-yyyy")
                Catch ex As Exception
                    strdob = objdt.Rows(0).Item(11)
                End Try

                LblData11.Text = strdob
                LblData12.Text = objdt.Rows(0).Item(12)
                LblData13.Text = objdt.Rows(0).Item(13)
                LblData14.Text = objdt.Rows(0).Item(14)
                LblData15.Text = objdt.Rows(0).Item(15)
                LblData16.Text = objdt.Rows(0).Item(16)
                LblData17.Text = objdt.Rows(0).Item(17)
                LblData18.Text = objdt.Rows(0).Item(18)
                LblData19.Text = objdt.Rows(0).Item(19)
                LblData20.Text = objdt.Rows(0).Item(20)
                LblData21.Text = objdt.Rows(0).Item(21)
                LblData22.Text = objdt.Rows(0).Item(22)
                LblData23.Text = objdt.Rows(0).Item(23)
                LblData24.Text = objdt.Rows(0).Item(24)

                Dim strentered As String
                Try
                    strentered = Convert.ToDateTime(objdt.Rows(0).Item(25)).ToString("dd-MMM-yyyy")
                Catch ex As Exception
                    strentered = objdt.Rows(0).Item(25)
                End Try
                LblData25.Text = strentered

                Dim strupdate As String
                Try
                    strupdate = Convert.ToDateTime(objdt.Rows(0).Item(26)).ToString("dd-MMM-yyyy")
                Catch ex As Exception
                    strupdate = objdt.Rows(0).Item(26)
                End Try

                LblData26.Text = strupdate
                LblData27.Text = objdt.Rows(0).Item(27)
                LblData28.Text = objdt.Rows(0).Item(28)
                LblData29.Text = objdt.Rows(0).Item(29) 'Added By Felix 11 Oct 2018


            End If
        End Using

    End Sub

End Class
