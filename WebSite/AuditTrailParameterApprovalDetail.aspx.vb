Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class AuditTrailParameterApprovalDetail
    Inherits Parent
#Region "Property"
    ''' <summary>
    ''' Get Type Confirm
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property
    ''' <summary>
    ''' Get ApprovalID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property AuditTrailParameterApprovalID() As Int64
        Get
            Return Me.Request.Params("ApprovalID")
        End Get
    End Property
    ''' <summary>
    ''' Get ParametersPendingApprovalID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ParametersPendingApprovalID() As Int64
        Get
            Return Me.Request.Params("PendingApprovalID")
        End Get
    End Property
#End Region

#Region "Load"
    ''' <summary>
    ''' Load TYPE Add
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadAuditTrailParameterAdd()
        Try
            Me.LabelTitle.Text = "Activity: Add AuditTrail Parameter"
            Using AccessAuditTrailParameterApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterApprovalTableAdapter
                If AccessAuditTrailParameterApproval.GetData.Rows.Count > 0 Then
                    Dim Row As AMLDAL.AMLDataSet.AuditTrailParameterApprovalRow = AccessAuditTrailParameterApproval.GetData.Rows(0)
                    Me.LabelMaximunAuditTrailRecord.Text = Row.MaximumTrailRecord
                    Dim TypeAlert As String
                    Select Case Row.AlertOptions
                        Case 1
                            TypeAlert = "Overwrite."
                            Me.LabelOperatorEmailAddress.Text = "N/A"
                            Me.LabelNumberofRecordBeforeReachOut.Text = "N/A"
                        Case 2
                            TypeAlert = "Alert by Mail."
                            Me.LabelOperatorEmailAddress.Text = Row.OperatorEmailAddress
                            Me.LabelNumberofRecordBeforeReachOut.Text = Row.RecordCountBeforeReachOut
                        Case 3
                            TypeAlert = "Clear AuditTrail Manualy)."
                            Me.LabelOperatorEmailAddress.Text = "N/A"
                            Me.LabelNumberofRecordBeforeReachOut.Text = "N/A"
                        Case Else
                            TypeAlert = "There is not Type Alert."
                    End Select
                    Me.LabelTypeAlert.Text = TypeAlert
                Else
                    Throw New Exception("Data not exist anymore. It's has been accepted or rejected by other user.")
                End If
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Load Type Edit
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadAuditTrailParameterEdit()
        Try
            Me.LabelTitle.Text = "Activity: Edit AuditTrail Parameter"
            Using AccessAuditTrailParameterApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterApprovalTableAdapter
                If AccessAuditTrailParameterApproval.GetData.Rows.Count > 0 Then
                    Dim Row As AMLDAL.AMLDataSet.AuditTrailParameterApprovalRow = AccessAuditTrailParameterApproval.GetData.Rows(0)

                    ' New
                    Me.LabelMaximunAuditTrailRecordNew.Text = Row.MaximumTrailRecord
                    Dim TypeAlertNew As String
                    Select Case Row.AlertOptions
                        Case 1
                            TypeAlertNew = "Overwrite ."
                            Me.LabelOperatorEmailAddressNew.Text = "N/A"
                            Me.LabelNumberofRecordBeforeReachOutNew.Text = "N/A"
                        Case 2
                            TypeAlertNew = "Alert by Mail."
                            Me.LabelOperatorEmailAddressNew.Text = Row.OperatorEmailAddress
                            Me.LabelNumberofRecordBeforeReachOutNew.Text = Row.RecordCountBeforeReachOut
                        Case 3
                            TypeAlertNew = "Clear AuditTrail Manualy."
                            Me.LabelOperatorEmailAddressNew.Text = "N/A"
                            Me.LabelNumberofRecordBeforeReachOutNew.Text = "N/A"
                        Case Else
                            TypeAlertNew = "There is not Type Alert."
                    End Select
                    Me.LabelTypeAlertNew.Text = TypeAlertNew

                    'Old
                    Me.LabelMaximunAuditTrailRecordOld.Text = Row.MaximumTrailRecord_Old
                    Dim TypeAlertOld As String
                    Select Case Row.AlertOptions_Old
                        Case 1
                            TypeAlertOld = "Overwrite."
                            Me.LabelOperatorEmailAddressOld.Text = "N/A"
                            Me.LabelNumberofRecordBeforeReachOutOld.Text = "N/A"
                        Case 2
                            TypeAlertOld = "Alert by Mail."
                            Me.LabelOperatorEmailAddressOld.Text = Row.OperatorEmailAddress
                            Dim strRecordCountBeforeReachOut As String
                            If Row.RecordCountBeforeReachOut_Old = 0 Then
                                strRecordCountBeforeReachOut = "N/A"
                            Else
                                strRecordCountBeforeReachOut = Row.RecordCountBeforeReachOut_Old
                            End If
                            Me.LabelNumberofRecordBeforeReachOutOld.Text = strRecordCountBeforeReachOut
                        Case 3
                            TypeAlertOld = "Clear AuditTrail Manualy."
                            Me.LabelOperatorEmailAddressOld.Text = "N/A"
                            Me.LabelNumberofRecordBeforeReachOutOld.Text = "N/A"
                        Case Else
                            TypeAlertOld = "There is not Type Alert."
                    End Select
                    Me.LabelTypeAlertOld.Text = TypeAlertOld
                Else
                    Throw New Exception("Data not exist anymore. It's has been accepted or rejected by other user.")
                End If
            End Using
        Catch
            Throw
        End Try
    End Sub
#End Region

#Region "Accept"
    ''' <summary>
    ''' Accept AuditTrail Parameter Add
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub AcceptAuditTrailParameterAdd()
        Try
            If Me.CekExistingData Then
                ' Get Row From AuditTrail ParameterApproval
                Using AccessAuditParameterApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterApprovalTableAdapter
                    Dim RowATApproval As AMLDAL.AMLDataSet.AuditTrailParameterApprovalRow = AccessAuditParameterApproval.GetData.Rows(0)
                    '1. Insert ke Table Audit Trail 
                    Using InsertAuditTrailParameter As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterTableAdapter
                        If InsertAuditTrailParameter.InsertAuditTrailParameter(CInt(RowATApproval.MaximumTrailRecord), CInt(RowATApproval.AlertOptions), RowATApproval.OperatorEmailAddress, CInt(RowATApproval.RecordCountBeforeReachOut), CDate(RowATApproval.CreatedDate), Now) Then
                            Me.InsertAuditTrail("Add", "Accept")
                            DeleteAllApproval()
                        Else
                            Throw New Exception("Unable to insert audit trail parameter.")
                        End If
                    End Using
                End Using
            Else
                Throw New Exception("Data not exist anymore.")
            End If
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Accept AuditTrail Parameter Edit
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub AcceptAuditTrailParameterEdit()
        Try
            If Me.CekExistingData Then
                ' Get Row From AuditTrail ParameterApproval
                Using AccessAuditParameterApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterApprovalTableAdapter
                    Dim RowATApproval As AMLDAL.AMLDataSet.AuditTrailParameterApprovalRow = AccessAuditParameterApproval.GetData.Rows(0)

                    'Insert ke Table Audit Trail 
                    Using UpdateAuditTrailParameter As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterTableAdapter
                        If UpdateAuditTrailParameter.UpdateAuditTrailParameter(CInt(RowATApproval.MaximumTrailRecord), CInt(RowATApproval.AlertOptions), RowATApproval.OperatorEmailAddress, CInt(RowATApproval.RecordCountBeforeReachOut), Now) Then
                            Me.InsertAuditTrail("Edit", "Accept")
                            Me.DeleteAllApproval()
                        Else
                            Throw New Exception("Unable to update audit trail parameter.")
                        End If
                    End Using
                End Using
            Else
                Throw New Exception("Data not exist anymore.")
            End If
        Catch
            Throw
        End Try
    End Sub
#End Region

#Region "Reject"
    ''' <summary>
    ''' Reject AuditTrail Parameter Add
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub RejectAuditTrailParameterAdd()
        Try
            Me.InsertAuditTrail("Add", "Reject")
            DeleteAllApproval()
            Me.Response.Redirect("Default.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' Reject AuditTrail Parameter Edit
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub RejectAuditTrailParameterEdit()
        Try
            Me.InsertAuditTrail("Edit", "Reject")
            DeleteAllApproval()
            Me.Response.Redirect("Default.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
    ''' <summary>
    ''' Cek existing data
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CekExistingData() As Boolean
        Using AccessAuditTrailParameterApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterApprovalTableAdapter
            Using dt As Data.DataTable = AccessAuditTrailParameterApproval.GetData
                Dim row() As Data.DataRow = dt.Select("ApprovalID=" & Me.AuditTrailParameterApprovalID)
                If row.Length = 0 Then
                    Return False
                Else
                    Return True
                    'Throw New Exception("Data not exist anymore.")
                End If
            End Using
        End Using
    End Function

#Region "Insert dan Cek AuditTrail"
    ''' <summary>
    ''' Insert ke Audit Trail
    ''' </summary>
    ''' <param name="mode"></param>
    ''' <param name="Action"></param>
    ''' <remarks></remarks>
    Private Sub InsertAuditTrail(ByVal mode As String, ByVal Action As String)
        Try
            Using AccessAuditTrailParameter As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterTableAdapter
                Using dtAuditTrailParameter As AMLDAL.AMLDataSet.AuditTrailParameterDataTable = AccessAuditTrailParameter.GetData
                    Dim RowAuditTrailParameter As AMLDAL.AMLDataSet.AuditTrailParameterRow = dtAuditTrailParameter.Rows(0)
                    If RowAuditTrailParameter.AlertOptions = 1 Then 'alert Overwrite
                        If Not Sahassa.AML.AuditTrailAlert.CheckOverwrite(6) Then
                            Throw New Exception("There is a problem in checking audittrail alert overwrite")
                        End If
                    ElseIf RowAuditTrailParameter.AlertOptions = 2 Then 'alert mail
                        If Not Sahassa.AML.AuditTrailAlert.CheckMailAlert(6) Then
                            Throw New Exception("There is a problem in checking audittrail alert by mail.")
                        End If
                    End If ' yg manual tidak perlu d cek krn ada audittrail maintenance
                End Using
            End Using
            Using AccessAuditTrailParameterApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterApprovalTableAdapter
                Using dtAuditTrailParameterApproval As AMLDAL.AMLDataSet.AuditTrailParameterApprovalDataTable = AccessAuditTrailParameterApproval.GetDataById(CInt(Me.AuditTrailParameterApprovalID))
                    Dim row As AMLDAL.AMLDataSet.AuditTrailParameterApprovalRow = dtAuditTrailParameterApproval.Rows(0)
                    Dim AlertOption_Old = ""
                    Dim AlertOption_New = ""
                    Dim OperatorMail_Old = ""
                    Dim OperatorMail_New = ""
                    Dim Limit_Old = ""
                    Dim Limit_New As String = ""
                    If mode.ToLower = "add" Then

                    Else ' edit
                        ' Insert KeAuditTrail

                        Select Case row.AlertOptions
                            Case 1
                                AlertOption_New = "Overwrite"
                                OperatorMail_New = ""
                                Limit_New = 0
                            Case 2
                                AlertOption_New = "Alert By Mail"
                                OperatorMail_New = row.OperatorEmailAddress
                            Case 3
                                AlertOption_New = "Manually"
                                OperatorMail_New = ""
                                Limit_New = 0
                        End Select

                        Select Case row.AlertOptions_Old
                            Case 1
                                AlertOption_Old = "Overwrite"
                                OperatorMail_Old = ""
                                Limit_Old = 0
                            Case 2
                                AlertOption_Old = "Alert By Mail"
                                OperatorMail_Old = row.OperatorEmailAddress_Old
                            Case 3
                                AlertOption_Old = "Manually"
                                OperatorMail_Old = ""
                                Limit_Old = 0
                        End Select
                        Using ParameterPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                            Dim UserCreated As String = ParameterPendingApproval.SelectParameters_PendingApprovalUserID(Me.ParametersPendingApprovalID)

                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                AccessAudit.Insert(Now, UserCreated, Sahassa.AML.Commonly.SessionUserId, "Parameters - AuditTrailParameter", "MaximumTrailRecord", mode, row.MaximumTrailRecord_Old, row.MaximumTrailRecord, Action)
                                AccessAudit.Insert(Now, UserCreated, Sahassa.AML.Commonly.SessionUserId, "Parameters - AuditTrailParameter", "AlertOptions", mode, AlertOption_Old, AlertOption_New, Action)
                                AccessAudit.Insert(Now, UserCreated, Sahassa.AML.Commonly.SessionUserId, "Parameters - AuditTrailParameter", "OperatorEmailAddress", mode, OperatorMail_Old, OperatorMail_New, Action)
                                AccessAudit.Insert(Now, UserCreated, Sahassa.AML.Commonly.SessionUserId, "Parameters - AuditTrailParameter", "RecordCountBeforeReachOut", mode, Limit_Old, Limit_New, Action)
                                AccessAudit.Insert(Now, UserCreated, Sahassa.AML.Commonly.SessionUserId, "Parameters - AuditTrailParameter", "CreatedDate", mode, row.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), Now, Action)
                                AccessAudit.Insert(Now, UserCreated, Sahassa.AML.Commonly.SessionUserId, "Parameters - AuditTrailParameter", "LastUpdateDate", mode, row.LastUpdateDate_Old.ToString("dd-MMMM-yyyy HH:mm"), Now, Action)
                            End Using
                        End Using
                    End If

                End Using
            End Using

        Catch
            Throw
        End Try
    End Sub
#End Region

#Region "Delete All Approval"

    ''' <summary>
    ''' Delete All Approval
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DeleteAllApproval()
        Dim oTrans As SqlTransaction = Nothing
        Try
            If Me.CekExistingData Then


                Using AccessAuditParameterApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterApprovalTableAdapter
                    '1. delete tuk table auditparameterapproval
                    oTrans = BeginTransaction(AccessAuditParameterApproval)
                    AccessAuditParameterApproval.DeleteAuditTrailParameterApproval(Me.AuditTrailParameterApprovalID)

                    '2. delete tuk table parameter pending approval
                    Using AccessParameterPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                        SetTransaction(AccessParameterPendingApproval, oTrans)
                        AccessParameterPendingApproval.DeleteParametersPendingApproval(Me.ParametersPendingApprovalID)
                    End Using

                    '3. delete tuk table parameter approval
                    Using AccessParameterApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                        SetTransaction(AccessParameterApproval, oTrans)
                        AccessParameterApproval.DeleteParametersApproval(Me.ParametersPendingApprovalID)
                    End Using
                End Using
                oTrans.Commit()
                'End Using
            Else
                Throw New Exception("Data not exist anymore.")
            End If
        Catch ex As Exception
            If Not oTrans Is Nothing Then oTrans.Rollback()
            Throw ex
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
                oTrans = Nothing
            End If
        End Try
    End Sub

#End Region

    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HideAll()
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.AuditTrailParameterAdd
                        Me.LoadAuditTrailParameterAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.AuditTrailParameterEdit
                        Me.LoadAuditTrailParameterEdit()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' Hide All
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub HideAll()
        Dim StrId As String = ""
        Select Case Me.ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.AuditTrailParameterAdd
                StrId = "ATPAdd"
            Case Sahassa.AML.Commonly.TypeConfirm.AuditTrailParameterEdit
                StrId = "ATPEdi"
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select
        For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
            ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID.Substring(0, 6) = StrId
        Next
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.AuditTrailParameterAdd
                    Me.AcceptAuditTrailParameterAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.AuditTrailParameterEdit
                    Me.AcceptAuditTrailParameterEdit()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "AuditTrailParameterManagementApproval.aspx"

            Me.Response.Redirect("AuditTrailParameterManagementApproval.aspx", False)

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.AuditTrailParameterAdd
                    Me.RejectAuditTrailParameterAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.AuditTrailParameterEdit
                    Me.RejectAuditTrailParameterEdit()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "AuditTrailParameterManagementApproval.aspx"

            Me.Response.Redirect("AuditTrailParameterManagementApproval.aspx", False)

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "AuditTrailParameterManagementApproval.aspx"

        Me.Response.Redirect("AuditTrailParameterManagementApproval.aspx", False)

    End Sub
End Class
