Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports AMLBLL.ValidateBLL
Imports AMLBLL.DataType
Imports Sahassa.AML


Partial Class CustomerCTRViewCustomer
    Inherits Parent

#Region "properties..."

    ReadOnly Property GetCIFNo() As String
        Get
            If Not Request.Params("CIFNo") Is Nothing Then
                Return CStr(Request.Params("CIFNo")).Replace("'", "''")
            Else
                Return ""
            End If
        End Get
    End Property

    ReadOnly Property GetTanggalTransaksi() As String
        Get
            If Not Request.Params("TanggalTransaksi") Is Nothing Then
                Return CStr(Request.Params("TanggalTransaksi")).Replace("'", "''")
            Else
                Return ""
            End If
        End Get
    End Property

    ReadOnly Property GetTipeTransaksi() As String
        Get
            If Not Request.Params("TipeTransaksi") Is Nothing Then
                Return CStr(Request.Params("TipeTransaksi")).Replace("'", "''")
            Else
                Return ""
            End If
        End Get
    End Property

    ReadOnly Property GetNomorTiketTransaksi() As String
        Get
            If Not Request.Params("NomorTiketTransaksi") Is Nothing Then
                Return CStr(Request.Params("NomorTiketTransaksi")).Replace("'", "''")
            Else
                Return ""
            End If
        End Get
    End Property

    Private Property SetnGetPK_CustomerCTR_ID() As Integer
        Get
            If Session("CustomerCTRViewCustomer.PK_CustomerCTR_ID") Is Nothing Then
                Session("CustomerCTRViewCustomer.PK_CustomerCTR_ID") = -1
            End If
            Return Session("CustomerCTRViewCustomer.PK_CustomerCTR_ID")
        End Get
        Set(ByVal value As Integer)
            Session("CustomerCTRViewCustomer.PK_CustomerCTR_ID") = value
        End Set
    End Property

    Private Property SetnGetRowEdit() As Integer
        Get
            If Session("CustomerCTRViewCustomer.RowEdit") Is Nothing Then
                Session("CustomerCTRViewCustomer.RowEdit") = -1
            End If
            Return Session("CustomerCTRViewCustomer.RowEdit")
        End Get
        Set(ByVal value As Integer)
            Session("CustomerCTRViewCustomer.RowEdit") = value
        End Set
    End Property

    Private Property SetnGetActionType() As String
        Get
            If Session("CustomerCTRViewCustomer.ActionType") Is Nothing Then
                Session("CustomerCTRViewCustomer.ActionType") = ""
            End If
            Return Session("CustomerCTRViewCustomer.ActionType")
        End Get
        Set(ByVal value As String)
            Session("CustomerCTRViewCustomer.ActionType") = value
        End Set
    End Property

    Private Property SetnGetTransaction() As TList(Of CustomerCTRTransaction)
        Get
            If Session("CustomerCTRViewCustomer.Transaction") Is Nothing Then
                Session("CustomerCTRViewCustomer.Transaction") = New TList(Of CustomerCTRTransaction)
            End If
            Return Session("CustomerCTRViewCustomer.Transaction")
        End Get
        Set(ByVal value As TList(Of CustomerCTRTransaction))
            Session("CustomerCTRViewCustomer.Transaction") = value
        End Set
    End Property

    Private Property SetnGetTransactionDeleted() As TList(Of CustomerCTRTransaction)
        Get
            If Session("CustomerCTRViewCustomer.TransactionDeleted") Is Nothing Then
                Session("CustomerCTRViewCustomer.TransactionDeleted") = New TList(Of CustomerCTRTransaction)
            End If
            Return Session("CustomerCTRViewCustomer.TransactionDeleted")
        End Get
        Set(ByVal value As TList(Of CustomerCTRTransaction))
            Session("CustomerCTRViewCustomer.TransactionDeleted") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CustomerCTRViewCustomer.Sort") Is Nothing, "PK_CustomerCTRTransaction_ID  asc", Session("CustomerCTRViewCustomer.Sort"))
        End Get
        Set(ByVal Value As String)
            Session("CustomerCTRViewCustomer.Sort") = Value
        End Set
    End Property

#End Region

#Region "Function..."
    Sub clearSession()
        Session("CustomerCTRViewCustomer.RowEdit") = Nothing
        Session("CustomerCTRViewCustomer.PK_CustomerCTR_ID") = Nothing
        Session("CustomerCTRViewCustomer.CIFNo") = Nothing
        Session("CustomerCTRViewCustomer.Transaction") = Nothing
        Session("CustomerCTRViewCustomer.Sort") = Nothing
        Session("CustomerCTRViewCustomer.ActionType") = Nothing
    End Sub

    Private Sub LoadData()
        Me.DdlTujuanTransaksi.Items.Clear()
        Me.DdlTujuanTransaksi.DataSource = AMLBLL.CustomerCTRBLL.GetAllMsTujuanTransaksi()
        Me.DdlTujuanTransaksi.DataValueField = "IdTujuanTransaksi"
        Me.DdlTujuanTransaksi.DataTextField = "NamaTujuanTransaksi"
        Me.DdlTujuanTransaksi.DataBind()
        Me.DdlTujuanTransaksi.Items.Insert(0, New ListItem("[Please Select Data]", "0"))
        Me.DdlTujuanTransaksi.SelectedIndex = 0

        Me.DdlRelasiDenganPemilikRekening.Items.Clear()
        Me.DdlRelasiDenganPemilikRekening.DataSource = AMLBLL.CustomerCTRBLL.GetAllMsRelasiDenganPemilikRekening()
        Me.DdlRelasiDenganPemilikRekening.DataValueField = "IdRelasiDenganPemilikRekening"
        Me.DdlRelasiDenganPemilikRekening.DataTextField = "NamaRelasiDenganPemilikRekening"
        Me.DdlRelasiDenganPemilikRekening.DataBind()
        Me.DdlRelasiDenganPemilikRekening.Items.Insert(0, New ListItem("[Please Select Data]", "0"))
        Me.DdlRelasiDenganPemilikRekening.SelectedIndex = 0
    End Sub
#End Region

#Region "events..."

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Me.GetCIFNo = "" Then
                Throw New Exception("Silahkan masukan data CIF")
            Else
                If Not IsPostBack Then
                    clearSession()

                    If Me.GetTanggalTransaksi <> "" Or Me.GetTipeTransaksi <> "" Or Me.GetNomorTiketTransaksi <> "" Then
                        Me.NewTrx()
                        'TxtTanggalTransaksi.Text = Me.GetTanggalTransaksi
                        TxtTanggalTransaksi.Text = CDate(Me.GetTanggalTransaksi).ToString("dd-MM-yyyy")
                        Select Case Me.GetTipeTransaksi
                            Case "C"
                                RblTipeTransaksi.SelectedValue = "C"
                            Case "D"
                                RblTipeTransaksi.SelectedValue = "D"
                            Case Else
                                RblTipeTransaksi.SelectedIndex = -1
                        End Select
                        TxtNomorTiketTransaksi.Text = Me.GetNomorTiketTransaksi
                    Else
                        Me.ShowEditTransaksi(False)
                    End If
                    Me.popUpTanggalTransaksi.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtTanggalTransaksi.ClientID & "'), 'dd-mm-yyyy')")
                    Me.popUpTanggalTransaksi.Style.Add("display", "")
                    LoadData()
                    LoadCustomerCTRToField()
                    LoadCustomerCTRTransaction()
                End If
            End If

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub imgOKMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOKMsg.Click
        Try
            Response.Redirect("CustomerCTRView.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Response.Redirect("CustomerCTRView.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

#Region "Similar Function..."

    Sub LoadCustomerCTRToField()

        Using objCustomer As CustomerInformationDetail_WebTempTable = DataRepository.CustomerInformationDetail_WebTempTableProvider.GetByCIFNo(Me.GetCIFNo)
            SafeDefaultValue = ""
            If Not IsNothing(objCustomer) Then
                TxtNomorCIF.Text = Safe(objCustomer.CIFNo)

                If objCustomer.CustomerType = "Individual" Then
                    txtTipeCustomer.Text = "Perorangan"
                    divPerorangan.Visible = True
                    divKorporasi.Visible = False

                    txtNamaLengkap.Text = Safe(objCustomer.CustomerName)
                    txtTempatLahir.Text = Safe(objCustomer.BirthPlace)
                    txtTglLahir.Text = objCustomer.DateOfBirth.GetValueOrDefault().ToString("dd-MM-yyyy")
                    txtIDType.Text = Safe(objCustomer.IdType)
                    txtNomorID.Text = Safe(objCustomer.IdNumber)

                    Try
                        Dim objParam As Object() = {Me.GetCIFNo}

                        Using ObjDs As Data.DataSet = DataRepository.Provider.ExecuteDataSet("usp_GetAlamat", objParam)
                            If ObjDs.Tables.Count > 0 Then
                                If ObjDs.Tables(0).Rows.Count > 0 Then
                                    txtDOMAlamat1.Text = Safe(ObjDs.Tables(0).Rows(0).Item("Alamat1"))
                                    txtDOMAlamat2.Text = Safe(ObjDs.Tables(0).Rows(0).Item("Alamat2"))
                                    txtDOMAlamat3.Text = Safe(ObjDs.Tables(0).Rows(0).Item("Alamat3"))
                                    txtDOMAlamat4.Text = Safe(ObjDs.Tables(0).Rows(0).Item("Alamat4"))
                                    txtDOMKodePos.Text = Safe(ObjDs.Tables(0).Rows(0).Item("PostalCode"))
                                End If
                            End If
                        End Using
                    Catch

                    End Try

                Else
                    txtTipeCustomer.Text = "Korporasi"
                    divPerorangan.Visible = False
                    divKorporasi.Visible = True

                    txtCORPNama.Text = Safe(objCustomer.CustomerName)
                    txtCORPCustomerSubType.Text = Safe(objCustomer.CustomerSubClass)
                    txtCORPBusinessType.Text = Safe(objCustomer.BusinessType)
                    txtCORPIndustryCode.Text = Safe(objCustomer.IndustryCode)
                    txtCORPIDType.Text = Safe(objCustomer.IdType)
                    txtCORPIDNumber.Text = Safe(objCustomer.IdNumber)


                    Try
                        Dim objParam As Object() = {Me.GetCIFNo}

                        Using ObjDs As Data.DataSet = DataRepository.Provider.ExecuteDataSet("usp_GetAlamat", objParam)
                            If ObjDs.Tables.Count > 0 Then
                                If ObjDs.Tables(0).Rows.Count > 0 Then
                                    txtCORPDLAlamat1.Text = Safe(ObjDs.Tables(0).Rows(0).Item("Alamat1"))
                                    txtCORPDLAlamat2.Text = Safe(ObjDs.Tables(0).Rows(0).Item("Alamat2"))
                                    txtCORPDLAlamat3.Text = Safe(ObjDs.Tables(0).Rows(0).Item("Alamat3"))
                                    txtCORPDLAlamat4.Text = Safe(ObjDs.Tables(0).Rows(0).Item("Alamat4"))
                                    txtCORPDLKodePos.Text = Safe(ObjDs.Tables(0).Rows(0).Item("PostalCode"))
                                End If
                            End If
                        End Using
                    Catch

                    End Try

                End If

            End If
        End Using
    End Sub

#End Region

#Region "Transaksi"
    Sub LoadCustomerCTRTransaction()
        ' Check sudah ada di table CustomerCTR lom
        ' bila sudah ada, maka load data transaksinya
        Using ObjListCustomerCTR As TList(Of CustomerCTR) = DataRepository.CustomerCTRProvider.GetPaged(CustomerCTRColumn.CIFNumber.ToString & "='" & Me.GetCIFNo.Replace("'", "''") & "' AND IsExistingCustomer = 1", "", 0, Integer.MaxValue, Nothing)
            If Not ObjListCustomerCTR Is Nothing Then
                If ObjListCustomerCTR.Count > 0 Then
                    Using ObjCustomerCTR As CustomerCTR = ObjListCustomerCTR.Item(0)
                        Me.SetnGetPK_CustomerCTR_ID = ObjCustomerCTR.PK_CustomerCTR_ID
                        Me.SetnGetTransaction = DataRepository.CustomerCTRTransactionProvider.GetPaged(CustomerCTRTransactionColumn.FK_CustomerCTR_ID.ToString & "=" & Me.SetnGetPK_CustomerCTR_ID & " AND " & CustomerCTRTransactionColumn.IsProcessed.ToString & "=0", CustomerCTRTransactionColumn.PK_CustomerCTRTransaction_ID.ToString, 0, Integer.MaxValue, Nothing)
                        'Me.SetnGetTransactionOri = Me.SetnGetTransaction
                        Me.GridViewTransaction.DataSource = Me.SetnGetTransaction
                        Me.GridViewTransaction.DataBind()
                    End Using
                End If
            End If
        End Using
        Me.SetnGetTransactionDeleted = New TList(Of CustomerCTRTransaction)
    End Sub

    Private Sub ClearTransactionUI()
        TxtTanggalTransaksi.Text = ""
        RblTipeTransaksi.SelectedIndex = -1
        TxtNomorTiketTransaksi.Text = ""
        DdlTujuanTransaksi.SelectedIndex = 0
        DdlRelasiDenganPemilikRekening.SelectedIndex = 0
        Me.SetnGetRowEdit = -1
    End Sub

    Private Sub ShowEditTransaksi(blnShow As Boolean)
        Me.TblEditTransaksi.Visible = blnShow
        Me.TrNewTrx.Visible = Not blnShow
    End Sub

    Private Function IsTransactionDataValid() As Boolean
        Dim SbErrorMessage As New StringBuilder

        Try
            If TxtTanggalTransaksi.Text = "" Then
                SbErrorMessage.AppendLine("Tanggal Transaksi harus diisi<br>")
            ElseIf Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtTanggalTransaksi.Text) = False Then
                SbErrorMessage.AppendLine("Format Tanggal Transaksi tidak valid, gunakan format dd-MM-yyyy<br>")
            End If

            If RblTipeTransaksi.SelectedIndex = -1 Then
                SbErrorMessage.AppendLine("Tipe Transaksi harus dipilih<br>")
            End If

            If TxtNomorTiketTransaksi.Text = "" Then
                SbErrorMessage.AppendLine("Nomor Tiket Transaksi harus diisi<br>")
            Else

                If AMLBLL.CustomerCTRBLL.IsAlreadyInCustomerCtrTransaction(TxtNomorTiketTransaksi.Text, SetnGetPK_CustomerCTR_ID.ToString) Then
                    SbErrorMessage.AppendLine("Nomor Tiket Transaksi sudah pernah di input<br>")
                End If

                If Me.SetnGetActionType = "add" Then
                    If SetnGetTransaction.FindAll(CustomerCTRTransactionColumn.TransactionTicketNumber, TxtNomorTiketTransaksi.Text).Count > 0 Then
                        SbErrorMessage.AppendLine("Nomor Tiket Transaksi sudah pernah di input<br>")
                    End If
                Else
                    If SetnGetTransaction.FindAll(CustomerCTRTransactionColumn.TransactionTicketNumber, TxtNomorTiketTransaksi.Text).Count > 0 Then
                        If SetnGetTransaction.Item(Me.SetnGetRowEdit).TransactionTicketNumber <> SetnGetTransaction.FindAll(CustomerCTRTransactionColumn.TransactionTicketNumber, TxtNomorTiketTransaksi.Text)(0).TransactionTicketNumber Then
                            SbErrorMessage.AppendLine("Nomor Tiket Transaksi sudah pernah di input<br>")
                        End If
                    End If
                End If
            End If


            If DdlTujuanTransaksi.SelectedIndex < 1 Then
                SbErrorMessage.AppendLine("Tujuan Transaksi harus dipilih<br>")
            End If
            If DdlRelasiDenganPemilikRekening.SelectedIndex < 1 Then
                SbErrorMessage.AppendLine("Relasi dengan Pemilik Rekening harus dipilih<br>")
            End If

            If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtTanggalTransaksi.Text) AndAlso TxtNomorTiketTransaksi.Text <> "" Then
                Dim strTicketDate1 As String = Right(TxtTanggalTransaksi.Text, 2) & Mid(TxtTanggalTransaksi.Text, 4, 2) & Left(TxtTanggalTransaksi.Text, 2)
                Dim strTicketDate2 As String = Left(TxtNomorTiketTransaksi.Text, 6)
                If strTicketDate1 <> strTicketDate2 Then
                    SbErrorMessage.AppendLine("Nomor tiket transaksi tidak sesuai dengan tanggal transaksi")
                End If
            End If

            If SbErrorMessage.Length > 0 Then
                Throw New Exception(SbErrorMessage.ToString())
            End If

            Return True
        Catch ex As Exception
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
            Return False
        End Try
    End Function

    Private Sub NewTrx()
        Me.ClearTransactionUI()
        ShowEditTransaksi(True)
        Me.SetnGetActionType = "add"
    End Sub

    Protected Sub BtnNewTrx_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles BtnNewTrx.Click
        Try
            Me.NewTrx()
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub BtnSave_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles BtnSave.Click
        Try
            If Me.IsTransactionDataValid() Then
                If Me.SetnGetActionType = "add" Then
                    Using ObjCustomerTransaction As CustomerCTRTransaction = New CustomerCTRTransaction()
                        'ObjCustomerTransaction.TransactionDate = SafeDate(Me.TxtTanggalTransaksi.Text)
                        ObjCustomerTransaction.TransactionDate = SafeDate(Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", Me.TxtTanggalTransaksi.Text).ToString("yyyy-MM-dd"))
                        ObjCustomerTransaction.TransactionType = RblTipeTransaksi.SelectedValue
                        ObjCustomerTransaction.TransactionTicketNumber = Me.TxtNomorTiketTransaksi.Text
                        ObjCustomerTransaction.FK_TujuanTransaksi_ID = Me.DdlTujuanTransaksi.SelectedValue
                        ObjCustomerTransaction.OtherTujuanTransaksi = Me.TxtOtherTujuanTransaksi.Text
                        ObjCustomerTransaction.FK_RelasiDenganPemilikRekening_ID = Me.DdlRelasiDenganPemilikRekening.SelectedValue
                        ObjCustomerTransaction.OtherRelasiDenganPemilikRekening = Me.TxtOtherRelasiDenganPemilikRekening.Text
                        ObjCustomerTransaction.FK_CustomerCTR_ID = -1
                        ObjCustomerTransaction.IsProcessed = 0
                        ObjCustomerTransaction.CreatedBy = Sahassa.AML.Commonly.SessionUserId
                        ObjCustomerTransaction.CreatedDate = DateTime.Now
                        ObjCustomerTransaction.LastUpdateBy = Sahassa.AML.Commonly.SessionUserId
                        ObjCustomerTransaction.LastUpdateDate = DateTime.Now
                        SetnGetTransaction.Add(ObjCustomerTransaction)
                    End Using
                ElseIf Me.SetnGetActionType = "edit" Then
                    Using ObjCustomerTransaction As CustomerCTRTransaction = SetnGetTransaction.Item(Me.SetnGetRowEdit)
                        'ObjCustomerTransaction.TransactionDate = SafeDate(Me.TxtTanggalTransaksi.Text)
                        ObjCustomerTransaction.TransactionDate = SafeDate(Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", Me.TxtTanggalTransaksi.Text).ToString("yyyy-MM-dd"))
                        ObjCustomerTransaction.TransactionType = RblTipeTransaksi.SelectedValue
                        ObjCustomerTransaction.TransactionTicketNumber = Me.TxtNomorTiketTransaksi.Text
                        ObjCustomerTransaction.FK_TujuanTransaksi_ID = Me.DdlTujuanTransaksi.SelectedValue
                        ObjCustomerTransaction.OtherTujuanTransaksi = Me.TxtOtherTujuanTransaksi.Text
                        ObjCustomerTransaction.FK_RelasiDenganPemilikRekening_ID = Me.DdlRelasiDenganPemilikRekening.SelectedValue
                        ObjCustomerTransaction.OtherRelasiDenganPemilikRekening = Me.TxtOtherRelasiDenganPemilikRekening.Text
                        ObjCustomerTransaction.LastUpdateBy = Sahassa.AML.Commonly.SessionUserId
                        ObjCustomerTransaction.LastUpdateDate = DateTime.Now
                    End Using

                End If

                GridViewTransaction.DataSource = SetnGetTransaction
                GridViewTransaction.DataBind()
                ShowEditTransaksi(False)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub BtnCancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Try
            Me.ClearTransactionUI()
            ShowEditTransaksi(False)
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridViewTransaction_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewTransaction.ItemDataBound
        If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

            e.Item.Cells(3).Text = CStr((e.Item.ItemIndex + 1))

            Dim objlbltransactiontype As Label = CType(e.Item.FindControl("LblTransactiontype"), Label)
            Dim objctransaction As CustomerCTRTransaction = CType(e.Item.DataItem, CustomerCTRTransaction)
            If objctransaction.TransactionType = "C" Then
                objlbltransactiontype.Text = "Setor"
            ElseIf objctransaction.TransactionType = "D" Then
                objlbltransactiontype.Text = "Tarik"
            End If
            Dim objCustomerCTRTransaction As CustomerCTRTransaction = e.Item.DataItem
            If e.Item.Cells(7).Text <> "&nbsp;" Then
                Using objLblTujuanTransaksi As Label = CType(e.Item.FindControl("LblTujuanTransaksi"), Label)
                    Using ObjMsTujuanTransaksi As MsTujuanTransaksi = AMLBLL.CustomerCTRBLL.GetMsTujuanTransaksiByPK(CInt(e.Item.Cells(1).Text))
                        If Not IsNothing(ObjMsTujuanTransaksi) Then
                            If objCustomerCTRTransaction.FK_TujuanTransaksi_ID = 1 Then
                                objLblTujuanTransaksi.Text = ObjMsTujuanTransaksi.NamaTujuanTransaksi + " : " & objCustomerCTRTransaction.OtherTujuanTransaksi
                            Else
                                objLblTujuanTransaksi.Text = ObjMsTujuanTransaksi.NamaTujuanTransaksi
                            End If
                        End If
                    End Using
                End Using
            End If

            If e.Item.Cells(8).Text <> "&nbsp;" Then
                Using objLblRelasiDenganPemilikRekening As Label = CType(e.Item.FindControl("LblRelasiDenganPemilikRekening"), Label)
                    Using ObjMsRelasiDenganPemilikRekening As MsRelasiDenganPemilikRekening = AMLBLL.CustomerCTRBLL.GetMsRelasiDenganPemilikRekeningByPK(CInt(e.Item.Cells(2).Text))
                        If Not IsNothing(ObjMsRelasiDenganPemilikRekening) Then
                            objLblRelasiDenganPemilikRekening.Text = ObjMsRelasiDenganPemilikRekening.NamaRelasiDenganPemilikRekening

                            If objCustomerCTRTransaction.FK_RelasiDenganPemilikRekening_ID = 1 Then
                                objLblRelasiDenganPemilikRekening.Text = ObjMsRelasiDenganPemilikRekening.NamaRelasiDenganPemilikRekening + " : " & objCustomerCTRTransaction.OtherRelasiDenganPemilikRekening
                            Else
                                objLblRelasiDenganPemilikRekening.Text = ObjMsRelasiDenganPemilikRekening.NamaRelasiDenganPemilikRekening
                            End If

                        End If
                    End Using
                End Using
            End If

            If (e.Item.Cells(12).HasControls()) Then
                Dim lnkbtnDelete As LinkButton = (e.Item.FindControl("delbutton"))
                lnkbtnDelete.Attributes.Add("onclick", "return confirm('Do you want to Delete?');")
            End If
        End If
    End Sub

    Protected Sub GridViewTransaction_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewTransaction.ItemCommand
        If e.CommandName.ToLower = "edit" Then

            Me.ClearTransactionUI()
            ShowEditTransaksi(True)
            Me.SetnGetActionType = "edit"
            'Me.SetnGetRowEdit = e.Item.ItemIndex
            Me.SetnGetRowEdit = (Me.GridViewTransaction.CurrentPageIndex * 10) + e.Item.ItemIndex

            Using ObjCustomerTransaction As CustomerCTRTransaction = Me.SetnGetTransaction.Item(Me.SetnGetRowEdit)
                Me.TxtTanggalTransaksi.Text = ObjCustomerTransaction.TransactionDate.ToString("dd-MM-yyyy")
                RblTipeTransaksi.SelectedValue = ObjCustomerTransaction.TransactionType
                Me.TxtNomorTiketTransaksi.Text = ObjCustomerTransaction.TransactionTicketNumber
                Me.DdlTujuanTransaksi.SelectedValue = ObjCustomerTransaction.FK_TujuanTransaksi_ID
                Me.TxtOtherTujuanTransaksi.Text = ObjCustomerTransaction.OtherTujuanTransaksi
                Me.DdlRelasiDenganPemilikRekening.SelectedValue = ObjCustomerTransaction.FK_RelasiDenganPemilikRekening_ID
                Me.TxtOtherRelasiDenganPemilikRekening.Text = ObjCustomerTransaction.OtherRelasiDenganPemilikRekening
            End Using
            ShowTujuanTransaksiOther()
            ShowRelasiDenganPemilikRekeningOther()
        ElseIf e.CommandName.ToLower = "delete" Then

        End If
    End Sub

#End Region
    Protected Sub delbutton_Click(sender As Object, e As EventArgs)
        Try

            Dim objgriditem As DataGridItem = CType(CType(sender, LinkButton).NamingContainer, DataGridItem)

            Me.SetnGetActionType = "delete"
            'Me.SetnGetRowEdit = objgriditem.ItemIndex
            Me.SetnGetRowEdit = (Me.GridViewTransaction.CurrentPageIndex * 10) + objgriditem.ItemIndex
            Me.SetnGetTransactionDeleted.Add(SetnGetTransaction(SetnGetRowEdit))
            Me.SetnGetTransaction.RemoveAt(Me.SetnGetRowEdit)
            If GridViewTransaction.PageCount > 1 Then
                If Me.SetnGetTransaction.Count Mod 10 = 0 Then
                    GridViewTransaction.CurrentPageIndex = GridViewTransaction.PageCount - 2
                End If
            End If
            GridViewTransaction.DataSource = SetnGetTransaction
            GridViewTransaction.DataBind()
            ShowEditTransaksi(False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub FillCustomerCTRObject(ByRef objCustomerCTR As CustomerCTR)
        If txtTipeCustomer.Text = "Perorangan" Then
            objCustomerCTR.CustomerType = 1
            FillOrNothing(objCustomerCTR.CustomerName, txtNamaLengkap.Text)
            If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", txtTglLahir.Text) = True Then
                FillOrNothing(objCustomerCTR.INDV_TanggalLahir, Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", txtTglLahir.Text))
            End If
            FillOrNothing(objCustomerCTR.INDV_TempatLahir, txtTempatLahir.Text)
            FillOrNothing(objCustomerCTR.INDV_NomorId, txtNomorID.Text)
            FillOrNothing(objCustomerCTR.INDV_DOM_KodePos, txtDOMKodePos.Text)
        Else
            objCustomerCTR.CustomerType = 2
            FillOrNothing(objCustomerCTR.CustomerName, txtNamaLengkap.Text)
        End If
        objCustomerCTR.IsExistingCustomer = "1"
        FillOrNothing(objCustomerCTR.CIFNumber, Me.GetCIFNo)

        FillOrNothing(objCustomerCTR.LastUpdateBy, Sahassa.AML.Commonly.SessionUserId)
        FillOrNothing(objCustomerCTR.LastUpdateDate, Now(), True, oDate)
    End Sub

    Sub hideSave()
        ImageSave.Visible = False
        ImageCancel.Visible = False
    End Sub

    Protected Sub ImageSave_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            Using OTrans As TransactionManager = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)

                Try

                    OTrans.BeginTransaction(Data.IsolationLevel.ReadUncommitted)
                    If Me.SetnGetPK_CustomerCTR_ID > 0 Then
                        Using ObjCustomerCtr As CustomerCTR = DataRepository.CustomerCTRProvider.GetByPK_CustomerCTR_ID(Me.SetnGetPK_CustomerCTR_ID)
                            FillCustomerCTRObject(ObjCustomerCtr)
                            DataRepository.CustomerCTRProvider.Save(OTrans, ObjCustomerCtr)

                            ' Set PK_CustomerCTR_ID for each Transaction
                            For Each ObjTrx As CustomerCTRTransaction In SetnGetTransaction
                                ObjTrx.FK_CustomerCTR_ID = ObjCustomerCtr.PK_CustomerCTR_ID
                            Next
                            DataRepository.CustomerCTRTransactionProvider.Save(OTrans, SetnGetTransaction)

                            For Each ObjTrx As CustomerCTRTransaction In SetnGetTransactionDeleted
                                Using objTtransactiongrips As TList(Of TransactionDetailCTRGrips) = DataRepository.TransactionDetailCTRGripsProvider.GetPaged(OTrans, TransactionDetailCTRGripsColumn.TransactionDate.ToString & "='" & ObjTrx.TransactionDate.ToString("yyyy-MM-dd") & "' and " & TransactionDetailCTRGripsColumn.TransactionTicketNo.ToString & "='" & ObjTrx.TransactionTicketNumber & "' ", "", 0, Integer.MaxValue, 0)
                                    If objTtransactiongrips.Count > 0 Then

                                        For Each item As TransactionDetailCTRGrips In objTtransactiongrips
                                            item.FK_CustomerCTR_ID = Nothing
                                            item.FK_CustomerCTRTransaction_ID = Nothing
                                        Next
                                        DataRepository.TransactionDetailCTRGripsProvider.Save(OTrans, objTtransactiongrips)

                                    End If
                                End Using
                            Next

                            OTrans.Commit()

                            mtvPage.ActiveViewIndex = 1
                            lblMsg.Text = "Data has been save successfully"
                            hideSave()

                            Dim strSQLMapping As String = String.Empty
                            For Each ObjTrx As CustomerCTRTransaction In SetnGetTransaction
                                strSQLMapping += " DELETE FROM TaskListCTRPelaku WITH (ROWLOCK)"
                                strSQLMapping += " WHERE TransactionDate = '" & ObjTrx.TransactionDate.ToString("yyyy-MM-dd") & "'"
                                strSQLMapping += " AND TransactionTicketNo = '" & ObjTrx.TransactionTicketNumber & "'"
                                strSQLMapping += vbCrLf
                            Next
                            If strSQLMapping <> String.Empty Then
                                DataRepository.Provider.ExecuteNonQuery(Data.CommandType.Text, strSQLMapping)
                            End If

                        End Using
                    Else
                        Using ObjCustomerCtr As New CustomerCTR
                            FillCustomerCTRObject(ObjCustomerCtr)
                            FillOrNothing(ObjCustomerCtr.CreatedBy, Sahassa.AML.Commonly.SessionUserId)
                            FillOrNothing(ObjCustomerCtr.CreatedDate, Now(), True, oDate)
                            DataRepository.CustomerCTRProvider.Save(OTrans, ObjCustomerCtr)

                            ' Set PK_CustomerCTR_ID for each Transaction
                            For Each ObjTrx As CustomerCTRTransaction In SetnGetTransaction
                                ObjTrx.FK_CustomerCTR_ID = ObjCustomerCtr.PK_CustomerCTR_ID
                            Next
                            DataRepository.CustomerCTRTransactionProvider.Save(OTrans, SetnGetTransaction)

                            For Each ObjTrx As CustomerCTRTransaction In SetnGetTransactionDeleted
                                Using objTtransactiongrips As TList(Of TransactionDetailCTRGrips) = DataRepository.TransactionDetailCTRGripsProvider.GetPaged(OTrans, TransactionDetailCTRGripsColumn.TransactionDate.ToString & "='" & ObjTrx.TransactionDate.ToString("yyyy-MM-dd") & "' and " & TransactionDetailCTRGripsColumn.TransactionTicketNo.ToString & "='" & ObjTrx.TransactionTicketNumber & "' ", "", 0, Integer.MaxValue, 0)
                                    If objTtransactiongrips.Count > 0 Then

                                        For Each item As TransactionDetailCTRGrips In objTtransactiongrips
                                            item.FK_CustomerCTR_ID = Nothing
                                            item.FK_CustomerCTRTransaction_ID = Nothing
                                        Next
                                        DataRepository.TransactionDetailCTRGripsProvider.Save(OTrans, objTtransactiongrips)

                                    End If
                                End Using
                            Next

                            OTrans.Commit()

                            mtvPage.ActiveViewIndex = 1
                            lblMsg.Text = "Data has been save successfully"
                            hideSave()

                            Dim strSQLMapping As String = String.Empty
                            For Each ObjTrx As CustomerCTRTransaction In SetnGetTransaction
                                strSQLMapping += " DELETE FROM TaskListCTRPelaku WITH (ROWLOCK)"
                                strSQLMapping += " WHERE TransactionDate = '" & ObjTrx.TransactionDate.ToString("yyyy-MM-dd") & "'"
                                strSQLMapping += " AND TransactionTicketNo = '" & ObjTrx.TransactionTicketNumber & "'"
                                strSQLMapping += vbCrLf
                            Next
                            If strSQLMapping <> String.Empty Then
                                DataRepository.Provider.ExecuteNonQuery(Data.CommandType.Text, strSQLMapping)
                            End If
                        End Using
                    End If

                Catch ex As Exception
                    If OTrans.IsOpen Then
                        OTrans.Rollback()
                    End If
                    Throw
                End Try
            End Using
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub

    Private Sub ShowTujuanTransaksiOther()
        If DdlTujuanTransaksi.SelectedValue = "1" Then
            Me.TxtOtherTujuanTransaksi.Visible = True
        Else
            Me.TxtOtherTujuanTransaksi.Visible = False
        End If
    End Sub

    Private Sub ShowRelasiDenganPemilikRekeningOther()
        If DdlRelasiDenganPemilikRekening.SelectedValue = "1" Then
            Me.TxtOtherRelasiDenganPemilikRekening.Visible = True
        Else
            Me.TxtOtherRelasiDenganPemilikRekening.Visible = False
        End If
    End Sub

    Protected Sub DdlTujuanTransaksi_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles DdlTujuanTransaksi.SelectedIndexChanged
        Me.TxtOtherTujuanTransaksi.Text = ""
        Me.ShowTujuanTransaksiOther()
    End Sub

    Protected Sub DdlRelasiDenganPemilikRekening_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles DdlRelasiDenganPemilikRekening.SelectedIndexChanged
        Me.TxtOtherRelasiDenganPemilikRekening.Text = ""
        Me.ShowRelasiDenganPemilikRekeningOther()
    End Sub

    Protected Sub GridViewTransaction_PageIndexChanged(source As Object, e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridViewTransaction.PageIndexChanged
        Try
            GridViewTransaction.CurrentPageIndex = e.NewPageIndex
            GridViewTransaction.DataSource = SetnGetTransaction
            GridViewTransaction.DataBind()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewTransaction_SortCommand(source As Object, e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridViewTransaction.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
            SetnGetTransaction.Sort(Me.SetnGetSort)
            GridViewTransaction.DataSource = SetnGetTransaction
            GridViewTransaction.DataBind()

            ShowEditTransaksi(False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class '830