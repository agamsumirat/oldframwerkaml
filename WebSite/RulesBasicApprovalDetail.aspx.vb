Imports System.Data.SqlClient
Imports Sahassa.AML.TableAdapterHelper

Partial Class RulesBasicApprovalDetail
    Inherits Parent

    Private ReadOnly Property PKRulesBasicPendingApprovalID() As String
        Get
            Return Request.Params("RulesBasic_PendingApprovalID")
        End Get
    End Property
    Private _orowRulesBasicPendingApprovalID As AMLDAL.BasicRulesDataSet.RulesBasic_PendingApprovalRow

    Private ReadOnly Property orowRulesBasicPendingApprovalID() As AMLDAL.BasicRulesDataSet.RulesBasic_PendingApprovalRow
        Get
            If _orowRulesBasicPendingApprovalID Is Nothing Then


                Using adapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasic_PendingApprovalTableAdapter
                    Using otable As AMLDAL.BasicRulesDataSet.RulesBasic_PendingApprovalDataTable = adapter.GetRulesBasicPendingApprovalBYPK(Me.PKRulesBasicPendingApprovalID)
                        If otable.Rows.Count > 0 Then
                            _orowRulesBasicPendingApprovalID = otable.Rows(0)
                            Return _orowRulesBasicPendingApprovalID
                        Else
                            _orowRulesBasicPendingApprovalID = Nothing
                            Return _orowRulesBasicPendingApprovalID
                        End If
                    End Using

                End Using
            Else
                Return _orowRulesBasicPendingApprovalID
            End If
        End Get
    End Property

    Private _oRowRulesBasicApproval As AMLDAL.BasicRulesDataSet.RulesBasicApprovalRow
    ''' <summary>
    ''' property ini untuk mengambil data detail approval berdasarkan pkpendingapprovalid
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property oRowRulesBasicApproval() As AMLDAL.BasicRulesDataSet.RulesBasicApprovalRow
        Get
            If _oRowRulesBasicApproval Is Nothing Then
                Using adapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasicApprovalTableAdapter
                    Using otable As AMLDAL.BasicRulesDataSet.RulesBasicApprovalDataTable = adapter.GetByPendingApprovalId(Me.PKRulesBasicPendingApprovalID)
                        If otable.Rows.Count > 0 Then
                            _oRowRulesBasicApproval = otable.Rows(0)
                            Return _oRowRulesBasicApproval
                        Else
                            _oRowRulesBasicApproval = Nothing
                            Return _oRowRulesBasicApproval
                        End If
                    End Using
                End Using
            Else
                Return _oRowRulesBasicApproval
            End If
        End Get
    End Property

    Private Sub LoadData()
        Dim oTypeMode As Type = GetType(Sahassa.AML.Commonly.TypeMode)
        Try

            If Not orowRulesBasicPendingApprovalID Is Nothing Then
                If Not orowRulesBasicPendingApprovalID.IsRulesBasic_PendingApprovalModeIDNull Then
                    LabelActivity.Text = [Enum].GetName(oTypeMode, orowRulesBasicPendingApprovalID.RulesBasic_PendingApprovalModeID)
                    If Not oRowRulesBasicApproval Is Nothing Then
                        Select Case orowRulesBasicPendingApprovalID.RulesBasic_PendingApprovalModeID
                            Case Sahassa.AML.Commonly.TypeMode.Add, Sahassa.AML.Commonly.TypeMode.Delete
                                PanelOldValue.Visible = False
                                PanelCustomerDataOLDValue.Visible = False
                                PanelTransactionOld.Visible = False
                                PanelNewValue.GroupingText = ""
                                PanelCustomerDataNewValue.GroupingText = ""
                                PanelTransactionNew.GroupingText = ""
                                'Info Basic Rules
                                LabelBasicRulesNameNew.Text = oRowRulesBasicApproval.RulesBasicName
                                If Not oRowRulesBasicApproval.IsRulesBasicDescriptionNull Then
                                    LabelBasicRulesDescriptionnew.Text = oRowRulesBasicApproval.RulesBasicDescription
                                End If
                                If Not oRowRulesBasicApproval.IsSTRAlertTypeNull Then
                                    Using STRAlertTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.STRAlertTypeTableAdapter
                                        Using STRAlertTypeTable As New AMLDAL.BasicRulesDataSet.STRAlertTypeDataTable
                                            STRAlertTypeAdapter.Fill(STRAlertTypeTable)
                                            If STRAlertTypeTable.Rows.Count > 0 Then
                                                Dim STRAlertTypeTableRow As AMLDAL.BasicRulesDataSet.STRAlertTypeRow
                                                STRAlertTypeTableRow = STRAlertTypeTable.FindBySTRAlertType(oRowRulesBasicApproval.STRAlertType)
                                                If Not STRAlertTypeTableRow Is Nothing Then
                                                    If Not STRAlertTypeTableRow.IsSTRAlertTypeNameNull Then
                                                        LabelBasicRulesAlertToNew.Text = STRAlertTypeTableRow.STRAlertTypeName
                                                    End If
                                                End If
                                            End If
                                        End Using
                                    End Using
                                End If
                                If Not oRowRulesBasicApproval.IsIsEnabledNull Then
                                    LabelBasicRulesEnableNew.Text = oRowRulesBasicApproval.IsEnabled
                                End If
                                'info customer
                                If Not oRowRulesBasicApproval.IsSourceCustomerOfNull Then
                                    Me.LabelCustomerOfNew.Text = oRowRulesBasicApproval.SourceCustomerOf
                                End If
                                If Not oRowRulesBasicApproval.IsSourceCustomerTypeNull Then
                                    If oRowRulesBasicApproval.SourceCustomerType = "A" Then
                                        Me.LabelCustomerTypeNew.Text = "Personal"
                                    Else
                                        If oRowRulesBasicApproval.SourceCustomerType = "B" Then
                                            Me.LabelCustomerTypeNew.Text = "Company"
                                        Else
                                            Me.LabelCustomerTypeNew.Text = oRowRulesBasicApproval.SourceCustomerType
                                        End If
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceCustomerNegativeListCategoryIdNull Then
                                    Dim StrCustomerNegativeListCategoryId As String
                                    Dim ArrCustomerNegativeListCategoryId As String()
                                    Dim StrCustomerNegativeListCategoryName As String = ""
                                    Dim Counter As Integer
                                    StrCustomerNegativeListCategoryId = oRowRulesBasicApproval.SourceCustomerNegativeListCategoryId
                                    If StrCustomerNegativeListCategoryId <> "" Then
                                        ArrCustomerNegativeListCategoryId = StrCustomerNegativeListCategoryId.Split(",")
                                        Using NegativeListCategoryAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.VerificationListCategoryTableAdapter
                                            Using NegativeListCategoryTable As New AMLDAL.BasicRulesDataSet.VerificationListCategoryDataTable
                                                NegativeListCategoryAdapter.Fill(NegativeListCategoryTable)
                                                If NegativeListCategoryTable.Rows.Count > 0 Then
                                                    Dim NegativeListCategoryTableRow As AMLDAL.BasicRulesDataSet.VerificationListCategoryRow
                                                    For Counter = 0 To ArrCustomerNegativeListCategoryId.Length - 1
                                                        NegativeListCategoryTableRow = NegativeListCategoryTable.FindByCategoryID(ArrCustomerNegativeListCategoryId(Counter).Trim())
                                                        If Not NegativeListCategoryTableRow Is Nothing Then
                                                            If Not NegativeListCategoryTableRow.IsCategoryNameNull Then
                                                                StrCustomerNegativeListCategoryName = StrCustomerNegativeListCategoryName & NegativeListCategoryTableRow.CategoryName & ", "
                                                            End If
                                                        End If
                                                    Next
                                                End If
                                            End Using
                                        End Using
                                        If StrCustomerNegativeListCategoryName.Length > 0 Then
                                            StrCustomerNegativeListCategoryName = StrCustomerNegativeListCategoryName.Remove(StrCustomerNegativeListCategoryName.Length - 2)
                                        End If
                                    Else
                                        StrCustomerNegativeListCategoryName = "None"
                                    End If
                                    Me.LabelNegativeListNew.Text = StrCustomerNegativeListCategoryName
                                End If
                                Me.LabelHighRiskBusinessNew.Text = Me.oRowRulesBasicApproval.SourceCustomerIsHighRiskBusiness.ToString()
                                Me.LabelHighRiskCountryNew.Text = Me.oRowRulesBasicApproval.SourceCustomerIsHighRiskCountry.ToString()
                                If Not oRowRulesBasicApproval.IsSourceCustomerSubTypeNull Then
                                    If oRowRulesBasicApproval.SourceCustomerSubType = "All" Then
                                        Me.LabelCustomerSubTypeNew.Text = oRowRulesBasicApproval.SourceCustomerSubType
                                    Else
                                        Using CustomerSubTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.CustomerSubTypeTableAdapter
                                            Using CustomerSubTypeTable As New AMLDAL.BasicRulesDataSet.CustomerSubTypeDataTable
                                                CustomerSubTypeAdapter.Fill(CustomerSubTypeTable)
                                                If CustomerSubTypeTable.Rows.Count > 0 Then
                                                    Dim CustomerSubTypeTableRow As AMLDAL.BasicRulesDataSet.CustomerSubTypeRow
                                                    CustomerSubTypeTableRow = CustomerSubTypeTable.FindByCustomerSubTypeCode(oRowRulesBasicApproval.SourceCustomerSubType)
                                                    If Not CustomerSubTypeTableRow Is Nothing Then
                                                        If Not CustomerSubTypeTableRow.IsCustomerSubTypeDescriptionNull Then
                                                            Me.LabelCustomerSubTypeNew.Text = CustomerSubTypeTableRow.CustomerSubTypeDescription
                                                        End If
                                                    End If
                                                End If
                                            End Using
                                        End Using
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceCustomerBusinessTypeNull Then
                                    If oRowRulesBasicApproval.SourceCustomerBusinessType = "All" Then
                                        Me.LabelBusinessTypeNew.Text = oRowRulesBasicApproval.SourceCustomerBusinessType
                                    Else
                                        Using BusinessTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.BusinessTypeTableAdapter
                                            Using BusinessTypeTable As New AMLDAL.BasicRulesDataSet.BusinessTypeDataTable
                                                BusinessTypeAdapter.Fill(BusinessTypeTable)
                                                If BusinessTypeTable.Rows.Count > 0 Then
                                                    Dim BusinessTypeTableRow As AMLDAL.BasicRulesDataSet.BusinessTypeRow
                                                    BusinessTypeTableRow = BusinessTypeTable.FindByBusinessTypeCode(oRowRulesBasicApproval.SourceCustomerBusinessType)
                                                    If Not BusinessTypeTableRow Is Nothing Then
                                                        If Not BusinessTypeTableRow.IsBusinessTypeDescriptionNull Then
                                                            Me.LabelBusinessTypeNew.Text = BusinessTypeTableRow.BusinessTypeDescription
                                                        End If
                                                    End If
                                                End If
                                            End Using
                                        End Using
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceCustomerInternalIndustryCodeNull Then
                                    If oRowRulesBasicApproval.SourceCustomerInternalIndustryCode = "All" Then
                                        Me.LabelInternalIndustriCodeNew.Text = oRowRulesBasicApproval.SourceCustomerInternalIndustryCode
                                    Else
                                        Using InternalIndustryCodeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.InternalIndustryCodeTableAdapter
                                            Using InternalIndustryCodeTable As New AMLDAL.BasicRulesDataSet.InternalIndustryCodeDataTable
                                                InternalIndustryCodeAdapter.Fill(InternalIndustryCodeTable)
                                                If InternalIndustryCodeTable.Rows.Count > 0 Then
                                                    Dim InternalIndustryCodeRow As AMLDAL.BasicRulesDataSet.InternalIndustryCodeRow
                                                    InternalIndustryCodeRow = InternalIndustryCodeTable.FindByInternalIndustryCode(oRowRulesBasicApproval.SourceCustomerInternalIndustryCode)
                                                    If Not InternalIndustryCodeRow Is Nothing Then
                                                        If Not InternalIndustryCodeRow.IsInternalIndustryDescriptionNull Then
                                                            Me.LabelInternalIndustriCodeNew.Text = InternalIndustryCodeRow.InternalIndustryDescription
                                                        End If
                                                    End If
                                                End If
                                            End Using
                                        End Using
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceAccountOpenedWithinNumberNull Then
                                    If oRowRulesBasicApproval.SourceAccountOpenedWithinNumber <> "" Then
                                        Me.LabelOpenedWithNew.Text = oRowRulesBasicApproval.SourceAccountOpenedWithinNumber
                                        If Not oRowRulesBasicApproval.IsSourceAccountOpenedWithinTypeNull Then
                                            Select Case oRowRulesBasicApproval.SourceAccountOpenedWithinType
                                                Case 1
                                                    Me.LabelOpenedWithNew.Text = Me.LabelOpenedWithNew.Text & " Day(s)"
                                                Case 2
                                                    Me.LabelOpenedWithNew.Text = Me.LabelOpenedWithNew.Text & " Month(s)"
                                                Case 3
                                                    Me.LabelOpenedWithNew.Text = Me.LabelOpenedWithNew.Text & " Year(s)"
                                            End Select
                                        End If
                                    Else
                                        Me.LabelOpenedWithNew.Text = "All"
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceAccountProductTypeNull Then
                                    Dim StrAccountProductType As String
                                    Dim ArrAccountProductType As String()
                                    Dim StrAccountProductTypeName As String = ""
                                    Dim Counter As Integer
                                    StrAccountProductType = oRowRulesBasicApproval.SourceAccountProductType
                                    If StrAccountProductType <> "" Then
                                        ArrAccountProductType = StrAccountProductType.Split(",")
                                        Using AccountTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.AccountTypeTableAdapter
                                            Using AccountTypeTable As New AMLDAL.BasicRulesDataSet.AccountTypeDataTable
                                                AccountTypeAdapter.Fill(AccountTypeTable)
                                                If AccountTypeTable.Rows.Count > 0 Then
                                                    Dim AccountTypeTableRow As AMLDAL.BasicRulesDataSet.AccountTypeRow
                                                    For Counter = 0 To ArrAccountProductType.Length - 1
                                                        AccountTypeTableRow = AccountTypeTable.FindByAccountTypeCode(ArrAccountProductType(Counter).Trim())
                                                        If Not AccountTypeTableRow Is Nothing Then
                                                            If Not AccountTypeTableRow.IsAccountTypeDescriptionNull Then
                                                                StrAccountProductTypeName = StrAccountProductTypeName & AccountTypeTableRow.AccountTypeDescription & ", "
                                                            End If
                                                        End If
                                                    Next
                                                End If
                                            End Using
                                        End Using
                                        If StrAccountProductTypeName.Length > 0 Then
                                            StrAccountProductTypeName = StrAccountProductTypeName.Remove(StrAccountProductTypeName.Length - 2)
                                        End If
                                    Else
                                        StrAccountProductTypeName = "All"
                                    End If

                                    Me.LabelAccountTypeNew.Text = StrAccountProductTypeName
                                End If
                                If Not oRowRulesBasicApproval.IsSourceAccountSegmentationNull Then
                                    Me.LabelSegmentationNew.Text = oRowRulesBasicApproval.SourceAccountSegmentation
                                End If
                                If Not oRowRulesBasicApproval.IsSourceAccountStatusNull Then
                                    If oRowRulesBasicApproval.SourceAccountStatus = "A" Then
                                        Me.LabelAccountStatusNew.Text = "All"
                                    Else
                                        Using AccountStatusAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.AccountStatusTableAdapter
                                            Using AccountStatusTable As New AMLDAL.BasicRulesDataSet.AccountStatusDataTable
                                                AccountStatusAdapter.Fill(AccountStatusTable)
                                                If AccountStatusTable.Rows.Count > 0 Then
                                                    Dim AccountStatusTableRow As AMLDAL.BasicRulesDataSet.AccountStatusRow
                                                    AccountStatusTableRow = AccountStatusTable.FindByAccountStatusCode(oRowRulesBasicApproval.SourceAccountStatus)
                                                    If Not AccountStatusTableRow Is Nothing Then
                                                        If Not AccountStatusTableRow.IsAccountStatusDescriptionNull Then
                                                            Me.LabelAccountStatusNew.Text = AccountStatusTableRow.AccountStatusDescription
                                                        End If
                                                    End If
                                                End If
                                            End Using
                                        End Using
                                    End If
                                End If

                                'info transaction
                                If Not oRowRulesBasicApproval.IsTransactionTypeNull Then
                                    If oRowRulesBasicApproval.TransactionType = 0 Then
                                        Me.LabelTransactionTypeNew.Text = "All"
                                    Else
                                        Using TransactionChannelTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.TransactionChannelTypeTableAdapter
                                            Using TransactionChannelTypeTable As New AMLDAL.BasicRulesDataSet.TransactionChannelTypeDataTable
                                                TransactionChannelTypeAdapter.Fill(TransactionChannelTypeTable)
                                                If TransactionChannelTypeTable.Rows.Count > 0 Then
                                                    Dim TransactionChannelTypeTableRow As AMLDAL.BasicRulesDataSet.TransactionChannelTypeRow
                                                    TransactionChannelTypeTableRow = TransactionChannelTypeTable.FindByPK_TransactionChannelType(oRowRulesBasicApproval.TransactionType)
                                                    If Not TransactionChannelTypeTableRow Is Nothing Then
                                                        If Not TransactionChannelTypeTableRow.IsTransactionChannelTypeNameNull Then
                                                            Me.LabelTransactionTypeNew.Text = TransactionChannelTypeTableRow.TransactionChannelTypeName
                                                        End If
                                                    End If
                                                End If
                                            End Using
                                        End Using
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsAuxiliaryTransactionCodeNull Then
                                    If oRowRulesBasicApproval.AuxiliaryTransactionCode <> "" Then
                                        Dim StrAuxiliaryTransactionCodeIN As String = ""
                                        If Not oRowRulesBasicApproval.IsAuxiliaryTransactionCodeIsINNull Then

                                            If oRowRulesBasicApproval.AuxiliaryTransactionCodeIsIN Then
                                                StrAuxiliaryTransactionCodeIN = "IN"
                                            Else
                                                StrAuxiliaryTransactionCodeIN = "NOT IN"
                                            End If
                                        End If
                                        Me.LabelAuxiliaryNew.Text = StrAuxiliaryTransactionCodeIN & " " & oRowRulesBasicApproval.AuxiliaryTransactionCode
                                    Else
                                        Me.LabelAuxiliaryNew.Text = "All"
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsTransactionAmountFromTypeNull Then
                                    If oRowRulesBasicApproval.TransactionAmountFromType = 0 Then
                                        Me.LabelAmountNew.Text = "Unspecified"
                                    Else
                                        Me.LabelAmountNew.Text = "Value"
                                        If Not oRowRulesBasicApproval.IsTransactionAmountFromNull Then
                                            Me.LabelAmountNew.Text = Me.LabelAmountNew.Text & " " & oRowRulesBasicApproval.TransactionAmountFrom
                                        End If
                                    End If
                                End If
                                
                                Me.LabelAmountNew.Text = Me.LabelAmountNew.Text & " - "
                                If Not oRowRulesBasicApproval.IsTransactionAmountToTypeNull Then
                                    If oRowRulesBasicApproval.TransactionAmountToType = 0 Then
                                        Me.LabelAmountNew.Text = Me.LabelAmountNew.Text & " Unspecified"
                                    Else
                                        Me.LabelAmountNew.Text = Me.LabelAmountNew.Text & " Value"
                                        If Not oRowRulesBasicApproval.IsTransactionAmountToNull Then
                                            Me.LabelAmountNew.Text = Me.LabelAmountNew.Text & " " & oRowRulesBasicApproval.TransactionAmountTo
                                        End If
                                    End If
                                End If

                                If Not oRowRulesBasicApproval.IsTransactionIsAmountTransactionGELimitTransactionNull Then
                                    Me.LabelCheckAmountNew.Text = oRowRulesBasicApproval.TransactionIsAmountTransactionGELimitTransaction.ToString()
                                End If
                                If Not oRowRulesBasicApproval.IsTransactionCreditDebitTypeNull Then
                                    Select Case oRowRulesBasicApproval.TransactionCreditDebitType
                                        Case "C"
                                            Me.LabelCreditDebitNew.Text = "Credit"
                                        Case "D"
                                            Me.LabelCreditDebitNew.Text = "Debit"
                                        Case Else
                                            Me.LabelCreditDebitNew.Text = "All"
                                    End Select
                                End If
                                If Not oRowRulesBasicApproval.IsTransactionFrequencyIsEnabledNull Then
                                    If oRowRulesBasicApproval.TransactionFrequencyIsEnabled Then
                                        If Not oRowRulesBasicApproval.IsTransactionFrequencyNull Then
                                            Me.LabelTransactionFrequencyNew.Text = oRowRulesBasicApproval.TransactionFrequency & " time(s)"
                                        End If
                                    Else
                                        Me.LabelTransactionFrequencyNew.Text = "All"
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsTransactionPeriodIsEnabledNull Then
                                    If oRowRulesBasicApproval.TransactionPeriodIsEnabled Then
                                        If Not oRowRulesBasicApproval.IsTransactionPeriodNumberNull Then
                                            Me.LabelTransactionPeriodNew.Text = oRowRulesBasicApproval.TransactionPeriodNumber
                                        End If
                                        If Not oRowRulesBasicApproval.IsTransactionPeriodTypeNull Then
                                            Select Case oRowRulesBasicApproval.TransactionPeriodType
                                                Case 1
                                                    Me.LabelTransactionPeriodNew.Text = Me.LabelTransactionPeriodNew.Text & " Day(s)"
                                                Case 2
                                                    Me.LabelTransactionPeriodNew.Text = Me.LabelTransactionPeriodNew.Text & " Month(s)"
                                                Case 3
                                                    Me.LabelTransactionPeriodNew.Text = Me.LabelTransactionPeriodNew.Text & " Year(s)"
                                            End Select
                                        End If
                                    Else
                                        Me.LabelTransactionPeriodNew.Text = "All"
                                    End If
                                End If

                                If Not oRowRulesBasicApproval.IsCountryCodeNull Then
                                    If oRowRulesBasicApproval.CountryCode <> "" Then
                                        Dim StrCountryCodeIN As String = ""
                                        If Not oRowRulesBasicApproval.IsCountryCodeIsINNull Then
                                            If oRowRulesBasicApproval.CountryCodeIsIN Then
                                                StrCountryCodeIN = "IN"
                                            Else
                                                StrCountryCodeIN = "NOT IN"
                                            End If
                                        End If
                                        Me.LabelCountryCodeNew.Text = StrCountryCodeIN & " " & oRowRulesBasicApproval.CountryCode
                                    Else
                                        Me.LabelCountryCodeNew.Text = "All"
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceFundIsOneCIFAndRelatedNull Then
                                    Me.LabelCIFRelatedNew.Text = oRowRulesBasicApproval.SourceFundIsOneCIFAndRelated.ToString()
                                End If
                            Case Sahassa.AML.Commonly.TypeMode.Edit
                                'Info Basic Rules
                                LabelBasicRulesNameNew.Text = oRowRulesBasicApproval.RulesBasicName
                                If Not oRowRulesBasicApproval.IsRulesBasicDescriptionNull Then
                                    LabelBasicRulesDescriptionnew.Text = oRowRulesBasicApproval.RulesBasicDescription
                                End If
                                If Not oRowRulesBasicApproval.IsSTRAlertTypeNull Then
                                    Using STRAlertTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.STRAlertTypeTableAdapter
                                        Using STRAlertTypeTable As New AMLDAL.BasicRulesDataSet.STRAlertTypeDataTable
                                            STRAlertTypeAdapter.Fill(STRAlertTypeTable)
                                            If STRAlertTypeTable.Rows.Count > 0 Then
                                                Dim STRAlertTypeTableRow As AMLDAL.BasicRulesDataSet.STRAlertTypeRow
                                                STRAlertTypeTableRow = STRAlertTypeTable.FindBySTRAlertType(oRowRulesBasicApproval.STRAlertType)
                                                If Not STRAlertTypeTableRow Is Nothing Then
                                                    If Not STRAlertTypeTableRow.IsSTRAlertTypeNameNull Then
                                                        LabelBasicRulesAlertToNew.Text = STRAlertTypeTableRow.STRAlertTypeName
                                                    End If
                                                End If
                                            End If
                                        End Using
                                    End Using
                                End If
                                If Not oRowRulesBasicApproval.IsIsEnabledNull Then
                                    LabelBasicRulesEnableNew.Text = oRowRulesBasicApproval.IsEnabled
                                End If
                                'info customer
                                If Not oRowRulesBasicApproval.IsSourceCustomerOfNull Then
                                    Me.LabelCustomerOfNew.Text = oRowRulesBasicApproval.SourceCustomerOf
                                End If
                                If Not oRowRulesBasicApproval.IsSourceCustomerTypeNull Then
                                    If oRowRulesBasicApproval.SourceCustomerType = "A" Then
                                        Me.LabelCustomerTypeNew.Text = "Personal"
                                    Else
                                        If oRowRulesBasicApproval.SourceCustomerType = "B" Then
                                            Me.LabelCustomerTypeNew.Text = "Company"
                                        Else
                                            Me.LabelCustomerTypeNew.Text = oRowRulesBasicApproval.SourceCustomerType
                                        End If
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceCustomerNegativeListCategoryIdNull Then
                                    Dim StrCustomerNegativeListCategoryId As String
                                    Dim ArrCustomerNegativeListCategoryId As String()
                                    Dim StrCustomerNegativeListCategoryName As String = ""
                                    Dim Counter As Integer
                                    StrCustomerNegativeListCategoryId = oRowRulesBasicApproval.SourceCustomerNegativeListCategoryId
                                    If StrCustomerNegativeListCategoryId <> "" Then
                                        ArrCustomerNegativeListCategoryId = StrCustomerNegativeListCategoryId.Split(",")
                                        Using NegativeListCategoryAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.VerificationListCategoryTableAdapter
                                            Using NegativeListCategoryTable As New AMLDAL.BasicRulesDataSet.VerificationListCategoryDataTable
                                                NegativeListCategoryAdapter.Fill(NegativeListCategoryTable)
                                                If NegativeListCategoryTable.Rows.Count > 0 Then
                                                    Dim NegativeListCategoryTableRow As AMLDAL.BasicRulesDataSet.VerificationListCategoryRow
                                                    For Counter = 0 To ArrCustomerNegativeListCategoryId.Length - 1
                                                        NegativeListCategoryTableRow = NegativeListCategoryTable.FindByCategoryID(ArrCustomerNegativeListCategoryId(Counter).Trim())
                                                        If Not NegativeListCategoryTableRow Is Nothing Then
                                                            If Not NegativeListCategoryTableRow.IsCategoryNameNull Then
                                                                StrCustomerNegativeListCategoryName = StrCustomerNegativeListCategoryName & NegativeListCategoryTableRow.CategoryName & ", "
                                                            End If
                                                        End If
                                                    Next
                                                End If
                                            End Using
                                        End Using
                                        If StrCustomerNegativeListCategoryName.Length > 0 Then
                                            StrCustomerNegativeListCategoryName = StrCustomerNegativeListCategoryName.Remove(StrCustomerNegativeListCategoryName.Length - 2)
                                        End If
                                    Else
                                        StrCustomerNegativeListCategoryName = "None"
                                    End If
                                    Me.LabelNegativeListNew.Text = StrCustomerNegativeListCategoryName
                                End If
                                Me.LabelHighRiskBusinessNew.Text = Me.oRowRulesBasicApproval.SourceCustomerIsHighRiskBusiness.ToString()
                                Me.LabelHighRiskCountryNew.Text = Me.oRowRulesBasicApproval.SourceCustomerIsHighRiskCountry.ToString()
                                If Not oRowRulesBasicApproval.IsSourceCustomerSubTypeNull Then
                                    If oRowRulesBasicApproval.SourceCustomerSubType = "All" Then
                                        Me.LabelCustomerSubTypeNew.Text = oRowRulesBasicApproval.SourceCustomerSubType
                                    Else
                                        Using CustomerSubTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.CustomerSubTypeTableAdapter
                                            Using CustomerSubTypeTable As New AMLDAL.BasicRulesDataSet.CustomerSubTypeDataTable
                                                CustomerSubTypeAdapter.Fill(CustomerSubTypeTable)
                                                If CustomerSubTypeTable.Rows.Count > 0 Then
                                                    Dim CustomerSubTypeTableRow As AMLDAL.BasicRulesDataSet.CustomerSubTypeRow
                                                    CustomerSubTypeTableRow = CustomerSubTypeTable.FindByCustomerSubTypeCode(oRowRulesBasicApproval.SourceCustomerSubType)
                                                    If Not CustomerSubTypeTableRow Is Nothing Then
                                                        If Not CustomerSubTypeTableRow.IsCustomerSubTypeDescriptionNull Then
                                                            Me.LabelCustomerSubTypeNew.Text = CustomerSubTypeTableRow.CustomerSubTypeDescription
                                                        End If
                                                    End If
                                                End If
                                            End Using
                                        End Using
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceCustomerBusinessTypeNull Then
                                    If oRowRulesBasicApproval.SourceCustomerBusinessType = "All" Then
                                        Me.LabelBusinessTypeNew.Text = oRowRulesBasicApproval.SourceCustomerBusinessType
                                    Else
                                        Using BusinessTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.BusinessTypeTableAdapter
                                            Using BusinessTypeTable As New AMLDAL.BasicRulesDataSet.BusinessTypeDataTable
                                                BusinessTypeAdapter.Fill(BusinessTypeTable)
                                                If BusinessTypeTable.Rows.Count > 0 Then
                                                    Dim BusinessTypeTableRow As AMLDAL.BasicRulesDataSet.BusinessTypeRow
                                                    BusinessTypeTableRow = BusinessTypeTable.FindByBusinessTypeCode(oRowRulesBasicApproval.SourceCustomerBusinessType)
                                                    If Not BusinessTypeTableRow Is Nothing Then
                                                        If Not BusinessTypeTableRow.IsBusinessTypeDescriptionNull Then
                                                            Me.LabelBusinessTypeNew.Text = BusinessTypeTableRow.BusinessTypeDescription
                                                        End If
                                                    End If
                                                End If
                                            End Using
                                        End Using
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceCustomerInternalIndustryCodeNull Then
                                    If oRowRulesBasicApproval.SourceCustomerInternalIndustryCode = "All" Then
                                        Me.LabelInternalIndustriCodeNew.Text = oRowRulesBasicApproval.SourceCustomerInternalIndustryCode
                                    Else
                                        Using InternalIndustryCodeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.InternalIndustryCodeTableAdapter
                                            Using InternalIndustryCodeTable As New AMLDAL.BasicRulesDataSet.InternalIndustryCodeDataTable
                                                InternalIndustryCodeAdapter.Fill(InternalIndustryCodeTable)
                                                If InternalIndustryCodeTable.Rows.Count > 0 Then
                                                    Dim InternalIndustryCodeRow As AMLDAL.BasicRulesDataSet.InternalIndustryCodeRow
                                                    InternalIndustryCodeRow = InternalIndustryCodeTable.FindByInternalIndustryCode(oRowRulesBasicApproval.SourceCustomerInternalIndustryCode)
                                                    If Not InternalIndustryCodeRow Is Nothing Then
                                                        If Not InternalIndustryCodeRow.IsInternalIndustryDescriptionNull Then
                                                            Me.LabelInternalIndustriCodeNew.Text = InternalIndustryCodeRow.InternalIndustryDescription
                                                        End If
                                                    End If
                                                End If
                                            End Using
                                        End Using
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceAccountOpenedWithinNumberNull Then
                                    If oRowRulesBasicApproval.SourceAccountOpenedWithinNumber <> "" Then
                                        Me.LabelOpenedWithNew.Text = oRowRulesBasicApproval.SourceAccountOpenedWithinNumber
                                        If Not oRowRulesBasicApproval.IsSourceAccountOpenedWithinTypeNull Then
                                            Select Case oRowRulesBasicApproval.SourceAccountOpenedWithinType
                                                Case 1
                                                    Me.LabelOpenedWithNew.Text = Me.LabelOpenedWithNew.Text & " Day(s)"
                                                Case 2
                                                    Me.LabelOpenedWithNew.Text = Me.LabelOpenedWithNew.Text & " Month(s)"
                                                Case 3
                                                    Me.LabelOpenedWithNew.Text = Me.LabelOpenedWithNew.Text & " Year(s)"
                                            End Select
                                        End If
                                    Else
                                        Me.LabelOpenedWithNew.Text = "All"
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceAccountProductTypeNull Then
                                    Dim StrAccountProductType As String
                                    Dim ArrAccountProductType As String()
                                    Dim StrAccountProductTypeName As String = ""
                                    Dim Counter As Integer
                                    StrAccountProductType = oRowRulesBasicApproval.SourceAccountProductType
                                    If StrAccountProductType <> "" Then
                                        ArrAccountProductType = StrAccountProductType.Split(",")
                                        Using AccountTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.AccountTypeTableAdapter
                                            Using AccountTypeTable As New AMLDAL.BasicRulesDataSet.AccountTypeDataTable
                                                AccountTypeAdapter.Fill(AccountTypeTable)
                                                If AccountTypeTable.Rows.Count > 0 Then
                                                    Dim AccountTypeTableRow As AMLDAL.BasicRulesDataSet.AccountTypeRow
                                                    For Counter = 0 To ArrAccountProductType.Length - 1
                                                        AccountTypeTableRow = AccountTypeTable.FindByAccountTypeCode(ArrAccountProductType(Counter))
                                                        If Not AccountTypeTableRow Is Nothing Then
                                                            If Not AccountTypeTableRow.IsAccountTypeDescriptionNull Then
                                                                StrAccountProductTypeName = StrAccountProductTypeName & AccountTypeTableRow.AccountTypeDescription & ", "
                                                            End If
                                                        End If
                                                    Next
                                                End If
                                            End Using
                                        End Using
                                        If StrAccountProductTypeName.Length > 0 Then
                                            StrAccountProductTypeName = StrAccountProductTypeName.Remove(StrAccountProductTypeName.Length - 2)
                                        End If
                                    Else
                                        StrAccountProductTypeName = "All"
                                    End If

                                    Me.LabelAccountTypeNew.Text = StrAccountProductTypeName
                                End If
                                If Not oRowRulesBasicApproval.IsSourceAccountSegmentationNull Then
                                    Me.LabelSegmentationNew.Text = oRowRulesBasicApproval.SourceAccountSegmentation
                                End If
                                If Not oRowRulesBasicApproval.IsSourceAccountStatusNull Then
                                    If oRowRulesBasicApproval.SourceAccountStatus = "A" Then
                                        Me.LabelAccountStatusNew.Text = "All"
                                    Else
                                        Using AccountStatusAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.AccountStatusTableAdapter
                                            Using AccountStatusTable As New AMLDAL.BasicRulesDataSet.AccountStatusDataTable
                                                AccountStatusAdapter.Fill(AccountStatusTable)
                                                If AccountStatusTable.Rows.Count > 0 Then
                                                    Dim AccountStatusTableRow As AMLDAL.BasicRulesDataSet.AccountStatusRow
                                                    AccountStatusTableRow = AccountStatusTable.FindByAccountStatusCode(oRowRulesBasicApproval.SourceAccountStatus)
                                                    If Not AccountStatusTableRow Is Nothing Then
                                                        If Not AccountStatusTableRow.IsAccountStatusDescriptionNull Then
                                                            Me.LabelAccountStatusNew.Text = AccountStatusTableRow.AccountStatusDescription
                                                        End If
                                                    End If
                                                End If
                                            End Using
                                        End Using
                                    End If
                                End If

                                'info transaction
                                If Not oRowRulesBasicApproval.IsTransactionTypeNull Then
                                    If oRowRulesBasicApproval.TransactionType = 0 Then
                                        Me.LabelTransactionTypeNew.Text = "All"
                                    Else
                                        Using TransactionChannelTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.TransactionChannelTypeTableAdapter
                                            Using TransactionChannelTypeTable As New AMLDAL.BasicRulesDataSet.TransactionChannelTypeDataTable
                                                TransactionChannelTypeAdapter.Fill(TransactionChannelTypeTable)
                                                If TransactionChannelTypeTable.Rows.Count > 0 Then
                                                    Dim TransactionChannelTypeTableRow As AMLDAL.BasicRulesDataSet.TransactionChannelTypeRow
                                                    TransactionChannelTypeTableRow = TransactionChannelTypeTable.FindByPK_TransactionChannelType(oRowRulesBasicApproval.TransactionType)
                                                    If Not TransactionChannelTypeTableRow Is Nothing Then
                                                        If Not TransactionChannelTypeTableRow.IsTransactionChannelTypeNameNull Then
                                                            Me.LabelTransactionTypeNew.Text = TransactionChannelTypeTableRow.TransactionChannelTypeName
                                                        End If
                                                    End If
                                                End If
                                            End Using
                                        End Using
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsAuxiliaryTransactionCodeNull Then
                                    If oRowRulesBasicApproval.AuxiliaryTransactionCode <> "" Then
                                        Dim StrAuxiliaryTransactionCodeIN As String = ""
                                        If Not oRowRulesBasicApproval.IsAuxiliaryTransactionCodeIsINNull Then

                                            If oRowRulesBasicApproval.AuxiliaryTransactionCodeIsIN Then
                                                StrAuxiliaryTransactionCodeIN = "IN"
                                            Else
                                                StrAuxiliaryTransactionCodeIN = "NOT IN"
                                            End If
                                        End If
                                        Me.LabelAuxiliaryNew.Text = StrAuxiliaryTransactionCodeIN & " " & oRowRulesBasicApproval.AuxiliaryTransactionCode
                                    Else
                                        Me.LabelAuxiliaryNew.Text = "All"
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsTransactionAmountFromTypeNull Then
                                    If oRowRulesBasicApproval.TransactionAmountFromType = 0 Then
                                        Me.LabelAmountNew.Text = "Unspecified"
                                    Else
                                        Me.LabelAmountNew.Text = "Value"
                                        If Not oRowRulesBasicApproval.IsTransactionAmountFromNull Then
                                            Me.LabelAmountNew.Text = Me.LabelAmountNew.Text & " " & oRowRulesBasicApproval.TransactionAmountFrom
                                        End If
                                    End If
                                End If

                                Me.LabelAmountNew.Text = Me.LabelAmountNew.Text & " - "
                                If Not oRowRulesBasicApproval.IsTransactionAmountToTypeNull Then
                                    If oRowRulesBasicApproval.TransactionAmountToType = 0 Then
                                        Me.LabelAmountNew.Text = Me.LabelAmountNew.Text & " Unspecified"
                                    Else
                                        Me.LabelAmountNew.Text = Me.LabelAmountNew.Text & " Value"
                                        If Not oRowRulesBasicApproval.IsTransactionAmountToNull Then
                                            Me.LabelAmountNew.Text = Me.LabelAmountNew.Text & " " & oRowRulesBasicApproval.TransactionAmountTo
                                        End If
                                    End If
                                End If

                                If Not oRowRulesBasicApproval.IsTransactionIsAmountTransactionGELimitTransactionNull Then
                                    Me.LabelCheckAmountNew.Text = oRowRulesBasicApproval.TransactionIsAmountTransactionGELimitTransaction.ToString()
                                End If
                                If Not oRowRulesBasicApproval.IsTransactionCreditDebitTypeNull Then
                                    Select Case oRowRulesBasicApproval.TransactionCreditDebitType
                                        Case "C"
                                            Me.LabelCreditDebitNew.Text = "Credit"
                                        Case "D"
                                            Me.LabelCreditDebitNew.Text = "Debit"
                                        Case Else
                                            Me.LabelCreditDebitNew.Text = "All"
                                    End Select
                                End If
                                If Not oRowRulesBasicApproval.IsTransactionFrequencyIsEnabledNull Then
                                    If oRowRulesBasicApproval.TransactionFrequencyIsEnabled Then
                                        If Not oRowRulesBasicApproval.IsTransactionFrequencyNull Then
                                            Me.LabelTransactionFrequencyNew.Text = oRowRulesBasicApproval.TransactionFrequency & " time(s)"
                                        End If
                                    Else
                                        Me.LabelTransactionFrequencyNew.Text = "All"
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsTransactionPeriodIsEnabledNull Then
                                    If oRowRulesBasicApproval.TransactionPeriodIsEnabled Then
                                        If Not oRowRulesBasicApproval.IsTransactionPeriodNumberNull Then
                                            Me.LabelTransactionPeriodNew.Text = oRowRulesBasicApproval.TransactionPeriodNumber
                                        End If
                                        If Not oRowRulesBasicApproval.IsTransactionPeriodTypeNull Then
                                            Select Case oRowRulesBasicApproval.TransactionPeriodType
                                                Case 1
                                                    Me.LabelTransactionPeriodNew.Text = Me.LabelTransactionPeriodNew.Text & " Day(s)"
                                                Case 2
                                                    Me.LabelTransactionPeriodNew.Text = Me.LabelTransactionPeriodNew.Text & " Month(s)"
                                                Case 3
                                                    Me.LabelTransactionPeriodNew.Text = Me.LabelTransactionPeriodNew.Text & " Year(s)"
                                            End Select
                                        End If
                                    Else
                                        Me.LabelTransactionPeriodNew.Text = "All"
                                    End If
                                End If

                                If Not oRowRulesBasicApproval.IsCountryCodeNull Then
                                    If oRowRulesBasicApproval.CountryCode <> "" Then
                                        Dim StrCountryCodeIN As String = ""
                                        If Not oRowRulesBasicApproval.IsCountryCodeIsINNull Then
                                            If oRowRulesBasicApproval.CountryCodeIsIN Then
                                                StrCountryCodeIN = "IN"
                                            Else
                                                StrCountryCodeIN = "NOT IN"
                                            End If
                                        End If
                                        Me.LabelCountryCodeNew.Text = StrCountryCodeIN & " " & oRowRulesBasicApproval.CountryCode
                                    Else
                                        Me.LabelCountryCodeNew.Text = "All"
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceFundIsOneCIFAndRelatedNull Then
                                    Me.LabelCIFRelatedNew.Text = oRowRulesBasicApproval.SourceFundIsOneCIFAndRelated.ToString()
                                End If


                                ' -- OLD VALUE --
                                'Info Basic Rules
                                Me.LabelBasicRulesNameOld.Text = oRowRulesBasicApproval.RulesBasicName_old
                                If Not oRowRulesBasicApproval.IsRulesBasicDescription_oldNull Then
                                    Me.LabelBasicRulesDescriptionOld.Text = oRowRulesBasicApproval.RulesBasicDescription_old
                                End If
                                If Not oRowRulesBasicApproval.IsSTRAlertType_oldNull Then
                                    Using STRAlertTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.STRAlertTypeTableAdapter
                                        Using STRAlertTypeTable As New AMLDAL.BasicRulesDataSet.STRAlertTypeDataTable
                                            STRAlertTypeAdapter.Fill(STRAlertTypeTable)
                                            If STRAlertTypeTable.Rows.Count > 0 Then
                                                Dim STRAlertTypeTableRow As AMLDAL.BasicRulesDataSet.STRAlertTypeRow
                                                STRAlertTypeTableRow = STRAlertTypeTable.FindBySTRAlertType(oRowRulesBasicApproval.STRAlertType_old)
                                                If Not STRAlertTypeTableRow Is Nothing Then
                                                    If Not STRAlertTypeTableRow.IsSTRAlertTypeNameNull Then
                                                        Me.LabelBasicRulesAlertToOld.Text = STRAlertTypeTableRow.STRAlertTypeName
                                                    End If
                                                End If
                                            End If
                                        End Using
                                    End Using
                                End If
                                If Not oRowRulesBasicApproval.IsIsEnabled_oldNull Then
                                    Me.LabelBasicRulesEnableOld.Text = oRowRulesBasicApproval.IsEnabled_old
                                End If
                                'info customer
                                If Not oRowRulesBasicApproval.IsSourceCustomerOf_oldNull Then
                                    Me.LabelCustomerOfOld.Text = oRowRulesBasicApproval.SourceCustomerOf_old
                                End If
                                If Not oRowRulesBasicApproval.IsSourceCustomerType_oldNull Then
                                    If oRowRulesBasicApproval.SourceCustomerType_old = "A" Then
                                        Me.LabelCustomerTypeOld.Text = "Personal"
                                    Else
                                        If oRowRulesBasicApproval.SourceCustomerType_old = "B" Then
                                            Me.LabelCustomerTypeOld.Text = "Company"
                                        Else
                                            Me.LabelCustomerTypeOld.Text = oRowRulesBasicApproval.SourceCustomerType_old
                                        End If
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceCustomerNegativeListCategoryId_oldNull Then
                                    Dim StrCustomerNegativeListCategoryId As String
                                    Dim ArrCustomerNegativeListCategoryId As String()
                                    Dim StrCustomerNegativeListCategoryName As String = ""
                                    Dim Counter As Integer
                                    StrCustomerNegativeListCategoryId = oRowRulesBasicApproval.SourceCustomerNegativeListCategoryId_old
                                    If StrCustomerNegativeListCategoryId <> "" Then
                                        ArrCustomerNegativeListCategoryId = StrCustomerNegativeListCategoryId.Split(",")
                                        Using NegativeListCategoryAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.VerificationListCategoryTableAdapter
                                            Using NegativeListCategoryTable As New AMLDAL.BasicRulesDataSet.VerificationListCategoryDataTable
                                                NegativeListCategoryAdapter.Fill(NegativeListCategoryTable)
                                                If NegativeListCategoryTable.Rows.Count > 0 Then
                                                    Dim NegativeListCategoryTableRow As AMLDAL.BasicRulesDataSet.VerificationListCategoryRow
                                                    For Counter = 0 To ArrCustomerNegativeListCategoryId.Length - 1
                                                        NegativeListCategoryTableRow = NegativeListCategoryTable.FindByCategoryID(ArrCustomerNegativeListCategoryId(Counter).Trim())
                                                        If Not NegativeListCategoryTableRow Is Nothing Then
                                                            If Not NegativeListCategoryTableRow.IsCategoryNameNull Then
                                                                StrCustomerNegativeListCategoryName = StrCustomerNegativeListCategoryName & NegativeListCategoryTableRow.CategoryName & ", "
                                                            End If
                                                        End If
                                                    Next
                                                End If
                                            End Using
                                        End Using
                                        If StrCustomerNegativeListCategoryName.Length > 0 Then
                                            StrCustomerNegativeListCategoryName = StrCustomerNegativeListCategoryName.Remove(StrCustomerNegativeListCategoryName.Length - 2)
                                        End If
                                    Else
                                        StrCustomerNegativeListCategoryName = "None"
                                    End If
                                    Me.LabelNegativeListOld.Text = StrCustomerNegativeListCategoryName
                                End If
                                Me.LabelHighRiskBusinessOld.Text = Me.oRowRulesBasicApproval.SourceCustomerIsHighRiskBusiness_old.ToString()
                                Me.LabelHighRiskCountryOld.Text = Me.oRowRulesBasicApproval.SourceCustomerIsHighRiskCountry_old.ToString()
                                If Not oRowRulesBasicApproval.IsSourceCustomerSubType_oldNull Then
                                    If oRowRulesBasicApproval.SourceCustomerSubType_old = "All" Then
                                        Me.LabelCustomerSubTypeOld.Text = oRowRulesBasicApproval.SourceCustomerSubType_old
                                    Else
                                        Using CustomerSubTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.CustomerSubTypeTableAdapter
                                            Using CustomerSubTypeTable As New AMLDAL.BasicRulesDataSet.CustomerSubTypeDataTable
                                                CustomerSubTypeAdapter.Fill(CustomerSubTypeTable)
                                                If CustomerSubTypeTable.Rows.Count > 0 Then
                                                    Dim CustomerSubTypeTableRow As AMLDAL.BasicRulesDataSet.CustomerSubTypeRow
                                                    CustomerSubTypeTableRow = CustomerSubTypeTable.FindByCustomerSubTypeCode(oRowRulesBasicApproval.SourceCustomerSubType_old)
                                                    If Not CustomerSubTypeTableRow Is Nothing Then
                                                        If Not CustomerSubTypeTableRow.IsCustomerSubTypeDescriptionNull Then
                                                            Me.LabelCustomerSubTypeOld.Text = CustomerSubTypeTableRow.CustomerSubTypeDescription
                                                        End If
                                                    End If
                                                End If
                                            End Using
                                        End Using
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceCustomerBusinessType_oldNull Then
                                    If oRowRulesBasicApproval.SourceCustomerBusinessType_old = "All" Then
                                        Me.LabelBusinessTypeOld.Text = oRowRulesBasicApproval.SourceCustomerBusinessType_old
                                    Else
                                        Using BusinessTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.BusinessTypeTableAdapter
                                            Using BusinessTypeTable As New AMLDAL.BasicRulesDataSet.BusinessTypeDataTable
                                                BusinessTypeAdapter.Fill(BusinessTypeTable)
                                                If BusinessTypeTable.Rows.Count > 0 Then
                                                    Dim BusinessTypeTableRow As AMLDAL.BasicRulesDataSet.BusinessTypeRow
                                                    BusinessTypeTableRow = BusinessTypeTable.FindByBusinessTypeCode(oRowRulesBasicApproval.SourceCustomerBusinessType_old)
                                                    If Not BusinessTypeTableRow Is Nothing Then
                                                        If Not BusinessTypeTableRow.IsBusinessTypeDescriptionNull Then
                                                            Me.LabelBusinessTypeOld.Text = BusinessTypeTableRow.BusinessTypeDescription
                                                        End If
                                                    End If
                                                End If
                                            End Using
                                        End Using
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceCustomerInternalIndustryCode_oldNull Then
                                    If oRowRulesBasicApproval.SourceCustomerInternalIndustryCode_old = "All" Then
                                        Me.LabelInternalIndustriCodeOld.Text = oRowRulesBasicApproval.SourceCustomerInternalIndustryCode_old
                                    Else
                                        Using InternalIndustryCodeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.InternalIndustryCodeTableAdapter
                                            Using InternalIndustryCodeTable As New AMLDAL.BasicRulesDataSet.InternalIndustryCodeDataTable
                                                InternalIndustryCodeAdapter.Fill(InternalIndustryCodeTable)
                                                If InternalIndustryCodeTable.Rows.Count > 0 Then
                                                    Dim InternalIndustryCodeRow As AMLDAL.BasicRulesDataSet.InternalIndustryCodeRow
                                                    InternalIndustryCodeRow = InternalIndustryCodeTable.FindByInternalIndustryCode(oRowRulesBasicApproval.SourceCustomerInternalIndustryCode_old)
                                                    If Not InternalIndustryCodeRow Is Nothing Then
                                                        If Not InternalIndustryCodeRow.IsInternalIndustryDescriptionNull Then
                                                            Me.LabelInternalIndustriCodeOld.Text = InternalIndustryCodeRow.InternalIndustryDescription
                                                        End If
                                                    End If
                                                End If
                                            End Using
                                        End Using
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceAccountOpenedWithinNumber_oldNull Then
                                    If oRowRulesBasicApproval.SourceAccountOpenedWithinNumber_old <> "" Then
                                        Me.LabelOpenedWithOld.Text = oRowRulesBasicApproval.SourceAccountOpenedWithinNumber_old
                                        If Not oRowRulesBasicApproval.IsSourceAccountOpenedWithinType_oldNull Then
                                            Select Case oRowRulesBasicApproval.SourceAccountOpenedWithinType_old
                                                Case 1
                                                    Me.LabelOpenedWithOld.Text = Me.LabelOpenedWithOld.Text & " Day(s)"
                                                Case 2
                                                    Me.LabelOpenedWithOld.Text = Me.LabelOpenedWithOld.Text & " Month(s)"
                                                Case 3
                                                    Me.LabelOpenedWithOld.Text = Me.LabelOpenedWithOld.Text & " Year(s)"
                                            End Select
                                        End If
                                    Else
                                        Me.LabelOpenedWithOld.Text = "All"
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceAccountProductType_oldNull Then
                                    Dim StrAccountProductType As String
                                    Dim ArrAccountProductType As String()
                                    Dim StrAccountProductTypeName As String = ""
                                    Dim Counter As Integer
                                    StrAccountProductType = oRowRulesBasicApproval.SourceAccountProductType_old
                                    If StrAccountProductType <> "" Then
                                        ArrAccountProductType = StrAccountProductType.Split(",")
                                        Using AccountTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.AccountTypeTableAdapter
                                            Using AccountTypeTable As New AMLDAL.BasicRulesDataSet.AccountTypeDataTable
                                                AccountTypeAdapter.Fill(AccountTypeTable)
                                                If AccountTypeTable.Rows.Count > 0 Then
                                                    Dim AccountTypeTableRow As AMLDAL.BasicRulesDataSet.AccountTypeRow
                                                    For Counter = 0 To ArrAccountProductType.Length - 1
                                                        AccountTypeTableRow = AccountTypeTable.FindByAccountTypeCode(ArrAccountProductType(Counter))
                                                        If Not AccountTypeTableRow Is Nothing Then
                                                            If Not AccountTypeTableRow.IsAccountTypeDescriptionNull Then
                                                                StrAccountProductTypeName = StrAccountProductTypeName & AccountTypeTableRow.AccountTypeDescription & ", "
                                                            End If
                                                        End If
                                                    Next
                                                End If
                                            End Using
                                        End Using
                                        If StrAccountProductTypeName.Length > 0 Then
                                            StrAccountProductTypeName = StrAccountProductTypeName.Remove(StrAccountProductTypeName.Length - 2)
                                        End If
                                    Else
                                        StrAccountProductTypeName = "All"
                                    End If

                                    Me.LabelAccountTypeOld.Text = StrAccountProductTypeName
                                End If
                                If Not oRowRulesBasicApproval.IsSourceAccountSegmentation_oldNull Then
                                    Me.LabelSegmentationOld.Text = oRowRulesBasicApproval.SourceAccountSegmentation_old
                                End If
                                If Not oRowRulesBasicApproval.IsSourceAccountStatus_oldNull Then
                                    If oRowRulesBasicApproval.SourceAccountStatus_old = "A" Then
                                        Me.LabelAccountStatusOld.Text = "All"
                                    Else
                                        Using AccountStatusAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.AccountStatusTableAdapter
                                            Using AccountStatusTable As New AMLDAL.BasicRulesDataSet.AccountStatusDataTable
                                                AccountStatusAdapter.Fill(AccountStatusTable)
                                                If AccountStatusTable.Rows.Count > 0 Then
                                                    Dim AccountStatusTableRow As AMLDAL.BasicRulesDataSet.AccountStatusRow
                                                    AccountStatusTableRow = AccountStatusTable.FindByAccountStatusCode(oRowRulesBasicApproval.SourceAccountStatus_old)
                                                    If Not AccountStatusTableRow Is Nothing Then
                                                        If Not AccountStatusTableRow.IsAccountStatusDescriptionNull Then
                                                            Me.LabelAccountStatusOld.Text = AccountStatusTableRow.AccountStatusDescription
                                                        End If
                                                    End If
                                                End If
                                            End Using
                                        End Using
                                    End If
                                End If

                                'info transaction
                                If Not oRowRulesBasicApproval.IsTransactionType_oldNull Then
                                    If oRowRulesBasicApproval.TransactionType_old = 0 Then
                                        Me.LabelTransactionTypeOld.Text = "All"
                                    Else
                                        Using TransactionChannelTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.TransactionChannelTypeTableAdapter
                                            Using TransactionChannelTypeTable As New AMLDAL.BasicRulesDataSet.TransactionChannelTypeDataTable
                                                TransactionChannelTypeAdapter.Fill(TransactionChannelTypeTable)
                                                If TransactionChannelTypeTable.Rows.Count > 0 Then
                                                    Dim TransactionChannelTypeTableRow As AMLDAL.BasicRulesDataSet.TransactionChannelTypeRow
                                                    TransactionChannelTypeTableRow = TransactionChannelTypeTable.FindByPK_TransactionChannelType(oRowRulesBasicApproval.TransactionType_old)
                                                    If Not TransactionChannelTypeTableRow Is Nothing Then
                                                        If Not TransactionChannelTypeTableRow.IsTransactionChannelTypeNameNull Then
                                                            Me.LabelTransactionTypeOld.Text = TransactionChannelTypeTableRow.TransactionChannelTypeName
                                                        End If
                                                    End If
                                                End If
                                            End Using
                                        End Using
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsAuxiliaryTransactionCode_oldNull Then
                                    If oRowRulesBasicApproval.AuxiliaryTransactionCode_old <> "" Then
                                        Dim StrAuxiliaryTransactionCodeIN As String = ""
                                        If Not oRowRulesBasicApproval.IsAuxiliaryTransactionCodeIsIN_oldNull Then

                                            If oRowRulesBasicApproval.AuxiliaryTransactionCodeIsIN_old Then
                                                StrAuxiliaryTransactionCodeIN = "IN"
                                            Else
                                                StrAuxiliaryTransactionCodeIN = "NOT IN"
                                            End If
                                        End If
                                        Me.LabelAuxiliaryOld.Text = StrAuxiliaryTransactionCodeIN & " " & oRowRulesBasicApproval.AuxiliaryTransactionCode_old
                                    Else
                                        Me.LabelAuxiliaryOld.Text = "All"
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsTransactionAmountFromType_oldNull Then
                                    If oRowRulesBasicApproval.TransactionAmountFromType_old = 0 Then
                                        Me.LabelAmountOld.Text = "Unspecified"
                                    Else
                                        Me.LabelAmountOld.Text = "Value"
                                        If Not oRowRulesBasicApproval.IsTransactionAmountFrom_oldNull Then
                                            Me.LabelAmountOld.Text = Me.LabelAmountOld.Text & " " & oRowRulesBasicApproval.TransactionAmountFrom_old
                                        End If
                                    End If
                                End If

                                Me.LabelAmountOld.Text = Me.LabelAmountOld.Text & " - "
                                If Not oRowRulesBasicApproval.IsTransactionAmountToType_oldNull Then
                                    If oRowRulesBasicApproval.TransactionAmountToType_old = 0 Then
                                        Me.LabelAmountOld.Text = Me.LabelAmountOld.Text & " Unspecified"
                                    Else
                                        Me.LabelAmountOld.Text = Me.LabelAmountOld.Text & " Value"
                                        If Not oRowRulesBasicApproval.IsTransactionAmountTo_oldNull Then
                                            Me.LabelAmountOld.Text = Me.LabelAmountOld.Text & " " & oRowRulesBasicApproval.TransactionAmountTo_old
                                        End If
                                    End If
                                End If

                                If Not oRowRulesBasicApproval.IsTransactionIsAmountTransactionGELimitTransaction_oldNull Then
                                    Me.LabelCheckAmountOld.Text = oRowRulesBasicApproval.TransactionIsAmountTransactionGELimitTransaction_old.ToString()
                                End If
                                If Not oRowRulesBasicApproval.IsTransactionCreditDebitType_oldNull Then
                                    Select Case oRowRulesBasicApproval.TransactionCreditDebitType_old
                                        Case "C"
                                            Me.LabelCreditDebitOld.Text = "Credit"
                                        Case "D"
                                            Me.LabelCreditDebitOld.Text = "Debit"
                                        Case Else
                                            Me.LabelCreditDebitOld.Text = "All"
                                    End Select
                                End If
                                If Not oRowRulesBasicApproval.IsTransactionFrequencyIsEnabled_oldNull Then
                                    If oRowRulesBasicApproval.TransactionFrequencyIsEnabled_old Then
                                        If Not oRowRulesBasicApproval.IsTransactionFrequency_oldNull Then
                                            Me.LabelTransactionFrequencyOld.Text = oRowRulesBasicApproval.TransactionFrequency_old & " time(s)"
                                        End If
                                    Else
                                        Me.LabelTransactionFrequencyOld.Text = "All"
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsTransactionPeriodIsEnabled_oldNull Then
                                    If oRowRulesBasicApproval.TransactionPeriodIsEnabled_old Then
                                        If Not oRowRulesBasicApproval.IsTransactionPeriodNumber_oldNull Then
                                            Me.LabelTransactionPeriodOld.Text = oRowRulesBasicApproval.TransactionPeriodNumber_old
                                        End If
                                        If Not oRowRulesBasicApproval.IsTransactionPeriodType_oldNull Then
                                            Select Case oRowRulesBasicApproval.TransactionPeriodType_old
                                                Case 1
                                                    Me.LabelTransactionPeriodOld.Text = Me.LabelTransactionPeriodOld.Text & " Day(s)"
                                                Case 2
                                                    Me.LabelTransactionPeriodOld.Text = Me.LabelTransactionPeriodOld.Text & " Month(s)"
                                                Case 3
                                                    Me.LabelTransactionPeriodOld.Text = Me.LabelTransactionPeriodOld.Text & " Year(s)"
                                            End Select
                                        End If
                                    Else
                                        Me.LabelTransactionPeriodOld.Text = "All"
                                    End If
                                End If

                                If Not oRowRulesBasicApproval.IsCountryCode_oldNull Then
                                    If oRowRulesBasicApproval.CountryCode_old <> "" Then
                                        Dim StrCountryCodeIN As String = ""
                                        If Not oRowRulesBasicApproval.IsCountryCodeIsIN_oldNull Then
                                            If oRowRulesBasicApproval.CountryCodeIsIN_old Then
                                                StrCountryCodeIN = "IN"
                                            Else
                                                StrCountryCodeIN = "NOT IN"
                                            End If
                                        End If
                                        Me.LabelCountryCodeOld.Text = StrCountryCodeIN & " " & oRowRulesBasicApproval.CountryCode_old
                                    Else
                                        Me.LabelCountryCodeOld.Text = "All"
                                    End If
                                End If
                                If Not oRowRulesBasicApproval.IsSourceFundIsOneCIFAndRelated_oldNull Then
                                    Me.LabelCIFRelatedOld.Text = oRowRulesBasicApproval.SourceFundIsOneCIFAndRelated_old.ToString()
                                End If
                        End Select
                    End If
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property
#End Region

#Region "Accept"
    Private Sub AcceptAdd()
        ''Dim oSQLTrans As SqlTransaction
        'Try
        '    Using adapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasicTableAdapter
        '        'UNDONE:belum selesai
        '        'adapter.Insert (oRowRulesBasicApproval.RulesBasicName ,oRowRulesBasicApproval.RulesBasicDescription ,oRowRulesBasicApproval.SourceCustomerOf,oRowRulesBasicApproval.SourceCustomerType ,oRowRulesBasicApproval.sourcecusto
        '    End Using
        '    'oSQLTrans.Commit()
        'Catch ex As Exception
        '    'If Not oSQLTrans Is Nothing Then
        '    '    oSQLTrans.Rollback()
        '    'End If
        'End Try
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using RulesBasicAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasicTableAdapter
                Dim objds As Data.DataSet = Sahassa.AML.Commonly.ExecuteDataset("SELECT rbpa.RulesBasic_PendingApprovalRulesBasicUserID FROM RulesBasic_PendingApproval rbpa WHERE rbpa.RulesBasic_PendingApprovalID=" & Me.PKRulesBasicPendingApprovalID)
                Dim userid As String = objds.Tables(0).Rows(0).Item(0)

                oSQLTrans = BeginTransaction(RulesBasicAdapter)
                Using RulesBasicApprovalAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasicApprovalTableAdapter
                    SetTransaction(RulesBasicApprovalAdapter, oSQLTrans)
                    Using RulesBasicApprovalTable As New AMLDAL.BasicRulesDataSet.RulesBasicApprovalDataTable
                        RulesBasicApprovalAdapter.FillByPendingApprovalId(RulesBasicApprovalTable, Me.PKRulesBasicPendingApprovalID)
                        'Bila RulesBasic_ApprovalTable.Rows.Count > 0 berarti Basic Rule tsb masih ada dlm tabel RulesBasic_Approval
                        If RulesBasicApprovalTable.Rows.Count > 0 Then
                            Dim RulesBasicApprovalTableRow As AMLDAL.BasicRulesDataSet.RulesBasicApprovalRow
                            RulesBasicApprovalTableRow = RulesBasicApprovalTable.Rows(0)


                            'Periksa apakah Basic Rule Name yg baru tsb sudah ada dlm tabel Basic Rule atau belum. 
                            Dim Counter As Integer
                            Using RulesBasicQueryAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.QueriesTableAdapter
                                Counter = RulesBasicQueryAdapter.RulesBasicGetCountByName(RulesBasicApprovalTableRow.RulesBasicName)
                            End Using


                            'Bila counter = 0 berarti RulesBasicName tsb belum ada dlm tabel RulesBasic, maka boleh ditambahkan
                            If Counter = 0 Then
                                'tambahkan item tersebut dalam tabel RulesBasic
                                RulesBasicAdapter.Insert(RulesBasicApprovalTableRow.RulesBasicName, RulesBasicApprovalTableRow.RulesBasicDescription, RulesBasicApprovalTableRow.STRAlertType, _
                                                        RulesBasicApprovalTableRow.IsEnabled, RulesBasicApprovalTableRow.SourceCustomerOf, RulesBasicApprovalTableRow.SourceCustomerType, _
                                                        RulesBasicApprovalTableRow.SourceCustomerNegativeListCategoryId, RulesBasicApprovalTableRow.SourceCustomerIsHighRiskBusiness, _
                                                        RulesBasicApprovalTableRow.SourceCustomerIsHighRiskCountry, RulesBasicApprovalTableRow.SourceCustomerSubType, RulesBasicApprovalTableRow.SourceCustomerBusinessType, _
                                                        RulesBasicApprovalTableRow.SourceCustomerInternalIndustryCode, RulesBasicApprovalTableRow.SourceAccountOpenedWithinNumber, _
                                                        RulesBasicApprovalTableRow.SourceAccountOpenedWithinType, RulesBasicApprovalTableRow.SourceAccountProductType, RulesBasicApprovalTableRow.SourceAccountStatus, _
                                                        RulesBasicApprovalTableRow.SourceAccountSegmentation, RulesBasicApprovalTableRow.TransactionType, RulesBasicApprovalTableRow.AuxiliaryTransactionCodeIsIN, _
                                                        RulesBasicApprovalTableRow.AuxiliaryTransactionCode, RulesBasicApprovalTableRow.TransactionAmountFromType, RulesBasicApprovalTableRow.TransactionAmountFrom, _
                                                        RulesBasicApprovalTableRow.TransactionAmountToType, RulesBasicApprovalTableRow.TransactionAmountTo, RulesBasicApprovalTableRow.TransactionIsAmountTransactionGELimitTransaction, _
                                                        RulesBasicApprovalTableRow.TransactionCreditDebitType, RulesBasicApprovalTableRow.TransactionFrequencyIsEnabled, RulesBasicApprovalTableRow.TransactionFrequency, _
                                                        RulesBasicApprovalTableRow.TransactionPeriodIsEnabled, RulesBasicApprovalTableRow.TransactionPeriodNumber, RulesBasicApprovalTableRow.TransactionPeriodType, _
                                                        RulesBasicApprovalTableRow.CountryCodeIsIN, RulesBasicApprovalTableRow.CountryCode, RulesBasicApprovalTableRow.SourceFundIsOneCIFAndRelated, _
                                                        RulesBasicApprovalTableRow.CaseGroupingBy, RulesBasicApprovalTableRow.Expression, RulesBasicApprovalTableRow.LastUpdatedBy, RulesBasicApprovalTableRow.LastUpdatedDate)
                                'catat aktifitas dalam tabel Audit Trail
                                Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
                                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                    SetTransaction(AccessAudit, oSQLTrans)
                                    AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "LastUpdatedDate", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                                    AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "BasicRuleName", "Add", "", RulesBasicApprovalTableRow.RulesBasicName, "Accepted")
                                    AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Description", "Add", "", RulesBasicApprovalTableRow.RulesBasicDescription, "Accepted")
                                    AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "AlertTo", "Add", "", Me.LabelBasicRulesAlertToNew.Text, "Accepted")
                                    AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Enabled", "Add", "", RulesBasicApprovalTableRow.IsEnabled.ToString(), "Accepted")
                                    AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Expression", "Add", "", RulesBasicApprovalTableRow.Expression, "Accepted")
                                End Using
                                'hapus item tersebut dalam tabel RulesBasic_Approval
                                RulesBasicApprovalAdapter.RulesBasic_ApprovalDeleteByPendingApprovalId(RulesBasicApprovalTableRow.FK_RulesBasic_PendingApprovalID)

                                'hapus item tersebut dalam tabel RulesBasic_PendingApproval
                                Using RulesBasic_PendingApprovalAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasic_PendingApprovalTableAdapter
                                    SetTransaction(RulesBasic_PendingApprovalAdapter, oSQLTrans)
                                    RulesBasic_PendingApprovalAdapter.RulesBasic_PendingApprovalDeleteByPendingApprovalID(RulesBasicApprovalTableRow.FK_RulesBasic_PendingApprovalID)
                                End Using
                                oSQLTrans.Commit()
                            Else 'Bila counter != 0 berarti RulesBasic Name tsb sudah ada dlm tabel RulesBasic, maka AcceptRulesBasicAdd gagal
                                Throw New Exception("Cannot add the following Basic Rule : " & RulesBasicApprovalTableRow.RulesBasicName & " because that Basic Rule Name already exists in the database.")
                            End If
                        Else 'Bila ObjTable.Rows.Count = 0 berarti RulesBasic tsb sudah tidak lagi berada dlm tabel RulesBasic_Approval
                            Throw New Exception("Cannot add the following Basic Rule : " & Me.LabelBasicRulesNameNew.Text & " because that Basic Rule is no longer in the approval table.")
                        End If
                    End Using
                End Using
            End Using
            'End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try

    End Sub

    Private Sub AcceptEdit()
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            Using RulesBasicAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasicTableAdapter
                Dim objds As Data.DataSet = Sahassa.AML.Commonly.ExecuteDataset("SELECT rbpa.RulesBasic_PendingApprovalRulesBasicUserID FROM RulesBasic_PendingApproval rbpa WHERE rbpa.RulesBasic_PendingApprovalID=" & Me.PKRulesBasicPendingApprovalID)
                Dim userid As String = objds.Tables(0).Rows(0).Item(0)

                oSQLTrans = BeginTransaction(RulesBasicAdapter)
                Using RulesBasicApprovalAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasicApprovalTableAdapter
                    SetTransaction(RulesBasicApprovalAdapter, oSQLTrans)

                    Using RulesBasicApprovalTable As New AMLDAL.BasicRulesDataSet.RulesBasicApprovalDataTable
                        RulesBasicApprovalAdapter.FillByPendingApprovalId(RulesBasicApprovalTable, Me.PKRulesBasicPendingApprovalID)
                        'Bila RulesBasic_ApprovalTable.Rows.Count > 0 berarti Basic Rule tsb masih ada dlm tabel RulesBasic_Approval
                        If RulesBasicApprovalTable.Rows.Count > 0 Then
                            Dim RulesBasicApprovalTableRow As AMLDAL.BasicRulesDataSet.RulesBasicApprovalRow
                            RulesBasicApprovalTableRow = RulesBasicApprovalTable.Rows(0)

                            'tambahkan item tersebut dalam tabel RulesBasic
                            Using RulesBasicTable As New AMLDAL.BasicRulesDataSet.RulesBasicDataTable
                                RulesBasicAdapter.FillByRulesBasicId(RulesBasicTable, RulesBasicApprovalTableRow.RulesBasicId)
                                If RulesBasicTable.Rows.Count > 0 Then
                                    Dim RulesBasicTableRow As AMLDAL.BasicRulesDataSet.RulesBasicRow
                                    RulesBasicTableRow = RulesBasicTable.Rows(0)
                                    RulesBasicTableRow.RulesBasicName = RulesBasicApprovalTableRow.RulesBasicName
                                    RulesBasicTableRow.RulesBasicDescription = RulesBasicApprovalTableRow.RulesBasicDescription
                                    RulesBasicTableRow.STRAlertType = RulesBasicApprovalTableRow.STRAlertType
                                    RulesBasicTableRow.IsEnabled = RulesBasicApprovalTableRow.IsEnabled
                                    RulesBasicTableRow.SourceCustomerOf = RulesBasicApprovalTableRow.SourceCustomerOf
                                    RulesBasicTableRow.SourceCustomerType = RulesBasicApprovalTableRow.SourceCustomerType
                                    RulesBasicTableRow.SourceCustomerNegativeListCategoryId = RulesBasicApprovalTableRow.SourceCustomerNegativeListCategoryId
                                    RulesBasicTableRow.SourceCustomerIsHighRiskBusiness = RulesBasicApprovalTableRow.SourceCustomerIsHighRiskBusiness
                                    RulesBasicTableRow.SourceCustomerIsHighRiskCountry = RulesBasicApprovalTableRow.SourceCustomerIsHighRiskCountry
                                    RulesBasicTableRow.SourceCustomerSubType = RulesBasicApprovalTableRow.SourceCustomerSubType
                                    RulesBasicTableRow.SourceCustomerBusinessType = RulesBasicApprovalTableRow.SourceCustomerBusinessType
                                    RulesBasicTableRow.SourceCustomerInternalIndustryCode = RulesBasicApprovalTableRow.SourceCustomerInternalIndustryCode
                                    RulesBasicTableRow.SourceAccountOpenedWithinNumber = RulesBasicApprovalTableRow.SourceAccountOpenedWithinNumber
                                    RulesBasicTableRow.SourceAccountOpenedWithinType = RulesBasicApprovalTableRow.SourceAccountOpenedWithinType
                                    RulesBasicTableRow.SourceAccountProductType = RulesBasicApprovalTableRow.SourceAccountProductType
                                    RulesBasicTableRow.SourceAccountStatus = RulesBasicApprovalTableRow.SourceAccountStatus
                                    RulesBasicTableRow.SourceAccountSegmentation = RulesBasicApprovalTableRow.SourceAccountSegmentation
                                    RulesBasicTableRow.TransactionType = RulesBasicApprovalTableRow.TransactionType
                                    RulesBasicTableRow.AuxiliaryTransactionCodeIsIN = RulesBasicApprovalTableRow.AuxiliaryTransactionCodeIsIN
                                    RulesBasicTableRow.AuxiliaryTransactionCode = RulesBasicApprovalTableRow.AuxiliaryTransactionCode
                                    RulesBasicTableRow.TransactionAmountFromType = RulesBasicApprovalTableRow.TransactionAmountFromType
                                    RulesBasicTableRow.TransactionAmountFrom = RulesBasicApprovalTableRow.TransactionAmountFrom
                                    RulesBasicTableRow.TransactionAmountToType = RulesBasicApprovalTableRow.TransactionAmountToType
                                    RulesBasicTableRow.TransactionAmountTo = RulesBasicApprovalTableRow.TransactionAmountTo
                                    RulesBasicTableRow.TransactionIsAmountTransactionGELimitTransaction = RulesBasicApprovalTableRow.TransactionIsAmountTransactionGELimitTransaction
                                    RulesBasicTableRow.TransactionCreditDebitType = RulesBasicApprovalTableRow.TransactionCreditDebitType
                                    RulesBasicTableRow.TransactionFrequencyIsEnabled = RulesBasicApprovalTableRow.TransactionFrequencyIsEnabled
                                    RulesBasicTableRow.TransactionFrequency = RulesBasicApprovalTableRow.TransactionFrequency
                                    RulesBasicTableRow.TransactionPeriodIsEnabled = RulesBasicApprovalTableRow.TransactionPeriodIsEnabled
                                    RulesBasicTableRow.TransactionPeriodNumber = RulesBasicApprovalTableRow.TransactionPeriodNumber
                                    RulesBasicTableRow.TransactionPeriodType = RulesBasicApprovalTableRow.TransactionPeriodType
                                    RulesBasicTableRow.CountryCodeIsIN = RulesBasicApprovalTableRow.CountryCodeIsIN
                                    RulesBasicTableRow.CountryCode = RulesBasicApprovalTableRow.CountryCode
                                    RulesBasicTableRow.SourceFundIsOneCIFAndRelated = RulesBasicApprovalTableRow.SourceFundIsOneCIFAndRelated
                                    RulesBasicTableRow.CaseGroupingBy = RulesBasicApprovalTableRow.CaseGroupingBy
                                    RulesBasicTableRow.Expression = RulesBasicApprovalTableRow.Expression
                                    RulesBasicTableRow.LastUpdatedBy = RulesBasicApprovalTableRow.LastUpdatedBy
                                    RulesBasicTableRow.LastUpdatedDate = RulesBasicApprovalTableRow.LastUpdatedDate
                                    RulesBasicAdapter.Update(RulesBasicTableRow)
                                End If
                            End Using
                            'RulesBasicAdapter.Insert(RulesBasicApprovalTableRow.RulesBasicName, RulesBasicApprovalTableRow.RulesBasicDescription, RulesBasicApprovalTableRow.STRAlertType, _
                            '                        RulesBasicApprovalTableRow.IsEnabled, RulesBasicApprovalTableRow.SourceCustomerOf, RulesBasicApprovalTableRow.SourceCustomerType, _
                            '                        RulesBasicApprovalTableRow.SourceCustomerNegativeListCategoryId, RulesBasicApprovalTableRow.SourceCustomerIsHighRiskBusiness, _
                            '                        RulesBasicApprovalTableRow.SourceCustomerIsHighRiskCountry, RulesBasicApprovalTableRow.SourceCustomerSubType, RulesBasicApprovalTableRow.SourceCustomerBusinessType, _
                            '                        RulesBasicApprovalTableRow.SourceCustomerInternalIndustryCode, RulesBasicApprovalTableRow.SourceAccountOpenedWithinNumber, _
                            '                        RulesBasicApprovalTableRow.SourceAccountOpenedWithinType, RulesBasicApprovalTableRow.SourceAccountProductType, RulesBasicApprovalTableRow.SourceAccountStatus, _
                            '                        RulesBasicApprovalTableRow.SourceAccountSegmentation, RulesBasicApprovalTableRow.TransactionType, RulesBasicApprovalTableRow.AuxiliaryTransactionCodeIsIN, _
                            '                        RulesBasicApprovalTableRow.AuxiliaryTransactionCode, RulesBasicApprovalTableRow.TransactionAmountFromType, RulesBasicApprovalTableRow.TransactionAmountFrom, _
                            '                        RulesBasicApprovalTableRow.TransactionAmountToType, RulesBasicApprovalTableRow.TransactionAmountTo, RulesBasicApprovalTableRow.TransactionIsAmountTransactionGELimitTransaction, _
                            '                        RulesBasicApprovalTableRow.TransactionCreditDebitType, RulesBasicApprovalTableRow.TransactionFrequencyIsEnabled, RulesBasicApprovalTableRow.TransactionFrequency, _
                            '                        RulesBasicApprovalTableRow.TransactionPeriodIsEnabled, RulesBasicApprovalTableRow.TransactionPeriodNumber, RulesBasicApprovalTableRow.TransactionPeriodType, _
                            '                        RulesBasicApprovalTableRow.CountryCodeIsIN, RulesBasicApprovalTableRow.CountryCode, RulesBasicApprovalTableRow.SourceFundIsOneCIFAndRelated, _
                            '                        RulesBasicApprovalTableRow.CaseGroupingBy, RulesBasicApprovalTableRow.Expression, RulesBasicApprovalTableRow.LastUpdatedBy, RulesBasicApprovalTableRow.LastUpdatedDate)
                            'catat aktifitas dalam tabel Audit Trail
                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                SetTransaction(AccessAudit, oSQLTrans)
                                Dim LastUpdatedDate_Old As Date = Now()
                                Dim LastUpdatedDate As Date = Now()
                                Dim StrExpression_Old As String = ""
                                Dim StrExpression As String = ""
                                If Not RulesBasicApprovalTableRow.IsLastUpdatedDate_oldNull Then
                                    LastUpdatedDate_Old = RulesBasicApprovalTableRow.LastUpdatedDate_old
                                End If
                                If Not RulesBasicApprovalTableRow.IsLastUpdatedDateNull Then
                                    LastUpdatedDate = RulesBasicApprovalTableRow.LastUpdatedDate
                                End If
                                If Not RulesBasicApprovalTableRow.IsExpression_oldNull Then
                                    StrExpression_Old = RulesBasicApprovalTableRow.Expression_old
                                End If
                                If Not RulesBasicApprovalTableRow.IsExpressionNull Then
                                    StrExpression = RulesBasicApprovalTableRow.Expression
                                End If

                                AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "LastUpdatedDate", "Edit", LastUpdatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), LastUpdatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                                AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "BasicRuleName", "Edit", Me.LabelBasicRulesNameOld.Text, Me.LabelBasicRulesNameNew.Text, "Accepted")
                                AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Description", "Edit", Me.LabelBasicRulesDescriptionOld.Text, Me.LabelBasicRulesDescriptionnew.Text, "Accepted")
                                AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "AlertTo", "Edit", Me.LabelBasicRulesAlertToOld.Text, Me.LabelBasicRulesAlertToNew.Text, "Accepted")
                                AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Enabled", "Edit", Me.LabelBasicRulesEnableOld.Text, Me.LabelBasicRulesEnableNew.Text, "Accepted")
                                AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Expression", "Edit", StrExpression_Old, StrExpression, "Accepted")
                            End Using
                            'hapus item tersebut dalam tabel RulesBasic_Approval
                            RulesBasicApprovalAdapter.RulesBasic_ApprovalDeleteByPendingApprovalId(RulesBasicApprovalTableRow.FK_RulesBasic_PendingApprovalID)

                            'hapus item tersebut dalam tabel RulesBasic_PendingApproval
                            Using RulesBasic_PendingApprovalAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasic_PendingApprovalTableAdapter
                                SetTransaction(RulesBasic_PendingApprovalAdapter, oSQLTrans)
                                RulesBasic_PendingApprovalAdapter.RulesBasic_PendingApprovalDeleteByPendingApprovalID(RulesBasicApprovalTableRow.FK_RulesBasic_PendingApprovalID)
                            End Using
                            oSQLTrans.Commit()
                        Else 'Bila ObjTable.Rows.Count = 0 berarti RulesBasic tsb sudah tidak lagi berada dlm tabel RulesBasic_Approval
                            Throw New Exception("Cannot approve the following Basic Rule : " & Me.LabelBasicRulesNameNew.Text & " because that Basic Rule is no longer in the approval table.")
                        End If
                    End Using
                End Using
            End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    Private Sub AcceptDelete()
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            Using RulesBasicAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasicTableAdapter
                Dim objds As Data.DataSet = Sahassa.AML.Commonly.ExecuteDataset("SELECT rbpa.RulesBasic_PendingApprovalRulesBasicUserID FROM RulesBasic_PendingApproval rbpa WHERE rbpa.RulesBasic_PendingApprovalID=" & Me.PKRulesBasicPendingApprovalID)
                Dim userid As String = objds.Tables(0).Rows(0).Item(0)

                oSQLTrans = BeginTransaction(RulesBasicAdapter)
                Using RulesBasicApprovalAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasicApprovalTableAdapter
                    SetTransaction(RulesBasicApprovalAdapter, oSQLTrans)
                    Using RulesBasicApprovalTable As New AMLDAL.BasicRulesDataSet.RulesBasicApprovalDataTable
                        RulesBasicApprovalAdapter.FillByPendingApprovalId(RulesBasicApprovalTable, Me.PKRulesBasicPendingApprovalID)
                        'Bila RulesBasic_ApprovalTable.Rows.Count > 0 berarti Basic Rule tsb masih ada dlm tabel RulesBasic_Approval
                        If RulesBasicApprovalTable.Rows.Count > 0 Then
                            Dim RulesBasicApprovalTableRow As AMLDAL.BasicRulesDataSet.RulesBasicApprovalRow
                            RulesBasicApprovalTableRow = RulesBasicApprovalTable.Rows(0)
                            'tambahkan item tersebut dalam tabel RulesBasic
                            Using RulesBasicTable As New AMLDAL.BasicRulesDataSet.RulesBasicDataTable
                                RulesBasicAdapter.FillByRulesBasicId(RulesBasicTable, RulesBasicApprovalTableRow.RulesBasicId)
                                If RulesBasicTable.Rows.Count > 0 Then
                                    Dim RulesBasicTableRow As AMLDAL.BasicRulesDataSet.RulesBasicRow
                                    RulesBasicTableRow = RulesBasicTable.Rows(0)
                                    RulesBasicTableRow.Delete()
                                    RulesBasicAdapter.Update(RulesBasicTableRow)
                                End If
                            End Using
                            'catat aktifitas dalam tabel Audit Trail
                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                SetTransaction(AccessAudit, oSQLTrans)
                                Dim LastUpdatedDate As Date = Now()
                                Dim StrExpression As String = ""
                                If Not RulesBasicApprovalTableRow.IsLastUpdatedDateNull Then
                                    LastUpdatedDate = RulesBasicApprovalTableRow.LastUpdatedDate
                                End If
                                If Not RulesBasicApprovalTableRow.IsExpressionNull Then
                                    StrExpression = RulesBasicApprovalTableRow.Expression
                                End If

                                AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "LastUpdatedDate", "Delete", "", LastUpdatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                                AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "BasicRuleName", "Delete", "", Me.LabelBasicRulesNameNew.Text, "Accepted")
                                AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Description", "Delete", "", Me.LabelBasicRulesDescriptionnew.Text, "Accepted")
                                AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "AlertTo", "Delete", "", Me.LabelBasicRulesAlertToNew.Text, "Accepted")
                                AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Enabled", "Delete", "", Me.LabelBasicRulesEnableNew.Text, "Accepted")
                                AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Expression", "Delete", "", StrExpression, "Accepted")
                            End Using
                            'hapus item tersebut dalam tabel RulesBasic_Approval
                            RulesBasicApprovalAdapter.RulesBasic_ApprovalDeleteByPendingApprovalId(RulesBasicApprovalTableRow.FK_RulesBasic_PendingApprovalID)

                            'hapus item tersebut dalam tabel RulesBasic_PendingApproval
                            Using RulesBasic_PendingApprovalAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasic_PendingApprovalTableAdapter
                                SetTransaction(RulesBasic_PendingApprovalAdapter, oSQLTrans)
                                RulesBasic_PendingApprovalAdapter.RulesBasic_PendingApprovalDeleteByPendingApprovalID(RulesBasicApprovalTableRow.FK_RulesBasic_PendingApprovalID)
                            End Using
                            oSQLTrans.Commit()

                        Else 'Bila ObjTable.Rows.Count = 0 berarti RulesBasic tsb sudah tidak lagi berada dlm tabel RulesBasic_Approval
                            Throw New Exception("Cannot delete the following Basic Rule : " & Me.LabelBasicRulesNameNew.Text & " because that Basic Rule is no longer in the approval table.")
                        End If
                    End Using
                End Using
            End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
#End Region

#Region "Reject"
    Private Sub RejectAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using RulesBasicApprovalAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasicApprovalTableAdapter
                Dim objds As Data.DataSet = Sahassa.AML.Commonly.ExecuteDataset("SELECT rbpa.RulesBasic_PendingApprovalRulesBasicUserID FROM RulesBasic_PendingApproval rbpa WHERE rbpa.RulesBasic_PendingApprovalID=" & Me.PKRulesBasicPendingApprovalID)
                Dim userid As String = objds.Tables(0).Rows(0).Item(0)

                oSQLTrans = BeginTransaction(RulesBasicApprovalAdapter)
                Using RulesBasicApprovalTable As New AMLDAL.BasicRulesDataSet.RulesBasicApprovalDataTable
                    RulesBasicApprovalAdapter.FillByPendingApprovalId(RulesBasicApprovalTable, Me.PKRulesBasicPendingApprovalID)
                    'Bila RulesBasic_ApprovalTable.Rows.Count > 0 berarti Basic Rule tsb masih ada dlm tabel RulesBasic_Approval
                    If RulesBasicApprovalTable.Rows.Count > 0 Then
                        Dim RulesBasicApprovalTableRow As AMLDAL.BasicRulesDataSet.RulesBasicApprovalRow
                        RulesBasicApprovalTableRow = RulesBasicApprovalTable.Rows(0)
                        Dim LastUpdatedDate As Date = Now()
                        Dim StrExpression As String = ""
                        If Not RulesBasicApprovalTableRow.IsLastUpdatedDateNull Then
                            LastUpdatedDate = RulesBasicApprovalTableRow.LastUpdatedDate
                        End If
                        If Not RulesBasicApprovalTableRow.IsExpressionNull Then
                            StrExpression = RulesBasicApprovalTableRow.Expression
                        End If
                        'catat aktifitas dalam tabel Audit Trail
                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            SetTransaction(AccessAudit, oSQLTrans)
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "LastUpdatedDate", "Add", "", LastUpdatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "BasicRuleName", "Add", "", Me.LabelBasicRulesNameNew.Text, "Rejected")
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Description", "Add", "", Me.LabelBasicRulesDescriptionnew.Text, "Rejected")
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "AlertTo", "Add", "", Me.LabelBasicRulesAlertToNew.Text, "Rejected")
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Enabled", "Add", "", Me.LabelBasicRulesEnableNew.Text, "Rejected")
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Expression", "Add", "", StrExpression, "Rejected")
                        End Using
                        'hapus item tersebut dalam tabel RulesBasic_Approval
                        RulesBasicApprovalAdapter.RulesBasic_ApprovalDeleteByPendingApprovalId(Me.PKRulesBasicPendingApprovalID)
                    End If
                End Using
                'hapus item tersebut dalam tabel RulesBasic_PendingApproval
                Using RulesBasic_PendingApprovalAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasic_PendingApprovalTableAdapter
                    SetTransaction(RulesBasic_PendingApprovalAdapter, oSQLTrans)
                    RulesBasic_PendingApprovalAdapter.RulesBasic_PendingApprovalDeleteByPendingApprovalID(Me.PKRulesBasicPendingApprovalID)
                End Using
                oSQLTrans.Commit()
            End Using


        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    Private Sub RejectEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using RulesBasicApprovalAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasicApprovalTableAdapter
                Dim objds As Data.DataSet = Sahassa.AML.Commonly.ExecuteDataset("SELECT rbpa.RulesBasic_PendingApprovalRulesBasicUserID FROM RulesBasic_PendingApproval rbpa WHERE rbpa.RulesBasic_PendingApprovalID=" & Me.PKRulesBasicPendingApprovalID)
                Dim userid As String = objds.Tables(0).Rows(0).Item(0)

                oSQLTrans = BeginTransaction(RulesBasicApprovalAdapter)
                Using RulesBasicApprovalTable As New AMLDAL.BasicRulesDataSet.RulesBasicApprovalDataTable
                    RulesBasicApprovalAdapter.FillByPendingApprovalId(RulesBasicApprovalTable, Me.PKRulesBasicPendingApprovalID)
                    'Bila RulesBasic_ApprovalTable.Rows.Count > 0 berarti Basic Rule tsb masih ada dlm tabel RulesBasic_Approval
                    If RulesBasicApprovalTable.Rows.Count > 0 Then
                        Dim RulesBasicApprovalTableRow As AMLDAL.BasicRulesDataSet.RulesBasicApprovalRow
                        RulesBasicApprovalTableRow = RulesBasicApprovalTable.Rows(0)
                        Dim LastUpdatedDate_Old As Date = Now()
                        Dim LastUpdatedDate As Date = Now()
                        Dim StrExpression_Old As String = ""
                        Dim StrExpression As String = ""
                        If Not RulesBasicApprovalTableRow.IsLastUpdatedDate_oldNull Then
                            LastUpdatedDate_Old = RulesBasicApprovalTableRow.LastUpdatedDate_old
                        End If
                        If Not RulesBasicApprovalTableRow.IsLastUpdatedDateNull Then
                            LastUpdatedDate = RulesBasicApprovalTableRow.LastUpdatedDate
                        End If
                        If Not RulesBasicApprovalTableRow.IsExpression_oldNull Then
                            StrExpression_Old = RulesBasicApprovalTableRow.Expression_old
                        End If
                        If Not RulesBasicApprovalTableRow.IsExpressionNull Then
                            StrExpression = RulesBasicApprovalTableRow.Expression
                        End If
                        'catat aktifitas dalam tabel Audit Trail
                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            SetTransaction(AccessAudit, oSQLTrans)
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "LastUpdatedDate", "Add", LastUpdatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), LastUpdatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "BasicRuleName", "Add", Me.LabelBasicRulesNameOld.Text, Me.LabelBasicRulesNameNew.Text, "Rejected")
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Description", "Add", Me.LabelBasicRulesDescriptionOld.Text, Me.LabelBasicRulesDescriptionnew.Text, "Rejected")
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "AlertTo", "Add", Me.LabelBasicRulesAlertToOld.Text, Me.LabelBasicRulesAlertToNew.Text, "Rejected")
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Enabled", "Add", Me.LabelBasicRulesEnableOld.Text, Me.LabelBasicRulesEnableNew.Text, "Rejected")
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Expression", "Add", StrExpression_Old, StrExpression, "Rejected")
                        End Using
                        'hapus item tersebut dalam tabel RulesBasic_Approval
                        RulesBasicApprovalAdapter.RulesBasic_ApprovalDeleteByPendingApprovalId(Me.PKRulesBasicPendingApprovalID)
                    End If
                End Using
                'hapus item tersebut dalam tabel RulesBasic_PendingApproval
                Using RulesBasic_PendingApprovalAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasic_PendingApprovalTableAdapter
                    SetTransaction(RulesBasic_PendingApprovalAdapter, oSQLTrans)
                    RulesBasic_PendingApprovalAdapter.RulesBasic_PendingApprovalDeleteByPendingApprovalID(Me.PKRulesBasicPendingApprovalID)
                End Using
                oSQLTrans.Commit()
            End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    Private Sub RejectDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using RulesBasicApprovalAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasicApprovalTableAdapter
                Dim objds As Data.DataSet = Sahassa.AML.Commonly.ExecuteDataset("SELECT rbpa.RulesBasic_PendingApprovalRulesBasicUserID FROM RulesBasic_PendingApproval rbpa WHERE rbpa.RulesBasic_PendingApprovalID=" & Me.PKRulesBasicPendingApprovalID)
                Dim userid As String = objds.Tables(0).Rows(0).Item(0)

                oSQLTrans = BeginTransaction(RulesBasicApprovalAdapter)
                Using RulesBasicApprovalTable As New AMLDAL.BasicRulesDataSet.RulesBasicApprovalDataTable
                    RulesBasicApprovalAdapter.FillByPendingApprovalId(RulesBasicApprovalTable, Me.PKRulesBasicPendingApprovalID)
                    'Bila RulesBasic_ApprovalTable.Rows.Count > 0 berarti Basic Rule tsb masih ada dlm tabel RulesBasic_Approval
                    If RulesBasicApprovalTable.Rows.Count > 0 Then
                        Dim RulesBasicApprovalTableRow As AMLDAL.BasicRulesDataSet.RulesBasicApprovalRow
                        RulesBasicApprovalTableRow = RulesBasicApprovalTable.Rows(0)
                        Dim LastUpdatedDate As Date = Now()
                        Dim StrExpression As String = ""
                        If Not RulesBasicApprovalTableRow.IsLastUpdatedDateNull Then
                            LastUpdatedDate = RulesBasicApprovalTableRow.LastUpdatedDate
                        End If
                        If Not RulesBasicApprovalTableRow.IsExpressionNull Then
                            StrExpression = RulesBasicApprovalTableRow.Expression
                        End If
                        'catat aktifitas dalam tabel Audit Trail
                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            SetTransaction(AccessAudit, oSQLTrans)
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "LastUpdatedDate", "Delete", "", LastUpdatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "BasicRuleName", "Delete", "", Me.LabelBasicRulesNameNew.Text, "Rejected")
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Description", "Delete", "", Me.LabelBasicRulesDescriptionnew.Text, "Rejected")
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "AlertTo", "Delete", "", Me.LabelBasicRulesAlertToNew.Text, "Rejected")
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Enabled", "Delete", "", Me.LabelBasicRulesEnableNew.Text, "Rejected")
                            AccessAudit.Insert(Now, userid, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Expression", "Delete", "", StrExpression, "Rejected")
                        End Using
                        'hapus item tersebut dalam tabel RulesBasic_Approval
                        RulesBasicApprovalAdapter.RulesBasic_ApprovalDeleteByPendingApprovalId(Me.PKRulesBasicPendingApprovalID)
                    End If
                End Using
                'hapus item tersebut dalam tabel RulesBasic_PendingApproval
                Using RulesBasic_PendingApprovalAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasic_PendingApprovalTableAdapter
                    SetTransaction(RulesBasic_PendingApprovalAdapter, oSQLTrans)
                    RulesBasic_PendingApprovalAdapter.RulesBasic_PendingApprovalDeleteByPendingApprovalID(Me.PKRulesBasicPendingApprovalID)
                End Using
                oSQLTrans.Commit()
            End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.LabelTitle.Text = "Rules Basic - Approval Detail"
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.LoadData()
                'Select Case Me.ParamType
                '    Case Sahassa.AML.Commonly.TypeConfirm.GroupAdd
                '        Me.LoadGroupAdd()
                '    Case Sahassa.AML.Commonly.TypeConfirm.GroupEdit
                '        Me.LoadGroupEdit()
                '    Case Sahassa.AML.Commonly.TypeConfirm.GroupDelete
                '        Me.LoadGroupDelete()
                '    Case Else
                '        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                'End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles MenuTab.MenuItemClick
        MultiViewTab.ActiveViewIndex = MenuTab.SelectedValue
    End Sub

    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click

        Try
            If Not orowRulesBasicPendingApprovalID Is Nothing Then
                Select Case orowRulesBasicPendingApprovalID.RulesBasic_PendingApprovalModeID
                    Case Sahassa.AML.Commonly.TypeMode.Add
                        AcceptAdd()
                    Case Sahassa.AML.Commonly.TypeMode.Edit
                        AcceptEdit()
                    Case Sahassa.AML.Commonly.TypeMode.Delete
                        AcceptDelete()
                End Select
                Response.Redirect("RulesBasicApproval.aspx", False)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            If Not orowRulesBasicPendingApprovalID Is Nothing Then
                Select Case orowRulesBasicPendingApprovalID.RulesBasic_PendingApprovalModeID
                    Case Sahassa.AML.Commonly.TypeMode.Add
                        RejectAdd()
                    Case Sahassa.AML.Commonly.TypeMode.Edit
                        RejectEdit()
                    Case Sahassa.AML.Commonly.TypeMode.Delete
                        RejectDelete()
                End Select
                Response.Redirect("RulesBasicApproval.aspx", False)
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Me.Response.Redirect("RulesBasicApproval.aspx", False)
    End Sub
End Class
