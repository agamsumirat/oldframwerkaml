Option Explicit On
Option Strict On
Imports SahassaNettier.Entities
Imports SahassaNettier.Data

Imports AMLBLL

Partial Class ResikoNasabahBaruApproval
    Inherits Parent
    Public Property SetnGetPreparerID() As String
        Get
            If Not Session("ResikoNasabahBaruApproval.PreparerID") Is Nothing Then
                Return CStr(Session("ResikoNasabahBaruApproval.PreparerID"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("ResikoNasabahBaruApproval.PreparerID") = value
        End Set
    End Property
    Public Property SetnGetEntryDateFrom() As String
        Get
            If Not Session("ResikoNasabahBaruApproval.CreatedDate") Is Nothing Then
                Return CStr(Session("ResikoNasabahBaruApproval.CreatedDate"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("ResikoNasabahBaruApproval.CreatedDate") = value
        End Set
    End Property
    Public Property SetnGetEntryDateUntil() As String
        Get
            If Not Session("ResikoNasabahBaruApproval.CreatedDateUntil") Is Nothing Then
                Return CStr(Session("ResikoNasabahBaruApproval.CreatedDateUntil"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("ResikoNasabahBaruApproval.CreatedDateUntil") = value
        End Set
    End Property

    Public Property SetnGetBindTableAll() As TList(Of ResikoNasabahBaru_Approval)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            If SetnGetPreparerID.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "FK_MsUserID IN(SELECT u.pkUserID FROM [User] u WHERE u.UserID LIKE '%" & SetnGetPreparerID.Trim.Replace("'", "''") & "%')"
            End If

            If SetnGetEntryDateFrom.Length > 0 AndAlso SetnGetEntryDateUntil.Length > 0 Then

                If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetEntryDateFrom) AndAlso Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetEntryDateFrom) Then

                    Dim tanggal As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateFrom).ToString("yyyy-MM-dd")
                    Dim tanggalAkhir As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateUntil).ToString("yyyy-MM-dd")
                    If DateDiff(DateInterval.Day, Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateFrom), Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateUntil)) > -1 Then

                        ReDim Preserve strWhereClause(strWhereClause.Length)

                        strWhereClause(strWhereClause.Length - 1) = ResikoNasabahBaru_ApprovalColumn.CreatedDate.ToString & " between '" & tanggal & " 00:00:00' and '" & tanggalAkhir & " 23:59:59'"
                    Else
                        Throw New Exception("Start Date must be greater than End Date.")
                    End If
                End If
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            If strAllWhereClause.Trim.Length > 0 Then
                strAllWhereClause += " and FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            Else
                strAllWhereClause += " FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            End If
            Session("ResikoNasabahBaruApproval.TableALL") = DataRepository.ResikoNasabahBaru_ApprovalProvider.GetPaged(strAllWhereClause, SetnGetSort, 0, Integer.MaxValue, 0)

            Return CType(Session("ResikoNasabahBaruApproval.TableALL"), TList(Of ResikoNasabahBaru_Approval))
        End Get
        Set(ByVal value As TList(Of ResikoNasabahBaru_Approval))
            Session("ResikoNasabahBaruApproval.Table") = value
        End Set
    End Property

#Region "Set Session"
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("ResikoNasabahBaruApproval.SelectedItem") Is Nothing, New ArrayList, Session("ResikoNasabahBaruApproval.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("ResikoNasabahBaruApproval.SelectedItem") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("ResikoNasabahBaruApproval.Sort") Is Nothing, "PK_ResikoNasabahBaru_ApprovalID  asc", Session("ResikoNasabahBaruApproval.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("ResikoNasabahBaruApproval.Sort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("ResikoNasabahBaruApproval.CurrentPage") Is Nothing, 0, Session("ResikoNasabahBaruApproval.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("ResikoNasabahBaruApproval.CurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("ResikoNasabahBaruApproval.RowTotal") Is Nothing, 0, Session("ResikoNasabahBaruApproval.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("ResikoNasabahBaruApproval.RowTotal") = Value
        End Set
    End Property
    Public Property SetAndGetSearchingCriteria() As String
        Get
            Return CStr(IIf(Session("MsUserViewAppPageSearchCriteria") Is Nothing, "", Session("MsUserViewAppPageSearchCriteria")))
        End Get
        Set(ByVal value As String)
            Session("MsUserViewAppPageSearchCriteria") = value
        End Set
    End Property

    Public Property SetnGetBindTable() As TList(Of ResikoNasabahBaru_Approval)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If

            If SetnGetPreparerID.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "FK_MsUserID IN(SELECT u.pkUserID FROM [User] u WHERE u.UserID LIKE '%" & SetnGetPreparerID.Trim.Replace("'", "''") & "%')"
            End If

            If SetnGetEntryDateFrom.Length > 0 AndAlso SetnGetEntryDateUntil.Length > 0 Then
                If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetEntryDateFrom) AndAlso Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetEntryDateFrom) Then
                    Dim tanggal As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateFrom).ToString("yyyy-MM-dd")
                    Dim tanggalAkhir As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateUntil).ToString("yyyy-MM-dd")
                    If DateDiff(DateInterval.Day, Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateFrom), Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateUntil)) > -1 Then

                        ReDim Preserve strWhereClause(strWhereClause.Length)

                        strWhereClause(strWhereClause.Length - 1) = ResikoNasabahBaru_ApprovalColumn.CreatedDate.ToString & " between '" & tanggal & " 00:00:00' and '" & tanggalAkhir & " 23:59:59'"
                    Else
                        Throw New Exception("Start Date must be greater than End Date.")
                    End If
                End If
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            If strAllWhereClause.Trim.Length > 0 Then
                strAllWhereClause += " and FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            Else
                strAllWhereClause += " FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            End If
            Session("ResikoNasabahBaruApproval.Table") = DataRepository.ResikoNasabahBaru_ApprovalProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)

            Return CType(Session("ResikoNasabahBaruApproval.Table"), TList(Of ResikoNasabahBaru_Approval))
        End Get
        Set(ByVal value As TList(Of ResikoNasabahBaru_Approval))
            Session("ResikoNasabahBaruApproval.Table") = value
        End Set
    End Property

#End Region

    Private Sub BindGrid()
        SettingControlSearching()
        Me.GridResikoNasabahBaruView.DataSource = Me.SetnGetBindTable
        Me.GridResikoNasabahBaruView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridResikoNasabahBaruView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridResikoNasabahBaruView.DataBind()

        If SetnGetRowTotal > 0 Then
            LabelNoRecordFound.Visible = False
        Else
            LabelNoRecordFound.Visible = True
        End If
    End Sub

    Private Sub ClearThisPageSessions()
        Me.SetnGetPreparerID = Nothing
        Me.SetnGetEntryDateFrom = Nothing
        Me.SetnGetEntryDateUntil = Nothing

        LblMessage.Visible = False
        LblMessage.Text = ""
        Me.SetnGetRowTotal = Nothing
        Me.SetnGetSort = Nothing
    End Sub

    Private Sub SettingPropertySearching()
        SetnGetPreparerID = TxtPreparer.Text.Trim
        SetnGetEntryDateFrom = TxtEntryDateFrom.Text
        SetnGetEntryDateUntil = TxtEntryDateUntil.Text
    End Sub

    Private Sub SettingControlSearching()
        TxtPreparer.Text = SetnGetPreparerID
        TxtEntryDateFrom.Text = SetnGetEntryDateFrom
        TxtEntryDateUntil.Text = SetnGetEntryDateUntil
    End Sub

    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridResikoNasabahBaruView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                Dim PKResikoNasabahBaruId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PKResikoNasabahBaruId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKResikoNasabahBaruId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKResikoNasabahBaruId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            LblMessage.Text = ""
            LblMessage.Visible = False
            If Not Page.IsPostBack Then
                ClearThisPageSessions()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                Me.popUpEntryDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtEntryDateFrom.ClientID & "'), 'dd-mm-yyyy')")
                Me.popUpEntryDateUntil.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtEntryDateUntil.ClientID & "'), 'dd-mm-yyyy')")
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            CollectSelected()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSearch.Click
        Try
            SettingPropertySearching()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub SetInfoNavigate()
        Me.PageCurrentPage.Text = (Me.SetnGetCurrentPage + 1).ToString
        Me.PageTotalPages.Text = Me.GetPageTotal.ToString
        Me.PageTotalRows.Text = Me.SetnGetRowTotal.ToString
        Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
        Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnClearSearch.Click
        Try
            SetnGetEntryDateFrom = Nothing
            SetnGetPreparerID = Nothing
            SetnGetEntryDateUntil = Nothing
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridResikoNasabahBaruView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim pkid As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(pkid) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            Else
        '                ArrTarget.Remove(pkid)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridResikoNasabahBaruView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub

    Public Sub BindGridView()
        GridResikoNasabahBaruView.DataSource = Me.SetnGetBindTable
        GridResikoNasabahBaruView.VirtualItemCount = Me.SetnGetRowTotal
        GridResikoNasabahBaruView.DataBind()
    End Sub

    Protected Sub GridResikoNasabahBaruView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridResikoNasabahBaruView.EditCommand
        Dim PKResikoNasabahBaruApprovalid As Integer
        Try
            PKResikoNasabahBaruApprovalid = CInt(e.Item.Cells(1).Text)
            Response.Redirect("ResikoNasabahBaruApprovalDetail.aspx?PKResikoNasabahBaruApprovalID=" & PKResikoNasabahBaruApprovalid, False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridResikoNasabahBaruView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridResikoNasabahBaruView.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim pk As String = e.Item.Cells(1).Text
                Dim objcheckbox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                objcheckbox.Checked = Me.SetnGetSelectedItem.Contains(pk)

                Dim IntUserId As Integer = CInt(e.Item.Cells(2).Text)
                Using ObjUser As User = DataRepository.UserProvider.GetByPkUserID(IntUserId)
                    If Not ObjUser Is Nothing Then
                        e.Item.Cells(2).Text = ObjUser.UserID
                    End If
                End Using

                If e.Item.Cells(4).Text = "2" Then
                    e.Item.Cells(4).Text = "Edit"
                End If

            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridResikoNasabahBaruView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridResikoNasabahBaruView.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindSelectedAll()
        Try
            Me.GridResikoNasabahBaruView.DataSource = SetnGetBindTableAll
            Me.GridResikoNasabahBaruView.AllowPaging = False
            Me.GridResikoNasabahBaruView.DataBind()

            For i As Integer = 0 To GridResikoNasabahBaruView.Items.Count - 1
                For y As Integer = 0 To GridResikoNasabahBaruView.Columns.Count - 1
                    GridResikoNasabahBaruView.Items(i).Cells(y).Attributes.Add("class", "text")
                Next
            Next
            Me.GridResikoNasabahBaruView.Columns(0).Visible = False
            Me.GridResikoNasabahBaruView.Columns(1).Visible = False
            Me.GridResikoNasabahBaruView.Columns(6).Visible = False
        Catch
            Throw
        End Try
    End Sub

    Private Sub BindSelected()
        Dim Rows As New ArrayList
        Dim AllList As TList(Of ResikoNasabahBaru_Approval) = DataRepository.ResikoNasabahBaru_ApprovalProvider.GetPaged("", "", 0, Integer.MaxValue, 0)

        For Each IdPk As Long In Me.SetnGetSelectedItem
            Dim oList As TList(Of ResikoNasabahBaru_Approval) = AllList.FindAll(ResikoNasabahBaru_ApprovalColumn.PK_ResikoNasabahBaru_ApprovalID.ToString, IdPk)
            If oList.Count > 0 Then
                Rows.Add(oList(0))
            End If
        Next
        Me.GridResikoNasabahBaruView.DataSource = Rows
        Me.GridResikoNasabahBaruView.AllowPaging = False
        Me.GridResikoNasabahBaruView.DataBind()
        For i As Integer = 0 To GridResikoNasabahBaruView.Items.Count - 1
            For y As Integer = 0 To GridResikoNasabahBaruView.Columns.Count - 1
                GridResikoNasabahBaruView.Items(i).Cells(y).Attributes.Add("class", "text")
            Next
        Next
        'Sembunyikan kolom ke 0, 1, 5 & 6 agar tidak ikut diekspor ke excel
        Me.GridResikoNasabahBaruView.Columns(0).Visible = False
        Me.GridResikoNasabahBaruView.Columns(1).Visible = False
        Me.GridResikoNasabahBaruView.Columns(6).Visible = False
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=ResikoNasabahBaruApproval.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridResikoNasabahBaruView)
            GridResikoNasabahBaruView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub lnkExportAllData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAllData.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelectedAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=ResikoNasabahBaruApprovalAll.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridResikoNasabahBaruView)
            GridResikoNasabahBaruView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) And (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
            CollectSelected()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
End Class