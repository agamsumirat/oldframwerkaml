Imports System.IO
Imports System.Data.SqlClient

Partial Class VerificationListUploadPEP
    Inherits Parent

    Private dsPEP As New Data.DataSet

    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.FileUploadPEP.ClientID
        End Get
    End Property

    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(1)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "ALL", "Update", "", "", "Upload PEPList")
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Function ExecNonQuery(ByVal SQLQuery As String) As Integer
        Dim SqlConn As SqlConnection = Nothing
        Dim SqlCmd As SqlCommand = Nothing
        Try
            SqlConn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("AMLConnectionString").ToString)
            SqlConn.Open()
            SqlCmd = New SqlCommand
            SqlCmd.Connection = SqlConn
            SqlCmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings.Get("SQLCommandTimeout")
            SqlCmd.CommandText = SQLQuery
            Return SqlCmd.ExecuteNonQuery()
        Catch tex As Threading.ThreadAbortException
            Throw tex
        Catch ex As Exception
            Throw ex
        Finally
            If Not SqlConn Is Nothing Then
                SqlConn.Close()
                SqlConn.Dispose()
                SqlConn = Nothing
            End If
            If Not SqlCmd Is Nothing Then
                SqlCmd.Dispose()
                SqlCmd = Nothing
            End If
        End Try
    End Function

    Private Function getStringFieldValue(ByVal objValue As Object) As String
        Dim strValue As String = ""
        Try
            If Not IsDBNull(objValue) Then
                strValue = CStr(objValue)
            End If
            Return strValue
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function SavePEPListExcelToSQLServer() As Boolean
        Dim StrSQL As String
        Dim dr As Data.DataRow
        ExecNonQuery("TRUNCATE TABLE PEPList")
        For Each dr In dsPEP.Tables(0).Rows
            If getStringFieldValue(dr.Item("NoReference")).Length > 0 Then
                StrSQL = "INSERT INTO [PEPList]([NoReference],[Nama],[GelarDepan],[GelarBelakang],[LembagaInstansi],[Jabatan],[JalanKantor],[AlamatKantor],[AlamatKantor2],[PropinsiKantor],[TeleponKantor],[JalanRumah],[AlamatRumah],[PropinsiRumah],[KodePosRumah],[TeleponRumah])"
                StrSQL = StrSQL & " VALUES ("
                StrSQL = StrSQL & " '" & getStringFieldValue(dr.Item("NoReference")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("Nama")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("GelarDepan")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("GelarBelakang")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("LembagaInstansi")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("Jabatan")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("JalanKantor")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("AlamatKantor")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("AlamatKantor2")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("PropinsiKantor")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("TeleponKantor")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("JalanRumah")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("AlamatRumah")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("PropinsiRumah")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("KodePosRumah")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("TeleponRumah")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " )"
                ExecNonQuery(StrSQL)
            End If
        Next
        Return True
    End Function

    Private Function ExecuteProcessPEPList(ByVal ProcessDate As DateTime) As Boolean
        ExecNonQuery("EXEC usp_ProcessPEPList '" & ProcessDate.ToString("yyyy-MM-dd") & "'")
        Return True
    End Function
    Private Function CekApproval() As Boolean
        Try
            Using AccessPEP As New AMLDAL.PEPListTableAdapters.PEPList_PendingApprovalTableAdapter
                Using dt As Data.DataTable = AccessPEP.GetData
                    If dt.Rows.Count > 0 Then
                        Return False
                    Else
                        Return True
                    End If
                End Using
            End Using
        Catch ex As Exception

        End Try
    End Function

    Protected Sub ImageUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageUpload.Click
        Try
            If Me.CekApproval Then            
                If Me.FileUploadPEP.PostedFile.ContentLength <> 0 Then
                    If Me.FileUploadPEP.HasFile Then
                        Dim destDir As String = Server.MapPath("Upload")
                        Dim fName As String = Path.GetFileName(Me.FileUploadPEP.PostedFile.FileName)
                        Dim destPath As String = Path.Combine(destDir, fName)
                        Me.FileUploadPEP.PostedFile.SaveAs(destPath)

                        Dim sConnectionString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=Excel 8.0;Data Source=" & destPath
                        Dim objConn As New Data.OleDb.OleDbConnection(sConnectionString)
                        objConn.Open()
                        Dim objCmdSelect As New Data.OleDb.OleDbCommand("SELECT * FROM [PEPList$]", objConn)
                        Dim objAdapter1 As New Data.OleDb.OleDbDataAdapter
                        objAdapter1.SelectCommand = objCmdSelect
                        objAdapter1.Fill(dsPEP)
                        objConn.Close()
                        SavePEPListExcelToSQLServer()

                        If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                            ' INSERT TO Audit Trail
                            InsertAuditTrail()
                            ' Update Verification List
                            ExecuteProcessPEPList(Now())
                            Me.LblSuccess.Visible = True
                            Me.LblSuccess.Text = "Success to Update Verification List for Category PEPList."
                        Else
                            Using PEPPendingApprovalAdapter As New AMLDAL.PEPListTableAdapters.PEPList_PendingApprovalTableAdapter
                                PEPPendingApprovalAdapter.Insert(Sahassa.AML.Commonly.SessionUserId, Now, "Verification List PEPList", 2, Now())
                            End Using
                            Dim MessagePendingID As Integer = 8865 'MessagePendingID 8865 = PEPList
                            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID, False)
                        End If
                    Else
                        Throw New Exception("Choose an PEP-List.xls file first.")
                    End If
                End If
            Else
                Throw New Exception("Upload PEP is already waiting for approval.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCancel.Click
        Me.Response.Redirect("Default.aspx", False)
    End Sub
End Class
