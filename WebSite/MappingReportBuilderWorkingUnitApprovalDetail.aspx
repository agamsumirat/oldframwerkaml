<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MappingReportBuilderWorkingUnitApprovalDetail.aspx.vb" Inherits="MappingReportBuilderWorkingUnitApprovalDetail"  %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">

<ajax:AjaxPanel runat="server" ID="Ajax1" Width="971px">
<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <table style="width: 100%">
        <tr>
            <td style="width: 200px; height: 15px;" valign="top">
                <asp:Label ID="LblMode" runat="server" Text="Mode"></asp:Label></td>
            <td style="width: 1px; height: 15px;" valign="top">
                :</td>
            <td valign="top" style="height: 15px">
                <asp:Label ID="LblAction" runat="server"></asp:Label></td>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td valign="top">
                <asp:Panel ID="PanelOld" runat="server" GroupingText="Old Value" Width="100%">
                    <table style="width: 100%">
                        
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label14" runat="server" Text="Query Name"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblFK_Group_ID_Old" runat="server"></asp:Label></td>
                        </tr>
                         <tr>
                            <td style="width: 20%" valign="top">
                                </td>
                            <td style="width: 1px" valign="top">
                                </td>
                            <td style="width: 80%" valign="top">
                                &nbsp;<asp:DataGrid ID="gridViewOLD" runat="server" AllowPaging="True" AllowSorting="True"
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None"
                                    BorderWidth="1px" CellPadding="4" Font-Size="XX-Small" ForeColor="Black" GridLines="Vertical"
                                    Width="100%">
                                    <FooterStyle BackColor="#CCCC99" />
                                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" 
                                        Mode="NumericPages" />
                                    <AlternatingItemStyle BackColor="White" />
                                    <ItemStyle BackColor="#F7F7DE" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="No." Visible="False"></asp:TemplateColumn>
                                        <asp:BoundColumn DataField="WorkingUnitName" HeaderText="Working Unit">
                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                        </asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="XX-Small" ForeColor="White" />
                                </asp:DataGrid></td>
                        </tr>
                        
                     </table>
                </asp:Panel>
            </td>
            <td valign="top">
                <asp:Panel ID="PanelNew" runat="server" GroupingText="New Value" Width="100%">
                    <table style="width: 100%">
                       
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label16" runat="server" Text="Query Name"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblFK_Group_ID_New" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                </td>
                            <td style="width: 1px" valign="top">
                                </td>
                            <td style="width: 80%" valign="top">
                                &nbsp;<asp:DataGrid ID="gridViewNew" runat="server" AllowPaging="True" AllowSorting="True"
                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None"
                                    BorderWidth="1px" CellPadding="4" Font-Size="XX-Small" ForeColor="Black" GridLines="Vertical"
                                    Width="100%">
                                    <FooterStyle BackColor="#CCCC99" />
                                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" 
                                        Mode="NumericPages" />
                                    <AlternatingItemStyle BackColor="White" />
                                    <ItemStyle BackColor="#F7F7DE" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="No." Visible="False"></asp:TemplateColumn>
                                        <asp:BoundColumn DataField="WorkingUnitName" HeaderText="Working Unit">
                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                        </asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="XX-Small" ForeColor="White" />
                                </asp:DataGrid></td>
                        </tr>
                       
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                <asp:ImageButton ID="ImageAccept" runat="server" CausesValidation="False" SkinID="AcceptButton" /><asp:ImageButton
                    ID="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton" /><asp:ImageButton
                        ID="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton" /><asp:CustomValidator
                            ID="cvalPageError" runat="server" Display="none"></asp:CustomValidator></td>
        </tr>
    </table>
  </ajax:AjaxPanel>
</asp:Content>




