Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Imports System.IO
Imports System.Collections.Generic
Partial Class MappingGroupMenuSTRPPATKApprovalDetail
    Inherits Parent

    Public ReadOnly Property PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID() As Integer
        Get
            Dim strTemp As String = Request.Params("PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID")
            Dim intResult As Integer
            If Integer.TryParse(strTemp, intResult) Then
                Return intResult
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Parameter PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID is invalid."

            End If
        End Get
    End Property

    Public ReadOnly Property ObjMappingGroupMenuSTRPPATK_ApprovalDetail() As TList(Of MappingGroupMenuSTRPPATK_ApprovalDetail)
        Get
            If Session("MappingGroupMenuSTRPPATKApprovalDetail.ObjMappingGroupMenuSTRPPATK_ApprovalDetail") Is Nothing Then

                Session("MappingGroupMenuSTRPPATKApprovalDetail.ObjMappingGroupMenuSTRPPATK_ApprovalDetail") = DataRepository.MappingGroupMenuSTRPPATK_ApprovalDetailProvider.GetPaged("PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID = '" & PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("MappingGroupMenuSTRPPATKApprovalDetail.ObjMappingGroupMenuSTRPPATK_ApprovalDetail"), TList(Of MappingGroupMenuSTRPPATK_ApprovalDetail))
        End Get
    End Property


    Sub LoadDataAdd()
        Try
            PanelOld.Visible = False
            PanelNew.Visible = True

            LblAction.Text = "Add"

            If ObjMappingGroupMenuSTRPPATK_ApprovalDetail.Count > 0 Then
                Using objGroupNew As Group = DataRepository.GroupProvider.GetByGroupID(CInt(ObjMappingGroupMenuSTRPPATK_ApprovalDetail(0).FK_Group_ID))
                    LblFK_Group_ID_New.Text = objGroupNew.GroupName
                End Using
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingGroupMenuSTRPPATKBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub loaddataEdit()
        Try
            PanelOld.Visible = True
            PanelNew.Visible = True

            LblAction.Text = "Edit"

            If ObjMappingGroupMenuSTRPPATK_ApprovalDetail.Count > 0 Then
                Using objGroupOld As Group = DataRepository.GroupProvider.GetByGroupID(CInt(ObjMappingGroupMenuSTRPPATK_ApprovalDetail(0).FK_Group_ID_Old))
                    LblFK_Group_ID_Old.Text = objGroupOld.GroupName
                End Using

                Using objGroupNew As Group = DataRepository.GroupProvider.GetByGroupID(CInt(ObjMappingGroupMenuSTRPPATK_ApprovalDetail(0).FK_Group_ID))
                    LblFK_Group_ID_New.Text = objGroupNew.GroupName
                End Using
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingGroupMenuSTRPPATKBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Sub LoadDataDelete()
        Try
            PanelOld.Visible = False
            PanelNew.Visible = True

            LblAction.Text = "Delete"

            If ObjMappingGroupMenuSTRPPATK_ApprovalDetail.Count > 0 Then
                Using objGroupNew As Group = DataRepository.GroupProvider.GetByGroupID(CInt(ObjMappingGroupMenuSTRPPATK_ApprovalDetail(0).FK_Group_ID))
                    LblFK_Group_ID_New.Text = objGroupNew.GroupName
                End Using
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingGroupMenuSTRPPATKBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub LoadData()
        Try
            If ObjMappingGroupMenuSTRPPATK_ApprovalDetail.Count > 0 Then
                Dim ListModeID As Integer = 0
                Using objMappingGroupMenuSTRPPATK_Approval As MappingGroupMenuSTRPPATK_Approval = DataRepository.MappingGroupMenuSTRPPATK_ApprovalProvider.GetByPK_MappingGroupMenuSTRPPATK_Approval_ID(CInt(ObjMappingGroupMenuSTRPPATK_ApprovalDetail(0).FK_MappingGroupMenuSTRPPATK_Approval_ID))
                    ListModeID = CInt(objMappingGroupMenuSTRPPATK_Approval.FK_ModeID)
                    If ListModeID = 1 Then
                        LoadDataAdd()
                    ElseIf ListModeID = 2 Then
                        loaddataEdit()
                    ElseIf ListModeID = 3 Then
                        LoadDataDelete()
                    End If
                End Using

            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingGroupMenuSTRPPATKBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKApproval.aspx"
            Me.Response.Redirect("MappingGroupMenuSTRPPATKApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                Session("MappingGroupMenuSTRPPATKApprovalDetail.ObjMappingGroupMenuSTRPPATK_ApprovalDetail") = Nothing
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    LabelTitle.Text = "Mapping Group Menu STR PPATK - Approval Detail"
                    LoadData()

                    

                End Using
            End If

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub



    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try

            Using ObjMappingGroupMenuSTRPPATK_ApprovalDetail As MappingGroupMenuSTRPPATK_ApprovalDetail = DataRepository.MappingGroupMenuSTRPPATK_ApprovalDetailProvider.GetByPK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID(PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID)
                If ObjMappingGroupMenuSTRPPATK_ApprovalDetail Is Nothing Then
                    Throw New AMLBLL.SahassaException(MappingGroupMenuSTRPPATKEnum.DATA_NOTVALID)
                End If
            End Using

            If LblAction.Text = "Add" Then
                AMLBLL.MappingGroupMenuSTRPPATKBLL.AcceptAddSTRPPATK(PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Edit" Then
                Using ObjList As MappingGroupMenuSTRPPATK = DataRepository.MappingGroupMenuSTRPPATKProvider.GetByPK_MappingGroupMenuSTRPPATK_ID(ObjMappingGroupMenuSTRPPATK_ApprovalDetail(0).PK_MappingGroupMenuSTRPPATK_ID)
                    If ObjList Is Nothing Then
                        Throw New AMLBLL.SahassaException(MappingGroupMenuSTRPPATKEnum.DATA_NOTVALID)
                    End If
                End Using
                AMLBLL.MappingGroupMenuSTRPPATKBLL.AcceptEditSTRPPATK(PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Delete" Then
                Using ObjList As MappingGroupMenuSTRPPATK = DataRepository.MappingGroupMenuSTRPPATKProvider.GetByPK_MappingGroupMenuSTRPPATK_ID(ObjMappingGroupMenuSTRPPATK_ApprovalDetail(0).PK_MappingGroupMenuSTRPPATK_ID)
                    If ObjList Is Nothing Then
                        Throw New AMLBLL.SahassaException(MappingGroupMenuSTRPPATKEnum.DATA_NOTVALID)
                    End If
                End Using
                AMLBLL.MappingGroupMenuSTRPPATKBLL.AcceptDeleteSTRPPATK(PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID)
            End If

            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKApproval.aspx"

            Me.Response.Redirect("MappingGroupMenuSTRPPATKApproval.aspx", False)

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Using ObjMappingGroupMenuSTRPPATK_ApprovalDetail As MappingGroupMenuSTRPPATK_ApprovalDetail = DataRepository.MappingGroupMenuSTRPPATK_ApprovalDetailProvider.GetByPK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID(PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID)
                If ObjMappingGroupMenuSTRPPATK_ApprovalDetail Is Nothing Then
                    Throw New AMLBLL.SahassaException(MappingGroupMenuSTRPPATKEnum.DATA_NOTVALID)
                End If
            End Using

            If LblAction.Text = "Add" Then
                AMLBLL.MappingGroupMenuSTRPPATKBLL.RejectAddSTRPPATK(PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Edit" Then
                AMLBLL.MappingGroupMenuSTRPPATKBLL.RejectEditSTRPPATK(PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Delete" Then
                AMLBLL.MappingGroupMenuSTRPPATKBLL.RejectDeleteSTRPPATK(PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID)
            End If

            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKApproval.aspx"

            Me.Response.Redirect("MappingGroupMenuSTRPPATKApproval.aspx", False)


        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
