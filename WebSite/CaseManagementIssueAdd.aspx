<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="CaseManagementIssueAdd.aspx.vb" Inherits="CaseManagementIssueAdd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Case Management Issue - Add New&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <span style="color: #000000">* Required</span></td>
        </tr>
    </table>
    <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="color: #000000; border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextIssueTitle"
                    ErrorMessage="Issue title  is required">*</asp:RequiredFieldValidator><br />
            </td>
            <td bgcolor="#ffffff" style="color: #000000; height: 24px" width="20%">
                Issue Title</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:TextBox ID="TextIssueTitle" runat="server" CssClass="textBox" MaxLength="150"
                    Width="200px"></asp:TextBox>
                <strong><span style="color: #ff0000">*</span></strong></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="6" style="width: 24px; height: 24px">
                <br />
                <br />
                <br />
                <br />
                &nbsp;</td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px">
                Description<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px">
                :<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px">
                <asp:TextBox ID="TextIssueDescription" runat="server" CssClass="textBox" Height="63px"
                    MaxLength="255" TextMode="MultiLine" Width="200px"></asp:TextBox></td>
        </tr>
        <tr class="formText">
        </tr>
        <tr class="formText">
        </tr>
        <tr class="formText">
        </tr>
        <tr class="formText">
        </tr>
        <tr class="formText">
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                Issue Status</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                :</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                <asp:DropDownList ID="CboIssueStatus" runat="server" CssClass="combobox">
                </asp:DropDownList></td>
        </tr>
        <tr bgcolor="#dddddd" class="formText" height="30">
            <td style="width: 24px">
                <img height="15" src="images/arrow.gif" width="15" /></td>
            <td colspan="3" style="height: 9px">
                <table border="0" cellpadding="3" cellspacing="0">
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="True" SkinID="AddButton" /></td>
                        <td>
                            <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton" /></td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
        </tr>
    </table>

    <script>
	    document.getElementById('<%=GetFocusID %>').focus();
    </script>

</asp:Content>

