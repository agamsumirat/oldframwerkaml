<%@ Page Language="VB" AutoEventWireup="false" CodeFile="NoWorkingUnitAssignedRequest.aspx.vb" Inherits="NoWorkingUnitAssignedRequest" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<%@ Register Src="webcontrol/footer.ascx" TagName="footer" TagPrefix="uc1" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>:: Anti Money Laundering ::</title>
		<link href="theme/aml.css" rel="stylesheet" type="text/css">
	<script src="script/x_load.js" type="text/javascript"></script>
	<script language="javascript">xInclude('script/x_core.js');</script>
		<script src="script/aml.js" ></script>	
			<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	</HEAD>
	<body bgColor="#ffffff" leftMargin="0" topMargin="0" marginheight="0" marginwidth="0">
	 <form id="form1" runat="server">
	<table background="images/main-header-bg.gif" height="81" width="100%" cellpadding="0" cellspacing="0" border="0">
  	    <tr>
            <td width="153"><img src="images/main-uang-dijemur-dan-logo.jpg" width="153" height="81" /></td>
  	        <td width="357"><img src="images/main-title.jpg" width="357" height="25" /></td>
  	        <td width="99%">&nbsp;</td>
  	        <td align="right" background="images/main-bar-merah-kanan.jpg" valign="top" class="toprightmenu"><img src="images/blank.gif" width="425" height="6">
  	        </td>               
  	    </tr>
  	</table>
<table cellSpacing="4" cellPadding="0" width="100%">
   							<TBODY>   													
								<tr>
									<td class="maintitle" vAlign="bottom" width="99%" bgColor="#ffffff"><asp:label id="lblTitle" runat="server">Request for Working Unit Assignment</asp:label></td>
								</tr>
								<tr>
									<td colSpan="4" bgColor="#ffffff">
									<div id="divcontent" class="divcontent">
									    <p>
                                            <font color="#ff0000"><b>
                                                <asp:Label ID="LabelUserID" runat="server" Text="LabelUserID"></asp:Label>
                                                is currently not assigned to any Working Units</b></font>.<br />
                                            <br>
                                            Unable to continue using the Anti Money Laudering System because some of the functionalities in the Anti Money Laundering System require that a User
                                            must already be assigned to at least one Working Unit. After clicking the OK button, you will be logged out
                                            so you can contact the Administrator and ask for one or more Working Units to be
                                            assigned to you.<br />
                                            <br />
                                            <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="True" SkinID="OKButton" /><br />
                                            <br />
                                            <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator><br />
                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                                        </p>
									</div>
									</td>
								</tr>							
								<tr>
  	<%--<td  background="images/main-footer-bgV2.gif" height="24" align="right" class="logintext" style="padding-right:10px; color: #ffffff;">Copyright &copy; Bank Niaga 2007, All Right Reserved - Version 1.0.0 (Build 1235) - Developed by Sahassa</td>--%>
  	  <td align="left" colspan="4" valign="top">
            <uc1:footer ID="Footer1" runat="server"/>
            </td>
  </tr>
					</table> 					
	</form>
	<script>arrangeView();</script>
	</body>
</HTML>

