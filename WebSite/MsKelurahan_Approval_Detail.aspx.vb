#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsKelurahan_APPROVAL_Detail
    Inherits Parent

#Region "Function"

    Sub HideControl()
        imgReject.Visible = False
        imgApprove.Visible = False
        ImageCancel.Visible = False
    End Sub

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsKelurahan_Approval_View.aspx")
    End Sub

    Protected Sub imgReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgReject.Click
        Try
            Rejected()
            ChangeMultiView(1)
            LblConfirmation.Text = "Data has been rejected"
            HideControl()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgApprove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgApprove.Click
        Try
            Using ObjMsKelurahan_Approval As MsKelurahan_Approval = DataRepository.MsKelurahan_ApprovalProvider.GetByPK_MsKelurahan_Approval_Id(parID)
                If ObjMsKelurahan_Approval.FK_MsMode_Id.GetValueOrDefault = 1 Then
                    Inserdata(ObjMsKelurahan_Approval)
                ElseIf ObjMsKelurahan_Approval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                    UpdateData(ObjMsKelurahan_Approval)
                ElseIf ObjMsKelurahan_Approval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                    DeleteData(ObjMsKelurahan_Approval)
                End If
            End Using
            ChangeMultiView(1)
            LblConfirmation.Text = "Data approved successful"
            HideControl()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
                ListmapingNew = New TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail)
                ListmapingOld = New TList(Of MappingMsKelurahanNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim PkObject As String
        'Load new data
        Using ObjMsKelurahan_ApprovalDetail As MsKelurahan_ApprovalDetail = DataRepository.MsKelurahan_ApprovalDetailProvider.GetPaged(MsKelurahan_ApprovalDetailColumn.FK_MsKelurahan_Approval_Id.ToString & _
         "=" & _
         parID, "", 0, 1, Nothing)(0)
            Dim CekMode As MsKelurahan_Approval = DataRepository.MsKelurahan_ApprovalProvider.GetByPK_MsKelurahan_Approval_Id(ObjMsKelurahan_ApprovalDetail.FK_MsKelurahan_Approval_Id)
            Dim nama As String = CekMode.FK_MsMode_Id
            If nama = 1 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 2 Then
                baru.Visible = True
                lama.Visible = True
                baruNew.Visible = True
                lamaOld.Visible = True
            ElseIf nama = 3 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 4 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            End If
            With ObjMsKelurahan_ApprovalDetail
                SafeDefaultValue = "-"
                HFKecamatanNew.Value = Safe(.IDKecamatan)
                Dim OKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(.IDKecamatan.GetValueOrDefault)
                If OKecamatan IsNot Nothing Then LBSearchNamaKecamatanNew.Text = Safe(OKecamatan.NamaKecamatan)

                txtIDKelurahannew.Text = Safe(.IDKelurahan)
                txtNamaKelurahannew.Text = Safe(.NamaKelurahan)

                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByNew.Text = Omsuser(0).UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyNew.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
                lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
                txtActivationNew.Text = SafeActiveInactive(.Activation)
                Dim L_objMappingMsKelurahanNCBSPPATK_Approval_Detail As TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail)
                L_objMappingMsKelurahanNCBSPPATK_Approval_Detail = DataRepository.MappingMsKelurahanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKelurahanNCBSPPATK_Approval_DetailColumn.PK_MsKelurahan_Approval_Id.ToString & _
                 "=" & _
                 parID, "", 0, Integer.MaxValue, Nothing)
                'Add mapping
                ListmapingNew.AddRange(L_objMappingMsKelurahanNCBSPPATK_Approval_Detail)

            End With
            PkObject = ObjMsKelurahan_ApprovalDetail.IDKelurahan
        End Using

        'Load Old Data
        Using ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(PkObject)
            If ObjMsKelurahan Is Nothing Then Return
            With ObjMsKelurahan
                SafeDefaultValue = "-"
                HFKecamatanOld.Value = Safe(.IDKecamatan)
                Dim OKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(.IDKecamatan.GetValueOrDefault)
                If OKecamatan IsNot Nothing Then LBSearchNamaKecamatanOld.Text = Safe(OKecamatan.NamaKecamatan)

                txtIDKelurahanOld.Text = Safe(.IDKelurahan)
                txtNamaKelurahanOld.Text = Safe(.NamaKelurahan)

                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByOld.Text = Omsuser(0).UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyOld.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
                lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
                Dim L_objMappingMsKelurahanNCBSPPATK As TList(Of MappingMsKelurahanNCBSPPATK)
                txtActivationOld.Text = SafeActiveInactive(.Activation)
                L_objMappingMsKelurahanNCBSPPATK = DataRepository.MappingMsKelurahanNCBSPPATKProvider.GetPaged(MappingMsKelurahanNCBSPPATKColumn.IDKelurahan.ToString & _
                   "=" & _
                   PkObject, "", 0, Integer.MaxValue, Nothing)
                ListmapingOld.AddRange(L_objMappingMsKelurahanNCBSPPATK)
            End With
        End Using
    End Sub



    Protected Sub imgBtnConfirmation_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnConfirmation.Click
        Try
            Response.Redirect("MsKelurahan_approval_view.aspx")
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping New sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingNew() As TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail)
        Get
            Return Session("ListmapingNew.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail))
            Session("ListmapingNew.data") = value
        End Set
    End Property

    ''' <summary>
    ''' Menyimpan Item untuk mapping Old sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingOld() As TList(Of MappingMsKelurahanNCBSPPATK)
        Get
            Return Session("ListmapingOld.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKelurahanNCBSPPATK))
            Session("ListmapingOld.data") = value
        End Set
    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKelurahanNCBSPPATK_Approval_Detail In ListmapingNew.FindAllDistinct("IDKelurahanNCBS")
                Temp.Add(i.IDKelurahanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKelurahanNCBSPPATK In ListmapingOld.FindAllDistinct("IDKelurahanNCBS")
                Temp.Add(i.IDKelurahanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

#Region "Action Approval..."

    Sub Rejected()
        Dim ObjMsKelurahan_Approval As MsKelurahan_Approval = DataRepository.MsKelurahan_ApprovalProvider.GetByPK_MsKelurahan_Approval_Id(parID)
        Dim ObjMsKelurahan_ApprovalDetail As MsKelurahan_ApprovalDetail = DataRepository.MsKelurahan_ApprovalDetailProvider.GetPaged(MsKelurahan_ApprovalDetailColumn.FK_MsKelurahan_Approval_Id.ToString & "=" & ObjMsKelurahan_Approval.PK_MsKelurahan_Approval_Id, "", 0, 1, Nothing)(0)
        Dim L_ObjMappingMsKelurahanNCBSPPATK_Approval_Detail As TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKelurahanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKelurahanNCBSPPATK_Approval_DetailColumn.PK_MsKelurahan_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)
        DeleteApproval(ObjMsKelurahan_Approval, ObjMsKelurahan_ApprovalDetail, L_ObjMappingMsKelurahanNCBSPPATK_Approval_Detail)
    End Sub

    Sub DeleteApproval(ByRef objMsKelurahan_Approval As MsKelurahan_Approval, ByRef ObjMsKelurahan_ApprovalDetail As MsKelurahan_ApprovalDetail, ByRef L_ObjMappingMsKelurahanNCBSPPATK_Approval_Detail As TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail))
        DataRepository.MsKelurahan_ApprovalProvider.Delete(objMsKelurahan_Approval)
        DataRepository.MsKelurahan_ApprovalDetailProvider.Delete(ObjMsKelurahan_ApprovalDetail)
        DataRepository.MappingMsKelurahanNCBSPPATK_Approval_DetailProvider.Delete(L_ObjMappingMsKelurahanNCBSPPATK_Approval_Detail)
    End Sub

    ''' <summary>
    ''' Delete Data dari  asli dari approval
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DeleteData(ByVal objMsKelurahan_Approval As MsKelurahan_Approval)
        '   '================= Delete Header ===========================================================
        Dim LObjMsKelurahan_ApprovalDetail As TList(Of MsKelurahan_ApprovalDetail) = DataRepository.MsKelurahan_ApprovalDetailProvider.GetPaged(MsKelurahan_ApprovalDetailColumn.FK_MsKelurahan_Approval_Id.ToString & "=" & objMsKelurahan_Approval.PK_MsKelurahan_Approval_Id, "", 0, 1, Nothing)
        If LObjMsKelurahan_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        Dim ObjMsKelurahan_ApprovalDetail As MsKelurahan_ApprovalDetail = LObjMsKelurahan_ApprovalDetail(0)
        Using ObjNewMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(ObjMsKelurahan_ApprovalDetail.IDKelurahan)
            DataRepository.MsKelurahanProvider.Delete(ObjNewMsKelurahan)
        End Using

        '======== Delete MAPPING =========================================
        Dim L_ObjMappingMsKelurahanNCBSPPATK_Approval_Detail As TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKelurahanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKelurahanNCBSPPATK_Approval_DetailColumn.PK_MsKelurahan_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


        For Each c As MappingMsKelurahanNCBSPPATK_Approval_Detail In L_ObjMappingMsKelurahanNCBSPPATK_Approval_Detail
            Using ObjNewMappingMsKelurahanNCBSPPATK As MappingMsKelurahanNCBSPPATK = DataRepository.MappingMsKelurahanNCBSPPATKProvider.GetByPK_MappingMsKelurahanNCBSPPATK_Id(c.PK_MappingMsKelurahanNCBSPPATK_Id)
                DataRepository.MappingMsKelurahanNCBSPPATKProvider.Delete(ObjNewMappingMsKelurahanNCBSPPATK)
            End Using
        Next

        '======== Delete Approval data ======================================
        DeleteApproval(objMsKelurahan_Approval, ObjMsKelurahan_ApprovalDetail, L_ObjMappingMsKelurahanNCBSPPATK_Approval_Detail)
    End Sub

    ''' <summary>
    ''' Update Data dari approval ke table asli 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateData(ByVal objMsKelurahan_Approval As MsKelurahan_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
        '   '================= Update Header ===========================================================
        Dim LObjMsKelurahan_ApprovalDetail As TList(Of MsKelurahan_ApprovalDetail) = DataRepository.MsKelurahan_ApprovalDetailProvider.GetPaged(MsKelurahan_ApprovalDetailColumn.FK_MsKelurahan_Approval_Id.ToString & "=" & objMsKelurahan_Approval.PK_MsKelurahan_Approval_Id, "", 0, 1, Nothing)
        If LObjMsKelurahan_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        Dim ObjMsKelurahan_ApprovalDetail As MsKelurahan_ApprovalDetail = LObjMsKelurahan_ApprovalDetail(0)
        Dim ObjMsKelurahanNew As MsKelurahan
        Using ObjMsKelurahanOld As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(ObjMsKelurahan_ApprovalDetail.IDKelurahan)
            If ObjMsKelurahanOld Is Nothing Then Throw New Exception("Data Not Found")
            ObjMsKelurahanNew = ObjMsKelurahanOld.Clone
            With ObjMsKelurahanNew
                FillOrNothing(.NamaKelurahan, ObjMsKelurahan_ApprovalDetail.NamaKelurahan)
                FillOrNothing(.CreatedBy, ObjMsKelurahan_ApprovalDetail.CreatedBy)
                FillOrNothing(.CreatedDate, ObjMsKelurahan_ApprovalDetail.CreatedDate)
                FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
                FillOrNothing(.ApprovedDate, Date.Now)
                .Activation = True
                .IDKecamatan = ObjMsKelurahan_ApprovalDetail.IDKecamatan
            End With
            DataRepository.MsKelurahanProvider.Save(ObjMsKelurahanNew)
            'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "MsKelurahan has Approved to Update data", ObjMsKelurahanNew, ObjMsKelurahanOld)
            '======== Update MAPPING =========================================
            'delete mapping item
            Using L_ObjMappingMsKelurahanNCBSPPATK As TList(Of MappingMsKelurahanNCBSPPATK) = DataRepository.MappingMsKelurahanNCBSPPATKProvider.GetPaged(MappingMsKelurahanNCBSPPATKColumn.IDKelurahan.ToString & _
             "=" & _
             ObjMsKelurahanNew.IDKelurahan, "", 0, Integer.MaxValue, Nothing)
                DataRepository.MappingMsKelurahanNCBSPPATKProvider.Delete(L_ObjMappingMsKelurahanNCBSPPATK)
            End Using
            'Insert mapping item
            Dim L_ObjMappingMsKelurahanNCBSPPATK_Approval_Detail As TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKelurahanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKelurahanNCBSPPATK_Approval_DetailColumn.PK_MsKelurahan_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            Dim L_ObjNewMappingMsKelurahanNCBSPPATK As New TList(Of MappingMsKelurahanNCBSPPATK)
            For Each c As MappingMsKelurahanNCBSPPATK_Approval_Detail In L_ObjMappingMsKelurahanNCBSPPATK_Approval_Detail
                Dim ObjNewMappingMsKelurahanNCBSPPATK As New MappingMsKelurahanNCBSPPATK '= DataRepository.MappingMsKelurahanNCBSPPATKProvider.GetByPK_MappingMsKelurahanNCBSPPATK_Id(c.PK_MappingMsKelurahanNCBSPPATK_Id.GetValueOrDefault)
                With ObjNewMappingMsKelurahanNCBSPPATK
                    FillOrNothing(.IDKelurahan, ObjMsKelurahanNew.IDKelurahan)
                    FillOrNothing(.Nama, c.Nama)
                    FillOrNothing(.IDKelurahanNCBS, c.IDKelurahanNCBS)
                    L_ObjNewMappingMsKelurahanNCBSPPATK.Add(ObjNewMappingMsKelurahanNCBSPPATK)
                End With
            Next
            DataRepository.MappingMsKelurahanNCBSPPATKProvider.Save(L_ObjNewMappingMsKelurahanNCBSPPATK)
            '======== Delete Approval data ======================================
            DeleteApproval(objMsKelurahan_Approval, ObjMsKelurahan_ApprovalDetail, L_ObjMappingMsKelurahanNCBSPPATK_Approval_Detail)
        End Using
    End Sub
    ''' <summary>
    ''' Insert Data dari approval ke table asli 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Inserdata(ByVal objMsKelurahan_Approval As MsKelurahan_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
        '   '================= Save Header ===========================================================
        Dim LObjMsKelurahan_ApprovalDetail As TList(Of MsKelurahan_ApprovalDetail) = DataRepository.MsKelurahan_ApprovalDetailProvider.GetPaged(MsKelurahan_ApprovalDetailColumn.FK_MsKelurahan_Approval_Id.ToString & "=" & objMsKelurahan_Approval.PK_MsKelurahan_Approval_Id, "", 0, 1, Nothing)
        'jika data tidak ada di database
        If LObjMsKelurahan_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        'binding approval ke table asli
        Dim ObjMsKelurahan_ApprovalDetail As MsKelurahan_ApprovalDetail = LObjMsKelurahan_ApprovalDetail(0)
        Using ObjNewMsKelurahan As New MsKelurahan()
            With ObjNewMsKelurahan
                FillOrNothing(.IDKelurahan, ObjMsKelurahan_ApprovalDetail.IDKelurahan)
                FillOrNothing(.NamaKelurahan, ObjMsKelurahan_ApprovalDetail.NamaKelurahan)
                FillOrNothing(.CreatedBy, ObjMsKelurahan_ApprovalDetail.CreatedBy)
                FillOrNothing(.CreatedDate, ObjMsKelurahan_ApprovalDetail.CreatedDate)
                FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
                FillOrNothing(.ApprovedDate, Date.Now)
                .Activation = True
                .IDKecamatan = ObjMsKelurahan_ApprovalDetail.IDKecamatan
            End With
            DataRepository.MsKelurahanProvider.Save(ObjNewMsKelurahan)
            'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "MsKelurahan has Approved to Insert data", ObjNewMsKelurahan, Nothing)
            '======== Insert MAPPING =========================================
            Dim L_ObjMappingMsKelurahanNCBSPPATK_Approval_Detail As TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKelurahanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKelurahanNCBSPPATK_Approval_DetailColumn.PK_MsKelurahan_Approval_Id.ToString & _
             "=" & _
             parID, "", 0, Integer.MaxValue, Nothing)
            Dim L_ObjNewMappingMsKelurahanNCBSPPATK As New TList(Of MappingMsKelurahanNCBSPPATK)()
            For Each c As MappingMsKelurahanNCBSPPATK_Approval_Detail In L_ObjMappingMsKelurahanNCBSPPATK_Approval_Detail
                Dim ObjNewMappingMsKelurahanNCBSPPATK As New MappingMsKelurahanNCBSPPATK()
                With ObjNewMappingMsKelurahanNCBSPPATK
                    FillOrNothing(.IDKelurahan, ObjNewMsKelurahan.IDKelurahan)
                    FillOrNothing(.Nama, c.Nama)
                    FillOrNothing(.IDKelurahanNCBS, c.IDKelurahanNCBS)
                    L_ObjNewMappingMsKelurahanNCBSPPATK.Add(ObjNewMappingMsKelurahanNCBSPPATK)
                End With
            Next
            DataRepository.MappingMsKelurahanNCBSPPATKProvider.Save(L_ObjNewMappingMsKelurahanNCBSPPATK)
            '======== Delete Approval data ======================================
            DeleteApproval(objMsKelurahan_Approval, ObjMsKelurahan_ApprovalDetail, L_ObjMappingMsKelurahanNCBSPPATK_Approval_Detail)
        End Using
    End Sub

#End Region

End Class


