<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MsProvince_UploadApprovalDetail_Detail.aspx.vb" Inherits="MsProvince_UploadApprovalDetail_Detail"  MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
	<script src="script/popcalendar.js"></script>
	<script language="javascript" type="text/javascript">
		function hidePanel(objhide, objpanel, imgmin, imgmax) {
			document.getElementById(objhide).style.display = 'none';
			document.getElementById(objpanel).src = imgmax;
		}
		// JScript File
		//Call picker master 

	  
	</script>
	<table cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>
				<img src="Images/blank.gif" width="5" height="1" />
			</td>
			<td>
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td>
							&nbsp;
						</td>
						<td width="99%" bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" />
						</td>
						<td bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" />
						</td>
					</tr>
				</table>
			</td>
			<td>
				<img src="Images/blank.gif" width="5" height="1" />
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td id="tdcontent" valign="top" bgcolor="#FFFFFF">
				
					<table id="tblpenampung" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td style="width: 14px">
								<img src="Images/blank.gif" width="20" height="100%" />
							</td>
							<td class="divcontentinside" bgcolor="#FFFFFF" style="width: 100%">
								&nbsp;
								<ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%" meta:resourcekey="AjaxPanel5Resource1">
									<asp:MultiView ID="MtvMsUser" runat="server" ActiveViewIndex="0">
										<asp:View ID="VwAdd" runat="server">
											<table cellspacing="0" cellpadding="0" border="0">
												<tbody>
												<tr>
																	<td bgcolor="#ffffff" colspan="2" style="font-size: 18px; border-top-style: none;
																		border-right-style: none; border-left-style: none; border-bottom-style: none"
																		valign="top">
																		<img src="Images/dot_title.gif" width="17" height="17">
																		<asp:Label ID="LblValue" runat="server" Font-Bold="True" Font-Size="Medium" 
																			Text="Master Province Upload Approval Detail "></asp:Label>
																		<hr />
																	</td>
																</tr>
													<tr>
														
                                                        <td id="baruNew" runat="server">
															<asp:Label ID="LblNewTitle" runat="server" Text="NEW" Font-Bold="True" Font-Size="Small"
																ForeColor="Blue"></asp:Label>
														</td>
														<td id="lamaOld" runat="server">
															<asp:Label ID="LblOldTitle" runat="server" Text="OLD" Font-Bold="True" Font-Size="Small"
																ForeColor="Blue"></asp:Label>
														</td>
													</tr>
													<tr>
														
                                                        <td id="baru" runat="server">
															<table id="TableNew" style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
																width="100%" bgcolor="#dddddd" border="0">
																
																<tr>
																	<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
																		border-right-style: none; border-left-style: none; border-bottom-style: none;
																		height: 26px;" valign="top">
																	</td>
																</tr>
												
																<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
        &nbsp;
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="lblIdProvincenew" runat="server" text="ID"></asp:label>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td style="height: 24px" valign="top">
        <asp:label id="txtIdProvincenew" runat="server" maxlength="255" tabindex="2" tooltip="IdProvince"
            width="400px"></asp:label>
    </td>
</tr>

<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
        &nbsp;
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="lblCodenew" runat="server" text="Code"></asp:label>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td style="height: 24px" valign="top">
        <asp:label id="txtCodenew" runat="server" maxlength="255" tabindex="2" tooltip="Code"
            width="400px"></asp:label>
    </td>
</tr>

<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
        &nbsp;
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="lblNamanew" runat="server" text="Nama"></asp:label>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td style="height: 24px" valign="top">
        <asp:label id="txtNamanew" runat="server" maxlength="255" tabindex="2" tooltip="Nama"
            width="400px"></asp:label>
    </td>
</tr>

<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
        &nbsp;
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="lblDescriptionnew" runat="server" text="Description"></asp:label>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td style="height: 24px" valign="top">
        <asp:label id="txtDescriptionnew" runat="server" maxlength="255" tabindex="2" tooltip="Description"
            width="400px"></asp:label>
    </td>
</tr>



																<tr bgcolor="#ffffff">
																	<td style="width: 22px; height: 24px">
																		&nbsp;
																	</td>
																	<td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
																		Mapping Item
																	</td>
																	<td style="width: 6px; height: 24px" valign="top">
																		:
																	</td>
																	<td style="height: 24px" valign="top">
																		<asp:ListBox ID="LBMappingNew" runat="server" Height="204px" Width="170px" 
																			CssClass="textbox">
																		</asp:ListBox>
																	</td>
																</tr>
																<tr bgcolor="#ffffff">
																	<td style="width: 22px; height: 24px">
																		&nbsp;
																	</td>
																	<td bgcolor="#fff7e6" style="height: 24px" valign="top">
																		Created Date
																	</td>
																	<td style="width: 6px; height: 24px" valign="top">
																		:
																	</td>
																	<td style="height: 24px" valign="top">
																		<asp:Label ID="lblCreatedDateNew" runat="server" ForeColor="#404040"></asp:Label>
																	</td>
																</tr>
																<tr bgcolor="#ffffff">
																	<td style="width: 22px; height: 24px">
																		&nbsp;
																	</td>
																	<td bgcolor="#fff7e6" style="height: 24px" valign="top">
																		Created By
																	</td>
																	<td style="width: 6px; height: 24px" valign="top">
																		:
																	</td>
																	<td style="height: 24px" valign="top">
																		<asp:Label ID="lblCreatedByNew" runat="server" ForeColor="#404040"></asp:Label>
																	</td>
																</tr>
															    <tr bgcolor="#ffffff">
                                                                    <td style="width: 22px; height: 24px">
                                                                        &nbsp;</td>
                                                                    <td bgcolor="#fff7e6" style="height: 24px" valign="top">
                                                                        Updated Date
                                                                    </td>
                                                                    <td style="width: 6px; height: 24px" valign="top">
                                                                        :
                                                                    </td>
                                                                    <td style="height: 24px" valign="top">
                                                                        <asp:Label ID="lblUpdatedDateNew" runat="server" ForeColor="#404040"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr bgcolor="#ffffff">
                                                                    <td style="width: 22px; height: 24px">
                                                                        &nbsp;</td>
                                                                    <td bgcolor="#fff7e6" style="height: 24px" valign="top">
                                                                        Updated By
                                                                    </td>
                                                                    <td style="width: 6px; height: 24px" valign="top">
                                                                        :
                                                                    </td>
                                                                    <td style="height: 24px" valign="top">
                                                                        <asp:Label ID="lblUpdatedbyNew" runat="server" ForeColor="#404040"></asp:Label>
                                                                    </td>
                                                                </tr>
															</table>
														</td>
														<td id="lama" runat="server">
														<table id="tableOld" style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
																width="100%" bgcolor="#dddddd" border="0">
																
																<tr>
																	<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
																		border-right-style: none; border-left-style: none; border-bottom-style: none;
																		height: 26px;" valign="top">
																	</td>
																</tr>
															<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
        &nbsp;
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="lblIdProvinceOld" runat="server" text="ID"></asp:label>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td style="height: 24px" valign="top">
        <asp:label id="txtIdProvinceOld" runat="server" maxlength="255" tabindex="2" tooltip="IdProvince"
            width="400px"></asp:label>
    </td>
</tr>

<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
        &nbsp;
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="lblCodeOld" runat="server" text="Code"></asp:label>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td style="height: 24px" valign="top">
        <asp:label id="txtCodeOld" runat="server" maxlength="255" tabindex="2" tooltip="Code"
            width="400px"></asp:label>
    </td>
</tr>

<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
        &nbsp;
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="lblNamaOld" runat="server" text="Nama"></asp:label>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td style="height: 24px" valign="top">
        <asp:label id="txtNamaOld" runat="server" maxlength="255" tabindex="2" tooltip="Nama"
            width="400px"></asp:label>
    </td>
</tr>

<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
        &nbsp;
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="lblDescriptionOld" runat="server" text="Description"></asp:label>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td style="height: 24px" valign="top">
        <asp:label id="txtDescriptionOld" runat="server" maxlength="255" tabindex="2" tooltip="Description"
            width="400px"></asp:label>
    </td>
</tr>


																<tr bgcolor="#ffffff">
																	<td style="width: 22px; height: 24px">
																		&nbsp;
																	</td>
																	<td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
																		Mapping Item
																	</td>
																	<td style="width: 6px; height: 24px" valign="top">
																		:
																	</td>
																	<td style="height: 24px" valign="top">
																		<asp:ListBox ID="LBmappingOld" runat="server" Height="204px" Width="170px" 
																			CssClass="textbox">
																		</asp:ListBox>
																	</td>
																</tr>
																<tr bgcolor="#ffffff">
																	<td style="width: 22px; height: 24px">
																		&nbsp;
																	</td>
																	<td bgcolor="#fff7e6" style="height: 24px" valign="top">
																		Created Date
																	</td>
																	<td style="width: 6px; height: 24px" valign="top">
																		:
																	</td>
																	<td style="height: 24px" valign="top">
																		<asp:Label ID="lblCreatedDateOld" runat="server" ForeColor="#404040"></asp:Label>
																	</td>
																</tr>
																<tr bgcolor="#ffffff">
                                                                    <td style="width: 22px; height: 24px">
                                                                        &nbsp;</td>
                                                                    <td bgcolor="#fff7e6" style="height: 24px" valign="top">
                                                                        Created By
                                                                    </td>
                                                                    <td style="width: 6px; height: 24px" valign="top">
                                                                        :
                                                                    </td>
                                                                    <td style="height: 24px" valign="top">
                                                                        <asp:Label ID="lblCreatedByOld" runat="server" ForeColor="#404040"></asp:Label>
                                                                    </td>
                                                                </tr>
																<tr bgcolor="#ffffff">
																	<td style="width: 22px; height: 24px">
																		&nbsp;</td>
																	<td bgcolor="#fff7e6" style="height: 24px" valign="top">
																		Updated Date
																	</td>
																	<td style="width: 6px; height: 24px" valign="top">
																		:
																	</td>
																	<td style="height: 24px" valign="top">
																		<asp:Label ID="lblUpdatedDateOld" runat="server" ForeColor="#404040"></asp:Label>
																	</td>
																</tr>
															    <tr bgcolor="#ffffff">
                                                                    <td style="width: 22px; height: 24px">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td bgcolor="#fff7e6" style="height: 24px" valign="top">
                                                                        Updated By
                                                                    </td>
                                                                    <td style="width: 6px; height: 24px" valign="top">
                                                                        :
                                                                    </td>
                                                                    <td style="height: 24px" valign="top">
                                                                        <asp:Label ID="lblUpdatedbyOld" runat="server" ForeColor="#404040"></asp:Label>
                                                                    </td>
                                                                </tr>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</asp:View>
									</asp:MultiView>
								</ajax:AjaxPanel>
							</td>
						</tr>
					</table>
				
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
				<ajax:AjaxPanel ID="AjaxPanel1" runat="server">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td background="Images/button-bground.gif" align="left" valign="middle">
								<img src="Images/blank.gif" width="5" height="1" />
							</td>
							<td background="Images/button-bground.gif" align="left" valign="middle">
								&nbsp;
							</td>
							<td background="Images/button-bground.gif" style="width: 5px">
								&nbsp;
							</td>
							<td background="Images/button-bground.gif">
								&nbsp;</td>
							<td background="Images/button-bground.gif">
								&nbsp;</td>
							<td background="Images/button-bground.gif">
								&nbsp;<asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" 
									ImageUrl="~/Images/button/back.gif"></asp:ImageButton>
							</td>
							<td width="99%" background="Images/button-bground.gif">
								<img src="Images/blank.gif" width="1" height="1" />
								<asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" meta:resourcekey="CvalHandleErrResource1"
									ValidationGroup="handle"></asp:CustomValidator>
								<asp:CustomValidator ID="CvalPageErr" runat="server" Display="None" meta:resourcekey="CvalPageErrResource1"></asp:CustomValidator>
							</td>
							<td>
								
							</td>
						</tr>
					</table>
				</ajax:AjaxPanel>
			</td>
		</tr>
	</table>
</asp:Content>


