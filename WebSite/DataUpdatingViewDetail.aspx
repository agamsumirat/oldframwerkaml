<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DataUpdatingViewDetail.aspx.vb" Inherits="DataUpdatingViewDetail" MasterPageFile="~/MasterPage.master" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Data Updating View Detail
                    <hr />
                </strong>
                </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000"></span></td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 2%; height: 24px"><br />
                </td>
			<td width="20%" bgColor="#ffffff" style="height: 24px; width: 10%;">
                CIF No</td>
			<td width="5" bgColor="#ffffff" style="height: 24px; width: 1%;">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px">
                &nbsp;<asp:Label ID="LblCIFNo" 
                    runat="server"></asp:Label></td>
		</tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 2%; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="width: 10%; height: 24px" width="20%">
                CIF Name</td>
            <td bgcolor="#ffffff" style="width: 1%; height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                &nbsp;<asp:Label ID="LblCIFName" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 2%; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="width: 10%; height: 24px" width="20%">
                Data Updating Reason</td>
            <td bgcolor="#ffffff" style="width: 1%; height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                &nbsp;<asp:Label ID="LblDataUpdatingReason" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 2%; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="width: 10%; height: 24px" width="20%">
                Data Updating Date</td>
            <td bgcolor="#ffffff" style="width: 1%; height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                &nbsp;<asp:Label ID="LblDataUpdatingDate" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" runat="server" id="datanotfound">
            <td bgcolor="#ffffff" style="width: 2%; height: 24px">
            </td>
            <td bgcolor="#ffffff" colspan="3" style="color: #ff0000; height: 24px" align="center">
                &nbsp;</td>
        </tr>
		<tr class="formText" id="datafound" runat="server">
			<td bgColor="#ffffff" style="width: 24px; height: 25px;">
                </td>
            <td bgcolor="#ffffff" colspan="3" style="height: 25px">
            <ajax:AjaxPanel ID="detail" runat="server">
                </ajax:AjaxPanel></td>
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="false" SkinID="BackButton"></asp:imagebutton></td>
						</tr>
				</table>               
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	
</asp:Content>
