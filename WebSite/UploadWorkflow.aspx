<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UploadWorkflow.aspx.vb" Inherits="UploadWorkflow"  %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">

<ajax:ajaxpanel id="AjaxPanel1" runat="server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">

<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Upload WorkFlow
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000"><asp:CustomValidator ID="cvalPageError" runat="server"
                    Display="None"></asp:CustomValidator></span></td>
        </tr>
    </table>	
    <table style="width: 100%">
        <tr>
            <td style="width: 200px" valign="top">
                Download Template</td>
            <td style="width: 1px" valign="top">
                :</td>
            <td colspan="2" valign="top">
                <asp:LinkButton ajaxcall="none" ID="LnkDownloadTemplate" runat="server">Download Template Workflow</asp:LinkButton></td>
        </tr>
        <tr>
            <td style="width: 200px" valign="top">
            </td>
            <td style="width: 1px" valign="top">
            </td>
            <td colspan="2" valign="top">
            </td>
        </tr>
        <tr>
            <td style="width: 200px" valign="top">
                Upload Template</td>
            <td style="width: 1px" valign="top">
                :</td>
            <td valign="top" style="width: 250px">
                <asp:FileUpload ID="FileUpload1" runat="server" />&nbsp;&nbsp;&nbsp;
            </td>
            <td valign="top">
                <asp:ImageButton ajaxcall="none" ID="ImgUpload" runat="server" ImageUrl="~/Images/button/save.gif" /></td>
        </tr>
    </table>
    

        </asp:View>
        <asp:View ID="View2" runat="server">
            &nbsp;<table style="width: 100%">
                <tr>
                    <td align="center">
                        <asp:Label ID="LblMessage" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:ImageButton ID="ImgBack" runat="server" ImageUrl="~/Images/button/ok.gif" /></td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    </ajax:ajaxpanel>
</asp:Content>

