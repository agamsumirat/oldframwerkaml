Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Imports System.IO
Imports System.Collections.Generic

Partial Class MappingReportBuilderWorkingUnitDelete
    Inherits Parent

    Public ReadOnly Property PK_MappingReportBuilderWorkingUnit_ID() As Integer
        Get
            Dim strTemp As String = Request.Params("PK_MappingReportBuilderWorkingUnit_ID")
            Dim intResult As Integer
            If Integer.TryParse(strTemp, intResult) Then
                Return intResult
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Parameter PK_MappingReportBuilderWorkingUnit_ID is invalid."

            End If
        End Get
    End Property

    Public ReadOnly Property ObjWorkingUnitdata() As TList(Of WorkingUnit)
        Get
            If Session("MappingReportBuilderWorkingUnitDelete.ObjWorkingUnitdata") Is Nothing Then
                Session("MappingReportBuilderWorkingUnitDelete.ObjWorkingUnitdata") = New TList(Of WorkingUnit)
            End If
            Return CType(Session("MappingReportBuilderWorkingUnitDelete.ObjWorkingUnitdata"), TList(Of WorkingUnit))
        End Get
    End Property

    Public ReadOnly Property ObjWorkingUnit() As TList(Of mappingreportBuilderWorkingUnitmap)
        Get
            If Session("MappingReportBuilderWorkingUnitDelete.ObjWorkingUnit") Is Nothing Then
                Session("MappingReportBuilderWorkingUnitDelete.ObjWorkingUnit") = DataRepository.mappingreportBuilderWorkingUnitmapProvider.GetPaged("FK_MappingReportBuilderWorkingUnit_ID = '" & PK_MappingReportBuilderWorkingUnit_ID & "'", "", 0, Integer.MaxValue, 0)
            End If
            Return CType(Session("MappingReportBuilderWorkingUnitDelete.ObjWorkingUnit"), TList(Of mappingreportBuilderWorkingUnitmap))
        End Get
    End Property

    Public ReadOnly Property Objvw_MappingReportBuilderWorkingUnit() As VList(Of vw_MappingReportBuilderWorkingUnit)
        Get
            If Session("MappingReportBuilderWorkingUnitDelete.Objvw_MappingReportBuilderWorkingUnit") Is Nothing Then

                Session("MappingReportBuilderWorkingUnitDelete.Objvw_MappingReportBuilderWorkingUnit") = DataRepository.vw_MappingReportBuilderWorkingUnitProvider.GetPaged("PK_MappingReportBuilderWorkingUnit_ID = '" & PK_MappingReportBuilderWorkingUnit_ID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("MappingReportBuilderWorkingUnitDelete.Objvw_MappingReportBuilderWorkingUnit"), VList(Of vw_MappingReportBuilderWorkingUnit))
        End Get
    End Property


    Private Sub loaddata()

        If Objvw_MappingReportBuilderWorkingUnit.Count > 0 Then

            TxtDelete.Text = Objvw_MappingReportBuilderWorkingUnit(0).QueryName
            For Each data As mappingreportBuilderWorkingUnitmap In ObjWorkingUnit
                Using ObjWorkingUnitDataaa As WorkingUnit = DataRepository.WorkingUnitProvider.GetByWorkingUnitID(CInt(data.FK_WorkingUnit_ID))
                    Dim listData As New WorkingUnit
                    With listData
                        .WorkingUnitName = ObjWorkingUnitDataaa.WorkingUnitName
                    End With
                    ObjWorkingUnitdata.Add(listData)
                End Using
            Next

            gridView.DataSource = ObjWorkingUnitdata
            gridView.DataBind()



        Else
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = MappingReportBuilderWorkingUnitBLL.DATA_NOTVALID
        End If
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oTrans As SqlTransaction = Nothing
        Try

            Using ObjList As MappingReportBuilderWorkingUnit = DataRepository.MappingReportBuilderWorkingUnitProvider.GetByPK_MappingReportBuilderWorkingUnit_ID(PK_MappingReportBuilderWorkingUnit_ID)
                If ObjList Is Nothing Then
                    Throw New AMLBLL.SahassaException(MappingReportBuilderWorkingUnitEnum.DATA_NOTVALID)
                End If
            End Using

            Dim FK_Report_ID As Integer = 0


            Dim QueryName As String = ""

            Using Objlist As TList(Of MappingReportBuilderWorkingUnit) = DataRepository.MappingReportBuilderWorkingUnitProvider.GetPaged("PK_MappingReportBuilderWorkingUnit_ID = '" & PK_MappingReportBuilderWorkingUnit_ID & "'", "", 0, Integer.MaxValue, 0)
                If Objlist.Count > 0 Then

                    AMLBLL.MappingReportBuilderWorkingUnitBLL.IsDataValidDeleteApproval(CStr(PK_MappingReportBuilderWorkingUnit_ID))

                    If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                        Using Objvw_MappingReportBuilderWorkingUnit As VList(Of vw_MappingReportBuilderWorkingUnit) = DataRepository.vw_MappingReportBuilderWorkingUnitProvider.GetPaged("PK_MappingReportBuilderWorkingUnit_ID = '" & PK_MappingReportBuilderWorkingUnit_ID & "'", "", 0, Integer.MaxValue, 0)
                            Me.LblSuccess.Text = "Terimakasih data " & Objvw_MappingReportBuilderWorkingUnit(0).QueryName & " sudah dihapus ."
                            Me.LblSuccess.Visible = True
                            Using ObjMappingReportBuilderWorkingUnit As TList(Of MappingReportBuilderWorkingUnit) = DataRepository.MappingReportBuilderWorkingUnitProvider.GetPaged("PK_MappingReportBuilderWorkingUnit_ID = '" & PK_MappingReportBuilderWorkingUnit_ID & "'", "", 0, Integer.MaxValue, 0)
                                FK_Report_ID = CInt(ObjMappingReportBuilderWorkingUnit(0).FK_Report_ID)
                                Dim nama As String

                                DataRepository.MappingReportBuilderWorkingUnitProvider.Delete(ObjMappingReportBuilderWorkingUnit)
                                Using ObjmappingreportBuilderWorkingUnitmap As TList(Of mappingreportBuilderWorkingUnitmap) = DataRepository.mappingreportBuilderWorkingUnitmapProvider.GetPaged("FK_MappingReportBuilderWorkingUnit_ID = '" & PK_MappingReportBuilderWorkingUnit_ID & "'", "", 0, Integer.MaxValue, 0)
                                    Using ObjQueryBuilder As QueryBuilder = DataRepository.QueryBuilderProvider.GetByPK_QueryBuilder_ID(CInt(FK_Report_ID))
                                        AMLBLL.MappingUserIdHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingReportBuilderWorkingUnit", "QueryName", "Delete", "", ObjQueryBuilder.QueryName, "Acc")
                                    End Using
                                    For Each ObjAuditList As mappingreportBuilderWorkingUnitmap In ObjmappingreportBuilderWorkingUnitmap
                                        Using objWorkingUnit As WorkingUnit = DataRepository.WorkingUnitProvider.GetByWorkingUnitID(CInt(ObjAuditList.FK_WorkingUnit_ID))
                                            nama = objWorkingUnit.WorkingUnitName
                                            AMLBLL.MappingReportBuilderWorkingUnitBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingReportBuilderWorkingUnit", "WorkingUnitName", "Delete", "", nama, "acc")
                                        End Using
                                    Next
                                    DataRepository.mappingreportBuilderWorkingUnitmapProvider.Delete(ObjmappingreportBuilderWorkingUnitmap)
                                End Using
                            End Using
                        End Using
                    Else
                        Using objlistapprovaldetail As TList(Of MappingReportBuilderWorkingUnit_ApprovalDetail) = DataRepository.MappingReportBuilderWorkingUnit_ApprovalDetailProvider.GetPaged("PK_MappingReportBuilderWorkingUnit_ID = '" & PK_MappingReportBuilderWorkingUnit_ID & "'", "", 0, Integer.MaxValue, 0)
                            If objlistapprovaldetail.Count > 0 Then
                                Me.LblSuccess.Text = "data tersebut sudah ada didalam pending approval."
                                Me.LblSuccess.Visible = True
                            Else

                                Using ObjmappingreportBuilderWorkingUnit_approval As New MappingReportBuilderWorkingUnit_Approval
                                    Using ObjmappingreportBuilderWorkingUnitList As TList(Of MappingReportBuilderWorkingUnit) = DataRepository.MappingReportBuilderWorkingUnitProvider.GetPaged("FK_Report_ID = '" & FK_Report_ID & "'", "", 0, Integer.MaxValue, 0)
                                        If ObjmappingreportBuilderWorkingUnitList.Count > 0 Then
                                            Sahassa.AML.Commonly.SessionIntendedPage = "MappingReportBuilderWorkingUnitView.aspx"
                                            Me.Response.Redirect("MappingReportBuilderWorkingUnitView.aspx", False)

                                        Else
                                            Using ObjMappingReportBuilderWorkingUnit As TList(Of MappingReportBuilderWorkingUnit) = DataRepository.MappingReportBuilderWorkingUnitProvider.GetPaged("PK_MappingReportBuilderWorkingUnit_ID = '" & PK_MappingReportBuilderWorkingUnit_ID & "'", "", 0, Integer.MaxValue, 0)
                                                FK_Report_ID = CInt(ObjMappingReportBuilderWorkingUnit(0).FK_Report_ID)

                                                Using objQueryBuilder As TList(Of QueryBuilder) = DataRepository.QueryBuilderProvider.GetPaged("PK_QueryBuilder_ID = '" & FK_Report_ID & "'", "", 0, Integer.MaxValue, 0)
                                                    QueryName = objQueryBuilder(0).QueryDescription
                                                    ObjmappingreportBuilderWorkingUnit_approval.FK_MsUserID = Sahassa.AML.Commonly.SessionPkUserId
                                                    ObjmappingreportBuilderWorkingUnit_approval.QueryName = QueryName
                                                    ObjmappingreportBuilderWorkingUnit_approval.FK_ModeID = CInt(Sahassa.AML.Commonly.TypeMode.Delete)
                                                    ObjmappingreportBuilderWorkingUnit_approval.CreatedDate = Now
                                                    DataRepository.MappingReportBuilderWorkingUnit_ApprovalProvider.Save(ObjmappingreportBuilderWorkingUnit_approval)

                                                    Using ObjmappingreportBuilderWorkingUnit_approvalDetail As New MappingReportBuilderWorkingUnit_ApprovalDetail
                                                        ObjmappingreportBuilderWorkingUnit_approvalDetail.FK_MappingReportBuilderWorkingUnit_Approval_ID = ObjmappingreportBuilderWorkingUnit_approval.PK_MappingReportBuilderWorkingUnit_Approval_ID
                                                        ObjmappingreportBuilderWorkingUnit_approvalDetail.PK_MappingReportBuilderWorkingUnit_ID = PK_MappingReportBuilderWorkingUnit_ID
                                                        ObjmappingreportBuilderWorkingUnit_approvalDetail.FK_Report_ID = CType(FK_Report_ID, Global.System.Nullable(Of Integer))
                                                        DataRepository.MappingReportBuilderWorkingUnit_ApprovalDetailProvider.Save(ObjmappingreportBuilderWorkingUnit_approvalDetail)

                                                        AMLBLL.MappingReportBuilderWorkingUnitBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingReportBuilderWorkingUnit", "QueryName", "Delete", "", QueryName, "PendingApproval")

                                                        For Each FileList As mappingreportBuilderWorkingUnitmap In ObjWorkingUnit
                                                            Dim ObjmappingreportBuilderWorkingUnit_approvalDetailmap As New mappingreportBuilderWorkingUnit_approvalDetailMap
                                                            With ObjmappingreportBuilderWorkingUnit_approvalDetailmap
                                                                Dim nama As String
                                                                .FK_MappingReportBuilderWorkingUnit_Approval_ID = CType(ObjmappingreportBuilderWorkingUnit_approval.PK_MappingReportBuilderWorkingUnit_Approval_ID, Global.System.Nullable(Of Integer))
                                                                .FK_WorkingUnit_ID = FileList.FK_WorkingUnit_ID
                                                                .FK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID = CType(ObjmappingreportBuilderWorkingUnit_approvalDetail.PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID, Global.System.Nullable(Of Integer))
                                                                .FK_MappingReportBuilderWorkingUnit_ID = ObjMappingReportBuilderWorkingUnit(0).PK_MappingReportBuilderWorkingUnit_ID
                                                                Using objWorkingUnit As WorkingUnit = DataRepository.WorkingUnitProvider.GetByWorkingUnitID(CInt(FileList.FK_WorkingUnit_ID))
                                                                    nama = objWorkingUnit.WorkingUnitName
                                                                    AMLBLL.MappingReportBuilderWorkingUnitBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingReportBuilderWorkingUnit", "WorkingUnitName", "Delete", "", nama, "PendingApproval")
                                                                End Using
                                                                DataRepository.mappingreportBuilderWorkingUnit_approvalDetailMapProvider.Save(ObjmappingreportBuilderWorkingUnit_approvalDetailmap)
                                                            End With
                                                        Next

                                                    End Using
                                                End Using
                                                'Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEView.aspx"
                                                'Me.Response.Redirect("MappingUserIdHIPORTJIVEView.aspx", False)
                                                Using ObjQueryBuilder As TList(Of QueryBuilder) = DataRepository.QueryBuilderProvider.GetPaged("PK_QueryBuilder_ID = '" & FK_Report_ID & "'", "", 0, Integer.MaxValue, 0)
                                                    Me.LblSuccess.Text = "Terimakasih data " & ObjQueryBuilder(0).QueryName & " sudah masuk kedalam Pending Approval ."
                                                    Me.LblSuccess.Visible = True
                                                End Using
                                            End Using
                                        End If
                                    End Using
                                End Using
                            End If
                        End Using
                    End If
                    MultiView1.ActiveViewIndex = 1
                Else
                    Me.LblSuccess.Text = "Data tersebut sudah terhapus ."
                    Me.LblSuccess.Visible = True
                End If
            End Using

        Catch ex As Exception
            If Not oTrans Is Nothing Then oTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
                oTrans = Nothing
            End If
        End Try
    End Sub



    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingReportBuilderWorkingUnitView.aspx"
            Me.Response.Redirect("MappingReportBuilderWorkingUnitView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                '    Transcope.Complete()
            End Using
            Session("MappingReportBuilderWorkingUnitDelete.ObjWorkingUnitdata") = Nothing
            Session("MappingReportBuilderWorkingUnitDelete.Objvw_MappingReportBuilderWorkingUnit") = Nothing
            If Not Page.IsPostBack Then
                loaddata()
                MultiView1.ActiveViewIndex = 0
            End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingReportBuilderWorkingUnitView.aspx"
            Me.Response.Redirect("MappingReportBuilderWorkingUnitView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class



