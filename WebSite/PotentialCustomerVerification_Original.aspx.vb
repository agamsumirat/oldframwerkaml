﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class PotentialCustomerVerification
    Inherits Parent


    Public Property FK_AML_Screening_Customer_Request_Id() As String
        Get
            Return Session("FK_AML_Screening_Customer_Request_Id")
        End Get
        Set(ByVal value As String)
            Session("FK_AML_Screening_Customer_Request_Id") = value
        End Set
    End Property
    Private Sub PotentialCustomerVerification_Load(sender As Object, e As EventArgs) Handles Me.Load

        Try


            If FK_AML_Screening_Customer_Request_Id Is Nothing Then
                PanelResult.Visible = False
                PanelCustomerScreening.Visible = True

            Else

                PanelResult.Visible = True
                PanelCustomerScreening.Visible = False
                BindGrid()
                If GrdResult.Rows.Count > 0 Then
                    lblSearching.Text = ""
                    LoadDataSearching()
                    'Response.Redirect("PotentialCustomerVerification.aspx", False)

                    'MagicAjax.AjaxCallHelper.Write("AJAXCbo.ClearIntervalForAjaxCall();")



                    'MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(Integer.MaxValue)
                    Response.AppendHeader("Refresh", -1)


                Else
                    lblSearching.Text = "Please Wait Until Data Loading Finish..."
                    LoadDataSearching()
                    ClearWatchlist()

                    Response.AppendHeader("Refresh", 5)


                    'MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(5000)
                End If
            End If



            Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtDOB.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUp.Style.Add("display", "")

            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
            End Using

        Catch ex1 As MagicAjax.MagicAjaxException
            'Response.Redirect("PotentialCustomerVerification.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Function IsDataValid() As Boolean
        If TxtName.Text.Trim = "" Then
            Throw New Exception("Name /Alias is required.")
        End If

        Return True
    End Function


    Sub LoadDataSearching()
        If Not FK_AML_Screening_Customer_Request_Id Is Nothing Then
            Using objresult As EkaDataNettier.Entities.AML_Screening_Customer_Request_Detail = EKABLL.ScreeningBLL.GetScreeningResultByPk(FK_AML_Screening_Customer_Request_Id)
                If Not objresult Is Nothing Then
                    LblName.Text = objresult.Nama
                    If objresult.DOB.HasValue Then
                        LblDOB.Text = objresult.DOB.GetValueOrDefault.ToString("dd-MMM-yyyy")
                    Else
                        LblDOB.Text = ""
                    End If

                    LblNationality.Text = objresult.Nationality
                End If

            End Using
        End If

    End Sub
    Sub BindGrid()

        GrdResult.DataSource = EKABLL.ScreeningBLL.GetResult(FK_AML_Screening_Customer_Request_Id)
        GrdResult.DataBind()



    End Sub
    Sub ClearWatchlist()
        LblBirthPlaceWatchList.Text = ""
        LblDOBWatchList.Text = ""
        LblListTypeWatchList.Text = ""
        LblNameWatchList.Text = ""
        LblNationalityWatchList.Text = ""
        LblYOBWatchlist.Text = ""
        LblCustomRemarks1.Text = ""
        LblCustomRemarks2.Text = ""
        LblCustomRemarks3.Text = ""
        LblCustomRemarks4.Text = ""
        LblCustomRemarks5.Text = ""
        LblCategoryType.Text = ""
        linkcategory.Text = ""
    End Sub
    Sub LoadWatchlist(intverificationlist As Long)
        Dim objdt As Data.DataTable = EKABLL.ScreeningBLL.GetWatchListData(intverificationlist)
        If Not objdt Is Nothing Then
            If objdt.Rows.Count > 0 Then


                LblBirthPlaceWatchList.Text = objdt.Rows(0).Item("BirthPlace").ToString

                Dim strdob As String = ""
                Try
                    strdob = Convert.ToDateTime(objdt.Rows(0).Item("DateOfBirth")).ToString("dd-MMM-yyyy")
                Catch ex As Exception
                    strdob = objdt.Rows(0).Item("DateOfBirth").ToString
                End Try

                LblDOBWatchList.Text = strdob

                LblListTypeWatchList.Text = objdt.Rows(0).Item("ListTypeName").ToString
                LblCategoryType.Text = objdt.Rows(0).Item("CategoryName").ToString
                linkcategory.Text = objdt.Rows(0).Item("CategoryName").ToString
                LblNameWatchList.Text = objdt.Rows(0).Item("DisplayName").ToString
                LblNationalityWatchList.Text = objdt.Rows(0).Item("Nationality").ToString
                LblYOBWatchlist.Text = objdt.Rows(0).Item("YOB").ToString
                LblCustomRemarks1.Text = objdt.Rows(0).Item("CustomRemark1").ToString
                LblCustomRemarks2.Text = objdt.Rows(0).Item("CustomRemark2").ToString
                LblCustomRemarks3.Text = objdt.Rows(0).Item("CustomRemark3").ToString
                LblCustomRemarks4.Text = objdt.Rows(0).Item("CustomRemark4").ToString
                LblCustomRemarks5.Text = objdt.Rows(0).Item("CustomRemark5").ToString
                Dim intVerificationCategoryid As Integer = objdt.Rows(0).Item("VerificationListCategoryId").ToString

                Dim intcategoryworldcheck As String = EKABLL.ScreeningBLL.GetSystemParameterByValueByID(4000)
                Dim strreffid As String = objdt.Rows(0).Item("RefId").ToString
                If intVerificationCategoryid = intcategoryworldcheck Then
                    linkcategory.Visible = True
                    LblCategoryType.Visible = False
                Else
                    linkcategory.Visible = False
                    LblCategoryType.Visible = True
                End If

                linkcategory.OnClientClick = "window.open('showwatchlist.aspx?uid=" & strreffid & "', 'ca', 'width=500,height=250,left=500,top=500,resizable=yes,scrollbars=yes')"
            Else
                    LblBirthPlaceWatchList.Text = ""
                LblDOBWatchList.Text = ""
                LblListTypeWatchList.Text = ""
                LblNameWatchList.Text = ""
                LblNationalityWatchList.Text = ""
                LblYOBWatchlist.Text = ""
                LblCustomRemarks1.Text = ""
                LblCustomRemarks2.Text = ""
                LblCustomRemarks3.Text = ""
                LblCustomRemarks4.Text = ""
                LblCustomRemarks5.Text = ""
                LblCategoryType.Text = ""
                linkcategory.Text = ""

            End If
        End If

    End Sub

    Private Sub ImageSearch_Click(sender As Object, e As ImageClickEventArgs) Handles ImageSearch.Click
        Try
            If IsDataValid() Then

                Dim dob As Nullable(Of Date) = Nothing
                If TxtDOB.Text.Trim.Length > 0 Then
                    dob = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TxtDOB.Text.Trim)
                End If
                FK_AML_Screening_Customer_Request_Id = Nothing
                FK_AML_Screening_Customer_Request_Id = EKABLL.ScreeningBLL.PotentialScreeningSatuan(TxtName.Text.Trim, dob, TxtNationality.Text.Trim)

                PanelResult.Visible = True
                PanelCustomerScreening.Visible = False
                BindGrid()
                LoadDataSearching()
                'MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(5000)
                Response.AppendHeader("Refresh", 5)

                lblSearching.Text = "Loading..."

                ClearWatchlist()

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub ClearInput()
        TxtName.Text = ""
        TxtDOB.Text = ""
        TxtNationality.Text = ""
    End Sub
    Private Sub ImageBack_Click(sender As Object, e As ImageClickEventArgs) Handles ImageBack.Click
        Try


            FK_AML_Screening_Customer_Request_Id = Nothing
            PanelResult.Visible = False
            PanelCustomerScreening.Visible = True
            ClearInput()

            'Response.Redirect("PotentialCustomerVerification.aspx", False)

            'MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(0)
            Response.AppendHeader("Refresh", -1)


        Catch ex1 As MagicAjax.MagicAjaxException

            ' Response.Redirect("PotentialCustomerVerification.aspx", False)
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub GrdResult_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GrdResult.SelectedIndexChanged
        Try
            Dim resultid As String = GrdResult.SelectedRow.Cells(2).Text
            'MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(0)

            Response.AppendHeader("Refresh", -1)

            If Integer.TryParse(resultid, 0) Then
                LoadWatchlist(resultid)
            End If


        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GrdResult_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GrdResult.PageIndexChanging
        Try
            GrdResult.PageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
