#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
#End Region

Partial Class MsKotaKab_edit
    Inherits Parent

#Region "Function"

    Private Sub LoadData()
        Dim ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.IDKotaKab.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsKotaKab Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsKotaKab
            SafeDefaultValue = "-"
            HFProvince.value = Safe(.IDPropinsi)
            Dim OProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(.IDPropinsi.GetValueOrDefault)
            If OProvince IsNot Nothing Then LBSearchNama.text = Safe(OProvince.Nama)

            txtIDKotaKab.Text = Safe(.IDKotaKab)
            txtNamaKotaKab.Text = Safe(.NamaKotaKab)

            chkactivation.checked = SafeBoolean(.Activation)
            Dim L_objMappingMsKotaKabNCBSPPATK As TList(Of MappingMsKotaKabNCBSPPATK)
            L_objMappingMsKotaKabNCBSPPATK = DataRepository.MappingMsKotaKabNCBSPPATKProvider.GetPaged(MappingMsKotaKabNCBSPPATKColumn.idKotakab.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


            Listmaping.AddRange(L_objMappingMsKotaKabNCBSPPATK)

        End With
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub

    ''' <summary>
    ''' Validasi men-handle seluruh syarat2 data valid
    ''' data yang lolos dari validasi dipastikan lolos ke database 
    ''' karena menggunakan FillorNothing
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ValidasiControl()
        '========================== validasi popup ref :  Province canot null or empty string ==================================================
        If ObjectAntiNull(HFProvince.Value) = False Then Throw New Exception("Province Must Be Filled ")
        '======================== Validasi textbox :  ID canot Null ====================================================
        If ObjectAntiNull(txtIDKotaKab.Text) = False Then Throw New Exception("ID  must be filled  ")
        If Not IsNumeric(Me.txtIDKotaKab.Text) Then
            Throw New Exception("ID must be in number format")
        End If
        '======================== Validasi textbox :  Nama canot Null ====================================================
        If ObjectAntiNull(txtNamaKotaKab.Text) = False Then Throw New Exception("Nama  must be filled  ")

        If DataRepository.MsKotaKabProvider.GetPaged("IDKotaKab = '" & txtIDKotaKab.Text & "' AND IDKotaKab <> '" & parID & "'", "", 0, Integer.MaxValue, 0).Count > 0 Then
            Throw New Exception("ID already exist in the database")
        End If

        If DataRepository.MsKotaKab_ApprovalDetailProvider.GetPaged(MsKotaKab_ApprovalDetailColumn.IDKotaKab.ToString & "='" & txtIDKotaKab.Text & "' AND IDKotaKab <> '" & parID & "'", "", 0, 1, Nothing).Count > 0 Then
            Throw New Exception("Data exist in List Waiting Approval")
        End If
        'If LBMapping.Items.Count = 0 Then Throw New Exception("data tidak punya list Mapping")
    End Sub

#End Region

#Region "events..."
    Protected Sub imgBrowseNameProvince_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBrowseNameProvince.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                LBSearchNama.Text = strData(1)
                HFProvince.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgBrowse_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBrowse.Click
        Try
            If Not Get_DataFromPicker Is Nothing Then
                Listmaping.AddRange(Get_DataFromPicker)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgDeleteItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDeleteItem.Click
        Try
            If LBMapping.SelectedIndex < 0 Then
                Throw New Exception("No data Selected")
            End If
            Listmaping.RemoveAt(LBMapping.SelectedIndex)

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(Sahassa.CommonCTRWeb.Commonly.SessionCurrentPage)
                Listmaping = New TList(Of MappingMsKotaKabNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then
                ValidasiControl()
                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsKotaKab_Approval As New MsKotaKab_Approval
                    With ObjMsKotaKab_Approval
                        FillOrNothing(.FK_MsMode_Id, 2, True, oInt)
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsKotaKab_ApprovalProvider.Save(ObjMsKotaKab_Approval)
                    KeyHeaderApproval = ObjMsKotaKab_Approval.PK_MsKotaKab_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsKotaKab_ApprovalDetail As New MsKotaKab_ApprovalDetail()
                    With objMsKotaKab_ApprovalDetail
                        Dim ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(parID)
                        FillOrNothing(.IDKotaKab, ObjMsKotaKab.IDKotaKab)
                        FillOrNothing(.NamaKotaKab, ObjMsKotaKab.NamaKotaKab)
                        FillOrNothing(.Activation, ObjMsKotaKab.Activation)
                        FillOrNothing(.CreatedDate, ObjMsKotaKab.CreatedDate)
                        FillOrNothing(.CreatedBy, ObjMsKotaKab.CreatedBy)
                        FillOrNothing(.FK_MsKotaKab_Approval_Id, KeyHeaderApproval)

                        '==== Edit data =============
                        FillOrNothing(.IDPropinsi, HFProvince.Value, True, oInt)
                        FillOrNothing(.IDKotaKab, txtIDKotaKab.Text, True, oInt)
                        FillOrNothing(.NamaKotaKab, txtNamaKotaKab.Text, True, Ovarchar)

                        FillOrNothing(.Activation, chkActivation.checked, True, oBit)
                        FillOrNothing(.LastUpdatedBy, SessionUserId)
                        FillOrNothing(.LastUpdatedDate, Date.Now)

                    End With
                    DataRepository.MsKotaKab_ApprovalDetailProvider.Save(objMsKotaKab_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsKotaKab_ApprovalDetail.PK_MsKotaKab_ApprovalDetail_Id
                    '========= Insert mapping item 
                    Dim LobjMappingMsKotaKabNCBSPPATK_Approval_Detail As New TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail)
                    For Each objMappingMsKotaKabNCBSPPATK As MappingMsKotaKabNCBSPPATK In Listmaping
                        Dim Mapping As New MappingMsKotaKabNCBSPPATK_Approval_Detail
                        With Mapping
                            FillOrNothing(.IDKotaKab, objMsKotaKab_ApprovalDetail.IDKotaKab)
                            FillOrNothing(.IDKotaKabNCBS, objMappingMsKotaKabNCBSPPATK.idKotakabNCBS)
                            FillOrNothing(.PK_MsKotaKab_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.Nama, objMappingMsKotaKabNCBSPPATK.Nama)
                            FillOrNothing(.PK_MappingMsKotaKabNCBSPPATK_approval_detail_Id, keyHeaderApprovalDetail)
                            LobjMappingMsKotaKabNCBSPPATK_Approval_Detail.Add(Mapping)
                        End With
                    Next
                    DataRepository.MappingMsKotaKabNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsKotaKabNCBSPPATK_Approval_Detail)
                    'session Maping item Clear
                    Listmaping.Clear()
                    ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
                    LblConfirmation.Text = "Data has been added and waiting for approval"
                    MtvMsUser.ActiveViewIndex = 1
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub



    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Response.Redirect("MsKotaKab_View.aspx")
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Response.Redirect("MsKotaKab_View.aspx")
    End Sub
#End Region

#Region "Property..."
    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Mengambil Data dari Picker
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Get_DataFromPicker() As TList(Of MappingMsKotaKabNCBSPPATK)
        Get
            Dim Temp As New TList(Of MappingMsKotaKabNCBSPPATK)
            Dim L_MsKotaKab_NCBS As TList(Of MsKotaKab_NCBS)
            Try
                If Session("PickerMsKotaKab_NCBS.Data") Is Nothing Then
                    Throw New Exception
                End If
                L_MsKotaKab_NCBS = Session("PickerMsKotaKab_NCBS.Data")
                For Each i As MsKotaKab_NCBS In L_MsKotaKab_NCBS
                    Dim Tempmapping As New MappingMsKotaKabNCBSPPATK
                    With Tempmapping
                        .idKotakabNCBS = i.IDKotaKab_NCBS
                        .Nama = i.NamaKotaKab_NCBS
                    End With
                    Temp.Add(Tempmapping)
                Next
            Catch ex As Exception
                L_MsKotaKab_NCBS = Nothing
            End Try
            Return Temp
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsKotaKabNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKotaKabNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKotaKabNCBSPPATK In Listmaping.FindAllDistinct("idKotakabNCBS")
                Temp.Add(i.idKotakabNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property
#End Region

End Class


