#Region "Imports..."
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Excel
Imports Sahassa.AML.Commonly
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports Sahassanettier.Data.SqlClient
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports AMLBLL.DataType
#End Region

Partial Class MsBidangUsaha_upload
	Inherits Parent

#Region "Structure..."

	Structure GrouP_MsBidangUsaha_success
		Dim MsBidangUsaha As MsBidangUsaha_ApprovalDetail
        Dim L_MsBidangUsahaNCBS_ApprovalDetail As TList(Of MsBidangUsahaNCBS_ApprovalDetail)
 
	End Structure

#End Region

#Region "Property umum"
	Private Property GroupTableSuccessData() As List(Of GrouP_MsBidangUsaha_success)
		Get
			Return Session("GroupTableSuccessData")
		End Get
		Set(ByVal value As List(Of GrouP_MsBidangUsaha_success))
			Session("GroupTableSuccessData") = value
		End Set
	End Property

    Private Property GroupTableAnomalyData() As List(Of AMLBLL.MsBidangUsahaBll.DT_Group_MsBidangUsaha)
        Get
            Return Session("GroupTableAnomalyData")
        End Get
        Set(ByVal value As List(Of AMLBLL.MsBidangUsahaBll.DT_Group_MsBidangUsaha))
            Session("GroupTableAnomalyData") = value
        End Set
    End Property

	Public ReadOnly Property GET_PKUserID() As Integer
		Get
			Return SessionPkUserId
		End Get
	End Property

	Public ReadOnly Property GET_UserName() As String
		Get
            Return SessionUserId
            'Return SessionNIK
		End Get
	End Property

	Private Property SetnGetDataTableForSuccessData() As TList(Of MsBidangUsaha_ApprovalDetail)
		Get
			Return CType(IIf(Session("MsBidangUsaha_upload.TableSuccessUpload") Is Nothing, Nothing, Session("MsBidangUsaha_upload.TableSuccessUpload")), TList(Of MsBidangUsaha_ApprovalDetail))
		End Get
		Set(ByVal value As TList(Of MsBidangUsaha_ApprovalDetail))
			Session("MsBidangUsaha_upload.TableSuccessUpload") = value
		End Set
	End Property

    Private Property SetnGetDataTableForAnomalyData() As Data.DataTable
        Get
            Return CType(IIf(Session("MsBidangUsaha_upload.TableAnomalyUpload") Is Nothing, Nothing, Session("MsBidangUsaha_upload.TableAnomalyUpload")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsBidangUsaha_upload.TableAnomalyUpload") = value
        End Set
    End Property

	Private Property DSexcelTemp() As Data.DataSet
		Get
			Return CType(IIf(Session("MsBidangUsaha_upload.DSexcelTemp") Is Nothing, Nothing, Session("MsBidangUsaha_upload.DSexcelTemp")), DataSet)
		End Get
		Set(ByVal value As Data.DataSet)
			Session("MsBidangUsaha_upload.DSexcelTemp") = value
		End Set
	End Property

#End Region

#Region "Property Tabel Temporary"
    Private Property DT_AllMsBidangUsaha() As Data.DataTable
        Get
            Return CType(IIf(Session("MsBidangUsaha_upload.DT_MsBidangUsaha") Is Nothing, Nothing, Session("MsBidangUsaha_upload.DT_MsBidangUsaha")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsBidangUsaha_upload.DT_MsBidangUsaha") = value
        End Set
    End Property

    Private Property DT_AllMsBidangUsahaNCBS_ApprovalDetail() As Data.DataTable
        Get
            Return CType(IIf(Session("MsBidangUsaha_upload.DT_MsBidangUsahaNCBS_ApprovalDetail") Is Nothing, Nothing, Session("MsBidangUsaha_upload.DT_MsBidangUsahaNCBS_ApprovalDetail")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsBidangUsaha_upload.DT_MsBidangUsahaNCBS_ApprovalDetail") = value
        End Set
    End Property

#End Region

#Region "Property Table Sukses Data"

	Private Property TableSuccessMsBidangUsaha As TList(Of MsBidangUsaha_ApprovalDetail)
		Get
			Return Session("TableSuccessMsBidangUsaha")
		End Get
		Set(ByVal value As TList(Of MsBidangUsaha_ApprovalDetail))
			Session("TableSuccessMsBidangUsaha") = value
		End Set
	End Property

	Private Property TableSuccessMsBidangUsahaNCBS_ApprovalDetail As TList(Of MsBidangUsahaNCBS_ApprovalDetail)
		Get
			Return Session("TableSuccessMsBidangUsahaNCBS_ApprovalDetail")
		End Get
		Set(ByVal value As TList(Of MsBidangUsahaNCBS_ApprovalDetail))
			Session("TableSuccessMsBidangUsahaNCBS_ApprovalDetail") = value
		End Set
	End Property


#End Region

#Region "Property Table Anomaly Data"

    Private Property TableAnomalyMsBidangUsaha As Data.DataTable
        Get
            Return Session("TableAnomalyMsBidangUsaha")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsBidangUsaha") = value
        End Set
    End Property

    Private Property TableAnomalyMsBidangUsahaNCBS_ApprovalDetail As Data.DataTable
        Get
            Return Session("TableAnomalyMsBidangUsahaNCBS_ApprovalDetail")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsBidangUsahaNCBS_ApprovalDetail") = value
        End Set
    End Property

#End Region

#Region "validasi.."

    Private Function ValidateInput(ByVal MsBidangUsaha As AMLBLL.MsBidangUsahaBll.DT_Group_MsBidangUsaha) As List(Of String)
        Dim temp As New List(Of String)
        Dim msgError As New StringBuilder
        Dim errorline As New StringBuilder
        Try
            'validasi
            ValidateMsBidangUsaha(MsBidangUsaha.MsBidangUsaha, msgError, errorline)
            If msgError.ToString = "" Then
                For Each ci As DataRow In MsBidangUsaha.L_MsBidangUsahaNCBS.Rows
                    'validasi NCBS
                    ValidateMsBidangUsahaNCBS_ApprovalDetail(ci, msgError, errorline)
                Next
            End If
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        Catch ex As Exception
            msgError.Append(ex.Message)
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        End Try
        Return temp
    End Function

	Private Shared Sub ValidateMsBidangUsahaNCBS_ApprovalDetail(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorLine As StringBuilder)
		Try
			 
				'======================== Validasi textbox :  IDBidangUsahaNCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("IDBidangUsahaBank")) = False Then
                msgError.AppendLine(" &bull; IDBidangUsahaBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsBidangUsahaBank : Line " & DTR("NO") & "<BR/>")
            End If

			'======================== Validasi textbox :  NamaBidangUsahaNCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("NamaBidangUsahaBank")) = False Then
                msgError.AppendLine(" &bull; NamaBidangUsahaBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsBidangUsahaBank : Line " & DTR("NO") & "<BR/>")
            End If

			Catch ex As Exception
			msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorLine.AppendLine(" &bull;MsBidangUsahaBank : Line " & DTR("NO") & "<BR/>")
		End Try
	End Sub

	Private Shared Sub ValidateMsBidangUsaha(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorline As StringBuilder)
		Try

			Using checkBlocking As TList(Of MsBidangUsaha_ApprovalDetail) = DataRepository.MsBidangUsaha_ApprovalDetailProvider.GetPaged("IdBidangUsaha=" & DTR("IdBidangUsaha"), "", 0, 1, Nothing)
				If checkBlocking.Count > 0 Then
					msgError.AppendLine(" &bull;MsBidangUsaha is waiting for approval <BR/>")
					errorline.AppendLine(" &bull;MsBidangUsaha : Line " & DTR("NO") & "<BR/>")
				End If
			End Using

		     '======================== Validasi textbox :  IdBidangUsaha   canot Null (#)====================================================
            If ObjectAntiNull(DTR("IdBidangUsaha")) = False Then
                msgError.AppendLine(" &bull; ID must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsBidangUsaha : Line " & DTR("NO") & "<BR/>")
            End If
  If ObjectAntiNull(DTR("IdBidangUsaha"))  Then
	  If objectNumOnly(DTR("IdBidangUsaha")) = False Then
	   msgError.AppendLine(" &bull; ID must be Numeric <BR/>")
	   errorLine.AppendLine(" &bull; MsBidangUsaha : Line " & DTR("NO") & "<BR/>")
	  end if
   End If
  '======================== Validasi textbox :  NamaBidangUsaha   canot Null (#)====================================================
            If ObjectAntiNull(DTR("NamaBidangUsaha")) = False Then
                msgError.AppendLine(" &bull; Nama must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsBidangUsaha : Line " & DTR("NO") & "<BR/>")
            End If

		Catch ex As Exception
			msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
			errorline.AppendLine(" &bull;MsBidangUsaha  : Line " & DTR("NO") & "<BR/>")
			LogError(ex)
		End Try
	End Sub

#End Region

#Region "Upload File"

    Public Function SaveFileImportToSave() As String
        Dim filePath As String
        Dim FileNameToGetData As String = ""
        Try
            If FileUploadKPI.PostedFile.ContentLength = 0 Then
                Return FileNameToGetData
            End If

            If Not FileUploadKPI.HasFile Then
                Return FileNameToGetData
            End If

            filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName)

            FileNameToGetData = FileUploadKPI.FileName
            If System.IO.File.Exists(filePath) Then
                Dim bexist As Boolean = True
                Dim i As Integer = 1
                While bexist
                    filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & FileUploadKPI.FileName.Split(CChar("."))(1))
                    FileNameToGetData = FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & Me.FileUploadKPI.FileName.Split(CChar("."))(1)
                    If Not System.IO.File.Exists(filePath) Then
                        bexist = False
                        Exit While
                    End If
                    i = i + 1
                End While
            End If
            Me.FileUploadKPI.PostedFile.SaveAs(filePath)
            Session("Filepath") = filePath
            Return FileNameToGetData
        Catch
            Throw
        End Try
    End Function

    ReadOnly Property FilePath As String
        Get
            Return Session("FilePath")
        End Get
    End Property

    Private Sub ProcessExcelFile(ByVal FullPathFileNameToExportExcel As String)

        Using stream As FileStream = File.Open(FullPathFileNameToExportExcel, FileMode.Open, FileAccess.Read)
            Using excelReader As IExcelDataReader = ExcelReaderFactory.CreateBinaryReader(stream)
                excelReader.IsFirstRowAsColumnNames = True
                DSexcelTemp = excelReader.AsDataSet()
                If DSexcelTemp.Tables("MsBidangUsaha").Columns(0).ColumnName Like "*Column*" Then
                    DSexcelTemp.Tables("MsBidangUsaha").Rows.Clear()
                End If
				
            End Using
        End Using

    End Sub


#End Region

#Region "Function..."

    Function getErrorNo(Id As String, nama As String) As String
        Dim temp As String = ""
        Try
            Dim DV As New DataView(DT_AllMsBidangUsahaNCBS_ApprovalDetail)
            'jika Null maka harus is null bukan kosong disini belum di handle
            DV.RowFilter = "IdBidangUsaha = '" & Id & "' and NamaBidangUsaha = '" & nama & "'"

            Dim DT As Data.DataTable = DV.ToTable
            If DT.Rows.Count > 0 Then
                temp = Safe(DT.Rows(0)("NO"))
            End If
        Catch ex As Exception
            temp = ex.Message
        End Try

        Return temp
    End Function
    ''' <summary>
    ''' Merubah excel ke Object Databale sekaligus memecahnya
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ImportExcelTempToDTproperty()
        Dim DT As New Data.DataTable
        'DT_MsBidangUsaha = DSexcelTemp.Tables("MsBidangUsaha")
        DT = DSexcelTemp.Tables("MsBidangUsaha")
        Dim DV As New DataView(DT)
        DT_AllMsBidangUsaha = DV.ToTable(True, "IdBidangUsaha", "NamaBidangUsaha", "activation")
        DT_AllMsBidangUsahaNCBS_ApprovalDetail = DSexcelTemp.Tables("MsBidangUsaha")
        DT_AllMsBidangUsaha.Columns.Add("NO")
        For Each i As DataRow In DT_AllMsBidangUsaha.Rows
            Dim PK As String = Safe(i("IdBidangUsaha"))
            Dim nama As String = Safe(i("NamaBidangUsaha"))
            i("NO") = getErrorNo(PK, nama)
        Next
    End Sub

    Private Sub ClearThisPageSessions()
        DT_AllMsBidangUsaha = Nothing
        DT_AllMsBidangUsahaNCBS_ApprovalDetail = Nothing
        GroupTableSuccessData = Nothing
        GroupTableAnomalyData = Nothing
        DSexcelTemp = Nothing
        SetnGetDataTableForAnomalyData = Nothing
        SetnGetDataTableForSuccessData = Nothing
    End Sub


    Function ConvertDTtoMsBidangUsahaNCBS_ApprovalDetail(ByVal DT As Data.DataTable) As TList(Of MsBidangUsahaNCBS_ApprovalDetail)
        Dim temp As New TList(Of MsBidangUsahaNCBS_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsBidangUsahaNCBS_ApprovalDetail As New MsBidangUsahaNCBS_ApprovalDetail
                With MsBidangUsahaNCBS_ApprovalDetail
                    FillOrNothing(.IDBidangUsahaNCBS, DTR("IDBidangUsahaBank"), True, oInt)
                    FillOrNothing(.NamaBidangUsahaNCBS, DTR("NamaBidangUsahaBank"), True, oString)
                    FillOrNothing(.Activation, 1, True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsBidangUsahaNCBS_ApprovalDetail)
                End With
            End Using

        Next

        Return temp
    End Function



	Function ConvertDTtoMsBidangUsaha(ByVal DTR As DataRow) As MsBidangUsaha_ApprovalDetail
		Dim temp As New MsBidangUsaha_ApprovalDetail
		Using MsBidangUsaha As New MsBidangUsaha_ApprovalDetail()
			With MsBidangUsaha
			    FillOrNothing(.IdBidangUsaha, DTR("IdBidangUsaha"),true,Oint)
 FillOrNothing(.NamaBidangUsaha, DTR("NamaBidangUsaha"),true,Ovarchar)

                FillOrNothing(.Activation, DTR("Activation"), True, oBit)
				FillOrNothing(.CreatedBy, GET_UserName, True, oString)
				FillOrNothing(.CreatedDate, Now, True, oDate)
			End With
			temp = MsBidangUsaha
		End Using

		Return temp
	End Function

    Function ConvertDTtoMsBidangUsaha(ByVal DT As Data.DataTable) As TList(Of MsBidangUsaha_ApprovalDetail)
        Dim temp As New TList(Of MsBidangUsaha_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsBidangUsaha As New MsBidangUsaha_ApprovalDetail()
                With MsBidangUsaha
                    FillOrNothing(.IdBidangUsaha, DTR("IdBidangUsaha"), True, oInt)
                    FillOrNothing(.NamaBidangUsaha, DTR("NamaBidangUsaha"), True, Ovarchar)

                    FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsBidangUsaha)
                End With
            End Using
        Next
        Return temp
    End Function

    Private Sub ChangeMultiView(ByVal index As Integer)
        MultiViewKPIOvertimeUpload.ActiveViewIndex = index
        If index = 0 Then
            Imgbtnsave.Visible = False
            Imgbtncancel.Visible = False
        ElseIf index = 1 Then
            Imgbtncancel.Visible = True
        ElseIf index = 2 Then
            Imgbtncancel.Visible = False
            Imgbtnsave.Visible = False
        End If
    End Sub

    Private Function GenerateGroupingMsBidangUsaha() As List(Of AMLBLL.MsBidangUsahaBll.DT_Group_MsBidangUsaha)
        Dim Ltemp As New List(Of AMLBLL.MsBidangUsahaBll.DT_Group_MsBidangUsaha)
        For i As Integer = 0 To DT_AllMsBidangUsaha.Rows.Count - 1
            Dim MsBidangUsaha As New AMLBLL.MsBidangUsahaBll.DT_Group_MsBidangUsaha
            With MsBidangUsaha
                .MsBidangUsaha = DT_AllMsBidangUsaha.Rows(i)
                .L_MsBidangUsahaNCBS = DT_AllMsBidangUsahaNCBS_ApprovalDetail.Clone

                'Insert MsBidangUsahaNCBS_ApprovalDetail
                Dim MsBidangUsaha_ID As String = Safe(.MsBidangUsaha("IdBidangUsaha"))
                Dim MsBidangUsaha_nama As String = Safe(.MsBidangUsaha("NamaBidangUsaha"))
                Dim MsBidangUsaha_Activation As String = Safe(.MsBidangUsaha("Activation"))
                If DT_AllMsBidangUsahaNCBS_ApprovalDetail.Rows.Count > 0 Then
                    Using DV As New DataView(DT_AllMsBidangUsahaNCBS_ApprovalDetail)
                        DV.RowFilter = "IdBidangUsaha='" & MsBidangUsaha_ID & "' and " & _
                                                  "NamaBidangUsaha='" & MsBidangUsaha_nama & "' and " & _
                                                  "activation='" & MsBidangUsaha_Activation & "'"
                        If DV.Count > 0 Then
                            Dim temp As Data.DataTable = DV.ToTable
                            For Each H As DataRow In temp.Rows
                                .L_MsBidangUsahaNCBS.ImportRow(H)
                            Next
                        End If
                    End Using
                End If




            End With
            Ltemp.Add(MsBidangUsaha)
        Next
        Return Ltemp
    End Function

    Public Sub BindGrid()
        lblValueFailedData.Text = SetnGetDataTableForAnomalyData.Rows.Count
        GridAnomalyList.DataSource = SetnGetDataTableForAnomalyData
        GridAnomalyList.DataBind()
        lblValueSuccessData.Text = SetnGetDataTableForSuccessData.Count
        GridSuccessList.DataSource = SetnGetDataTableForSuccessData
        lblValueuploadeddata.Text = DT_AllMsBidangUsaha.Rows.Count
        GridSuccessList.DataBind()
    End Sub

#End Region

#Region "events..."


    Protected Sub ImgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtncancel.Click
        ClearThisPageSessions()
        ChangeMultiView(0)
        LinkButtonExportExcel.Visible = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If IsPostBack Then
                Return
            End If

            ClearThisPageSessions()
            SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            ChangeMultiView(0)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        ChangeMultiView(0)
    End Sub

    Protected Sub GridAnomalyList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridAnomalyList.PageIndexChanged
        GridAnomalyList.CurrentPageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GridSuccessList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridSuccessList.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                'Show mapping item
                Dim StringMaping As New StringBuilder
                Dim DV As New DataView(DT_AllMsBidangUsahaNCBS_ApprovalDetail)
                Dim DT As New Data.DataTable
                DV.RowFilter = "IdBidangUsaha='" & e.Item.Cells(0).Text & "' and " & "NamaBidangUsaha='" & e.Item.Cells(1).Text & "'"
                DT = DV.ToTable
                If DT.Rows.Count > 0 Then
                    Dim countField As Integer = 7
                    Dim lengthField As Integer = 0
                    For Each idx As DataRow In DT.Rows
                        With idx
                            If lengthField > countField Then
                                StringMaping.AppendLine(idx("NamaBidangUsahaBank") & "(" & idx("IDBidangUsahaBank") & ");")
                                lengthField = 0
                            Else
                                StringMaping.AppendLine(idx("NamaBidangUsahaBank") & "(" & idx("IDBidangUsahaBank") & ");")
                                lengthField += 1
                            End If

                        End With
                    Next
                Else
                    StringMaping.Append("-")
                End If
                e.Item.Cells(CollCount).Text = StringMaping.ToString

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridSuccessList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridSuccessList.PageIndexChanged
        Dim GridTarget As DataGrid = CType(source, DataGrid)
        Try
            GridSuccessList.CurrentPageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgSaveSuccessData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtnsave.Click
        Try
            'File belum diupload
            If DT_AllMsBidangUsaha Is Nothing Then
                Throw New Exception("Load data failed")
            End If

            'Save MsBidangUsaha Approval (Header)
            Dim KeyApp As String
            Using Aproval As New MsBidangUsaha_Approval()
                With Aproval
                    .RequestedBy = GET_PKUserID
                    .RequestedDate = Date.Now
                    .FK_MsMode_Id = 2 'Edit
                    .IsUpload = True
                End With
                DataRepository.MsBidangUsaha_ApprovalProvider.Save(Aproval)
                'ambil key approval
                KeyApp = Aproval.PK_MsBidangUsaha_Approval_Id
            End Using

            For k As Integer = 0 To GroupTableSuccessData.Count - 1

                Dim MsBidangUsaha As GrouP_MsBidangUsaha_success = GroupTableSuccessData(k)


                With MsBidangUsaha
                    'save MsBidangUsaha approval detil
                    .MsBidangUsaha.FK_MsBidangUsaha_Approval_Id = KeyApp
                    DataRepository.MsBidangUsaha_ApprovalDetailProvider.Save(.MsBidangUsaha)
                    'ambil key MsBidangUsaha approval detil
                    Dim IdMsBidangUsaha As String = .MsBidangUsaha.IdBidangUsaha
                    Dim L_MappingBidangUsahaNCBS_Approval_Detail_Approval_Detail As New TList(Of MappingBidangUsahaNCBS_Approval_Detail)
                    'save mapping dan NCBS
                    For Each OMsBidangUsahaNCBS_ApprovalDetail_ApprovalDetail As MsBidangUsahaNCBS_ApprovalDetail In .L_MsBidangUsahaNCBS_ApprovalDetail
                        OMsBidangUsahaNCBS_ApprovalDetail_ApprovalDetail.PK_MsBidangUsaha_Approval_Id = KeyApp
                        Using OMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail As New MappingBidangUsahaNCBS_Approval_Detail
                            With OMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail
                                FillOrNothing(.IDBidangUsahaNCBS, OMsBidangUsahaNCBS_ApprovalDetail_ApprovalDetail.IDBidangUsahaNCBS, True, oInt)
                                FillOrNothing(.BidangUsahaId, IdMsBidangUsaha, True, oInt)
                                FillOrNothing(.PK_MsBidangUsaha_Approval_id, KeyApp, True, oInt)
                                FillOrNothing(.NAMA, OMsBidangUsahaNCBS_ApprovalDetail_ApprovalDetail.NamaBidangUsahaNCBS, True, oString)
                            End With
                            L_MappingBidangUsahaNCBS_Approval_Detail_Approval_Detail.Add(OMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail)
                        End Using
                    Next
                    'save maping dan NCBS
                    DataRepository.MappingBidangUsahaNCBS_Approval_DetailProvider.Save(L_MappingBidangUsahaNCBS_Approval_Detail_Approval_Detail)
                    DataRepository.MsBidangUsahaNCBS_ApprovalDetailProvider.Save(.L_MsBidangUsahaNCBS_ApprovalDetail)

                End With
            Next

            LblConfirmation.Text = "MsBidangUsaha data has edited (" & SetnGetDataTableForSuccessData.Count & " Rows / MsBidangUsaha)"
            ChangeMultiView(2)

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgUpload.Click
        Dim CombinedFileName As String
        GroupTableSuccessData = New List(Of GrouP_MsBidangUsaha_success)
        GroupTableAnomalyData = New List(Of AMLBLL.MsBidangUsahaBll.DT_Group_MsBidangUsaha)
        Try
            'validasi File
            If FileUploadKPI.FileName = "" Then
                Throw New Exception("Data required")
            End If

            If Path.GetExtension(FileUploadKPI.FileName) <> ".xls" Then
                Throw New Exception("Invalid File Upload Extension")
            End If

            CombinedFileName = SaveFileImportToSave()
            If String.Compare(CombinedFileName, "", False) <> 0 Then
                Try
                    ProcessExcelFile(FilePath)
                Catch ex As Exception
                    Throw New Exception("Invalid File Format")
                End Try
            Else
                Throw New Exception("Invalid File Format")
            End If

            'Import from Excell to property Global
            ImportExcelTempToDTproperty()

            Dim LG_MsBidangUsaha As List(Of AMLBLL.MsBidangUsahaBll.DT_Group_MsBidangUsaha) = GenerateGroupingMsBidangUsaha()
            'tambahkan kolom untuk tampilan di grid anomaly
            DT_AllMsBidangUsaha.Columns.Add("Description")
            DT_AllMsBidangUsaha.Columns.Add("errorline")

            'Membuat tempory untuk yang anomali dan yang sukses
            Dim DTnormal As Data.DataTable = DT_AllMsBidangUsaha.Clone
            Dim DTAnomaly As Data.DataTable = DT_AllMsBidangUsaha.Clone

            'proses pemilahan data anomaly dan data sukses melalui validasi 
            For i As Integer = 0 To LG_MsBidangUsaha.Count - 1
                Dim Lerror As List(Of String) = ValidateInput(LG_MsBidangUsaha(i))
                Dim MSG As String = Lerror(0)
                Dim LINE As String = Lerror(1)
                If MSG = "" Then
                    DTnormal.ImportRow(LG_MsBidangUsaha(i).MsBidangUsaha)
                    Dim GroupMsBidangUsaha As New GrouP_MsBidangUsaha_success
                    With GroupMsBidangUsaha
                        'konversi dari datatable ke Object Nettier
                        .MsBidangUsaha = ConvertDTtoMsBidangUsaha(LG_MsBidangUsaha(i).MsBidangUsaha)
                        .L_MsBidangUsahaNCBS_ApprovalDetail = ConvertDTtoMsBidangUsahaNCBS_ApprovalDetail(LG_MsBidangUsaha(i).L_MsBidangUsahaNCBS)
                    End With
                    'masukan ke group sukses
                    GroupTableSuccessData.Add(GroupMsBidangUsaha)
                Else
                    DT_AllMsBidangUsaha.Rows(i)("Description") = MSG
                    DT_AllMsBidangUsaha.Rows(i)("errorline") = LINE
                    DTAnomaly.ImportRow(DT_AllMsBidangUsaha.Rows(i))
                    GroupTableAnomalyData.Add(LG_MsBidangUsaha(i))
                End If

            Next

            SetnGetDataTableForSuccessData = ConvertDTtoMsBidangUsaha(DTnormal)
            SetnGetDataTableForAnomalyData = DTAnomaly

            If DTAnomaly.Rows.Count > 0 Then
                Imgbtnsave.Visible = False
                Imgbtncancel.Visible = True
            Else
                Imgbtnsave.Visible = True
                Imgbtncancel.Visible = True
            End If

            BindGrid()
            If GridAnomalyList.Items.Count < 1 Then
                LinkButtonExportExcel.Visible = False
            Else
                LinkButtonExportExcel.Visible = True
            End If
            ChangeMultiView(1)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Dim Exp As New AMLBLL.MsBidangUsahaBll
            Dim FolderPath As String = Server.MapPath("FolderTemplate")

            Dim buffer As Byte() = Exp.ExportDTtoExcel(GroupTableAnomalyData, FolderPath)
            Response.AddHeader("Content-type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment; filename=MsBidangUsahaUpload.xls")
            Response.BinaryWrite(buffer)
            Response.Flush()
            Response.End()

        Catch ex As Exception
		    cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered 

    End Sub

    Protected Sub GridSuccessList_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles GridSuccessList.SelectedIndexChanged

    End Sub
End Class






