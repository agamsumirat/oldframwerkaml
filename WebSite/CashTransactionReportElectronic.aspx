<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CashTransactionReportElectronic.aspx.vb" Inherits="CashTransactionReportElectronic" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table id="title"  border="2" cellpadding="2" cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Electronic Cash Transaction Report</strong>
            </td>
        </tr>
        <tr>
            <td>
	            <ajax:ajaxpanel id="AjaxPanel3" runat="server" Width="99%">
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
                </ajax:ajaxpanel>
                <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="99%">
                    <asp:Label ID="UpdatePPATKConfirmationNumberStatusLabel" runat="server" ForeColor="Blue"
                        Visible="False"></asp:Label></ajax:AjaxPanel>                                
            </td>
        </tr>
    </table>
    <TABLE cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd" border="0">
        <TR>
            <TD align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></TD>
				<TD vAlign="top" width="98%" bgColor="#ffffff">
				    <TABLE cellSpacing="4" cellPadding="0" width="100%" border="0">
						<TR>
							<TD class="Regtext" noWrap>
                                Select Date :</TD>
							<TD vAlign="middle" noWrap>
							    <ajax:ajaxpanel id="AjaxPanel1" runat="server" Width="169px">                                
                                <asp:textbox id="TextSearch" tabIndex="4" runat="server" CssClass="searcheditbox"></asp:textbox><input id="popUp" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                    border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                    border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif); width: 16px;" title="Click to show calendar"
                                    type="button" />
                                </ajax:ajaxpanel>
                            </TD>
							<TD vAlign="middle" style="width: 99%" >
							    <ajax:ajaxpanel id="AjaxPanel2" runat="server">
                                    <asp:Button ID="GenerateFileButton" runat="server" Text="Generate Electronic CTR" />&nbsp;</ajax:ajaxpanel>&nbsp;
                            </TD>
 
						</TR>
                        <tr>
                            <td class="Regtext" nowrap="nowrap">
                            </td>
                            <td nowrap="nowrap" valign="middle">
                            </td>
                            <td style="width: 485px" valign="middle">
                                <asp:Button
                                        ID="UpdateButton" runat="server" Text="Update PPATK Confirmation Number" /></td>

                        </tr>
                        <tr>
                            <td class="Regtext" colspan="3" nowrap="nowrap">
                                <%--<ajax:AjaxPanel ID="AjaxPanel4" runat="server" Width="99%">--%>
                                    <asp:Panel ID="PPATKConfirmationNumberPanel" runat="server" Height="50px" Width="100%">
                                        <table width="100%">
                                            <tr>
                                                <td width="30%">PPATK Confirmation Number File :</td>
                                                <td>
                                                    <asp:FileUpload ID="FileUpload1" runat="server" /></td>
                                                <td style="width: 99%">
                                                    <asp:Button ID="SaveButton" runat="server" Text="Update" />
                                                    <asp:Button ID="CancelUpdateButton" runat="server" Text="Cancel" /></td>
                                            </tr>
                                        </table>
                                        </asp:Panel>
                                <%--</ajax:AjaxPanel>--%>
                            </td>
                            <td class="Regtext" colspan="1" nowrap="nowrap">
                            </td>
                        </tr>
					</TABLE>
                </TD>
			</TR>
	</TABLE>
</asp:Content>

