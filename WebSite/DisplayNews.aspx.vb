Partial Class DisplayNews
    Inherits System.Web.UI.Page

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetNewsId() As String
        Get
            Return Me.Request.Params("NewsID")
        End Get
    End Property

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Try
        
            Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
                Using TableNews As AMLDAL.AMLDataSet.NewsDataTable = AccessNews.GetDataByNewsID(Me.GetNewsId)
                    If TableNews.Rows.Count > 0 Then
                        Dim TableRowNews As AMLDAL.AMLDataSet.NewsRow = TableNews.Rows(0)

                        'ViewState("NewsID_Old") = TableRowNews.NewsID
                        'Me.LabelNewsID.Text = ViewState("NewsID_Old")

                        'ViewState("NewsTitle_Old") = TableRowNews.NewsTitle


                        'ViewState("NewsSummary_Old") = TableRowNews.NewsSummary
                        'Me.TextNewsSummary.Text = ViewState("NewsSummary_Old")

                        'ViewState("NewsContent_Old") = TableRowNews.NewsContent

                        'ViewState("NewsStartDate_Old") = TableRowNews.NewsStartDate
                        'Me.TextStartDate.Text = ViewState("NewsStartDate_Old")

                        'ViewState("NewsEndDate_Old") = TableRowNews.NewsEndDate
                        'Me.TextEndDate.Text = ViewState("NewsEndDate_Old")

                        Me.LabelEntryDate.Text = TableRowNews.NewsCreatedDate
                        Me.LabelNewsTitle.Text = TableRowNews.NewsTitle
                        Me.TextNewsContent.Text = TableRowNews.NewsContent
                        GridAttach.DataSource = oRowNewsAttach
                        GridAttach.DataBind()
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillEditData()
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Private Sub LogError(ByVal ex As Exception)
        Dim Logger As log4net.ILog = log4net.LogManager.GetLogger("AMLError")
        Logger.Error("Exception has occured", ex)
    End Sub
    Protected Function GenerateLink(ByVal PK_NewsAttachment As Integer, ByVal Filename As String, ByVal Fk_NewsApprovalId As Integer) As String
        Return String.Format("<a href=""{0}"">{1}</a> ", ResolveUrl("~\NewsDownloadAttach.aspx?PK_NewsAttachment=" & PK_NewsAttachment), Filename)
    End Function
    Protected Sub GridAttach_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridAttach.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim PK As Integer = GridAttach.DataKeys(e.Row.RowIndex).Value
            Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                CType(e.Row.FindControl("Datalist1"), System.Web.UI.WebControls.DataList).DataSource = adapter.GetNewsAttachmentByPK(PK)
                CType(e.Row.FindControl("Datalist1"), System.Web.UI.WebControls.DataList).DataBind()
            End Using
        End If
    End Sub
    Private _oRowNewsAttach As AMLDAL.AMLDataSet.NewsAttachmentDataTable
    Private ReadOnly Property oRowNewsAttach() As AMLDAL.AMLDataSet.NewsAttachmentDataTable
        Get
            If Not _oRowNewsAttach Is Nothing Then
                Return _oRowNewsAttach
            Else
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                    _oRowNewsAttach = adapter.GetByFk_NewsID(GetNewsId)
                    Return _oRowNewsAttach
                End Using
            End If

        End Get
    End Property
End Class
