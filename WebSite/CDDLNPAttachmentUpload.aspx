<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CDDLNPAttachmentUpload.aspx.vb"
    Inherits="CDDLNPAttachmentUpload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="theme/aml.css" rel="stylesheet" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="combobox" Width="400px" />
        <asp:ImageButton ID="ImageButtonInsert" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/Add.gif"
            AlternateText="Add" />
        <br />
        <asp:ListBox ID="ListAttachment" runat="server" CssClass="combobox" Width="400px"></asp:ListBox>
        <asp:ImageButton ID="ImageButtonRemove" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/Remove.gif"
            AlternateText="Remove" />
    </form>
</body>
</html>
