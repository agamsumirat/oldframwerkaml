Imports System.Data
Imports System.Data.SqlClient

Partial Class CaseManagementWorkflowDetail
    Inherits Parent

#Region "Property"
    ''' <summary>
    ''' SetnGet TxtApprovers ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetTxtApproversID() As String
        Get
            Return Session("TxtApproversID")
        End Get
        Set(ByVal value As String)
            Session("TxtApproversID") = value
        End Set
    End Property
    ''' <summary>
    ''' SetnGet TxtNotifyOthersID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetTxtNotifyOthersID()
        Get
            Return Session("TxtNotifyOthersID")
        End Get
        Set(ByVal value)
            Session("TxtNotifyOthersID") = value
        End Set
    End Property
    ''' <summary>
    ''' SetnGetApprovers Values
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetApproversValues() As ArrayList
        Get
            Return Session("ArrApproversValues")
        End Get
        Set(ByVal value As ArrayList)
            Session("ArrApproversValues") = value
        End Set
    End Property
    ''' <summary>
    ''' SetnGet DueDateValues
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetDueDateValues() As ArrayList
        Get
            Return Session("ArrDueDateValues")
        End Get
        Set(ByVal value As ArrayList)
            Session("ArrDueDateValues") = value
        End Set
    End Property
    ''' <summary>
    ''' SetnGetNotifyOthers Values
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetNotifyOthersValues() As ArrayList
        Get
            Return Session("ArrNotifyOthersValues")
        End Get
        Set(ByVal value As ArrayList)
            Session("ArrNotifyOthersValues") = value
        End Set
    End Property
    ''' <summary>
    ''' Account Owner ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetAccountOwnerID() As String
        Get
            Return Me.Request.Params.Get("AccountOwnerID")
        End Get
    End Property
    Private ReadOnly Property GetSegment() As String
        Get
            Return Trim(Me.Request.Params.Get("Segment"))
        End Get
    End Property
    Private ReadOnly Property GetSBU() As String
        Get
            Return Trim(Me.Request.Params.Get("SBU"))
        End Get
    End Property
    Private ReadOnly Property GetSUBSBU() As String
        Get
            Return Trim(Me.Request.Params.Get("SUBSBU"))
        End Get
    End Property
    Private ReadOnly Property GetRM() As String
        Get
            Return Trim(Me.Request.Params.Get("RM"))
        End Get
    End Property
    Private ReadOnly Property GetVIPCode() As String
        Get
            Return Trim(Me.Request.Params.Get("VIPCode"))
        End Get
    End Property
    Private ReadOnly Property GetInsiderCode() As String
        Get
            Return Trim(Me.Request.Params.Get("InsiderCode"))
        End Get
    End Property
    Private ReadOnly Property GetCaseAlertDescription() As String
        Get
            Return Trim(Me.Request.Params.Get("CaseAlertDescription"))
        End Get
    End Property
    Private ReadOnly Property GetStatus() As String
        Get
            Return Me.Request.Params.Get("Status")
        End Get
    End Property
    ''' <summary>
    ''' Status
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetStatusAccountOwner() As Integer
        Get
            Return Me.Request.Params.Get("Status")
        End Get
    End Property
    ''' <summary>
    ''' SetnGetEmailTemplate
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetEmailTemplate() As Data.DataTable
        Get
            Return Session("EmailTemplate")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("EmailTemplate") = value
        End Set
    End Property
    ''' <summary>
    ''' SetnGetValuesGeneralCaseManagement
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRootDetail() As Data.DataTable
        Get
            Return Session("RootDetail")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("RootDetail") = value
        End Set
    End Property
    Private Property SetnGetRootGeneral() As Data.DataTable
        Get
            Return Session("RootGeneral")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("RootGeneral") = value
        End Set
    End Property
    ''' <summary>
    ''' SetnGetMode
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetMode() As String
        Get
            Return Session("Mode")
        End Get
        Set(ByVal value As String)
            Session("Mode") = value
        End Set
    End Property
    ''' <summary>
    ''' Is Updated
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SetnGetIsUpdated() As Boolean
        Get
            If Session("IsUpdated") Is Nothing Then
                Return False
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("IsUpdated") = value
        End Set
    End Property
    ''' <summary>
    ''' OldGeneralValues
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetOldGeneralValues() As Data.DataTable
        Get
            Return Session("OldGeneralValues")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("OldGeneralValues") = value
        End Set
    End Property

    Private Property SetnGetOldDetailValues() As Data.DataTable
        Get
            Return Session("OldDetailValues")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("OldDetailValues") = value
        End Set
    End Property

    Private Property SetnGetOldEmailTemplateValues() As Data.DataTable
        Get
            Return Session("OldEmailTemplateValues")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("OldEmailTemplateValues") = value
        End Set
    End Property
#End Region

#Region "SetnGet Value Approvers, DueDate(days), dan NotifyOthers serta General"
    ''' <summary>
    ''' Set Temp Values
    ''' </summary>
    ''' <param name="GridviewCategory"></param>
    ''' <param name="ArrayTempValues"></param>
    ''' <remarks></remarks>
    Private Sub SetTempValues(ByVal GridviewCategory As GridView, ByRef ArrayTempValues As ArrayList)
        Try
            If GridviewCategory.Rows.Count > 0 AndAlso ArrayTempValues IsNot Nothing Then
                For i As Integer = 0 To ArrayTempValues.Count - 1
                    Dim RowsCategory As GridViewRow = GridviewCategory.Rows(i)
                    Dim TxtCategory As TextBox = RowsCategory.Cells(0).Controls(1)
                    TxtCategory.Text = ArrayTempValues.Item(i).ToString
                Next
                ' Clear session
                ArrayTempValues.Clear()
                ArrayTempValues = Nothing
            End If
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Get Temp Values
    ''' </summary>
    ''' <param name="GridviewCategory"></param>
    ''' <param name="Category"></param>
    ''' <remarks></remarks>
    Private Sub GetTempValues(ByVal GridviewCategory As GridView, ByVal Category As String, ByVal intParalel As Integer)
        Try
            If GridviewCategory.Rows.Count > 0 Then
                Dim oArrValueCategory As New ArrayList
                Dim IntCounter As Integer = 0
                If intParalel < GridviewCategory.Rows.Count Then
                    IntCounter = intParalel - 1
                Else
                    IntCounter = GridviewCategory.Rows.Count - 1
                End If
                For i As Integer = 0 To IntCounter
                    Dim RowCategory As GridViewRow = GridviewCategory.Rows(i)
                    Dim TxtCategory As TextBox = RowCategory.Cells(0).Controls(1)
                    oArrValueCategory.Add(TxtCategory.Text)
                Next
                Select Case Category.ToLower
                    Case "approvers"
                        Me.SetnGetApproversValues = oArrValueCategory
                    Case "duedate"
                        Me.SetnGetDueDateValues = oArrValueCategory
                    Case "notifyothers"
                        Me.SetnGetNotifyOthersValues = oArrValueCategory
                End Select
            End If
        Catch
            Throw
        End Try
    End Sub


    ''' <summary>
    ''' Temp values
    ''' </summary>
    ''' <param name="RowIndexFirstGrid"></param>
    ''' <param name="Actifity"></param>
    ''' <param name="Category"></param>
    ''' <remarks></remarks>
    Private Sub TempValues(ByVal RowIndexFirstGrid As Integer, ByVal Actifity As String, ByVal Category As String, Optional ByVal intParalel As Integer = 0)
        Try
            Dim FirstRowsGrid As GridViewRow = GridViewParent.Rows(RowIndexFirstGrid) ' looping row in first grid 
            Dim GridApprovers As GridView = FirstRowsGrid.Cells(3).Controls(1)
            Dim GridDueDate As GridView = FirstRowsGrid.Cells(4).Controls(1)
            Dim GridNotifyOthers As GridView = FirstRowsGrid.Cells(5).Controls(1)
            Select Case Actifity.ToLower
                Case "set"
                    Select Case Category.ToLower
                        Case "approvers"
                            Me.SetTempValues(GridApprovers, Me.SetnGetApproversValues)
                        Case "duedate"
                            Me.SetTempValues(GridDueDate, Me.SetnGetDueDateValues)
                        Case "notifyothers"
                            Me.SetTempValues(GridNotifyOthers, Me.SetnGetNotifyOthersValues)
                    End Select
                Case "get"
                    Select Case Category.ToLower
                        Case "approvers"
                            Me.GetTempValues(GridApprovers, "Approvers", intParalel)
                        Case "duedate"
                            Me.GetTempValues(GridDueDate, "DueDate", intParalel)
                        Case "notifyothers"
                            Me.GetTempValues(GridNotifyOthers, "NotifyOthers", intParalel)
                    End Select
            End Select
        Catch
            Throw
        End Try
    End Sub

#End Region

#Region "Pick UserID"
    ''' <summary>
    ''' ButtonApprovers Init
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ButtonApprovers_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim BtnApprovers As Button = sender
            Dim GridApproversRow As GridViewRow = CType(BtnApprovers.NamingContainer, GridViewRow)
            Dim TxtApprovers As TextBox = GridApproversRow.Cells(0).Controls(1)
            BtnApprovers.Attributes.Add("onClick", "return popWin('" & TxtApprovers.ClientID & "');")
            'Me.SetnGetClientID = TxtAppropvers.ClientID
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    ''' <summary>
    ''' BtnNotifyOther Init
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub BtnNotifyOther_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim BtnNotifyOther As Button = sender
            Dim GridNotifyOthersRow As GridViewRow = CType(BtnNotifyOther.NamingContainer, GridViewRow)

            Dim TxtNotifyOthers As TextBox = GridNotifyOthersRow.Cells(0).Controls(1)
            BtnNotifyOther.Attributes.Add("onClick", "return popWin('" & TxtNotifyOthers.ClientID & "');")
            'Me.SetnGetClientID = TxtNotifyOthers.ClientID
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

#Region "Load Data and PageLoad"
    ''' <summary>
    ''' Create First grid
    ''' </summary>
    ''' <param name="JmlUpdate"></param>
    ''' <remarks></remarks>
    Private Sub CreateFirstTable(ByVal JmlUpdate As Integer)
        Try
            Dim DtTableFirst As New Data.DataTable
            DtTableFirst.Columns.Add(New Data.DataColumn("IDSerial"))
            DtTableFirst.Columns.Add(New Data.DataColumn("Workflow Step"))
            DtTableFirst.Columns.Add(New Data.DataColumn("OfParalel"))
            DtTableFirst.Columns.Add(New Data.DataColumn("Approvers"))
            DtTableFirst.Columns.Add(New Data.DataColumn("Due Date(days)"))
            DtTableFirst.Columns.Add(New Data.DataColumn("Notify Others"))
            DtTableFirst.Columns.Add(New Data.DataColumn("Change Email Template"))
            For i As Integer = 1 To JmlUpdate
                Dim Rows As Data.DataRow = DtTableFirst.NewRow
                Rows(0) = i
                DtTableFirst.Rows.Add(Rows)
            Next
            Me.GridViewParent.DataSource = DtTableFirst
            Me.GridViewParent.DataBind()
            Me.DisableOrEnableBtnEmailTemplate(False)
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageSave.Visible = False
                Me.LblAccountOwner.Text = Me.Request.Params.Get("AccountOwnerName")
                Me.LblAlertCaseDescription.Text = Me.GetCaseAlertDescription
                Me.LblVipCode.Text = Me.GetVIPCode
                Me.LblInsiderCode.Text = Me.GetInsiderCode
                Me.LblSegment.Text = Me.GetSegment
                Me.LblStatus.Text = Me.GetStatus
                Me.LblSBU.Text = Me.Request.Params.Get("SBU")
                Me.LblSubSBU.Text = Me.Request.Params.Get("SubSBU")
                Me.LblRM.Text = Me.Request.Params.Get("RM")
                Dim DtRootGeneral As New Data.DataTable
                DtRootGeneral.Columns.Add(New Data.DataColumn("WorkflowName", GetType(String)))
                DtRootGeneral.Columns.Add(New Data.DataColumn("Paralel", GetType(Integer)))
                Me.SetnGetRootGeneral = DtRootGeneral
                ' Check Status
                If Not CheckStatusIsUnconfigured() Then
                    ' berarti dia Configurated
                    Me.GenerateCaseManagementWorkflowConfigurated()
                    Me.DisableOrEnableBtnEmailTemplate(True)
                    ' set value email template
                    'Using oAccessEmailTemplate As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowEmailTemplateTableAdapter
                    '    Me.SetnGetEmailTemplate = oAccessEmailTemplate.GetData(Me.GetAccountOwnerID)
                    'End Using
                    Using objcmd As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("SelectCaseManagementWorkflowEmailTemplate2")
                        objcmd.Parameters.Add(New SqlParameter("@AccountOwnerID", Me.GetAccountOwnerID))
                        objcmd.Parameters.Add(New SqlParameter("@VIPCode", Me.GetVIPCode))
                        objcmd.Parameters.Add(New SqlParameter("@Segment", Me.GetSegment))
                        objcmd.Parameters.Add(New SqlParameter("@InsiderCode", Me.GetInsiderCode))
                        objcmd.Parameters.Add(New SqlParameter("@CaseAlertDescription", Me.GetCaseAlertDescription))

                        Using dsGeneral As Data.DataSet = SahassaNettier.Data.DataRepository.Provider.ExecuteDataSet(objcmd)
                            If dsGeneral.Tables.Count > 0 Then
                                Using dtGeneral As Data.DataTable = dsGeneral.Tables(0)
                                    Me.SetnGetEmailTemplate = dtGeneral
                                End Using
                            End If
                        End Using
                    End Using
                    Me.SetnGetMode = "Edit"
                    Me.ImageSave.Visible = True
                Else
                    Me.SetnGetMode = "Add"
                    Me.SetnGetEmailTemplate = Nothing
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    ''' <summary>
    ''' Create First Grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageButtonUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonUpdate.Click
        Try
            If Me.TxtJumlahSerial.Text <> "" Then
                If Regex.IsMatch(Me.TxtJumlahSerial.Text, "\b\d+\b") = True Then
                    If Convert.ToInt16(Me.TxtJumlahSerial.Text) <= 2 Then
                        Throw New Exception("The Number of Serial is must be greater or equal than 3.")
                    Else

                        'Checking Email Template jika Root terjadi pengurangan
                        If (Convert.ToInt16(Me.TxtJumlahSerial.Text) < Me.GridViewParent.Rows.Count) And Me.GridViewParent.Rows.Count <> 0 Then
                            If Me.SetnGetEmailTemplate IsNot Nothing Then
                                If Me.SetnGetEmailTemplate.Rows.Count <> 0 Then
                                    For i As Integer = Me.GridViewParent.Rows.Count - 1 To Convert.ToInt16(Me.TxtJumlahSerial.Text) Step -1
                                        Dim j As Integer = 0
                                        For Each RowEmail As Data.DataRow In Me.SetnGetEmailTemplate.Rows
                                            If i + 1 = RowEmail(0) Then
                                                Me.SetnGetEmailTemplate.Rows(j).Delete()
                                                Exit For
                                            End If
                                            j += 1
                                        Next
                                    Next
                                End If
                            End If
                        End If
                        ' Get value general
                        GetRootTempValuesGeneral()
                        Me.CreateFirstTable(Convert.ToInt16(Me.TxtJumlahSerial.Text))
                        SetRootTempValuesGeneral()
                        If Me.SetnGetIsUpdated Then
                            Me.DisableOrEnableBtnEmailTemplate(True)
                        End If
                        Me.SetnGetIsUpdated = True
                        Me.ImageSave.Visible = True
                    End If
                Else
                    Throw New Exception("The number of serial must be numeric.")
                End If
            Else
                Throw New Exception("The number of serial is required.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        Finally
            If SetnGetRootDetail IsNot Nothing Then
                Me.SetnGetRootDetail.Clear()
                Me.SetnGetRootDetail = Nothing
            End If
        End Try
    End Sub
    ''' <summary>
    ''' GenerateCaseManagementWorkflowConfigurated
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GenerateCaseManagementWorkflowConfigurated()
        Try
            Using objcommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("JmlCaseManagementWorkflowByAccountOwnerID")
                objcommand.Parameters.Add(New SqlParameter("@AccountOwnerID", Me.GetAccountOwnerID))
                objcommand.Parameters.Add(New SqlParameter("@VIPCode", Me.GetVIPCode))
                objcommand.Parameters.Add(New SqlParameter("@Segment", Me.GetSegment))
                objcommand.Parameters.Add(New SqlParameter("@InsiderCode", Me.GetInsiderCode))
                objcommand.Parameters.Add(New SqlParameter("@CaseAlertDescription", Me.GetCaseAlertDescription))

                Dim JmlOfSerial As Integer = SahassaNettier.Data.DataRepository.Provider.ExecuteScalar(objcommand)

                Me.TxtJumlahSerial.Text = JmlOfSerial
                'Create First Grid
                Me.CreateFirstTable(JmlOfSerial)
            End Using

            ' Get Data General Case Management Workflow
            'Using oAccessGetGeneral As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowGeneralTableAdapter
            '    Dim DtGeneral As Data.DataTable = oAccessGetGeneral.GetData(Me.GetAccountOwnerID)
            '    Me.SetnGetOldGeneralValues = DtGeneral
            '    Me.SetValueToGridSerialWorkflowAndParalel(DtGeneral)
            'End Using
            Using objcmd As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("SelectCaseManagementWorkflowGeneral2")
                objcmd.Parameters.Add(New SqlParameter("@AccountOwnerID", Me.GetAccountOwnerID))
                objcmd.Parameters.Add(New SqlParameter("@VIPCode", Me.GetVIPCode))
                objcmd.Parameters.Add(New SqlParameter("@Segment", Me.GetSegment))
                objcmd.Parameters.Add(New SqlParameter("@InsiderCode", Me.GetInsiderCode))
                objcmd.Parameters.Add(New SqlParameter("@CaseAlertDescription", Me.GetCaseAlertDescription))

                Using dsGeneral As Data.DataSet = SahassaNettier.Data.DataRepository.Provider.ExecuteDataSet(objcmd)
                    If dsGeneral.Tables.Count > 0 Then
                        Using dtGeneral As Data.DataTable = dsGeneral.Tables(0)
                            Me.SetnGetOldGeneralValues = dtGeneral
                            Me.SetValueToGridSerialWorkflowAndParalel(dtGeneral)
                        End Using
                    End If
                End Using
            End Using

            ' Get Data Detail Case Management Workflow
            Using oAccessGetDetail As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowDetailTableAdapter
                Dim DtDetail As Data.DataTable = oAccessGetDetail.GetData(Me.GetAccountOwnerID, Me.GetVIPCode, Me.GetSegment, Me.GetInsiderCode, Me.GetCaseAlertDescription)
                Me.SetnGetOldDetailValues = DtDetail
                If DtDetail.Rows.Count > 0 Then
                    Me.GenerateAndSetValueToGridApproversDueDateNotifyOthers(DtDetail)
                End If
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' SetValue To GridSerialWorkflowAndParalel
    ''' </summary>
    ''' <param name="DtGeneral"></param>
    ''' <remarks></remarks>
    Private Sub SetValueToGridSerialWorkflowAndParalel(ByVal DtGeneral As Data.DataTable)
        Try
            Dim Counter As Integer = 0
            For Each RowsGridSerialWorkflowAndParalel As GridViewRow In Me.GridViewParent.Rows
                Dim TxtWorkflowStepName As TextBox = CType(RowsGridSerialWorkflowAndParalel.Cells(1).Controls(1), TextBox)
                Dim TxtParalel As TextBox = CType(RowsGridSerialWorkflowAndParalel.Cells(2).Controls(1), TextBox)
                Dim dtGeneralCaseManagement As Data.DataTable = DtGeneral
                Dim RowGeneralCaseManagement As Data.DataRow = dtGeneralCaseManagement.Rows(Counter)
                TxtWorkflowStepName.Text = RowGeneralCaseManagement("WorkflowStep")
                TxtParalel.Text = RowGeneralCaseManagement("Paralel")
                Counter += 1
            Next
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Generate And SetValue To GridApproversDueDateNotifyOthers
    ''' </summary>
    ''' <param name="DtDetail"></param>
    ''' <remarks></remarks>
    Private Sub GenerateAndSetValueToGridApproversDueDateNotifyOthers(ByVal DtDetail As Data.DataTable)
        Try
           
            Dim IntLastRowIndex As Integer = 0
            For Each FirstRowsGrid As GridViewRow In GridViewParent.Rows ' looping row in first grid 

                Dim TxtParalel As TextBox = CType(FirstRowsGrid.Cells(2).Controls(1), TextBox)

                Dim GridApprovers As GridView = FirstRowsGrid.Cells(3).Controls(1) ' ambil id grid approvers
                Dim GridDueDate As GridView = FirstRowsGrid.Cells(4).Controls(1) ' ambil id grid DueDate
                Dim GridNotifyOthers As GridView = FirstRowsGrid.Cells(5).Controls(1) ' ambil id grid Notify Others

                CreateGridDetailCaseManagement(Convert.ToInt16(TxtParalel.Text), GridApprovers, GridDueDate, GridNotifyOthers)

                ' Set value
                For i As Integer = 0 To Convert.ToInt16(TxtParalel.Text) - 1
                    Dim Rows As Data.DataRow = DtDetail.Rows(IntLastRowIndex)

                    Dim RowsApproversGrid As GridViewRow = GridApprovers.Rows(i)
                    Dim RowsDueDate As GridViewRow = GridDueDate.Rows(i)
                    Dim RowsNotifyOthers As GridViewRow = GridNotifyOthers.Rows(i)

                    Dim TxtApprovers As TextBox = RowsApproversGrid.Cells(0).Controls(1)
                    Dim TxtDueDate As TextBox = RowsDueDate.Cells(0).Controls(1)
                    Dim TxtNotifyOthers As TextBox = RowsNotifyOthers.Cells(0).Controls(1)

                    TxtApprovers.Text = Me.GetUserIdByPk(Rows(1)) ' TXT Approvers
                    TxtDueDate.Text = Rows(2) ' TXT DueDate
                    TxtNotifyOthers.Text = Me.GetUserIdByPk(Rows(3)) ' TXT Notify Others
                    IntLastRowIndex += 1
                Next
            Next
        Catch
            Throw
        End Try
    End Sub
#End Region

#Region "Change Email Template"
    ''' <summary>
    ''' Disable/Enable Button Email
    ''' </summary>
    ''' <param name="boolEnable"></param>
    ''' <remarks></remarks>
    Public Sub DisableOrEnableBtnEmailTemplate(ByVal boolEnable As Boolean)
        Try
            For Each s As GridViewRow In Me.GridViewParent.Rows
                Dim BtnChangeEmailTemplate As Button = s.Cells(6).Controls(1) ' button change email template
                BtnChangeEmailTemplate.Enabled = boolEnable
            Next
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Change email template per serial
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub BtnChangeEmailTemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim BtnChangeEmailTemplate As Button = sender
            Dim FirstGridRow As GridViewRow = BtnChangeEmailTemplate.NamingContainer
            Dim oTextWorkFlowStep As TextBox = FirstGridRow.Cells(1).Controls(1)
            Dim WorkflowName As String = oTextWorkFlowStep.Text
            Dim Serial As String = FirstGridRow.Cells(0).Text

            MultiViewCaseManagement.ActiveViewIndex = 1
            Me.LblSerialEmail.Text = Serial
            Me.LblWorkflowStepNameEmail.Text = WorkflowName
            Me.LblHeader.Text = "Case Management Email Template"

            'clear
            Me.TxtSubjectEmail.Text = ""
            Me.TxtBodyEmail.Text = ""

            If Me.SetnGetMode = "Edit" Then
                ' edit
                If Me.SetnGetEmailTemplate Is Nothing Then
                    Using oAccessEmailTemplate As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowEmailTemplateTableAdapter
                        Me.SetnGetEmailTemplate = oAccessEmailTemplate.GetData(Me.GetAccountOwnerID)
                    End Using
                End If
                Dim Row() As Data.DataRow = Me.SetnGetEmailTemplate.Select("SerialNo=" & Serial)
                If Row.Length > 0 Then
                    Me.TxtSubjectEmail.Text = Row(0)(1).ToString ' Subject Email Template
                    Me.TxtBodyEmail.Text = Row(0)(2).ToString ' Body Email Template
                End If
            Else
                If Me.SetnGetEmailTemplate IsNot Nothing Then
                    For Each Row As Data.DataRow In Me.SetnGetEmailTemplate.Rows
                        If Row(0).ToString = Me.LblSerialEmail.Text Then
                            Me.TxtSubjectEmail.Text = Row(1) ' SUBJECT
                            Me.TxtBodyEmail.Text = Row(2) ' BODY
                            Exit For
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' Cancel Email Template
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancelEmail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancelEmail.Click
        Me.MultiViewCaseManagement.ActiveViewIndex = 0
        Me.LblHeader.Text = "Case Management Workflow - Detail"
    End Sub

    ''' <summary>
    ''' Save Email Template
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSaveEmail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSaveEmail.Click
        Try
            If Not Me.CheckValidEmailTemplate() Then
                Throw New Exception("Subject Email is required.")
            Else
                Dim DtEmailTemplate As Data.DataTable

                If Me.SetnGetEmailTemplate Is Nothing Then
                    DtEmailTemplate = New Data.DataTable
                    DtEmailTemplate.Columns.Add(New Data.DataColumn("SerialNo", GetType(Integer)))
                    DtEmailTemplate.Columns.Add(New Data.DataColumn("Subject", GetType(String)))
                    DtEmailTemplate.Columns.Add(New Data.DataColumn("Body", GetType(String)))
                    Me.SaveCaseManagementEmailTemplate("Add", Convert.ToInt16(Me.LblSerialEmail.Text), Me.TxtSubjectEmail.Text, Me.TxtBodyEmail.Text, DtEmailTemplate)
                Else
                    '' cek jika sudah ada / edit email template (antara ADD ato Edit)
                    DtEmailTemplate = CType(Me.SetnGetEmailTemplate, Data.DataTable)
                    Dim RowIndexEmail As Integer = 0
                    Dim flag As Boolean = False ' Add

                    For i As Integer = 0 To DtEmailTemplate.Rows.Count - 1
                        If DtEmailTemplate.Rows(i)(0).ToString = Me.LblSerialEmail.Text Then
                            RowIndexEmail = i
                            flag = True 'Edit
                            Exit For
                        End If
                    Next

                    If flag Then ' Edit
                        Me.SaveCaseManagementEmailTemplate("Edit", Convert.ToInt16(Me.LblSerialEmail.Text), Me.TxtSubjectEmail.Text, Me.TxtBodyEmail.Text, DtEmailTemplate, RowIndexEmail)
                    Else ' Add
                        Me.SaveCaseManagementEmailTemplate("Add", Convert.ToInt16(Me.LblSerialEmail.Text), Me.TxtSubjectEmail.Text, Me.TxtBodyEmail.Text, DtEmailTemplate)
                    End If
                End If
                ' Change View 
                Me.MultiViewCaseManagement.ActiveViewIndex = 0
                Me.LblHeader.Text = "Case Management Workflow - Detail"
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    ''' <summary>
    ''' Save Email Template
    ''' </summary>
    ''' <param name="Mode"></param>
    ''' <param name="intSerial"></param>
    ''' <param name="strSubject"></param>
    ''' <param name="strBody"></param>
    ''' <remarks></remarks>
    Private Sub SaveCaseManagementEmailTemplate(ByVal Mode As String, ByVal intSerial As Integer, ByVal strSubject As String, ByVal strBody As String, ByVal DtEmail As Data.DataTable, Optional ByVal RowIndex As Integer = 0)
        Try
            Dim RowEmail As Data.DataRow
            If Mode = "Add" Then
                RowEmail = DtEmail.NewRow
                RowEmail(0) = intSerial
                RowEmail(1) = strSubject
                RowEmail(2) = strBody
                DtEmail.Rows.Add(RowEmail)
            ElseIf Mode = "Edit" Then
                RowEmail = DtEmail.Rows(RowIndex)
                RowEmail(0) = intSerial
                RowEmail(1) = strSubject
                RowEmail(2) = strBody
            End If
            Me.SetnGetEmailTemplate = DtEmail
        Catch
            Throw
        End Try
    End Sub

#End Region

#Region "Generate Approvers, DueDate(days), dan NotifyOthers"
    ''' <summary>
    ''' CreateGrid Detail CaseManagement
    ''' </summary>
    ''' <param name="JmlParalel"></param>
    ''' <param name="ApproversGrid"></param>
    ''' <param name="DueDateGrid"></param>
    ''' <param name="NotifyOthersGrid"></param>
    ''' <remarks></remarks>
    Private Sub CreateGridDetailCaseManagement(ByVal JmlParalel As Integer, ByVal ApproversGrid As GridView, ByVal DueDateGrid As GridView, ByVal NotifyOthersGrid As GridView)
        Try
            Dim DtTableApprovers As New Data.DataTable
            Dim DtTableDueDate As New Data.DataTable
            Dim DtTableNotifyOthers As New Data.DataTable

            DtTableApprovers.Columns.Add(New Data.DataColumn(""))
            DtTableApprovers.Columns.Add(New Data.DataColumn("RowIndexApprovers"))
            DtTableNotifyOthers.Columns.Add(New Data.DataColumn(""))
            DtTableNotifyOthers.Columns.Add(New Data.DataColumn("RowIndexNotifyOthers"))

            For i As Integer = 1 To JmlParalel
                ' Approvers
                Dim ApproversRows As Data.DataRow = DtTableApprovers.NewRow
                DtTableApprovers.Rows.Add(ApproversRows)

                ' Due Date
                Dim DueDateRows As Data.DataRow = DtTableDueDate.NewRow
                DtTableDueDate.Rows.Add(DueDateRows)

                ' Notify Others
                Dim NotifyOthersRows As Data.DataRow = DtTableNotifyOthers.NewRow
                DtTableNotifyOthers.Rows.Add(NotifyOthersRows)
            Next

            ' Approvers
            ApproversGrid.DataSource = DtTableApprovers
            ApproversGrid.DataBind()

            ' Due Date
            DueDateGrid.DataSource = DtTableDueDate
            DueDateGrid.DataBind()

            ' Notify Others
            NotifyOthersGrid.DataSource = DtTableNotifyOthers
            NotifyOthersGrid.DataBind()

            '------------Clear Data Table----------------------------------------------------------------
            DtTableApprovers.Clear()
            DtTableDueDate.Clear()
            DtTableNotifyOthers.Clear()
            DtTableApprovers = Nothing
            DtTableDueDate = Nothing
            DtTableNotifyOthers = Nothing
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Generate
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageButtonParalel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim BtnParalel As ImageButton = sender
            Dim FirstGrid As GridViewRow = BtnParalel.NamingContainer
            Dim TxtPararel As TextBox = FirstGrid.Cells(2).Controls(1) ' column paralel dan ambil control textbox paralel
            Dim StrPattern As String = "\b\d+\b"

            If TxtPararel.Text = "" Then
                Throw New Exception("# of Paralel is empty. Please insert a number. ")
            ElseIf Not Regex.IsMatch(TxtPararel.Text, StrPattern) Then
                Throw New Exception("Please insert a number. Format input is not valid")
            ElseIf Convert.ToInt16(TxtPararel.Text) <= 0 Then
                Throw New Exception("Please input # of Paralel greater than 0.")
            Else
                Me.TempValues(Convert.ToInt16(FirstGrid.Cells(0).Text) - 1, "Get", "Approvers", Convert.ToInt16(TxtPararel.Text))
                Me.TempValues(Convert.ToInt16(FirstGrid.Cells(0).Text) - 1, "Get", "DueDate", Convert.ToInt16(TxtPararel.Text))
                Me.TempValues(Convert.ToInt16(FirstGrid.Cells(0).Text) - 1, "Get", "NotifyOthers", Convert.ToInt16(TxtPararel.Text))
                Me.DisableOrEnableBtnEmailTemplate(True)
                Dim JmlParalel As Integer = TxtPararel.Text
                '-------------Approvers-----------------------------------------------------------------
                Dim ApproversGrid As GridView = FirstGrid.Cells(3).Controls(1) 'ambil control gridview Approvers
                Dim DueDateGrid As GridView = FirstGrid.Cells(4).Controls(1) ' ambil control gridview DueDate
                Dim NotifyOthersGrid As GridView = FirstGrid.Cells(5).Controls(1) ' ambil control gridview Notify Others

                ' Create Grid Detail
                Me.CreateGridDetailCaseManagement(JmlParalel, ApproversGrid, DueDateGrid, NotifyOthersGrid)

                '-------------Set Value Ke Textbox Masing-Masing---------------------------------------------
                Me.TempValues(Convert.ToInt16(FirstGrid.Cells(0).Text) - 1, "Set", "Approvers")
                Me.TempValues(Convert.ToInt16(FirstGrid.Cells(0).Text) - 1, "Set", "DueDate")
                Me.TempValues(Convert.ToInt16(FirstGrid.Cells(0).Text) - 1, "Set", "NotifyOthers")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            ClientScript.RegisterClientScriptBlock(Page.GetType, "Script", "<script language='javascript'>alert('" & ex.Message & "');</script>")
        End Try
    End Sub

#End Region

#Region "Checking"
    ''' <summary>
    ''' function checking status Account Owner 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CheckStatusIsUnconfigured() As Boolean
        Try
            'Using oAccessCaseManagement As New AMLDAL.CaseManagementWorkflowTableAdapters.GetStatusAccountOwnerDatabyAccountOwnerIdTableAdapter
            '    Using oTableStatus As AMLDAL.CaseManagementWorkflow.GetStatusAccountOwnerDatabyAccountOwnerIdDataTable = oAccessCaseManagement.GetData(Me.GetAccountOwnerID)
            '        Dim Status As String = oTableStatus.Rows(0)(0) ' Description Status
            '        If Status.ToLower = "unconfigured" Then
            '            Return True
            '        Else
            '            Return False
            '        End If
            '    End Using
            'End Using
            If GetStatus.ToLower = "unconfigured" Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Function

    ''' <summary>
    ''' Check Valid EmailTemplate
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckValidEmailTemplate() As Boolean
        Try
            If Me.TxtSubjectEmail.Text = "" Then
                Return False
            Else
                Return True
            End If
        Catch
            Throw
        End Try
    End Function

#End Region

#Region "Save Case Management"


    Function IsDataWorkFlowValid() As Boolean

        If Me.GridViewParent.Rows.Count <= 2 Then
            Throw New Exception("Count Serial must greater or equal 3")
        End If
        If SetnGetEmailTemplate Is Nothing Then
            Throw New Exception("Email Template must be inputed")
        End If
        If SetnGetEmailTemplate.Rows.Count < TxtJumlahSerial.Text Then
            Throw New Exception("All Email must be inputed")
        End If
        Return True
    End Function


    ''' <summary>
    ''' Save Case Management workflow
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If IsDataWorkFlowValid() Then


                ' Cek apakah superuser apa tidak
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    Me.SaveCaseManagementWorkflow(True)
                    'change Status User               
                    Using oAccessChangeStatus As New AMLDAL.CaseManagementWorkflowTableAdapters.QueriesTableAdapter
                        oAccessChangeStatus.ChangeStatusAccountOwner(1, Me.GetAccountOwnerID)
                    End Using
                    Me.LblSuccess.Visible = True
                Else ' masuk pending approval
                    Me.SaveCaseManagementWorkflow(False)
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' SaveCaseManagementWorkflow
    ''' </summary>
    ''' <param name="boolIsSuperUser"></param>
    ''' <remarks></remarks>
    Public Sub SaveCaseManagementWorkflow(ByVal boolIsSuperUser As Boolean)
        Try

            If boolIsSuperUser Then

                Me.InsertCaseManagementWorkflowBySuperUser()
            Else
                Me.InsertCaseManagementWorkflowByNonSuperUser()
                ' Message Pending
                Dim MessagePendingID As Integer = 9001 'MessagePendingID 8201 = Case Management Workflow Add /Edit
                Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & Me.LblAccountOwner.Text
                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & Me.LblAccountOwner.Text, False)
            End If
        Catch ex As Exception
            Throw ex
        Finally
            ' clear all session
            ' Email template
            If Me.SetnGetEmailTemplate IsNot Nothing Then
                Me.SetnGetEmailTemplate.Clear()
                Me.SetnGetEmailTemplate = Nothing
            End If
            'Approvers
            If Me.SetnGetApproversValues IsNot Nothing Then
                Me.SetnGetApproversValues.Clear()
                Me.SetnGetApproversValues = Nothing
            End If
            'DueDate
            If Me.SetnGetDueDateValues IsNot Nothing Then
                Me.SetnGetDueDateValues.Clear()
                Me.SetnGetDueDateValues = Nothing
            End If
            'Notify Others 
            If Me.SetnGetNotifyOthersValues IsNot Nothing Then
                Me.SetnGetNotifyOthersValues.Clear()
                Me.SetnGetNotifyOthersValues = Nothing
            End If
            ' Root General
            If Me.SetnGetRootGeneral IsNot Nothing Then
                Me.SetnGetRootGeneral.Clear()
                Me.SetnGetRootGeneral = Nothing
            End If
            'Root Detail 
            If Me.SetnGetRootDetail IsNot Nothing Then
                Me.SetnGetRootDetail.Clear()
                Me.SetnGetRootDetail = Nothing
            End If
            ' Email Template
            If Me.SetnGetEmailTemplate IsNot Nothing Then
                Me.SetnGetEmailTemplate.Clear()
                Me.SetnGetEmailTemplate = Nothing
            End If
        End Try
    End Sub

#Region "Insert By SuperUser / Non SuperUser"

    ''' <summary>
    ''' Insert By SuperUser
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertCaseManagementWorkflowBySuperUser()
        Dim Trans As SqlTransaction
        Try

            If Me.SetnGetMode = "Edit" Then
                ' Delete Case Management WorkFlow 
                Using oAccessCMWQueries As New AMLDAL.CaseManagementWorkflowTableAdapters.TransTableAdapterTableAdapter
                    Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(oAccessCMWQueries)

                    oAccessCMWQueries.DeleteCaseManagementWorkflow(Me.GetAccountOwnerID)
                End Using
            End If

            Dim Pk_CMW_ID As Decimal
            ' Insert into master case management
            Using oAccessMasterCaseManagement As New AMLDAL.CaseManagementWorkflowTableAdapters.InsertCaseManagementWorkflowTableAdapter
                If Me.SetnGetMode = "Edit" Then
                    Sahassa.AML.TableAdapterHelper.SetTransaction(oAccessMasterCaseManagement, Trans)
                Else
                    Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(oAccessMasterCaseManagement)
                End If

                Pk_CMW_ID = oAccessMasterCaseManagement.Fill(Me.GetAccountOwnerID, Me.GetSegment, Me.GetVIPCode, Me.GetInsiderCode, Me.GetSBU, Me.GetSUBSBU, Me.GetRM, Me.GetCaseAlertDescription, Now, Now)
            End Using

            Dim DtTempAuditTrail As New Data.DataTable
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("SerialNo", GetType(String)))
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("WorkflowName", GetType(String)))
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("Paralel", GetType(String)))
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("Approvers", GetType(String)))
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("DueDate", GetType(String)))
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("NotifyOthers", GetType(String)))
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("SubjectEmailTemplate", GetType(String)))
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("BodyEmailTemplate", GetType(String)))



            Using oAccessGeneralCaseManagement As New AMLDAL.CaseManagementWorkflowTableAdapters.TransTableAdapterTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(oAccessGeneralCaseManagement, Trans)
                For Each FirstRowsGrid As GridViewRow In Me.GridViewParent.Rows ' looping row in first grid 
                    Dim IntSerial As Int32 = Convert.ToInt32(FirstRowsGrid.Cells(0).Text)
                    Dim TxtWorkflowName As TextBox = CType(FirstRowsGrid.Cells(1).Controls(1), TextBox)
                    Dim TxtParalel As TextBox = CType(FirstRowsGrid.Cells(2).Controls(1), TextBox)

                    ' Checking ValidationGeneralCaseManagement
                    CheckingValidationGeneralCaseManagement(TxtWorkflowName.Text, Convert.ToInt64(TxtParalel.Text), IntSerial)

                    ' insert Case Management General (Serial,Workflow Name,Paralel)
                    oAccessGeneralCaseManagement.InsertCaseManagementWorkflowGeneral(Convert.ToInt64(Pk_CMW_ID), IntSerial, TxtWorkflowName.Text, TxtParalel.Text)

                    Dim StrTempApprovers As String = ""
                    Dim StrTempDueDate As String = ""
                    Dim StrTempNotifyOthers As String = ""

                    For i As Integer = 0 To Convert.ToInt16(TxtParalel.Text) - 1
                        Dim GridApprovers As GridView = FirstRowsGrid.Cells(3).Controls(1) ' ambil id grid approvers
                        Dim GridDueDate As GridView = FirstRowsGrid.Cells(4).Controls(1) ' ambil id grid DueDate
                        Dim GridNotifyOthers As GridView = FirstRowsGrid.Cells(5).Controls(1) ' ambil id grid Notify Others
                        Dim RowsApproversGrid As GridViewRow = GridApprovers.Rows(i)
                        Dim RowsDueDate As GridViewRow = GridDueDate.Rows(i)
                        Dim RowsNotifyOthers As GridViewRow = GridNotifyOthers.Rows(i)

                        Dim TxtApprovers As TextBox = RowsApproversGrid.Cells(0).Controls(1)
                        Dim TxtDueDate As TextBox = RowsDueDate.Cells(0).Controls(1)
                        Dim TxtNotifyOthers As TextBox = RowsNotifyOthers.Cells(0).Controls(1)

                        ' Checking if there is nothing or null
                        CheckingValidationDetailCaseManagement(TxtApprovers.Text, TxtDueDate.Text, TxtNotifyOthers.Text)

                        ' Insert Case Management Detail (Approvers, Due Date, Notify Others)
                        oAccessGeneralCaseManagement.InsertCaseManagementWorkflowDetail(Convert.ToInt64(Pk_CMW_ID), IntSerial, Me.GetPkUserId(TxtApprovers.Text), Convert.ToInt64(TxtDueDate.Text), Me.GetPkUserId(TxtNotifyOthers.Text))

                        If i = Convert.ToInt16(TxtParalel.Text) - 1 Then
                            StrTempApprovers &= TxtApprovers.Text
                            StrTempDueDate &= TxtDueDate.Text
                            StrTempNotifyOthers &= TxtNotifyOthers.Text
                        Else
                            StrTempApprovers &= TxtApprovers.Text & " | "
                            StrTempDueDate &= TxtDueDate.Text & " | "
                            StrTempNotifyOthers &= TxtNotifyOthers.Text & " | "
                        End If
                    Next

                    Dim Rows() As Data.DataRow = Me.SetnGetEmailTemplate.Select("SerialNo=" & IntSerial)
                    Dim Subject As String = Nothing
                    Dim Body As String = Nothing
                    If Rows.Length > 0 Then
                        Subject = Rows(0)(1).ToString
                        Body = Rows(0)(2).ToString
                    End If

                    '--------------------- Temp Value General for Audit Trail -------------------------------
                    Dim RowAuditTrail As Data.DataRow
                    RowAuditTrail = DtTempAuditTrail.NewRow
                    RowAuditTrail(0) = FirstRowsGrid.Cells(0).Text
                    RowAuditTrail(1) = TxtWorkflowName.Text
                    RowAuditTrail(2) = TxtParalel.Text
                    RowAuditTrail(3) = StrTempApprovers
                    RowAuditTrail(4) = StrTempDueDate
                    RowAuditTrail(5) = StrTempNotifyOthers
                    RowAuditTrail(6) = Subject
                    RowAuditTrail(7) = Body

                    DtTempAuditTrail.Rows.Add(RowAuditTrail)
                    '----------------------------------------------------------------------------------------

                    ' Save EmailTemplate 
                    Me.InsertEmailTemplate(Convert.ToInt16(Pk_CMW_ID), IntSerial, True, Trans)
                Next
            End Using
            Trans.Commit()
            ' Insert into AuditTrail
            Me.InsertIntoAuditTrail(DtTempAuditTrail)

            ' transscope is complete
            'oTrans.Complete()
            'End Using
        Catch ex As Exception
            If Not Trans Is Nothing Then Trans.Rollback()
            Throw ex
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
                Trans = Nothing
            End If
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="DtAuditTrail"></param>
    ''' <remarks></remarks>
    Private Sub InsertIntoAuditTrail(ByRef DtAuditTrail As Data.DataTable)
        Try
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                Using oAccessCreatedDate As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCreateDateCMWTableAdapter
                    Dim DtCreatedDate As AMLDAL.CaseManagementWorkflow.SelectCreateDateCMWDataTable = oAccessCreatedDate.GetData(Me.GetAccountOwnerID)
                    Dim CreatedDate As DateTime = DtCreatedDate.Rows(0)("CreatedDate")
                    Dim LastUpdatedDate As DateTime = DtCreatedDate.Rows(0)("LastUpdateDate")

                    If Me.SetnGetMode = "Edit" Then
                        ' Checking AuditTrail
                        Sahassa.AML.AuditTrailAlert.CheckMailAlert(11 * (DtAuditTrail.Rows.Count + Me.SetnGetOldGeneralValues.Rows.Count))

                        'Get All General Old Values
                        For Each OldRow As Data.DataRow In SetnGetOldGeneralValues.Rows
                            Dim StrApprovers_Old As String = ""
                            Dim StrDueDate_Old As String = ""
                            Dim StrNotifyOthers_Old As String = ""
                            Dim StrSubject_Old As String = ""
                            Dim StrBody_Old As String = ""

                            ' Get detail CMW Old per serial No
                            Using oAccessDetailCMW_Old As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowDetail_BySerialNoTableAdapter
                                Dim DtDetailCMW_Old As Data.DataTable = oAccessDetailCMW_Old.GetData(Me.GetAccountOwnerID, OldRow(0))
                                If DtDetailCMW_Old.Rows.Count > 0 Then
                                    For i As Integer = 0 To DtDetailCMW_Old.Rows.Count - 1
                                        Dim RowsDetail As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowDetail_BySerialNoRow = DtDetailCMW_Old.Rows(i)
                                        If i = DtDetailCMW_Old.Rows.Count - 1 Then
                                            StrApprovers_Old &= RowsDetail.Approvers
                                            StrDueDate_Old &= RowsDetail.DueDate
                                            StrNotifyOthers_Old &= RowsDetail.NotifyOthers
                                        Else
                                            StrApprovers_Old &= RowsDetail.Approvers & " | "
                                            StrDueDate_Old &= RowsDetail.DueDate & " | "
                                            StrNotifyOthers_Old &= RowsDetail.NotifyOthers & " | "
                                        End If
                                    Next
                                End If
                            End Using

                            ' Get email template per serial no
                            Using oAccessEmailTemplate_Old As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowEmailTemplate_BySerialNoTableAdapter
                                Dim DtEmail As Data.DataTable = oAccessEmailTemplate_Old.GetData(Me.GetAccountOwnerID, OldRow(0))
                                If DtEmail.Rows.Count > 0 Then
                                    Dim RowEmail As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowEmailTemplate_BySerialNoRow = DtEmail.Rows(0)
                                    StrSubject_Old = RowEmail.Subject_EmailTemplate
                                    StrBody_Old = RowEmail.Body_EmailTemplate
                                End If
                            End Using

                            ' insert Audit Trail        
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "AccountOwnerName", "Delete", "", Me.LblAccountOwner.Text, "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "SerialNo", "Delete", "", OldRow(0), "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "WorkflowName", "Delete", "", OldRow(1), "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "Paralel", "Delete", "", OldRow(2), "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "Approvers", "Delete", "", StrApprovers_Old, "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "DueDate", "Delete", "", StrDueDate_Old, "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "NotifyOthers", "Delete", "", StrNotifyOthers_Old, "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "SubjectEmailTemplate", "Delete", "", StrSubject_Old, "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "BodyEmailTemplate", "Delete", "", StrBody_Old, "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "CreatedDate", "Delete", "", CreatedDate, "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "LastModifiedDate", "Delete", "", LastUpdatedDate, "Accepted")
                        Next
                    Else 'Edit
                        Sahassa.AML.AuditTrailAlert.CheckMailAlert(11 * DtAuditTrail.Rows.Count)
                    End If

                    ' Insert Audit trail untuk yang baru
                    For Each RowAuditTrail As Data.DataRow In DtAuditTrail.Rows
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "AccountOwnerName", "Add", "", Me.LblAccountOwner.Text, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "SerialNo", "Add", "", RowAuditTrail(0), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "WorkflowName", "Add", "", RowAuditTrail(1), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "Paralel", "Add", "", RowAuditTrail(2), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "Approvers", "Delete", "", RowAuditTrail(3), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "DueDate", "Delete", "", RowAuditTrail(4), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "NotifyOthers", "Delete", "", RowAuditTrail(5), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "SubjectEmailTemplate", "Delete", "", RowAuditTrail(6), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "BodyEmailTemplate", "Delete", "", RowAuditTrail(7), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "CreatedDate", "Add", "", CreatedDate, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - CaseManagementWorkflow", "LastModifiedDate", "Add", "", Now, "Accepted")
                    Next
                End Using
            End Using
        Catch
            Throw
        Finally
            ' clear data table
            If DtAuditTrail IsNot Nothing Then
                DtAuditTrail.Clear()
                DtAuditTrail = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Insert By Non SuperUser
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertCaseManagementWorkflowByNonSuperUser()
        Dim Trans As SqlTransaction = Nothing

        Try
            Dim Pk_CMW_Approval_ID As Decimal
            ' Insert into master case management

            Using oAccessMasterCaseManagementApproval As New AMLDAL.CaseManagementWorkflowTableAdapters.InsertCaseManagementWorkflow_ApprovalTableAdapter
                Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(oAccessMasterCaseManagementApproval, Data.IsolationLevel.ReadUncommitted)

                Pk_CMW_Approval_ID = oAccessMasterCaseManagementApproval.Fill(Me.GetAccountOwnerID, GetSegment, GetVIPCode, Trim(GetInsiderCode), GetCaseAlertDescription, Me.GetSBU, Me.GetSUBSBU, Me.GetRM, Sahassa.AML.Commonly.SessionUserId, SetnGetMode, Now)
            End Using

            Using oAccessQueryCaseManagement As New AMLDAL.CaseManagementWorkflowTableAdapters.TransTableAdapterTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(oAccessQueryCaseManagement, Trans)

                For Each FirstRowsGrid As GridViewRow In Me.GridViewParent.Rows ' looping row in first grid 
                    Dim IntSerial As Int32 = Convert.ToInt32(FirstRowsGrid.Cells(0).Text)
                    Dim TxtWorkflowName As TextBox = CType(FirstRowsGrid.Cells(1).Controls(1), TextBox)
                    Dim TxtParalel As TextBox = CType(FirstRowsGrid.Cells(2).Controls(1), TextBox)

                    CheckingValidationGeneralCaseManagement(TxtWorkflowName.Text, Convert.ToInt64(TxtParalel.Text), IntSerial)

                    ' insert Case Management General Approval (Serial,Workflow Name,Paralel)
                    oAccessQueryCaseManagement.InsertCaseManagementWorkflowGeneral_ApprovalDetail(Convert.ToInt64(Pk_CMW_Approval_ID), IntSerial, TxtWorkflowName.Text, TxtParalel.Text)

                    For i As Integer = 0 To Convert.ToInt16(TxtParalel.Text) - 1

                        Dim GridApprovers As GridView = FirstRowsGrid.Cells(3).Controls(1) ' ambil id grid approvers
                        Dim GridDueDate As GridView = FirstRowsGrid.Cells(4).Controls(1) ' ambil id grid DueDate
                        Dim GridNotifyOthers As GridView = FirstRowsGrid.Cells(5).Controls(1) ' ambil id grid Notify Others
                        Dim RowsApproversGrid As GridViewRow = GridApprovers.Rows(i)
                        Dim RowsDueDate As GridViewRow = GridDueDate.Rows(i)
                        Dim RowsNotifyOthers As GridViewRow = GridNotifyOthers.Rows(i)

                        Dim TxtApprovers As TextBox = RowsApproversGrid.Cells(0).Controls(1)
                        Dim TxtDueDate As TextBox = RowsDueDate.Cells(0).Controls(1)
                        Dim TxtNotifyOthers As TextBox = RowsNotifyOthers.Cells(0).Controls(1)

                        ' Checking if there is nothing or null
                        CheckingValidationDetailCaseManagement(TxtApprovers.Text, TxtDueDate.Text, TxtNotifyOthers.Text)

                        ' Insert Case Management Detail Approval(Approvers, Due Date, Notify Others)
                        oAccessQueryCaseManagement.InsertCaseManagementWorkflowDetail_ApprovalDetail(Convert.ToInt64(Pk_CMW_Approval_ID), IntSerial, Me.GetPkUserId(TxtApprovers.Text), Convert.ToInt64(TxtDueDate.Text), Me.GetPkUserId(TxtNotifyOthers.Text))
                    Next
                    ' Insert Email Template
                    Me.InsertEmailTemplate(Convert.ToInt16(Pk_CMW_Approval_ID), IntSerial, False, Trans)
                Next
            End Using
            Trans.Commit()
            ' transcope is complete
            'oTrans.Complete()
            'End Using
        Catch ex As Exception
            If Not Trans Is Nothing Then Trans.Rollback()
            Throw ex
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
                Trans = Nothing
            End If
        End Try
    End Sub

#Region "Insert EmailTemplate By SuperUser / NonSuperUser"
    ''' <summary>
    ''' InsertEmailTemplate
    ''' </summary>
    ''' <param name="Pk_CMW_ID"></param>
    ''' <param name="intSerial"></param>
    ''' <param name="isSuperUser"></param>
    ''' <remarks></remarks>
    Private Sub InsertEmailTemplate(ByVal Pk_CMW_ID As Int16, ByVal intSerial As Integer, ByVal isSuperUser As Boolean, ByRef TransRef As SqlTransaction)
        Try
            Using oAccessEmailTemplate As New AMLDAL.CaseManagementWorkflowTableAdapters.TransTableAdapterTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(oAccessEmailTemplate, TransRef)
                If Me.SetnGetEmailTemplate IsNot Nothing Then
                    Dim RowEmailTemplate() As Data.DataRow = Me.SetnGetEmailTemplate.Select("SerialNo=" & intSerial)
                    Dim StrSubject As String = ""
                    Dim StrBody As String = ""
                    If Not RowEmailTemplate Is Nothing Then
                        If RowEmailTemplate.Length > 0 Then
                            StrSubject = RowEmailTemplate(0)(1)
                            StrBody = RowEmailTemplate(0)(2)
                        End If
                    End If
                    If isSuperUser Then
                        oAccessEmailTemplate.InsertCaseManagementWorkflowEmailTemplate(Pk_CMW_ID, intSerial, StrSubject, StrBody)
                    Else
                        oAccessEmailTemplate.InsertCaseManagementWorkflowEmailTemplate_ApprovalDetail(Pk_CMW_ID, intSerial, StrSubject, StrBody)
                    End If
                End If
            End Using
        Catch
            Throw
        End Try
    End Sub
#End Region

#End Region

#Region "Checking"

    ''' <summary>
    ''' CheckingValidationGeneralCaseManagement
    ''' </summary>
    ''' <param name="strWorkflowName"></param>
    ''' <param name="intParalel"></param>
    ''' <remarks></remarks>
    Public Sub CheckingValidationGeneralCaseManagement(ByVal strWorkflowName As String, ByVal intParalel As Int64, ByVal IntSerial As Integer)
        Try
            If strWorkflowName = "" Then
                Throw New Exception("Workflow Name for Serial '" & IntSerial & "' is required.")
            End If
            If intParalel <= 0 Then
                Throw New Exception("Paralel for Serial '" & IntSerial & "' must be greater than zero.")
            End If
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' CheckingValidation DetailCaseManagement
    ''' </summary>
    ''' <param name="Approvers"></param>
    ''' <param name="DueDate"></param>
    ''' <param name="NotifyOthers"></param>
    ''' <remarks></remarks>
    Public Sub CheckingValidationDetailCaseManagement(ByVal Approvers As String, ByVal DueDate As String, ByVal NotifyOthers As String)
        Try
            Dim StrPattern As String = "\b\d+\b"
            If Approvers = "" Then
                Throw New Exception("Approvers is required. Minimum one User id selected.")
            End If
            If Regex.IsMatch(DueDate, StrPattern) = False Then
                Throw New Exception("Wrong input duedate. Please insert a numeric.")
            End If
            ' Commented by Johan 11:22 AM 12/3/2007
            ' Caused by there is no mandatory
            'If NotifyOthers = "" Then
            '    Throw New Exception("Notify Others is required. Minimum one User id selected.")
            'End If
        Catch
            Throw
        End Try
    End Sub

#End Region

    ''' <summary>
    ''' get Pk user id
    ''' </summary>
    ''' <param name="strUserID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetPkUserId(ByVal strUserID As String) As String
        Try
            Dim StrPkUserID As String = Nothing
            If strUserID <> "" Then
                Dim oArrUserId As String() = strUserID.Split(";")
                Using oAccessUserID As New AMLDAL.CaseManagementWorkflowTableAdapters.GetPkUserIdTableAdapter
                    For i As Integer = 0 To oArrUserId.Length - 1
                        If i = oArrUserId.Length - 1 Then
                            StrPkUserID &= Convert.ToString(oAccessUserID.GetPkUserID(oArrUserId(i)))
                        Else
                            StrPkUserID &= Convert.ToString(oAccessUserID.GetPkUserID(oArrUserId(i))) & "|"
                        End If
                    Next
                End Using
            Else
                StrPkUserID = "0"
            End If
            Return StrPkUserID
        Catch
            Throw
        End Try
    End Function
    ''' <summary>
    ''' Get UserId By Pk
    ''' </summary>
    ''' <param name="PkUserID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetUserIdByPk(ByVal PkUserID As String) As String
        Try
            Dim StrUserID As String = Nothing
            If PkUserID <> "" Then
                Dim oArrPkUserId As String() = PkUserID.Split("|")
                Using oAccessUserID As New AMLDAL.CaseManagementWorkflowTableAdapters.GetUserIDByPKTableAdapter
                    For i As Integer = 0 To oArrPkUserId.Length - 1
                        If i = oArrPkUserId.Length - 1 Then
                            StrUserID &= Convert.ToString(oAccessUserID.Fill(Convert.ToInt32(oArrPkUserId(i))))
                        Else
                            StrUserID &= Convert.ToString(oAccessUserID.Fill(Convert.ToInt32(oArrPkUserId(i)))) & ";"
                        End If
                    Next
                End Using
            End If
            Return StrUserID
        Catch
            Throw
        End Try
    End Function
#End Region

#Region "Change ROOT"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="o_RowGridRoot"></param>
    ''' <param name="intParalel"></param>
    ''' <param name="Category"></param>
    ''' <param name="intSerial"></param>
    ''' <remarks></remarks>
    Public Sub GetTempValueFromRoot(ByVal o_RowGridRoot As GridViewRow, ByVal intParalel As Int16, ByVal Category As String, Optional ByVal intSerial As Integer = 1)
        Try
            Dim GridviewApprovers As GridView = CType(o_RowGridRoot.Cells(3).Controls(1), GridView)
            Dim GridviewDueDate As GridView = CType(o_RowGridRoot.Cells(4).Controls(1), GridView)
            Dim GridviewNotifyOthers As GridView = CType(o_RowGridRoot.Cells(5).Controls(1), GridView)
            Select Case Category.ToLower
                Case "get"
                    For j As Integer = 0 To intParalel
                        If GridviewApprovers.Rows.Count <> 0 Then
                            Dim RowApprovers As GridViewRow = GridviewApprovers.Rows(j)
                            Dim RowDueDate As GridViewRow = GridviewDueDate.Rows(j)
                            Dim RowNotifyOthers As GridViewRow = GridviewNotifyOthers.Rows(j)
                            Dim TxtApprovers As TextBox = CType(RowApprovers.Cells(0).Controls(1), TextBox)
                            Dim TxtDueDate As TextBox = CType(RowDueDate.Cells(0).Controls(1), TextBox)
                            Dim TxtNotifyOthers As TextBox = CType(RowNotifyOthers.Cells(0).Controls(1), TextBox)

                            Dim RowTempGeneral As Data.DataRow = Me.SetnGetRootDetail.NewRow
                            RowTempGeneral(0) = Convert.ToInt16(o_RowGridRoot.Cells(0).Text) ' No Serial
                            RowTempGeneral(1) = TxtApprovers.Text
                            If TxtDueDate.Text = "" Then
                                TxtDueDate.Text = 0
                            End If
                            RowTempGeneral(2) = TxtDueDate.Text
                            RowTempGeneral(3) = TxtNotifyOthers.Text
                            Me.SetnGetRootDetail.Rows.Add(RowTempGeneral)
                        Else
                            Exit For
                        End If
                    Next
                Case "set"
                    Dim RowTempDetail() As Data.DataRow = Me.SetnGetRootDetail.Select("Serial=" & intSerial)
                    For i As Integer = 0 To RowTempDetail.Length - 1
                        Dim RowApprovers As GridViewRow = GridviewApprovers.Rows(i)
                        Dim RowDueDate As GridViewRow = GridviewDueDate.Rows(i)
                        Dim RowNotifyOthers As GridViewRow = GridviewNotifyOthers.Rows(i)
                        Dim TxtApprovers As TextBox = CType(RowApprovers.Cells(0).Controls(1), TextBox)
                        Dim TxtDueDate As TextBox = CType(RowDueDate.Cells(0).Controls(1), TextBox)
                        Dim TxtNotifyOthers As TextBox = CType(RowNotifyOthers.Cells(0).Controls(1), TextBox)
                        Dim SelectedRow As Data.DataRow = RowTempDetail(i)
                        TxtApprovers.Text = SelectedRow(1)
                        TxtNotifyOthers.Text = SelectedRow(3)
                        TxtDueDate.Text = SelectedRow(2)
                    Next
            End Select
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetRootTempValuesGeneral()
        Try
            If Me.GridViewParent.Rows.Count > 0 Then
                Dim RowsValueGeneral As Data.DataRow
                If Me.SetnGetRootGeneral IsNot Nothing Then
                    Me.SetnGetRootGeneral.Rows.Clear()
                End If
                Dim IntCounter As Integer = 0
                If Convert.ToInt16(Me.TxtJumlahSerial.Text) > Me.GridViewParent.Rows.Count Then
                    IntCounter = Me.GridViewParent.Rows.Count - 1
                Else
                    IntCounter = Convert.ToInt16(Me.TxtJumlahSerial.Text) - 1
                End If
                For i As Integer = 0 To IntCounter
                    Dim RowGeneral As GridViewRow = Me.GridViewParent.Rows(i)
                    Dim TxtWorkflow As TextBox = CType(RowGeneral.Cells(1).Controls(1), TextBox)
                    Dim TxtParalel As TextBox = CType(RowGeneral.Cells(2).Controls(1), TextBox)

                    If Me.SetnGetRootDetail Is Nothing Then
                        Dim DtTempAllDetail As New Data.DataTable
                        DtTempAllDetail.Columns.Add(New Data.DataColumn("Serial", GetType(Integer)))
                        DtTempAllDetail.Columns.Add(New Data.DataColumn("Approvers", GetType(String)))
                        DtTempAllDetail.Columns.Add(New Data.DataColumn("DueDate", GetType(Integer)))
                        DtTempAllDetail.Columns.Add(New Data.DataColumn("NotifyOthers", GetType(String)))
                        Me.SetnGetRootDetail = DtTempAllDetail
                    End If

                    If TxtParalel.Text <> "" And Regex.IsMatch(TxtParalel.Text, "\b\d+\b") = True Then
                        Me.GetTempValueFromRoot(RowGeneral, Convert.ToInt16(TxtParalel.Text) - 1, "Get", Convert.ToInt16(RowGeneral.Cells(0).Text))
                    End If

                    RowsValueGeneral = Me.SetnGetRootGeneral.NewRow
                    RowsValueGeneral(0) = TxtWorkflow.Text
                    If TxtParalel.Text = "" Or Regex.IsMatch(TxtParalel.Text, "\b\d+\b") = False Then
                        TxtParalel.Text = 0
                    End If
                    RowsValueGeneral(1) = TxtParalel.Text
                    Me.SetnGetRootGeneral.Rows.Add(RowsValueGeneral)
                Next
            End If
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetRootTempValuesGeneral()
        Try
            If Me.SetnGetRootGeneral.Rows.Count > 0 Then
                For i As Integer = 0 To Me.SetnGetRootGeneral.Rows.Count - 1
                    Dim RowGeneral As GridViewRow = Me.GridViewParent.Rows(i)
                    Dim Txtworkflow As TextBox = CType(RowGeneral.Cells(1).Controls(1), TextBox)
                    Dim TxtParalel As TextBox = CType(RowGeneral.Cells(2).Controls(1), TextBox)
                    Txtworkflow.Text = Me.SetnGetRootGeneral.Rows(i)(0)
                    TxtParalel.Text = Me.SetnGetRootGeneral.Rows(i)(1)
                    Dim ApproversGrid As GridView = RowGeneral.Cells(3).Controls(1) 'ambil control gridview Approvers
                    Dim DueDateGrid As GridView = RowGeneral.Cells(4).Controls(1) ' ambil control gridview DueDate
                    Dim NotifyOthersGrid As GridView = RowGeneral.Cells(5).Controls(1) ' ambil control gridview Notify Others
                    If TxtParalel.Text <> "" Or Regex.IsMatch(TxtParalel.Text, "\b\d+\b") Then
                        CreateGridDetailCaseManagement(Convert.ToInt16(TxtParalel.Text), ApproversGrid, DueDateGrid, NotifyOthersGrid)
                        Me.GetTempValueFromRoot(RowGeneral, Convert.ToInt16(TxtParalel.Text), "Set", Convert.ToInt16(RowGeneral.Cells(0).Text))
                    End If
                Next

            End If
        Catch
            Throw
        End Try
    End Sub
#End Region

    ''' <summary>
    '''  Cancel Save
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("CaseManagementWorkflowView.aspx", False)
    End Sub
End Class
