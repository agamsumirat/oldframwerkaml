﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports System.Data

Partial Class CDDLNPMappingQuestionApprovalDetail
    Inherits Parent

    Private ReadOnly Property GetPK_MappingSettingCDDBagianQuestion_Approval_Id() As Int32
        Get
            Return IIf(Request.QueryString("ID") = String.Empty, 0, Request.QueryString("ID"))
        End Get
    End Property

    Private Property SetnGetFK_SettingCDDLNP_ID() As Int32
        Get
            Return CType(Session("CDDLNPMappingQuestionApproval.FK_SettingCDDLNP_ID"), Int32)
        End Get
        Set(ByVal value As Int32)
            Session("CDDLNPMappingQuestionApproval.FK_SettingCDDLNP_ID") = value
        End Set
    End Property

#Region "Old"
    Private Property SetnGetCurrentPageOld() As Int32
        Get
            Return IIf(Session("CDDLNPMappingQuestionOld.CurrentPage") Is Nothing, 0, Session("CDDLNPMappingQuestionOld.CurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPMappingQuestionOld.CurrentPage") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotalOld() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetBindTableOld.Count)
        End Get
    End Property

    Private Property SetnGetBindTableOld() As TList(Of MappingSettingCDDBagianQuestion)
        Get
            Return CType(Session("MappingSettingCDDBagianQuestionOld"), TList(Of MappingSettingCDDBagianQuestion))
        End Get
        Set(ByVal value As TList(Of MappingSettingCDDBagianQuestion))
            Session("MappingSettingCDDBagianQuestionOld") = value
        End Set
    End Property

    Public Sub PageNavigateOld(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPageOld = 0
                Case "Prev" : Me.SetnGetCurrentPageOld -= 1
                Case "Next" : Me.SetnGetCurrentPageOld += 1
                Case "Last" : Me.SetnGetCurrentPageOld = Me.GetPageTotalOld - 1
                Case Else : Throw New Exception("Unknown Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Private Sub SetInfoNavigateOld()
        Try
            Me.PageCurrentPageOld.Text = Me.SetnGetCurrentPageOld + 1
            Me.PageTotalPagesOld.Text = Me.GetPageTotalOld
            Me.PageTotalRowsOld.Text = Me.SetnGetBindTableOld.Count
            Me.LinkButtonNextOld.Enabled = (Not Me.SetnGetCurrentPageOld + 1 = Me.GetPageTotalOld) AndAlso Me.GetPageTotalOld <> 0
            Me.LinkButtonLastOld.Enabled = (Not Me.SetnGetCurrentPageOld + 1 = Me.GetPageTotalOld) AndAlso Me.GetPageTotalOld <> 0
            Me.LinkButtonFirstOld.Enabled = Not Me.SetnGetCurrentPageOld = 0
            Me.LinkButtonPreviousOld.Enabled = Not Me.SetnGetCurrentPageOld = 0
        Catch
            Throw
        End Try
    End Sub

#End Region

#Region "New"
    Private Property SetnGetCurrentPageNew() As Int32
        Get
            Return IIf(Session("CDDLNPMappingQuestionNew.CurrentPage") Is Nothing, 0, Session("CDDLNPMappingQuestionNew.CurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPMappingQuestionNew.CurrentPage") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotalNew() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetBindTableNew.Count)
        End Get
    End Property

    Private Property SetnGetBindTableNew() As TList(Of MappingSettingCDDBagianQuestion)
        Get
            Return CType(Session("MappingSettingCDDBagianQuestionNew"), TList(Of MappingSettingCDDBagianQuestion))
        End Get
        Set(ByVal value As TList(Of MappingSettingCDDBagianQuestion))
            Session("MappingSettingCDDBagianQuestionNew") = value
        End Set
    End Property

    Public Sub PageNavigateNew(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPageNew = 0
                Case "Prev" : Me.SetnGetCurrentPageNew -= 1
                Case "Next" : Me.SetnGetCurrentPageNew += 1
                Case "Last" : Me.SetnGetCurrentPageNew = Me.GetPageTotalNew - 1
                Case Else : Throw New Exception("Unknown Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Private Sub SetInfoNavigateNew()
        Try
            Me.PageCurrentPageNew.Text = Me.SetnGetCurrentPageNew + 1
            Me.PageTotalPagesNew.Text = Me.GetPageTotalNew
            Me.PageTotalRowsNew.Text = Me.SetnGetBindTableNew.Count
            Me.LinkButtonNextNew.Enabled = (Not Me.SetnGetCurrentPageNew + 1 = Me.GetPageTotalNew) AndAlso Me.GetPageTotalNew <> 0
            Me.LinkButtonLastNew.Enabled = (Not Me.SetnGetCurrentPageNew + 1 = Me.GetPageTotalNew) AndAlso Me.GetPageTotalNew <> 0
            Me.LinkButtonFirstNew.Enabled = Not Me.SetnGetCurrentPageNew = 0
            Me.LinkButtonPreviousNew.Enabled = Not Me.SetnGetCurrentPageNew = 0
        Catch
            Throw
        End Try
    End Sub
#End Region

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageError.PreRender
        If cvalPageError.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("CDDLNPMappingQuestionApproval.aspx")
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                'Load data Approval Header
                Using objApproval As VList(Of vw_MappingSettingCDDBagianQuestion_Approval) = DataRepository.vw_MappingSettingCDDBagianQuestion_ApprovalProvider.GetPaged("PK_MappingSettingCDDBagianQuestion_Approval_Id = " & Me.GetPK_MappingSettingCDDBagianQuestion_Approval_Id, String.Empty, 0, Int32.MaxValue, 0)
                    If objApproval.Count > 0 Then
                        lblCDDType.Text = objApproval(0).CDD_Type
                        lblCDDNasabahType.Text = objApproval(0).CDD_NasabahType_ID
                        lblRequestedBy.Text = objApproval(0).UserName
                        lblRequestedDate.Text = objApproval(0).RequestedDate.GetValueOrDefault.ToString("dd-MMM-yyyy hh:mm")
                        lblAction.Text = objApproval(0).Mode
                        SetnGetFK_SettingCDDLNP_ID = objApproval(0).FK_SettingCDDLNP_ID.GetValueOrDefault

                        'Load old mapping
                        SetnGetBindTableOld = New TList(Of MappingSettingCDDBagianQuestion)
                        For Each objOldMapping As vw_CDDLNP_Form In DataRepository.vw_CDDLNP_FormProvider.GetPaged("FK_SettingCDDLNP_ID = " & Me.SetnGetFK_SettingCDDLNP_ID, "FK_MappingSettingCDDBagian_ID  asc", 0, Int32.MaxValue, 0)
                            SetnGetBindTableOld.Add(MappingSettingCDDBagianQuestion.CreateMappingSettingCDDBagianQuestion(objOldMapping.FK_MappingSettingCDDBagian_ID, objOldMapping.PK_CDD_Question_ID))
                        Next

                        'Load new mapping
                        SetnGetBindTableNew = New TList(Of MappingSettingCDDBagianQuestion)
                        For Each objNewMapping As MappingSettingCDDBagianQuestion_ApprovalDetail In DataRepository.MappingSettingCDDBagianQuestion_ApprovalDetailProvider.GetPaged("FK_MappingSettingCDDBagianQuestion_Approval_Id = " & Me.GetPK_MappingSettingCDDBagianQuestion_Approval_Id, "FK_MappingSettingCDDBagian_ID  asc", 0, Int32.MaxValue, 0)
                            SetnGetBindTableNew.Add(MappingSettingCDDBagianQuestion.CreateMappingSettingCDDBagianQuestion(objNewMapping.FK_MappingSettingCDDBagian_ID, objNewMapping.FK_CDD_Question_ID))
                        Next

                        Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                            Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                            AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                        End Using

                    Else 'Handle jika ID tidak ditemukan
                        Me.Response.Redirect("CDDLNPMessage.aspx?ID=404", False)
                    End If
                End Using
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)

        Try
            objTransManager.BeginTransaction()

            'Delete Approval
            DataRepository.MappingSettingCDDBagianQuestion_ApprovalProvider.Delete(objTransManager, Me.GetPK_MappingSettingCDDBagianQuestion_Approval_Id)
            'Delete Approval Detail
            DataRepository.MappingSettingCDDBagianQuestion_ApprovalDetailProvider.Delete(objTransManager, DataRepository.MappingSettingCDDBagianQuestion_ApprovalDetailProvider.GetPaged("FK_MappingSettingCDDBagianQuestion_Approval_Id = " & Me.GetPK_MappingSettingCDDBagianQuestion_Approval_Id, String.Empty, 0, Int32.MaxValue, 0))

            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10022", False)
        Catch ex As Exception
            objTransManager.Rollback()
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)

        Try
            objTransManager.BeginTransaction()

            'Get PK Mapping
            Dim strPK_MappingSettingCDDBagian_ID As String = String.Empty
            For Each objSetting As MappingSettingCDDBagian In DataRepository.MappingSettingCDDBagianProvider.GetPaged(objTransManager, "FK_SettingCDDLNP_ID = " & Me.SetnGetFK_SettingCDDLNP_ID, String.Empty, 0, Int32.MaxValue, 0)
                strPK_MappingSettingCDDBagian_ID &= objSetting.PK_MappingSettingCDDBagian_ID & ","
            Next
            strPK_MappingSettingCDDBagian_ID = strPK_MappingSettingCDDBagian_ID.Remove(strPK_MappingSettingCDDBagian_ID.LastIndexOf(","), 1)

            'Delete Approval
            DataRepository.MappingSettingCDDBagianQuestion_ApprovalProvider.Delete(objTransManager, Me.GetPK_MappingSettingCDDBagianQuestion_Approval_Id)
            'Delete Approval Detail
            DataRepository.MappingSettingCDDBagianQuestion_ApprovalDetailProvider.Delete(objTransManager, DataRepository.MappingSettingCDDBagianQuestion_ApprovalDetailProvider.GetPaged("FK_MappingSettingCDDBagianQuestion_Approval_Id = " & Me.GetPK_MappingSettingCDDBagianQuestion_Approval_Id, String.Empty, 0, Int32.MaxValue, 0))
            'Delete Old Mapping
            DataRepository.MappingSettingCDDBagianQuestionProvider.Delete(objTransManager, DataRepository.MappingSettingCDDBagianQuestionProvider.GetPaged("FK_MappingSettingCDDBagian_ID IN (" & strPK_MappingSettingCDDBagian_ID & ")", String.Empty, 0, Int32.MaxValue, 0))
            'Insert New Mapping
            DataRepository.MappingSettingCDDBagianQuestionProvider.Save(objTransManager, Me.SetnGetBindTableNew)

            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10023", False)
        Catch ex As Exception
            objTransManager.Rollback()
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub

    Protected Sub GridViewCDDQuestion_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewCDDQuestionOld.ItemDataBound, GridViewCDDQuestionNew.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                Using objBagian As VList(Of vw_MappingSettingCDDBagian) = DataRepository.vw_MappingSettingCDDBagianProvider.GetPaged("PK_MappingSettingCDDBagian_ID = " & e.Item.Cells(0).Text, String.Empty, 0, Int32.MaxValue, 0)
                    If objBagian.Count > 0 Then
                        e.Item.Cells(1).Text = objBagian(0).CDDBagianName
                    End If
                End Using

                Using objQuestion As CDD_Question = DataRepository.CDD_QuestionProvider.GetByPK_CDD_Question_ID(e.Item.Cells(2).Text)
                    If objQuestion IsNot Nothing Then
                        e.Item.Cells(3).Text = objQuestion.CDD_Question
                    End If
                End Using

            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            GridViewCDDQuestionOld.DataSource = SetnGetBindTableOld.GetRange(Me.SetnGetCurrentPageOld * Sahassa.AML.Commonly.GetDisplayedTotalRow, Sahassa.AML.Commonly.GetDisplayedTotalRow)
            GridViewCDDQuestionOld.DataBind()
            Me.SetInfoNavigateOld()
            GridViewCDDQuestionNew.DataSource = SetnGetBindTableNew.GetRange(Me.SetnGetCurrentPageNew * Sahassa.AML.Commonly.GetDisplayedTotalRow, Sahassa.AML.Commonly.GetDisplayedTotalRow)
            GridViewCDDQuestionNew.DataBind()
            Me.SetInfoNavigateNew()
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

End Class