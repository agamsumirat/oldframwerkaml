#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
#End Region

Partial Class MsKepemilikan_Delete
    Inherits Parent

#Region "Function"

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = Listmaping
        LBMapping.DataTextField = "Nama"
        LBMapping.DataValueField = "IDKepemilikanNCBS"
        LBMapping.DataBind()
    End Sub


#End Region

#Region "events..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
                Listmaping = New TList(Of MappingMsKepemilikanNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim ObjMsKepemilikan As MsKepemilikan = DataRepository.MsKepemilikanProvider.GetPaged(MsKepemilikanColumn.IDKepemilikan.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsKepemilikan Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsKepemilikan
            SafeDefaultValue = "-"
            lblIDKepemilikan.Text = .IDKepemilikan
            lblNamaKepemilikan.Text = Safe(.NamaKepemilikan)

            'other info
            Dim Omsuser As TList(Of User)
            Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
            If Omsuser.Count > 0 Then
                lblCreatedBy.Text = Omsuser(0).UserName
            End If
            lblCreatedDate.Text = FormatDate(.CreatedDate)
            Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
            If Omsuser.Count > 0 Then
                lblUpdatedby.Text = Omsuser(0).UserName
            End If
            lblUpdatedDate.Text = FormatDate(.LastUpdatedDate)
            lblActivation.Text = SafeActiveInactive(.Activation)
            Dim L_objMappingMsKepemilikanNCBSPPATK As TList(Of MappingMsKepemilikanNCBSPPATK)
            L_objMappingMsKepemilikanNCBSPPATK = DataRepository.MappingMsKepemilikanNCBSPPATKProvider.GetPaged(MappingMsKepemilikanNCBSPPATKColumn.IDKepemilikan.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            Listmaping.AddRange(L_objMappingMsKepemilikanNCBSPPATK)
        End With
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

#Region "Property..."

    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsKepemilikanNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKepemilikanNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property

#End Region

#Region "events..."
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsKepemilikan_View.aspx")
    End Sub

    Protected Sub imgOk_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOk.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then

                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsKepemilikan_Approval As New MsKepemilikan_Approval
                    With ObjMsKepemilikan_Approval
                        .FK_MsMode_Id = 3
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsKepemilikan_ApprovalProvider.Save(ObjMsKepemilikan_Approval)
                    KeyHeaderApproval = ObjMsKepemilikan_Approval.PK_MsKepemilikan_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsKepemilikan_ApprovalDetail As New MsKepemilikan_ApprovalDetail()
                    With objMsKepemilikan_ApprovalDetail
                        Dim ObjMsKepemilikan As MsKepemilikan = DataRepository.MsKepemilikanProvider.GetByIDKepemilikan(parID)
                        FillOrNothing(.IDKepemilikan, lblIDKepemilikan.Text, True, oInt)
                        FillOrNothing(.NamaKepemilikan, lblNamaKepemilikan.Text, True, Ovarchar)

                        FillOrNothing(.IDKepemilikan, ObjMsKepemilikan.IDKepemilikan)
                        FillOrNothing(.Activation, ObjMsKepemilikan.Activation)
                        FillOrNothing(.CreatedDate, ObjMsKepemilikan.CreatedDate)
                        FillOrNothing(.CreatedBy, ObjMsKepemilikan.CreatedBy)
                        FillOrNothing(.FK_MsKepemilikan_Approval_Id, KeyHeaderApproval)
                    End With
                    DataRepository.MsKepemilikan_ApprovalDetailProvider.Save(objMsKepemilikan_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsKepemilikan_ApprovalDetail.PK_MsKepemilikan_ApprovalDetail_Id

                    '========= Insert mapping item 
                    Dim LobjMappingMsKepemilikanNCBSPPATK_Approval_Detail As New TList(Of MappingMsKepemilikanNCBSPPATK_Approval_Detail)

                    Dim L_ObjMappingMsKepemilikanNCBSPPATK As TList(Of MappingMsKepemilikanNCBSPPATK) = DataRepository.MappingMsKepemilikanNCBSPPATKProvider.GetPaged(MappingMsKepemilikanNCBSPPATKColumn.IDKepemilikan.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

                    For Each objMappingMsKepemilikanNCBSPPATK As MappingMsKepemilikanNCBSPPATK In L_ObjMappingMsKepemilikanNCBSPPATK
                        Dim objMappingMsKepemilikanNCBSPPATK_Approval_Detail As New MappingMsKepemilikanNCBSPPATK_Approval_Detail
                        With objMappingMsKepemilikanNCBSPPATK_Approval_Detail
                            FillOrNothing(.IDKepemilikan, objMappingMsKepemilikanNCBSPPATK.PK_MappingMsKepemilikanNCBSPPATK_Id)
                            FillOrNothing(.IDKepemilikanNCBS, objMappingMsKepemilikanNCBSPPATK.IDKepemilikanNCBS)
                            FillOrNothing(.PK_MsKepemilikan_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.PK_MappingMsKepemilikanNCBSPPATK_Id, objMappingMsKepemilikanNCBSPPATK.PK_MappingMsKepemilikanNCBSPPATK_Id)
                            .Nama = objMappingMsKepemilikanNCBSPPATK.Nama
                            LobjMappingMsKepemilikanNCBSPPATK_Approval_Detail.Add(objMappingMsKepemilikanNCBSPPATK_Approval_Detail)
                        End With
                        DataRepository.MappingMsKepemilikanNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsKepemilikanNCBSPPATK_Approval_Detail)
                    Next
                    'session Maping item Clear
                    Listmaping.Clear()
                    Me.imgOk.Visible = False
                    Me.ImageCancel.Visible = False
                    LblConfirmation.Text = "Data has been Delete and waiting for approval"
                    MtvMsUser.ActiveViewIndex = 1
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Response.Redirect("MsKepemilikan_View.aspx")
    End Sub

#End Region

End Class



