
Partial Class PrintProposalSTR
    Inherits System.Web.UI.Page

    Private ReadOnly Property PKProposalSTRID() As String
        Get
            Return Request.Params("PKProposalSTRID")
        End Get
    End Property

    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Response.Redirect("ProposalSTRDetail.aspx?PKProposalSTRID=" & Me.PKProposalSTRID, False)
    End Sub
End Class
