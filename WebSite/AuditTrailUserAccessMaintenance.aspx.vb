Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class AuditTrailUserAccessMaintenance
    Inherits Parent

    Private Property SetnGetStartDate() As DateTime
        Get
            Return Session("StartDate")
        End Get
        Set(ByVal value As DateTime)
            Session("StartDate") = value
        End Set
    End Property

    Private Property SetnGetEndDate() As DateTime
        Get
            Return Session("EndDate")
        End Get
        Set(ByVal value As DateTime)
            Session("EndDate") = value
        End Set
    End Property

    ''' <summary>
    ''' Load Data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.PopUp1.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextStartDate.ClientID & "'), 'dd-mmm-yyyy')")
                Me.PopUp1.Style.Add("display", "")
                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextEndDate.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "")

                HideAll()
                Using AccessAuditTrailUserAccess As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Me.LblTotalRecord.Text = AccessAuditTrailUserAccess.CountTotalRowAuditTrailUserAccess
                End Using

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' Hide All
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub HideAll()
        Try
            Dim OptionChoosen As String = ""
            Dim StrId As String
            If Me.RadioDate.Checked Then
                OptionChoosen = "DateChoose"
            Else
                OptionChoosen = "CountChoose"
            End If
            Select Case OptionChoosen
                Case "DateChoose"
                    StrId = "OptionD"
                Case "CountChoose"
                    StrId = "OptionC"
                Case Else
                    Throw New Exception("Type not supported type:" & OptionChoosen.ToString)
            End Select
            For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
                ObjRow.Visible = ObjRow.ID = "Button1" Or ObjRow.ID = "tampilan" Or ObjRow.ID = "netral1" Or ObjRow.ID = "netral2" Or ObjRow.ID = "tampilan3" Or ObjRow.ID = "tampilan1" Or ObjRow.ID.Substring(0, 7) = StrId
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Check Radio Date
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub RadioDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioDate.CheckedChanged
        Try
            HideAll()
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Check Radio Count
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub RadioCount_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioCount.CheckedChanged
        Try
            HideAll()
        Catch
            Throw
        End Try
    End Sub

    Public Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(1)
            Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "AuditTrailUserAccess Maintenance", "Purge", "Delete", "", "", "")
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Delete Audit Trail
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Purge_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Purge.Click
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            If Page.IsValid Then
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    Using AccessQueryAuditTrail As New AMLDAL.AMLDataSetTableAdapters.QueriesSHSMenu
                        If Me.RadioDate.Checked Then
                            If Me.CekTanggal = "Valid" Then
                                Me.TextCount.CausesValidation = False
                                AccessQueryAuditTrail.DeleteAuditTrailUserAccessByDate(Me.SetnGetStartDate, Me.SetnGetEndDate)
                                InsertAuditTrail()
                                Me.LblSuccess.Visible = True
                                Me.LblSuccess.Text = "Audit Trail User Access records from the following dates : '" & Me.TextStartDate.Text & "' to '" & Me.TextEndDate.Text & "' have been deleted."
                            End If
                        ElseIf Me.RadioCount.Checked Then
                            Me.TextEndDate.CausesValidation = False
                            Me.TextStartDate.CausesValidation = False
                            If Me.TextCount.Text > 0 Then
                                AccessQueryAuditTrail.DeleteAuditTrailUserAccessByCount(CInt(Me.TextCount.Text))
                                InsertAuditTrail()
                                Me.LblSuccess.Visible = True
                                Me.LblSuccess.Text = Me.TextCount.Text & " Audit Trail User Access records have been deleted."
                            Else
                                Throw New Exception("Enter a number greater than zero.")
                            End If
                        End If
                    End Using
                Else
                    Dim startdate, enddate As DateTime
                    Dim count As Integer
                    Dim typepurge As String = ""
                    If Me.RadioDate.Checked Then
                        If Me.CekTanggal = "Valid" Then
                            typepurge = "Date"
                            startdate = Me.SetnGetStartDate
                            enddate = Me.SetnGetEndDate
                            count = 0
                        End If
                    ElseIf Me.RadioCount.Checked Then
                        typepurge = "Count"
                        startdate = Now
                        enddate = Now

                        If Me.TextCount.Text > 0 Then
                            count = CInt(Me.TextCount.Text)
                        Else
                            Throw New Exception("Enter a number greater than zero.")
                        End If
                    End If

                    Using AccessAuditTrailUserAccessApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailUserAccessMaintenance_ApprovalTableAdapter
                        oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAuditTrailUserAccessApproval, Data.IsolationLevel.ReadUncommitted)
                        Dim pk_AuditTrailUserAccess_Maintenance_Id As Int64 = AccessAuditTrailUserAccessApproval.InsertAuditTrailUserAccessMaintenanceApproval(startdate, enddate, count)

                        Using AccessAuditTrailUserAccessPendingApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailUserAccessMaintenance_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrailUserAccessPendingApproval, oSQLTrans)
                            AccessAuditTrailUserAccessPendingApproval.Insert(typepurge, Sahassa.AML.Commonly.SessionUserId, Now, pk_AuditTrailUserAccess_Maintenance_Id)
                        End Using
                    End Using

                    oSQLTrans.Commit()

                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=8885"

                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=8885", False)
                End If
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    ''' <summary>
    ''' Cek tanggal
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CekTanggal() As String
        Try
            Dim StartDateSearch, EndDateSearch As DateTime
            Try
                StartDateSearch = DateTime.Parse(Me.TextStartDate.Text, New System.Globalization.CultureInfo("id-ID"))
                EndDateSearch = DateTime.Parse(Me.TextEndDate.Text, New System.Globalization.CultureInfo("id-ID"))
            Catch ex As ArgumentOutOfRangeException
                Throw New Sahassa.AML.CustomException(ex.Message)
            Catch ex As FormatException
                Throw New Sahassa.AML.CustomException(ex.Message)
            End Try
            Me.SetnGetStartDate = StartDateSearch.ToString("dd-MMM-yyyy 00:00")
            Me.SetnGetEndDate = EndDateSearch.ToString("dd-MMM-yyyy 23:59")

            Dim StartDate As DateTime = CDate(Me.TextStartDate.Text)
            Dim EndDate As DateTime = CDate(Me.TextEndDate.Text)

            If StartDate > EndDate Then
                Throw New Exception("Start Date must be less than End Date.")
            Else
                Return "Valid"
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function
    ''' <summary>
    ''' export to excel
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Export.Click
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(1)
            Using AcessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AcessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "AuditTrailUserAccess Maintenance", "Purge", "Export to Excel", Nothing, Nothing, Nothing)
            End Using
            Dim opt As Integer
            If Me.RadioDate.Checked Then
                If Me.CekTanggal = "Valid" Then
                    opt = 1
                End If
            Else : opt = 2
            End If
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindGrid(opt)
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=AuditTrailUserAccessMaintenanceExport.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(Me.GridAuditTrailUserAccess)
            Me.GridAuditTrailUserAccess.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' BindGrid
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindGrid(ByVal Opt As Integer)
        Try
            If Opt = 1 Then
                Using AccessSelectAuditTrailUserAccess As New AMLDAL.AMLDataSetTableAdapters.SelectAuditTrailUserAccessByDateTableAdapter
                    Using dtDate As AMLDAL.AMLDataSet.SelectAuditTrailUserAccessByDateDataTable = AccessSelectAuditTrailUserAccess.GetData(Me.SetnGetStartDate, Me.SetnGetEndDate)
                        Me.GridAuditTrailUserAccess.DataSource = dtDate
                        Me.GridAuditTrailUserAccess.AllowPaging = False
                        Me.GridAuditTrailUserAccess.DataBind()

                    End Using
                End Using
            Else
                Using AccessSelectAuditTrailUserAccess As New AMLDAL.AMLDataSetTableAdapters.SelectAuditTrailUserAccessByCountTableAdapter
                    Using dtCount As AMLDAL.AMLDataSet.SelectAuditTrailUserAccessByCountDataTable = AccessSelectAuditTrailUserAccess.GetData(CInt(Me.TextCount.Text))
                        Me.GridAuditTrailUserAccess.DataSource = dtCount
                        Me.GridAuditTrailUserAccess.AllowPaging = False
                        Me.GridAuditTrailUserAccess.DataBind()
                    End Using
                End Using
            End If
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' clear control
    ''' </summary>
    ''' <param name="control"></param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
        Return
    End Sub 'ClearControls
End Class
