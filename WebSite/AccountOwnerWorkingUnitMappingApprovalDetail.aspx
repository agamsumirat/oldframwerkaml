<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AccountOwnerWorkingUnitMappingApprovalDetail.aspx.vb" Inherits="AccountOwnerWorkingUnitMappingApprovalDetail" title="Account Owner Working Unit Mapping Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <TABLE id="TableDetail" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="2" runat="server">
	    <TR class="formText" id="UserAdd1">
		    <TD bgColor="#ffffff" height="24" style="width: 22px">&nbsp;</TD>
		    <TD bgColor="#ffffff">
                Account Owner</TD>
		    <TD bgColor="#ffffff" style="width: 44px">:</TD>
		    <TD width="80%" bgColor="#ffffff"><asp:label id="LabelAccountOwnerAdd" runat="server"></asp:label></TD>
	    </TR>
	    <tr class="formText" id="UserAdd2">
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Working Unit</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelWorkingUnitAdd" runat="server"></asp:Label></td>
	    </tr>
	    <tr class="formText" id="UserEdi1">
		    <td height="24" colspan="4" bgcolor="#ffffcc">&nbsp;Old Values</td>
		    <td colspan="4" bgcolor="#ffffcc">&nbsp;New Values</td>
	    </tr>
	    <tr class="formText" id="UserEdi2">
		    <td bgcolor="#ffffff" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Account Owner</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelAccountOwnerOld" runat="server"></asp:label></td>
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Account Owner</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelAccountOwnerNew" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserEdi3">
		    <td bgcolor="#ffffff" style="width: 22px"></td>
		    <td bgcolor="#ffffff">
                Working Unit</td>
		    <td bgcolor="#ffffff" style="width: 44px">:<br />
            </td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelWorkingUnitOld" runat="server"></asp:Label></td>
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Working Unit</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelWorkingUnitNew" runat="server"></asp:Label></td>
	    </tr>
	    <tr class="formText" id="UserDel1">
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Account Owner</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td width="80%" bgcolor="#ffffff"><asp:label id="LabelAccountOwnerDelete" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserDel2">
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Working Unit</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td width="80%" bgcolor="#ffffff">
                <asp:Label ID="LabelWorkingUnitDelete" runat="server"></asp:Label></td>
	    </tr>
	    <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="False" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="none"></asp:CustomValidator></TD>
	    </TR>
    </TABLE>
</asp:Content>