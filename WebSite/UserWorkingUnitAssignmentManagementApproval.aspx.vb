
Partial Class UserWorkingUnitAssignmentManagementApproval
    Inherits Parent

#Region " Property "
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("MsUserWorkingUnitAssignmentManagementApprovalViewSelected") Is Nothing, New ArrayList, Session("MsUserWorkingUnitAssignmentManagementApprovalViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("MsUserWorkingUnitAssignmentManagementApprovalViewSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("MsUserWorkingUnitAssignmentManagementApprovalViewFieldSearch") Is Nothing, "", Session("MsUserWorkingUnitAssignmentManagementApprovalViewFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsUserWorkingUnitAssignmentManagementApprovalViewFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("MsUserWorkingUnitAssignmentManagementApprovalViewValueSearch") Is Nothing, "", Session("MsUserWorkingUnitAssignmentManagementApprovalViewValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsUserWorkingUnitAssignmentManagementApprovalViewValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("MsUserWorkingUnitAssignmentManagementApprovalViewSort") Is Nothing, "Parameters_PendingApprovalUserID  asc", Session("MsUserWorkingUnitAssignmentManagementApprovalViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("MsUserWorkingUnitAssignmentManagementApprovalViewSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("MsUserWorkingUnitAssignmentManagementApprovalViewCurrentPage") Is Nothing, 0, Session("MsUserWorkingUnitAssignmentManagementApprovalViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsUserWorkingUnitAssignmentManagementApprovalViewCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("MsUserWorkingUnitAssignmentManagementApprovalViewRowTotal") Is Nothing, 0, Session("MsUserWorkingUnitAssignmentManagementApprovalViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsUserWorkingUnitAssignmentManagementApprovalViewRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("MsUserWorkingUnitAssignmentManagementApprovalViewData") Is Nothing, New AMLDAL.AMLDataSet.SelectFilterParameterPendingApprovalUserAssignmentDataTable, Session("MsUserWorkingUnitAssignmentManagementApprovalViewData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsUserWorkingUnitAssignmentManagementApprovalViewData") = value
        End Set
    End Property

    Private Property SetnGetUserID() As ArrayList
        Get
            Return IIf(Session("UserIDWorkingUnit") Is Nothing, New ArrayList, Session("UserIDWorkingUnit"))
        End Get
        Set(ByVal value As ArrayList)
            Session("UserIDWorkingUnit") = value
        End Set
    End Property

    ''' <summary>
    ''' get id for focues
    ''' </summary>
    ''' <value></value>
    ''' <returns>element control id</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TextSearch.ClientID
        End Get
    End Property

  
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' fill search
    ''' </summary>
    ''' <remarks>
    ''' ParametersName
    ''' Parameters_PendingApprovalUserID
    ''' Parameters_PendingApprovalEntryDate
    ''' Parameters_PendingApprovalModeID
    ''' </remarks>
    ''' <history>
    ''' Modification of similar function created on 05/06/2006 by [Ariwibawa]
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Created By", " Parameters_PendingApprovalUserID Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("UserID", " UserID Like '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid edit command handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    '''     [Hendry]    14/05/2007 Modified 
    ''' </history>
    ''' -----------------------------------------------------------------------------

    Private Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Try
            Dim TypeConfirm As String = ""

            Using AccessParameterApp As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignment_ApprovalTableAdapter
                Dim ApprovalID As Int32
                Using dt As AMLDAL.AMLDataSet.UserWorkingUnitAssignment_ApprovalDataTable = AccessParameterApp.GetAllDataByUserId(e.Item.Cells(2).Text)
                    If dt.Rows.Count > 0 Then
                        For Each row As AMLDAL.AMLDataSet.UserWorkingUnitAssignment_ApprovalRow In dt.Rows
                            ApprovalID = CInt(row.ApprovalID)
                        Next
                        Using AccessParameterApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                            Using dtApproval As Data.DataTable = AccessParameterApproval.GetData
                                Dim row() As AMLDAL.AMLDataSet.Parameters_ApprovalRow = dtApproval.Select("ParameterItemApprovalID =" & ApprovalID & "")
                                TypeConfirm = row(0).ModeID
                            End Using
                        End Using
                    End If
                End Using
            End Using

            Dim URL As String = ""
            URL = "UserWorkingUnitAssignmentApprovalDetail.aspx?TypeConfirm=" & TypeConfirm & "&UserId=" & e.Item.Cells(2).Text & "&CreatedBy=" & e.Item.Cells(1).Text

            Sahassa.AML.Commonly.SessionIntendedPage = URL

            MagicAjax.AjaxCallHelper.Redirect(URL)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("MsUserWorkingUnitAssignmentManagementApprovalViewSelected") = Nothing
        Session("MsUserWorkingUnitAssignmentManagementApprovalViewFieldSearch") = Nothing
        Session("MsUserWorkingUnitAssignmentManagementApprovalViewValueSearch") = Nothing
        Session("MsUserWorkingUnitAssignmentManagementApprovalViewSort") = Nothing
        Session("MsUserWorkingUnitAssignmentManagementApprovalViewCurrentPage") = Nothing
        Session("MsUserWorkingUnitAssignmentManagementApprovalViewRowTotal") = Nothing
        Session("MsUserWorkingUnitAssignmentManagementApprovalViewData") = Nothing
        Session("UserIDWorkingUnit") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' onclick="popUpCalendar(this, document.getElementById('TextSearch'), 'dd-mmm-yyyy')" 
    ''' </remarks>
    ''' <history>
    ''' Modification of similar function created on 05/06/2006 by [Ariwibawa]
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()
                Me.ComboSearch.Attributes.Add("onchange", "javascript:CheckNoPopUp('" & Me.TextSearch.ClientID & "');")

                'Me.ComboSearch.Attributes.Add("onchange", "javascript:Check(this, 2, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "');")
                'Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                'Me.popUp.Style.Add("display", "none")

                Using AccessUserWorkingUnitParametersApproval As New AMLDAL.AMLDataSetTableAdapters.SelectFilterParameterPendingApprovalUserAssignmentTableAdapter                    
                    Me.SetnGetBindTable = AccessUserWorkingUnitParametersApproval.GetData(Sahassa.AML.Commonly.SessionUserId, 8)
                End Using
                Me.FillSearch()
                Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim CreatedBy As String = gridRow.Cells(1).Text
                Dim UserId As String = gridRow.Cells(2).Text
                Dim ArrCreatedBy As ArrayList = Me.SetnGetSelectedItem
                Dim ArrUserId As ArrayList = Me.SetnGetUserID


                If ArrCreatedBy.Contains(CreatedBy) And ArrUserId.Contains(UserId) Then
                    If Not chkBox.Checked Then
                        ArrCreatedBy.Remove(CreatedBy)
                        ArrUserId.Remove(UserId)
                    End If
                Else
                    If chkBox.Checked Then
                        ArrCreatedBy.Add(CreatedBy)
                        ArrUserId.Add(UserId)
                    End If
                End If



                'If ArrCreatedBy.Contains(CreatedBy) Then
                '    If Not chkBox.Checked Then
                '        ArrCreatedBy.Remove(CreatedBy)
                '    End If
                'Else
                '    If chkBox.Checked Then ArrCreatedBy.Add(CreatedBy)
                'End If

                'If ArrUserId.Contains(UserId) Then
                '    If Not chkBox.Checked Then
                '        ArrUserId.Remove(UserId)
                '    End If
                'Else
                '    If chkBox.Checked Then ArrUserId.Add(UserId)
                'End If

                Me.SetnGetSelectedItem = ArrCreatedBy
                Me.SetnGetUserID = ArrUserId
            End If
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Dim Rows() As AMLDAL.AMLDataSet.SelectFilterParameterPendingApprovalUserAssignmentRow = Me.SetnGetBindTable.Select(Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), Me.SetnGetSort)
        Me.GridMSUserView.DataSource = Rows
        Me.SetnGetRowTotal = Rows.Length
        Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridMSUserView.DataBind()
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
                Me.SetnGetValueSearch = Me.TextSearch.Text
            ElseIf Me.ComboSearch.SelectedIndex = 1 Then
                'Dim DateSearch As DateTime
                'Try
                '    DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
                'Catch ex As ArgumentOutOfRangeException
                '    Throw New Exception("Input character cannot be convert to datetime.")
                'Catch ex As FormatException
                '    Throw New Exception("Unknown format date")
                'End Try
                'Me.SetnGetValueSearch = DateSearch.ToString("dd-MMM-yyyy")                
                Me.SetnGetValueSearch = Me.TextSearch.Text
            Else
                Me.SetnGetValueSearch = Me.TextSearch.Text
            End If

            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text) And Me.SetnGetUserID.Contains(e.Item.Cells(2).Text)
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'Me.SetnGetSelectedItem.Clear()
        'For Each gridRow As DataGridItem In Me.GridMSUserView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim CreatedBy As String = gridRow.Cells(1).Text
        '        Dim UserID As String = gridRow.Cells(2).Text
        '        Dim ArrCreated As ArrayList = Me.SetnGetSelectedItem
        '        Dim ArrUserID As ArrayList = Me.SetnGetUserID

        '        If Me.SetnGetSelectedItem.Contains(CreatedBy) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrCreated.Add(CreatedBy)
        '            Else
        '                ArrCreated.Remove(CreatedBy)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrCreated.Add(CreatedBy)
        '            End If
        '        End If

        '        If Me.SetnGetUserID.Contains(UserID) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrUserID.Add(UserID)
        '            Else
        '                ArrUserID.Remove(UserID)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrUserID.Add(UserID)
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrCreated
        '        Me.SetnGetUserID = ArrUserID
        '    End If
        'Next

        Dim ArrCreated As ArrayList = Me.SetnGetSelectedItem
        Dim ArrUserID As ArrayList = Me.SetnGetUserID
        For Each gridRow As DataGridItem In GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim CreatedBy As String = gridRow.Cells(1).Text
                Dim UserID As String = gridRow.Cells(2).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(CreatedBy) = False Then
                        ArrCreated.Add(CreatedBy)
                    End If
                    If Me.SetnGetUserID.Contains(UserID) = False Then
                        ArrUserID.Add(UserID)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(CreatedBy) Then
                        ArrCreated.Remove(CreatedBy)
                    End If
                    If Me.SetnGetUserID.Contains(UserID) Then
                        ArrUserID.Remove(UserID)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrCreated
        Me.SetnGetUserID = ArrUserID
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()
        Dim Rows As New ArrayList
        For Each IdPk As Int64 In Me.SetnGetSelectedItem
            Dim rowData() As AMLDAL.AMLDataSet.Parameters_PendingApprovalRow = Me.SetnGetBindTable.Select("Parameters_PendingApprovalID = " & IdPk)
            If rowData.Length > 0 Then
                Rows.Add(rowData(0))
            End If
        Next
        Me.GridMSUserView.DataSource = Rows
        Me.GridMSUserView.AllowPaging = False
        Me.GridMSUserView.DataBind()

        'Sembunyikan kolom ke 0, 1, 6 & 7 agar tidak ikut diekspor ke excel
        Me.GridMSUserView.Columns(0).Visible = False
        Me.GridMSUserView.Columns(1).Visible = False
        Me.GridMSUserView.Columns(6).Visible = False
        Me.GridMSUserView.Columns(7).Visible = False
    End Sub

    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' Export to Excel
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Me.ExportToExcel()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Sub ExportToExcel()
        Dim range As String = ""
        Dim xlApp As Excel.ApplicationClass
        Dim xlWorkbooks As Excel.Workbooks
        Dim xlWorkbook As Excel.Workbook
        Dim xlWorkSheets As Excel.Sheets
        Dim xlWorkSheet As Excel.Worksheet
        Dim xlRange As Excel.Range
        Dim xlCells As Excel.Range
        Dim xlFont As Excel.Font

        Try
            xlApp = New Excel.ApplicationClass
            xlApp.DisplayAlerts = False
            xlApp.UserControl = False
            xlApp.Visible = False
            xlWorkbooks = xlApp.Workbooks
            xlWorkbook = xlWorkbooks.Add
            xlWorkSheets = xlWorkbook.Worksheets

            Dim ArrCollectVerificationList As ArrayList = Me.SetnGetSelectedItem


            ' ID Number
            Try
                xlWorkSheet = xlWorkSheets.Add
                xlWorkSheet.Name = "UserWorkingUnitApproval"
                xlCells = xlWorkSheet.Cells

                range = "A1:F1"
                xlRange = xlWorkSheet.Range(range)
                xlRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                xlRange.Interior.Color = RGB(192, 0, 0)
                xlFont = xlRange.Font
                xlFont.Color = RGB(255, 255, 255)
                xlRange.Merge()
                xlRange.Value = "User Working Unit Assignment Approval"
                NAR(xlFont)
                NAR(xlRange)

                ' Create Header Alias
                Dim HeaderUserWorkingUnit() As String = {"CreateBy", "UserID", "Working Unit Name", "Created Date", "New Last Updated Date", "Old Last Updated Date"}
                For i As Integer = 0 To 5
                    xlCells = xlWorkSheet.Cells
                    xlRange = xlCells(2, i + 1)
                    xlRange.Value = HeaderUserWorkingUnit(i)
                    xlFont = xlRange.Font
                    xlFont.Bold = True
                    NAR(xlFont)
                    NAR(xlRange)
                Next

                Dim IntCounter As Integer = 3
                Dim ArrCreatedBy As ArrayList = Me.SetnGetSelectedItem
                Dim ArrUserID As ArrayList = Me.SetnGetUserID
                Using AccessUserWorkingUnit As New AMLDAL.UserWorkingUnitAssignmentTableAdapters.SelectFilterUWUA_ForExcelTableAdapter
                    For i As Integer = 0 To Me.SetnGetSelectedItem.Count - 1
                        Using dt As Data.DataTable = AccessUserWorkingUnit.GetData(ArrUserID(i), ArrCreatedBy(i))
                            If dt.Rows.Count > 0 Then
                                For Each Row As AMLDAL.UserWorkingUnitAssignment.SelectFilterUWUA_ForExcelRow In dt.Rows
                                    Dim StrValueUserWorkingUnitApproval() As String = {Row.CreatedBy, Row.UserID, Row.New_WorkingUnitName, Row.CreatedDate, Row.Old_WorkingUnitName, Row.OLd_LastUpdateDate}
                                    For j As Integer = 0 To 5
                                        xlCells = xlWorkSheet.Cells
                                        xlRange = xlCells(IntCounter, j + 1)
                                        xlRange.Value = StrValueUserWorkingUnitApproval(j)
                                        NAR(xlRange)
                                    Next
                                    IntCounter += 1
                                Next
                            End If
                        End Using
                    Next
                End Using
                xlWorkSheet.Columns.AutoFit()
            Catch
                Throw
            Finally
                NAR(xlWorkSheet)
            End Try
            xlWorkbook.SaveAs(Me.Server.MapPath("Temp") & "\" & Sahassa.AML.Commonly.SessionUserId & "UserWorkingUnitAssignmentApproval.xls")

            NAR(xlRange)
            NAR(xlWorkSheet)
            NAR(xlWorkbook)
            NAR(xlWorkbooks)
            If Not (xlApp Is Nothing) Then
                xlApp.Quit()
            End If
            NAR(xlApp)
            'GC cleanup 
            System.GC.Collect()
            System.GC.WaitForPendingFinalizers()
            Me.Response.Redirect("temp/" & Sahassa.AML.Commonly.SessionUserId & "UserWorkingUnitAssignmentApproval.xls", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            NAR(xlRange)
            NAR(xlWorkSheet)
            NAR(xlWorkbook)
            NAR(xlWorkbooks)
            If Not (xlApp Is Nothing) Then
                xlApp.Quit()
            End If
            NAR(xlApp)
            'GC cleanup 
            System.GC.Collect()
            System.GC.WaitForPendingFinalizers()
        End Try
    End Sub

    ''' <summary>
    ''' NAR
    ''' </summary>
    ''' <param name="obj"></param>
    ''' <remarks></remarks>
    Private Sub NAR(ByRef obj As Object)
        Dim intReferenceCount As Integer
        If Not obj Is Nothing Then
            Try
                Do
                    intReferenceCount = System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
                Loop While intReferenceCount > 0
            Catch
            Finally
                obj = Nothing
            End Try
        End If
    End Sub
End Class
