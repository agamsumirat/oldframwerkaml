Imports System.Windows.Forms
Imports System.Data
Imports System.Data.SqlClient
Imports Sahassa.AML.TableAdapterHelper
Imports SahassaNettier.Data

Partial Class NewsDelete
    Inherits Parent

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetNewsId() As String
        Get
            Return Me.Request.Params("NewsID")
        End Get
    End Property


    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "NewsView.aspx"

        Me.Response.Redirect("NewsView.aspx", False)
    End Sub

#Region "Delete News By SU dan Audit Trail"

    ''' <summary>
    '''  insert news
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DeleteNewsBySU()
        Try
            Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
                AccessNews.DeleteNews(CInt(Me.GetNewsId))
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' audit trail
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertAuditTrail()
        Try
            Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
                Using dt As Data.DataTable = AccessNews.GetDataByNewsID(CInt(Me.GetNewsId))
                    Dim RowNews As AMLDAL.AMLDataSet.NewsRow = dt.Rows(0)
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(7)
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsID", "Delete", RowNews.NewsID, RowNews.NewsID, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsTitle", "Delete", "", RowNews.NewsTitle, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsSummary", "Delete", "", RowNews.NewsSummary, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsContent", "Delete", "", RowNews.NewsContent, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsStartDate", "Delete", "", RowNews.NewsStartDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsEndDate", "Delete", "", RowNews.NewsEndDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsCreatedDate", "Delete", "", RowNews.NewsCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                    End Using
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageUpdate.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                'Buat variabel untuk menampung nilai-nilai baru
                Dim NewsID As String = Me.LabelNewsID.Text
                Dim NewsTitle As String = Me.lblTextNewsTitle.Text
                Dim NewsSummary As String = Me.lblTextNewsSummary.Text
                Dim NewsContent As String = Me.lblTextNewsContent.Text
                Dim NewsStartDate As DateTime = CDate(Me.lblTextStartDate.Text)
                Dim NewsEndDate As DateTime = CDate(Me.lblTextEndDate.Text)

                'Periksa apakah NewsTitle tsb sdh ada dalam tabel Group atau belum
                Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
                    Dim counter As Int32 = AccessLevelType.CountMatchingNews(NewsTitle)

                    'Counter > 0 berarti NewsTitle tersebut masih ada dalam tabel News dan bisa didelete
                    If counter > 0 Then
                        'Periksa apakah NewsTitle tsb dalam status pending approval atau tidak
                        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.News_ApprovalTableAdapter
                            oSQLTrans = BeginTransaction(AccessPending, IsolationLevel.ReadUncommitted)
                            counter = AccessPending.CountNewsApproval(NewsID)

                            'Counter = 0 berarti NewsTitle tersebut tidak dalam status pending approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertAuditTrail()
                                    Me.DeleteNewsBySU()
                                    Me.LblSuccess.Visible = True
                                    Me.LblSuccess.Text = "Success to Delete News."
                                Else
                                    'Buat variabel untuk menampung nilai-nilai lama
                                    Dim NewsID_Old As Int64 = ViewState("NewsID_Old")
                                    Dim NewsTitle_Old As String = ViewState("NewsTitle_Old")
                                    Dim NewsSummary_Old As String = ViewState("NewsSummary_Old")
                                    Dim NewsContent_Old As String = ViewState("NewsContent_Old")
                                    Dim NewsStartDate_Old As DateTime = ViewState("NewsStartDate_Old")
                                    Dim NewsEndDate_Old As DateTime = ViewState("NewsEndDate_Old")
                                    Dim NewsCreatedDate_Old As DateTime = ViewState("NewsCreatedDate_Old")

                                    'variabel untuk menampung nilai identity dari tabel NewsPendingApprovalID
                                    Dim NewsPendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope()
                                    Using AccessNewsPendingApproval As New AMLDAL.AMLDataSetTableAdapters.News_PendingApprovalTableAdapter
                                        SetTransaction(AccessNewsPendingApproval, oSQLTrans)
                                        'Tambahkan ke dalam tabel News_PendingApproval dengan ModeID = 3 (Delete) 
                                        NewsPendingApprovalID = AccessNewsPendingApproval.InsertNews_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "News Delete", 3)
                                        'Tambahkan ke dalam tabel News_Approval dengan ModeID = 3 (Delete) 
                                        AccessPending.Insert(NewsPendingApprovalID, 3, NewsID, NewsTitle, NewsSummary, NewsContent, NewsStartDate, NewsEndDate, Now, NewsID_Old, NewsTitle_Old, NewsSummary_Old, NewsContent_Old, NewsStartDate_Old, NewsStartDate_Old, NewsCreatedDate_Old)

                                        oSQLTrans.Commit()
                                        'TransScope.Complete()
                                    End Using
                                    'End Using

                                    'Penambahan Attach
                                    Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                                        adapter.UpdateNewsAttachmentFk_NewsApprovalIdbyUserId(Sahassa.AML.Commonly.SessionPkUserId, NewsPendingApprovalID)
                                    End Using
                                    '------------------

                                    Dim MessagePendingID As Integer = 8103 'MessagePendingID 8103 = News Delete 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & (Regex.Replace(NewsTitle, "<.*?>", "")).Trim

                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & (Regex.Replace(NewsTitle, "<.*?>", "")).Trim, False)
                                End If
                            Else
                                Throw New Exception("Cannot delete the following News: '" & NewsTitle & "' because it is currently waiting for approval.")
                            End If
                        End Using
                    Else 'Counter = 0 berarti NewsTitle tersebut tidak ada dalam tabel News
                        Throw New Exception("Cannot delete the following News: '" & NewsTitle & "' because that News Title does not exist in the database anymore.")
                    End If
                End Using
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
            Using TableNews As AMLDAL.AMLDataSet.NewsDataTable = AccessNews.GetDataByNewsID(Me.GetNewsId)
                If TableNews.Rows.Count > 0 Then
                    Dim TableRowNews As AMLDAL.AMLDataSet.NewsRow = TableNews.Rows(0)

                    ViewState("NewsID_Old") = TableRowNews.NewsID
                    Me.LabelNewsID.Text = ViewState("NewsID_Old")

                    ViewState("NewsTitle_Old") = TableRowNews.NewsTitle
                    Me.lblTextNewsTitle.Text = ViewState("NewsTitle_Old")

                    ViewState("NewsSummary_Old") = TableRowNews.NewsSummary
                    Me.lblTextNewsSummary.Text = ViewState("NewsSummary_Old")

                    ViewState("NewsContent_Old") = TableRowNews.NewsContent
                    Me.lblTextNewsContent.Text = ViewState("NewsContent_Old")

                    ViewState("NewsStartDate_Old") = CType(TableRowNews.NewsStartDate, Date)
                    Me.lblTextStartDate.Text = String.Format("{0:dd-MMMM-yyyy}", ViewState("NewsStartDate_Old"))

                    ViewState("NewsEndDate_Old") = CType(TableRowNews.NewsEndDate, Date)
                    Me.lblTextEndDate.Text = String.Format("{0:dd-MMMM-yyyy}", ViewState("NewsEndDate_Old"))

                    ViewState("NewsCreatedDate_Old") = TableRowNews.NewsCreatedDate

                    'attach
                    Session("FirstLoad.NewsEdit") = 1
                    Using objcmd As New System.Data.SqlClient.SqlCommand("INSERTNewsAttachmentbyFK_NewsIDforEdit")
                        objcmd.CommandType = System.Data.CommandType.StoredProcedure
                        objcmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings("SQLCommandTimeout")
                        objcmd.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FK_NewsID", TableRowNews.NewsID))
                        objcmd.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FK_UserID", Sahassa.AML.Commonly.SessionPkUserId))
                        DataRepository.Provider.ExecuteScalar(objcmd)
                    End Using
                    Session("FirstLoad.NewsEdit") = Nothing
                    GridOldAttach.DataSource = oRowOldNewsAttach
                    GridOldAttach.DataBind()
                End If
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then

                Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                    adapter.DeleteNewsAttachmentbyUserId(Sahassa.AML.Commonly.SessionPkUserId)
                End Using

                Me.FillEditData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub GridOldAttach_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridOldAttach.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim PK As Integer = GridOldAttach.DataKeys(e.Row.RowIndex).Value
            Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                CType(e.Row.FindControl("Datalist1"), System.Web.UI.WebControls.DataList).DataSource = adapter.GetNewsAttachmentByPK(PK)
                CType(e.Row.FindControl("Datalist1"), System.Web.UI.WebControls.DataList).DataBind()
            End Using
        End If
    End Sub
    Private _oRowOldNewsAttach As AMLDAL.AMLDataSet.NewsAttachmentDataTable
    Private ReadOnly Property oRowOldNewsAttach() As AMLDAL.AMLDataSet.NewsAttachmentDataTable
        Get
            If Not _oRowOldNewsAttach Is Nothing Then
                Return _oRowOldNewsAttach
            Else
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                    _oRowOldNewsAttach = adapter.GetNewsAttachmentByUserIdCreateFirst(Sahassa.AML.Commonly.SessionPkUserId)
                    '_oRowOldNewsAttach = adapter.GetByFk_NewsID(LabelNewsID.Text)
                    Return _oRowOldNewsAttach
                End Using
            End If

        End Get
    End Property
    Protected Function GenerateLink(ByVal PK_NewsAttachment As Integer, ByVal Filename As String) As String
        Return String.Format("<a href=""{0}"">{1}</a> ", ResolveUrl("~\NewsDownloadAttach.aspx?PK_NewsAttachment=" & PK_NewsAttachment), Filename)
    End Function
End Class