Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Imports System.IO
Imports System.Collections.Generic

Partial Class MappingUserIdHiportJiveAdd
    Inherits Parent

    Public Property SetnGetMode() As String
        Get
            If Not Session("MappingUserIdHIPORTJIVEAdd.Mode") Is Nothing Then
                Return CStr(Session("MappingUserIdHIPORTJIVEAdd.Mode"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingUserIdHIPORTJIVEAdd.Mode") = value
        End Set
    End Property

    Private Sub LoadMode()
        Session("MappingUserIdHIPORTJIVEAdd.Mode") = Nothing
        'CboUserId.Items.Clear()

        'CboUserId.AppendDataBoundItems = True
        'CboUserId.DataSource = MappingUserIdHiportJiveBLL.GeModeData
        'CboUserId.DataTextField = UserColumn.UserName.ToString
        'CboUserId.DataValueField = UserColumn.UserID.ToString
        'CboUserId.DataBind()
        'CboUserId.Items.Insert(0, New ListItem("...", ""))

    End Sub

    Sub clearData()
        TxtKodeAO.Text = ""
        LblUserID.Text = "Click Browse Button To Choose User"
        LabelUserName.Text = ""
        HUserID.Value = ""
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oTrans As SqlTransaction = Nothing
        Try
            Dim UserName As String
            UserName = LblUserID.Text
            Dim UserId As Integer = 0
            Dim ObjCheckUser As TList(Of User) = DataRepository.UserProvider.GetPaged("UserID = '" & UserName & "'", "", 0, Integer.MaxValue, 0)
            If ObjCheckUser.Count > 0 Then
                UserId = CInt(ObjCheckUser(0).pkUserID)
            End If
          
            If HUserID.Value <> "" Then

                AMLBLL.MappingGroupMenuHiportJiveBLL.IsDataValidAddApproval(CStr(UserId))

                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    Using ObjMappingUserIdHIPORTJIVE As New MappingUserIdHiportJive
                        Using Objvw_MappingUserIdHIPORTJIVEList As VList(Of vw_MappingUserIdHiportJive) = DataRepository.vw_MappingUserIdHiportJiveProvider.GetPaged("FK_User_ID = '" & UserId & "'", "", 0, Integer.MaxValue, 0)
                            If Objvw_MappingUserIdHIPORTJIVEList.Count > 0 Then
                                'Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEView.aspx"
                                'Me.Response.Redirect("MappingUserIdHIPORTJIVEView.aspx", False)

                                Me.LblSuccess.Text = "Data " & Objvw_MappingUserIdHIPORTJIVEList(0).UserName & " already exists"
                                Me.LblSuccess.Visible = True
                                LoadMode()
                            Else
                                ObjMappingUserIdHIPORTJIVE.FK_User_ID = UserId
                                ObjMappingUserIdHIPORTJIVE.Kode_AO = TxtKodeAO.Text
                                AMLBLL.MappingUserIdHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingUserIdHIPORTJIVE", "UserName", "Add", "", UserId.ToString, "Acc")
                                AMLBLL.MappingUserIdHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingUserIdHIPORTJIVE", "Kode_AO", "Add", "", TxtKodeAO.Text, "Acc")
                                DataRepository.MappingUserIdHiportJiveProvider.Save(ObjMappingUserIdHIPORTJIVE)
                                'Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEView.aspx"
                                'Me.Response.Redirect("MappingUserIdHIPORTJIVEView.aspx", False)
                                Using ObjUser As TList(Of User) = DataRepository.UserProvider.GetPaged("PKUserID = '" & UserId & "'", "", 0, Integer.MaxValue, 0)
                                    Me.LblSuccess.Text = "Data saved successfully"
                                    Me.LblSuccess.Visible = True
                                    LoadMode()
                                    clearData()
                                End Using
                            End If
                        End Using
                    End Using
                Else
                    Using ObjMappingUserIdHIPORTJIVE_Approval As New MappingUserIdHiportJive_Approval

                        Using objUser As TList(Of User) = DataRepository.UserProvider.GetPaged("PKUserID = '" & UserId & "'", "", 0, Integer.MaxValue, 0)
                            UserName = objUser(0).UserName
                            AMLBLL.MappingUserIdHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingUserIdHIPORTJIVE", "UserName", "Add", "", UserName, "PendingApproval")
                            AMLBLL.MappingUserIdHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingUserIdHIPORTJIVE", "Kode_AO", "Add", "", TxtKodeAO.Text, "PendingApproval")
                            ObjMappingUserIdHIPORTJIVE_Approval.FK_MsUserID = Sahassa.AML.Commonly.SessionPkUserId
                            ObjMappingUserIdHIPORTJIVE_Approval.UserName = UserName
                            ObjMappingUserIdHIPORTJIVE_Approval.FK_ModeID = CInt(Sahassa.AML.Commonly.TypeMode.Add)
                            ObjMappingUserIdHIPORTJIVE_Approval.CreatedDate = Now
                            DataRepository.MappingUserIdHiportJive_ApprovalProvider.Save(ObjMappingUserIdHIPORTJIVE_Approval)

                            Using ObjMappingUserIdHIPORTJIVE_approvalDetail As New MappingUserIdHiportJive_ApprovalDetail
                                ObjMappingUserIdHIPORTJIVE_approvalDetail.FK_MappingUserIdHiportJive_Approval_ID = ObjMappingUserIdHIPORTJIVE_Approval.PK_MappingUserIdHiportJive_Approval_ID
                                ObjMappingUserIdHIPORTJIVE_approvalDetail.FK_User_ID = UserId
                                ObjMappingUserIdHIPORTJIVE_approvalDetail.Kode_AO = TxtKodeAO.Text
                                DataRepository.MappingUserIdHiportJive_ApprovalDetailProvider.Save(ObjMappingUserIdHIPORTJIVE_approvalDetail)
                            End Using
                        End Using
                        'Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEView.aspx"
                        'Me.Response.Redirect("MappingUserIdHIPORTJIVEView.aspx", False)
                        Using ObjUser As TList(Of User) = DataRepository.UserProvider.GetPaged("PKUserID = '" & UserId & "'", "", 0, Integer.MaxValue, 0)
                            Me.LblSuccess.Text = "Data saved successfully and waiting for Approval."
                            Me.LblSuccess.Visible = True
                            LoadMode()
                            clearData()
                        End Using
                    End Using
                End If
            Else
                Me.LblSuccess.Text = "Please select User"
                Me.LblSuccess.Visible = True
            End If
                
        Catch ex As Exception
            If Not oTrans Is Nothing Then oTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
                oTrans = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEView.aspx"
            Me.Response.Redirect("MappingUserIdHIPORTJIVEView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub SelectUser(ByVal IntUserid As Long)
        HUserID.Value = IntUserid.ToString
        Using objUser As SahassaNettier.Entities.User = UserBLL.GetUserByPkUserID(IntUserid)
            If Not objUser Is Nothing Then
                LblUserID.Text = objUser.UserID
                LabelUserName.Text = objUser.UserName
            Else
                LblUserID.Text = ""
                LabelUserName.Text = ""
            End If
        End Using

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                '    Transcope.Complete()
            End Using
            AddHandler PopUpUser1.SelectUser, AddressOf SelectUser
            If Not Page.IsPostBack Then
                LoadMode()
            End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub btnBrowse_Click(sender As Object, e As System.EventArgs) Handles btnBrowse.Click
        Try

            Page.ClientScript.RegisterStartupScript(Me.GetType(), "idpopup2", "popUpDrag('" & CType(sender, LinkButton).ClientID & "','divBrowseUser');", True)
            PopUpUser1.initData()

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try

            If Not Page.ClientScript.IsClientScriptIncludeRegistered("idpopup1") Then
                Page.ClientScript.RegisterClientScriptInclude(Me.GetType(), "idpopup1", ResolveClientUrl("script/popupdrag.js"))
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


End Class


