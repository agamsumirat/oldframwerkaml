﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports System.Data

Partial Class CDDLNPHeaderEdit
    Inherits Parent

    Private ReadOnly Property GetPK_CDD_Bagian_ID() As Int32
        Get
            Return Request.QueryString("ID")
        End Get
    End Property

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageError.PreRender
        If cvalPageError.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                Using objCDD_Bagian As CDD_Bagian = DataRepository.CDD_BagianProvider.GetByPK_CDD_Bagian_ID(Me.GetPK_CDD_Bagian_ID)
                    If objCDD_Bagian IsNot Nothing Then
                        txtCDDBagianName.Text = objCDD_Bagian.CDDBagianName

                        Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                            Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                            AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                        End Using

                    Else 'Handle jika ID tidak ditemukan
                        Me.Response.Redirect("CDDLNPHeaderView.aspx", False)
                    End If
                End Using
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim objApproval As New CDD_Bagian_Approval
        Dim objApproval_Detail As New CDD_Bagian_ApprovalDetail

        Try
            objTransManager.BeginTransaction()

            If txtCDDBagianName.Text.Trim = String.Empty Then
                Throw New Exception("Header Description is required")
            End If

            If DataRepository.CDD_Bagian_ApprovalDetailProvider.GetTotalItems("PK_CDD_Bagian_id=" & Me.GetPK_CDD_Bagian_ID, 0) > 0 Then
                Throw New Exception("CDD LNP Header for ID " & Me.GetPK_CDD_Bagian_ID & " is currently waiting for approval.")
            End If

            objApproval.RequestedBy = Sahassa.AML.Commonly.SessionPkUserId
            objApproval.RequestedDate = Date.Now
            objApproval.FK_MsMode_Id = 2
            DataRepository.CDD_Bagian_ApprovalProvider.Save(objTransManager, objApproval)


            objApproval_Detail.FK_CDD_Bagian_Approval_Id = objApproval.PK_CDD_Bagian_Approval_Id
            objApproval_Detail.PK_CDD_Bagian_ID = Me.GetPK_CDD_Bagian_ID
            objApproval_Detail.CDDBagianName = txtCDDBagianName.Text.Trim
            DataRepository.CDD_Bagian_ApprovalDetailProvider.Save(objTransManager, objApproval_Detail)


            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10521", False)
        Catch ex As Exception
            objTransManager.Rollback()
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
            If Not objApproval Is Nothing Then
                objApproval.Dispose()
                objApproval = Nothing
            End If
            If Not objApproval_Detail Is Nothing Then
                objApproval_Detail.Dispose()
                objApproval_Detail = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("CDDLNPHeaderView.aspx")
    End Sub
End Class