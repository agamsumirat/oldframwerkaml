<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SignOut.aspx.vb" Inherits="SignOut" title="Sign Out" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>:: Anti Money Laundering ::</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="theme/aml.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="0" bgColor="#ffffff" >
    <form id="form1" runat="server">
        <table style="height:100%;width:100%" id="tablebasic" cellSpacing="0" cellPadding="0" border="0">
            <tr>
                <td background="images/validationbground.gif">
                <ajax:AjaxPanel ID="a" runat="server" Width="99%">
                    <asp:validationsummary id="ValidationSummary1" runat="server" CssClass="validation" Width="100%" HeaderText="There were errors on the page:"></asp:validationsummary>
                </ajax:AjaxPanel>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="Table2" bgcolor="#dddddd" border="0" bordercolor="#ffffff" cellpadding="0"
                        cellspacing="0" style="position: static; width: 100%; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                        <tr>
                      <td bgcolor="#ffffff"><img src="images/front-titleV2.gif" width="417" height="152"></td>
                        </tr>
                        <tr>
                      <td height="130"><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	  	                <tr>
			                <td width="147" style="height: 130px"><img src="images/front-uang-dijemur.jpg" width="147" height="130"></td>
			                <td width="99%" style="height: 130px;"><img src="images/front-bghitam.gif" width="100%" height="130"></td>
			                <td style="height: 130px"><img src="images/front-bar-merah-kananv2.jpg" width="319" height="130"></td>
		                </tr>
	                  </table></td>
                  </tr>
                        <tr class="formText">
                            <td bgcolor="#ffffff" rowspan="3" valign="top">             
                                <table height="100%">
                                    <tr>
                                        <td align="center" colspan="2" style="padding-left: 20px; padding-top: 20px" valign="top" id="tdcontent">
                                            <table id="Table1" bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="2"
                        cellspacing="1" style="position: static" width="70%">
                                    <tr  bgcolor="#dddddd" class="formText" height="30" align="center">
                            <td  colspan="3" style="width: 100%" align="center">
                                <span style="font-size: 12pt"><strong>Sign Out</strong></span></td>
                        </tr>
                        <tr class="formText">
                            <td align="center" bgcolor="#ffffff" rowspan="2">
                                <br />
                                You have been successfully signed out.<br />
                                <br />
                                <asp:LinkButton ID="LinkButton1" runat="server">Click here to log back in.</asp:LinkButton><br />
                                <br />
                                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator></td>
                        </tr>
                        <tr class="formText">
                        </tr>            
                                </table>
                                            <br />
                                        </td>
                                    </tr>
		                <tr>
			                <td style="padding-left:20px;padding-top:20px;" valign="top"></td>
			                <td valign="bottom" align="right" width="99%"><table>
				                <tr>
					                <td align="right" style="padding-right:10px">Copyright &copy; Bank CIMB Niaga 20010-2011, All Rights Reserved<br>
						                Version 2.0.0 (Build 9999)<br>
						                <br>
						                Developed by <a href="http://www.sahassa.co.id" class="stdbold">Sahassa</a></td>
					                </tr>
			                </table></td>
		                </tr></table>
                            </td>
                        </tr>
                        <tr class="formText">
                        </tr>
                        <tr>
                  </tr>
                    </table>
	            </td>	
            </tr>
         </table>
    </form>   
</body>
</html>