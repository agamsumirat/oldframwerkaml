#Region "Imports..."
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports AMLBLL.ValidateBLL
Imports System
Imports Sahassa.AML.Commonly
Imports AMLBLL.DataType
Imports AMLBLL
#End Region


Partial Class LTKT_APPROVALDETAIL_View
    Inherits Parent

#Region "Member..."
    Private ReadOnly BindGridFromExcel As Boolean
#End Region

#Region "Property"

    Public ReadOnly Property SetnGeNIK() As String
        Get
            'Return SessionNIK
            Return SessionUserId
        End Get
    End Property

    Private Sub ShowButonApproveReject(ByVal Show As Boolean)
        imgReject.Visible = Show
        imgApprove.Visible = Show
    End Sub


    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("PK_LTKT_Approval_Id.Sort") Is Nothing, " PK_LTKT_Approval_Id  asc", Session("LTKT_APPROVALDETAIL.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("LTKT_APPROVALDETAIL.Sort") = Value
        End Set
    End Property
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("LTKT_APPROVALDETAIL.SelectedItem") Is Nothing, New ArrayList, Session("LTKT_APPROVALDETAIL.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("LTKT_APPROVALDETAIL.SelectedItem") = value
        End Set
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("LTKT_APPROVALDETAIL.RowTotal") Is Nothing, 0, Session("LTKT_APPROVALDETAIL.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("LTKT_APPROVALDETAIL.RowTotal") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return iTotalPages(SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("LTKT_APPROVALDETAIL.CurrentPage") Is Nothing, 0, Session("LTKT_APPROVALDETAIL.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("LTKT_APPROVALDETAIL.CurrentPage") = Value
        End Set
    End Property
    Public ReadOnly Property SetnGetBindTable() As VList(Of vw_LTKTuploadApprovalDetail)
        Get
            Dim strWhereClause As String
            If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
                SetnGetCurrentPage = GetPageTotal - 1
            End If

            'TODO lepas lagi kalo sudah projeknya disatukan
            strWhereClause = "PK_LTKT_Approval_Id=" & Request.Item("ID")
            Return DataRepository.vw_LTKTuploadApprovalDetailProvider.GetPaged(strWhereClause, SetnGetSort, SetnGetCurrentPage, GetDisplayedTotalRow, SetnGetRowTotal)

        End Get

    End Property

#End Region

#Region "Function"

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : SetnGetCurrentPage = 0
                Case "Prev" : SetnGetCurrentPage -= 1
                Case "Next" : SetnGetCurrentPage += 1
                Case "Last" : SetnGetCurrentPage = GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            CollectSelected()

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub SetInfoNavigate()
        PageCurrentPage.Text = (SetnGetCurrentPage + 1).ToString
        PageTotalPages.Text = GetPageTotal.ToString
        PageTotalRows.Text = SetnGetRowTotal.ToString
        LinkButtonNext.Enabled = (Not SetnGetCurrentPage + 1 = GetPageTotal) AndAlso GetPageTotal <> 0
        LinkButtonLast.Enabled = (Not SetnGetCurrentPage + 1 = GetPageTotal) AndAlso GetPageTotal <> 0
        First.Enabled = Not SetnGetCurrentPage = 0
        LinkButtonPrevious.Enabled = Not SetnGetCurrentPage = 0
    End Sub

    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim TotalRow As Int16 = 0
        For Each gridRow As DataGridItem In GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                TotalRow = CType(TotalRow + 1, Int16)
            End If
        Next
        If TotalRow = 0 Then
            'If i = totalrow Then
            CheckBoxSelectAll.Checked = False
            'End If
        Else
            If i = TotalRow Then
                CheckBoxSelectAll.Checked = True
            Else
                CheckBoxSelectAll.Checked = False
            End If
        End If
    End Sub
    Private Sub HideButtonGrid()
        With GridMsUserView
            .Columns(.Columns.Count - 4).Visible = False
            .Columns(.Columns.Count - 3).Visible = False
            .Columns(.Columns.Count - 2).Visible = False
            .Columns(.Columns.Count - 1).Visible = False
        End With
    End Sub

    Private Sub BindSelectedAll()
        Try
            GridMsUserView.DataSource = DataRepository.vw_LTKTuploadApprovalDetailProvider.GetAll
            GridMsUserView.AllowPaging = False
            GridMsUserView.DataBind()

            For i As Integer = 0 To GridMsUserView.Items.Count - 1
                For y As Integer = 0 To GridMsUserView.Columns.Count - 1
                    GridMsUserView.Items(i).Cells(y).Attributes.Add("class", "text")
                Next
            Next
            HideButtonGrid()
        Catch
            Throw
        End Try


    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                    'Skip
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub

    Private Sub NewRenderMethod(ByVal writer As HtmlTextWriter, ByVal ctl As Control)
        Dim cell As TableCell = DirectCast(ctl.Controls(ctl.Controls.Count - 1), TableCell)
        For i As Integer = 0 To ctl.Controls.Count - 1   'dikurang sama yang hidden
            If i = 0 Then
                'create header column pertama - rowspan=""2"" untuk membentuk merge rows sebanyak 2 rows
                writer.Write("<TD rowspan=""2"" style=""color: white"" align=""center"">  </TD>" & vbLf)
            ElseIf i = 1 Then
                ctl.Controls(2).RenderControl(writer)
            ElseIf i = 2 Then
                ctl.Controls(3).RenderControl(writer)
            ElseIf i = 3 Then
                ctl.Controls(4).RenderControl(writer)
            ElseIf i = 4 Then
                ctl.Controls(5).RenderControl(writer)
            ElseIf i = 5 Then
                ctl.Controls(6).RenderControl(writer)
            ElseIf i = 6 Then
                ctl.Controls(7).RenderControl(writer)
            ElseIf i = 7 Then
                'create header column kedua samapi ke empat -  colspan=""3"" untuk membentuk merge column sebanyak 3 column 
                writer.Write("<TD colspan=""2"" style=""color: white"" align=""center""> PJK Office </TD>" & vbLf)
            ElseIf i = 9 Then
                writer.Write("<TD colspan=""2"" style=""color: white"" align=""center""> Transaction Nominal </TD>" & vbLf)
            ElseIf i = 11 Then
                writer.Write("<TD colspan=""2"" style=""color: white"" align=""center""> Transaction Detail </TD>" & vbLf)
            ElseIf i = 12 Then
                ctl.Controls(14).RenderControl(writer)
            End If
        Next

        'Merupakan control header dari datagrid   



        'ctl.Controls(21).RenderControl(writer)
        'ctl.Controls(22).RenderControl(writer)
        'ctl.Controls(23).RenderControl(writer)
        'ctl.Controls(24).RenderControl(writer)
        'ctl.Controls(25).RenderControl(writer)

        GridMsUserView.HeaderStyle.AddAttributesToRender(writer)

        '***     Insert the second row 
        writer.RenderBeginTag("TR")

        'For i As Integer = 0 To ctl.Controls.Count - 1
        '    ctl.Controls(i).RenderControl(writer)
        'Next

        ctl.Controls(8).RenderControl(writer)
        ctl.Controls(9).RenderControl(writer)
        ctl.Controls(10).RenderControl(writer)
        ctl.Controls(11).RenderControl(writer)
        ctl.Controls(12).RenderControl(writer)
        ctl.Controls(13).RenderControl(writer)
        'ctl.Controls(20).RenderControl(writer)

        writer.RenderEndTag()
    End Sub

    Private Sub BindSelected()
        Dim Rows As New ArrayList

        For Each IdDIN As Int64 In SetnGetSelectedItem
            Using rowData As VList(Of vw_LTKTuploadApprovalDetail) = DataRepository.vw_LTKTuploadApprovalDetailProvider.GetPaged(" PK_LTKT_Approval_Id = " & IdDIN & "", "", 0, Integer.MaxValue, 0)
                If rowData.Count > 0 Then
                    Rows.Add(rowData(0))
                End If
            End Using
        Next

        GridMsUserView.DataSource = Rows
        GridMsUserView.AllowPaging = False
        GridMsUserView.DataBind()
        For i As Integer = 0 To GridMsUserView.Items.Count - 1
            For y As Integer = 0 To GridMsUserView.Columns.Count - 1
                GridMsUserView.Items(i).Cells(y).Attributes.Add("class", "text")
            Next
        Next

        HideButtonGrid()

    End Sub
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                Dim PKMsUserId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = SetnGetSelectedItem
                If ArrTarget.Contains(PKMsUserId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKMsUserId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKMsUserId)
                End If
                SetnGetSelectedItem = ArrTarget
            End If
        Next

    End Sub


    Private Sub ClearThisPageSessions()
        Clearproperty()
        LblMessage.Visible = False
        LblMessage.Text = ""
        SetnGetRowTotal = Nothing
        SetnGetSort = Nothing
        SetnGetCurrentPage = 0
    End Sub
    Private Sub Clearproperty()



    End Sub
    Private Sub popDateControl()

    End Sub
    Private Sub BindComboBox()

    End Sub

    Private Sub bindgrid()
        GridMsUserView.DataSource = SetnGetBindTable
        If SetnGetBindTable.Count > 0 Then
            lblRequestBy.Text = SetnGetBindTable(0).RequestedBy
            lblRequestDate.Text = SetnGetBindTable(0).RequestedDate.Value.ToString("dd-MM-yyyy")
        End If

        GridMsUserView.CurrentPageIndex = SetnGetCurrentPage
        GridMsUserView.VirtualItemCount = SetnGetRowTotal
        GridMsUserView.DataBind()
        'SettingConfigConfiguration()
        If SetnGetRowTotal > 0 Then
            LabelNoRecordFound.Visible = False
            ShowButonApproveReject(True)
        Else
            ShowButonApproveReject(False)
            LabelNoRecordFound.Visible = True
        End If
    End Sub
    'Private Sub SettingConfigConfiguration()
    '    Dim ObjMappingModuleActionList As TList(Of MappingMsShsModuleMsShsAction) = Nothing
    '    Dim bAllowAdd As Boolean
    '    Dim bAllowEdit As Boolean = False
    '    Dim bAllowDelete As Boolean = False
    '    Dim bAlllowDeleteOperation As Boolean = False
    '    Try
    '        Using ObjSecurityConfigurationBll As New SahassaBLL.SecurityConfigurationBll
    '            ObjMappingModuleActionList = ObjSecurityConfigurationBll.LoadConfiguration(CInt(SessionGroupMenuId), MsShsModuleList.FORMTKT_APPROVALDETAIL)
    '            For Each ObjMappingModuleAction As MappingMsShsModuleMsShsAction In ObjMappingModuleActionList
    '                If ObjMappingModuleAction.FK_ShsModule_id.GetValueOrDefault(0) = MsShsModuleList.FORMTKT_APPROVALDETAIL Then
    '                    If ObjMappingModuleAction.FK_ShsAction_id.GetValueOrDefault(0) = MsShsActionList.Add Then
    '                        bAllowAdd = True
    '                        Exit For
    '                    End If
    '                End If
    '            Next
    '            For Each ObjMappingModuleAction As MappingMsShsModuleMsShsAction In ObjMappingModuleActionList
    '                If ObjMappingModuleAction.FK_ShsModule_id.GetValueOrDefault(0) = MsShsModuleList.FORMTKT_APPROVALDETAIL Then
    '                    If ObjMappingModuleAction.FK_ShsAction_id.GetValueOrDefault(0) = MsShsActionList.Edit Then
    '                        bAllowEdit = True
    '                        Exit For
    '                    End If
    '                End If
    '            Next
    '            For Each ObjMappingModuleAction As MappingMsShsModuleMsShsAction In ObjMappingModuleActionList
    '                If ObjMappingModuleAction.FK_ShsModule_id.GetValueOrDefault(0) = MsShsModuleList.FORMTKT_APPROVALDETAIL Then
    '                    If ObjMappingModuleAction.FK_ShsAction_id.GetValueOrDefault(0) = MsShsActionList.Delete Then
    '                        bAllowDelete = True
    '                        Exit For
    '                    End If
    '                End If
    '            Next
    '            For Each ObjMappingModuleAction As MappingMsShsModuleMsShsAction In ObjMappingModuleActionList
    '                If ObjMappingModuleAction.FK_ShsModule_id.GetValueOrDefault(0) = MsShsModuleList.FORMTKT_APPROVALDETAIL Then
    '                    If ObjMappingModuleAction.FK_ShsAction_id.GetValueOrDefault(0) = MsShsActionList.DeleteOperation Then
    '                        bAlllowDeleteOperation = True
    '                        Exit For
    '                    End If
    '                End If
    '            Next

    '            If bAllowAdd Then
    '                LinkButtonAddNew.Visible = True
    '            Else
    '                LinkButtonAddNew.Visible = False
    '            End If

    '            GridMsUserView.Columns(GridMsUserView.Columns.Count - 3).Visible = bAllowEdit
    '            GridMsUserView.Columns(GridMsUserView.Columns.Count - 2).Visible = bAlllowDeleteOperation
    '            GridMsUserView.Columns(GridMsUserView.Columns.Count - 1).Visible = bAllowDelete

    '        End Using
    '    Catch ex As Exception
    '        LogError(ex)
    '        CvalPageErr.IsValid = False
    '        CvalPageErr.ErrorMessage = ex.Message
    '    End Try

    'End Sub
#End Region

#Region "events..."

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgReject.Click

        Try
            ' Throw New Exception("halo")
            Dim keyapprov As String = "0"
            For Each i As vw_LTKTuploadApprovalDetail In SetnGetBindTable
                '================================ Deklaration ===================================================
                Dim LTKT_ApprovalDetail As LTKT_ApprovalDetail
                Dim L_LTKTTransactionCashIn_ApprovalDetail As TList(Of LTKTTransactionCashIn_ApprovalDetail)
                Dim L_LTKTTransactionCashOut_ApprovalDetail As TList(Of LTKTTransactionCashOut_ApprovalDetail)
                Dim L_LTKTDetailCashoutTransaction As New TList(Of LTKTDetailCashOutTransaction_ApprovalDetail)
                Dim L_LTKTDetailCashInTransaction As New TList(Of LTKTDetailCashInTransaction_ApprovalDetail)
                '============================================================================================

                '=========== Get LTKT Approval
                LTKT_ApprovalDetail = DataRepository.LTKT_ApprovalDetailProvider.GetPaged("PK_LTKT_Id=" & i.PK_LTKT_Id, "", 0, Integer.MaxValue, Nothing)(0)

                keyapprov = LTKT_ApprovalDetail.FK_LTKT_Approval_Id.GetValueOrDefault(0).ToString

                Dim LTKTID As String = LTKT_ApprovalDetail.PK_LTKT_Id


                L_LTKTTransactionCashIn_ApprovalDetail = DataRepository.LTKTTransactionCashIn_ApprovalDetailProvider.GetPaged("FK_LTKT_Id=" & LTKTID & " and FK_LTKT_ApprovalDetail_Id=" & keyapprov, "", 0, Integer.MaxValue, Nothing)

                For Each cashin As LTKTTransactionCashIn_ApprovalDetail In L_LTKTTransactionCashIn_ApprovalDetail
                    L_LTKTDetailCashInTransaction = DataRepository.LTKTDetailCashInTransaction_ApprovalDetailProvider.GetPaged("FK_LTKTTransactionCashIn_Id=" & cashin.PK_LTKTTransactionCashIn_Id, "", 0, Integer.MaxValue, Nothing)
                Next

                L_LTKTTransactionCashOut_ApprovalDetail = DataRepository.LTKTTransactionCashOut_ApprovalDetailProvider.GetPaged("FK_LTKT_Id=" & LTKTID & " and FK_LTKT_ApprovalDetail_Id=" & keyapprov, "", 0, Integer.MaxValue, Nothing)

                For Each cashout As LTKTTransactionCashOut_ApprovalDetail In L_LTKTTransactionCashOut_ApprovalDetail
                    L_LTKTDetailCashoutTransaction = DataRepository.LTKTDetailCashOutTransaction_ApprovalDetailProvider.GetPaged("FK_LTKTTransactionCashOut_Id=" & cashout.PK_LTKTTransactionCashOut_Id, "", 0, Integer.MaxValue, Nothing)
                Next

                DataRepository.LTKT_ApprovalDetailProvider.Delete(LTKT_ApprovalDetail)
                DataRepository.LTKTTransactionCashIn_ApprovalDetailProvider.Delete(L_LTKTTransactionCashIn_ApprovalDetail)
                DataRepository.LTKTTransactionCashOut_ApprovalDetailProvider.Delete(L_LTKTTransactionCashOut_ApprovalDetail)
                DataRepository.LTKTDetailCashInTransaction_ApprovalDetailProvider.Delete(L_LTKTDetailCashInTransaction)
                DataRepository.LTKTDetailCashOutTransaction_ApprovalDetailProvider.Delete(L_LTKTDetailCashoutTransaction)
            Next
            MultiViewUtama.ActiveViewIndex = 1
            ShowButonApproveReject(False)
            ImageCancel.Visible = False

            Using app As LTKT_Approval = DataRepository.LTKT_ApprovalProvider.GetByPK_LTKT_Approval_Id(CInt(Request.Params("ID")))

                If Not IsNothing(app) Then
                    Using objMsUser As User = DataRepository.UserProvider.GetBypkUserID(app.RequestedBy)
                        If Not IsNothing(objMsUser) Then

                            Session("LTKT_ApprovalDetail_View.ApproverUser") = objMsUser.UserEmailAddress & ";"
                        End If
                    End Using
                    DataRepository.LTKT_ApprovalProvider.Delete(app)
                End If
            End Using

            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = "Rejected Success"
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("LTKT_APPROVAL_View.aspx")
    End Sub

    Protected Sub imgOkRejectReason_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOkRejectReason.Click
        'Kirim Email ke yang punya data

        Dim ReasonOfRejecting As String = txtReasonReject.Text
        Dim ApproverUser As String = vbNullString
        Dim EmailBodyData As String = vbNullString

        'EmailBodyData = "Your LTKT is rejected by (User Id : " & Sahassa.AML.Commonly.SessionNIK & "-" & Sahassa.AML.Commonly.SessionStaffName & ")<br>Here is the reason:<br> " & txtReasonReject.Text
        'EmailBodyData = EmailBLL.getEmailBodyTemplate("LTKT Reject", "LTKT", "", ReasonOfRejecting)


        ApproverUser = Session("LTKT_ApprovalDetail_View.ApproverUser")
        Session("LTKT_ApprovalDetail_View.ApproverUser") = Nothing

        'Using CommonEmail As New Sahassa.AML.EmailSend
        '    'CommonEmail.Subject = "Approval LTKT Rejeceted (User Id : " & Sahassa.AML.Commonly.SessionNIK & "-" & Sahassa.AML.Commonly.SessionStaffName & ")"
        '    CommonEmail.Subject = "LTKT Reject"
        '    CommonEmail.Sender = "Cash Transaction Report Application"
        '    CommonEmail.Recipient = ApproverUser
        '    CommonEmail.Body = EmailBodyData
        '    CommonEmail.SendEmail()
        'End Using



        Response.Redirect("LTKT_APPROVAL_View.aspx")
    End Sub

    Protected Sub GridDataView_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMsUserView.ItemCreated
        Dim lit As ListItemType = e.Item.ItemType
        If ListItemType.Header = lit Then
            e.Item.SetRenderMethodDelegate(New RenderMethod(AddressOf NewRenderMethod))

            'Merupakan control header untuk merge rows setelah dilakukan custom header
            e.Item.Cells(0).RowSpan = 2
            e.Item.Cells(2).RowSpan = 2
            e.Item.Cells(3).RowSpan = 2
            e.Item.Cells(4).RowSpan = 2
            e.Item.Cells(5).RowSpan = 2
            e.Item.Cells(6).RowSpan = 2
            e.Item.Cells(7).RowSpan = 2
            e.Item.Cells(14).RowSpan = 2
            'e.Item.Cells(21).RowSpan = 2
            'e.Item.Cells(22).RowSpan = 2
            'e.Item.Cells(23).RowSpan = 2
            'e.Item.Cells(24).RowSpan = 2
            'e.Item.Cells(25).RowSpan = 2


        End If
    End Sub

    Protected Sub btnConfirmed_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgApprove.Click
        Try
            Dim keyapprov As String = "0"
            For Each i As vw_LTKTuploadApprovalDetail In SetnGetBindTable
                '================================ Deklaration ===================================================
                Dim LTKT As LTKT
                Dim L_Cashin As TList(Of LTKTTransactionCashIn)
                Dim L_DetailCashin As New TList(Of LTKTDetailCashInTransaction)
                Dim L_CashOut As TList(Of LTKTTransactionCashOut)
                Dim L_DetailCashOut As New TList(Of LTKTDetailCashOutTransaction)

                Dim LTKT_ApprovalDetail As LTKT_ApprovalDetail
                Dim L_LTKTTransactionCashIn_ApprovalDetail As TList(Of LTKTTransactionCashIn_ApprovalDetail)
                Dim L_LTKTTransactionCashOut_ApprovalDetail As TList(Of LTKTTransactionCashOut_ApprovalDetail)
                Dim L_LTKTDetailCashoutTransaction As New TList(Of LTKTDetailCashOutTransaction_ApprovalDetail)
                Dim L_LTKTDetailCashInTransaction As New TList(Of LTKTDetailCashInTransaction_ApprovalDetail)
                '============================================================================================

                'Get LTKT Approval
                LTKT_ApprovalDetail = DataRepository.LTKT_ApprovalDetailProvider.GetPaged("PK_LTKT_Id=" & i.PK_LTKT_Id, "", 0, Integer.MaxValue, Nothing)(0)
                LTKT = ConvertTo_LTKT(LTKT_ApprovalDetail)
                If LTKT Is Nothing Then
                    Throw New Exception("LTKT Tidak eksis")
                    Exit Sub
                End If
                keyapprov = LTKT_ApprovalDetail.FK_LTKT_Approval_Id.GetValueOrDefault.ToString

                Dim LTKTID As String = LTKT.PK_LTKT_Id

                L_LTKTTransactionCashIn_ApprovalDetail = DataRepository.LTKTTransactionCashIn_ApprovalDetailProvider.GetPaged("FK_LTKT_Id=" & LTKTID & " and FK_LTKT_ApprovalDetail_Id=" & keyapprov, "", 0, Integer.MaxValue, Nothing)
                L_Cashin = ConvertTo_LTKTTRANSACTIONCASHIN(L_LTKTTransactionCashIn_ApprovalDetail)


                For Each cashin As LTKTTransactionCashIn_ApprovalDetail In L_LTKTTransactionCashIn_ApprovalDetail
                    L_LTKTDetailCashInTransaction = DataRepository.LTKTDetailCashInTransaction_ApprovalDetailProvider.GetPaged("FK_LTKTTransactionCashIn_Id=" & cashin.PK_LTKTTransactionCashIn_Id & " and FK_LTKT_ApprovalDetail_Id=" & keyapprov, "", 0, Integer.MaxValue, Nothing)
                    L_DetailCashin.AddRange(ConvertTo_LTKTDETAILCASHINTRANSACTION(L_LTKTDetailCashInTransaction))
                Next

                L_LTKTTransactionCashOut_ApprovalDetail = DataRepository.LTKTTransactionCashOut_ApprovalDetailProvider.GetPaged("FK_LTKT_Id=" & LTKTID & " and FK_LTKT_ApprovalDetail_Id=" & keyapprov, "", 0, Integer.MaxValue, Nothing)
                L_CashOut = ConvertTo_LTKTTRANSACTIONCASHOUT(L_LTKTTransactionCashOut_ApprovalDetail)


                For Each cashout As LTKTTransactionCashOut_ApprovalDetail In L_LTKTTransactionCashOut_ApprovalDetail
                    L_LTKTDetailCashoutTransaction = DataRepository.LTKTDetailCashOutTransaction_ApprovalDetailProvider.GetPaged("FK_LTKTTransactionCashOut_Id=" & cashout.PK_LTKTTransactionCashOut_Id & " and FK_LTKT_ApprovalDetail_Id=" & keyapprov, "", 0, Integer.MaxValue, Nothing)
                    L_DetailCashOut.AddRange(ConvertTo_LTKTDETAILCASHOUTTRANSACTION(L_LTKTDetailCashoutTransaction))
                Next

                DataRepository.LTKTProvider.Update(LTKT)
                DataRepository.LTKTTransactionCashInProvider.Update(L_Cashin)
                DataRepository.LTKTTransactionCashOutProvider.Update(L_CashOut)
                DataRepository.LTKTDetailCashInTransactionProvider.Update(L_DetailCashin)
                DataRepository.LTKTDetailCashOutTransactionProvider.Update(L_DetailCashOut)

                DataRepository.LTKT_ApprovalDetailProvider.Delete(LTKT_ApprovalDetail)
                DataRepository.LTKTTransactionCashIn_ApprovalDetailProvider.Delete(L_LTKTTransactionCashIn_ApprovalDetail)
                DataRepository.LTKTTransactionCashOut_ApprovalDetailProvider.Delete(L_LTKTTransactionCashOut_ApprovalDetail)
                DataRepository.LTKTDetailCashInTransaction_ApprovalDetailProvider.Delete(L_LTKTDetailCashInTransaction)
                DataRepository.LTKTDetailCashOutTransaction_ApprovalDetailProvider.Delete(L_LTKTDetailCashoutTransaction)
            Next
            Using app As LTKT_Approval = DataRepository.LTKT_ApprovalProvider.GetByPK_LTKT_Approval_Id(CInt(keyapprov))
                DataRepository.LTKT_ApprovalProvider.Delete(app)
            End Using
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = "Confirmed Success"
            ShowButonApproveReject(False)
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IsPostBack Then
                MultiViewUtama.ActiveViewIndex = 0
            End If
            popDateControl()
            BindComboBox()
            LblMessage.Text = ""
            LblMessage.Visible = False
            If Not Page.IsPostBack Then
                ClearThisPageSessions()

                'Insert Audit access
                ' AuditTrailAccess(Page)
            End If

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            bindgrid()
            SetInfoNavigate()
            SetCheckedAll()

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In GridMsUserView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim pkid As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = SetnGetSelectedItem
        '        If SetnGetSelectedItem.Contains(pkid) Then
        '            If CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            Else
        '                ArrTarget.Remove(pkid)
        '            End If
        '        Else
        '            If CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            End If
        '        End If
        '        SetnGetSelectedItem = ArrTarget
        '    End If
        'Next

        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget

    End Sub

    Protected Sub GridMsUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMsUserView.EditCommand
        Dim PKMsUserApprovalid As Integer
        Try
            PKMsUserApprovalid = CInt(e.Item.Cells(1).Text)
            Response.Redirect("UserApprovalDetail.aspx?PKMsUserApprovalID=" & PKMsUserApprovalid, False)
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub GridMsUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMsUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                chkBox.Checked = SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (SetnGetCurrentPage * GetDisplayedTotalRow))
                End If

                If e.Item.Cells(GridMsUserView.Columns.Count - 5).Text = "C" Then
                    e.Item.Cells(GridMsUserView.Columns.Count - 2).Enabled = False
                End If

                If e.Item.Cells(4).Text = "01-Jan-1900" Then
                    e.Item.Cells(4).Text = ""
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub GridMsUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMsUserView.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            SetnGetSort = ChangeSortCommand(e.SortExpression)
            GridUser.Columns(IndexSort(GridUser, e.SortExpression)).SortExpression = SetnGetSort

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click

        Try
            CollectSelected()
            Const strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelected()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=UserApproval.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            EnableViewState = False
            Using stringWrite As System.IO.StringWriter = New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                ClearControls(GridMsUserView)
                GridMsUserView.RenderControl(htmlWrite)
                Response.Write(strStyle)
                Response.Write(stringWrite.ToString())
            End Using
            Response.End()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) AndAlso (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    Throw New Exception("EXECPTION_PAGENUMBER_GREATER_TOTALPAGENUMBER")
                End If
            Else
                Throw New Exception("EXECPTION_PAGENUMBER_NUMERICPOSITIF")
            End If
            CollectSelected()
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
        If CvalHandleErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalPageErr.PreRender
        If CvalPageErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Protected Sub lnkExportAllData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAllData.Click

        Try

            Const strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelectedAll()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=UserApprovalAll.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            EnableViewState = False
            Using stringWrite As System.IO.StringWriter = New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                ClearControls(GridMsUserView)
                GridMsUserView.RenderControl(htmlWrite)
                Response.Write(strStyle)
                Response.Write(stringWrite.ToString())
            End Using
            Response.End()
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

#End Region

#Region "Convert Object.."

    Function ConvertTo_LTKTTRANSACTIONCASHIN(ByVal ListSourceObject As TList(Of LTKTTransactionCashIn_ApprovalDetail)) As TList(Of LTKTTransactionCashIn)
        Dim temp As New TList(Of LTKTTransactionCashIn)
        For Each SourceObject As LTKTTransactionCashIn_ApprovalDetail In ListSourceObject
            Using DestObject As LTKTTransactionCashIn = DataRepository.LTKTTransactionCashInProvider.GetByPK_LTKTTransactionCashIn_Id(SourceObject.PK_LTKTTransactionCashIn_Id)
                If DestObject Is Nothing Then
                    Continue For
                End If

                With DestObject
                    FillOrNothing(.CORP_TipeAlamat, SourceObject.CORP_TipeAlamat, True, oByte)
                    FillOrNothing(.CORP_FK_MsKecamatan_Id, SourceObject.CORP_FK_MsKecamatan_Id, True, oInt)
                    FillOrNothing(.CORP_FK_MsKotaKab_Id, SourceObject.CORP_FK_MsKotaKab_Id, True, oInt)
                    FillOrNothing(.CORP_FK_MsProvince_Id, SourceObject.CORP_FK_MsProvince_Id, True, oInt)
                    FillOrNothing(.CORP_FK_MsNegara_Id, SourceObject.CORP_FK_MsNegara_Id, True, oInt)
                    FillOrNothing(.INDV_FK_MsIDType_Id, SourceObject.INDV_FK_MsIDType_Id, True, oInt)
                    FillOrNothing(.CORP_FK_MsBentukBadanUsaha_Id, SourceObject.CORP_FK_MsBentukBadanUsaha_Id, True, oInt)
                    FillOrNothing(.CORP_FK_MsBidangUsaha_Id, SourceObject.CORP_FK_MsBidangUsaha_Id, True, oInt)
                    FillOrNothing(.CORP_LN_FK_MsNegara_Id, SourceObject.CORP_LN_FK_MsNegara_Id, True, oInt)
                    FillOrNothing(.CORP_LN_FK_MsProvince_Id, SourceObject.CORP_LN_FK_MsProvince_Id, True, oInt)
                    FillOrNothing(.CORP_LN_MsKotaKab_Id, SourceObject.CORP_LN_MsKotaKab_Id, True, oInt)
                    FillOrNothing(.FK_LTKT_Id, SourceObject.FK_LTKT_Id, True, oInt)
                    FillOrNothing(.FK_MsKotaKab_Id, SourceObject.FK_MsKotaKab_Id, True, oInt)
                    FillOrNothing(.FK_MsProvince_Id, SourceObject.FK_MsProvince_Id, True, oInt)
                    FillOrNothing(.INDV_FK_MsNegara_Id, SourceObject.INDV_FK_MsNegara_Id, True, oInt)
                    FillOrNothing(.INDV_DOM_FK_MsKecamatan_Id, SourceObject.INDV_DOM_FK_MsKecamatan_Id, True, oInt)
                    FillOrNothing(.INDV_DOM_FK_MsKotaKab_Id, SourceObject.INDV_DOM_FK_MsKotaKab_Id, True, oInt)
                    FillOrNothing(.INDV_DOM_FK_MsProvince_Id, SourceObject.INDV_DOM_FK_MsProvince_Id, True, oInt)
                    FillOrNothing(.INDV_DOM_FK_MsNegara_Id, SourceObject.INDV_DOM_FK_MsNegara_Id, True, oInt)
                    FillOrNothing(.INDV_ID_FK_MsKecamatan_Id, SourceObject.INDV_ID_FK_MsKecamatan_Id, True, oInt)
                    FillOrNothing(.INDV_ID_FK_MsKotaKab_Id, SourceObject.INDV_ID_FK_MsKotaKab_Id, True, oInt)
                    FillOrNothing(.INDV_ID_FK_MsProvince_Id, SourceObject.INDV_ID_FK_MsProvince_Id, True, oInt)
                    FillOrNothing(.INDV_ID_FK_MsNegara_Id, SourceObject.INDV_ID_FK_MsNegara_Id, True, oInt)
                    FillOrNothing(.INDV_NA_FK_MsNegara_Id, SourceObject.INDV_NA_FK_MsNegara_Id, True, oInt)
                    FillOrNothing(.INDV_NA_FK_MsProvince_Id, SourceObject.INDV_NA_FK_MsProvince_Id, True, oInt)
                    FillOrNothing(.INDV_NA_FK_MsKotaKab_Id, SourceObject.INDV_NA_FK_MsKotaKab_Id, True, oInt)
                    FillOrNothing(.INDV_FK_MsPekerjaan_Id, SourceObject.INDV_FK_MsPekerjaan_Id, True, oInt)
                    FillOrNothing(.INDV_TanggalLahir, SourceObject.INDV_TanggalLahir, True, oDate)
                    FillOrNothing(.TanggalTransaksi, SourceObject.TanggalTransaksi, True, oDate)
                    FillOrNothing(.Total, SourceObject.Total, True, oDecimal)
                    FillOrNothing(.CORP_FK_MsKelurahan_Id, SourceObject.CORP_FK_MsKelurahan_Id, True, OLong)
                    FillOrNothing(.INDV_ID_FK_MsKelurahan_Id, SourceObject.INDV_ID_FK_MsKelurahan_Id, True, OLong)
                    FillOrNothing(.INDV_DOM_FK_MsKelurahan_Id, SourceObject.INDV_DOM_FK_MsKelurahan_Id, True, OLong)
                    FillOrNothing(.INDV_NomorId, SourceObject.INDV_NomorId, True, oString)
                    FillOrNothing(.INDV_NPWP, SourceObject.INDV_NPWP, True, oString)
                    FillOrNothing(.CORP_Nama, SourceObject.CORP_Nama, True, oString)
                    FillOrNothing(.CORP_LN_KodePos, SourceObject.CORP_LN_KodePos, True, oString)
                    FillOrNothing(.CORP_NPWP, SourceObject.CORP_NPWP, True, oString)
                    FillOrNothing(.CORP_TujuanTransaksi, SourceObject.CORP_TujuanTransaksi, True, oString)
                    FillOrNothing(.CORP_SumberDana, SourceObject.CORP_SumberDana, True, oString)
                    FillOrNothing(.CORP_NamaBankLain, SourceObject.CORP_NamaBankLain, True, oString)
                    FillOrNothing(.CORP_NomorRekeningTujuan, SourceObject.CORP_NomorRekeningTujuan, True, oString)
                    FillOrNothing(.CORP_NamaJalan, SourceObject.CORP_NamaJalan, True, oString)
                    FillOrNothing(.CORP_RTRW, SourceObject.CORP_RTRW, True, oString)
                    FillOrNothing(.CORP_KodePos, SourceObject.CORP_KodePos, True, oString)
                    FillOrNothing(.CORP_LN_NamaJalan, SourceObject.CORP_LN_NamaJalan, True, oString)
                    FillOrNothing(.NamaKantorPJK, SourceObject.NamaKantorPJK, True, oString)
                    FillOrNothing(.NomorRekening, SourceObject.NomorRekening, True, oString)
                    FillOrNothing(.INDV_Gelar, SourceObject.INDV_Gelar, True, oString)
                    FillOrNothing(.INDV_NamaLengkap, SourceObject.INDV_NamaLengkap, True, oString)
                    FillOrNothing(.INDV_TempatLahir, SourceObject.INDV_TempatLahir, True, oString)
                    FillOrNothing(.INDV_DOM_NamaJalan, SourceObject.INDV_DOM_NamaJalan, True, oString)
                    FillOrNothing(.INDV_DOM_RTRW, SourceObject.INDV_DOM_RTRW, True, oString)
                    FillOrNothing(.INDV_DOM_KodePos, SourceObject.INDV_DOM_KodePos, True, oString)
                    FillOrNothing(.INDV_ID_NamaJalan, SourceObject.INDV_ID_NamaJalan, True, oString)
                    FillOrNothing(.INDV_ID_RTRW, SourceObject.INDV_ID_RTRW, True, oString)
                    FillOrNothing(.INDV_Jabatan, SourceObject.INDV_Jabatan, True, oString)
                    FillOrNothing(.INDV_PenghasilanRataRata, SourceObject.INDV_PenghasilanRataRata, True, oString)
                    FillOrNothing(.INDV_TempatBekerja, SourceObject.INDV_TempatBekerja, True, oString)
                    FillOrNothing(.INDV_TujuanTransaksi, SourceObject.INDV_TujuanTransaksi, True, oString)
                    FillOrNothing(.INDV_SumberDana, SourceObject.INDV_SumberDana, True, oString)
                    FillOrNothing(.INDV_NamaBankLain, SourceObject.INDV_NamaBankLain, True, oString)
                    FillOrNothing(.INDV_NomorRekeningTujuan, SourceObject.INDV_NomorRekeningTujuan, True, oString)
                    FillOrNothing(.INDV_NA_KodePos, SourceObject.INDV_NA_KodePos, True, oString)
                    FillOrNothing(.INDV_ID_KodePos, SourceObject.INDV_ID_KodePos, True, oString)
                    FillOrNothing(.INDV_NA_NamaJalan, SourceObject.INDV_NA_NamaJalan, True, oString)
                    FillOrNothing(.INDV_Kewarganegaraan, SourceObject.INDV_Kewarganegaraan, True, oString)
                    FillOrNothing(.LastUpdateDate, SourceObject.LastUpdateDate, True, oDate)
                    FillOrNothing(.LastUpdateBy, SourceObject.LastUpdateDate, True, oString)
                    temp.Add(DestObject)
                End With
            End Using

        Next

        Return temp
    End Function

    Function ConvertTo_LTKTTRANSACTIONCASHOUT(ByVal ListSourceObject As TList(Of LTKTTransactionCashOut_ApprovalDetail)) As TList(Of LTKTTransactionCashOut)
        Dim temp As New TList(Of LTKTTransactionCashOut)
        For Each SourceObject As LTKTTransactionCashOut_ApprovalDetail In ListSourceObject
            Using DestObject As LTKTTransactionCashOut = DataRepository.LTKTTransactionCashOutProvider.GetByPK_LTKTTransactionCashOut_Id(SourceObject.PK_LTKTTransactionCashOut_Id)
                If DestObject Is Nothing Then
                    Continue For
                End If
                With DestObject
                    FillOrNothing(.CORP_TipeAlamat, SourceObject.CORP_TipeAlamat, True, oByte)
                    FillOrNothing(.CORP_FK_MsKecamatan_Id, SourceObject.CORP_FK_MsKecamatan_Id, True, oInt)
                    FillOrNothing(.CORP_FK_MsKotaKab_Id, SourceObject.CORP_FK_MsKotaKab_Id, True, oInt)
                    FillOrNothing(.CORP_FK_MsProvince_Id, SourceObject.CORP_FK_MsProvince_Id, True, oInt)
                    FillOrNothing(.CORP_FK_MsNegara_Id, SourceObject.CORP_FK_MsNegara_Id, True, oInt)
                    FillOrNothing(.INDV_FK_MsIDType_Id, SourceObject.INDV_FK_MsIDType_Id, True, oInt)
                    FillOrNothing(.CORP_FK_MsBentukBadanUsaha_Id, SourceObject.CORP_FK_MsBentukBadanUsaha_Id, True, oInt)
                    FillOrNothing(.CORP_FK_MsBidangUsaha_Id, SourceObject.CORP_FK_MsBidangUsaha_Id, True, oInt)
                    FillOrNothing(.CORP_LN_FK_MsNegara_Id, SourceObject.CORP_LN_FK_MsNegara_Id, True, oInt)
                    FillOrNothing(.CORP_LN_FK_MsProvince_Id, SourceObject.CORP_LN_FK_MsProvince_Id, True, oInt)
                    FillOrNothing(.CORP_LN_MsKotaKab_Id, SourceObject.CORP_LN_MsKotaKab_Id, True, oInt)
                    FillOrNothing(.FK_LTKT_Id, SourceObject.FK_LTKT_Id, True, oInt)
                    FillOrNothing(.FK_MsKotaKab_Id, SourceObject.FK_MsKotaKab_Id, True, oInt)
                    FillOrNothing(.FK_MsProvince_Id, SourceObject.FK_MsProvince_Id, True, oInt)
                    FillOrNothing(.INDV_FK_MsNegara_Id, SourceObject.INDV_FK_MsNegara_Id, True, oInt)
                    FillOrNothing(.INDV_DOM_FK_MsKecamatan_Id, SourceObject.INDV_DOM_FK_MsKecamatan_Id, True, oInt)
                    FillOrNothing(.INDV_DOM_FK_MsKotaKab_Id, SourceObject.INDV_DOM_FK_MsKotaKab_Id, True, oInt)
                    FillOrNothing(.INDV_DOM_FK_MsProvince_Id, SourceObject.INDV_DOM_FK_MsProvince_Id, True, oInt)
                    FillOrNothing(.INDV_DOM_FK_MsNegara_Id, SourceObject.INDV_DOM_FK_MsNegara_Id, True, oInt)
                    FillOrNothing(.INDV_ID_FK_MsKecamatan_Id, SourceObject.INDV_ID_FK_MsKecamatan_Id, True, oInt)
                    FillOrNothing(.INDV_ID_FK_MsKotaKab_Id, SourceObject.INDV_ID_FK_MsKotaKab_Id, True, oInt)
                    FillOrNothing(.INDV_ID_FK_MsProvince_Id, SourceObject.INDV_ID_FK_MsProvince_Id, True, oInt)
                    FillOrNothing(.INDV_ID_FK_MsNegara_Id, SourceObject.INDV_ID_FK_MsNegara_Id, True, oInt)
                    FillOrNothing(.INDV_NA_FK_MsNegara_Id, SourceObject.INDV_NA_FK_MsNegara_Id, True, oInt)
                    FillOrNothing(.INDV_NA_FK_MsProvince_Id, SourceObject.INDV_NA_FK_MsProvince_Id, True, oInt)
                    FillOrNothing(.INDV_NA_FK_MsKotaKab_Id, SourceObject.INDV_NA_FK_MsKotaKab_Id, True, oInt)
                    FillOrNothing(.INDV_FK_MsPekerjaan_Id, SourceObject.INDV_FK_MsPekerjaan_Id, True, oInt)
                    FillOrNothing(.INDV_TanggalLahir, SourceObject.INDV_TanggalLahir, True, oDate)
                    FillOrNothing(.TanggalTransaksi, SourceObject.TanggalTransaksi, True, oDate)
                    FillOrNothing(.Total, SourceObject.Total, True, oDecimal)
                    FillOrNothing(.CORP_FK_MsKelurahan_Id, SourceObject.CORP_FK_MsKelurahan_Id, True, OLong)
                    FillOrNothing(.INDV_ID_FK_MsKelurahan_Id, SourceObject.INDV_ID_FK_MsKelurahan_Id, True, OLong)
                    FillOrNothing(.INDV_DOM_FK_MsKelurahan_Id, SourceObject.INDV_DOM_FK_MsKelurahan_Id, True, OLong)
                    FillOrNothing(.INDV_NomorId, SourceObject.INDV_NomorId, True, oString)
                    FillOrNothing(.INDV_NPWP, SourceObject.INDV_NPWP, True, oString)
                    FillOrNothing(.CORP_Nama, SourceObject.CORP_Nama, True, oString)
                    FillOrNothing(.CORP_LN_KodePos, SourceObject.CORP_LN_KodePos, True, oString)
                    FillOrNothing(.CORP_NPWP, SourceObject.CORP_NPWP, True, oString)
                    FillOrNothing(.CORP_TujuanTransaksi, SourceObject.CORP_TujuanTransaksi, True, oString)
                    FillOrNothing(.CORP_SumberDana, SourceObject.CORP_SumberDana, True, oString)
                    FillOrNothing(.CORP_NamaBankLain, SourceObject.CORP_NamaBankLain, True, oString)
                    FillOrNothing(.CORP_NomorRekeningTujuan, SourceObject.CORP_NomorRekeningTujuan, True, oString)
                    FillOrNothing(.CORP_NamaJalan, SourceObject.CORP_NamaJalan, True, oString)
                    FillOrNothing(.CORP_RTRW, SourceObject.CORP_RTRW, True, oString)
                    FillOrNothing(.CORP_KodePos, SourceObject.CORP_KodePos, True, oString)
                    FillOrNothing(.CORP_LN_NamaJalan, SourceObject.CORP_LN_NamaJalan, True, oString)
                    FillOrNothing(.NamaKantorPJK, SourceObject.NamaKantorPJK, True, oString)
                    FillOrNothing(.NomorRekening, SourceObject.NomorRekening, True, oString)
                    FillOrNothing(.INDV_Gelar, SourceObject.INDV_Gelar, True, oString)
                    FillOrNothing(.INDV_NamaLengkap, SourceObject.INDV_NamaLengkap, True, oString)
                    FillOrNothing(.INDV_TempatLahir, SourceObject.INDV_TempatLahir, True, oString)
                    FillOrNothing(.INDV_DOM_NamaJalan, SourceObject.INDV_DOM_NamaJalan, True, oString)
                    FillOrNothing(.INDV_DOM_RTRW, SourceObject.INDV_DOM_RTRW, True, oString)
                    FillOrNothing(.INDV_DOM_KodePos, SourceObject.INDV_DOM_KodePos, True, oString)
                    FillOrNothing(.INDV_ID_NamaJalan, SourceObject.INDV_ID_NamaJalan, True, oString)
                    FillOrNothing(.INDV_ID_RTRW, SourceObject.INDV_ID_RTRW, True, oString)
                    FillOrNothing(.INDV_Jabatan, SourceObject.INDV_Jabatan, True, oString)
                    FillOrNothing(.INDV_PenghasilanRataRata, SourceObject.INDV_PenghasilanRataRata, True, oString)
                    FillOrNothing(.INDV_TempatBekerja, SourceObject.INDV_TempatBekerja, True, oString)
                    FillOrNothing(.INDV_TujuanTransaksi, SourceObject.INDV_TujuanTransaksi, True, oString)
                    FillOrNothing(.INDV_SumberDana, SourceObject.INDV_SumberDana, True, oString)
                    FillOrNothing(.INDV_NamaBankLain, SourceObject.INDV_NamaBankLain, True, oString)
                    FillOrNothing(.INDV_NomorRekeningTujuan, SourceObject.INDV_NomorRekeningTujuan, True, oString)
                    FillOrNothing(.INDV_NA_KodePos, SourceObject.INDV_NA_KodePos, True, oString)
                    FillOrNothing(.INDV_ID_KodePos, SourceObject.INDV_ID_KodePos, True, oString)
                    FillOrNothing(.INDV_NA_NamaJalan, SourceObject.INDV_NA_NamaJalan, True, oString)
                    FillOrNothing(.INDV_Kewarganegaraan, SourceObject.INDV_Kewarganegaraan, True, oString)
                    FillOrNothing(.LastUpdateDate, SourceObject.LastUpdateDate, True, oDate)
                    FillOrNothing(.LastUpdateBy, SourceObject.LastUpdateBy, True, oString)
                End With
                temp.Add(DestObject)
            End Using
        Next

        Return temp
    End Function

    Function ConvertTo_LTKT(ByVal SourceObject As LTKT_ApprovalDetail) As LTKT
        Dim temp As New LTKT
        Using DestObject As LTKT = DataRepository.LTKTProvider.GetByPK_LTKT_Id(SourceObject.PK_LTKT_Id)
            If DestObject Is Nothing Then
                Return Nothing
                Exit Function
            End If
            With DestObject
                ' FillOrNothing(.PK_LTKT_Id, SourceObject.PK_LTKT_Id, True, OLong)
                FillOrNothing(.TipeTerlapor, SourceObject.TipeTerlapor, True, oByte)
                FillOrNothing(.CORP_TipeAlamat, SourceObject.CORP_TipeAlamat, True, oByte)
                FillOrNothing(.CORP_FK_MsBidangUsaha_Id, SourceObject.CORP_FK_MsBidangUsaha_Id, True, oInt)
                FillOrNothing(.INDV_FK_MsIDType_Id, SourceObject.INDV_FK_MsIDType_Id, True, oInt)
                FillOrNothing(.CORP_FK_MsBentukBadanUsaha_Id, SourceObject.CORP_FK_MsBentukBadanUsaha_Id, True, oInt)
                FillOrNothing(.CORP_FK_MsKecamatan_Id, SourceObject.CORP_FK_MsKecamatan_Id, True, oInt)
                FillOrNothing(.CORP_FK_MsKotaKab_Id, SourceObject.CORP_FK_MsKotaKab_Id, True, oInt)
                FillOrNothing(.CORP_FK_MsProvince_Id, SourceObject.CORP_FK_MsProvince_Id, True, oInt)
                FillOrNothing(.CORP_FK_MsNegara_Id, SourceObject.CORP_FK_MsNegara_Id, True, oInt)
                FillOrNothing(.CORP_LN_FK_MsNegara_Id, SourceObject.CORP_LN_FK_MsNegara_Id, True, oInt)
                FillOrNothing(.CORP_LN_FK_MsProvince_Id, SourceObject.CORP_LN_FK_MsProvince_Id, True, oInt)
                FillOrNothing(.CORP_LN_MsKotaKab_Id, SourceObject.CORP_LN_MsKotaKab_Id, True, oInt)
                FillOrNothing(.FK_MsKepemilikan_Id, SourceObject.FK_MsKepemilikan_ID, True, oInt)
                FillOrNothing(.INDV_FK_MsNegara_Id, SourceObject.INDV_FK_MsNegara_Id, True, oInt)
                FillOrNothing(.INDV_DOM_FK_MsKecamatan_Id, SourceObject.INDV_DOM_FK_MsKecamatan_Id, True, oInt)
                FillOrNothing(.INDV_DOM_FK_MsKotaKab_Id, SourceObject.INDV_DOM_FK_MsKotaKab_Id, True, oInt)
                FillOrNothing(.INDV_DOM_FK_MsProvince_Id, SourceObject.INDV_DOM_FK_MsProvince_Id, True, oInt)
                FillOrNothing(.INDV_DOM_FK_MsNegara_Id, SourceObject.INDV_DOM_FK_MsNegara_Id, True, oInt)
                FillOrNothing(.INDV_ID_FK_MsKecamatan_Id, SourceObject.INDV_ID_FK_MsKecamatan_Id, True, oInt)
                FillOrNothing(.INDV_ID_FK_MsKotaKab_Id, SourceObject.INDV_ID_FK_MsKotaKab_Id, True, oInt)
                FillOrNothing(.INDV_ID_FK_MsProvince_Id, SourceObject.INDV_ID_FK_MsProvince_Id, True, oInt)
                FillOrNothing(.INDV_ID_FK_MsNegara_Id, SourceObject.INDV_ID_FK_MsNegara_Id, True, oInt)
                FillOrNothing(.INDV_NA_FK_MsNegara_Id, SourceObject.INDV_NA_FK_MsNegara_Id, True, oInt)
                FillOrNothing(.INDV_NA_FK_MsProvince_Id, SourceObject.INDV_NA_FK_MsProvince_Id, True, oInt)
                FillOrNothing(.INDV_NA_FK_MsKotaKab_Id, SourceObject.INDV_NA_FK_MsKotaKab_Id, True, oInt)
                FillOrNothing(.INDV_FK_MsPekerjaan_Id, SourceObject.INDV_FK_MsPekerjaan_Id, True, oInt)
                FillOrNothing(.INDV_TanggalLahir, SourceObject.INDV_TanggalLahir, True, oDate)
                FillOrNothing(.TanggalLaporan, SourceObject.TanggalLaporan, True, oDate)
                'FillOrNothing(.CIFNo, SourceObject.CIFNo, True, oDecimal)
                FillOrNothing(.CORP_FK_MsKelurahan_Id, SourceObject.CORP_FK_MsKelurahan_Id, True, OLong)
                FillOrNothing(.INDV_ID_FK_MsKelurahan_Id, SourceObject.INDV_ID_FK_MsKelurahan_Id, True, OLong)
                FillOrNothing(.INDV_DOM_FK_MsKelurahan_Id, SourceObject.INDV_DOM_FK_MsKelurahan_Id, True, OLong)
                FillOrNothing(.NoLTKTKoreksi, SourceObject.NoLTKTKoreksi, True, oString)
                FillOrNothing(.INDV_DOM_NamaJalan, SourceObject.INDV_DOM_NamaJalan, True, oString)
                FillOrNothing(.INDV_DOM_RTRW, SourceObject.INDV_DOM_RTRW, True, oString)
                FillOrNothing(.NoRekening, SourceObject.NoRekening, True, oString)
                FillOrNothing(.NamaPJKPelapor, SourceObject.NamaPJKPelapor, True, oString)
                FillOrNothing(.NamaPejabatPJKPelapor, SourceObject.NamaPejabatPJKPelapor, True, oString)
                FillOrNothing(.INDV_Gelar, SourceObject.INDV_Gelar, True, oString)
                FillOrNothing(.INDV_NamaLengkap, SourceObject.INDV_NamaLengkap, True, oString)
                FillOrNothing(.INDV_TempatLahir, SourceObject.INDV_TempatLahir, True, oString)
                FillOrNothing(.INDV_Jabatan, SourceObject.INDV_Jabatan, True, oString)
                FillOrNothing(.INDV_PenghasilanRataRata, SourceObject.INDV_PenghasilanRataRata, True, oString)
                FillOrNothing(.INDV_TempatBekerja, SourceObject.INDV_TempatBekerja, True, oString)
                FillOrNothing(.INDV_NA_KodePos, SourceObject.INDV_NA_KodePos, True, oString)
                FillOrNothing(.INDV_ID_KodePos, SourceObject.INDV_ID_KodePos, True, oString)
                FillOrNothing(.INDV_NA_NamaJalan, SourceObject.INDV_NA_NamaJalan, True, oString)
                FillOrNothing(.INDV_DOM_KodePos, SourceObject.INDV_DOM_KodePos, True, oString)
                FillOrNothing(.INDV_ID_NamaJalan, SourceObject.INDV_ID_NamaJalan, True, oString)
                FillOrNothing(.INDV_ID_RTRW, SourceObject.INDV_ID_RTRW, True, oString)
                FillOrNothing(.CORP_NamaJalan, SourceObject.CORP_NamaJalan, True, oString)
                FillOrNothing(.CORP_RTRW, SourceObject.CORP_RTRW, True, oString)
                FillOrNothing(.CORP_Nama, SourceObject.CORP_Nama, True, oString)
                FillOrNothing(.INDV_NomorId, SourceObject.INDV_NomorId, True, oString)
                FillOrNothing(.INDV_NPWP, SourceObject.INDV_NPWP, True, oString)
                FillOrNothing(.CORP_LN_KodePos, SourceObject.CORP_LN_KodePos, True, oString)
                FillOrNothing(.CORP_NPWP, SourceObject.CORP_NPWP, True, oString)
                FillOrNothing(.CORP_KodePos, SourceObject.CORP_KodePos, True, oString)
                FillOrNothing(.CORP_LN_NamaJalan, SourceObject.CORP_LN_NamaJalan, True, oString)
                FillOrNothing(.JenisLaporan, SourceObject.JenisLaporan, True, oString)
                FillOrNothing(.INDV_Kewarganegaraan, SourceObject.INDV_Kewarganegaraan, True, oString)
                FillOrNothing(.CreatedDate, SourceObject.CreatedDate, True, oDate)
                FillOrNothing(.CreatedBy, SourceObject.CreatedBy, True, oString)
                FillOrNothing(.LastUpdateDate, Date.Now, True, oDate)
            End With
            temp = DestObject
        End Using

        Return temp
    End Function

    Function ConvertTo_LTKTDETAILCASHINTRANSACTION(ByVal ListSourceObject As TList(Of LTKTDetailCashInTransaction_ApprovalDetail)) As TList(Of LTKTDetailCashInTransaction)
        Dim temp As New TList(Of LTKTDetailCashInTransaction)
        For Each SourceObject As LTKTDetailCashInTransaction_ApprovalDetail In ListSourceObject

            Using DestObject As LTKTDetailCashInTransaction = DataRepository.LTKTDetailCashInTransactionProvider.GetByPK_LTKTDetailCashInTransaction_Id(SourceObject.PK_LTKTDetailCashInTransaction_Id)
                If DestObject Is Nothing Then
                    Continue For
                End If
                With DestObject
                    FillOrNothing(.Asing_FK_MsCurrency_Id, SourceObject.Asing_FK_MsCurrency_Id, True, oInt)
                    FillOrNothing(.Asing_KursTransaksi, SourceObject.Asing_KursTransaksi, True, oDecimal)
                    FillOrNothing(.Asing_TotalKasMasukDalamRupiah, SourceObject.Asing_TotalKasMasukDalamRupiah, True, oDecimal)
                    FillOrNothing(.TotalKasMasuk, SourceObject.TotalKasMasuk, True, oDecimal)
                    FillOrNothing(.KasMasuk, SourceObject.KasMasuk, True, oDecimal)
                    'FillOrNothing(.PK_LTKTDetailCashInTransaction_Id, SourceObject.PK_LTKTDetailCashInTransaction_Id, True, OLong)
                    FillOrNothing(.FK_LTKTTransactionCashIn_Id, SourceObject.FK_LTKTTransactionCashIn_Id, True, OLong)
                    temp.Add(DestObject)
                End With
            End Using

        Next


        Return temp
    End Function

    Function ConvertTo_LTKTDETAILCASHOUTTRANSACTION(ByVal ListSourceObject As TList(Of LTKTDetailCashOutTransaction_ApprovalDetail)) As TList(Of LTKTDetailCashOutTransaction)
        Dim temp As New TList(Of LTKTDetailCashOutTransaction)
        For Each SourceObject As LTKTDetailCashOutTransaction_ApprovalDetail In ListSourceObject
            Using DestObject As LTKTDetailCashOutTransaction = DataRepository.LTKTDetailCashOutTransactionProvider.GetByPK_LTKTDetailCashOutTransaction_Id(SourceObject.PK_LTKTDetailCashOutTransaction_Id)
                If DestObject Is Nothing Then
                    Continue For
                End If
                With DestObject
                    FillOrNothing(.Asing_FK_MsCurrency_Id, SourceObject.Asing_FK_MsCurrency_Id, True, oInt)
                    FillOrNothing(.Asing_KursTransaksi, SourceObject.Asing_KursTransaksi, True, oDecimal)
                    FillOrNothing(.Asing_TotalKasKeluarDalamRupiah, SourceObject.Asing_TotalKasKeluarDalamRupiah, True, oDecimal)
                    FillOrNothing(.TotalKasKeluar, SourceObject.TotalKasKeluar, True, oDecimal)
                    FillOrNothing(.KasKeluar, SourceObject.KasKeluar, True, oDecimal)
                    ' FillOrNothing(.PK_LTKTDetailCashOutTransaction_Id, SourceObject.PK_LTKTDetailCashOutTransaction_Id, True, OLong)
                    FillOrNothing(.FK_LTKTTransactionCashOut_Id, SourceObject.FK_LTKTTransactionCashOut_Id, True, OLong)
                    temp.Add(DestObject)
                End With
            End Using


        Next

        Return temp
    End Function

#End Region

    

End Class

