<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CustomerCTRDelete.aspx.vb" Inherits="CustomerCTRDelete" %>


<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

	<script src="Script/popcalendar.js"></script>
	

	<script language="javascript" type="text/javascript">
	    function hidePanel(objhide, objpanel, imgmin, imgmax) {
	        document.getElementById(objhide).style.display = 'none';
	        document.getElementById(objpanel).src = imgmax;
	    }
	    // JScript File

	    function popWin2() {
	        var height = '600px';
	        var width = '550px';
	        var left = (screen.availWidth - width) / 2;
	        var top = (screen.availHeight - height) / 2;
	        var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

	        window.showModalDialog("PickerKelurahan.aspx", "#1", winSetting);

	    }


	    function popWinKecamatan() {
	        var height = '600px';
	        var width = '550px';
	        var left = (screen.availWidth - width) / 2;
	        var top = (screen.availHeight - height) / 2;
	        var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

	        window.showModalDialog("PickerKecamatan.aspx", "#2", winSetting);
	    }
	    function popWinNegara() {
	        var height = '600px';
	        var width = '550px';
	        var left = (screen.availWidth - width) / 2;
	        var top = (screen.availHeight - height) / 2;
	        var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

	        window.showModalDialog("PickerNegara.aspx", "#3", winSetting);
	    }
	    function popWinMataUang() {
	        var height = '600px';
	        var width = '550px';
	        var left = (screen.availWidth - width) / 2;
	        var top = (screen.availHeight - height) / 2;
	        var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

	        window.showModalDialog("PickerMataUang.aspx", "#4", winSetting);

	    }
	    function popWinProvinsi() {
	        var height = '600px';
	        var width = '550px';
	        var left = (screen.availWidth - width) / 2;
	        var top = (screen.availHeight - height) / 2;
	        var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

	        window.showModalDialog("PickerProvinsi.aspx", "#5", winSetting);
	    }
	    function popWinPekerjaan() {
	        var height = '600px';
	        var width = '550px';
	        var left = (screen.availWidth - width) / 2;
	        var top = (screen.availHeight - height) / 2;
	        var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

	        window.showModalDialog("PickerPekerjaan.aspx", "#6", winSetting);
	    }
	    function popWinKotaKab() {
	        var height = '600px';
	        var width = '550px';
	        var left = (screen.availWidth - width) / 2;
	        var top = (screen.availHeight - height) / 2;
	        var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

	        window.showModalDialog("PickerKotaKab.aspx", "#7", winSetting);
	    }
	    function popWinBidangUsaha() {
	        var height = '600px';
	        var width = '550px';
	        var left = (screen.availWidth - width) / 2;
	        var top = (screen.availHeight - height) / 2;
	        var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

	        window.showModalDialog("PickerBidangUsaha.aspx", "#8", winSetting);
	    }
	</script>

	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td>
				<img src="Images/blank.gif" width="5" height="1" /></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td>
							</td>
						<td width="99%" bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" /></td>
						<td bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" /></td>
					</tr>
				</table>
			</td>
			<td>
				<img src="Images/blank.gif" width="5" height="1" /></td>
		</tr>
		<tr>
			<td>
			</td>
			<td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
				<div id="divcontent" >
					<table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td bgcolor="#ffffff" class="divcontentinside" colspan="2">
								<img src="Images/blank.gif" width="20" height="100%" /><ajax:AjaxPanel ID="a" runat="server">
								</ajax:AjaxPanel>
								<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" border="0"
									style="border-top-style: none; border-right-style: none; border-left-style: none;
									border-bottom-style: none">
									<tr>
										<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
											border-right-style: none; border-left-style: none; border-bottom-style: none">
											<img src="Images/dot_title.gif" width="17" height="17">
											<strong>
												<asp:Label ID="Label1" runat="server" Text="Customer CTR - Delete"></asp:Label></strong>
											<hr />
										</td>
									</tr>
									<tr>
										<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
											border-right-style: none; border-left-style: none; border-bottom-style: none">
											<ajax:AjaxPanel ID="AjaxPanel1" runat="server">
												<asp:Label ID="LblSucces" CssClass="validationok" Width="94%" runat="server" Visible="False"></asp:Label>
											</ajax:AjaxPanel>

										</td>
									</tr>
                                   
								</table>
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											&nbsp;</td>
									</tr>
								</table>
								<ajax:AjaxPanel ID="AjaxMultiView" runat="server" Width="100%">
									<asp:MultiView ID="mtvPage" runat="server" ActiveViewIndex="0">
										<asp:View ID="vwTransaksi" runat="server">
											<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
												border="0">
												<tbody>
                                                     <tr class="formText">
                                                        <td bgcolor="#ffffff" style="width: 500px; border-top-style: none; border-right-style: none;
                                                            border-left-style: none; height: 24px; border-bottom-style: none">
                                                            &nbsp;<asp:Image ID="Image2" runat="server" ImageUrl="~/Images/validationsign_animate.gif" />
                                                            &nbsp;<asp:Label ID="Label4" runat="server" Text="The following Data will be deleted :"></asp:Label>
                                                        </td>
                                                    </tr>

													<tr>
														<td style="height: 6px" valign="middle" align="left" width="100%" background="Images/search-bar-background.gif">
															<table cellspacing="0" cellpadding="0" border="0">
																<tbody>
																	<tr>
																		<td class="formtext">
																			<asp:Label ID="Label2" runat="server" Text="IDENTITAS " Font-Bold="True"></asp:Label>&nbsp;</td>
																		<td>
																			&nbsp;</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
													<tr id="Id">
														<td bgcolor="#ffffff" colspan="2">
															<ajax:AjaxPanel ID="ajxPnl" runat="server" Width="100%">
<table width="100%">
																									<tbody>
																										<tr>
																											<td class="formText">
																												Tipe Customer</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:RadioButtonList ID="rblTipePelapor" runat="server" AutoPostBack="True"
																													RepeatDirection="Horizontal" Enabled="False">
																													<asp:ListItem Value="1">Perorangan</asp:ListItem>
																													<asp:ListItem Value="2">Korporasi</asp:ListItem>
																												</asp:RadioButtonList>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext" colspan="3">
																												<div id="divPerorangan" runat="server" hidden="false">
																													<table>
																														<tbody>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<strong>Perorangan</strong>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Gelar</td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:TextBox ID="txtGelar" runat="server" CssClass="textBox" Enabled="False"></asp:TextBox></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Nama Lengkap <span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:TextBox ID="txtNamaLengkap" runat="server" CssClass="textBox" 
                                                                                                                                        Width="370px" Enabled="False"></asp:TextBox></td>
																															</tr>
																															<tr>
                                                                                                                                <td class="formText">
                                                                                                                                    Nama Alias</td>
                                                                                                                                <td style="width: 5px">
                                                                                                                                    :</td>
                                                                                                                                <td class="formtext">
                                                                                                                                    <asp:TextBox ID="txtNamaAlias" runat="server" CssClass="textBox" Width="370px" 
                                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                            </tr>
																															<tr>
																																<td class="formText">
																																	Tempat Lahir <span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:TextBox ID="txtTempatLahir" runat="server" CssClass="textBox" 
                                                                                                                                        Width="240px" Enabled="False"></asp:TextBox></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Tanggal Lahir <span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<br />
																																	<asp:TextBox ID="txtTglLahir" runat="server" CssClass="textBox" Width="96px"
																																		MaxLength="50" Enabled="False"></asp:TextBox></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Kewarganegaraan (Pilih salah satu) <span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:RadioButtonList ID="rblKewarganegaraan" runat="server" AutoPostBack="True"
																																		RepeatDirection="Horizontal" Enabled="False">
																																		<asp:ListItem Value="1">WNI</asp:ListItem>
																																		<asp:ListItem Value="2">WNA</asp:ListItem>
																																	</asp:RadioButtonList></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Negara <span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:DropDownList ID="cboNegara" runat="server" CssClass="combobox" 
                                                                                                                                        Enabled="False">
																																	</asp:DropDownList></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Alamat Domisili</td>
																																<td style="width: 5px">
																																</td>
																																<td class="formtext">
																																</td>
																															</tr>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<table>
																																		<tbody>
																																			<tr>
																																				<td class="formtext">
																																					&nbsp; &nbsp;&nbsp;
																																				</td>
																																				<td class="formText">
																																					Nama Jalan</td>
																																				<td style="width: 5px">
																																					:
																																				</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtDOMNamaJalan" runat="server" CssClass="textBox" 
                                                                                                                                                        Width="370px" Enabled="False"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					RT</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtDOMRT" runat="server" CssClass="textBox" Enabled="False"></asp:TextBox></td>
																																			</tr>
																																			<tr>
                                                                                                                                                <td class="formtext">
                                                                                                                                                    &nbsp;</td>
                                                                                                                                                <td class="formText">
                                                                                                                                                    RW</td>
                                                                                                                                                <td style="width: 5px">
                                                                                                                                                    :</td>
                                                                                                                                                <td class="formtext">
                                                                                                                                                    <asp:TextBox ID="txtDOMRW" runat="server" CssClass="textBox" Enabled="False"></asp:TextBox>
                                                                                                                                                </td>
                                                                                                                                            </tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kelurahan</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext" valign="top">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtDOMKelurahan" runat="server" CssClass="textbox" Width="167px"
																																										Enabled="False" ajaxcall="none"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kecamatan</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					&nbsp;<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtDOMKecamatan" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kota / Kabupaten<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtDOMKotaKab" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Provinsi<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtDOMProvinsi" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
                                                                                                                                            <tr>
                                                                                                                                                <td class="formtext">
                                                                                                                                                </td>
                                                                                                                                                <td class="formText">
                                                                                                                                                    Negara<span style="color: #cc0000">*</span></td>
                                                                                                                                                <td style="width: 5px">
                                                                                                                                                    :</td>
                                                                                                                                                <td class="formtext">
                                                                                                                                                    <table style="width: 127px">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td>
                                                                                                                                                                    <asp:TextBox ID="txtDOMNegara" runat="server" CssClass="textBox" Enabled="False"
                                                                                                                                                                        Width="167px"></asp:TextBox></td>
                                                                                                                                                                <td>
                                                                                                                                                                    &nbsp;</td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kode Pos</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtDOMKodePos" runat="server" CssClass="textBox" 
                                                                                                                                                        Enabled="False"></asp:TextBox></td>
                                                                                                                                            </tr>
                                                                                                                                        
																																		</tbody>
																																	</table>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Alamat Sesuai Bukti Identitas</td>
																																<td style="width: 5px">
																																</td>
																																<td class="formtext">
																																	<asp:CheckBox ID="chkCopyAlamatDOM" runat="server" Text="Sama dengan Alamat Domisili"
																																		AutoPostBack="True" Enabled="False"></asp:CheckBox></td>
																															</tr>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<table>
																																		<tbody>
																																			<tr>
																																				<td class="formText">
																																					&nbsp; &nbsp;&nbsp;
																																				</td>
																																				<td class="formText">
																																					Nama Jalan</td>
																																				<td style="width: 5px">
																																					:
																																				</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtIDNamaJalan" runat="server" CssClass="textBox" 
                                                                                                                                                        Width="370px" Enabled="False"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					RT</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtIDRT" runat="server" CssClass="textBox" Enabled="False"></asp:TextBox></td>
																																			</tr>
																																			<tr>
                                                                                                                                                <td class="formText">
                                                                                                                                                    &nbsp;</td>
                                                                                                                                                <td class="formText">
                                                                                                                                                    RW</td>
                                                                                                                                                <td style="width: 5px">
                                                                                                                                                    :</td>
                                                                                                                                                <td class="formtext">
                                                                                                                                                    <asp:TextBox ID="txtIDRW" runat="server" CssClass="textBox" Enabled="False"></asp:TextBox>
                                                                                                                                                </td>
                                                                                                                                            </tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Kelurahan</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext" valign="top">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtIDKelurahan" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Kecamatan</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					&nbsp;<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtIDKecamatan" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Kota / Kabupaten<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtIDKotaKab" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Provinsi<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td style="height: 22px">
																																									<asp:TextBox ID="txtIDProvinsi" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td style="height: 22px">
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
                                                                                                                                            <tr>
                                                                                                                                                <td class="formtext">
                                                                                                                                                </td>
                                                                                                                                                <td class="formText">
                                                                                                                                                    Negara<span style="color: #cc0000">*</span></td>
                                                                                                                                                <td style="width: 5px">
                                                                                                                                                    :</td>
                                                                                                                                                <td class="formtext">
                                                                                                                                                    <table style="width: 127px">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td>
                                                                                                                                                                    <asp:TextBox ID="txtIDNegara" runat="server" CssClass="textBox" 
                                                                                                                                                                        Enabled="False" Width="167px"></asp:TextBox>
                                                                                                                                                                </td>
                                                                                                                                                                <td>
                                                                                                                                                                    &nbsp;</td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Kode Pos</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtIDKodePos" runat="server" CssClass="textBox" 
                                                                                                                                                        Enabled="False"></asp:TextBox></td>
                                                                                                                                            </tr>
																																		</tbody>
																																	</table>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Alamat Sesuai Negara Asal</td>
																																<td style="width: 5px">
																																</td>
																																<td>
																																</td>
																															</tr>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<table>
																																		<tbody>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Nama Jalan</td>
																																				<td style="width: 5px">
																																					:
																																				</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtNANamaJalan" runat="server" CssClass="textBox" 
                                                                                                                                                        Width="370px" Enabled="False"></asp:TextBox></td>
																																			    <td class="formtext">
                                                                                                                                                    &nbsp;</td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																					&nbsp; &nbsp;&nbsp;
																																				</td>
																																				<td class="formText">
																																					Negara<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtNANegara" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			    <td class="formtext" style="font-style: italic">
                                                                                                                                                    &nbsp;</td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Provinsi</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext" valign="top">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtNAProvinsi" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			    <td class="formtext" valign="top">
                                                                                                                                                    &nbsp;</td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Kota
																																				</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtNAKota" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			    <td class="formtext">
                                                                                                                                                    &nbsp;</td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Kode Pos<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtNAKodePos" runat="server" CssClass="textBox" 
                                                                                                                                                        Enabled="False"></asp:TextBox></td>
																																			    <td class="formtext" style="font-style: italic">
                                                                                                                                                    &nbsp;</td>
																																			</tr>
																																		</tbody>
																																	</table>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Jenis Dokumen Identitas<span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<ajax:AjaxPanel ID="ajPnlDokId" runat="server">
																																		<asp:DropDownList runat="server" ID="cboJenisDocID" CssClass="combobox" 
                                                                                                                                            Enabled="False">
																																		</asp:DropDownList>
																																	</ajax:AjaxPanel>
																																</td>
																															</tr>
																															<tr>
																																<td id="tdNomorId" class="formtext" colspan="3" runat="server">
																																	<table>
																																		<tbody>
																																			<tr>
																																				<td class="formText">
																																					Nomor Identitas <span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td style="width: 260px" class="formtext">
																																					<asp:TextBox ID="txtNomorID" runat="server" CssClass="textBox" Width="257px" 
                                                                                                                                                        Enabled="False"></asp:TextBox>
																																				</td>
																																			</tr>
																																		</tbody>
																																	</table>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Nomor Pokok Wajib Pajak (NPWP)</td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:TextBox ID="txtNPWP" runat="server" CssClass="textBox" Width="257px" 
                                                                                                                                        Enabled="False"></asp:TextBox></td>
																															</tr>
																															<tr>
																																<td style="height: 15px" class="formText">
																																	Pekerjaan</td>
																																<td style="width: 5px; height: 15px">
																																</td>
																																<td style="height: 15px" class="formtext">
																																</td>
																															</tr>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<table>
																																		<tbody>
																																			<tr>
																																				<td class="formtext">
																																					&nbsp; &nbsp;&nbsp;
																																				</td>
																																				<td class="formText">
																																					Pekerjaan<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td style="width: 260px" class="formtext">
																																					<table>
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtPekerjaan" runat="server" CssClass="textBox" Width="257px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Jabatan</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td style="width: 260px" class="formtext">
																																					<asp:TextBox ID="txtJabatan" runat="server" CssClass="textBox" Width="257px" 
                                                                                                                                                        Enabled="False"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Penghasilan rata-rata/th (Rp)</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td style="width: 260px" class="formtext">
																																					<asp:TextBox ID="txtPenghasilanRataRata" runat="server" CssClass="textBox"
																																						Width="257px" Enabled="False"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Tempat kerja</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td style="width: 260px" class="formtext">
																																					<asp:TextBox ID="txtTempatKerja" runat="server" CssClass="textBox" 
                                                                                                                                                        Width="257px" Enabled="False"></asp:TextBox></td>
																																			</tr>
																																		</tbody>
																																	</table>
																																</td>
																															</tr>
																														</tbody>
																													</table>
																												</div>
																												<div id="divKorporasi" runat="server" hidden="true">
																													<table>
																														<tbody>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<strong>Korporasi</strong></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Bentuk Badan Usaha<span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:DropDownList ID="cboCORPBentukBadanUsaha" runat="server" 
                                                                                                                                        CssClass="combobox" Enabled="False">
																																	</asp:DropDownList></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Nama Korporasi <span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:TextBox ID="txtCORPNama" runat="server" CssClass="textBox" Width="370px" 
                                                                                                                                        Enabled="False"></asp:TextBox></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Bidang Usaha Korporasi<span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<table style="width: 127px">
																																		<tbody>
																																			<tr>
																																				<td>
																																					<asp:TextBox ID="txtCORPBidangUsaha" runat="server" CssClass="textBox" Width="167px"
																																						Enabled="False"></asp:TextBox></td>
																																				<td>
																																					&nbsp;</td>
																																			</tr>
																																		</tbody>
																																	</table>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Alamat Korporasi<span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:RadioButtonList ID="rblCORPTipeAlamat" runat="server" AutoPostBack="True"
																																		RepeatDirection="Horizontal" Enabled="False">
																																		<asp:ListItem>Dalam Negeri</asp:ListItem>
																																		<asp:ListItem>Luar Negeri</asp:ListItem>
																																	</asp:RadioButtonList></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Alamat Lengkap Korporasi</td>
																																<td style="width: 5px">
																																</td>
																																<td class="formtext">
																																</td>
																															</tr>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<table>
																																		<tbody>
																																			<tr>
																																				<td class="formtext">
																																					&nbsp; &nbsp;&nbsp;
																																				</td>
																																				<td class="formText">
																																					Nama Jalan</td>
																																				<td style="width: 5px">
																																					:
																																				</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtCORPDLNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					RT</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtCORPDLRT" runat="server" CssClass="textBox" Enabled="False"></asp:TextBox></td>
																																			</tr>
																																			<tr>
                                                                                                                                                <td class="formtext">
                                                                                                                                                    &nbsp;</td>
                                                                                                                                                <td class="formText">
                                                                                                                                                    RW</td>
                                                                                                                                                <td style="width: 5px">
                                                                                                                                                    :</td>
                                                                                                                                                <td class="formtext">
                                                                                                                                                    <asp:TextBox ID="txtCORPDLRW" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                                                </td>
                                                                                                                                            </tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kelurahan</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext" valign="top">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtCORPDLKelurahan" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kecamatan</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					&nbsp;<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtCORPDLKecamatan" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kota / Kabupaten<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtCORPDLKotaKab" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kode Pos</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtCORPDLKodePos" runat="server" CssClass="textBox" 
                                                                                                                                                        Enabled="False"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Provinsi<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtCORPDLProvinsi" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Negara<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtCORPDLNegara" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
																																		</tbody>
																																	</table>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Alamat Korporasi Luar Negeri</td>
																																<td style="width: 5px">
																																</td>
																																<td class="formtext">
																																</td>
																															</tr>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<table>
																																		<tbody>
																																			<tr>
																																				<td class="formtext">
																																					&nbsp; &nbsp;&nbsp;
																																				</td>
																																				<td style="width: 69px" class="formText">
																																					Nama Jalan</td>
																																				<td style="width: 5px">
																																					:
																																				</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtCORPLNNamaJalan" runat="server" CssClass="textBox" 
                                                                                                                                                        Width="370px" Enabled="False"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td style="width: 69px" class="formText">
																																					Negara<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtCORPLNNegara" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td style="width: 69px" class="formText">
																																					Provinsi</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext" valign="top">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td style="height: 22px">
																																									<asp:TextBox ID="txtCORPLNProvinsi" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td style="height: 22px">
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td style="width: 69px" class="formText">
																																					Kota
																																				</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtCORPLNKota" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									&nbsp;</td>
																																							</tr>
																																						</tbody>
																																					</table>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td style="width: 69px" class="formText">
																																					Kode Pos<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtCORPLNKodePos" runat="server" CssClass="textBox" 
                                                                                                                                                        Enabled="False"></asp:TextBox></td>
																																			</tr>
																																		</tbody>
																																	</table>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	Nomor Pokok Wajib Pajak (NPWP)</td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:TextBox ID="txtCORPNPWP" runat="server" CssClass="textBox" Width="257px" 
                                                                                                                                        Enabled="False"></asp:TextBox></td>
																															</tr>
																														</tbody>
																													</table>
																												</div>
																											</td>
																										</tr>
																									</tbody>
																								</table>
															
															</ajax:AjaxPanel>
														</td>
													</tr>
													<tr>
														<td style="height: 6px" valign="middle" align="left" width="100%" background="Images/search-bar-background.gif">
															<table cellspacing="0" cellpadding="0" border="0">
																<tbody>
																	<tr>
																		<td class="formtext">
																			<asp:Label ID="Label3" runat="server" Text="TRANSAKSI" Font-Bold="True"></asp:Label>&nbsp;</td>
																		<td>
																			&nbsp;</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
													<tr id="Transaksi">
														<td bgcolor="#ffffff" colspan="2">
																										<table>
                                             
                                               
                                                <tr>
                                                    <td bgcolor="#ffffff" colspan="2">
                                                        <asp:DataGrid ID="GridViewTransaction" runat="server" AllowPaging="True" 
                                                            AllowSorting="True" AutoGenerateColumns="False" BackColor="White" 
                                                            BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                            Font-Size="XX-Small" Font-Strikeout="False" Font-Underline="False" 
                                                            ForeColor="Black" GridLines="Vertical" HorizontalAlign="Left" Width="100%">
                                                            <AlternatingItemStyle BackColor="White" />
                                                            <Columns>
                                                                <asp:BoundColumn DataField="PK_CustomerCTRTransaction_ID" Visible="False">
                                                                    <HeaderStyle Width="0%" />
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="FK_TujuanTransaksi_ID" Visible="False">
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="FK_RelasiDenganPemilikRekening_ID" Visible="False">
                                                                </asp:BoundColumn>
                                                                <asp:TemplateColumn HeaderText="No.">
                                                                    <HeaderStyle ForeColor="White" />
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="TransactionDate" DataFormatString="{0:yyyy-MM-dd}" 
                                                                    HeaderText="Transaction Date">
                                                                     <HeaderStyle ForeColor="White" />
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="TransactionType" HeaderText="Transaction Type"> <HeaderStyle ForeColor="White" /></asp:BoundColumn>
                                                                <asp:BoundColumn DataField="TransactionTicketNumber" 
                                                                    HeaderText="TransactionTicketNumber"> <HeaderStyle ForeColor="White" /></asp:BoundColumn>
                                                                <asp:TemplateColumn HeaderText="Tujuan Transaksi"> <HeaderStyle ForeColor="White" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LblTujuanTransaksi" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="TextBox1" runat="server" 
                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.NamaTujuanTransaksi") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Relasi Dengan Pemilik Rekening"> <HeaderStyle ForeColor="White" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LblRelasiDenganPemilikRekening" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="TextBox2" runat="server" 
                                                                            Text='<%# DataBinder.Eval(Container, "DataItem.NamaRelasiDenganPemilikRekening") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="CreatedDate" DataFormatString="{0:yyyy-MM-dd}" 
                                                                    HeaderText="Input Date"> <HeaderStyle ForeColor="White" />
                                                                    
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="LastUpdateDate" HeaderText="Update Date" 
                                                                    DataFormatString="{0:yyyy-MM-dd}"> <HeaderStyle ForeColor="White" /></asp:BoundColumn>
                                                                <asp:ButtonColumn CommandName="Edit" Text="Edit" Visible="False"></asp:ButtonColumn>
                                                                <asp:ButtonColumn CommandName="Delete" Text="Delete" Visible="False"></asp:ButtonColumn>
                                                            </Columns>
                                                            <FooterStyle BackColor="#CCCC99" />
                                                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                            <ItemStyle BackColor="#F7F7DE" />
                                                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" 
                                                                Mode="NumericPages" Visible="False" />
                                                            <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                        </asp:DataGrid>
                                                    </td>
                                                </tr>

                                            </table></td>
													</tr>
												</tbody>
											</table>
										</asp:View>
										<asp:View ID="vwMessage" runat="server">
											<table width="100%" style="horiz-align: center;">
												<tr>
													<td class="formtext" align="center">
														<asp:Label runat="server" ID="lblMsg"></asp:Label>
													</td>
												</tr>
												<tr>
													<td class="formtext" align="center">
														<asp:ImageButton ID="imgOKMsg" runat="server"  ImageUrl="~/Images/button/Ok.gif" />
													</td>
												</tr>
											</table>
										</asp:View>
									</asp:MultiView>
								</ajax:AjaxPanel>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
				<ajax:AjaxPanel ID="AjaxPanel5" runat="server">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td background="Images/button-bground.gif" align="left" valign="middle">
								<img src="Images/blank.gif" width="5" height="1" /></td>
							<td background="Images/button-bground.gif" align="left" valign="middle">
								<img src="images/arrow.gif" width="15" height="15" />&nbsp;</td>
							<td background="Images/button-bground.gif" style="width: 5px">
								&nbsp;</td>
							<td background="Images/button-bground.gif">
								<asp:ImageButton ID="ImageDelete" runat="server"  
                                    ImageUrl="~/Images/button/delete.gif">
								</asp:ImageButton>&nbsp;</td>
							<td background="Images/button-bground.gif" style="width: 62px">
								<asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" 
									ImageUrl="~/Images/Button/Cancel.gif"></asp:ImageButton>
							</td>
							<td width="99%" background="Images/button-bground.gif">
								<img src="Images/blank.gif" width="1" height="1" /></td>
							<td>
								</td>
						</tr>
					</table>
				</ajax:AjaxPanel>
			</td>
		</tr>
	</table>
	<asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
