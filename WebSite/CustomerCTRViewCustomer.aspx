<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CustomerCTRViewCustomer.aspx.vb" Inherits="CustomerCTRViewCustomer" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <script src="Script/popcalendar.js"></script>
    <script language="javascript" type="text/javascript">
        function hidePanel(objhide, objpanel, imgmin, imgmax) {
            document.getElementById(objhide).style.display = 'none';
            document.getElementById(objpanel).src = imgmax;
        }
        // JScript File

    </script>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                        </td>
                        <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td bgcolor="#ffffff" class="divcontentinside" colspan="2">
                                <img src="Images/blank.gif" width="20" height="100%" />
                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" border="0"
                                    style="border-top-style: none; border-right-style: none; border-left-style: none;
                                    border-bottom-style: none">
                                    <tr>
                                        <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                            <img src="Images/dot_title.gif" width="17" height="17">
                                            <strong>
                                                <asp:Label ID="Label1" runat="server" Text="Customer CTR - View Customer"></asp:Label></strong>
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                            <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                                <asp:Label ID="LblSucces" CssClass="validationok" Width="94%" runat="server" Visible="False"></asp:Label>
                                            </ajax:AjaxPanel>
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <ajax:AjaxPanel ID="AjaxMultiView" runat="server" Width="100%">
                                    <asp:MultiView runat="server" ID="mtvPage" ActiveViewIndex="0">
                                        <asp:View runat="server" ID="vwTransaksi">
                                            <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" border="0"
                                                bgcolor="#dddddd">
                                                <tr>
                                                    <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                        style="height: 6px">
                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td class="formtext">
                                                                    <asp:Label ID="Label2" runat="server" Text="IDENTITAS " Font-Bold="True"></asp:Label>&nbsp;
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr id="IdTerlapor">
                                                    <td bgcolor="#ffffff" colspan="2">
                                                        <ajax:AjaxPanel runat="server" ID="ajxPnlTerlapor" Width="100%">
                                                            <table width="100%">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="width: 100px" class="formText">
                                                                            Nomor CIF
                                                                        </td>
                                                                        <td style="width: 5px">
                                                                            :
                                                                        </td>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="TxtNomorCIF" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formText" style="width: 100px">
                                                                            Tipe Customer
                                                                        </td>
                                                                        <td style="width: 5px">
                                                                            :
                                                                        </td>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="txtTipeCustomer" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtext" colspan="3">
                                                                            <div id="divPerorangan" runat="server" hidden="false">
                                                                                <table>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="formtext" colspan="3">
                                                                                                <strong>Perorangan</strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formText">
                                                                                                Nama Lengkap
                                                                                            </td>
                                                                                            <td style="width: 5px">
                                                                                                :
                                                                                            </td>
                                                                                            <td class="formtext">
                                                                                                <asp:Label ID="txtNamaLengkap" runat="server" Width="370px"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formText">
                                                                                                Tempat Lahir
                                                                                            </td>
                                                                                            <td style="width: 5px">
                                                                                                :
                                                                                            </td>
                                                                                            <td class="formtext">
                                                                                                <asp:Label ID="txtTempatLahir" runat="server" Width="240px"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formText">
                                                                                                Tanggal Lahir
                                                                                            </td>
                                                                                            <td style="width: 5px">
                                                                                                :
                                                                                            </td>
                                                                                            <td class="formtext">
                                                                                                <asp:Label ID="txtTglLahir" runat="server" Width="96px" MaxLength="50"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formText">
                                                                                                Jenis Dokumen Identitas
                                                                                            </td>
                                                                                            <td style="width: 5px">
                                                                                                :
                                                                                            </td>
                                                                                            <td class="formtext">
                                                                                                <asp:Label ID="txtIDType" runat="server" MaxLength="50" Width="96px"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formText">
                                                                                                Nomor Identitas
                                                                                            </td>
                                                                                            <td style="width: 5px">
                                                                                                :
                                                                                            </td>
                                                                                            <td style="width: 260px" class="formtext">
                                                                                                <asp:Label ID="txtNomorID" runat="server" Width="257px"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formText">
                                                                                                Alamat
                                                                                            </td>
                                                                                            <td style="width: 5px">
                                                                                            </td>
                                                                                            <td class="formtext">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formtext" colspan="3">
                                                                                                <table>
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                                &nbsp; &nbsp;&nbsp;
                                                                                                            </td>
                                                                                                            <td class="formText">
                                                                                                                Alamat
                                                                                                            </td>
                                                                                                            <td style="width: 5px">
                                                                                                                :
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                <asp:Label ID="txtDOMAlamat1" runat="server" Width="370px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="formText">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td style="width: 5px">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                <asp:Label ID="txtDOMAlamat2" runat="server" Width="370px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="formText">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td style="width: 5px">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                <asp:Label ID="txtDOMAlamat3" runat="server" Width="370px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="formText">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td style="width: 5px">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                <asp:Label ID="txtDOMAlamat4" runat="server" Width="370px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                            </td>
                                                                                                            <td class="formText">
                                                                                                                Kode Pos
                                                                                                            </td>
                                                                                                            <td style="width: 5px">
                                                                                                                :
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                <asp:Label ID="txtDOMKodePos" runat="server"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <div id="divKorporasi" runat="server" hidden="true">
                                                                                <table>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="formtext" colspan="3">
                                                                                                <strong>Korporasi</strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formText">
                                                                                                Nama Korporasi
                                                                                            </td>
                                                                                            <td style="width: 5px">
                                                                                                :
                                                                                            </td>
                                                                                            <td class="formtext">
                                                                                                <asp:Label ID="txtCORPNama" runat="server" Width="370px"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formText">
                                                                                                Customer Sub Type
                                                                                            </td>
                                                                                            <td style="width: 5px">
                                                                                                :
                                                                                            </td>
                                                                                            <td class="formtext">
                                                                                                <table style="width: 127px">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="txtCORPCustomerSubType" runat="server" Width="167px"></asp:Label>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formText">
                                                                                                Business Type
                                                                                            </td>
                                                                                            <td style="width: 5px">
                                                                                                :
                                                                                            </td>
                                                                                            <td class="formtext">
                                                                                                <asp:Label ID="txtCORPBusinessType" runat="server" Width="167px"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formText">
                                                                                                Industry Code
                                                                                            </td>
                                                                                            <td style="width: 5px">
                                                                                                :
                                                                                            </td>
                                                                                            <td class="formtext">
                                                                                                <asp:Label ID="txtCORPIndustryCode" runat="server" Width="167px"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formText">
                                                                                                Jenis Dokumen Identitas
                                                                                            </td>
                                                                                            <td style="width: 5px">
                                                                                                :
                                                                                            </td>
                                                                                            <td class="formtext">
                                                                                                <asp:Label ID="txtCORPIDType" runat="server" MaxLength="50" Width="96px"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formText">
                                                                                                Nomor Identitas
                                                                                            </td>
                                                                                            <td style="width: 5px">
                                                                                                :
                                                                                            </td>
                                                                                            <td style="width: 260px" class="formtext">
                                                                                                <asp:Label ID="txtCORPIDNumber" runat="server" Width="257px"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formText">
                                                                                                Alamat Lengkap Korporasi
                                                                                            </td>
                                                                                            <td style="width: 5px">
                                                                                            </td>
                                                                                            <td class="formtext">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formtext" colspan="3">
                                                                                                <table>
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                                &nbsp; &nbsp;&nbsp;
                                                                                                            </td>
                                                                                                            <td class="formText">
                                                                                                                Nama Jalan
                                                                                                            </td>
                                                                                                            <td style="width: 5px">
                                                                                                                :
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                <asp:Label ID="txtCORPDLAlamat1" runat="server" Width="370px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="formText">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td style="width: 5px">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                <asp:Label ID="txtCORPDLAlamat2" runat="server" Width="370px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="formText">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td style="width: 5px">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                <asp:Label ID="txtCORPDLAlamat3" runat="server" Width="370px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="formText">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td style="width: 5px">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                <asp:Label ID="txtCORPDLAlamat4" runat="server" Width="370px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                            <td class="formText">
                                                                                                                Kode Pos
                                                                                                            </td>
                                                                                                            <td style="width: 5px">
                                                                                                                :
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                <asp:Label ID="txtCORPDLKodePos" runat="server" Width="370px"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 6px" valign="middle" align="left" width="100%" background="Images/search-bar-background.gif">
                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="formtext">
                                                                        <asp:Label ID="Label4" runat="server" Text="TRANSAKSI" Font-Bold="True"></asp:Label>&nbsp;
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr id="Transaksi">
                                                    <td bgcolor="#ffffff" colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" id="TblEditTransaksi" runat="server">
                                                                        <tr>
                                                                            <td width="25%">
                                                                                Tanggal Transaksi<span style="color: #cc0000">*</span>
                                                                            </td>
                                                                            <td width="1px">
                                                                                :
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TxtTanggalTransaksi" runat="server" CssClass="searcheditbox"></asp:TextBox><input
                                                                                    id="popUpTanggalTransaksi" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                                                    border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                                                    border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                                                    width: 16px;" type="button" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="25%">
                                                                                Tipe Transaksi<span style="color: #cc0000">*</span>
                                                                            </td>
                                                                            <td width="1px">
                                                                                :
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButtonList ID="RblTipeTransaksi" runat="server" RepeatDirection="Horizontal">
                                                                                    <asp:ListItem Value="D">Tarik</asp:ListItem>
                                                                                    <asp:ListItem Value="C">Setor</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="25%">
                                                                                Nomor Tiket Transaksi<span style="color: #cc0000">*</span>
                                                                            </td>
                                                                            <td width="1px">
                                                                                :
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TxtNomorTiketTransaksi" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" Width="246px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="25%">
                                                                                Tujuan Transaksi<span style="color: #cc0000">*</span>
                                                                            </td>
                                                                            <td width="1px">
                                                                                :
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="DdlTujuanTransaksi" runat="server" CssClass="searcheditcbo"
                                                                                    AutoPostBack="True">
                                                                                </asp:DropDownList>
                                                                                <asp:TextBox ID="TxtOtherTujuanTransaksi" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" Visible="False" Width="246px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="25%">
                                                                                Relasi dengan Pemilik Rekening<span style="color: #cc0000">*</span>
                                                                            </td>
                                                                            <td width="1px">
                                                                                :
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="DdlRelasiDenganPemilikRekening" runat="server" CssClass="searcheditcbo"
                                                                                    AutoPostBack="True">
                                                                                </asp:DropDownList>
                                                                                <asp:TextBox ID="TxtOtherRelasiDenganPemilikRekening" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" Visible="False" Width="246px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="25%">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td width="1px">
                                                                                &nbsp;
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="25%" colspan="3">
                                                                                <asp:ImageButton ID="BtnSave" runat="server" ImageUrl="~/Images/button/save.gif" />
                                                                                <asp:ImageButton ID="BtnCancel" runat="server" ImageUrl="~/Images/button/cancel.gif" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="TrNewTrx" runat="server">
                                                                <td>
                                                                    <asp:ImageButton ID="BtnNewTrx" runat="server" ImageUrl="~/Images/button/newdata.gif" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td bgcolor="#ffffff" colspan="2">
                                                                    <asp:DataGrid ID="GridViewTransaction" runat="server" AllowPaging="True" AllowSorting="True"
                                                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None"
                                                                        BorderWidth="1px" CellPadding="4" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                        Font-Size="XX-Small" Font-Strikeout="False" Font-Underline="False" ForeColor="Black"
                                                                        GridLines="Vertical" HorizontalAlign="Left" Width="100%">
                                                                        <AlternatingItemStyle BackColor="White" />
                                                                        <Columns>
                                                                            <asp:BoundColumn DataField="PK_CustomerCTRTransaction_ID" Visible="False">
                                                                                <HeaderStyle Width="0%" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="FK_TujuanTransaksi_ID" Visible="False"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="FK_RelasiDenganPemilikRekening_ID" Visible="False"></asp:BoundColumn>
                                                                            <asp:TemplateColumn HeaderText="No.">
                                                                                <HeaderStyle ForeColor="White" />
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn DataField="TransactionDate" DataFormatString="{0:yyyy-MM-dd}" HeaderText="Transaction Date"
                                                                                SortExpression="TransactionDate  desc">
                                                                                <HeaderStyle ForeColor="White" />
                                                                            </asp:BoundColumn>
                                                                            <asp:TemplateColumn HeaderText="Transaction Type" SortExpression="TransactionType  desc">
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TransactionType") %>'></asp:TextBox>
                                                                                </EditItemTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="LblTransactiontype" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TransactionType") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                    Font-Underline="False" ForeColor="White" />
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn DataField="TransactionTicketNumber" HeaderText="TransactionTicketNumber"
                                                                                SortExpression="TransactionTicketNumber  desc">
                                                                                <HeaderStyle ForeColor="White" />
                                                                            </asp:BoundColumn>
                                                                            <asp:TemplateColumn HeaderText="Tujuan Transaksi">
                                                                                <HeaderStyle ForeColor="White" />
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="LblTujuanTransaksi" runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.NamaTujuanTransaksi") %>'></asp:TextBox>
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Relasi Dengan Pemilik Rekening">
                                                                                <HeaderStyle ForeColor="White" />
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="LblRelasiDenganPemilikRekening" runat="server"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.NamaRelasiDenganPemilikRekening") %>'></asp:TextBox>
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn DataField="CreatedDate" DataFormatString="{0:yyyy-MM-dd}" HeaderText="Input Date"
                                                                                SortExpression="CreatedDate  desc">
                                                                                <HeaderStyle ForeColor="White" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="LastUpdateDate" DataFormatString="{0:yyyy-MM-dd}" HeaderText="Update Date"
                                                                                SortExpression="LastUpdateDate  desc">
                                                                                <HeaderStyle ForeColor="White" />
                                                                            </asp:BoundColumn>
                                                                            <asp:ButtonColumn CommandName="Edit" Text="Edit"></asp:ButtonColumn>
                                                                            <asp:TemplateColumn>
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="delbutton" runat="server" CausesValidation="false" CommandName="Delete"
                                                                                        OnClick="delbutton_Click" Text="Delete"></asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                        </Columns>
                                                                        <FooterStyle BackColor="#CCCC99" />
                                                                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                        <ItemStyle BackColor="#F7F7DE" />
                                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
                                                                        <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                    </asp:DataGrid>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                        <asp:View runat="server" ID="vwMessage">
                                            <table width="100%" style="horiz-align: center;">
                                                <tr>
                                                    <td class="formtext" align="center">
                                                        <asp:Label runat="server" ID="lblMsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="formtext" align="center">
                                                        <asp:ImageButton ID="imgOKMsg" runat="server" ImageUrl="~/Images/button/Ok.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </asp:MultiView>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td background="Images/button-bground.gif" align="left" valign="middle">
                                <img src="Images/blank.gif" width="5" height="1" />
                            </td>
                            <td background="Images/button-bground.gif" align="left" valign="middle">
                                <img src="images/arrow.gif" width="15" height="15" />&nbsp;
                            </td>
                            <td background="Images/button-bground.gif" style="width: 5px">
                                &nbsp;
                            </td>
                            <td background="Images/button-bground.gif">
                                <asp:ImageButton ID="ImageSave" runat="server" ImageUrl="~/Images/Button/Save.gif" />
                            </td>
                            <td background="Images/button-bground.gif">
                                <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" ImageUrl="~/Images/button/back.gif">
                                </asp:ImageButton>&nbsp;
                            </td>
                            <td width="99%" background="Images/button-bground.gif">
                                <img src="Images/blank.gif" width="1" height="1" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </ajax:AjaxPanel>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
