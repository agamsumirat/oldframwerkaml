Partial Class CashTransactionReport_FilePrint
    Inherits System.Web.UI.Page


    Private ReadOnly Property PK_CTRID() As String
        Get
            Return Request.Params("PK_CTRID")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.reportViewer_content.ShowPrintButton = True
        Me.reportViewer_content.LocalReport.Refresh()
        Me.reportViewer_content.DataBind()
    End Sub
End Class
