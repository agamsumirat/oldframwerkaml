<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PopupCDDQuestion.aspx.vb"
    Inherits="PopupCDDQuestion" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>:: Anti Money Laundering ::</title>
    <base target="_self" />
    <link href="theme/aml.css" rel="stylesheet" type="text/css" />
    <link href="theme/popcalendar.css" type="text/css" rel="stylesheet" />
    <link href="Theme/TabCss.css" rel="stylesheet" type="text/css" />

    <script language="javascript">
        function GetRowValues(val)
        {
            // hardcoded value used to minimize the code.
            // ControlID can instead be passed as query string to the popup window
            window.opener.document.getElementById('ctl00_cpContent_textBranch').value = val;
            //window.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
            height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
            border-bottom-style: none" width="100%">
            <tr>
                <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                    border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/dot_title.gif" />
                    <strong>CDD Question&nbsp;</strong></td>
            </tr>
        </table>
        <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
            border="2">
            <tr>
                <td align="left" bgcolor="#eeeeee">
                    <img height="15" src="images/arrow.gif" width="15"></td>
                <td valign="top" width="98%" bgcolor="#ffffff">
                    <table cellspacing="4" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="Regtext" nowrap>
                                Search By :
                            </td>
                            <td valign="middle" nowrap>
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <asp:DropDownList ID="ComboSearch" TabIndex="1" runat="server" Width="120px" CssClass="searcheditcbo">
                                    </asp:DropDownList>&nbsp;
                                    <asp:TextBox ID="TextSearch" TabIndex="2" runat="server" CssClass="searcheditbox"></asp:TextBox></ajax:AjaxPanel>
                            </td>
                            <td valign="middle" width="99%">
                                <ajax:AjaxPanel ID="AjaxPanel2" runat="server">
                                    <asp:ImageButton ID="ImageSearchButton" TabIndex="3" runat="server" SkinID="SearchButton"
                                        ImageUrl="~/Images/button/search.gif"></asp:ImageButton></ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                    <ajax:AjaxPanel ID="AjaxPanel14" runat="server">
                        <asp:CustomValidator ID="CustomValidator1" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
                </td>
            </tr>
        </table>
        <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
            border="2">
            <tr>
                <td bgcolor="#ffffff">
                    <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                        <asp:DataGrid ID="GridView" runat="server" AllowCustomPaging="True" AutoGenerateColumns="False"
                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                            CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="100%" AllowSorting="True">
                            <FooterStyle BackColor="#CCCC99" />
                            <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <AlternatingItemStyle BackColor="White" />
                            <ItemStyle BackColor="#F7F7DE" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <Columns>
                                <asp:BoundColumn DataField="PK_CDD_Question_ID" SortExpression="PK_CDD_Question_ID desc"
                                    Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="CDD_Question" SortExpression="CDD_Question desc" HeaderText="Question">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="QuestionTypeName" HeaderText="Question Type" SortExpression="QuestionTypeName desc">
                                </asp:BoundColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LnkSelect" runat="server" CausesValidation="false" CommandName="Select"
                                            Text="Select"></asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle Width="5%" />
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE"
                                Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                    </ajax:AjaxPanel>
                </td>
            </tr>
            <tr>
                <td bgcolor="#ffffff">
                    <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                        bgcolor="#ffffff" border="2">
                        <tr class="regtext" align="center" bgcolor="#dddddd">
                            <td valign="top" align="left" width="50%" bgcolor="#ffffff">
                                Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                    <asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>
                                    &nbsp;of&nbsp;
                                    <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label>
                                </ajax:AjaxPanel>
                            </td>
                            <td valign="top" align="right" width="50%" bgcolor="#ffffff">
                                Total Records&nbsp;
                                <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                    <asp:Label ID="PageTotalRows" runat="server">0</asp:Label>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                    <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                        bgcolor="#ffffff" border="2">
                        <tr bgcolor="#ffffff">
                            <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                                <hr color="#f40101" noshade size="1">
                            </td>
                        </tr>
                        <tr>
                            <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                                Go to page</td>
                            <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                                <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                    <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                        <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox>
                                    </ajax:AjaxPanel>
                                </font>
                            </td>
                            <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                                <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                    <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton" ImageUrl="~/Images/button/go.gif">
                                    </asp:ImageButton>
                                </ajax:AjaxPanel>
                            </td>
                            <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                <img height="5" src="images/first.gif" width="6">
                            </td>
                            <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                    <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                        OnCommand="PageNavigate">First</asp:LinkButton>
                                </ajax:AjaxPanel>
                            </td>
                            <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                <img height="5" src="images/prev.gif" width="6"></td>
                            <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
                                <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                    <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                        OnCommand="PageNavigate">Previous</asp:LinkButton>
                                </ajax:AjaxPanel>
                            </td>
                            <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
                                <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                    <a class="pageNav" href="#">
                                        <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                            OnCommand="PageNavigate">Next</asp:LinkButton>
                                    </a>
                                </ajax:AjaxPanel>
                            </td>
                            <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                <img height="5" src="images/next.gif" width="6"></td>
                            <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
                                <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                    <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                        OnCommand="PageNavigate">Last</asp:LinkButton>
                                </ajax:AjaxPanel>
                            </td>
                            <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                <img height="5" src="images/last.gif" width="6"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
            </ajax:AjaxPanel>
        </table>
    </form>
</body>
</html>
