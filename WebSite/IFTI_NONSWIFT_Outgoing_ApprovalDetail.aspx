﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="IFTI_NONSWIFT_Outgoing_ApprovalDetail.aspx.vb" Inherits="IFTI_NONSWIFT_Outgoing_ApprovalDetail" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <div id="divcontent" class="divcontent">
    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
              <td bgcolor="White">
                <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">
                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                        <asp:View ID="ViewNonSwiftOutAppDet" runat="server">
                                <table id="TblSearchNonSwiftOutcoming" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <img src="Images/dot_title.gif" width="17" height="17" />
                                                <strong>
                                                    <asp:Label ID="Label5" runat="server" Text="IFTI Non SWIFT Outgoing - Approval Detail "></asp:Label>
                                                </strong>
                                                <hr />
                                             </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ajax:AjaxPanel ID="AjaxPanel2" runat="server" Width="885px" meta:resourcekey="AjaxPanel1Resource1">
																	<table style="height: 100%">
																		<tr>
																			<td colspan="3" style="height: 7px">
																			</td>
																		</tr>
																		<tr>
																			<td nowrap style="height: 26px; width: 107px;">
																				<asp:Label ID="LabelMsUser_StaffName" runat="server" Text="User"></asp:Label>
																			</td>
																			<td style="width: 19px;">
																				:
																			</td>
																			<td style="height: 26px; width: 765px;">
																				<asp:TextBox ID="txtMsUser_StaffName" runat="server" CssClass="searcheditbox" TabIndex="2"
																					Width="308px" Enabled="False"></asp:TextBox>
																			</td>
																		</tr>
																		<tr>
																			<td nowrap style="height: 26px; width: 107px;">
																				<asp:Label ID="LabelRequestedDate" runat="server" Text="Requested Date"></asp:Label>
																			</td>
																			<td style="width: 19px;">
																				:
																			</td>
																			<td style="height: 26px; width: 765px;">
																				<asp:TextBox ID="txtRequestedDate1" runat="server" CssClass="textBox" MaxLength="1000"
																					TabIndex="2" Width="308px" ToolTip="RequestedDate" Enabled="False"></asp:TextBox>
																			</td>
																		</tr>
																		
																		<tr>
																			<td colspan="3">
																			</td>
																		</tr>
																		<tr>
																			<td colspan="3">
																				&nbsp;&nbsp;&nbsp;
																				</td>
																		</tr>
																	</table>
											    </ajax:AjaxPanel>
                                            </td>
                                        </tr>
                                </table>
                                <table id="UMUM" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                    width="100%" bgcolor="#dddddd" border="2">
                                    <tr>
                                        <td background="Images/search-bar-background.gif" valign="middle" align="left" style="height: 6px;
                                            width: 100%;" width="100%">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="formtext">
                                                        <asp:Label ID="Label25" runat="server" Text="A. Umum" Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <a href="#" onclick="javascript:ShowHidePanel('NonSwiftOut_Umum','Img13','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                            title="click to minimize or maximize">
                                                            <img id="Img13" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="NonSwiftOut_Umum">
                                        <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;" width="100%">
                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <tr>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                        <asp:Label ID="Label13" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                        <asp:Label ID="Label22" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                        <asp:Label ID="Label24" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label27" runat="server" Text="a. No. LTDLN "></asp:Label><asp:Label
                                                            ID="Label28" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="height: 22px; width: 15%;">
                                                        <asp:TextBox ID="NonSwiftOutUmum_LTDNOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="NonSwiftOutUmum_LTDNNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label50" runat="server" Text="b. No. LTDLN Koreksi " Width="135px"></asp:Label></td>
                                                    <td style="width: 15%; height: 22px;">
                                                        <asp:TextBox ID="NonSwiftOutUmum_LtdnKoreksiOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="NonSwiftOutUmum_LtdnKoreksiNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label66" runat="server" Text="c. Tanggal Laporan "></asp:Label><asp:Label
                                                            ID="Label67" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="width: 15%; height: 22px;">
                                                        <asp:TextBox ID="NonSwiftOutUmum_TanggalLaporanOld" runat="server" CssClass="searcheditbox"
                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="NonSwiftOutUmum_TanggalLaporanNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label68" runat="server" Text="d. Nama PJK Bank Pelapor " Width="155px"></asp:Label>
                                                        <asp:Label ID="Label72" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="width: 15%; height: 22px;">
                                                        <asp:TextBox ID="NonSwiftOutUmum_NamaPJKBankOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="248px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="NonSwiftOutUmum_NamaPJKBankNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="248px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label73" runat="server" Text="e. Nama Pejabat PJK Bank Pelapor" Width="215px"></asp:Label>
                                                        <asp:Label ID="Label74" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="height: 22px; width: 15%;">
                                                        <asp:TextBox ID="NonSwiftOutUmum_NamaPejabatPJKBankOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="NonSwiftOutUmum_NamaPejabatPJKBankNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label75" runat="server" Text="f. Jenis Laporan"></asp:Label>
                                                        <asp:Label ID="Label76" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="width: 15%; height: 22px;" align="left">
                                                        <asp:RadioButtonList ID="RbNonSwiftOutUmum_JenisLaporanOld" runat="server" RepeatColumns="2"
                                                            RepeatDirection="Horizontal" Enabled="False">
                                                            <asp:ListItem Value="1">Baru</asp:ListItem>
                                                            <asp:ListItem Value="2">Recall</asp:ListItem>
                                                            <asp:ListItem Value="3">Koreksi</asp:ListItem>
                                                            <asp:ListItem Value="4">Reject</asp:ListItem>
                                                        </asp:RadioButtonList></td>
                                                    <td align="left" style="width: 20%">
                                                        <asp:RadioButtonList ID="RbNonSwiftOutUmum_JenisLaporanNew" runat="server" RepeatColumns="2"
                                                            RepeatDirection="Horizontal" Enabled="False">
                                                            <asp:ListItem Value="1">Baru</asp:ListItem>
                                                            <asp:ListItem Value="2">Recall</asp:ListItem>
                                                            <asp:ListItem Value="3">Koreksi</asp:ListItem>
                                                            <asp:ListItem Value="4">Reject</asp:ListItem>
                                                        </asp:RadioButtonList></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; height: 22px; background-color: #fff7e6;">
                                                    </td>
                                                    <td style="width: 15%; height: 22px;">
                                                    </td>
                                                    <td style="width: 20%">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table id="PENGIRIM" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                    width="100%" bgcolor="#dddddd" border="2">
                                    <tr>
                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                            style="height: 6px">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="formtext">
                                                        <asp:Label ID="Label10" runat="server" Text="B Identitas Pengirim / Pengirim Asal"
                                                            Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <a href="#" onclick="javascript:ShowHidePanel('BNonSWIFTOutIdentitasPengirim','Img8','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                            title="click to minimize or maximize">
                                                            <img id="Img8" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="NonSWIFTOutIdentitasPengirim">
                                        <td valign="top" bgcolor="#ffffff" style="height: 125px" width="100%">
                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                            </table>
                                            <table width="100%">
                                                <tr>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                        <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                        <asp:Label ID="Label36" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                        <asp:Label ID="Label37" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 27px;">
                                                        <asp:Label ID="Label190" runat="server" Text="No. Rek " Width="59px"></asp:Label>
                                                        <asp:Label ID="Label191" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="width: 15%; height: 27px;" align="left">
                                                        <asp:TextBox ID="TxtNonSWIFTOutPengirim_rekeningOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%;" align="left">
                                                        <asp:TextBox ID="TxtNonSWIFTOutPengirim_rekeningNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="trNonSWIFTOutPengirimTipePengirim" runat="server">
                                                    <td style="width: 15%; background-color: #fff7e6; height: 27px;">
                                                        <asp:Label ID="Label192" runat="server" Text="       "></asp:Label>
                                                        <asp:Label ID="Label193" runat="server" Text="       Tipe Pengirim"></asp:Label>
                                                        <asp:Label ID="Label194" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%; height: 27px;" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Height="16px">
                                                            <asp:RadioButtonList ID="RbPengirimNasabah_TipePengirimOld" runat="server" RepeatDirection="Horizontal"
                                                                AutoPostBack="True" Width="195px" Enabled="False">
                                                                <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                            </asp:RadioButtonList></ajax:AjaxPanel></td>
                                                    <td style="width: 20%;" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel3" runat="server" Height="16px">
                                                            <asp:RadioButtonList ID="RbPengirimNasabah_TipePengirimNew" runat="server" RepeatDirection="Horizontal"
                                                                AutoPostBack="True" Width="195px" Enabled="False">
                                                                <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                            </asp:RadioButtonList></ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                                <tr id="trNonSWIFTOutPengirimTipeNasabah" runat="server" visible="False">
                                                    <td style="width: 15%; height: 27px; background-color: #fff7e6">
                                                        <asp:Label ID="Label9256" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                        <asp:Label ID="Label9257" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%; height: 27px" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel779" runat="server">
                                                            <asp:RadioButtonList ID="Rb_IdenPengirimNas_TipeNasabahOld" runat="server" AutoPostBack="True"
                                                                RepeatDirection="Horizontal" Enabled="False">
                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                    <td style="width: 20%;" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                                                            <asp:RadioButtonList ID="Rb_IdenPengirimNas_TipeNasabahNew" runat="server" AutoPostBack="True"
                                                                RepeatDirection="Horizontal" Enabled="False">
                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:MultiView ID="MultiViewNonSWIFTOutIdenPengirim" runat="server" ActiveViewIndex="0">
                                                <asp:View ID="ViewNonSWIFTOutIdenPengirimNasabah" runat="server">
                                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                        bgcolor="#dddddd" border="2">
                                                        <tr>
                                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                style="height: 6px">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Label11" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <a href="#" onclick="javascript:ShowHidePanel('BNonSWIFTOutIdentitasPengirim1nasabah','Imgs9','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                title="click to minimize or maximize">
                                                                                <img id="Imgs9" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="BNonSWIFTOutIdentitasPengirim1nasabah">
                                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                </table>
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                    <tr>
                                                                        <td style="width: 100%">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100%">
                                                                            <asp:MultiView ID="MultiViewPengirimNasabah" runat="server" ActiveViewIndex="0">
                                                                                <asp:View ID="ViewPengirimNasabah_IND" runat="server">
                                                                                    <table style="width: 100%; height: 49px">
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="Label30" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                <asp:Label ID="Label31" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="height: 15px; width: 15%;">
                                                                                            </td>
                                                                                           <%-- <td style="height: 15px; width: 35px;">
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                <asp:Label ID="Label38" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                            </td>
                                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                <asp:Label ID="Label39" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                            </td>
                                                                                           <%-- <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                <asp:Label ID="Label41" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label29" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                <asp:Label ID="Label263" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_NamaLengkapOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                          <%--  <td style="width: 35px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_NamaLengkapNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label32" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="136px"></asp:Label>
                                                                                                <asp:Label ID="Label264" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_tanggalLahirOld" runat="server" CssClass="searcheditbox"
                                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                         <%--   <td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_tanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                &nbsp;</td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label33" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                    Width="184px"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:RadioButtonList ID="Rb_IdenPengirimNas_Ind_kewarganegaraanOld" runat="server"
                                                                                                    RepeatDirection="Horizontal" Enabled="False">
                                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                </asp:RadioButtonList>
                                                                                            </td>
                                                                                         <%--   <td style="width: 222px">
                                                                                                <asp:RadioButtonList ID="Rb_IdenPengirimNas_Ind_kewarganegaraanNew" runat="server"
                                                                                                    RepeatDirection="Horizontal" Enabled="False">
                                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                </asp:RadioButtonList>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label34" runat="server" Text="d. Negara "></asp:Label>
                                                                                                <br />
                                                                                                <asp:Label ID="Label40" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                    Width="216px"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_negaraOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_negaraOld" runat="server" />
                                                                                            </td>
                                                                                         <%--   <td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_negaraNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_negaraNew" runat="server" />
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label47" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_negaralainOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <%--<td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_negaralainNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label35" runat="server" Text="e. Pekerjaan"></asp:Label>
                                                                                                <asp:Label ID="Label266" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_pekerjaanOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_pekerjaanOld" runat="server" />
                                                                                            </td>
                                                                                            <%--<td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_pekerjaanNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_pekerjaanNew" runat="server" />
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelu45" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_PekerjaanlainOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <%--<td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_PekerjaanlainNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labely37" runat="server" Text="f. Alamat Domisili"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                            </td>
                                                                                            <%--<td style="width: 35px">
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labely38" runat="server" Text="Alamat"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_AlamatOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                            <%--<td style="width: 222px; height: 20px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_AlamatNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labelo39" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                                           <td style="height: 15px; width: 15%;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_KotaOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_Kota_domOld" runat="server" />
                                                                                            </td>
                                                                                           <%-- <td style="height: 18px; width: 222px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_KotaNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_Kota_domNew" runat="server" />
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labelk57" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_kotalainOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <%--<td style="width: 222px; height: 18px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_kotalainNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelj41" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_provinsiOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_provinsi_domOld" runat="server" />
                                                                                            </td>
                                                                                            <%--<td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_provinsiNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_provinsi_domNew" runat="server" />
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labeli186" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_provinsiLainOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                           <%-- <td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_provinsiLainNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelj43" runat="server" Text="Alamat"></asp:Label>
                                                                                                <asp:Label ID="Labelj267" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_alamatIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                            <%--<td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_alamatIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelj44" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                                <asp:Label ID="Labelj268" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_kotaIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_kotaIdentitasOld" runat="server" />
                                                                                            </td>
                                                                                           <%-- <td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_kotaIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_kotaIdentitasNew" runat="server" />
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labeli83" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_KotaLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <%--<td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_KotaLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labeol46" runat="server" Text="Provinsi"></asp:Label>
                                                                                                <asp:Label ID="Labeli269" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="height: 15px; width: 15%;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_ProvinsiIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_ProvinsiIdenOld" runat="server" />
                                                                                            </td>
                                                                                            <%--<td style="height: 18px; width: 222px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_ProvinsiIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_ProvinsiIdenNew" runat="server" />
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labeli62" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_ProvinsilainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
<%--                                                                                            <td style="width: 222px; height: 18px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_ProvinsilainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labeli48" runat="server" Text="h. Jenis Dokument Identitas"></asp:Label>
                                                                                                <asp:Label ID="Labeli270" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:DropDownList ID="CboPengirimNasInd_jenisidentitasOld" runat="server" Enabled="False">
                                                                                                </asp:DropDownList></td>
                                                                                           <%-- <td style="width: 222px">
                                                                                                <asp:DropDownList ID="CboPengirimNasInd_jenisidentitasNew" runat="server" Enabled="False">
                                                                                                </asp:DropDownList>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="LabeliI49" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                                <asp:Label ID="LabeliI271" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="height: 15px; width: 15%;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_noIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <%--<td style="width: 222px; height: 18px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_noIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                            </td>
                                                                                            <td style="height: 15px; width: 15%;">
                                                                                            </td>
                                                                                           <%-- <td style="width: 222px; height: 15px;">
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:View>
                                                                                <asp:View ID="ViewPengirimNasabah_Korp" runat="server">
                                                                                    <table style="height: 31px" width="100%">
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="LabelIu51" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 15%;">
                                                                                            </td>
                                                                                        <%--    <td style="height: 15px; width: 20%; color: #000000;">
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                <asp:Label ID="Label42" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                            </td>
                                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                <asp:Label ID="Label43" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                            </td>
                                                                                            <%--<td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                <asp:Label ID="Label44" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labeu9306" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BentukBadanUsahaOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BentukBadanUsahaNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelb9307" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="188px"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BentukBadanUsahaLainOld" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BentukBadanUsahaLainNew" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label5i5" runat="server" Text="b. Nama Korporasi" Width="130px" Height="16px"></asp:Label>
                                                                                                <asp:Label ID="Labeli2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_NamaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_NamaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelr4" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label>
                                                                                                <asp:Label ID="Labelo273" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BidangUsahaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Corp_BidangUsahaKorpOld" runat="server" />
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BidangUsahaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Corp_BidangUsahaKorpNew" runat="server" />
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label8" runat="server" Text="Bidang Usaha Lainnya " Width="151px"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BidangUsahaLainKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BidangUsahaLainKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelk58" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                            <td style="width: 15%">
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px;">
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelk59" runat="server" Text="Alamat"></asp:Label>
                                                                                                <asp:Label ID="Labelk274" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_alamatkorpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="342px" Enabled="False"></asp:TextBox></td>
                                                                                           <%-- <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_alamatkorpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="342px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelk61" runat="server" Text="Kota / Kabupaten "></asp:Label>
                                                                                                <asp:Label ID="Labelk275" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_kotakorpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Corp_kotakorpOld" runat="server" />
                                                                                            </td>
                                                                                           <%-- <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_kotakorpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Corp_kotakorpNew" runat="server" />
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labeli69" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                            <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_kotakorplainOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                           <%-- <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_kotakorplainNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelu63" runat="server" Text="Provinsi"></asp:Label>
                                                                                                <asp:Label ID="Label2u76" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_provKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Corp_provKorpOld" runat="server" />
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_provKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Corp_provKorpNew" runat="server" />
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelu78" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                            <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_provKorpLainOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <%--<td style="width: 20%; height: 15px; color: #000000;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_provKorpLainNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelu64" runat="server" Text="e. Alamat Korporasi di Luar Negeri"></asp:Label></td>
                                                                                            <td style="width: 15%">
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px;">
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelu9258" runat="server" Text="Alamat"></asp:Label></td>
                                                                                            <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_AlamatLuarOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_AlamatLuarNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelu60" runat="server" Text="f. Alamat Cabang Pengirim Asal"></asp:Label></td>
                                                                                            <td style="width: 15%">
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px;">
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelu9259" runat="server" Text="Alamat"></asp:Label></td>
                                                                                            <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_AlamatAsalOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_AlamatAsalNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:View>
                                                                            </asp:MultiView>
                                                                        </td>
                                                                        <td style="width: 100%">
                                                                            <asp:MultiView ID="MultiViewPengirimNasabah_New" runat="server" ActiveViewIndex="0">
                                                                                <asp:View ID="ViewPengirimNasabah_IND_new" runat="server">
                                                                                    <table style="width: 100%; height: 49px">
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="Label1" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                           <%-- <td style="height: 15px; width: 15%;">
                                                                                            </td>--%>
                                                                                            <td style="height: 15px; width: 35px;">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                            </td>
                                                                                          <%--  <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                <asp:Label ID="Label93" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                            </td>--%>
                                                                                            <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                <asp:Label ID="Label94" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label95" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                <asp:Label ID="Label96" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                           <%-- <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TextBox1" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 35px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_NamaLengkapNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label97" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="136px"></asp:Label>
                                                                                                <asp:Label ID="Label98" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                           <%-- <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TextBox3" runat="server" CssClass="searcheditbox"
                                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                            <td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_tanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label99" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                    Width="184px"></asp:Label></td>
                                                                                           <%-- <td style="width: 15%; height: 15px;">
                                                                                                <asp:RadioButtonList ID="RadioButtonList1" runat="server"
                                                                                                    RepeatDirection="Horizontal" Enabled="False">
                                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                </asp:RadioButtonList>
                                                                                            </td>--%>
                                                                                            <td style="width: 222px">
                                                                                                <asp:RadioButtonList ID="Rb_IdenPengirimNas_Ind_kewarganegaraanNew" runat="server"
                                                                                                    RepeatDirection="Horizontal" Enabled="False">
                                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                </asp:RadioButtonList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label100" runat="server" Text="d. Negara "></asp:Label>
                                                                                                <br />
                                                                                                <asp:Label ID="Label101" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                    Width="216px"></asp:Label>
                                                                                            </td>
                                                                                           <%-- <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TextBox5" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                                                                            </td>--%>
                                                                                            <td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_negaraNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_negaraNew" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label102" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TextBox7" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_negaralainNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label103" runat="server" Text="e. Pekerjaan"></asp:Label>
                                                                                                <asp:Label ID="Label104" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TextBox9" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HiddenField3" runat="server" />
                                                                                            </td>--%>
                                                                                            <td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_pekerjaanNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_pekerjaanNew" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label105" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TextBox11" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_PekerjaanlainNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label106" runat="server" Text="f. Alamat Domisili"></asp:Label></td>
                                                                                           <%-- <td style="width: 15%; height: 15px;">
                                                                                            </td>--%>
                                                                                            <td style="width: 35px">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label107" runat="server" Text="Alamat"></asp:Label>
                                                                                            </td>
                                                                                            <%--<td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TextBox13" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 222px; height: 20px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_AlamatNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="Label108" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                                           <%-- <td style="height: 15px; width: 15%;">
                                                                                                <asp:TextBox ID="TextBox15" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HiddenField5" runat="server" />
                                                                                            </td>--%>
                                                                                            <td style="height: 18px; width: 222px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_KotaNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_Kota_domNew" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="Label109" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                           <%-- <td style="width: 15%; height: 15px">
                                                                                                <asp:TextBox ID="TextBox17" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 222px; height: 18px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_kotalainNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label110" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TextBox19" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HiddenField7" runat="server" />
                                                                                            </td>--%>
                                                                                            <td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_provinsiNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_provinsi_domNew" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labeli186_new" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_provinsiLainOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_provinsiLainNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelj43_new" runat="server" Text="Alamat"></asp:Label>
                                                                                                <asp:Label ID="Labelj267_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_alamatIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_alamatIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelj44_new" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                                <asp:Label ID="Labelj268_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_kotaIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_kotaIdentitasOld" runat="server" />
                                                                                            </td>--%>
                                                                                            <td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_kotaIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_kotaIdentitasNew" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labeli83_new" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; height: 15px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_KotaLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 222px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_KotaLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labeol46_new" runat="server" Text="Provinsi"></asp:Label>
                                                                                                <asp:Label ID="Labeli269_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <%--<td style="height: 15px; width: 15%;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_ProvinsiIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_ProvinsiIdenOld" runat="server" />
                                                                                            </td>--%>
                                                                                            <td style="height: 18px; width: 222px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_ProvinsiIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Ind_ProvinsiIdenNew" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labeli62_new" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                           <%-- <td style="width: 15%; height: 15px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_ProvinsilainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 222px; height: 18px">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_ProvinsilainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labeli48_new" runat="server" Text="h. Jenis Dokument Identitas"></asp:Label>
                                                                                                <asp:Label ID="Labeli270_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                           <%-- <td style="width: 15%; height: 15px;">
                                                                                                <asp:DropDownList ID="CboPengirimNasInd_jenisidentitasOld" runat="server" Enabled="False">
                                                                                                </asp:DropDownList></td>--%>
                                                                                            <td style="width: 222px">
                                                                                                <asp:DropDownList ID="CboPengirimNasInd_jenisidentitasNew" runat="server" Enabled="False">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="LabeliI49_new" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                                <asp:Label ID="LabeliI271_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                           <%-- <td style="height: 15px; width: 15%;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_noIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 222px; height: 18px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Ind_noIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                            </td>
                                                                                            <%--<td style="height: 15px; width: 15%;">
                                                                                            </td>--%>
                                                                                            <td style="width: 222px; height: 15px;">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:View>
                                                                                <asp:View ID="ViewPengirimNasabah_Korp_new" runat="server">
                                                                                    <table style="height: 31px" width="100%">
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="LabelIu51_new" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                            </td>
                                                                                            <%--<td style="width: 15%;">
                                                                                            </td>--%>
                                                                                            <td style="height: 15px; width: 20%; color: #000000;">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                <asp:Label ID="Label42_new" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                            </td>
                                                                                          <%--  <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                <asp:Label ID="Label43" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                            </td>--%>
                                                                                            <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                <asp:Label ID="Label44" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labeu9306_new" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label>
                                                                                            </td>
                                                                                           <%-- <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BentukBadanUsahaOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BentukBadanUsahaNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelb9307_new" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="188px"></asp:Label>
                                                                                            </td>
                                                                                           <%-- <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BentukBadanUsahaLainOld" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BentukBadanUsahaLainNew" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label5i5_new" runat="server" Text="b. Nama Korporasi" Width="130px" Height="16px"></asp:Label>
                                                                                                <asp:Label ID="Labeli2_new" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                            </td>
                                                                                        <%--    <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_NamaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_NamaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelr4_new" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label>
                                                                                                <asp:Label ID="Labelo273_new" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                            </td>
                                                                                            <%--<td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BidangUsahaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Corp_BidangUsahaKorpOld" runat="server" />
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BidangUsahaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Corp_BidangUsahaKorpNew" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label8_new" runat="server" Text="Bidang Usaha Lainnya " Width="151px"></asp:Label>
                                                                                            </td>
                                                                                           <%-- <td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BidangUsahaLainKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_BidangUsahaLainKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelk58_new" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                            <%--<td style="width: 15%">
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelk59_new" runat="server" Text="Alamat"></asp:Label>
                                                                                                <asp:Label ID="Labelk274_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <%--<td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_alamatkorpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="342px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_alamatkorpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="342px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelk61_new" runat="server" Text="Kota / Kabupaten "></asp:Label>
                                                                                                <asp:Label ID="Labelk275_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <%--<td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_kotakorpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Corp_kotakorpOld" runat="server" />
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_kotakorpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Corp_kotakorpNew" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labeli69_new" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                            <%--<td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_kotakorplainOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_kotakorplainNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelu63_new" runat="server" Text="Provinsi"></asp:Label>
                                                                                                <asp:Label ID="Label2u76_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <%--<td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_provKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Corp_provKorpOld" runat="server" />
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_provKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HfIdenPengirimNas_Corp_provKorpNew" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelu78_new" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                            <%--<td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_provKorpLainOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 20%; height: 15px; color: #000000;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_provKorpLainNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelu64_new" runat="server" Text="e. Alamat Korporasi di Luar Negeri"></asp:Label></td>
                                                                                            <%--<td style="width: 15%">
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelu9258_new" runat="server" Text="Alamat"></asp:Label></td>
                                                                                            <%--<td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_AlamatLuarOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_AlamatLuarNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelu60_new" runat="server" Text="f. Alamat Cabang Pengirim Asal"></asp:Label></td>
                                                                                        <%--    <td style="width: 15%">
                                                                                            </td--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Labelu9259_new" runat="server" Text="Alamat"></asp:Label></td>
                                                                                            <%--<td style="width: 15%">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_AlamatAsalOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="TxtIdenPengirimNas_Corp_AlamatAsalNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:View>
                                                                            </asp:MultiView>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View ID="ViewNonSWIFTOutIdenPengirimNonNasabah" runat="server">
                                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                        bgcolor="#dddddd" border="2">
                                                        <tr>
                                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                style="height: 6px">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Label18" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <a href="#" onclick="javascript:ShowHidePanel('BNonSwiftOutIdentitasPengirimNonNasabah','Imgs10','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                title="click to minimize or maximize">
                                                                                <img id="Imgs10" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="BNonSwiftOutIdentitasPengirimNonNasabah">
                                                            <td valign="top" bgcolor="#ffffff" style="height: 125px" width="100%">
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                </table>
                                                                <table style="height: 49px; width: 100%;">
                                                                    <tr>
                                                                        <td style="background-color: #fff7e6;" colspan="3" width="100%" align="center">
                                                                            <asp:RadioButtonList ID="RbPengirimNonNasabah_100Juta" runat="server" RepeatDirection="Horizontal"
                                                                                Enabled="False">
                                                                                <asp:ListItem Value="0">&lt; 100 Juta (Kurs yang digunakan)</asp:ListItem>
                                                                                <asp:ListItem Value="1">&gt; 100 Juta (Kurs yang digunakan)</asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                            <asp:Label ID="Label45" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                        </td>
                                                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                            <asp:Label ID="Label46" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                        </td>
                                                                        <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                            <asp:Label ID="Label48" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labely81" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                            <asp:Label ID="Labely85" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_namaOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_namaNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labele82" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="177px"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_TanggalLahirOld" runat="server" CssClass="searcheditbox"
                                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_TanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            &nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labeil95" runat="server" Text="c. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                        <td style="width: 15%">
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelr96" runat="server" Text="Alamat"></asp:Label></td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_alamatidenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_alamatidenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelr97" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                            <asp:Label ID="Label87" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                        <td style="width: 15%" align="left">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_kotaIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="HfPengirimNonNasabah_kotaIdenOld" runat="server" />
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_kotaIdenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="HfPengirimNonNasabah_kotaIdenNew" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labeli88" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_KotaLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_KotaLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6;">
                                                                            <asp:Label ID="Labeli98" runat="server" Text="Provinsi"></asp:Label>
                                                                            <asp:Label ID="Label277" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                        <td style="width: 15%" align="left">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_ProvIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="HfPengirimNonNasabah_ProvIdenOld" runat="server" />
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_ProvIdenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="HfPengirimNonNasabah_ProvIdenNew" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelo99" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                        <td style="width: 15%;">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_ProvLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%;">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_ProvLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6;">
                                                                            <asp:Label ID="Label1o00" runat="server" Text="d. Jenis Dokument Identitas"></asp:Label>
                                                                            <asp:Label ID="Label89" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                        <td style="width: 15%;">
                                                                            <asp:DropDownList ID="CboPengirimNonNasabah_JenisDokumenOld" runat="server" Enabled="False">
                                                                            </asp:DropDownList></td>
                                                                        <td style="width: 20%;">
                                                                            <asp:DropDownList ID="CboPengirimNonNasabah_JenisDokumenNew" runat="server" Enabled="False">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6;">
                                                                            <asp:Label ID="Labelo101" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                            <asp:Label ID="Label278" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                        <td style="width: 15%;">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_NomorIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%;">
                                                                            <asp:TextBox ID="TxtPengirimNonNasabah_NomorIdenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6;">
                                                                        </td>
                                                                        <td style="width: 15%;">
                                                                        </td>
                                                                        <td style="width: 20%;">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                            </asp:MultiView>
                                        </td>
                                    </tr>
                                </table>
                                <table id="BENEFICIARY OWNER" align="center" bordercolor="#ffffff" cellspacing="1"
                                    cellpadding="2" width="100%" bgcolor="#dddddd" border="2">
                                    <tr>
                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                            style="height: 6px">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="formtext">
                                                        <asp:Label ID="Labelr7" runat="server" Text="C. Beneficiary Owner " Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <a href="#" onclick="javascript:ShowHidePanel('CBeneficiaryOwner','Img5','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                            title="click to minimize or maximize">
                                                            <img id="Img5" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="CBeneficiaryOwner">
                                        <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                            <table style="width: 100%; height: 60px">
                                                <tr>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                        <asp:Label ID="Label51" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                        <asp:Label ID="Label52" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                        <asp:Label ID="Label55" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; height: 82px; background-color: #fff7e6;" bgcolor="#FFF7E6">
                                                        <asp:Label ID="Label20" runat="server" Text="Keterlibatan Dengan Pemilik Dana ( Beneficial Owner )"
                                                            Width="329px"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%; height: 82px;" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel17" runat="server" Width="100%">
                                                            <asp:DropDownList ID="KeterlibatanBenefOld" runat="server" AutoPostBack="True" Enabled="False">
                                                                <asp:ListItem Value="1">Ya</asp:ListItem>
                                                                <asp:ListItem Value="2">Tidak</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                    <td style="width: 20%; height: 82px;" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel18" runat="server" Width="100%">
                                                            <asp:DropDownList ID="KeterlibatanBenefNew" runat="server" AutoPostBack="True" Enabled="False">
                                                                <asp:ListItem Value="1">Ya</asp:ListItem>
                                                                <asp:ListItem Value="2">Tidak</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                                <tr id="row1a" runat="server">
                                                    <td style="width: 15%; background-color: #fff7e6; height: 82px;">
                                                        <asp:Label ID="Labelt162" runat="server" Text="Hubungan dengan Pemilik Dana (Beneficial Owner)"
                                                            Width="310px"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%; height: 82px;">
                                                        <asp:TextBox ID="BenfOwnerHubunganPemilikDanaOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="343px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%; height: 82px;">
                                                        <asp:TextBox ID="BenfOwnerHubunganPemilikDanaNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="343px" Enabled="False"></asp:TextBox></td>
                                                </tr>
                                                <tr id="row2a" runat="server">
                                                    <td style="width: 15%; background-color: #fff7e6; height: 82px;">
                                                        <asp:Label ID="Label9t255" runat="server" Text=" Tipe Pengirim " Width="113px"></asp:Label></td>
                                                    <td style="width: 15%; height: 82px;">
                                                        <ajax:AjaxPanel ID="AjaxPanely6" runat="server">
                                                            <asp:RadioButtonList ID="Rb_BOwnerNas_TipePengirimOld" runat="server" RepeatDirection="Horizontal"
                                                                AutoPostBack="True" Width="189px" Enabled="False">
                                                                <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                    <td style="width: 20%; height: 82px;">
                                                        <ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                                            <asp:RadioButtonList ID="Rb_BOwnerNas_TipePengirimNew" runat="server" RepeatDirection="Horizontal"
                                                                AutoPostBack="True" Width="189px" Enabled="False">
                                                                <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:MultiView ID="MultiViewNonSWIFTOutBOwner" runat="server" ActiveViewIndex="0">
                                                <asp:View ID="ViewNonSWIFTOutBOwnerNasabah" runat="server">
                                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                        bgcolor="#dddddd" border="2" id="TableNonSWIFTOutBOwnerNas">
                                                        <tr>
                                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                style="height: 6px">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Label7" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <a href="#" onclick="javascript:ShowHidePanel('BOwner1nasabah','Img6','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                title="click to minimize or maximize">
                                                                                <img id="Img6" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="BOwner1nasabah">
                                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                </table>
                                                                <table style="width: 100%; height: 49px">
                                                                    <tr>
                                                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                            <asp:Label ID="Label57" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                        </td>
                                                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                            <asp:Label ID="Label58" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                        </td>
                                                                        <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                            <asp:Label ID="Label59" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelf1163" runat="server" Text="a. No. Rek "></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_rekeningOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_rekeningNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labele146" runat="server" Text="b. Nama Lengkap"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_NamaOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_NamaNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Label148" runat="server" Text="c. Tanggal lahir (tgl/bln/thn)" Width="167px"
                                                                                Height="16px"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_tanggalLahirOld" runat="server" CssClass="searcheditbox"
                                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_tanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            &nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labeld150" runat="server" Text="d. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                        <td style="width: 15%">
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelw151" runat="server" Text="Alamat"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_AlamatIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_AlamatIdenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelt152" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_KotaIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="hfBenfOwnerNasabah_KotaIdenOld" runat="server" />
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_KotaIdenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="hfBenfOwnerNasabah_KotaIdenNew" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelt154" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_kotaLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_kotaLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6;">
                                                                            <asp:Label ID="Labelt155" runat="server" Text="Provinsi"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%;">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_ProvinsiIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="hfBenfOwnerNasabah_ProvinsiIdenOld" runat="server" />
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_ProvinsiIdenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="hfBenfOwnerNasabah_ProvinsiIdenNew" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelt156" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                        <td style="width: 15%;">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_ProvinsiLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%;">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_ProvinsiLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelt158" runat="server" Text="e. Jenis Dokument Identitas"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:DropDownList ID="CBOBenfOwnerNasabah_JenisDokumenOld" runat="server" Enabled="False">
                                                                            </asp:DropDownList></td>
                                                                        <td style="width: 20%">
                                                                            <asp:DropDownList ID="CBOBenfOwnerNasabah_JenisDokumenNew" runat="server" Enabled="False">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6;">
                                                                            <asp:Label ID="Labelt160" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%;">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_NomorIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%;">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_NomorIdenMew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6;">
                                                                        </td>
                                                                        <td style="width: 15%;">
                                                                        </td>
                                                                        <td style="width: 20%;">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View ID="ViewNonSWIFTOutBOwnerNonNasabah" runat="server">
                                                    <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                        border-right-style: none; border-left-style: none; border-bottom-style: none"
                                                        id="TableNonSWIFTOutBOwnerNonNas">
                                                        <tr>
                                                            <td>
                                                                <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                                    bgcolor="#dddddd" border="2">
                                                                    <tr>
                                                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                            style="height: 6px">
                                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td class="formtext" style="height: 14px">
                                                                                        <asp:Label ID="Labelt9" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                                                    </td>
                                                                                    <td style="height: 14px">
                                                                                        <a href="#" onclick="javascript:ShowHidePanel('BOwnerNonNasabah','Img7','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                            title="click to minimize or maximize">
                                                                                            <img id="Img7" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="BOwnerNonNasabah">
                                                                        <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                            </table>
                                                                            <table style="width: 100%; height: 49px">
                                                                                <tr>
                                                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                        <asp:Label ID="Label60" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                    </td>
                                                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                        <asp:Label ID="Label61" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                    </td>
                                                                                    <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                        <asp:Label ID="Label62" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelu157" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 15%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_NamaOld" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_NamaNew" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Label1t66" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="188px"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 15%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_TanggalLahirOld" runat="server" CssClass="searcheditbox"
                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_TanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelt168" runat="server" Text="c. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                    <td style="width: 15%">
                                                                                    </td>
                                                                                    <td style="width: 20%">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labele169" runat="server" Text="Alamat"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 15%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_AlamatIdenOld" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_AlamatIdenNew" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Label1t70" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 15%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_KotaIdenOld" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hfBenfOwnerNonNasabah_KotaIdenOld" runat="server" />
                                                                                    </td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_KotaIdenNew" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hfBenfOwnerNonNasabah_KotaIdenNew" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelo172" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                    <td style="width: 15%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_KotaLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_KotaLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%; background-color: #fff7e6;">
                                                                                        <asp:Label ID="Labelo173" runat="server" Text="Provinsi"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 15%;">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_ProvinsiIdenOld" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hfBenfOwnerNonNasabah_ProvinsiIdenOld" runat="server" />
                                                                                    </td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_ProvinsiIdennew" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hfBenfOwnerNonNasabah_ProvinsiIdenNew" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelo174" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                    <td style="width: 15%;">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_ProvinsiLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 20%;">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_ProvinsiLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelo176" runat="server" Text="d. Jenis Dokument Identitas"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 15%">
                                                                                        <asp:DropDownList ID="CBOBenfOwnerNonNasabah_JenisDokumenOld" runat="server" Enabled="False">
                                                                                        </asp:DropDownList></td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:DropDownList ID="CBOBenfOwnerNonNasabah_JenisDokumenNew" runat="server" Enabled="False">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%; background-color: #fff7e6;">
                                                                                        <asp:Label ID="Labelo178" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                                    <td style="width: 15%;">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_NomorIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 20%;">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_NomorIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 15%; background-color: #fff7e6;">
                                                                                        &nbsp;</td>
                                                                                    <td style="width: 15%;">
                                                                                        &nbsp;</td>
                                                                                    <td style="width: 20%;">
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                            </asp:MultiView>
                                        </td>
                                    </tr>
                                </table>
                                <table id="PENERIMA" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                    width="100%" bgcolor="#dddddd" border="2">
                                    <tr>
                                        <td background="Images/search-bar-background.gif" valign="middle" align="left" style="height: 11px;
                                            width: 100%;" width="100%">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="formtext">
                                                        <asp:Label ID="Label49" runat="server" Text="D. Identitas Penerima Di luar Negeri"
                                                            Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <a href="#" onclick="javascript:ShowHidePanel('DNonSwiftOutidentitasPENERIMA','Img1E4','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                            title="click to minimize or maximize">
                                                            <img id="Img1E4" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="DNonSwiftOutidentitasPENERIMA">
                                        <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;" width="100%">
                                            <table style="width: 100%">
                                                <tr id="Tr1" runat="server" visible="false">
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                        <asp:Label ID="Label63" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                        <asp:Label ID="Label64" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                        <asp:Label ID="Label65" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="trNonSwiftOutTipepengirim" runat="server" visible="false">
                                                    <td style="width: 15%">
                                                        <asp:Label ID="Label53" runat="server" Text=" Tipe Penerima"></asp:Label>
                                                    </td>
                                                    <td align="left" style="border: thin dashed silver; width: 15%;">
                                                        <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                                            <asp:RadioButtonList ID="RbNonSwiftOutPenerimaNonNasabah_TipePenerimaOld" runat="server"
                                                                AutoPostBack="True" RepeatDirection="Horizontal" Width="189px" Enabled="False">
                                                                <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                    <td align="left" style="width: 15%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                        border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                        <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                                            <asp:RadioButtonList ID="RbNonSwiftOutPenerimaNonNasabah_TipePenerimaNew" runat="server"
                                                                AutoPostBack="True" RepeatDirection="Horizontal" Width="189px" Enabled="False">
                                                                <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                                <tr id="trNonSwiftOutTipeNasabah" runat="server" visible="false">
                                                    <td style="width: 15%">
                                                        <asp:Label ID="Label54" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                    </td>
                                                    <td align="left" style="border: thin dashed silver; width: 15%;">
                                                        <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                                            <asp:RadioButtonList ID="RbNonSwiftOutPenerimaNonNasabah_TipeNasabahOld" runat="server"
                                                                AutoPostBack="True" RepeatDirection="Horizontal" Enabled="False">
                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                    <td align="left" style="width: 15%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                        border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                        <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                                            <asp:RadioButtonList ID="RbNonSwiftOutPenerimaNonNasabah_TipeNasabahNew" runat="server"
                                                                AutoPostBack="True" RepeatDirection="Horizontal" Enabled="False">
                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <ajax:AjaxPanel ID="AjaxPanel9" runat="server" Width="100%">
                                                <asp:MultiView ID="MultiViewNonSwiftOutPenerima" runat="server" ActiveViewIndex="0">
                                                    <asp:View ID="ViewNonSwiftOutPenerimaNasabah" runat="server">
                                                        <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                            bgcolor="#dddddd" border="2" id="TABLE3" onclick="return TABLE1_onclick()">
                                                            <tr>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                    style="height: 6px">
                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td class="formtext" style="height: 14px">
                                                                                <asp:Label ID="Label56" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                                            </td>
                                                                            <td style="height: 14px">
                                                                                <a href="#" onclick="javascript:ShowHidePanel('B1NonSwiftOutidentitasPENerima1nasabah','Img1e5','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                    title="click to minimize or maximize">
                                                                                    <img id="Img1e5" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="B1NonSwiftOutidentitasPENerima1nasabah">
                                                                <td valign="top" bgcolor="#ffffff" style="height: 125px" width="100%">
                                                                    <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                        border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                        <tr>
                                                                            <td>
                                                                                <table style="width: 100%">
                                                                                    <tr>
                                                                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                            <asp:Label ID="Label69" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                        </td>
                                                                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                            <asp:Label ID="Label70" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                        </td>
                                                                                        <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                            <asp:Label ID="Label71" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                                            <asp:Label ID="Label77" runat="server" Text="No. Rek " Width="48px"></asp:Label>
                                                                                            <asp:Label ID="Label79" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 15%">
                                                                                            <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_IND_RekeningOld" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_IND_RekeningNew" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="100%">
                                                                                <asp:MultiView ID="MultiViewPenerimaAkhirNasabah" runat="server" ActiveViewIndex="0">
                                                                                    <asp:View ID="VwNonSwiftOut_PEN_IND" runat="server">
                                                                                        <ajax:AjaxPanel ID="AjaxPanel10" runat="server" Width="100%">
                                                                                            <table style="width: 100%; height: 49px">
                                                                                                <tr>
                                                                                                    <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label1o95" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="height: 15px; width: 15%;">
                                                                                                    </td>
                                                                                                  <%--  <td style="height: 15px; width: 20%;">
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                        <asp:Label ID="Label78" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                                    </td>
                                                                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                        <asp:Label ID="Label80" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                                    </td>
                                                                                                  <%--  <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                        <asp:Label ID="Label81" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label19o7" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                        <asp:Label ID="Label12o5" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_namaOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_namaNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label19o8" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="174px"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_TanggalLahirOld" runat="server" CssClass="searcheditbox"
                                                                                                            TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>
                                                                                                   <%-- <td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_TanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                                            TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                        &nbsp;</td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Labelo200" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                            Width="223px"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;">
                                                                                                        <asp:RadioButtonList ID="RbPenerimaNasabah_IND_WarganegaraOld" runat="server" RepeatDirection="Horizontal">
                                                                                                            <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                            <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                        </asp:RadioButtonList></td>
                                                                                                    <%--<td style="width: 20%; height: 15px;">
                                                                                                        <asp:RadioButtonList ID="RbPenerimaNasabah_IND_WarganegaraNew" runat="server" RepeatDirection="Horizontal">
                                                                                                            <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                            <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label201" runat="server" Text="d. Negara "></asp:Label>
                                                                                                        <br />
                                                                                                        <asp:Label ID="Label202" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                            Width="264px"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_negaraOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfPenerimaNasabah_IND_negaraOld" runat="server" />
                                                                                                    </td>
                                                                                                    <%--<td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_negaraNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfPenerimaNasabah_IND_negaraNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label203" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_negaraLainOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_negaraLainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label213" runat="server" Text="e. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;">
                                                                                                    </td>
                                                                                                  <%--  <td style="width: 20%; height: 15px;">
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label214" runat="server" Text="Alamat"></asp:Label>
                                                                                                        <asp:Label ID="Label9328" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 15%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_alamatIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="341px"></asp:TextBox></td>
                                                                                                   <%-- <td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_alamatIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="341px"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label205" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_negaraBagianOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>
                                                                                                   <%-- <td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_negaraBagianNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label204" runat="server" Text="Negara"></asp:Label>
                                                                                                        <asp:Label ID="Label9329" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 15%; height: 15px">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_negaraIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfPenerimaNasabah_IND_negaraIdenOld" runat="server" />
                                                                                                    </td>
                                                                                                    <%--<td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_negaraIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfPenerimaNasabah_IND_negaraIdenNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label206" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                    <td style="height: 15px; width: 15%;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_negaraLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>
                                                                                                  <%--  <td style="width: 20%; height: 15px">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_negaraLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label220" runat="server" Text="f. Jenis Dokument Identitas"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;">
                                                                                                        <asp:DropDownList ID="CBOPenerimaNasabah_IND_JenisIdenOld" runat="server">
                                                                                                        </asp:DropDownList></td>
                                                                                                    <%--<td style="width: 20%; height: 15px;">
                                                                                                        <asp:DropDownList ID="CBOPenerimaNasabah_IND_JenisIdenNew" runat="server">
                                                                                                        </asp:DropDownList>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label221" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                                                    <td style="height: 15px; width: 15%;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_NomorIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_NomorIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                    </td>
                                                                                                    <td style="height: 15px; width: 15%;">
                                                                                                    </td>
                                                                                                    <%--<td style="width: 20%; height: 15px;">
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ajax:AjaxPanel>
                                                                                    </asp:View>
                                                                                    <asp:View ID="VwNonSwiftOut_PEN_Corp" runat="server">
                                                                                        <table style="height: 31px" width="100%">
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label223" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                    <asp:Label ID="Label224" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                <td style="height: 15px; color: #000000; width: 15%;" align="left">
                                                                                                </td>
                                                                                                <%--<td style="width: 20%; color: #000000; height: 15px" align="left">
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr style="color: #000000">
                                                                                                <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                    <asp:Label ID="Label82" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                                </td>
                                                                                                <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                    <asp:Label ID="Label83" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                                </td>
                                                                                               <%-- <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                    <asp:Label ID="Label84" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr style="color: #000000">
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label225" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label></td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                <%--<td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label226" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="194px"></asp:Label></td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaLainnyaOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                <%--<td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaLainnyaNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label227" runat="server" Text="b. Nama Korporasi" Width="114px"></asp:Label>
                                                                                                    <asp:Label ID="Label149" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_namaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                <%--<td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_namaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label228" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaKorpOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfNonSwiftOutPenerimaNasabah_Korp_BidangUsahaOld" runat="server" />
                                                                                                </td>
                                                                                               <%-- <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaKorpNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfNonSwiftOutPenerimaNasabah_Korp_BidangUsahaNew" runat="server" />
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label9" runat="server" Text="Bidang Usaha Lainnya" Width="147px"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaLainKorpOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                <%--<td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaLainKorpNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label229" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                </td>
                                                                                                <%--<td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label4" runat="server" Text="Alamat"></asp:Label>
                                                                                                    <asp:Label ID="Label9319" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 15%; height: 15px; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_AlamatKorpOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                <%--<td style="width: 20%; height: 15px; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_AlamatKorpNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label6" runat="server" Text="Negara Bagian / Kota "></asp:Label>
                                                                                                    <asp:Label ID="Label9321" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                </td>
                                                                                                <td style="height: 15px; width: 15%; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_NegaraBagianOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfNonSwiftOutPenerimaNasabah_Korp_NegaraBagianOld" runat="server" />
                                                                                                </td>
                                                                                                <%--<td style="height: 15px; width: 20%; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_NegaraBagianNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfNonSwiftOutPenerimaNasabah_Korp_NegaraBagianNew" runat="server" />
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label14" runat="server" Text="Negara"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_NegaraOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfNonSwiftOutPenerimaNasabah_Korp_NegaraOld" runat="server" />
                                                                                                </td>
                                                                                                <%--<td align="left" style="width: 20%; color: #000000; height: 15px">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_NegaraNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfNonSwiftOutPenerimaNasabah_Korp_NegaraNew" runat="server" />
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label15" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_NegaralainOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                <%--<td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_NegaralainNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:View>
                                                                                </asp:MultiView>
                                                                            </td>
                                                                            <td width="100%">
                                                                                <asp:MultiView ID="MultiViewPenerimaAkhirNasabah_New" runat="server" ActiveViewIndex="0">
                                                                                    <asp:View ID="VwNonSwiftOut_PEN_IND_new" runat="server">
                                                                                        <ajax:AjaxPanel ID="AjaxPanel13" runat="server" Width="100%">
                                                                                            <table style="width: 100%; height: 49px">
                                                                                                <tr>
                                                                                                    <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label41" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                    </td>
                                                                                                   <%-- <td style="height: 15px; width: 15%;">
                                                                                                    </td>--%>
                                                                                                    <td style="height: 15px; width: 20%;">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                        <asp:Label ID="Label93" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                                    </td>
                                                                                                  <%--  <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                        <asp:Label ID="Label111" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                                    </td>--%>
                                                                                                    <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                        <asp:Label ID="Label112" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label113" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                        <asp:Label ID="Label114" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                  <%--  <td style="width: 15%; height: 15px;">
                                                                                                        <asp:TextBox ID="TextBox1" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_namaNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label115" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="174px"></asp:Label></td>
                                                                                                  <%--  <td style="width: 15%; height: 15px;">
                                                                                                        <asp:TextBox ID="TextBox3" runat="server" CssClass="searcheditbox"
                                                                                                            TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                    <td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_TanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                                            TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                        &nbsp;</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label116" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                            Width="223px"></asp:Label></td>
                                                                                                   <%-- <td style="width: 15%; height: 15px;">
                                                                                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                                                                                                            <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                            <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                        </asp:RadioButtonList></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;">
                                                                                                        <asp:RadioButtonList ID="RbPenerimaNasabah_IND_WarganegaraNew" runat="server" RepeatDirection="Horizontal">
                                                                                                            <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                            <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label117" runat="server" Text="d. Negara "></asp:Label>
                                                                                                        <br />
                                                                                                        <asp:Label ID="Label118" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                            Width="264px"></asp:Label></td>
                                                                                                    <%--<td style="width: 15%; height: 15px;">
                                                                                                        <asp:TextBox ID="TextBox5" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                                                                                    </td>--%>
                                                                                                    <td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_negaraNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HiddenField2" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label119" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                    <%--<td style="width: 15%; height: 15px;">
                                                                                                        <asp:TextBox ID="TextBox7" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_negaraLainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label120" runat="server" Text="e. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                                   <%-- <td style="width: 15%; height: 15px;">
                                                                                                    </td>--%>
                                                                                                    <td style="width: 20%; height: 15px;">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label121" runat="server" Text="Alamat"></asp:Label>
                                                                                                        <asp:Label ID="Label122" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                    </td>
                                                                                                    <%--<td style="width: 15%; height: 15px;">
                                                                                                        <asp:TextBox ID="TextBox9" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="341px"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_alamatIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="341px"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label123" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                                                    <%--<td style="width: 15%; height: 15px;">
                                                                                                        <asp:TextBox ID="TextBox11" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                    <td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_negaraBagianNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label124" runat="server" Text="Negara"></asp:Label>
                                                                                                        <asp:Label ID="Label125" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                    </td>
                                                                                                    <%--<td style="width: 15%; height: 15px">
                                                                                                        <asp:TextBox ID="TextBox13" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HiddenField3" runat="server" />
                                                                                                    </td>--%>
                                                                                                    <td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_negaraIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfPenerimaNasabah_IND_negaraIdenNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label126" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                   <%-- <td style="height: 15px; width: 15%;">
                                                                                                        <asp:TextBox ID="TextBox15" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                    <td style="width: 20%; height: 15px">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_negaraLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label127" runat="server" Text="f. Jenis Dokument Identitas"></asp:Label></td>
                                                                                                    <%--<td style="width: 15%; height: 15px;">
                                                                                                        <asp:DropDownList ID="DropDownList1" runat="server">
                                                                                                        </asp:DropDownList></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;">
                                                                                                        <asp:DropDownList ID="CBOPenerimaNasabah_IND_JenisIdenNew" runat="server">
                                                                                                        </asp:DropDownList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                        <asp:Label ID="Label128" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                                                    <%--<td style="height: 15px; width: 15%;">
                                                                                                        <asp:TextBox ID="TextBox17" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;">
                                                                                                        <asp:TextBox ID="txtPenerimaNasabah_IND_NomorIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                    </td>
                                                                                                   <%-- <td style="height: 15px; width: 15%;">
                                                                                                    </td>--%>
                                                                                                    <td style="width: 20%; height: 15px;">
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ajax:AjaxPanel>
                                                                                    </asp:View>
                                                                                    <asp:View ID="VwNonSwiftOut_PEN_Corp_new" runat="server">
                                                                                        <table style="height: 31px" width="100%">
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label129_new" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                    <asp:Label ID="Label224_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                               <%-- <td style="height: 15px; color: #000000; width: 15%;" align="left">
                                                                                                </td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px" align="left">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr style="color: #000000">
                                                                                                <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                    <asp:Label ID="Label82_new" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                                </td>
                                                                                               <%-- <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                                    <asp:Label ID="Label83" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                                </td>--%>
                                                                                                <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                    <asp:Label ID="Label84_new" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr style="color: #000000">
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label225_new" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label></td>
                                                                                               <%-- <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label226_new" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="194px"></asp:Label></td>
                                                                                               <%-- <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaLainnyaOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BentukBadanUsahaLainnyaNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label227_new" runat="server" Text="b. Nama Korporasi" Width="114px"></asp:Label>
                                                                                                    <asp:Label ID="Label149_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                <%--<td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_namaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_namaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label228_new" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label>
                                                                                                </td>
                                                                                                <%--<td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaKorpOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfNonSwiftOutPenerimaNasabah_Korp_BidangUsahaOld" runat="server" />
                                                                                                </td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaKorpNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfNonSwiftOutPenerimaNasabah_Korp_BidangUsahaNew" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label9_new" runat="server" Text="Bidang Usaha Lainnya" Width="147px"></asp:Label>
                                                                                                </td>
                                                                                                <%--<td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaLainKorpOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_BidangUsahaLainKorpNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label229_new" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                                <%--<td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                </td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label4_new" runat="server" Text="Alamat"></asp:Label>
                                                                                                    <asp:Label ID="Label9319_new" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                </td>
                                                                                                <%--<td style="width: 15%; height: 15px; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_AlamatKorpOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                <td style="width: 20%; height: 15px; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_AlamatKorpNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label6_new" runat="server" Text="Negara Bagian / Kota "></asp:Label>
                                                                                                    <asp:Label ID="Label9321_new" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                </td>
                                                                                                <%--<td style="height: 15px; width: 15%; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_NegaraBagianOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfNonSwiftOutPenerimaNasabah_Korp_NegaraBagianOld" runat="server" />
                                                                                                </td>--%>
                                                                                                <td style="height: 15px; width: 20%; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_NegaraBagianNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfNonSwiftOutPenerimaNasabah_Korp_NegaraBagianNew" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label14_new" runat="server" Text="Negara"></asp:Label>
                                                                                                </td>
                                                                                                <%--<td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_NegaraOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfNonSwiftOutPenerimaNasabah_Korp_NegaraOld" runat="server" />
                                                                                                </td>--%>
                                                                                                <td align="left" style="width: 20%; color: #000000; height: 15px">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_NegaraNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfNonSwiftOutPenerimaNasabah_Korp_NegaraNew" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label15_new" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                               <%-- <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_NegaralainOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtNonSwiftOutPenerimaNasabah_Korp_NegaralainNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:View>
                                                                                </asp:MultiView>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:View>
                                                    <asp:View ID="ViewNonSwiftOutPenerimaNonNasabah" runat="server">
                                                        <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                            bgcolor="#dddddd" border="2">
                                                            <tr>
                                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                    style="height: 7px">
                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td class="formtext" style="height: 14px">
                                                                                <asp:Label ID="LabelNonSwiftOut4" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                                            </td>
                                                                            <td style="height: 14px">
                                                                                <a href="#" onclick="javascript:ShowHidePanel('B1NonSwiftOutidentitasPENerimaonNasabah','Imgt23','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                    title="click to minimize or maximize">
                                                                                    <img id="Imgt23" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="B1NonSwiftOutidentitasPENerimaonNasabah">
                                                                <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                    <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                        border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                    </table>
                                                                    <table style="height: 49px; width: 100%;">
                                                                        <tr>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                <asp:Label ID="Label85" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                            </td>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                <asp:Label ID="Label86" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                            </td>
                                                                            <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                <asp:Label ID="Label88" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label21" runat="server" Text="a. Kode Rahasia"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 15%">
                                                                                <asp:TextBox ID="TxtNonSwiftOutPenerimaNonNasabah_KodeRahasaOld" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 20%">
                                                                                <asp:TextBox ID="TxtNonSwiftOutPenerimaNonNasabah_KodeRahasaNew" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label23" runat="server" Text="b. No. Rek"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 15%">
                                                                                <asp:TextBox ID="TxtNonSwiftOutPenerimaNonNasabah_NoRekOld" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 20%">
                                                                                <asp:TextBox ID="TxtNonSwiftOutPenerimaNonNasabah_NoRekNew" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label26" runat="server" Text="c. Nama Bank"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 15%">
                                                                                <asp:TextBox ID="TxtNonSwiftOutPenerimaNonNasabah_namaBankOld" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 20%">
                                                                                <asp:TextBox ID="TxtNonSwiftOutPenerimaNonNasabah_namaBankNew" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labelq81" runat="server" Text="d. Nama Lengkap"></asp:Label>
                                                                                <asp:Label ID="Labelq85" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 15%">
                                                                                <asp:TextBox ID="TxtNonSwiftOutPenerimaNonNasabah_namaOld" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 20%">
                                                                                <asp:TextBox ID="TxtNonSwiftOutPenerimaNonNasabah_namaNew" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelNonSwiftOut95" runat="server" Text="e. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                            <td style="width: 15%">
                                                                                <asp:TextBox ID="TxtNonSwiftOutPenerimaNonNasabah_alamatidenOld" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 20%">
                                                                                <asp:TextBox ID="TxtNonSwiftOutPenerimaNonNasabah_alamatidenNew" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #FFF7E6;">
                                                                                <asp:Label ID="Labelb199" runat="server" Text="Alamat"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 15%">
                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_AlamatOld" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 20%">
                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_AlamatNew" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #FFF7E6;">
                                                                                <asp:Label ID="Labelh208" runat="server" Text="Negara"></asp:Label>
                                                                                <asp:Label ID="Labelh159" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 15%;">
                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_NegaraOld" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 20%">
                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_NegaraNew" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #FFF7E6;">
                                                                                <asp:Label ID="Labelh209" runat="server" Text="Negara Lainnya"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 15%;">
                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_NegaraLainOld" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 20%;">
                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_NegaraLainNew" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:View>
                                                </asp:MultiView>
                                            </ajax:AjaxPanel>
                                        </td>
                                    </tr>
                                </table>
                                <table id="TRANSAKSI" align="center" bgcolor="#dddddd" border="2" bordercolor="#ffffff"
                                    cellpadding="2" cellspacing="1" width="99%">
                                    <tr>
                                        <td align="left" background="Images/search-bar-background.gif" style="height: 6px;
                                            width: 100%;" valign="middle">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="formtext">
                                                        <asp:Label ID="Label9275" runat="server" Font-Bold="True" Text="E. Transaksi"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <a href="#" onclick="javascript:ShowHidePanel('ENonSwiftOuttransaksi','Imgrr25','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                            title="click to minimize or maximize">
                                                            <img id="Imgrr25" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" />
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Label ID="Label9276" runat="server" Font-Italic="True" Text="(Diisi oleh PJK Bank  yang bertindak sebagai Penyelenggara Pengirim Asal atau sebagai Penyelenggara Penerus)"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="ENonSwiftOuttransaksi">
                                        <td bgcolor="#ffffff" style="height: 125px; width: 100%;" valign="top">
                                            <table border="0" cellpadding="2" cellspacing="1" style="border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none"
                                                width="100%">
                                            </table>
                                            <table style="width: 100%; height: 49px">
                                                <tr>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                        <asp:Label ID="Label90" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                        <asp:Label ID="Label91" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                        <asp:Label ID="Label92" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label9277" runat="server" Text="a. Tanggal Transaksi (tgl/bln/thn) "></asp:Label>
                                                        <asp:Label ID="Label9278" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_NonSwiftOuttanggalOld" runat="server" CssClass="searcheditbox"
                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_NonSwiftOuttanggalNew" runat="server" CssClass="searcheditbox"
                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9287" runat="server" Text="b. Kantor Cabang Penyelenggara Pengirim Asal"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_NonSwiftOutkantorCabangPengirimOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="250px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_NonSwiftOutkantorCabangPengirimNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="250px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9290" runat="server" Text=" c. Tanggal Transaksi/Mata Uang/Nilai Transaksi"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                    </td>
                                                    <td style="width: 20%">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9291" runat="server" Text="Tanggal Transaksi (Value Date)"></asp:Label>
                                                        <asp:Label ID="Label9292" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_NonSwiftOutValueTanggalTransaksiOld" runat="server" CssClass="searcheditbox"
                                                            TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_NonSwiftOutValueTanggalTransaksiNew" runat="server" CssClass="searcheditbox"
                                                            TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label9293" runat="server" Text="Nilai Transaksi (Amount of The EFT)"></asp:Label>
                                                        <asp:Label ID="Label9315" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="TxtnilaitransaksiOld" runat="server" CssClass="searcheditbox" TabIndex="2"
                                                            Width="122px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="TxtnilaitransaksiNew" runat="server" CssClass="searcheditbox" TabIndex="2"
                                                            Width="122px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <tr>
                                                        <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                            <asp:Label ID="Label9294" runat="server" Text="Mata Uang Transaksi (currency of the EFT)"></asp:Label>
                                                            <asp:Label ID="Label9295" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutMataUangTransaksiOld" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutMataUangTransaksiNew" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                            <asp:Label ID="Label16" runat="server" Text="Mata Uang Lainnya"></asp:Label>
                                                            <asp:Label ID="Label17" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutMataUangTransaksiLainnyaOld" runat="server"
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutMataUangTransaksiLainnyaNew" runat="server"
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Label9296" runat="server" Text="Nilai Transaksi Keuangan dalam Rupiah"></asp:Label>
                                                            <asp:Label ID="Label9327" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutAmountdalamRupiahOld" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutAmountdalamRupiahNew" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Label9297" runat="server" Text="d. Mata Uang/Nilai Transaksi Keuangan yang diinstruksikan"></asp:Label>
                                                        </td>
                                                        <td style="width: 15%">
                                                        </td>
                                                        <td style="width: 20%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Label9298" runat="server" Text="Mata Uang yang diinstruksikan "></asp:Label>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutcurrencyOld" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutcurrencyNew" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Label19" runat="server" Text="Mata Uang Lainnya"></asp:Label>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutcurrencyLainnyaOld" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutcurrencyLainnyaNew" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Label9299" runat="server" Text="Nilai Transaksi Keuangan yang diinstruksikan"></asp:Label>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutinstructedAmountOld" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutinstructedAmountNew" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Label9304" runat="server" Text="e. Tujuan Transaksi "></asp:Label>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutTujuanTransaksiOld" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutTujuanTransaksiNew" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Label9305" runat="server" Text="f. Sumber Penggunaan Dana "></asp:Label>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutSumberPenggunaanDanaOld" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="250px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="Transaksi_NonSwiftOutSumberPenggunaanDanaNew" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="250px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        </td>
                                                        <td style="width: 15%">
                                                        </td>
                                                        <td style="width: 20%">
                                                        </td>
                                                    </tr>
                                                    <table id="FUNCTION BUTTON" width="100%">
                                                        <tr>
                                                            <td style="width: 387px">
                                                            </td>
                                                            <td style="width: 122px">
                                                                <asp:Button ID="BtnSave" runat="server" Text="Accept" />
                                                            </td>
                                                            <td style="width: 107px">
                                                                <asp:Button ID="BtnReject" runat="server" Text="Reject" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="BtnCancel" runat="server" Text="<<Back" />
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View ID="VwConfirmation" runat="server">
                                <table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                    width="100%" bgcolor="#dddddd" border="0">
                                    <tr bgcolor="#ffffff">
                                        <td colspan="2" align="center" style="height: 17px">
                                            <asp:Label ID="LblConfirmation" runat="server" meta:resourcekey="LblConfirmationResource1"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr bgcolor="#ffffff">
                                        <td align="center" colspan="2">
                                            <asp:ImageButton ID="ImgBtnAdd" runat="server" ImageUrl="~/images/button/Ok.gif"
                                                CausesValidation="False" meta:resourcekey="ImgBtnAddResource1" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                        </asp:MultiView>
                    </ajax:AjaxPanel>
                </td>
            </tr>
        </table>
        <ajax:AjaxPanel ID="AjaxPanel14" runat="server" meta:resourcekey="AjaxPanel14Resource1">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" background="Images/button-bground.gif" valign="middle">
                        <img height="1" src="Images/blank.gif" width="5" />
                    </td>
                    <td align="left" background="Images/button-bground.gif" valign="middle">
                        <img height="15" src="images/arrow.gif" width="15" />&nbsp;
                    </td>
                    <td background="Images/button-bground.gif">
                        &nbsp;
                    </td>
                    <td background="Images/button-bground.gif">
                        &nbsp;
                    </td>
                    <td background="Images/button-bground.gif">
                        &nbsp;
                    </td>
                    <td background="Images/button-bground.gif" width="99%">
                        <img height="1" src="Images/blank.gif" width="1" />
                        <asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" meta:resourcekey="CvalHandleErrResource1"
                            ValidationGroup="handle"></asp:CustomValidator>
                        <asp:CustomValidator ID="CvalPageErr" runat="server" Display="None" meta:resourcekey="CvalPageErrResource1"></asp:CustomValidator>
                    </td>
                    <td>
                        
                    </td>
                </tr>
            </table>
            <asp:CustomValidator ID="CustomValidator1" runat="server" Display="None" meta:resourcekey="CustomValidator1Resource1"
                ValidationGroup="handle"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </asp:CustomValidator>
            <asp:CustomValidator ID="CustomValidator2" runat="server" Display="None" meta:resourcekey="CustomValidator2Resource1"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </asp:CustomValidator>
        </ajax:AjaxPanel>
    </div>
</asp:Content>