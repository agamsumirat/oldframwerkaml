<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="CaseManagementConfirmationNumberViewDetail.aspx.vb" Inherits="CaseManagementConfirmationNumberViewDetail" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
  <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Case Detail&nbsp;
                    <hr />
                </strong>
                <ajax:AjaxPanel ID="AjaxPanel15" runat="server">
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
            </td>
        </tr>
    </table>
    <table cellpadding="1" cellspacing="1" style="width: 100%">
        <tr>
            <td width="1%" nowrap="noWrap">
                Case Number</td>
            <td width="1">
                :</td>
            <td width="99%">
                <asp:Label ID="LabelCaseNumber" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td width="1%" style="height: 15px" nowrap="noWrap">
                Description</td>
            <td style="height: 15px" width="1">
                :</td>
            <td style="height: 15px" width="99%">
                <asp:Label ID="LabelDescription" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td width="1%" nowrap="noWrap">
                Status</td>
            <td width="1">
                :</td>
            <td width="99%">
                <asp:Label ID="LabelStatus" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td width="1%" nowrap="noWrap">
                Created Date</td>
            <td width="1">
                :</td>
            <td width="99%">
                <asp:Label ID="LabelCreatedDate" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td width="1%" nowrap="noWrap">
                Last Updated Date</td>
            <td width="1">
                :</td>
            <td width="99%">
                <asp:Label ID="LabelLastUpdatedDate" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="1%">
                Proposed By</td>
            <td width="1">
                :</td>
            <td width="99%">
                <asp:Label ID="LabelProposedBy" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="1%">
                Account Owner Name</td>
            <td width="1">
                :</td>
            <td width="99%">
                <asp:Label ID="LabelAccountOwnerName" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="1%">
                PIC</td>
            <td width="1">
                :</td>
            <td width="99%">
                <asp:Label ID="LabelPIC" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="1%">
                Has Open Issue</td>
            <td width="1">
                :</td>
            <td width="99%">
                <asp:Label ID="LabelHasOpenIssue" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td nowrap="nowrap" style="height: 18px" width="1%">
                Last Proposed Action</td>
            <td style="height: 18px" width="1">
                :</td>
            <td style="height: 18px" width="99%">
                <asp:Label ID="LabelLastProposedAction" runat="server"></asp:Label></td>
        </tr>
           <tr>
            <td nowrap="nowrap" style="height: 18px" width="1%">
                Physically reported to Regulator</td>
            <td style="height: 18px" width="1">
                :</td>
            <td style="height: 18px" width="99%">
                <asp:Label ID="LabelReportedtoRegulator" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td nowrap="nowrap" style="height: 18px" width="1%">
                PPATK STR Confirmation No</td>
            <td style="height: 18px" width="1">
                :</td>
            <td style="height: 18px" width="99%">
                <asp:Label ID="LabelConfirmationNo" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3" style="height: 18px">
                &nbsp;<br>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="validation"
                    ValidationGroup="FillInvestigation" Width="100%" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Panel ID="Panel1" runat="server" Height="100%" Width="100% " GroupingText="Confirmation Number">
                    <table style="width: 100%">
                        <tr>
                            <td style="height: 15px" nowrap="noWrap" valign="top" width="1%">
                                Reported</td>
                            <td style="height: 15px" width="1">
                                :</td>
                            <td style="height: 15px" width="99%">
                                <asp:CheckBox ID="CheckBoxReported" runat="server" Text="Physically reported to Regulator" /></td>
                        </tr>
                        <tr>
                            <td nowrap="noWrap" width="1%">
                                PPATK STR Confirmation No</td>
                            <td width="1">
                                :</td>
                            <td width="99%">
                                <asp:TextBox ID="TextBoxConfirmationNo" runat="server" MaxLength="150"></asp:TextBox></td>
                        </tr>
                    </table>
                    </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
                <asp:ImageButton ID="ImagePrint" runat="server" ImageUrl="~/Images/button/printstrform.gif" />
                <asp:ImageButton ID="ImageButtonSave" runat="server" SkinID="SaveButton" />
                <asp:ImageButton ID="ImageButton1" runat="server" SkinID="BackButton" /></td>
        </tr>
    </table>
</asp:Content>

