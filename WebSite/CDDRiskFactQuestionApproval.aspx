<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CDDRiskFactQuestionApproval.aspx.vb" Inherits="CDDRiskFactQuestionApproval"
    ValidateRequest="false" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script src="script/popcalendar.js"></script>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="20" height="100%" /></td>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%">
                                    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                        border="2">
                                        <tr bgcolor="#ffffff">
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <strong>
                                                    <img src="Images/dot_title.gif" width="17" height="17">
                                                    <asp:Label ID="Label7" runat="server" Text="CDD LNP Risk Fact Question Approval"></asp:Label>
                                                    <hr />
                                                </strong>
                                            </td>
                                        </tr>
                                    </table>
                                    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                        border="2">
                                        <tr id="searchbox">
                                            <td colspan="2" valign="top" width="98%" bgcolor="#ffffff">
                                                <table cellspacing="4" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                            style="height: 6px">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="formtext">
                                                                        <asp:Label ID="Label6" runat="server" Text="Search Criteria" Font-Bold="True"></asp:Label>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        <a href="#" onclick="javascript:ShowHidePanel('SearchCriteria','searchimage4','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                            title="click to minimize or maximize">
                                                                            <img id="searchimage4" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                                width="12px"></a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="SearchCriteria">
                                                        <td valign="top" bgcolor="#ffffff">
                                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                <tr>
                                                                    <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                                        Requested By</td>
                                                                    <td nowrap style="width: 75%; height: 22px;" valign="top">
                                                                        <asp:TextBox ID="txtRequestedBy" runat="server" CssClass="searcheditbox" TabIndex="2"
                                                                            Width="308px"></asp:TextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                                        Requested Date</td>
                                                                    <td nowrap style="width: 75%; height: 22px;" valign="top">
                                                                        <asp:TextBox ID="txtRequestedDateFrom" runat="server" CssClass="textBox" MaxLength="1000"
                                                                            TabIndex="2" ToolTip="RequestedDate" Width="120px"></asp:TextBox>
                                                                        <input id="PopRequestedDateFrom" runat="server" style="border-right: #ffffff 0px solid;
                                                                            border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                            border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                            height: 17px" title="Click to show calendar" type="button" />
                                                                        s/d
                                                                        <asp:TextBox ID="txtRequestedDateTo" runat="server" CssClass="textBox" MaxLength="1000"
                                                                            TabIndex="2" ToolTip="RequestedDate" Width="120px"></asp:TextBox>
                                                                        <input id="PopRequestedDateTo" runat="server" style="border-right: #ffffff 0px solid;
                                                                            border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                            border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                            height: 17px" title="Click to show calendar" type="button" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap" style="width: 15%; background-color: #fff7e6">
                                                                        Mode</td>
                                                                    <td nowrap="nowrap" style="width: 75%; height: 22px" valign="top">
                                                                        <asp:DropDownList ID="ddlMode" runat="server" CssClass="comboBox">
                                                                        </asp:DropDownList></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" height="10">
                                                                        &nbsp;<asp:ImageButton ID="ImageButtonSearch" TabIndex="3" runat="server" SkinID="SearchButton"
                                                                            CausesValidation="False" ImageUrl="~/Images/Button/Search.gif"></asp:ImageButton>&nbsp;<asp:ImageButton
                                                                                ID="ImageClear" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/clearSearch.gif"
                                                                                TabIndex="3" /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                                                    <asp:CustomValidator ID="CvalPageErr" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                        border="2">
                                        <tr>
                                            <td bgcolor="#ffffff">
                                                <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                                                    <asp:DataGrid ID="GridViewCDDLNP" runat="server" AutoGenerateColumns="False" Font-Size="XX-Small"
                                                        BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None" Width="100%"
                                                        GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE" ForeColor="Black">
                                                        <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                        <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
                                                        <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                        <ItemStyle BackColor="#F7F7DE"></ItemStyle>
                                                        <HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B">
                                                        </HeaderStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server"></asp:CheckBox>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="2%" />
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="PK_CDD_RiskFactQuestion_Approval_Id" Visible="False"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="UserName" HeaderText="Requested By" SortExpression="UserName desc">
                                                                <HeaderStyle Width="31%" />
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Mode" HeaderText="Mode" SortExpression="Mode desc">
                                                                <ItemStyle Wrap="False" Font-Italic="False" Font-Strikeout="False" Font-Underline="False"
                                                                    Font-Overline="False" Font-Bold="False"></ItemStyle>
                                                                <HeaderStyle Wrap="False" Font-Italic="False" Font-Strikeout="False" Font-Underline="False"
                                                                    Font-Overline="False" Font-Bold="True" Width="31%"></HeaderStyle>
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="RequestedDate" HeaderText="Requested Date" SortExpression="RequestedDate  asc"
                                                                DataFormatString="{0:dd-MM-yyyy}">
                                                                <ItemStyle Wrap="False" Font-Italic="False" Font-Strikeout="False" Font-Underline="False"
                                                                    Font-Overline="False" Font-Bold="False"></ItemStyle>
                                                                <HeaderStyle Width="31%" />
                                                            </asp:BoundColumn>
                                                            <asp:TemplateColumn>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LnkDetail" runat="server" CausesValidation="false" CommandName="Detail"
                                                                        Text="Detail"></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="5%" />
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                    <asp:Label ID="LabelNoRecordFound" runat="server" Text="No record match with your criteria"
                                                        CssClass="text" Visible="False"></asp:Label></ajax:AjaxPanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #ffffff">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td nowrap>
                                                            <ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                                                &nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
                                                                &nbsp;
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                        <td width="99%">
                                                            &nbsp;&nbsp;<asp:LinkButton ID="lnkExportData" ajaxcall="none" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                runat="server">Export 
		        to Excel</asp:LinkButton>&nbsp;
                                                            <asp:LinkButton ID="lnkExportAllData" runat="server" ajaxcall="none" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                Text="Export All to Excel"></asp:LinkButton></td>
                                                        <td align="right" nowrap>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#ffffff">
                                                <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                    bgcolor="#ffffff" border="2">
                                                    <tr class="regtext" align="center" bgcolor="#dddddd">
                                                        <td valign="top" align="left" width="50%" bgcolor="#ffffff">
                                                            Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel7" runat="server"><asp:Label ID="PageCurrentPage"
                                                                runat="server" CssClass="regtext">0</asp:Label>&nbsp;of&nbsp;
                                                                <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label></ajax:AjaxPanel></td>
                                                        <td valign="top" align="right" width="50%" bgcolor="#ffffff">
                                                            Total Records&nbsp;
                                                            <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                                                <asp:Label ID="PageTotalRows" runat="server">0</asp:Label></ajax:AjaxPanel></td>
                                                    </tr>
                                                </table>
                                                <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                    bgcolor="#ffffff" border="2">
                                                    <tr bgcolor="#ffffff">
                                                        <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                                                            <hr color="#f40101" noshade size="1">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                                                            Go to page</td>
                                                        <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                                                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                                <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                                                    <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox></ajax:AjaxPanel>
                                                            </font>
                                                        </td>
                                                        <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                                                            <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                                                <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton" ImageUrl="~/Images/button/go.gif">
                                                                </asp:ImageButton></ajax:AjaxPanel>
                                                        </td>
                                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                            <img height="5" src="images/first.gif" width="6">
                                                        </td>
                                                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                                            <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                                                <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                                                    OnCommand="PageNavigate">First</asp:LinkButton></ajax:AjaxPanel>
                                                        </td>
                                                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                                            <img height="5" src="images/prev.gif" width="6"></td>
                                                        <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
                                                            <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                                                <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                                                    OnCommand="PageNavigate">Previous</asp:LinkButton></ajax:AjaxPanel>
                                                        </td>
                                                        <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
                                                            <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                                                <a class="pageNav" href="#">
                                                                    <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                                                        OnCommand="PageNavigate">Next</asp:LinkButton></a></ajax:AjaxPanel></td>
                                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                            <img height="5" src="images/next.gif" width="6"></td>
                                                        <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
                                                            <ajax:AjaxPanel ID="AjaxPanel16" runat="server">
                                                                <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                                                    OnCommand="PageNavigate">Last</asp:LinkButton></ajax:AjaxPanel>
                                                        </td>
                                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                            <img height="5" src="images/last.gif" width="6"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
