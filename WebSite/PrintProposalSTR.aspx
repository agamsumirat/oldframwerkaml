<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="PrintProposalSTR.aspx.vb" Inherits="PrintProposalSTR" title="Print Proposal STR" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table border=0 cellpadding=0 cellspacing=0 width="99%">
        <tr>
            <td style="width: 576px">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
        Height="400px" Width="100%">
        <LocalReport ReportPath="ReportProposalSTR.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="ProposalSTR_ProposalSTR" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetProposalSTRByPK"
        TypeName="AMLDAL.ProposalSTRTableAdapters.ProposalSTRTableAdapter" DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_PK_ProposalSTRID" Type="Int64" />
            <asp:Parameter Name="Original_FK_CaseManagementID" Type="Int64" />
            <asp:Parameter Name="Original_CreatedDate" Type="DateTime" />
            <asp:Parameter Name="Original_FK_ProposalSTRStatusID" Type="Int32" />
            <asp:Parameter Name="Original_UserID" Type="String" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="FK_CaseManagementID" Type="Int64" />
            <asp:Parameter Name="CreatedDate" Type="DateTime" />
            <asp:Parameter Name="KasusPolisi" Type="String" />
            <asp:Parameter Name="IndicatorMencurigakan" Type="String" />
            <asp:Parameter Name="UnsurTKM" Type="String" />
            <asp:Parameter Name="LainLain" Type="String" />
            <asp:Parameter Name="Kesimpulan" Type="String" />
            <asp:Parameter Name="FK_ProposalSTRStatusID" Type="Int32" />
            <asp:Parameter Name="UserID" Type="String" />
            <asp:Parameter Name="Original_PK_ProposalSTRID" Type="Int64" />
            <asp:Parameter Name="Original_FK_CaseManagementID" Type="Int64" />
            <asp:Parameter Name="Original_CreatedDate" Type="DateTime" />
            <asp:Parameter Name="Original_FK_ProposalSTRStatusID" Type="Int32" />
            <asp:Parameter Name="Original_UserID" Type="String" />
            <asp:Parameter Name="PK_ProposalSTRID" Type="Int64" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="PK_ProposalSTRID" QueryStringField="PKProposalSTRID"
                Type="Int64" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="FK_CaseManagementID" Type="Int64" />
            <asp:Parameter Name="CreatedDate" Type="DateTime" />
            <asp:Parameter Name="KasusPolisi" Type="String" />
            <asp:Parameter Name="IndicatorMencurigakan" Type="String" />
            <asp:Parameter Name="UnsurTKM" Type="String" />
            <asp:Parameter Name="LainLain" Type="String" />
            <asp:Parameter Name="Kesimpulan" Type="String" />
            <asp:Parameter Name="FK_ProposalSTRStatusID" Type="Int32" />
            <asp:Parameter Name="UserID" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td style="width: 576px">
            <asp:ImageButton ID="ImageBack" runat="server" SkinID="BackButton"/>
            </td>
        </tr>
    </table>
    <br />
    
</asp:Content>

