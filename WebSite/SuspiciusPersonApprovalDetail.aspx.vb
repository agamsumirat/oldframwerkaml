Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports System.Data
Imports System.Data.SqlClient
Partial Class SuspiciusPersonApprovalDetail
    Inherits Parent



    Public ReadOnly Property PK_Suspicius_Approval_ID() As Integer
        Get
            Return Request.Params("PK_Suspicius_Approval_ID")
        End Get

    End Property



    Public ReadOnly Property objSuspicius_ApprovalDetail() As TList(Of Suspicius_ApprovalDetail)
        Get
            If Session("SuspiciusPersonApprovalDetail.objSuspicius_ApprovalDetail") Is Nothing Then
                Session("SuspiciusPersonApprovalDetail.objSuspicius_ApprovalDetail") = AMLBLL.SuspiciusPersonBLL.GetSuspiciusApprovalDetail(Me.PK_Suspicius_Approval_ID)

            End If
            Return Session("SuspiciusPersonApprovalDetail.objSuspicius_ApprovalDetail")
        End Get
    End Property

    Sub ClearSession()
        Session("SuspiciusPersonApprovalDetail.objSuspicius_ApprovalDetail") = Nothing

    End Sub

    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    Sub LoadDataAdd()
        PanelOld.Visible = False
        PanelNew.Visible = True

        LblAction.Text = "Add"



        If objSuspicius_ApprovalDetail.Count > 0 Then
            LblNewName.Text = objSuspicius_ApprovalDetail(0).Nama
            lblNewCIF.Text = objSuspicius_ApprovalDetail(0).CIFNo
            LblNewBirthDate.Text = objSuspicius_ApprovalDetail(0).TanggalLahir.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy")
            LblNewBirthPlace.Text = objSuspicius_ApprovalDetail(0).TempatLahir
            LblNewIdentityNo.Text = objSuspicius_ApprovalDetail(0).NoIdentitas
            LblNewAlamat.Text = objSuspicius_ApprovalDetail(0).Alamat
            LblNewKecamatanKelurahan.Text = objSuspicius_ApprovalDetail(0).KecamatanKeluarahan
            LblNewPhoneNo.Text = objSuspicius_ApprovalDetail(0).NoTelp
            lblNewJob.Text = objSuspicius_ApprovalDetail(0).Pekerjaan
            LblNewWorkPlace.Text = objSuspicius_ApprovalDetail(0).AlamatTempatKerja
            LblNewNPWP.Text = objSuspicius_ApprovalDetail(0).NPWP
            LblNewDescription.Text = objSuspicius_ApprovalDetail(0).Description
            LblNewCreatedBy.Text = objSuspicius_ApprovalDetail(0).UseridCreator

            'Perubahan
            If LblNewDescription.Text.Trim.Length > 0 Then
                TextDescription.Text = "Artificial STR-" & LblNewDescription.Text
            Else
                TextDescription.Text = "Artificial STR"
            End If

        End If
    End Sub


    Sub LoadDataEdit()
        PanelOld.Visible = True
        PanelNew.Visible = True

        LblAction.Text = "Edit"

        If objSuspicius_ApprovalDetail.Count > 0 Then

            LblOldName.Text = objSuspicius_ApprovalDetail(0).Nama_old
            LblOldCIF.Text = objSuspicius_ApprovalDetail(0).CIFNo_Old
            LblOldBirthDate.Text = objSuspicius_ApprovalDetail(0).TanggalLahir_old.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy")
            LblOldBirthPlace.Text = objSuspicius_ApprovalDetail(0).TempatLahir_old
            LblOldIdentityNo.Text = objSuspicius_ApprovalDetail(0).NoIdentitas_old
            LblOldAlamat.Text = objSuspicius_ApprovalDetail(0).Alamat_old
            LblOldKecamatanKelurahan.Text = objSuspicius_ApprovalDetail(0).KecamatanKeluarahan_old
            LblOLdPhoneNo.Text = objSuspicius_ApprovalDetail(0).NoTelp_old
            lblOldJob.Text = objSuspicius_ApprovalDetail(0).Pekerjaan_old
            LblOldWorkPlace.Text = objSuspicius_ApprovalDetail(0).AlamatTempatKerja_old
            LblOldNPWP.Text = objSuspicius_ApprovalDetail(0).NPWP_old
            LblOldDescription.Text = objSuspicius_ApprovalDetail(0).Description_old
            LblOldCreatedBy.Text = objSuspicius_ApprovalDetail(0).UseridCreator_old


            LblNewName.Text = objSuspicius_ApprovalDetail(0).Nama
            lblNewCIF.Text = objSuspicius_ApprovalDetail(0).CIFNo
            LblNewBirthDate.Text = objSuspicius_ApprovalDetail(0).TanggalLahir.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy")
            LblNewBirthPlace.Text = objSuspicius_ApprovalDetail(0).TempatLahir
            LblNewIdentityNo.Text = objSuspicius_ApprovalDetail(0).NoIdentitas
            LblNewAlamat.Text = objSuspicius_ApprovalDetail(0).Alamat
            LblNewKecamatanKelurahan.Text = objSuspicius_ApprovalDetail(0).KecamatanKeluarahan
            LblNewPhoneNo.Text = objSuspicius_ApprovalDetail(0).NoTelp
            lblNewJob.Text = objSuspicius_ApprovalDetail(0).Pekerjaan
            LblNewWorkPlace.Text = objSuspicius_ApprovalDetail(0).AlamatTempatKerja
            LblNewNPWP.Text = objSuspicius_ApprovalDetail(0).NPWP
            LblNewDescription.Text = objSuspicius_ApprovalDetail(0).Description
            LblNewCreatedBy.Text = objSuspicius_ApprovalDetail(0).UseridCreator

            'Perubahan
            If LblNewDescription.Text.Trim.Length > 0 Then
                TextDescription.Text = "Artificial STR-" & LblNewDescription.Text
            Else
                TextDescription.Text = "Artificial STR"
            End If

        End If
    End Sub


    Sub LoadDataDelete()
        PanelOld.Visible = False
        PanelNew.Visible = True

        LblAction.Text = "Delete"

        If objSuspicius_ApprovalDetail.Count > 0 Then

            LblNewName.Text = objSuspicius_ApprovalDetail(0).Nama
            lblNewCIF.Text = objSuspicius_ApprovalDetail(0).CIFNo
            LblNewBirthDate.Text = objSuspicius_ApprovalDetail(0).TanggalLahir.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy")
            LblNewBirthPlace.Text = objSuspicius_ApprovalDetail(0).TempatLahir
            LblNewIdentityNo.Text = objSuspicius_ApprovalDetail(0).NoIdentitas
            LblNewAlamat.Text = objSuspicius_ApprovalDetail(0).Alamat
            LblNewKecamatanKelurahan.Text = objSuspicius_ApprovalDetail(0).KecamatanKeluarahan
            LblNewPhoneNo.Text = objSuspicius_ApprovalDetail(0).NoTelp
            lblNewJob.Text = objSuspicius_ApprovalDetail(0).Pekerjaan
            LblNewWorkPlace.Text = objSuspicius_ApprovalDetail(0).AlamatTempatKerja
            LblNewNPWP.Text = objSuspicius_ApprovalDetail(0).NPWP
            LblNewDescription.Text = objSuspicius_ApprovalDetail(0).Description
            LblNewCreatedBy.Text = objSuspicius_ApprovalDetail(0).UseridCreator
        End If
    End Sub


    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            AddHandler PopUpAccountOwner1.SelectUser, AddressOf SelectUser
            If Not IsPostBack Then
                ClearSession()
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    LabelTitle.Text = "Custom Alert Approval Detail"
                    Select Case Me.ParamType
                        Case Sahassa.AML.Commonly.TypeConfirm.SuspiciusPersonAdd
                            SearchBar.Visible = True
                            LoadDataAdd()
                        Case Sahassa.AML.Commonly.TypeConfirm.SuspiciusPersonEdit
                            LoadDataEdit()
                            SearchBar.Visible = False
                        Case Sahassa.AML.Commonly.TypeConfirm.SuspiciusPersonDelete
                            LoadDataDelete()
                            SearchBar.Visible = False
                        Case Else
                            Throw New Exception("Type not supported type:" & Me.ParamType.ToString)

                    End Select

                End Using
            End If

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Sub AcceptSuspiciusPersonAdd()
        'AMLBLL.SuspiciusPersonBLL.AcceptAddSuspiciusPerson(Me.PK_Suspicius_Approval_ID)

        'enhancement
        Dim Trans As SqlTransaction = Nothing
        Dim intAccountOwnerid As Integer
        Dim PKSuspiciusPersonId As Integer
        Dim strpreparer As String
        Try
            If IsDataValid() Then
                Try
                    Using OTrans As TransactionManager = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
                        Try
                            OTrans.BeginTransaction()
                            Using objSuspiciusPersonapproval As Suspicius_Approval = DataRepository.Suspicius_ApprovalProvider.GetByPK_Suspicius_Approval_ID(Me.PK_Suspicius_Approval_ID)
                                If Not objSuspiciusPersonapproval Is Nothing Then
                                    Using objSuspiciusPersonApprovalDetail As TList(Of Suspicius_ApprovalDetail) = DataRepository.Suspicius_ApprovalDetailProvider.GetPaged(Suspicius_ApprovalDetailColumn.FK_Suspicius_Approval.ToString & "=" & Me.PK_Suspicius_Approval_ID, "", 0, Integer.MaxValue, 0)
                                        If objSuspiciusPersonApprovalDetail.Count > 0 Then
                                            strpreparer = objSuspiciusPersonapproval.UserID
                                            AddAuditTrail(OTrans, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Custom Alert", "Nama", "Add", objSuspiciusPersonApprovalDetail(0).Nama, "", "Approval")
                                            AddAuditTrail(OTrans, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Custom Alert", "CIF", "Add", objSuspiciusPersonApprovalDetail(0).CIFNo, "", "Approval")
                                            AddAuditTrail(OTrans, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Custom Alert", "TanggalLahir", "Add", objSuspiciusPersonApprovalDetail(0).TanggalLahir.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy").Replace("01-Jan-1900", "Approval"), "", "Approval")
                                            AddAuditTrail(OTrans, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Custom Alert", "TempatLahir", "Add", objSuspiciusPersonApprovalDetail(0).TempatLahir, "", "Approval")
                                            AddAuditTrail(OTrans, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Custom Alert", "NoIdentitas", "Add", objSuspiciusPersonApprovalDetail(0).NoIdentitas, "", "Approval")
                                            AddAuditTrail(OTrans, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Custom Alert", "Alamat", "Add", objSuspiciusPersonApprovalDetail(0).Alamat, "", "Approval")
                                            AddAuditTrail(OTrans, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Custom Alert", "KecamatanKeluarahan", "Add", objSuspiciusPersonApprovalDetail(0).KecamatanKeluarahan, "", "Approval")
                                            AddAuditTrail(OTrans, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Custom Alert", "NoTelp", "Add", objSuspiciusPersonApprovalDetail(0).NoTelp, "", "Approval")
                                            AddAuditTrail(OTrans, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Custom Alert", "Pekerjaan", "Add", objSuspiciusPersonApprovalDetail(0).Pekerjaan, "", "Approval")
                                            AddAuditTrail(OTrans, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Custom Alert", "AlamatTempatKerja", "Add", objSuspiciusPersonApprovalDetail(0).AlamatTempatKerja, "", "Approval")
                                            AddAuditTrail(OTrans, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Custom Alert", "NPWP", "Add", objSuspiciusPersonApprovalDetail(0).NPWP, "", "Approval")
                                            AddAuditTrail(OTrans, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Custom Alert", "Description", "Add", objSuspiciusPersonApprovalDetail(0).Description, "", "Approval")
                                            AddAuditTrail(OTrans, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Custom Alert", "CreatedDate", "Add", objSuspiciusPersonApprovalDetail(0).CreatedDate.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy"), "", "Approval")
                                            AddAuditTrail(OTrans, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Custom Alert", "LastUpdateDate", "Add", objSuspiciusPersonApprovalDetail(0).LastUpdateDate.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy"), "", "Approval")
                                            AddAuditTrail(OTrans, strpreparer, Sahassa.AML.Commonly.SessionUserId, "Custom Alert", "UseridCreator", "Add", objSuspiciusPersonApprovalDetail(0).UseridCreator, "", "Approval")


                                            Using objnewSuspiciusPerson As New SuspiciusPerson
                                                With objnewSuspiciusPerson
                                                    .Nama = objSuspiciusPersonApprovalDetail(0).Nama
                                                    .CIFNo = objSuspiciusPersonApprovalDetail(0).CIFNo
                                                    .TanggalLahir = objSuspiciusPersonApprovalDetail(0).TanggalLahir
                                                    .TempatLahir = objSuspiciusPersonApprovalDetail(0).TempatLahir
                                                    .NoIdentitas = objSuspiciusPersonApprovalDetail(0).NoIdentitas
                                                    .Alamat = objSuspiciusPersonApprovalDetail(0).Alamat
                                                    .KecamatanKeluarahan = objSuspiciusPersonApprovalDetail(0).KecamatanKeluarahan
                                                    .NoTelp = objSuspiciusPersonApprovalDetail(0).NoTelp
                                                    .Pekerjaan = objSuspiciusPersonApprovalDetail(0).Pekerjaan
                                                    .AlamatTempatKerja = objSuspiciusPersonApprovalDetail(0).AlamatTempatKerja
                                                    .NPWP = objSuspiciusPersonApprovalDetail(0).NPWP
                                                    .Description = objSuspiciusPersonApprovalDetail(0).Description
                                                    .CreatedDate = objSuspiciusPersonApprovalDetail(0).CreatedDate
                                                    .LastUpdateDate = objSuspiciusPersonApprovalDetail(0).LastUpdateDate
                                                    .UseridCreator = objSuspiciusPersonApprovalDetail(0).UseridCreator

                                                End With
                                                DataRepository.SuspiciusPersonProvider.Save(OTrans, objnewSuspiciusPerson)
                                                PKSuspiciusPersonId = objnewSuspiciusPerson.PK_SuspiciusPerson_ID
                                            End Using
                                            DataRepository.Suspicius_ApprovalDetailProvider.Delete(OTrans, objSuspiciusPersonApprovalDetail)
                                        End If

                                    End Using
                                    DataRepository.Suspicius_ApprovalProvider.Delete(OTrans, objSuspiciusPersonapproval)
                                End If
                            End Using
                            OTrans.Commit()
                        Catch ex As Exception
                            OTrans.Rollback()
                            Throw
                        End Try
                    End Using
                Catch
                    Throw
                End Try

                Dim StrWorkflowstep As String = ""
                Dim StrPIC As String = ""
                Dim PK_CaseManagementID As String
                Dim DateCase As DateTime
                Dim CaseManagementWorkflowTable As New AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable
                Dim CaseManagementWorkflowTableRows() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = Nothing
                Dim CaseManagementWorkflowTableRow As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = Nothing


                intAccountOwnerid = haccountowner.Value
                Me.VIPCode = CboVIPCode.SelectedValue
                Me.InsiderCode = CboInsidercode.SelectedValue
                Me.Segment = CboSegment.SelectedValue
                Me.SBU = cbosbu.SelectedValue
                Me.SUBSBU = cbosubsbu.SelectedValue
                Me.RM = CboRM.SelectedValue
                Using AccessTable As New AMLDAL.NewCaseTableAdapters.CaseManagementTableAdapter
                    Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessTable)
                    Using AdapterCaseManagementWorkflow As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter

                        CaseManagementWorkflowTable = AdapterCaseManagementWorkflow.GetSettingWorkFlowCaseManagementByAccountOwnerID(intAccountOwnerid, Me.VIPCode, Me.InsiderCode, Me.Segment, TextDescription.Text, Me.SBU, Me.SUBSBU, Me.RM)
                        If Not CaseManagementWorkflowTable Is Nothing Then
                            If CaseManagementWorkflowTable.Rows.Count > 0 Then

                                'CaseManagementWorkflowTableRows = CaseManagementWorkflowTable.Select("SerialNo=1 AND Paralel=1")
                                CaseManagementWorkflowTableRows = CaseManagementWorkflowTable.Select("SerialNo=1")
                                If Not CaseManagementWorkflowTableRows Is Nothing Then
                                    If CaseManagementWorkflowTableRows.Length > 0 Then

                                        CaseManagementWorkflowTableRow = CaseManagementWorkflowTableRows(0)
                                        If Not CaseManagementWorkflowTableRow.IsApproversNull Then
                                            Dim ArrApprovers() As String = Nothing
                                            ArrApprovers = CaseManagementWorkflowTableRow.Approvers.Split("|"c)
                                            If Not ArrApprovers Is Nothing Then
                                                If ArrApprovers.Length > 0 Then
                                                    StrPIC = Me.GetUserIDbyPK(ArrApprovers(0))
                                                End If
                                            End If
                                        End If
                                        If Not CaseManagementWorkflowTableRow.IsWorkflowStepNull Then
                                            StrWorkflowstep = CaseManagementWorkflowTableRow.WorkflowStep
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End Using
                    DateCase = DateTime.Now()
                    PK_CaseManagementID = AccessTable.InsertCaseManagement(Me.TextDescription.Text, 1, StrWorkflowstep, DateCase, DateCase, intAccountOwnerid, StrPIC, False, 0, False, "", Nothing, Sahassa.AML.Commonly.SessionUserId, "", Nothing, "", objSuspicius_ApprovalDetail(0).Nama, objSuspicius_ApprovalDetail(0).CIFNo, "", Me.VIPCode, Me.InsiderCode, Me.Segment)
                    If PK_CaseManagementID > 0 Then
                        ' Insert ke MapCaseManagementTransaction
                        Using objnewCasemanagmentsuspiciusperson As New CaseManagementSuspiciusPerson
                            With objnewCasemanagmentsuspiciusperson
                                .FK_CaseManagement_ID = PK_CaseManagementID
                                .FK_SuspiciusPerson_ID = PKSuspiciusPersonId
                            End With
                            DataRepository.CaseManagementSuspiciusPersonProvider.Save(objnewCasemanagmentsuspiciusperson)
                        End Using

                        'Perbaikan case double button 29 Oktober 2014
                        Using cmdQUESTION As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_GenerateQuestionCaseManagement")
                            cmdQUESTION.Parameters.Add(New SqlParameter("@PKCaseManagementID", PK_CaseManagementID))
                            cmdQUESTION.Parameters.Add(New SqlParameter("@DescriptioinCaseAlert", TextDescription.Text))
                            SahassaNettier.Data.DataRepository.Provider.ExecuteNonQuery(cmdQUESTION)
                        End Using

                        ' Insert ke MapCaseManagementWorkflow
                        Using MapCaseManagementAdapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementWorkflowTableAdapter
                            MapCaseManagementAdapter.Insert(PK_CaseManagementID, "SYSTEM", DateCase, DateCase)
                        End Using
                        ' Insert ke MapCaseManagementWorkflowHistory
                        Using MapCaseManagementHistoryAdapter As New AMLDAL.CaseManagementTableAdapters.SelectMapCaseManagementWorkflowHistoryTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(MapCaseManagementHistoryAdapter, Trans)
                            MapCaseManagementHistoryAdapter.InsertMapCaseManagementWorkflowHistory(PK_CaseManagementID, DateCase, 1, StrWorkflowstep, "", "", Nothing, True, "", intAccountOwnerid, Nothing)
                            'MapCaseManagementHistoryAdapter.InsertMapCaseManagementWorkflowHistory(PK_CaseManagementID, DateCase, 2, StrWorkflowstep, StrPIC, "", Nothing, False, "", Me.AccountOwnerId, Nothing)
                            ' Insert to Workflow
                            Dim Counter As Integer
                            Dim ListOfUser As String
                            Dim ApproverCounter As Integer
                            Dim StrEmailRecepients As String
                            Dim StrEmailCC As String
                            Dim StrEmailSubject As String
                            Dim StrEmailBody As String
                            If Not CaseManagementWorkflowTableRows Is Nothing Then
                                If CaseManagementWorkflowTableRows.Length > 0 Then
                                    For Counter = 0 To CaseManagementWorkflowTableRows.Length - 1
                                        CaseManagementWorkflowTableRow = CaseManagementWorkflowTableRows(Counter)
                                        ListOfUser = StrPIC
                                        StrEmailRecepients = ""
                                        StrEmailCC = ""
                                        If Not CaseManagementWorkflowTableRow.IsApproversNull Then
                                            Dim ArrApprovers() As String = Nothing
                                            ArrApprovers = CaseManagementWorkflowTableRow.Approvers.Split("|"c)
                                            If Not ArrApprovers Is Nothing Then
                                                If ArrApprovers.Length > 0 Then
                                                    ListOfUser = ""

                                                    For ApproverCounter = 0 To ArrApprovers.Length - 1
                                                        ListOfUser = ListOfUser & Me.GetUserIDbyPK(ArrApprovers(ApproverCounter)) & ";"
                                                        StrEmailRecepients += GetEmailAddress(ArrApprovers(ApproverCounter)) & ";"
                                                    Next
                                                    If ListOfUser.Length > 0 Then
                                                        ListOfUser = ListOfUser.Remove(ListOfUser.Length - 1)
                                                    End If
                                                    If StrEmailRecepients.Length > 0 Then
                                                        StrEmailRecepients = StrEmailRecepients.Remove(StrEmailRecepients.Length - 1)
                                                    End If
                                                End If
                                            End If
                                        End If
                                        If Not CaseManagementWorkflowTableRow.IsNotifyOthersNull Then
                                            Dim ArrOtherApprovers() As String = Nothing
                                            ArrOtherApprovers = CaseManagementWorkflowTableRow.NotifyOthers.Split("|"c)
                                            If Not ArrOtherApprovers Is Nothing Then
                                                If ArrOtherApprovers.Length > 0 Then

                                                    For ApproverCounter = 0 To ArrOtherApprovers.Length - 1
                                                        StrEmailCC += GetEmailAddress(ArrOtherApprovers(ApproverCounter)) & ";"
                                                    Next
                                                    If StrEmailCC.Length > 0 Then
                                                        StrEmailCC = StrEmailCC.Remove(StrEmailCC.Length - 1)
                                                    End If
                                                End If
                                            End If
                                        End If

                                        MapCaseManagementHistoryAdapter.InsertMapCaseManagementWorkflowHistory(PK_CaseManagementID, DateCase, 2, StrWorkflowstep, ListOfUser, "", Nothing, False, "", intAccountOwnerid, Nothing)
                                        If StrEmailRecepients <> "" Then
                                            StrEmailSubject = GetEmailSubject(CaseManagementWorkflowTableRow.Pk_CMW_Id, CaseManagementWorkflowTableRow.SerialNo)
                                            StrEmailBody = GetEmailBody(CaseManagementWorkflowTableRow.Pk_CMW_Id, CaseManagementWorkflowTableRow.SerialNo)
                                            SendEmail(StrEmailRecepients, StrEmailCC, StrEmailSubject, StrEmailBody)
                                        End If

                                    Next
                                End If
                            End If
                        End Using
                    End If
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, Trans)
                        AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "CreateNewCaseSuspiciusPerson", "CaseDescription", "Add", "", Me.TextDescription.Text, "Accepted")
                        AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "CreateNewCaseSuspiciusPerson", "Workflowstep", "Add", "", StrWorkflowstep, "Accepted")
                        AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "CreateNewCaseSuspiciusPerson", "CreatedDate", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "CreateNewCaseSuspiciusPerson", "LastUpdated", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "CreateNewCaseSuspiciusPerson", "Fk_AccountOwnerId", "Add", "", intAccountOwnerid, "Accepted")
                        AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "CreateNewCaseSuspiciusPerson", "PIC", "Add", "", StrPIC, "Accepted")
                        AccessAudit.Insert(Now, strpreparer, Sahassa.AML.Commonly.SessionUserId, "CreateNewCaseSuspiciusPerson", "ProposedBy", "Add", "", Sahassa.AML.Commonly.SessionUserId, "Accepted")
                    End Using
                    Trans.Commit()
                End Using

                Sahassa.AML.Commonly.SessionIntendedPage = "SuspiciusPersonView.aspx"
                Response.Redirect("SuspiciusPersonView.aspx", False)
            End If
        Catch tex As Threading.ThreadAbortException
            ' ignore
        Catch ex As Exception
            If Not Trans Is Nothing Then
                Trans.Rollback()
            End If
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
            End If
        End Try

    End Sub

    Sub AddAuditTrail(ByRef ObjTransactionManager As TransactionManager, strcreator As String, ByVal StrAuditTrailApprovedBy As String, ByVal StrAuditTrailModuleName As String, ByVal StrAuditTrailFieldName As String, ByVal StrAuditTrailOperation As String, ByVal StrAuditTrailOldValue As String, ByVal StrAuditTrailNewValue As String, ByVal StrAuditTrailDescription As String)
        Using ObjAuditTrail As New AuditTrail
            With ObjAuditTrail
                .AuditTrailDate = Date.Now
                .AuditTrailCreatedBy = strcreator
                .AuditTrailApprovedBy = StrAuditTrailApprovedBy
                .AuditTrailModuleName = StrAuditTrailModuleName
                .AuditTrailFieldName = StrAuditTrailFieldName
                .AuditTrailOperation = StrAuditTrailOperation
                .AuditTrailOldValue = StrAuditTrailOldValue
                .AuditTrailNewValue = StrAuditTrailNewValue
                .AuditTrailDescription = StrAuditTrailDescription

                'Save
                DataRepository.AuditTrailProvider.Save(ObjTransactionManager, ObjAuditTrail)
            End With
        End Using
    End Sub

    Sub AcceptSuspiciusPersonEdit()
        AMLBLL.SuspiciusPersonBLL.AcceptEditSuspiciusPerson(Me.PK_Suspicius_Approval_ID)
    End Sub

    Sub AcceptSuspiciusPersonDelete()
        AMLBLL.SuspiciusPersonBLL.AcceptDeleteSuspiciusPerson(Me.PK_Suspicius_Approval_ID)
    End Sub


    Sub rejectSuspiciusPersonAdd()
        AMLBLL.SuspiciusPersonBLL.RejectAddSuspiciusPerson(Me.PK_Suspicius_Approval_ID)
    End Sub
    Sub rejectSuspiciusPersonedit()
        AMLBLL.SuspiciusPersonBLL.RejectEditSuspiciusPerson(Me.PK_Suspicius_Approval_ID)
    End Sub

    Sub rejectSuspiciusPersondelete()
        AMLBLL.SuspiciusPersonBLL.RejectDeleteSuspiciusPerson(Me.PK_Suspicius_Approval_ID)
    End Sub



    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click

        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.SuspiciusPersonAdd
                    Me.AcceptSuspiciusPersonAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.SuspiciusPersonEdit
                    Me.AcceptSuspiciusPersonEdit()
                    Sahassa.AML.Commonly.SessionIntendedPage = "SuspiciusPersonApproval.aspx"
                    Me.Response.Redirect("SuspiciusPersonApproval.aspx", False)

                Case Sahassa.AML.Commonly.TypeConfirm.SuspiciusPersonDelete
                    Me.AcceptSuspiciusPersonDelete()
                    Sahassa.AML.Commonly.SessionIntendedPage = "SuspiciusPersonApproval.aspx"
                    Me.Response.Redirect("SuspiciusPersonApproval.aspx", False)

                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click

        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.SuspiciusPersonAdd
                    Me.rejectSuspiciusPersonAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.SuspiciusPersonEdit
                    Me.rejectSuspiciusPersonedit()
                Case Sahassa.AML.Commonly.TypeConfirm.SuspiciusPersonDelete
                    Me.rejectSuspiciusPersondelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "SuspiciusPersonApproval.aspx"

            Me.Response.Redirect("SuspiciusPersonApproval.aspx", False)


        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click

        Try

            Response.Redirect("SuspiciusPersonApproval.aspx", False)
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Sub SelectUser(ByVal IntUserid As String)
        haccountowner.Value = IntUserid

        LblAccountOwner.Text = AMLBLL.SuspiciusPersonBLL.GetAccountOwnerIDbyPk(IntUserid)

        IsiVIPCode(haccountowner.Value)
        IsiInsidercode(haccountowner.Value)
        IsiSegment(haccountowner.Value)
        IsiPIC()
        'Using objUser As SahassaNettier.Entities.User = UserBLL.GetUserByPkUserID(IntUserid)
        '    If Not objUser Is Nothing Then
        '        LblUserID.Text = objUser.UserID
        '        LabelUserName.Text = objUser.UserName
        '        Session("ResetPassword.SaltUser") = objUser.UserPasswordSalt


        '    Else
        '        LblUserID.Text = ""
        '        LabelUserName.Text = ""
        '        Session("ResetPassword.SaltUser") = ""
        '    End If
        'End Using

    End Sub



    Private _RM As String
    Public Property RM() As String
        Get
            Return _RM
        End Get
        Set(ByVal value As String)
            _RM = value
        End Set
    End Property


    Private _SUBSBU As String
    Public Property SUBSBU() As String
        Get
            Return _SUBSBU
        End Get
        Set(ByVal value As String)
            _SUBSBU = value
        End Set
    End Property

    Private _SBU As String
    Public Property SBU() As String
        Get
            Return _SBU
        End Get
        Set(ByVal value As String)
            _SBU = value
        End Set
    End Property

    Private _InsiderCode As String
    Public Property InsiderCode() As String
        Get
            Return _InsiderCode
        End Get
        Set(ByVal value As String)
            _InsiderCode = value
        End Set
    End Property

    Private _VIPCode As String
    Public Property VIPCode() As String
        Get
            Return _VIPCode
        End Get
        Set(ByVal value As String)
            _VIPCode = value
        End Set
    End Property

    Private _Segment As String
    Public Property Segment() As String
        Get
            Return _Segment
        End Get
        Set(ByVal value As String)
            _Segment = value
        End Set
    End Property
    Protected Sub IsiPIC()
        Me.VIPCode = CboVIPCode.SelectedValue
        Me.InsiderCode = CboInsidercode.SelectedValue
        Me.Segment = CboSegment.SelectedValue
        Me.SBU = cbosbu.SelectedValue
        Me.SUBSBU = cbosubsbu.SelectedValue
        Me.RM = CboRM.SelectedValue

        Me.LabelPIC.Text = Me.GetPICByAccountOwnerId(Me.haccountowner.Value, Me.VIPCode, Me.InsiderCode, Me.Segment, Me.SBU, Me.SUBSBU, Me.RM)
    End Sub
    Sub IsiVIPCode(strAccountOwner As String)
        CboVIPCode.Items.Clear()
        For Each item As DataRow In AMLBLL.CreateNewCaseBLL.GetListVIPCodeWorkflowByAccountOwnerId(strAccountOwner).Rows
            CboVIPCode.Items.Add(New ListItem(item(0).ToString & " - " & item(1).ToString, item(0).ToString))
        Next

    End Sub
    Sub IsiInsidercode(strAccountOwner As String)
        CboInsidercode.Items.Clear()
        For Each item As DataRow In AMLBLL.CreateNewCaseBLL.GetListInsiderCodeWorkflowByAccountOwnerId(strAccountOwner).Rows
            CboInsidercode.Items.Add(New ListItem(item(0).ToString & " - " & item(1).ToString, item(0).ToString))
        Next

    End Sub
    Sub IsiSegment(strAccountOwner As String)
        CboSegment.Items.Clear()
        For Each item As DataRow In AMLBLL.CreateNewCaseBLL.GetListSegmentWorkflowByAccountOwnerId(strAccountOwner).Rows
            CboSegment.Items.Add(New ListItem(item(0).ToString & " - " & item(1).ToString, item(0).ToString))
        Next

    End Sub
    Private Function GetPICByAccountOwnerId(ByVal StrAccountOwnerId As String, strVipcode As String, strInsidercode As String, strSegment As String, strsbu As String, strsubsbu As String, strrm As String) As String
        Dim StrUserId As String = ""
        Using AdapterCaseManagementWorkflow As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter
            Dim CaseManagementWorkflowTable As New AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable
            CaseManagementWorkflowTable = AdapterCaseManagementWorkflow.GetSettingWorkFlowCaseManagementByAccountOwnerID(StrAccountOwnerId, strVipcode, strInsidercode, strSegment, "Artificial STR", strsbu, strsubsbu, strrm)
            If Not CaseManagementWorkflowTable Is Nothing Then
                If CaseManagementWorkflowTable.Rows.Count > 0 Then
                    Dim CaseManagementWorkflowTableRows() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
                    CaseManagementWorkflowTableRows = CaseManagementWorkflowTable.Select("SerialNo=1 AND Paralel=1")
                    If Not CaseManagementWorkflowTableRows Is Nothing Then
                        If CaseManagementWorkflowTableRows.Length > 0 Then
                            Dim CaseManagementWorkflowTableRow As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
                            CaseManagementWorkflowTableRow = CaseManagementWorkflowTableRows(0)
                            If Not CaseManagementWorkflowTableRow.IsApproversNull Then
                                Dim ArrApprovers() As String = Nothing
                                ArrApprovers = CaseManagementWorkflowTableRow.Approvers.Split("|"c)
                                If Not ArrApprovers Is Nothing Then
                                    If ArrApprovers.Length > 0 Then
                                        StrUserId = Me.GetUserIDbyPK(ArrApprovers(0))
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End Using
        Return StrUserId
    End Function
    Private Function GetUserIDbyPK(ByVal PKUserId As String) As String
        Try
            Dim StrUserId As String = ""
            If PKUserId <> "" Then
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Using oTable As AMLDAL.AMLDataSet.UserDataTable = adapter.GetDataByUserID(PKUserId)
                        If oTable.Rows.Count > 0 Then
                            StrUserId = CType(oTable.Rows(0), AMLDAL.AMLDataSet.UserRow).UserID
                        End If
                    End Using
                End Using
            End If
            Return StrUserId
        Catch ex As Exception
            Throw
        End Try
    End Function
    Protected Sub ImgBrowse_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBrowse.Click

        Try
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "idpopup2", "popUpDrag('" & CType(sender, ImageButton).ClientID & "','divBrowseUser');", True)
            PopUpAccountOwner1.initData()

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try

            If Not Page.ClientScript.IsClientScriptIncludeRegistered("idpopup1") Then
                Page.ClientScript.RegisterClientScriptInclude(Me.GetType(), "idpopup1", ResolveClientUrl("script/popupdrag.js"))
            End If


        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Function IsDataValid() As Boolean
        If haccountowner.Value = "" Then
            Throw New Exception("Please Select Account Owner ID")
        End If
        If LabelPIC.Text = "" Then
            Throw New Exception("Please Setting Account Owner for " & LblAccountOwner.Text)
        End If
        Return True
    End Function
    Private Function GetEmailAddress(ByVal PKUserID As String) As String
        Try
            If PKUserID <> "" Then
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Using otable As AMLDAL.AMLDataSet.UserDataTable = adapter.GetDataByUserID(PKUserID)
                        If otable.Rows.Count > 0 Then
                            If CType(otable.Rows(0), AMLDAL.AMLDataSet.UserRow).IsUserEmailAddressNull Then
                                Return ""
                            Else
                                Return CType(otable.Rows(0), AMLDAL.AMLDataSet.UserRow).UserEmailAddress
                            End If
                        Else
                            Return ""
                        End If
                    End Using
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Function GetEmailSubject(ByVal PKCMWID As Long, ByVal intSerialNo As Integer) As String
        Try
            Using adapter As New AMLDAL.CaseManagementTableAdapters.CaseManagementWorkflowEmailTemplateTableAdapter

                Using otable As AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateDataTable = adapter.GetEmailTemplate(PKCMWID, intSerialNo)
                    If otable.Rows.Count > 0 Then
                        If Not CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).IsSubject_EmailTemplateNull Then
                            Return CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).Subject_EmailTemplate
                        Else
                            Return ""
                        End If
                    Else
                        Return ""
                    End If

                End Using
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Function GetEmailBody(ByVal PKCMWID As Long, ByVal intSerialNo As Integer) As String
        Try
            Using adapter As New AMLDAL.CaseManagementTableAdapters.CaseManagementWorkflowEmailTemplateTableAdapter

                Using otable As AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateDataTable = adapter.GetEmailTemplate(PKCMWID, intSerialNo)
                    If otable.Rows.Count > 0 Then
                        If Not CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).IsBody_EmailTemplateNull Then
                            Return CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).Body_EmailTemplate
                        Else
                            Return ""
                        End If
                    Else
                        Return ""
                    End If

                End Using
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Sub SendEmail(ByVal StrRecipientTo As String, ByVal StrRecipientCC As String, ByVal StrSubject As String, ByVal strbody As String)
        Dim oEmail As Sahassa.AML.EMail
        Try
            oEmail = New Sahassa.AML.EMail
            oEmail.Sender = System.Configuration.ConfigurationManager.AppSettings("FromMailAddress")
            oEmail.Recipient = StrRecipientTo
            oEmail.RecipientCC = StrRecipientCC
            oEmail.Subject = StrSubject
            oEmail.Body = strbody.Replace(vbLf, "<br>")
            oEmail.SendEmail()
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub
End Class
