<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AccountInformationDetail.aspx.vb" Inherits="AccountInformationDetail" MasterPageFile="~/MasterPage.master" %>
<%@ Register Assembly="WebChart" Namespace="WebChart" TagPrefix="Web" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<%@ Register Src="webcontrol/PickerAuxTrxCodeMultiple.ascx" TagName="PickerAuxTrxCodeMultiple"
    TagPrefix="uc2" %>
<%@ Register Src="webcontrol/PopUpStatisticModus.ascx" TagName="PopUpStatisticModus"
    TagPrefix="uc1" %>
<asp:Content ID="ContentAccountInformation" ContentPlaceHolderID="cpContent" runat="server">
    
        <table id="TABLE1" bgcolor="#dddddd" border="2" bordercolor="#fffff" cellpadding="0"
            cellspacing="0" width="99%">
            <tr>
                <td style="width: 5px; height: 91px">
                </td>
                <td style="width: 100%; height: 91px; background-color: #ffffff" valign="top">
                    <table width="100%">
                        <tr>
                            <td style="width: 1%">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/dot_title.gif" /></td>
                            <td style="width: 100px">
                                <strong style="font-size: medium; width: 50%">
                                    <asp:Label ID="Label1" runat="server" CssClass="MainTitle" Text="ACCOUNT INFORMATION DETAIL"
                                        Width="400px"></asp:Label></strong></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <ajax:AjaxPanel ID="Ajaxpanel20" runat="server">
                                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
                                    <uc1:PopUpStatisticModus ID="PopUpStatisticModus1" runat="server" />
                                </ajax:AjaxPanel></td>
                        </tr>
                    </table>
                    <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="1" cellspacing="1"
                        width="100%">
                        <tr style="background-color: #ffffff">
                            <td colspan="6">
                                </td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td width="10%">
                                                                            CIF No</td>
                            <td style="width: 1%">
                                :</td>
                            <td style="width: 39%">
                                <%--&nbsp;<asp:Label ID="LblCIFNo" runat="server"></asp:Label></td>--%>&nbsp;<asp:HyperLink ID="LnkCIFNo" runat="server"></asp:HyperLink></td>
                            <td width="10%">
                                Customer Type</td>
                            <td style="width: 1%">
                                :</td>
                            <td style="width: 39%">
                                <asp:Label ID="CustomerTypeLabel" runat="server" Width="226px"></asp:Label></td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td width="10%">
                                Account Number</td>
                            <td style="width: 1%">
                                :</td>
                            <td style="width: 39%">
                                &nbsp;<asp:Label ID="LblAccountNumber" runat="server"></asp:Label>
                                </td>
                            <td width="10%">
                                Customer Sub Type</td>
                            <td style="width: 1%">
                                :</td>
                            <td style="width: 39%">
                                <asp:Label ID="CustomerSubTypeLabel" runat="server" Width="225px"></asp:Label></td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td width="10%" style="height: 30px">
                                Name</td>
                            <td style="width: 1%; height: 30px;">
                                :</td>
                            <td style="width: 39%; height: 30px;">
                                &nbsp;<asp:Label ID="LblName" runat="server" Width="320px"></asp:Label></td>
                            <td width="10%" style="height: 30px">
                                Business Type</td>
                            <td style="width: 1%; height: 30px;">
                                :</td>
                            <td style="width: 39%; height: 30px;">
                                <asp:Label ID="BusinessTypeLabel" runat="server" Width="378px"></asp:Label></td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td>
                                Account Owner</td>
                            <td style="width: 100px; height: 17px">
                                :</td>
                            <td style="width: 490px; height: 17px">
                                &nbsp;<asp:Label ID="LblAccountOwner" runat="server" Width="317px"></asp:Label></td>
                            <td style="width: 752px; height: 17px">
                                Internal Industry Code</td>
                            <td style="width: 100px; height: 17px">
                                :</td>
                            <td style="width: 100px; height: 17px">
                                <asp:Label ID="InternalIndustryCodeLabel" runat="server" Width="376px"></asp:Label></td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td>
                                SBU</td>
                            <td style="width: 100px; height: 17px">
                                :</td>
                            <td style="width: 490px; height: 17px">
                                &nbsp;<asp:Label ID="lblSBU" runat="server" Width="317px"></asp:Label></td>
                            <td style="width: 752px; height: 17px">
                                Sub SBU</td>
                            <td style="width: 100px; height: 17px">
                                :</td>
                            <td style="width: 100px; height: 17px">
                                <asp:Label ID="lblSubSBU" runat="server" Width="376px"></asp:Label></td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td>
                                RC Code</td>
                            <td style="width: 100px; height: 17px">
                                :</td>
                            <td style="width: 490px; height: 17px">
                                &nbsp;<asp:Label ID="lblRCCode" runat="server" Width="317px"></asp:Label></td>
                            <td style="width: 752px; height: 17px">
                                Company Code</td>
                            <td style="width: 100px; height: 17px">
                                :</td>
                            <td style="width: 100px; height: 17px">
                                <asp:Label ID="lblCompCode" runat="server" Width="376px"></asp:Label></td>
                        </tr>


                    </table>
                </td>
            </tr>            
            <tr>
                <td style="width: 5px">
                </td>
                <td style="background-color: #ffffff" valign="top">
                    <table cellpadding="0" cellspacing="0" width="99%">
                        <tr>
                            <td>
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">    
                                    <asp:Menu ID="Menu1" runat="server" CssClass="tabs" OnMenuItemClick="Menu1_MenuItemClick"
                                    Orientation="Horizontal" StaticEnableDefaultPopOutImage="False">
                                    <StaticMenuItemStyle CssClass="tab" />
                                    <StaticSelectedStyle CssClass="selectedTab" />
                                    <Items>
                                        <asp:MenuItem Selected="True" Text="General" Value="0"></asp:MenuItem>
                                        <asp:MenuItem Text="CDD Information" Value="1"></asp:MenuItem>                                        
                                        <asp:MenuItem Text="Financial Information" Value="2"></asp:MenuItem>
                                        <asp:MenuItem Text="Transaction Detail" Value="3"></asp:MenuItem>
                                        <asp:MenuItem Text="Graphical Transaction Analysis" Value="4"></asp:MenuItem>
                                        <asp:MenuItem Text="Peer Group Analysis" Value="5"></asp:MenuItem>
                                        <asp:MenuItem Text="Joint Account" Value="6"></asp:MenuItem>
                                        <asp:MenuItem Text="Statistic Financial Modus" Value="7"></asp:MenuItem>
                                        <%--<asp:MenuItem Text="Loan Information" Value="8"></asp:MenuItem>
                                        <asp:MenuItem Text="Credit Card Information" Value="9" ></asp:MenuItem>--%>
                                    </Items>
                                </asp:Menu>
                                </ajax:AjaxPanel>    
                                <ajax:AjaxPanel ID="AjaxPanel19" runat="server" Width="100%">
                                                            
                                <table class="tabContents">
                                    <tr>
                                        <td>
                                            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                                                <asp:View ID="TabGeneral" runat="server">
                                                    <ajax:AjaxPanel ID="AjaxPanel2" runat="server" Width="100%"> 
                                                    <table class="TabArea" width="100%">
                                                        <tr id="Tr13" runat="server">
                                                            <td id="TdGeneralAvailable" runat="server" align="left" class="maintitle">
                                                                GENERAL</td>
                                                        </tr>
                                                        <tr id="Tr14" runat="server">
                                                            <td id="TdGeneralNotAvailable" runat="server" align="left" style="color: red">
                                                                Data Not Available</td>
                                                        </tr>
                                                        <tr id="Tr15" runat="server">
                                                            <td id="TdDataGeneralAvailable" runat="server" style="width: 50%">
                                                                <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="1" cellspacing="1"
                                                                    width="100%">
                                                                    <tr style="background-color: #ffffff">
                                                                        <td style="width: 200px">
                                                                            Account Type</td>
                                                                        <td width="1%">
                                                                            :</td>
                                                                        <td style="width: 100px">
                                                                            <asp:Label ID="LblAccountType" runat="server" Width="208px"></asp:Label></td>
                                                                        <td style="width: 100px" width="200px">
                                                                            Account officer</td>
                                                                        <td style="width: 1px" width="1px">
                                                                            :</td>
                                                                        <td style="width: 100px">
                                                                            <asp:Label ID="LblAccountOfficer" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td style="width: 200px;">
                                                                            Account Opening Date</td>
                                                                        <td width="1%" style="height: 6px">
                                                                            :</td>
                                                                        <td style="width: 100px; height: 6px;">
                                                                            <asp:Label ID="LblOpeningDate" runat="server" Width="207px"></asp:Label></td>
                                                                        <td style="width: 100px; height: 6px;" width="200px">
                                                                            Line Of Business</td>
                                                                        <td style="width: 1px; " width="1px">
                                                                            :</td>
                                                                        <td style="width: 100px; height: 6px;">
                                                                            <asp:Label ID="LblLineOfBusiness" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td style="width: 200px; height: 27px;">
                                                                            Product</td>
                                                                        <td width="1%" style="height: 27px">
                                                                            :</td>
                                                                        <td style="width: 250px; height: 27px;">
                                                                            <asp:Label ID="LblProduct" runat="server" Width="205px"></asp:Label></td>
                                                                        <td style="width: 250px; height: 27px;" width="200px">
                                                                            &nbsp;</td>
                                                                        <td style="width: 1px; height: 27px;" width="1px">
                                                                            &nbsp;</td>
                                                                        <td style="width: 250px; height: 27px;">
                                                                            &nbsp;</td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td style="width: 200px;">
                                                                            Account Currency</td>
                                                                        <td width="1%" style="height: 17px">
                                                                            :</td>
                                                                        <td style="width: 100px; height: 17px;">
                                                                            <asp:Label ID="LblCurrency" runat="server"></asp:Label></td>
                                                                        <td style="width: 100px; height: 17px;" width="200px">
                                                                            &nbsp;</td>
                                                                        <td style="width: 1px; " width="1px">
                                                                            &nbsp;</td>
                                                                        <td style="width: 100px; height: 17px;">
                                                                            &nbsp;</td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td style="width: 200px;">
                                                                            Status</td>
                                                                        <td width="1%" style="height: 17px">
                                                                            :</td>
                                                                        <td style="height: 17px">
                                                                            <asp:Label ID="LblStatus" runat="server"></asp:Label></td>
                                                                        <td style="height: 17px" width="200px">
                                                                            &nbsp;</td>
                                                                        <td style="width: 1px;" width="1px">
                                                                            &nbsp;</td>
                                                                        <td style="height: 17px">
                                                                            &nbsp;</td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td style="width: 200px;">
                                                                            Status Date</td>
                                                                        <td style="height: 17px" width="1%">
                                                                            :</td>
                                                                        <td style="height: 17px">
                                                                            <asp:Label ID="LblStatusDate" runat="server"></asp:Label></td>
                                                                        <td style="height: 17px" width="200px">
                                                                            &nbsp;</td>
                                                                        <td style="width: 1px;" width="1px">
                                                                            &nbsp;</td>
                                                                        <td style="height: 17px">
                                                                            &nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    </ajax:AjaxPanel>                                                    
                                                </asp:View>
                                                <asp:View ID="TabKYCInformation" runat="server">
                                                    <ajax:AjaxPanel ID="AjaxPanel3" runat="server" Width="100%">
                                                    <table style="clip: rect(auto auto auto auto)" width="100%">
                                                        <tr id="Tr1" runat="server" valign="top">
                                                            <td id="Td1" runat="server" align="left" class="maintitle">
                                                                cdd Information</td>
                                                        </tr>
                                                        <tr id="AddressNotFound" runat="server">
                                                            <td id="TdKYCNotAvailable" runat="server" align="left" style="color: #ff0000" valign="top">
                                                                Data Not Available</td>
                                                        </tr>
                                                        <tr id="AddresFound" runat="server">
                                                            <td id="TdKYCAvailable" runat="server" valign="top"><table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="1" cellspacing="1"
                                                                    width="75%">
                                                                <tr style="background-color: #ffffff">
                                                                    <td style="width: 15%">
                                                                        Nominal Transaction Normal</td>
                                                                    <td width="1%">
                                                                        :</td>
                                                                    <td style="width: 35%">
                                                                        <asp:Label ID="LblNominalTransactionNormal" runat="server" Width="420px"></asp:Label></td>
                                                                </tr>
                                                                <tr style="background-color: #ffffff">
                                                                    <td>
                                                                        Tujuan berhubungan dengan bank</td>
                                                                    <td width="1%">
                                                                        :</td>
                                                                    <td style="width: 100px">
                                                                        <asp:Label ID="LblHubDgBank" runat="server" Width="420px"></asp:Label></td>
                                                                </tr>
                                                                <tr style="background-color: #ffffff">
                                                                    <td>
                                                                        Purpose of opening account with bank</td>
                                                                    <td width="1%">
                                                                        :</td>
                                                                    <td style="width: 100px">
                                                                        <asp:Label ID="LblTujuanPenggunaanDana" runat="server" Width="424px"></asp:Label>&nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr style="background-color: #ffffff">
                                                                    <td>
                                                                        Last CDD Updated</td>
                                                                    <td width="1%">
                                                                        :</td>
                                                                    <td style="width: 100px">
                                                                        <asp:Label ID="LblLastUpdatedCIF" runat="server" Width="424px"></asp:Label></td>
                                                                </tr>
                                                            </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    </ajax:AjaxPanel>
                                                </asp:View>
                                                <asp:View ID="TabFinancialInformation" runat="server">
                                                    <ajax:AjaxPanel ID="AjaxPanel14" runat="server" Width="100%">
                                                        <table class="TabArea" width="100%">
                                                        <tr id="Tr2" runat="server">
                                                            <td id="Td4" runat="server" align="left" class="maintitle" style="height: 10px">
                                                                financial information</td>
                                                        </tr>
                                                        <tr id="TrIdNumberFound" runat="server">
                                                            <td id="TdFinancialInformationNotAvailable" runat="server" align="left" style="color: #ff0000;
                                                                height: 15px">
                                                                Data Not Available</td>
                                                        </tr>
                                                        <tr id="Tr3" runat="server">
                                                            <td id="TdFinancialInformationAvailable" runat="server">
                                                                <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="1" cellspacing="1"
                                                                    width="75%">
                                                                    <tr style="background-color: #ffffff">
                                                                        <td style="width: 15%">
                                                                            Current Balance</td>
                                                                        <td width="1%">
                                                                            :</td>
                                                                        <td style="width: 35%">
                                                                            <asp:Label ID="LblCurBalance" runat="server"></asp:Label></td>
                                                                                </tr>                                                         
                                                                        <tr id="Tr4" runat="server">
                                                                        <td id="Td2" runat="server" colspan="3">
                                                                            <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="1" cellspacing="1"
                                                                                width="100%">
                                                                                <tr style="background-color: #ffffff" align="right">
                                                                                    <td style="width: 15%" align="left" bgcolor="gainsboro"><asp:Label ID="Label2" runat="server" Text="Transaction" Font-Bold="True"></asp:Label></td>
                                                                                    <td style="width: 10%" bgcolor="gainsboro">#Debit</td>
                                                                                    <td style="width: 10%" bgcolor="gainsboro">#Credit</td>
                                                                                    <td style="width: 15%" bgcolor="gainsboro">Avg Debit Amount</td>
                                                                                    <td style="width: 15%" bgcolor="gainsboro">Avg Credit Amount</td>
                                                                                    <td style="width: 15%" bgcolor="gainsboro">Total Debit Amount</td>
                                                                                    <td style="width: 15%" bgcolor="gainsboro">Total Credit Amount</td>
                                                                                </tr>                                                                    
                                                                                <tr style="background-color: #ffffff" align="right">
                                                                                    <td style="width: 15%" align="left">Yesterday</td>
                                                                                    <td style="width: 10%"><asp:Label ID="LblYesterdayDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 10%"><asp:Label ID="LblYesterdayCredit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblYesterdayAvgDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblYesterdayAvgCredit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblYesterdayTotalDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblYesterdayTotalCredit" runat="server"></asp:Label></td>
                                                                                </tr>                                                                    
                                                                                <tr style="background-color: #ffffff" align="right">
                                                                                    <td style="width: 15%" align="left">Last Week</td>
                                                                                    <td style="width: 10%"><asp:Label ID="LblLastWeekDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 10%"><asp:Label ID="LblLastWeekCredit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLastWeekAvgDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLastWeekAvgCredit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLastWeekTotalDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLastWeekTotalCredit" runat="server"></asp:Label></td>
                                                                                </tr>                                                                    
                                                                                <tr style="background-color: #ffffff" align="right">
                                                                                    <td style="width: 15%; height: 17px;" align="left">Last Month</td>
                                                                                    <td style="width: 10%; height: 17px;"><asp:Label ID="LblLastMonthDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 10%; height: 17px;"><asp:Label ID="LblLastMonthCredit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%; height: 17px;"><asp:Label ID="LblLastMonthAvgDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%; height: 17px;"><asp:Label ID="LblLastMonthAvgCredit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%; height: 17px;"><asp:Label ID="LblLastMonthTotalDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%; height: 17px;"><asp:Label ID="LblLastMonthTotalCredit" runat="server"></asp:Label></td>
                                                                                </tr>                                                                    
                                                                                <tr style="background-color: #ffffff" align="right">
                                                                                    <td style="width: 15%" align="left">Last 3 Month</td>
                                                                                    <td style="width: 10%"><asp:Label ID="LblLast3MonthDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 10%"><asp:Label ID="LblLast3MonthCredit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast3MonthAvgDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast3MonthAvgCredit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast3MonthTotalDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast3MonthTotalCredit" runat="server"></asp:Label></td>
                                                                                </tr>                                                                    
                                                                                <tr style="background-color: #ffffff" align="right">
                                                                                    <td style="width: 15%" align="left">Last 6 Month</td>
                                                                                    <td style="width: 10%"><asp:Label ID="LblLast6MonthDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 10%"><asp:Label ID="LblLast6MonthCredit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast6MonthAvgDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast6MonthAvgCredit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast6MonthTotalDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast6MonthTotalCredit" runat="server"></asp:Label></td>
                                                                                </tr>                                                                    
                                                                                <tr style="background-color: #ffffff" align="right">
                                                                                    <td style="width: 15%" align="left">Last 1 Year</td>
                                                                                    <td style="width: 10%"><asp:Label ID="LblLast1YearDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 10%"><asp:Label ID="LblLast1YearCredit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast1YearAvgDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast1YearAvgCredit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast1YearTotalDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast1YearTotalCredit" runat="server"></asp:Label></td>
                                                                                </tr>                                                                    
                                                                                <tr style="background-color: #ffffff" align="right">
                                                                                    <td style="width: 15%" align="left">Last 2 Year</td>
                                                                                    <td style="width: 10%"><asp:Label ID="LblLast2YearDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 10%"><asp:Label ID="LblLast2YearCredit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast2YearAvgDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast2YearAvgCredit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast2YearTotalDebit" runat="server"></asp:Label></td>
                                                                                    <td style="width: 15%"><asp:Label ID="LblLast2YearTotalCredit" runat="server"></asp:Label></td>
                                                                                </tr>                                                                    
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                   
                                                                </table>
                                                            </td>
                                                        </tr>



                                                    </table>                                                    
                                                    </ajax:AjaxPanel>
                                                </asp:View>
                                                <asp:View ID="TabTransactionDetail" runat="server">
                                                    <table class="TabArea" width="100%">
                                                        <tr id="Tr16" runat="server">
                                                            <td id="tdTransactionDetail" runat="server" align="left" class="maintitle">
                                                                Transaction Detail</td>
                                                        </tr>
                                                        <tr id="Tr18" runat="server" style="background-color: #ffffff">
                                                            <td id="TdTransactionDetailAvailable" runat="server" >
                                                                <ajax:AjaxPanel ID="AjaxPanel17" runat="server" Width="100%">
                                                                <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="1" cellspacing="1"
                                                                    width="100%">
                                                                   	<tr style="background-color: #ffffff">                                                                   	
                                   	                                      <td nowrap style="height: 27px; width: 15%;"><asp:Label ID="LblFrom" runat="server" Text="From"></asp:Label></td>
                                                                           <td colspan="2" style="height: 27px; width: 200px;">
                                                                                <asp:TextBox ID="TxtDate1" runat="server" CssClass="searcheditbox" TabIndex="2" Width="165px"></asp:TextBox>
                                                                              &nbsp;<input id="popUp1" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                                border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                                                border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif); width: 17px;" title="Click to show calendar"
                                                                                type="button" />
                                                                           </td>

                                                                          <td style="height: 27px; width: 9px;">
                                                                                <asp:Label ID="Label3" runat="server" Text="to"></asp:Label>
                                                                          </td>              
                                                                           <td colspan="2" style="height: 27px">
                                                                                <asp:TextBox ID="TxtDate2" runat="server" CssClass="searcheditbox" TabIndex="2" Width="165px"></asp:TextBox>
                                                                              &nbsp;<input id="PopUp2" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                                border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                                                border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif); width: 17px;" title="Click to show calendar"
                                                                                type="button" />
                                                                           </td>
                                                                     </tr >
                                                                     <tr style="background-color: #ffffff">
                                   	                                      <td nowrap style="height: 27px; width: 15%;"><asp:Label ID="Label4" runat="server" Text="Auxiliary Transaction Code"></asp:Label></td>

                                                                          <td style="height: 27px" colspan="5">  
                                                                                <asp:TextBox ID="TxtAuxTrxCode" runat="server" CssClass="searcheditbox" TabIndex="2" Width="165px"></asp:TextBox>
                                                                              <asp:Button ID="BtnBrowseAuxiliaryTrxCode" runat="server" Text="..." />&nbsp;
                                                                              <asp:HiddenField ID="HfAuxTrxCode" runat="server" />
                                                                              <uc2:PickerAuxTrxCodeMultiple id="PickerAuxTrxCodeMultiple1" runat="server">
                                                                                
                                                                                

                                                                                
                                                                              </uc2:PickerAuxTrxCodeMultiple></td>
                                                                     </tr>
                                                                     <tr style="background-color: #ffffff">
                                   	                                      <td nowrap style="height: 27px; width: 15%;"><asp:Label ID="Label5" runat="server" Text="Debit/Credit"></asp:Label></td>

                                                                          <td style="height: 27px" colspan="5">  
                                                                              <asp:DropDownList ID="DebitOrCreditDropDownList" runat="server">
                                                                                  <asp:ListItem>All</asp:ListItem>
                                                                                  <asp:ListItem Value="D">Debit</asp:ListItem>
                                                                                  <asp:ListItem Value="C">Credit</asp:ListItem>
                                                                              </asp:DropDownList></td>
                                                                     </tr>
                                                                   	<tr style="background-color: #ffffff">
                                   	                                      <td nowrap style="height: 27px; width: 15%;"><asp:Label ID="Label6" runat="server" Text="Amount"></asp:Label></td>

                                                                          <td style="width: 200px; height: 27px">  
                                                                                <asp:TextBox ID="TxtAmount1" runat="server" CssClass="searcheditbox" TabIndex="2" Width="165px"></asp:TextBox>
                                                                                </td>
                                                                          <td style="height: 27px; width: 21px;">
                                                                                <asp:Label ID="Label7" runat="server" Text="to"></asp:Label>
                                                                          </td>              
                                                                           <td colspan="3">
                                                                                <asp:TextBox ID="txtAmount2" runat="server" CssClass="searcheditbox" TabIndex="2" Width="165px"></asp:TextBox>
                                                                           </td>
                                                                    </tr>
                                                                    	<tr style="background-color: #ffffff">
                                   	                                      <td nowrap style="height: 27px; width: 15%;"><asp:Label ID="Label8" runat="server" Text="Transaction Remark"></asp:Label></td>
                                                                            <td colspan="5">
                                                                                <asp:TextBox ID="TransactionRemarkTextBox" runat="server" CssClass="searcheditbox" TabIndex="2" Width="277px"></asp:TextBox>
                                                                            </td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td nowrap="nowrap" style="width: 15%; height: 27px">
                                                                            <asp:Label ID="Label13" runat="server" Text="Memo Remark"></asp:Label></td>
                                                                        <td colspan="5">
                                                                            <asp:TextBox ID="MemoRemarkTextBox" runat="server" CssClass="searcheditbox" TabIndex="2"
                                                                                Width="277px"></asp:TextBox></td>
                                                                    </tr>
                                                                    
                                                                   <tr style="background-color: #ffffff">
                                                                       <td colspan="7">
									                                        <asp:ImageButton ID="ImageButtonSearch" runat="server" ImageUrl="~/Images/button/search.gif" />
                                                                              <asp:ImageButton ID="imgClearSearch" runat="server" ImageUrl="~/Images/button/clearsearch.gif" /></td>
                                                                   </tr>
                                                                                              
                                                                    
                                                           </table>
                                                                </ajax:AjaxPanel>
	                                                        <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		                                                        border="2">
		                                                        <tr>
			                                                        <td bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel4" runat="server">
                                                               <asp:DataGrid ID="GridTransDetail" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                                                    CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="100%">
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
                                                                    <AlternatingItemStyle BackColor="White" />
                                                                    <ItemStyle BackColor="#F7F7DE" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <Columns>
                                                                        <asp:TemplateColumn><ItemTemplate>
                                                                        <asp:CheckBox id="CheckBoxExporttoExcel" runat="server"></asp:CheckBox> 
                                                                        </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="TransactionDetailId" HeaderText="TransactionDetailId"
                                                                            Visible="False"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="TransactionDate" DataFormatString="{0:dd-MMM-yyyy}"
                                                                            HeaderText="Transaction Date" SortExpression="TransactionDetail.TransactionDate  desc"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="AccountOwner" HeaderText="Transaction Branch" SortExpression="CAST(TransactionDetail.AccountOwnerId AS VARCHAR)  + ' - ' + AccountOwner.AccountOwnerName DESC">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="TransactionChannelTypeName" HeaderText="Transaction Channel Type"
                                                                            SortExpression="TransactionChannelTypeName desc"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Country" HeaderText="Country" SortExpression="TransactionDetail.CountryCode + ' - ' + ISNULL(JHCOUN.JHCNAM,'') desc">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="BankCode" HeaderText="Bank Code" SortExpression="BankCode desc">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="BankName" HeaderText="Bank Name" SortExpression="BankName desc">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="AuxiliaryTransactionCode" HeaderText="Auxiliary Transaction Code"
                                                                            SortExpression="AuxiliaryTransactionCode desc"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="TransactionAmount" HeaderText="Transaction Amount" SortExpression="TransactionAmount desc" DataFormatString="{0:#,#.00}">
                                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                Font-Underline="False" HorizontalAlign="Right" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="CurrencyType" HeaderText="Currency Type" SortExpression="CurrencyType desc">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="DebitORCredit" HeaderText="Debit/Credit" SortExpression="DebitORCredit desc">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="MemoRemark" HeaderText="Memo Remark" SortExpression="MemoRemark desc">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="TransactionRemark" HeaderText="Transaction Remark" SortExpression="TransactionRemark desc">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="TransactionExchangeRate" HeaderText="Transaction Exchange Rate"
                                                                            SortExpression="TransactionExchangeRate desc" DataFormatString="{0:#,#.00}">
                                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                Font-Underline="False" HorizontalAlign="Right" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="TransactionLocalEquivalent" HeaderText="Transaction Local Equivalent"
                                                                            SortExpression="TransactionLocalEquivalent desc" DataFormatString="{0:#,#.00}">
                                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                Font-Underline="False" HorizontalAlign="Right" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="TransactionTicketNo" HeaderText="Transaction Ticket#"
                                                                            SortExpression="TransactionTicketNo desc"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="UserId" HeaderText="UserId" SortExpression="UserId desc">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="AccountLawan" HeaderText="Acc No" SortExpression="Transactiondetail.AccountNoLawan  desc">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="AccountNameLawan" HeaderText="Acc Name" SortExpression="Transactiondetail.AccountNameLawan   desc">
                                                                        </asp:BoundColumn>
                                                                        <%--<asp:TemplateColumn HeaderText="Counterpart Acc No">
                                                                            <ItemTemplate>
                                                                            <ajax:AjaxPanel runat="server" ID="test1">
                                                                                <asp:LinkButton ID="lnkcounterpartacc" runat="server" 
                                                                                    onclick="lnkcounterpartacc_Click">Show Counterpart Acc</asp:LinkButton>
                                                                                
                                                                                <asp:Label ID="LblCounterPartAcc" runat="server"></asp:Label>
                                                                                            </ajax:AjaxPanel>
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox runat="server" 
                                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.AccountLawan") %>'></asp:TextBox>
                                                                            </EditItemTemplate>
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Counterpart Name">
                                                                            <ItemTemplate>
                                                                             <ajax:AjaxPanel runat="server" ID="test2">
                                                                                      <asp:LinkButton ID="lnkcounterpartname" runat="server" 
                                                                                    onclick="lnkcounterpartName_Click">Show Counterpart Name</asp:LinkButton>
                                                                                
                                                                                <asp:Label ID="LblCounterPartName" runat="server"></asp:Label>
                                                                                             </ajax:AjaxPanel>
                                                                            </ItemTemplate>
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox runat="server" 
                                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.AccountNameLawan") %>'></asp:TextBox>
                                                                            </EditItemTemplate>
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                                        </asp:TemplateColumn>--%>
                                                          </Columns>
                                                                                <EditItemStyle BackColor="#7C6F57" />
                                                        </asp:datagrid>
                                                                        <asp:Label ID="LabelNoRecordFound" runat="server" Text="No record match with your criteria" CssClass="text" Visible="False"></asp:Label></ajax:ajaxpanel>
				                                                        </td>
		                                                        </tr>
		                                                        <tr>
		                                                            <td style="background-color:#ffffff">
		                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
		                                                                <tr>
			                                                        <td nowrap><ajax:ajaxpanel id="AjaxPanel5" runat="server">&nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
			                                                          &nbsp; </ajax:ajaxpanel> </td>
			                                                         <td style="width: 66%" align="right">&nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" runat="server" ajaxcall="none">Export to Excel</asp:LinkButton>
                                                                         <asp:LinkButton ID="LnkBtnExportAllToExcel" runat="server"  ajaxcall="none">Export All to Excel</asp:LinkButton></td>
		                                                            </tr>
                                                              </table></td>
		                                                        </tr>
		                                                        <tr>
			                                                        <td bgColor="#ffffff">
				                                                        <TABLE id="Table3" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					                                                        border="2">
					                                                        <TR class="regtext" align="center" bgColor="#dddddd">
						                                                        <TD vAlign="top" align="left" width="50%" bgColor="#ffffff"><asp:Label ID="Label9" runat="server" Text="Page"></asp:Label>&nbsp;<ajax:ajaxpanel id="AjaxPanel6" runat="server"><asp:label id="PageCurrentPage" runat="server" CssClass="regtext">0</asp:label>&nbsp;<asp:Label ID="Label10" runat="server" Text="of"></asp:Label>&nbsp;
							                                                        <asp:label id="PageTotalPages" runat="server" CssClass="regtext">0</asp:label></ajax:ajaxpanel></TD>
						                                                        <TD vAlign="top" align="right" width="50%" bgColor="#ffffff"><asp:Label ID="Label11" runat="server" Text="Total Records"></asp:Label>&nbsp;
							                                                        <ajax:ajaxpanel id="AjaxPanel7" runat="server">
                                                                                        <asp:label id="PageTotalRows" runat="server">0</asp:label></ajax:ajaxpanel></TD>
					                                                        </TR>
				                                                        </TABLE>
				                                                        <TABLE id="Table4" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					                                                        border="2">
					                                                        <TR bgColor="#ffffff">
						                                                        <TD class="regtext" vAlign="middle" align="left" colSpan="11" height="7">
							                                                        <HR color="#f40101" noShade SIZE="1">
						                                                        </TD>
					                                                        </TR>
					                                                        <TR>
						                                                        <TD class="regtext" vAlign="middle" align="left" width="63" bgColor="#ffffff" style="height: 86px"><asp:Label ID="Label12" runat="server" Text="Go to page"></asp:Label></TD>
						                                                        <TD class="regtext" vAlign="middle" align="left" width="5" bgColor="#ffffff" style="height: 86px">
                                                                                    <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
									                                                        <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                                                                                <asp:TextBox ID="TextGoToPage" runat="server" 
                                                                            CssClass="searcheditbox" Width="38px"></asp:TextBox></ajax:AjaxPanel>
								                                                        </font></TD>
						                                                        <TD class="regtext" vAlign="middle" align="left" bgColor="#ffffff" style="height: 86px"><ajax:ajaxpanel id="AjaxPanel9" runat="server">
								                                                        <asp:imagebutton id="ImageButtonGo" runat="server" SkinID="GoButton" ImageUrl="~/Images/button/go.gif"></asp:imagebutton></ajax:ajaxpanel>
							                                                        </TD>
						                                                        <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff" style="height: 86px"><IMG height="5" src="images/first.gif" width="6">
						                                                        </TD>
						                                                        <TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff" style="height: 86px"><ajax:ajaxpanel id="AjaxPanel10" runat="server">
								                                                        <asp:linkbutton id="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First" OnCommand="PageNavigate">First</asp:linkbutton></ajax:ajaxpanel>
							                                                        </TD>
						                                                        <TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff" style="height: 86px"><IMG height="5" src="images/prev.gif" width="6"></TD>
						                                                        <TD class="regtext" vAlign="middle" align="right" width="14" bgColor="#ffffff" style="height: 86px">
						                                                        <ajax:ajaxpanel id="AjaxPanel11" runat="server">
								                                                        <asp:linkbutton id="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev" OnCommand="PageNavigate">Previous</asp:linkbutton></ajax:ajaxpanel>
							                                                        </TD>
						                                                        <TD class="regtext" vAlign="middle" align="right" width="60" bgColor="#ffffff" style="height: 86px"><ajax:ajaxpanel id="AjaxPanel12" runat="server"><A class="pageNav" href="#">
									                                                        <asp:linkbutton id="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next" OnCommand="PageNavigate">Next</asp:linkbutton></A></ajax:ajaxpanel></TD>
						                                                        <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff" style="height: 86px"><IMG height="5" src="images/next.gif" width="6"></TD>
						                                                        <TD class="regtext" vAlign="middle" align="left" width="25" bgColor="#ffffff" style="height: 86px"><ajax:ajaxpanel id="AjaxPanel13" runat="server">
								                                                        <asp:linkbutton id="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last" OnCommand="PageNavigate">Last</asp:linkbutton></ajax:ajaxpanel>
							                                                        </TD>
						                                                        <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff" style="height: 86px"><IMG height="5" src="images/last.gif" width="6"></TD>
					                                                        </TR>
				                                                        </TABLE>
			                                                        </td>
		                                                        </tr>
	                                                        </table>
<%--	                                                            <ajax:AjaxPanel ID="AjaxPanel18" runat="server" Width="100%">	                                                            
                                                                    <br />
                                                                    <br />
                                                                    <br />
                                                                <Web:ChartControl ID="ChartControl1" runat="server" BorderColor="White"
                                                                    BorderWidth="1px" HasChartLegend="False" Width="954px" YCustomEnd="0"
                                                                    YCustomStart="0" YValuesInterval="0" GridLines="Both" ShowTitlesOnBackground="False" Height="500px" LeftChartPadding="50">
                                                                    <YAxisFont Font="Tahoma, 8pt, style=Bold" ForeColor="White" StringFormat="Far,Near,Character,LineLimit" />
                                                                    <XTitle ForeColor="White" StringFormat="Center,Far,Character,LineLimit"
                                                                        Text="Transaction Date" />
                                                                    <ChartTitle Font="Tahoma, 12pt, style=Bold" ForeColor="White" StringFormat="Near,Near,Character,LineLimit" />
                                                                    <Border Color="LightGray" />
                                                                    <XAxisFont Font="Tahoma, 8pt, style=Bold" ForeColor="White" StringFormat="Center,Near,Character,LineLimit" />
                                                                    <Background Color="#FFFF80" EndPoint="100, 400" HatchStyle="DiagonalBrick" Angle="90" />
                                                                    <Charts>
                                                                        <Web:ColumnChart Name="Chart" ShowLineMarkers="False">
                                                                            <Fill CenterColor="LightGray" Color="LightGray" />
                                                                            <DataLabels Position="Center" Font="Tahoma, 8pt, style=Bold" ForeColor="White" NumberFormat="#,0.00" Visible="True">
                                                                                <Border Color="Transparent" />
                                                                                <Background Color="Transparent" />
                                                                            </DataLabels>
                                                                            <Line Width="1" />
                                                                        </Web:ColumnChart>
                                                                    </Charts>
                                                                    <YTitle ForeColor="White" StringFormat="Near,Near,Character,DirectionVertical"
                                                                        Text="Amount (Thousands)" />
                                                                    <PlotBackground Angle="90" EndPoint="100, 400" Color="Gray" />
                                                                </Web:ChartControl>
                                                                </ajax:AjaxPanel>--%>
                                                           </td>
                                                            
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View ID="TabGraphicalTransaction" runat="server">
                                                    <%--<ajax:AjaxPanel ID="AjaxPanel15" runat="server" Width="100%">--%>
                                                    <table class="TabArea" width="100%">
                                                        <tr id="Tr19" runat="server">
                                                            <td id="Td33" runat="server" align="left" class="maintitle" style="height: 29px" width="100%">
                                                                Graphical Transaction</td>
                                                        </tr>
                                                        <tr id="Tr21">
                                                            <td valign="top">
                                                                <%--<iframe src="graphicaltransactionanalysisview.aspx" width="100%" scrolling="yes" height="800" id="IFrameGraphicalTransaction"></iframe>--%>
                                                                <iframe src="GraphicalTransactionAnalysisView.aspx?AccountNo=<%=Me.GetAccountNo.ToString()%>&StartDate=<%=Me.GetStartDate()%>&EndDate=<%=Me.GetEndDate()%>" width="1000" height="1100" scrolling="yes" ></iframe>
<%--                                                                    <iframe src="GraphicalTransactionAnalysisView.aspx?AccountNo=<%=Me.Request.Params.Item("AccountNo").ToString()%>&StartDate=<%=Sahassa.AML.Commonly.ConvertToDate("yyyy-MM-dd", Me.Request.Params.Item("StartDate")).ToString ("yyyy-MM-dd")%>&EndDate=<%=Sahassa.AML.Commonly.ConvertToDate("yyyy-MM-dd", Me.Request.Params.Item("EndDate")).ToString ("yyyy-MM-dd")%>" width="1000" height="1100" scrolling="yes" >
                                                                    </iframe>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <%-- </ajax:AjaxPanel>--%>
                                                </asp:View>
                                                <asp:View ID="TabPeerGroupAnalysis" runat="server">
                                                    <ajax:AjaxPanel ID="AjaxPanel16" runat="server" Width="100%">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="maintitle">
                                                                Peer Group Analysis</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table style="width: 100%">
                                                                    <tr>
                                                                        <td style="width: 1%" nowrap>
                                                                            Peer Group Name</td>
                                                                        <td width="1"  >
                                                                            :</td>
                                                                        <td colspan="2" width="99%">
                                                                            <asp:DropDownList ID="cboPeerGroupName" runat="server" CssClass="combobox">
                                                                            </asp:DropDownList></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 1%" nowrap>
                                                                            Peer Group Characteristic</td>
                                                                        <td width="1">
                                                                            :</td>
                                                                        <td colspan="2" width="99%">
                                                                            <asp:DropDownList ID="cboPeerGroupChar" runat="server" CssClass="combobox">
                                                                            </asp:DropDownList></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4">
                                                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/button/go.gif" /></td>
                                                                    </tr>
                                                                </table>
                                                                <table style="width: 100%">
                                                                    <tr>
                                                                        <td>
                                                                            </td>
                                                                        <td>
                                                                            </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <Web:ChartControl ID="ChartControlAccount" runat="server" BorderStyle="Outset" BorderWidth="5px" ChartPadding="30" GridLines="Both" HasChartLegend="False" Padding="13" YCustomEnd="100" YCustomStart="0" YValuesInterval="0" TopPadding="20">
                                                                                
                                                                                

                                                                                
                                                                                <YAxisFont Font="Tahoma, 8pt, style=Bold" ForeColor="115, 138, 156" StringFormat="Far,Near,Character,LineLimit" />
                                                                                
                                                                                

                                                                                
                                                                                <XTitle ForeColor="White" StringFormat="Center,Near,Character,LineLimit" Font="Tahoma, 8pt, style=Bold" />
                                                                                
                                                                                
                                                                                
                                                                                <PlotBackground Angle="90" EndPoint="100, 400" ForeColor="#FFFFC0" />
                                                                                
                                                                                
                                                                                
                                                                                <ChartTitle Font="Verdana, 10pt, style=Bold" ForeColor="DeepSkyBlue" StringFormat="Near,Near,Character,LineLimit" Text="Account Characteristic" />
                                                                                
                                                                                
                                                                                
                                                                                <Border Color="222, 186, 132" Width="2" />
                                                                                
                                                                                
                                                                                
                                                                                <XAxisFont Font="Tahoma, 8pt, style=Bold" ForeColor="115, 138, 156" StringFormat="Center,Near,Character,LineLimit" />
                                                                                
                                                                                

                                                                                
                                                                                <Background Angle="90" Color="#FFFF80" EndPoint="100, 400" HatchStyle="DiagonalBrick" ForeColor="#FFFF80" Type="LinearGradient" />
                                                                                
                                                                                
                                                                                <charts>
                                                                                
                                                                                <Web:ColumnChart ShowLineMarkers="False">
                                                                                        <datalabels font="Tahoma, 8pt, style=Bold" forecolor="DarkGray" numberformat="#,0.00" visible="True">
                                                                                            <border color="Transparent" />
                                                                                            <background color="Transparent" />
                                                                                        </datalabels>
                                                                                    </Web:ColumnChart>
                                                                                

                                                                                </charts>
                                                                                

                                                                                

                                                                                
                                                                                <YTitle ForeColor="White" StringFormat="Center,Near,Character,DirectionVertical" Font="Tahoma, 8pt, style=Bold" />
                                                                                
                                                                                

                                                                                
                                                                            </Web:ChartControl></td>
                                                                        <td>
                                                                            <Web:ChartControl ID="ChartControlPeerGroup" runat="server" BorderStyle="Outset" BorderWidth="5px" ChartPadding="30" GridLines="Both" HasChartLegend="False" Padding="13" ShowTitlesOnBackground="False" YCustomEnd="100" YCustomStart="0" YValuesInterval="0" DefaultImageUrl="~/Images/NoDataAvailable.GIF">
                                                                                
                                                                                
                                                                                
                                                                                <YAxisFont Font="Tahoma, 8pt, style=Bold" ForeColor="115, 138, 156" StringFormat="Far,Near,Character,LineLimit" />
                                                                                
                                                                                
                                                                                
                                                                                <XTitle ForeColor="White" StringFormat="Center,Near,Character,LineLimit" Font="Tahoma, 8pt, style=Bold" />
                                                                                
                                                                                
                                                                                
                                                                                <ChartTitle Font="Verdana, 10pt, style=Bold" ForeColor="DeepSkyBlue" StringFormat="Near,Near,Character,LineLimit" Text="Peer Group Characteristic" />
                                                                                
                                                                                
                                                                                
                                                                                <Border Color="222, 186, 132" Width="2" />
                                                                                
                                                                                
                                                                                
                                                                                <XAxisFont Font="Tahoma, 8pt, style=Bold" ForeColor="115, 138, 156" StringFormat="Center,Near,Character,LineLimit" />
                                                                                
                                                                                
                                                                                
                                                                                <Background Color="#FFFF80" Angle="90" EndPoint="100, 400" ForeColor="#FFFF80" HatchStyle="DiagonalBrick" Type="LinearGradient" />
                                                                                
                                                                                
                                                                                <charts>
                                                                                
                                                                                <Web:ColumnChart ShowLineMarkers="False">
                                                                                        <datalabels font="Tahoma, 8pt, style=Bold" forecolor="DarkGray" numberformat="#,0.00" visible="True">
                                                                                            <border color="Transparent" />
                                                                                            <background color="Transparent" />
                                                                                        </datalabels>
                                                                                    </Web:ColumnChart>
                                                                                
                                                                                </charts>
                                                                                
                                                                                
                                                                                
                                                                                <YTitle ForeColor="White" StringFormat="Center,Near,Character,DirectionVertical" Font="Tahoma, 8pt, style=Bold" />
                                                                                
                                                                                
                                                                                
                                                                            </Web:ChartControl></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 15px">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    </ajax:AjaxPanel>
                                                    
                                                </asp:View>
                                                <asp:View ID="TabJointAccount" runat="server">
                                                    <table width="100%"  class="TabArea">
                                                        <tr id="Tr5" runat="server">
                                                            <td id="Td32" runat="server" align="left" class="maintitle">
                                                                Joint Account Information</td>
                                                        </tr>
                                                        <tr runat="server" id="Tr17">
                                                            <td id="TdJointAccountNotAvailable" align="left" style="color: #ff0000; height: 15px;" runat="server">
                                                                This account is not Joint Account</td>
                                                        </tr>
                                                        <tr runat="server" id="Tr6">
                                                            
                                                            <td id="TdJointAccountAvailable" runat="server">
                                                                <table>
                                                                    <tr><td align="left" style="color: #ff0000; height: 15px;">This account is Joint Account of following customers:</td></tr>
                                                                    <tr>
                                                                        <td><asp:DataGrid ID="GridJointAccount" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                                    CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="300px">
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
                                                                    <AlternatingItemStyle BackColor="White" />
                                                                    <ItemStyle BackColor="#F7F7DE" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <Columns>
                                                                        <%--<asp:BoundColumn DataField="CIFNo" HeaderText="CIF Number" SortExpression="CIFNo desc"></asp:BoundColumn>--%>
                                                                        <asp:TemplateColumn HeaderText="CIF Number" SortExpression="CIFNo desc">
                                                                          <ItemTemplate> 
                                                                            <%#GenerateCIFLink(Eval("CIFNo"))%>
                                                                          </ItemTemplate>
                                                                        </asp:TemplateColumn>   
                                                                        <asp:BoundColumn DataField="CustomerName" HeaderText="Customer Name" SortExpression="CustomerName desc">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="JointAccountOwnership" HeaderText="Account Ownership Type"
                                                                            SortExpression="JointAccountOwnership desc"></asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                                          </td>
                                                                     </tr>
                                                                </table>
                                                                </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>  
                                                 <asp:View ID="StatisticFinancialModus" runat="server">
                                                     &nbsp;<asp:GridView ID="GridViewFinancialModus" runat="server" AutoGenerateColumns="False"
                                                         BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                                         CellPadding="4" EmptyDataText="There is no Statistic Fianancial Modus Record"
                                                         ForeColor="Black" ShowHeader="False" EnableModelValidation="True">
                                                         <RowStyle BackColor="#F7F7DE" />
                                                         <Columns>
                                                             <asp:BoundField DataField="TransactionPeriod" HeaderText="TransactionPeriod" />
                                                             <asp:BoundField DataField="AmountDebet" HeaderText="D" DataFormatString="{0:###,###.##}">
                                                                 <ItemStyle Width="100px" />
                                                             </asp:BoundField>
                                                             <asp:BoundField DataField="AmountKredit" HeaderText="C" DataFormatString="{0:###,###.##}">
                                                                 <ItemStyle Width="100px" />
                                                             </asp:BoundField>
                                                             <%--<asp:BoundField DataField="FreqDebet" HeaderText="D">
                                                                 <ItemStyle Width="100px" />
                                                             </asp:BoundField>
                                                             <asp:BoundField DataField="FreqKredit" HeaderText="C">
                                                                 <ItemStyle Width="100px" />
                                                             </asp:BoundField>
                                                             <asp:BoundField DataField="FreqPerDayPeriodDebet" HeaderText="D">
                                                                 <ItemStyle Width="100px" />
                                                             </asp:BoundField>
                                                             <asp:BoundField DataField="FreqPerDayPeriodKredit" HeaderText="C">
                                                                 <ItemStyle Width="100px" />
                                                             </asp:BoundField>--%>
                                                         </Columns>
                                                         <FooterStyle BackColor="#CCCC99" />
                                                         <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                         <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                         <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                         <AlternatingRowStyle BackColor="White" />
                                                     </asp:GridView>
                                                 </asp:View>             
                                                <asp:View ID="TabLoanInformation" runat="server">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="300">
                                                                Jenis Pinjaman</td>
                                                            <td width="1">
                                                                :</td>
                                                            <td width="70%">
                                                                <asp:Label ID="LblJenisPinjaman" runat="server" Text="" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="300">
                                                                Plafon</td>
                                                            <td width="1">
                                                                :</td>
                                                            <td width="70%">
                                                                <asp:Label ID="LblPlafon" runat="server" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="300">
                                                                Opening Date</td>
                                                            <td width="1">
                                                                :</td>
                                                            <td width="70%">
                                                                <asp:Label ID="LblOpenDate" runat="server" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="300">
                                                                Maturity Date</td>
                                                            <td width="1">
                                                                :</td>
                                                            <td width="70%">
                                                                <asp:Label ID="LblMaturityDate" runat="server" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="300">
                                                                Closing Date</td>
                                                            <td width="1">
                                                                :</td>
                                                            <td width="70%">
                                                                <asp:Label ID="LblClosingDate" runat="server" /></td>
                                                        </tr>
                                                    </table>
                                                    <asp:GridView ID="GrdCollateral" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical">
                                                        <Columns>
                                                            <asp:BoundField DataField="Kolateral" HeaderText="Colateral" />
                                                            <asp:BoundField DataField="NilaiKolateral" DataFormatString="{0:#,###,00}" HeaderText="Value"
                                                                HtmlEncode="False" />
                                                        </Columns>
                                                        <RowStyle BackColor="#F7F7DE" />
                                                        <FooterStyle BackColor="#CCCC99" />
                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                    </asp:GridView>
                                                </asp:View>
                                                <asp:View ID="TabCreditCardInformation" runat="server">
                                                <table width="100%">
                                                        <tr>
                                                            <td width="300px">
                                                                Outstanding
                                                            </td>
                                                            <td width="1px">
                                                                :</td>
                                                            <td width="70%">
                                                                <asp:Label runat="server" ID="LblOUtStanding" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="300px">
                                                                Limit Credit Card</td>
                                                            <td width="1px">
                                                                :</td>
                                                            <td width="70%">
                                                                <asp:Label ID="LblLimitCreditCard" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="300px">
                                                                &nbsp;</td>
                                                            <td width="1px">
                                                                &nbsp;</td>
                                                            <td width="70%">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="300px">
                                                                &nbsp;</td>
                                                            <td width="1px">
                                                                &nbsp;</td>
                                                            <td width="70%">
                                                                &nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </asp:View>                                                                                  
                                            </asp:MultiView>
                                        </td>
                                    </tr>
                                </table>
                                </ajax:AjaxPanel>    
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 21px">
                </td>
                <td bgcolor="#ffffff" style="height: 21px">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 94px" valign="top">
                                <asp:Label ID="LblMessage" runat="server" Font-Bold="True" Font-Size="12px" ForeColor="#FF8000"></asp:Label></td>
                            <td>
                                <asp:Label ID="Lblmessageisi" runat="server" Font-Bold="True" Font-Size="12px" ForeColor="#FF8000"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 21px">&nbsp;</td>
                <td bgcolor="#ffffff" style="height: 21px">
                    <img id="BackImageButton" src="Images/button/back.gif" onclick="javascript:history.back()" />&nbsp;<asp:ImageButton
                        ID="ImgPrint" runat="server" ImageUrl="~/Images/button/print.gif" /></td>
            </tr>
        </table>
</asp:Content>