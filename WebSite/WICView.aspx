<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="WICView.aspx.vb" Inherits="WICView" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script src="Script/popcalendar.js"></script>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div >
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="10" height="100%" /></td>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <img src="Images/dot_title.gif" width="17" height="17" />
                                                <strong>
                                                    <asp:Label ID="Label1" runat="server" Text="WIC - View"></asp:Label>
                                                </strong>
                                                <hr />
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Label6" runat="server" Text="Search Criteria" Font-Bold="True"></asp:Label>
                                                            &nbsp;</td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('SearchCriteria','searchimage4','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="searchimage4" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="SearchCriteria">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label7" runat="server" Text="Name"></asp:Label></td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="TxtFullName" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="250px" MaxLength="50"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="width: 15%; background-color: #FFF7E6;" valign="top">
                                                            <asp:Label ID="LblRequestedDateSearch" runat="server" Text="Date of Birth"></asp:Label></td>
                                                        <td nowrap style="width: 75%;" valign="top">
                                                            <asp:TextBox ID="TxtDateOfBirth" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="100px"></asp:TextBox><input id="popUpDateOfBirth" title="Click to show calendar"
                                                                    style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                                    border-left: #ffffff 0px solid; border-bottom: #ffffff 0px solid; height: 17px;
                                                                    background-image: url(Script/Calendar/cal.gif); width: 16px;" type="button" onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')"
                                                                    runat="server" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="width: 15%; background-color: #FFF7E6; height: 22px;" valign="top">
                                                            <asp:Label ID="LblAction" runat="server" Text="IDType"></asp:Label></td>
                                                        <td nowrap style="width: 75%; height: 22px;" valign="top">
                                                            <asp:DropDownList ID="CboIDType" runat="server" CssClass="comboBox">
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label2" runat="server" Text="ID Number"></asp:Label></td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="TxtIDNo" TabIndex="2" runat="server" CssClass="searcheditbox" Width="125px"
                                                                MaxLength="50"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label9" runat="server" Text="NPWP"></asp:Label></td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtNPWP" TabIndex="2" runat="server" CssClass="searcheditbox" Width="125px"
                                                                MaxLength="50"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label3" runat="server" Text="Transaction Date"></asp:Label></td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtTransactionDate" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="125px" MaxLength="50"></asp:TextBox>
                                                            <input id="popUpTransactionDate" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                                border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                                border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                                width: 16px;" type="button" onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')"
                                                                runat="server"></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%; height: 22px;">
                                                            <asp:Label ID="Label8" runat="server" Text="PJK Office"></asp:Label></td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%; height: 22px;">
                                                            <asp:TextBox ID="txtPJKOffice" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="125px" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label4" runat="server" Text="Transaction Nominal CashIn"></asp:Label></td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtTransNominalBCashIn" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="125px" MaxLength="50"></asp:TextBox>
                                                            s/d
                                                            <asp:TextBox ID="txtTransNominalUCashIn" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label10" runat="server" Text="Transaction Nominal CashOut"></asp:Label></td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtTransNominalBCashOut" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="125px" MaxLength="50"></asp:TextBox>
                                                            s/d
                                                            <asp:TextBox ID="txtTransNominalUCashOut" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            PPATk Confirmation No</td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="TxtPPATK" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap="nowrap" style="width: 15%; background-color: #fff7e6">
                                                            Last Update Date</td>
                                                        <td nowrap="nowrap" style="width: 35%; background-color: #ffffff">
                                                            <asp:TextBox ID="TxtlastUpdateStart" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox>
                                                            <input id="btnupdateStart" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                                border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                                border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                                width: 16px;" type="button" onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')"
                                                                runat="server">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                        </td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="height: 10px">
                                                            &nbsp;<asp:ImageButton ID="ImageButtonSearch" TabIndex="3" runat="server" SkinID="SearchButton"
                                                                CausesValidation="False" ImageUrl="~/Images/Button/Search.gif"></asp:ImageButton>&nbsp;<asp:ImageButton
                                                                    ID="Imagebutton1" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/clearSearch.gif" TabIndex="3" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr id="TR1">
                                            <td valign="top" width="98%" bgcolor="#ffffff" style="height: 357px">
                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="0" width="100%" bgcolor="#dddddd"
                                                    border="2">
                                                    <tr>
                                                        <td bgcolor="#ffffff" background="images/testbg.gif">
                                                            <asp:DataGrid ID="GridDataView" runat="server" Font-Size="XX-Small"
                                                                CellPadding="4" AllowPaging="True" Width="100%" GridLines="Vertical"
                                                                AllowSorting="True" ForeColor="Black" Font-Bold="False" Font-Italic="False"
                                                                Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                                                                HorizontalAlign="Left" AutoGenerateColumns="False" BackColor="White" 
                                                                BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px">
                                                                <AlternatingItemStyle BackColor="White" />
                                                                <Columns>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="2%" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="Pk_WIC_Id" Visible="False">
                                                                        <HeaderStyle Width="0%" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="INDV_FK_MsIDType_Id" SortExpression="INDV_FK_MsIDType_Id desc"
                                                                        Visible="False">
                                                                        <HeaderStyle Width="25%" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                            Font-Strikeout="False" Font-Underline="False" Wrap="False" HorizontalAlign="Left"
                                                                            VerticalAlign="Middle" />
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="TipeTerlapor" Visible="False"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="INDV_NPWP" HeaderText="Individual NPWP" SortExpression="INDV_NPWP desc"
                                                                        Visible="False"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="CORP_NPWP" HeaderText="Corporate NPWP" SortExpression="CORP_NPWP desc"
                                                                        Visible="False"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="INDV_NamaLengkap" HeaderText="Individual Name" SortExpression="INDV_NamaLengkap desc"
                                                                        Visible="False">
                                                                        <HeaderStyle Width="25%" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                            Font-Strikeout="False" Font-Underline="False" Wrap="False" HorizontalAlign="Left"
                                                                            VerticalAlign="Middle" />
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="CORP_Nama" HeaderText="Corporate Name" SortExpression="CORP_Nama desc"
                                                                        Visible="False"></asp:BoundColumn>
                                                                    <asp:TemplateColumn HeaderText="No.">
                                                                        <HeaderStyle ForeColor="White" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="INDV_NamaLengkap" HeaderText="Name" SortExpression="INDV_NamaLengkap desc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="INDV_TanggalLahir" HeaderText="Date of Birth" SortExpression="INDV_TanggalLahir desc">
                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" />
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                                    </asp:BoundColumn>
                                                                    <asp:TemplateColumn HeaderText="ID Type" SortExpression="INDV_FK_MsIDType_Id desc">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="LblIDType" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="INDV_NomorId" HeaderText="ID Number" SortExpression="INDV_NomorId desc">
                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" />
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="INDV_NPWP" HeaderText="NPWP" SortExpression="INDV_NPWP desc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="KantorPJKCashIn" HeaderText="CashIn" SortExpression="KantorPJKCashIn desc">
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" Wrap="False" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="KantorPJKCashOut" HeaderText="CashOut" SortExpression="KantorPJKCashOut desc">
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" Wrap="False" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="NoRekening" HeaderText="Account Number" SortExpression="NoRekening desc"
                                                                        Visible="False"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="LastTransactionDateCashIn" HeaderText="CashIn" SortExpression="LastTransactionDateCashIn desc">
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" Wrap="False" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="LastTransactionDateCashOut" HeaderText="CashOut" SortExpression="LastTransactionDateCashOut desc">
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Middle" HorizontalAlign="Left" Wrap="False" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="CashInNominal" HeaderText="CashIn" SortExpression="CashInNominal desc">
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="CashOutNominal" HeaderText="CashOut" SortExpression="CashOutNominal desc">
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Detail" HeaderText="Detail" SortExpression="Detail desc">
                                                                    </asp:BoundColumn>
                                                                    <asp:TemplateColumn HeaderText="Status">
                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Strikeout="False" Font-Underline="True"
                                                                            Wrap="False" HorizontalAlign="Left" ForeColor="White" VerticalAlign="Middle" />
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="LblStatus" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="PPATKConfirmationNumber" HeaderText="PPATK Confirmation No"
                                                                        SortExpression="PPATKConfirmationNumber  desc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="LastUpdateDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Last Update Date"
                                                                        SortExpression="LastUpdateDate  desc"></asp:BoundColumn>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="Btndetail" runat="server" CommandName="detail">Detail</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="btnEdit" runat="server" CommandName="edit">Edit</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="btnDelete" runat="server" CommandName="delete">Delete</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                                <FooterStyle BackColor="#CCCC99" />
                                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                <ItemStyle BackColor="#F7F7DE" />
                                                                <PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" 
                                                                    BackColor="#F7F7DE" Mode="NumericPages">
                                                                </PagerStyle>
                                                                <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #ffffff">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td nowrap style="height: 20px">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td style="width: 81px">
                                                                                    <asp:CheckBox ID="CheckBoxSelectAll" runat="server" AutoPostBack="True" Text="Select All" />
                                                                                </td>
                                                                                <td style="width: 62px">
                                                                                    Export To:
                                                                                </td>
                                                                                <td style="width: 30px">
                                                                                    <asp:DropDownList ID="cboFormatFile" runat="server" CssClass="combobox">
                                                                                        <asp:ListItem>Excel</asp:ListItem>
                                                                                        <asp:ListItem>Xml</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td style="width: 120px">
                                                                                    <asp:RadioButtonList ID="rblData" runat="server" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Selected="True">Selected</asp:ListItem>
                                                                                        <asp:ListItem>All</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="lnkExportData" runat="server" Text="Export" ajaxcall="none"></asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="height: 20px">
                                                                        &nbsp;&nbsp;
                                                                    </td>
                                                                    <td align="right" nowrap style="height: 20px">
                                                                        <asp:LinkButton ID="LinkButtonAddNew" runat="server">Add New</asp:LinkButton>
                                                                        &nbsp;&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>   
                                                <table width="100%">                                    
                                                <tr>
                                                    <td bgcolor="#ffffff">
                                                        <table id="Table3" bgcolor="#ffffff" border="2" bordercolor="#ffffff" cellpadding="2"
                                                            cellspacing="1" width="100%">
                                                            <tr align="center" bgcolor="#dddddd" class="regtext">
                                                                <td align="left" bgcolor="#ffffff" style="height: 19px" valign="top" width="50%">
                                                                    Page&nbsp;<asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>
                                                                    &nbsp;of&nbsp;
                                                                    <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label>
                                                                </td>
                                                                <td align="right" bgcolor="#ffffff" style="height: 19px" valign="top" width="50%">
                                                                    Total Records&nbsp;
                                                                    <asp:Label ID="PageTotalRows" runat="server">0</asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table id="Table4" bgcolor="#ffffff" border="2" bordercolor="#ffffff" cellpadding="2"
                                                            cellspacing="1" width="100%">
                                                            <tr bgcolor="#ffffff">
                                                                <td align="left" class="regtext" colspan="11" height="7" valign="middle">
                                                                    <hr></hr>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="63">
                                                                    Go to page</td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="5">
                                                                    <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                                        <asp:TextBox ID="TextGoToPage" runat="server" CssClass="searcheditbox" Width="38px"></asp:TextBox>
                                                                    </font>
                                                                </td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle">
                                                                    <asp:ImageButton ID="ImageButtonGo" runat="server" ImageUrl="~/Images/Button/Go.gif"
                                                                        SkinID="GoButton" />
                                                                </td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                    <img height="5" src="images/first.gif" width="6"> </img>
                                                                </td>
                                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                    <asp:LinkButton ID="LinkButtonFirst" runat="server" CommandName="First" CssClass="regtext"
                                                                        OnCommand="PageNavigate">First</asp:LinkButton>
                                                                </td>
                                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                    <img height="5" src="images/prev.gif" width="6"></img></td>
                                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="14">
                                                                    <asp:LinkButton ID="LinkButtonPrevious" runat="server" CommandName="Prev" CssClass="regtext"
                                                                        OnCommand="PageNavigate">Previous</asp:LinkButton>
                                                                </td>
                                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="60">
                                                                    <a class="pageNav" href="#">
                                                                        <asp:LinkButton ID="LinkButtonNext" runat="server" CommandName="Next" CssClass="regtext"
                                                                            OnCommand="PageNavigate">Next</asp:LinkButton>
                                                                    </a>
                                                                </td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                    <img height="5" src="images/next.gif" width="6"></img></td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="25">
                                                                    <asp:LinkButton ID="LinkButtonLast" runat="server" CommandName="Last" CssClass="regtext"
                                                                        OnCommand="PageNavigate">Last</asp:LinkButton>
                                                                </td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                    <img height="5" src="images/last.gif" width="6"></img></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </table>   
                                            </td>
                                        </tr>
                                    </table>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="Images/blank.gif" width="5" height="1" /></td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            &nbsp;</td>
                        <td width="99%" background="Images/button-bground.gif">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
