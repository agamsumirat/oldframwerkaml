<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DataUpdatingParameterApprovalDetail.aspx.vb" Inherits="DataUpdatingParameterApprovalDetail" title="Data Updating Parameter Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <TABLE id="TableDetail" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="2" runat="server">
	       <TR class="formText" id="UserAdd1">
		     <td width="5%" bgcolor="#ffffff" style="height: 32px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none; width: 3%;"></td>
		    <td Width="35%" bgcolor="#ffffff" style="height: 32px;" colspan="3">
                <br />
                <asp:Panel ID="Panel2" runat="server" GroupingText="Based on Abnormal Transaction"
                    Height="50px">
                    Abnormal Trans &gt;= &nbsp;&nbsp;<asp:Label ID="LabelAbnormalTransAdd" runat="server"></asp:Label><br />
                </asp:Panel>
            </td>
	    </TR>
	    <tr class="formText" id="UserEdi1">
		    <td height="24" colspan="4" bgcolor="#ffffcc">&nbsp;Old Values</td>
		    <td colspan="4" bgcolor="#ffffcc">&nbsp;New Values</td>
	    </tr>
	    <tr class="formText" id="UserEdi2">
		    <td width="5%" bgcolor="#ffffff" style="height: 32px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none; width: 3%;"></td>
		    <td Width="35%" bgcolor="#ffffff" style="height: 32px;" colspan="3">
                <br />
                <asp:Panel ID="PanelDataUpdatingParameter" runat="server" GroupingText="Based on Abnormal Transaction"
                    Height="50px">
                    Abnormal Trans &gt;= &nbsp;&nbsp;<asp:Label ID="LabelAbnormalTransOld" runat="server"></asp:Label><br />
                </asp:Panel>
            </td>
		    <td width="5%" bgcolor="#ffffff" style="height: 32px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none; width: 3%;"></td>
            <td Width="320px" bgcolor="#ffffff" colspan="3" style="height: 32px">
                <br />
                <asp:Panel ID="Panel1" runat="server" GroupingText="Based on Abnormal Transaction"
                    Height="50px">
                    Abnormal Trans &gt;= &nbsp;&nbsp;<asp:Label ID="LabelAbnormalTransNew" runat="server"></asp:Label><br />
                </asp:Panel>
            </td>
	    </tr>
	    <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD style="width: 35px"><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="True" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="Dynamic"></asp:CustomValidator></TD>
	    </TR>
    </TABLE>
</asp:Content>