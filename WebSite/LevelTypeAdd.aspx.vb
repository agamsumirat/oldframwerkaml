Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class LevelTypeAdd
    Inherits Parent

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "LevelTypeView.aspx"

        Me.Response.Redirect("LevelTypeView.aspx", False)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertLevelTypeBySU()
        Try
            Dim LevelTypeDescription As String
            If Me.TextLevelTypeDescription.Text.Length <= 255 Then
                LevelTypeDescription = Me.TextLevelTypeDescription.Text
            Else
                LevelTypeDescription = Me.TextLevelTypeDescription.Text.Substring(0, 255)
            End If

            Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                AccessLevelType.Insert(Me.TextLevelTypeName.Text, LevelTypeDescription, Now)
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)

            Dim LevelTypeDescription As String
            If Me.TextLevelTypeDescription.Text.Length <= 255 Then
                LevelTypeDescription = Me.TextLevelTypeDescription.Text
            Else
                LevelTypeDescription = Me.TextLevelTypeDescription.Text.Substring(0, 255)
            End If

            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeName", "Add", "", Me.TextLevelTypeName.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeDesc", "Add", "", LevelTypeDescription, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeCreatedDate", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As Sqltransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                Dim LevelTypeName As String = Trim(Me.TextLevelTypeName.Text)

                'Periksa apakah Nama LevelType tersebut sudah ada dalam tabel LevelType atau belum
                Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                    Dim counter As Int32 = AccessLevelType.CountMatchingLevelType(LevelTypeName)

                    'Counter = 0 berarti Nama Level Type tersebut belum pernah ada dalam tabel LevelType
                    If counter = 0 Then
                        'Periksa apakah Nama Level Type tersebut sudah ada dalam tabel LevelType_Approval atau belum
                        Using AccessLevelTypeApproval As New AMLDAL.AMLDataSetTableAdapters.LevelType_ApprovalTableAdapter
                            oSQLTrans = BeginTransaction(AccessLevelTypeApproval, IsolationLevel.ReadUncommitted)

                            counter = AccessLevelTypeApproval.CountLevelTypeApprovalByLevelTypeName(LevelTypeName)

                            'Counter = 0 berarti Level Type tersebut statusnya tidak dalam pending approval dan boleh ditambahkan dlm tabel LevelType_Approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertLevelTypeBySU()
                                    Me.InsertAuditTrail()
                                    Me.LblSuccess.Text = "Success to Insert Level Type."
                                    Me.LblSuccess.Visible = True
                                Else
                                    Dim LevelTypeDescription As String
                                    If Me.TextLevelTypeDescription.Text.Length <= 255 Then
                                        LevelTypeDescription = Me.TextLevelTypeDescription.Text
                                    Else
                                        LevelTypeDescription = Me.TextLevelTypeDescription.Text.Substring(0, 255)
                                    End If

                                    Dim LevelTypePendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope
                                    Using AccessLevelTypePendingApproval As New AMLDAL.AMLDataSetTableAdapters.LevelType_PendingApprovalTableAdapter
                                        SetTransaction(AccessLevelTypePendingApproval, oSQLTrans)
                                        'Tambahkan ke dalam tabel LevelType_PendingApproval dengan ModeID = 1 (Add) 
                                        LevelTypePendingApprovalID = AccessLevelTypePendingApproval.InsertLevelType_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Level Type Add", 1)

                                        'Tambahkan ke dalam tabel LevelType_Approval dengan ModeID = 1 (Add) 
                                        AccessLevelTypeApproval.Insert(LevelTypePendingApprovalID, 1, Nothing, LevelTypeName, LevelTypeDescription, Now, Nothing, Nothing, Nothing, Nothing)

                                        '        TransScope.Complete()
                                        oSQLTrans.Commit()
                                    End Using
                                    'End Using

                                    Dim MessagePendingID As Integer = 8401 'MessagePendingID 8401 = LevelType Add 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & LevelTypeName

                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & LevelTypeName, False)
                                End If
                            Else
                                Throw New Exception("Cannot add the following Level Type: '" & LevelTypeName & "' because it is currently waiting for approval.")
                            End If
                        End Using
                    Else
                        Throw New Exception("Cannot add the following Level Type: '" & LevelTypeName & "' because that Level Type Name already exists in the database.")
                    End If
                End Using
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '    Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class