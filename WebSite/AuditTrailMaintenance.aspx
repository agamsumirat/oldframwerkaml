<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="AuditTrailMaintenance.aspx.vb" Inherits="AuditTrailMaintenance" title="Audit Trail Maintenance" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
  	  <td id="tdcontent" height="99%" valign="top"><div id="divcontent" class="divcontent" ><table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td class="divcontentinside">
			<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr id="netral1">

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Audit Trail Maintenance&nbsp;
                </strong>
            </td>
        </tr>
        <tr>
            <td>                    
                <ajax:AjaxPanel ID="sukses" runat="server" Width="95%">
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Width="90%" Visible="False"></asp:Label></ajax:AjaxPanel>
            </td>
        </tr>
    </table>
     
                    <table bordercolor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
	    border="2" id="TableDetail" runat="server">
                        <tr class="formText" id="netral2">
                            <td bgcolor="#ffffff" colspan="4" height="24">
                                Total Record Audit Trail :
                                <asp:Label ID="LblTotalRecord" runat="server"></asp:Label>&nbsp;</td>
                        </tr>
	    <tr class="formText" id="tampilan1">
		    <td bgcolor="#ffffff" height="24" style="width: 1%">
                &nbsp;</td>
            <td bgcolor="#ffffff" colspan="3">
                <asp:RadioButton ID="RadioDate" runat="server" GroupName="option" Text="By Date" AutoPostBack="True" Width="72px" Checked="True" /></td>
	    </tr>
        <tr class="formText" id="OptionDate">
            <td bgcolor="#ffffff" height="24" style="width: 14px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextStartDate"
                        ErrorMessage="Please Fill Start Date.">*</asp:RequiredFieldValidator><br />
                <asp:RequiredFieldValidator
                        ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextEndDate" ErrorMessage="Please Fill End Date.">*</asp:RequiredFieldValidator></td>
            <td bgcolor="#ffffff" height="24" colspan="3">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Select Date : &nbsp; &nbsp; &nbsp; &nbsp;<asp:TextBox id="TextStartDate" runat="server" Width="120px" CssClass="textBox"></asp:TextBox>
                <input id="PopUp1" runat="server" name="popUpCalc1" style="border-right: #ffffff 0px solid;
                    border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                    border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif); width: 16px;" title="Click to show calendar"
                    type="button"  />
                to
			    <asp:TextBox id="TextEndDate" runat="server" Width="120px" CssClass="textBox"></asp:TextBox>
                <input id="popUp" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                    border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                    border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif); width: 16px;" title="Click to show calendar"
                    type="button"  />
            </td>
        </tr>
        <tr class="formText" id="tampilan">
            <td bgcolor="#ffffff" height="24" style="width: 14px; border-top-style: none; border-right-style: none; border-left-style: none; height: 30px; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" colspan="3">
                <asp:RadioButton ID="RadioCount" runat="server" GroupName="option" Text="By Count" Width="88px" AutoPostBack="True" /></td>
        </tr>
        <tr class="formText" id="OptionCount">
            <td bgcolor="#ffffff" style="width: 14px; height: 30px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="count must be numeric."
                    ValidationExpression="^([1-9]|([1-9][0-9]+))$" ControlToValidate="TextCount" Display="Dynamic">*</asp:RegularExpressionValidator><br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                    ErrorMessage="Please Fill Count." ControlToValidate="TextCount">*</asp:RequiredFieldValidator></td>
            <td bgcolor="#ffffff" style="height: 30px" colspan="3">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <asp:TextBox ID="TextCount" runat="server" CssClass="textbox"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr class="formText" id="Button1">
            <td height="24" style="width: 14px" bgColor="#dddddd"><IMG alt="" height="15" src="images/arrow.gif" width="15" />
            </td>
            <td align="left" bgcolor="#dddddd" colspan="3">
                <asp:imagebutton ID="Purge" runat="server" skinid="PurgeButton" CausesValidation="True"/>
                &nbsp;&nbsp;<asp:imagebutton ID="Export" runat="server" skinid="exportButton" CausesValidation="True" /><br />
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator></td>
        </tr>
        <tr id="aaaaaaaaaa">
        <td colspan="4">
            
   <asp:DataGrid id="GridAuditTrail" runat="server" AutoGenerateColumns="False"
						Font-Size="XX-Small" BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None"
						AllowPaging="True" width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE"
						ForeColor="Black" Visible="true">
                <FooterStyle BackColor="#CCCC99"></FooterStyle>
						<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
						<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
						<ItemStyle BackColor="#F7F7DE"></ItemStyle>
						<HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B"></HeaderStyle>
						
						<Columns>
						    <asp:BoundColumn DataField="AuditTrailDate" HeaderText="AuditTrail Date" DataFormatString="{0:dd-MMMM-yyyy hh:mm:ss}">
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" />
                            </asp:BoundColumn>
                            
                            <asp:BoundColumn DataField="AuditTrailCreatedBy" HeaderText="AuditTrail Created By" >
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" />
                            </asp:BoundColumn>
                             <asp:BoundColumn DataField="AuditTrailApprovedBy" HeaderText="AuditTrail Approved By" >
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" />
                            </asp:BoundColumn>
                             <asp:BoundColumn DataField="AuditTrailModuleName" HeaderText="AuditTrail Module Name" >
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" />
                            </asp:BoundColumn>
                            
                            <asp:BoundColumn DataField="AuditTrailFieldName" HeaderText="AuditTrail Field Name" >
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AuditTrailOperation" HeaderText="AuditTrail Operation" >
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" />
                            </asp:BoundColumn>
                            
                            <asp:BoundColumn DataField="AuditTrailOldValue" HeaderText="AuditTrail Old Value" >
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AuditTrailNewValue" HeaderText="AuditTrail New Value" >
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AuditTrailDescription" HeaderText="AuditTrail Description" >
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" HorizontalAlign="Left" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" />
                            </asp:BoundColumn>
						</Columns>
						
            </asp:DataGrid>
  
        
        </td>          
        </tr>           
        </table>
            </td>
		</tr>
	  </table>
	  
	  </div></td>
  </tr>
</table>
<script language="javascript" type="text/javascript">arrangeView();</script>
</asp:Content>