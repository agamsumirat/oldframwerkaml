<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CTRParameterSetting.aspx.vb" Inherits="CTRParameterSetting" title="CTR Parameter Setting" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>CTR Parameter&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000">* Required</span></td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2"  style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px"><br />
                </td>
			<td width="20%" bgColor="#ffffff" style="height: 24px">
                Transaction Amount<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                    ControlToValidate="TextTransactionAmount" Display="Dynamic" ErrorMessage="Transaction Ammount Is Required">*</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TextTransactionAmount"
                    Display="Dynamic" ErrorMessage="Transaction Ammount Must Numeric" Operator="DataTypeCheck"
                    Type="Double">*</asp:CompareValidator></td>
			<td bgColor="#ffffff" style="height: 24px; width: 12px;">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px"><asp:textbox id="TextTransactionAmount" runat="server" CssClass="textBox" MaxLength="50" Width="200px" ></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" rowspan="6">
              
             </td>
			<td bgColor="#ffffff" rowspan="6">
                CTR Email To
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextCTREmailTo"
                    Display="Dynamic" ErrorMessage="CTR Email To is required">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextCTREmailTo"
                    Display="Dynamic" ErrorMessage="CTR Email to not in correct format email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
            <td bgcolor="#ffffff" rowspan="6" style="width: 12px;">
                :
            </td>
            <td bgcolor="#ffffff" rowspan="6">
                <asp:TextBox ID="TextCTREmailTo" runat="server" CssClass="textBox" MaxLength="255" Width="200px"></asp:TextBox>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1">
            </td>
            <td bgcolor="#ffffff" rowspan="1">
                CTR Email CC<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                    ControlToValidate="TextEmailCC" Display="Dynamic" ErrorMessage="CTR Email CC is required">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextEmailCC"
                    Display="Dynamic" ErrorMessage="CTR Email CC not in correct format email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
            <td bgcolor="#ffffff" rowspan="1" style="width: 12px">
                :</td>
            <td bgcolor="#ffffff" rowspan="1">
                <asp:TextBox ID="TextEmailCC" runat="server" CssClass="textBox" MaxLength="255" Width="200px"></asp:TextBox>
                <strong><span style="color: #ff0000">*</span></strong></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1">
            </td>
            <td bgcolor="#ffffff" rowspan="1">
                CTR Email BCC<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                    ControlToValidate="TextCTREmailBCC" Display="Dynamic" ErrorMessage="CTR Email BCC is required">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextCTREmailBCC"
                    Display="Dynamic" ErrorMessage="CTR Email BCC not in correct format email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
            <td bgcolor="#ffffff" rowspan="1" style="width: 12px">
                :</td>
            <td bgcolor="#ffffff" rowspan="1">
                <asp:TextBox ID="TextCTREmailBCC" runat="server" CssClass="textBox" MaxLength="255"
                    Width="200px"></asp:TextBox>
                <strong><span style="color: #ff0000">*</span></strong></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1">
            </td>
            <td bgcolor="#ffffff" rowspan="1">
                CTR Email Subject<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                    ControlToValidate="TextEmailSubject" Display="Dynamic" ErrorMessage="CTR Email Subject is required">*</asp:RequiredFieldValidator>
                </td>
            <td bgcolor="#ffffff" rowspan="1" style="width: 12px">
                :</td>
            <td bgcolor="#ffffff" rowspan="1">
                <asp:TextBox ID="TextEmailSubject" runat="server" CssClass="textBox" MaxLength="255"
                    Width="200px"></asp:TextBox>
                <strong><span style="color: #ff0000">*</span></strong></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1">
            </td>
            <td bgcolor="#ffffff" rowspan="1" valign="top">
                CTR Email Body<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                    ControlToValidate="TextEmailBody" Display="Dynamic" ErrorMessage="CTR Email Body is required">*</asp:RequiredFieldValidator>
                </td>
            <td bgcolor="#ffffff" rowspan="1" style="width: 12px">
                :</td>
            <td bgcolor="#ffffff" rowspan="1">
                <asp:TextBox ID="TextEmailBody" runat="server" CssClass="textBox" MaxLength="255"
                    Rows="4" TextMode="MultiLine" Width="200px"></asp:TextBox>
                <strong><span style="color: #ff0000">*</span></strong></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1">
            </td>
            <td bgcolor="#ffffff" rowspan="1" valign="top">
                CTR PIC<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="TextPIC"
                    Display="Dynamic" ErrorMessage="CTR PIC is required">*</asp:RequiredFieldValidator></td>
            <td bgcolor="#ffffff" rowspan="1" style="width: 12px">
            </td>
            <td bgcolor="#ffffff" rowspan="1">
                <asp:TextBox ID="TextPIC" runat="server" CssClass="textBox" MaxLength="255" Width="200px"></asp:TextBox>
                <strong><span style="color: #ff0000">*</span></strong></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="width: 12px; height: 24px">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
            </td>
        </tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="AddButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>

</asp:Content>
