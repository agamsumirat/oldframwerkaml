Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class WorkingUnitManagementApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param pk user management
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamPkWorkingUnitManagement() As Int64
        Get
            Return Me.Request.Params("WorkingUnitID")
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserID() As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_PendingApprovalTableAdapter
                Return AccessPending.SelectWorkingUnit_PendingApprovalUserID(Me.ParamPkWorkingUnitManagement)
            End Using
        End Get
    End Property
#End Region

#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    02/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HideAll()
        Dim StrId As String = ""
        Select Case Me.ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.WorkingUnitAdd
                StrId = "UserAdd"
            Case Sahassa.AML.Commonly.TypeConfirm.WorkingUnitEdit
                StrId = "UserEdi"
            Case Sahassa.AML.Commonly.TypeConfirm.WorkingUnitDelete
                StrId = "UserDel"
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select
        For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
            ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID.Substring(0, 7) = StrId
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load WorkingUnit add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    02/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadWorkingUnitAdd()
        Me.LabelTitle.Text = "Activity: Add Working Unit"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetWorkingUnitApprovalData(Me.ParamPkWorkingUnitManagement)
                'Bila ObjTable.Rows.Count > 0 berarti WorkingUnit tsb masih ada dlm tabel WorkingUnit_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.WorkingUnit_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelWorkingUnitNameAdd.Text = rowData.WorkingUnitName
                    Me.TextWorkingUnitDescriptionAdd.Text = rowData.WorkingUnitDesc

                    Using objLevelTypeName As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                        Me.LabelLevelTypeNameAdd.Text = objLevelTypeName.GetLevelTypeNameByLevelTypeID(rowData.LevelTypeID)
                    End Using

                    Using objWorkingUnitParentName As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                        Me.LabelWorkingUnitParentNameAdd.Text = rowData.WorkingUnitParentID.ToString & " - " & objWorkingUnitParentName.GetWorkingUnitNameByWorkingUnitID(rowData.WorkingUnitParentID)
                    End Using
                Else 'Bila ObjTable.Rows.Count = 0 berarti WorkingUnit tsb sudah tidak lagi berada dlm tabel WorkingUnit_Approval
                    Throw New Exception("Cannot load data from the following Working Unit: " & Me.LabelWorkingUnitNameAdd.Text & " because that Working Unit is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load WorkingUnit edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    02/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadWorkingUnitEdit()
        Me.LabelTitle.Text = "Activity: Edit Working Unit"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetWorkingUnitApprovalData(Me.ParamPkWorkingUnitManagement)
                'Bila ObjTable.Rows.Count > 0 berarti WorkingUnit tsb masih ada dlm tabel WorkingUnit_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.WorkingUnit_ApprovalRow = ObjTable.Rows(0)
                    LabelNewWorkingUnitID.Text = rowData.WorkingUnitID
                    LabelNewWorkingUnitName.Text = rowData.WorkingUnitName
                    TextNewWorkingUnitDescription.Text = rowData.WorkingUnitDesc
                    Using objNewLevelTypeName As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                        Me.LabelNewLevelTypeName.Text = objNewLevelTypeName.GetLevelTypeNameByLevelTypeID(rowData.LevelTypeID)
                    End Using

                    Using objNewWorkingUnitParentName As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                        Me.LabelNewWorkingUnitParentName.Text = rowData.WorkingUnitParentID.ToString & " - " & objNewWorkingUnitParentName.GetWorkingUnitNameByWorkingUnitID(rowData.WorkingUnitParentID)
                    End Using

                    LabelOldWorkingUnitID.Text = rowData.WorkingUnitID_Old
                    LabelOldWorkingUnitName.Text = rowData.WorkingUnitName_Old
                    TextOldWorkingUnitDescription.Text = rowData.WorkingUnitDesc_Old
                    Using objOldLevelTypeName As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                        Me.LabelOldLevelTypeName.Text = objOldLevelTypeName.GetLevelTypeNameByLevelTypeID(rowData.LevelTypeID_Old)
                    End Using

                    Using objOldWorkingUnitParentName As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                        Me.LabelOldWorkingUnitParentName.Text = rowData.WorkingUnitParentID_Old.ToString & " - " & objOldWorkingUnitParentName.GetWorkingUnitNameByWorkingUnitID(rowData.WorkingUnitParentID_Old)
                    End Using
                Else 'Bila ObjTable.Rows.Count = 0 berarti WorkingUnit tsb sudah tidak lagi berada dlm tabel WorkingUnit_Approval
                    Throw New Exception("Cannot load data from the following Working Unit: " & Me.LabelOldWorkingUnitName.Text & " because that Working Unit is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load WorkingUnit delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    02/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadWorkingUnitDelete()
        Me.LabelTitle.Text = "Activity: Delete Working Unit"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetWorkingUnitApprovalData(Me.ParamPkWorkingUnitManagement)
                'Bila ObjTable.Rows.Count > 0 berarti WorkingUnit tsb masih ada dlm tabel WorkingUnit_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.WorkingUnit_ApprovalRow = ObjTable.Rows(0)
                    LabelWorkingUnitIDDelete.Text = rowData.WorkingUnitID
                    LabelWorkingUnitNameDelete.Text = rowData.WorkingUnitName
                    TextWorkingUnitDescriptionDelete.Text = rowData.WorkingUnitDesc
                    Using objLevelTypeName As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                        Me.LabelLevelTypeNameDelete.Text = objLevelTypeName.GetLevelTypeNameByLevelTypeID(rowData.LevelTypeID)
                    End Using

                    Using objWorkingUnitParentName As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                        Me.LabelWorkingUnitParentNameDelete.Text = rowData.WorkingUnitParentID.ToString & " - " & objWorkingUnitParentName.GetWorkingUnitNameByWorkingUnitID(rowData.WorkingUnitParentID)
                    End Using
                Else 'Bila ObjTable.Rows.Count = 0 berarti WorkingUnit tsb sudah tidak lagi berada dlm tabel WorkingUnit_Approval
                    Throw New Exception("Cannot load data from the following Working Unit: " & Me.LabelWorkingUnitNameDelete.Text & " because that Working Unit is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub
#End Region
#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' WorkingUnit add accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    02/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptWorkingUnitAdd()
        Dim oSQLTrans As SqlTransaction = Nothing

        'Using TranScope As New Transactions.TransactionScope
        Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessWorkingUnit, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetWorkingUnitApprovalData(Me.ParamPkWorkingUnitManagement)

                'Bila ObjTable.Rows.Count > 0 berarti WorkingUnit tsb masih ada dlm tabel WorkingUnit_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.WorkingUnit_ApprovalRow = ObjTable.Rows(0)

                    'Periksa apakah WorkingUnitName yg baru tsb sudah ada dlm tabel WorkingUnit atau belum. 
                    Dim counter As Int32 = AccessWorkingUnit.CountMatchingWorkingUnit(rowData.WorkingUnitName)

                    'Bila counter = 0 berarti WorkingUnitName tsb belum ada dlm tabel WorkingUnit, maka boleh ditambahkan
                    If counter = 0 Then
                        'tambahkan item tersebut dalam tabel WorkingUnit
                        AccessWorkingUnit.Insert(rowData.WorkingUnitName, rowData.LevelTypeID, rowData.WorkingUnitParentID, rowData.WorkingUnitDesc, rowData.WorkingUnitCreatedDate)

                        'catat aktifitas dalam tabel Audit Trail
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitName", "Add", "", rowData.WorkingUnitName, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "LevelTypeID", "Add", "", rowData.LevelTypeID, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitParentID", "Add", "", rowData.WorkingUnitParentID, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitDesc", "Add", "", rowData.WorkingUnitDesc, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitCreatedDate", "Add", "", rowData.WorkingUnitCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                            'hapus item tersebut dalam tabel WorkingUnit_Approval
                            AccessPending.DeleteWorkingUnitApproval(rowData.WorkingUnit_PendingApprovalID)

                            'hapus item tersebut dalam tabel WorkingUnit_PendingApproval
                            Using AccessPendingWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_PendingApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingWorkingUnit, oSQLTrans)
                                AccessPendingWorkingUnit.DeleteWorkingUnitPendingApproval(rowData.WorkingUnit_PendingApprovalID)
                            End Using

                            oSQLTrans.Commit()
                        End Using
                    Else 'Bila counter != 0 berarti WorkingUnitName tsb sudah ada dlm tabel WorkingUnit, maka AcceptWorkingUnitAdd gagal
                        Throw New Exception("Cannot add the following Working Unit: " & rowData.WorkingUnitName & " because that Working Unit Name already exists in the database.")
                    End If
                Else 'Bila ObjTable.Rows.Count = 0 berarti WorkingUnit tsb sudah tidak lagi berada dlm tabel WorkingUnit_Approval
                    Throw New Exception("Cannot add the following Working Unit: " & Me.LabelWorkingUnitNameAdd.Text & " because that Working Unit is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' WorkingUnit edit accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    02/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptWorkingUnitEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessWorkingUnit, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetWorkingUnitApprovalData(Me.ParamPkWorkingUnitManagement)

                'Bila ObjTable.Rows.Count > 0 berarti WorkingUnit tsb masih ada dlm tabel WorkingUnit_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.WorkingUnit_ApprovalRow = ObjTable.Rows(0)

                    'Periksa apakah WorkingUnitName yg baru tsb sudah ada dlm tabel WorkingUnit atau belum. 
                    Dim counter As Int32 = AccessWorkingUnit.CountMatchingWorkingUnit(rowData.WorkingUnitName)

                    'Bila tidak ada perubahan WorkingUnitName
                    If rowData.WorkingUnitName = rowData.WorkingUnitName_Old Then
                        GoTo Edit
                    Else 'Bila ada perubahan WorkingUnitName
                        'Counter = 0 berarti WorkingUnitName tersebut tidak ada dalam tabel WorkingUnit
                        If counter = 0 Then
Edit:
                            'update item tersebut dalam tabel WorkingUnit
                            AccessWorkingUnit.UpdateWorkingUnit(rowData.WorkingUnitID, rowData.WorkingUnitName, rowData.LevelTypeID, rowData.WorkingUnitParentID, rowData.WorkingUnitDesc)

                            'catat aktifitas dalam tabel Audit Trail
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitName", "Edit", rowData.WorkingUnitName_Old, rowData.WorkingUnitName, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "LevelTypeID", "Edit", rowData.LevelTypeID_Old, rowData.LevelTypeID, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitParentID", "Edit", rowData.WorkingUnitParentID_Old, rowData.WorkingUnitParentID, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitDesc", "Edit", rowData.WorkingUnitDesc_Old, rowData.WorkingUnitDesc, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitID", "Edit", rowData.WorkingUnitID_Old, rowData.WorkingUnitID, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitCreatedDate", "Edit", rowData.WorkingUnitCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.WorkingUnitCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                                'hapus item tersebut dalam tabel WorkingUnit_Approval
                                AccessPending.DeleteWorkingUnitApproval(rowData.WorkingUnit_PendingApprovalID)

                                'hapus item tersebut dalam tabel WorkingUnit_PendingApproval
                                Using AccessPendingWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_PendingApprovalTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingWorkingUnit, oSQLTrans)
                                    AccessPendingWorkingUnit.DeleteWorkingUnitPendingApproval(rowData.WorkingUnit_PendingApprovalID)
                                End Using
                                oSQLTrans.Commit()
                            End Using
                        Else 'Bila counter != 0 berarti WorkingUnitName tsb telah ada dlm tabel WorkingUnit, maka AcceptWorkingUnitEdit gagal
                            Throw New Exception("Cannot change to the following Working Unit Name: " & rowData.WorkingUnitName & " because that Working Unit Name already exists in the database.")
                        End If
                    End If
                Else 'Bila ObjTable.Rows.Count = 0 berarti WorkingUnit tsb sudah tidak lagi berada dlm tabel WorkingUnit_Approval
                    Throw New Exception("Cannot edit the following Working Unit: " & Me.LabelOldWorkingUnitName.Text & " because that Working Unit is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' WorkingUnit delete accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    02/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptWorkingUnitDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessWorkingUnit, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetWorkingUnitApprovalData(Me.ParamPkWorkingUnitManagement)

                'Bila ObjTable.Rows.Count > 0 berarti WorkingUnit tsb masih ada dlm tabel WorkingUnit_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.WorkingUnit_ApprovalRow = ObjTable.Rows(0)

                    'Periksa apakah WorkingUnit yg hendak didelete tsb ada dlm tabelnya atau tidak. 
                    Dim counter As Int32 = AccessWorkingUnit.CountMatchingWorkingUnit(rowData.WorkingUnitName)

                    'Bila counter = 0 berarti WorkingUnit tsb tidak ada dlm tabel WorkingUnit, maka AcceptWorkingUnitDelete gagal
                    If counter = 0 Then
                        Throw New Exception("Cannot delete the following Working Unit: " & rowData.WorkingUnitName & " because that Working Unit does not exist in the database anymore.")
                    Else 'Bila counter != 0, maka WorkingUnit tsb bisa didelete
                        'hapus item tersebut dari tabel WorkingUnit
                        AccessWorkingUnit.DeleteWorkingUnit(rowData.WorkingUnitID)

                        'catat aktifitas dalam tabel Audit Trail
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitName", "Delete", rowData.WorkingUnitName_Old, rowData.WorkingUnitName, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "LevelTypeID", "Delete", rowData.LevelTypeID_Old, rowData.LevelTypeID, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitParentID", "Delete", rowData.WorkingUnitParentID_Old, rowData.WorkingUnitParentID, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitDesc", "Delete", rowData.WorkingUnitDesc_Old, rowData.WorkingUnitDesc, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitID", "Delete", rowData.WorkingUnitID_Old, rowData.WorkingUnitID, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitCreatedDate", "Delete", rowData.WorkingUnitCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.WorkingUnitCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                            'hapus item tersebut dalam tabel WorkingUnit_Approval
                            AccessPending.DeleteWorkingUnitApproval(rowData.WorkingUnit_PendingApprovalID)

                            'hapus item tersebut dalam tabel WorkingUnit_PendingApproval
                            Using AccessPendingWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_PendingApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingWorkingUnit, oSQLTrans)
                                AccessPendingWorkingUnit.DeleteWorkingUnitPendingApproval(rowData.WorkingUnit_PendingApprovalID)
                            End Using

                            oSQLTrans.Commit()
                        End Using
                    End If
                Else 'Bila ObjTable.Rows.Count = 0 berarti WorkingUnit tsb sudah tidak lagi berada dlm tabel WorkingUnit_Approval
                    Throw New Exception("Cannot delete the following Working Unit: " & Me.LabelWorkingUnitNameDelete.Text & " because that Working Unit is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub

#End Region
#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject WorkingUnit add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    02/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectWorkingUnitAdd()
        Dim oSQLTrans As SqlTransaction = Nothing

        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
            Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessWorkingUnit, oSQLTrans)
                Using AccessWorkingUnitPending As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_PendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessWorkingUnitPending, oSQLTrans)
                    Using TableWorkingUnit As Data.DataTable = AccessWorkingUnit.GetWorkingUnitApprovalData(Me.ParamPkWorkingUnitManagement)

                        'Bila TableWorkingUnit.Rows.Count > 0 berarti WorkingUnit tsb masih ada dlm tabel WorkingUnit_Approval
                        If TableWorkingUnit.Rows.Count > 0 Then
                            Dim ObjRow As AMLDAL.AMLDataSet.WorkingUnit_ApprovalRow = TableWorkingUnit.Rows(0)

                            'catat aktifitas dalam tabel Audit Trail
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitName", "Add", "", ObjRow.WorkingUnitName, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "LevelTypeID", "Add", "", ObjRow.LevelTypeID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitParentID", "Add", "", ObjRow.WorkingUnitParentID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitDesc", "Add", "", ObjRow.WorkingUnitDesc, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitCreatedDate", "Add", "", ObjRow.WorkingUnitCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                            'hapus item tersebut dalam tabel WorkingUnit_Approval
                            AccessWorkingUnit.DeleteWorkingUnitApproval(Me.ParamPkWorkingUnitManagement)

                            'hapus item tersebut dalam tabel WorkingUnit_PendingApproval
                            AccessWorkingUnitPending.DeleteWorkingUnitPendingApproval(Me.ParamPkWorkingUnitManagement)
                            oSQLTrans.Commit()
                        Else 'Bila TableWorkingUnit.Rows.Count = 0 berarti WorkingUnit tsb sudah tidak lagi berada dlm tabel WorkingUnit_Approval
                            Throw New Exception("Operation failed. The following Working Unit: " & Me.LabelWorkingUnitNameAdd.Text & " is no longer in the approval table.")
                        End If
                    End Using
                End Using
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject WorkingUnit edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    02/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectWorkingUnitEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
            Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessWorkingUnit, oSQLTrans)
                Using AccessWorkingUnitPending As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_PendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessWorkingUnitPending, oSQLTrans)
                    Using TableWorkingUnit As Data.DataTable = AccessWorkingUnit.GetWorkingUnitApprovalData(Me.ParamPkWorkingUnitManagement)

                        'Bila TableWorkingUnit.Rows.Count > 0 berarti WorkingUnit tsb masih ada dlm tabel WorkingUnit_Approval
                        If TableWorkingUnit.Rows.Count > 0 Then
                            Dim ObjRow As AMLDAL.AMLDataSet.WorkingUnit_ApprovalRow = TableWorkingUnit.Rows(0)

                            'catat aktifitas dalam tabel Audit Trail
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitName", "Edit", ObjRow.WorkingUnitName_Old, ObjRow.WorkingUnitName, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "LevelTypeID", "Edit", ObjRow.LevelTypeID_Old, ObjRow.LevelTypeID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitParentID", "Edit", ObjRow.WorkingUnitParentID_Old, ObjRow.WorkingUnitParentID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitDesc", "Edit", ObjRow.WorkingUnitDesc_Old, ObjRow.WorkingUnitDesc, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitID", "Edit", ObjRow.WorkingUnitID_Old, ObjRow.WorkingUnitID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitCreatedDate", "Edit", ObjRow.WorkingUnitCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.WorkingUnitCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                            'hapus item tersebut dalam tabel WorkingUnit_Approval
                            AccessWorkingUnit.DeleteWorkingUnitApproval(Me.ParamPkWorkingUnitManagement)

                            'hapus item tersebut dalam tabel WorkingUnit_PendingApproval
                            AccessWorkingUnitPending.DeleteWorkingUnitPendingApproval(Me.ParamPkWorkingUnitManagement)
                            oSQLTrans.Commit()
                        Else 'Bila TableWorkingUnit.Rows.Count = 0 berarti WorkingUnit tsb sudah tidak lagi berada dlm tabel WorkingUnit_Approval
                            Throw New Exception("Operation failed. The following Working Unit: " & Me.LabelOldWorkingUnitName.Text & " is no longer in the approval table.")
                        End If
                    End Using
                End Using
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject WorkingUnit delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    02/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectWorkingUnitDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
            Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessWorkingUnit, oSQLTrans)
                Using AccessWorkingUnitPending As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_PendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessWorkingUnitPending, oSQLTrans)
                    Using TableWorkingUnit As Data.DataTable = AccessWorkingUnit.GetWorkingUnitApprovalData(Me.ParamPkWorkingUnitManagement)

                        'Bila TableWorkingUnit.Rows.Count > 0 berarti WorkingUnit tsb masih ada dlm tabel WorkingUnit_Approval
                        If TableWorkingUnit.Rows.Count > 0 Then
                            Dim ObjRow As AMLDAL.AMLDataSet.WorkingUnit_ApprovalRow = TableWorkingUnit.Rows(0)

                            'catat aktifitas dalam tabel Audit Trail
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitName", "Delete", ObjRow.WorkingUnitName_Old, ObjRow.WorkingUnitName, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "LevelTypeID", "Delete", ObjRow.LevelTypeID_Old, ObjRow.LevelTypeID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitParentID", "Delete", ObjRow.WorkingUnitParentID_Old, ObjRow.WorkingUnitParentID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitDesc", "Delete", ObjRow.WorkingUnitDesc_Old, ObjRow.WorkingUnitDesc, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitID", "Delete", ObjRow.WorkingUnitID_Old, ObjRow.WorkingUnitID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitCreatedDate", "Delete", ObjRow.WorkingUnitCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.WorkingUnitCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                            'hapus item tersebut dalam tabel WorkingUnit_Approval
                            AccessWorkingUnit.DeleteWorkingUnitApproval(Me.ParamPkWorkingUnitManagement)

                            'hapus item tersebut dalam tabel WorkingUnit_PendingApproval
                            AccessWorkingUnitPending.DeleteWorkingUnitPendingApproval(Me.ParamPkWorkingUnitManagement)
                            oSQLTrans.Commit()
                        Else 'Bila TableWorkingUnit.Rows.Count = 0 berarti WorkingUnit tsb sudah tidak lagi berada dlm tabel WorkingUnit_Approval
                            Throw New Exception("Operation failed. The following Working Unit: " & Me.LabelWorkingUnitNameDelete.Text & " is no longer in the approval table.")
                        End If
                    End Using
                End Using
            End Using
        End Using
        'End Using
    End Sub
#End Region

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HideAll()
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.WorkingUnitAdd
                        Me.LoadWorkingUnitAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.WorkingUnitEdit
                        Me.LoadWorkingUnitEdit()
                    Case Sahassa.AML.Commonly.TypeConfirm.WorkingUnitDelete
                        Me.LoadWorkingUnitDelete()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '    Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.WorkingUnitAdd
                    Me.AcceptWorkingUnitAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.WorkingUnitEdit
                    Me.AcceptWorkingUnitEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.WorkingUnitDelete
                    Me.AcceptWorkingUnitDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "WorkingUnitManagementApproval.aspx"

            Me.Response.Redirect("WorkingUnitManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.WorkingUnitAdd
                    Me.RejectWorkingUnitAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.WorkingUnitEdit
                    Me.RejectWorkingUnitEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.WorkingUnitDelete
                    Me.RejectWorkingUnitDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "WorkingUnitManagementApproval.aspx"

            Me.Response.Redirect("WorkingUnitManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "WorkingUnitManagementApproval.aspx"

        Me.Response.Redirect("WorkingUnitManagementApproval.aspx", False)
    End Sub
End Class