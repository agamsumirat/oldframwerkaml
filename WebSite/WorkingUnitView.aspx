<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="WorkingUnitView.aspx.vb" Inherits="WorkingUnitView" title="Working Unit View" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	 <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Working Unit - View&nbsp;
                    <hr />
                </strong>
            </td>
        </tr>
    </table>    
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" id="TABLE1">
		<tr>
			<td bgColor="#ffffff">
                <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                    width="100%">
                    <tr>
                        <td align="left" bgcolor="#eeeeee">
                            <img height="15" src="images/arrow.gif" width="15" /></td>
                        <td bgcolor="#ffffff" valign="top" width="98%">
                            <table border="0" cellpadding="0" cellspacing="4" width="100%">                              
                                <tr>
                                    <td class="Regtext" nowrap="nowrap">
                                        Search By :
                                    </td>
                                    <td nowrap="nowrap" valign="middle">
                                        <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                            <asp:DropDownList ID="ComboSearch" runat="server" CssClass="searcheditcbo" TabIndex="1"
                                                Width="141px">
                                            </asp:DropDownList>&nbsp;
                                            <asp:TextBox ID="TextSearch" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></ajax:AjaxPanel></td>
                                    <td valign="middle" width="99%">
                                        <ajax:AjaxPanel ID="AjaxPanel2" runat="server">
                                            <asp:ImageButton ID="ImageButtonSearch" runat="server" SkinID="SearchButton"
                                                TabIndex="3" /></ajax:AjaxPanel>
                                    </td>
                                </tr>
                            </table>
                            <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel></td>
                    </tr>
                </table>               
                <ajax:ajaxpanel id="AjaxPanel4" runat="server">
					<asp:datagrid id="GridMSUserView" runat="server" AutoGenerateColumns="False"
						Font-Size="XX-Small" BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None"
						AllowPaging="True" width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE"
						ForeColor="Black">
						<FooterStyle BackColor="#CCCC99"></FooterStyle>
						<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
						<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
						<ItemStyle BackColor="#F7F7DE"></ItemStyle>
						<HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B"></HeaderStyle>
						<Columns>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="WorkingUnitID" SortExpression="WorkingUnitID desc" HeaderText="Working Unit ID">
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" Wrap="False" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="WorkingUnitName" HeaderText="Working Unit Name" SortExpression="WorkingUnitName desc">
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" HorizontalAlign="Center" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" HorizontalAlign="Center" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="LevelTypeName" HeaderText="Level Type"
                                SortExpression="LevelTypeName desc">
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" HorizontalAlign="Center" />
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="WorkingUnitParentName" HeaderText="Working Unit Parent"
                                SortExpression="WorkingUnitParentName desc">
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" HorizontalAlign="Center" />
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="WorkingUnitDesc" HeaderText="Description" SortExpression="WorkingUnitDesc desc">
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" />
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" HorizontalAlign="Center" Wrap="False" />
                            </asp:BoundColumn>
                            <asp:EditCommandColumn EditText="Edit"></asp:EditCommandColumn>
                            <asp:ButtonColumn CommandName="Delete" Text="Delete"></asp:ButtonColumn>
						</Columns>
						<PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages"></PagerStyle>
					</asp:datagrid></ajax:ajaxpanel>
				</td>
		</tr>
		<tr>
		   <td style="background-color:#ffffff"><table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td nowrap><ajax:ajaxpanel id="AjaxPanel5" runat="server">&nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
			  &nbsp; </ajax:ajaxpanel> </td>
			 <td width="99%">&nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" runat="server">Export 
		        to Excel</asp:LinkButton></td>
			<td align="right" nowrap><asp:LinkButton ID="LinkButtonAddNew" runat="server">Add 
	        New</asp:LinkButton>&nbsp;&nbsp;</td>
		</tr>
      </table></td>
		</tr>
		<tr>
			<td bgColor="#ffffff">
				<TABLE id="Table3" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
					<TR class="regtext" align="center" bgColor="#dddddd">
						<TD vAlign="top" align="left" width="50%" bgColor="#ffffff">Page&nbsp;<ajax:ajaxpanel id="AjaxPanel6" runat="server"><asp:label id="PageCurrentPage" runat="server" CssClass="regtext">0</asp:label>&nbsp;of&nbsp;
							<asp:label id="PageTotalPages" runat="server" CssClass="regtext">0</asp:label></ajax:ajaxpanel></TD>
						<TD vAlign="top" align="right" width="50%" bgColor="#ffffff">Total Records&nbsp;
							<ajax:ajaxpanel id="AjaxPanel7" runat="server"><asp:label id="PageTotalRows" runat="server">0</asp:label></ajax:ajaxpanel></TD>
					</TR>
				</TABLE>
				<TABLE id="Table4" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
					<TR bgColor="#ffffff">
						<TD class="regtext" vAlign="middle" align="left" colSpan="11" height="7">
							<HR color="#f40101" noShade SIZE="1">
						</TD>
					</TR>
					<TR>
						<TD class="regtext" vAlign="middle" align="left" width="63" bgColor="#ffffff">Go to 
							page</TD>
						<TD class="regtext" vAlign="middle" align="left" width="5" bgColor="#ffffff"><FONT face="Verdana, Arial, Helvetica, sans-serif" size="1">
									<ajax:ajaxpanel id="AjaxPanel8" runat="server"><asp:textbox id="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:textbox></ajax:ajaxpanel>
								</FONT></TD>
						<TD class="regtext" vAlign="middle" align="left" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel9" runat="server">
								<asp:imagebutton id="ImageButtonGo" runat="server" SkinID="GoButton"></asp:imagebutton></ajax:ajaxpanel>
							</TD>
						<TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/first.gif" width="6">
						</TD>
						<TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel10" runat="server">
								<asp:linkbutton id="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First" OnCommand="PageNavigate">First</asp:linkbutton></ajax:ajaxpanel>
							</TD>
						<TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><IMG height="5" src="images/prev.gif" width="6"></TD>
						<TD class="regtext" vAlign="middle" align="right" width="14" bgColor="#ffffff">
						<ajax:ajaxpanel id="AjaxPanel11" runat="server">
								<asp:linkbutton id="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev" OnCommand="PageNavigate">Previous</asp:linkbutton></ajax:ajaxpanel>
							</TD>
						<TD class="regtext" vAlign="middle" align="right" width="60" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel12" runat="server"><A class="pageNav" href="#">
									<asp:linkbutton id="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next" OnCommand="PageNavigate">Next</asp:linkbutton></A></ajax:ajaxpanel></TD>
						<TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/next.gif" width="6"></TD>
						<TD class="regtext" vAlign="middle" align="left" width="25" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel13" runat="server">
								<asp:linkbutton id="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last" OnCommand="PageNavigate">Last</asp:linkbutton></ajax:ajaxpanel>
							</TD>
						<TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/last.gif" width="6"></TD>
					</TR>
				</TABLE>
			</td>
		</tr>
	</table>
</asp:Content>