﻿Imports AMLBLL
Imports SahassaNettier.Entities

Partial Class WorkFlowHistoryAccessEdit
    Inherits Parent


    Public ReadOnly Property objPkEditID() As Integer
        Get
            Dim strtemp As String = Request.Params("PKEditID")
            Dim intresult As Integer
            If Integer.TryParse(strtemp, intresult) Then
                Return intresult
            Else
                Throw New SahassaException("PKEdit Must Numeric")
            End If
        End Get
    End Property

    Public Property ObjWorkflowHistoryAccess() As WorkflowHistoryAccess
        Get
            If Session("WorkFlowHistoryAccessAdd.ObjWorkflowHistoryAccess ") Is Nothing Then
                
                Session("WorkFlowHistoryAccessAdd.ObjWorkflowHistoryAccess ") = WorkFlowHistoryAccessBLL.GetWorkflowHistoryAccessByPk(Me.objPkEditID)


                Return CType(Session("WorkFlowHistoryAccessAdd.ObjWorkflowHistoryAccess "), WorkflowHistoryAccess)
            Else
                Return CType(Session("WorkFlowHistoryAccessAdd.ObjWorkflowHistoryAccess "), WorkflowHistoryAccess)
            End If
        End Get
        Set(value As WorkflowHistoryAccess)
            Session("WorkFlowHistoryAccessAdd.ObjWorkflowHistoryAccess ") = value
        End Set
    End Property




    Public Property ObjTWorkflowHistoryAccessDetail() As TList(Of WorkflowHistoryAccessDetail)
        Get
            If Session("WorkFlowHistoryAccessAdd.ObjTWorkflowHistoryAccessDetail") Is Nothing Then
                Session("WorkFlowHistoryAccessAdd.ObjTWorkflowHistoryAccessDetail") = WorkFlowHistoryAccessBLL.GetTlistWorkflowHistoryAccessDetail(WorkflowHistoryAccessDetailColumn.FK_WorkflowHistoryAccess_ID.ToString & "=" & Me.objPkEditID, "", 0, Integer.MaxValue, 0)
                Return CType(Session("WorkFlowHistoryAccessAdd.ObjTWorkflowHistoryAccessDetail"), TList(Of WorkflowHistoryAccessDetail))
            Else
                Return CType(Session("WorkFlowHistoryAccessAdd.ObjTWorkflowHistoryAccessDetail"), TList(Of WorkflowHistoryAccessDetail))
            End If
        End Get
        Set(value As TList(Of WorkflowHistoryAccessDetail))
            Session("WorkFlowHistoryAccessAdd.ObjTWorkflowHistoryAccessDetail") = value
        End Set
    End Property

  

    Sub LoadWorkFlowDetail()
        CboAccessView.Items.Clear()
        CboAccessView.AppendDataBoundItems = True
        CboAccessView.Items.Add(New ListItem("Please Select Access View Other Than click Add Detail", "0"))
        CboAccessView.DataSource = WorkFlowHistoryAccessBLL.GetWorkflowViewOther
        CboAccessView.DataTextField = "WorkflowStep"
        CboAccessView.DataValueField = "WorkflowStep"
        CboAccessView.DataBind()
    End Sub

    Sub ClearSession()
        ObjWorkflowHistoryAccess = Nothing
        ObjTWorkflowHistoryAccessDetail = Nothing
    End Sub
    Sub BindAccessViewOther()
        GrdWorkFlowAccess.DataSource = ObjTWorkflowHistoryAccessDetail
        GrdWorkFlowAccess.DataBind()
    End Sub

    Sub LoadData()
        If Not ObjWorkflowHistoryAccess Is Nothing Then
            LblWorkflowStep.Text = ObjWorkflowHistoryAccess.WorkflowStep
        End If
        BindAccessViewOther()

    End Sub
    Function IsDataValid() As Boolean
        Dim strErrorMessage As New StringBuilder
        
        If CboAccessView.SelectedValue = "0" Then
            strErrorMessage.Append("Please Select Access View Other.</br>")
        End If

        If Not ObjTWorkflowHistoryAccessDetail.Find(WorkflowHistoryAccessDetailColumn.AccessViewOtherWorkflow, CboAccessView.SelectedValue) Is Nothing Then
            strErrorMessage.Append("Access View Other " & CboAccessView.SelectedValue & " already exist.</br>")
        End If

        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New SahassaException(strErrorMessage.ToString)
        Else
            Return True
        End If

    End Function

    Protected Sub Page_Load1(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using


                LoadWorkFlowDetail()
                LoadData()
            End If



        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try


    End Sub
    Protected Sub GrdWorkFlowAccess_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GrdWorkFlowAccess.RowDeleting
        Try
            Dim pk As Integer = GrdWorkFlowAccess.DataKeys(e.RowIndex).Value
            Using objdel As WorkflowHistoryAccessDetail = ObjTWorkflowHistoryAccessDetail.Find(WorkflowHistoryAccessDetailColumn.PK_WorkflowHistoryAccessDetail_ID, CInt(pk))
                If Not objdel Is Nothing Then
                    ObjTWorkflowHistoryAccessDetail.Remove(objdel)
                    BindAccessViewOther()
                End If
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub ImgAddAccessView_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgAddAccessView.Click
        Try
            If IsDataValid() Then

                Dim generator As New Random
                Dim randomValue As Integer
                randomValue = generator.Next(1, Integer.MaxValue)
                While Not ObjTWorkflowHistoryAccessDetail.Find(WorkflowHistoryAccessDetailColumn.FK_WorkflowHistoryAccess_ID, CInt(randomValue)) Is Nothing
                    randomValue = generator.Next(1, Integer.MaxValue)
                End While

                Using objnew As WorkflowHistoryAccessDetail = ObjTWorkflowHistoryAccessDetail.AddNew()
                    With objnew
                        .PK_WorkflowHistoryAccessDetail_ID = randomValue
                        .FK_WorkflowHistoryAccess_ID = ObjWorkflowHistoryAccess.PK_WorkflowHistoryAccess_ID
                        .AccessViewOtherWorkflow = CboAccessView.SelectedValue
                    End With
                End Using
                BindAccessViewOther()
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Function IsDataValidSave() As Boolean
        Dim strErrorMessage As New StringBuilder

        
        'If CboAccessView.SelectedValue = "0" Then
        '    strErrorMessage.Append("Please Select Access View Other.</br>")
        'End If


        If ObjTWorkflowHistoryAccessDetail.Count = 0 Then
            strErrorMessage.Append("Access View Other required minimal 1.</br>")
        End If
        If WorkFlowHistoryAccessBLL.IsAlreadyExistInApproval(ObjWorkflowHistoryAccess.WorkflowStep) Then
            strErrorMessage.Append("Workflow Step " & ObjWorkflowHistoryAccess.WorkflowStep & " already exist in Approval.</br>")
        End If
        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New SahassaException(strErrorMessage.ToString)
        Else
            Return True
        End If
    End Function
    Protected Sub ImageSave_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If IsDataValidSave Then

                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    WorkFlowHistoryAccessBLL.SaveEditWorkflowHistorySU(ObjWorkflowHistoryAccess, ObjTWorkflowHistoryAccessDetail)
                    LblConfirmation.Text = "Data Workflow History Access Saved "
                Else
                    WorkFlowHistoryAccessBLL.SaveEditApprovalWorkflowHistory(ObjWorkflowHistoryAccess, ObjTWorkflowHistoryAccessDetail)
                    LblConfirmation.Text = "Data Workflow History Access Saved into Pending Approval. "
                End If
                MultiView1.ActiveViewIndex = 1


            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub ImgBack_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Try
            Response.Redirect("WorkFlowHistoryAccessView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageCancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Response.Redirect("WorkFlowHistoryAccessView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
