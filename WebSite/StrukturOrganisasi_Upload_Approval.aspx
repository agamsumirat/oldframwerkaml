﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="StrukturOrganisasi_Upload_Approval.aspx.vb" Inherits="StrukturOrganisasi_Upload_Approval" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<%--<script src="script/popcalendar.js"></script>--%>			
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF" width="100%">
					<div id="divcontent" class="divcontent">
						<table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>								
								<td class="divcontentinside" bgcolor="#FFFFFF">
                                    <ajax:AjaxPanel ID="a" runat="server" meta:resourcekey="aResource1">
                                        &nbsp;<asp:ValidationSummary ID="ValidationSummaryhandle" runat="server" 
                                                                CssClass="validationok" ForeColor="Black" 
                                                                meta:resourcekey="ValidationSummaryhandleResource1" ValidationGroup="handle" />
                                    </ajax:AjaxPanel>
                                    <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%" meta:resourcekey="AjaxPanel5Resource1">
                                                    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
											                height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
											                border-bottom-style: none" bgcolor="#dddddd" width="100%">
											            <tr bgcolor="#ffffff">
												<td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
													border-right-style: none; border-left-style: none; border-bottom-style: none; width: 100%;">
													<strong>
														<img src="Images/dot_title.gif" width="17" height="17">
														<asp:Label ID="Label7" runat="server" Text="STRUKTUR ORGANISASI UPLOAD APPROVAL" 
                                                        Font-Size="Small"></asp:Label>
														<hr />
													</strong>&nbsp;&nbsp;&nbsp;&nbsp;<ajax:AjaxPanel ID="AjxMessage" runat="server" meta:resourcekey="AjxMessageResource1">
														<asp:Label ID="LblMessage" background="images/validationbground.gif" runat="server"
															CssClass="validationok" Width="100%" meta:resourcekey="LblMessageResource1"></asp:Label>
													</ajax:AjaxPanel>
												</td>
											            </tr>
										            </table>
										            <asp:Panel ID="Panel1" runat="server" DefaultButton="ImgBtnSearch" Width="100%" meta:resourcekey="Panel1Resource1">
										<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
												border="2">
												<tr>
													<td colspan="2" background="images/search-bar-background.gif" height="18" valign="middle"
														width="100%" align="right" style="text-align: left">
														<table cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td style="height: 16px">
																	<strong>Search box</strong> &nbsp;
																</td>
																<td style="height: 16px">
																	<a href="#" onclick="javascript:UpdateSearchBox()" title="click to minimize or maximize">
																		<img id="searchimage" src="images/search-bar-minimize.gif" border="0" style="width: 19px;
																			height: 14px"></a>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr id="searchbox">
													<td colspan="2" valign="top" width="100%" bgcolor="#ffffff">
														<table cellpadding="0" width="100%" border="0">
															<tr>
																<td valign="middle" nowrap>
																	<ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%" 
                                                                        meta:resourcekey="AjaxPanel1Resource1">
																		<table style="height: 100%" width="100%">
																			<tr>
																				<td colspan="3" style="height: 7px">
																				</td>
																			</tr>
																			<tr>
																				<td nowrap style="height: 26px; width: 132px;">
																					<asp:Label ID="LabelMsUser_StaffName" runat="server" Text="Name"></asp:Label>
																				</td>
																				<td style="width: 19px;">
																					:
																				</td>
																				<td style="height: 26px; width: 765px;">
																					&nbsp;&nbsp;<asp:TextBox ID="txtMsUser_StaffName" runat="server" CssClass="searcheditbox" 
                                                                                        TabIndex="2" Width="296px"></asp:TextBox>
																				</td>
																			</tr>
																			<tr>
																				<td nowrap style="height: 26px; width: 132px;">
																					<asp:Label ID="LabelRequestedDate" runat="server" Text="Requested Date"></asp:Label>
																				</td>
																				<td style="width: 19px;">
																					:
																				</td>
																				<td style="height: 26px; width: 765px;">
                                                                                    &nbsp;
																					<asp:TextBox ID="txtRequestedDate" runat="server" CssClass="textBox" 
                                                                                        MaxLength="1000" TabIndex="2" ToolTip="RequestedDate" Width="120px">
                                                                                        </asp:TextBox>
                                                                                    <input id="PopRequestedDate" runat="server" style="border-right: #ffffff 0px solid;
																						border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
																						border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
																						height: 17px" title="Click to show calendar" type="button" />
                                                                                    s/d&nbsp;
                                                                                    <asp:TextBox ID="txtRequestedDate2" runat="server" CssClass="textBox" 
                                                                                        MaxLength="1000" TabIndex="2" ToolTip="RequestedDate" Width="120px">
                                                                                        </asp:TextBox>
                                                                                    <input id="PopRequestedDate2" runat="server" style="border-right: #ffffff 0px solid;
																						border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
																						border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
																						height: 17px" title="Click to show calendar" type="button" />
																				</td>
																			</tr>
																			<tr>
																				<td colspan="3">
																				</td>
																			</tr>
																			<tr>
																				<td colspan="3" width="100%">
																					<asp:ImageButton ID="ImgBtnSearch" TabIndex="3" runat="server" SkinID="SearchButton"
																						ImageUrl="~/Images/button/search.gif" meta:resourcekey="ImgBtnSearchResource1">
																					</asp:ImageButton>
																					<asp:ImageButton ID="ImgBtnClearSearch" runat="server" ImageUrl="~/Images/button/clearsearch.gif"
																						meta:resourcekey="ImgBtnClearSearchResource1" />
																				</td>
																			</tr>
																		</table>
																		
																	</ajax:AjaxPanel>
																</td>
															</tr>
														</table>
														<ajax:AjaxPanel ID="AjaxPanel3" runat="server" 
                                                            meta:resourcekey="AjaxPanel3Resource1" Width="100%">
															<asp:CustomValidator ID="CvalPageErr" runat="server" Display="None" meta:resourcekey="CvalPageErrResource1"></asp:CustomValidator></ajax:AjaxPanel>
														<ajax:AjaxPanel ID="Ajaxpanel2" runat="server" 
                                                            meta:resourcekey="Ajaxpanel2Resource1" Width="100%">
															<asp:CustomValidator ID="CvalHandleErr" runat="server" ValidationGroup="handle" Display="None"
																meta:resourcekey="CvalHandleErrResource1"></asp:CustomValidator></ajax:AjaxPanel>
													</td>
												</tr>
											</table>
										</asp:Panel>
										            <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
											               border="2">										
                                                    <tr>                                                    
                                                        <td bgcolor="#ffffff">
                                                            <ajax:AjaxPanel ID="AjaxPanel4" runat="server" 
                                                                meta:resourcekey="AjaxPanel4Resource1" Width="100%">                                                                
                                                                <asp:DataGrid ID="GV" runat="server" AllowCustomPaging="True" AllowSorting="True" AutoGenerateColumns="False" 
                                                                    CellPadding="4" Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                    Font-Size="Medium" Font-Strikeout="False" Font-Underline="False" 
                                                                    ForeColor="Black" GridLines="Vertical" HorizontalAlign="Center" 
                                                                    meta:resourcekey="GridMsUserViewResource1" Width="100%" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px">
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" 
                                                                        Visible="False" Mode="NumericPages" />
                                                                    <AlternatingItemStyle BackColor="White" />
                                                                    <ItemStyle BackColor="#F7F7DE" HorizontalAlign="Left" VerticalAlign="Top" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" 
                                                                        HorizontalAlign="Left" VerticalAlign="Middle" Width="200px" Wrap="False" ForeColor="White" />
                                                                    <Columns>
                                                                        <asp:TemplateColumn>
																            <ItemTemplate>
																	            <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" meta:resourcekey="CheckBoxExporttoExcelResource1">
																	            </asp:CheckBox>
																            </ItemTemplate>
																            <HeaderStyle Width="10px" />
															            </asp:TemplateColumn>
                                                                        <asp:BoundColumn HeaderText="No">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                Font-Strikeout="False" Font-Underline="False" ForeColor="Black" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="PK_StrukturOrganisasi_Approval_Id" Visible="False">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="UserName" HeaderText="Requested By" 
                                                                            SortExpression="UserName asc"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="RequestedDate" DataFormatString="{0:dd-MM-yyyy}" 
                                                                            HeaderText="Import Date" SortExpression="RequestedDate asc">
                                                                        </asp:BoundColumn>
                                                                        <asp:HyperLinkColumn DataNavigateUrlField="PK_StrukturOrganisasi_Approval_Id" 
                                                                            DataNavigateUrlFormatString="StrukturOrganisasi_Upload_ApprovalDetail.aspx?PK_StrukturOrganisasi_Approval_Id={0}" 
                                                                            Text="Detail"></asp:HyperLinkColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                                <asp:Label ID="LabelNoRecordFound" runat="server" CssClass="text" 
                                                                    meta:resourcekey="LabelNoRecordFoundResource1" 
                                                                    Text="No record match with your criteria" Visible="False">
                                                                </asp:Label>
                                                                
                                                            </ajax:AjaxPanel>
                                                        </td>                                                               
                                                    </tr>
                                                
                                                        <tr>
                                                            <td style="background-color: #ffffff" width="100%">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td nowrap style="height: 49px; width: 66px;">
                                                                            <ajax:AjaxPanel ID="AjaxPanel15" runat="server" meta:resourcekey="AjaxPanel6Resource1" Width="72px">
																            <asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True"
																	            meta:resourcekey="CheckBoxSelectAllResource1" />
																       
															                </ajax:AjaxPanel>
                                                                        </td>
                                                                        <td style="width: 92%; height: 49px;">
                                                                           
                                                                            <ajax:AjaxPanel ID="AjaxPanel6" runat="server" 
                                                                                meta:resourcekey="AjaxPanel6Resource1" Width="99%">
                                                                                &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" ajaxcall="none" meta:resourcekey="lnkExportAllDataResource1"
                                                                                    OnClientClick="aspnetForm.encoding = 'multipart/form-data';" Text="Export to Excel"></asp:LinkButton>
                                                                                &nbsp;&nbsp; &nbsp;&nbsp;
                                                                                <asp:LinkButton ID="lnkExportAllData" runat="server" ajaxcall="none" 
                                                                                    meta:resourcekey="lnkExportAllDataResource1" 
                                                                                    OnClientClick="aspnetForm.encoding = 'multipart/form-data';" 
                                                                                    Text="Export All to Excel"></asp:LinkButton>
                                                                            </ajax:AjaxPanel>
                                                                        </td>
                                                                        <td style="text-align: right; height: 49px;" width="99%">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#ffffff" width="100%">
                                                                <table ID="Table3" bgcolor="#ffffff" border="2" bordercolor="#ffffff" 
                                                                    cellpadding="2" cellspacing="1" width="100%">
                                                                    <tr align="center" bgcolor="#dddddd" class="regtext">
                                                                        <td align="left" bgcolor="#ffffff" style="height: 46px" valign="top" 
                                                                            width="50%">
                                                                            Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel7" runat="server" 
                                                                                meta:resourcekey="AjaxPanel7Resource1">
                                                                                <asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext" 
                                                                                    meta:resourcekey="PageCurrentPageResource1">0</asp:Label>
                                                                                &nbsp;of&nbsp;
                                                                                <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext" 
                                                                                    meta:resourcekey="PageTotalPagesResource1">0</asp:Label>
                                                                            </ajax:AjaxPanel>
                                                                        </td>
                                                                        <td align="right" bgcolor="#ffffff" style="height: 46px" valign="top" 
                                                                            width="50%">
                                                                            Total Records&nbsp;
                                                                            <ajax:AjaxPanel ID="AjaxPanel8" runat="server" 
                                                                                meta:resourcekey="AjaxPanel8Resource1">
                                                                                <asp:Label ID="PageTotalRows" runat="server" 
                                                                                    meta:resourcekey="PageTotalRowsResource1">0</asp:Label>
                                                                            </ajax:AjaxPanel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table ID="Table4" bgcolor="#ffffff" border="2" bordercolor="#ffffff" 
                                                                    cellpadding="2" cellspacing="1" width="100%">
                                                                    <tr bgcolor="#ffffff">
                                                                        <td align="left" class="regtext" colspan="11" height="7" valign="middle">
                                                                            <hr color="#f40101" noshade size="1" width="100%"></hr>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="63">
                                                                            Go to page
                                                                        </td>
                                                                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="5">
                                                                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                                            <ajax:AjaxPanel ID="AjaxPanel9" runat="server" 
                                                                                meta:resourcekey="AjaxPanel9Resource1">
                                                                                <asp:TextBox ID="TextGoToPage" runat="server" CssClass="searcheditbox" 
                                                                                    meta:resourcekey="TextGoToPageResource1" Width="38px"></asp:TextBox>
                                                                            </ajax:AjaxPanel>
                                                                            </font>
                                                                        </td>
                                                                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle">
                                                                            <ajax:AjaxPanel ID="AjaxPanel10" runat="server" 
                                                                                meta:resourcekey="AjaxPanel10Resource1">
                                                                                <asp:ImageButton ID="ImageButtonGo" runat="server" 
                                                                                    ImageUrl="~/Images/button/go.gif" meta:resourcekey="ImageButtonGoResource1" 
                                                                                    SkinID="GoButton" />
                                                                            </ajax:AjaxPanel>
                                                                        </td>
                                                                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                            <img height="5" src="images/first.gif" width="6"> </img></td>
                                                                        <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                            <ajax:AjaxPanel ID="AjaxPanel11" runat="server" 
                                                                                meta:resourcekey="AjaxPanel11Resource1">
                                                                                <asp:LinkButton ID="First" runat="server" CommandName="First" 
                                                                                    CssClass="regtext" meta:resourcekey="LinkButtonFirstResource1" 
                                                                                    OnCommand="PageNavigate">First</asp:LinkButton>
                                                                            </ajax:AjaxPanel>
                                                                        </td>
                                                                        <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                            <img height="5" src="images/prev.gif" width="6"> </img></td>
                                                                        <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="14">
                                                                            <ajax:AjaxPanel ID="AjaxPanel12" runat="server" 
                                                                                meta:resourcekey="AjaxPanel12Resource1">
                                                                                <asp:LinkButton ID="LinkButtonPrevious" runat="server" CommandName="Prev" 
                                                                                    CssClass="regtext" meta:resourcekey="LinkButtonPreviousResource1" 
                                                                                    OnCommand="PageNavigate">Previous</asp:LinkButton>
                                                                            </ajax:AjaxPanel>
                                                                        </td>
                                                                        <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="60">
                                                                            <ajax:AjaxPanel ID="AjaxPanel13" runat="server" 
                                                                                meta:resourcekey="AjaxPanel13Resource1">
                                                                                <a class="pageNav" href="#">
                                                                                <asp:LinkButton ID="LinkButtonNext" runat="server" CommandName="Next" 
                                                                                    CssClass="regtext" meta:resourcekey="LinkButtonNextResource1" 
                                                                                    OnCommand="PageNavigate">Next</asp:LinkButton>
                                                                                </a>
                                                                            </ajax:AjaxPanel>
                                                                        </td>
                                                                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                            <img height="5" src="images/next.gif" width="6"> </img></td>
                                                                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="25">
                                                                            <ajax:AjaxPanel ID="AjaxPanel16" runat="server" 
                                                                                meta:resourcekey="AjaxPanel16Resource1">
                                                                                <asp:LinkButton ID="LinkButtonLast" runat="server" CommandName="Last" 
                                                                                    CssClass="regtext" meta:resourcekey="LinkButtonLastResource1" 
                                                                                    OnCommand="PageNavigate">Last</asp:LinkButton>
                                                                            </ajax:AjaxPanel>
                                                                        </td>
                                                                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                            <img height="5" src="images/last.gif" width="6"> </img></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </table>
                                                    </ajax:AjaxPanel>
                               </td>
                            </tr>
						</table>
                    </div> 								
				</td>
			</tr>
			<tr>							
					<ajax:AjaxPanel ID="AjaxPanel14" runat="server" meta:resourcekey="AjaxPanel14Resource1">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td align="left" background="Images/button-bground.gif" valign="middle">
									<img height="1" src="Images/blank.gif" width="5" />
								</td>
								<td align="left" background="Images/button-bground.gif" valign="middle">
									<img height="15" src="images/arrow.gif" width="15" />&nbsp;
								</td>
								<td background="Images/button-bground.gif">
									&nbsp;
								</td>
								<td background="Images/button-bground.gif">
									&nbsp;
								</td>
								<td background="Images/button-bground.gif">
									&nbsp;
								</td>
								<td background="Images/button-bground.gif" width="99%">
									<img height="1" src="Images/blank.gif" width="1" />
								</td>
								<td>
									
								</td>
							</tr>
						</table>
						<asp:CustomValidator ID="CustomValidator1" runat="server" Display="None" ValidationGroup="handle"
							    meta:resourcekey="CustomValidator1Resource1"></asp:CustomValidator>
                        <asp:CustomValidator ID="CustomValidator2" runat="server" Display="None" 
                                meta:resourcekey="CustomValidator2Resource1"></asp:CustomValidator>
                        </ajax:AjaxPanel>
            </tr>	
	        </table>
</asp:Content>