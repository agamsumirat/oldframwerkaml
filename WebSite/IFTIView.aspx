<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="IFTIView.aspx.vb" Inherits="IFTIView" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <%--<script src="Script/popcalendar.js"></script>--%>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td style="height: 13px">
                            </td>
                        <td width="99%" bgcolor="#FFFFFF" style="height: 13px">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td bgcolor="#FFFFFF" style="height: 13px">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="10" height="100%" /></td>
                            <td class="divcontentinside" bgcolor="#FFFFFF" style="width: 1323px">
                                <%--<ajax:AjaxPanel ID="a" runat="server" Width="488px">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="There were uncompleteness on the page:"
                                        Width="95%" CssClass="validation"></asp:ValidationSummary>
                                </ajax:AjaxPanel>--%>
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <img src="Images/dot_title.gif" width="17" height="17" />
                                                <strong>
                                                    <asp:Label ID="Label1" runat="server" Text="IFTI - View"></asp:Label>
                                                </strong>
                                                <hr />
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Label6" runat="server" Text="Search Criteria" Font-Bold="True"></asp:Label>
                                                            &nbsp;</td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('SearchCriteria','searchimage4','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="searchimage4" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="SearchCriteria">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label7" runat="server" Text="Name"></asp:Label></td>
                                                        <td nowrap style="background-color: #ffffff; width: 33%">
                                                            <asp:TextBox ID="TxtFullName" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="125px" MaxLength="50"></asp:TextBox></td>
                                                        <td nowrap="nowrap" style="width: 15%; height: 24px; background-color: #fff7e6">
                                                            <asp:Label ID="Label16" runat="server" Text="Local Equivalen "></asp:Label></td>
                                                        <td nowrap="nowrap" style="width: 35%; background-color: #ffffff">
                                                            <asp:TextBox ID="txtLocalEquivalenBegin" runat="server" CssClass="searcheditbox"
                                                                MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                            &nbsp; &nbsp;&nbsp; to &nbsp;<asp:TextBox ID="txtLocalEquivalenEnd" runat="server"
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap="nowrap" style="width: 15%; height: 24px; background-color: #fff7e6">
                                                            <asp:Label ID="Label5" runat="server" Text="SWIFT/NON SWIFT"></asp:Label></td>
                                                        <td nowrap="nowrap" style="width: 33%; height: 24px; background-color: #ffffff">
                                                            <asp:RadioButtonList ID="rbTransactionSwift" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="0">All</asp:ListItem>
                                                                <asp:ListItem Value="1">Swift</asp:ListItem>
                                                                <asp:ListItem Value="2">Non Swift</asp:ListItem>
                                                            </asp:RadioButtonList></td>
                                                        <td nowrap="nowrap" style="width: 15%; height: 24px; background-color: #fff7e6">
                                                            <asp:Label ID="Label17" runat="server" Text="Data Valid"></asp:Label></td>
                                                        <td nowrap="nowrap" style="width: 35%; height: 24px; background-color: #ffffff">
                                                            <asp:RadioButtonList ID="rbDataValid" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="0">All</asp:ListItem>
                                                                <asp:ListItem Value="1">Valid</asp:ListItem>
                                                                <asp:ListItem Value="2">Invalid</asp:ListItem>
                                                            </asp:RadioButtonList></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap="nowrap" style="width: 15%; height: 25px; background-color: #fff7e6">
                                                            <asp:Label ID="Label11" runat="server" Text="Type Transaction"></asp:Label></td>
                                                        <td nowrap="nowrap" style="width: 33%; height: 25px; background-color: #ffffff">
                                                            <asp:RadioButtonList ID="rbTransactionType" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="0">All</asp:ListItem>
                                                                <asp:ListItem Value="1">Incoming</asp:ListItem>
                                                                <asp:ListItem Value="2">Outgoing</asp:ListItem>
                                                            </asp:RadioButtonList></td>
                                                        <td nowrap="nowrap" style="width: 15%; height: 25px; background-color: #fff7e6">
                                                            <br />
                                                            <asp:RadioButtonList ID="rbDateType" runat="server">
                                                                <asp:ListItem Value="0">Transaction Date</asp:ListItem>
                                                                <asp:ListItem Value="1">Report Date</asp:ListItem>
                                                            </asp:RadioButtonList></td>
                                                        <td nowrap="nowrap" style="width: 35%; height: 25px; background-color: #ffffff">
                                                            <table>
                                                                <tr>
                                                                    <td style="width: 150px">
                                                                        <asp:TextBox ID="txtTransactionDateBegin" runat="server" CssClass="searcheditbox" TabIndex="2" Width="122px"></asp:TextBox>
                                                                        <input id="popUpTransactionDateBegin" title="Click to show calendar"
                                                                    style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                                    border-left: #ffffff 0px solid; border-bottom: #ffffff 0px solid; height: 17px;
                                                                    background-image: url(Script/Calendar/cal.gif); width: 16px;" type="button" onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')"
                                                                    runat="server" /></td>
                                                                    <td style="width: 14px">
                                                                        to</td>
                                                                    <td style="width: 131px">
                                                                        <asp:TextBox ID="txtTransactionDateEnd" runat="server" CssClass="searcheditbox" TabIndex="2" Width="100px"></asp:TextBox>
                                                                        <input id="popUpTransactionDateEnd" title="Click to show calendar"
                                                                    style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                                    border-left: #ffffff 0px solid; border-bottom: #ffffff 0px solid; height: 17px;
                                                                    background-image: url(Script/Calendar/cal.gif); width: 16px;" type="button" onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')"
                                                                    runat="server" /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap="nowrap" style="width: 15%; height: 12px; background-color: #fff7e6">
                                                            <asp:Label ID="Label12" runat="server" Text="Currency"></asp:Label></td>
                                                        <td nowrap="nowrap" style="width: 33%; height: 12px; background-color: #ffffff">
                                                            <asp:DropDownList ID="cboCurrency" runat="server">
                                                            </asp:DropDownList></td><td nowrap="nowrap" style="width: 15%; height: 24px; background-color: #fff7e6">
                                                            <asp:Label ID="Label18" runat="server" Text="Aging"></asp:Label></td>
                                                        <td nowrap="nowrap" style="width: 35%; height: 24px; background-color: #ffffff">
                                                            <asp:TextBox ID="txtAging" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="53px"></asp:TextBox>
                                                            days</td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap="nowrap" style="width: 15%; height: 24px; background-color: #fff7e6">
                                                            <asp:Label ID="Label13" runat="server" Text="Nominal "></asp:Label></td>
                                                        <td nowrap="nowrap" style="width: 33%; height: 24px; background-color: #ffffff">
                                                            <asp:TextBox ID="txtNominalBegin" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox>&nbsp; to &nbsp;<asp:TextBox ID="txtNominalEnd"
                                                                    runat="server" CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td nowrap="nowrap" style="width: 15%; height: 24px; background-color: #fff7e6">
                                                            <asp:Label ID="Label19" runat="server" Text="Payment Detail"></asp:Label></td>
                                                        <td nowrap="nowrap" style="width: 35%; height: 24px; background-color: #ffffff">
                                                            <asp:TextBox ID="txtPaymentDetail" runat="server" CssClass="searcheditbox" Height="71px"
                                                                MaxLength="50" TabIndex="2" TextMode="MultiLine" Width="355px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap="nowrap" style="width: 15%; height: 43px; background-color: #fff7e6">
                                                            <asp:Label ID="Label14" runat="server" Text="Report to PPATK"></asp:Label></td>
                                                        <td nowrap="nowrap" style="width: 33%; height: 43px; background-color: #ffffff">
                                                            <asp:RadioButtonList ID="rbReporttoPPATK" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="0">All</asp:ListItem>
                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                <asp:ListItem Value="2">No</asp:ListItem>
                                                            </asp:RadioButtonList></td><td nowrap="nowrap" style="width: 15%; height: 24px; background-color: #fff7e6">
                                                                <asp:Label ID="Label2" runat="server" Text="Sender Reference Number"></asp:Label></td>
                                                        <td nowrap="nowrap" style="width: 33%; height: 24px; background-color: #ffffff">
                                                            <asp:TextBox ID="txtSenderReference" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="241px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap="nowrap" style="width: 15%; height: 24px; background-color: #fff7e6">
                                                            <asp:Label ID="Label15" runat="server" Text="Account Number"></asp:Label></td>
                                                        <td nowrap="nowrap" style="width: 33%; height: 24px; background-color: #ffffff">
                                                            <asp:TextBox ID="txtAccountNumber" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="241px"></asp:TextBox></td>
                                                        <td nowrap="nowrap">
                                                        </td>
                                                        <td nowrap="nowrap" style="width: 35%; height: 24px; background-color: #ffffff">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%; height: 48px;">
                                                        </td>
                                                        <td nowrap style="background-color: #ffffff; width: 33%; height: 48px;">
                                                        </td>
                                                        <td nowrap="nowrap">
                                                        </td>
                                                        <td nowrap="nowrap" style="width: 35%; background-color: #ffffff; height: 48px;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="height: 6px">
                                                            &nbsp;<asp:ImageButton ID="ImageButtonSearch" TabIndex="3" runat="server" 
                                                                CausesValidation="False" ImageUrl="~/Images/Button/Search.gif"></asp:ImageButton>&nbsp;
                                                                  <asp:ImageButton ID="ImagebuttonClear" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/clearSearch.gif" TabIndex="3" /></td>
                                                        <td colspan="1" style="width: 169px; height: 6px">
                                                        </td>
                                                        <td colspan="1" style="height: 6px">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr id="TR1">
                                            <td valign="top" width="98%" bgcolor="#ffffff" style="height: 357px">
                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="0" width="100%" bgcolor="#dddddd"
                                                    border="2">
                                                    <tr>
                                                        <td bgcolor="#ffffff" background="images/testbg.gif">
                                                            <asp:DataGrid ID="GridDataView" runat="server" Font-Size="XX-Small"
                                                                CellPadding="4" AllowPaging="True" Width="100%" GridLines="Vertical"
                                                                AllowSorting="True" ForeColor="Black" Font-Bold="False" Font-Italic="False"
                                                                Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" AllowCustomPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px">
                                                                <Columns>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="2%" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="PK_IFTI_ID" Visible="False">
                                                                        <HeaderStyle Width="0%" />
                                                                    </asp:BoundColumn>
                                                                    <asp:TemplateColumn HeaderText="No.">
                                                                        <HeaderStyle ForeColor="White" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="nama" HeaderText="Name" SortExpression="nama desc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="TransactionSwift" HeaderText="SWIFT/NON SWIFT" SortExpression="TransactionSwift desc">
                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" />
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Left" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="SenderReference" HeaderText="Sender's Reference Number"
                                                                        SortExpression="SenderReference desc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="TransactionType" HeaderText="Type Transaction" SortExpression="TransactionType desc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="ValueDate_FK_Currency_ID" HeaderText="Currency" SortExpression="ValueDate_FK_Currency_ID desc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn  DataFormatString="{0:###,###,###,###,###,###.00}" DataField="ValueDate_NilaiTransaksi" HeaderText="Nominal " SortExpression="ValueDate_NilaiTransaksi desc">
                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" Wrap="False" />
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Right" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataFormatString="{0:###,###,###,###,###,###.00}" DataField="ValueDate_NilaiTransaksiIDR" HeaderText="Local Equivalen " SortExpression="ValueDate_NilaiTransaksiIDR desc">
                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Right" Wrap="False" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="IsDataValid" HeaderText="Data Valid" SortExpression="IsDataValid desc">
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Left" Wrap="False" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="TanggalLaporan" HeaderText="Report Date" SortExpression="TanggalLaporan desc" DataFormatString="{0:dd-MMM-yyyy}">
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Left" Wrap="False" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="TanggalTransaksi" HeaderText="Transaction Date" SortExpression="TanggalTransaksi desc" DataFormatString="{0:dd-MMM-yyyy}"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="AccountNumber" HeaderText="Account Number" SortExpression="AccountNumber desc">
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Left" Wrap="False" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="ReportToPPATK" HeaderText="Report to PPATK" SortExpression="ReportToPPATK desc">
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" VerticalAlign="Top" HorizontalAlign="Left" Wrap="False" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Aging" HeaderText="Aging" SortExpression="Aging desc">
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="PaymentDetail" HeaderText="Payment Detail" SortExpression="PaymentDetail desc">
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:BoundColumn>
                                                                    <asp:TemplateColumn HeaderText="Error Description">
                                                                        <ItemTemplate>
                                                                            <asp:BulletedList ID="BLErrorDescription" runat="server">
                                                                            </asp:BulletedList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="AutoReplace" HeaderText="Autoreplace" SortExpression="AutoReplace desc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="LTDLNNo" HeaderText="PPATK Reference Number" SortExpression="LTDLNNo desc">
                                                                    </asp:BoundColumn>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="Btndetail" runat="server" CommandName="detail">Detail</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="btnEdit" runat="server" CommandName="edit">Edit</asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn Visible="False" DataField="FK_IFTI_Type_ID"></asp:BoundColumn>
                                                                </Columns>
                                                                <PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages">
                                                                </PagerStyle>
                                                                <AlternatingItemStyle VerticalAlign="Top" Wrap="False" BackColor="White" />
                                                                <ItemStyle VerticalAlign="Top" Wrap="False" BackColor="#F7F7DE" />
                                                                <FooterStyle BackColor="#CCCC99" />
                                                                <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:Label ID="LabelDataNotFound" runat="server" Font-Size="X-Large" Text="Data Not Found"
                                                    Visible="False"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #ffffff">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td nowrap style="height: 20px">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td style="width: 81px">
                                                                                    <asp:CheckBox ID="CheckBoxSelectAll" runat="server" AutoPostBack="True" Text="Select All" />
                                                                                </td>
                                                                                <td style="width: 139px">
                                                                                    &nbsp;<asp:LinkButton ID="LinkButtonExportExcel" runat="server" ajaxcall="none" meta:resourcekey="LinkButtonExportExcelResource1"
                                                                                        OnClientClick="aspnetForm.encoding = 'multipart/form-data';" Width="152px">Export Selected to Excel</asp:LinkButton></td>
                                                                                <td style="width: 172px">
                                                                                    &nbsp;<asp:LinkButton ID="lnkExportAllData" runat="server" ajaxcall="none" meta:resourcekey="lnkExportAllDataResource1"
                                                                                        OnClientClick="aspnetForm.encoding = 'multipart/form-data';" Text="Export All Filtered Data to Excel"
                                                                                        Width="188px"></asp:LinkButton></td>
                                                                                <td style="width: 120px">
                                                                                    &nbsp;</td>
                                                                                <td>
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="height: 20px">
                                                                        &nbsp;&nbsp;
                                                                    </td>
                                                                    <td align="right" nowrap style="height: 20px">
                                                                        &nbsp;
                                                                        &nbsp;&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>   
                                                <table width="100%">                                    
                                                <tr>
                                                    <td bgcolor="#ffffff">
                                                        <table id="Table3" bgcolor="#ffffff" border="2" bordercolor="#ffffff" cellpadding="2"
                                                            cellspacing="1" width="100%">
                                                            <tr align="center" bgcolor="#dddddd" class="regtext">
                                                                <td align="left" bgcolor="#ffffff" style="height: 49px" valign="top" width="50%">
                                                                    Page&nbsp;<asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>
                                                                    &nbsp;of&nbsp;
                                                                    <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label>
                                                                </td>
                                                                <td align="right" bgcolor="#ffffff" style="height: 49px" valign="top" width="50%">
                                                                    Total Records&nbsp;
                                                                    <asp:Label ID="PageTotalRows" runat="server">0</asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table id="Table4" bgcolor="#ffffff" border="2" bordercolor="#ffffff" cellpadding="2"
                                                            cellspacing="1" width="100%">
                                                            <tr bgcolor="#ffffff">
                                                                <td align="left" class="regtext" colspan="11" height="7" valign="middle">
                                                                    <hr></hr>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="63">
                                                                    Go to page</td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="5">
                                                                    <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                                        <asp:TextBox ID="TextGoToPage" runat="server" CssClass="searcheditbox" Width="38px"></asp:TextBox>
                                                                    </font>
                                                                </td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle">
                                                                    <asp:ImageButton ID="ImageButtonGo" runat="server" ImageUrl="~/Images/Button/Go.gif"
                                                                         />
                                                                </td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                    <img height="5" src="images/first.gif" width="6"> </img>
                                                                </td>
                                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                    <asp:LinkButton ID="LinkButtonFirst" runat="server" CommandName="First" CssClass="regtext"
                                                                        OnCommand="PageNavigate">First</asp:LinkButton>
                                                                </td>
                                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                    <img height="5" src="images/prev.gif" width="6"></img></td>
                                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="14">
                                                                    <asp:LinkButton ID="LinkButtonPrevious" runat="server" CommandName="Prev" CssClass="regtext"
                                                                        OnCommand="PageNavigate">Previous</asp:LinkButton>
                                                                </td>
                                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="60">
                                                                    <a class="pageNav" href="#">
                                                                        <asp:LinkButton ID="LinkButtonNext" runat="server" CommandName="Next" CssClass="regtext"
                                                                            OnCommand="PageNavigate">Next</asp:LinkButton>
                                                                    </a>
                                                                </td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                    <img height="5" src="images/next.gif" width="6"></img></td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="25">
                                                                    <asp:LinkButton ID="LinkButtonLast" runat="server" CommandName="Last" CssClass="regtext"
                                                                        OnCommand="PageNavigate">Last</asp:LinkButton>
                                                                </td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                    <img height="5" src="images/last.gif" width="6"></img></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </table>   
                                            </td>
                                        </tr>
                                    </table>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="Images/blank.gif" width="5" height="1" /></td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="images/arrow.gif" width="15" height="15" /></td>
                        <td width="99%" background="Images/button-bground.gif">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td>
                            </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
