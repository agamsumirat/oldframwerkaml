Imports System.Web.Security

Partial Class SignOut
    Inherits Parent

    Private Sub AuditTrail()
        'Using Transcope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
            AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, "Logout")
            EKABLL.ScreeningBLL.SetUserAuditInfoLoginNewframework(Sahassa.AML.Commonly.SessionUserId, "Logout", Me.Request.UserHostAddress, "", 4)
            '        Transcope.Complete()
        End Using
        'End Using
    End Sub

    Private Sub MsUserClear()
        'Using Transcope As New Transactions.TransactionScope
        Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
            AccessUser.SignOutUser(Sahassa.AML.Commonly.SessionUserId)

            '        Transcope.Complete()
        End Using
        'End Using
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Me.Response.Cache.SetCacheability(HttpCacheability.NoCache)

            If Sahassa.AML.Commonly.SessionUserId.Length > 0 Then
                Me.AuditTrail()
                Me.MsUserClear()
            End If
            Me.Session.Abandon()
            Me.Request.Cookies.Clear()
            System.Web.Security.FormsAuthentication.SignOut()

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "Login.aspx"
        Me.Response.Redirect("Login.aspx", False)
    End Sub
End Class
