﻿#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports AMLBLL.AuditTrailBLL
Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
Imports System.Data.SqlClient
#End Region

Partial Class IFTI_SWIFT_ApprovalDetail
    Inherits Parent
    Private ReadOnly BindGridFromExcel As Boolean
#Region "Property"

    Public ReadOnly Property SetnGetUserID() As String
        Get
            Return SessionPkUserId
        End Get

    End Property

    Private Property GetTotalInsert() As Double
        Get
            Return Session("GetTotalInsert")
        End Get
        Set(ByVal value As Double)
            Session("GetTotalInsert") = value
        End Set
    End Property

    Private Property GetTotalUpdate() As Double
        Get
            Return Session("GetTotalUpdate")
        End Get
        Set(ByVal value As Double)
            Session("GetTotalUpdate") = value
        End Set
    End Property

    Public ReadOnly Property parID As String
        Get
            Return Request.Params("PK_IFTI_Approval_Id")
        End Get
    End Property

    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.Sort") Is Nothing, " FK_IFTI_Approval_Id  asc", Session("IFTI_Approval_Detail.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("IFTI_Approval_Detail.Sort") = Value
        End Set
    End Property

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.SelectedItem") Is Nothing, New ArrayList, Session("IFTI_Approval_Detail.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("IFTI_Approval_Detail.SelectedItem") = value
        End Set
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.RowTotal") Is Nothing, 0, Session("IFTI_Approval_Detail.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("IFTI_Approval_Detail.RowTotal") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return iTotalPages(SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.CurrentPage") Is Nothing, 0, Session("IFTI_Approval_Detail.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("IFTI_Approval_Detail.CurrentPage") = Value
        End Set
    End Property
    Private Sub LoadIDType()
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenNew.Items.Clear()
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenNew.Items.Add("-Select-")
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenNew.AppendDataBoundItems = True
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenNew.DataTextField = "IDType"
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenNew.DataValueField = "PK_IFTI_IDType"
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenNew.DataBind()

        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenOld.Items.Clear()
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenOld.Items.Add("-Select-")
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenOld.AppendDataBoundItems = True
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenOld.DataTextField = "IDType"
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenOld.DataValueField = "PK_IFTI_IDType"
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenOld.DataBind()

        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.Items.Clear()
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.Items.Add("-Select-")
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.AppendDataBoundItems = True
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.DataTextField = "IDType"
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.DataValueField = "PK_IFTI_IDType"
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.DataBind()

        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.Items.Clear()
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.Items.Add("-Select-")
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.AppendDataBoundItems = True
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.DataTextField = "IDType"
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.DataValueField = "PK_IFTI_IDType"
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.DataBind()

        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenOld.Items.Clear()
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenOld.Items.Add("-Select-")
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenOld.AppendDataBoundItems = True
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenOld.DataTextField = "IDType"
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenOld.DataValueField = "PK_IFTI_IDType"
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenOld.DataBind()

        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenNew.Items.Clear()
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenNew.Items.Add("-Select-")
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenNew.AppendDataBoundItems = True
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenNew.DataTextField = "IDType"
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenNew.DataValueField = "PK_IFTI_IDType"
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenNew.DataBind()

        Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitasOld.Items.Clear()
        Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitasOld.Items.Add("-Select-")
        Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitasOld.AppendDataBoundItems = True
        Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitasOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitasOld.DataTextField = "IDType"
        Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitasOld.DataValueField = "PK_IFTI_IDType"
        Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitasOld.DataBind()

        Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitasNew.Items.Clear()
        Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitasNew.Items.Add("-Select-")
        Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitasNew.AppendDataBoundItems = True
        Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitasNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitasNew.DataTextField = "IDType"
        Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitasNew.DataValueField = "PK_IFTI_IDType"
        Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitasNew.DataBind()

    End Sub

    Public ReadOnly Property SetnGetBindTable(Optional ByVal AllRecord As Boolean = False) As TList(Of IFTI_Approval_Beneficiary)
        Get
            Dim TotalDisplay As Integer = 0
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
                SetnGetCurrentPage = GetPageTotal - 1
            End If

            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = "FK_IFTI_Approval_Id=" & parID

            If AllRecord = False Then
                TotalDisplay = GetDisplayedTotalRow
            Else
                TotalDisplay = Integer.MaxValue
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            Return DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, TotalDisplay, SetnGetRowTotal)

        End Get

    End Property
    Private _DataNonSwiftIn As IFTI_Approval_Detail
    Private ReadOnly Property DataNonSwiftIn() As IFTI_Approval_Detail
        Get
            Try
                If _DataNonSwiftIn Is Nothing Then
                    Dim otable As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)
                    If Not otable Is Nothing Then
                        _DataNonSwiftIn = otable
                        Return _DataNonSwiftIn
                    Else
                        Return Nothing
                    End If

                Else
                    Return _DataNonSwiftIn
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property
    Private _Databenefeciary As IFTI_Beneficiary
    Private ReadOnly Property Databenefeciary() As IFTI_Beneficiary
        Get
            Dim test As Integer = Session("FK_IFTI_Beneficiary_ID")
            Try
                If _Databenefeciary Is Nothing Then
                    Using otable As IFTI_Beneficiary = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID =" & test, "", 0, Integer.MaxValue, 0)(0)
                        If Not otable Is Nothing Then
                            _Databenefeciary = otable
                            Return _Databenefeciary
                        Else
                            Return Nothing
                        End If
                    End Using
                Else
                    Return _Databenefeciary
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property
    Private _DatabenefeciaryApproval As IFTI_Approval_Beneficiary
    Private ReadOnly Property DatabenefeciaryApproval() As IFTI_Approval_Beneficiary
        Get
            Dim test As Integer = Session("PK_IFTI_Approval_Beneficiary_ID")
            Try
                If _DatabenefeciaryApproval Is Nothing Then
                    Using otable As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_Approval_Id =" & DataNonSwiftIn.FK_IFTI_Approval_Id.ToString, "", 0, Integer.MaxValue, 0)(0)
                        Dim otables As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("PK_IFTI_Approval_Beneficiary_ID =" & test, "", 0, Integer.MaxValue, 0)(0)
                        If Not otable Is Nothing Then
                            _DatabenefeciaryApproval = otable
                            Return _DatabenefeciaryApproval
                        Else
                            Return Nothing
                        End If
                    End Using
                Else
                    Return _DatabenefeciaryApproval
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property

    Private Sub bindgrid()
        Try
            If Not DataNonSwiftIn Is Nothing Then

                '++++++++++++++++++++++++++++++++++++++++UMUM+++++++++++++++++++++++++++++++++++++++++++'

                If Not DataNonSwiftIn.LTDLNNo = "" Then
                    SwiftInUmum_LTDNNew.Text = DataNonSwiftIn.LTDLNNo
                End If
                If Not DataNonSwiftIn.LTDLNNo_Old = "" Then
                    SwiftInUmum_LTDNOld.Text = DataNonSwiftIn.LTDLNNo_Old
                End If
                If Not DataNonSwiftIn.LTDLNNoKoreksi = "" Then
                    SwiftInUmum_LtdnKoreksiNew.Text = DataNonSwiftIn.LTDLNNoKoreksi
                End If
                If Not DataNonSwiftIn.LTDLNNoKoreksi_Old = "" Then
                    SwiftInUmum_LtdnKoreksiOld.Text = DataNonSwiftIn.LTDLNNoKoreksi_Old
                End If
                If Not IsNothing(DataNonSwiftIn.TanggalLaporan) Then
                    SwiftInUmum_TanggalLaporanNew.Text = DataNonSwiftIn.TanggalLaporan
                End If
                If Not IsNothing(DataNonSwiftIn.TanggalLaporan_Old) Then
                    SwiftInUmum_TanggalLaporanOld.Text = DataNonSwiftIn.TanggalLaporan_Old
                End If
                If Not DataNonSwiftIn.NamaPJKBankPelapor = "" Then
                    SwiftInUmum_NamaPJKBankNew.Text = DataNonSwiftIn.NamaPJKBankPelapor
                End If
                If Not DataNonSwiftIn.NamaPJKBankPelapor_Old = "" Then
                    SwiftInUmum_NamaPJKBankOld.Text = DataNonSwiftIn.NamaPJKBankPelapor_Old
                End If
                If Not DataNonSwiftIn.NamaPejabatPJKBankPelapor = "" Then
                    SwiftInUmum_NamaPejabatPJKBankNew.Text = DataNonSwiftIn.NamaPejabatPJKBankPelapor
                End If
                If Not DataNonSwiftIn.NamaPejabatPJKBankPelapor_Old = "" Then
                    SwiftInUmum_NamaPejabatPJKBankOld.Text = DataNonSwiftIn.NamaPejabatPJKBankPelapor_Old
                End If
                If Not IsNothing(DataNonSwiftIn.JenisLaporan) Then
                    RbSwiftInUmum_JenisLaporanNew.SelectedValue = DataNonSwiftIn.JenisLaporan.GetValueOrDefault
                End If
                If Not IsNothing(DataNonSwiftIn.JenisLaporan_Old) Then
                    RbSwiftInUmum_JenisLaporanOld.SelectedValue = DataNonSwiftIn.JenisLaporan_Old.GetValueOrDefault
                End If

                '+++++++++++++++++++++++++++++++++++++++++++++PENGIRIM+++++++++++++++++++++++++++++++++++++++++++'

                If Not DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening = "" Then
                    txtSwiftInPengirim_rekeningNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening
                End If
                If Not DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening_Old = "" Then
                    txtSwiftInPengirim_rekeningOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening_Old
                End If
                'old
                If ObjectAntiNull(DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old) = True Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old.GetValueOrDefault(0)
                        Case 1
                            Me.RBPenerimaNasabah_TipePengirimOld.SelectedValue = 1
                            MultiViewSwiftInIdenPengirim.ActiveViewIndex = 0
                        Case 2
                            Me.RBPenerimaNasabah_TipePengirimOld.SelectedValue = 1
                            MultiViewSwiftInIdenPengirim.ActiveViewIndex = 0
                        Case 3
                            Me.RBPenerimaNasabah_TipePengirimOld.SelectedValue = 2
                            MultiViewSwiftInIdenPengirim.ActiveViewIndex = 1
                            PengirimNonNasabah()
                    End Select
                End If
                'new
                If ObjectAntiNull(DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID) = True Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID.GetValueOrDefault(0)
                        Case 1
                            Me.RBPenerimaNasabah_TipePengirimNew.SelectedValue = 1
                        Case 2
                            Me.RBPenerimaNasabah_TipePengirimNew.SelectedValue = 1
                        Case 3
                            Me.RBPenerimaNasabah_TipePengirimNew.SelectedValue = 2
                    End Select
                End If
                'old
                If ObjectAntiNull(DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old) = True Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old.GetValueOrDefault(0)
                        Case 1
                            Me.Rb_IdenPenerimaNas_TipeNasabahOld.SelectedValue = 1
                            MultiViewPengirimNasabah_OLD.ActiveViewIndex = 0
                            PengirimNasabah()
                        Case 2
                            Me.Rb_IdenPenerimaNas_TipeNasabahOld.SelectedValue = 2
                            MultiViewPengirimNasabah_OLD.ActiveViewIndex = 1
                            PengirimKorporasi()

                    End Select
                End If
                'new
                If ObjectAntiNull(DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID) = True Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID.GetValueOrDefault(0)
                        Case 1
                            Me.Rb_IdenPenerimaNas_TipeNasabahNew.SelectedValue = 1
                            MultiViewPengirimNasabah_New.ActiveViewIndex = 0
                            PengirimNasabahNew()
                        Case 2
                            Me.Rb_IdenPenerimaNas_TipeNasabahNew.SelectedValue = 2
                            MultiViewPengirimNasabah_New.ActiveViewIndex = 1
                            PengirimKorporasiNew()
                    End Select
                End If

                '==========================================================================================='

                'If ObjectAntiNull(Databenefeciary.PJKBank_type) = True Then
                '    Select Case Databenefeciary.PJKBank_type.GetValueOrDefault
                '        Case 1
                '            Me.cboSwiftInPenerimaNonNasabah_TipePJKBankOld.SelectedValue = 1
                '            MultiViewSwiftInPenerima.ActiveViewIndex = 0
                '        Case 2
                '            Me.cboSwiftInPenerimaNonNasabah_TipePJKBankOld.SelectedValue = 2
                '            MultiViewSwiftInPenerima.ActiveViewIndex = 1
                '    End Select
                'End If

                'If ObjectAntiNull(Databenefeciary.PJKBank_type) = True Then
                '    Select Case DatabenefeciaryApproval.PJKBank_type.GetValueOrDefault
                '        Case 1
                '            Me.cboSwiftInPenerimaNonNasabah_TipePJKBankNew.SelectedValue = 1
                '        Case 2
                '            Me.cboSwiftInPenerimaNonNasabah_TipePJKBankNew.SelectedValue = 2
                '    End Select
                'End If
                '+++++++++++++++++++++++++++++++++++++++++++++C.1 IDENTITAS PENERIMA++++++++++++++++++++++++++++++++++++++++++++'
                databindgrid()
                Transaksi()
                Informasi()

            End If

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub PengirimNasabah()

        '---------------------------------------------Negara-------------------------------------'
        'Dim idnegara As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara
        Dim idnegara_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old.GetValueOrDefault(0)
        'Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        'getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '---------------------------------------------Negara iden-------------------------------------'
        'Dim idnegaraiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Negara
        Dim idnegaraiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Negara_Old.GetValueOrDefault(0)
        'Dim getnegaraidentype As MsNegara
        Dim getnegaraidentype_old As MsNegara
        'getnegaraidentype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegaraiden)
        getnegaraidentype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegaraiden_old)

        '========================================NASABAH INDIVIDU========================================='

        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap = "" Then
        '    txtSwiftInPengirimNasabah_IND_namaNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old = "" Then
            txtSwiftInPengirimNasabah_IND_namaOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir = "" Then
        '    txtSwiftInPengirimNasabah_IND_TanggalLahirNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir
        'End If
        If Not IsNothing(DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old) Then
            txtSwiftInPengirimNasabah_IND_TanggalLahirOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan = "" Then
        '    RbSwiftInPengirimNasabah_IND_WargaNegaraNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan.GetValueOrDefault
        'End If
        If Not IsNothing(DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old) Then
            RbSwiftInPengirimNasabah_IND_WargaNegaraOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old.GetValueOrDefault
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Negara) = True Then
        '    txtSwiftInPengirimNasabah_IND_negaraNew.Text = getnegaratype.NamaNegara
        'End If
        If Not IsNothing(getnegaratype_old) Then
            txtSwiftInPengirimNasabah_IND_negaraOld.Text = getnegaratype_old.NamaNegara
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya) = True Then
        '    txtSwiftInPengirimNasabah_IND_negaraLainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya_Old = "" Then
            txtSwiftInPengirimNasabah_IND_negaraLainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya_Old
        End If
        'txtSwiftInPengirimNasabah_IND_alamatIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat
        txtSwiftInPengirimNasabah_IND_alamatIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat_Old
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraKota) = True Then
        '    txtSwiftInPengirimNasabah_IND_KotaNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraKota
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraKota_Old) = True Then
            txtSwiftInPengirimNasabah_IND_KotaOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraKota_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_Negara) = True Then
        '    txtSwiftInPengirimNasabah_IND_negaraIdenNew.Text = getnegaraidentype.NamaNegara
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_Negara_Old) = True Then
            txtSwiftInPengirimNasabah_IND_negaraIdenOld.Text = getnegaraidentype_old.NamaNegara
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraLainnya) = True Then
        '    txtSwiftInPengirimNasabah_IND_negaraLainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraLainnya
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraLainnya_Old) = True Then
            txtSwiftInPengirimNasabah_IND_negaraLainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraLainnya_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NoTelp) = True Then
        '    txtSwiftInPengirimNasabah_IND_NoTelpNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoTelp
        'End If
        txtSwiftInPengirimNasabah_IND_NoTelpOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoTelp_Old
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType) = True Then
        '    CBOSwiftInPengirimNasabah_IND_JenisIdenNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old) = True Then
            CBOSwiftInPengirimNasabah_IND_JenisIdenOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NomorID) = True Then
        '    txtSwiftInPengirimNasabah_IND_NomorIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID
        'End If
        txtSwiftInPengirimNasabah_IND_NomorIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID_Old
    End Sub
    Private Sub PengirimNasabahNew()

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara.GetValueOrDefault(0)
        'Dim idnegara_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old
        Dim getnegaratype As MsNegara
        'Dim getnegaratype_old As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        'getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '---------------------------------------------Negara iden-------------------------------------'
        Dim idnegaraiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Negara.GetValueOrDefault(0)
        'Dim idnegaraiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Negara_Old
        Dim getnegaraidentype As MsNegara
        'Dim getnegaraidentype_old As MsNegara
        getnegaraidentype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegaraiden)
        'getnegaraidentype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegaraiden_old)

        '========================================NASABAH INDIVIDU========================================='

        If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap = "" Then
            txtSwiftInPengirimNasabah_IND_namaNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old = "" Then
        '    txtSwiftInPengirimNasabah_IND_namaOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old
        'End If
        If Not IsNothing(DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir) Then
            txtSwiftInPengirimNasabah_IND_TanggalLahirNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old = "" Then
        '    txtSwiftInPengirimNasabah_IND_TanggalLahirOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old
        'End If
        If Not IsNothing(DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan) Then
            RbSwiftInPengirimNasabah_IND_WargaNegaraNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan.GetValueOrDefault
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old = "" Then
        '    RbSwiftInPengirimNasabah_IND_WargaNegaraOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old.GetValueOrDefault
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Negara) = True Then
            txtSwiftInPengirimNasabah_IND_negaraNew.Text = getnegaratype.NamaNegara
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old) = True Then
        '    txtSwiftInPengirimNasabah_IND_negaraOld.Text = getnegaratype_old.NamaNegara
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya) = True Then
            txtSwiftInPengirimNasabah_IND_negaraLainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya_Old = "" Then
        '    txtSwiftInPengirimNasabah_IND_negaraLainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya_Old
        'End If
        txtSwiftInPengirimNasabah_IND_alamatIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat
        'txtSwiftInPengirimNasabah_IND_alamatIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraKota) = True Then
            txtSwiftInPengirimNasabah_IND_KotaNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraKota
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraKota_Old) = True Then
        '    txtSwiftInPengirimNasabah_IND_KotaOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraKota_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_Negara) = True Then
            txtSwiftInPengirimNasabah_IND_negaraIdenNew.Text = getnegaraidentype.NamaNegara
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_Negara_Old) = True Then
        '    txtSwiftInPengirimNasabah_IND_negaraIdenOld.Text = getnegaraidentype_old.NamaNegara
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraLainnya) = True Then
            txtSwiftInPengirimNasabah_IND_negaraLainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraLainnya
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraLainnya_Old) = True Then
        '    txtSwiftInPengirimNasabah_IND_negaraLainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraLainnya_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NoTelp) = True Then
            txtSwiftInPengirimNasabah_IND_NoTelpNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoTelp
        End If
        'txtSwiftInPengirimNasabah_IND_NoTelpOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoTelp_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType) = True Then
            CBOSwiftInPengirimNasabah_IND_JenisIdenNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old) = True Then
        '    CBOSwiftInPengirimNasabah_IND_JenisIdenOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NomorID) = True Then
            txtSwiftInPengirimNasabah_IND_NomorIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID
        End If
        'txtSwiftInPengirimNasabah_IND_NomorIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID_Old
    End Sub
    Private Sub PengirimKorporasi()
        '---------------------------------------------Negara-------------------------------------'
        ' Dim idnegara As String = DataNonSwiftIn.Sender_Nasabah_CORP_Negara.GetValueOrDefault(0)
        Dim idnegara_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_Negara_Old.GetValueOrDefault(0)
        'Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        'getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        'Dim idKotakab As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab_Old.GetValueOrDefault(0)
        'Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        'getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '=========================================KORPORASI============================================='

        'txtSwiftInPengirimNasabah_Korp_namaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi
        txtSwiftInPengirimNasabah_Korp_namaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi_Old
        'txtSwiftInPengirimNasabah_Korp_AlamatLengkapNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap
        txtSwiftInPengirimNasabah_Korp_AlamatLengkapOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap_Old
        'If Not IsNothing(getKotakabtype) Then
        '    txtSwiftInPengirimNasabah_Korp_KotaNew.Text = getKotakabtype.NamaKotaKab
        'End If
        If Not IsNothing(getKotakabtype_old) Then
            txtSwiftInPengirimNasabah_Korp_KotaOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        'If Not IsNothing(getnegaratype) Then
        '    txtSwiftInPengirimNasabah_Korp_NegaraNew.Text = getnegaratype.NamaNegara
        'End If
        If Not IsNothing(getnegaratype_old) Then
            txtSwiftInPengirimNasabah_Korp_NegaraOld.Text = getnegaratype_old.NamaNegara
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NegaraLainnya) = True Then
        '    txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NegaraLainnya
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NegaraLainnya_Old) = True Then
            txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NegaraLainnya_Old
        End If
        'txtSwiftInPengirimNasabah_Korp_NoTelpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NoTelp
        txtSwiftInPengirimNasabah_Korp_NoTelpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NoTelp_Old
    End Sub
    Private Sub PengirimKorporasiNew()
        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DataNonSwiftIn.Sender_Nasabah_CORP_Negara.GetValueOrDefault(0)
        'Dim idnegara_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_Negara_Old.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        'Dim getnegaratype_old As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        'getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab.GetValueOrDefault(0)
        'Dim idKotakab_old As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        'Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        'getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '=========================================KORPORASI============================================='

        txtSwiftInPengirimNasabah_Korp_namaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi
        'txtSwiftInPengirimNasabah_Korp_namaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi_Old
        txtSwiftInPengirimNasabah_Korp_AlamatLengkapNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap
        'txtSwiftInPengirimNasabah_Korp_AlamatLengkapOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap_Old
        If Not IsNothing(getKotakabtype) Then
            txtSwiftInPengirimNasabah_Korp_KotaNew.Text = getKotakabtype.NamaKotaKab
        End If
        'If Not IsNothing(getKotakabtype_old) Then
        '    txtSwiftInPengirimNasabah_Korp_KotaOld.Text = getKotakabtype_old.NamaKotaKab
        'End If
        If Not IsNothing(getnegaratype) Then
            txtSwiftInPengirimNasabah_Korp_NegaraNew.Text = getnegaratype.NamaNegara
        End If
        'If Not IsNothing(getnegaratype_old) Then
        '    txtSwiftInPengirimNasabah_Korp_NegaraOld.Text = getnegaratype_old.NamaNegara
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NegaraLainnya) = True Then
            txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NegaraLainnya
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NegaraLainnya_Old) = True Then
        '    txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NegaraLainnya_Old
        'End If
        txtSwiftInPengirimNasabah_Korp_NoTelpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NoTelp
        'txtSwiftInPengirimNasabah_Korp_NoTelpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NoTelp_Old
    End Sub
    Private Sub PengirimNonNasabah()

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DataNonSwiftIn.Sender_NonNasabah_ID_Negara.GetValueOrDefault(0)
        Dim idnegara_old As String = DataNonSwiftIn.Sender_NonNasabah_ID_Negara_Old.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '==============================================NON NASABAH==========================================='

        txtSwiftInPengirimNonNasabah_rekeningNew.Text = DataNonSwiftIn.Sender_NonNasabah_NoRekening
        txtSwiftInPengirimNonNasabah_rekeningOld.Text = DataNonSwiftIn.Sender_NonNasabah_NoRekening_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_NamaBank) = True Then
            txtSwiftInPengirimNonNasabah_namabankNew.Text = DataNonSwiftIn.Sender_NonNasabah_NamaBank
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_NamaBank_Old) = True Then
            txtSwiftInPengirimNonNasabah_namabankOld.Text = DataNonSwiftIn.Sender_NonNasabah_NamaBank_Old
        End If
        txtSwiftInPengirimNonNasabah_NamaNew.Text = DataNonSwiftIn.Sender_NonNasabah_NamaLengkap
        txtSwiftInPengirimNonNasabah_NamaOld.Text = DataNonSwiftIn.Sender_NonNasabah_NamaLengkap_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_TanggalLahir) = True Then
            txtSwiftInPengirimNonNasabah_TanggalLahirNew.Text = DataNonSwiftIn.Sender_NonNasabah_TanggalLahir
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_TanggalLahir_Old) = True Then
            txtSwiftInPengirimNonNasabah_TanggalLahirOld.Text = DataNonSwiftIn.Sender_NonNasabah_TanggalLahir_Old
        End If

        txtSwiftInPengirimNonNasabah_AlamatNew.Text = DataNonSwiftIn.Sender_NonNasabah_ID_Alamat
        txtSwiftInPengirimNonNasabah_AlamatOld.Text = DataNonSwiftIn.Sender_NonNasabah_ID_Alamat_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_NegaraBagian) = True Then
            txtSwiftInPengirimNonNasabah_KotaNew.Text = DataNonSwiftIn.Sender_NonNasabah_ID_NegaraBagian
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_NegaraBagian_Old) = True Then
            txtSwiftInPengirimNonNasabah_KotaOld.Text = DataNonSwiftIn.Sender_NonNasabah_ID_NegaraBagian_Old
        End If
        If Not IsNothing(getnegaratype) Then
            txtSwiftInPengirimNonNasabah_NegaraNew.Text = getnegaratype.NamaNegara
        End If
        If Not IsNothing(getnegaratype_old) Then
            txtSwiftInPengirimNonNasabah_NegaraOld.Text = getnegaratype_old.NamaNegara
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_NegaraLainnya) = True Then
            txtSwiftInPengirimNonNasabah_negaraLainNew.Text = DataNonSwiftIn.Sender_NonNasabah_ID_NegaraLainnya
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_NegaraLainnya_Old) = True Then
            txtSwiftInPengirimNonNasabah_negaraLainOld.Text = DataNonSwiftIn.Sender_NonNasabah_ID_NegaraLainnya_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_NoTelp) = True Then
            txtSwiftInPengirimNonNasabah_NoTelpNew.Text = DataNonSwiftIn.Sender_NonNasabah_NoTelp
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_NoTelp_Old) = True Then
            txtSwiftInPengirimNonNasabah_NoTelpOld.Text = DataNonSwiftIn.Sender_NonNasabah_NoTelp_Old
        End If
    End Sub
    Private Sub PenerimaNasabah()

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Negara.GetValueOrDefault(0)
        Dim idnegara_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_Negara.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '------------------------------------------------pekerjaan-------------------------------------'
        Dim idpekerjaan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Pekerjaan.GetValueOrDefault(0)
        Dim idpekerjaan_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_Pekerjaan.GetValueOrDefault(0)
        Dim getpekerjaantype As MsPekerjaan
        Dim getpekerjaantype_old As MsPekerjaan
        getpekerjaantype = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan)
        getpekerjaantype_old = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom.GetValueOrDefault(0)
        Dim idKotakab_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom.GetValueOrDefault(0)
        Dim idPropinsi_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '------------------------------------------------Kota kab Iden---------------------------------------------'
        Dim idKotakabiden As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden.GetValueOrDefault(0)
        Dim idKotakabiden_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden.GetValueOrDefault(0)
        Dim getKotakabidentype As MsKotaKab
        Dim getKotakabidentype_old As MsKotaKab
        getKotakabidentype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabidentype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi Iden------------------------------------------------'
        Dim idPropinsiiden As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden.GetValueOrDefault(0)
        Dim idPropinsiiden_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden.GetValueOrDefault(0)
        Dim getPropinsiidentype As MsProvince
        Dim getPropinsiidentype_old As MsProvince
        getPropinsiidentype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsiidentype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '============================================NASABAH==========================================='

        txtSwiftInPenerimaNasabah_IND_namaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NamaLengkap
        txtSwiftInPenerimaNasabah_IND_namaOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NamaLengkap
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir) = True Then
            txtSwiftInPenerimaNasabah_IND_TanggalLahirNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_TanggalLahir) = True Then
            txtSwiftInPenerimaNasabah_IND_TanggalLahirOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_TanggalLahir
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan) = True Then
            RbSwiftInPenerimaNasabah_IND_KewarganegaraanNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_KewargaNegaraan) = True Then
            RbSwiftInPenerimaNasabah_IND_KewarganegaraanOld.SelectedValue = Databenefeciary.Beneficiary_Nasabah_INDV_KewargaNegaraan
        End If
        If Not IsNothing(getnegaratype) Then
            txtSwiftInPenerimaNasabah_IND_negaraNew.Text = getnegaratype.NamaNegara
        End If
        If Not IsNothing(getnegaratype_old) Then
            txtSwiftInPenerimaNasabah_IND_negaraOld.Text = getnegaratype_old.NamaNegara
        End If
        txtSwiftInPenerimaNasabah_IND_negaralainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
        txtSwiftInPenerimaNasabah_IND_negaralainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
        If Not IsNothing(getpekerjaantype) Then
            txtSwiftInPenerimaNasabah_IND_pekerjaanNew.Text = getpekerjaantype.NamaPekerjaan
        End If
        If Not IsNothing(getpekerjaantype_old) Then
            txtSwiftInPenerimaNasabah_IND_pekerjaanOld.Text = getpekerjaantype_old.NamaPekerjaan
        End If
        TxtSwiftInIdenPenerimaNas_Ind_PekerjaanlainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_PekerjaanLainnya
        TxtSwiftInIdenPenerimaNas_Ind_PekerjaanlainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_PekerjaanLainnya
        TxtSwiftInIdenPenerimaNas_Ind_AlamatNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
        TxtSwiftInIdenPenerimaNas_Ind_AlamatOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
        If Not IsNothing(getKotakabtype) Then
            TxtSwiftInIdenPenerimaNas_Ind_KotaNew.Text = getKotakabtype.NamaKotaKab
        End If
        If Not IsNothing(getKotakabtype_old) Then
            TxtSwiftInIdenPenerimaNas_Ind_KotaOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        TxtSwiftInIdenPenerimaNas_Ind_kotalainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
        TxtSwiftInIdenPenerimaNas_Ind_kotalainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
        If Not IsNothing(getPropinsitype) Then
            TxtSwiftInIdenPenerimaNas_Ind_provinsiNew.Text = getPropinsitype.Nama
        End If
        If Not IsNothing(getPropinsitype_old) Then
            TxtSwiftInIdenPenerimaNas_Ind_provinsiOld.Text = getPropinsitype_old.Nama
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_provinsiLainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_provinsiLainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Iden) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitasNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Iden) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitasOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        End If
        If Not IsNothing(getKotakabidentype) Then
            TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitasNew.Text = getKotakabidentype.NamaKotaKab
        End If
        If Not IsNothing(getKotakabidentype_old) Then
            TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitasOld.Text = getKotakabidentype_old.NamaKotaKab
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_KotaLainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_KotaLainIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
        End If
        If Not IsNothing(getPropinsiidentype) Then
            TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIdenNew.Text = getPropinsiidentype.Nama
        End If
        If Not IsNothing(getPropinsiidentype_old) Then
            TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIdenOld.Text = getPropinsiidentype_old.Nama
        End If
        TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
        TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
        TxtISwiftIndenPenerimaNas_Ind_noTelpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoTelp
        TxtISwiftIndenPenerimaNas_Ind_noTelpOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NoTelp
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType) = True Then
            cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_FK_IFTI_IDType) = True Then
            cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.SelectedValue = Databenefeciary.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID) = True Then
            TxtISwiftIndenPenerimaNas_Ind_noIdentitasNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_NomorID) = True Then
            TxtISwiftIndenPenerimaNas_Ind_noIdentitasOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NomorID
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan) = True Then
            txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuanganNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan) = True Then
            txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuanganOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
        End If
    End Sub
    Private Sub PenerimaNasabahx()

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Negara.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)

        '------------------------------------------------pekerjaan-------------------------------------'
        Dim idpekerjaan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Pekerjaan.GetValueOrDefault(0)
        Dim getpekerjaantype As MsPekerjaan
        getpekerjaantype = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan)

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)

        '------------------------------------------------Kota kab Iden---------------------------------------------'
        Dim idKotakabiden As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden.GetValueOrDefault(0)
        Dim getKotakabidentype As MsKotaKab
        getKotakabidentype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakabiden)

        '-------------------------------------------------propinsi Iden------------------------------------------------'
        Dim idPropinsiiden As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden.GetValueOrDefault(0)
        Dim getPropinsiidentype As MsProvince
        getPropinsiidentype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsiiden)

        '============================================NASABAH==========================================='

        txtSwiftInPenerimaNasabah_IND_namaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NamaLengkap
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir) = True Then
            txtSwiftInPenerimaNasabah_IND_TanggalLahirNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan) = True Then
            RbSwiftInPenerimaNasabah_IND_KewarganegaraanNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan
        End If
        If Not IsNothing(getnegaratype) Then
            txtSwiftInPenerimaNasabah_IND_negaraNew.Text = getnegaratype.NamaNegara
        End If
        txtSwiftInPenerimaNasabah_IND_negaralainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
        If Not IsNothing(getpekerjaantype) Then
            txtSwiftInPenerimaNasabah_IND_pekerjaanNew.Text = getpekerjaantype.NamaPekerjaan
        End If
        TxtSwiftInIdenPenerimaNas_Ind_PekerjaanlainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_PekerjaanLainnya
        TxtSwiftInIdenPenerimaNas_Ind_AlamatNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
        If Not IsNothing(getKotakabtype) Then
            TxtSwiftInIdenPenerimaNas_Ind_KotaNew.Text = getKotakabtype.NamaKotaKab
        End If
        TxtSwiftInIdenPenerimaNas_Ind_kotalainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
        If Not IsNothing(getPropinsitype) Then
            TxtSwiftInIdenPenerimaNas_Ind_provinsiNew.Text = getPropinsitype.Nama
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_provinsiLainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Iden) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitasNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        End If
        If Not IsNothing(getKotakabidentype) Then
            TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitasNew.Text = getKotakabidentype.NamaKotaKab
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_KotaLainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
        End If
        If Not IsNothing(getPropinsiidentype) Then
            TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIdenNew.Text = getPropinsiidentype.Nama
        End If
        TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
        TxtISwiftIndenPenerimaNas_Ind_noTelpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoTelp
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType) = True Then
            cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID) = True Then
            TxtISwiftIndenPenerimaNas_Ind_noIdentitasNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan) = True Then
            txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuanganNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
        End If
    End Sub
    Private Sub PenerimaKorporasi()

        '--------------------------------------- Bentuk Badan Usaha ---------------------------------------'
        Dim idBentukBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim idBentukBadan_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim getBentukbadantype As MsBentukBidangUsaha
        Dim getBentukbadantype_old As MsBentukBidangUsaha
        getBentukbadantype = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentukBadan)
        getBentukbadantype_old = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentukBadan)

        '------------------------------------------ Badan Usaha -------------------------------------------'
        Dim idBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        Dim idBadan_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        Dim getbadantype As MsBidangUsaha
        Dim getbadantype_old As MsBidangUsaha
        getbadantype = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)
        getbadantype_old = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_ID_KotaKab.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Propinsi.GetValueOrDefault(0)
        Dim idPropinsi_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_ID_Propinsi.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '==================================================KORPORASI=========================================='
        If Not IsNothing(getBentukbadantype) Then
            txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaNew.Text = getBentukbadantype.BentukBidangUsaha
        End If
        If Not IsNothing(getBentukbadantype_old) Then
            txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaOld.Text = getBentukbadantype_old.BentukBidangUsaha
        End If
        txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnyaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnyaOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
            txtSwiftInPenerimaNasabah_Korp_namaKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
            txtSwiftInPenerimaNasabah_Korp_namaKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NamaKorporasi
        End If
        If Not IsNothing(getbadantype) Then
            txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpNew.Text = getbadantype.NamaBidangUsaha
        End If
        If Not IsNothing(getbadantype_old) Then
            txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpOld.Text = getbadantype_old.NamaBidangUsaha
        End If
        txtSwiftInPenerimaNasabah_Korp_BidangUsahaLainnyaKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_BidangUsahaLainnya) = True Then
            txtSwiftInPenerimaNasabah_Korp_BidangUsahaLainnyaKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        End If
        txtSwiftInPenerimaNasabah_Korp_AlamatKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_AlamatLengkap
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_AlamatLengkap) = True Then
            txtSwiftInPenerimaNasabah_Korp_AlamatKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_AlamatLengkap
        End If
        If Not IsNothing(getKotakabtype) Then
            txtSwiftInPenerimaNasabah_Korp_KotakabNew.Text = getKotakabtype.NamaKotaKab
        End If
        If Not IsNothing(getKotakabtype_old) Then
            txtSwiftInPenerimaNasabah_Korp_KotakabOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        txtSwiftInPenerimaNasabah_Korp_kotaLainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya) = True Then
            txtSwiftInPenerimaNasabah_Korp_kotaLainOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
        End If
        If Not IsNothing(getPropinsitype) Then
            txtSwiftInPenerimaNasabah_Korp_propinsiNew.Text = getPropinsitype.Nama
        End If
        If Not IsNothing(getPropinsitype_old) Then
            txtSwiftInPenerimaNasabah_Korp_propinsiOld.Text = getPropinsitype_old.Nama
        End If
        txtSwiftInPenerimaNasabah_Korp_propinsilainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya) = True Then
            txtSwiftInPenerimaNasabah_Korp_propinsilainOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
        End If
        txtSwiftInPenerimaNasabah_Korp_NoTelpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NoTelp
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_NoTelp) = True Then
            txtSwiftInPenerimaNasabah_Korp_NoTelpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NoTelp
        End If
        txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuanganNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan.GetValueOrDefault(0)
        txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuanganOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan.GetValueOrDefault(0)
    End Sub
    Private Sub PenerimaKorporasix()
        '--------------------------------------- Bentuk Badan Usaha ---------------------------------------'
        Dim idBentukBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim getBentukbadantype As MsBentukBidangUsaha
        getBentukbadantype = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentukBadan)

        '------------------------------------------ Badan Usaha -------------------------------------------'
        Dim idBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        Dim getbadantype As MsBidangUsaha
        getbadantype = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKab.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Propinsi.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)

        '==================================================KORPORASI=========================================='
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id) = True Then
            txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaNew.Text = getBentukbadantype.BentukBidangUsaha
        End If
        txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnyaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
            txtSwiftInPenerimaNasabah_Korp_namaKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi
        End If
        If Not IsNothing(getbadantype) Then
            txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpNew.Text = getbadantype.NamaBidangUsaha
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BidangUsahaLainnya) = True Then
            txtSwiftInPenerimaNasabah_Korp_BidangUsahaLainnyaKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_AlamatLengkap) = True Then
            txtSwiftInPenerimaNasabah_Korp_AlamatKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_AlamatLengkap
        End If
        txtSwiftInPenerimaNasabah_Korp_KotakabNew.Text = getKotakabtype.NamaKotaKab
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya) = True Then
            txtSwiftInPenerimaNasabah_Korp_kotaLainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
        End If
        If Not IsNothing(getPropinsitype) Then
            txtSwiftInPenerimaNasabah_Korp_propinsiNew.Text = getPropinsitype.Nama
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya) = True Then
            txtSwiftInPenerimaNasabah_Korp_propinsilainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
        End If
        txtSwiftInPenerimaNasabah_Korp_NoTelpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NoTelp
        If Not IsNothing(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan) Then
            txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuanganNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
        End If

    End Sub
    Private Sub PenerimaNonNasabah()

        '==================================================NON NASABAH=========================================='

        TxtSwiftInPenerimaNonNasabah_namaNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaLengkap
        TxtSwiftInPenerimaNonNasabah_namaOld.Text = Databenefeciary.Beneficiary_NonNasabah_NamaLengkap
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_TanggalLahir) = True Then
            TxtSwiftInPenerimaNonNasabah_TanggalLahirNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_TanggalLahir
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_TanggalLahir) = True Then
            TxtSwiftInPenerimaNonNasabah_TanggalLahirOld.Text = Databenefeciary.Beneficiary_NonNasabah_TanggalLahir
        End If
        TxtSwiftInPenerimaNonNasabah_alamatidenNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_Alamat
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_ID_Alamat) = True Then
            TxtSwiftInPenerimaNonNasabah_alamatidenOld.Text = Databenefeciary.Beneficiary_NonNasabah_ID_Alamat
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NoTelp) = True Then
            TxtSwiftInPenerimaNonNasabah_NoTeleponNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NoTelp
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_NoTelp) = True Then
            TxtSwiftInPenerimaNonNasabah_NoTeleponOld.Text = Databenefeciary.Beneficiary_NonNasabah_NoTelp
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_FK_IFTI_IDType) = True Then
            CboSwiftInPenerimaNonNasabah_JenisDokumenNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_NonNasabah_FK_IFTI_IDType
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_FK_IFTI_IDType) = True Then
            CboSwiftInPenerimaNonNasabah_JenisDokumenOld.SelectedValue = Databenefeciary.Beneficiary_NonNasabah_FK_IFTI_IDType
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NomorID) = True Then
            TxtSwiftInPenerimaNonNasabah_NomorIdenNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NomorID
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_NomorID) = True Then
            TxtSwiftInPenerimaNonNasabah_NomorIdenOld.Text = Databenefeciary.Beneficiary_NonNasabah_NomorID
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NilaiTransaksikeuangan) = True Then
            TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuanganNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NilaiTransaksikeuangan
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_NilaiTransaksikeuangan) = True Then
            TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuanganOld.Text = Databenefeciary.Beneficiary_NonNasabah_NilaiTransaksikeuangan
        End If
    End Sub
    Private Sub PenerimaNonNasabahx()

        '==================================================NON NASABAH=========================================='

        TxtSwiftInPenerimaNonNasabah_namaNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaLengkap
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_TanggalLahir) = True Then
            TxtSwiftInPenerimaNonNasabah_TanggalLahirNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_TanggalLahir
        End If
        TxtSwiftInPenerimaNonNasabah_alamatidenNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_Alamat
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NoTelp) = True Then
            TxtSwiftInPenerimaNonNasabah_NoTeleponNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NoTelp
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_FK_IFTI_IDType) = True Then
            CboSwiftInPenerimaNonNasabah_JenisDokumenNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_NonNasabah_FK_IFTI_IDType
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NomorID) = True Then
            TxtSwiftInPenerimaNonNasabah_NomorIdenNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NomorID
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NilaiTransaksikeuangan) = True Then
            TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuanganNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NilaiTransaksikeuangan
        End If
    End Sub
    Private Sub PenerusNasabah()

        '---------------------------------------------Negara-------------------------------------'
        'Dim idnegara As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Negara.GetValueOrDefault(0)
        Dim idnegara_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Negara
        'Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        'getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '------------------------------------------------pekerjaan-------------------------------------'
        'Dim idpekerjaan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Pekerjaan.GetValueOrDefault(0)
        Dim idpekerjaan_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_Pekerjaan
        'Dim getpekerjaantype As MsPekerjaan
        Dim getpekerjaantype_old As MsPekerjaan
        'getpekerjaantype = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan)
        getpekerjaantype_old = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        'Dim idKotakab As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom.GetValueOrDefault(0)
        Dim idKotakab_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom
        'Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        'getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        'Dim idPropinsi As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom.GetValueOrDefault(0)
        Dim idPropinsi_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom
        'Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        'getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '------------------------------------------------Kota kab Iden---------------------------------------------'
        'Dim idKotakabiden As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden.GetValueOrDefault(0)
        Dim idKotakabiden_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden
        'Dim getKotakabidentype As MsKotaKab
        Dim getKotakabidentype_old As MsKotaKab
        'getKotakabidentype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabidentype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakabiden_old)

        '-------------------------------------------------propinsi Iden------------------------------------------------'
        'Dim idPropinsiiden As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden.GetValueOrDefault(0)
        Dim idPropinsiiden_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden
        'Dim getPropinsiidentype As MsProvince
        Dim getPropinsiidentype_old As MsProvince
        'getPropinsiidentype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsiidentype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsiiden_old)

        '============================================NASABAH==========================================='
        'txtSwiftInPenerimaPenerus_RekeningNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoRekening
        TxtSwiftInPenerimaPenerus_RekeningOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NoRekening
        'TxtSwiftInPenerimaPenerus_Ind_namaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NamaLengkap
        TxtSwiftInPenerimaPenerus_Ind_namaOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NamaLengkap
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir) = True Then
        '    TxtSwiftInPenerimaPenerus_Ind_TanggalLahirNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_TanggalLahir) = True Then
            TxtSwiftInPenerimaPenerus_Ind_TanggalLahirOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_TanggalLahir
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan) = True Then
        '    RBSwiftInPenerimaPenerus_Ind_KewarganegaraanNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_KewargaNegaraan) = True Then
            RBSwiftInPenerimaPenerus_Ind_KewarganegaraanOld.SelectedValue = Databenefeciary.Beneficiary_Nasabah_INDV_KewargaNegaraan
        End If
        'If Not IsNothing(getnegaratype) Then
        '    TxtSwiftInPenerimaPenerus_IND_negaraNew.Text = getnegaratype.NamaNegara
        'End If
        If Not IsNothing(getnegaratype_old) Then
            TxtSwiftInPenerimaPenerus_IND_negaraOld.Text = getnegaratype_old.NamaNegara
        End If
        'TxtSwiftInPenerimaPenerus_IND_negaralainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
        TxtSwiftInPenerimaPenerus_IND_negaralainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
        'If Not IsNothing(getpekerjaantype) Then
        '    TxtSwiftInPenerimaPenerus_IND_pekerjaanNew.Text = getpekerjaantype.NamaPekerjaan
        'End If
        If Not IsNothing(getpekerjaantype_old) Then
            TxtSwiftInPenerimaPenerus_IND_pekerjaanOld.Text = getpekerjaantype_old.NamaPekerjaan
        End If
        'TxtSwiftInPenerimaPenerus_IND_pekerjaanLainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_PekerjaanLainnya
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_PekerjaanLainnya) = True Then
            TxtSwiftInPenerimaPenerus_IND_pekerjaanLainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_PekerjaanLainnya
        End If
        'TxtSwiftInPenerimaPenerus_IND_alamatDomNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Dom) = True Then
            TxtSwiftInPenerimaPenerus_IND_alamatDomOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
        End If
        'If Not IsNothing(getKotakabtype) Then
        '    TxtSwiftInPenerimaPenerus_IND_kotaDomNew.Text = getKotakabtype.NamaKotaKab
        'End If
        If Not IsNothing(getKotakabtype_old) Then
            TxtSwiftInPenerimaPenerus_IND_kotaDomOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        'TxtSwiftInPenerimaPenerus_IND_kotaLainDomNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
        TxtSwiftInPenerimaPenerus_IND_kotaLainDomOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
        'If Not IsNothing(getPropinsitype) Then
        '    TxtSwiftInPenerimaPenerus_IND_ProvDomNew.Text = getPropinsitype.Nama
        'End If
        If Not IsNothing(getPropinsitype_old) Then
            TxtSwiftInPenerimaPenerus_IND_ProvDomOld.Text = getPropinsitype_old.Nama
        End If
        'TxtSwiftInPenerimaPenerus_IND_ProvLainDomNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom) = True Then
            TxtSwiftInPenerimaPenerus_IND_ProvLainDomOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Iden) = True Then
        '    TxtSwiftInPenerimaPenerus_IND_alamatIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Iden) = True Then
            TxtSwiftInPenerimaPenerus_IND_alamatIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        End If
        'If Not IsNothing(getKotakabidentype) Then
        '    TxtSwiftInPenerimaPenerus_IND_kotaIdenNew.Text = getKotakabidentype.NamaKotaKab
        'End If
        If Not IsNothing(getKotakabidentype_old) Then
            TxtSwiftInPenerimaPenerus_IND_kotaIdenOld.Text = getKotakabidentype_old.NamaKotaKab
        End If
        'TxtSwiftInPenerimaPenerus_IND_KotaLainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
        TxtSwiftInPenerimaPenerus_IND_KotaLainIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
        'If Not IsNothing(getPropinsiidentype) Then
        '    TxtSwiftInPenerimaPenerus_IND_ProvIdenNew.Text = getPropinsiidentype.Nama
        'End If
        If Not IsNothing(getPropinsiidentype_old) Then
            TxtSwiftInPenerimaPenerus_IND_ProvIdenOld.Text = getPropinsiidentype_old.Nama
        End If
        'TxtSwiftInPenerimaPenerus_IND_ProvLainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
        TxtSwiftInPenerimaPenerus_IND_ProvLainIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
        'TxtSwiftInPenerimaPenerus_IND_NoTelpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoTelp
        TxtSwiftInPenerimaPenerus_IND_NoTelpOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NoTelp
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType) = True Then
        '    CBoSwiftInPenerimaPenerus_IND_jenisIdentitasNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_FK_IFTI_IDType) = True Then
            CBoSwiftInPenerimaPenerus_IND_jenisIdentitasOld.SelectedValue = Databenefeciary.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID) = True Then
        '    TxtSwiftInPenerimaPenerus_IND_nomoridentitasNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_NomorID) = True Then
            TxtSwiftInPenerimaPenerus_IND_nomoridentitasOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NomorID
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan) = True Then
        '    TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksiNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan) = True Then
            TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksiOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
        End If
    End Sub
    Private Sub PenerusNasabahx()

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Negara.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)

        '------------------------------------------------pekerjaan-------------------------------------'
        Dim idpekerjaan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Pekerjaan.GetValueOrDefault(0)
        Dim getpekerjaantype As MsPekerjaan
        getpekerjaantype = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan)

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)

        '------------------------------------------------Kota kab Iden---------------------------------------------'
        Dim idKotakabiden As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden.GetValueOrDefault(0)
        Dim getKotakabidentype As MsKotaKab
        getKotakabidentype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakabiden)

        '-------------------------------------------------propinsi Iden------------------------------------------------'
        Dim idPropinsiiden As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden.GetValueOrDefault(0)
        Dim getPropinsiidentype As MsProvince
        getPropinsiidentype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsiiden)

        '============================================NASABAH==========================================='
        TxtSwiftInPenerimaPenerus_RekeningNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoRekening
        TxtSwiftInPenerimaPenerus_Ind_namaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NamaLengkap
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir) = True Then
            TxtSwiftInPenerimaPenerus_Ind_TanggalLahirNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan) = True Then
            RBSwiftInPenerimaPenerus_Ind_KewarganegaraanNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan
        End If
        If Not IsNothing(getnegaratype) Then
            TxtSwiftInPenerimaPenerus_IND_negaraNew.Text = getnegaratype.NamaNegara
        End If
        TxtSwiftInPenerimaPenerus_IND_negaralainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
        If Not IsNothing(getpekerjaantype) Then
            TxtSwiftInPenerimaPenerus_IND_pekerjaanNew.Text = getpekerjaantype.NamaPekerjaan
        End If
        TxtSwiftInPenerimaPenerus_IND_pekerjaanLainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_PekerjaanLainnya
        TxtSwiftInPenerimaPenerus_IND_alamatDomNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
        If Not IsNothing(getKotakabtype) Then
            TxtSwiftInPenerimaPenerus_IND_kotaDomNew.Text = getKotakabtype.NamaKotaKab
        End If
        TxtSwiftInPenerimaPenerus_IND_kotaLainDomNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
        If Not IsNothing(getPropinsitype) Then
            TxtSwiftInPenerimaPenerus_IND_ProvDomNew.Text = getPropinsitype.Nama
        End If
        TxtSwiftInPenerimaPenerus_IND_ProvLainDomNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Iden) = True Then
            TxtSwiftInPenerimaPenerus_IND_alamatIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        End If
        If Not IsNothing(getKotakabidentype) Then
            TxtSwiftInPenerimaPenerus_IND_kotaIdenNew.Text = getKotakabidentype.NamaKotaKab
        End If
        TxtSwiftInPenerimaPenerus_IND_KotaLainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
        If Not IsNothing(getPropinsiidentype) Then
            TxtSwiftInPenerimaPenerus_IND_ProvIdenNew.Text = getPropinsiidentype.Nama
        End If
        TxtSwiftInPenerimaPenerus_IND_ProvLainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
        TxtSwiftInPenerimaPenerus_IND_NoTelpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoTelp
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType) = True Then
            CBoSwiftInPenerimaPenerus_IND_jenisIdentitasNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID) = True Then
            TxtSwiftInPenerimaPenerus_IND_nomoridentitasNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan) = True Then
            TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksiNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
        End If
    End Sub
    Private Sub PenerusKorporasi()

        '----------------------------------------Bentuk Badan Usaha----------------------------------------'
        'Dim idBentukBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim idBentukBadan_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
        'Dim getBentukbadantype As MsBentukBidangUsaha
        Dim getBentukbadantype_old As MsBentukBidangUsaha
        'getBentukbadantype = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentukBadan)
        getBentukbadantype_old = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentukBadan_old)

        '--------------------------------------------Badan Usaha----------------------------------------'
        'Dim idBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim idBadan_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
        'Dim getbadantype As MsBidangUsaha
        Dim getbadantype_old As MsBidangUsaha
        'getbadantype = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)
        getbadantype_old = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        'Dim idKotakab As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_ID_KotaKab
        'Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        'getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        'Dim idPropinsi As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Propinsi.GetValueOrDefault(0)
        Dim idPropinsi_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_ID_Propinsi
        'Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        'getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)
        '==================================================KORPORASI=========================================='
        'TxtSwiftInPenerimaPenerus_RekeningNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NoRekening
        TxtSwiftInPenerimaPenerus_RekeningOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NoRekening
        'txtSwiftInPenerimaPenerus_Korp_namabankNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaBank
        txtSwiftInPenerimaPenerus_Korp_namabankOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NamaBank
        'If Not IsNothing(getBentukbadantype) Then
        '    txtSwiftInPenerimaPenerus_Korp_bentukbadanusahaNew.Text = getBentukbadantype.BentukBidangUsaha
        'End If
        If Not IsNothing(getBentukbadantype_old) Then
            txtSwiftInPenerimaPenerus_Korp_bentukbadanusahaOld.Text = getBentukbadantype_old.BentukBidangUsaha
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya) = True Then
        '    txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya) = True Then
            txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLainOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
        '    txtSwiftInPenerimaPenerus_Korp_NamaKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
            txtSwiftInPenerimaPenerus_Korp_NamaKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NamaKorporasi
        End If
        'If Not IsNothing(getbadantype) Then
        '    txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorpNew.Text = getbadantype.NamaBidangUsaha
        'End If
        If Not IsNothing(getbadantype_old) Then
            txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorpOld.Text = getbadantype_old.NamaBidangUsaha
        End If
        'txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorplainnyaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorplainnyaOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        'txtSwiftInPenerimaPenerus_Korp_alamatkorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_AlamatLengkap
        txtSwiftInPenerimaPenerus_Korp_alamatkorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_AlamatLengkap
        'If Not IsNothing(getKotakabtype) Then
        '    txtSwiftInPenerimaPenerus_Korp_kotakorpNew.Text = getKotakabtype.NamaKotaKab
        'End If
        If Not IsNothing(getKotakabtype_old) Then
            txtSwiftInPenerimaPenerus_Korp_kotakorpOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya) = True Then
        '    txtSwiftInPenerimaPenerus_Korp_kotalainnyakorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya) = True Then
            txtSwiftInPenerimaPenerus_Korp_kotalainnyakorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
        End If
        'If Not IsNothing(getPropinsitype) Then
        '    txtSwiftInPenerimaPenerus_Korp_ProvinsiKorpNew.Text = getPropinsitype.Nama
        'End If
        If Not IsNothing(getPropinsitype_old) Then
            txtSwiftInPenerimaPenerus_Korp_ProvinsiKorpOld.Text = getPropinsitype_old.Nama
        End If
        'txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya) = True Then
            txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
        End If
        'txtSwiftInPenerimaPenerus_Korp_NoTelpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NoTelp
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_NoTelp) = True Then
            txtSwiftInPenerimaPenerus_Korp_NoTelpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NoTelp
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan) = True Then
        '    txtSwiftInPenerimaPenerus_Korp_NilaiTransaksiNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan) = True Then
            txtSwiftInPenerimaPenerus_Korp_NilaiTransaksiOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
        End If
    End Sub
    Private Sub PenerusKorporasix()

        '----------------------------------------Bentuk Badan Usaha----------------------------------------'
        Dim idBentukBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim getBentukbadantype As MsBentukBidangUsaha
        getBentukbadantype = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentukBadan)

        '--------------------------------------------Badan Usaha----------------------------------------'
        Dim idBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim getbadantype As MsBidangUsaha
        getbadantype = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKab.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Propinsi.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        '==================================================KORPORASI=========================================='
        TxtSwiftInPenerimaPenerus_RekeningNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NoRekening
        txtSwiftInPenerimaPenerus_Korp_namabankNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaBank
        If Not IsNothing(getBentukbadantype) Then
            txtSwiftInPenerimaPenerus_Korp_bentukbadanusahaNew.Text = getBentukbadantype.BentukBidangUsaha
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya) = True Then
            txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
            txtSwiftInPenerimaPenerus_Korp_NamaKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi
        End If
        If Not IsNothing(getbadantype) Then
            txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorpNew.Text = getbadantype.NamaBidangUsaha
        End If
        txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorplainnyaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        txtSwiftInPenerimaPenerus_Korp_alamatkorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_AlamatLengkap
        If Not IsNothing(getKotakabtype) Then
            txtSwiftInPenerimaPenerus_Korp_kotakorpNew.Text = getKotakabtype.NamaKotaKab
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya) = True Then
            txtSwiftInPenerimaPenerus_Korp_kotalainnyakorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
        End If
        If Not IsNothing(getPropinsitype) Then
            txtSwiftInPenerimaPenerus_Korp_ProvinsiKorpNew.Text = getPropinsitype.Nama
        End If
        txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
        txtSwiftInPenerimaPenerus_Korp_NoTelpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NoTelp
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan) = True Then
            txtSwiftInPenerimaPenerus_Korp_NilaiTransaksiNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
        End If
    End Sub
    Private Sub Transaksi()

        '+++++++++++++++++++++++++++++++++++++++++++++TRANSAKSI+++++++++++++++++++++++++++++++++++++++++++++'
        Dim idCurrent As String = DataNonSwiftIn.ValueDate_FK_Currency_ID.GetValueOrDefault(0)
        Dim idcurrent_old As String = DataNonSwiftIn.ValueDate_FK_Currency_ID_Old.GetValueOrDefault(0)
        Dim getbadantype As MsCurrency
        Dim getbadantype_old As MsCurrency
        getbadantype = DataRepository.MsCurrencyProvider.GetByIdCurrency(idCurrent)
        getbadantype_old = DataRepository.MsCurrencyProvider.GetByIdCurrency(idcurrent_old)


        Dim idCurrent1 As String = DataNonSwiftIn.Instructed_Currency.GetValueOrDefault(0)
        Dim idcurrent1_old As String = DataNonSwiftIn.Instructed_Currency_Old.GetValueOrDefault(0)
        Dim getbadantype1 As MsCurrency
        Dim getbadantype1_old As MsCurrency
        getbadantype1 = DataRepository.MsCurrencyProvider.GetByIdCurrency(idCurrent1)
        getbadantype1_old = DataRepository.MsCurrencyProvider.GetByIdCurrency(idcurrent1_old)

        Transaksi_SwiftIntanggalNew.Text = DataNonSwiftIn.TanggalTransaksi
        Transaksi_SwiftIntanggalOld.Text = DataNonSwiftIn.TanggalTransaksi_Old
        If ObjectAntiNull(DataNonSwiftIn.ValueDate_TanggalTransaksi) = True Then
            Transaksi_SwiftInwaktutransaksiNew.Text = DataNonSwiftIn.ValueDate_TanggalTransaksi
        End If
        If ObjectAntiNull(DataNonSwiftIn.ValueDate_TanggalTransaksi_Old) = True Then
            Transaksi_SwiftInwaktutransaksiOld.Text = DataNonSwiftIn.ValueDate_TanggalTransaksi_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.SenderReference) = True Then
            Transaksi_SwiftInSenderNew.Text = DataNonSwiftIn.SenderReference
        End If
        If ObjectAntiNull(DataNonSwiftIn.SenderReference_Old) = True Then
            Transaksi_SwiftInSenderOld.Text = DataNonSwiftIn.SenderReference_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.BankOperationCode) = True Then
            Transaksi_SwiftInBankOperationCodeNew.Text = DataNonSwiftIn.BankOperationCode
        End If
        If ObjectAntiNull(DataNonSwiftIn.BankOperationCode_Old) = True Then
            Transaksi_SwiftInBankOperationCodeOld.Text = DataNonSwiftIn.BankOperationCode_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.InstructionCode) = True Then
            Transaksi_SwiftInInstructionCodeNew.Text = DataNonSwiftIn.InstructionCode
        End If
        If ObjectAntiNull(DataNonSwiftIn.InstructionCode_Old) = True Then
            Transaksi_SwiftInInstructionCodeOld.Text = DataNonSwiftIn.InstructionCode_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.KantorCabangPenyelengaraPengirimAsal) = True Then
            Transaksi_SwiftInkantorCabangPengirimNew.Text = DataNonSwiftIn.KantorCabangPenyelengaraPengirimAsal
        End If
        If ObjectAntiNull(DataNonSwiftIn.KantorCabangPenyelengaraPengirimAsal_Old) = True Then
            Transaksi_SwiftInkantorCabangPengirimOld.Text = DataNonSwiftIn.KantorCabangPenyelengaraPengirimAsal_Old
        End If
        Transaksi_SwiftInkodeTipeTransaksiNew.Text = DataNonSwiftIn.TransactionCode
        Transaksi_SwiftInkodeTipeTransaksiOld.Text = DataNonSwiftIn.TransactionCode_Old
        Transaksi_SwiftInValueTanggalTransaksiNew.Text = DataNonSwiftIn.TanggalTransaksi
        Transaksi_SwiftInValueTanggalTransaksiOld.Text = DataNonSwiftIn.TanggalTransaksi_Old

        TxtNilaitransaksiNew.Text = DataNonSwiftIn.ValueDate_NilaiTransaksi.GetValueOrDefault
        TxtNilaitransaksiOld.Text = DataNonSwiftIn.ValueDate_NilaiTransaksi_Old.GetValueOrDefault
        If Not IsNothing(getbadantype) Then
            Transaksi_SwiftInMataUangTransaksiNew.Text = getbadantype.Name
        End If

        If Not IsNothing(getbadantype_old) Then
            Transaksi_SwiftInMataUangTransaksiOld.Text = getbadantype_old.Name
        End If

        Transaksi_SwiftInMataUangTransaksiLainnyaNew.Text = DataNonSwiftIn.ValueDate_CurrencyLainnya
        Transaksi_SwiftInMataUangTransaksiLainnyaOld.Text = DataNonSwiftIn.ValueDate_CurrencyLainnya_Old
        Transaksi_SwiftInAmountdalamRupiahNew.Text = DataNonSwiftIn.ValueDate_NilaiTransaksiIDR
        Transaksi_SwiftInAmountdalamRupiahOld.Text = DataNonSwiftIn.ValueDate_NilaiTransaksiIDR_Old
        If ObjectAntiNull(DataNonSwiftIn.Instructed_Currency) = True Then
            Transaksi_SwiftIncurrencyNew.Text = getStringFieldValue(getbadantype1.Code)
        End If
        If ObjectAntiNull(DataNonSwiftIn.Instructed_Currency_Old) = True Then
            Transaksi_SwiftIncurrencyOld.Text = getStringFieldValue(getbadantype1_old.Code)
        End If

        Transaksi_SwiftIncurrencyLainnyaNew.Text = DataNonSwiftIn.Instructed_CurrencyLainnya
        Transaksi_SwiftIncurrencyLainnyaOld.Text = DataNonSwiftIn.Instructed_CurrencyLainnya_Old
        Transaksi_SwiftIninstructedAmountNew.Text = DataNonSwiftIn.Instructed_Amount.GetValueOrDefault(0)
        Transaksi_SwiftIninstructedAmountOld.Text = DataNonSwiftIn.Instructed_Amount_Old.GetValueOrDefault(0)

        Transaksi_SwiftInnilaiTukarNew.Text = DataNonSwiftIn.ExchangeRate.GetValueOrDefault(0)
        Transaksi_SwiftInnilaiTukarOld.Text = DataNonSwiftIn.ExchangeRate_Old.GetValueOrDefault(0)
        Transaksi_SwiftInsendingInstitutionNew.Text = DataNonSwiftIn.SendingInstitution
        Transaksi_SwiftInsendingInstitutionOld.Text = DataNonSwiftIn.SendingInstitution_Old
        Transaksi_SwiftInTujuanTransaksiNew.Text = DataNonSwiftIn.TujuanTransaksi
        Transaksi_SwiftInTujuanTransaksiOld.Text = DataNonSwiftIn.TujuanTransaksi_Old
        Transaksi_SwiftInSumberPenggunaanDanaNew.Text = DataNonSwiftIn.SumberPenggunaanDana
        Transaksi_SwiftInSumberPenggunaanDanaOld.Text = DataNonSwiftIn.SumberPenggunaanDana_Old
    End Sub
    Private Sub Informasi()

        '++++++++++++++++++++++++++++++++++++++++++INFORMASI LAINNYA+++++++++++++++++++++++++++++++++++++++++'

        InformasiLainnya_SwiftInSenderNew.Text = DataNonSwiftIn.InformationAbout_SenderCorrespondent
        InformasiLainnya_SwiftInSenderOld.Text = DataNonSwiftIn.InformationAbout_SenderCorrespondent_Old
        InformasiLainnya_SwiftInreceiverNew.Text = DataNonSwiftIn.InformationAbout_ReceiverCorrespondent
        InformasiLainnya_SwiftInreceiverOld.Text = DataNonSwiftIn.InformationAbout_ReceiverCorrespondent_Old
        InformasiLainnya_SwiftInthirdReimbursementNew.Text = DataNonSwiftIn.InformationAbout_Thirdreimbursementinstitution
        InformasiLainnya_SwiftInthirdReimbursementOld.Text = DataNonSwiftIn.InformationAbout_Thirdreimbursementinstitution_Old
        InformasiLainnya_SwiftInintermediaryNew.Text = DataNonSwiftIn.InformationAbout_IntermediaryInstitution
        InformasiLainnya_SwiftInintermediaryOld.Text = DataNonSwiftIn.InformationAbout_IntermediaryInstitution_Old
        InformasiLainnya_SwiftInRemittanceNew.Text = DataNonSwiftIn.RemittanceInformation
        InformasiLainnya_SwiftInRemittanceOld.Text = DataNonSwiftIn.RemittanceInformation_Old
        InformasiLainnya_SwiftInSenderToReceiverNew.Text = DataNonSwiftIn.SendertoReceiverInformation
        InformasiLainnya_SwiftInSenderToReceiverOld.Text = DataNonSwiftIn.SendertoReceiverInformation_Old
        InformasiLainnya_SwiftInRegulatoryReportNew.Text = DataNonSwiftIn.RegulatoryReporting
        InformasiLainnya_SwiftInRegulatoryReportOld.Text = DataNonSwiftIn.RegulatoryReporting_Old
        InformasiLainnya_SwiftInEnvelopeContentsNew.Text = DataNonSwiftIn.EnvelopeContents
        InformasiLainnya_SwiftInEnvelopeContentsOld.Text = DataNonSwiftIn.EnvelopeContents_Old
    End Sub
    Private Sub LoadWithID()
        Me.DataPenerima.ActiveViewIndex = 0
        DataPenerima.Visible = True
        txtSwiftInPenerimaNasabah_IND_RekeningNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoRekening
        txtSwiftInPenerimaNasabah_IND_RekeningOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NoRekening

        If ObjectAntiNull(Databenefeciary.FK_IFTI_NasabahType_ID) = True Then
            Select Case Databenefeciary.FK_IFTI_NasabahType_ID.GetValueOrDefault
                Case 1
                    Me.RbSwiftInPenerimaNonNasabah_TipePenerimaOld.SelectedValue = 1
                    MultiViewSwiftInPenerimaAkhir.ActiveViewIndex = 0
                Case 2
                    Me.RbSwiftInPenerimaNonNasabah_TipePenerimaOld.SelectedValue = 1
                    MultiViewSwiftInPenerimaAkhir.ActiveViewIndex = 0
                Case 3
                    Me.RbSwiftInPenerimaNonNasabah_TipePenerimaOld.SelectedValue = 2
                    MultiViewSwiftInPenerimaAkhir.ActiveViewIndex = 1
                    PenerimaNonNasabah()
            End Select
        End If

        If ObjectAntiNull(DatabenefeciaryApproval.FK_IFTI_NasabahType_ID) = True Then
            Select Case DatabenefeciaryApproval.FK_IFTI_NasabahType_ID.GetValueOrDefault
                Case 1
                    Me.RbSwiftInPenerimaNonNasabah_TipePenerimaNew.SelectedValue = 1
                    MultiViewSwiftInPenerimaAkhir.ActiveViewIndex = 0
                Case 2
                    Me.RbSwiftInPenerimaNonNasabah_TipePenerimaNew.SelectedValue = 1
                    MultiViewSwiftInPenerimaAkhir.ActiveViewIndex = 0
                Case 3
                    Me.RbSwiftInPenerimaNonNasabah_TipePenerimaNew.SelectedValue = 2
                    MultiViewSwiftInPenerimaAkhir.ActiveViewIndex = 1
                    PenerimaNonNasabah()
            End Select
        End If

        If ObjectAntiNull(Databenefeciary.FK_IFTI_NasabahType_ID) = True Then
            Select Case Databenefeciary.FK_IFTI_NasabahType_ID.GetValueOrDefault
                Case 1
                    Me.RbSwiftInPenerimaNonNasabah_TipeNasabahOld.SelectedValue = 1
                    MultiViewPenerimaAkhirNasabah_OLD.ActiveViewIndex = 0
                    PenerimaNasabah()
                Case 2
                    Me.RbSwiftInPenerimaNonNasabah_TipeNasabahOld.SelectedValue = 2
                    MultiViewPenerimaAkhirNasabah_OLD.ActiveViewIndex = 1
                    PenerimaKorporasi()
            End Select
        End If

        If ObjectAntiNull(Databenefeciary.FK_IFTI_NasabahType_ID) = True Then
            Select Case DatabenefeciaryApproval.FK_IFTI_NasabahType_ID.GetValueOrDefault
                Case 1
                    Me.RbSwiftInPenerimaNonNasabah_TipeNasabahNew.SelectedValue = 1
                    MultiViewPenerimaAkhirNasabah_New.ActiveViewIndex = 0
                    PenerimaNasabahx()
                Case 2
                    Me.RbSwiftInPenerimaNonNasabah_TipeNasabahNew.SelectedValue = 2
                    MultiViewPenerimaAkhirNasabah_New.ActiveViewIndex = 1
                    PenerimaKorporasix()
            End Select
        End If


        '+++++++++++++++++++++++++++++++++++++++++++++C.2 IDENTITAS PENERIMA PENERUS++++++++++++++++++++++++++++++++++++++++++++'

        If ObjectAntiNull(Databenefeciary.FK_IFTI_NasabahType_ID) = True Then
            Select Case Databenefeciary.FK_IFTI_NasabahType_ID.GetValueOrDefault
                Case 4
                    Me.RbSwiftInPenerimaPenerusNonNasabah_TipePenerimaOld.SelectedValue = 1
                    MultiViewSwiftInPenerimaPenerus_OLD.ActiveViewIndex = 0
                    PenerusNasabah()
                Case 5
                    Me.RbSwiftInPenerimaPenerusNonNasabah_TipePenerimaOld.SelectedValue = 2
                    MultiViewSwiftInPenerimaPenerus_OLD.ActiveViewIndex = 1
                    PenerusKorporasi()
            End Select
        End If

        If ObjectAntiNull(Databenefeciary.FK_IFTI_NasabahType_ID) = True Then
            Select Case DatabenefeciaryApproval.FK_IFTI_NasabahType_ID.GetValueOrDefault
                Case 4
                    Me.RbSwiftInPenerimaPenerusNonNasabah_TipePenerimaNew.SelectedValue = 1
                    MultiViewSwiftInPenerimaPenerus_New.ActiveViewIndex = 0
                    PenerusNasabahx()
                Case 5
                    Me.RbSwiftInPenerimaPenerusNonNasabah_TipePenerimaNew.SelectedValue = 2
                    MultiViewSwiftInPenerimaPenerus_New.ActiveViewIndex = 1
                    PenerusKorporasix()
            End Select
        End If
    End Sub
#End Region

#Region "Function"
    Private Sub ClearThisPageSessions()

        SetnGetRowTotal = Nothing
        SetnGetSort = Nothing
        SetnGetCurrentPage = 0
    End Sub
    Sub HideControl()
        BtnReject.Visible = False
        BtnSave.Visible = False
        BtnCancel.Visible = False
    End Sub
    Private Sub LoadData()
        Dim PkObject As String
        Dim ObjIFTI_Approval As vw_IFTI_Approval = DataRepository.vw_IFTI_ApprovalProvider.GetPaged("PK_IFTI_Approval_id = " & parID, "", 0, Integer.MaxValue, 0)(0)
        With ObjIFTI_Approval
            SafeDefaultValue = "-"
            txtMsUser_StaffName.Text = .RequestedBy
            txtRequestedDate1.Text = .RequestedDate

            'other info
            Dim Omsuser As User
            Omsuser = DataRepository.UserProvider.GetBypkUserID(.RequestedBy)
            If Omsuser IsNot Nothing Then
                txtMsUser_StaffName.Text = Omsuser.UserName
            Else
                txtMsUser_StaffName.Text = SafeDefaultValue
            End If

            Omsuser = DataRepository.UserProvider.GetBypkUserID(.RequestedBy)
            If Omsuser IsNot Nothing Then
                txtMsUser_StaffName.Text = Omsuser.UserName
            Else
                txtMsUser_StaffName.Text = SafeDefaultValue
            End If
        End With
        PkObject = ObjIFTI_Approval.PK_IFTI_Approval_Id

    End Sub
#End Region

#Region "Event"

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("IFTI_Approvals.aspx")
    End Sub

    Protected Sub BtnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReject.Click
        Try
            Dim Key_ApprovalID As String = "0"
            Dim Idx As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_id = " & parID, "", 0, Integer.MaxValue, 0)(0)
            '----------------------------------------------------------------------------------------------------------------------------------------------------------------
            Dim IFTI_ApprovalDetail As IFTI_Approval_Detail
            Dim IFTI_Approval As IFTI_Approval
            Dim ifti_AppBenef As TList(Of IFTI_Approval_Beneficiary)

            'Get IFTI Approval detail
            IFTI_ApprovalDetail = DataRepository.IFTI_Approval_DetailProvider.GetPaged( _
                IFTI_Approval_DetailColumn.PK_IFTI_Approval_Detail_Id.ToString & "=" & Idx.PK_IFTI_Approval_Detail_Id.ToString, _
                "", 0, Integer.MaxValue, Nothing)(0)
            'Get IFTI Approval
            IFTI_Approval = DataRepository.IFTI_ApprovalProvider.GetPaged( _
                IFTI_ApprovalColumn.PK_IFTI_Approval_Id.ToString & "=" & Idx.FK_IFTI_Approval_Id.ToString, _
                "", 0, Integer.MaxValue, Nothing)(0)
            Key_ApprovalID = IFTI_ApprovalDetail.PK_IFTI_ID
            ' Dim fkBenef As Integer

            'get ifti app beneficiary
            ifti_AppBenef = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged( _
                IFTI_Approval_BeneficiaryColumn.FK_IFTI_Approval_Id.ToString & "=" & Idx.FK_IFTI_Approval_Id.ToString, _
                "", 0, Integer.MaxValue, Nothing)


            'Get Key IFTI
            Dim IFTIID As String = IFTI_ApprovalDetail.PK_IFTI_ID

            'delete IFTI_approval and IFTI_ApprovalDetail

            DataRepository.IFTI_Approval_DetailProvider.Delete(IFTI_ApprovalDetail)
            DataRepository.IFTI_ApprovalProvider.Delete(IFTI_Approval)
            DataRepository.IFTI_Approval_BeneficiaryProvider.Delete(ifti_AppBenef)
            'delete app beneficiary
            'Dim i As Integer = 0
            'For Each objifti_appBenef As IFTI_Approval_Beneficiary In ifti_AppBenef
            '    fkBenef = ifti_AppBenef(i).FK_IFTI_Beneficiary_ID
            'Dim deleteAppBenef As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged( _
            '               IFTI_Approval_BeneficiaryColumn.FK_IFTI_Approval_Id.ToString & "=" & Idx.FK_IFTI_Approval_Id.ToString, _
            '               "", 0, Integer.MaxValue, Nothing)(0)

            'Next




            'MtvMsUser.ActiveViewIndex = 1
            BtnCancel.Visible = False

            MultiView1.ActiveViewIndex = 1
            LblConfirmation.Text = "Rejected Success"

            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = "Rejected Success"
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        'Dim oSQLTrans As SqlTransaction = Nothing
        Dim otrans As TransactionManager = Nothing
        Try
            otrans = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
            otrans.BeginTransaction()


            Using iftiApprovalDetailAdapter As New IFTI_Approval_Detail

                Using RulesiftiApprovalDetailAdapter As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)
                    Using IFTIAdapter As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(RulesiftiApprovalDetailAdapter.PK_IFTI_ID)
                        'Bila RulesBasic_ApprovalTable.Rows.Count > 0 berarti Basic Rule tsb masih ada dlm tabel RulesBasic_Approval
                        If Not RulesiftiApprovalDetailAdapter Is Nothing Then
                            Dim IFTIApprovalTableRow As IFTI_Approval_Detail
                            IFTIApprovalTableRow = RulesiftiApprovalDetailAdapter


                            Dim Counter As Integer
                            'Using IFTIQueryAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.QueriesTableAdapter
                            'Using IFTIQueryAdapter As TList(Of IFTI) = DataRepository.IFTIProvider.GetPaged("LTDLNNO='" & SwiftInUmum_LTDNNew.Text & "'", "", 0, Integer.MaxValue, 0)
                            '    'Counter = RulesBasicQueryAdapter.Count.ToString(IFTIApprovalTableRow.KYE_RulesBasicName)
                            'End Using

                            AcceptDeleteBeneficiary()

                            'Bila counter = 0 berarti Data IFTI tsb belum ada dlm tabel IFTI, maka boleh ditambahkan
                            If Counter = 0 Then
                                With IFTIAdapter
                                    .IsDataValid = 1
                                    .FK_IFTI_Type_ID = IFTIApprovalTableRow.FK_IFTI_Type_ID
                                    .LTDLNNo = IFTIApprovalTableRow.LTDLNNo
                                    .LTDLNNoKoreksi = IFTIApprovalTableRow.LTDLNNoKoreksi
                                    .TanggalLaporan = IFTIApprovalTableRow.TanggalLaporan
                                    .NamaPJKBankPelapor = IFTIApprovalTableRow.NamaPJKBankPelapor
                                    .NamaPejabatPJKBankPelapor = IFTIApprovalTableRow.NamaPejabatPJKBankPelapor
                                    .JenisLaporan = IFTIApprovalTableRow.JenisLaporan
                                    .Sender_FK_IFTI_NasabahType_ID = IFTIApprovalTableRow.Sender_FK_IFTI_NasabahType_ID
                                    .Sender_Nasabah_INDV_NoRekening = IFTIApprovalTableRow.Sender_Nasabah_INDV_NoRekening
                                    .Sender_Nasabah_INDV_NamaLengkap = IFTIApprovalTableRow.Sender_Nasabah_INDV_NamaLengkap
                                    .Sender_Nasabah_INDV_TanggalLahir = IFTIApprovalTableRow.Sender_Nasabah_INDV_TanggalLahir
                                    .Sender_Nasabah_INDV_KewargaNegaraan = IFTIApprovalTableRow.Sender_Nasabah_INDV_KewargaNegaraan
                                    .Sender_Nasabah_INDV_Negara = IFTIApprovalTableRow.Sender_Nasabah_INDV_KewargaNegaraan
                                    .Sender_Nasabah_INDV_NegaraLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_NegaraLainnya
                                    .Sender_Nasabah_INDV_Pekerjaan = IFTIApprovalTableRow.Sender_Nasabah_INDV_Pekerjaan
                                    .Sender_Nasabah_INDV_PekerjaanLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_PekerjaanLainnya
                                    .Sender_Nasabah_INDV_DOM_Alamat = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_Alamat
                                    .Sender_Nasabah_INDV_DOM_Propinsi = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_Propinsi
                                    .Sender_Nasabah_INDV_DOM_PropinsiLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_PropinsiLainnya
                                    .Sender_Nasabah_INDV_DOM_KotaKab = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_KotaKab
                                    .Sender_Nasabah_INDV_DOM_KotaKabLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_KotaKabLainnya
                                    .Sender_Nasabah_INDV_ID_Alamat = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_Alamat
                                    .Sender_Nasabah_INDV_ID_Propinsi = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_Propinsi
                                    .Sender_Nasabah_INDV_ID_PropinsiLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_PropinsiLainnya
                                    .Sender_Nasabah_INDV_ID_KotaKab = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_KotaKab
                                    .Sender_Nasabah_INDV_ID_KotaKabLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_KotaKabLainnya
                                    .Sender_Nasabah_INDV_ID_Negara = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_Negara
                                    .Sender_Nasabah_INDV_FK_IFTI_IDType = IFTIApprovalTableRow.Sender_Nasabah_INDV_FK_IFTI_IDType
                                    .Sender_Nasabah_INDV_NomorID = IFTIApprovalTableRow.Sender_Nasabah_INDV_NomorID
                                    .Sender_Nasabah_CORP_NoRekening = IFTIApprovalTableRow.Sender_Nasabah_CORP_NoRekening
                                    .Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id = IFTIApprovalTableRow.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
                                    .Sender_Nasabah_CORP_BentukBadanUsahaLainnya = IFTIApprovalTableRow.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
                                    .Sender_Nasabah_CORP_NamaKorporasi = IFTIApprovalTableRow.Sender_Nasabah_CORP_NamaKorporasi
                                    .Sender_Nasabah_CORP_FK_MsBidangUsaha_Id = IFTIApprovalTableRow.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id
                                    .Sender_Nasabah_CORP_BidangUsahaLainnya = IFTIApprovalTableRow.Sender_Nasabah_CORP_BidangUsahaLainnya
                                    .Sender_Nasabah_CORP_AlamatLengkap = IFTIApprovalTableRow.Sender_Nasabah_CORP_AlamatLengkap
                                    .Sender_Nasabah_CORP_Propinsi = IFTIApprovalTableRow.Sender_Nasabah_CORP_Propinsi
                                    .Sender_Nasabah_CORP_PropinsiLainnya = IFTIApprovalTableRow.Sender_Nasabah_CORP_PropinsiLainnya
                                    .Sender_Nasabah_CORP_KotaKab = IFTIApprovalTableRow.Sender_Nasabah_COR_KotaKab
                                    .Sender_Nasabah_CORP_KotaKabLainnya = IFTIApprovalTableRow.Sender_Nasabah_COR_KotaKabLainnya
                                    .Sender_Nasabah_CORP_Negara = IFTIApprovalTableRow.Sender_Nasabah_CORP_Negara
                                    .Sender_Nasabah_CORP_LN_AlamatLengkap = IFTIApprovalTableRow.Sender_Nasabah_CORP_LN_AlamatLengkap
                                    .Sender_Nasabah_CORP_Alamat_KantorCabangPengirim = IFTIApprovalTableRow.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
                                    .FK_IFTI_NonNasabahNominalType_ID = IFTIApprovalTableRow.FK_IFTI_NonNasabahNominalType_ID
                                    .Sender_NonNasabah_NamaLengkap = IFTIApprovalTableRow.Sender_NonNasabah_NamaLengkap
                                    .Sender_NonNasabah_TanggalLahir = IFTIApprovalTableRow.Sender_NonNasabah_TanggalLahir
                                    .Sender_NonNasabah_ID_Alamat = IFTIApprovalTableRow.Sender_NonNasabah_ID_Alamat
                                    .Sender_NonNasabah_ID_Propinsi = IFTIApprovalTableRow.Sender_NonNasabah_ID_Propinsi
                                    .Sender_NonNasabah_ID_PropinsiLainnya = IFTIApprovalTableRow.Sender_NonNasabah_ID_PropinsiLainnya
                                    .Sender_NonNasabah_ID_KotaKab = IFTIApprovalTableRow.Sender_NonNasabah_ID_KotaKab
                                    .Sender_NonNasabah_ID_KotaKabLainnya = IFTIApprovalTableRow.Sender_NonNasabah_ID_KotaKabLainnya
                                    .Sender_NonNasabah_FK_IFTI_IDType = IFTIApprovalTableRow.Sender_NonNasabah_FK_IFTI_IDType
                                    .Sender_NonNasabah_NomorID = IFTIApprovalTableRow.Sender_NonNasabah_NomorID
                                    .FK_IFTI_BeneficialOwnerType_ID = IFTIApprovalTableRow.FK_IFTI_BeneficialOwnerType_ID
                                    .BeneficialOwner_HubunganDenganPemilikDana = IFTIApprovalTableRow.BeneficialOwner_HubunganDenganPemilikDana
                                    .BeneficialOwner_NoRekening = IFTIApprovalTableRow.BeneficialOwner_NoRekening
                                    .BeneficialOwner_NamaLengkap = IFTIApprovalTableRow.BeneficialOwner_NamaLengkap
                                    .BeneficialOwner_TanggalLahir = IFTIApprovalTableRow.BeneficialOwner_TanggalLahir
                                    .BeneficialOwner_ID_Alamat = IFTIApprovalTableRow.BeneficialOwner_ID_Alamat
                                    .BeneficialOwner_ID_Propinsi = IFTIApprovalTableRow.BeneficialOwner_ID_Propinsi
                                    .BeneficialOwner_ID_PropinsiLainnya = IFTIApprovalTableRow.BeneficialOwner_ID_PropinsiLainnya
                                    .BeneficialOwner_ID_KotaKab = IFTIApprovalTableRow.BeneficialOwner_ID_KotaKab
                                    .BeneficialOwner_ID_KotaKabLainnya = IFTIApprovalTableRow.BeneficialOwner_ID_KotaKabLainnya
                                    .BeneficialOwner_FK_IFTI_IDType = IFTIApprovalTableRow.BeneficialOwner_FK_IFTI_IDType
                                    .BeneficialOwner_NomorID = IFTIApprovalTableRow.BeneficialOwner_NomorID
                                    .TanggalTransaksi = IFTIApprovalTableRow.TanggalTransaksi
                                    .TimeIndication = IFTIApprovalTableRow.TimeIndication
                                    .SenderReference = IFTIApprovalTableRow.SenderReference
                                    .BankOperationCode = IFTIApprovalTableRow.BankOperationCode
                                    .InstructionCode = IFTIApprovalTableRow.InstructionCode
                                    .KantorCabangPenyelengaraPengirimAsal = IFTIApprovalTableRow.KantorCabangPenyelengaraPengirimAsal
                                    .TransactionCode = IFTIApprovalTableRow.TransactionCode
                                    .ValueDate_TanggalTransaksi = IFTIApprovalTableRow.ValueDate_TanggalTransaksi
                                    .ValueDate_NilaiTransaksi = IFTIApprovalTableRow.ValueDate_NilaiTransaksi
                                    .ValueDate_FK_Currency_ID = IFTIApprovalTableRow.ValueDate_FK_Currency_ID
                                    .ValueDate_NilaiTransaksiIDR = IFTIApprovalTableRow.ValueDate_NilaiTransaksiIDR
                                    .Instructed_Currency = IFTIApprovalTableRow.Instructed_Currency
                                    .Instructed_Amount = IFTIApprovalTableRow.Instructed_Amount
                                    .ExchangeRate = IFTIApprovalTableRow.ExchangeRate
                                    .SendingInstitution = IFTIApprovalTableRow.SendingInstitution
                                    '.BeneficiaryInstitution = IFTIApprovalTableRow.bene
                                    .TujuanTransaksi = IFTIApprovalTableRow.TujuanTransaksi
                                    .SumberPenggunaanDana = IFTIApprovalTableRow.SumberPenggunaanDana
                                    .InformationAbout_SenderCorrespondent = IFTIApprovalTableRow.InformationAbout_SenderCorrespondent
                                    .InformationAbout_ReceiverCorrespondent = IFTIApprovalTableRow.InformationAbout_ReceiverCorrespondent
                                    .InformationAbout_Thirdreimbursementinstitution = IFTIApprovalTableRow.InformationAbout_Thirdreimbursementinstitution
                                    .InformationAbout_IntermediaryInstitution = IFTIApprovalTableRow.InformationAbout_IntermediaryInstitution
                                    '.InformationAbout_BeneficiaryCustomerAccountInstitution = IFTIApprovalTableRow.InformationAbout_BeneficiaryCustomerAccountInstitution
                                    .RemittanceInformation = IFTIApprovalTableRow.RemittanceInformation
                                    .SendertoReceiverInformation = IFTIApprovalTableRow.SendertoReceiverInformation
                                    .RegulatoryReporting = IFTIApprovalTableRow.RegulatoryReporting
                                    .EnvelopeContents = IFTIApprovalTableRow.EnvelopeContents
                                    .LastUpdateDate = Now.ToString("yyyy-MM-dd")
                                End With
                                DataRepository.IFTIProvider.save(otrans, IFTIAdapter)

                                'delete item tersebut dalam table ifti_approval & ifti_approval_detail

                                Dim delete As IFTI_Approval = DataRepository.IFTI_ApprovalProvider.GetByPK_IFTI_Approval_Id(parID)
                                delete.PK_IFTI_Approval_Id = parID
                                DataRepository.IFTI_ApprovalProvider.Delete(otrans, delete)

                                Dim DeleteDetail As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_Id =" & parID, "", 0, Integer.MaxValue, 0)(0)
                                DeleteDetail.FK_IFTI_Approval_Id = parID
                                DataRepository.IFTI_Approval_DetailProvider.Delete(otrans, DeleteDetail)

                                'delete item error list dari ifti_errorlist
                                Dim DeleteError As TList(Of IFTI_ErrorDescription) = DataRepository.IFTI_ErrorDescriptionProvider.GetPaged("FK_IFTI_ID = " & IFTIAdapter.PK_IFTI_ID, "", 0, Integer.MaxValue, 0)

                                DataRepository.IFTI_ErrorDescriptionProvider.Delete(DeleteError)
                                GenerateListOfGeneratedIFTI(otrans)


                                otrans.Commit()
                            End If
                        End If
                    End Using
                End Using
            End Using

            MultiView1.ActiveViewIndex = 1
            LblConfirmation.Text = "Data Accepted to IFTI Table"

            'End Using
        Catch ex As Exception
            If Not otrans Is Nothing Then
                otrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not otrans Is Nothing Then
                otrans.Dispose()
                otrans = Nothing
            End If
        End Try
    End Sub


    Function GenerateListOfGeneratedIFTI(ByVal objtransaction As TransactionManager) As Boolean

        Using objCommand As SqlCommand = Commonly.GetSQLCommandStoreProcedure("usp_GenerateListOfGeneratedIfti")
            DataRepository.Provider.ExecuteNonQuery(objtransaction, objCommand)
        End Using


    End Function


    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Response.Redirect("IFTI_Approvals.aspx")
    End Sub
    Private Sub AcceptDeleteBeneficiary()
        Try
            'Dim test As Integer = Session("FK_IFTI_Beneficiary_ID")
            'Using RulesIFTIBeneveciaryApproval As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)
            '    Dim IFTIBeneveciaryApprovalTableRow As IFTI_Approval_Beneficiary
            '    IFTIBeneveciaryApprovalTableRow = RulesIFTIBeneveciaryApproval
            Dim otrans As TransactionManager = Nothing
            Try
                otrans = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
                otrans.BeginTransaction()

                For Each idx As IFTI_Approval_Beneficiary In SetnGetBindTable
                    Dim objintFK_IFTI_Beneficiary_ID As Integer = 0
                    If idx.FK_IFTI_Beneficiary_ID.ToString = "" Then
                        objintFK_IFTI_Beneficiary_ID = 0
                    Else
                        objintFK_IFTI_Beneficiary_ID = idx.FK_IFTI_Beneficiary_ID.ToString
                    End If


                    Dim countID As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID = " & objintFK_IFTI_Beneficiary_ID, "", 0, Integer.MaxValue, 0)
                    If countID.Count = 1 Then
                        Dim benefapp As IFTI_Beneficiary = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID = " & objintFK_IFTI_Beneficiary_ID, "", 0, Integer.MaxValue, 0)(0)
                        With benefapp
                            .FK_IFTI_ID = idx.FK_IFTI_ID
                            .FK_IFTI_NasabahType_ID = idx.FK_IFTI_NasabahType_ID
                            .Beneficiary_Nasabah_INDV_NoRekening = idx.Beneficiary_Nasabah_INDV_NoRekening
                            .Beneficiary_Nasabah_INDV_NamaLengkap = idx.Beneficiary_Nasabah_INDV_NamaLengkap
                            .Beneficiary_Nasabah_INDV_TanggalLahir = idx.Beneficiary_Nasabah_INDV_TanggalLahir
                            .Beneficiary_Nasabah_INDV_KewargaNegaraan = idx.Beneficiary_Nasabah_INDV_KewargaNegaraan
                            .Beneficiary_Nasabah_INDV_Pekerjaan = idx.Beneficiary_Nasabah_INDV_Pekerjaan
                            .Beneficiary_Nasabah_INDV_PekerjaanLainnya = idx.Beneficiary_Nasabah_INDV_PekerjaanLainnya
                            .Beneficiary_Nasabah_INDV_Negara = idx.Beneficiary_Nasabah_INDV_Negara
                            .Beneficiary_Nasabah_INDV_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_NegaraLainnya
                            .Beneficiary_Nasabah_INDV_ID_Alamat_Dom = idx.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
                            .Beneficiary_Nasabah_INDV_ID_KotaKab_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom
                            .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
                            .Beneficiary_Nasabah_INDV_ID_Propinsi_Dom = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom
                            .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                            .Beneficiary_Nasabah_INDV_ID_Alamat_Iden = idx.Beneficiary_Nasabah_INDV_ID_Alamat_Iden
                            .Beneficiary_Nasabah_INDV_ID_KotaKab_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden
                            .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
                            .Beneficiary_Nasabah_INDV_ID_Propinsi_Iden = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden
                            .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
                            .Beneficiary_Nasabah_INDV_ID_NegaraBagian = idx.Beneficiary_Nasabah_INDV_ID_NegaraBagian
                            .Beneficiary_Nasabah_INDV_ID_Negara = idx.Beneficiary_Nasabah_INDV_ID_Negara
                            .Beneficiary_Nasabah_INDV_ID_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
                            .Beneficiary_Nasabah_INDV_NoTelp = idx.Beneficiary_Nasabah_INDV_NoTelp
                            .Beneficiary_Nasabah_INDV_FK_IFTI_IDType = idx.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
                            .Beneficiary_Nasabah_INDV_NomorID = idx.Beneficiary_Nasabah_INDV_NomorID
                            .Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
                            .Beneficiary_Nasabah_CORP_NoRekening = idx.Beneficiary_Nasabah_CORP_NoRekening
                            .Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
                            .Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
                            .Beneficiary_Nasabah_CORP_NamaKorporasi = idx.Beneficiary_Nasabah_CORP_NamaKorporasi
                            .Beneficiary_Nasabah_CORP_NoTelp = idx.Beneficiary_Nasabah_CORP_NoTelp
                            .Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
                            .Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id
                            .Beneficiary_Nasabah_CORP_BidangUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
                            .Beneficiary_Nasabah_CORP_AlamatLengkap = idx.Beneficiary_Nasabah_CORP_AlamatLengkap
                            .Beneficiary_Nasabah_CORP_ID_KotaKab = idx.Beneficiary_Nasabah_CORP_ID_KotaKab
                            .Beneficiary_Nasabah_CORP_ID_KotaKabLainnya = idx.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
                            .Beneficiary_Nasabah_CORP_ID_Propinsi = idx.Beneficiary_Nasabah_CORP_ID_Propinsi
                            .Beneficiary_Nasabah_CORP_ID_PropinsiLainnya = idx.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
                            .Beneficiary_Nasabah_CORP_ID_NegaraBagian = idx.Beneficiary_Nasabah_CORP_ID_NegaraBagian
                            .Beneficiary_Nasabah_CORP_ID_Negara = idx.Beneficiary_Nasabah_CORP_ID_Negara
                            .Beneficiary_Nasabah_CORP_ID_NegaraLainnya = idx.Beneficiary_Nasabah_CORP_ID_NegaraLainnya
                            .Beneficiary_NonNasabah_NoRekening = idx.Beneficiary_NonNasabah_NoRekening
                            .Beneficiary_NonNasabah_KodeRahasia = idx.Beneficiary_NonNasabah_KodeRahasia
                            .Beneficiary_NonNasabah_NamaLengkap = idx.Beneficiary_NonNasabah_NamaLengkap
                            .Beneficiary_NonNasabah_NamaBank = idx.Beneficiary_NonNasabah_NamaBank
                            .Beneficiary_NonNasabah_TanggalLahir = idx.Beneficiary_NonNasabah_TanggalLahir
                            .Beneficiary_NonNasabah_NoTelp = idx.Beneficiary_NonNasabah_NoTelp
                            .Beneficiary_NonNasabah_NilaiTransaksikeuangan = idx.Beneficiary_NonNasabah_NilaiTransaksikeuangan
                            .Beneficiary_NonNasabah_ID_Alamat = idx.Beneficiary_NonNasabah_ID_Alamat
                            .Beneficiary_NonNasabah_ID_NegaraBagian = idx.Beneficiary_NonNasabah_ID_NegaraBagian
                            .Beneficiary_NonNasabah_ID_Negara = idx.Beneficiary_NonNasabah_ID_Negara
                            .Beneficiary_NonNasabah_ID_NegaraLainnya = idx.Beneficiary_NonNasabah_ID_NegaraLainnya
                            .Beneficiary_NonNasabah_FK_IFTI_IDType = idx.Beneficiary_NonNasabah_FK_IFTI_IDType
                            .Beneficiary_NonNasabah_NomorID = idx.Beneficiary_NonNasabah_NomorID
                        End With
                        DataRepository.IFTI_BeneficiaryProvider.Save(otrans, benefapp)

                    Else
                        Dim newbenef As New IFTI_Beneficiary
                        With newbenef
                            .FK_IFTI_ID = idx.FK_IFTI_ID
                            .FK_IFTI_NasabahType_ID = idx.FK_IFTI_NasabahType_ID
                            .Beneficiary_Nasabah_INDV_NoRekening = idx.Beneficiary_Nasabah_INDV_NoRekening
                            .Beneficiary_Nasabah_INDV_NamaLengkap = idx.Beneficiary_Nasabah_INDV_NamaLengkap
                            .Beneficiary_Nasabah_INDV_TanggalLahir = idx.Beneficiary_Nasabah_INDV_TanggalLahir
                            .Beneficiary_Nasabah_INDV_KewargaNegaraan = idx.Beneficiary_Nasabah_INDV_KewargaNegaraan
                            .Beneficiary_Nasabah_INDV_Pekerjaan = idx.Beneficiary_Nasabah_INDV_Pekerjaan
                            .Beneficiary_Nasabah_INDV_PekerjaanLainnya = idx.Beneficiary_Nasabah_INDV_PekerjaanLainnya
                            .Beneficiary_Nasabah_INDV_Negara = idx.Beneficiary_Nasabah_INDV_Negara
                            .Beneficiary_Nasabah_INDV_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_NegaraLainnya
                            .Beneficiary_Nasabah_INDV_ID_Alamat_Dom = idx.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
                            .Beneficiary_Nasabah_INDV_ID_KotaKab_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom
                            .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
                            .Beneficiary_Nasabah_INDV_ID_Propinsi_Dom = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom
                            .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                            .Beneficiary_Nasabah_INDV_ID_Alamat_Iden = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                            .Beneficiary_Nasabah_INDV_ID_KotaKab_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden
                            .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
                            .Beneficiary_Nasabah_INDV_ID_Propinsi_Iden = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden
                            .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
                            .Beneficiary_Nasabah_INDV_ID_NegaraBagian = idx.Beneficiary_Nasabah_INDV_ID_NegaraBagian
                            .Beneficiary_Nasabah_INDV_ID_Negara = idx.Beneficiary_Nasabah_INDV_ID_Negara
                            .Beneficiary_Nasabah_INDV_ID_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
                            .Beneficiary_Nasabah_INDV_NoTelp = idx.Beneficiary_Nasabah_INDV_NoTelp
                            .Beneficiary_Nasabah_INDV_FK_IFTI_IDType = idx.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
                            .Beneficiary_Nasabah_INDV_NomorID = idx.Beneficiary_Nasabah_INDV_NomorID
                            .Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
                            .Beneficiary_Nasabah_CORP_NoRekening = idx.Beneficiary_Nasabah_CORP_NoRekening
                            .Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
                            .Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
                            .Beneficiary_Nasabah_CORP_NamaKorporasi = idx.Beneficiary_Nasabah_CORP_NamaKorporasi
                            .Beneficiary_Nasabah_CORP_NoTelp = idx.Beneficiary_Nasabah_CORP_NoTelp
                            .Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
                            .Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id
                            .Beneficiary_Nasabah_CORP_BidangUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
                            .Beneficiary_Nasabah_CORP_AlamatLengkap = idx.Beneficiary_Nasabah_CORP_AlamatLengkap
                            .Beneficiary_Nasabah_CORP_ID_KotaKab = idx.Beneficiary_Nasabah_CORP_ID_KotaKab
                            .Beneficiary_Nasabah_CORP_ID_KotaKabLainnya = idx.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
                            .Beneficiary_Nasabah_CORP_ID_Propinsi = idx.Beneficiary_Nasabah_CORP_ID_Propinsi
                            .Beneficiary_Nasabah_CORP_ID_PropinsiLainnya = idx.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
                            .Beneficiary_Nasabah_CORP_ID_NegaraBagian = idx.Beneficiary_Nasabah_CORP_ID_NegaraBagian
                            .Beneficiary_Nasabah_CORP_ID_Negara = idx.Beneficiary_Nasabah_CORP_ID_Negara
                            .Beneficiary_Nasabah_CORP_ID_NegaraLainnya = idx.Beneficiary_Nasabah_CORP_ID_NegaraLainnya
                            .Beneficiary_NonNasabah_NoRekening = idx.Beneficiary_NonNasabah_NoRekening
                            .Beneficiary_NonNasabah_KodeRahasia = idx.Beneficiary_NonNasabah_KodeRahasia
                            .Beneficiary_NonNasabah_NamaLengkap = idx.Beneficiary_NonNasabah_NamaLengkap
                            .Beneficiary_NonNasabah_NamaBank = idx.Beneficiary_NonNasabah_NamaBank
                            .Beneficiary_NonNasabah_TanggalLahir = idx.Beneficiary_NonNasabah_TanggalLahir
                            .Beneficiary_NonNasabah_NoTelp = idx.Beneficiary_NonNasabah_NoTelp
                            .Beneficiary_NonNasabah_NilaiTransaksikeuangan = idx.Beneficiary_NonNasabah_NilaiTransaksikeuangan
                            .Beneficiary_NonNasabah_ID_Alamat = idx.Beneficiary_NonNasabah_ID_Alamat
                            .Beneficiary_NonNasabah_ID_NegaraBagian = idx.Beneficiary_NonNasabah_ID_NegaraBagian
                            .Beneficiary_NonNasabah_ID_Negara = idx.Beneficiary_NonNasabah_ID_Negara
                            .Beneficiary_NonNasabah_ID_NegaraLainnya = idx.Beneficiary_NonNasabah_ID_NegaraLainnya
                            .Beneficiary_NonNasabah_FK_IFTI_IDType = idx.Beneficiary_NonNasabah_FK_IFTI_IDType
                            .Beneficiary_NonNasabah_NomorID = idx.Beneficiary_NonNasabah_NomorID
                        End With
                        DataRepository.IFTI_BeneficiaryProvider.Save(otrans, newbenef)
                    End If
                    Dim deleteAppBenef As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("PK_IFTI_Approval_Beneficiary_ID =" & idx.PK_IFTI_Approval_Beneficiary_ID.ToString, "", 0, Integer.MaxValue, 0)(0)

                    deleteAppBenef.FK_IFTI_Approval_Id = parID
                    DataRepository.IFTI_Approval_BeneficiaryProvider.Delete(otrans, deleteAppBenef)
                Next
                otrans.Commit()
            Catch ex As Exception
                If Not otrans Is Nothing Then
                    otrans.Rollback()
                End If
                LogError(ex)
                Throw
            Finally
                If Not otrans Is Nothing Then
                    otrans.Dispose()
                    otrans = Nothing
                End If
            End Try

        Catch ex As Exception

        End Try
    End Sub
#End Region

    Private Sub databindgrid()
        GV.CurrentPageIndex = SetnGetCurrentPage
        GV.VirtualItemCount = SetnGetRowTotal
        GV.DataSource = SetnGetBindTable
        GV.DataBind()
        If SetnGetRowTotal > 0 Then
            LabelNoRecordFound.Visible = False
        Else
            LabelNoRecordFound.Visible = True
        End If
    End Sub
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            LoadIDType()
            LoadData()
            bindgrid()
            If Not Page.IsPostBack Then
                ClearThisPageSessions()
                AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GV_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GV.EditCommand
        Try




            Dim TypeId1 As Int64
            Dim TypeId2 As Int64
            TypeId1 = e.Item.Cells(15).Text
            Session("PK_IFTI_Approval_Beneficiary_ID") = TypeId1
            Session("FK_IFTI_Beneficiary_ID") = DatabenefeciaryApproval.FK_IFTI_Beneficiary_ID.ToString
            '==========================================================================================='

            If ObjectAntiNull(Databenefeciary.PJKBank_type) = True Then
                Select Case Databenefeciary.PJKBank_type.GetValueOrDefault
                    Case 1
                        Me.cboSwiftInPenerimaNonNasabah_TipePJKBankOld.SelectedValue = 1
                        MultiViewSwiftInPenerima.ActiveViewIndex = 0
                    Case 2
                        Me.cboSwiftInPenerimaNonNasabah_TipePJKBankOld.SelectedValue = 2
                        MultiViewSwiftInPenerima.ActiveViewIndex = 1
                End Select
            End If

            If ObjectAntiNull(DatabenefeciaryApproval.PJKBank_type) = True Then
                Select Case DatabenefeciaryApproval.PJKBank_type.GetValueOrDefault
                    Case 1
                        Me.cboSwiftInPenerimaNonNasabah_TipePJKBankNew.SelectedValue = 1
                    Case 2
                        Me.cboSwiftInPenerimaNonNasabah_TipePJKBankNew.SelectedValue = 2
                End Select
            End If
            '+++++++++++++++++++++++++++++++++++++++++++++C.1 IDENTITAS PENERIMA++++++++++++++++++++++++++++++++++++++++++++'
            If ObjectAntiNull(DatabenefeciaryApproval.FK_IFTI_Beneficiary_ID.ToString) = True Then
                TypeId2 = DatabenefeciaryApproval.FK_IFTI_Beneficiary_ID
            Else
                TypeId2 = ObjectAntiNull(e.Item.Cells(16).Text)
            End If


            Me.DataPenerima.ActiveViewIndex = 0
            DataPenerima.Visible = True
            Me.DataPenerima.ActiveViewIndex = 0
            Using getid As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID = " & TypeId2, "", 0, Integer.MaxValue, 0)
                If (getid.Count = 1) Then
                    LoadWithID()
                Else
                    'newRecord
                    txtSwiftInPenerimaNasabah_IND_RekeningNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoRekening

                    If ObjectAntiNull(DatabenefeciaryApproval.FK_IFTI_NasabahType_ID) = True Then
                        Select Case DatabenefeciaryApproval.FK_IFTI_NasabahType_ID.GetValueOrDefault
                            Case 1
                                Me.RbSwiftInPenerimaNonNasabah_TipePenerimaNew.SelectedValue = 1
                                MultiViewSwiftInPenerimaAkhir.ActiveViewIndex = 0
                            Case 2
                                Me.RbSwiftInPenerimaNonNasabah_TipePenerimaNew.SelectedValue = 1
                                MultiViewSwiftInPenerimaAkhir.ActiveViewIndex = 0
                            Case 3
                                Me.RbSwiftInPenerimaNonNasabah_TipePenerimaNew.SelectedValue = 2
                                MultiViewSwiftInPenerimaAkhir.ActiveViewIndex = 1
                                PenerimaNonNasabahx()
                        End Select
                    End If

                    If ObjectAntiNull(DatabenefeciaryApproval.FK_IFTI_NasabahType_ID) = True Then
                        Select Case DatabenefeciaryApproval.FK_IFTI_NasabahType_ID.GetValueOrDefault
                            Case 1
                                Me.RbSwiftInPenerimaNonNasabah_TipeNasabahNew.SelectedValue = 1
                                MultiViewPenerimaAkhirNasabah_New.ActiveViewIndex = 0
                                PenerimaNasabahx()
                            Case 2
                                Me.RbSwiftInPenerimaNonNasabah_TipeNasabahNew.SelectedValue = 2
                                MultiViewPenerimaAkhirNasabah_New.ActiveViewIndex = 1
                                PenerimaKorporasi()
                        End Select
                    End If


                    '+++++++++++++++++++++++++++++++++++++++++++++C.2 IDENTITAS PENERIMA PENERUS++++++++++++++++++++++++++++++++++++++++++++'

                    If ObjectAntiNull(DatabenefeciaryApproval.FK_IFTI_NasabahType_ID) = True Then
                        Select Case DatabenefeciaryApproval.FK_IFTI_NasabahType_ID.GetValueOrDefault
                            Case 4
                                Me.RbSwiftInPenerimaPenerusNonNasabah_TipePenerimaNew.SelectedValue = 1
                                MultiViewSwiftInPenerimaPenerus_New.ActiveViewIndex = 0
                                PenerusNasabahx()
                            Case 5
                                Me.RbSwiftInPenerimaPenerusNonNasabah_TipePenerimaNew.SelectedValue = 2
                                MultiViewSwiftInPenerimaPenerus_New.ActiveViewIndex = 1
                                PenerusKorporasix()
                        End Select
                    End If
                End If
            End Using
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub
    
    Protected Sub GV_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GV.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                If BindGridFromExcel = True Then
                    e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1) + (SetnGetCurrentPage * GetDisplayedTotalRow))
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
End Class