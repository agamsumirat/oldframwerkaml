
Partial Class ProposalSTRWorkflowSettingsApproval
    Inherits Parent

#Region " Property "
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("ProposalSTRWorkflowSettingsApprovalSelected") Is Nothing, New ArrayList, Session("ProposalSTRWorkflowSettingsApprovalSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("ProposalSTRWorkflowSettingsApprovalSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("ProposalSTRWorkflowSettingsApprovalFieldSearch") Is Nothing, "", Session("ProposalSTRWorkflowSettingsApprovalFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("ProposalSTRWorkflowSettingsApprovalFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("ProposalSTRWorkflowSettingsApprovalValueSearch") Is Nothing, "", Session("ProposalSTRWorkflowSettingsApprovalValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("ProposalSTRWorkflowSettingsApprovalValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("ProposalSTRWorkflowSettingsApprovalSort") Is Nothing, "UserId_Approval  asc", Session("ProposalSTRWorkflowSettingsApprovalSort"))
        End Get
        Set(ByVal Value As String)
            Session("ProposalSTRWorkflowSettingsApprovalSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("ProposalSTRWorkflowSettingsApprovalCurrentPage") Is Nothing, 0, Session("ProposalSTRWorkflowSettingsApprovalCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("ProposalSTRWorkflowSettingsApprovalCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("ProposalSTRWorkflowSettingsApprovalRowTotal") Is Nothing, 0, Session("ProposalSTRWorkflowSettingsApprovalRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("ProposalSTRWorkflowSettingsApprovalRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("ProposalSTRWorkflowSettingsApprovalData") Is Nothing, New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectProposalSTRWorkflowApprovalViewTableAdapter, Session("ProposalSTRWorkflowSettingsApprovalData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("ProposalSTRWorkflowSettingsApprovalData") = value
        End Set
    End Property
#End Region
    ''' <summary>
    ''' Fill Search
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Created By", "UserID_Approval Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Mode", "Mode_Approval Like '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' ClearThisPageSessions
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ClearThisPageSessions()
        Session("ProposalSTRWorkflowSettingsApprovalSelected") = Nothing
        Session("ProposalSTRWorkflowSettingsApprovalFieldSearch") = Nothing
        Session("ProposalSTRWorkflowSettingsApprovalValueSearch") = Nothing
        Session("ProposalSTRWorkflowSettingsApprovalSort") = Nothing
        Session("ProposalSTRWorkflowSettingsApprovalCurrentPage") = Nothing
        Session("ProposalSTRWorkflowSettingsApprovalRowTotal") = Nothing
        Session("ProposalSTRWorkflowSettingsApprovalData") = Nothing
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()
                Using AccessProposalSTRWorkflow As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectProposalSTRWorkflowApprovalViewTableAdapter
                    Me.SetnGetBindTable = AccessProposalSTRWorkflow.GetData(Sahassa.AML.Commonly.SessionUserId)
                End Using
                Me.FillSearch()
                Me.GridProposalSTRWorkflowApproval.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' PageNavigate
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' BindGrid
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub BindGrid()
        Try
            Dim Rows() As AMLDAL.ProposalSTRWorkflowSettings.SelectProposalSTRWorkflowApprovalViewRow = Me.SetnGetBindTable.Select(Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), Me.SetnGetSort)
            Me.GridProposalSTRWorkflowApproval.DataSource = Rows
            Me.SetnGetRowTotal = Rows.Length
            Me.GridProposalSTRWorkflowApproval.CurrentPageIndex = Me.SetnGetCurrentPage
            Me.GridProposalSTRWorkflowApproval.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridProposalSTRWorkflowApproval.DataBind()
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' SetInfoNavigate
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridProposalSTRWorkflowApproval_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridProposalSTRWorkflowApproval.EditCommand
        Try
            Using oAccessCountApproval As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.CountProposalSTRWorkflowApprovalTableAdapter
                Dim IntCountApproval As Integer = oAccessCountApproval.Fill
                If IntCountApproval > 0 Then
                    Me.Response.Redirect("ProposalSTRWorkflowSettingsApprovalDetail.aspx?Mode=" & e.Item.Cells(1).Text, False)
                Else
                    Throw New Exception("Proposal STR Workflow Settings is not available.")
                End If
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridProposalSTRWorkflowApproval_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridProposalSTRWorkflowApproval.SortCommand
        Dim GridProposalSTRWorkflowApproval As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridProposalSTRWorkflowApproval.Columns(Sahassa.AML.Commonly.IndexSort(GridProposalSTRWorkflowApproval, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try            
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
            End If
            Me.SetnGetValueSearch = Me.TextSearch.Text
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try            
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
