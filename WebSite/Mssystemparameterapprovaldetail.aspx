﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Mssystemparameterapprovaldetail.aspx.vb" Inherits="Mssystemparameterapprovaldetail" %>


<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:content id="Content1" contentplaceholderid="cpContent" runat="Server">
	<script src="script/popcalendar.js"></script>
	<script language="javascript" type="text/javascript">
	    function hidePanel(objhide, objpanel, imgmin, imgmax) {
	        document.getElementById(objhide).style.display = 'none';
	        document.getElementById(objpanel).src = imgmax;
	    }
	    // JScript File
	    //Call picker master 

	  
	</script>
	<table cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>
				<img src="Images/blank.gif" width="5" height="1" />
			</td>
			<td>
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td>
							&nbsp;</td>
						<td width="99%" bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" />
						</td>
						<td bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" />
						</td>
					</tr>
				</table>
			</td>
			<td>
				<img src="Images/blank.gif" width="5" height="1" />
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td id="tdcontent" valign="top" bgcolor="#FFFFFF">
				<div id="divcontent" class="divcontent">
					<table id="tblpenampung" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td style="width: 14px">
								<img src="Images/blank.gif" width="20" height="100%" />
							</td>
							<td class="divcontentinside" bgcolor="#FFFFFF" style="width: 100%">
								&nbsp;<ajax:AjaxPanel ID="a" runat="server" meta:resourcekey="aResource1"><%--<asp:ValidationSummary
									ID="ValidationSummaryunhandle" runat="server" CssClass="validation" meta:resourcekey="ValidationSummaryunhandleResource1" />--%><asp:ValidationSummary ID="ValidationSummaryhandle" runat="server" CssClass="validationok"
										ForeColor="Black" ValidationGroup="handle" meta:resourcekey="ValidationSummaryhandleResource1" />
								</ajax:AjaxPanel>
								<ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%" meta:resourcekey="AjaxPanel5Resource1">
									<asp:MultiView ID="MtvMsUser" runat="server" ActiveViewIndex="0">
										<asp:View ID="VwAdd" runat="server">
											<table cellspacing="0" cellpadding="0" border="0" width="100%">
												<tbody>
												<tr>
																	<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
																		border-right-style: none; border-left-style: none; border-bottom-style: none"
																		valign="top">
																		<img src="Images/dot_title.gif" width="17" height="17">
																		<asp:Label ID="LblValue" runat="server" Font-Bold="True" Font-Size="Medium" Text="Master System Parameter - Approval Detail"></asp:Label>
																		<hr />
																	</td>
																</tr>
                                                              <tr>
                                                                <td style="width: 4%" >Request Date</td>
                                                                    <td style="width: 1%">
                                                                        :</td>
                                                                  <td style="width: 18%">
                                                                      <asp:Label ID="lblRequestedDate" runat="server" ForeColor="#404040"></asp:Label>
                                                                  </td>
                                                                    <td style="width: 3%" >
                                                                        &nbsp;</td>
                                                                                                                             
                                                                
                                                                </tr>
                                                                <tr>
                                                                <td style="width: 4%">Request By</td>
                                                                    <td style="width: 1%">
                                                                        :</td>
                                                                    <td style="width: 8%">
                                                                        <asp:Label ID="lblRequestedby" runat="server" ForeColor="#404040"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                               
                                                                </tr>

                                                                <tr>
                                                                <td style="width: 18%" colspan="3">&nbsp;</td>
                                                                    <td style="width: 3%">
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                               
                                                                </tr>
                                                                   <tr>
                                                        <td id="baruNew" runat="server" colspan="3">
															<asp:Label ID="LblNewTitle" runat="server" Text="NEW" Font-Bold="True" Font-Size="Small"
																ForeColor="Blue"></asp:Label>
														</td>
                                                        <td id="lamaOld" runat="server">
															<asp:Label ID="LblOldTitle" runat="server" Text="OLD" Font-Bold="True" Font-Size="Small"
																ForeColor="Blue"></asp:Label>
														</td>
													</tr>
													<tr>
                                                        <td id="baru" runat="server" colspan="3">
															<table id="TableNew" style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
																width="100%" bgcolor="#dddddd" border="0">
																
																<tr>
																	<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
																		border-right-style: none; border-left-style: none; border-bottom-style: none;
																		height: 26px;" valign="top">
																	</td>
																</tr>
																<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
        &nbsp;
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="lblMsSystemParameter_Namenew" runat="server" text="Name"></asp:label>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td style="height: 24px" valign="top">
        <asp:label id="txtMsSystemParameter_Namenew" runat="server" maxlength="255" tabindex="2" tooltip="MsSystemParameter_Name"
            width="400px"></asp:label>
    </td>
</tr>

<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
        &nbsp;
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="lblMsSystemParameter_Valuenew" runat="server" text="Value"></asp:label>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td style="height: 24px" valign="top">
        <asp:label id="txtMsSystemParameter_Valuenew" runat="server" maxlength="255" tabindex="2" tooltip="MsSystemParameter_Value"
            width="400px"></asp:label>
    </td>
</tr>
																  <%--<tr bgcolor="#ffffff">
                                                    <td style="height: 24px">
                                                        &nbsp;
                                                    </td>
                                                    <td bgcolor="#fff7e6" style="height: 24px" valign="top">
                                                        Created Date
                                                    </td>
                                                    <td style="width: 6px; height: 24px" valign="top">
                                                        :
                                                    </td>
                                                    <td style="height: 24px" valign="top">
                                                        <asp:Label ID="lblCreatedDateNew" runat="server" ForeColor="#404040"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr bgcolor="#ffffff">
                                                    <td style="height: 24px">
                                                        &nbsp;
                                                    </td>
                                                    <td bgcolor="#fff7e6" style="height: 24px" valign="top">
                                                        Created By
                                                    </td>
                                                    <td style="width: 6px; height: 24px" valign="top">
                                                        :
                                                    </td>
                                                    <td style="height: 24px" valign="top">
                                                        <asp:Label ID="lblCreatedByNew" runat="server" ForeColor="#404040"></asp:Label>
                                                    </td>
                                                </tr>--%>
															</table>
														</td>
                                                        <td id="lama" runat="server">
														<table id="tableOld" style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
																width="100%" bgcolor="#dddddd" border="0">
																
																<tr>
																	<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
																		border-right-style: none; border-left-style: none; border-bottom-style: none;
																		height: 26px;" valign="top">
																	</td>
																</tr>
																<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
        &nbsp;
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="lblMsSystemParameter_NameOld" runat="server" text="Name"></asp:label>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td style="height: 24px" valign="top">
        <asp:label id="txtMsSystemParameter_NameOld" runat="server" maxlength="255" tabindex="2" tooltip="MsSystemParameter_Name"
            width="400px"></asp:label>
    </td>
</tr>

<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
        &nbsp;
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="lblMsSystemParameter_ValueOld" runat="server" text="Value"></asp:label>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td style="height: 24px" valign="top">
        <asp:label id="txtMsSystemParameter_ValueOld" runat="server" maxlength="255" tabindex="2" tooltip="MsSystemParameter_Value"
            width="400px"></asp:label>
    </td>
</tr>
														<%--		  <tr bgcolor="#ffffff">
                                                    <td style="height: 24px">
                                                        &nbsp;
                                                    </td>
                                                    <td bgcolor="#fff7e6" style="height: 24px" valign="top">
                                                        Created Date
                                                    </td>
                                                    <td style="width: 6px; height: 24px" valign="top">
                                                        :
                                                    </td>
                                                    <td style="height: 24px" valign="top">
                                                        <asp:Label ID="lblCreatedDateOld" runat="server" ForeColor="#404040"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr bgcolor="#ffffff">
                                                    <td style="height: 24px">
                                                        &nbsp;
                                                    </td>
                                                    <td bgcolor="#fff7e6" style="height: 24px" valign="top">
                                                        Created By
                                                    </td>
                                                    <td style="width: 6px; height: 24px" valign="top">
                                                        :
                                                    </td>
                                                    <td style="height: 24px" valign="top">
                                                        <asp:Label ID="lblCreatedByOld" runat="server" ForeColor="#404040"></asp:Label>
                                                    </td>
                                                </tr>--%>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</asp:View>
										<asp:View ID="VwConfirmation" runat="server">
											<table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
												width="100%" bgcolor="#dddddd" border="0">
												<tr bgcolor="#ffffff">
													<td colspan="2" align="center" style="height: 17px">
														<asp:Label ID="LblConfirmation" runat="server" meta:resourcekey="LblConfirmationResource1"></asp:Label>
													</td>
												</tr>
												<tr bgcolor="#ffffff">
													<td align="center" colspan="2">
														<asp:ImageButton ID="imgBtnConfirmation" runat="server" ImageUrl="~/images/button/Ok.gif"
															CausesValidation="False" meta:resourcekey="ImgBtnAddResource1" />
													</td>
												</tr>
											</table>
										</asp:View>
										<asp:View ID="RejectReason" runat="server">
											<table width="100%" style="horiz-align: center;">
												<tr>
													<td>
														<table border="0" align="center">
															<tr>
																<td class="formtext" align="left" style="height: 15px">
																	<asp:Label runat="server" ID="lblReject"></asp:Label>
																</td>
															</tr>
															<tr>
																<td>
																	<asp:TextBox ID="txtReasonReject" runat="server" Columns="100" Rows="20" TextMode="MultiLine"></asp:TextBox>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td class="formtext" align="center" style="height: 27px">
														<asp:ImageButton ID="imgOkRejectReason" runat="server" SkinID="AddButton" ImageUrl="~/Images/button/Ok.gif" />
													</td>
												</tr>
											</table>
										</asp:View>
									</asp:MultiView>
								</ajax:AjaxPanel>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
				<ajax:AjaxPanel ID="AjaxPanel1" runat="server">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td background="Images/button-bground.gif" align="left" valign="middle">
								<img src="Images/blank.gif" width="5" height="1" />
							</td>
							<td background="Images/button-bground.gif" align="left" valign="middle">
								<img src="images/arrow.gif" width="15" height="15" />
							</td>
							<td background="Images/button-bground.gif" style="width: 5px">
								&nbsp;
							</td>
							<td background="Images/button-bground.gif">
								<asp:ImageButton ID="imgApprove" runat="server" ImageUrl="~/Images/button/accept.gif" />
							</td>
							<td background="Images/button-bground.gif">
								&nbsp;<asp:ImageButton ID="imgReject" runat="server" CausesValidation="False" SkinID="RejectButton"
									ImageUrl="~/Images/button/reject.gif"></asp:ImageButton>
							</td>
							<td background="Images/button-bground.gif">
								&nbsp;<asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="BackButton"
									ImageUrl="~/Images/button/back.gif"></asp:ImageButton>
							</td>
							<td width="99%" background="Images/button-bground.gif">
								<img src="Images/blank.gif" width="1" height="1" />
								<asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" meta:resourcekey="CvalHandleErrResource1"
									ValidationGroup="handle"></asp:CustomValidator>
								<asp:CustomValidator ID="CvalPageErr" runat="server" Display="None" meta:resourcekey="CvalPageErrResource1"></asp:CustomValidator>
							</td>
							<td>
								&nbsp;</td>
						</tr>
					</table>
				</ajax:AjaxPanel>
			</td>
		</tr>
	</table>
</asp:content>

