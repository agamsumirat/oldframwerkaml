<%@ Page Language="VB" MasterPageFile="~/CDDLNPPrintMasterPage.master" AutoEventWireup="false"
    CodeFile="PopUpPotentialCustomerVerification.aspx.vb" Inherits="PopUpPotentialCustomerVerification"
    Title="PopUpPotentialCustomerVerification" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="99%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Customer Screening&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="90%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <span style="color: #ff0000">* Required</span></td>
        </tr>
    </table>

    <ajax:AjaxPanel ID="potential" runat="server" Width="99%"  >
            <asp:Panel ID="PanelCustomerScreening" runat="server" >
                <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="99%" bgColor="#dddddd"
		    border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            <tr class="formText">
                <td bgcolor="#ffffff" style="height: 24px; width: 1%;">
                    &nbsp;</td>
                <td bgcolor="#ffffff" style="height: 24px; width: 10%;" valign="top" width="20%">
                    Name / Alias</td>
                <td bgcolor="#ffffff" style="height: 24px" valign="top" width="1%">
                    :</td>
                <td bgcolor="#ffffff" style="height: 24px" width="90%">
                    <asp:TextBox ID="TxtName" runat="server" CssClass="textBox" Width="280px" 
                        MaxLength="150"></asp:TextBox>
                    <strong><span style="color: #ff0000">* </span></strong>
                    <br />
                    <br />
                    <strong><span style="color: #ff0000"></span></strong></td>
            </tr>
            <tr class="formText">
                <td bgcolor="#ffffff" style="width: 1%; height: 24px">
                </td>
                <td bgcolor="#ffffff" style="width: 10%; height: 24px" valign="top" width="20%">
                    Date Of Birth</td>
                <td bgcolor="#ffffff" style="height: 24px" valign="top" width="1%">
                    :</td>
                <td bgcolor="#ffffff" style="height: 24px" width="90%">
                    <asp:TextBox ID="TxtDOB" runat="server" CssClass="textBox" MaxLength="20" Width="152px"></asp:TextBox>
                    <strong><span style="color: #ff0000">
                        <input id="popUp" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                            border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                            border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif); width: 16px;" title="Click to show calendar"
                            type="button" />
                        </span></strong></td>
            </tr>
		    <tr class="formText">
			    <td bgColor="#ffffff" style="height: 24px"><br />
                    </td>
			    <td  bgColor="#ffffff" style="height: 24px" valign="top">
                    Nationality</td>
			    <td bgColor="#ffffff" style="height: 24px" valign="top">:</td>
			    <td bgColor="#ffffff" style="height: 24px"><asp:textbox id="TxtNationality" 
                        runat="server" CssClass="textBox" Width="280px" MaxLength="255"></asp:textbox>
                    <strong><span style="color: #ff0000"></span></strong>
                    <br />
                    <br />
                    <br />
                    <strong><span style="color: #ff0000"></span></strong></td>
		    </tr>
		    <tr class="formText" bgColor="#dddddd" height="30">
			    <td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			    <td colSpan="3" style="height: 9px">
				    <table cellSpacing="0" cellPadding="3" border="0">
					    <tr>
						    <td>
						        <asp:imagebutton id="ImageSearch" runat="server" ajaxcall="none" CausesValidation="True" 
                                    SkinID="SearchButton" ImageUrl="~/Images/button/search.gif"  ></asp:imagebutton>
						    </td>
						    <td>
						        <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
						    </td>
					    </tr>
				    </table>               

			    </td>
		    </tr>
	        </table>
        </asp:Panel>
    </ajax:AjaxPanel>

</asp:Content>
