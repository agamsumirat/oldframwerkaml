Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class UserWorkingUnitAssignmentApprovalDetail
    Inherits Parent
   


    Public Old_LastUpdateDate, New_LastUpdateDate, CreateDate As Date
#Region "Property"

    ''' <summary>
    ''' Create date
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SetAndGetCreateDate() As Date
        Get
            Return ViewState("CreateDate")
        End Get
        Set(ByVal value As Date)
            ViewState("CreateDate") = value
        End Set
    End Property
    ''' <summary>
    ''' Old Last Update date
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SetAndGetOld_LastUpdateDate() As Date
        Get
            Return ViewState("Old_LastUpdateDate")
        End Get
        Set(ByVal value As Date)
            Viewstate("Old_LastUpdateDate") = value
        End Set
    End Property
    ''' <summary>
    ''' New Last Update Date
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SetAndGetNew_LastUpdateDate() As Date
        Get
            Return ViewState("New_LastUpdateDate")
        End Get
        Set(ByVal value As Date)
            ViewState("New_LastUpdateDate") = value
        End Set
    End Property

    ''' <summary>
    ''' Get Type Parameter
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ParamType() As String
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ParamUserID() As String
        Get
            Return Me.Request.Params("UserID")
        End Get
    End Property
    ''' <summary>
    ''' Created By
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ParamCreatedUserID() As String
        Get
            Return Me.Request.Params("CreatedBy")
        End Get
    End Property

#End Region

#Region "Accept"
    ''' <summary>
    ''' Accept Add
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub AcceptUserWorkingUnitAssignment(ByVal mode As String)
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            If Me.CekIsApprovalExist(Me.LabelUserID.Text) Then
                If mode.ToLower = "edit" Then

                    'Using TransScope As New Transactions.TransactionScope
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4 * (Me.ListNewValue.Items.Count + Me.ListOldValue.Items.Count))
                    Using AccessUserWorkingUnitApproval As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignment_ApprovalTableAdapter
                        oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessUserWorkingUnitApproval, Data.IsolationLevel.ReadUncommitted)
                        Dim dt As Data.DataTable = AccessUserWorkingUnitApproval.GetAllDataByUserId(Me.ParamUserID)
                        If dt.Rows.Count > 0 Then
                            '1. delete all approval

                            For Each Row As AMLDAL.AMLDataSet.UserWorkingUnitAssignment_ApprovalRow In dt.Rows
                                ' isi yg new 
                                Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessWorkingUnit, oSQLTrans)
                                    Dim WorkingUnitName As String = AccessWorkingUnit.GetWorkingUnitNameByWorkingUnitID(CInt(Row.WorkingUnitID))
                                    Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrail, oSQLTrans)
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "UserID", "Add", Me.LabelUserID.Text, Me.LabelUserID.Text, "Accepted")
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "WorkingUnitID", "Add", "", WorkingUnitName, "Accepted")
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "CreatedDate", "Add", "", Row.CreatedDate.ToString("dd-MMMM-yyyy"), "Accepted")
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "LasUpdateDate", "Add", "", Now.ToString("dd-MMMM-yyyy"), "Accepted")
                                    End Using
                                End Using
                            Next
                        End If

                        ' isi yang lama 
                        Using AccessUserWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignmentTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessUserWorkingUnit, oSQLTrans)
                            For Each RowUWU As AMLDAL.AMLDataSet.UserWorkingUnitAssignmentRow In AccessUserWorkingUnit.GetData.Select("UserID='" & Me.ParamUserID & "'")
                                Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessWorkingUnit, oSQLTrans)
                                    Dim WorkingUnitName_Old As String = AccessWorkingUnit.GetWorkingUnitNameByWorkingUnitID(CInt(RowUWU.WorkingUnitID))
                                    Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrail, oSQLTrans)
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "UserID", "Delete", Me.LabelUserID.Text, Me.LabelUserID.Text, "Accepted")
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "WorkingUnitID", "Delete", WorkingUnitName_Old, "", "Accepted")
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "CreatedDate", "Delete", Me.LabelEntryDate.Text, Me.LabelEntryDate.Text, "Accepted")
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "LasUpdateDate", "Delete", Now.ToString("dd-MMMM-yyyy"), "", "Accepted")
                                    End Using
                                End Using
                            Next
                        End Using
                    End Using

                    Me.DeleteAllApproval()
                    '1. Insert kedalam table User Working Unit Assignment 
                    Using AccessInsertUWUA As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignmentTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessInsertUWUA, oSQLTrans)
                        Using AccessCountUWUA As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignmentTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessCountUWUA, oSQLTrans)
                            AccessInsertUWUA.DeleteUserWorkingUnitByUserID(Me.LabelUserID.Text)
                            For Each item As ListItem In Me.ListNewValue.Items
                                AccessInsertUWUA.Insert(Me.LabelUserID.Text, item.Value, CDate(Me.LabelEntryDate.Text).ToShortDateString, Today)
                            Next
                        End Using
                    End Using
                    '3. change status
                    Using AccessChangeStatus As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessChangeStatus, oSQLTrans)
                        If Me.ListNewValue.Items.Count <> 0 Then
                            AccessChangeStatus.UpdateStatusUser(Me.LabelUserID.Text, CBool(Boolean.TrueString))
                        Else
                            AccessChangeStatus.UpdateStatusUser(Me.LabelUserID.Text, CBool(Boolean.FalseString))
                        End If
                    End Using

                    oSQLTrans.Commit()

                    'End Using

                Else ' delete
                    Me.DeleteAllApproval()
                    Using AccessDeleteUserWorkingUnitAssignment As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignmentTableAdapter
                        AccessDeleteUserWorkingUnitAssignment.DeleteUserWorkingUnitByUserID(ParamUserID)
                    End Using
                    Using AccessChangeStatus As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                        If Me.ListNewValue.Items.Count <> 0 Then
                            AccessChangeStatus.UpdateStatusUser(Me.LabelUserID.Text, CBool(Boolean.TrueString))
                        Else
                            AccessChangeStatus.UpdateStatusUser(Me.LabelUserID.Text, CBool(Boolean.FalseString))
                        End If
                    End Using
                End If
            Else
                Throw New Exception("User Working unit Assignment for User ID: '" & Me.LabelUserID.Text & " is not available.")
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

#Region "Reject"
    ''' <summary>
    ''' reject Add
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub RejectUserWorkingUnitAssignment(ByVal mode As String)
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            If Me.CekIsApprovalExist(Me.LabelUserID.Text) Then
                If mode.ToLower = "edit" Then

                    'Using TransScope As New Transactions.TransactionScope
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4 * (Me.ListNewValue.Items.Count + Me.ListOldValue.Items.Count))
                    Using AccessUserWorkingUnitApproval As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignment_ApprovalTableAdapter
                        oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessUserWorkingUnitApproval, Data.IsolationLevel.ReadUncommitted)
                        Dim dt As Data.DataTable = AccessUserWorkingUnitApproval.GetAllDataByUserId(Me.ParamUserID)
                        If dt.Rows.Count > 0 Then
                            For Each Row As AMLDAL.AMLDataSet.UserWorkingUnitAssignment_ApprovalRow In dt.Rows
                                ' isi yg new 
                                Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessWorkingUnit, oSQLTrans)
                                    Dim WorkingUnitName As String = AccessWorkingUnit.GetWorkingUnitNameByWorkingUnitID(CInt(Row.WorkingUnitID))
                                    Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrail, oSQLTrans)
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "UserID", "Add", Me.LabelUserID.Text, Me.LabelUserID.Text, "Rejected")
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "WorkingUnitID", "Add", "", WorkingUnitName, "Rejected")
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "CreatedDate", "Add", "", Row.CreatedDate.ToString("dd-MMMM-yyyy"), "Rejected")
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "LasUpdateDate", "Add", "", Now.ToString("dd-MMMM-yyyy"), "Rejected")
                                    End Using
                                End Using
                            Next
                        End If

                        ' isi yang lama 
                        Using AccessUserWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignmentTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessUserWorkingUnit, oSQLTrans)
                            For Each RowUWU As AMLDAL.AMLDataSet.UserWorkingUnitAssignmentRow In AccessUserWorkingUnit.GetData.Select("UserID='" & Me.ParamUserID & "'")
                                Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessWorkingUnit, oSQLTrans)
                                    Dim WorkingUnitName_Old As String = AccessWorkingUnit.GetWorkingUnitNameByWorkingUnitID(CInt(RowUWU.WorkingUnitID))
                                    Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrail, oSQLTrans)
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "UserID", "Delete", Me.LabelUserID.Text, Me.LabelUserID.Text, "Rejected")
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "WorkingUnitID", "Delete", WorkingUnitName_Old, "", "Rejected")
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "CreatedDate", "Delete", Me.LabelEntryDate.Text, Me.LabelEntryDate.Text, "Rejected")
                                        AccessAuditTrail.Insert(Now, Me.ParamCreatedUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "LasUpdateDate", "Delete", Now.ToString("dd-MMMM-yyyy"), "", "Rejected")
                                    End Using
                                End Using
                            Next
                        End Using
                    End Using

                    ' tidak usah update, karena reject
                    ''2. change status
                    'Using AccessChangeStatus As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    '    If Me.ListNewValue.Items.Count <> 0 Then
                    '        AccessChangeStatus.UpdateStatusUser(Me.LabelUserID.Text, CBool(Boolean.TrueString))
                    '    Else
                    '        AccessChangeStatus.UpdateStatusUser(Me.LabelUserID.Text, CBool(Boolean.FalseString))
                    '    End If
                    'End Using

                    oSQLTrans.Commit()
                    'End Using
                    'terakhir  delete all approval
                    Me.DeleteAllApproval()
                Else
                    Me.DeleteAllApproval()
                    'Using AccessChangeStatus As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    '    If Me.ListNewValue.Items.Count <> 0 Then
                    '        AccessChangeStatus.UpdateStatusUser(Me.LabelUserID.Text, CBool(Boolean.TrueString))
                    '    Else
                    '        AccessChangeStatus.UpdateStatusUser(Me.LabelUserID.Text, CBool(Boolean.FalseString))
                    '    End If
                    'End Using
                End If
            Else
                Throw New Exception("User Working unit Assignment for User ID: '" & Me.LabelUserID.Text & " is not available.")
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub


    
#End Region

#Region "Load Data"

    ''' <summary>
    ''' Load 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadUserWorkingUnit(ByVal mode As String)
        Try
            Me.LabelUserID.Text = Me.ParamUserID
            Me.LabelCreatedBy.Text = Me.ParamCreatedUserID
            If mode.ToLower = "edit" Then
                Using AccessUserWorkingUnitApproval As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignment_ApprovalTableAdapter
                    Dim dt As Data.DataTable = AccessUserWorkingUnitApproval.GetAllDataByUserId(Me.ParamUserID)
                    If dt.Rows.Count > 0 Then
                        For Each Row As AMLDAL.AMLDataSet.UserWorkingUnitAssignment_ApprovalRow In dt.Rows
                            ' isi yg new 
                            Me.LabelEntryDate.Text = Row.CreatedDate.ToString("dd-MMMM-yyyy")
                            Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                                Dim WorkingUnitName As String = AccessWorkingUnit.GetWorkingUnitNameByWorkingUnitID(CInt(Row.WorkingUnitID))
                                Me.ListNewValue.Items.Add(New ListItem(WorkingUnitName, Row.WorkingUnitID))
                            End Using
                        Next
                        ' isi yang lama 
                        Using AccessUserWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignmentTableAdapter
                            For Each RowUWU As AMLDAL.AMLDataSet.UserWorkingUnitAssignmentRow In AccessUserWorkingUnit.GetData.Select("UserID='" & Me.ParamUserID & "'")
                                Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                                    Dim WorkingUnitName_Old As String = AccessWorkingUnit.GetWorkingUnitNameByWorkingUnitID(CInt(RowUWU.WorkingUnitID))
                                    Me.ListOldValue.Items.Add(New ListItem(WorkingUnitName_Old, RowUWU.WorkingUnitID))
                                End Using
                            Next
                        End Using
                    End If
                End Using
            Else ' delete
                Using AccessUserWorkingUnitApproval As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignment_ApprovalTableAdapter
                    Dim dt As Data.DataTable = AccessUserWorkingUnitApproval.GetAllDataByUserId(Me.ParamUserID)
                    If dt.Rows.Count > 0 Then
                        For Each Row As AMLDAL.AMLDataSet.UserWorkingUnitAssignment_ApprovalRow In dt.Rows
                            ' isi yg new 
                            Me.LabelEntryDate.Text = Row.CreatedDate.ToString("dd-MMMM-yyyy")                          
                        Next
                    End If
                End Using
                ' isi yang lama 
                Using AccessUserWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignmentTableAdapter
                    For Each RowUWU As AMLDAL.AMLDataSet.UserWorkingUnitAssignmentRow In AccessUserWorkingUnit.GetData.Select("UserID='" & Me.ParamUserID & "'")
                        Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                            Dim WorkingUnitName_Old As String = AccessWorkingUnit.GetWorkingUnitNameByWorkingUnitID(CInt(RowUWU.WorkingUnitID))
                            Me.ListOldValue.Items.Add(New ListItem(WorkingUnitName_Old, RowUWU.WorkingUnitID))
                        End Using
                    Next
                End Using
                    End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

#Region "Delete All Approval"
    Private Sub DeleteAllApproval()
        Try                        
            Using AccessUWUA_Approval As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignment_ApprovalTableAdapter
                Using dt As AMLDAL.AMLDataSet.UserWorkingUnitAssignment_ApprovalDataTable = AccessUWUA_Approval.GetData
                    Dim Row() As AMLDAL.AMLDataSet.UserWorkingUnitAssignment_ApprovalRow = dt.Select("UserId='" & Me.ParamUserID & "'")
                    If Row.Length > 0 Then
                        For i As Integer = 0 To Row.Length - 1
                            Using AccessParameter_Approval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                                Dim rowApproval() As AMLDAL.AMLDataSet.Parameters_ApprovalRow = AccessParameter_Approval.GetData.Select("ParameterItemApprovalID=" & Row(i).ApprovalID)

                                ' delete parameter pending
                                Using AccessUWUA_PendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                                    AccessUWUA_PendingApproval.DeleteParametersPendingApproval(CInt(rowApproval(0).Parameters_PendingApprovalID))
                                End Using

                                ' delete parameter approval
                                AccessParameter_Approval.DeleteParametersApproval(CInt(rowApproval(0).Parameters_ApprovalID))

                                ' delete userworkingunit approval
                                AccessUWUA_Approval.DeleteUserWorkingUnitAssignmentApproval(CInt(Row(i).ApprovalID))
                            End Using
                        Next
                    End If
                End Using
            End Using
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.LabelTitle.Text = "User Working Unit Assignment Approval Detail"
                Select Case Me.ParamType
                    Case 2
                        If Me.CekIsApprovalExist(Me.ParamUserID) Then
                            LoadUserWorkingUnit("Edit")
                        End If
                    Case 3
                        If Me.CekIsApprovalExist(Me.ParamUserID) Then
                            LoadUserWorkingUnit("Delete")
                        End If
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '        Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' cek is approval
    ''' </summary>
    ''' <param name="strUserId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CekIsApprovalExist(ByVal strUserId As String) As Boolean
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.CountUserWorkingUnitAssignmentApprovalByUserIdTableAdapter
            Dim count As Int32 = AccessPending.GetData(strUserId).Rows(0)("jml")
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function

    ''' <summary>
    ''' Event Accept
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            If Me.ParamType = "2" Then
                Me.AcceptUserWorkingUnitAssignment("Edit")
            Else
                Me.AcceptUserWorkingUnitAssignment("Delete")
            End If

            Sahassa.AML.Commonly.SessionIntendedPage = "UserWorkingUnitAssignmentManagementApproval.aspx"

            Me.Response.Redirect("UserWorkingUnitAssignmentManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' Event Reject
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            If Me.ParamType = "2" Then
                Me.RejectUserWorkingUnitAssignment("Edit")
            Else
                Me.RejectUserWorkingUnitAssignment("Delete")
            End If

            Sahassa.AML.Commonly.SessionIntendedPage = "UserWorkingUnitAssignmentManagementApproval.aspx"

            Me.Response.Redirect("UserWorkingUnitAssignmentManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' Event Back
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "UserWorkingUnitAssignmentManagementApproval.aspx"

        Me.Response.Redirect("UserWorkingUnitAssignmentManagementApproval.aspx", False)
    End Sub
End Class
