Partial Class DisplayAllNews
    Inherits System.Web.UI.Page

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Using NewsCount As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
                'Dim count As Int32 = NewsCount.CountNewsAtLoginPage(DateTime.Now)
                Dim count As Int32 = NewsCount.CountNewsAtLoginPage()
                'Kalau count = 0 berarti tidak ada News yang siap ditampilkan
                If count > 0 Then
                    Me.tablebasic.Visible = True
                Else
                    Me.tablebasic.Visible = False
                End If
            End Using
        Catch ex As Exception

        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
                Me.NewsRepeater.DataSource = AccessNews.GetNewsforDisplayAllNewsPage()
                Me.NewsRepeater.DataBind()
            End Using
        Catch
            Throw
        End Try
    End Sub


    Protected Sub NewsRepeater_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles NewsRepeater.ItemCommand
        If e.CommandName = "ShowMoreNews" Then
            'Ambil nilai NewsID
            Dim NewsID As Integer = Convert.ToInt32(e.CommandArgument)

            'Redirect ke halaman detail News dgn NewsID tersebut
            Me.Response.Redirect("DisplayNews.aspx?NewsID=" & NewsID, False)
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Private Sub LogError(ByVal ex As Exception)
        Dim Logger As log4net.ILog = log4net.LogManager.GetLogger("AMLError")
        Logger.Error("Exception has occured", ex)
    End Sub

    Protected Sub NewsRepeater_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles NewsRepeater.ItemDataBound

    End Sub
End Class
