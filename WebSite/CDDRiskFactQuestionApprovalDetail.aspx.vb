Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities

Partial Class CDDRiskFactQuestionApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Devina Ariyanti]	27/2/2014	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property GetPK_CDDriskfactQuestionApp_ID() As String
        Get
            Return Request.QueryString("ID")
        End Get
    End Property

    Private ReadOnly Property GetPK_CDDriskfactQuestion_ID() As Int32
        Get
            Using adapter As TList(Of CDD_RiskFactQuestion_ApprovalDetail) = DataRepository.CDD_RiskFactQuestion_ApprovalDetailProvider.GetPaged(CDD_RiskFactQuestion_ApprovalDetailColumn.FK_CDD_RiskFactQuestion_Approval_Id.ToString & " = " & GetPK_CDDriskfactQuestionApp_ID, "", 0, Integer.MaxValue, 0)

                If adapter.Count = 0 Then
                    Throw New Exception("Approval ID not exist  or already deleted")
                Else
                    Return adapter(0).PK_CDD_RiskFactQuestion_ID
                End If
            End Using
        End Get
    End Property

    Private ReadOnly Property GetCDDRiskFactQuestionNEW() As TList(Of CDD_RiskFactQuestion_ApprovalDetail)
        Get
            Session("CDDRiskFactQuestion.NEW") = DataRepository.CDD_RiskFactQuestion_ApprovalDetailProvider.GetPaged(CDD_RiskFactQuestion_ApprovalDetailColumn.FK_CDD_RiskFactQuestion_Approval_Id.ToString & " = " & GetPK_CDDriskfactQuestionApp_ID, "", 0, Integer.MaxValue, 0)
            Return Session("CDDRiskFactQuestion.NEW")
        End Get
    End Property

    Private ReadOnly Property GetCDDRiskFactQuestionOLD() As TList(Of CDD_RiskFactQuestion)
        Get
            Session("CDDRiskFactQuestion.OLD") = DataRepository.CDD_RiskFactQuestionProvider.GetPaged(CDD_RiskFactQuestionColumn.PK_CDD_RiskFactQuestion_ID.ToString & " = " & GetPK_CDDriskfactQuestion_ID, "", 0, Integer.MaxValue, 0)
            Return Session("CDDRiskFactQuestion.OLD")
        End Get
    End Property

    Private ReadOnly Property GetCDDRiskFactQuestionDetailNEW() As TList(Of CDD_RiskFactQuestionDetail_ApprovalDetail)
        Get
            Session("CDDRiskFactQuestionDetail.NEW") = DataRepository.CDD_RiskFactQuestionDetail_ApprovalDetailProvider.GetPaged(CDD_RiskFactQuestionDetail_ApprovalDetailColumn.FK_CDD_RiskFactQuestion_Approval_Id.ToString & " = " & GetPK_CDDriskfactQuestionApp_ID, "", 0, Integer.MaxValue, 0)
            Return Session("CDDRiskFactQuestionDetail.NEW")
        End Get
    End Property

    Private ReadOnly Property GetCDDRiskFactQuestionDetailOld() As TList(Of CDD_RiskFactQuestionDetail)
        Get
            Session("CDDRiskFactQuestionDetail.OLD") = DataRepository.CDD_RiskFactQuestionDetailProvider.GetPaged(CDD_RiskFactQuestionDetailColumn.FK_CDD_RiskFactQuestion_ID.ToString & " = " & GetPK_CDDriskfactQuestion_ID, "", 0, Integer.MaxValue, 0)
            Return Session("CDDRiskFactQuestionDetail.OLD")
        End Get
    End Property
#End Region

    Private Sub AbandonSession()
        Session("CDDRiskFactQuestion.NEW") = Nothing
        Session("CDDRiskFactQuestion.OLD") = Nothing
        Session("CDDRiskFactQuestionDetail.NEW") = Nothing
        Session("CDDRiskFactQuestionDetail.OLD") = Nothing
    End Sub
#Region " Load "
    Private Sub LoadDataCDDRiskFactQuestionApproval()
        Using objRiskFactQuestionApp As VList(Of vw_CDD_RiskFactQuestion_Approval) = DataRepository.vw_CDD_RiskFactQuestion_ApprovalProvider.GetPaged(vw_CDD_RiskFactQuestion_ApprovalColumn.PK_CDD_RiskFactQuestion_Approval_Id.ToString & " = " & GetPK_CDDriskfactQuestionApp_ID, "", 0, Integer.MaxValue, 0)
            If objRiskFactQuestionApp.Count > 0 Then
                lblRequestedBy.Text = objRiskFactQuestionApp(0).UserName
                lblRequestedDate.Text = objRiskFactQuestionApp(0).RequestedDate.GetValueOrDefault.ToString("dd-MMM-yyyy HH:mm")
                lblAction.Text = objRiskFactQuestionApp(0).Mode
            End If
        End Using

        If GetCDDRiskFactQuestionOLD.Count = 0 Then
            Me.lblOldRiskFact.Text = "-"
            Me.lblOldQuestion.Text = "-"
            Me.gridOldQuestionDetail.DataSource = New TList(Of CDD_QuestionDetail_ApprovalDetail)
            Me.gridOldQuestionDetail.DataBind()
        Else
            Using objRiskFact As RiskFact = DataRepository.RiskFactProvider.GetByPk_RiskFact_ID(GetCDDRiskFactQuestionOLD(0).FK_Risk_Fact_ID)
                If objRiskFact IsNot Nothing Then
                    Me.lblOldRiskFact.Text = objRiskFact.Pk_RiskFact_ID & " - " & objRiskFact.RiskFactName
                End If
            End Using
            Me.lblOldQuestion.Text = GetCDDRiskFactQuestionOLD(0).Question
            Me.gridOldQuestionDetail.DataSource = GetCDDRiskFactQuestionDetailOld
            Me.gridOldQuestionDetail.DataBind()
        End If


        If GetCDDRiskFactQuestionNEW.Count = 0 OrElse GetCDDRiskFactQuestionDetailNEW.Count = 0 Then
            Me.lblNewRiskFact.Text = "-"
            Me.lblNewQuestion.Text = "-"
            Me.gridNewQuestionDetail.DataSource = New TList(Of CDD_QuestionDetail)
            Me.gridNewQuestionDetail.DataBind()
        Else
            Using objRiskFact As RiskFact = DataRepository.RiskFactProvider.GetByPk_RiskFact_ID(GetCDDRiskFactQuestionNEW(0).FK_Risk_Fact_ID)
                If objRiskFact IsNot Nothing Then
                    Me.lblNewRiskFact.Text = objRiskFact.Pk_RiskFact_ID & " - " & objRiskFact.RiskFactName
                End If
            End Using
            Me.lblNewQuestion.Text = GetCDDRiskFactQuestionNEW(0).Question
            Me.gridNewQuestionDetail.DataSource = GetCDDRiskFactQuestionDetailNEW
            Me.gridNewQuestionDetail.DataBind()
        End If
    End Sub
#End Region

#Region " Accept "
    Private Sub AcceptDataCDDRiskFactQuestionAdd()
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim objCDDRiskFactQuestion As New CDD_RiskFactQuestion
        Dim objCDDRiskFactQuestionDetail As New TList(Of CDD_RiskFactQuestionDetail)

        Try
            objTransManager.BeginTransaction()

            If Not GetCDDRiskFactQuestionNEW Is Nothing Then
                'save cdd question
                objCDDRiskFactQuestion.Question = GetCDDRiskFactQuestionNEW(0).Question
                objCDDRiskFactQuestion.FK_Risk_Fact_ID = GetCDDRiskFactQuestionNEW(0).FK_Risk_Fact_ID
                DataRepository.CDD_RiskFactQuestionProvider.Save(objTransManager, objCDDRiskFactQuestion) 'Save Question

                'save cdd question detail
                For Each row As CDD_RiskFactQuestionDetail_ApprovalDetail In GetCDDRiskFactQuestionDetailNEW
                    Dim rowCDDRiskFactQuestionDetail As New CDD_RiskFactQuestionDetail

                    rowCDDRiskFactQuestionDetail.FK_CDD_RiskFactQuestion_ID = objCDDRiskFactQuestion.PK_CDD_RiskFactQuestion_ID
                    rowCDDRiskFactQuestionDetail.RiskFactDetail = row.RiskFactDetail
                    rowCDDRiskFactQuestionDetail.RiskFactScore = row.RiskFactScore

                    objCDDRiskFactQuestionDetail.Add(rowCDDRiskFactQuestionDetail)
                Next
                DataRepository.CDD_RiskFactQuestionDetailProvider.Save(objTransManager, objCDDRiskFactQuestionDetail) 'Save Question Detail

                'Delete Approval
                DataRepository.CDD_RiskFactQuestion_ApprovalProvider.Delete(objTransManager, GetPK_CDDriskfactQuestionApp_ID)
                DataRepository.CDD_RiskFactQuestion_ApprovalDetailProvider.Delete(objTransManager, GetCDDRiskFactQuestionNEW)
                DataRepository.CDD_RiskFactQuestionDetail_ApprovalDetailProvider.Delete(objTransManager, GetCDDRiskFactQuestionDetailNEW)
            End If

            objTransManager.Commit()
        Catch ex As Exception
            If Not objTransManager Is Nothing Then
                objTransManager.Rollback()
            End If
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub

    Private Sub AcceptDataCDDRiskFactQuestionEdit()
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim objCDDRiskFactQuestion As New CDD_RiskFactQuestion
        Dim objCDDRiskFactQuestionDetail As New TList(Of CDD_RiskFactQuestionDetail)

        Try
            objTransManager.BeginTransaction()

            If Not GetCDDRiskFactQuestionNEW Is Nothing Then
                'Save question
                objCDDRiskFactQuestion = DataRepository.CDD_RiskFactQuestionProvider.GetByPK_CDD_RiskFactQuestion_ID(objTransManager, GetPK_CDDriskfactQuestion_ID)
                objCDDRiskFactQuestion.FK_Risk_Fact_ID = GetCDDRiskFactQuestionNEW(0).FK_Risk_Fact_ID
                objCDDRiskFactQuestion.Question = GetCDDRiskFactQuestionNEW(0).Question
                DataRepository.CDD_RiskFactQuestionProvider.Save(objTransManager, objCDDRiskFactQuestion) 'Save Risk fact Question

                'Delete old question detail
                DataRepository.CDD_RiskFactQuestionDetailProvider.Delete(objTransManager, DataRepository.CDD_RiskFactQuestionDetailProvider.GetPaged(objTransManager, "FK_CDD_RiskFactQuestion_ID = " & GetPK_CDDriskfactQuestion_ID, String.Empty, 0, Integer.MaxValue, 0))

                'Save new question detail
                For Each row As CDD_RiskFactQuestionDetail_ApprovalDetail In GetCDDRiskFactQuestionDetailNEW
                    objCDDRiskFactQuestionDetail.Add(New CDD_RiskFactQuestionDetail)
                    objCDDRiskFactQuestionDetail(objCDDRiskFactQuestionDetail.Count - 1).FK_CDD_RiskFactQuestion_ID = objCDDRiskFactQuestion.PK_CDD_RiskFactQuestion_ID
                    objCDDRiskFactQuestionDetail(objCDDRiskFactQuestionDetail.Count - 1).RiskFactDetail = row.RiskFactDetail
                    objCDDRiskFactQuestionDetail(objCDDRiskFactQuestionDetail.Count - 1).RiskFactScore = row.RiskFactScore
                Next
                DataRepository.CDD_RiskFactQuestionDetailProvider.Save(objTransManager, objCDDRiskFactQuestionDetail)

                'Delete approval
                DataRepository.CDD_RiskFactQuestion_ApprovalProvider.Delete(objTransManager, GetPK_CDDriskfactQuestionApp_ID)
                DataRepository.CDD_RiskFactQuestion_ApprovalDetailProvider.Delete(objTransManager, GetCDDRiskFactQuestionNEW)
                DataRepository.CDD_RiskFactQuestionDetail_ApprovalDetailProvider.Delete(objTransManager, GetCDDRiskFactQuestionDetailNEW)
            End If

            objTransManager.Commit()
        Catch ex As Exception
            If Not objTransManager Is Nothing Then
                objTransManager.Rollback()
            End If
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub

    Private Sub AcceptDataCDDRiskFactQuestionDelete()
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Try
            objTransManager.BeginTransaction()

            If Not GetCDDRiskFactQuestionNEW Is Nothing Then
                DataRepository.CDD_RiskFactQuestionProvider.Delete(objTransManager, GetPK_CDDriskfactQuestion_ID)
                DataRepository.CDD_RiskFactQuestionDetailProvider.Delete(objTransManager, GetCDDRiskFactQuestionDetailOld)

                DataRepository.CDD_RiskFactQuestion_ApprovalProvider.Delete(objTransManager, GetPK_CDDriskfactQuestionApp_ID)
                DataRepository.CDD_RiskFactQuestion_ApprovalDetailProvider.Delete(objTransManager, GetCDDRiskFactQuestionNEW)
                DataRepository.CDD_RiskFactQuestionDetail_ApprovalDetailProvider.Delete(objTransManager, GetCDDRiskFactQuestionDetailNEW)
            End If

            objTransManager.Commit()
        Catch ex As Exception
            If Not objTransManager Is Nothing Then
                objTransManager.Rollback()
            End If
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub
#End Region

#Region " Reject "
    Private Sub RejectDataRiskFactQuestionReject()
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Try

            If Not GetCDDRiskFactQuestionNEW Is Nothing Then
                objTransManager.BeginTransaction()

                DataRepository.CDD_RiskFactQuestion_ApprovalProvider.Delete(objTransManager, GetPK_CDDriskfactQuestionApp_ID)
                DataRepository.CDD_RiskFactQuestion_ApprovalDetailProvider.Delete(objTransManager, GetCDDRiskFactQuestionNEW)
                DataRepository.CDD_RiskFactQuestionDetail_ApprovalDetailProvider.Delete(objTransManager, GetCDDRiskFactQuestionDetailNEW)

                objTransManager.Commit()
            End If
        Catch ex As Exception
            If Not objTransManager Is Nothing Then
                objTransManager.Rollback()
            End If
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")

                Using objRiskFactQuestionApp As VList(Of vw_CDD_RiskFactQuestion_Approval) = DataRepository.vw_CDD_RiskFactQuestion_ApprovalProvider.GetPaged(vw_CDD_RiskFactQuestion_ApprovalColumn.PK_CDD_RiskFactQuestion_Approval_Id.ToString & " = " & GetPK_CDDriskfactQuestionApp_ID, "", 0, Integer.MaxValue, 0)
                    If objRiskFactQuestionApp.Count > 0 Then
                        Me.LoadDataCDDRiskFactQuestionApproval()
                    Else
                        Sahassa.AML.Commonly.SessionIntendedPage = "CDDRiskFactQuestionApproval.aspx"
                        Me.Response.Redirect(Sahassa.AML.Commonly.SessionIntendedPage, False)
                    End If
                End Using

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Using objRiskFactQuestionApp As VList(Of vw_CDD_RiskFactQuestion_Approval) = DataRepository.vw_CDD_RiskFactQuestion_ApprovalProvider.GetPaged(vw_CDD_RiskFactQuestion_ApprovalColumn.PK_CDD_RiskFactQuestion_Approval_Id.ToString & " = " & GetPK_CDDriskfactQuestionApp_ID, "", 0, Integer.MaxValue, 0)
                If Not objRiskFactQuestionApp Is Nothing Then
                    Dim mode As Integer = objRiskFactQuestionApp(0).FK_MsMode_Id
                    Select Case mode
                        Case TypeConfirm.Add
                            Me.AcceptDataCDDRiskFactQuestionAdd()
                        Case TypeConfirm.Edit
                            Me.AcceptDataCDDRiskFactQuestionEdit()
                        Case TypeConfirm.Delete
                            Me.AcceptDataCDDRiskFactQuestionDelete()
                        Case Else
                            Throw New Exception("Type not supported type:" & mode)
                    End Select

                    AbandonSession()
                    Sahassa.AML.Commonly.SessionIntendedPage = "CDDLNPMessage.aspx?ID=104" & mode & "3"
                    Me.Response.Redirect(Sahassa.AML.Commonly.SessionIntendedPage, False)
                End If
            End Using
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Using objRiskFactQuestionApp As VList(Of vw_CDD_RiskFactQuestion_Approval) = DataRepository.vw_CDD_RiskFactQuestion_ApprovalProvider.GetPaged(vw_CDD_RiskFactQuestion_ApprovalColumn.PK_CDD_RiskFactQuestion_Approval_Id.ToString & " = " & GetPK_CDDriskfactQuestionApp_ID, "", 0, Integer.MaxValue, 0)
                If Not objRiskFactQuestionApp Is Nothing Then
                    Dim mode As Integer = objRiskFactQuestionApp(0).FK_MsMode_Id
                    Select Case mode
                        Case TypeConfirm.Add
                            Me.RejectDataRiskFactQuestionReject()
                        Case TypeConfirm.Edit
                            Me.RejectDataRiskFactQuestionReject()
                        Case TypeConfirm.Delete
                            Me.RejectDataRiskFactQuestionReject()
                        Case Else
                            Throw New Exception("Type not supported type:" & mode)
                    End Select

                    AbandonSession()
                    Sahassa.AML.Commonly.SessionIntendedPage = "CDDLNPMessage.aspx?ID=104" & mode & "2"
                    Me.Response.Redirect(Sahassa.AML.Commonly.SessionIntendedPage, False)
                End If
            End Using
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        AbandonSession()
        Sahassa.AML.Commonly.SessionIntendedPage = "CDDRiskFactQuestionApproval.aspx"
        Me.Response.Redirect(Sahassa.AML.Commonly.SessionIntendedPage, False)
    End Sub

    Public Enum TypeConfirm
        Add = 1
        Edit
        Delete
    End Enum
End Class