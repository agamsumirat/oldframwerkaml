Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Imports System.IO
Imports System.Collections.Generic

Partial Class MappingReportBuilderWorkingUnitApprovalDetail
    Inherits Parent

    Public ReadOnly Property PK_MappingReportBuilderWorkingUnit_Approval_ID() As Integer
        Get
            Dim strTemp As String = Request.Params("PK_MappingReportBuilderWorkingUnit_Approval_ID")
            Dim intResult As Integer
            If Integer.TryParse(strTemp, intResult) Then
                Return intResult
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Parameter PK_MappingReportBuilderWorkingUnit_Approval_ID is invalid."

            End If
        End Get
    End Property
    Public ReadOnly Property PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID() As Integer
        Get
            Return CInt(ObjMappingReportBuilderWorkingUnit_ApprovalDetail(0).PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID)
        End Get
    End Property

    Public Property ObjMappingReportBuilderWorkingUnit_Approval() As MappingReportBuilderWorkingUnit_Approval
        Get
            If Session("MappingReportBuilderWorkingUnitApprovalDetail.ObjMappingReportBuilderWorkingUnit_Approval") Is Nothing Then
                Session("MappingReportBuilderWorkingUnitApprovalDetail.ObjMappingReportBuilderWorkingUnit_Approval") = AMLBLL.MappingReportBuilderWorkingUnitBLL.GetMappingReportBuilderWorkingUnit_ApprovalByPk(Me.PK_MappingReportBuilderWorkingUnit_Approval_ID)
                Return CType(Session("MappingReportBuilderWorkingUnitApprovalDetail.ObjMappingReportBuilderWorkingUnit_Approval"), MappingReportBuilderWorkingUnit_Approval)
            Else
                Return CType(Session("MappingReportBuilderWorkingUnitApprovalDetail.ObjMappingReportBuilderWorkingUnit_Approval"), MappingReportBuilderWorkingUnit_Approval)
            End If
        End Get
        Set(value As MappingReportBuilderWorkingUnit_Approval)
            Session("MappingReportBuilderWorkingUnitApprovalDetail.ObjMappingReportBuilderWorkingUnit_Approval") = value
        End Set
    End Property


    Public ReadOnly Property ObjMappingReportBuilderWorkingUnit_ApprovalDetail() As TList(Of MappingReportBuilderWorkingUnit_ApprovalDetail)
        Get
            If Session("MappingReportBuilderWorkingUnitApprovalDetail.ObjMappingReportBuilderWorkingUnit_ApprovalDetail") Is Nothing Then

                Session("MappingReportBuilderWorkingUnitApprovalDetail.ObjMappingReportBuilderWorkingUnit_ApprovalDetail") = DataRepository.MappingReportBuilderWorkingUnit_ApprovalDetailProvider.GetPaged(MappingReportBuilderWorkingUnit_ApprovalDetailColumn.PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID.ToString & "= '" & ObjMappingReportBuilderWorkingUnit_Approval.PK_MappingReportBuilderWorkingUnit_Approval_ID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("MappingReportBuilderWorkingUnitApprovalDetail.ObjMappingReportBuilderWorkingUnit_ApprovalDetail"), TList(Of MappingReportBuilderWorkingUnit_ApprovalDetail))
        End Get
    End Property

    Public ReadOnly Property ObjWorkingUnitNew() As TList(Of WorkingUnit)
        Get
            If Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitNew") Is Nothing Then
                Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitNew") = New TList(Of WorkingUnit)
            End If
            Return CType(Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitNew"), TList(Of WorkingUnit))
        End Get
    End Property

    Public Property ObjWorkingUnitOld() As TList(Of WorkingUnit)
        Get
            If Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitOld") Is Nothing Then
                Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitOld") = New TList(Of WorkingUnit)
            End If
            Return CType(Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitOld"), TList(Of WorkingUnit))
        End Get
        Set(value As TList(Of WorkingUnit))
            Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitOld") = value
        End Set
    End Property


    Sub LoadDataAdd()
        Try
            PanelOld.Visible = False
            PanelNew.Visible = True

            LblAction.Text = "Add"
            Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitNew") = Nothing

            If ObjMappingReportBuilderWorkingUnit_ApprovalDetail.Count > 0 Then
                Using objQueryBuilder As QueryBuilder = DataRepository.QueryBuilderProvider.GetByPK_QueryBuilder_ID(CInt(ObjMappingReportBuilderWorkingUnit_ApprovalDetail(0).FK_Report_ID))
                    LblFK_Group_ID_New.Text = objQueryBuilder.QueryDescription
                End Using
                Dim iii As Integer = 0

                Dim ObjMappingreportBuilderWorkingUnit_approvalDetailmap As TList(Of mappingreportBuilderWorkingUnit_approvalDetailMap)
                ObjMappingreportBuilderWorkingUnit_approvalDetailmap = DataRepository.mappingreportBuilderWorkingUnit_approvalDetailMapProvider.GetPaged(mappingreportBuilderWorkingUnit_approvalDetailMapColumn.FK_MappingReportBuilderWorkingUnit_Approval_ID.ToString & " = " & PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID, "", 0, Integer.MaxValue, 0)

                Do While iii < ObjMappingreportBuilderWorkingUnit_approvalDetailmap.Count
                    Using ObjWorkingUnitlist As New WorkingUnit
                        Using objWorkingUnit As WorkingUnit = DataRepository.WorkingUnitProvider.GetByWorkingUnitID(CInt(ObjMappingreportBuilderWorkingUnit_approvalDetailmap(iii).FK_WorkingUnit_ID))
                            ObjWorkingUnitlist.WorkingUnitName = objWorkingUnit.WorkingUnitName
                            ObjWorkingUnitlist.WorkingUnitID = objWorkingUnit.WorkingUnitID
                        End Using
                        ObjWorkingUnitNew.Add(ObjWorkingUnitlist)
                    End Using
                    iii = iii + 1
                Loop
                gridViewNew.DataSource = ObjWorkingUnitNew
                gridViewNew.DataBind()
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingReportBuilderWorkingUnitBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub loaddataEdit()
        Try
            PanelOld.Visible = True
            PanelNew.Visible = True

            LblAction.Text = "Edit"
            Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitNew") = Nothing
            Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitOld") = Nothing

            If ObjMappingReportBuilderWorkingUnit_ApprovalDetail.Count > 0 Then
                Using objQueryBuilder As QueryBuilder = DataRepository.QueryBuilderProvider.GetByPK_QueryBuilder_ID(CInt(ObjMappingReportBuilderWorkingUnit_ApprovalDetail(0).FK_Report_ID))
                    LblFK_Group_ID_New.Text = objQueryBuilder.QueryDescription

                    Dim iii As Integer = 0

                    Dim ObjMappingreportBuilderWorkingUnit_approvalDetailmap As TList(Of mappingreportBuilderWorkingUnit_approvalDetailMap)
                    ObjMappingreportBuilderWorkingUnit_approvalDetailmap = DataRepository.mappingreportBuilderWorkingUnit_approvalDetailMapProvider.GetPaged(mappingreportBuilderWorkingUnit_approvalDetailMapColumn.FK_MappingReportBuilderWorkingUnit_Approval_ID.ToString & " = " & PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID, "", 0, Integer.MaxValue, 0)

                    Do While iii < ObjMappingreportBuilderWorkingUnit_approvalDetailmap.Count
                        Using ObjWorkingUnitlist As New WorkingUnit
                            Using objWorkingUnit As WorkingUnit = DataRepository.WorkingUnitProvider.GetByWorkingUnitID(CInt(ObjMappingreportBuilderWorkingUnit_approvalDetailmap(iii).FK_WorkingUnit_ID))
                                ObjWorkingUnitlist.WorkingUnitName = objWorkingUnit.WorkingUnitName
                                ObjWorkingUnitlist.WorkingUnitID = objWorkingUnit.WorkingUnitID
                            End Using
                            ObjWorkingUnitNew.Add(ObjWorkingUnitlist)
                        End Using
                        iii = iii + 1
                    Loop
                    gridViewNew.DataSource = ObjWorkingUnitNew
                    gridViewNew.DataBind()
                End Using



                Using objQueryBuilder As QueryBuilder = DataRepository.QueryBuilderProvider.GetByPK_QueryBuilder_ID(CInt(ObjMappingReportBuilderWorkingUnit_ApprovalDetail(0).FK_Report_ID))
                    LblFK_Group_ID_Old.Text = objQueryBuilder.QueryDescription

                    ObjWorkingUnitOld = DataRepository.WorkingUnitProvider.GetPaged("WorkingUnitID IN ( SELECT mbwu.FK_WorkingUnit_ID FROM mappingreportBuilderWorkingUnitmap mbwu WHERE mbwu.FK_MappingReportBuilderWorkingUnit_ID=" & ObjMappingReportBuilderWorkingUnit_ApprovalDetail(0).PK_MappingReportBuilderWorkingUnit_ID & "	)", "", 0, Integer.MaxValue, 0)

                    gridViewOLD.DataSource = ObjWorkingUnitOld
                    gridViewOLD.DataBind()
                End Using
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingReportBuilderWorkingUnitBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Sub LoadDataDelete()
        Try
            PanelOld.Visible = False
            PanelNew.Visible = True

            LblAction.Text = "Delete"
            Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitNew") = Nothing

            If ObjMappingReportBuilderWorkingUnit_ApprovalDetail.Count > 0 Then
                Using objQueryBuilder As QueryBuilder = DataRepository.QueryBuilderProvider.GetByPK_QueryBuilder_ID(CInt(ObjMappingReportBuilderWorkingUnit_ApprovalDetail(0).FK_Report_ID))
                    LblFK_Group_ID_New.Text = objQueryBuilder.QueryDescription

                    Dim iii As Integer = 0

                    Dim ObjMappingreportBuilderWorkingUnit_approvalDetailmap As TList(Of mappingreportBuilderWorkingUnit_approvalDetailMap)
                    ObjMappingreportBuilderWorkingUnit_approvalDetailmap = DataRepository.mappingreportBuilderWorkingUnit_approvalDetailMapProvider.GetPaged(mappingreportBuilderWorkingUnit_approvalDetailMapColumn.FK_MappingReportBuilderWorkingUnit_Approval_ID.ToString & " = " & PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID, "", 0, Integer.MaxValue, 0)

                    Do While iii < ObjMappingreportBuilderWorkingUnit_approvalDetailmap.Count
                        Using ObjWorkingUnitlist As New WorkingUnit
                            Using objWorkingUnit As WorkingUnit = DataRepository.WorkingUnitProvider.GetByWorkingUnitID(CInt(ObjMappingreportBuilderWorkingUnit_approvalDetailmap(iii).FK_WorkingUnit_ID))
                                ObjWorkingUnitlist.WorkingUnitName = objWorkingUnit.WorkingUnitName
                                ObjWorkingUnitlist.WorkingUnitID = objWorkingUnit.WorkingUnitID
                            End Using
                            ObjWorkingUnitNew.Add(ObjWorkingUnitlist)
                        End Using
                        iii = iii + 1
                    Loop
                    gridViewNew.DataSource = ObjWorkingUnitNew
                    gridViewNew.DataBind()
                End Using
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingReportBuilderWorkingUnitBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub LoadData()
        Try
            If ObjMappingReportBuilderWorkingUnit_ApprovalDetail.Count > 0 Then
                Dim ListModeID As Integer = 0
                Using objMappingReportBuilderWorkingUnit_Approval As MappingReportBuilderWorkingUnit_Approval = DataRepository.MappingReportBuilderWorkingUnit_ApprovalProvider.GetByPK_MappingReportBuilderWorkingUnit_Approval_ID(CInt(ObjMappingReportBuilderWorkingUnit_ApprovalDetail(0).FK_MappingReportBuilderWorkingUnit_Approval_ID))
                    ListModeID = CInt(objMappingReportBuilderWorkingUnit_Approval.FK_ModeID)
                    If ListModeID = 1 Then
                        LoadDataAdd()
                    ElseIf ListModeID = 2 Then
                        loaddataEdit()
                    ElseIf ListModeID = 3 Then
                        LoadDataDelete()
                    End If
                End Using

            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingReportBuilderWorkingUnitBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingReportBuilderWorkingUnitApproval.aspx"
            Me.Response.Redirect("MappingReportBuilderWorkingUnitApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Sub ClearSession()
        ObjMappingReportBuilderWorkingUnit_Approval = Nothing
        Session("MappingReportBuilderWorkingUnitApprovalDetail.ObjMappingReportBuilderWorkingUnit_ApprovalDetail") = Nothing
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                ClearSession()

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    LabelTitle.Text = "Custom Alert Approval Detail"
                    LoadData()



                End Using
            End If

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub



    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try

            Using ObjMappingReportBuilderWorkingUnit_ApprovalDetail As MappingReportBuilderWorkingUnit_ApprovalDetail = DataRepository.MappingReportBuilderWorkingUnit_ApprovalDetailProvider.GetByPK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID(PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID)
                If ObjMappingReportBuilderWorkingUnit_ApprovalDetail Is Nothing Then
                    Throw New AMLBLL.SahassaException(MappingReportBuilderWorkingUnitEnum.DATA_NOTVALID)
                End If
            End Using

            If LblAction.Text = "Add" Then
                AMLBLL.MappingReportBuilderWorkingUnitBLL.AcceptAddReportBuilder(PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Edit" Then
                Using ObjList As MappingReportBuilderWorkingUnit = DataRepository.MappingReportBuilderWorkingUnitProvider.GetByPK_MappingReportBuilderWorkingUnit_ID(ObjMappingReportBuilderWorkingUnit_ApprovalDetail(0).PK_MappingReportBuilderWorkingUnit_ID)
                    If ObjList Is Nothing Then
                        Throw New AMLBLL.SahassaException(MappingReportBuilderWorkingUnitEnum.DATA_NOTVALID)
                    End If
                End Using
                AMLBLL.MappingReportBuilderWorkingUnitBLL.AcceptEditReportBuilder(PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Delete" Then
                Using ObjList As MappingReportBuilderWorkingUnit = DataRepository.MappingReportBuilderWorkingUnitProvider.GetByPK_MappingReportBuilderWorkingUnit_ID(ObjMappingReportBuilderWorkingUnit_ApprovalDetail(0).PK_MappingReportBuilderWorkingUnit_ID)
                    If ObjList Is Nothing Then
                        Throw New AMLBLL.SahassaException(MappingReportBuilderWorkingUnitEnum.DATA_NOTVALID)
                    End If
                End Using
                AMLBLL.MappingReportBuilderWorkingUnitBLL.AcceptDeleteReportBuilder(PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID)
            End If

            Sahassa.AML.Commonly.SessionIntendedPage = "MappingReportBuilderWorkingUnitApproval.aspx"

            Me.Response.Redirect("MappingReportBuilderWorkingUnitApproval.aspx", False)

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try

            Using ObjMappingReportBuilderWorkingUnit_ApprovalDetail As MappingReportBuilderWorkingUnit_ApprovalDetail = DataRepository.MappingReportBuilderWorkingUnit_ApprovalDetailProvider.GetByPK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID(PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID)
                If ObjMappingReportBuilderWorkingUnit_ApprovalDetail Is Nothing Then
                    Throw New AMLBLL.SahassaException(MappingReportBuilderWorkingUnitEnum.DATA_NOTVALID)
                End If
            End Using

            If LblAction.Text = "Add" Then
                AMLBLL.MappingReportBuilderWorkingUnitBLL.RejectAddReportBuilder(PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Edit" Then
                AMLBLL.MappingReportBuilderWorkingUnitBLL.RejectEditReportBuilder(PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Delete" Then
                AMLBLL.MappingReportBuilderWorkingUnitBLL.RejectDeleteReportBuilder(PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID)
            End If

            Sahassa.AML.Commonly.SessionIntendedPage = "MappingReportBuilderWorkingUnitApproval.aspx"

            Me.Response.Redirect("MappingReportBuilderWorkingUnitApproval.aspx", False)


        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub gridViewOLD_PageIndexChanged(source As Object, e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gridViewOLD.PageIndexChanged
        Try
            gridViewOLD.CurrentPageIndex = e.NewPageIndex

            gridViewOLD.DataSource = ObjWorkingUnitOld
            gridViewOLD.DataBind()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub gridViewNew_PageIndexChanged(source As Object, e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gridViewNew.PageIndexChanged
        Try
            gridViewNew.CurrentPageIndex = e.NewPageIndex

            gridViewNew.DataSource = ObjWorkingUnitNew
            gridViewNew.DataBind()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class


