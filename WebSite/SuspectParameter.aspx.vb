Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Public Class SuspectParameter
    Inherits Parent

#Region " Property "
    ''' <summary>
    ''' get focus id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TextPercentageSuspect.ClientID
        End Get
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cek approved
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Function CekApproved() As Boolean
        Using AccessParameters As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
            Dim count As Int32 = AccessParameters.CountParametersApproval(2)
            If count > 0 Then
                Return False
            Else
                Return True
            End If
        End Using
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get data login parameter
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	03/07/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GetData()
        Using AccessSuspectParameter As New AMLDAL.AMLDataSetTableAdapters.SuspectParameterTableAdapter
            Using TableSuspectParameter As AMLDAL.AMLDataSet.SuspectParameterDataTable = AccessSuspectParameter.GetData
                If TableSuspectParameter.Rows.Count > 0 Then
                    Dim TableRowSuspectParameter As AMLDAL.AMLDataSet.SuspectParameterRow = TableSuspectParameter.Rows(0)

                    ViewState("SuspectPercentage_Old") = CType(TableRowSuspectParameter.SuspectPercentage, Int16)
                    Me.TextPercentageSuspect.Text = ViewState("SuspectPercentage_Old")

                    ViewState("SuspectNamePercentage_Old") = CType(TableRowSuspectParameter.SuspectNamePercentage, Int16)
                    Me.TextName.Text = ViewState("SuspectNamePercentage_Old")

                    ViewState("SuspectDOBPercentage_Old") = CType(TableRowSuspectParameter.SuspectDOBPercentage, Int16)
                    Me.TextDOB.Text = ViewState("SuspectDOBPercentage_Old")

                    ViewState("SuspectIDNoPercentage_Old") = CType(TableRowSuspectParameter.SuspectIDNoPercentage, Int16)
                    Me.TextIDNo.Text = ViewState("SuspectIDNoPercentage_Old")

                    ViewState("SuspectAddressPercentage_Old") = CType(TableRowSuspectParameter.SuspectAddressPercentage, Int16)
                    Me.TextAddress.Text = ViewState("SuspectAddressPercentage_Old")

                    ViewState("CreatedDate_Old") = CType(TableRowSuspectParameter.CreatedDate, Date)
                    ViewState("LastUpdateDate_Old") = CType(TableRowSuspectParameter.LastUpdateDate, Date)

                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load page handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Me.CekApproved() Then
                If Not Me.IsPostBack Then
                    Me.GetData()

                    Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                    'Using Transcope As New Transactions.TransactionScope
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                        Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                        AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                        'Transcope.Complete()
                    End Using
                    'End Using
                End If
            Else
                Throw New Exception("Cannot update Suspect Parameter because it is currently waiting for approval")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cancel event handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "Default.aspx"

        Me.Response.Redirect("Default.aspx", False)
    End Sub
    ''' <summary>
    ''' Insert Audit Trail
    ''' </summary>
    ''' <param name="mode"></param>
    ''' <param name="Action"></param>
    ''' <remarks></remarks>
    Private Function InsertAuditTrailSU(ByVal mode As String, ByVal Action As String) As Boolean
        Try
            Using AccessAuditTrailParameter As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterTableAdapter
                Using dtAuditTrailParameter As AMLDAL.AMLDataSet.AuditTrailParameterDataTable = AccessAuditTrailParameter.GetData
                    Dim RowAuditTrailParameter As AMLDAL.AMLDataSet.AuditTrailParameterRow = dtAuditTrailParameter.Rows(0)
                    If RowAuditTrailParameter.AlertOptions = 1 Then 'alert Overwrite
                        If Not Sahassa.AML.AuditTrailAlert.CheckOverwrite(7) Then
                            Throw New Exception("There is a problem in checking audittrail alert overwrite")
                        End If
                    ElseIf RowAuditTrailParameter.AlertOptions = 2 Then 'alert mail
                        If Not Sahassa.AML.AuditTrailAlert.CheckMailAlert(7) Then
                            Throw New Exception("There is a problem in checking audittrail alert by mail.")
                        End If
                    End If ' yg manual tidak perlu d cek krn ada audittrail maintenance
                End Using
            End Using

            Using AccessSuspectParameter As New AMLDAL.AMLDataSetTableAdapters.SuspectParameterTableAdapter
                Using TableSuspectParameter As AMLDAL.AMLDataSet.SuspectParameterDataTable = AccessSuspectParameter.GetData
                    Dim RowOld As AMLDAL.AMLDataSet.SuspectParameterRow = TableSuspectParameter.Rows(0)
                    If mode.ToLower = "add" Then
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectPercentage", mode, "", Me.TextPercentageSuspect.Text, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectNamePercentage", mode, "", Me.TextName.Text, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectDOBPercentage", mode, "", TextDOB.Text, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectIDNoPercentage", mode, "", TextIDNo.Text, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectAddressPercentage", mode, "", TextAddress.Text, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "CreatedDate", mode, Now.ToString("dd-MMMM-yyyy HH:mm"), Now.ToString("dd-MMMM-yyyy HH:mm"), Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "LastUpdateDate", mode, Now.ToString("dd-MMMM-yyyy HH:mm"), Now.ToString("dd-MMMM-yyyy HH:mm"), Action)
                        End Using
                    Else 'edit
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectPercentage", mode, RowOld.SuspectPercentage, Me.TextPercentageSuspect.Text, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectNamePercentage", mode, RowOld.SuspectNamePercentage, Me.TextName.Text, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectDOBPercentage", mode, RowOld.SuspectDOBPercentage, TextDOB.Text, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectIDNoPercentage", mode, RowOld.SuspectIDNoPercentage, TextIDNo.Text, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectAddressPercentage", mode, RowOld.SuspectAddressPercentage, TextAddress.Text, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "CreatedDate", mode, RowOld.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), Now.ToString("dd-MMMM-yyyy HH:mm"), Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "LastUpdateDate", mode, RowOld.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), Now.ToString("dd-MMMM-yyyy HH:mm"), Action)
                        End Using
                    End If
                End Using
            End Using
            Return True
        Catch
            Return False
        End Try
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' save handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>    
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry] 14/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            If Me.CekApproved Then
                Page.Validate()
                If Page.IsValid Then
                    'Using TranScope As New Transactions.TransactionScope
                    Using ParametersPending As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                        oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(ParametersPending, Data.IsolationLevel.ReadUncommitted)
                        'Perika apakah sudah ada entry dalam tabel SuspectParameter, bila belum ada, maka buat entry baru dlm tabel SuspectParameter,
                        'tapi bila entry sudah ada dalam tabel SuspectParameter, maka update entry tsb
                        Using AccessSuspectParameter As New AMLDAL.AMLDataSetTableAdapters.SuspectParameterTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessSuspectParameter, oSQLTrans)
                            Using TableSuspectParameter As AMLDAL.AMLDataSet.SuspectParameterDataTable = AccessSuspectParameter.GetData
                                Using SuspectParameterApproval As New AMLDAL.AMLDataSetTableAdapters.SuspectParameter_ApprovalTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(SuspectParameterApproval, oSQLTrans)
                                    'Buat variabel untuk menampung nilai-nilai baru
                                    Dim SuspectPercentage As Int16 = Me.TextPercentageSuspect.Text
                                    Dim SuspectNamePercentage As Int16 = Me.TextName.Text
                                    Dim SuspectDOBPercentage As Int16 = Me.TextDOB.Text
                                    Dim SuspectIDNoPercentage As Int16 = Me.TextIDNo.Text
                                    Dim SuspectAddressPercentage As Int16 = Me.TextAddress.Text

                                    Dim ModeID As Int16
                                    Dim SuspectParameterApprovalID As Int64

                                    If Sahassa.AML.Commonly.SessionPkUserId = 1 Then ' Super User
                                        'Using TranScope As New Transactions.TransactionScope
                                        If TableSuspectParameter.Rows.Count > 0 Then 'Edit
                                            If Me.InsertAuditTrailSU("Edit", "Accepted") Then
                                                AccessSuspectParameter.UpdateSuspectParameter(SuspectPercentage, SuspectNamePercentage, SuspectDOBPercentage, SuspectIDNoPercentage, SuspectAddressPercentage, Now)
                                                Me.LblSucces.Visible = True
                                                Me.LblSucces.Text = "Suspect Parameter Succeed to Update."
                                                oSQLTrans.Commit()
                                            Else
                                                Throw New Exception("Failed to insert audit trail.")
                                            End If
                                        Else ' Add by SU
                                            If Me.InsertAuditTrailSU("Add", "Accepted") Then
                                                If AccessSuspectParameter.Insert(SuspectPercentage, SuspectNamePercentage, SuspectDOBPercentage, SuspectIDNoPercentage, SuspectAddressPercentage, Now, Now) > 0 Then
                                                    Me.InsertAuditTrailSU("Add", "Accepted")
                                                    Me.LblSucces.Visible = True
                                                    Me.LblSucces.Text = "Suspect Parameter Succeed to Save."
                                                    oSQLTrans.Commit()
                                                Else
                                                    Throw New Exception("Failed to save Suspect Parameter.")
                                                End If
                                            Else
                                                Throw New Exception("Failed to insert audit trail.")
                                            End If
                                        End If
                                        'End Using
                                    Else ' Bukan Super User
                                        'Using TranScope As New Transactions.TransactionScope
                                        'Bila tabel SuspectParameter tidak kosong, maka update entry tsb & set ModeID = 2 (Edit)
                                        If TableSuspectParameter.Rows.Count > 0 Then
                                            'Buat variabel untuk menampung nilai-nilai lama
                                            Dim SuspectPercentage_Old As Int16 = ViewState("SuspectPercentage_Old")
                                            Dim SuspectNamePercentage_Old As Int16 = ViewState("SuspectNamePercentage_Old")
                                            Dim SuspectDOBPercentage_Old As Int16 = ViewState("SuspectDOBPercentage_Old")
                                            Dim SuspectIDNoPercentage_Old As Int16 = ViewState("SuspectIDNoPercentage_Old")
                                            Dim SuspectAddressPercentage_Old As Int16 = ViewState("SuspectAddressPercentage_Old")
                                            Dim CreatedDate_Old As DateTime = ViewState("CreatedDate_Old")
                                            Dim LastUpdate_Old As DateTime = ViewState("LastUpdateDate_Old")
                                            ModeID = 2 'Edit

                                            'SuspectParameterApprovalID adalah primary key dari tabel SuspectParameter_Approval yang sifatnya identity dan baru dibentuk setelah row baru dibuat
                                            SuspectParameterApprovalID = SuspectParameterApproval.InsertSuspectParameter_Approval(SuspectPercentage, SuspectNamePercentage, SuspectDOBPercentage, SuspectIDNoPercentage, SuspectAddressPercentage, CreatedDate_Old, Now, _
                                                                                                                                  SuspectPercentage_Old, SuspectNamePercentage_Old, SuspectDOBPercentage_Old, SuspectIDNoPercentage_Old, SuspectAddressPercentage_Old, CreatedDate_Old, LastUpdate_Old)
                                        Else 'Bila tabel SuspectParameter kosong, maka buat entry baru & set ModeID = 1 (Add)
                                            ModeID = 1 'Add

                                            'SuspectParameterApprovalID adalah primary key dari tabel SuspectParameter_Approval yang sifatnya identity dan baru dibentuk setelah row baru dibuat
                                            SuspectParameterApprovalID = SuspectParameterApproval.InsertSuspectParameter_Approval(SuspectPercentage, SuspectNamePercentage, SuspectDOBPercentage, SuspectIDNoPercentage, SuspectAddressPercentage, Now, Now, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
                                        End If


                                        'ParametersPendingApprovalID adalah primary key dari tabel Parameters_PendingApproval yang sifatnya identity dan baru dibentuk setelah row baru dibuat 
                                        Dim ParametersPendingApprovalID As Int64 = ParametersPending.InsertParameters_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, ModeID)

                                        Using ParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                                            Sahassa.AML.TableAdapterHelper.SetTransaction(ParametersApproval, oSQLTrans)
                                            'ParameterItemID 2 = SuspectParameter
                                            ParametersApproval.Insert(ParametersPendingApprovalID, ModeID, 2, SuspectParameterApprovalID)
                                        End Using

                                        oSQLTrans.Commit()

                                        If ModeID = 1 Then
                                            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=8821"

                                            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=8821", False)
                                        ElseIf ModeID = 2 Then
                                            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=8822"

                                            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=8822", False)
                                        End If
                                    End If
                                End Using
                            End Using
                        End Using
                    End Using
                    'End Using
                    'End Using

                    'oSQLTrans.Commit()
                    'End Using
                End If
            Else
            Throw New Exception("Cannot update Suspect Parameter because it is currently waiting for approval")
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

End Class