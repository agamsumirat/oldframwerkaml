<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UserDelete.aspx.vb" Inherits="UserDelete" title="User Delete" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="231">
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none" colspan="4">
                <strong><span style="font-size: 18px">User - Delete
                    <hr />
                </span></strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none" width="5">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/validationsign_animate.gif" /></td>
            <td bgcolor="#ffffff" colspan="3" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>The following User will be deleted :</strong></td>
        </tr>
		<tr class="formText">
			<td width="5" bgColor="#ffffff" style="height: 30px">
                </td>
			<td width="20%" bgColor="#ffffff" style="height: 30px">User ID</td>
			<td width="5" bgColor="#ffffff" style="height: 30px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 30px"><asp:textbox id="TextUserid" runat="server" CssClass="textBox" MaxLength="10" ReadOnly="True" Width="200px" ></asp:textbox></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24">
                </td>
			<td bgColor="#ffffff">
                User Name</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff"><asp:textbox id="TextUserName" runat="server" CssClass="textBox" MaxLength="50" Width="200px" ReadOnly="True"></asp:textbox><strong><span style="color: #ff0000"></span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24">
                </td>
			<td bgColor="#ffffff">
                Email Address</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff">
                <asp:TextBox ID="TextboxEmailAddr" runat="server" CssClass="textBox" MaxLength="50" ReadOnly="True" Width="200px" ></asp:TextBox></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24">&nbsp;</td>
			<td bgColor="#ffffff">
                Mobile Phone</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff">
                <asp:TextBox ID="TextboxMobilePhone" runat="server" CssClass="textBox" MaxLength="15" ReadOnly="True" Width="200px" ></asp:TextBox></td>
		</tr>
        <tr class="formText">
			<td bgColor="#ffffff" height="24">
			<td bgColor="#ffffff">
                Teller ID</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff">
                <asp:TextBox ID="TextboxTellerID" runat="server" CssClass="textBox" MaxLength="10" ReadOnly="True" Width="200px" ></asp:TextBox>
			</td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24">&nbsp;</td>
			<td bgColor="#ffffff">
                Group</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff">
                <asp:Label ID="LabelGroupName" runat="server"></asp:Label></td>
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td width="15"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="deleteButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	<script>
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>