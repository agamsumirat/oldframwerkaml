#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsBentukBidangUsaha_APPROVAL_Detail
    Inherits Parent

#Region "Function"

    Sub HideControl()
        imgReject.Visible = False
        imgApprove.Visible = False
        ImageCancel.Visible = False
    End Sub

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsBentukBidangUsaha_Approval_View.aspx")
    End Sub

    Protected Sub imgReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgReject.Click
        Try
            Rejected()
            ChangeMultiView(1)
            LblConfirmation.Text = "Data has been rejected"
            HideControl()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgApprove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgApprove.Click
        Try
            Using ObjMsBentukBidangUsaha_Approval As MsBentukBidangUsaha_Approval = DataRepository.MsBentukBidangUsaha_ApprovalProvider.GetByPK_MsBentukBidangUsaha_Approval_Id(parID)
                If ObjMsBentukBidangUsaha_Approval.FK_MsMode_Id.GetValueOrDefault = 1 Then
                    Inserdata(ObjMsBentukBidangUsaha_Approval)
                ElseIf ObjMsBentukBidangUsaha_Approval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                    UpdateData(ObjMsBentukBidangUsaha_Approval)
                ElseIf ObjMsBentukBidangUsaha_Approval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                    DeleteData(ObjMsBentukBidangUsaha_Approval)
                End If
            End Using
            ChangeMultiView(1)
            LblConfirmation.Text = "Data approved successful"
            HideControl()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                ListmapingNew = New TList(Of MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
                ListmapingOld = New TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim PkObject As String
        'Load new data
        Using ObjMsBentukBidangUsaha_ApprovalDetail As MsBentukBidangUsaha_ApprovalDetail = DataRepository.MsBentukBidangUsaha_ApprovalDetailProvider.GetPaged(MsBentukBidangUsaha_ApprovalDetailColumn.FK_MsBentukBidangUsaha_Approval_Id.ToString & _
         "=" & _
         parID, "", 0, 1, Nothing)(0)
            Dim CekMode As MsBentukBidangUsaha_Approval = DataRepository.MsBentukBidangUsaha_ApprovalProvider.GetByPK_MsBentukBidangUsaha_Approval_Id(ObjMsBentukBidangUsaha_ApprovalDetail.FK_MsBentukBidangUsaha_Approval_Id)
            Dim nama As String = CekMode.FK_MsMode_Id
            If nama = 1 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 2 Then
                baru.Visible = True
                lama.Visible = True
                baruNew.Visible = True
                lamaOld.Visible = True
            ElseIf nama = 3 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 4 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            End If
            With ObjMsBentukBidangUsaha_ApprovalDetail
                SafeDefaultValue = "-"
                txtIdBentukBidangUsahanew.Text = Safe(.IdBentukBidangUsaha)
                txtBentukBidangUsahanew.Text = Safe(.BentukBidangUsaha)

                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByNew.Text = Omsuser(0).UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyNew.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
                lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
                txtActivationNew.Text = SafeActiveInactive(.Activation)
                Dim L_objMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail As TList(Of MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
                L_objMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail = DataRepository.MappingMsBentukBidangUsahaNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsBentukBidangUsahaNCBSPPATK_Approval_DetailColumn.PK_MsBentukBidangUsaha_Approval_Id.ToString & _
                 "=" & _
                 parID, "", 0, Integer.MaxValue, Nothing)
                'Add mapping
                ListmapingNew.AddRange(L_objMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)

            End With
            PkObject = ObjMsBentukBidangUsaha_ApprovalDetail.IdBentukBidangUsaha
        End Using

        'Load Old Data
        Using ObjMsBentukBidangUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(PkObject)
            If ObjMsBentukBidangUsaha Is Nothing Then Return
            With ObjMsBentukBidangUsaha
                SafeDefaultValue = "-"
                txtIdBentukBidangUsahaOld.Text = Safe(.IdBentukBidangUsaha)
                txtBentukBidangUsahaOld.Text = Safe(.BentukBidangUsaha)

                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByOld.Text = Omsuser(0).UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyOld.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
                lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
                Dim L_objMappingMsBentukBidangUsahaNCBSPPATK As TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
                txtActivationOld.Text = SafeActiveInactive(.Activation)
                L_objMappingMsBentukBidangUsahaNCBSPPATK = DataRepository.MappingMsBentukBidangUsahaNCBSPPATKProvider.GetPaged(MappingMsBentukBidangUsahaNCBSPPATKColumn.IdBentukBidangUsaha.ToString & _
                   "=" & _
                   PkObject, "", 0, Integer.MaxValue, Nothing)
                ListmapingOld.AddRange(L_objMappingMsBentukBidangUsahaNCBSPPATK)
            End With
        End Using
    End Sub



    Protected Sub imgBtnConfirmation_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnConfirmation.Click
        Try
            Response.Redirect("MsBentukBidangUsaha_approval_view.aspx")
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping New sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingNew() As TList(Of MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
        Get
            Return Session("ListmapingNew.data")
        End Get
        Set(ByVal value As TList(Of MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail))
            Session("ListmapingNew.data") = value
        End Set
    End Property

    ''' <summary>
    ''' Menyimpan Item untuk mapping Old sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingOld() As TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
        Get
            Return Session("ListmapingOld.data")
        End Get
        Set(ByVal value As TList(Of MappingMsBentukBidangUsahaNCBSPPATK))
            Session("ListmapingOld.data") = value
        End Set
    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail In ListmapingNew.FindAllDistinct("IdBentukBidangusahaNCBS")
                Temp.Add(i.IdBentukBidangUsahaNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsBentukBidangUsahaNCBSPPATK In ListmapingOld.FindAllDistinct("IdBentukBidangusahaNCBS")
                Temp.Add(i.IdBentukBidangUsahaNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

#Region "Action Approval..."

    Sub Rejected()
        Dim ObjMsBentukBidangUsaha_Approval As MsBentukBidangUsaha_Approval = DataRepository.MsBentukBidangUsaha_ApprovalProvider.GetByPK_MsBentukBidangUsaha_Approval_Id(parID)
        Dim ObjMsBentukBidangUsaha_ApprovalDetail As MsBentukBidangUsaha_ApprovalDetail = DataRepository.MsBentukBidangUsaha_ApprovalDetailProvider.GetPaged(MsBentukBidangUsaha_ApprovalDetailColumn.FK_MsBentukBidangUsaha_Approval_Id.ToString & "=" & ObjMsBentukBidangUsaha_Approval.PK_MsBentukBidangUsaha_Approval_Id, "", 0, 1, Nothing)(0)
        Dim L_ObjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail As TList(Of MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail) = DataRepository.MappingMsBentukBidangUsahaNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsBentukBidangUsahaNCBSPPATK_Approval_DetailColumn.PK_MsBentukBidangUsaha_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)
        DeleteApproval(ObjMsBentukBidangUsaha_Approval, ObjMsBentukBidangUsaha_ApprovalDetail, L_ObjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
    End Sub

    Sub DeleteApproval(ByRef objMsBentukBidangUsaha_Approval As MsBentukBidangUsaha_Approval, ByRef ObjMsBentukBidangUsaha_ApprovalDetail As MsBentukBidangUsaha_ApprovalDetail, ByRef L_ObjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail As TList(Of MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail))
        DataRepository.MsBentukBidangUsaha_ApprovalProvider.Delete(objMsBentukBidangUsaha_Approval)
        DataRepository.MsBentukBidangUsaha_ApprovalDetailProvider.Delete(ObjMsBentukBidangUsaha_ApprovalDetail)
        DataRepository.MappingMsBentukBidangUsahaNCBSPPATK_Approval_DetailProvider.Delete(L_ObjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
    End Sub

    ''' <summary>
    ''' Delete Data dari  asli dari approval
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DeleteData(ByVal objMsBentukBidangUsaha_Approval As MsBentukBidangUsaha_Approval)
        '   '================= Delete Header ===========================================================
        Dim LObjMsBentukBidangUsaha_ApprovalDetail As TList(Of MsBentukBidangUsaha_ApprovalDetail) = DataRepository.MsBentukBidangUsaha_ApprovalDetailProvider.GetPaged(MsBentukBidangUsaha_ApprovalDetailColumn.FK_MsBentukBidangUsaha_Approval_Id.ToString & "=" & objMsBentukBidangUsaha_Approval.PK_MsBentukBidangUsaha_Approval_Id, "", 0, 1, Nothing)
        If LObjMsBentukBidangUsaha_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        Dim ObjMsBentukBidangUsaha_ApprovalDetail As MsBentukBidangUsaha_ApprovalDetail = LObjMsBentukBidangUsaha_ApprovalDetail(0)
        Using ObjNewMsBentukBidangUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(ObjMsBentukBidangUsaha_ApprovalDetail.IdBentukBidangUsaha)
            DataRepository.MsBentukBidangUsahaProvider.Delete(ObjNewMsBentukBidangUsaha)
        End Using

        '======== Delete MAPPING =========================================
        Dim L_ObjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail As TList(Of MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail) = DataRepository.MappingMsBentukBidangUsahaNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsBentukBidangUsahaNCBSPPATK_Approval_DetailColumn.PK_MsBentukBidangUsaha_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


        For Each c As MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail In L_ObjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail
            Using ObjNewMappingMsBentukBidangUsahaNCBSPPATK As MappingMsBentukBidangUsahaNCBSPPATK = DataRepository.MappingMsBentukBidangUsahaNCBSPPATKProvider.GetByPK_MappingMsBentukBidangUsahaNCBSPPATK_Id(c.PK_MappingMsBentukBidangUsahaNCBSPPATK_Id)
                DataRepository.MappingMsBentukBidangUsahaNCBSPPATKProvider.Delete(ObjNewMappingMsBentukBidangUsahaNCBSPPATK)
            End Using
        Next

        '======== Delete Approval data ======================================
        DeleteApproval(objMsBentukBidangUsaha_Approval, ObjMsBentukBidangUsaha_ApprovalDetail, L_ObjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
    End Sub

    ''' <summary>
    ''' Update Data dari approval ke table asli 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateData(ByVal objMsBentukBidangUsaha_Approval As MsBentukBidangUsaha_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
        '   '================= Update Header ===========================================================
        Dim LObjMsBentukBidangUsaha_ApprovalDetail As TList(Of MsBentukBidangUsaha_ApprovalDetail) = DataRepository.MsBentukBidangUsaha_ApprovalDetailProvider.GetPaged(MsBentukBidangUsaha_ApprovalDetailColumn.FK_MsBentukBidangUsaha_Approval_Id.ToString & "=" & objMsBentukBidangUsaha_Approval.PK_MsBentukBidangUsaha_Approval_Id, "", 0, 1, Nothing)
        If LObjMsBentukBidangUsaha_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        Dim ObjMsBentukBidangUsaha_ApprovalDetail As MsBentukBidangUsaha_ApprovalDetail = LObjMsBentukBidangUsaha_ApprovalDetail(0)
        Dim ObjMsBentukBidangUsahaNew As MsBentukBidangUsaha
        Using ObjMsBentukBidangUsahaOld As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(ObjMsBentukBidangUsaha_ApprovalDetail.IdBentukBidangUsaha)
            If ObjMsBentukBidangUsahaOld Is Nothing Then Throw New Exception("Data Not Found")
            ObjMsBentukBidangUsahaNew = ObjMsBentukBidangUsahaOld.Clone
            With ObjMsBentukBidangUsahaNew
                FillOrNothing(.BentukBidangUsaha, ObjMsBentukBidangUsaha_ApprovalDetail.BentukBidangUsaha)
                FillOrNothing(.CreatedBy, ObjMsBentukBidangUsaha_ApprovalDetail.CreatedBy)
                FillOrNothing(.CreatedDate, ObjMsBentukBidangUsaha_ApprovalDetail.CreatedDate)
                FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
                FillOrNothing(.ApprovedDate, Date.Now)
            End With
            DataRepository.MsBentukBidangUsahaProvider.Save(ObjMsBentukBidangUsahaNew)
            'Insert auditrail
            InsertAuditTrail()
            '======== Update MAPPING =========================================
            'delete mapping item
            Using L_ObjMappingMsBentukBidangUsahaNCBSPPATK As TList(Of MappingMsBentukBidangUsahaNCBSPPATK) = DataRepository.MappingMsBentukBidangUsahaNCBSPPATKProvider.GetPaged(MappingMsBentukBidangUsahaNCBSPPATKColumn.IdBentukBidangUsaha.ToString & _
             "=" & _
             ObjMsBentukBidangUsahaNew.IdBentukBidangUsaha, "", 0, Integer.MaxValue, Nothing)
                DataRepository.MappingMsBentukBidangUsahaNCBSPPATKProvider.Delete(L_ObjMappingMsBentukBidangUsahaNCBSPPATK)
            End Using
            'Insert mapping item
            Dim L_ObjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail As TList(Of MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail) = DataRepository.MappingMsBentukBidangUsahaNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsBentukBidangUsahaNCBSPPATK_Approval_DetailColumn.PK_MsBentukBidangUsaha_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            Dim L_ObjNewMappingMsBentukBidangUsahaNCBSPPATK As New TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
            For Each c As MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail In L_ObjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail
                Dim ObjNewMappingMsBentukBidangUsahaNCBSPPATK As New MappingMsBentukBidangUsahaNCBSPPATK '= DataRepository.MappingMsBentukBidangUsahaNCBSPPATKProvider.GetByPK_MappingMsBentukBidangUsahaNCBSPPATK_Id(c.PK_MappingMsBentukBidangUsahaNCBSPPATK_Id.GetValueOrDefault)
                With ObjNewMappingMsBentukBidangUsahaNCBSPPATK
                    FillOrNothing(.IdBentukBidangUsaha, ObjMsBentukBidangUsahaNew.IdBentukBidangUsaha)
                    FillOrNothing(.Nama, c.Nama)
                    FillOrNothing(.IdBentukBidangUsahaNCBS, c.IdBentukBidangUsahaNCBS)
                    L_ObjNewMappingMsBentukBidangUsahaNCBSPPATK.Add(ObjNewMappingMsBentukBidangUsahaNCBSPPATK)
                End With
            Next
            DataRepository.MappingMsBentukBidangUsahaNCBSPPATKProvider.Save(L_ObjNewMappingMsBentukBidangUsahaNCBSPPATK)
            '======== Delete Approval data ======================================
            DeleteApproval(objMsBentukBidangUsaha_Approval, ObjMsBentukBidangUsaha_ApprovalDetail, L_ObjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
        End Using
    End Sub
    ''' <summary>
    ''' Insert Data dari approval ke table asli 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Inserdata(ByVal objMsBentukBidangUsaha_Approval As MsBentukBidangUsaha_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
        '   '================= Save Header ===========================================================
        Dim LObjMsBentukBidangUsaha_ApprovalDetail As TList(Of MsBentukBidangUsaha_ApprovalDetail) = DataRepository.MsBentukBidangUsaha_ApprovalDetailProvider.GetPaged(MsBentukBidangUsaha_ApprovalDetailColumn.FK_MsBentukBidangUsaha_Approval_Id.ToString & "=" & objMsBentukBidangUsaha_Approval.PK_MsBentukBidangUsaha_Approval_Id, "", 0, 1, Nothing)
        'jika data tidak ada di database
        If LObjMsBentukBidangUsaha_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        'binding approval ke table asli
        Dim ObjMsBentukBidangUsaha_ApprovalDetail As MsBentukBidangUsaha_ApprovalDetail = LObjMsBentukBidangUsaha_ApprovalDetail(0)
        Using ObjNewMsBentukBidangUsaha As New MsBentukBidangUsaha()
            With ObjNewMsBentukBidangUsaha
                FillOrNothing(.IdBentukBidangUsaha, ObjMsBentukBidangUsaha_ApprovalDetail.IdBentukBidangUsaha)
                FillOrNothing(.BentukBidangUsaha, ObjMsBentukBidangUsaha_ApprovalDetail.BentukBidangUsaha)
                FillOrNothing(.CreatedBy, ObjMsBentukBidangUsaha_ApprovalDetail.CreatedBy)
                FillOrNothing(.CreatedDate, ObjMsBentukBidangUsaha_ApprovalDetail.CreatedDate)
                FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
                FillOrNothing(.ApprovedDate, Date.Now)
                .activation = True
            End With
            DataRepository.MsBentukBidangUsahaProvider.Save(ObjNewMsBentukBidangUsaha)
            'Insert auditrail
            InsertAuditTrail()
            '======== Insert MAPPING =========================================
            Dim L_ObjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail As TList(Of MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail) = DataRepository.MappingMsBentukBidangUsahaNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsBentukBidangUsahaNCBSPPATK_Approval_DetailColumn.PK_MsBentukBidangUsaha_Approval_Id.ToString & _
             "=" & _
             parID, "", 0, Integer.MaxValue, Nothing)
            Dim L_ObjNewMappingMsBentukBidangUsahaNCBSPPATK As New TList(Of MappingMsBentukBidangUsahaNCBSPPATK)()
            For Each c As MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail In L_ObjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail
                Dim ObjNewMappingMsBentukBidangUsahaNCBSPPATK As New MappingMsBentukBidangUsahaNCBSPPATK()
                With ObjNewMappingMsBentukBidangUsahaNCBSPPATK
                    FillOrNothing(.IdBentukBidangUsaha, ObjNewMsBentukBidangUsaha.IdBentukBidangUsaha)
                    FillOrNothing(.Nama, c.Nama)
                    FillOrNothing(.IdBentukBidangUsahaNCBS, c.IdBentukBidangUsahaNCBS)
                    L_ObjNewMappingMsBentukBidangUsahaNCBSPPATK.Add(ObjNewMappingMsBentukBidangUsahaNCBSPPATK)
                End With
            Next
            DataRepository.MappingMsBentukBidangUsahaNCBSPPATKProvider.Save(L_ObjNewMappingMsBentukBidangUsahaNCBSPPATK)
            '======== Delete Approval data ======================================
            DeleteApproval(objMsBentukBidangUsaha_Approval, ObjMsBentukBidangUsaha_ApprovalDetail, L_ObjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
        End Using
    End Sub

#End Region

    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(8)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBentukBidangUsaha", "ID", "Add", txtIdBentukBidangUsahaOld.Text, txtIdBentukBidangUsahanew.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBentukBidangUsaha", "Nama", "Add", txtBentukBidangUsahaOld.Text, txtBentukBidangUsahanew.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBentukBidangUsaha", "Activation", "Add", txtActivationOld.Text, txtActivationNew.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBentukBidangUsaha", "Mapping Item", "Add", LBmappingOld.Text, LBMappingNew.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBentukBidangUsaha", "Created Date", "Add", lblCreatedDateOld.Text, lblCreatedDateNew.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBentukBidangUsaha", "Created By", "Add", lblCreatedByOld.Text, lblCreatedByNew.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBentukBidangUsaha", "Updated Date", "Add", lblUpdatedDateOld.Text, lblUpdatedDateNew.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBentukBidangUsaha", "Updated By", "Add", lblUpdatedbyOld.Text, lblUpdatedbyNew.Text, "Accepted")
            End Using
        Catch
            Throw
        End Try
    End Sub

End Class


