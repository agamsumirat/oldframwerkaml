Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class AccountOwnerWorkingUnitMappingApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param pk RiskRating management
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property AccountOwnerWorkingUnitMappingApprovalID() As Int64
        Get
            Return Me.Request.Params("ApprovalID")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param Parameters_PendingApprovalID
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParametersPendingApprovalID() As Int64
        Get
            Return Me.Request.Params("PendingApprovalID")
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserID() As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                Return AccessPending.SelectParameters_PendingApprovalUserID(Me.ParametersPendingApprovalID)
            End Using
        End Get
    End Property
#End Region

#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HideAll()
        Dim StrId As String = ""
        Select Case Me.ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.AccountOwnerWorkingUnitMappingAdd
                StrId = "UserAdd"
            Case Sahassa.AML.Commonly.TypeConfirm.AccountOwnerWorkingUnitMappingEdit
                StrId = "UserEdi"
            Case Sahassa.AML.Commonly.TypeConfirm.AccountOwnerWorkingUnitMappingDelete
                StrId = "UserDel"
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select
        For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
            ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID.Substring(0, 7) = StrId
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load user add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadAccountOwnerWorkingUnitMappingAdd()
        Me.LabelTitle.Text = "Activity: Add Account Owner Working Unit Mapping"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMapping_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetAccountOwnerWorkingUnitMappingApprovalDetailData(Me.AccountOwnerWorkingUnitMappingApprovalID)
                'Bila ObjTable.Rows.Count > 0 berarti AccountOwnerWorkingUnitMapping tsb masih ada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMapping_ApprovalRow = ObjTable.Rows(0)

                    Using AccessAccountOwner As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
                        Dim AccountOwnerTable As Data.DataTable = AccessAccountOwner.GetDataByAccountOwnerId(rowData.AccountOwnerID)
                        Dim AccountOwnerRow As AMLDAL.AMLDataSet.AccountOwnerRow = AccountOwnerTable.Rows(0)
                        Me.LabelAccountOwnerAdd.Text = AccountOwnerRow.AccountOwnerId & " - " & AccountOwnerRow.AccountOwnerName
                    End Using

                    Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                        Dim WorkingUnitTable As Data.DataTable = AccessWorkingUnit.GetDataByWorkingUnitID(rowData.WorkingUnitID)
                        Dim WorkingUnitRow As AMLDAL.AMLDataSet.WorkingUnitRow = WorkingUnitTable.Rows(0)
                        Me.LabelWorkingUnitAdd.Text = WorkingUnitRow.WorkingUnitID & " - " & WorkingUnitRow.WorkingUnitName
                    End Using
                Else 'Bila ObjTable.Rows.Count = 0 berarti AccountOwnerWorkingUnitMapping tsb sudah tidak lagi berada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                    Throw New Exception("Cannot load data from the following Account Owner : " & Me.LabelAccountOwnerAdd.Text & " because that Account Owner Working Unit Mapping is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load user edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadAccountOwnerWorkingUnitMappingEdit()
        Me.LabelTitle.Text = "Activity: Edit Account Owner Working Unit Mapping"

        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMapping_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetAccountOwnerWorkingUnitMappingApprovalDetailData(Me.AccountOwnerWorkingUnitMappingApprovalID)
                'Bila ObjTable.Rows.Count > 0 berarti AccountOwnerWorkingUnitMapping tsb masih ada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMapping_ApprovalRow = ObjTable.Rows(0)

                    Using AccessAccountOwner As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
                        'Tarik informasi untuk New Values
                        Dim AccountOwnerTableNew As Data.DataTable = AccessAccountOwner.GetDataByAccountOwnerId(rowData.AccountOwnerID)
                        Dim AccountOwnerRowNew As AMLDAL.AMLDataSet.AccountOwnerRow = AccountOwnerTableNew.Rows(0)
                        Me.LabelAccountOwnerNew.Text = AccountOwnerRowNew.AccountOwnerId & " - " & AccountOwnerRowNew.AccountOwnerName

                        'Tarik informasi untuk Old Values
                        Dim AccountOwnerTableOld As Data.DataTable = AccessAccountOwner.GetDataByAccountOwnerId(rowData.AccountOwnerID_Old)
                        Dim AccountOwnerRowOld As AMLDAL.AMLDataSet.AccountOwnerRow = AccountOwnerTableOld.Rows(0)
                        Me.LabelAccountOwnerOld.Text = AccountOwnerRowOld.AccountOwnerId & " - " & AccountOwnerRowOld.AccountOwnerName
                    End Using

                    Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                        'Tarik informasi untuk New Values
                        Dim WorkingUnitTableNew As Data.DataTable = AccessWorkingUnit.GetDataByWorkingUnitID(rowData.WorkingUnitID)
                        Dim WorkingUnitRowNew As AMLDAL.AMLDataSet.WorkingUnitRow = WorkingUnitTableNew.Rows(0)
                        Me.LabelWorkingUnitNew.Text = WorkingUnitRowNew.WorkingUnitID & " - " & WorkingUnitRowNew.WorkingUnitName

                        'Tarik informasi untuk Old Values
                        Dim WorkingUnitTableOld As Data.DataTable = AccessWorkingUnit.GetDataByWorkingUnitID(rowData.WorkingUnitID_Old)
                        Dim WorkingUnitRowOld As AMLDAL.AMLDataSet.WorkingUnitRow = WorkingUnitTableOld.Rows(0)
                        Me.LabelWorkingUnitOld.Text = WorkingUnitRowOld.WorkingUnitID & " - " & WorkingUnitRowOld.WorkingUnitName
                    End Using
                Else 'Bila ObjTable.Rows.Count = 0 berarti AccountOwnerWorkingUnitMapping tsb sudah tidak lagi berada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                    Throw New Exception("Cannot load data from the following Account Owner : " & Me.LabelAccountOwnerOld.Text & " because that Account Owner Working Unit Mapping is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load user delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadAccountOwnerWorkingUnitMappingDelete()
        Me.LabelTitle.Text = "Activity: Delete Account Owner Working Unit Mapping"

        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMapping_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetAccountOwnerWorkingUnitMappingApprovalDetailData(Me.AccountOwnerWorkingUnitMappingApprovalID)
                'Bila ObjTable.Rows.Count > 0 berarti AccountOwnerWorkingUnitMapping tsb masih ada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMapping_ApprovalRow = ObjTable.Rows(0)

                    Using AccessAccountOwner As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
                        Dim AccountOwnerTable As Data.DataTable = AccessAccountOwner.GetDataByAccountOwnerId(rowData.AccountOwnerID)
                        Dim AccountOwnerRow As AMLDAL.AMLDataSet.AccountOwnerRow = AccountOwnerTable.Rows(0)
                        Me.LabelAccountOwnerDelete.Text = AccountOwnerRow.AccountOwnerId & " - " & AccountOwnerRow.AccountOwnerName
                    End Using

                    Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                        Dim WorkingUnitTable As Data.DataTable = AccessWorkingUnit.GetDataByWorkingUnitID(rowData.WorkingUnitID)
                        Dim WorkingUnitRow As AMLDAL.AMLDataSet.WorkingUnitRow = WorkingUnitTable.Rows(0)
                        Me.LabelWorkingUnitDelete.Text = WorkingUnitRow.WorkingUnitID & " - " & WorkingUnitRow.WorkingUnitName
                    End Using
                Else 'Bila ObjTable.Rows.Count = 0 berarti AccountOwnerWorkingUnitMapping tsb sudah tidak lagi berada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                    Throw New Exception("Cannot load data from the following Account Owner : " & Me.LabelAccountOwnerDelete.Text & " because that Account Owner Working Unit Mapping is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub
#End Region

#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' user add accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptAccountOwnerWorkingUnitMappingAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using AccessAccountOwnerWorkingUnitMapping As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMappingTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAccountOwnerWorkingUnitMapping, Data.IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMapping_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                        Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                            Dim ObjTable As Data.DataTable = AccessPending.GetAccountOwnerWorkingUnitMappingApprovalDetailData(Me.AccountOwnerWorkingUnitMappingApprovalID)

                            'Jika ObjTable.Rows.Count > 0 berarti AccountOwnerWorkingUnitMapping tsb masih ada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                            If ObjTable.Rows.Count > 0 Then
                                Dim rowData As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMapping_ApprovalRow = ObjTable.Rows(0)

                                'Periksa apakah AccountOwnerWorkingUnitMapping yg baru tsb sdh ada dlm tabel AccountOwnerWorkingUnitMapping atau belum. 
                                Dim counter As Int32 = AccessAccountOwnerWorkingUnitMapping.CountMatchingAccountOwnerWorkingUnitMapping(rowData.AccountOwnerID)

                                'Jika Counter = 0 berarti AccountOwnerWorkingUnitMapping tsb blm ada dlm tabel AccountOwnerWorkingUnitMapping
                                If counter = 0 Then
                                    'tambahkan item tersebut dalam tabel LoginParameter
                                    AccessAccountOwnerWorkingUnitMapping.Insert(rowData.AccountOwnerID, rowData.WorkingUnitID, rowData.CreatedDate, rowData.LastUpdateDate)

                                    'catat aktifitas dalam tabel Audit Trail
                                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)

                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "AccountOwnerID", "Add", "", rowData.AccountOwnerID, "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "WorkingUnitID", "Add", "", rowData.WorkingUnitID, "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "CreatedDate", "Add", "", rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "LastUpdateDate", "Add", "", rowData.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                                        'Ubah AccountOwnerWorkingUnitMappingStatus dari False menjadi True 
                                        Using AccessAccountOwner As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
                                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAccountOwner, oSQLTrans)
                                            AccessAccountOwner.UpdateAccountOwnerWorkingUnitMappingStatus(rowData.AccountOwnerID, "True")

                                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "AccountOwner", "AccountOwnerWorkingUnitMappingStatus", "Edit", "False", "True", "Accepted")
                                        End Using

                                        'hapus item tsb dalam tabel Parameters_Approval
                                        AccessParametersApproval.DeleteParametersApproval(Me.ParametersPendingApprovalID)

                                        'hapus item tsb dalam tabel LoginParameter_Approval
                                        AccessPending.DeleteAccountOwnerWorkingUnitMappingApproval(rowData.ApprovalID)

                                        'hapus item tsb dalam tabel Parameters_PendingApproval
                                        AccessParametersPendingApproval.DeleteParametersPendingApproval(Me.ParametersPendingApprovalID)
                                    End Using

                                    oSQLTrans.Commit()
                                Else 'Jika Counter != 0 berarti AccountOwnerWorkingUnitMapping tsb sdh ada dlm tabel AccountOwnerWorkingUnitMapping, maka AcceptAccountOwnerWorkingUnitMappingAdd gagal
                                    Throw New Exception("Cannot add Account Owner Working Unit Mapping for the following Account Owner : " & rowData.AccountOwnerName & " because that Account Owner Working Unit Mapping already exists in the database.")
                                End If
                            Else 'Bila ObjTable.Rows.Count = 0 berarti AccountOwnerWorkingUnitMapping tsb sudah tidak lagi berada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                                Throw New Exception("Cannot add Account Owner Working Unit Mapping for the following Account Owner : " & Me.LabelAccountOwnerAdd.Text & " because that Account Owner Working Unit Mapping is no longer in the approval table.")
                            End If
                        End Using
                    End Using
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' LevelType edit accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptAccountOwnerWorkingUnitMappingEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using AccessAccountOwnerWorkingUnitMapping As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMappingTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAccountOwnerWorkingUnitMapping, Data.IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMapping_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                        Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                            Dim ObjTable As Data.DataTable = AccessPending.GetAccountOwnerWorkingUnitMappingApprovalDetailData(Me.AccountOwnerWorkingUnitMappingApprovalID)

                            'Jika ObjTable.Rows.Count > 0 berarti AccountOwnerWorkingUnitMapping tsb masih ada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                            If ObjTable.Rows.Count > 0 Then
                                Dim rowData As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMapping_ApprovalRow = ObjTable.Rows(0)

                                'Periksa apakah AccountOwnerWorkingUnitMapping yg baru tsb sdh ada dlm tabel AccountOwnerWorkingUnitMapping atau belum. 
                                Dim counter As Int32 = AccessAccountOwnerWorkingUnitMapping.CountMatchingAccountOwnerWorkingUnitMapping(rowData.AccountOwnerID)

                                'Jika Counter > 0 berarti AccountOwnerWorkingUnitMapping tsb msh ada dlm tabel AccountOwnerWorkingUnitMapping dan bisa diedit
                                If counter > 0 Then
                                    'tambahkan item tersebut dalam tabel LoginParameter
                                    AccessAccountOwnerWorkingUnitMapping.UpdateAccountOwnerWorkingUnitMapping(rowData.AccountOwnerID, rowData.WorkingUnitID, rowData.LastUpdateDate)

                                    'catat aktifitas dalam tabel Audit Trail
                                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)

                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "AccountOwnerID", "Edit", rowData.AccountOwnerID_Old, rowData.AccountOwnerID, "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "WorkingUnitID", "Edit", rowData.WorkingUnitID_Old, rowData.WorkingUnitID, "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "CreatedDate", "Edit", rowData.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "LastUpdateDate", "Edit", rowData.LastUpdateDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                                        'hapus item tsb dalam tabel Parameters_Approval
                                        AccessParametersApproval.DeleteParametersApproval(Me.ParametersPendingApprovalID)

                                        'hapus item tsb dalam tabel LoginParameter_Approval
                                        AccessPending.DeleteAccountOwnerWorkingUnitMappingApproval(rowData.ApprovalID)

                                        'hapus item tsb dalam tabel Parameters_PendingApproval
                                        AccessParametersPendingApproval.DeleteParametersPendingApproval(Me.ParametersPendingApprovalID)
                                    End Using

                                    oSQLTrans.Commit()
                                Else 'Jika Counter = 0 berarti AccountOwnerWorkingUnitMapping tsb sdh tdk ada dlm tabel AccountOwnerWorkingUnitMapping, maka AcceptAccountOwnerWorkingUnitMappingEdit gagal
                                    Throw New Exception("Cannot edit the Account Owner Working Unit Mapping for the following Account Owner : " & rowData.AccountOwnerName & " because that Account Owner Working Unit Mapping no longer exists in the database.")
                                End If
                            Else 'Bila ObjTable.Rows.Count = 0 berarti AccountOwnerWorkingUnitMapping tsb sudah tidak lagi berada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                                Throw New Exception("Cannot edit the Account Owner Working Unit Mapping for the following Account Owner : " & Me.LabelAccountOwnerAdd.Text & " because that Account Owner Working Unit Mapping is no longer in the approval table.")
                            End If
                        End Using
                    End Using
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' LevelType delete accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptAccountOwnerWorkingUnitMappingDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using AccessAccountOwnerWorkingUnitMapping As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMappingTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAccountOwnerWorkingUnitMapping, Data.IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMapping_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                        Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                            Dim ObjTable As Data.DataTable = AccessPending.GetAccountOwnerWorkingUnitMappingApprovalDetailData(Me.AccountOwnerWorkingUnitMappingApprovalID)

                            'Jika ObjTable.Rows.Count > 0 berarti AccountOwnerWorkingUnitMapping tsb masih ada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                            If ObjTable.Rows.Count > 0 Then
                                Dim rowData As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMapping_ApprovalRow = ObjTable.Rows(0)

                                'Periksa apakah AccountOwnerWorkingUnitMapping yg baru tsb sdh ada dlm tabel AccountOwnerWorkingUnitMapping atau belum. 
                                Dim counter As Int32 = AccessAccountOwnerWorkingUnitMapping.CountMatchingAccountOwnerWorkingUnitMapping(rowData.AccountOwnerID)

                                'Jika Counter = 0 berarti AccountOwnerWorkingUnitMapping tsb sdh tdk ada dlm tabel AccountOwnerWorkingUnitMapping dan tdk bisa dihapus
                                If counter = 0 Then
                                    Throw New Exception("Cannot delete Account Owner Working Unit Mapping for the following Account Owner : " & rowData.AccountOwnerName & " because that Account Owner Working Unit Mapping no longer exists in the database.")
                                Else 'Jika Counter != 0 berarti AccountOwnerWorkingUnitMapping tsb msh ada dlm tabel AccountOwnerWorkingUnitMapping dan bisa dihapus
                                    'Hapus AccountOwnerWorkingUnitMapping untuk Account Owner tsb
                                    AccessAccountOwnerWorkingUnitMapping.DeleteAccountOwnerWorkingUnitMapping(rowData.AccountOwnerID)

                                    'catat aktifitas dalam tabel Audit Trail
                                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "AccountOwnerID", "Delete", rowData.AccountOwnerID_Old, rowData.AccountOwnerID, "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "WorkingUnitID", "Delete", rowData.WorkingUnitID_Old, rowData.WorkingUnitID, "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "CreatedDate", "Delete", rowData.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "LastUpdateDate", "Delete", rowData.LastUpdateDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                                        'Ubah AccountOwnerWorkingUnitMappingStatus dari True menjadi False 
                                        Using AccessAccountOwner As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
                                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAccountOwner, oSQLTrans)
                                            AccessAccountOwner.UpdateAccountOwnerWorkingUnitMappingStatus(rowData.AccountOwnerID, "False")

                                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "AccountOwner", "AccountOwnerWorkingUnitMappingStatus", "Edit", "True", "False", "Accepted")
                                        End Using

                                        'hapus item tsb dalam tabel Parameters_Approval
                                        AccessParametersApproval.DeleteParametersApproval(Me.ParametersPendingApprovalID)

                                        'hapus item tsb dalam tabel LoginParameter_Approval
                                        AccessPending.DeleteAccountOwnerWorkingUnitMappingApproval(rowData.ApprovalID)

                                        'hapus item tsb dalam tabel Parameters_PendingApproval
                                        AccessParametersPendingApproval.DeleteParametersPendingApproval(Me.ParametersPendingApprovalID)
                                    End Using

                                    oSQLTrans.Commit()
                                End If
                            Else 'Bila ObjTable.Rows.Count = 0 berarti AccountOwnerWorkingUnitMapping tsb sudah tidak lagi berada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                                Throw New Exception("Cannot delete Account Owner Working Unit Mapping for the following Account Owner : " & Me.LabelAccountOwnerDelete.Text & " because that Account Owner Working Unit Mapping is no longer in the approval table.")
                            End If
                        End Using
                    End Using
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

#End Region
#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject user add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectAccountOwnerWorkingUnitMappingAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMapping_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                        Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                            Using TableAccountOwnerWorkingUnitMapping As Data.DataTable = AccessPending.GetAccountOwnerWorkingUnitMappingApprovalDetailData(Me.AccountOwnerWorkingUnitMappingApprovalID)

                                'Bila ObjTable.Rows.Count > 0 berarti AccountOwnerWorkingUnitMapping tsb masih ada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                                If TableAccountOwnerWorkingUnitMapping.Rows.Count > 0 Then
                                    Dim ObjRow As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMapping_ApprovalRow = TableAccountOwnerWorkingUnitMapping.Rows(0)

                                    'catat aktifitas dalam tabel Audit Trail
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "AccountOwnerID", "Add", "", ObjRow.AccountOwnerID, "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "WorkingUnitID", "Add", "", ObjRow.WorkingUnitID, "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "CreatedDate", "Add", "", ObjRow.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "LastUpdateDate", "Add", "", ObjRow.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                                    'hapus item tersebut dalam tabel AccountOwnerWorkingUnitMapping
                                    AccessPending.DeleteAccountOwnerWorkingUnitMappingApproval(Me.AccountOwnerWorkingUnitMappingApprovalID)

                                    'hapus item tersebut dalam tabel Parameters_Approval
                                    AccessParametersApproval.DeleteParametersApproval(Me.ParametersPendingApprovalID)

                                    'hapus item tersebut dalam tabel Parameters_PendingApproval
                                    AccessParametersPendingApproval.DeleteParametersPendingApproval(Me.ParametersPendingApprovalID)

                                    oSQLTrans.Commit()
                                Else 'Bila ObjTable.Rows.Count = 0 berarti AccountOwnerWorkingUnitMapping tsb sudah tidak lagi berada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                                    Throw New Exception("Operation failed. The following Account Owner: " & Me.LabelWorkingUnitAdd.Text & " is no longer in the approval table.")
                                End If
                            End Using
                        End Using
                    End Using
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject user edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectAccountOwnerWorkingUnitMappingEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMapping_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                        Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                            Using TableAccountOwnerWorkingUnitMapping As Data.DataTable = AccessPending.GetAccountOwnerWorkingUnitMappingApprovalDetailData(Me.AccountOwnerWorkingUnitMappingApprovalID)

                                'Bila ObjTable.Rows.Count > 0 berarti AccountOwnerWorkingUnitMapping tsb masih ada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                                If TableAccountOwnerWorkingUnitMapping.Rows.Count > 0 Then
                                    Dim ObjRow As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMapping_ApprovalRow = TableAccountOwnerWorkingUnitMapping.Rows(0)

                                    'catat aktifitas dalam tabel Audit Trail
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "AccountOwnerID", "Edit", ObjRow.AccountOwnerID_Old, ObjRow.AccountOwnerID, "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "WorkingUnitID", "Edit", ObjRow.WorkingUnitID_Old, ObjRow.WorkingUnitID, "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "CreatedDate", "Edit", ObjRow.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "LastUpdateDate", "Edit", ObjRow.LastUpdateDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                                    'hapus item tersebut dalam tabel AccountOwnerWorkingUnitMapping
                                    AccessPending.DeleteAccountOwnerWorkingUnitMappingApproval(Me.AccountOwnerWorkingUnitMappingApprovalID)

                                    'hapus item tersebut dalam tabel Parameters_Approval
                                    AccessParametersApproval.DeleteParametersApproval(Me.ParametersPendingApprovalID)

                                    'hapus item tersebut dalam tabel Parameters_PendingApproval
                                    AccessParametersPendingApproval.DeleteParametersPendingApproval(Me.ParametersPendingApprovalID)

                                    oSQLTrans.Commit()
                                Else 'Bila ObjTable.Rows.Count = 0 berarti AccountOwnerWorkingUnitMapping tsb sudah tidak lagi berada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                                    Throw New Exception("Operation failed. The following Account Owner: " & Me.LabelWorkingUnitAdd.Text & " is no longer in the approval table.")
                                End If
                            End Using
                        End Using
                    End Using
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject user delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    30/04/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectAccountOwnerWorkingUnitMappingDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMapping_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                        Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                            Using TableAccountOwnerWorkingUnitMapping As Data.DataTable = AccessPending.GetAccountOwnerWorkingUnitMappingApprovalDetailData(Me.AccountOwnerWorkingUnitMappingApprovalID)

                                'Bila ObjTable.Rows.Count > 0 berarti AccountOwnerWorkingUnitMapping tsb masih ada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                                If TableAccountOwnerWorkingUnitMapping.Rows.Count > 0 Then
                                    Dim ObjRow As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMapping_ApprovalRow = TableAccountOwnerWorkingUnitMapping.Rows(0)

                                    'catat aktifitas dalam tabel Audit Trail
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "AccountOwnerID", "Delete", ObjRow.AccountOwnerID_Old, ObjRow.AccountOwnerID, "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "WorkingUnitID", "Delete", ObjRow.WorkingUnitID_Old, ObjRow.WorkingUnitID, "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "CreatedDate", "Delete", ObjRow.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "LastUpdateDate", "Delete", ObjRow.LastUpdateDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                                    'hapus item tersebut dalam tabel AccountOwnerWorkingUnitMapping
                                    AccessPending.DeleteAccountOwnerWorkingUnitMappingApproval(Me.AccountOwnerWorkingUnitMappingApprovalID)

                                    'hapus item tersebut dalam tabel Parameters_Approval
                                    AccessParametersApproval.DeleteParametersApproval(Me.ParametersPendingApprovalID)

                                    'hapus item tersebut dalam tabel Parameters_PendingApproval
                                    AccessParametersPendingApproval.DeleteParametersPendingApproval(Me.ParametersPendingApprovalID)

                                    oSQLTrans.Commit()
                                Else 'Bila ObjTable.Rows.Count = 0 berarti AccountOwnerWorkingUnitMapping tsb sudah tidak lagi berada dlm tabel AccountOwnerWorkingUnitMapping_Approval
                                    Throw New Exception("Operation failed. The following Account Owner: " & Me.LabelWorkingUnitAdd.Text & " is no longer in the approval table.")
                                End If
                            End Using
                        End Using
                    End Using
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HideAll()
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.AccountOwnerWorkingUnitMappingAdd
                        Me.LoadAccountOwnerWorkingUnitMappingAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.AccountOwnerWorkingUnitMappingEdit
                        Me.LoadAccountOwnerWorkingUnitMappingEdit()
                    Case Sahassa.AML.Commonly.TypeConfirm.AccountOwnerWorkingUnitMappingDelete
                        Me.LoadAccountOwnerWorkingUnitMappingDelete()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.AccountOwnerWorkingUnitMappingAdd
                    Me.AcceptAccountOwnerWorkingUnitMappingAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.AccountOwnerWorkingUnitMappingEdit
                    Me.AcceptAccountOwnerWorkingUnitMappingEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.AccountOwnerWorkingUnitMappingDelete
                    Me.AcceptAccountOwnerWorkingUnitMappingDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "AccountOwnerWorkingUnitMappingManagementApproval.aspx"

            Me.Response.Redirect("AccountOwnerWorkingUnitMappingManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.AccountOwnerWorkingUnitMappingAdd
                    Me.RejectAccountOwnerWorkingUnitMappingAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.AccountOwnerWorkingUnitMappingEdit
                    Me.RejectAccountOwnerWorkingUnitMappingEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.AccountOwnerWorkingUnitMappingDelete
                    Me.RejectAccountOwnerWorkingUnitMappingDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "AccountOwnerWorkingUnitMappingManagementApproval.aspx"

            Me.Response.Redirect("AccountOwnerWorkingUnitMappingManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "AccountOwnerWorkingUnitMappingManagementApproval.aspx"

        Me.Response.Redirect("AccountOwnerWorkingUnitMappingManagementApproval.aspx", False)
    End Sub
End Class