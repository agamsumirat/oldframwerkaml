Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper
Partial Class UserWorkingUnitAssignmentView
    Inherits Parent

#Region " Property "
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("MsUserWorkingUnitAssignmentViewSelected") Is Nothing, New ArrayList, Session("MsUserWorkingUnitAssignmentViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("MsUserWorkingUnitAssignmentViewSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("MsUserWorkingUnitAssignmentViewFieldSearch") Is Nothing, "", Session("MsUserWorkingUnitAssignmentViewFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsUserWorkingUnitAssignmentViewFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("MsUserWorkingUnitAssignmentViewValueSearch") Is Nothing, "", Session("MsUserWorkingUnitAssignmentViewValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsUserWorkingUnitAssignmentViewValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("MsUserWorkingUnitAssignmentViewSort") Is Nothing, "UserId  asc", Session("MsUserWorkingUnitAssignmentViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("MsUserWorkingUnitAssignmentViewSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("MsUserWorkingUnitAssignmentViewCurrentPage") Is Nothing, 0, Session("MsUserWorkingUnitAssignmentViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsUserWorkingUnitAssignmentViewCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("MsUserWorkingUnitAssignmentViewRowTotal") Is Nothing, 0, Session("MsUserWorkingUnitAssignmentViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsUserWorkingUnitAssignmentViewRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("MsUserWorkingUnitAssignmentViewData") Is Nothing, New AMLDAL.AMLDataSet.UserDataTable, Session("MsUserWorkingUnitAssignmentViewData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsUserWorkingUnitAssignmentViewData") = value
        End Set
    End Property

    ''' <summary>
    ''' get id for focues
    ''' </summary>
    ''' <value></value>
    ''' <returns>element control id</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TextSearch.ClientID
        End Get
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' fill search
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("User ID", "UserID Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Status", "Status Like '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("MsUserWorkingUnitAssignmentViewSelected") = Nothing
        Session("MsUserWorkingUnitAssignmentViewFieldSearch") = Nothing
        Session("MsUserWorkingUnitAssignmentViewValueSearch") = Nothing
        Session("MsUserWorkingUnitAssignmentViewSort") = Nothing
        Session("MsUserWorkingUnitAssignmentViewCurrentPage") = Nothing
        Session("MsUserWorkingUnitAssignmentViewRowTotal") = Nothing
        Session("MsUserWorkingUnitAssignmentViewData") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()
                Me.ComboSearch.Attributes.Add("onchange", "javascript:CheckNoPopUp('" & Me.TextSearch.ClientID & "');")

                Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Me.SetnGetBindTable = AccessUser.GetUserWorkingUnitAssignmentData
                End Using
                Me.FillSearch()
                Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Dim Rows() As AMLDAL.AMLDataSet.UserRow = Me.SetnGetBindTable.Select(Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), Me.SetnGetSort)
        Me.GridMSUserView.DataSource = Rows
        Me.SetnGetRowTotal = Rows.Length
        Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridMSUserView.DataBind()
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
            End If
            Me.SetnGetValueSearch = Me.TextSearch.Text
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cek if userid already edited
    ''' </summary>
    ''' <param name="strUserId"></param>
    ''' <returns></returns>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Function CekIsEdited(ByVal strUserId As String) As Boolean
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.CountUserWorkingUnitAssignmentApprovalByUserIdTableAdapter
            Dim count As Int32 = AccessPending.GetData(strUserId).Rows(0)("jml")
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid edit command handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Dim strUserId As String = e.Item.Cells(1).Text
        Try
            If e.Item.Cells(1).Text.ToLower = "superuser" Then
                Throw New Exception("Super User Can't be Editted")
            Else
                If Me.CekIsEdited(strUserId) = False Then
                    Sahassa.AML.Commonly.SessionIntendedPage = "UserWorkingUnitAssignment.aspx?UserID=" & strUserId

                    Me.Response.Redirect("UserWorkingUnitAssignment.aspx?UserID=" & strUserId, False)
                Else
                    Throw New Exception("Cannot edit the following User ID: '" & strUserId & "' because it is currently waiting for approval")
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' is deleted
    ''' </summary>
    ''' <param name="strUserId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsDeleted(ByVal strUserId As String) As Boolean
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.CountUserWorkingUnitAssignmentApprovalByUserIdTableAdapter
            Dim count As Int32 = AccessPending.GetData(strUserId).Rows(0)("jml")
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function

    Private Sub DeleteUserWorkingUnitAssignmentSU(ByVal strUserId As String)
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            Dim countAuditTrail As Integer

            Dim UserWorkingUnitAssignmentTable As AMLDAL.AMLDataSet.UserWorkingUnitAssignmentDataTable

            Using AccessUserWorkingUnitAssignment As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignmentTableAdapter
                UserWorkingUnitAssignmentTable = AccessUserWorkingUnitAssignment.GetWorkingUnitsByUserID(strUserId)

                countAuditTrail = (4 * UserWorkingUnitAssignmentTable.Rows.Count)
            End Using

            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(countAuditTrail)

            Dim CurrentUserID As String = Sahassa.AML.Commonly.SessionUserId

            '            Using TransScope As New Transactions.TransactionScope
            Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAuditTrail, Data.IsolationLevel.ReadUncommitted)
                Using AccessUserWorkingUnitAssignment As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignmentTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessUserWorkingUnitAssignment, oSQLTrans)

                    For Each UserWorkingUnitAssignmentRow As AMLDAL.AMLDataSet.UserWorkingUnitAssignmentRow In UserWorkingUnitAssignmentTable.Rows
                        AccessAuditTrail.Insert(Now, CurrentUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "UserID", "Delete", strUserId, "", "Accepted")
                        AccessAuditTrail.Insert(Now, CurrentUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "WorkingUnitID", "Delete", UserWorkingUnitAssignmentRow.WorkingUnitID, "", "Accepted")
                        AccessAuditTrail.Insert(Now, CurrentUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "CreatedDate", "Delete", UserWorkingUnitAssignmentRow.CreatedDate, "", "Accepted")
                        AccessAuditTrail.Insert(Now, CurrentUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "LasUpdateDate", "Delete", UserWorkingUnitAssignmentRow.LastUpdateDate, "", "Accepted")
                        AccessUserWorkingUnitAssignment.DeleteUserWorkingUnitAssignment(strUserId, UserWorkingUnitAssignmentRow.WorkingUnitID)
                    Next

                    Using AccessChangeStatus As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessChangeStatus, oSQLTrans)
                        AccessChangeStatus.UpdateStatusUser(strUserId, False)
                    End Using
                End Using
            End Using

            oSQLTrans.Commit()

            Me.Response.Redirect("UserWorkingUnitAssignmentView.aspx", False)
            '            End Using

        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try

    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid delete event handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.DeleteCommand
        Dim strUserId As String = e.Item.Cells(1).Text
        Try
            If strUserId.ToLower = "superuser" Then
                Throw New Exception("Super User cannot be deleted.")
            Else

                If Me.IsDeleted(strUserId) = False Then
                    If e.Item.Cells(2).Text.ToLower <> "unassigned" Then
                        'Jika bukan SuperUser yg melakukan
                        If Sahassa.AML.Commonly.SessionPkUserId <> 1 Then
                            Using AccessGetUWUA As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignmentTableAdapter
                                Using dtUWUA As AMLDAL.AMLDataSet.UserWorkingUnitAssignmentDataTable = AccessGetUWUA.GetData
                                    For Each row As AMLDAL.AMLDataSet.UserWorkingUnitAssignmentRow In dtUWUA.Select("UserID='" & e.Item.Cells(1).Text & "'")

                                        Dim IdApprovalUWUA As Integer
                                        Using AccessInsertApprovalUWUA As New AMLDAL.AMLDataSetTableAdapters.InsertUserWorkingUnitAssignmentApprovalTableAdapter
                                            IdApprovalUWUA = CInt(AccessInsertApprovalUWUA.InsertUserWorkingUnitAssignmentApproval(e.Item.Cells(1).Text, row.WorkingUnitID, row.CreatedDate, row.LastUpdateDate, "", Nothing, Nothing, Nothing, Sahassa.AML.Commonly.SessionUserId).Rows(0)(0))
                                        End Using

                                        Using AccessInsertParameterPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                                            Dim IdPendingApproval As Integer = CInt(AccessInsertParameterPendingApproval.InsertParameters_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, Sahassa.AML.Commonly.TypeMode.Delete))

                                            Using AccessInsertParameterApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                                                AccessInsertParameterApproval.Insert(IdPendingApproval, Sahassa.AML.Commonly.TypeMode.Delete, 8, IdApprovalUWUA)
                                            End Using
                                        End Using
                                    Next
                                End Using
                            End Using

                            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=8863&Identifier=" & e.Item.Cells(1).Text

                            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=8863&Identifier=" & e.Item.Cells(1).Text, False)
                        Else 'Jika SuperUser yg melakukan
                            DeleteUserWorkingUnitAssignmentSU(strUserId)
                        End If
                    Else
                        Throw New Exception("Cannot delete the following UserID: '" & strUserId & "' because working unit is not available.")
                    End If
                Else
                    Throw New Exception("Cannot delete the following UserID: '" & strUserId & "' because it is currently waiting for approval")
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                Dim LinkDelete As LinkButton = e.Item.Cells(e.Item.Cells.Count - 1).Controls(0)
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)

                If Sahassa.AML.Commonly.SessionPkUserId <> 1 Then
                    Dim StrDelMessage As String = "javascript:return confirm('"
                    StrDelMessage += "Klik OK akan membuat User Id \'" & e.Item.Cells(1).Text.Replace("\", "\\").Replace("'", "\'") & "\' masuk ke dalam list pending approval"
                    StrDelMessage += "?')"
                    LinkDelete.Attributes.Add("onclick", StrDelMessage)
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim UserId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(UserId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(UserId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(UserId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()
        Dim Rows As New ArrayList
        For Each IdPk As String In Me.SetnGetSelectedItem
            Dim rowData() As AMLDAL.AMLDataSet.UserRow = Me.SetnGetBindTable.Select("UserId = '" & IdPk & "'")
            If rowData.Length > 0 Then
                Rows.Add(rowData(0))
            End If
        Next
        Me.GridMSUserView.DataSource = Rows
        Me.GridMSUserView.AllowPaging = False
        Me.GridMSUserView.DataBind()

        'Sembunyikan kolom ke 0, 3 & 4 agar tidak ikut diekspor ke excel
        Me.GridMSUserView.Columns(0).Visible = False
        Me.GridMSUserView.Columns(3).Visible = False
        Me.GridMSUserView.Columns(4).Visible = False
    End Sub

    ''' <summary>
    ''' export button 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=UserWorkingUnitAssignmentView.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim UserID As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(UserID) = False Then
                        ArrTarget.Add(UserID)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(UserID) Then
                        ArrTarget.Remove(UserID)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub
End Class