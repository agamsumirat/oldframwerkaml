<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TransactionTypeAuxiliaryMappingEdit.aspx.vb" Inherits="TransactionTypeAuxiliaryMappingEdit" MasterPageFile="~/MasterPage.master" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
  <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/dot_title.gif" />
                <strong>Transaction Type Auxiliary Transaction Code Mapping - Edit&nbsp; </strong>
            </td>
        </tr>
    </table>
    <TABLE borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
        <TR>
            <TD align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></TD>
				<TD vAlign="top" width="98%" bgColor="#ffffff">
                    <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="1" cellspacing="1"
                        width="100%">
                        <tr style="background-color: #ffffff">
                            <td style="width: 30%">
                                Auxiliary Transaction Code</td>
                            <td style="width: 1%">
                                :</td>
                            <td style="width: 69%">
                                &nbsp;<asp:Label ID="LblTransType" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td style="width: 30%">
                                Transaction Type</td>
                            <td style="width: 1%">
                                :</td>
                            <td style="width: 69%">
                                &nbsp;<asp:DropDownList ID="CboTransType" runat="server" CssClass="searcheditbox" Width="225px"></asp:DropDownList>
                            </td>
                        </tr>
                    </table>                				
				</TD>
			</TR>
			<TR>
                <TD align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></TD>
                <td>
                    <asp:ImageButton ID="ImageButtonSave" runat="server" ImageUrl="~/Images/button/save.gif" />&nbsp;
                    <asp:ImageButton ID="ImageButtonCancel" runat="server" ImageUrl="~/Images/button/cancel.gif" />
                </td>
            </TR>
	</TABLE>
    <ajax:ajaxpanel id="AjaxPanel3" runat="server">
        <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
    </ajax:ajaxpanel>  

</asp:Content>