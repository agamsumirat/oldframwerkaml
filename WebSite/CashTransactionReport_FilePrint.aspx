<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CashTransactionReport_FilePrint.aspx.vb" Inherits="CashTransactionReport_FilePrint" title="CTR Print" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <rsweb:ReportViewer ID="reportViewer_content" runat="server" width="100%" height="800px" Font-Names="Verdana" Font-Size="8pt">
    <LocalReport ReportPath="CTR.rdlc" EnableExternalImages="True">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="CTRReport_pAccountNameGetter" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="CTRReport_pIdentityNumberGetter" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource3" Name="CTRReport_pAccountTypeGetter" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource4" Name="CTRReport_pAccountNumberGetter" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource5" Name="CTRReport_pNPWPGetter" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource6" Name="CTRReport_pIdentityTypeGetter" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource7" Name="CTRReport_pBranchNameGetter" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource8" Name="CTRReport_pReportingOfficerGetter" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource9" Name="CTRReport_pTransactionTypeGetter" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource10" Name="CTRReport_pAddressGetter" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource11" Name="CTRReport_pDOBGetter" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource12" Name="CTRReport_pBusinessGetter" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource13" Name="CTRReport_pKTPReader" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource14" Name="CTRReport_pSIMReader" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource15" Name="CTRReport_pPassporReader" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource16" Name="CTRReport_pKITASReader" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource16" runat="server" SelectMethod="GetKITAS"
        TypeName="AMLDAL.CTRReportTableAdapters.pKITASReaderTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="key" QueryStringField="key" Type="string" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource15" runat="server" SelectMethod="GetPassport"
        TypeName="AMLDAL.CTRReportTableAdapters.pPassporReaderTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="key" QueryStringField="key" Type="string" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource14" runat="server" SelectMethod="GetSIM"
        TypeName="AMLDAL.CTRReportTableAdapters.pSIMReaderTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="key" QueryStringField="key" Type="string" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource13" runat="server" SelectMethod="GetKTP"
        TypeName="AMLDAL.CTRReportTableAdapters.pKTPReaderTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="key" QueryStringField="key" Type="string" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource12" runat="server" SelectMethod="GetBusiness"
        TypeName="AMLDAL.CTRReportTableAdapters.pBusinessGetterTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="key" QueryStringField="key" Type="string" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource11" runat="server" SelectMethod="GetDOB"
        TypeName="AMLDAL.CTRReportTableAdapters.pDOBGetterTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="key" QueryStringField="key" Type="string" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource10" runat="server" SelectMethod="GetAddress"
        TypeName="AMLDAL.CTRReportTableAdapters.pAddressGetterTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="key" QueryStringField="key" Type="string" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource9" runat="server" SelectMethod="GetTransactionData"
        TypeName="AMLDAL.CTRReportTableAdapters.pTransactionTypeGetterTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="key" QueryStringField="key" Type="string" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource8" runat="server" SelectMethod="GetReportingOfficerName"
        TypeName="AMLDAL.CTRReportTableAdapters.pReportingOfficerGetterTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="key" QueryStringField="key" Type="string" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource7" runat="server" SelectMethod="GetBranchNameData"
        TypeName="AMLDAL.CTRReportTableAdapters.pBranchNameGetterTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="key" QueryStringField="key" Type="string" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource6" runat="server" SelectMethod="GetIdentityType"
        TypeName="AMLDAL.CTRReportTableAdapters.pIdentityTypeGetterTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter Name="key" QueryStringField="key" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource5" runat="server" SelectMethod="GetNPWP"
        TypeName="AMLDAL.CTRReportTableAdapters.pNPWPGetterTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="key" QueryStringField="key" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" SelectMethod="GetAccountNumber"
        TypeName="AMLDAL.CTRReportTableAdapters.pAccountNumberGetterTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="key" QueryStringField="key" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" SelectMethod="GetAccountType"
        TypeName="AMLDAL.CTRReportTableAdapters.pAccountTypeGetterTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="key" QueryStringField="key" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetIdentityNumber"
        TypeName="AMLDAL.CTRReportTableAdapters.pIdentityNumberGetterTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="key" QueryStringField="key" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetAccountName"
        TypeName="AMLDAL.CTRReportTableAdapters.pAccountNameGetterTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="key" QueryStringField="key" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

