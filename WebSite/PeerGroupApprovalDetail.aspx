<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="PeerGroupApprovalDetail.aspx.vb" Inherits="PeerGroupApprovalDetail" title="Peer Group - Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
	
    <TABLE id="TableDetailAdd" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="0" runat="server">
	    <TR class="formText" id="UserAdd1">
		    <TD bgColor="#ffffff" height="24" style="width: 22px">&nbsp;</TD>
		    <TD bgColor="#ffffff" style="width: 112px" nowrap>
                Peer Group Name</TD>
		    <TD bgColor="#ffffff" style="width=1px">:</TD>
		    <TD width="99%" bgColor="#ffffff"><asp:label id="LabelPeerGroupNameAdd" runat="server"></asp:label></TD>
	    </TR>
	    <tr class="formText" >
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff" style="width: 112px">
                Description
            </td>
		    <td bgcolor="#ffffff"  style="width=1px">:</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelDescriptionAdd" runat="server" Width="100%"></asp:Label></td>
	    </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff" style="width: 112px">
                Enabled</td>
            <td bgcolor="#ffffff" style=" width=1px">:</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelEnabledAdd" runat="server" Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff" style="width: 112px">
                Expression</td>
            <td bgcolor="#ffffff" style="width=1px">:</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelSQLExpressionAdd" runat="server" Width="100%"></asp:Label></td>
        </tr>
        </Table>
        <Table id="TableDetailEdit" runat="server" cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="0">
	    <tr class="formText">
		    <td height="24" colspan="4" bgcolor="#ffffcc">&nbsp;Old Values</td>
		    <td colspan="4" bgcolor="#ffffcc">&nbsp;New Values</td>
	    </tr>
	    <tr class="formText" id="UserEdi2">
		    <td bgcolor="#ffffff" style="width: 22px; height: 32px">&nbsp;</td>
		    <td bgcolor="#ffffff" style="height: 32px;" nowrap>
                Peer Group Name</td>
		    <td bgcolor="#ffffff" style=" height: 32px; width =1px">:</td>
		    <td width="40%" bgcolor="#ffffff" style="height: 32px"><asp:label id="LabelPeerGroupNameEditOld" runat="server"></asp:label></td>
		    <td width="5" bgcolor="#ffffff" style="height: 32px">&nbsp;</td>
		    <td width="10%" bgcolor="#ffffff" style="height: 32px">
                Peer Group Name</td>
		    <td width="5" bgcolor="#ffffff" style="height: 32px; width =1px">:</td>
		    <td width="40%" bgcolor="#ffffff" style="height: 32px"><asp:label id="LabelPeerGroupNameEditNew" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserEdi3">
		    <td bgcolor="#ffffff" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Description</td>
		    <td bgcolor="#ffffff" style=" width =1px">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelDescriptionEditOld" runat="server"></asp:label></td>
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Description</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelDescriptionEditNew" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserEdi4">
		    <td bgcolor="#ffffff" style="width: 22px; height: 32px;">&nbsp;</td>
		    <td bgcolor="#ffffff" style="height: 32px">
                Enabled<br />
               
            </td>
		    <td bgcolor="#ffffff" style="; width =1px; height: 32px;">:
              
            </td>
		    <td bgcolor="#ffffff" style="height: 32px">
                <asp:Label ID="LabelEnabledEditOld" runat="server"></asp:Label></td>
		    <td bgcolor="#ffffff" style="height: 32px">&nbsp;</td>
		    <td bgcolor="#ffffff" style="height: 32px">
                Enabled</td>
		    <td bgcolor="#ffffff" style="width=1px; height: 32px;" >:
            </td>
		    <td bgcolor="#ffffff" style="height: 32px">
                <asp:Label ID="LabelEnabledEditNew" runat="server"></asp:Label></td>
	    </tr>
            <tr class="formText">
                <td bgcolor="#ffffff" style="width: 22px; height: 32px">
                </td>
                <td bgcolor="#ffffff" style="height: 32px">
                    Expression</td>
                <td bgcolor="#ffffff" style="width: 1px; height: 32px">
                    :</td>
                <td bgcolor="#ffffff" style="height: 32px">
                    <asp:Label ID="LabelSQLExpressionEditOld" runat="server"></asp:Label></td>
                <td bgcolor="#ffffff" style="height: 32px">
                </td>
                <td bgcolor="#ffffff" style="height: 32px">
                    Expression</td>
                <td bgcolor="#ffffff" style="width: 1px; height: 32px">
                    :</td>
                <td bgcolor="#ffffff" style="height: 32px">
                    <asp:Label ID="LabelSQLExpressionEditNew" runat="server"></asp:Label></td>
            </tr>
            </table>
            <table id="TableDetailDelete" runat="server"  cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="0"  >
	    <TR class="formText" >
		    <TD bgColor="#ffffff" style="height: 23px; width: 22px;">&nbsp;</TD>
		    <TD bgColor="#ffffff" style="height: 23px;" nowrap>
                Peer Group Name</TD>
		    <TD bgColor="#ffffff" style="height: 23px; width: 1px;">:</TD>
		    <TD width="80%" bgColor="#ffffff" style="height: 23px"><asp:label id="LabelPeerGroupNameDelete" runat="server"></asp:label></TD>
	    </TR>
	    <tr class="formText" >
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Description</td>
		    <td bgcolor="#ffffff" style="width: 1px">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelDescriptionDelete" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" >
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Enabled</td>
		    <td bgcolor="#ffffff" style="width: 1px">:
            </td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelEnabledDelete" runat="server"></asp:Label></td>
	    </tr>
                <tr class="formText">
                    <td bgcolor="#ffffff" height="24" style="width: 22px">
                    </td>
                    <td bgcolor="#ffffff">
                        Expression</td>
                    <td bgcolor="#ffffff" style="width: 1px">
                        :</td>
                    <td bgcolor="#ffffff">
                        <asp:Label ID="LabelSQLExpressionDelete" runat="server"></asp:Label></td>
                </tr>
	    </table>
	    <table width ="100%">
	    <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="False" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="none"></asp:CustomValidator></TD>
	    </TR>
    </TABLE>

</asp:Content>

