<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false"
    CodeFile="CDDLNPQuestionApprovalDetail.aspx.vb" Inherits="CDDLNPQuestionApprovalDetail"
    Title="CDD Question - Approval Detail" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>
                    <img height="17" src="Images/dot_title.gif" width="17" />
                    <asp:Label ID="lblHeader" runat="server" Text="EDD Question Approval Detail"></asp:Label>&nbsp;
                    <hr />
                </strong>
            </td>
        </tr>
    </table>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none">
        <tr class="formText" height="20">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" width="20%">
                Requested By</td>
            <td bgcolor="#ffffff" width="5px">
                :</td>
            <td bgcolor="#ffffff" colspan="2" width="80%">
                <asp:Label ID="lblRequestedBy" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" height="20">
            <td bgcolor="#ffffff">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                Requested Date</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:Label ID="lblRequestedDate" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" height="20">
            <td bgcolor="#ffffff">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                Action</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:Label ID="lblAction" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff" colspan="5">
                &nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" colspan="4" width="50%" valign="top">
                <table bordercolor="#dddddd" cellspacing="1" cellpadding="2" width="100%" bgcolor="#ffffff"
                    border="2" height="72" style="border-top-style: none; border-right-style: none;
                    border-left-style: none; border-bottom-style: none">
                    <tr valign="top">
                        <td width="20%">
                        </td>
                        <td width="40%">
                            <strong>Old Data</strong></td>
                        <td width="40%">
                            <strong>New Data</strong></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            Question
                        </td>
                        <td>
                            <asp:Label ID="lblOldQuestion" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblNewQuestion" runat="server"></asp:Label></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            Question Type</td>
                        <td>
                            <asp:Label ID="lblOldQuestionType" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblNewQuestionType" runat="server"></asp:Label></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            Parent Question</td>
                        <td>
                            <asp:Label ID="lblOldParentQuestion" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblNewParentQuestion" runat="server"></asp:Label></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            Is Required</td>
                        <td>
                            <asp:Label ID="lblOldIsRequired" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblNewIsRequired" runat="server"></asp:Label></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            Question Sequence</td>
                        <td>
                            <asp:Label ID="lblOldQuestionSequence" runat="server"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblNewQuestionSequence" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            Question Detail</td>
                        <td>
                            <asp:DataGrid ID="gridOldQuestionDetail" runat="server" AutoGenerateColumns="False"
                                BackColor="White" BorderColor="#DEDFDE" CellPadding="4" Font-Size="XX-Small"
                                ForeColor="Black" GridLines="Vertical" Width="100%">
                                <FooterStyle BackColor="#CCCC99" />
                                <AlternatingItemStyle BackColor="White" />
                                <ItemStyle BackColor="#F7F7DE" />
                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:BoundColumn DataField="QuestionDetail" HeaderText="Question Detail" SortExpression="QuestionDetail  asc">
                                        <HeaderStyle ForeColor="white" Width="35%" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="FK_CDD_VAlidation_ID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Validation">
                                        <HeaderStyle ForeColor="white" Width="15%" />
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                        <td>
                            <asp:DataGrid ID="gridNewQuestionDetail" runat="server" AutoGenerateColumns="False"
                                BackColor="White" BorderColor="#DEDFDE" CellPadding="4" Font-Size="XX-Small"
                                ForeColor="Black" GridLines="Vertical" Width="100%">
                                <FooterStyle BackColor="#CCCC99" />
                                <AlternatingItemStyle BackColor="White" />
                                <ItemStyle BackColor="#F7F7DE" />
                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:BoundColumn DataField="QuestionDetail" HeaderText="Question Detail" SortExpression="QuestionDetail  asc">
                                        <HeaderStyle ForeColor="white" Width="35%" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="FK_CDD_VAlidation_ID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Validation">
                                        <HeaderStyle ForeColor="white" Width="15%" />
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="formText" bgcolor="#dddddd" height="30">
            <td style="width: 5px">
                <img height="15" src="images/arrow.gif" width="15"></td>
            <td colspan="6" style="height: 9px">
                <table cellspacing="0" cellpadding="3" border="0">
                    <tr>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                                <asp:ImageButton ID="ImageAccept" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Accept.gif" /></ajax:AjaxPanel></td>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                <asp:ImageButton ID="ImageReject" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Reject.gif">
                                </asp:ImageButton></ajax:AjaxPanel></td>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                <asp:ImageButton ID="ImageCancel" runat="server" ImageUrl="~/Images/Button/Cancel.gif">
                                </asp:ImageButton></ajax:AjaxPanel></td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator></td>
        </tr>
    </table>
</asp:Content>
