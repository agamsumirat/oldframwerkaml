<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustomTransactionAnalysis.aspx.vb" Inherits="CustomTransactionAnalysis" MasterPageFile="~/MasterPage.master" title="Custom Transaction Analysis"%> 
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="ContentAccountInformation" ContentPlaceHolderID="cpContent" runat="server">
    <ajax:AjaxPanel ID="PanelAccountInformationDetail" runat="server" Width="749px">
        <table id="TABLE1" bgcolor="#dddddd" border="2" bordercolor="#fffff" cellpadding="0"
            cellspacing="0" width="99%">
            <tr>
                <td style="width: 5px; height: 30px">
                </td>
                <td style="width: 466px; height: 30px; background-color: #ffffff" valign="top">
                    <table width="100%">
                        <tr>
                            <td style="width: 1%">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/dot_title.gif" /></td>
                            <td style="width: 100px">
                                <strong style="font-size: medium; width: 50%">
                                    <asp:Label ID="Label1" runat="server" CssClass="MainTitle" Text="CUSTOM TRANSACTION ANALYSIS"
                                        Width="400px"></asp:Label></strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 5px">
                </td>
                <td style="background-color: #ffffff; width: 466px;" valign="top">
                    <table cellpadding="0" cellspacing="0" width="99%">
                        <tr>
                            <td style="width: 676px">
                                &nbsp;<table class="tabContents">
                                    <tr>
                                        <td style="width: 714px">
                                                    <table class="TabArea" width="100%">
                                                        <tr id="Tr13" runat="server">
                                                            <td id="TdGeneralAvailable" runat="server" align="left" class="maintitle" style="width: 315px">
                                                                SEARCH</td>
                                                        </tr>
                                                        <tr id="Tr14" runat="server">
                                                            <td id="TdGeneralNotAvailable" runat="server" align="left" style="color: red; width: 315px; height: 15px;">
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextDateFrom"
                                                                    ErrorMessage="Tanggal harus diisi" Width="117px"></asp:RequiredFieldValidator></td>
                                                        </tr>
                                                        <tr id="Tr15" runat="server">
                                                            <td id="TdDataGeneralAvailable" runat="server" style="width: 315px">
                                                                <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="1" cellspacing="1" style="width: 208%">
                                                                    
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                                                            Date <span style="color: #ff0000">*</span></td>
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="5">
                                                                            :</td>
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                                                            &nbsp;<asp:TextBox ID="TextDateFrom" runat="server" Width="96px" CssClass="searcheditbox"></asp:TextBox>
                                                                            <input id="popUpFromDate" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                                border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                                border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                                height: 17px" title="Click to show calendar" type="button" />
                                                                            &nbsp; until&nbsp; 
                                                                            <asp:TextBox ID="TextDateTo" runat="server" Width="92px" CssClass="searcheditbox"></asp:TextBox>
                                                                            <input id="PopCalcToDate" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                                border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                                border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                                height: 17px" title="Click to show calendar" type="button" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                                                            CIF No</td>
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="5">
                                                                            :</td>
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                                                            &nbsp;<asp:TextBox ID="TextCIF" runat="server" CssClass="searcheditbox" MaxLength="14"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                                                            Name</td>
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="5">
                                                                            :</td>
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                                                            &nbsp;<asp:TextBox ID="TextName" runat="server" CssClass="searcheditbox" Width="247px"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                                                            Account No</td>
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="5">
                                                                            :</td>
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                                                            &nbsp;<asp:TextBox ID="TextAccountNo" runat="server" CssClass="searcheditbox" MaxLength="20" Width="193px"></asp:TextBox></td>
                                                                    </tr>
<%--                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                                                            Transaction Type</td>
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="5">
                                                                            :</td>
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                                                            &nbsp;<asp:DropDownList ID="ListTransactionType" runat="server" CssClass="searcheditbox">
                                                                            </asp:DropDownList></td>
                                                                    </tr>--%>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                                                            Transaction Amount</td>
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="5">
                                                                            :</td>
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                                                            &nbsp;<asp:TextBox ID="TextAmountFrom" runat="server" Width="175px" CssClass="searcheditbox"></asp:TextBox>
                                                                            until&nbsp; 
                                                                            <asp:TextBox ID="TextAmountTo" runat="server" Width="175px" CssClass="searcheditbox"></asp:TextBox>                                                                            
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td style="width: 466px">
                    &nbsp;<asp:ImageButton ID="ImageSearch" runat="server" SkinID="SearchButton" />
                    <asp:CustomValidator ID="CValPageError" runat="server" Display="None"></asp:CustomValidator><br />
                </td>
            </tr>
        </table>
        
    </ajax:AjaxPanel>
</asp:Content>