﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class InsiderCodeadd
    Inherits Parent

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "InsiderCodeView.aspx"

            Me.Response.Redirect("InsiderCodeView.aspx", False)
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Function isValidData() As Boolean
        Try
            If txtInsiderCode.Text.Trim.Length = 0 Then
                Throw New Exception("InsiderCode Code must be filled")
            End If
            If txtInsiderCode.Text.Trim.Length > 1 Then
                Throw New Exception("InsiderCode must be filled by 1 character only")
            End If

            If Not InsiderCodeBLL.IsUniqueInsiderCode(txtInsiderCode.Text) Then
                Throw New Exception("Sement Code " & txtInsiderCode.Text & " existed")
            End If
            Return True
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
    End Function

    Sub ClearControl()
        txtDescription.Text = ""
        txtInsiderCode.Text = ""
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        If isValidData() Then
            Try

                Using objUser As User = AMLBLL.UserBLL.GetUserByPkUserID(Sahassa.AML.Commonly.SessionPkUserId)
                    If Not IsNothing(objUser) Then
                        Using objInsiderCode As New InsiderCode
                            objInsiderCode.InsiderCodeId = txtInsiderCode.Text
                            objInsiderCode.InsiderCodeName = txtDescription.Text
                            objInsiderCode.Activation = True
                            objInsiderCode.CreatedDate = Now
                            objInsiderCode.CreatedBy = objUser.UserName
                            objInsiderCode.LastUpdateDate = Now
                            objInsiderCode.LastUpdateBy = objUser.UserName



                            If Sahassa.AML.Commonly.SessionGroupName.ToLower = "superuser" Then
                                If InsiderCodeBLL.SaveAdd(objInsiderCode) Then
                                    lblMessage.Text = "Insert Data Success Click Ok to Add new data"
                                    mtvInsiderCodeAdd.ActiveViewIndex = 1
                                End If
                            Else
                                If InsiderCodeBLL.SaveAddApproval(objInsiderCode) Then
                                    lblMessage.Text = "Data has been inserted to Approval Click Ok to Add new data"
                                    mtvInsiderCodeAdd.ActiveViewIndex = 1
                                End If
                            End If
                        End Using
                    End If
                End Using

            Catch ex As Exception
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                mtvInsiderCodeAdd.ActiveViewIndex = 0

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            mtvInsiderCodeAdd.ActiveViewIndex = 0
            ClearControl()
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBack.Click
        Try
            Response.Redirect("InsiderCodeView.aspx")
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class


