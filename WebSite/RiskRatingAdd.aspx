<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="RiskRatingAdd.aspx.vb" Inherits="RiskRatingAdd" title="Risk Rating Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Risk Rating - Add New&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSucces" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000">* Required</span></td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72">       
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px"><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ErrorMessage="Risk Rating Name cannot be blank" Display="Dynamic"
					ControlToValidate="TextRiskRatingName">*</asp:requiredfieldvalidator><br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorNewsTitle" runat="server"
                    ControlToValidate="TextRiskRatingName" ErrorMessage="Risk Rating Name must starts with a letter then it can be followed by any letters, numbers or underscore"
                    ValidationExpression="[a-zA-Z](\w*\s*)*">*</asp:RegularExpressionValidator></td>
			<td width="20%" bgColor="#ffffff" style="height: 24px">
                Risk Rating Name</td>
			<td width="5" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px"><asp:textbox id="TextRiskRatingName" runat="server" CssClass="textBox" MaxLength="50"></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px;" rowspan="6">&nbsp;</td>
			<td bgColor="#ffffff" rowspan="6" style="height: 24px">
                Description<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px">
                :<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px">
                <asp:textbox id="TextRiskRatingDescription" runat="server" CssClass="textBox" MaxLength="255" Width="200px" Height="63px" TextMode="MultiLine"></asp:textbox></td>
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px">
				<asp:requiredfieldvalidator id="rfvRiskScoreFrom" runat="server" ErrorMessage="Risk Score From must be filled." Display="Dynamic" ControlToValidate="TextRiskScoreFrom">*</asp:requiredfieldvalidator><br />
				<asp:requiredfieldvalidator id="rfvRiskScoreTo" runat="server" ErrorMessage="Risk Score To must be filled." Display="Dynamic" ControlToValidate="TextRiskScoreTo">*</asp:requiredfieldvalidator><br />
                <asp:CompareValidator ID="CompareValidatorScoreFrom" runat="server" ErrorMessage="Risk Score From must be a number and smaller than Risk Score To" ControlToCompare="TextRiskScoreTo" ControlToValidate="TextRiskScoreFrom" Operator="LessThan" Type="Integer">*</asp:CompareValidator><br />
                <asp:CompareValidator ID="CompareValidatorScoreTo" runat="server"
                    ControlToValidate="TextRiskScoreTo" ErrorMessage="Risk Score To must be a number less than or equal to 10000"
                    Operator="LessThanEqual" Type="Integer" ValueToCompare="10000">*</asp:CompareValidator></td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                Risk Score</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                :</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                From
                <asp:TextBox ID="TextRiskScoreFrom" runat="server" MaxLength="4" Width="53px"></asp:TextBox>
                to
                <asp:TextBox ID="TextRiskScoreTo" runat="server" MaxLength="4" Width="53px"></asp:TextBox>
            </td>
        </tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="AddButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>            
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	
</asp:Content>

