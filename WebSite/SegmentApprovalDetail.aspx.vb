﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class SegmentApprovalDetailViewPage
    Inherits Parent

    ReadOnly Property GetPk_SegmentApproval As Long
        Get
            If IsNumeric(Request.Params("SegmentApprovalID")) Then
                If Not IsNothing(Session("SegmentApprovalDetail.PK")) Then
                    Return CLng(Session("SegmentApprovalDetail.PK"))
                Else
                    Session("SegmentApprovalDetail.PK") = Request.Params("SegmentApprovalID")
                    Return CLng(Session("SegmentApprovalDetail.PK"))
                End If
            End If
            Return 0
        End Get
    End Property

    Sub clearSession()
        Session("SegmentApprovalDetail.PK") = Nothing
    End Sub

    Sub LoadDataAll()
        Using objSegmentApproval As SegmentApproval = SegmentBLL.getSegmentApprovalByPK(GetPk_SegmentApproval)
            If Not IsNothing(objSegmentApproval) Then
                If objSegmentApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Add Then
                    LblAction.Text = "Add"
                ElseIf objSegmentApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Edit Then
                    LblAction.Text = "Edit"
                ElseIf objSegmentApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Delete Then
                    LblAction.Text = "Delete"
                ElseIf objSegmentApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Activation Then
                    LblAction.Text = "Activation"
                End If
            End If
            Using objUser As User = UserBLL.GetUserByPkUserID(objSegmentApproval.FK_MsUserID)
                If Not IsNothing(objUser) Then
                    LblRequestBy.Text = objUser.UserName
                End If
            End Using
            LblRequestDate.Text = CDate(objSegmentApproval.CreatedDate).ToString("dd-MM-yyyy")


            Using objSegmentApprovalDetail As SegmentApprovalDetail = SegmentBLL.getSegmentApprovalDetailByPKApproval(GetPk_SegmentApproval)
                If LblAction.Text = "Add" Then
                    LoadNewData()
                ElseIf LblAction.Text = "Edit" Then
                    LoadNewData()
                    LoadOldData(objSegmentApprovalDetail.PK_Segment_ID)
                    compareDataByContainer(PanelNew, PanelOld)
                ElseIf LblAction.Text = "Delete" Then
                    LoadOldData(objSegmentApprovalDetail.PK_Segment_ID)
                ElseIf LblAction.Text = "Activation" Then
                    LoadNewData()
                    LoadOldData(objSegmentApprovalDetail.PK_Segment_ID)
                    compareDataByContainer(PanelNew, PanelOld)
                End If
            End Using


        End Using

    End Sub

    Sub LoadNewData()
        Using objSegmentApprovalDetail As SegmentApprovalDetail = SegmentBLL.getSegmentApprovalDetailByPKApproval(GetPk_SegmentApproval)
            If Not IsNothing(objSegmentApprovalDetail) Then
                txtSegmentCode.Text = objSegmentApprovalDetail.SegmentCode.ToString
                txtDescription.Text = objSegmentApprovalDetail.SegmentDescription.ToString
                txtActivation.Text = objSegmentApprovalDetail.Activation.GetValueOrDefault(0).ToString
                PanelNew.Visible = True
            End If
        End Using
    End Sub

    Sub LoadOldData(ByVal PK_Segment_ID As Long)
        Using objSegment As Segment = SegmentBLL.getSegmentByPk(PK_Segment_ID)
            If Not IsNothing(objSegment) Then
                old_txtSegmentCode.Text = objSegment.SegmentCode.ToString
                old_txtDescription.Text = objSegment.SegmentDescription.ToString
                old_txtActivation.Text = objSegment.Activation.GetValueOrDefault(0).ToString
                PanelOld.Visible = True
            End If
        End Using
    End Sub


    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                clearSession()
                mtvApproval.ActiveViewIndex = 0

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                LoadDataAll()
            End If
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgBtnAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAccept.Click
        Try
            If LblAction.Text = "Add" Then
                If SegmentBLL.acceptAddOrDelete(GetPk_SegmentApproval, "Add") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Adding Data Approved"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Edit" Then
                If SegmentBLL.acceptEditOrActivate(GetPk_SegmentApproval, "Edit") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Editing Data Approved"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Delete" Then
                If SegmentBLL.acceptAddOrDelete(GetPk_SegmentApproval, "Delete") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Deleting Data Approved"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Activation" Then
                If SegmentBLL.acceptEditOrActivate(GetPk_SegmentApproval, "Activation") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Activation Data Approved"
                    AjaxPanel1.Visible = False
                End If
            End If
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImgBtnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnReject.Click
        Try
            If LblAction.Text = "Add" Then
                If SegmentBLL.rejectAddOrDelete(GetPk_SegmentApproval, "Add") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Adding Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Edit" Then
                If SegmentBLL.rejectEditOrActivate(GetPk_SegmentApproval, "Edit") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Editing Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Delete" Then
                If SegmentBLL.rejectAddOrDelete(GetPk_SegmentApproval, "Delete") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Deleting Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Activation" Then
                If SegmentBLL.rejectEditOrActivate(GetPk_SegmentApproval, "Activation") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Activation Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            End If
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("SegmentApproval.aspx")
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Response.Redirect("SegmentApproval.aspx")
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub compareDataByContainer(ByRef containerNew As Control, ByRef containerOld As Control)
        Dim index As Integer = 0
        For Each item As Control In containerNew.Controls
            If TypeOf (item) Is Label Then

                If CType(item, Label).Text.ToLower <> "old value" And CType(item, Label).Text.ToLower <> "new value" Then
                    If CType(item, Label).Text <> CType(containerOld.Controls(index), Label).Text Then
                        CType(item, Label).ForeColor = Drawing.Color.Red
                        CType(containerOld.Controls(index), Label).ForeColor = Drawing.Color.Red
                    End If
                End If
            End If
            index = index + 1
        Next
    End Sub
End Class
