
Partial Class DataUpdatingViewDetail
    Inherits Parent

#Region " Property "
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(ViewState("DataUpdatingViewSelected") Is Nothing, New ArrayList, ViewState("DataUpdatingViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataUpdatingViewSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(ViewState("DataUpdatingViewFieldSearch") Is Nothing, "", ViewState("DataUpdatingViewFieldSearch"))
        End Get
        Set(ByVal Value As String)
            ViewState("DataUpdatingViewFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(ViewState("DataUpdatingViewValueSearch") Is Nothing, "", ViewState("DataUpdatingViewValueSearch"))
        End Get
        Set(ByVal Value As String)
            ViewState("DataUpdatingViewValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(ViewState("DataUpdatingViewSort") Is Nothing, "TransactionDate  asc", ViewState("DataUpdatingViewSort"))
        End Get
        Set(ByVal Value As String)
            ViewState("DataUpdatingViewSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(ViewState("DataUpdatingViewCurrentPage") Is Nothing, 0, ViewState("DataUpdatingViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            ViewState("DataUpdatingViewCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(ViewState("DataUpdatingViewRowTotal") Is Nothing, 0, ViewState("DataUpdatingViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            ViewState("DataUpdatingViewRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Private Property SetnGetBindTable() As Data.DataTable
    '    Get
    '        Return IIf(ViewState("DataUpdatingViewData") Is Nothing, New AMLDAL.DataUpdatingTableAdapters.SelectDataUpdatingDetailTableAdapter, ViewState("DataUpdatingViewData"))
    '    End Get
    '    Set(ByVal value As Data.DataTable)
    '        ViewState("DataUpdatingViewData") = value
    '    End Set
    'End Property
    ''' <summary>
    ''' Account No
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetCIFNo() As String
        Get
            Return Me.Request.Params.Get("CIFNo")
        End Get
    End Property

    ''' <summary>
    ''' Data Updating Date
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetDataUpdatingDate() As String
        Get
            Return Me.Request.Params.Get("DataUpdatingDate")
        End Get
    End Property

    ''' <summary>
    ''' AccountName
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetCIFName() As String
        Get
            Return Me.Request.Params.Get("CIFName")
        End Get
    End Property

    ''' <summary>
    ''' DataUpdatingReason
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetDataUpdatingReason() As String
        Get
            Return Me.Request.Params.Get("DataUpdatingReason")
        End Get
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                'Using AccessDataUpdatingDetail As New AMLDAL.DataUpdatingTableAdapters.SelectDataUpdatingDetailTableAdapter
                '    Me.SetnGetBindTable = AccessDataUpdatingDetail.GetData(Me.GetAccountNo, Me.GetDataUpdatingDate)
                'End Using
                Me.LblCIFNo.Text = Me.GetCIFNo
                Me.LblCIFName.Text = Me.GetCIFName
                Me.LblDataUpdatingReason.Text = Me.GetDataUpdatingReason
                Me.LblDataUpdatingDate.Text = Me.GetDataUpdatingDate
                'Me.MsGridDataUpdatingDetail.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    'Public Sub BindGrid()
    '    Try
    '        Dim Rows() As AMLDAL.DataUpdating.SelectDataUpdatingDetailRow = Me.SetnGetBindTable.Select("AccountNo='" & Me.GetAccountNo & "'", Me.SetnGetSort)
    '        If Rows.Length > 0 Then
    '            Me.datafound.Visible = True
    '            Me.datanotfound.Visible = False
    '            Me.MsGridDataUpdatingDetail.DataSource = Rows
    '            Me.SetnGetRowTotal = Rows.Length
    '            Me.MsGridDataUpdatingDetail.CurrentPageIndex = Me.SetnGetCurrentPage
    '            Me.MsGridDataUpdatingDetail.VirtualItemCount = Me.SetnGetRowTotal
    '            Me.MsGridDataUpdatingDetail.DataBind()
    '        Else
    '            Me.datanotfound.Visible = True
    '            Me.datafound.Visible = False
    '        End If
    '    Catch
    '        Throw
    '    End Try
    'End Sub


    

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    'Private Sub MsGridDataUpdatingDetail_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles MsGridDataUpdatingDetail.SortCommand
    '    Dim GridUser As DataGrid = source
    '    Try
    '        Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
    '        GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
    '    Catch ex As Exception
    '        Me.cvalPageError.IsValid = False
    '        Me.cvalPageError.ErrorMessage = ex.Message
    '        LogError(ex)
    '    End Try
    'End Sub

   

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ' Me.BindGrid()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' Back
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Me.Response.Redirect("DataUpdatingView.aspx", False)
    End Sub
End Class
