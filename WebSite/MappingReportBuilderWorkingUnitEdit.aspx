<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
	CodeFile="MappingReportBuilderWorkingUnitEdit.aspx.vb" Inherits="MappingReportBuilderWorkingUnitEdit"
	 %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<%@ Register src="webcontrol/PickerWorkingUnit.ascx" tagname="PickerWorkingUnit" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">

<ajax:AjaxPanel ID="AjaxPanel2" runat="server" >
	<uc1:PickerWorkingUnit ID="PickerWorkingUnit1" runat="server" />
	
</ajax:AjaxPanel> 

<ajax:AjaxPanel runat="server" ID="Ajax1">

<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
		cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
		border-left-style: none; border-bottom-style: none" width="100%">
		<tr>
			<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
				border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
				<strong>Mapping Report&nbsp; Builder Working Unit - Edit
					<hr />
				</strong>
				<asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
					Width="100%"></asp:Label></td>
		</tr>
		<tr class="formText">
			<td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
				border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000">* Required</span></td>
		</tr>
	</table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px">
				</td>
			<td width="20%" bgColor="#ffffff" style="height: 24px">
				Report Builder</td>
			<td width="5" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px">
				&nbsp;<asp:DropDownList ID="CboReportBuilder" runat="server" CssClass="combobox" Width="182px">
				</asp:DropDownList><strong><span style="color: #ff0000"></span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px">
				</td>
			<td width="20%" bgColor="#ffffff" style="height: 24px">
				Working Unit</td>
			<td width="5" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px">
				&nbsp;<strong><span style="color: #ff0000"><asp:LinkButton ID="btnBrowse" 
					runat="server" CausesValidation="False" CssClass="ovalbutton"><span>Browse</span></asp:LinkButton>
				<span>
				<asp:HiddenField ID="HWorkingUnit" runat="server" />
				<asp:Label ID="LblUserID" runat="server">Click Browse Button To Choose Working Unit</asp:Label>
				</span></span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px">
				</td>
			<td width="20%" bgColor="#ffffff" style="height: 24px">
				</td>
			<td width="5" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px">
				&nbsp;<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td style="height: 33px">
							&nbsp;</td>
						
					</tr>
				</table>
				<strong><span style="color: #ff0000"></span></strong>
			 </td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px">
				</td>
			<td width="20%" bgColor="#ffffff" style="height: 24px">
				</td>
			<td width="5" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px">
				&nbsp;
				<asp:DataGrid ID="gridView" runat="server"  AllowPaging="True"
					AllowSorting="True" AutoGenerateColumns="False" CellPadding="4"
					Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="XX-Small"
					Font-Strikeout="False" Font-Underline="False" ForeColor="Black" GridLines="Vertical"
					HorizontalAlign="Left" SkinID="gridview" Width="97%" BackColor="White" 
					BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px">
					<FooterStyle BackColor="#CCCC99" />
					<SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
					<PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" 
						Mode="NumericPages" />
					<AlternatingItemStyle BackColor="White" />
					<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" BackColor="#F7F7DE" />
					<Columns>
						<asp:TemplateColumn HeaderText="No." Visible="False">
							<EditItemTemplate>
								<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
							</EditItemTemplate>
							<ItemTemplate>
								<asp:Label ID="LabelNo" runat="server"></asp:Label>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn DataField="FK_WorkingUnit_ID" HeaderText="workingUnitID" 
							Visible="False"></asp:BoundColumn>
						<asp:TemplateColumn HeaderText="Working Unit">
							<EditItemTemplate>
								<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
							</EditItemTemplate>
							<ItemTemplate>
								<asp:Label ID="LblWorkingUnit" runat="server"></asp:Label>
							</ItemTemplate>
							<HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
								Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
						</asp:TemplateColumn>
						<asp:TemplateColumn>
							<ItemTemplate>
								<asp:LinkButton ID="lnkGridDelete" runat="server" CausesValidation="False" CommandName="DELETE"
									Text="Delete" onclick="lnkGridDelete_Click"></asp:LinkButton>
							</ItemTemplate>
							<HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
								Font-Underline="False" ForeColor="White" />
						</asp:TemplateColumn>
					</Columns>
					<HeaderStyle Font-Bold="True" HorizontalAlign="Left" VerticalAlign="Top" Width="25%"
						Wrap="False" BackColor="#6B696B" ForeColor="White" />
				</asp:DataGrid><strong><span style="color: #ff0000"></span></strong></td>
		</tr>



		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td style="height: 36px"><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="SaveButton"></asp:imagebutton></td>
						<td style="height: 36px; width: 35px;"><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               
				<asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	</ajax:AjaxPanel>
</asp:Content>


