
Imports System.Data.SqlClient
Imports Sahassa.AML

Partial Class CustomerVerificationJudgementDetailApproval

    Inherits Parent

    ''' <summary>
    ''' Get CustomerVerificationJudgementId
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property PK_Aml_Customer_Judgement_ID() As Integer
        Get
            Return Me.Request.Params.Get("PK_Aml_Customer_Judgement_ID")
        End Get
    End Property


    Public Property Aml_Customer_Judgement() As Data.DataTable
        Get
            Return Session("CustomerVerificationJudgementDetailApproval.Aml_Customer_Judgement")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CustomerVerificationJudgementDetailApproval.Aml_Customer_Judgement") = value
        End Set
    End Property



    Public Property Aml_Watchlist_Judgement() As Data.DataTable
        Get
            Return Session("CustomerVerificationJudgementDetailApproval.Aml_Watchlist_Judgement")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CustomerVerificationJudgementDetailApproval.Aml_Watchlist_Judgement") = value
        End Set
    End Property

    Sub LoadDatajudgement()
        If Not Aml_Customer_Judgement Is Nothing Then
            LblCIFNo.Text = Aml_Customer_Judgement.Rows(0).Item("CIFNO")
            LblName.Text = Aml_Customer_Judgement.Rows(0).Item("Nama")

            LblRelatedCustomerType.Text = Aml_Customer_Judgement.Rows(0).Item("RelatedCustomerType")
            LblBranch.Text = Aml_Customer_Judgement.Rows(0).Item("AccountOwnerID")
            If Aml_Customer_Judgement.Rows(0).Item("DOB").ToString <> "" Then
                LblDateOfBirth.Text = Convert.ToDateTime(Aml_Customer_Judgement.Rows(0).Item("DOB")).ToString("dd-MMM-yyyy")
            End If

            If Aml_Customer_Judgement.Rows(0).Item("openingCIF").ToString <> "" Then
                LblOpeningDate.Text = Convert.ToDateTime(Aml_Customer_Judgement.Rows(0).Item("openingCIF")).ToString("dd-MMM-yyyy")
            End If

            lblBirthPlaceCIF.Text = AMLBLL.AccountInformationBLL.GetBirthPlace(Aml_Customer_Judgement.Rows(0).Item("CIFNO"))
            AddressTextBox.Text = Aml_Customer_Judgement.Rows(0).Item("Alamat").ToString
            IdentityNumberTextBox.Text = Aml_Customer_Judgement.Rows(0).Item("identitas").ToString
            TxtPhoneNumberCustomer.Text = Aml_Customer_Judgement.Rows(0).Item("telpno").ToString


        End If



    End Sub

    ''' <summary>
    '''  Load Data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then

                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Aml_Customer_Judgement = EKABLL.ScreeningBLL.getAMLCustomerJudgement(PK_Aml_Customer_Judgement_ID)
                Dim arrlistvalue As New ArrayList

                If Not Aml_Customer_Judgement Is Nothing Then
                    LoadDatajudgement()

                    Aml_Watchlist_Judgement = EKABLL.ScreeningBLL.GetWatchListData(Aml_Customer_Judgement.Rows(0).Item("verificationlistid"), Aml_Customer_Judgement.Rows(0).Item("PK_Aml_Customer_Judgement_ID"))

                    Me.GridMSUserView.DataSource = Aml_Watchlist_Judgement
                    Me.GridMSUserView.PageSize = Int32.MaxValue
                    Me.GridMSUserView.DataBind()

                    If Aml_Watchlist_Judgement.Rows.Count > 0 Then

                        NameSuspectTextBox.Text = Aml_Watchlist_Judgement.Rows(0).Item("DisplayName")
                        LblListTypeSuspect.Text = Aml_Watchlist_Judgement.Rows(0).Item("ListTypeName")
                        LblListCategorySuspect.Text = Aml_Watchlist_Judgement.Rows(0).Item("CategoryName")
                        LblDateOfBirthSuspect.Text = Aml_Watchlist_Judgement.Rows(0).Item("DateOfBirth")
                        LblBirthPlace.Text = Aml_Watchlist_Judgement.Rows(0).Item("BirthPlace")
                        AddressSuspectTextBox.Text = Aml_Watchlist_Judgement.Rows(0).Item("Alamat")
                        IdentityNumberSuspectTextBox.Text = Aml_Watchlist_Judgement.Rows(0).Item("identitynumber")
                        TxtCustomRemark1.Text = Aml_Watchlist_Judgement.Rows(0).Item("CustomRemark1")
                        TxtCustomRemark2.Text = Aml_Watchlist_Judgement.Rows(0).Item("CustomRemark2")
                        TxtCustomRemark3.Text = Aml_Watchlist_Judgement.Rows(0).Item("CustomRemark3")
                        TxtCustomRemark4.Text = Aml_Watchlist_Judgement.Rows(0).Item("CustomRemark4")
                        TxtCustomRemark5.Text = Aml_Watchlist_Judgement.Rows(0).Item("CustomRemark5")
                        Dim intcategoryworldcheck As String = EKABLL.ScreeningBLL.GetSystemParameterByValueByID(4000)
                        Dim strreffid As String = Aml_Watchlist_Judgement.Rows(0).Item("RefId").ToString
                        If Aml_Watchlist_Judgement.Rows(0).Item("CategoryID") = intcategoryworldcheck Then
                            BtnDetail.Visible = True
                        Else
                            BtnDetail.Visible = False
                        End If

                        BtnDetail.OnClientClick = "window.open('showwatchlist.aspx?uid=" & strreffid & "', 'ca', 'width=500,height=250,left=500,top=500,resizable=yes,scrollbars=yes')"


                        Dim StrAddressSuspect As String
                        Dim StrIdentitySuspect As String

                        For Each row As Data.DataRow In Aml_Watchlist_Judgement.Rows


                            StrAddressSuspect = row.Item("Alamat")
                            StrIdentitySuspect = row.Item("identitynumber")

                            ' Tampilkan       
                            Try
                                arrlistvalue.Add(New ListItem("Match with Name " & row.Item("DisplayName") & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB '" & Convert.ToDateTime(row.Item("DateOfBirth")).ToString("dd-MMMM-yyyy") & "';" & row.Item("ListTypeName") & ";" & row.Item("CategoryName") & vbCrLf, Aml_Customer_Judgement.Rows(0).Item("PK_Aml_Customer_Judgement_ID")))
                            Catch ex As Exception
                                arrlistvalue.Add(New ListItem("Match with Name " & row.Item("DisplayName") & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB '';" & row.Item("ListTypeName ") & ";" & row.Item("CategoryName") & vbCrLf, Aml_Customer_Judgement.Rows(0).Item("PK_Aml_Customer_Judgement_ID")))
                            End Try

                            Try
                                Me.TxtJudgementComment.Text = Me.TxtJudgementComment.Text & "Match with Name " & row.Item("DisplayName") & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB" & row.Item("DateOfBirth").ToString("dd-MMMM-yyyy") & ";" & row.Item("ListTypeName") & ";" & row.Item("CategoryName") & vbCrLf
                            Catch ex As Exception
                                Me.TxtJudgementComment.Text = Me.TxtJudgementComment.Text & "Match with Name " & row.Item("DisplayName") & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB;" & row.Item("ListTypeName") & ";" & row.Item("CategoryName") & vbCrLf
                            End Try

                        Next
                        Session("JudgementCommentDetail") = arrlistvalue



                    End If





                End If

                'Using AccessCustomerVerificationDetail As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectCustomerVerificationJudgementFilterTableAdapter
                '    ' Get Detail for data Customer
                '    Using dtCustomerFilter As AMLDAL.CustomerVerificationJudgement.SelectCustomerVerificationJudgementFilterDataTable = AccessCustomerVerificationDetail.GetData(Me.CustomerVerificationJudgementId)
                '        Dim RowCustomerFilter As AMLDAL.CustomerVerificationJudgement.SelectCustomerVerificationJudgementFilterRow = dtCustomerFilter.Rows(0)

                '        Me.LblCIFNo.Text = RowCustomerFilter.CIFNo
                '        Me.LblName.Text = RowCustomerFilter.Name
                '        Try
                '            Me.LblDateOfBirth.Text = RowCustomerFilter.DateOfBirth.ToString("dd-MMMM-yyyy")
                '        Catch ex As Exception
                '            Me.LblDateOfBirth.Text = ""
                '        End Try
                '        lblBirthPlaceCIF.Text = AMLBLL.AccountInformationBLL.GetBirthPlace(RowCustomerFilter.CIFNo)

                '        Me.LblBranch.Text = RowCustomerFilter.BranchName
                '        Me.LabelUserJudgement.Text = Sahassa.AML.Commonly.SessionUserId
                '        Me.LabelTglJudgement.Text = Now.ToLongDateString
                '        Try
                '            Me.LblOpeningDate.Text = RowCustomerFilter.OpeningDate.ToString("dd-MMMM-yyyy")
                '        Catch ex As Exception
                '            Me.LblOpeningDate.Text = ""
                '        End Try
                '        Me.GetCCDAddressCustomer(RowCustomerFilter.CIFNo)
                '        Me.GetIdentityNumberCustomer(RowCustomerFilter.CIFNo)
                '        Me.GetPhoneNumberCustomer(RowCustomerFilter.CIFNo)
                '        Me.LblBeneficialOwner.Text = GetBeneficialOwner(RowCustomerFilter.CIFNo, RowCustomerFilter.Name)
                '    End Using
                'End Using

                '' Get All Detail Suspect Customer
                'Dim StrAddressSuspect As String = ""
                'Dim StrIdentitySuspect As String = ""
                'Using AccessCustomerVerification As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectCustomerVerificationJudgementDetailTableAdapter
                '    Using dt As AMLDAL.CustomerVerificationJudgement.SelectCustomerVerificationJudgementDetailDataTable = AccessCustomerVerification.GetData(Me.CustomerVerificationJudgementId)
                '        Me.GridMSUserView.DataSource = dt
                '        Me.GridMSUserView.PageSize = Int32.MaxValue
                '        Me.GridMSUserView.DataBind()

                '        For Each row As AMLDAL.CustomerVerificationJudgement.SelectCustomerVerificationJudgementDetailRow In dt.Rows

                '            StrAddressSuspect = Me.GetAddressSuspect(CInt(row.VerificationListId))
                '            StrIdentitySuspect = Me.GetIdentityNumberSuspect(CInt(row.VerificationListId))

                '            ' Tampilkan       
                '            Try
                '                arrlistvalue.Add(New ListItem("Match with Name " & row.DisplayName & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB '" & row.DateOfBirth.ToString("dd-MMMM-yyyy") & "';" & row.ListTypeName & ";" & row.CategoryName & vbCrLf, row.CustomerVerificationJudgementDetailId))
                '            Catch ex As Exception
                '                arrlistvalue.Add(New ListItem("Match with Name " & row.DisplayName & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB '';" & row.ListTypeName & ";" & row.CategoryName & vbCrLf, row.CustomerVerificationJudgementDetailId))
                '            End Try

                '            Try
                '                Me.TxtJudgementComment.Text = Me.TxtJudgementComment.Text & "Match with Name " & row.DisplayName & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB" & row.DateOfBirth.ToString("dd-MMMM-yyyy") & ";" & row.ListTypeName & ";" & row.CategoryName & vbCrLf
                '            Catch ex As Exception
                '                Me.TxtJudgementComment.Text = Me.TxtJudgementComment.Text & "Match with Name " & row.DisplayName & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB;" & row.ListTypeName & ";" & row.CategoryName & vbCrLf
                '            End Try

                '        Next
                '        Session("JudgementCommentDetail") = arrlistvalue
                '    End Using
                'End Using

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' GetIdentityNumberCustomer
    ''' </summary>
    ''' <param name="CIFNo"></param>
    ''' <remarks></remarks>
    Private Sub GetIdentityNumberCustomer(ByVal CIFNo As String)
        Try
            'Me.ListIdentityNumber.Items.Clear()
            Me.IdentityNumberTextBox.Text = ""
            ' Get Detail Identity Number Customer
            Using AccessCCDIdentity As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectCDD_CIF_IdNumberTableAdapter
                Using dtCCDIdentity As Data.DataTable = AccessCCDIdentity.GetData(CIFNo)
                    For Each itemCCDIdentity As AMLDAL.CustomerVerificationJudgement.SelectCDD_CIF_IdNumberRow In dtCCDIdentity.Rows
                        'Me.ListIdentityNumber.Items.Add(itemCCDIdentity.IDNumber)
                        Me.IdentityNumberTextBox.Text = Me.IdentityNumberTextBox.Text & itemCCDIdentity.IDNumber & vbCrLf
                    Next
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' GetCCDAddressCustomer
    ''' </summary>
    ''' <param name="CIFNo"></param>
    ''' <remarks></remarks>
    Private Sub GetCCDAddressCustomer(ByVal CIFNo As String)
        Try
            'Me.ListAddress.Items.Clear()
            Me.AddressTextBox.Text = ""
            ' Get Detail Address Customer
            Using AccessCCDAddress As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectCDD_CIF_AddressTableAdapter
                Using dtCCDAddress As Data.DataTable = AccessCCDAddress.GetData(CIFNo)
                    For Each itemCCDAddress As AMLDAL.CustomerVerificationJudgement.SelectCDD_CIF_AddressRow In dtCCDAddress.Rows
                        'Me.ListAddress.Items.Add(itemCCDAddress.Address)
                        Me.AddressTextBox.Text = Me.AddressTextBox.Text & itemCCDAddress.Address & vbCrLf
                    Next
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub

    Private Sub GetPhoneNumberCustomer(ByVal CIFNo As String)
        Me.TxtPhoneNumberCustomer.Text = ""
        ' Get Detail Customer Phone Number
        Using AccessCustomerInformation As New AMLDAL.CustomerInformationTableAdapters.SelectCustomerInformationPhoneDetailTableAdapter
            Using dtPhone As Data.DataTable = AccessCustomerInformation.GetData(CIFNo)
                For Each itemPhone As AMLDAL.CustomerInformation.SelectCustomerInformationPhoneDetailRow In dtPhone.Rows
                    Me.TxtPhoneNumberCustomer.Text = Me.TxtPhoneNumberCustomer.Text & itemPhone.Type & " - " & itemPhone.Description & vbCrLf
                Next
            End Using
        End Using
    End Sub
    Function GetBeneficialOwner(strcif As String, strName As String) As String
        Using objCommand As System.Data.SqlClient.SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_GetBeneficialOwner")
            objCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CIFNo", strcif))
            objCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NamaCIF", strName))
            Return SahassaNettier.Data.DataRepository.Provider.ExecuteScalar(objCommand)
        End Using
    End Function


    ''' <summary>
    ''' Get Identity Number Suspect 
    ''' </summary>
    ''' <param name="VerificationListId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetIdentityNumberSuspect(ByVal VerificationListId As Int64) As String
        Try
            Dim StrIdentitySuspect As String = ""
            ' Identity Suspect
            Using AccessIdentitySuspect As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectIDNumberSuspectTableAdapter
                Using dtIdentityNumber As Data.DataTable = AccessIdentitySuspect.GetData(CInt(VerificationListId))
                    For Each RowIdentitySuspect As AMLDAL.CustomerVerificationJudgement.SelectIDNumberSuspectRow In dtIdentityNumber.Rows
                        StrIdentitySuspect &= RowIdentitySuspect.IDNumber & ","
                    Next
                    If StrIdentitySuspect.Length > 0 Then
                        StrIdentitySuspect = StrIdentitySuspect.Substring(0, StrIdentitySuspect.Length - 1)
                    End If
                End Using
            End Using
            Return StrIdentitySuspect
        Catch
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Get Address Suspect 
    ''' </summary>
    ''' <param name="VerificationListId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetAddressSuspect(ByVal VerificationListId As Int64) As String
        Try
            Dim StrAddressSuspect As String = ""
            Using AccessAddressSuspect As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectAddressSuspectTableAdapter
                Using dtAddressSuspect As Data.DataTable = AccessAddressSuspect.GetData(CInt(VerificationListId))
                    For Each RowAddressSuspect As AMLDAL.CustomerVerificationJudgement.SelectAddressSuspectRow In dtAddressSuspect.Rows
                        StrAddressSuspect &= RowAddressSuspect.Address & ","
                    Next
                    If StrAddressSuspect.Length > 0 Then
                        StrAddressSuspect = StrAddressSuspect.Substring(0, StrAddressSuspect.Length - 1)
                    End If

                End Using
            End Using
            Return StrAddressSuspect
        Catch
            Throw
        End Try
    End Function

    Private Sub GetVerificationListAlias(ByVal VerificationListId As Int64)
        ' Alias
        Me.NameSuspectTextBox.Text = ""
        Using AccessDetailAliasPotentialCustomerVerification As New AMLDAL.PotentialCustomerVerificationTableAdapters.SelectVerificationList_AliasTableAdapter
            Using dtDetailAliasPotentialCustomerVerification As Data.DataTable = AccessDetailAliasPotentialCustomerVerification.GetData(VerificationListId)
                For Each RowDetailAliasPotentialCustomerVerification As AMLDAL.PotentialCustomerVerification.SelectVerificationList_AliasRow In dtDetailAliasPotentialCustomerVerification.Rows
                    Me.NameSuspectTextBox.Text = Me.NameSuspectTextBox.Text & RowDetailAliasPotentialCustomerVerification.Name & vbCrLf
                Next
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' GetIdentityNumberSuspectDetail
    ''' </summary>
    ''' <param name="VerificationListId"></param>
    ''' <remarks></remarks>
    Private Sub GetIdentityNumberSuspectDetail(ByVal VerificationListId As Int64)
        Try
            'Me.ListIndetityNumberSuspect.Items.Clear()
            Me.IdentityNumberSuspectTextBox.Text = ""
            Dim StrIdentitySuspect As String = ""
            ' Identity Suspect
            Using AccessIdentitySuspect As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectIDNumberSuspectTableAdapter
                Using dtIdentityNumber As Data.DataTable = AccessIdentitySuspect.GetData(CInt(VerificationListId))
                    For Each RowIdentitySuspect As AMLDAL.CustomerVerificationJudgement.SelectIDNumberSuspectRow In dtIdentityNumber.Rows
                        'Me.ListIndetityNumberSuspect.Items.Add(RowIdentitySuspect.IDNumber)
                        Me.IdentityNumberSuspectTextBox.Text = Me.IdentityNumberSuspectTextBox.Text & RowIdentitySuspect.IDNumber & vbCrLf
                    Next
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Get Address Suspect Detail
    ''' </summary>
    ''' <param name="VerificationListId"></param>
    ''' <remarks></remarks>
    Private Sub GetAddressSuspectDetail(ByVal VerificationListId As Int64)
        Try
            'Me.ListAddressSuspect.Items.Clear()
            Me.AddressSuspectTextBox.Text = ""
            Dim StrAddressSuspect As String = ""
            Using AccessAddressSuspect As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectAddressSuspectTableAdapter
                Using dtAddressSuspect As Data.DataTable = AccessAddressSuspect.GetData(CInt(VerificationListId))
                    For Each RowAddressSuspect As AMLDAL.CustomerVerificationJudgement.SelectAddressSuspectRow In dtAddressSuspect.Rows
                        'Me.ListAddressSuspect.Items.Add(RowAddressSuspect.Address)
                        Me.AddressSuspectTextBox.Text = Me.AddressSuspectTextBox.Text & RowAddressSuspect.Address & vbCrLf
                    Next
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub


    ''' <summary>
    ''' Radio Match dan UnMatch
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub RadioButtonList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Me.TxtJudgementComment.Text = ""
            Dim RadioListMatch As RadioButtonList = CType(sender, RadioButtonList)
            Dim dgItem As DataGridItem = CType(RadioListMatch.NamingContainer, DataGridItem)

            Dim arrlist As ArrayList = Session("JudgementCommentDetail")
            Dim unique As String = dgItem.Cells(0).Text
            Dim listitem As ListItem
            Dim Arrtemp As New ArrayList
            If RadioListMatch.SelectedIndex = 0 Then
                If arrlist.Count <> 0 Then
                    For i As Integer = 0 To arrlist.Count - 1
                        listitem = arrlist(i)
                        If listitem.Value = dgItem.Cells(0).Text Then
                            listitem.Text = listitem.Text.Replace("UnMatch", "Match")
                            arrlist(i) = listitem
                        End If
                    Next
                End If
            Else
                If arrlist.Count <> 0 Then
                    For i As Integer = 0 To arrlist.Count - 1
                        listitem = arrlist(i)
                        If listitem.Value = dgItem.Cells(0).Text Then
                            listitem.Text = listitem.Text.Replace("Match", "UnMatch")
                            arrlist(i) = listitem
                        End If
                    Next
                End If
            End If
            Session("JudgementCommentDetail") = arrlist
            For i As Integer = 0 To arrlist.Count - 1
                listitem = arrlist(i)
                Me.TxtJudgementComment.Text = Me.TxtJudgementComment.Text & listitem.Text
            Next
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' Get Detail Suspect
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Try
            'Me.LblNameSuspect.Text = e.Item.Cells(1).Text
            LoadDatajudgement()

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Function IsDataValid() As Boolean
        Dim strErrorMessage As New StringBuilder
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim radioMatch As RadioButtonList = gridRow.FindControl("RadioButtonList1")

                If radioMatch.SelectedValue = "" Then
                    strErrorMessage.Append("Please Select Match or UnMatch for Name " & gridRow.Cells(2).Text & "<br/>")
                End If
            End If
        Next

        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New Exception(strErrorMessage.ToString)
        Else
            Return True
        End If

    End Function



    Private Function IsApproval(ByVal PK_Aml_Customer_Judgement_ID As Long)


        Dim intcountApproval As Integer = 0
        Using objCommand As SqlCommand = Commonly.GetSQLCommandStoreProcedure("AML_usp_IsCustomerVerificationJudgementInApproval")
            objCommand.Parameters.Add(New SqlParameter("@PK_Aml_Customer_Judgement_ID", PK_Aml_Customer_Judgement_ID))
            intcountApproval = EkaDataNettier.Data.DataRepository.Provider.ExecuteScalar(objCommand)

            If intcountApproval > 0 Then
                Return True
            Else
                Return False
            End If
        End Using




    End Function


    ''' <summary>
    ''' Save to Approval
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    'Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
    '    Try

    '        If IsDataValid() Then
    '            If Not Me.IsApproval(Aml_Customer_Judgement.Rows(0).Item("PK_Aml_Customer_Judgement_ID")) Then


    '                Dim BoolIsMatch As Boolean
    '                For Each gridRow As DataGridItem In Me.GridMSUserView.Items
    '                    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
    '                        Dim radioMatch As RadioButtonList = gridRow.FindControl("RadioButtonList1")

    '                        If radioMatch.SelectedIndex = 1 Then
    '                            BoolIsMatch = False
    '                        Else
    '                            BoolIsMatch = True
    '                        End If

    '                        Aml_Customer_Judgement.Rows(0).Item("judgementresult") = BoolIsMatch
    '                        Aml_Customer_Judgement.Rows(0).Item("judgementby") = Sahassa.AML.Commonly.SessionUserId
    '                        Aml_Customer_Judgement.Rows(0).Item("judgementcomment") = TxtJudgementComment.Text.Trim
    '                        Aml_Customer_Judgement.Rows(0).Item("judgementdate") = Now


    '                    End If
    '                Next


    '                EKABLL.ScreeningBLL.saveScreening(Aml_Customer_Judgement)


    '                '    Using AccessInsertCustomerInList As New AMLDAL.CustomerVerificationJudgementTableAdapters.QueriesCustomerVerificationJudgement
    '                '        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
    '                '            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
    '                '                Dim r As RadioButtonList = gridRow.FindControl("RadioButtonList1")
    '                '                If r.SelectedIndex = 0 Then
    '                '                    AccessInsertCustomerInList.InsertCustomerInList(Me.LblCIFNo.Text, CInt(gridRow.Cells(8).Text), CDate(Me.LabelTglJudgement.Text).ToShortDateString, Now, CDbl(gridRow.Cells(6).Text), Me.LabelUserJudgement.Text, Sahassa.AML.Commonly.SessionUserId)
    '                '                End If
    '                '            End If
    '                '        Next
    '                '    End Using
    '                '    InsertHistoryApproval(CustomerVerificationJudgement_Approval_Id)
    '                '    DeleteAllApproval(CustomerVerificationJudgement_Approval_Id)

    '                'End Using



    '                '' delete Customer Verification Judgement
    '                'Using AccessCustomerVerificationJudgement As New AMLDAL.CustomerVerificationJudgementTableAdapters.QueriesCustomerVerificationJudgement
    '                '    AccessCustomerVerificationJudgement.DeleteCustomerVerificationJudgementByCIFNo(Me.LblCIFNo.Text)
    '                'End Using


    '                '' delete Customer Verification Judgement
    '                'Using AccessCustomerVerificationJudgement As New AMLDAL.CustomerVerificationJudgementTableAdapters.QueriesCustomerVerificationJudgement
    '                '    AccessCustomerVerificationJudgement.DeleteCustomerVerificationjudgement(CInt(Me.CustomerVerificationJudgementId))
    '                'End Using
    '                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=8891&Identifier=" & Me.LblName.Text, False)
    '            Else
    '                Throw New Exception("Data Customer Verification Judgement is waiting for Approval.")
    '            End If
    '        End If


    '    Catch ex As Exception
    '        Me.cvalPageError.IsValid = False
    '        Me.cvalPageError.ErrorMessage = ex.Message
    '        LogError(ex)
    '    End Try
    'End Sub
    Private Sub DeleteAllApproval(ByVal appid As Long)
        Try
            Using AccessDeleteQuery As New AMLDAL.CustomerVerificationJudgementTableAdapters.QueriesCustomerVerificationJudgement
                AccessDeleteQuery.DeleteCustomerVerificationJudgement_Approval(appid)
                AccessDeleteQuery.DeleteCustomerVerificationJudgementDetail_Approval(appid)
            End Using

        Catch
            Throw
        End Try
    End Sub
    Private Sub InsertHistoryApproval(ByVal appid As Long)
        Try
            'Using TransScope As New Transactions.TransactionScope              
            Using QueryInsertHistory As New AMLDAL.CustomerVerificationJudgementTableAdapters.QueriesCustomerVerificationJudgement
                QueryInsertHistory.InsertCustomerVerificationJudgement_Approval_History(appid, CDate(Me.LabelTglJudgement.Text).ToShortDateString, Me.LblCIFNo.Text, Me.LabelUserJudgement.Text, Sahassa.AML.Commonly.SessionUserId, Me.TxtJudgementComment.Text)
                For Each gridRow As DataGridItem In Me.GridMSUserView.Items
                    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                        Dim radiolist As RadioButtonList = gridRow.FindControl("RadioButtonList1")
                        Dim IsMatch As Boolean = True
                        If radiolist.SelectedIndex = 1 Then
                            IsMatch = False
                        End If
                        QueryInsertHistory.InsertCustomerVerificationJudgementDetail_Approval_History(gridRow.Cells(0).Text, appid, gridRow.Cells(8).Text, gridRow.Cells(6).Text, IsMatch)
                    End If
                Next
            End Using
            'TransScope.Complete()
            'End Using
        Catch
            Throw
        End Try
    End Sub


    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "CustomerVerificationJudgementApproval.aspx"

        MagicAjax.AjaxCallHelper.Redirect("CustomerVerificationJudgementApproval.aspx")
    End Sub





    Protected Sub ImageReject_Click(sender As Object, e As ImageClickEventArgs) Handles ImageReject.Click
        Try


            EKABLL.ScreeningBLL.rejectJudgement(PK_Aml_Customer_Judgement_ID, Sahassa.AML.Commonly.SessionUserId)

            Sahassa.AML.Commonly.SessionIntendedPage = "CustomerVerificationJudgementApproval.aspx"

            MagicAjax.AjaxCallHelper.Redirect("CustomerVerificationJudgementApproval.aspx")


        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub ImageAccept_Click(sender As Object, e As ImageClickEventArgs) Handles ImageAccept.Click
        Try


            EKABLL.ScreeningBLL.AcceptJudgement(PK_Aml_Customer_Judgement_ID, Sahassa.AML.Commonly.SessionUserId)

            Sahassa.AML.Commonly.SessionIntendedPage = "CustomerVerificationJudgementApproval.aspx"

            MagicAjax.AjaxCallHelper.Redirect("CustomerVerificationJudgementApproval.aspx")

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class

