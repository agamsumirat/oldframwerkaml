Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports Sahassa.AML.Commonly
Imports AMLBLL
Partial Class IFTIFileGenerate
    Inherits Parent


    Public Property ObjListOfGeneratedIFTI() As ListOfGeneratedIFTI
        Get
            If Session("IFTIFileGenerate.ObjListOfGeneratedIFTI") Is Nothing Then
                Session("IFTIFileGenerate.ObjListOfGeneratedIFTI") = ListOfGeneratedIFTIBLL.GetListOfGeneratedIFTIByPk(Me.PKListOfTransactionIFTI)
                Return CType(Session("IFTIFileGenerate.ObjListOfGeneratedIFTI"), ListOfGeneratedIFTI)
            Else
                Return CType(Session("IFTIFileGenerate.ObjListOfGeneratedIFTI"), ListOfGeneratedIFTI)
            End If
        End Get
        Set(ByVal value As ListOfGeneratedIFTI)
            Session("IFTIFileGenerate.ObjListOfGeneratedIFTI") = value
        End Set
    End Property

    Public ReadOnly Property PKListOfTransactionIFTI() As Long
        Get
            Dim temp As String
            Dim retvalue As Long
            temp = Request.Params("PkListOfGeneratedIFTIID")
            If Not Long.TryParse(temp, retvalue) Then
                Throw New Exception("PkListOfGeneratedIFTIID is not Vaid")
            End If
            Return retvalue
        End Get
    End Property


    Sub ClearSession()
        ObjListOfGeneratedIFTI = Nothing
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                ClearSession()
                Sahassa.aml.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.aml.Commonly.SessionCurrentPage & " succeeded"
                Using ObjAuditTrailUserAccess As AuditTrail_UserAccess = New AuditTrail_UserAccess
                    With ObjAuditTrailUserAccess
                        .AuditTrail_UserAccessUserid = Sahassa.AML.Commonly.SessionUserId
                        .AuditTrail_UserAccessActionDate = DateTime.Now
                        .AuditTrail_UserAccessAction = UserAccessAction
                    End With
                    DataRepository.AuditTrail_UserAccessProvider.Save(ObjAuditTrailUserAccess)
                End Using
                Me.LoadData()
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Sub LoadData()
        If Not ObjListOfGeneratedIFTI Is Nothing Then

            LblLastConfirmBy.Text = ObjListOfGeneratedIFTI.LastConfirmedBy

            Using objMsCTRReportTypeFile As MsCTRReportTypeFile = DataRepository.MsCTRReportTypeFileProvider.GetByPk_MsCTRReportTypeFile_Id(ObjListOfGeneratedIFTI.Fk_MsCTRReportTypeFile_Id)
                If Not objMsCTRReportTypeFile Is Nothing Then
                    LblReportType.Text = objMsCTRReportTypeFile.MsCTRReportTypeFile_Name
                End If
            End Using


            Using objMsStatusUploadPPATK As MsStatusUploadPPATK = DataRepository.MsStatusUploadPPATKProvider.GetByPk_MsStatusUploadPPATK_Id(ObjListOfGeneratedIFTI.Fk_MsStatusUploadPPATK_Id)
                If Not objMsStatusUploadPPATK Is Nothing Then
                    LblStatusUploadPPATK.Text = objMsStatusUploadPPATK.MsStatusUploadPPATK_Name
                End If
            End Using



            LblTotalInvalid.Text = ObjListOfGeneratedIFTI.TotalInvalid.GetValueOrDefault(0)
            LblTransactionType.Text = ObjListOfGeneratedIFTI.SwiftType
            LblTotalTransaction.text = ObjListOfGeneratedIFTI.TotalTransaksi
            LblTotalValid.Text = ObjListOfGeneratedIFTI.TotalValid
            LblTransactionDate.Text = ObjListOfGeneratedIFTI.TransactionDate.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy").Replace("01-Jan-1900", "")


        End If


    End Sub



    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Try
            Response.Redirect("ListOfGeneratedIFTIFile.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgGenerate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgGenerate.Click
        Dim dirPath As String = Server.MapPath(Request.ServerVariables("PATH_INFO")).Substring(0, Server.MapPath(Request.ServerVariables("PATH_INFO")).LastIndexOf("\") + 1) & "FolderExport\"
        Dim dirPathTemplate As String = Server.MapPath(Request.ServerVariables("PATH_INFO")).Substring(0, Server.MapPath(Request.ServerVariables("PATH_INFO")).LastIndexOf("\") + 1) & "FolderTemplate\"
        Dim FileReturn As String = ""
        Try
            dirPath = dirPath & Sahassa.AML.Commonly.SessionUserId & "\"
            If Not IO.Directory.Exists(dirPath) Then
                IO.Directory.CreateDirectory(dirPath)
            End If

            If Not ObjListOfGeneratedIFTI Is Nothing Then
                Dim intIFTItype As Integer
                Using objIFTI_Type As TList(Of IFTI_Type) = DataRepository.IFTI_TypeProvider.GetPaged(IFTI_TypeColumn.IFTIType.ToString & "='" & ObjListOfGeneratedIFTI.SwiftType & "'", "", 0, Integer.MaxValue, 0)
                    If objIFTI_Type.Count > 0 Then
                        intIFTItype = objIFTI_Type(0).PK_IFTI_Type_ID
                    End If
                End Using

                Select Case intIFTItype
                    Case 1
                        FileReturn = IFTIReportGeneratorBLL.GenerateReportXMLSwiftOutgoing(ObjListOfGeneratedIFTI.TransactionDate, ObjListOfGeneratedIFTI.LastUpdateIFTIDate, dirPath, dirPathTemplate)
                    Case 2
                        FileReturn = IFTIReportGeneratorBLL.GenerateReportXMLSwiftIncoming(ObjListOfGeneratedIFTI.TransactionDate, ObjListOfGeneratedIFTI.LastUpdateIFTIDate, dirPath, dirPathTemplate)
                    Case 3
                        FileReturn = IFTIReportGeneratorBLL.GenerateReportXMLNonSwiftOutgoing(ObjListOfGeneratedIFTI.TransactionDate, ObjListOfGeneratedIFTI.LastUpdateIFTIDate, dirPath, dirPathTemplate)
                    Case 4
                        FileReturn = IFTIReportGeneratorBLL.GenerateReportXMLNonSwiftIncoming(ObjListOfGeneratedIFTI.TransactionDate, ObjListOfGeneratedIFTI.LastUpdateIFTIDate, dirPath, dirPathTemplate)
                End Select



                If Not ObjListOfGeneratedIFTI Is Nothing Then
                    ObjListOfGeneratedIFTI.ReportDate = Now.ToString("yyyy-MM-dd")
                    ObjListOfGeneratedIFTI.Fk_MsStatusUploadPPATK_Id = 2
                    DataRepository.ListOfGeneratedIFTIProvider.Save(ObjListOfGeneratedIFTI)
                End If







                Response.Clear()
                Response.ClearHeaders()
                Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(FileReturn))
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "application/octet-stream"
                Response.BinaryWrite(IO.File.ReadAllBytes(FileReturn))
                Response.End()
            End If


        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


End Class
