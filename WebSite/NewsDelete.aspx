<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="NewsDelete.aspx.vb" Inherits="NewsDelete" title="News Delete" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>News - Delete&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
       <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/validationsign_animate.gif" /></td>
            <td bgcolor="#ffffff" colspan="3" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>
                The following News will be deleted :</strong></td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72">             
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                News ID</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:Label ID="LabelNewsID" runat="server"></asp:Label></td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px">&nbsp;</td>
			<td width="20%" bgColor="#ffffff" style="height: 24px">
                Title</td>
			<td width="5" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px">
                <asp:Label ID="lblTextNewsTitle" runat="server"></asp:Label>
            <asp:TextBox ID="TextNewsTitle" runat="server" CssClass="textBox" 
                    MaxLength="100" ReadOnly="True" Width="500px" Visible="False"></asp:TextBox><strong><span style="color: #ff0000"></span></strong></td>
		</tr>
		 <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                Start Date</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                :</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                <asp:Label ID="lblTextStartDate" runat="server"></asp:Label>
                <asp:TextBox ID="TextStartDate" runat="server" CssClass="textBox" 
                    MaxLength="20" ReadOnly="True" Visible="False"></asp:TextBox>
                </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                End Date</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                :</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                <asp:Label ID="lblTextEndDate" runat="server"></asp:Label>
                <asp:TextBox ID="TextEndDate" runat="server" CssClass="textBox" MaxLength="20" 
                    ReadOnly="True" Visible="False"></asp:TextBox>
                </td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px;">&nbsp;<br />
                <br />
                <br />
            </td>
			<td bgColor="#ffffff" style="height: 24px">
                News Summary<br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="height: 24px">
                :<br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="height: 24px">
                <asp:Label ID="lblTextNewsSummary" runat="server"></asp:Label>
                <ckeditor:ckeditorcontrol
                    id="TextNewsSummary" runat="server" maxlength="255" toolbar="Basic" 
                    ReadOnly="True" EditingBlock="False" Enabled="False" 
                    ToolbarStartupExpanded="False" Visible="False"></ckeditor:ckeditorcontrol><strong><span
                    style="color: #ff0000"></span></strong></td>
		</tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="height: 24px">
                News Content<br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="height: 24px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="height: 24px">
                &nbsp;<asp:Label ID="lblTextNewsContent" runat="server"></asp:Label>
                <ckeditor:ckeditorcontrol id="TextNewsContent" runat="server" maxlength="8000" 
                    toolbar="Basic" ReadOnly="True" Visible="False"></ckeditor:ckeditorcontrol>
                </td>
        </tr>
        <tr>
         <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td nowrap="noWrap" width="1%" bgcolor="#ffffff">
                Attachment
            </td>
            <td width="1"bgcolor="#ffffff">
                :
            </td>
            <td  bgcolor="#ffffff" >
                        <asp:GridView ID="GridOldAttach" runat="server" AutoGenerateColumns="False"
                            CellPadding="4" ForeColor="Black" GridLines="Vertical" DataKeyNames="PK_NewsAttachment"
                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" 
                            BorderWidth="1px" EnableModelValidation="True">
                            <FooterStyle BackColor="#CCCC99" />
                            <Columns>
                                <asp:BoundField HeaderText="PK_NewsAttachment" Visible="False"
                                    DataField="PK_NewsAttachment">
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Attach File">
                                    <ItemTemplate>
                                        <asp:DataList ID="DataList1" runat="server">
                                            <ItemTemplate>
                                                <%#GenerateLink(Eval("PK_NewsAttachment"), Eval("Filename"))%>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </td>
        </tr>            
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageUpdate" runat="server" CausesValidation="True" SkinID="DeleteButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>            
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	
</asp:Content>

