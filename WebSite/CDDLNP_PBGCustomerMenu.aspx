﻿ <%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CDDLNP_PBGCustomerMenu.aspx.vb" Inherits="CDDLNP_PBGCustomerMenu" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
                <td bgcolor="#ffffff" colspan="4" style="font-size: 23px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>
                    <img height="17" src="Images/dot_title.gif" width="17" />
                    <asp:Label ID="lblHeader" runat="server" Text="EDD for Private Banking Customer"></asp:Label>&nbsp;
                    <hr />
                </strong>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None" />
            </td>
        </tr>
    </table>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="0" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none">
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff" width="100%" colspan="2" style="font-size:18px">
                <strong>Create EDD File</strong></td>
        </tr>
        <tr>
            <td width="1%">
                &nbsp;
            </td>
            <td width="99%">
                <table>
                    <tr>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                <asp:ImageButton ID="ImageAddNew" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/newcustomer.gif" />
                            </ajax:AjaxPanel>
                        </td>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel2" runat="server">
                                <asp:ImageButton ID="ImageAddExisting" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/existingcustomer.gif" />
                            </ajax:AjaxPanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff" colspan="2" style="font-size:18px">
                <strong>EDD File Inquiry</strong></td>
        </tr>
        <tr>
            <td width="5px">
                &nbsp;
            </td>
            <td>
                <table>
                    <tr>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                                <asp:ImageButton ID="ImageViewNew" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/newcustomer.gif" />
                            </ajax:AjaxPanel>
                        </td>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                                <asp:ImageButton ID="ImageViewExisting" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/existingcustomer.gif" />
                            </ajax:AjaxPanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <%--        <tr class="formText" height="20px">
            <td bgcolor="#ffffff" colspan="2">
                &nbsp;</td>
        </tr>
        <tr class="formText" bgcolor="#dddddd" height="30">
            <td style="width: 5px">
                <img height="15" src="images/arrow.gif" width="15">
            </td>
            <td style="height: 9px">
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None" />
            </td>
        </tr>--%>
    </table>
</asp:Content>
