﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class PotentialCustomerVerification
    Inherits Parent


    Public Property FK_AML_Screening_Customer_Request_Id() As String
        Get
            Return Session("FK_AML_Screening_Customer_Request_Id")
        End Get
        Set(ByVal value As String)
            Session("FK_AML_Screening_Customer_Request_Id") = value
        End Set
    End Property

    Private Property CurrentVerificationListID() As String
        Get
            Return Session("CurrentVerificationListID")
        End Get
        Set(ByVal value As String)
            Session("CurrentVerificationListID") = value
        End Set
    End Property

    Private Sub PotentialCustomerVerification_Load(sender As Object, e As EventArgs) Handles Me.Load

        Try


            If FK_AML_Screening_Customer_Request_Id Is Nothing Then
                PanelResult.Visible = False
                PanelCustomerScreening.Visible = True

            Else

                PanelResult.Visible = True
                PanelCustomerScreening.Visible = False
                BindGrid()
                If GrdResult.Rows.Count > 0 Then
                    lblSearching.Text = ""
                    LoadDataSearching()
                    'Response.Redirect("PotentialCustomerVerification.aspx", False)

                    'MagicAjax.AjaxCallHelper.Write("AJAXCbo.ClearIntervalForAjaxCall();")



                    'MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(Integer.MaxValue)
                    Response.AppendHeader("Refresh", -1)


                Else
                    lblSearching.Text = "Please Wait Until Data Loading Finish..."
                    LoadDataSearching()
                    ClearWatchlist()

                    Response.AppendHeader("Refresh", 5)


                    'MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(5000)
                End If
            End If



            Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtDOB.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUp.Style.Add("display", "")

            GrdResult.SelectedIndex = -1

            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
            End Using

        Catch ex1 As MagicAjax.MagicAjaxException
            'Response.Redirect("PotentialCustomerVerification.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Function IsDataValid() As Boolean
        If TxtName.Text.Trim = "" Then
            Throw New Exception("Name /Alias is required.")
        End If

        Return True
    End Function


    Sub LoadDataSearching()
        If Not FK_AML_Screening_Customer_Request_Id Is Nothing Then
            Using objresult As EkaDataNettier.Entities.AML_Screening_Customer_Request_Detail = EKABLL.ScreeningBLL.GetScreeningResultByPk(FK_AML_Screening_Customer_Request_Id)
                If Not objresult Is Nothing Then
                    LblName.Text = objresult.Nama
                    If objresult.DOB.HasValue Then
                        LblDOB.Text = objresult.DOB.GetValueOrDefault.ToString("dd-MMM-yyyy")
                    Else
                        LblDOB.Text = ""
                    End If

                    LblNationality.Text = objresult.Nationality
                End If

            End Using
        End If

    End Sub
    Sub BindGrid()

        GrdResult.DataSource = EKABLL.ScreeningBLL.GetResult(FK_AML_Screening_Customer_Request_Id)
        GrdResult.DataBind()

    End Sub
    Sub ClearWatchlist()
        LblBirthPlaceWatchList.Text = ""
        LblDOBWatchList.Text = ""
        LblListTypeWatchList.Text = ""
        LblNameWatchList.Text = ""
        LblNationalityWatchList.Text = ""
        LblYOBWatchlist.Text = ""
        LblCustomRemarks1.Text = ""
        LblCustomRemarks2.Text = ""
        LblCustomRemarks3.Text = ""
        LblCustomRemarks4.Text = ""
        LblCustomRemarks5.Text = ""
        LblCategoryType.Text = ""
        linkcategory.Text = ""
    End Sub
    Sub LoadWatchlist(intverificationlist As Long)
        Dim objdt As Data.DataTable = EKABLL.ScreeningBLL.GetWatchListData(intverificationlist)
        If Not objdt Is Nothing Then
            If objdt.Rows.Count > 0 Then


                LblBirthPlaceWatchList.Text = objdt.Rows(0).Item("BirthPlace").ToString

                Dim strdob As String = ""
                Try
                    strdob = Convert.ToDateTime(objdt.Rows(0).Item("DateOfBirth")).ToString("dd-MMM-yyyy")
                Catch ex As Exception
                    strdob = objdt.Rows(0).Item("DateOfBirth").ToString
                End Try

                LblDOBWatchList.Text = strdob

                LblListTypeWatchList.Text = objdt.Rows(0).Item("ListTypeName").ToString
                LblCategoryType.Text = objdt.Rows(0).Item("CategoryName").ToString
                linkcategory.Text = objdt.Rows(0).Item("CategoryName").ToString
                LblNameWatchList.Text = objdt.Rows(0).Item("DisplayName").ToString
                LblNationalityWatchList.Text = objdt.Rows(0).Item("Nationality").ToString
                LblYOBWatchlist.Text = objdt.Rows(0).Item("YOB").ToString
                LblCustomRemarks1.Text = objdt.Rows(0).Item("CustomRemark1").ToString
                LblCustomRemarks2.Text = objdt.Rows(0).Item("CustomRemark2").ToString
                LblCustomRemarks3.Text = objdt.Rows(0).Item("CustomRemark3").ToString
                LblCustomRemarks4.Text = objdt.Rows(0).Item("CustomRemark4").ToString
                LblCustomRemarks5.Text = objdt.Rows(0).Item("CustomRemark5").ToString
                Dim intVerificationCategoryid As Integer = objdt.Rows(0).Item("VerificationListCategoryId").ToString

                Dim intcategoryworldcheck As String = EKABLL.ScreeningBLL.GetSystemParameterByValueByID(4000)
                Dim strreffid As String = objdt.Rows(0).Item("RefId").ToString
                If intVerificationCategoryid = intcategoryworldcheck Then
                    linkcategory.Visible = True
                    LblCategoryType.Visible = False
                Else
                    linkcategory.Visible = False
                    LblCategoryType.Visible = True
                End If

                linkcategory.OnClientClick = "window.open('showwatchlist.aspx?uid=" & strreffid & "', 'ca', 'width=500,height=250,left=500,top=500,resizable=yes,scrollbars=yes')"
            Else
                LblBirthPlaceWatchList.Text = ""
                LblDOBWatchList.Text = ""
                LblListTypeWatchList.Text = ""
                LblNameWatchList.Text = ""
                LblNationalityWatchList.Text = ""
                LblYOBWatchlist.Text = ""
                LblCustomRemarks1.Text = ""
                LblCustomRemarks2.Text = ""
                LblCustomRemarks3.Text = ""
                LblCustomRemarks4.Text = ""
                LblCustomRemarks5.Text = ""
                LblCategoryType.Text = ""
                linkcategory.Text = ""
            End If
        End If

    End Sub

    Sub LoadRelationWatchList(verificationID As Long)

        Dim objdata As Data.DataTable = EKABLL.ScreeningBLL.GetRelationcustomerScreening(verificationID)

        If objdata.Rows.Count > 0 Then
            LblAssociate.Visible = True
        Else
            LblAssociate.Visible = False
        End If

        GridViewRelationCstmr.DataSource = objdata
        GridViewRelationCstmr.DataBind()

        'Dim intVerificationCategoryid As Integer = objdata.Rows(0).Item("VerificationListCategoryId").ToString
        'Dim intcategoryworldcheck As String = EKABLL.ScreeningBLL.GetSystemParameterByValueByID(4000)
        'Dim strreffid As String = objdata.Rows(0).Item("RefId").ToString
        'If intVerificationCategoryid = intcategoryworldcheck Then
        '    linkcategory.Visible = True
        '    LblCategoryType.Visible = False
        'Else
        '    linkcategory.Visible = False
        '    LblCategoryType.Visible = True
        'End If

        'linkcategory.OnClientClick = "window.open('showwatchlist.aspx?uid=" & strreffid & "', 'ca', 'width=500,height=250,left=500,top=500,resizable=yes,scrollbars=yes')"

        'rptRelationCustomer.DataSource = objdata
        'rptRelationCustomer.DataBind()

        'LblNationality3.
        'rptRelationCustomer.Controls


    End Sub

    Private Sub ImageSearch_Click(sender As Object, e As ImageClickEventArgs) Handles ImageSearch.Click
        Try
            If IsDataValid() Then

                Dim dob As Nullable(Of Date) = Nothing
                If TxtDOB.Text.Trim.Length > 0 Then
                    dob = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TxtDOB.Text.Trim)
                End If
                FK_AML_Screening_Customer_Request_Id = Nothing
                FK_AML_Screening_Customer_Request_Id = EKABLL.ScreeningBLL.PotentialScreeningSatuan(TxtName.Text.Trim, dob, TxtNationality.Text.Trim)

                PanelResult.Visible = True
                PanelCustomerScreening.Visible = False
                BindGrid()
                LoadDataSearching()
                'MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(5000)
                'Response.AppendHeader("Refresh", 5)

                'lblSearching.Text = "Loading..."

                ClearWatchlist()
                ClearRelationWatchlistTable()

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub ClearInput()
        TxtName.Text = ""
        TxtDOB.Text = ""
        TxtNationality.Text = ""
    End Sub

    Private Sub ClearRelationWatchlistTable()

        GridViewRelationCstmr.DataSource = Nothing
        GridViewRelationCstmr.DataBind()

    End Sub

    Private Sub ImageBack_Click(sender As Object, e As ImageClickEventArgs) Handles ImageBack.Click
        Try


            FK_AML_Screening_Customer_Request_Id = Nothing
            CurrentVerificationListID = Nothing
            PanelResult.Visible = False
            PanelCustomerScreening.Visible = True
            ClearInput()

            'Response.Redirect("PotentialCustomerVerification.aspx", False)

            'MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(0)
            Response.AppendHeader("Refresh", -1)


        Catch ex1 As MagicAjax.MagicAjaxException

            ' Response.Redirect("PotentialCustomerVerification.aspx", False)
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub GrdResult_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GrdResult.SelectedIndexChanged
        Try
            Dim resultid As String = GrdResult.SelectedRow.Cells(2).Text

            'MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(0)

            Response.AppendHeader("Refresh", -1)

            If Integer.TryParse(resultid, 0) Then
                LoadWatchlist(resultid)
                LoadRelationWatchList(resultid)
            End If

            CurrentVerificationListID = resultid


        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GrdResult_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GrdResult.PageIndexChanging
        Try
            GrdResult.PageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    'Private Sub GridViewRelationCstmr_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridViewRelationCstmr.SelectedIndexChanged
    '    Try
    '        'Dim resultidrelation As String = GridViewRelationCstmr.SelectedRow.Cells(3).Text
    '        Dim index As Integer = GridViewRelationCstmr.SelectedRow.RowIndex
    '        'Dim categoryID As String = GridViewRelationCstmr.SelectedRow.Cells(1).Text
    '        'Dim refID As String = GridViewRelationCstmr.SelectedRow.Cells(2).Text

    '        Dim categoryID As String = GridViewRelationCstmr.DataKeys(index).Values(0)
    '        Dim refID As String = GridViewRelationCstmr.DataKeys(index).Values(1)

    '        Dim scriptManager As ClientScriptManager = Page.ClientScript

    '        Response.AppendHeader("Refresh", -1)

    '        Dim worldCheckCategory As String = EKABLL.ScreeningBLL.GetSystemParameterByValueByID(4000)

    '        If categoryID = worldCheckCategory Then

    '            'Dim link As LinkButton = e.Row.FindControl("lnkDetailRelation")
    '            'linkcategory.OnClientClick = "window.open('showwatchlist.aspx?uid=" & strreffid & "', 'ca', 'width=500,height=250,left=500,top=500,resizable=yes,scrollbars=yes')"

    '            scriptManager.RegisterClientScriptBlock(Page.GetType(), "OpenWindow", "window.open('showwatchlist.aspx?uid=" & refID & "', 'ca', 'width=500,height=250,left=500,top=500,resizable=yes,scrollbars=yes')")
    '        Else
    '            scriptManager.RegisterClientScriptBlock(Page.GetType(), "OpenWindow", "window.open('www.google.com')")
    '        End If

    '    Catch ex As Exception
    '        LogError(ex)
    '        Me.cvalPageError.IsValid = False
    '        Me.cvalPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    'Private Sub GridViewRelationCstmr_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridViewRelationCstmr.RowCommand

    '    If e.CommandName = "test" Then
    '        Dim x As LinkButton = DirectCast(e.CommandSource, LinkButton)
    '        x.OnClientClick = "window.open('www.google.com')"
    '    End If

    'End Sub

    Private Sub GridViewRelationCstmr_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridViewRelationCstmr.RowDataBound

        Dim relationCategoryID As String
        Dim refID As String
        Dim relationCustomerID As String
        Dim linkButton As LinkButton
        Dim index As Integer
        Dim worldCheckCategory As String
        Dim builder As New StringBuilder()

        If e.Row.RowType = DataControlRowType.DataRow Then

            index = e.Row.RowIndex

            refID = GridViewRelationCstmr.DataKeys(index).Values(0)
            relationCategoryID = GridViewRelationCstmr.DataKeys(index).Values(1)

            'Bisa verificationlistid jika dia watchlist atau nomor CIF dari CFMAST jika dia customer
            relationCustomerID = GridViewRelationCstmr.DataKeys(index).Values(2)

            linkButton = DirectCast(e.Row.Cells(0).FindControl("lnkRelationDetail"), LinkButton)

            'Jika tidak ada relation category name nya kemungkinan dia bertipe customer (dari CFMAST)
            'Karena tidak ada detailnya, maka linkbutton detail dihilangkan dan proses tidak perlu dilanjutkan
            If relationCategoryID = "0" Then
                linkButton.Visible = False
            Else
                worldCheckCategory = EKABLL.ScreeningBLL.GetSystemParameterByValueByID(4000)

                If relationCategoryID = worldCheckCategory Then
                    builder.Append("window.open('showwatchlist.aspx?uid=")
                    builder.Append(refID)
                    builder.Append("', 'ca', 'width=500,height=250,left=500,top=500,resizable=yes,scrollbars=yes')")
                Else
                    builder.Append("window.open('ShowWatchlist_NonWorldCheck.aspx?uid=")
                    builder.Append(relationCustomerID)
                    builder.Append("', 'ca', 'width=500,height=250,left=500,top=500,resizable=yes,scrollbars=yes')")
                End If

                linkButton.OnClientClick = builder.ToString()
            End If

        End If

    End Sub

End Class
