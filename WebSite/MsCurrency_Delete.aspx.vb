#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
#End Region

Partial Class MsCurrency_Delete
    Inherits Parent

#Region "Function"

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = Listmaping
        LBMapping.DataTextField = "Description"
        LBMapping.DataValueField = "IdCurrencyNCBS"
        LBMapping.DataBind()
    End Sub


#End Region

#Region "events..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                Listmaping = New TList(Of MsCurrencyNCBS)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim ObjMsCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.IdCurrency.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsCurrency Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsCurrency
            SafeDefaultValue = "-"
            lblIdCurrency.Text = .IdCurrency
            lblCode.Text = Safe(.Code)
            lblName.Text = Safe(.Name)

            Dim L_objMappingMsCurrencyNCBSPPATK As TList(Of MappingMsCurrencyNCBSPPATK)
            L_objMappingMsCurrencyNCBSPPATK = DataRepository.MappingMsCurrencyNCBSPPATKProvider.GetPaged(MappingMsCurrencyNCBSPPATKColumn.IdCurrency.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            For Each i As MappingMsCurrencyNCBSPPATK In L_objMappingMsCurrencyNCBSPPATK
                Dim TempObj As MsCurrencyNCBS = DataRepository.MsCurrencyNCBSProvider.GetByIdCurrencyNCBS(i.PK_MappingMsCurrencyNCBSPPATK_Id)
                If TempObj Is Nothing Then Continue For
                Listmaping.Add(TempObj)
            Next
        End With
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

#Region "Property..."

    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MsCurrencyNCBS)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MsCurrencyNCBS))
            Session("Listmaping.data") = value
        End Set
    End Property

#End Region

#Region "events..."
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsCurrency_View.aspx")
    End Sub

    Protected Sub imgOk_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOk.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then

                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsCurrency_Approval As New MsCurrency_Approval
                    With ObjMsCurrency_Approval
                        .FK_MsMode_Id = 3
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsCurrency_ApprovalProvider.Save(ObjMsCurrency_Approval)
                    KeyHeaderApproval = ObjMsCurrency_Approval.PK_MsCurrencyPPATK_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsCurrency_ApprovalDetail As New MsCurrency_ApprovalDetail()
                    With objMsCurrency_ApprovalDetail
                        Dim ObjMsCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(parID)
                        FillOrNothing(.IdCurrency, lblIdCurrency.Text, True, oInt)
                        FillOrNothing(.Code, lblCode.Text, True, Ovarchar)
                        FillOrNothing(.Name, lblName.Text, True, Ovarchar)

                        FillOrNothing(.IdCurrency, ObjMsCurrency.IdCurrency)
                        FillOrNothing(.Activation, ObjMsCurrency.Activation)
                        FillOrNothing(.CreatedDate, ObjMsCurrency.CreatedDate)
                        FillOrNothing(.CreatedBy, ObjMsCurrency.CreatedBy)
                        FillOrNothing(.PK_MsCurrency_Approval_id, KeyHeaderApproval)
                    End With
                    DataRepository.MsCurrency_ApprovalDetailProvider.Save(objMsCurrency_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsCurrency_ApprovalDetail.PK_MsCurrency_Approval_detail_id

                    '========= Insert mapping item 
                    Dim LobjMappingMsCurrencyNCBSPPATK_Approval_Detail As New TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail)

                    Dim L_ObjMappingMsCurrencyNCBSPPATK As TList(Of MappingMsCurrencyNCBSPPATK) = DataRepository.MappingMsCurrencyNCBSPPATKProvider.GetPaged(MappingMsCurrencyNCBSPPATKColumn.IdCurrency.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

                    For Each objMappingMsCurrencyNCBSPPATK As MappingMsCurrencyNCBSPPATK In L_ObjMappingMsCurrencyNCBSPPATK
                        Dim objMappingMsCurrencyNCBSPPATK_Approval_Detail As New MappingMsCurrencyNCBSPPATK_Approval_Detail
                        With objMappingMsCurrencyNCBSPPATK_Approval_Detail
                            FillOrNothing(.Pk_MsCurrency_Id, objMappingMsCurrencyNCBSPPATK.PK_MappingMsCurrencyNCBSPPATK_Id)
                            FillOrNothing(.PK_MappingMsCurrencyNCBSPPATK_Id, objMappingMsCurrencyNCBSPPATK.IdCurrencyNCBS)
                            FillOrNothing(.PK_MappingMsCurrencyNCBSPPATK_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.PK_MappingMsCurrencyNCBSPPATK_Id, objMappingMsCurrencyNCBSPPATK.PK_MappingMsCurrencyNCBSPPATK_Id)
                            LobjMappingMsCurrencyNCBSPPATK_Approval_Detail.Add(objMappingMsCurrencyNCBSPPATK_Approval_Detail)
                        End With
                        DataRepository.MappingMsCurrencyNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsCurrencyNCBSPPATK_Approval_Detail)
                    Next
                    'session Maping item Clear
                    Listmaping.Clear()
                    Me.imgOk.Visible = False
                    Me.ImageCancel.Visible = False
                    LblConfirmation.Text = "Data has been Delete and waiting for approval"
                    MtvMsUser.ActiveViewIndex = 1
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Response.Redirect("MsCurrency_View.aspx")
    End Sub

#End Region

End Class



