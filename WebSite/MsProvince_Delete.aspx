<%@ Page Language="VB" AutoEventWireup="false" CodeFile="~/MsProvince_Delete.aspx.vb"
	Inherits="MsProvince_Delete" MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
	<table cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>
				<img src="Images/blank.gif" width="5" height="1" />
			</td>
			<td>
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td>
							&nbsp;
						</td>
						<td width="99%" bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" />
						</td>
						<td bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" />
						</td>
					</tr>
				</table>
			</td>
			<td>
				<img src="Images/blank.gif" width="5" height="1" />
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td id="tdcontent" valign="top" bgcolor="#FFFFFF">
				
					<table id="tblpenampung" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td style="width: 14px">
								<img src="Images/blank.gif" width="20" height="100%" />
							</td>
							<td class="divcontentinside" bgcolor="#FFFFFF" style="width: 100%">
								&nbsp;
								<ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%" meta:resourcekey="AjaxPanel5Resource1">
									<asp:MultiView ID="MtvMsUser" runat="server" ActiveViewIndex="0">
										<asp:View ID="VwAdd" runat="server">
											<table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
												width="100%" bgcolor="#dddddd" border="0">
												<tr>
													<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
														border-right-style: none; border-left-style: none; border-bottom-style: none"
														valign="top">
														<img src="Images/dot_title.gif" width="17" height="17">
														<asp:Label ID="LblValue" runat="server" Font-Bold="True" Font-Size="Medium" 
															Text="%%Title%% Delete"></asp:Label>
														<hr />
													</td>
												</tr>
												<tr>
													<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
														border-right-style: none; border-left-style: none; border-bottom-style: none;
														height: 26px;" valign="top">
													</td>
												</tr>
											<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 15px">
    </td>
    <td bgcolor="#fff7e6" style="width: 156px; height: 15px" valign="top">
        <asp:label id="LabelIdProvince" runat="server" 
            text="ID"></asp:label>
    </td>
    <td style="width: 6px; height: 15px" valign="top">
        :
    </td>
    <td colspan="7" style="height: 15px" valign="top">
        <asp:label id="lblIdProvince" runat="server" forecolor="#404040" ></asp:label>
    </td>
</tr>

<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 15px">
    </td>
    <td bgcolor="#fff7e6" style="width: 156px; height: 15px" valign="top">
        <asp:label id="LabelCode" runat="server" 
            text="Code"></asp:label>
    </td>
    <td style="width: 6px; height: 15px" valign="top">
        :
    </td>
    <td colspan="7" style="height: 15px" valign="top">
        <asp:label id="lblCode" runat="server" forecolor="#404040" ></asp:label>
    </td>
</tr>

<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 15px">
    </td>
    <td bgcolor="#fff7e6" style="width: 156px; height: 15px" valign="top">
        <asp:label id="LabelNama" runat="server" 
            text="Nama"></asp:label>
    </td>
    <td style="width: 6px; height: 15px" valign="top">
        :
    </td>
    <td colspan="7" style="height: 15px" valign="top">
        <asp:label id="lblNama" runat="server" forecolor="#404040" ></asp:label>
    </td>
</tr>

<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 15px">
    </td>
    <td bgcolor="#fff7e6" style="width: 156px; height: 15px" valign="top">
        <asp:label id="LabelDescription" runat="server" 
            text="Description"></asp:label>
    </td>
    <td style="width: 6px; height: 15px" valign="top">
        :
    </td>
    <td colspan="7" style="height: 15px" valign="top">
        <asp:label id="lblDescription" runat="server" forecolor="#404040" ></asp:label>
    </td>
</tr>


												<tr bgcolor="#ffffff">
													<td style="width: 22px; height: 24px">
														&nbsp;</td>
													<td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
														Mapping Item
													</td>
													<td style="width: 6px; height: 24px" valign="top">
														:</td>
													<td style="height: 24px" valign="top">
														<asp:ListBox ID="LBMapping" runat="server" Height="204px" 
															Width="170px" CssClass="textbox"></asp:ListBox>
													</td>
												</tr>
												<tr bgcolor="#ffffff">
													<td style="width: 22px; height: 24px">
														&nbsp;</td>
													<td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
														&nbsp;</td>
													<td style="width: 6px; height: 24px" valign="top">
														&nbsp;</td>
													<td style="height: 24px" valign="top">
														&nbsp;</td>
												</tr>
												<tr bgcolor="#ffffff">
													<td style="width: 22px; height: 24px">
														&nbsp;</td>
													<td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
														&nbsp;</td>
													<td style="width: 6px; height: 24px" valign="top">
														&nbsp;</td>
													<td style="height: 24px" valign="top">
														&nbsp;</td>
												</tr>
											</table>
										</asp:View>
										<asp:View ID="VwConfirmation" runat="server">
											<table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
												width="100%" bgcolor="#dddddd" border="0">
												<tr bgcolor="#ffffff">
													<td colspan="2" align="center" style="height: 17px">
														<asp:Label ID="LblConfirmation" runat="server" meta:resourcekey="LblConfirmationResource1"></asp:Label>
													</td>
												</tr>
												<tr bgcolor="#ffffff">
													<td align="center" colspan="2">
														<asp:ImageButton ID="ImgBtnAdd" runat="server" ImageUrl="~/images/button/Ok.gif"
															CausesValidation="False" meta:resourcekey="ImgBtnAddResource1" />
													</td>
												</tr>
											</table>
										</asp:View>
									</asp:MultiView>
								</ajax:AjaxPanel>
							</td>
						</tr>
					</table>
				
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
				<ajax:AjaxPanel ID="AjaxPanel1" runat="server">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td background="Images/button-bground.gif" align="left" valign="middle">
								<img src="Images/blank.gif" width="5" height="1" /></td>
							<td background="Images/button-bground.gif" align="left" valign="middle">
								&nbsp;</td>
							<td background="Images/button-bground.gif" style="width: 5px">
								&nbsp;</td>
							<td background="Images/button-bground.gif">
								&nbsp;</td>
							<td background="Images/button-bground.gif">
							<asp:ImageButton ID="imgOk" runat="server" CausesValidation="False" 
									ImageUrl="~/Images/button/process.gif"></asp:ImageButton>
								</td>
							<td background="Images/button-bground.gif">
								&nbsp;<asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" 
									ImageUrl="~/Images/button/back.gif"></asp:ImageButton></td>
							<td width="99%" background="Images/button-bground.gif">
								<img src="Images/blank.gif" width="1" height="1" />
								<asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" 
									meta:resourcekey="CvalHandleErrResource1" ValidationGroup="handle"></asp:CustomValidator>
								<asp:CustomValidator ID="CvalPageErr" runat="server" Display="None" 
									meta:resourcekey="CvalPageErrResource1"></asp:CustomValidator>
							</td>
							<td>
								</td>
						</tr>
					</table>
				</ajax:AjaxPanel>
			</td>
		</tr>
	</table>
</asp:Content>

