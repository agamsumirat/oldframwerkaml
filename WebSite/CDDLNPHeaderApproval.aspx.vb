Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL

Partial Class CDDLNPHeaderApproval
    Inherits Parent

#Region " Property "
    Private Property SetnGetFK_MsMode_Id() As Int32
        Get
            Return CType(IIf(Session("CDDLNPHeaderApproval.FK_MsMode_Id") Is Nothing, 0, Session("CDDLNPHeaderApproval.FK_MsMode_Id")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPHeaderApproval.FK_MsMode_Id") = Value
        End Set
    End Property
    Private Property SetnGetRequestedBy() As String
        Get
            Return CType(IIf(Session("CDDLNPHeaderApproval.RequestedBy") Is Nothing, String.Empty, Session("CDDLNPHeaderApproval.RequestedBy")), String)
        End Get
        Set(ByVal Value As String)
            Session("CDDLNPHeaderApproval.RequestedBy") = Value
        End Set
    End Property
    Private Property SetnGetRequestedDateFrom() As String
        Get
            Return CType(IIf(Session("CDDLNPHeaderApproval.RequestedDateFrom") Is Nothing, String.Empty, Session("CDDLNPHeaderApproval.RequestedDateFrom")), String)
        End Get
        Set(ByVal Value As String)
            Session("CDDLNPHeaderApproval.RequestedDateFrom") = Value
        End Set
    End Property
    Private Property SetnGetRequestedDateTo() As String
        Get
            Return CType(IIf(Session("CDDLNPHeaderApproval.RequestedDateTo") Is Nothing, String.Empty, Session("CDDLNPHeaderApproval.RequestedDateTo")), String)
        End Get
        Set(ByVal Value As String)
            Session("CDDLNPHeaderApproval.RequestedDateTo") = Value
        End Set
    End Property

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("CDDLNPHeaderApproval.Selected") Is Nothing, New ArrayList, Session("CDDLNPHeaderApproval.Selected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CDDLNPHeaderApproval.Selected") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CDDLNPHeaderApproval.Sort") Is Nothing, "PK_CDD_Bagian_Approval_Id  desc", Session("CDDLNPHeaderApproval.Sort"))
        End Get
        Set(ByVal Value As String)
            Session("CDDLNPHeaderApproval.Sort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CDDLNPHeaderApproval.CurrentPage") Is Nothing, 0, Session("CDDLNPHeaderApproval.CurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPHeaderApproval.CurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CDDLNPHeaderApproval.RowTotal") Is Nothing, 0, Session("CDDLNPHeaderApproval.RowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPHeaderApproval.RowTotal") = Value
        End Set
    End Property
    Private Function SetnGetBindTable() As VList(Of vw_CDD_Bagian_Approval)
        Return DataRepository.vw_CDD_Bagian_ApprovalProvider.GetPaged(SearchFilter, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, SetnGetRowTotal)
    End Function
#End Region

    Private Function SearchFilter() As String
        Dim strWhereClause(-1) As String

        If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
            Me.SetnGetCurrentPage = Me.GetPageTotal - 1
        ElseIf Me.SetnGetCurrentPage = -1 Then
            Me.SetnGetCurrentPage = 0
        End If

        Try

            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = "RequestedBy <> '" & Sahassa.AML.Commonly.SessionPkUserId & "' AND RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Sahassa.AML.Commonly.SessionUserId & "'))"

            If Me.SetnGetFK_MsMode_Id > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "FK_MsMode_Id = " & SetnGetFK_MsMode_Id
            End If

            If Me.SetnGetRequestedBy.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "UserName LIKE '%" & SetnGetRequestedBy & "%'"
            End If

            If Me.SetnGetRequestedDateFrom.Length > 0 AndAlso Me.SetnGetRequestedDateTo.Length > 0 Then
                If IsDate(SetnGetRequestedDateFrom) AndAlso IsDate(SetnGetRequestedDateTo) Then
                    If SetnGetRequestedDateFrom <= SetnGetRequestedDateTo Then
                        ReDim Preserve strWhereClause(strWhereClause.Length)
                        strWhereClause(strWhereClause.Length - 1) = "(CONVERT(DATETIME,CONVERT(VARCHAR(20),RequestedDate,112),112)  BETWEEN '" & SetnGetRequestedDateFrom & "' AND '" & SetnGetRequestedDateTo & "')"
                    Else
                        Throw New Exception("Requested date searching is not valid")
                    End If
                Else
                    Throw New Exception("Requested date format is not valid")
                End If
            End If

            Return String.Join(" AND ", strWhereClause)
        Catch
            Throw
        End Try
    End Function
    Private Sub ClearThisPageSessions()
        Me.SetnGetFK_MsMode_Id = 0
        Me.SetnGetRequestedBy = String.Empty
        Me.SetnGetRequestedDateFrom = String.Empty
        Me.SetnGetRequestedDateTo = String.Empty
        Me.SetnGetSelectedItem = Nothing
        Me.SetnGetSort = Nothing
        Me.SetnGetCurrentPage = Nothing
        Me.SetnGetRowTotal = Nothing
    End Sub
    Private Sub BindComboBox()
        ddlMode.Items.Clear()
        ddlMode.Items.Add(New ListItem("[All]", 0))
        For Each objMode As SahassaNettier.Entities.Mode In SahassaNettier.Data.DataRepository.ModeProvider.GetAll
            ddlMode.Items.Add(New ListItem(objMode.Nama, objMode.MsModeID))
        Next
    End Sub
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridViewCDDLNP.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                Dim PkId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PkId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PkId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PkId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            CollectSelected()
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub SetInfoNavigate()
        Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
        Me.PageTotalPages.Text = Me.GetPageTotal
        Me.PageTotalRows.Text = Me.SetnGetRowTotal
        Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
        Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
    End Sub
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls
    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim totalrow As Int16 = 0
        For Each gridRow As DataGridItem In Me.GridViewCDDLNP.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                totalrow = CType(totalrow + 1, Int16)
            End If
        Next
        If i = totalrow Then
            Me.CheckBoxSelectAll.Checked = True
        Else
            Me.CheckBoxSelectAll.Checked = False
        End If
    End Sub
    Private Sub BindGrid()
        SettingControlSearching()
        Me.GridViewCDDLNP.DataSource = Me.SetnGetBindTable
        Me.GridViewCDDLNP.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridViewCDDLNP.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridViewCDDLNP.DataBind()

        If SetnGetRowTotal > 0 Then
            LabelNoRecordFound.Visible = False
        Else
            LabelNoRecordFound.Visible = True
        End If
    End Sub
    Private Sub SettingControlSearching()
        txtRequestedBy.Text = Me.SetnGetRequestedBy
        txtRequestedDateFrom.Text = Me.SetnGetRequestedDateFrom
        txtRequestedDateTo.Text = Me.SetnGetRequestedDateTo
        ddlMode.SelectedValue = Me.SetnGetFK_MsMode_Id
    End Sub
    Private Sub BindSelected()
        Dim SbPk As New StringBuilder
        SbPk.Append("0, ")
        For Each IdPk As Int64 In Me.SetnGetSelectedItem
            SbPk.Append(IdPk.ToString & ", ")
        Next

        Me.GridViewCDDLNP.DataSource = DataRepository.vw_CDD_Bagian_ApprovalProvider.GetPaged("PK_CDD_Bagian_Approval_Id in (" & SbPk.ToString.Substring(0, SbPk.Length - 2) & ") ", Me.SetnGetSort, 0, Integer.MaxValue, 0)
        Me.GridViewCDDLNP.AllowPaging = False
        Me.GridViewCDDLNP.DataBind()

        Me.GridViewCDDLNP.Columns(0).Visible = False
        Me.GridViewCDDLNP.Columns(1).Visible = False
        Me.GridViewCDDLNP.Columns(GridViewCDDLNP.Columns.Count - 1).Visible = False
    End Sub
    Private Sub BindSelectedAll()
        Me.GridViewCDDLNP.DataSource = DataRepository.vw_CDD_Bagian_ApprovalProvider.GetPaged(SearchFilter, Me.SetnGetSort, 0, Int32.MaxValue, 0)
        Me.GridViewCDDLNP.AllowPaging = False
        Me.GridViewCDDLNP.DataBind()

        Me.GridViewCDDLNP.Columns(0).Visible = False
        Me.GridViewCDDLNP.Columns(1).Visible = False
        Me.GridViewCDDLNP.Columns(GridViewCDDLNP.Columns.Count - 1).Visible = False
    End Sub

#Region "Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                Me.ClearThisPageSessions()
                Me.BindComboBox()
                PopRequestedDateFrom.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtRequestedDateFrom.ClientID & "'), 'dd-mmm-yyyy')")
                PopRequestedDateTo.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtRequestedDateTo.ClientID & "'), 'dd-mmm-yyyy')")

                Me.GridViewCDDLNP.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
            Me.SetCheckedAll()
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.SetnGetFK_MsMode_Id = ddlMode.SelectedValue
            Me.SetnGetRequestedBy = txtRequestedBy.Text
            Me.SetnGetRequestedDateFrom = txtRequestedDateFrom.Text
            Me.SetnGetRequestedDateTo = txtRequestedDateTo.Text
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub ImageClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageClear.Click
        Try
            Me.ClearThisPageSessions()
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub GridViewCDDLNP_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewCDDLNP.ItemCommand
        Select Case e.CommandName.ToLower
            Case "detail"
                Response.Redirect("CDDLNPHeaderApprovalDetail.aspx?ID=" & e.Item.Cells(1).Text)

        End Select
    End Sub
    Protected Sub GridViewCDDLNP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewCDDLNP.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim chkBox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub GridViewCDDLNP_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridViewCDDLNP.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) AndAlso (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
            CollectSelected()
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridViewCDDLNP.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim pkid As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(pkid) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            Else
        '                ArrTarget.Remove(pkid)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next

        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridViewCDDLNP.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget

    End Sub
    Protected Sub lnkExportData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportData.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=CDDLNPHeaderApproval_Selected.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridViewCDDLNP)
            GridViewCDDLNP.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub lnkExportAllData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAllData.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelectedAll()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=CDDLNPHeaderApproval_All.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridViewCDDLNP)
            GridViewCDDLNP.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
#End Region
End Class