Imports System.Data.SqlClient
Imports Sahassa.AML.TableAdapterHelper
Partial Class ProposalSTRCreateDetail
    Inherits Parent
#Region "Property"

    Private ReadOnly Property PKCaseManagementID() As Integer
        Get

            Dim temp As String = ""
            Dim jml As Integer = 0
            temp = Request.Params("PK_CaseManagementID")
            If temp = "" Or Not IsNumeric(temp) Then
                Throw New Exception("PK_CaseManagementID not valid")
            Else
                'Cek kalau untuk pKCaseManagement ini kalau sudah ada proposalSTRnya redirect ke ProposalSTRCreateview.aspx
                Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRTableAdapter
                    jml = adapter.CountProposalSTRByFKCaseManagementID(temp)
                    If jml > 0 Then
                        Response.Redirect("ProposalSTRCreateView.aspx", False)
                    Else
                        Return temp
                    End If
                End Using
            End If
        End Get
    End Property
#End Region
    Private Function GetEmailAddress(ByVal PKUserID As String) As String
        Try
            Using adapter As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                Using otable As AMLDAL.AMLDataSet.UserDataTable = adapter.GetDataByUserID(PKUserID)
                    If otable.Rows.Count > 0 Then
                        If CType(otable.Rows(0), AMLDAL.AMLDataSet.UserRow).IsUserEmailAddressNull Then
                            Return ""
                        Else
                            Return CType(otable.Rows(0), AMLDAL.AMLDataSet.UserRow).UserEmailAddress
                        End If
                    Else
                        Return ""
                    End If
                End Using
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Function GetUserIDbyPK(ByVal PKUSERID As String) As String
        Try
            Using adapter As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                Using oTable As AMLDAL.AMLDataSet.UserDataTable = adapter.GetDataByUserID(PKUSERID)
                    If oTable.Rows.Count > 0 Then
                        Return CType(oTable.Rows(0), AMLDAL.AMLDataSet.UserRow).UserID
                    End If
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function
    ''' <summary>
    ''' start workflow untuk proposal str
    ''' </summary>
    ''' <param name="osqltrans"></param>
    ''' <remarks></remarks>
    Private Sub StartWorkFlow(ByVal intPK As Integer, ByRef osqltrans As SqlTransaction)
        Dim oWorkflowSettting() As AMLDAL.ProposalSTR.ProposalSTRSettingWorkflowRow
        Try
            'insert ke table proposalworkflowhistory dengan eventtype workflowstarted
            Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRWorkflowHistoryTableAdapter
                SetTransaction(adapter, osqltrans)
                Using adapterWorkflow As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRSettingWorkflowTableAdapter
                    SetTransaction(adapterWorkflow, osqltrans)
                    'Cari yang paling rendah
                    oWorkflowSettting = adapterWorkflow.GetSettingWorkflow().Select("serialno=1")
                    If oWorkflowSettting.Length > 0 Then
                        'insert workflow Started
                        adapter.Insert(intPK, Nothing, EnProposalEventType.WorkFlowStated, "", "", "", "", Nothing, Now)

                    End If
                    For i As Integer = 0 To oWorkflowSettting.Length - 1
                        Dim strPKUserApprover As String = oWorkflowSettting(i).Approvers
                        Dim arrPKUserApprover() As String = strPKUserApprover.Split("|")
                        Dim strPKNotifyUser As String = oWorkflowSettting(i).NotifyOthers
                        Dim arrPKNOtifyUser() As String = strPKNotifyUser.Split("|")
                        Dim strEmailRecepients As String = ""
                        Dim strEmailCC As String = ""
                        Dim strEmailSubject As String = ""
                        Dim strEmailBody As String = ""

                        Dim strListUser As String = ""
                        Dim strDescription As String = ""
                        For j As Integer = 0 To arrPKUserApprover.Length - 1
                            strListUser += GetUserIDbyPK(arrPKUserApprover(j))
                            strEmailRecepients += GetEmailAddress(arrPKUserApprover(j))
                            If j <> arrPKUserApprover.Length - 1 Then
                                strListUser += ";"
                                strEmailRecepients += ";"
                            End If
                        Next
                        For j As Integer = 0 To arrPKNOtifyUser.Length - 1
                            strEmailCC += GetEmailAddress(arrPKNOtifyUser(j))
                            If j <> arrPKNOtifyUser.Length - 1 Then
                                strEmailCC += ";"
                            End If
                        Next
                        strEmailSubject = getEmailSubject(oWorkflowSettting(i).SerialNo)
                        strEmailBody = getEmailBody(oWorkflowSettting(i).SerialNo)
                        If oWorkflowSettting.Length > 1 Then
                            strDescription = "Parallel " & i + 1
                        Else
                            strDescription = ""
                        End If
                        adapter.Insert(intPK, Nothing, EnProposalEventType.TaskStarted, oWorkflowSettting(i).WorkflowStep, strListUser, "", "", False, Now)
                        SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                    Next
                End Using
                '

            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Private Sub SendEmail(ByVal StrRecipientTo As String, ByVal StrRecipientCC As String, ByVal StrSubject As String, ByVal strbody As String)
        Dim oEmail As Sahassa.AML.EMail
        Try
            oEmail = New Sahassa.AML.EMail
            oEmail.Sender = System.Configuration.ConfigurationManager.AppSettings("FromMailAddress")
            oEmail.Recipient = StrRecipientTo
            oEmail.RecipientCC = StrRecipientCC
            oEmail.Subject = StrSubject
            oEmail.Body = strbody.Replace(vbLf, "<br>")
            oEmail.SendEmail()
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Private Function getEmailBody(ByVal serialno As Integer) As String
        Try
            Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRWorkflowEmailTemplateTableAdapter
                Dim orow() As AMLDAL.ProposalSTR.ProposalSTRWorkflowEmailTemplateRow = adapter.GetData.Select("serialno=" & serialno)
                If orow.Length > 0 Then
                    If Not orow(0).IsBody_EmailTemplateNull Then
                        Return orow(0).Body_EmailTemplate
                    Else
                        Return ""
                    End If
                Else
                    Return ""
                End If
            End Using

        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Function getEmailSubject(ByVal serialno As Integer) As String
        Try
            Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRWorkflowEmailTemplateTableAdapter
                Dim orow() As AMLDAL.ProposalSTR.ProposalSTRWorkflowEmailTemplateRow = adapter.GetData.Select("serialno=" & serialno)
                If orow.Length > 0 Then
                    If Not orow(0).IsSubject_EmailTemplateNull Then
                        Return orow(0).Subject_EmailTemplate
                    Else
                        Return ""
                    End If
                Else
                    Return ""
                End If
            End Using



        Catch ex As Exception
            LogError(ex)
            Throw


        End Try
    End Function
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Dim intPK As Integer
        Try
            If Page.IsValid Then
                Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRTableAdapter
                    oSQLTrans = BeginTransaction(adapter)
                    intPK = adapter.InsertProposalSTRWorkflowHistory(Me.PKCaseManagementID, Now, TextKasusPosisi.Text.Trim, TextIndikatorMencurigakan.Text.Trim, TextUnsurTKM.Text.Trim, TextLainLain.Text.Trim, TextKesimpulan.Text.Trim, EnProposalStatus.bNew, Sahassa.AML.Commonly.SessionUserId)
                    'Startworkflowproposalstr
                    StartWorkFlow(intPK, oSQLTrans)
                    oSQLTrans.Commit()
                    Response.Redirect("ProposalSTRCreateView.aspx", False)
                End Using
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            LogError(ex)
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
            End If
        End Try
    End Sub
    Public Enum EnProposalStatus
        bNew = 1
        InProgress
        Complated
    End Enum
    Public Enum EnProposalEventType
        WorkFlowStated = 1
        TaskStarted
        TaskCompleted
        TaskCanceledBySystem
        WorkFlowFinish
    End Enum
    Public Enum EnProposalResponse
        DilaporkankePPATKsebagaiSTR = 1
        DilaporkankePPATKsebagaiInformasi
        TidakDilaporkankePPATKsebagaiSTR
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Me.CaseIdHyperLink.Text = Me.PKCaseManagementID
            Me.CaseIdHyperLink.NavigateUrl = "CaseManagementViewDetail.aspx?PK_CaseManagementID=" & Me.PKCaseManagementID
            Using CaseManagementAdapter As New AMLDAL.CaseManagementTableAdapters.SelectCaseManagementTableAdapter
                Dim CaseManagementTable As New AMLDAL.CaseManagement.SelectCaseManagementDataTable
                CaseManagementTable = CaseManagementAdapter.GetVw_CaseManagementByPK(Me.PKCaseManagementID)
                If Not CaseManagementTable Is Nothing Then
                    If CaseManagementTable.Rows.Count > 0 Then
                        Dim CaseManagementTableRow As AMLDAL.CaseManagement.SelectCaseManagementRow
                        CaseManagementTableRow = CaseManagementTable.Rows(0)
                        If Not CaseManagementTableRow Is Nothing Then
                            If Not CaseManagementTableRow.IsCaseDescriptionNull Then
                                Me.CaseIdHyperLink.Text = Me.CaseIdHyperLink.Text & " - " & CaseManagementTableRow.CaseDescription
                            End If
                        End If
                    End If
                End If
            End Using
        End If
    End Sub
End Class

