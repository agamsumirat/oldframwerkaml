Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities

Partial Class CDDLNPAttachmentDownload
    Inherits System.Web.UI.Page
    Private ReadOnly Property getPK_CDDLNP_AttachmentID() As String
        Get
            Dim temp As String = Request.Params("PK_CDDLNP_AttachmentID")
            If temp.Trim.Length = 0 OrElse Not IsNumeric(temp) Then
                Throw New Exception("AttachmentID is invalid")
            Else
                Return temp
            End If
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Using objAttachment As CDDLNP_Attachment = DataRepository.CDDLNP_AttachmentProvider.GetByPK_CDDLNP_AttachmentID(Me.getPK_CDDLNP_AttachmentID)
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & objAttachment.FileName)
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Me.EnableViewState = False
            Response.ContentType = objAttachment.ContentType
            Response.BinaryWrite(objAttachment.FileSteam)
            Response.End()
        End Using
    End Sub
End Class
