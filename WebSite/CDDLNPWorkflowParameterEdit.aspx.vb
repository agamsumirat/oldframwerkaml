﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports System.Data

Partial Class CDDLNPWorkflowParameterEdit
    Inherits Parent

    Private ReadOnly Property GetPK_CDDLNP_WorkflowParameter_id() As Int32
        Get
            Return Request.QueryString("ID")
        End Get
    End Property

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageError.PreRender
        If cvalPageError.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                Using objCDDLNP_WorkflowParameter As CDDLNP_WorkflowParameter = DataRepository.CDDLNP_WorkflowParameterProvider.GetByPK_CDDLNP_WorkflowParameter_id(Me.GetPK_CDDLNP_WorkflowParameter_id)
                    If objCDDLNP_WorkflowParameter IsNot Nothing Then
                        txtGroupWorkflow.Text = objCDDLNP_WorkflowParameter.GroupWorkflow
                        lblLevel.Text = objCDDLNP_WorkflowParameter.Level

                        Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                            Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                            AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                        End Using

                    Else 'Handle jika ID tidak ditemukan
                        Me.Response.Redirect("CDDLNPWorkflowParameterView.aspx", False)
                    End If
                End Using
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim objApproval As New CDDLNP_WorkflowParameter_Approval
        Dim objApproval_Detail As New CDDLNP_WorkflowParameter_ApprovalDetail

        Try
            objTransManager.BeginTransaction()

            If txtGroupWorkflow.Text.Trim = String.Empty Then
                Throw New Exception("Group Workflow is required")
            End If

            If DataRepository.CDDLNP_WorkflowParameter_ApprovalDetailProvider.GetTotalItems(objTransManager, "PK_CDDLNP_WorkflowParameter_id=" & Me.GetPK_CDDLNP_WorkflowParameter_id, 0) > 0 Then
                Throw New Exception("CDD LNP Workflow Parameter for ID " & Me.GetPK_CDDLNP_WorkflowParameter_id & " is currently waiting for approval.")
            End If

            objApproval.RequestedBy = Sahassa.AML.Commonly.SessionPkUserId
            objApproval.RequestedDate = Date.Now
            objApproval.FK_MsMode_Id = 2
            DataRepository.CDDLNP_WorkflowParameter_ApprovalProvider.Save(objTransManager, objApproval)


            objApproval_Detail.FK_CDDLNP_WorkflowParameter_Approval_Id = objApproval.PK_CDDLNP_WorkflowParameter_Approval_Id
            objApproval_Detail.PK_CDDLNP_WorkflowParameter_id = Me.GetPK_CDDLNP_WorkflowParameter_id
            objApproval_Detail.Level = lblLevel.Text
            objApproval_Detail.GroupWorkflow = txtGroupWorkflow.Text.Trim
            DataRepository.CDDLNP_WorkflowParameter_ApprovalDetailProvider.Save(objTransManager, objApproval_Detail)


            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10221", False)
        Catch ex As Exception
            objTransManager.Rollback()
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
            If Not objApproval Is Nothing Then
                objApproval.Dispose()
                objApproval = Nothing
            End If
            If Not objApproval_Detail Is Nothing Then
                objApproval_Detail.Dispose()
                objApproval_Detail = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("CDDLNPWorkflowParameterView.aspx")
    End Sub
End Class