Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Imports System.IO
Imports System.Collections.Generic

Partial Class MappingReportBuilderWorkingUnitView
    Inherits Parent

    Public Property PK_MappingreportBuilderWorkingUnit_ID() As String
        Get
            Return CType(Session("MappingreportBuilderWorkingUnitView.PK_MappingreportBuilderWorkingUnit_ID"), String)
        End Get
        Set(ByVal value As String)
            Session("MappingreportBuilderWorkingUnitView.PK_MappingreportBuilderWorkingUnit_ID") = value
        End Set
    End Property

    Public ReadOnly Property ObjWorkingUnit() As TList(Of WorkingUnit)
        Get
            If Session("MappingReportBuilderWorkingUnitDelete.ObjWorkingUnitdata") Is Nothing Then
                Session("MappingReportBuilderWorkingUnitDelete.ObjWorkingUnitdata") = New TList(Of WorkingUnit)
            End If
            Return CType(Session("MappingReportBuilderWorkingUnitDelete.ObjWorkingUnitdata"), TList(Of WorkingUnit))
        End Get
    End Property

    Public Property SetnGetQueryName() As String
        Get
            If Not Session("MappingreportBuilderWorkingUnitView.SetnGetQueryName") Is Nothing Then
                Return CStr(Session("MappingreportBuilderWorkingUnitView.SetnGetQueryName"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingreportBuilderWorkingUnitView.SetnGetQueryName") = value
        End Set
    End Property



    Public Property SetnGetWorkingUnitName() As String
        Get
            If Not Session("MappingreportBuilderWorkingUnitView.SetnGetWorkingUnitName") Is Nothing Then
                Return CStr(Session("MappingreportBuilderWorkingUnitView.SetnGetWorkingUnitName"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingreportBuilderWorkingUnitView.SetnGetWorkingUnitName") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("MappingreportBuilderWorkingUnitView.Sort") Is Nothing, "QueryName  asc", Session("MappingreportBuilderWorkingUnitView.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("MappingreportBuilderWorkingUnitView.Sort") = Value
        End Set
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("MappingreportBuilderWorkingUnitView.RowTotal") Is Nothing, 0, Session("MappingreportBuilderWorkingUnitView.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MappingreportBuilderWorkingUnitView.RowTotal") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("MappingreportBuilderWorkingUnitView.CurrentPage") Is Nothing, 0, Session("MappingreportBuilderWorkingUnitView.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MappingreportBuilderWorkingUnitView.CurrentPage") = Value
        End Set
    End Property

    Function Getvw_MappingreportBuilderWorkingUnit(ByVal strWhereClause As String, ByVal strOrderBy As String, ByVal intStart As Integer, ByVal intPageLength As Integer, ByRef intCount As Integer) As VList(Of vw_MappingReportBuilderWorkingUnit)
        Dim Objvw_MappingreportBuilderWorkingUnit As VList(Of vw_MappingreportBuilderWorkingUnit) = Nothing
        Objvw_MappingreportBuilderWorkingUnit = DataRepository.vw_MappingreportBuilderWorkingUnitProvider.GetPaged(strWhereClause, strOrderBy, intStart, intPageLength, intCount)
        Return Objvw_MappingreportBuilderWorkingUnit
    End Function

    Public Property SetnGetBindTable() As VList(Of vw_MappingReportBuilderWorkingUnit)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            If SetnGetQueryName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "QueryName like '%" & SetnGetQueryName.Trim.Replace("'", "''") & "%'"
            End If
            If SetnGetWorkingUnitName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Name like '%" & SetnGetWorkingUnitName.Trim.Replace("'", "''") & "%'"
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            'If strAllWhereClause.Trim.Length > 0 Then
            '    strAllWhereClause += " and Fk_MsUserApproval_Id=" & Sahassa.AML.Commonly.SessionUserApprovalId & " and " & vw_MsHelp_ApprovalColumn.FK_MsUserID.ToString & " <> " & Sahassa.AML.Commonly.SessionPkUserId
            'Else
            '    strAllWhereClause += "  Fk_MsUserApproval_Id =" & Sahassa.AML.Commonly.SessionUserApprovalId & " and " & vw_MsHelp_ApprovalColumn.FK_MsUserID.ToString & " <> " & Sahassa.AML.Commonly.SessionPkUserId

            'End If
            Session("MappingreportBuilderWorkingUnitView.Table") = Getvw_MappingreportBuilderWorkingUnit(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)


            Return CType(Session("MappingreportBuilderWorkingUnitView.Table"), VList(Of vw_MappingReportBuilderWorkingUnit))


        End Get
        Set(ByVal value As VList(Of vw_MappingReportBuilderWorkingUnit))
            Session("MappingreportBuilderWorkingUnitView.Table") = value
        End Set
    End Property



    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        'Session("MappingreportBuilderWorkingUnitView.PK_MappingUserIdSTRPPATK_ApprovalDetail_ID") = Nothing
        Session("MappingreportBuilderWorkingUnitView.SetnGetUserName") = Nothing
        Session("MappingreportBuilderWorkingUnitView.PK_MappingreportBuilderWorkingUnit_ID") = Nothing
        Session("MappingReportBuilderWorkingUnitDelete.ObjWorkingUnitdata") = Nothing
        Session("MappingreportBuilderWorkingUnitView.SetnGetQueryName") = Nothing
        Session("MappingreportBuilderWorkingUnitView.SetnGetQueryName") = Nothing
        Session("MappingreportBuilderWorkingUnitView.SetnGetWorkingUnitName") = Nothing
        SetnGetCurrentPage = Nothing
        Me.SetnGetRowTotal = Nothing
        Me.SetnGetSort = Nothing
    End Sub


    Private Sub bindgrid()
        SettingControlSearching()
        Me.GridRBWU.DataSource = Me.SetnGetBindTable
        Me.GridRBWU.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridRBWU.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridRBWU.DataBind()

        'If SetnGetRowTotal > 0 Then
        '    LabelNoRecordFound.Visible = False
        'Else
        '    LabelNoRecordFound.Visible = True
        'End If
    End Sub

    Private Sub SettingPropertySearching()

        SetnGetQueryName = TxtQueryName.Text.Trim
        SetnGetWorkingUnitName = TxtWorkingunit.Text.Trim

    End Sub

    Private Sub SettingControlSearching()

        TxtQueryName.Text = SetnGetQueryName
        TxtWorkingunit.Text = SetnGetWorkingUnitName

    End Sub



    Private Sub SetInfoNavigate()

        Me.PageCurrentPage.Text = (Me.SetnGetCurrentPage + 1).ToString
        Me.PageTotalPages.Text = Me.GetPageTotal.ToString
        Me.PageTotalRows.Text = Me.SetnGetRowTotal.ToString
        Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
        Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Try
            Me.bindgrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub ImageButtonSearchCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearchCancel.Click
        Try

            SetnGetQueryName = Nothing
            SetnGetWorkingUnitName = Nothing                 
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.SetnGetCurrentPage = 0
            SettingPropertySearching()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

                ClearThisPageSessions()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                bindgrid()

            End If
            GridRBWU.PageSize = CInt(Sahassa.AML.Commonly.GetDisplayedTotalRow)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LinkButtonAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddNew.Click

        Try
            Response.Redirect("MappingreportBuilderWorkingUnitAdd.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub GridRBWU_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridRBWU.EditCommand
        Dim PK_MappingreportBuilderWorkingUnit_ID As Integer 'primary key
        'Dim StrUserID As String  'unik
        Try
            PK_MappingreportBuilderWorkingUnit_ID = CType(e.Item.Cells(1).Text, Integer)
            'StrUserID = e.Item.Cells(2).Text

            Response.Redirect("MappingreportBuilderWorkingUnitEdit.aspx?PK_MappingreportBuilderWorkingUnit_ID=" & PK_MappingreportBuilderWorkingUnit_ID, False)
            'Using ObjMsBranchBll As New SahassaBLL.MsHelpBLL
            '    If ObjMsBranchBll.IsNotExistInApproval(StrUserID) Then
            '        Response.Redirect("MsHelpEdit.aspx?PkMsHelpID=" & PkMsHelpID, False)
            '    End If
            'End Using

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridRBWU_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridRBWU.DeleteCommand
        Dim PK_MappingreportBuilderWorkingUnit_ID As Integer 'primary key
        'Dim StrUnikID As String  'unik
        Try
            PK_MappingreportBuilderWorkingUnit_ID = CType(e.Item.Cells(1).Text, Integer)
            'StrUnikID = e.Item.Cells(2).Text

            Response.Redirect("MappingreportBuilderWorkingUnitDelete.aspx?PK_MappingreportBuilderWorkingUnit_ID=" & PK_MappingreportBuilderWorkingUnit_ID, False)
            'Using ObjMsBranchBll As New SahassaBLL.MsHelpBLL
            '    If ObjMsBranchBll.IsNotExistInApproval(StrUnikID) Then
            '        Response.Redirect("MsHelpDelete.aspx?PkMsHelpID=" & PkMsHelpID, False)
            '    End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub



    Protected Sub GridRBWU_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridRBWU.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Session("MappingReportBuilderWorkingUnitDelete.ObjWorkingUnitdata") = Nothing

                Dim EditButton As LinkButton = CType(e.Item.Cells(4).FindControl("LnkEdit"), LinkButton)
                Dim DelButton As LinkButton = CType(e.Item.Cells(5).FindControl("LnkDelete"), LinkButton)

                Dim ObjGridReport As DataGrid = CType(e.Item.FindControl("GridView"), DataGrid)
                Dim PKAuditProjectID As Integer = CInt(e.Item.Cells(1).Text)
                Dim ObjPkAuditProjectID() As Object = {PKAuditProjectID}

                Dim iii As Integer = 0

                Dim ObjmappingreportBuilderWorkingUnitmap As TList(Of mappingreportBuilderWorkingUnitmap)
                ObjmappingreportBuilderWorkingUnitmap = DataRepository.mappingreportBuilderWorkingUnitmapProvider.GetPaged(mappingreportBuilderWorkingUnitmapColumn.FK_MappingReportBuilderWorkingUnit_ID.ToString & " = " & PKAuditProjectID, "", 0, Integer.MaxValue, 0)

                Do While iii < ObjmappingreportBuilderWorkingUnitmap.Count
                    Using ObjWorkingUnitlist As New WorkingUnit
                        Using objWorkingUnit As WorkingUnit = DataRepository.WorkingUnitProvider.GetByWorkingUnitID(CInt(ObjmappingreportBuilderWorkingUnitmap(iii).FK_WorkingUnit_ID))
                            ObjWorkingUnitlist.WorkingUnitName = objWorkingUnit.WorkingUnitName
                            ObjWorkingUnitlist.WorkingUnitID = objWorkingUnit.WorkingUnitID
                        End Using
                        ObjWorkingUnit.Add(ObjWorkingUnitlist)
                    End Using
                    iii = iii + 1
                Loop


                ObjGridReport.DataSource = ObjWorkingUnit
                'ObjGridReport.DataKeyField = mappingreportBuilderWorkingUnitmapColumn.PK_MappingReportBuilderWorkingUnit_Map_ID.ToString
                ObjGridReport.DataBind()

                If Not ObjmappingreportBuilderWorkingUnitmap Is Nothing Then
                    ObjmappingreportBuilderWorkingUnitmap.Dispose()
                    ObjmappingreportBuilderWorkingUnitmap = Nothing
                End If



                e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try            
            If IsNumeric(Me.TextGoToPage.Text) AndAlso (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    Throw New Exception(Sahassa.AML.CommonlyEnum.EXECPTION_PAGENUMBER_GREATER_TOTALPAGENUMBER)
                End If
            Else
                Throw New Exception(Sahassa.AML.CommonlyEnum.EXECPTION_PAGENUMBER_NUMERICPOSITIF)
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridRBWU_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridRBWU.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class



