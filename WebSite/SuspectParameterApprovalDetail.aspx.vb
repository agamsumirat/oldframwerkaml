Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class SuspectParameterApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param pk SuspectParameter ApprovalID 
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property SuspectParameterApprovalID() As Int64
        Get
            Return Me.Request.Params("ApprovalID")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param Parameters_PendingApprovalID
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParametersPendingApprovalID() As Int64
        Get
            Return Me.Request.Params("PendingApprovalID")
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserID() As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                Return AccessPending.SelectParameters_PendingApprovalUserID(Me.ParametersPendingApprovalID)
            End Using
        End Get
    End Property
#End Region

#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HideAll()
        Dim StrId As String = ""
        Select Case Me.ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.SuspectParameterAdd
                StrId = "UserAdd"
            Case Sahassa.AML.Commonly.TypeConfirm.SuspectParameterEdit
                StrId = "UserEdi"
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select
        For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
            ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID.Substring(0, 7) = StrId
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load LoadSuspectParameterAdd 
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadSuspectParameterAdd()
        Me.LabelTitle.Text = "Activity: Add Suspect Parameter"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.SuspectParameter_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetSuspectParameterApprovalData(Me.SuspectParameterApprovalID)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.SuspectParameter_ApprovalRow = ObjTable.Rows(0)

                    Me.LabelPercentageSuspectAdd.Text = rowData.SuspectPercentage
                    Me.LabelNameAdd.Text = rowData.SuspectNamePercentage
                    Me.LabelDOBAdd.Text = rowData.SuspectDOBPercentage
                    Me.LabelIDNoAdd.Text = rowData.SuspectIDNoPercentage
                    Me.LabelAddressAdd.Text = rowData.SuspectAddressPercentage

                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load SuspectParameter edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadSuspectParameterEdit()
        Me.LabelTitle.Text = "Activity: Edit Suspect Parameter"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.SuspectParameter_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetSuspectParameterApprovalData(Me.SuspectParameterApprovalID)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.SuspectParameter_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelPercentageSuspectNew.Text = rowData.SuspectPercentage
                    Me.LabelNameNew.Text = rowData.SuspectNamePercentage
                    Me.LabelDOBNew.Text = rowData.SuspectDOBPercentage
                    Me.LabelIDNoNew.Text = rowData.SuspectIDNoPercentage
                    Me.LabelAddressNew.Text = rowData.SuspectAddressPercentage

                    Me.LabelPercentageSuspectOld.Text = rowData.SuspectPercentage_Old
                    Me.LabelNameOld.Text = rowData.SuspectNamePercentage_Old
                    Me.LabelDOBOld.Text = rowData.SuspectDOBPercentage_Old
                    Me.LabelIDNoOld.Text = rowData.SuspectIDNoPercentage_Old
                    Me.LabelAddressOld.Text = rowData.SuspectAddressPercentage_Old

                End If
            End Using
        End Using
    End Sub
#End Region

#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' AcceptSuspectParameter add accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptSuspectParameterAdd()
        Try
            Using AccessSuspectParameter As New AMLDAL.AMLDataSetTableAdapters.SuspectParameterTableAdapter
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.SuspectParameter_ApprovalTableAdapter
                    Dim ObjTable As Data.DataTable = AccessPending.GetSuspectParameterApprovalData(Me.SuspectParameterApprovalID)
                    If ObjTable.Rows.Count > 0 Then
                        Dim rowData As AMLDAL.AMLDataSet.SuspectParameter_ApprovalRow = ObjTable.Rows(0)
                        'tambahkan item tersebut dalam tabel SuspectParameter
                        AccessSuspectParameter.Insert(rowData.SuspectPercentage, rowData.SuspectNamePercentage, rowData.SuspectDOBPercentage, rowData.SuspectIDNoPercentage, rowData.SuspectAddressPercentage, rowData.CreatedDate, rowData.LastUpdateDate)
                        If Not Me.InsertAuditTrail("Add", "Accepted") Then
                            Throw New Exception("Insert Audit Trail failed.")
                        Else
                            If Not Me.DeleteAllApproval() Then
                                Throw New Exception("Failed to delete suspect parameter approval.")
                            End If
                        End If
                    End If
                End Using
            End Using

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' SuspectParameter edit accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptSuspectParameterEdit()
        Try
            Using AccessSuspectParameter As New AMLDAL.AMLDataSetTableAdapters.SuspectParameterTableAdapter
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.SuspectParameter_ApprovalTableAdapter
                    Dim ObjTable As Data.DataTable = AccessPending.GetSuspectParameterApprovalData(Me.SuspectParameterApprovalID)
                    If ObjTable.Rows.Count > 0 Then
                        Dim rowData As AMLDAL.AMLDataSet.SuspectParameter_ApprovalRow = ObjTable.Rows(0)
                        'update item tersebut dalam tabel SuspectParameter
                        AccessSuspectParameter.UpdateSuspectParameter(rowData.SuspectPercentage, rowData.SuspectNamePercentage, rowData.SuspectDOBPercentage, rowData.SuspectIDNoPercentage, rowData.SuspectAddressPercentage, rowData.LastUpdateDate)
                        If Me.InsertAuditTrail("Edit", "Accepted") Then
                            If Not Me.DeleteAllApproval() Then
                                Throw New Exception("Failed to delete suspect parameter approval. ")
                            End If
                        Else
                            Throw New Exception("Insert Audit Trail failed.")
                        End If
                    End If
                End Using
            End Using

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

#End Region

#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject SuspectParameter add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectSuspectParameterAdd()
        Try
            If Me.InsertAuditTrail("Add", "Rejected") Then
                If Not Me.DeleteAllApproval() Then
                    Throw New Exception("Failed to delete suspect parameter approval. ")
                End If
            Else
                Throw New Exception("Insert Audit Trail failed.")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' SuspectParameter reject edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectSuspectParameterEdit()
        Try
            If Me.InsertAuditTrail("Edit", "Rejected") Then
                If Not Me.DeleteAllApproval() Then
                    Throw New Exception("Failed to delete suspect parameter approval. ")
                End If
            Else
                Throw New Exception("Insert Audit Trail failed.")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

#Region "Delete All Approval"
    ''' <summary>
    ''' Delete All Approval 
    ''' </summary>
    ''' <remarks></remarks>
    Private Function DeleteAllApproval() As Boolean
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            'Using TransScope As New Transactions.TransactionScope
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.SuspectParameter_ApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessPending, Data.IsolationLevel.ReadUncommitted)

                Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                    SetTransaction(AccessParametersApproval, oSQLTrans)

                    Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                        SetTransaction(AccessParametersPendingApproval, oSQLTrans)

                        Dim ObjTable As Data.DataTable = AccessPending.GetSuspectParameterApprovalData(Me.SuspectParameterApprovalID)
                        If ObjTable.Rows.Count > 0 Then
                            Dim rowData As AMLDAL.AMLDataSet.SuspectParameter_ApprovalRow = ObjTable.Rows(0)

                            'hapus item tsb dalam tabel Parameters_Approval
                            AccessParametersApproval.DeleteParametersApproval(Me.ParametersPendingApprovalID)

                            'hapus item tsb dalam tabel SuspectParameter_Approval
                            AccessPending.DeleteSuspectParameterApproval(rowData.ApprovalID)

                            'hapus item tsb dalam tabel Parameters_PendingApproval
                            AccessParametersPendingApproval.DeleteParametersPendingApproval(Me.ParametersPendingApprovalID)
                        End If
                    End Using
                End Using
            End Using
            oSQLTrans.Commit()
            Return True
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()

            Throw
            Return False
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Function
#End Region

#Region "Insert Audit Trail Suspect Parameter"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="mode"></param>
    ''' <param name="Action"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function InsertAuditTrail(ByVal mode As String, ByVal Action As String) As Boolean
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            'Using TransScope As New Transactions.TransactionScope
            Using AccessAuditTrailParameter As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAuditTrailParameter, Data.IsolationLevel.ReadUncommitted)

                Using dtAuditTrailParameter As AMLDAL.AMLDataSet.AuditTrailParameterDataTable = AccessAuditTrailParameter.GetData
                    
                    Dim RowAuditTrailParameter As AMLDAL.AMLDataSet.AuditTrailParameterRow = dtAuditTrailParameter.Rows(0)
                    If RowAuditTrailParameter.AlertOptions = 1 Then 'alert Overwrite
                        If Not Sahassa.AML.AuditTrailAlert.CheckOverwrite(7) Then
                            Throw New Exception("There is a problem in checking audittrail alert overwrite")
                        End If
                    ElseIf RowAuditTrailParameter.AlertOptions = 2 Then 'alert mail
                        If Not Sahassa.AML.AuditTrailAlert.CheckMailAlert(7) Then
                            Throw New Exception("There is a problem in checking audittrail alert by mail.")
                        End If
                    End If ' yg manual tidak perlu d cek krn ada audittrail maintenance
                End Using
                'End Using

                Using AccessSuspectParameterApproval As New AMLDAL.AMLDataSetTableAdapters.SuspectParameter_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessSuspectParameterApproval, oSQLTrans)

                    Using dtSuspectParameterApproval As AMLDAL.AMLDataSet.SuspectParameter_ApprovalDataTable = AccessSuspectParameterApproval.GetData
                        
                        Dim Row() As AMLDAL.AMLDataSet.SuspectParameter_ApprovalRow = dtSuspectParameterApproval.Select("ApprovalID=" & SuspectParameterApprovalID)
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)

                            If mode.ToLower = "add" Then
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectPercentage", mode, "", Row(0).SuspectPercentage.ToString, Action)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectNamePercentage", mode, "", Row(0).SuspectNamePercentage.ToString, Action)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectDOBPercentage", mode, "", Row(0).SuspectDOBPercentage.ToString, Action)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectIDNoPercentage", mode, "", Row(0).SuspectIDNoPercentage.ToString, Action)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectAddressPercentage", mode, "", Row(0).SuspectAddressPercentage.ToString, Action)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "CreatedDate", mode, Row(0).CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), Row(0).CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), Action)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "LastUpdateDate", mode, Row(0).LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), Row(0).LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), Action)
                            Else 'Edit
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectPercentage", mode, Row(0).SuspectPercentage_Old.ToString, Row(0).SuspectPercentage.ToString, Action)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectNamePercentage", mode, Row(0).SuspectNamePercentage_Old.ToString, Row(0).SuspectNamePercentage.ToString, Action)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectDOBPercentage", mode, Row(0).SuspectDOBPercentage_Old.ToString, Row(0).SuspectDOBPercentage.ToString, Action)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectIDNoPercentage", mode, Row(0).SuspectIDNoPercentage_Old.ToString, Row(0).SuspectIDNoPercentage.ToString, Action)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "SuspectAddressPercentage", mode, Row(0).SuspectAddressPercentage_Old.ToString, Row(0).SuspectAddressPercentage.ToString, Action)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "CreatedDate", mode, Row(0).CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), Row(0).CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), Action)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - SuspectParameter", "LastUpdateDate", mode, Row(0).LastUpdateDate_Old.ToString("dd-MMMM-yyyy HH:mm"), Row(0).LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), Action)
                            End If
                        End Using
                    End Using
                End Using
                oSQLTrans.Commit()
                Return True
            End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()

            Return False
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Function
#End Region
    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HideAll()

                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.SuspectParameterAdd
                        Me.LoadSuspectParameterAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.SuspectParameterEdit
                        Me.LoadSuspectParameterEdit()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '        Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Page.Validate()
            If Page.IsValid Then
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.SuspectParameterAdd
                        Me.AcceptSuspectParameterAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.SuspectParameterEdit
                        Me.AcceptSuspectParameterEdit()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionIntendedPage = "SuspectParameterManagementApproval.aspx"

                Me.Response.Redirect("SuspectParameterManagementApproval.aspx", False)
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.SuspectParameterAdd
                    Me.RejectSuspectParameterAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.SuspectParameterEdit
                    Me.RejectSuspectParameterEdit()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "SuspectParameterManagementApproval.aspx"

            Me.Response.Redirect("SuspectParameterManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "SuspectParameterManagementApproval.aspx"

        Me.Response.Redirect("SuspectParameterManagementApproval.aspx", False)
    End Sub
End Class