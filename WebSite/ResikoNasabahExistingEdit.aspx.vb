Option Explicit On
Option Strict On
Imports AMLBLL
Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports SahassaNettier.Services
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Sahassa.AML.Commonly

Partial Class ResikoNasabahExistingEdit
    Inherits Parent

    Public Property ObjResikoNasabahExisting() As TList(Of ResikoNasabahExisting)
        Get
            If Session("ResikoNasabahExistingEdit.ObjResikoNasabahExisting") Is Nothing Then
                Using objreturn As TList(Of ResikoNasabahExisting) = DataRepository.ResikoNasabahExistingProvider.GetPaged("", "", 0, Integer.MaxValue, 0)
                    Session("ResikoNasabahExistingEdit.ObjResikoNasabahExisting") = objreturn
                End Using
            End If
            Return CType(Session("ResikoNasabahExistingEdit.ObjResikoNasabahExisting"), TList(Of ResikoNasabahExisting))
        End Get
        Set(ByVal value As TList(Of ResikoNasabahExisting))
            Session("ResikoNasabahExistingEdit.ObjResikoNasabahExisting") = value
        End Set
    End Property

    Public Property PkResikoNasabahExistingEditId() As Integer
        Get
            If Session("ResikoNasabahExistingEdit.PkResikoNasabahExistingEditId") Is Nothing Then
                Session("ResikoNasabahExistingEdit.PkResikoNasabahExistingEditId") = 0
            End If
            Return CType(Session("ResikoNasabahExistingEdit.PkResikoNasabahExistingEditId"), Integer)
        End Get
        Set(ByVal value As Integer)
            Session("ResikoNasabahExistingEdit.PkResikoNasabahExistingEditId") = value
        End Set
    End Property

    Public Property ObjEditResikoNasabahExisting() As ResikoNasabahExisting
        Get
            Return CType(Session("ObjEditResikoNasabahExisting.ObjResikoNasabahExisting"), ResikoNasabahExisting)
        End Get
        Set(ByVal value As ResikoNasabahExisting)
            Session("ObjEditResikoNasabahExisting.ObjResikoNasabahExisting") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                'Add Event Calender
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                BindAmount()
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message

        End Try
    End Sub

    Sub ClearSession()
        Session("ResikoNasabahExistingEdit.ConfigApproval") = Nothing
        Session("ResikoNasabahExistingEdit.objResikoNasabahExisting") = Nothing
        Session("objeditResikoNasabahExisting.objResikoNasabahExisting") = Nothing
        Session("objappResikoNasabahExisting.objResikoNasabahExisting") = Nothing

        Session("ResikoNasabahExistingEdit.PkResikoNasabahExistingEditId") = Nothing
    End Sub

    Public Sub IsDataValid(ByVal StrStatistikTransaksi As String, ByVal StrIdentitas As String, ByVal StrTipologi As String, ByVal StrScoringAwal As String, ByVal StrRiskScoringAkhir As String)
        'Validasi Harus diisi
        If StrStatistikTransaksi = "" Then
            Throw New Exception("Statistik Transaksi harus diisi.")
        End If

        If StrIdentitas = "" Then
            Throw New Exception("Identitas harus diisi.")
        End If

        If StrTipologi = "" Then
            Throw New Exception("Tipologi harus diisi.")
        End If

        If StrScoringAwal = "" Then
            Throw New Exception("Scoring Awal harus diisi.")
        End If

        If StrRiskScoringAkhir = "" Then
            Throw New Exception("Risk Scoring Akhir harus diisi.")
        Else
            'risk scoring akhir harus H
            If StrRiskScoringAkhir <> "H" Then
                Throw New Exception("Risk Scoring Akhir harus bernilai H.")
            End If
        End If

        'Tidak boleh sama dengan kombinasi yang sudah ada
        For Each ObjResikoNasabahExistingItem As ResikoNasabahExisting In Me.ObjResikoNasabahExisting
            With ObjResikoNasabahExistingItem
                If Me.PkResikoNasabahExistingEditId <> .PK_ResikoNasabahExisting_ID Then
                    If StrStatistikTransaksi = .StatistikTransaksi Then
                        If StrIdentitas = .Identitas Then
                            If StrTipologi = .Tipologi Then
                                If StrScoringAwal = .ScoringAwal Then
                                    If StrRiskScoringAkhir = .RiskScoringAkhir Then
                                        Throw New Exception("Kombinasi sudah ada.")
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End With
        Next
    End Sub

    Private Sub SaveEdit()
        Try
            Using ObjResikoNasabahExistingBll As New ResikoNasabahExistingBLL
                If ObjResikoNasabahExistingBll.SaveDirectly(Me.ObjResikoNasabahExisting) Then
                    LblConfirmation.Text = "Success to Edit Resiko Nasabah Existing."
                    MtvResikoNasabahExisting.ActiveViewIndex = 1
                End If
            End Using
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub SaveToApproval()
        Try
            Using ObjResikoNasabahExistingBll As New ResikoNasabahExistingBLL
                If ObjResikoNasabahExistingBll.SaveToApproval(ObjResikoNasabahExisting) Then
                    LblConfirmation.Text = "Resiko Nasabah Existing parameter has been changed and it is currently waiting for approval."
                    MtvResikoNasabahExisting.ActiveViewIndex = 1
                End If
            End Using
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            'cek apakah sudah ada di pending approval
            Dim IntCount As Integer = 0
            DataRepository.ResikoNasabahExisting_ApprovalProvider.GetPaged("", "", 0, 1, IntCount)

            If IntCount = 0 Then
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    SaveEdit()
                Else
                    SaveToApproval()
                End If
            Else
                Throw New Exception("Cannot edit Resiko Nasabah Existing because it is currently waiting for approval.")
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("Default.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Try
            Response.Redirect("Default.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub ClearControl()
        Me.CboStatistikTransaksi.SelectedIndex = 0
        Me.CboIdentitas.SelectedIndex = 0
        Me.CboTipologi.SelectedIndex = 0
        Me.CboScoringAwal.SelectedIndex = 0

    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Try
            ClearControl()
            MtvResikoNasabahExisting.ActiveViewIndex = 0
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
    '    If CvalHandleErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    'Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalPageErr.PreRender
    '    If CvalPageErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If MtvResikoNasabahExisting.ActiveViewIndex = 0 Then
                ImgBtnSave.Visible = True
                ImgBackAdd.Visible = True
            Else
                ImgBtnSave.Visible = False
                ImgBackAdd.Visible = False
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Function BindAmount() As Boolean
        Me.GrdVwResikoNasabahExisting.DataSource = Me.ObjResikoNasabahExisting
        Me.GrdVwResikoNasabahExisting.DataBind()
    End Function

    Protected Sub GrdVwResikoNasabahExisting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GrdVwResikoNasabahExisting.RowDataBound
        Dim PKMsAction As String = Nothing
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim objData As ResikoNasabahExisting = CType(e.Row.DataItem, ResikoNasabahExisting)
                Dim lnkButtonRemove As LinkButton = CType(e.Row.FindControl("lnkButtonRemove"), LinkButton)
                lnkButtonRemove.CommandName = "Remove"
                lnkButtonRemove.CommandArgument = objData.PK_ResikoNasabahExisting_ID.ToString

                Dim LinkBtnEdit As LinkButton = CType(e.Row.FindControl("LinkBtnEdit"), LinkButton)
                LinkBtnEdit.CommandName = "EditData"
                LinkBtnEdit.CommandArgument = objData.PK_ResikoNasabahExisting_ID.ToString
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAdd.Click
        Try
            If ObjEditResikoNasabahExisting Is Nothing Then
                Me.IsDataValid(Me.CboStatistikTransaksi.SelectedValue.ToString, Me.CboIdentitas.SelectedValue.ToString, Me.CboTipologi.SelectedValue.ToString, Me.CboScoringAwal.SelectedValue.ToString, LblResikoAkhir.Text)

                Dim ObjResikoNasabahExistingItem As New ResikoNasabahExisting

                Dim generator As New Random
                Dim randomValue As Integer
                randomValue = generator.Next(1, Integer.MaxValue)
                While Me.ObjResikoNasabahExisting.FindAll(ResikoNasabahExistingColumn.PK_ResikoNasabahExisting_ID, randomValue).Count = 1
                    randomValue = generator.Next(1, Integer.MaxValue)
                End While

                With ObjResikoNasabahExistingItem
                    .PK_ResikoNasabahExisting_ID = randomValue

                    .StatistikTransaksi = Me.CboStatistikTransaksi.SelectedValue.ToString
                    .Identitas = Me.CboIdentitas.SelectedValue.ToString
                    .Tipologi = Me.CboTipologi.SelectedValue.ToString
                    .ScoringAwal = Me.CboScoringAwal.SelectedValue.ToString
                    .RiskScoringAkhir = LblResikoAkhir.Text
                End With

                ObjResikoNasabahExisting.Add(ObjResikoNasabahExistingItem)

                BindAmount()
                ClearControl()
            Else
                Me.IsDataValid(Me.CboStatistikTransaksi.SelectedValue.ToString, Me.CboIdentitas.SelectedValue.ToString, Me.CboTipologi.SelectedValue.ToString, Me.CboScoringAwal.SelectedValue.ToString, LblResikoAkhir.Text)

                Using ObjEditResikoNasabahExisting As ResikoNasabahExisting = Me.ObjResikoNasabahExisting.Find(SahassaNettier.Entities.ResikoNasabahExistingColumn.PK_ResikoNasabahExisting_ID, Me.PkResikoNasabahExistingEditId)
                    If Not ObjEditResikoNasabahExisting Is Nothing Then
                        With ObjEditResikoNasabahExisting
                            .StatistikTransaksi = Me.CboStatistikTransaksi.SelectedValue.ToString
                            .Identitas = Me.CboIdentitas.SelectedValue.ToString
                            .Tipologi = Me.CboTipologi.SelectedValue.ToString
                            .ScoringAwal = Me.CboScoringAwal.SelectedValue.ToString
                            .RiskScoringAkhir = LblResikoAkhir.Text
                        End With
                    End If
                End Using

                BindAmount()
                ClearControl()
                ObjEditResikoNasabahExisting = Nothing
                ImgBtnCancel.Visible = False
            End If

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GrdVwResikoNasabahExisting_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdVwResikoNasabahExisting.RowCommand
        If e.CommandName = "Remove" Then

            Dim idx As Integer = CInt(e.CommandArgument)
            Using ObjDeleted As ResikoNasabahExisting = ObjResikoNasabahExisting.Find(ResikoNasabahExistingColumn.PK_ResikoNasabahExisting_ID, idx)
                If Not ObjDeleted Is Nothing Then
                    If Not ObjEditResikoNasabahExisting Is Nothing Then
                        If ObjDeleted.PK_ResikoNasabahExisting_ID = Me.PkResikoNasabahExistingEditId Then
                            ImgBtnCancel_Click(ImgBtnCancel, Nothing)
                        End If
                    End If

                    ObjResikoNasabahExisting.Remove(ObjDeleted)
                End If
                BindAmount()
            End Using
        ElseIf e.CommandName = "EditData" Then
            Dim idx As Integer = CInt(e.CommandArgument)

            Using ObjEdit As ResikoNasabahExisting = ObjResikoNasabahExisting.Find(ResikoNasabahExistingColumn.PK_ResikoNasabahExisting_ID, idx)
                If Not ObjEdit Is Nothing Then
                    ObjEditResikoNasabahExisting = ObjEdit
                    Me.PkResikoNasabahExistingEditId = ObjEdit.PK_ResikoNasabahExisting_ID
                    LoadEditData()
                End If
            End Using
        End If
    End Sub

    Sub LoadEditData()
        If Not Me.ObjEditResikoNasabahExisting Is Nothing Then
            With Me.ObjEditResikoNasabahExisting
                Me.CboStatistikTransaksi.SelectedValue = .StatistikTransaksi
                Me.CboIdentitas.SelectedValue = .Identitas
                Me.CboTipologi.SelectedValue = .Tipologi
                Me.CboScoringAwal.SelectedValue = .ScoringAwal

            End With
        End If

        ImgBtnCancel.Visible = True
    End Sub

    Protected Sub ImgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnCancel.Click
        Try
            ClearControl()
            ObjEditResikoNasabahExisting = Nothing
            Me.PkResikoNasabahExistingEditId = Nothing
            ImgBtnCancel.Visible = False
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message

        End Try
    End Sub
End Class