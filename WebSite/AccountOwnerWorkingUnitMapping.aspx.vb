Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Public Class AccountOwnerWorkingUnitMapping
    Inherits Parent

    Private AccountOwnerName As String

#Region " Property "
    ''' <summary>
    ''' get focus id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.DropDownListWorkingUnit.ClientID
        End Get
    End Property

    Public ReadOnly Property GetAccountOwnerId() As String
        Get
            Return Me.Request.Params("AccountOwnerId")
        End Get
    End Property
#End Region

#Region " Update Account Owner Working Unit Mapping dan Audit trail"
    Private Sub UpdateAccountOwnerWorkingUnitMappingBySU()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Dim AccountOwnerID = ViewState("AccountOwnerID_Old")
            Dim WorkingUnitID As Int32 = Me.DropDownListWorkingUnit.SelectedValue

            Using AccessAccountOwner As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAccountOwner, Data.IsolationLevel.ReadUncommitted)
                Using AccessAccountOwnerWorkingUnitMapping As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMappingTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAccountOwnerWorkingUnitMapping, oSQLTrans)
                    Dim objAccountOwnerTable As Data.DataTable = AccessAccountOwner.GetDataByAccountOwnerId(Me.GetAccountOwnerId)

                    Dim objAccountOwnerRow As AMLDAL.AMLDataSet.AccountOwnerRow = objAccountOwnerTable.Rows(0)

                    'Berarti sudah pernah dimapped ke Working Unit sblmnya
                    If objAccountOwnerRow.AccountOwnerWorkingUnitMappingStatus Then
                        AccessAccountOwnerWorkingUnitMapping.UpdateAccountOwnerWorkingUnitMapping(objAccountOwnerRow.AccountOwnerId, Me.DropDownListWorkingUnit.SelectedValue, Now)
                    Else 'Berarti blm pernah dimapped ke Working Unit sblmnya
                        AccessAccountOwnerWorkingUnitMapping.Insert(objAccountOwnerRow.AccountOwnerId, Me.DropDownListWorkingUnit.SelectedValue, Now, Now)

                        AccessAccountOwner.UpdateAccountOwnerWorkingUnitMappingStatus(objAccountOwnerRow.AccountOwnerId, "True")
                    End If
                End Using
            End Using
            oSQLTrans.Commit()
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    Private Sub InsertAuditTrail()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using AccessAccountOwner As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAccountOwner, Data.IsolationLevel.ReadUncommitted)
                Using AccessAccountOwnerWorkingUnitMapping As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMappingTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAccountOwnerWorkingUnitMapping, oSQLTrans)
                    Dim objAccountOwnerTable As Data.DataTable = AccessAccountOwner.GetDataByAccountOwnerId(Me.GetAccountOwnerId)
                    Dim objAccountOwnerRow As AMLDAL.AMLDataSet.AccountOwnerRow = objAccountOwnerTable.Rows(0)

                    'Berarti sudah pernah dimapped ke Working Unit sblmnya
                    If objAccountOwnerRow.AccountOwnerWorkingUnitMappingStatus Then
                        Using dt As Data.DataTable = AccessAccountOwnerWorkingUnitMapping.GetDataByAccountOwnerId(Me.GetAccountOwnerId)
                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
                            Dim RowData() As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMappingRow = dt.Select("AccountOwnerId='" & CInt(Me.GetAccountOwnerId) & "'")
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "AccountOwnerID", "Edit", RowData(0).AccountOwnerID, RowData(0).AccountOwnerID, "Accepted")
                                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "WorkingUnitID", "Edit", RowData(0).WorkingUnitID, Me.DropDownListWorkingUnit.SelectedValue, "Accepted")
                                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "CreatedDate", "Edit", RowData(0).CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), DateTime.Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "LastUpdateDate", "Edit", RowData(0).LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), DateTime.Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                            End Using
                        End Using
                    Else 'Berarti blm pernah dimapped ke Working Unit sblmnya
                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)

                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "AccountOwnerID", "Add", "", objAccountOwnerRow.AccountOwnerId, "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "WorkingUnitID", "Add", "", Me.DropDownListWorkingUnit.SelectedValue, "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "CreatedDate", "Add", "", DateTime.Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "LastUpdateDate", "Add", "", DateTime.Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "AccountOwner", "AccountOwnerWorkingUnitMappingStatus", "Edit", "False", "True", "Accepted")
                        End Using
                    End If
                End Using
            End Using
            oSQLTrans.Commit()
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' fill group
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroupWorkingUnit()
        Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
            'Bind DropDownListCC webcontrol
            Me.DropDownListWorkingUnit.DataSource = AccessGroup.GetData
            Me.DropDownListWorkingUnit.DataTextField = "WorkingUnitName"
            Me.DropDownListWorkingUnit.DataValueField = "WorkingUnitID"
            Me.DropDownListWorkingUnit.DataBind()
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get data login parameter
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	03/07/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GetData()
        Try
            Me.LabelAccountOwnerName.Text = Me.GetAccountOwnerId

            Using AccessAccountOwner As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
                Dim objAccountOwnerTable As Data.DataTable = AccessAccountOwner.GetDataByAccountOwnerId(Me.GetAccountOwnerId)
                Dim objAccountOwnerRow As AMLDAL.AMLDataSet.AccountOwnerRow = objAccountOwnerTable.Rows(0)
                'Me.AccountOwnerName = objAccountOwnerRow.AccountOwnerName
                Me.LabelAccountOwnerName.Text &= " - " & objAccountOwnerRow.AccountOwnerName

                If objAccountOwnerRow.AccountOwnerWorkingUnitMappingStatus Then
                    Using AccessAccountOwnerWorkingUnitMapping As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMappingTableAdapter
                        Using objtable As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMappingDataTable = AccessAccountOwnerWorkingUnitMapping.GetDataByAccountOwnerId(Me.GetAccountOwnerId)
                            Dim objrow As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMappingRow = objtable.Rows(0)

                            Me.DropDownListWorkingUnit.SelectedValue = objrow.WorkingUnitID

                            'ViewState("AccountOwnerID_Old") = CType(objrow.AccountOwnerID, Integer)
                            ViewState("AccountOwnerID_Old") = CType(objrow.AccountOwnerID, String)
                            ViewState("WorkingUnitID_Old") = CType(objrow.WorkingUnitID, Integer)
                            ViewState("CreatedDate_Old") = CDate(objrow.CreatedDate)
                            ViewState("LastUpdateDate_Old") = CType(objrow.LastUpdateDate, Date)
                        End Using
                    End Using
                Else
                    'do nothing
                    'ViewState("WorkingUnitID_Old") = Me.DropDownListWorkingUnit.SelectedValue
                End If
            End Using
        Catch
            Throw
        End Try
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load page handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillGroupWorkingUnit()
                Me.GetData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cancel event handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "AccountOwnerWorkingUnitMappingView.aspx"

        Me.Response.Redirect("AccountOwnerWorkingUnitMappingView.aspx", False)
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' save handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>    
    ''' </remarks>
    ''' <history>
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Periksa apakah User melakukan perubahan Working Unit Mapping
            If Me.CekIsApproval(Me.GetAccountOwnerId) Then
                Throw New Exception("Cannot add Account Owner Working Unit Mapping for the following Account Owner : " & Me.AccountOwnerName & " because it is currently waiting for approval.")
            Else 'Jika tdk dlm status pending approval
                'Jika SuperUser yg melakukan penambahan/perubahan
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    Me.InsertAuditTrail()
                    Me.UpdateAccountOwnerWorkingUnitMappingBySU()
                    Me.LblSuccess.Text = "The Account Working Unit Mapping has been updated."
                    Me.LblSuccess.Visible = True
                Else 'Jika bukan SuperUser yg melakukan penambahan/perubahan
                    Dim MessagePendingID As Integer
                    Using AccessAccountOwner As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
                        oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAccountOwner, Data.IsolationLevel.ReadUncommitted)
                        Using AccountOwnerTable As AMLDAL.AMLDataSet.AccountOwnerDataTable = AccessAccountOwner.GetDataByAccountOwnerId(Me.GetAccountOwnerId)
                            Dim AccountOwnerRow As AMLDAL.AMLDataSet.AccountOwnerRow = AccountOwnerTable.Rows(0)
                            Me.AccountOwnerName = AccountOwnerRow.AccountOwnerName

                            Using AccessAccountOwnerWorkingUnitMapping As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMappingTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAccountOwnerWorkingUnitMapping, oSQLTrans)
                                Using AccessApproval As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMapping_ApprovalTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApproval, oSQLTrans)
                                    'Dim AccountOwnerWorkingUnitMappingRow As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMappingRow = AccessAccountOwnerWorkingUnitMapping.GetDataByAccountOwnerId(Me.GetAccountOwnerId).Rows(0)

                                    Dim AccountWorkingUnitMappingApprovalID As Int64
                                    Dim ModeID As Int16

                                    'Jika Status = Mapped berarti ModeId = 2(Edit)
                                    If AccountOwnerRow.AccountOwnerWorkingUnitMappingStatus Then
                                        'Buat variabel untuk menampung nilai-nilai lama
                                        Dim AccountOwnerID_Old As String = ViewState("AccountOwnerID_Old")
                                        Dim WorkingUnitID_Old As Int32 = ViewState("WorkingUnitID_Old")
                                        Dim CreatedDate_Old As DateTime = ViewState("CreatedDate_Old")
                                        Dim LastUpdate_Old As DateTime = ViewState("LastUpdateDate_Old")
                                        ModeID = 2
                                        MessagePendingID = 8872 'MessagePendingID 8872 = AccountOwnerWorkingUnitMapping Add 

                                        AccountWorkingUnitMappingApprovalID = AccessApproval.InsertAccountOwnerWorkingUnitMapping_Approval(Me.GetAccountOwnerId, Me.DropDownListWorkingUnit.SelectedValue, CreatedDate_Old, Now, AccountOwnerID_Old, WorkingUnitID_Old, CreatedDate_Old, LastUpdate_Old)
                                    Else 'Jika Status = Unmapped berarti ModeId = 1(Add)
                                        ModeID = 1
                                        MessagePendingID = 8871 'MessagePendingID 8871 = AccountOwnerWorkingUnitMapping Add 

                                        AccountWorkingUnitMappingApprovalID = AccessApproval.InsertAccountOwnerWorkingUnitMapping_Approval(Me.GetAccountOwnerId, Me.DropDownListWorkingUnit.SelectedValue, Now, Now, Nothing, Nothing, Nothing, Nothing)
                                    End If

                                    Using ParametersPending As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(ParametersPending, oSQLTrans)
                                        'ParametersPendingApprovalID adalah primary key dari tabel Parameters_PendingApproval yang sifatnya identity dan baru dibentuk setelah row baru dibuat 
                                        Dim ParametersPendingApprovalID As Int64 = ParametersPending.InsertParameters_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, ModeID)

                                        Using ParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                                            Sahassa.AML.TableAdapterHelper.SetTransaction(ParametersApproval, oSQLTrans)
                                            'ParameterItemID 9 = AccountOwnerWorkingUnitMapping
                                            ParametersApproval.Insert(ParametersPendingApprovalID, ModeID, 9, AccountWorkingUnitMappingApprovalID)
                                        End Using
                                    End Using
                                End Using
                            End Using
                        End Using
                    End Using

                    oSQLTrans.Commit()

                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & Me.AccountOwnerName
                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & Me.AccountOwnerName, False)
                    'End Using
                End If
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub


    ''' <summary>
    ''' cek is approval
    ''' </summary>
    ''' <param name="strUserId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CekIsApproval(ByVal AccountOwnerId As String) As Boolean
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMapping_ApprovalTableAdapter
            Dim count As Int32 = AccessPending.CountAccountOwnerWorkingUnitMappingApproval(AccountOwnerId)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function
End Class