<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DataUpdatingEmailTemplate.aspx.vb" Inherits="DataUpdatingEmailTemplate" title="Data Updating - Email Template" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
	    border="2">
        <tr>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none" colspan="6">
                <strong><span style="font-size: 13pt">Data Updating - Email Template</span><br />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="90%"></asp:Label></td>
        </tr>
	    <tr class="formText">
		    <td bgColor="#ffffff" height="24" style="width: 1%">
            </td>
		    <td bgColor="#ffffff" colspan="3" style="width: 1%">
                To </td>
            <td bgcolor="#ffffff" colspan="1" style="width: 35%">
                <asp:DropDownList ID="DropDownListTO" runat="server" Width="215px" CssClass="combobox">
            </asp:DropDownList></td>
            <td bgcolor="#ffffff" colspan="1" style="width: 1%">
            </td>
	    </tr>
	    <tr class="formText">
		    <td bgcolor="#ffffff">
                <br />
            </td>
		    <td bgcolor="#ffffff" colspan="3" style="height: 32px">
                CC </td>
            <td bgcolor="#ffffff" colspan="1" style="height: 32px">
                <asp:DropDownList ID="DropDownListCC" runat="server" Width="215px" CssClass="combobox">
                </asp:DropDownList></td>
            <td bgcolor="#ffffff" colspan="1" style="height: 32px">
            </td>
	    </tr>
	    <tr class="formText">
		    <td bgcolor="#ffffff">
                &nbsp;</td>
		    <td bgcolor="#ffffff" style="height: 32px" colspan="3">
                BCC </td>
            <td bgcolor="#ffffff" colspan="1" style="height: 32px">
                <asp:DropDownList ID="DropDownListBCC" runat="server" Width="215px" CssClass="combobox">
                </asp:DropDownList></td>
            <td bgcolor="#ffffff" colspan="1" style="height: 32px">
            </td>
	    </tr>
	    <tr class="formText">
		    <td bgcolor="#ffffff" height="24">
			    <asp:RequiredFieldValidator id="RequiredFieldValidatorSubject" runat="server" Display="Dynamic" ErrorMessage="Email Subject cannot be blank"
				    ControlToValidate="TextSubject">*</asp:RequiredFieldValidator></td>
		    <td bgcolor="#ffffff" colspan="3">
                Subject&nbsp;
            </td>
            <td bgcolor="#ffffff" colspan="1">
			    <asp:TextBox id="TextSubject" runat="server" Width="210px" CssClass="textBox" MaxLength="100"></asp:TextBox><strong><span
                    style="color: #ff0000">*</span></strong></td>
            <td bgcolor="#ffffff" colspan="1">
            </td>
	    </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24">
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorBody" runat="server" ControlToValidate="TextBody"
                    Display="Dynamic" ErrorMessage="Email Body cannot be blank">*</asp:RequiredFieldValidator><br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" colspan="3">
                Body <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" colspan="1">
                <asp:TextBox ID="TextBody" runat="server" CssClass="textBox" Height="106px" MaxLength="1000"
                    TextMode="MultiLine" Width="289px"></asp:TextBox><strong><span style="color: #ff0000">*</span></strong></td>
            <td bgcolor="#ffffff" colspan="1">
            </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24">
            </td>
            <td bgcolor="#ffffff" colspan="5">
                <asp:CheckBox ID="CheckIncludeAttachmentData" runat="server" Text="Include attachment data to be updated" />&nbsp;</td>
        </tr>
	    <tr class="formText" bgColor="#dddddd" height="30">
		    <td><IMG height="15" src="images/arrow.gif" width="15"></td>
		    <td colSpan="5">
			    <table cellSpacing="0" cellPadding="3" border="0">
				    <tr>
					    <td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="SaveButton"></asp:imagebutton></td>
					    <td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
				    </tr>
			    </table>
                <span style="color: #ff0000">* Required</span><br />
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator></td>
	    </tr>
    </table>
	<script language="javascript">
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>