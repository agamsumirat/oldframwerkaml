<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="VerificationListUploadPEP.aspx.vb" Inherits="VerificationListUploadPEP" %>
<asp:Content ID="Content1" ContentPlaceHolderID=cpContent Runat="Server">
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4">
                <strong><span style="font-size: 18px">Verification List - Upload PEP</span></strong>
                <hr />
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="95%"></asp:Label>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
        </tr>
      		<tr class="formText" style="height:24px">
			<td bgColor="#ffffff"  style="width: 22px">
                <asp:RegularExpressionValidator ID="RegExpressionXLS" runat="server" ErrorMessage="Choose an .xls file type for PEP list." ControlToValidate="FileUploadPEP" ValidationExpression='^[a-zA-Z]:\\([^/:*?"<>|\r\n]*).xls$' Display="Dynamic" >*</asp:RegularExpressionValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FileUploadPEP"
                    ErrorMessage="File Upload PEP is required." Display="Dynamic">*</asp:RequiredFieldValidator></td>
			<td width="20%" bgColor="#ffffff">
                Select File to Upload</td>
			<td width="5" bgColor="#ffffff">:</td>
			<td width="80%" bgColor="#ffffff">
                <asp:FileUpload ID="FileUploadPEP" runat="server" CssClass="textbox" Width="640px" />
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/FolderTemplate/PEPListTemplate.xls">Download Template</asp:HyperLink>
                </td>
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageUpload" runat="server" CausesValidation="True" SkinID="SaveButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageButtonCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>              
				
            </td>
		</tr>
	</table>
	<script>
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>
