<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SuspectParameterApprovalDetail.aspx.vb" Inherits="SuspectParameterApprovalDetail" title="Suspect Parameter Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <TABLE id="TableDetail" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="2" runat="server">
	    <TR class="formText" id="UserAdd1">
		    <TD bgColor="#ffffff" height="24" style="width: 22px">&nbsp;</TD>
		    <TD bgColor="#ffffff" colspan="1">
                </TD>
            <td bgcolor="#ffffff" colspan="3" style="width: 76px">
                Percentage Suspect &gt;=</td>
		    <TD width="80%" bgColor="#ffffff"><asp:label id="LabelPercentageSuspectAdd" runat="server"></asp:label></TD>
	    </TR>
        <tr class="formText" id="UserAdd2">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff" colspan="1">
                <strong>Field</strong></td>
            <td bgcolor="#ffffff" colspan="3" style="width: 76px">
            </td>
            <td bgcolor="#ffffff" width="80%">
                </td>
        </tr>
        <tr class="formText" id="UserAdd3">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff" colspan="1">
                </td>
            <td bgcolor="#ffffff" colspan="3" style="width: 76px">
                Name</td>
            <td bgcolor="#ffffff" width="80%">
                <asp:Label ID="LabelNameAdd" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserAdd4">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff" colspan="1">
                </td>
            <td bgcolor="#ffffff" colspan="3" style="width: 76px">
                Date of Birth</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelDOBAdd" runat="server"></asp:Label></td>
        </tr>
	    <tr class="formText" id="UserAdd5">
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff" colspan="1">
            </td>
            <td bgcolor="#ffffff" colspan="3" style="width: 76px">
                ID Number</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelIDNoAdd" runat="server"></asp:Label></td>
	    </tr>
        <tr class="formText" id="UserAdd6">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff" colspan="1">
            </td>
            <td bgcolor="#ffffff" colspan="3" style="width: 76px">
                Address</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelAddressAdd" runat="server"></asp:Label></td>
        </tr>
	    <tr class="formText" id="UserEdi1">
		    <td colspan="6" bgcolor="#ffffcc" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none; height: 28px;">&nbsp;Old Values</td>
		    <td colspan="5" bgcolor="#ffffcc" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none; height: 28px;">&nbsp;New Values</td>
	    </tr>
	    <tr class="formText" id="UserEdi2">
		    <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none; height: 43px;">&nbsp;</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none; height: 43px;">
            </td>
		    <td bgcolor="#ffffff" style="height: 43px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none; width: 76px;" colspan="3">
                Percentage Suspect &gt;=</td>
		    <td bgcolor="#ffffff" style="height: 43px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;"><asp:label id="LabelPercentageSuspectOld" runat="server"></asp:label></td>
            <td bgcolor="#ffffff" colspan="2" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 43px; border-bottom-style: none; width: 37px;">
                &nbsp;</td>
            <td bgcolor="#ffffff" colspan="2" style="border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none; height: 43px;">
                Percentage Suspect &gt;=</td>
		    <td bgcolor="#ffffff" style="height: 43px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;"><asp:label id="LabelPercentageSuspectNew" runat="server"></asp:label></td>
	    </tr>
        <tr class="formText" id="UserEdi3">
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td width="10%" bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <strong>Field</strong></td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none; width: 76px;" colspan="3">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" colspan="2" style="border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none; width: 37px;">
                <strong>Field</strong></td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none" colspan="2">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
        </tr>
	    <tr class="formText" id="UserEdi4">
		    <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 17px; border-bottom-style: none;">&nbsp;</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; height: 17px; border-bottom-style: none;">
            </td>
		    <td bgcolor="#ffffff" style="height: 17px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none; width: 76px;" colspan="3">
                Name</td>
		    <td bgcolor="#ffffff" style="height: 17px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;"><asp:label id="LabelNameOld" runat="server"></asp:label></td>
            <td bgcolor="#ffffff" colspan="2" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 17px; border-bottom-style: none; width: 37px;">
                &nbsp;</td>
		    <td bgcolor="#ffffff" style="height: 17px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" colspan="2">
                Name</td>
		    <td bgcolor="#ffffff" style="height: 17px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;"><asp:label id="LabelNameNew" runat="server"></asp:label></td>
	    </tr>
        <tr class="formText" id="UserEdi5">
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none; width: 76px;" colspan="3">
                Date of Birth</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <asp:Label ID="LabelDOBOld" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff" colspan="2" style="border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none; width: 37px;">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" colspan="2">
                Date of Birth</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <asp:Label ID="LabelDOBNew" runat="server"></asp:Label></td>
        </tr>
	    <tr class="formText" id="UserEdi6">
		    <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">&nbsp;</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
		    <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none; width: 76px;" colspan="3">
                ID Number</td>
		    <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <asp:Label ID="LabelIDNoOld" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff" colspan="2" style="border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none; width: 37px;">
                &nbsp;</td>
		    <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" colspan="2">
                ID Number</td>
		    <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <asp:Label ID="LabelIDNoNew" runat="server"></asp:Label></td>
	    </tr>
        <tr class="formText" id="UserEdi7">
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none; width: 76px;" colspan="3">
                Address</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <asp:Label ID="LabelAddressOld" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff" colspan="2" style="border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none; width: 37px;">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" colspan="2">
                Address</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <asp:Label ID="LabelAddressNew" runat="server"></asp:Label></td>
        </tr>
	    <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;"><IMG height="15" src="images/arrow.gif" width="15"></TD>
<%--            <td style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
--%>		    <TD colSpan="10" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD style="width: 35px"><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="True" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator></TD>
	    </TR>
    </TABLE>
</asp:Content>