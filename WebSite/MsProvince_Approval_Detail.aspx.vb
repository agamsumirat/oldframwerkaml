#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsProvince_APPROVAL_Detail
    Inherits Parent

#Region "Function"

    Sub HideControl()
        imgReject.Visible = False
        imgApprove.Visible = False
        ImageCancel.Visible = False
    End Sub

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsProvince_Approval_View.aspx")
    End Sub

    Protected Sub imgReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgReject.Click
        Try
            Rejected()
            ChangeMultiView(1)
            LblConfirmation.Text = "Data has been rejected"
            HideControl()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgApprove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgApprove.Click
        Try
            Using ObjMsProvince_Approval As MsProvince_Approval = DataRepository.MsProvince_ApprovalProvider.GetByPk_MsProvince_Approval_Id(parID)
                If ObjMsProvince_Approval.Fk_MsMode_Id = 1 Then
                    Inserdata(ObjMsProvince_Approval)
                ElseIf ObjMsProvince_Approval.Fk_MsMode_Id = 2 Then
                    UpdateData(ObjMsProvince_Approval)
                ElseIf ObjMsProvince_Approval.Fk_MsMode_Id = 3 Then
                    DeleteData(ObjMsProvince_Approval)
                End If
            End Using
            ChangeMultiView(1)
            LblConfirmation.Text = "Data approved successful"
            HideControl()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
                ListmapingNew = New TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail)
                ListmapingOld = New TList(Of MappingMsProvinceNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim PkObject As String
        'Load new data
        Using ObjMsProvince_ApprovalDetail As MsProvince_ApprovalDetail = DataRepository.MsProvince_ApprovalDetailProvider.GetPaged(MsProvince_ApprovalDetailColumn.Fk_MsProvince_Approval_Id.ToString & _
         "=" & _
         parID, "", 0, 1, Nothing)(0)
            Dim CekMode As MsProvince_Approval = DataRepository.MsProvince_ApprovalProvider.GetByPk_MsProvince_Approval_Id(ObjMsProvince_ApprovalDetail.Fk_MsProvince_Approval_Id)
            Dim nama As String = CekMode.Fk_MsMode_Id
            If nama = 1 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 2 Then
                baru.Visible = True
                lama.Visible = True
                baruNew.Visible = True
                lamaOld.Visible = True
            ElseIf nama = 3 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 4 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            End If
            With ObjMsProvince_ApprovalDetail
                SafeDefaultValue = "-"
                txtIdProvincenew.Text = Safe(.IdProvince)
                txtCodenew.Text = Safe(.Code)
                txtNamanew.Text = Safe(.Nama)
                txtDescriptionnew.Text = Safe(.Description)

                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByNew.Text = Omsuser(0).UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyNew.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
                lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
                txtActivationNew.Text = SafeActiveInactive(.Activation)
                Dim L_objMappingMsProvinceNCBSPPATK_Approval_Detail As TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail)
                L_objMappingMsProvinceNCBSPPATK_Approval_Detail = DataRepository.MappingMsProvinceNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsProvinceNCBSPPATK_Approval_DetailColumn.PK_MsProvince_Approval_Id.ToString & _
                 "=" & _
                 parID, "", 0, Integer.MaxValue, Nothing)
                'Add mapping
                ListmapingNew.AddRange(L_objMappingMsProvinceNCBSPPATK_Approval_Detail)

            End With
            PkObject = ObjMsProvince_ApprovalDetail.IdProvince
        End Using

        'Load Old Data
        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(PkObject)
            If ObjMsProvince Is Nothing Then Return
            With ObjMsProvince
                SafeDefaultValue = "-"
                txtIdProvinceOld.Text = Safe(.IdProvince)
                txtCodeOld.Text = Safe(.Code)
                txtNamaOld.Text = Safe(.Nama)
                txtDescriptionOld.Text = Safe(.Description)

                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByOld.Text = Omsuser(0).UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyOld.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
                lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
                Dim L_objMappingMsProvinceNCBSPPATK As TList(Of MappingMsProvinceNCBSPPATK)
                txtActivationOld.Text = SafeActiveInactive(.Activation)
                L_objMappingMsProvinceNCBSPPATK = DataRepository.MappingMsProvinceNCBSPPATKProvider.GetPaged(MappingMsProvinceNCBSPPATKColumn.Pk_MsProvince_Id.ToString & _
                 "=" & _
                 PkObject, "", 0, Integer.MaxValue, Nothing)
                ListmapingOld.AddRange(L_objMappingMsProvinceNCBSPPATK)
            End With
        End Using
    End Sub



    Protected Sub imgBtnConfirmation_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnConfirmation.Click
        Try
            Response.Redirect("MsProvince_approval_view.aspx")
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping New sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingNew() As TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail)
        Get
            Return Session("ListmapingNew.data")
        End Get
        Set(ByVal value As TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail))
            Session("ListmapingNew.data") = value
        End Set
    End Property

    ''' <summary>
    ''' Menyimpan Item untuk mapping Old sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingOld() As TList(Of MappingMsProvinceNCBSPPATK)
        Get
            Return Session("ListmapingOld.data")
        End Get
        Set(ByVal value As TList(Of MappingMsProvinceNCBSPPATK))
            Session("ListmapingOld.data") = value
        End Set
    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsProvinceNCBSPPATK_Approval_Detail In ListmapingNew.FindAllDistinct("IdProvinceNCBS")
                Temp.Add(i.IDProvinceNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsProvinceNCBSPPATK In ListmapingOld.FindAllDistinct("IdProvinceNCBS")
                Temp.Add(i.Pk_MsProvinceNCBS_Id.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

#Region "Action Approval..."

    Sub Rejected()
        Dim ObjMsProvince_Approval As MsProvince_Approval = DataRepository.MsProvince_ApprovalProvider.GetByPk_MsProvince_Approval_Id(parID)
        Dim ObjMsProvince_ApprovalDetail As MsProvince_ApprovalDetail = DataRepository.MsProvince_ApprovalDetailProvider.GetPaged(MsProvince_ApprovalDetailColumn.Fk_MsProvince_Approval_Id.ToString & "=" & ObjMsProvince_Approval.Pk_MsProvince_Approval_Id, "", 0, 1, Nothing)(0)
        Dim L_ObjMappingMsProvinceNCBSPPATK_Approval_Detail As TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail) = DataRepository.MappingMsProvinceNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsProvinceNCBSPPATK_Approval_DetailColumn.PK_MsProvince_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)
        DeleteApproval(ObjMsProvince_Approval, ObjMsProvince_ApprovalDetail, L_ObjMappingMsProvinceNCBSPPATK_Approval_Detail)
    End Sub

    Sub DeleteApproval(ByRef objMsProvince_Approval As MsProvince_Approval, ByRef ObjMsProvince_ApprovalDetail As MsProvince_ApprovalDetail, ByRef L_ObjMappingMsProvinceNCBSPPATK_Approval_Detail As TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail))
        DataRepository.MsProvince_ApprovalProvider.Delete(objMsProvince_Approval)
        DataRepository.MsProvince_ApprovalDetailProvider.Delete(ObjMsProvince_ApprovalDetail)
        DataRepository.MappingMsProvinceNCBSPPATK_Approval_DetailProvider.Delete(L_ObjMappingMsProvinceNCBSPPATK_Approval_Detail)
    End Sub

    ''' <summary>
    ''' Delete Data dari  asli dari approval
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DeleteData(ByVal objMsProvince_Approval As MsProvince_Approval)
        '   '================= Delete Header ===========================================================
        Dim LObjMsProvince_ApprovalDetail As TList(Of MsProvince_ApprovalDetail) = DataRepository.MsProvince_ApprovalDetailProvider.GetPaged(MsProvince_ApprovalDetailColumn.Fk_MsProvince_Approval_Id.ToString & "=" & objMsProvince_Approval.Pk_MsProvince_Approval_Id, "", 0, 1, Nothing)
        If LObjMsProvince_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        Dim ObjMsProvince_ApprovalDetail As MsProvince_ApprovalDetail = LObjMsProvince_ApprovalDetail(0)
        Using ObjNewMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(ObjMsProvince_ApprovalDetail.IdProvince)
            DataRepository.MsProvinceProvider.Delete(ObjNewMsProvince)
        End Using

        '======== Delete MAPPING =========================================
        Dim L_ObjMappingMsProvinceNCBSPPATK_Approval_Detail As TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail) = DataRepository.MappingMsProvinceNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsProvinceNCBSPPATK_Approval_DetailColumn.PK_MsProvince_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


        For Each c As MappingMsProvinceNCBSPPATK_Approval_Detail In L_ObjMappingMsProvinceNCBSPPATK_Approval_Detail
            Using ObjNewMappingMsProvinceNCBSPPATK As MappingMsProvinceNCBSPPATK = DataRepository.MappingMsProvinceNCBSPPATKProvider.GetByPK_MappingMsProvinceNCBSPPATK_Id(c.PK_MappingMsProvinceNCBSPPATK_Id)
                DataRepository.MappingMsProvinceNCBSPPATKProvider.Delete(ObjNewMappingMsProvinceNCBSPPATK)
            End Using
        Next

        '======== Delete Approval data ======================================
        DeleteApproval(objMsProvince_Approval, ObjMsProvince_ApprovalDetail, L_ObjMappingMsProvinceNCBSPPATK_Approval_Detail)
    End Sub

    ''' <summary>
    ''' Update Data dari approval ke table asli 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateData(ByVal objMsProvince_Approval As MsProvince_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
        '   '================= Update Header ===========================================================
        Dim LObjMsProvince_ApprovalDetail As TList(Of MsProvince_ApprovalDetail) = DataRepository.MsProvince_ApprovalDetailProvider.GetPaged(MsProvince_ApprovalDetailColumn.Fk_MsProvince_Approval_Id.ToString & "=" & objMsProvince_Approval.Pk_MsProvince_Approval_Id, "", 0, 1, Nothing)
        If LObjMsProvince_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        Dim ObjMsProvince_ApprovalDetail As MsProvince_ApprovalDetail = LObjMsProvince_ApprovalDetail(0)
        Dim ObjMsProvinceNew As MsProvince
        Using ObjMsProvinceOld As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(ObjMsProvince_ApprovalDetail.IdProvince)
            If ObjMsProvinceOld Is Nothing Then Throw New Exception("Data Not Found")
            ObjMsProvinceNew = ObjMsProvinceOld.Clone
            With ObjMsProvinceNew
                FillOrNothing(.Nama, ObjMsProvince_ApprovalDetail.Nama)
                FillOrNothing(.CreatedBy, ObjMsProvince_ApprovalDetail.CreatedBy)
                FillOrNothing(.CreatedDate, ObjMsProvince_ApprovalDetail.CreatedDate)
                FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
                FillOrNothing(.ApprovedDate, Date.Now)
                .Activation = ObjMsProvince_ApprovalDetail.Activation
                .Code = ObjMsProvince_ApprovalDetail.Code
                .Description = ObjMsProvince_ApprovalDetail.Description
            End With
            DataRepository.MsProvinceProvider.Save(ObjMsProvinceNew)
            'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.AML.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "MsProvince has Approved to Update data", ObjMsProvinceNew, ObjMsProvinceOld)
            '======== Update MAPPING =========================================
            'delete mapping item
            Using L_ObjMappingMsProvinceNCBSPPATK As TList(Of MappingMsProvinceNCBSPPATK) = DataRepository.MappingMsProvinceNCBSPPATKProvider.GetPaged(MappingMsProvinceNCBSPPATKColumn.Pk_MsProvince_Id.ToString & _
    "=" & _
    ObjMsProvinceNew.IdProvince, "", 0, Integer.MaxValue, Nothing)
                DataRepository.MappingMsProvinceNCBSPPATKProvider.Delete(L_ObjMappingMsProvinceNCBSPPATK)
            End Using
            'Insert mapping item
            Dim L_ObjMappingMsProvinceNCBSPPATK_Approval_Detail As TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail) = DataRepository.MappingMsProvinceNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsProvinceNCBSPPATK_Approval_DetailColumn.PK_MsProvince_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            Dim L_ObjNewMappingMsProvinceNCBSPPATK As New TList(Of MappingMsProvinceNCBSPPATK)
            For Each c As MappingMsProvinceNCBSPPATK_Approval_Detail In L_ObjMappingMsProvinceNCBSPPATK_Approval_Detail
                Dim ObjNewMappingMsProvinceNCBSPPATK As New MappingMsProvinceNCBSPPATK '= DataRepository.MappingMsProvinceNCBSPPATKProvider.GetByPK_MappingMsProvinceNCBSPPATK_Id(c.PK_MappingMsProvinceNCBSPPATK_Id.GetValueOrDefault)
                With ObjNewMappingMsProvinceNCBSPPATK
                    FillOrNothing(.Pk_MsProvince_Id, ObjMsProvinceNew.IdProvince)
                    FillOrNothing(.Nama, c.Nama)
                    FillOrNothing(.Pk_MsProvinceNCBS_Id, c.IDProvinceNCBS)
                    L_ObjNewMappingMsProvinceNCBSPPATK.Add(ObjNewMappingMsProvinceNCBSPPATK)
                End With
            Next
            DataRepository.MappingMsProvinceNCBSPPATKProvider.Save(L_ObjNewMappingMsProvinceNCBSPPATK)
            '======== Delete Approval data ======================================
            DeleteApproval(objMsProvince_Approval, ObjMsProvince_ApprovalDetail, L_ObjMappingMsProvinceNCBSPPATK_Approval_Detail)
        End Using
    End Sub
    ''' <summary>
    ''' Insert Data dari approval ke table asli 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Inserdata(ByVal objMsProvince_Approval As MsProvince_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
        '   '================= Save Header ===========================================================
        Dim LObjMsProvince_ApprovalDetail As TList(Of MsProvince_ApprovalDetail) = DataRepository.MsProvince_ApprovalDetailProvider.GetPaged(MsProvince_ApprovalDetailColumn.Fk_MsProvince_Approval_Id.ToString & "=" & objMsProvince_Approval.Pk_MsProvince_Approval_Id, "", 0, 1, Nothing)
        'jika data tidak ada di database
        If LObjMsProvince_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        'binding approval ke table asli
        Dim ObjMsProvince_ApprovalDetail As MsProvince_ApprovalDetail = LObjMsProvince_ApprovalDetail(0)
        Using ObjNewMsProvince As New MsProvince()
            With ObjNewMsProvince
                FillOrNothing(.IdProvince, ObjMsProvince_ApprovalDetail.IdProvince)
                FillOrNothing(.Nama, ObjMsProvince_ApprovalDetail.Nama)
                FillOrNothing(.CreatedBy, ObjMsProvince_ApprovalDetail.CreatedBy)
                FillOrNothing(.CreatedDate, ObjMsProvince_ApprovalDetail.CreatedDate)
                FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
                FillOrNothing(.ApprovedDate, Date.Now)
                .Activation = True
                .Code = ObjMsProvince_ApprovalDetail.Code
                .Description = ObjMsProvince_ApprovalDetail.Description
            End With
            DataRepository.MsProvinceProvider.Save(ObjNewMsProvince)
            'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "MsProvince has Approved to Insert data", ObjNewMsProvince, Nothing)
            '======== Insert MAPPING =========================================
            Dim L_ObjMappingMsProvinceNCBSPPATK_Approval_Detail As TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail) = DataRepository.MappingMsProvinceNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsProvinceNCBSPPATK_Approval_DetailColumn.PK_MsProvince_Approval_Id.ToString & _
             "=" & _
             parID, "", 0, Integer.MaxValue, Nothing)
            Dim L_ObjNewMappingMsProvinceNCBSPPATK As New TList(Of MappingMsProvinceNCBSPPATK)()
            For Each c As MappingMsProvinceNCBSPPATK_Approval_Detail In L_ObjMappingMsProvinceNCBSPPATK_Approval_Detail
                Dim ObjNewMappingMsProvinceNCBSPPATK As New MappingMsProvinceNCBSPPATK()
                With ObjNewMappingMsProvinceNCBSPPATK
                    FillOrNothing(.Pk_MsProvince_Id, ObjNewMsProvince.IdProvince)
                    FillOrNothing(.Nama, c.Nama)
                    FillOrNothing(.Pk_MsProvinceNCBS_Id, c.IDProvinceNCBS)
                    L_ObjNewMappingMsProvinceNCBSPPATK.Add(ObjNewMappingMsProvinceNCBSPPATK)
                End With
            Next
            DataRepository.MappingMsProvinceNCBSPPATKProvider.Save(L_ObjNewMappingMsProvinceNCBSPPATK)
            '======== Delete Approval data ======================================
            DeleteApproval(objMsProvince_Approval, ObjMsProvince_ApprovalDetail, L_ObjMappingMsProvinceNCBSPPATK_Approval_Detail)
        End Using
    End Sub

#End Region

End Class


