
Partial Class AccountCaseHistoryDetail
    Inherits Parent

    Private ReadOnly Property GetAccountNo() As Double
        Get
            Return Me.Page.Request.Params.Get("AccountNo")
        End Get
    End Property

#Region "Set Session"
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("AccountCaseHistoryPageSelected") Is Nothing, New ArrayList, Session("AccountCaseHistoryPageSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("AccountCaseHistoryPageSelected") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("AccountCaseHistoryPageSort") Is Nothing, "CaseManagement.PK_CaseManagementID asc", Session("AccountCaseHistoryPageSort"))
        End Get
        Set(ByVal Value As String)
            Session("AccountCaseHistoryPageSort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("AccountCaseHistoryCurrentPage") Is Nothing, 0, Session("AccountCaseHistoryCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("AccountCaseHistoryCurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("AccountCaseHistoryRowTotal") Is Nothing, 0, Session("AccountCaseHistoryRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("AccountCaseHistoryRowTotal") = Value
        End Set
    End Property
    Private Property SetAndGetSearchingCriteria() As String
        Get
            Return IIf(Session("AccountCaseHistorySearchCriteria") Is Nothing, "", Session("AccountCaseHistorySearchCriteria"))
        End Get
        Set(ByVal Value As String)
            Session("AccountCaseHistorySearchCriteria") = Value
        End Set
    End Property
    Private Function SetAllSearchingCriteria() As String
        Dim StrSearch As String = "abc.AccountNo = '" & Me.GetAccountNo & "'"
        Try
            StrSearch = "abc.AccountNo = '" & Me.GetAccountNo & "'"
            Return StrSearch
        Catch
            Throw
            Return ""
        End Try
    End Function
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("AccountCaseHistoryViewData") Is Nothing, Sahassa.AML.Commonly.GetDatasetPaging(Me.TablesRelated, Me.Pk, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.Fields, Me.SetAndGetSearchingCriteria, Me.GroupBy), Session("AccountCaseHistoryViewData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("AccountCaseHistoryViewData") = value
        End Set
    End Property
#End Region

    Private Sub ClearThisPageSessions()
        Session("AccountCaseHistoryPageSelected") = Nothing
        Session("AccountCaseHistoryPageSort") = Nothing
        Session("AccountCaseHistoryCurrentPage") = Nothing
        Session("AccountCaseHistoryRowTotal") = Nothing
        Session("AccountCaseHistorySearchCriteria") = Nothing
        Session("AccountCaseHistoryViewData") = Nothing
    End Sub

    Protected Function GenerateCaseNoLink(ByVal StrCaseNo As String) As String
        If Convert.IsDBNull(StrCaseNo) Then
            Return "Case Number is Not Valid"
        Else
            Return String.Format("<a href=""{0}"">{1}</a> ", ResolveUrl("~\CaseManagementViewDetail.aspx?PK_CaseManagementID=" & StrCaseNo), StrCaseNo)
        End If
    End Function

#Region "Set Property For Table Bind Grid"
    Private ReadOnly Property TablesRelated() As String
        Get
            Return "CaseManagement INNER JOIN (SELECT AccountNo,FK_CaseManagementID FROM MapCaseManagementTransaction UNION SELECT AccountNo,FK_CaseManagementID  FROM CaseManagementProfileKeuanganTransaction union SELECT AccountNo,FK_CaseManagementID  FROM CaseManagementProfileKeuanganTransactionFreqPerPeriod )ABC ON " _
                 & "CaseManagement.PK_CaseManagementID = abc.FK_CaseManagementID " _
                 & "INNER JOIN CaseStatus ON CaseManagement.FK_CaseStatusID = CaseStatus.CaseStatusId"
        End Get
    End Property
    Private ReadOnly Property Pk() As String
        Get
            Return "CaseManagement.PK_CaseManagementID"
        End Get
    End Property
    Private ReadOnly Property Fields() As String
        Get
            'Return "MapCaseManagementTransaction.Pk_MapCaseManagementTransactionId, " _
            '     & "MapCaseManagementTransaction.AccountNo, CaseManagement.CreatedDate, " _
            '     & "CaseManagement.PK_CaseManagementID AS CaseNumber, CaseStatus.CaseStatusDescription, " _
            '     & "CaseManagement.IsReportedtoRegulator AS ReportedToRegulator"
            Return "CaseManagement.PK_CaseManagementID, " _
                 & "CaseManagement.CreatedDate, " _
                 & "CaseStatus.CaseStatusDescription, " _
                 & "CaseManagement.IsReportedtoRegulator"
        End Get
    End Property

    Private ReadOnly Property GroupBy() As String
        Get
            Return "CaseManagement.PK_CaseManagementID, " _
                 & "CaseManagement.CreatedDate, " _
                 & "CaseStatus.CaseStatusDescription, " _
                 & "CaseManagement.IsReportedtoRegulator"
        End Get
    End Property

#End Region

#Region "Bind Grid View"
    Public Sub BindGrid()
        Me.SetnGetBindTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.TablesRelated, Me.Pk, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.Fields, Me.SetAndGetSearchingCriteria, Me.GroupBy)
        Me.GridCaseHist.DataSource = Me.SetnGetBindTable
        Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCount(Me.TablesRelated, Me.SetAndGetSearchingCriteria)
        Me.GridCaseHist.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridCaseHist.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridCaseHist.DataBind()
        If SetnGetRowTotal > 0 Then
            LabelNoRecordFound.Visible = False
        Else
            LabelNoRecordFound.Visible = True
        End If
    End Sub
#End Region

#Region "Paging Event"
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
#End Region
#Region "Sorting Event"

    Protected Sub GridCaseHist_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridCaseHist.SortCommand
        Dim GridCaseHistoryPage As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridCaseHistoryPage.Columns(Sahassa.AML.Commonly.IndexSort(GridCaseHist, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
#Region "Excel Export Event"
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridCaseHist.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim PkId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PkId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PkId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PkId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls
    Private Sub BindSelected()
        Dim Rows As New ArrayList
        Dim oTableCaseHistory As New Data.DataTable
        Dim oRowCaseHistory As Data.DataRow
        oTableCaseHistory.Columns.Add("PK_CaseManagementID", GetType(String))
        oTableCaseHistory.Columns("PK_CaseManagementID").Caption = "Case Number"
        oTableCaseHistory.Columns.Add("CreatedDate", GetType(String))
        oTableCaseHistory.Columns.Add("CaseStatusDescription", GetType(String))
        oTableCaseHistory.Columns.Add("IsReportedtoRegulator", GetType(String))
        oTableCaseHistory.Columns("IsReportedtoRegulator").Caption = "Reported to Regulator"
        For Each IdPk As String In Me.SetnGetSelectedItem
            Dim rowData() As Data.DataRow = Me.SetnGetBindTable.Select("PK_CaseManagementID = " & IdPk & "")
            If rowData.Length > 0 Then
                oRowCaseHistory = oTableCaseHistory.NewRow
                oRowCaseHistory("PK_CaseManagementID") = rowData(0)("PK_CaseManagementID")
                oRowCaseHistory("CreatedDate") = rowData(0)("CreatedDate")
                oRowCaseHistory("CaseStatusDescription") = rowData(0)("CaseStatusDescription")
                oRowCaseHistory("IsReportedtoRegulator") = rowData(0)("IsReportedtoRegulator")
                oTableCaseHistory.Rows.Add(oRowCaseHistory)
            End If
        Next
        Me.GridCaseHist.DataSource = oTableCaseHistory
        Me.GridCaseHist.AllowPaging = False
        Me.GridCaseHist.DataBind()

        'Sembunyikan kolom agar tidak ikut diekspor ke excel
        Me.GridCaseHist.Columns(0).Visible = False
        Me.GridCaseHist.Columns(1).Visible = False
    End Sub
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=AccountCaseHistoryDetail.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridCaseHist)
            GridCaseHist.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridCaseHist.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim CaseHistoryId As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(CaseHistoryId) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(CaseHistoryId)
        '            Else
        '                ArrTarget.Remove(CaseHistoryId)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(CaseHistoryId)
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next

        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridCaseHist.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub
#End Region

    Protected Sub GridCaseHist_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridCaseHist.ItemDataBound
        'bikin indexing
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                'e.Item.Cells(1).Text = e.Item.ItemIndex + 1
            End If
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    '===============================================================
    Private Sub LoadDetailGeneral()
        Try
            Using AccessAccountInformation As New AMLDAL.AccountInformationTableAdapters.SelectAllAccount_WebTempTableByAccountNoTableAdapter
                Using dtDetailGeneral As Data.DataTable = AccessAccountInformation.GetAllAccount_WebTempTableByAccountNo(Me.GetAccountNo)
                    If dtDetailGeneral.Rows.Count > 0 Then
                        Dim Rows As AMLDAL.AccountInformation.SelectAllAccount_WebTempTableByAccountNoRow = dtDetailGeneral.Rows(0)
                        Me.LblAccountNumber.Text = Rows.AccountNo
                        If Not Rows.IsAccountNameNull Then
                            Me.LblName.Text = Rows.AccountName
                        End If
                        If Not Rows.IsAccountOwnerNameNull Then
                            Me.LblBranch.Text = Rows.AccountOwnerName
                        End If
                    Else
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Me.LoadDetailGeneral()
                Me.GridCaseHist.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                Me.SetAndGetSearchingCriteria = Me.SetAllSearchingCriteria

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Me.Response.Redirect("AccountCaseHistory.aspx", False)
    End Sub
End Class
