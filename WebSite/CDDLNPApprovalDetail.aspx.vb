Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports AMLBLL.CDDLNPBLL
Imports System.Collections.Generic
Imports System.IO

Partial Class CDDLNPApprovalDetail
    Inherits Parent
    Private BindGridFromExcel As Boolean = False

#Region " Property "
    Private ReadOnly Property GetFK_CDDLNP_ID() As String
        Get
            Return IIf(Request.QueryString("ID") = String.Empty, 0, Request.QueryString("ID"))
        End Get
    End Property

    Private Property SetnGetFK_ParentRef_ID() As Int32
        Get
            Return IIf(Session("CDDLNPApprovalDetail.ParentID") Is Nothing, 0, Session("CDDLNPApprovalDetail.ParentID"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPApprovalDetail.ParentID") = Value
        End Set
    End Property

    Private Property SetnGetScreeningNasabahID() As Int32
        Get
            Return IIf(Session("CDDLNPApprovalDetail.ScreeningNasabahID") Is Nothing, 0, Session("CDDLNPApprovalDetail.ScreeningNasabahID"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPApprovalDetail.ScreeningNasabahID") = Value
        End Set
    End Property

    Private Property SetnGetScreeningBOID() As Int32
        Get
            Return IIf(Session("CDDLNPApprovalDetail.ScreeningBOID") Is Nothing, 0, Session("CDDLNPApprovalDetail.ScreeningBOID"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPApprovalDetail.ScreeningBOID") = Value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CDDLNPApprovalDetail.Sort") Is Nothing, "LastApprovalDate  asc", Session("CDDLNPApprovalDetail.Sort"))
        End Get
        Set(ByVal Value As String)
            Session("CDDLNPApprovalDetail.Sort") = Value
        End Set
    End Property

    Private Property SetnGetPEPNasabah() As Boolean
        Get
            Return IIf(Session("CDDLNPApprovalDetail.PEPNasabah") Is Nothing, False, Session("CDDLNPApprovalDetail.PEPNasabah"))
        End Get
        Set(ByVal Value As Boolean)
            Session("CDDLNPApprovalDetail.PEPNasabah") = Value
        End Set
    End Property

    Private Property SetnGetPEPBO() As Boolean
        Get
            Return IIf(Session("CDDLNPApprovalDetail.PEPBO") Is Nothing, False, Session("CDDLNPApprovalDetail.PEPBO"))
        End Get
        Set(ByVal Value As Boolean)
            Session("CDDLNPApprovalDetail.PEPBO") = Value
        End Set
    End Property

    Private WriteOnly Property ScreeningCustomerPK As Int64
        Set
            Session("ScreeningCustomerPK") = Value
        End Set
    End Property

    Private Function SetnGetBindTable() As TList(Of CDDLNP_Approval)
        Return DataRepository.CDDLNP_ApprovalProvider.GetPaged("FK_CDDLNP_ID = " & Me.GetFK_CDDLNP_ID, SetnGetSort, 0, Int32.MaxValue, 0)
    End Function

    Protected Function GenerateLink(ByVal PK_CDDLNP_AttachmentID As Integer, ByVal Filename As String) As String
        Return String.Format("<a href=""{0}"">{1}</a> ", ResolveUrl("~\CDDLNPAttachmentDownload.aspx?PK_CDDLNP_AttachmentID=" & PK_CDDLNP_AttachmentID), Filename)
    End Function
#End Region

    Private Sub ClearThisPageSessions()
        Me.SetnGetSort = Nothing
    End Sub

    Protected Sub GridViewCDDLNP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewCDDLNP.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))

                Select Case e.Item.Cells(3).Text
                    Case 1
                        e.Item.Cells(8).Text = e.Item.Cells(4).Text & " (" & e.Item.Cells(6).Text & ") has modified the CDD."

                    Case 2
                        If e.Item.Cells(4).Text = SetnGetBindTable(0).RequestedBy Then
                            e.Item.Cells(8).Text = e.Item.Cells(4).Text & " (" & e.Item.Cells(6).Text & ") has requested the CDD approval."
                        Else
                            e.Item.Cells(8).Text = e.Item.Cells(4).Text & " (" & e.Item.Cells(6).Text & ") has approved the CDD."
                        End If

                    Case 3
                        e.Item.Cells(8).Text = e.Item.Cells(4).Text & " (" & e.Item.Cells(6).Text & ") has decided to return the CDD to the requestor for corrections."

                    Case 4
                        e.Item.Cells(8).Text = e.Item.Cells(4).Text & " (" & e.Item.Cells(6).Text & ") has made FINAL decision that the CDD is DECLINED."

                    Case 5
                        e.Item.Cells(8).Text = e.Item.Cells(4).Text & " (" & e.Item.Cells(6).Text & ") has made FINAL decision that the CDD is APPROVED."

                End Select
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewCDDLNP_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridViewCDDLNP.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub BindGrid()
        Me.GridViewCDDLNP.DataSource = Me.SetnGetBindTable
        Me.GridViewCDDLNP.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Dim strCreatedBy As String = String.Empty
                Dim strStatement1 As String = "Saya menyatakan bahwa berdasarkan pengetahuan saya yang sebenar-benarnya, informasi yang terdapat pada lembar EDD ini adalah benar dan telah diverifikasi sesuai dengan ketentuan [EDD] PT Bank CIMB Niaga yang berlaku, dan bahwa ketentuan [EDD] PT Bank CIMB Niaga telah dipenuhi"
                Dim strStatement2 As String = "Saya menyatakan bahwa saya telah melakukan review terhadap EDD ini dan menyatakan setuju dengan hasil EDD yang dilakukan oleh RM, dan berdasarkan pengetahuan saya, ketentuan [EDD] PT Bank CIMB Niaga telah dipenuhi"
                Dim totalSuspect As Long

                ' Informasi Nasabah
                Using objCDDLNP As VList(Of vw_CDDLNP_View) = DataRepository.vw_CDDLNP_ViewProvider.GetPaged("PK_CDDLNP_ID = " & Me.GetFK_CDDLNP_ID, String.Empty, 0, Int32.MaxValue, 0)
                    If objCDDLNP.Count > 0 Then
                        lblCDDTypeNasabah.Text = objCDDLNP(0).CDD_Type
                        hfCDDTypeNasabah.Value = objCDDLNP(0).FK_CDD_Type_ID
                        lblCustomerTypeNasabah.Text = objCDDLNP(0).CDD_NasabahType_ID
                        hfCustomerTypeNasabah.Value = objCDDLNP(0).FK_CDD_NasabahType_ID
                        lblNameNasabah.Text = objCDDLNP(0).Nama
                        lblRatingPersonalNasabah.Text = objCDDLNP(0).AMLRiskRatingPersonal
                        lblRatingGabunganNasabah.Text = objCDDLNP(0).AMLRiskRatingGabungan
                        lblIsPEPNasabah.Text = GetYesNo(objCDDLNP(0).IsPEP)
                        SetnGetScreeningNasabahID = objCDDLNP(0).FK_AuditTrail_Potensial_Screening_Customer_ID.GetValueOrDefault(0)

                        totalSuspect = EKABLL.CDDBLL.GetTotalSuspectScreening(SetnGetScreeningNasabahID)

                        If totalSuspect < 0 Then
                            lnkSuspectNasabah.Visible = False
                            lblScreeningNasabah.Visible = True
                        Else
                            lnkSuspectNasabah.Text = totalSuspect.ToString() & " suspect(s)"
                            lnkSuspectNasabah.Visible = True
                            lblScreeningNasabah.Visible = False
                        End If

                        'Using objAudit As SahassaNettier.Entities.AuditTrail_Potensial_Screening_Customer = SahassaNettier.Data.DataRepository.AuditTrail_Potensial_Screening_CustomerProvider.GetByPK_AuditTrail_Potensial_Screening_Customer_ID(SetnGetScreeningNasabahID)
                        '    If objAudit Is Nothing Then
                        '        lnkSuspectNasabah.Visible = False
                        '        lblScreeningNasabah.Visible = True
                        '    Else
                        '        lnkSuspectNasabah.Text = objAudit.SuspectMatch.ToString & " suspect(s)"
                        '        lnkSuspectNasabah.Visible = True
                        '        lblScreeningNasabah.Visible = False
                        '    End If
                        'End Using

                        Select Case objCDDLNP(0).FK_CDD_Type_ID.GetValueOrDefault(0)
                            Case 1, 2
                                strStatement1 = strStatement1.Replace("[EDD]", "EDD Private Banking")
                                strStatement2 = strStatement2.Replace("[EDD]", "EDD Private Banking")

                            Case 3, 4
                                strStatement1 = strStatement1.Replace("[EDD]", "EDD Layanan Niaga Prima")
                                strStatement2 = strStatement2.Replace("[EDD]", "EDD Layanan Niaga Prima")

                            Case Else
                                strStatement1 = strStatement1.Replace("[EDD]", "EDD")
                                strStatement2 = strStatement2.Replace("[EDD]", "EDD")

                        End Select
                        chkPernyataanSubmit.Text = strStatement1
                        chkPernyataanReview.Text = strStatement2

                        For Each objAttachment As CDDLNP_Attachment In DataRepository.CDDLNP_AttachmentProvider.GetPaged("FK_CDDLNP_ID = " & Me.GetFK_CDDLNP_ID, String.Empty, 0, Int32.MaxValue, 0)
                            RowAttachmentNasabah.Controls.Add(New LiteralControl(Me.GenerateLink(objAttachment.PK_CDDLNP_AttachmentID, objAttachment.FileName) & "</BR>"))
                        Next

                        strCreatedBy = objCDDLNP(0).CreatedBy
                        SetnGetPEPNasabah = objCDDLNP(0).IsPEP
                        lnkCDDNasabah.Visible = True
                        lblCDDNasabah.Visible = False
                    Else
                        lnkCDDNasabah.Visible = False
                        lblCDDNasabah.Visible = True
                    End If
                End Using

                ' Informasi Beneficial Owner
                Using objCDDLNP As VList(Of vw_CDDLNP_View) = DataRepository.vw_CDDLNP_ViewProvider.GetPaged("FK_RefParent_ID = " & Me.GetFK_CDDLNP_ID, String.Empty, 0, Int32.MaxValue, 0)
                    If objCDDLNP.Count > 0 Then
                        lblCDDTypeBO.Text = objCDDLNP(0).CDD_Type
                        hfCDDTypeBO.Value = objCDDLNP(0).FK_CDD_Type_ID
                        lblCustomerTypeBO.Text = objCDDLNP(0).CDD_NasabahType_ID
                        hfCustomerTypeBO.Value = objCDDLNP(0).FK_CDD_NasabahType_ID
                        lblNameBO.Text = objCDDLNP(0).Nama
                        lblRatingPersonalBO.Text = objCDDLNP(0).AMLRiskRatingPersonal
                        lblRatingGabunganBO.Text = objCDDLNP(0).AMLRiskRatingGabungan
                        lblIsPEPBO.Text = GetYesNo(objCDDLNP(0).IsPEP)
                        SetnGetScreeningBOID = objCDDLNP(0).FK_AuditTrail_Potensial_Screening_Customer_ID.GetValueOrDefault(0)
                        SetnGetFK_ParentRef_ID = objCDDLNP(0).PK_CDDLNP_ID

                        totalSuspect = EKABLL.CDDBLL.GetTotalSuspectScreening(SetnGetScreeningNasabahID)

                        If totalSuspect < 0 Then
                            lnkSuspectBO.Visible = False
                            lblScreeningBO.Visible = True
                        Else
                            lnkSuspectBO.Text = totalSuspect.ToString() & " suspect(s)"
                            lnkSuspectBO.Visible = True
                            lblScreeningBO.Visible = False
                        End If

                        'Using objAudit As SahassaNettier.Entities.AuditTrail_Potensial_Screening_Customer = SahassaNettier.Data.DataRepository.AuditTrail_Potensial_Screening_CustomerProvider.GetByPK_AuditTrail_Potensial_Screening_Customer_ID(SetnGetScreeningBOID)
                        '    If objAudit Is Nothing Then
                        '        lnkSuspectBO.Visible = False
                        '        lblScreeningBO.Visible = True
                        '    Else
                        '        lnkSuspectBO.Text = objAudit.SuspectMatch.ToString & " suspect(s)"
                        '        lnkSuspectBO.Visible = True
                        '        lblScreeningBO.Visible = False
                        '    End If
                        'End Using

                        For Each objAttachment As CDDLNP_Attachment In DataRepository.CDDLNP_AttachmentProvider.GetPaged("FK_CDDLNP_ID = " & Me.SetnGetFK_ParentRef_ID, String.Empty, 0, Int32.MaxValue, 0)
                            RowAttachmentBO.Controls.Add(New LiteralControl(Me.GenerateLink(objAttachment.PK_CDDLNP_AttachmentID, objAttachment.FileName) & "</BR>"))
                        Next

                        strCreatedBy = objCDDLNP(0).CreatedBy
                        SetnGetPEPBO = objCDDLNP(0).IsPEP
                        lnkCDDBO.Visible = True
                        lblCDDBO.Visible = False
                    Else
                        lnkCDDBO.Visible = False
                        lblCDDBO.Visible = True
                    End If
                End Using



                'Jika page detail diakses oleh si pembuat form
                If strCreatedBy = Sahassa.AML.Commonly.SessionUserId Then

                    '(tidak dipakai terkait done & propose)
                    'Jika sedang dalam proses approval 
                    'If DataRepository.vw_CDDLNP_ApprovalProvider.GetTotalItems("FK_CDDLNP_ID = " & Me.GetFK_CDDLNP_ID & " AND LastApprovalStatus = 2", 0) > 0 Then
                    '    RowViewer.Visible = True
                    'Else 'Jika tidak dalam proses approval 
                    '    RowRequestor.Visible = True
                    '    RowComment.Visible = True
                    'End If
                    RowViewer.Visible = True

                Else 'Jika page detail diakses oleh bukan si pembuat form

                    'Jika user merupakan workflow form tersebut
                    Using objCDDLNPBLL As New CDDLNPBLL
                        If DataRepository.vw_CDDLNP_ApprovalProvider.GetTotalItems("FK_CDDLNP_ID = " & Me.GetFK_CDDLNP_ID & " AND LastApprovalBy IN (" & objCDDLNPBLL.GetChildUser & ")", 0) > 0 Then
                            RowApprover.Visible = True
                            RowComment.Visible = True
                        Else 'Jika user bukan merupakan workflow form tersebut
                            RowViewer.Visible = True
                        End If
                    End Using
                End If

                

                Me.ClearThisPageSessions()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                'Else
                '    Me.Response.Redirect("CDDLNPMessage.aspx?ID=404", False)
            End If


        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageDecline.Click, ImageApprove.Click, ImageRequestChange.Click
        Dim imageButton As ImageButton = CType(sender, ImageButton)
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim CurrentWorkflowLevel As Int32 = SetnGetBindTable(SetnGetBindTable.Count - 1).LastWorkflowLevel + 1

        Dim intApprovalStatus As Int32 = 2
        Dim intMsgID As Int32 = 404

        Try
            objTransManager.BeginTransaction()


            Select Case imageButton.ID
                Case "ImageApprove"
                    If Not chkPernyataanReview.Checked Then
                        Throw New Exception("Please check the statement to confirm your approval.")
                    End If

                    If CurrentWorkflowLevel = DataRepository.Provider.ExecuteScalar(objTransManager, Data.CommandType.Text, "usp_GetFinalApprovalLevel '" & lblRatingGabunganNasabah.Text & "', " & (SetnGetPEPNasabah Or SetnGetPEPBO)) Then
                        intApprovalStatus = 5 'Final decision "Approved"
                    Else
                        intApprovalStatus = 2 'Submit to next level
                    End If
                    intMsgID = 3442

                Case "ImageDecline"
                    If txtComments.Text.Trim = String.Empty Then
                        Throw New Exception("Please provide reason why this cdd is declined.")
                    End If
                    intApprovalStatus = 4 'Final decision "Declined"
                    intMsgID = 3443

                Case "ImageRequestChange"
                    If txtComments.Text.Trim = String.Empty Then
                        Throw New Exception("Please provide what kind of additional information needed for this CDD.")
                    End If
                    intApprovalStatus = 3 'Decision "Request changed"
                    intMsgID = 3444

            End Select

            Using objApproval As New CDDLNP_Approval
                objApproval.FK_CDDLNP_ID = GetFK_CDDLNP_ID
                objApproval.RequestedBy = SetnGetBindTable(SetnGetBindTable.Count - 1).RequestedBy
                objApproval.RequestedDate = SetnGetBindTable(SetnGetBindTable.Count - 1).RequestedDate
                objApproval.LastApprovalStatus = intApprovalStatus
                objApproval.LastApprovalBy = Sahassa.AML.Commonly.SessionUserId
                objApproval.LastApprovalDate = Date.Now
                Using objWorkflowGroup As CDDLNP_WorkflowParameter = DataRepository.CDDLNP_WorkflowParameterProvider.GetByPK_CDDLNP_WorkflowParameter_id(CurrentWorkflowLevel)
                    objApproval.LastWorkflowGroup = objWorkflowGroup.GroupWorkflow
                End Using
                objApproval.LastWorkflowLevel = CurrentWorkflowLevel
                objApproval.Comments = txtComments.Text

                DataRepository.CDDLNP_ApprovalProvider.Save(objTransManager, objApproval)
            End Using

            objTransManager.Commit()

            CDDLNPEmailSend.CDDLNPEmailSend(GetFK_CDDLNP_ID)

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=" & intMsgID)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageSubmit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSubmit.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim CurrentWorkflowLevel As Int32 = 1
        Dim intApprovalStatus As Int32 = 2

        Try
            objTransManager.BeginTransaction()

            If Not chkPernyataanSubmit.Checked Then
                Throw New Exception("Please check the statement to confirm your request.")
            End If

            Dim isExist As Boolean = False
            For Each objWorkflow As CDDLNP_WorkflowUpload In DataRepository.CDDLNP_WorkflowUploadProvider.GetPaged(objTransManager, "List_RM LIKE '%" & Sahassa.AML.Commonly.SessionUserId & "%'", String.Empty, 0, Int32.MaxValue, 0)
                For Each objUserId As String In objWorkflow.List_RM.Split(";")
                    If String.Compare(Sahassa.AML.Commonly.SessionUserId, objUserId, StringComparison.OrdinalIgnoreCase) = 0 Then
                        isExist = True
                    End If
                Next
            Next

            If Not isExist Then
                Throw New Exception("Your user ID has not been assigned for workflow approval. Please contact your administrator.")
            End If

            Using objApproval As New CDDLNP_Approval
                objApproval.FK_CDDLNP_ID = GetFK_CDDLNP_ID
                objApproval.RequestedBy = Sahassa.AML.Commonly.SessionUserId
                objApproval.RequestedDate = Date.Now
                objApproval.LastApprovalStatus = intApprovalStatus
                objApproval.LastApprovalBy = Sahassa.AML.Commonly.SessionUserId
                objApproval.LastApprovalDate = Date.Now
                Using objWorkflowGroup As CDDLNP_WorkflowParameter = DataRepository.CDDLNP_WorkflowParameterProvider.GetByPK_CDDLNP_WorkflowParameter_id(CurrentWorkflowLevel)
                    objApproval.LastWorkflowGroup = objWorkflowGroup.GroupWorkflow
                End Using
                objApproval.LastWorkflowLevel = CurrentWorkflowLevel
                objApproval.Comments = txtComments.Text

                DataRepository.CDDLNP_ApprovalProvider.Save(objTransManager, objApproval)
            End Using

            objTransManager.Commit()

            CDDLNPEmailSend.CDDLNPEmailSend(GetFK_CDDLNP_ID)

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=3441")
        Catch ex As Exception
            objTransManager.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageBackApproval_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBackApproval.Click, ImageBackRequestor.Click, ImageBackViewer.Click
        Response.Redirect(Session("CDDLNP.BackPage"))
    End Sub

    Private Function GetYesNo(ByVal value As Boolean) As String
        Select Case value
            Case True : Return "Yes"
            Case False : Return "No"
            Case Else : Return "No"
        End Select
    End Function

    Protected Sub lnkCDDNasabah_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCDDNasabah.Click
        Try
            Me.Controls.Add(New LiteralControl("<script>popupWindow = window.open('CDDLNPPrint.aspx?CID=" & Me.GetFK_CDDLNP_ID & "&TID=" & hfCDDTypeNasabah.Value & "&NTID=" & hfCustomerTypeNasabah.Value & "','mustunique','scrollbars=1,directories=0,height=600,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0');popupWindow.focus();</script> "))
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub lnkCDDBO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCDDBO.Click
        Try
            Me.Controls.Add(New LiteralControl("<script>popupWindow = window.open('CDDLNPPrint.aspx?CID=" & Me.SetnGetFK_ParentRef_ID & "&TID=" & hfCDDTypeBO.Value & "&NTID=" & hfCustomerTypeBO.Value & "','mustunique','scrollbars=1,directories=0,height=600,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0');popupWindow.focus();</script> "))
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub lnkSuspectNasabah_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSuspectNasabah.Click
        Try
            'SessionCDDLNPAuditTrailId = SetnGetScreeningNasabahID
            ScreeningCustomerPK = SetnGetScreeningNasabahID
            Me.Controls.Add(New LiteralControl("<script>popupWindow = window.open('PopUpPotentialCustomerVerificationResult.aspx','mustunique','scrollbars=1,directories=0,height=600,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0');popupWindow.focus();</script> "))

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub lnkSuspectBO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSuspectBO.Click
        Try
            SessionCDDLNPAuditTrailId = SetnGetScreeningBOID
            Me.Controls.Add(New LiteralControl("<script>popupWindow = window.open('PopUpPotentialCustomerVerificationResult.aspx','mustunique','scrollbars=1,directories=0,height=600,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0');popupWindow.focus();</script> "))

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageErr.PreRender
        If cvalPageErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub
End Class