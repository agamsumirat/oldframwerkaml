﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL

Partial Class CDDLNPHistory
    Inherits Parent

    Private ReadOnly Property GetCIF() As String
        Get
            Return Request.QueryString("ID")
        End Get
    End Property

    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CDDLNPHistory.Sort") Is Nothing, "PK_CDDLNP_ID  desc", Session("CDDLNPHistory.Sort"))
        End Get
        Set(ByVal Value As String)
            Session("CDDLNPHistory.Sort") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.GridViewCDDLNP.DataSource = DataRepository.vw_CDDLNP_ViewProvider.GetPaged("CIF = '" & GetCIF & "' AND CreatedBy = '" & Sahassa.AML.Commonly.SessionUserId & "'", "CreatedDate", 0, Int32.MaxValue, 0)
            Me.GridViewCDDLNP.DataBind()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewCDDLNP_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridViewCDDLNP.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewCDDLNP_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewCDDLNP.ItemCommand
        Select Case e.CommandName.ToLower
            Case "detail"
                Response.Redirect("CDDLNPPrint.aspx?CID=" & e.Item.Cells(1).Text & "&TID=" & e.Item.Cells(3).Text & "&NTID=" & e.Item.Cells(5).Text)

        End Select
    End Sub
End Class