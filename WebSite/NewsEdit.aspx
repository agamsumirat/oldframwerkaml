<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="NewsEdit.aspx.vb" Inherits="NewsEdit" title="News Edit" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>News - Edit&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000">* Required</span></td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                News ID</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:Label ID="LabelNewsID" runat="server"></asp:Label></td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px">
                &nbsp;</td>
			<td width="20%" bgColor="#ffffff" style="height: 24px">
                Title</td>
			<td width="5" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px">
            &nbsp;<CKEditor:CKEditorControl ID="TextNewsTitleCK" runat="server" MaxLength="255" Toolbar="Basic"></CKEditor:CKEditorControl>
                <asp:TextBox ID="TextNewsTitle" runat="server" CssClass="textBox" 
                    MaxLength="100" Width="500px" Visible="False"></asp:TextBox>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <asp:RequiredFieldValidator ID="RequiredfieldStartDate" runat="server" ControlToValidate="TextStartDate"
                    Display="Dynamic" ErrorMessage="Start Date cannot be blank">*</asp:RequiredFieldValidator><br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorStartDate" runat="server" ControlToValidate="TextStartDate"
                    ErrorMessage="Start Date format must be 'dd-MMM-YYYY'" ValidationExpression="(\d{2}-[a-zA-Z]{3}-\d{4})">*</asp:RegularExpressionValidator></td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                Start Date</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                :</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                <asp:TextBox ID="TextStartDate" runat="server" CssClass="textBox" MaxLength="20"></asp:TextBox>
                <strong><span style="color: #ff0000">*
                    <input id="cmdStartDatePicker" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                        border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                        border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif); width: 16px;" title="Click to show calendar"
                        type="button" />
                    &nbsp;</span></strong></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <asp:RequiredFieldValidator ID="RequiredfieldEndDate" runat="server" ControlToValidate="TextEndDate"
                    Display="Dynamic" ErrorMessage="End Date cannot be blank">*</asp:RequiredFieldValidator><br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorEndDate" runat="server" ControlToValidate="TextEndDate"
                    ErrorMessage="End Date format must be 'dd-MMM-YYYY'" ValidationExpression="(\d{2}-[a-zA-Z]{3}-\d{4})">*</asp:RegularExpressionValidator></td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                End Date</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                :</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                <asp:TextBox ID="TextEndDate" runat="server" CssClass="textBox" MaxLength="20"></asp:TextBox>
                <strong><span style="color: #ff0000">*
                    <input id="cmdEndDatePicker" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                        border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                        border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif); width: 16px;" title="Click to show calendar"
                        type="button" />&nbsp;</span></strong></td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px;" rowspan="6">
                <asp:RequiredFieldValidator ID="RequiredfieldNewsSummary" runat="server" ControlToValidate="TextNewsSummary"
                    ErrorMessage="News Summary is required">*</asp:RequiredFieldValidator><br />
                <br />
                <br />
                <br />
                <br />
            </td>
			<td bgColor="#ffffff" rowspan="6" style="height: 24px">
                News Summary<br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px">
                :<br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px">
                &nbsp;<ckeditor:ckeditorcontrol
                    id="TextNewsSummary" runat="server" maxlength="255" toolbar="Basic"></ckeditor:ckeditorcontrol><strong><span
                    style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px">
                <asp:RequiredFieldValidator ID="RequiredfieldNewsContent" runat="server" ControlToValidate="TextNewsContent"
                    ErrorMessage="News Content is required">*</asp:RequiredFieldValidator><br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                News Content<br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                &nbsp;<ckeditor:ckeditorcontrol id="TextNewsContent" runat="server" maxlength="8000" toolbar="Basic"></ckeditor:ckeditorcontrol>
                <strong><span style="color: #ff0000">*</span></strong></td>
        </tr> 
        <tr>
         <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td nowrap="noWrap" width="1%" bgcolor="#ffffff">
                Attachment
            </td>
            <td width="1"bgcolor="#ffffff">
                :
            </td>
            <td width="99%" bgcolor="#ffffff">
                <asp:FileUpload ID="FileUpload1" runat="server" />
                <asp:Button ID="ButtonAdd" runat="server" Text="Add" ValidationGroup="FillInvestigation" />
                <br />
                <br />
                <asp:ListBox ID="ListAttachment" runat="server" Width="400px"></asp:ListBox>
                <asp:Button ID="ButtonDelete" runat="server" Text="Delete" />
            </td>
        </tr>        
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageUpdate" runat="server" CausesValidation="True" SkinID="UpdateButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>              
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	
</asp:Content>

