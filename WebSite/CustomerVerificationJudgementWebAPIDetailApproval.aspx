<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CustomerVerificationJudgementWebAPIDetailApproval.aspx.vb" Inherits="CustomerVerificationJudgementWebAPIDetailApproval" ValidateRequest="false" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>

<asp:Content ID="a" ContentPlaceHolderID="cpContent" runat="server">
    <ajax:AjaxPanel ID="panel" runat="server" Width="100%">
    <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; border-bottom-style: none">
                <strong>Customer Verification Judgement - Approval Detail &nbsp;
                </strong>
                </td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="99%" bgColor="#dddddd"
		border="2" height="72">
       <%-- <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Level Type - Add New<br />
                    <hr />
                </strong>
            </td>
        </tr>--%>
        <%--<tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000">* Required</span></td>
        </tr>--%>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px; width: 1%;">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px" colspan="3">
                <strong>
                Match with Following</strong>:</td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px; width: 25px;">
            </td>
            <td bgcolor="#ffffff" colspan="3" rowspan="1" style="height: 6px">
            <div style="overflow:auto;height:100px">
                <table id="tableGrid" width="80%">
                    <tr>
                        <td>
                            <ajax:AjaxPanel ID="grid" runat="server">
                      <asp:datagrid id="GridMSUserView" runat="server" AutoGenerateColumns="False"
						Font-Size="XX-Small" CellPadding="4"
						AllowPaging="True" width="100%" AllowSorting="True" ForeColor="Black" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" GridLines="Vertical"> 
        <FooterStyle BackColor="#CCCC99"></FooterStyle>
        <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
        <ItemStyle BackColor="#F7F7DE"></ItemStyle>
        <HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B"></HeaderStyle>
						<Columns>

                            <asp:BoundColumn DataField="PK_Aml_Customer_Judgement_ID" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="VerificationListId" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="DisplayName" HeaderText="Name" SortExpression="Name desc" ></asp:BoundColumn>
                            <asp:BoundColumn DataField="DateOfBirth" HeaderText="Date Of Birth" DataFormatString="{0:dd-MMM-yyyy}" SortExpression="DateOfBirth desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="BirthPlace" HeaderText="Birth Place" 
                                SortExpression="BirthPlace  desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ListTypeName" HeaderText="List Type" SortExpression="ListTypeName desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CategoryName" HeaderText="List Category" SortExpression="CategoryName desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PercentMatch" HeaderText="Percent Match (%)" SortExpression="PercentMatch desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PercentConfidential" HeaderText="Percent Confidential (%)" SortExpression="PercentConfidential desc">
                            </asp:BoundColumn>

                             <asp:BoundColumn DataField="JudgementResult" HeaderText="JudgementResult" SortExpression="JudgementResult desc">
                            </asp:BoundColumn>
                            
                            <asp:BoundColumn DataField="VerificationListId" Visible="False"></asp:BoundColumn>
                           
						</Columns>
						<PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages"></PagerStyle>
                          <AlternatingItemStyle BackColor="White" />
					</asp:datagrid></ajax:AjaxPanel>&nbsp;
					
				
                        </td>
                    </tr>
                </table>
            </div>
                </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 25px; height: 24px">
            </td>
            <td bgcolor="#ffffff" colspan="3" rowspan="1" style="height: 6px">
                <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" >
                    <tr class="formText">
                        <td bgcolor="ButtonFace" colspan="3">
                            <strong>
                Data</strong></td>                        
                        <td bgcolor="ButtonFace" colspan="3">
                            <strong>
                Suspect Match With</strong></td>
                    </tr>
                    <tr class="formText">
                        <td bgcolor="#ffffff" colspan="3" style="height: 6px; width: 55%;" valign="top">
                        <div id="1">
                            <table id="data" width="100%" borderColor="#ffffff" cellSpacing="1" cellPadding="2"  bgColor="#dddddd" border="2">		
                                <tr class="formText">
                                    <td bgcolor="#ffffff" style="width: 6%; height: 10px">
                                        CIF No</td>
                                    <td bgcolor="#ffffff" style="width: 1%;">
                                        :</td>
                                    <td bgcolor="#ffffff" style="width: 18%;">
                                        <asp:Label ID="LblCIFNo" runat="server"></asp:Label></td>
                                </tr>
                                <tr class="formText">
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        Name</td>
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        :</td>
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        <asp:Label ID="LblName" runat="server"></asp:Label></td>
                                </tr>
                                <tr class="formText">
                                    <td bgcolor="#ffffff" style="height: 19px">Related Customer Type</td>
                                    <td bgcolor="#ffffff" style="height: 19px">:</td>
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        <asp:Label ID="LblRelatedCustomerType" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="formText">
                                    <td bgcolor="#ffffff" style="height: 10px">
                                        Branch</td>
                                    <td bgcolor="#ffffff">
                                        :</td>
                                    <td bgcolor="#ffffff">
                                        <asp:Label ID="LblBranch" runat="server"></asp:Label></td>
                                </tr>
                                <tr class="formText">
                                    <td bgcolor="#ffffff" style="height: 10px">
                                        Opening Date</td>
                                    <td bgcolor="#ffffff">
                                        :</td>
                                    <td bgcolor="#ffffff">
                                        <asp:Label ID="LblOpeningDate" runat="server"></asp:Label></td>
                                </tr>
                                <tr class="formText">
                                    <td bgcolor="#ffffff" style="height: 10px">
                                        Date Of Birth</td>
                                    <td bgcolor="#ffffff">
                                        :</td>
                                    <td bgcolor="#ffffff">
                                        <asp:Label ID="LblDateOfBirth" runat="server"></asp:Label></td>
                                </tr>
                                <tr class="formText">
                                    <td bgcolor="#ffffff" style="height: 10px">
                                        Birth Place</td>
                                    <td bgcolor="#ffffff">
                                        :</td>
                                    <td bgcolor="#ffffff">
                                        <asp:Label ID="lblBirthPlaceCIF" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="formText">
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        Address</td>
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        :</td>
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        <asp:TextBox ID="AddressTextBox" runat="server" CssClass="textbox" Height="50px"
                                            TextMode="MultiLine" Width="99%"></asp:TextBox></td>
                                </tr>
                                <tr class="formText">
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        Identity Number</td>
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        :</td>
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        <asp:TextBox ID="IdentityNumberTextBox" runat="server" CssClass="textbox" Height="50px"
                                            TextMode="MultiLine" Width="99%"></asp:TextBox></td>
                                </tr>
                                <tr class="formText">
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        Phone Number</td>
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        :</td>
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        <asp:TextBox ID="TxtPhoneNumberCustomer" runat="server" CssClass="textbox" Height="50px" TextMode="MultiLine"
                                            Width="99%"></asp:TextBox></td>
                                </tr>
                                
                                <tr class="formText">
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        &nbsp;</td>
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        &nbsp;</td>
                                    <td bgcolor="#ffffff" style="height: 19px">
                                        &nbsp;</td>
                                </tr>
                                
                            </table>
                            </div>
                        </td>
                        <td bgcolor="#ffffff" colspan="3" style="height: 15px" valign="top">
                        <div>
                        <table id="Table1" width="100%" borderColor="#ffffff" cellSpacing="1" cellPadding="2"  bgColor="#dddddd" border="2">
                            <tr>
                                <td bgcolor="#ffffff" style="height: 10px; color: #ffffff;" colspan="3">
                                    a</td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff" style="height: 19px">
                                    Name</td>
                                <td bgcolor="#ffffff" style="width: 10px; height: 19px;">
                                    :</td>
                                <td bgcolor="#ffffff" style="height: 19px">
                                    <asp:TextBox ID="NameSuspectTextBox" runat="server" CssClass="textbox" Height="50px" TextMode="MultiLine"
                                        Width="99%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff" style="height: 10px">
                                    List Type</td>
                                <td bgcolor="#ffffff" style="width: 10px;">
                                    :</td>
                                <td bgcolor="#ffffff">
                                    <asp:Label ID="LblListTypeSuspect" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff" style="height: 10px">
                                    List Category</td>
                                <td bgcolor="#ffffff" style="width: 10px;">
                                    :</td>
                                <td bgcolor="#ffffff">
                                    <asp:Label ID="LblListCategorySuspect" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff" style="height: 10px">
                                    Date Of Birth</td>
                                <td bgcolor="#ffffff" style="width: 10px;">
                                    :</td>
                                <td bgcolor="#ffffff">
                                    <asp:Label ID="LblDateOfBirthSuspect" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff" style="height: 10px">
                                    Birth Place</td>
                                <td bgcolor="#ffffff" style="width: 10px;">
                                    :</td>
                                <td bgcolor="#ffffff">
                                    <asp:Label ID="LblBirthPlace" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff" style="height: 19px">
                                    Address</td>
                                <td bgcolor="#ffffff" style="height: 19px; width: 10px;">
                                    :</td>
                                <td bgcolor="#ffffff" style="height: 19px">
                                    <asp:TextBox ID="AddressSuspectTextBox" runat="server" CssClass="textbox" Height="50px"
                                        TextMode="MultiLine" Width="99%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff" style="height: 19px">
                                    Identity Number</td>
                                <td bgcolor="#ffffff" style="height: 19px; width: 10px;">
                                    :</td>
                                <td bgcolor="#ffffff" style="height: 19px">
                                    <asp:TextBox ID="IdentityNumberSuspectTextBox" runat="server" CssClass="textbox" Height="50px" TextMode="MultiLine"
                                        Width="99%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff" style="height: 19px">
                                    Custom Remark 1</td>
                                <td bgcolor="#ffffff" style="width: 10px; height: 19px">
                                    :</td>
                                <td bgcolor="#ffffff" style="height: 19px" valign="top">
                                    <asp:TextBox ID="TxtCustomRemark1" runat="server" CssClass="textbox" Height="50px"
                                        TextMode="MultiLine" Width="99%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff" style="height: 19px">
                                    Custom Remark 2</td>
                                <td bgcolor="#ffffff" style="width: 10px; height: 19px">
                                    :</td>
                                <td bgcolor="#ffffff" style="height: 19px" valign="top">
                                    <asp:TextBox ID="TxtCustomRemark2" runat="server" CssClass="textbox" Height="50px"
                                        TextMode="MultiLine" Width="99%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff" style="height: 19px">
                                    Custom Remark 3</td>
                                <td bgcolor="#ffffff" style="width: 10px; height: 19px">
                                    :</td>
                                <td bgcolor="#ffffff" style="height: 19px" valign="top">
                                    <asp:TextBox ID="TxtCustomRemark3" runat="server" CssClass="textbox" Height="50px"
                                        TextMode="MultiLine" Width="99%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff" style="height: 19px">
                                    Custom Remark 4</td>
                                <td bgcolor="#ffffff" style="width: 10px; height: 19px">
                                    :</td>
                                <td bgcolor="#ffffff" style="height: 19px" valign="top">
                                    <asp:TextBox ID="TxtCustomRemark4" runat="server" CssClass="textbox" Height="50px"
                                        TextMode="MultiLine" Width="99%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff" style="height: 19px">
                                    Custom Remark 5</td>
                                <td bgcolor="#ffffff" style="width: 10px; height: 19px">
                                    :</td>
                                <td bgcolor="#ffffff" style="height: 19px" valign="top">
                                    <asp:TextBox ID="TxtCustomRemark5" runat="server" CssClass="textbox" Height="50px"
                                        TextMode="MultiLine" Width="99%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff" style="height: 19px">
                                    <asp:ImageButton ID="BtnDetail" runat="server" Visible="false" />
                                </td>
                                <td bgcolor="#ffffff" style="width: 10px; height: 19px">&nbsp;</td>
                                <td bgcolor="#ffffff" style="height: 19px" valign="top">&nbsp;</td>
                            </tr>
                        </table>
                        </div>
                        
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px; width: 25px;">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 6px; width: 18%;">
                Judgement Comment</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 6px; width: 1%;">
                :
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 6px; width: 663px;">
                at
                <asp:Label ID="LabelTglJudgement" runat="server"></asp:Label>
                by
                <asp:Label ID="LabelUserJudgement" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px; width: 25px;">
            </td>
            <td bgcolor="#ffffff" colspan="3" rowspan="1" style="height: 6px">
    <asp:TextBox ID="TxtJudgementComment" runat="server" TextMode="MultiLine" Width="100%" Height="72px"></asp:TextBox></td>
        </tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 25px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="True" SkinID="AcceptButton"></asp:imagebutton></td>
                        <td style="width: 25px" valign="middle">
                            <asp:ImageButton ID="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton" /></td>
                        <td style="width: 25px" valign="middle">
                            <asp:ImageButton ID="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton" /></td>
                        <td style="width: 7px">
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
					</tr>
				</table>               </td>
		</tr>
	</table>
        &nbsp;
        <br />
    </ajax:AjaxPanel>
</asp:Content>