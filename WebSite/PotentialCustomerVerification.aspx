﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="PotentialCustomerVerification.aspx.vb" Inherits="PotentialCustomerVerification" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    
    <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="99%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong> Customer Screening&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="90%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">&nbsp;</td>
        </tr>
    </table>
    <ajax:AjaxPanel ID="potential" runat="server" Width="99%"  >
        <asp:Panel ID="PanelCustomerScreening" runat="server" >
            <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="99%" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
        <tr class="formText">
            <td bgcolor="#ffffff" style="height: 24px; width: 1%;">
                &nbsp;</td>
            <td bgcolor="#ffffff" style="height: 24px; width: 10%;" valign="top" width="20%">
                Name / Alias</td>
            <td bgcolor="#ffffff" style="height: 24px" valign="top" width="1%">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="90%">
                <asp:TextBox ID="TxtName" runat="server" CssClass="textBox" Width="280px" 
                    MaxLength="150"></asp:TextBox>
                <strong><span style="color: #ff0000">* </span></strong>
                <br />
                <br />
                <strong><span style="color: #ff0000"></span></strong></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 1%; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="width: 10%; height: 24px" valign="top" width="20%">
                Date Of Birth</td>
            <td bgcolor="#ffffff" style="height: 24px" valign="top" width="1%">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="90%">
                <asp:TextBox ID="TxtDOB" runat="server" CssClass="textBox" MaxLength="20" Width="152px"></asp:TextBox>
                <strong><span style="color: #ff0000">
                    <input id="popUp" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                        border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                        border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif); width: 16px;" title="Click to show calendar"
                        type="button" />
                    </span></strong></td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="height: 24px"><br />
                </td>
			<td  bgColor="#ffffff" style="height: 24px" valign="top">
                Nationality</td>
			<td bgColor="#ffffff" style="height: 24px" valign="top">:</td>
			<td bgColor="#ffffff" style="height: 24px"><asp:textbox id="TxtNationality" 
                    runat="server" CssClass="textBox" Width="280px" MaxLength="255"></asp:textbox>
                <strong><span style="color: #ff0000"></span></strong>
                <br />
                <br />
                <br />
                <strong><span style="color: #ff0000"></span></strong></td>
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSearch" runat="server" ajaxcall="none" CausesValidation="True" 
                                SkinID="SearchButton" ImageUrl="~/Images/button/search.gif"  ></asp:imagebutton></td>
						<td>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
					</tr>
				</table>               </td>
		</tr>
	</table>
        </asp:Panel>

        <asp:Panel ID="PanelResult" runat="server" Visible="false">

            <table style="width: 100%;">
                <tr>
                    <td colspan="3" style="width: 100%">
                        <strong>Customer Screening - Search Result &nbsp; <asp:Label ID="lblSearching" runat="server" Text="Label"></asp:Label> </strong>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="width: 100%">
                        <asp:GridView ID="GrdResult" runat="server" AllowPaging="True" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True" ForeColor="Black" GridLines="Vertical" Width="90%" AutoGenerateColumns="False">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:CommandField SelectText="Detail" ShowSelectButton="True" />
                                <asp:BoundField DataField="ID" HeaderText="ID" Visible="False" />
                                <asp:BoundField DataField="VerificationListID" HeaderText="VerificationListID" />
                                <asp:BoundField DataField="Nama" HeaderText="Nama" />
                                <asp:BoundField DataField="Nationality" HeaderText="Nationality" />
                                <asp:BoundField DataField="DOB" HeaderText="DOB" />
                                <asp:BoundField DataField="YOB" HeaderText="YOB" />
                                <asp:BoundField DataField="percenmatch" HeaderText="PercentMatch" />
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <RowStyle BackColor="#F7F7DE"   />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />                                            
                        </asp:GridView>
                    </td>
                </tr>
                </table>
            <table style="width: 100%;" >
                <tr>
                    <td style="width: 10px">&nbsp;</td>
                    <td style="width: 49%">&nbsp;</td>
                    <td style="width: 49%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px">&nbsp;</td>
                    <td style="width: 49%"><strong>Searching Data</strong></td>
                    <td style="width: 49%"><strong>Match With</strong></td>
                </tr>
                <tr>
                    <td style="width: 10px">&nbsp;</td>
                    <td style="width: 49%" valign="top">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 70px">Name</td>
                                <td style="width: 10px">:</td>
                                <td style="width: 90%">
                                    <asp:Label ID="LblName" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70px">DOB</td>
                                <td>:</td>
                                <td>
                                    <asp:Label ID="LblDOB" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70px">Nationality</td>
                                <td>:</td>
                                <td>
                                    <asp:Label ID="LblNationality" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 49%">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 150px">Nama</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblNameWatchList" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">DOB</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblDOBWatchList" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">YOB</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblYOBWatchlist" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Nationality</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblNationalityWatchList" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Birth Place</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblBirthPlaceWatchList" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">List Type</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblListTypeWatchList" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Category Type</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblCategoryType" runat="server"></asp:Label>
                                    &nbsp;<asp:LinkButton ID="linkcategory" runat="server" Visible="False"></asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Custom Remark 1</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblCustomRemarks1" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Custom Remark 2</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblCustomRemarks2" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Custom Remaks 3</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblCustomRemarks3" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Custom Remaks 4</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblCustomRemarks4" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Custom Remaks 5</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblCustomRemarks5" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">
                                    &nbsp;</td>
                                <td style="width: 5px">&nbsp;</td>
                                <td style="width: 70%">&nbsp;</td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="LblAssociate" runat="server" Visible="false">Associate</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <asp:GridView ID="GridViewRelationCstmr" runat="server" AllowPaging="True" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True" ForeColor="Black" GridLines="Vertical" Width="90%" AutoGenerateColumns="False" DataKeyNames="RefID, Relation_Category_ID, Relation_Customer_ID">
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkRelationDetail" runat="server" Text="Detail"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="RefId" Visible="false"  />
                                    <asp:BoundField DataField="Relation_Customer_ID" Visible="false"  />
                                    <asp:BoundField DataField="Relation_Category_ID" Visible="false" />
                                    <asp:BoundField DataField="Relation_Category_Name" HeaderText="Type" />
                                    <asp:BoundField DataField="RelationTypeName" HeaderText="Relation" />
                                    <asp:BoundField DataField="Relation_Customer_Name" HeaderText="Nama" />
                                    <asp:BoundField DataField="Nationality" HeaderText="Nationality" />
                                
                                </Columns>
                                <FooterStyle BackColor="#CCCC99" />
                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                <RowStyle BackColor="#F7F7DE"   />
                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />                                            
                            </asp:GridView>
                            </tr>
                           
                        </table>
                    </td>
                </tr>
                <tr class="formText" bgColor="#dddddd" >
                    <td style="width: 15px">
                        <img height="15" src="images/arrow.gif" width="15"> </img>&nbsp;</td>
                    <td colspan="2" style="width:90%">
                        <asp:ImageButton ID="ImageBack" runat="server" CausesValidation="True" ImageUrl="~/Images/button/back.gif" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

</ajax:AjaxPanel>
	<script language="javascript" type="text/javascript">
	   
	</script>

</asp:Content>

