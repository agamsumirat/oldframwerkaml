Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Imports System.IO
Imports System.Collections.Generic
Partial Class MappingGroupMenuSTRPPATKAdd
    Inherits Parent

    Public Property SetnGetMode() As String
        Get
            If Not Session("MappingGroupMenuSTRPPATKAdd.Mode") Is Nothing Then
                Return CStr(Session("MappingGroupMenuSTRPPATKAdd.Mode"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingGroupMenuSTRPPATKAdd.Mode") = value
        End Set
    End Property

    Private Sub LoadMode()
        Session("MappingGroupMenuSTRPPATKAdd.Mode") = Nothing
        CboGroupMenu.Items.Clear()
        Using ObjSTRPPATKBll As New AMLBLL.MappingGroupMenuSTRPPATKBLL
            CboGroupMenu.AppendDataBoundItems = True
            CboGroupMenu.DataSource = ObjSTRPPATKBll.GeModeData
            CboGroupMenu.DataTextField = GroupColumn.GroupName.ToString
            CboGroupMenu.DataValueField = GroupColumn.GroupID.ToString
            CboGroupMenu.DataBind()
            CboGroupMenu.Items.Insert(0, New ListItem("...", ""))
        End Using
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oTrans As SqlTransaction = Nothing
        Try
            Dim GroupID As Integer
            If CboGroupMenu.Text = "" Then
                GroupID = 0
            Else
                GroupID = CInt(CboGroupMenu.Text)
            End If

            Dim GroupName As String = ""
            If GroupID > 0 Then

                AMLBLL.MappingGroupMenuSTRPPATKBLL.IsDataValidAddApproval(CStr(GroupID))

                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    Using ObjMappingGroupMenuSTRPPATK As New MappingGroupMenuSTRPPATK
                        Using Objvw_MappingGroupMenuSTRPPATKList As VList(Of vw_MappingGroupMenuSTRPPATK) = DataRepository.vw_MappingGroupMenuSTRPPATKProvider.GetPaged("FK_Group_ID = '" & GroupID & "'", "", 0, Integer.MaxValue, 0)
                            If Objvw_MappingGroupMenuSTRPPATKList.Count > 0 Then
                                'Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKView.aspx"
                                'Me.Response.Redirect("MappingGroupMenuSTRPPATKView.aspx", False)

                                Me.LblSuccess.Text = "Data " & Objvw_MappingGroupMenuSTRPPATKList(0).GroupName & " already exists."
                                Me.LblSuccess.Visible = True
                                LoadMode()
                            Else
                                ObjMappingGroupMenuSTRPPATK.FK_Group_ID = GroupID
                                DataRepository.MappingGroupMenuSTRPPATKProvider.Save(ObjMappingGroupMenuSTRPPATK)
                                'Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKView.aspx"
                                'Me.Response.Redirect("MappingGroupMenuSTRPPATKView.aspx", False)
                                Using ObjGroup As TList(Of Group) = DataRepository.GroupProvider.GetPaged("GroupID = '" & GroupID & "'", "", 0, Integer.MaxValue, 0)

                                    AMLBLL.MappingGroupMenuSTRPPATKBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingGroupMenuSTRPPATK", "GroupName", "Add", "", ObjGroup(0).GroupName, "Acc")
                                    Me.LblSuccess.Text = "Data saved successfully"
                                    Me.LblSuccess.Visible = True
                                    LoadMode()
                                End Using
                            End If
                        End Using
                    End Using
                Else
                    Using ObjMappingGroupMenuSTRPPATK_Approval As New MappingGroupMenuSTRPPATK_Approval
                        Using ObjMappingGroupMenuSTRPPATKList As TList(Of MappingGroupMenuSTRPPATK) = DataRepository.MappingGroupMenuSTRPPATKProvider.GetPaged("FK_Group_ID = '" & GroupID & "'", "", 0, Integer.MaxValue, 0)
                            If ObjMappingGroupMenuSTRPPATKList.Count > 0 Then
                                Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKView.aspx"
                                Me.Response.Redirect("MappingGroupMenuSTRPPATKView.aspx", False)

                            Else
                                Using objgroup As TList(Of Group) = DataRepository.GroupProvider.GetPaged("GroupID = '" & GroupID & "'", "", 0, Integer.MaxValue, 0)
                                    GroupName = objgroup(0).GroupName
                                    AMLBLL.MappingGroupMenuSTRPPATKBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingGroupMenuSTRPPATK", "GroupName", "Add", "", GroupName, "PendingApproval")
                                    ObjMappingGroupMenuSTRPPATK_Approval.FK_MsUserID = Sahassa.AML.Commonly.SessionPkUserId
                                    ObjMappingGroupMenuSTRPPATK_Approval.GroupMenu = GroupName
                                    ObjMappingGroupMenuSTRPPATK_Approval.FK_ModeID = CInt(Sahassa.AML.Commonly.TypeMode.Add)
                                    ObjMappingGroupMenuSTRPPATK_Approval.CreatedDate = Now
                                    DataRepository.MappingGroupMenuSTRPPATK_ApprovalProvider.Save(ObjMappingGroupMenuSTRPPATK_Approval)

                                    Using ObjMappingGroupMenuSTRPPATK_approvalDetail As New MappingGroupMenuSTRPPATK_ApprovalDetail
                                        ObjMappingGroupMenuSTRPPATK_approvalDetail.FK_MappingGroupMenuSTRPPATK_Approval_ID = ObjMappingGroupMenuSTRPPATK_Approval.PK_MappingGroupMenuSTRPPATK_Approval_ID
                                        ObjMappingGroupMenuSTRPPATK_approvalDetail.FK_Group_ID = GroupID
                                        DataRepository.MappingGroupMenuSTRPPATK_ApprovalDetailProvider.Save(ObjMappingGroupMenuSTRPPATK_approvalDetail)
                                    End Using
                                End Using
                                'Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKView.aspx"
                                'Me.Response.Redirect("MappingGroupMenuSTRPPATKView.aspx", False)
                                Using ObjGroup As TList(Of Group) = DataRepository.GroupProvider.GetPaged("GroupID = '" & GroupID & "'", "", 0, Integer.MaxValue, 0)
                                    Me.LblSuccess.Text = "Data saved successfully and waiting for Approval."
                                    Me.LblSuccess.Visible = True
                                    LoadMode()
                                End Using
                            End If
                        End Using

                    End Using
                End If
            Else
                Me.LblSuccess.Text = "Please select Group Menu"
                Me.LblSuccess.Visible = True
            End If
        Catch ex As Exception
            If Not oTrans Is Nothing Then oTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
                oTrans = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKView.aspx"
            Me.Response.Redirect("MappingGroupMenuSTRPPATKView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                '    Transcope.Complete()
            End Using
            If Not Page.IsPostBack Then
                LoadMode()
            End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
