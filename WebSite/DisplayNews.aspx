<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DisplayNews.aspx.vb" Inherits="DisplayNews" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<%@ Register Src="webcontrol/footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>:: Anti Money Laundering ::</title>
		<link href="theme/aml.css" rel="stylesheet" type="text/css">
	<script src="script/x_load.js" type="text/javascript"></script>
	<script language="javascript">xInclude('script/x_core.js');</script>
		<script src="script/aml.js" ></script>	
			<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	</HEAD>
	<body bgColor="#ffffff" leftMargin="0" topMargin="0" marginheight="0" marginwidth="0">
	 <form id="form1" runat="server">
	  <table background="images/main-header-bg.gif" height="81" width="100%" cellpadding="0" cellspacing="0" border="0">
  	    <tr>
            <td width="153"><img src="images/main-uang-dijemur-dan-logo.jpg" width="153" height="81" /></td>
  	        <td width="357"><img src="images/main-title.jpg" width="357" height="25" /></td>
  	        <td width="99%">&nbsp;</td>
  	        <td align="right" background="images/main-bar-merah-kanan.jpg" valign="top" class="toprightmenu"><img src="images/blank.gif" width="425" height="6"><br>
                  </td>               
  	    </tr>
  	</table> 
    <table id="tablebasic" style="width: 100%; height: 184px" cellpadding="0" cellspacing="0" border="0" >
        <tr>
            <td colspan="1" rowspan="1" style="width: 8px">
                &nbsp;&nbsp;
            </td>
            <td colspan="3" rowspan="1" style="width: 654px" id="tdcontent" valign="top">
              <div id="divcontent" class="divcontent" >
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td style="width: 654px">
                            <br />
              
                  <br />
                <br />
                <asp:Label ID="LabelNewsTitle" runat="server" Font-Bold="True" Font-Italic="False" Font-Underline="True" Font-Size="20px"></asp:Label>
              <br />
                  <asp:Label ID="LabelEntryDate" runat="server" Font-Bold="False"></asp:Label><br />
                            
                            <asp:Label ID="TextNewsContent" runat="server" Text="Label"></asp:Label>
                <br />
                <br />
                <asp:GridView ID="GridAttach" runat="server" AutoGenerateColumns="False"
                            CellPadding="4" ForeColor="Black" GridLines="Vertical" DataKeyNames="PK_NewsAttachment"
                            BackColor="White" BorderStyle="None" 
                            BorderWidth="0px" EnableModelValidation="True">
                            <FooterStyle BackColor="#CCCC99" />
                            <Columns>
                                <asp:BoundField HeaderText="PK_NewsAttachment" Visible="False"
                                    DataField="PK_NewsAttachment" ShowHeader="False">
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:DataList ID="DataList1" runat="server">
                                            <ItemTemplate>
                                                <%#GenerateLink(Eval("PK_NewsAttachment"), Eval("Filename"), Eval("Fk_NewsApprovalId"))%>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </ItemTemplate>
                                    <FooterStyle Wrap="False" />
                                    <ItemStyle Wrap="False" />
                                </asp:TemplateField>
                            </Columns>
                            <%--<RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />--%>
                            <%--<PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />--%>
                            <%--<HeaderStyle BackColor="#FFFFFF" Font-Bold="True" ForeColor="White" />--%>
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                <br />
                <asp:HyperLink ID="HyperLinkMoreNews" runat="server" Target="_self" NavigateUrl="~/DisplayAllNews.aspx">[Show All News]</asp:HyperLink>&nbsp;
                &nbsp;<asp:HyperLink ID="HyperLinkBack" runat="server" NavigateUrl="~/Login.aspx" Target="_self">[Back to Login Page]</asp:HyperLink>
                        </td>
                    </tr>
                </table>
              </div>                     
            </td>
        </tr>
        <tr>
            <td colspan="4" rowspan="1">
            <uc1:footer ID="Footer1" runat="server" />
            </td>
        </tr>
          </table>
</form>
    	<script>arrangeViewNoMenu();
    	window.onresize=arrangeViewNoMenu;
    	</script>
	</body>
</HTML>