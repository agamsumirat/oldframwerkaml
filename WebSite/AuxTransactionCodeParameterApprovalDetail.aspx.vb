Imports System.Data
Imports System.Data.SqlClient

Partial Class AuxTransactionCodeParameterApprovalDetail
    Inherits Parent

    Private InMenuManagementId As Integer
    Private InGroupId As String

    Public ReadOnly Property GetPkMenuPendingApproval() As Integer
        Get
            Return CInt(Me.Request.Params("MenuManagementId"))
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            GetDataAccessAuthority()

            If Not Page.IsPostBack Then
                Me.ImageButtonAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageButtonReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage += ex.Message & "<br>"
            LogError(ex)
        End Try
    End Sub

    Private Sub GetDataAccessAuthority()
        Try
            ' New Value
            ListNewValue.Visible = True
            Using AccessProduct As New AMLDAL.AuxTransactionCodeTableAdapters.AuxTransactionCodeTableAdapter
                Me.ListNewValue.Items.Clear()
                Me.ListNewValue.DataSource = AccessProduct.ViewNewValue(Session("AuxTransactionCodeApprovalApprovalID"))
                Me.ListNewValue.DataTextField = "TransactionDescription"
                Me.ListNewValue.DataValueField = "TransactionCode"
                Me.ListNewValue.DataBind()
            End Using

            ' Old
            ListOldValue.Visible = True
            Using AccessProduct As New AMLDAL.AuxTransactionCodeTableAdapters.AuxTransactionCodeTableAdapter
                Me.ListOldValue.Items.Clear()
                Me.ListOldValue.DataSource = AccessProduct.SelectSelected
                Me.ListOldValue.DataTextField = "TransactionDescription"
                Me.ListOldValue.DataValueField = "TransactionCode"
                Me.ListOldValue.DataBind()
            End Using
        Catch
            Throw
        End Try
    End Sub

    Private Sub ImageButtonAccept_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAccept.Click
        Try
            AccessAuthorityApprove()

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage += ex.Message & "<br>"
            LogError(ex)
        End Try
    End Sub

    Private Sub ImageButtonReject_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReject.Click
        Try
            AccessAuthorityReject()

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage += ex.Message & "<br>"
            LogError(ex)
        End Try
    End Sub

    Public Function CekExistingFileSecurityApproval(ByVal pk_menu_pending_id As Integer) As Boolean
        Try
            Using accessFileSecurity As New AMLDAL.AMLDataSetTableAdapters.Menu_PendingApprovalByIDTableAdapter
                Using dt As AMLDAL.AMLDataSet.Menu_PendingApprovalByIDDataTable = accessFileSecurity.GetData(pk_menu_pending_id)
                    If dt.Rows.Count > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub AccessAuthorityApprove()
        Dim Trans As SqlTransaction = Nothing
        Try
            Using AccessTable As New AMLDAL.AuxTransactionCodeTableAdapters.AuxTransactionCodeTableAdapter
                Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessTable)
                ' Hapus semua data pada table AuxTransactionCodeParameter
                AccessTable.DeleteByTransactionCode()

                Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    'Masukkan data baru ke table AuxTransactionCodeParameter
                    For x As Integer = 0 To Me.ListNewValue.Items.Count - 1
                        Me.ListNewValue.SelectedIndex = x
                        AccessTable.InsertNewTransactionCode(Me.ListNewValue.SelectedValue, Now)
                        AccessAuditTrail.Insert(Now, Nothing, Sahassa.AML.Commonly.SessionUserId, "AuxTransactionCodeParameter", "AuxTransactionCode", "Add", "", Me.ListNewValue.SelectedValue, "Accepted")
                        AccessAuditTrail.Insert(Now, Nothing, Sahassa.AML.Commonly.SessionUserId, "AuxTransactionCodeParameter", "LastUpdatedDate", "Add", "", Now, "Accepted")
                    Next
                End Using
                'Hapus data pada table AuxTransactionCodeParameter_Approval
                AccessTable.DeleteTransactionApproval(Session("AuxTransactionCodeApprovalApprovalID"))
                'Hapus data pada table AuxTransactionCodeParameter_PendingApproval
                AccessTable.DeleteTransactionPendingApproval(Session("AuxTransactionCodeApprovalApprovalID"))
                Trans.Commit()
            End Using

            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=81304"
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=81304", False)
        Catch ex As Exception
            If Not Trans Is Nothing Then Trans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
                Trans = Nothing
            End If
        End Try
    End Sub

    Private Sub AccessAuthorityReject()
        Dim Trans As SqlTransaction = Nothing
        Try
            Using AccessTable As New AMLDAL.AuxTransactionCodeTableAdapters.AuxTransactionCodeTableAdapter
                Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessTable)

                'Hapus data pada table AuxTransactionCodeParameter_Approval
                AccessTable.DeleteTransactionApproval(Session("AuxTransactionCodeApprovalApprovalID"))
                'Hapus data pada table AuxTransactionCodeParameter_PendingApproval
                AccessTable.DeleteTransactionPendingApproval(Session("AuxTransactionCodeApprovalApprovalID"))
                Trans.Commit()
            End Using

            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=81304"
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=81304", False)
        Catch ex As Exception
            If Not Trans Is Nothing Then Trans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
                Trans = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "AuxTransactionCodeParameterApproval.aspx"
        Me.Response.Redirect("AuxTransactionCodeParameterApproval.aspx", False)
    End Sub
End Class