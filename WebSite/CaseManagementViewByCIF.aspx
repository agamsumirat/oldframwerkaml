﻿<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="CaseManagementViewByCIF.aspx.vb"Inherits="CaseManagementViewByCIF" Title="Case Management - View"  Culture="id-ID" UICulture ="id-ID" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Alert Case Management By CIF- View
                </strong>
            </td>
        </tr>
    </table>
  
        <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
            border="2">
            <tr id="searchbox">
                <td valign="top" width="98%" bgcolor="#ffffff">
                    <table cellspacing="4" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td valign="middle" nowrap>
                                <ajax:AjaxPanel ID="AjaxPanel14" runat="server">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td nowrap style="height: 26px">
                                                Customer Name</td>
                                            <td nowrap style="height: 26px">
                                                :</td>
                                            <td nowrap style="height: 26px">
                                                <asp:TextBox ID="txtCustomerName" runat="server" CssClass="searcheditbox" 
                                                    TabIndex="2"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td nowrap style="height: 26px">
                                                CIF No</td>
                                            <td nowrap style="height: 26px">
                                                :</td>
                                            <td nowrap style="height: 26px">
                                                <asp:TextBox ID="txtCIFNo" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" style="height: 26px">
                                                Total Case ID</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                <asp:TextBox ID="txtTotal" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" style="height: 26px">
                                                Aging</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                <asp:TextBox ID="txtAgingFrom" runat="server" CssClass="searcheditbox" 
                                                    TabIndex="2"></asp:TextBox>
                                                to
                                                <asp:TextBox ID="txtAgingTo" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" style="height: 26px">
                                                AML/CFT Risk</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                <asp:TextBox ID="TxtAMLRisk" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="10">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <asp:ImageButton ID="ImageButtonSearch" TabIndex="3" runat="server" SkinID="SearchButton"
                                                    ImageUrl="~/Images/button/search.gif"></asp:ImageButton>
                                                <asp:ImageButton ID="ImageClearSearch" runat="server" ImageUrl="~/Images/button/clearsearch.gif"  CausesValidation ="false"  /></td>
                                        </tr>
                                    </table>
                                    &nbsp;&nbsp;&nbsp;
                                </ajax:AjaxPanel>
                            </td>
                            <td valign="middle" width="99%">
                                &nbsp;</td>
                        </tr>
                    </table>
                    <ajax:AjaxPanel ID="AjaxPanel15" runat="server">
                        <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
                </td>
            </tr>
        </table>
    
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2">
        <tr>
            <td bgcolor="#ffffff">
                <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                    <asp:DataGrid ID="GridMSUserView" runat="server" AutoGenerateColumns="False" Font-Size="XX-Small"
                        BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
                        Width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE" ForeColor="Black">
                        <FooterStyle BackColor="#CCCC99"></FooterStyle>
                        <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
                        <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                        <ItemStyle BackColor="#F7F7DE"></ItemStyle>
                        <HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B">
                        </HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="CustomerName" HeaderText="Customer Name" 
                                SortExpression="CustomerName desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CIFNo" HeaderText="CIF No" 
                                SortExpression="CIFNo desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Total" HeaderText="Total Case ID"
                                SortExpression="total desc">
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Aging" HeaderText="Aging"
                                SortExpression="aging  desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="amlrisk" HeaderText="AML/CFT Risk" SortExpression="amlrisk  desc">
                            </asp:BoundColumn>
                            <asp:TemplateColumn>
                                <EditItemTemplate>
                                    <asp:LinkButton runat="server" CommandName="Update" Text=""></asp:LinkButton>
                                    <asp:LinkButton runat="server" CausesValidation="false" CommandName="Cancel" Text=""></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="linkdetail" runat="server" CausesValidation="false" CommandName="Edit" Text="Detail"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </ajax:AjaxPanel>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="background-color: #ffffff">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td nowrap>
                            <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                                &nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
                                &nbsp;
                            </ajax:AjaxPanel>
                        </td>
                        <td width="99%">
                            &nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" runat="server">Export 
		        to Excel</asp:LinkButton>
                            <asp:LinkButton ID="LnkBtnExportAllToExcel" runat="server">Export All to Excel</asp:LinkButton></td>
                        <td align="right" nowrap>
                            &nbsp;&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                    bgcolor="#ffffff" border="2">
                    <tr class="regtext" align="center" bgcolor="#dddddd">
                        <td valign="top" align="left" width="50%" bgcolor="#ffffff">
                            Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                <asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>
                                &nbsp;of&nbsp;
                                <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label>
                            </ajax:AjaxPanel>
                        </td>
                        <td valign="top" align="right" width="50%" bgcolor="#ffffff">
                            Total Records&nbsp;
                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                <asp:Label ID="PageTotalRows" runat="server">0</asp:Label>
                            </ajax:AjaxPanel>
                        </td>
                    </tr>
                </table>
                <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                    bgcolor="#ffffff" border="2">
                    <tr bgcolor="#ffffff">
                        <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                            <hr color="#f40101" noshade size="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                            Go to page</td>
                        <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                    <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox>
                                </ajax:AjaxPanel>
                            </font>
                        </td>
                        <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton"></asp:ImageButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/first.gif" width="6">
                        </td>
                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                    OnCommand="PageNavigate">First</asp:LinkButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/prev.gif" width="6"></td>
                        <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                    OnCommand="PageNavigate">Previous</asp:LinkButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                <a class="pageNav" href="#">
                                    <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                        OnCommand="PageNavigate">Next</asp:LinkButton>
                                </a>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/next.gif" width="6"></td>
                        <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                    OnCommand="PageNavigate">Last</asp:LinkButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/last.gif" width="6"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
