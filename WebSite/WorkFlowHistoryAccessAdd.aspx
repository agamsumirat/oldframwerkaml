﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="WorkFlowHistoryAccessAdd.aspx.vb" Inherits="WorkFlowHistoryAccessAdd" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<ajax:ajaxpanel id="AjaxPanel1" runat="server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
        
	<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Workflow History Access- Add New&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000">* Required</span></td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px">
                </td>
			<td width="20%" bgColor="#ffffff" style="height: 24px">
                WorkFlow Step</td>
			<td width="5" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px">
                <asp:DropDownList ID="CboWorkflowStep" runat="server" CssClass="combobox">
                </asp:DropDownList>
            
            </td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px;" valign="top">
                
                </td>
			<td bgColor="#ffffff" style="height: 24px" valign="top">
                Access View Other<br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="height: 24px" valign="top">
                :<br />
               
            </td>
            <td bgcolor="#ffffff" style="height: 24px" valign="top">
                <table  cellpadding="1" cellspacing="1">
                    <tr>
                        <td>                  <asp:DropDownList ID="CboAccessView" runat="server" 
                                CssClass="combobox">
                </asp:DropDownList>  
                            <asp:ImageButton ID="ImgAddAccessView" runat="server" 
                                ImageUrl="~/Images/button/adddetail.gif" />
            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView runat="server" AutoGenerateColumns="False" ID="GrdWorkFlowAccess" 
                                EnableModelValidation="True" 
                                DataKeyNames="PK_WorkflowHistoryAccessDetail_ID" >
                                <Columns>
                                    
                                    <asp:BoundField DataField="AccessViewOtherWorkflow" 
                                        HeaderText="Access View Other" />
                                    <asp:CommandField ShowDeleteButton="True" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="AddButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
        </asp:View>
       <asp:View ID="VwConfirmation" runat="server">
                                            <table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                                width="100%" bgcolor="#dddddd" border="0">
                                                <tr bgcolor="#ffffff">
                                                    <td colspan="2" align="center" style="height: 17px">
                                                        <asp:Label ID="LblConfirmation" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr bgcolor="#ffffff">
                                                    <td align="center" colspan="2">
                                                        <asp:ImageButton ID="ImgBack" runat="server" ImageUrl="~/images/button/back.gif"
                                                            CausesValidation="False" /></td>
                                                </tr>
                                            </table>
                                        </asp:View>
    </asp:MultiView>

	</ajax:ajaxpanel>
</asp:Content>

