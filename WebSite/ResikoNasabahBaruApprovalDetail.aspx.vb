Option Explicit On
Option Strict On

Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports AMLBLL

Partial Class ResikoNasabahBaruApprovalDetail
    Inherits Parent

    Public ReadOnly Property PKResikoNasabahBaruApprovalID() As Integer
        Get
            If Session("ResikoNasabahBaruApprovalDetail.PK") Is Nothing Then
                Dim StrTmppkResikoNasabahBaruApprovalId As String

                StrTmppkResikoNasabahBaruApprovalId = Request.Params("PKResikoNasabahBaruApprovalID")
                If Integer.TryParse(StrTmppkResikoNasabahBaruApprovalId, 0) Then
                    Session("ResikoNasabahBaruApprovalDetail.PK") = StrTmppkResikoNasabahBaruApprovalId
                Else
                    Throw New SahassaException("Parameter PKResikoNasabahBaruApprovalID is not valid.")
                End If

                Return CInt(Session("ResikoNasabahBaruApprovalDetail.PK"))
            Else
                Return CInt(Session("ResikoNasabahBaruApprovalDetail.PK"))
            End If
        End Get
    End Property

    Public ReadOnly Property ObjResikoNasabahBaruApproval() As TList(Of ResikoNasabahBaru_Approval)
        Get
            Dim PkApprovalid As Integer
            Try
                If Session("ResikoNasabahBaruApprovalDetail.ObjResikoNasabahBaruApproval") Is Nothing Then
                    PkApprovalid = PKResikoNasabahBaruApprovalID
                    Session("ResikoNasabahBaruApprovalDetail.ObjResikoNasabahBaruApproval") = DataRepository.ResikoNasabahBaru_ApprovalProvider.GetPaged("PK_ResikoNasabahBaru_ApprovalID = " & PkApprovalid, "", 0, Integer.MaxValue, 0)
                End If
                Return CType(Session("ResikoNasabahBaruApprovalDetail.ObjResikoNasabahBaruApproval"), TList(Of ResikoNasabahBaru_Approval))
            Finally
            End Try
        End Get
    End Property

    Private Sub LoadDataApproval()
        If ObjResikoNasabahBaruApproval.Count > 0 Then
            If ObjResikoNasabahBaruApproval(0).FK_ModeID.GetValueOrDefault(0) = 2 Then
                Me.LblAction.Text = "Edit"
            End If

            Dim strRequestBy As String = ""
            Using objRequestBy As User = SahassaNettier.Data.DataRepository.UserProvider.GetByPkUserID(ObjResikoNasabahBaruApproval(0).FK_MsUserID.GetValueOrDefault(0))
                If Not objRequestBy Is Nothing Then
                    strRequestBy = objRequestBy.UserID & " ( " & objRequestBy.UserName & " ) "
                End If
            End Using

            LblRequestBy.Text = strRequestBy
            LblRequestDate.Text = ObjResikoNasabahBaruApproval(0).CreatedDate.GetValueOrDefault().ToString("dd-MM-yyyy")

            If ObjResikoNasabahBaruApproval(0).FK_ModeID.GetValueOrDefault(0) = 2 Then
                'Old
                Me.GrdVwResikoNasabahBaruOld.DataSource = DataRepository.ResikoNasabahBaruProvider.GetPaged("", "PK_ResikoNasabahBaru_ID", 0, Integer.MaxValue, 0)
                Me.GrdVwResikoNasabahBaruOld.DataBind()

                'New
                Me.GrdVwResikoNasabahBaruNew.DataSource = DataRepository.ResikoNasabahBaru_ApprovalDetailProvider.GetPaged("", "PK_ResikoNasabahBaru_ApprovalDetail_ID", 0, Integer.MaxValue, 0)
                Me.GrdVwResikoNasabahBaruNew.DataBind()
            End If
        Else
            'kalau ternyata sudah di approve /reject dan memakai tombol back, maka redirect ke viewapproval
            Response.Redirect("ResikoNasabahBaruApproval.aspx", False)
        End If
    End Sub

    Private Sub ClearSession()
        Session("ResikoNasabahBaruApprovalDetail.PK") = Nothing
        Session("ResikoNasabahBaruApprovalDetail.ObjResikoNasabahBaruApproval") = Nothing
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                LoadDataApproval()
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAccept.Click
        Try
            If ObjResikoNasabahBaruApproval.Count > 0 Then
                Using ObjResikoNasabahBaruBll As New ResikoNasabahBaruBLL
                    If ObjResikoNasabahBaruBll.AcceptEdit Then
                        Response.Redirect("ResikoNasabahBaruApproval.aspx", False)
                    End If
                End Using
            Else
                Response.Redirect("ResikoNasabahBaruApproval.aspx", False)
            End If

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnReject.Click
        Try
            If ObjResikoNasabahBaruApproval.Count > 0 Then
                Using ObjResikoNasabahBaruBll As New ResikoNasabahBaruBLL
                    If ObjResikoNasabahBaruBll.RejectEdit Then
                        Response.Redirect("ResikoNasabahBaruApproval.aspx", False)
                    End If
                End Using
            Else
                Response.Redirect("ResikoNasabahBaruApproval.aspx", False)
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("ResikoNasabahBaruApproval.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
    '    If CvalHandleErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    'Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalPageErr.PreRender
    '    If CvalPageErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub
End Class