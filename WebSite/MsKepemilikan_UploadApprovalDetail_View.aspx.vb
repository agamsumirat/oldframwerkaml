#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports System.Collections.Generic
Imports System.IO
Imports Sahassa.AML.Commonly
Imports AMLBLL.ValidateBLL
Imports AMLBLL.MsKepemilikanBll
Imports AMLBLL.DataType
Imports System.Drawing
#End Region
Partial Class MsKepemilikan_UploadApprovalDetail_View
    Inherits Parent


#Region "Member"
    Private ReadOnly BindGridFromExcel As Boolean
#End Region

#Region "Property"

    Public ReadOnly Property SetnGetUserID() As String
        Get
            Return SessionPkUserId
        End Get

    End Property

    Private Property GetTotalInsert() As Double
        Get
            Return Session("MsKepemilikan.GetTotalInsert")
        End Get
        Set(ByVal value As Double)
            Session("MsKepemilikan.GetTotalInsert") = value
        End Set
    End Property

    Private Property GetTotalUpdate() As Double
        Get
            Return Session("MsKepemilikan.GetTotalUpdate")
        End Get
        Set(ByVal value As Double)
            Session("MsKepemilikan.GetTotalUpdate") = value
        End Set
    End Property

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property

    Public Property SetIDKepemilikan() As String
        Get
            If Not Session("MsKepemilikan_ApprovalDetail.SetIDKepemilikan") Is Nothing Then
                Return CStr(Session("MsKepemilikan_ApprovalDetail.SetIDKepemilikan"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MsKepemilikan_ApprovalDetail.SetIDKepemilikan") = value
        End Set
    End Property

    Public Property SetNamaKepemilikan() As String
        Get
            If Not Session("MsKepemilikan_ApprovalDetail.SetNamaKepemilikan") Is Nothing Then
                Return CStr(Session("MsKepemilikan_ApprovalDetail.SetNamaKepemilikan"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MsKepemilikan_ApprovalDetail.SetNamaKepemilikan") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("MsKepemilikan_ApprovalDetail.Sort") Is Nothing, " IDKepemilikan  asc", Session("MsKepemilikan_ApprovalDetail.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("MsKepemilikan_ApprovalDetail.Sort") = Value
        End Set
    End Property

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("MsKepemilikan_ApprovalDetail.SelectedItem") Is Nothing, New ArrayList, Session("MsKepemilikan_ApprovalDetail.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("MsKepemilikan_ApprovalDetail.SelectedItem") = value
        End Set
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("MsKepemilikan_ApprovalDetail.RowTotal") Is Nothing, 0, Session("MsKepemilikan_ApprovalDetail.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MsKepemilikan_ApprovalDetail.RowTotal") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return iTotalPages(SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("MsKepemilikan_ApprovalDetail.CurrentPage") Is Nothing, 0, Session("MsKepemilikan_ApprovalDetail.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MsKepemilikan_ApprovalDetail.CurrentPage") = Value
        End Set
    End Property

    Public ReadOnly Property SetnGetBindTable() As VList(Of Vw_MsKepemilikan_UploadApprovalDetail)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String
            If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
                SetnGetCurrentPage = GetPageTotal - 1
            End If

            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = "FK_MsKepemilikan_Approval_id=" & parID

            If SetIDKepemilikan.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = Vw_MsKepemilikan_UploadApprovalDetailColumn.IDKepemilikan.ToString & " like '%" & SetIDKepemilikan.Trim.Replace("'", "''") & "%'"
            End If
            If SetNamaKepemilikan.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = Vw_MsKepemilikan_UploadApprovalDetailColumn.NamaKepemilikan.ToString & " like '%" & SetNamaKepemilikan.Trim.Replace("'", "''") & "%'"
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            Return DataRepository.Vw_MsKepemilikan_UploadApprovalDetailProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, GetDisplayedTotalRow, SetnGetRowTotal)

        End Get

    End Property


#End Region

#Region "Function"

    Function ConvertTo_MappingMsKepemilikanNCBSPPATKl(ByVal SourceObject As TList(Of MappingMsKepemilikanNCBSPPATK_Approval_Detail)) As TList(Of MappingMsKepemilikanNCBSPPATK)
        Dim temp As New TList(Of MappingMsKepemilikanNCBSPPATK)
        For Each i As MappingMsKepemilikanNCBSPPATK_Approval_Detail In SourceObject
            Dim DestObject As New MappingMsKepemilikanNCBSPPATK
            With DestObject
                FillOrNothing(.IDKepemilikanNCBS, i.IDKepemilikanNCBS, True, oInt)
                FillOrNothing(.IDKepemilikan, i.IDKepemilikan, True, oInt)
                FillOrNothing(.Nama, i.Nama)
            End With

            temp.Add(DestObject)
            DestObject.Dispose()
        Next
        Return temp
    End Function

    Function ConvertTo_MsKepemilikanNCBS(ByVal SourceObject As TList(Of MsKepemilikanNCBS_ApprovalDetail)) As TList(Of MsKepemilikanNCBS)
        Dim temp As New TList(Of MsKepemilikanNCBS)
        For Each i As MsKepemilikanNCBS_ApprovalDetail In SourceObject
            Dim DestObject As MsKepemilikanNCBS = DataRepository.MsKepemilikanNCBSProvider.GetByIDKepemilikanNCBS(i.IDKepemilikanNCBS)
            If DestObject Is Nothing Then
                DestObject = New MsKepemilikanNCBS
            Else
            End If

            With DestObject
                FillOrNothing(.IDKepemilikanNCBS, i.IDKepemilikanNCBS, True, oInt)
                FillOrNothing(.NamaKepemilikanNCBS, i.NamaKepemilikanNCBS, True, oString)
                FillOrNothing(.Activation, i.Activation, True, oBit)
                FillOrNothing(.CreatedBy, i.CreatedBy, True, oString)
                FillOrNothing(.CreatedDate, i.CreatedDate, True, oDate)
                FillOrNothing(.ApprovedDate, Now, True, oDate)
                FillOrNothing(.ApprovedBy, SessionPkUserId, True, oString)
            End With

            temp.Add(DestObject)
            DestObject.Dispose()
        Next

        Return temp
    End Function
    Function ConvertTo_MsKepemilikan(ByVal SourceObject As MsKepemilikan_ApprovalDetail) As MsKepemilikan
        Dim temp As New MsKepemilikan
        Dim DestObject As MsKepemilikan = DataRepository.MsKepemilikanProvider.GetByIDKepemilikan(SourceObject.IDKepemilikan)
        If DestObject Is Nothing Then
            GetTotalInsert += 1
            DestObject = New MsKepemilikan
        Else
            GetTotalUpdate += 1
        End If

        With DestObject
            FillOrNothing(.IDKepemilikan, SourceObject.IDKepemilikan, True, oInt)
            FillOrNothing(.NamaKepemilikan, SourceObject.NamaKepemilikan, True, oString)
            FillOrNothing(.Activation, SourceObject.Activation, True, oBit)
            FillOrNothing(.CreatedBy, SourceObject.CreatedBy, True, oString)
            FillOrNothing(.CreatedDate, SourceObject.CreatedDate, True, oDate)
            FillOrNothing(.ApprovedDate, Now, True, oDate)
            FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
        End With

        temp = DestObject
        DestObject.Dispose()

        Return temp
    End Function

    Private Sub ShowButonApproveReject(ByVal Show As Boolean)
        imgReject.Visible = Show
        imgApprove.Visible = Show
    End Sub

    Private Sub BindSelected()
        Dim Rows As New ArrayList

        For Each IdDIN As Int64 In SetnGetSelectedItem
            Using rowData As VList(Of Vw_MsKepemilikan_UploadApprovalDetail) = DataRepository.Vw_MsKepemilikan_UploadApprovalDetailProvider.GetPaged(" IDKepemilikan = " & IdDIN & "", "", 0, Integer.MaxValue, 0)
                If rowData.Count > 0 Then
                    Rows.Add(rowData(0))
                End If
            End Using
        Next

        GridMsUserView.DataSource = Rows
        GridMsUserView.AllowPaging = False
        GridMsUserView.DataBind()
        For i As Integer = 0 To GridMsUserView.Items.Count - 1
            For y As Integer = 0 To GridMsUserView.Columns.Count - 1
                GridMsUserView.Items(i).Cells(y).Attributes.Add("class", "text")
            Next
        Next

        HideButtonGrid()

    End Sub

    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                Dim PKMsUserId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = SetnGetSelectedItem
                If ArrTarget.Contains(PKMsUserId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKMsUserId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKMsUserId)
                End If
                SetnGetSelectedItem = ArrTarget
            End If
        Next

    End Sub

    Private Sub SettingControlSearching()
        txtNamaKepemilikan.Text = SetNamaKepemilikan

    End Sub

    Private Sub SettingPropertySearching()
        SetNamaKepemilikan = txtNamaKepemilikan.Text.Trim
    End Sub

    Private Sub ClearThisPageSessions()
        Clearproperty()
        LblMessage.Visible = False
        LblMessage.Text = ""
        SetnGetRowTotal = Nothing
        SetnGetSort = Nothing
        SetnGetCurrentPage = 0
    End Sub

    Private Sub Clearproperty()
        SetIDKepemilikan = Nothing
        SetNamaKepemilikan = Nothing
    End Sub

    Private Sub popDateControl()

    End Sub

    Private Sub BindComboBox()

    End Sub

    Private Sub bindgrid()
        SettingControlSearching()
        GridMsUserView.DataSource = SetnGetBindTable
        GridMsUserView.CurrentPageIndex = SetnGetCurrentPage
        GridMsUserView.VirtualItemCount = SetnGetRowTotal
        GridMsUserView.DataBind()
        If SetnGetRowTotal > 0 Then
            LabelNoRecordFound.Visible = False
            ShowButonApproveReject(True)
        Else
            LabelNoRecordFound.Visible = True
            ShowButonApproveReject(False)
        End If
    End Sub

#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsKepemilikan_UploadApproval_View.aspx")
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            popDateControl()
            BindComboBox()
            LblMessage.Text = ""
            LblMessage.Visible = False
            MultiViewUtama.ActiveViewIndex = 0
            If Not Page.IsPostBack Then
                ClearThisPageSessions()
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : SetnGetCurrentPage = 0
                Case "Prev" : SetnGetCurrentPage -= 1
                Case "Next" : SetnGetCurrentPage += 1
                Case "Last" : SetnGetCurrentPage = GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            CollectSelected()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSearch.Click

        Try
            SettingPropertySearching()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub
    Private Sub SetInfoNavigate()
        PageCurrentPage.Text = (SetnGetCurrentPage + 1).ToString
        PageTotalPages.Text = GetPageTotal.ToString
        PageTotalRows.Text = SetnGetRowTotal.ToString
        LinkButtonNext.Enabled = (Not SetnGetCurrentPage + 1 = GetPageTotal) AndAlso GetPageTotal <> 0
        LinkButtonLast.Enabled = (Not SetnGetCurrentPage + 1 = GetPageTotal) AndAlso GetPageTotal <> 0
        First.Enabled = Not SetnGetCurrentPage = 0
        LinkButtonPrevious.Enabled = Not SetnGetCurrentPage = 0
    End Sub

    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim TotalRow As Int16 = 0
        For Each gridRow As DataGridItem In GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                TotalRow = CType(TotalRow + 1, Int16)
            End If
        Next
        If TotalRow = 0 Then
            CheckBoxSelectAll.Checked = False
        Else
            If i = TotalRow Then
                CheckBoxSelectAll.Checked = True
            Else
                CheckBoxSelectAll.Checked = False
            End If
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            bindgrid()
            SetInfoNavigate()
            SetCheckedAll()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnClearSearch.Click
        Try
            Clearproperty()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In GridMsUserView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim pkid As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = SetnGetSelectedItem
        '        If SetnGetSelectedItem.Contains(pkid) Then
        '            If CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            Else
        '                ArrTarget.Remove(pkid)
        '            End If
        '        Else
        '            If CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            End If
        '        End If
        '        SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub


    Protected Sub GridMsUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMsUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                chkBox.Checked = SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (SetnGetCurrentPage * GetDisplayedTotalRow))
                End If

                If e.Item.Cells(GridMsUserView.Columns.Count - 5).Text = "C" Then
                    e.Item.Cells(GridMsUserView.Columns.Count - 2).Enabled = False
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub GridMsUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMsUserView.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            SetnGetSort = ChangeSortCommand(e.SortExpression)
            GridUser.Columns(IndexSort(GridUser, e.SortExpression)).SortExpression = SetnGetSort
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub HideButtonGrid()
        With GridMsUserView
            .Columns(.Columns.Count - 1).Visible = False
        End With
    End Sub

    Private Sub BindSelectedAll()
        Try
            GridMsUserView.DataSource = DataRepository.Vw_MsKepemilikan_UploadApprovalDetailProvider.GetAll
            GridMsUserView.AllowPaging = False
            GridMsUserView.DataBind()

            For i As Integer = 0 To GridMsUserView.Items.Count - 1
                For y As Integer = 0 To GridMsUserView.Columns.Count - 1
                    GridMsUserView.Items(i).Cells(y).Attributes.Add("class", "text")
                Next
            Next
            HideButtonGrid()
        Catch
            Throw
        End Try


    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                    'Skip
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click

        Try
            CollectSelected()
            Const strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelected()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=MsKepemilikanUploadApproval.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            EnableViewState = False
            Using stringWrite As System.IO.StringWriter = New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                ClearControls(GridMsUserView)
                GridMsUserView.RenderControl(htmlWrite)
                Response.Write(strStyle)
                Response.Write(stringWrite.ToString())
            End Using
            Response.End()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) AndAlso (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    'Throw New Exception("SahassaCommonly.CommonlyEnum.EXECPTION_PAGENUMBER_GREATER_TOTALPAGENUMBER")
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                'Throw New Exception("SahassaCommonly.CommonlyEnum.EXECPTION_PAGENUMBER_NUMERICPOSITIF")
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
            CollectSelected()
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub lnkExportAllData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAllData.Click
        Try
            Const strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelectedAll()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=MsKepemilikanUploadApprovalAll.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            EnableViewState = False
            Using stringWrite As System.IO.StringWriter = New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                ClearControls(GridMsUserView)
                GridMsUserView.RenderControl(htmlWrite)
                Response.Write(strStyle)
                Response.Write(stringWrite.ToString())
            End Using
            Response.End()
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub


#End Region

#Region "Essential..."

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgReject.Click
        Try
            Dim Key_ApprovalID As String = "0"
            For Each Idx As Vw_MsKepemilikan_UploadApprovalDetail In SetnGetBindTable
                '----------------------------------------------------------------------------------------------------------------------------------------------------------------
                Dim OMsKepemilikan_ApprovalDetail As MsKepemilikan_ApprovalDetail
                Dim L_MsKepemilikanNCBS_ApprovalDetail_ApprovalDetail As New TList(Of MsKepemilikanNCBS_ApprovalDetail)
                Dim L_MappingMsKepemilikanNCBSPPATK_Approval_Detail As New TList(Of MappingMsKepemilikanNCBSPPATK_Approval_Detail)
                'Get MsKepemilikan Approval detail
                OMsKepemilikan_ApprovalDetail = DataRepository.MsKepemilikan_ApprovalDetailProvider.GetPaged( _
                    MsKepemilikan_ApprovalDetailColumn.IDKepemilikan.ToString & "=" & Idx.IDKepemilikan, _
                    "", 0, Integer.MaxValue, Nothing)(0)
                'Get Key Approval
                Key_ApprovalID = OMsKepemilikan_ApprovalDetail.FK_MsKepemilikan_Approval_Id.GetValueOrDefault().ToString()
                'Get Key MsKepemilikan
                Dim MsKepemilikanID As String = OMsKepemilikan_ApprovalDetail.IDKepemilikan

                'Get List Mapping
                L_MappingMsKepemilikanNCBSPPATK_Approval_Detail = DataRepository.MappingMsKepemilikanNCBSPPATK_Approval_DetailProvider.GetPaged( _
                    MappingMsKepemilikanNCBSPPATK_Approval_DetailColumn.IDKepemilikan.ToString & "=" & MsKepemilikanID & _
                    " and " & MappingMsKepemilikanNCBSPPATK_Approval_DetailColumn.PK_MsKepemilikan_Approval_Id.ToString & "=" & Key_ApprovalID _
                    , "", 0, Integer.MaxValue, Nothing)

                For Each IdxA As MappingMsKepemilikanNCBSPPATK_Approval_Detail In L_MappingMsKepemilikanNCBSPPATK_Approval_Detail
                    Dim OMsKepemilikanNCBS_ApprovalDetail_ApprovalDetail As MsKepemilikanNCBS_ApprovalDetail
                    Dim Ltemp As TList(Of MsKepemilikanNCBS_ApprovalDetail) = DataRepository.MsKepemilikanNCBS_ApprovalDetailProvider.GetPaged( _
                        MsKepemilikanNCBS_ApprovalDetailColumn.IDKepemilikanNCBS.ToString & "=" & IdxA.IDKepemilikanNCBS.Value & " and " _
                        & MsKepemilikanNCBS_ApprovalDetailColumn.PK_MsKepemilikan_Approval_Id.ToString & "=" & Key_ApprovalID _
                        , "", 0, Integer.MaxValue, Nothing)

                    If Ltemp.Count > 0 Then
                        OMsKepemilikanNCBS_ApprovalDetail_ApprovalDetail = Ltemp(0)
                        L_MsKepemilikanNCBS_ApprovalDetail_ApprovalDetail.Add(OMsKepemilikanNCBS_ApprovalDetail_ApprovalDetail)
                    End If
                Next

                'Delete Approval Detail
                DataRepository.MsKepemilikan_ApprovalDetailProvider.Delete(OMsKepemilikan_ApprovalDetail)
                DataRepository.MsKepemilikanNCBS_ApprovalDetailProvider.Delete(L_MsKepemilikanNCBS_ApprovalDetail_ApprovalDetail)
                DataRepository.MappingMsKepemilikanNCBSPPATK_Approval_DetailProvider.Delete(L_MappingMsKepemilikanNCBSPPATK_Approval_Detail)
            Next

            Using app As MsKepemilikan_Approval = DataRepository.MsKepemilikan_ApprovalProvider.GetByPK_MsKepemilikan_Approval_Id(CInt(Key_ApprovalID))
                If Not IsNothing(app) Then
                    DataRepository.MsKepemilikan_ApprovalProvider.Delete(app)
                End If
            End Using

            
            

            Using app As MsKepemilikan_Approval = DataRepository.MsKepemilikan_ApprovalProvider.GetByPK_MsKepemilikan_Approval_Id(CInt(Key_ApprovalID))
                If Not IsNothing(app) Then
                    Using objMsUser As User = DataRepository.UserProvider.GetByPkUserID(app.RequestedBy)
                        If Not IsNothing(objMsUser) Then
                            Session("MsKepemilikan_ApprovalDetail_View.ApproverUser") = objMsUser.UserEmailAddress & ";"
                        End If
                    End Using
                End If
            End Using

            ShowButonApproveReject(False)
            Me.LblMessage.Text = "Rejected Success "
            Me.LblMessage.Visible = True
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub btnConfirmed_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgApprove.Click
        Try
            Dim Key_ApprovalID As String = ""
            For Each Idx As Vw_MsKepemilikan_UploadApprovalDetail In SetnGetBindTable
                '================================ Deklaration ===================================================
                Dim OMsKepemilikan As MsKepemilikan
                Dim L_MsKepemilikanNCBS As New TList(Of MsKepemilikanNCBS)
                Dim L_MappingMsKepemilikanNCBSPPATK As New TList(Of MappingMsKepemilikanNCBSPPATK)
                '----------------------------------------------------------------------------------------------------------------------------------------------------------------
                Dim OMsKepemilikan_ApprovalDetail As MsKepemilikan_ApprovalDetail
                Dim L_MsKepemilikanNCBS_ApprovalDetail As New TList(Of MsKepemilikanNCBS_ApprovalDetail)
                Dim L_MappingMsKepemilikanNCBSPPATK_Approval_Detail As New TList(Of MappingMsKepemilikanNCBSPPATK_Approval_Detail)
                'Get MsKepemilikan Approval detail
                OMsKepemilikan_ApprovalDetail = DataRepository.MsKepemilikan_ApprovalDetailProvider.GetPaged( _
                    MsKepemilikan_ApprovalDetailColumn.IDKepemilikan.ToString & "=" & Idx.IDKepemilikan, _
                    "", 0, Integer.MaxValue, Nothing)(0)
                'Get Key Approval
                Key_ApprovalID = OMsKepemilikan_ApprovalDetail.FK_MsKepemilikan_Approval_Id.GetValueOrDefault().ToString()
                'Get Key MsKepemilikan
                Dim MsKepemilikanID As String = OMsKepemilikan_ApprovalDetail.IDKepemilikan

                'Get List Mapping
                L_MappingMsKepemilikanNCBSPPATK_Approval_Detail = DataRepository.MappingMsKepemilikanNCBSPPATK_Approval_DetailProvider.GetPaged( _
                    MappingMsKepemilikanNCBSPPATK_Approval_DetailColumn.IDKepemilikan.ToString & "=" & MsKepemilikanID & _
                    " and " & MappingMsKepemilikanNCBSPPATK_Approval_DetailColumn.PK_MsKepemilikan_Approval_Id.ToString & "=" & Key_ApprovalID _
                    , "", 0, Integer.MaxValue, Nothing)

                For Each IdxA As MappingMsKepemilikanNCBSPPATK_Approval_Detail In L_MappingMsKepemilikanNCBSPPATK_Approval_Detail
                    Dim OMsKepemilikanNCBS_ApprovalDetail As MsKepemilikanNCBS_ApprovalDetail
                    Dim Ltemp As TList(Of MsKepemilikanNCBS_ApprovalDetail) = DataRepository.MsKepemilikanNCBS_ApprovalDetailProvider.GetPaged( _
                        MsKepemilikanNCBS_ApprovalDetailColumn.IDKepemilikanNCBS.ToString & "=" & IdxA.IDKepemilikanNCBS.Value & " and " _
                        & MsKepemilikanNCBS_ApprovalDetailColumn.PK_MsKepemilikan_Approval_Id.ToString & "=" & Key_ApprovalID _
                        , "", 0, Integer.MaxValue, Nothing)

                    If Ltemp.Count > 0 Then
                        OMsKepemilikanNCBS_ApprovalDetail = Ltemp(0)
                        L_MsKepemilikanNCBS_ApprovalDetail.Add(OMsKepemilikanNCBS_ApprovalDetail)
                    End If
                Next

                'Convert approval ke object sebenarnya 
                OMsKepemilikan = ConvertTo_MsKepemilikan(OMsKepemilikan_ApprovalDetail)
                L_MsKepemilikanNCBS = ConvertTo_MsKepemilikanNCBS(L_MsKepemilikanNCBS_ApprovalDetail)
                L_MappingMsKepemilikanNCBSPPATK = ConvertTo_MappingMsKepemilikanNCBSPPATKl(L_MappingMsKepemilikanNCBSPPATK_Approval_Detail)
                '======================= Save MsKepemilikan =========================================================================
                DataRepository.MsKepemilikanProvider.Save(OMsKepemilikan)
                DataRepository.MsKepemilikanNCBSProvider.Save(L_MsKepemilikanNCBS)
                Dim L_mappingDelete As TList(Of MappingMsKepemilikanNCBSPPATK) = DataRepository.MappingMsKepemilikanNCBSPPATKProvider.GetPaged( _
                        MappingMsKepemilikanNCBSPPATKColumn.IDKepemilikan.ToString & "= " & OMsKepemilikan.IDKepemilikan, "", 0, Integer.MaxValue, Nothing)
                DataRepository.MappingMsKepemilikanNCBSPPATKProvider.Delete(L_mappingDelete)
                DataRepository.MappingMsKepemilikanNCBSPPATKProvider.Save(L_MappingMsKepemilikanNCBSPPATK)
                'Delete Approval Detail
                DataRepository.MsKepemilikan_ApprovalDetailProvider.Delete(OMsKepemilikan_ApprovalDetail)
                DataRepository.MsKepemilikanNCBS_ApprovalDetailProvider.Delete(L_MsKepemilikanNCBS_ApprovalDetail)
                DataRepository.MappingMsKepemilikanNCBSPPATK_Approval_DetailProvider.Delete(L_MappingMsKepemilikanNCBSPPATK_Approval_Detail)
            Next

            Using app As MsKepemilikan_Approval = DataRepository.MsKepemilikan_ApprovalProvider.GetByPK_MsKepemilikan_Approval_Id(CInt(Key_ApprovalID))
                If Not IsNothing(app) Then
                    DataRepository.MsKepemilikan_ApprovalProvider.Delete(app)
                End If
            End Using

            Dim ket As String = ""
            If GetTotalInsert > 0 Then
                ket = "(Insert :" & GetTotalInsert & "; Update:" & GetTotalUpdate & "  )"
            End If
            Me.LblMessage.Text = "Confirmed Success " & ket
            Me.LblMessage.Visible = True
            ShowButonApproveReject(False)
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region
End Class



