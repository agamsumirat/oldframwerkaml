﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="showwatchlist.aspx.vb" Inherits="showwatchlist" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Theme/aml.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <%--<strong>Data Worldcheck</strong> --%>
        <table class="auto-style1">
            <tr class="formText">
                <td style="width: 30%">UID</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData0" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">LAST NAME</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData1" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">FIRST NAME</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData2" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">ALIASES</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData3" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">LOW QUALITY ALIASES</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData4" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">ALTERNATIVE SPELLING</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData5" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">CATEGORY</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData6" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">TITLE</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData7" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">SUB-CATEGORY</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData8" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">POSITION</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData9" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">AGE</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData10" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">DOB</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData11" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">PLACE OF BIRTH</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData12" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">DECEASED</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData13" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">PASSPORTS</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData14" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">SSN</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData15" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">LOCATIONS</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData16" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">COUNTRIES</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData17" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">COMPANIES</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData18" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">E I</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData19" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">LINKED TO</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData20" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">FURTHER INFORMATION</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData21" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">KEYWORDS</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData22" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">EXTERNAL SOURCES</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData23" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">UPDATE CATEGORY</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData24" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">ENTERED</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData25" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">UPDATED</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData26" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">EDITOR</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData27" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">AGE DATE (AS OF DATE)</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData28" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">World Check Keyword Category</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="LblData29" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">&nbsp;</td>
                <td style="width: 10px">&nbsp;</td>
                <td style="width: 70%">&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 30%"><asp:imagebutton id="ImageSearch" runat="server" CausesValidation="True" ImageUrl="~/Images/button/ok.gif" OnClientClick="window.close();"></asp:imagebutton></td>
                <td style="width: 10px">&nbsp;</td>
                <td style="width: 70%">&nbsp;</td>
            </tr>
            </table>
   
    </div>
    </form>
</body>
</html>
