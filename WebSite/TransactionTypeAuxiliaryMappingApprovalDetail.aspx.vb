
Partial Class TransactionTypeAuxiliaryMappingApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param pk Group management
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamPkPendingApprovalID() As Int64
        Get
            Return Me.Request.Params("PkApprovalId")
        End Get
    End Property


#End Region

    Private Function CheckDataExists() As Boolean
        Dim AuxTransCode As String = Session("TransactionTypeAuxiliaryMappingApprovalDetailAuxTrans")
        Using CekData As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.QueriesTableAdapter
            Dim count As Int32 = CekData.CekTransactionTypeAuxiliaryTransactionCodeMapping(AuxTransCode)

            If count = 0 Then
                Return False
            Else
                Return True
            End If
        End Using
    End Function

    Private Sub FillData()
        Try
            Dim PkApproval As Int64 = Me.ParamPkPendingApprovalID
            Dim ApprovalModeId As Int16 = Me.ParamType
            Using AccessApproval As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.SelectApprovalTransactionTypeAuxiliaryMappingTableAdapter
                Using AccessTable As AMLDAL.TransactionTypeAuxiliaryMapping.SelectApprovalTransactionTypeAuxiliaryMappingDataTable = AccessApproval.SelectApprovalTransactionTypeAuxiliaryMapping(PkApproval)
                    Dim AccessRow As AMLDAL.TransactionTypeAuxiliaryMapping.SelectApprovalTransactionTypeAuxiliaryMappingRow = AccessTable.Rows(0)

                    Select Case ApprovalModeId
                        Case 2
                            Me.MultiView1.ActiveViewIndex = 0

                            Me.TxtAuxTransNew.Text = AccessRow.AuxiliaryTransactionCode & "-" & AccessRow.TransactionDescription
                            Me.TxtAuxTransOld.Text = AccessRow.AuxiliaryTransactionCode_Old & "-" & AccessRow.TransactionDescription_Old

                            If AccessRow.IsTransactionTypeNameNull Then
                                Me.TxtTransTypeNew.Text = AccessRow.TransactionTypeId
                            Else
                                Me.TxtTransTypeNew.Text = AccessRow.TransactionTypeId & "-" & AccessRow.TransactionTypeName
                            End If

                            Me.TxtTransTypeOld.Text = AccessRow.TransactionTypeId_Old
                        Case 3
                            Me.MultiView1.ActiveViewIndex = 1

                            Me.TxtAuxTranDel.Text = AccessRow.AuxiliaryTransactionCode & "-" & AccessRow.TransactionDescription

                            If AccessRow.IsTransactionTypeNameNull Then
                                Me.TxtTransTypeDel.Text = AccessRow.TransactionTypeId
                            Else
                                Me.TxtTransTypeDel.Text = AccessRow.TransactionTypeId & "-" & AccessRow.TransactionTypeName
                            End If
                    End Select

                    Session("TransactionTypeAuxiliaryMappingApprovalDetailAuxTrans") = AccessRow.AuxiliaryTransactionCode
                    Session("TransactionTypeAuxiliaryMappingApprovalDetailAuxTransOld") = AccessRow.AuxiliaryTransactionCode_Old
                    Session("TransactionTypeAuxiliaryMappingApprovalDetailAuxTransType") = AccessRow.TransactionTypeId
                    Session("TransactionTypeAuxiliaryMappingApprovalDetailAuxTransTypeOld") = AccessRow.TransactionTypeId_Old
                End Using
            End Using

            Select Case ApprovalModeId
                Case 2

                    Using AccessApproval As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.SelectTransactionTypeByIdTableAdapter
                        Using AccessTable As AMLDAL.TransactionTypeAuxiliaryMapping.SelectTransactionTypeByIdDataTable = AccessApproval.SelectTransactionTypeById(Me.TxtTransTypeOld.Text)
                            If AccessTable.Count > 0 Then
                                Dim AccessRow As AMLDAL.TransactionTypeAuxiliaryMapping.SelectTransactionTypeByIdRow = AccessTable.Rows(0)
                                If AccessRow.IsTransactionTypeNameNull Then
                                    Me.TxtTransTypeOld.Text = Me.TxtTransTypeOld.Text
                                Else
                                    Me.TxtTransTypeOld.Text = Me.TxtTransTypeOld.Text & "-" & AccessRow.TransactionTypeName
                                End If

                            End If
                        End Using
                    End Using
                    If Me.TxtAuxTransOld.Text = "0" Then Me.TxtAuxTransOld.Text = Nothing
                    If Me.TxtTransTypeOld.Text = "0" Then Me.TxtTransTypeOld.Text = Nothing
            End Select


        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                'UNDONE:Dikomentari karena baru ditambahin
                Me.ImgBtnAcceptDelete.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImgBtnAcceptEdit.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImgBtnRejectDelete.Attributes.Add("onclick", "javascript: if(confirm('Do you want to Reject this activity ?')== false) return false;")
                Me.ImgBtnRejectEdit.Attributes.Add("onclick", "javascript: if(confirm('Do you want to Reject this activity ?')== false) return false;")

                Me.FillData()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                    'End Using
                End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnBackDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnBackDelete.Click, ImgBtnBackEdit.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "TransactionTypeAuxiliaryMappingApproval.aspx"
        MagicAjax.AjaxCallHelper.Redirect("TransactionTypeAuxiliaryMappingApproval.aspx")
    End Sub

    Private Sub AcceptApproval()
        Try
            Select Case CType(Me.ParamType, Int16)
                Case 2
                    Me.EditAccepted()
                Case 3
                    Me.DeleteAccepted()
            End Select
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub

    Private Sub RejectApproval()
        Try
            Select Case CType(Me.ParamType, Int16)
                Case 2
                    Me.EditRejected()
                Case 3
                    Me.DeleteRejected()
            End Select
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub

    Private Function EditAccepted() As Boolean
        Try
            Using AuxiliaryTrans As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.UpdateTransactionTypeAuxiliaryTransactionCodeMappingTableAdapter
                Dim PkApproval As Int64 = Me.ParamPkPendingApprovalID
                Dim TransIdOld As String = Session("TransactionTypeAuxiliaryMappingApprovalDetailAuxTransTypeOld")
                Dim TransId As Integer = Session("TransactionTypeAuxiliaryMappingApprovalDetailAuxTransType")
                Dim AuxTransCode As String = Session("TransactionTypeAuxiliaryMappingApprovalDetailAuxTrans")

                If Me.CheckDataExists = True Then
                    AuxiliaryTrans.UpdateTransactionTypeAuxiliaryTransactionCodeMapping(TransId, AuxTransCode)

                    Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "TransactionTypeAuxiliaryMappingEdit", "TransactionTypeId", "Edit", TransIdOld, TransId, "Accepted")
                        End Using
                    End Using
                Else
                    AuxiliaryTrans.InsertTransactionTypeAuxiliaryTransactionCodeMapping(TransId, AuxTransCode)

                    Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "TransactionTypeAuxiliaryMappingEdit", "TransactionTypeId", "Add", Nothing, TransId, "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "TransactionTypeAuxiliaryMappingEdit", "AuxiliaryTransactionCode", "Add", Nothing, AuxTransCode, "Accepted")
                        End Using
                    End Using
                End If
                AuxiliaryTrans.DeleteTransactionTypeAuxiliaryTransactionCodeMapping_Approval(PkApproval)
                AuxiliaryTrans.DeleteTransactionTypeAuxiliaryTransactionCodeMapping_ApprovalDetail(PkApproval)

            End Using

            Return True
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Private Function EditRejected() As Boolean
        Try
            Using AuxiliaryTrans As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.UpdateTransactionTypeAuxiliaryTransactionCodeMappingTableAdapter
                Dim PkApproval As Int64 = Me.ParamPkPendingApprovalID
                Dim TransIdOld As String = Session("TransactionTypeAuxiliaryMappingApprovalDetailAuxTransTypeOld")
                Dim TransId As Integer = Session("TransactionTypeAuxiliaryMappingApprovalDetailAuxTransType")
                Dim AuxTransCode As String = Session("TransactionTypeAuxiliaryMappingApprovalDetailAuxTrans")

                If Me.CheckDataExists = True Then
                    Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "TransactionTypeAuxiliaryMappingEdit", "TransactionTypeId", "Edit", TransIdOld, TransId, "Accepted")
                        End Using
                    End Using
                Else
                    Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "TransactionTypeAuxiliaryMappingEdit", "TransactionTypeId", "Add", Nothing, TransId, "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "TransactionTypeAuxiliaryMappingEdit", "AuxiliaryTransactionCode", "Add", Nothing, AuxTransCode, "Accepted")
                        End Using
                    End Using
                End If
                AuxiliaryTrans.DeleteTransactionTypeAuxiliaryTransactionCodeMapping_Approval(PkApproval)
                AuxiliaryTrans.DeleteTransactionTypeAuxiliaryTransactionCodeMapping_ApprovalDetail(PkApproval)

            End Using

            Return True
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Private Function DeleteAccepted() As Boolean
        Try
            Using AuxiliaryTrans As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.UpdateTransactionTypeAuxiliaryTransactionCodeMappingTableAdapter
                Dim PkApproval As Int64 = Me.ParamPkPendingApprovalID
                Dim TransIdOld As String = Session("TransactionTypeAuxiliaryMappingApprovalDetailAuxTransTypeOld")
                Dim AuxTransCode As String = Session("TransactionTypeAuxiliaryMappingApprovalDetailAuxTrans")

                AuxiliaryTrans.DeleteTransactionTypeAuxiliaryTransactionCodeMapping(AuxTransCode)

                AuxiliaryTrans.DeleteTransactionTypeAuxiliaryTransactionCodeMapping_Approval(PkApproval)
                AuxiliaryTrans.DeleteTransactionTypeAuxiliaryTransactionCodeMapping_ApprovalDetail(PkApproval)

                Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "TransactionTypeAuxiliaryMappingEdit", "TransactionTypeId", "Delete", TransIdOld, Nothing, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "TransactionTypeAuxiliaryMappingEdit", "AuxiliaryTransactionCode", "Delete", AuxTransCode, Nothing, "Accepted")
                    End Using
                End Using

            End Using

            Return True
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Private Function DeleteRejected() As Boolean
        Try
            Using AuxiliaryTrans As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.UpdateTransactionTypeAuxiliaryTransactionCodeMappingTableAdapter
                Dim PkApproval As Int64 = Me.ParamPkPendingApprovalID
                Dim TransIdOld As String = Session("TransactionTypeAuxiliaryMappingApprovalDetailAuxTransTypeOld")
                Dim AuxTransCode As String = Session("TransactionTypeAuxiliaryMappingApprovalDetailAuxTrans")

                AuxiliaryTrans.DeleteTransactionTypeAuxiliaryTransactionCodeMapping_Approval(PkApproval)
                AuxiliaryTrans.DeleteTransactionTypeAuxiliaryTransactionCodeMapping_ApprovalDetail(PkApproval)

                Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "TransactionTypeAuxiliaryMappingEdit", "TransactionTypeId", "Delete", TransIdOld, Nothing, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "TransactionTypeAuxiliaryMappingEdit", "AuxiliaryTransactionCode", "Delete", AuxTransCode, Nothing, "Accepted")
                    End Using
                End Using

            End Using

            Return True
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Protected Sub ImgBtnAcceptEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAcceptEdit.Click, ImgBtnAcceptDelete.Click
        Try

            Me.AcceptApproval()

            Me.Response.Redirect("TransactionTypeAuxiliaryMappingApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnRejectEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnRejectEdit.Click, ImgBtnRejectDelete.Click
        Try

            Me.RejectApproval()

            Me.Response.Redirect("TransactionTypeAuxiliaryMappingApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
