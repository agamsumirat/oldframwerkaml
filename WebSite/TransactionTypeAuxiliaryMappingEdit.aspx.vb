Imports System.Data
Imports System.Data.SqlClient

Partial Class TransactionTypeAuxiliaryMappingEdit
    Inherits Parent

    Private ReadOnly Property ParamAuxTransCode() As String
        Get
            Return Me.Request.Params("AuxTransCode")
        End Get
    End Property

    Private ReadOnly Property ParamAuxTransCodeDesc() As String
        Get
            Return Me.Request.Params("AuxTransCodeDesc")
        End Get
    End Property

    Private ReadOnly Property ParamTransactionTypeId() As String
        Get
            Return Me.Request.Params("TransactionTypeId")
        End Get
    End Property

    Private Sub FillComboTransactionType()
        Try
            Dim PkTransType As String = Me.ParamTransactionTypeId
            Using AccessTrans As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.SelectTransactionTypeTableAdapter
                Me.CboTransType.Items.Clear()
                Me.CboTransType.DataSource = AccessTrans.GetTransactionType
                Me.CboTransType.DataValueField = "TransactionTypeId"
                Me.CboTransType.DataTextField = "TransactionTypeDesc"
                Me.CboTransType.DataBind()
                Me.CboTransType.Items.Insert(0, New ListItem("[-]", "0"))
                Me.CboTransType.SelectedIndex = 0

                Dim y As Integer = 0
                For x As Integer = 0 To Me.CboTransType.Items.Count - 1
                    Me.CboTransType.SelectedIndex = x

                    If Me.CboTransType.SelectedValue = PkTransType Then
                        y = x
                    End If
                Next
                Me.CboTransType.SelectedIndex = y
            End Using
        Catch
            Throw
        End Try
    End Sub

    Private Function CheckDataExists() As Boolean
        Dim AuxTransCode As String = Me.ParamAuxTransCode
        Using CekData As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.QueriesTableAdapter
            Dim count As Int32 = CekData.CekTransactionTypeAuxiliaryTransactionCodeMapping(AuxTransCode)

            If count = 0 Then
                Return False
            Else
                Return True
            End If
        End Using
    End Function

    Public Function CheckDataPendingApprovalStatus() As Boolean
        Dim AuxTransCode As String = Me.ParamAuxTransCode
        Using ApprovalTableAdapter As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.QueriesTableAdapter
            Dim count As Int32 = ApprovalTableAdapter.CekStatusTransactionTypeAuxiliaryTransactionCodeMapping_PendingApproval(AuxTransCode)

            If count = 0 Then
                Return False
            Else
                Return True
            End If
        End Using
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Me.FillComboTransactionType()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
                Me.LblTransType.Text = Me.ParamAuxTransCode & " - " & Me.ParamAuxTransCodeDesc
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButtonCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "TransactionTypeAuxiliaryMapping.aspx"
        Page.Response.Redirect("TransactionTypeAuxiliaryMapping.aspx", False)
    End Sub

    Protected Sub ImageButtonSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSave.Click
        Try

            If Me.CheckDataPendingApprovalStatus = True Then
                Throw New Exception("Cannot edit the following data: '" & Me.LblTransType.Text & "' because it is currently waiting for approval.")
            End If
            If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                If EditBySu() Then
                    Sahassa.AML.Commonly.SessionIntendedPage = "TransactionTypeAuxiliaryMapping.aspx"
                    Response.Redirect("TransactionTypeAuxiliaryMapping.aspx", False)
                End If
            Else
                If EditData() Then
                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=81305", False)
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Function EditBySu() As Boolean
        Try
            Using AuxiliaryTrans As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.UpdateTransactionTypeAuxiliaryTransactionCodeMappingTableAdapter
                Dim TransIdOld As String = Me.ParamTransactionTypeId
                Dim TransId As Integer = Me.CboTransType.SelectedValue
                Dim AuxTransCode As String = Me.ParamAuxTransCode

                If Me.CheckDataExists = True Then
                    AuxiliaryTrans.UpdateTransactionTypeAuxiliaryTransactionCodeMapping(TransId, AuxTransCode)

                    Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "TransactionTypeAuxiliaryMappingEdit", "TransactionTypeId", "Edit", TransIdOld, TransId, "Accepted")
                        End Using
                    End Using
                Else
                    AuxiliaryTrans.InsertTransactionTypeAuxiliaryTransactionCodeMapping(TransId, AuxTransCode)

                    Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "TransactionTypeAuxiliaryMappingEdit", "TransactionTypeId", "Add", Nothing, TransId, "Accepted")
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "TransactionTypeAuxiliaryMappingEdit", "AuxiliaryTransactionCode", "Add", Nothing, AuxTransCode, "Accepted")
                        End Using
                    End Using
                End If

            End Using

            Return True
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Private Function EditData() As Boolean
        Dim ApprovalMasterId As Int64

        Try
            Dim TransIdOld As String = Me.ParamTransactionTypeId
            Dim TransId As Integer = Me.CboTransType.SelectedValue
            Dim AuxTransCode As String = Me.ParamAuxTransCode
            Dim CreatedDate As DateTime = DateTime.Now
            Dim CreatedBy As String = Sahassa.AML.Commonly.SessionUserId

            If Not IsNumeric(TransIdOld) Then TransIdOld = Nothing

            Using ApprovalTableAdapter As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.UpdateTransactionTypeAuxiliaryTransactionCodeMappingTableAdapter

                ApprovalMasterId = ApprovalTableAdapter.InsertTransactionTypeAuxiliaryTransactionCodeMapping_Approval(CreatedDate, CreatedBy, 2)

                ApprovalTableAdapter.InsertTransactionTypeAuxiliaryTransactionCodeMapping_PendingApproval(ApprovalMasterId, TransId, AuxTransCode, TransIdOld, AuxTransCode)

                Return True
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
End Class
