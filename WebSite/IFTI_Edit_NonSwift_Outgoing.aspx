
<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" 
CodeFile="IFTI_Edit_NonSwift_Outgoing.aspx.vb" Inherits="IFTI_Edit_NonSwift_Outgoing"  %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
 <script src="Script/popcalendar.js"></script>
 <script language="javascript" type="text/javascript">
     function hidePanel(objhide, objpanel, imgmin, imgmax) {
         document.getElementById(objhide).style.display = 'none';
         document.getElementById(objpanel).src = imgmax;
     }
     // JScript File


     function popWinNegara() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerNegara.aspx", "#3", winSetting);
     }

     function popWinProvinsi() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerProvinsi.aspx", "#5", winSetting);
     }
     function popWinPekerjaan() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerPekerjaan.aspx", "#6", winSetting);
     }
     function popWinKotaKab() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerKotaKab.aspx", "#7", winSetting);
     }
     function popWinBidangUsaha() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerBidangUsaha.aspx", "#8", winSetting);
     }
     function popWinMataUang() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerMataUang.aspx", "#4", winSetting);

     }

	</script>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td style="height: 20px">
                            </td>
                        <td width="99%" bgcolor="#FFFFFF" style="height: 20px">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td bgcolor="#FFFFFF" style="height: 20px">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="10" height="100%" /></td>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="a" runat="server">
                                    </asp:ValidationSummary>
                                </ajax:AjaxPanel>
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <asp:MultiView ID="MultiViewEditNonSwiftOut" runat="server" ActiveViewIndex="0">
                                    <asp:View ID="ViewNonSwiftOut" runat="server">
                                         <table id="Tabley4" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <img src="Images/dot_title.gif" width="17" height="17" />
                                                <strong>
                                                    <asp:Label ID="Label51" runat="server" 
                                                    Text="IFTI  Non Swift Outgoing - Edit "></asp:Label>
                                                </strong>
                                                <hr />
                                                </td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" align="left"
                                                style="height: 6px; width: 100%;">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Label153" runat="server" Text="A. Umum" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('NonSwiftOutUmum','Img14','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Img14" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="NonSwiftOutUmum">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                             <tr>
                                                <td style="width: 257px; background-color: #fff7e6; height: 22px;">
                                                    <asp:Label ID="Label185" runat="server" Text="a. No. LTDLN "></asp:Label></td>
                                                <td style="height: 22px">
                                                    <asp:TextBox ID="TxtUmum_NonSwiftOutLTDLN" runat="server" CssClass="searcheditbox" MaxLength="30"
                                                        TabIndex="2" Width="125px"></asp:TextBox></td>                                                                                            
                                             </tr>
                                              <tr>
                                                <td style="width: 257px; background-color: #fff7e6">
                                                    <asp:Label ID="Label188" runat="server" Text="b. No. LTDLN Koreksi " Width="132px"></asp:Label></td>
                                                <td>
                                                    <asp:TextBox ID="TxtUmum_NonSwiftOutLTDLNKoreksi" runat="server" CssClass="searcheditbox" MaxLength="30"
                                                        TabIndex="2" Width="125px"></asp:TextBox></td>                                                                                            
                                             </tr>
                                              <tr>
                                                <td style="width: 257px; background-color: #fff7e6">
                                                    <asp:Label ID="Labele199" runat="server" Text="c. Tanggal Laporan "></asp:Label><asp:Label
                                                        ID="Label222" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                <td>
                                                    <asp:TextBox ID="TxtUmum_NonSwiftOutTanggalLaporan" runat="server" CssClass="searcheditbox"
                                                        TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                    <input id="PopUpUmum_NonSwiftOut_TanggalLaporan" runat="server"
                                                            onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')" style="border-right: #ffffff 0px solid;
                                                            border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                            border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                            height: 17px" title="Click to show calendar" type="button" /></td>                                                                                            
                                             </tr>
                                              <tr>
                                                <td style="width: 257px; background-color: #fff7e6">
                                                    <asp:Label ID="Label241" runat="server" Text="d. Nama PJK Bank Pelapor " Width="162px"></asp:Label>
                                                    <asp:Label ID="Label242" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                <td>
                                                    <asp:TextBox ID="TxtUmum_NonSwiftOutPJKBankPelapor" runat="server" 
                                                        CssClass="searcheditbox" MaxLength="100" TabIndex="2" Width="125px"></asp:TextBox>
                                                  </td>                                                                                            
                                             </tr>
                                              <tr>
                                                <td style="width: 257px; background-color: #fff7e6; height: 22px;">
                                                    <asp:Label ID="Label255" runat="server" Text="e. Nama Pejabat PJK Bank Pelapor" Width="204px"></asp:Label>
                                                    <asp:Label ID="Label271" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                <td style="height: 22px">
                                                    <asp:TextBox ID="TxtUmum_NonSwiftOutNamaPejabatPJKBankPelapor" runat="server" CssClass="searcheditbox" MaxLength="100"
                                                        TabIndex="2" Width="125px"></asp:TextBox></td>                                                                                            
                                             </tr>
                                              <tr>
                                                <td style="width: 257px; background-color: #fff7e6">
                                                    <asp:Label ID="Label272" runat="server" Text="f. Jenis Laporan"></asp:Label>
                                                    <asp:Label ID="Label282" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                <td>
                                                    <asp:RadioButtonList ID="Rb_Umum_NonSwiftOut_jenislaporan" runat="server" 
                                                        RepeatColumns="2" RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="1">Baru</asp:ListItem>
                                                        <asp:ListItem Value="2">Recall</asp:ListItem>
                                                        <asp:ListItem Value="3">Koreksi</asp:ListItem>
                                                        <asp:ListItem Value="4">Reject</asp:ListItem>
                                                    </asp:RadioButtonList></td>                                                                                            
                                             </tr>
                                              <tr>
                                                <td style="width: 257px">
                                                </td>
                                                <td>
                                                </td>                                                                                            
                                             </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%">
                                           <tr>
                                                <td>
                                                     <table id= "Table5" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" align="left"
                                                style="height: 11px; width: 100%;">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Label285" runat="server" Text="B.  Identitas Pengirim" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('B1NonSwiftOutidentitasPENGIRIM','Imgg1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Imgg1" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                                </td>
                                        </tr>
                                        <tr id="B1NonSwiftOutidentitasPENGIRIM">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;">
                                                <table style="width: 100%">
                                                    <tr id="trNonSwiftOutPengirimNas_TipePengirim" runat="server" visible="false">
                                                        <td style="width: 25%">
                                                        </td>
                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                            <asp:Label ID="Label286" runat="server" Text="       Tipe Pengirim "></asp:Label></td>
                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                            <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                                                <asp:RadioButtonList ID="Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" Width="189px" Enabled="False">
                                                                    <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                    <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                        <td style="width: 25%">
                                                        </td>
                                                    </tr>
                                                    <tr id="trNonSwiftOutTipeNasabah" runat="server" visible="false">
                                                        <td style="width: 25%">
                                                        </td>
                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                            <asp:Label ID="Label287" runat="server" Text="Tipe Nasabah "></asp:Label></td>
                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                            <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                                                <asp:RadioButtonList ID="Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                                    <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                    <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                        <td style="width: 25%">
                                                        </td>
                                                    </tr>
                                                </table>
												<ajax:AjaxPanel ID="AjaxPanel13" runat="server" Width="100%">
                                                     <asp:MultiView ID="MultiViewNonSwiftOutJenisNasabah" runat="server">
                                                            <asp:View ID="ViewNonSwiftOutPengirimNasabah" runat="server">
                                                             <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2" id="TABLENonSwiftOutNasabah" >
                                         <tr>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext" style="height: 14px">
                                                            <asp:Label ID="Label288" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td style="height: 14px">
                                                            <a href="#" onclick="javascript:ShowHidePanel('B1NonSwiftOutidentitasPENGIRIM1nasabah','Imgg2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Imgg2" src="Images/search-bar-minimize.gif" border="0" height="12"
                                                                    width="12" /></a>
                                                        </td>
                                                    </tr>
                                       </table>
                                            </td>
                                        </tr>
                                        <tr id="B1NonSwiftOutidentitasPENGIRIM1nasabah">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <tr>
                                                    <td style="width: 100%">
                                                        <table>
                                                            <tr>
                                                                <td style="width: 59%; background-color: #fff7e6">
                                                                    <asp:Label ID="Label289" runat="server" Text="No. Rek " Width="84px"></asp:Label>
                                                                    <asp:Label ID="Label290" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                 <td style="width: 124px">
                                                                     <asp:TextBox ID="TxtIdenNonSwiftOutPengirim_Norekening" runat="server" 
                                                                         CssClass="searcheditbox" MaxLength="50"
                                                                         TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                 <td style="width: 53px">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    
                                                </tr>
                                                 <tr>
                                                    <td style="width: 610px">
                                                        <asp:MultiView ID="MultiViewNonSwiftOutPengirimNasabah" runat="server" 
                                                            ActiveViewIndex="0">
                                                       
                                                            <asp:View ID="VwNonSwiftOut_PNG_Ind" runat="server">
                                                             <ajax:AjaxPanel ID="AjaxPanel11NonSwiftOut" runat="server">
                                                                   <table style="width: 98%; height: 49px">
                                                                        <tr>
                                                                            <td style="width: 50%; height: 15px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label291" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; width: 222px;">
                                                                            </td>
                                                                            <td style="height: 15px; width: 35px;">
                                                                            </td>
                                                                            <td style="width: 47px; height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label293" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                <asp:Label ID="Label294" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_NamaLengkap" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="255"
                                                                                    TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                            <td style="width: 35px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label295" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label>
                                                                                <asp:Label ID="Label296" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_TanggalLahir" runat="server" 
                                                                                    CssClass="searcheditbox" TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                <input id="popUpTanggalLahirNonSwiftOutPengirimNasabah_Ind" runat="server"
                                                            onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')" style="border-right: #ffffff 0px solid;
                                                            border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                            border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                            height: 17px" title="Click to show calendar" type="button" /></td>
                                                                            <td style="width: 35px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                                </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label297" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                    Width="223px"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:RadioButtonList ID="RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan" 
                                                                                    runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                </asp:RadioButtonList></td>
                                                                            <td style="width: 35px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="NegaraNonSwift_pengirimPerorangan" runat="server" visible="false">
                                                                            <td style="width: 50%; background-color: #fff7e6"  >
                                                                                <asp:Label ID="Label298" runat="server" Text=" Negara "></asp:Label>
                                                                                <br />
                                                                                <asp:Label ID="Label299" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                    Width="258px"></asp:Label></td>
                                                                            <td style="width: 222px" >
                                                                                &nbsp;
                                                                                <table style="width: 127px">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                                                &nbsp;<asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_Negara" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <td style="width: 68px">
                                                                                <asp:ImageButton ID="ImageButton_IdenNonSwiftOutPengirimNas_Ind_Negara" 
                                                                                    runat="server" ImageUrl="~/Images/button/browse.gif" 
                                                                                    OnClientClick="javascript:popWinNegara();" /></td>
                                                                                            <td style="width: 68px">
                                                                                                <asp:ImageButton ID="RMIdenNonSwiftOutPengirimNas_Ind_Negara" runat="server" ImageUrl="~/Images/button/remove.gif"
                                                                                                    OnClientClick="javascript:popWinNegara();" /></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <asp:HiddenField ID="hfIdenNonSwiftOutPengirimNas_Ind_Negara" runat="server" />
                                                                            </td>
                                                                            <td style="width: 35px" >
                                                                                </td>
                                                                            <td style="width: 47px" >
                                                                            </td>
                                                                        </tr>
                                                                       <tr  id="NegaraLainNonSwift_pengirimPerorangan" runat="server" visible="false">
                                                                           <td style="width: 50%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label300" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px">
                                                                               <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_NegaraLain" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                           <td style="width: 35px">
                                                                           </td>
                                                                           <td style="width: 47px" >
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label301" runat="server" Text="d. Pekerjaan"></asp:Label>
                                                                                <asp:Label ID="Label302" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                &nbsp;
                                                                                <table style="width: 127px">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                                <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaan" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;</td>
                                                                                            <td style="width: 68px">
                                                                                <asp:ImageButton ID="imgButton_IdenNonSwiftOutPengirimNas_Ind_Pekerjaan" 
                                                                                    runat="server" ImageUrl="~/Images/button/browse.gif" 
                                                                                    OnClientClick="javascript:popWinPekerjaan();" /></td>
                                                                                            <td style="width: 68px">
                                                                                                <asp:ImageButton ID="RMIdenNonSwiftOutPengirimNas_Ind_Pekerjaan" runat="server" ImageUrl="~/Images/button/remove.gif"
                                                                                                    OnClientClick="javascript:popWinNegara();" /></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <asp:HiddenField ID="hfIdenNonSwiftOutPengirimNas_Ind_pekerjaan" runat="server" />
                                                                            </td>
                                                                            <td style="width: 35px">
                                                                                </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                       <tr id="trIdenNonSwiftOutPengirimNas_Ind_Pekerjaanlain" runat="server" visible="false">
                                                                           <td style="width: 50%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label45" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px">
                                                                                <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaanlain" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                           <td style="width: 35px">
                                                                           </td>
                                                                           <td style="width: 47px">
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label37" runat="server" Text="e. Alamat Domisili"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                </td>
                                                                            <td style="width: 35px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6; height: 20px;">
                                                                                <asp:Label ID="Label38" runat="server" Text="Alamat"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 222px; height: 20px;">
                                                                                <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_Alamat" runat="server" CssClass="searcheditbox" MaxLength="100"
                                                                                    TabIndex="2" Width="341px"></asp:TextBox></td>
                                                                            <td style="width: 35px; height: 20px;">
                                                                            </td>
                                                                            <td style="width: 47px; height: 20px;">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label39" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                            <td style="height: 18px; width: 222px;">
                                                                                &nbsp;
                                                                                <table style="width: 127px">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                                <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_Kota" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;</td>
                                                                                            <td style="width: 68px">
                                                                                <asp:ImageButton ID="imgButtonNonSwiftOutPengirimNasKotaDom" runat="server" ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinKotaKab();" /></td>
                                                                                            <td style="width: 68px">
                                                                                                <asp:ImageButton ID="RMNonSwiftOutPengirimNasKotaDom" runat="server" ImageUrl="~/Images/button/remove.gif"
                                                                                                    OnClientClick="javascript:popWinNegara();" /></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <asp:HiddenField ID="hfIdenNonSwiftOutPengirimNas_Ind_Kota_dom" runat="server" />
                                                                            </td>
                                                                            <td style="height: 18px; width: 35px;">
                                                                                </td>
                                                                            <td style="width: 47px; height: 18px">
                                                                            </td>
                                                                        </tr>
                                                                       <tr id="trIdenNonSwiftOutPengirimNas_Ind_kotalain" runat="server" visible="false">
                                                                           <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                               <asp:Label ID="Label57" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px; height: 18px">
                                                                                <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_kotalain" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                           <td style="width: 35px; height: 18px">
                                                                           </td>
                                                                           <td style="width: 47px; height: 18px">
                                                                           </td>
                                                                       </tr>
                                                                        <tr id="trIdenNonSwiftOutPengirimNas_Ind_provinsi" runat="server" visible="false">
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label41" runat="server" Text="Provinsi"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                &nbsp;
                                                                                <table style="width: 127px">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                               <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_provinsi" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;</td>
                                                                                            <td style="width: 68px">
                                                                                <asp:ImageButton ID="imgButtonNonSwiftOutPengirimNasProvinsiDom" runat="server" ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinProvinsi();" /></td>
                                                                                            <td style="width: 68px">
                                                                                                <asp:ImageButton ID="RMNonSwiftOutPengirimNasProvinsiDom" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <asp:HiddenField ID="hfIdenNonSwiftOutPengirimNas_Ind_provinsi_dom" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;</td>
                                                                            <td style="width: 47px">
                                                                                &nbsp;</td>
                                                                        </tr>
                                                                       <tr id="trIdenNonSwiftOutPengirimNas_Ind_provinsiLain" runat="server" visible="false">
                                                                           <td style="width: 50%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label186" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px">
                                                                               <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_provinsiLain" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                           <td style="width: 35px">
                                                                           </td>
                                                                           <td style="width: 47px">
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6; height: 22px;">
                                                                                <asp:Label ID="Label42" runat="server" Text="f. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                            <td style="width: 222px; height: 22px;">
                                                                                <%-- <ajax:AjaxPanel ID="AjaxPanelCheckPengirimNas_Ind_alamat" runat="server">--%>
                                                                                <asp:CheckBox ID="CheckNonSwiftOutPengirimNas_Ind_alamat" runat="server" 
                                                                                    Text="Sama dengan Alamat Domisili" AutoPostBack="True" />
                                                                                    
                                                                                   <%--</ajax:AjaxPanel>--%></td>
                                                                            <td style="width: 35px; height: 22px;">
                                                                            </td>
                                                                            <td style="width: 47px; height: 22px;">
                                                                            </td>
                                                                        </tr>
                                                                       <tr>
                                                                           <td style="width: 50%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label43" runat="server" Text="Alamat"></asp:Label>
                                                                               <asp:Label ID="Label267" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                           <td style="width: 222px">
                                                                               <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_alamatIdentitas" runat="server" CssClass="searcheditbox" MaxLength="100"
                                                                                   TabIndex="2" Width="341px"></asp:TextBox></td>
                                                                           <td style="width: 35px">
                                                                           </td>
                                                                           <td style="width: 47px">
                                                                           </td>
                                                                       </tr>
                                                                       <tr>
                                                                           <td style="width: 50%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label44" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                               <asp:Label ID="Label268" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                           <td style="width: 222px">
                                                                               &nbsp;
                                                                               <table style="width: 127px">
                                                                                   <tbody>
                                                                                       <tr>
                                                                                           <td>
                                                                                               &nbsp;<asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                           <td style="width: 68px">
                                                                               <asp:ImageButton ID="imgButtonNonSwiftOutPengirimNasKotaIden" runat="server" ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinKotaKab();" /></td>
                                                                                           <td style="width: 68px">
                                                                                               <asp:ImageButton ID="RMNonSwiftOutPengirimNasKotaIden" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                                       </tr>
                                                                                   </tbody>
                                                                               </table>
                                                                               <asp:HiddenField ID="hfIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas" runat="server" />
                                                                           </td>
                                                                           <td style="width: 35px">
                                                                           </td>
                                                                           <td style="width: 47px">
                                                                           </td>
                                                                       </tr>
                                                                       <tr id="trIdenNonSwiftOutPengirimNas_Ind_KotaLainIden" runat="server" visible="false">
                                                                           <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                               <asp:Label ID="Label83" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px; height: 18px">
                                                                               <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_KotaLainIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                           <td style="width: 35px">
                                                                           </td>
                                                                           <td style="width: 47px">
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Label46" runat="server" Text="Provinsi"></asp:Label>
                                                                                <asp:Label ID="Label269" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="height: 18px; width: 222px;">
                                                                                &nbsp;
                                                                                <table style="width: 127px">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                                                &nbsp;<asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <td style="width: 68px">
                                                                                <asp:ImageButton ID="imgButtonNonSwiftOutPengirimNasProvinsiIden" runat="server" ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinProvinsi();" /></td>
                                                                                            <td style="width: 68px">
                                                                                                <asp:ImageButton ID="RMNonSwiftOutPengirimNasProvinsiIden" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <asp:HiddenField ID="hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;</td>
                                                                            <td style="width: 47px; height: 18px">
                                                                                &nbsp;</td>
                                                                        </tr>
                                                                       <tr id="trIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden" runat="server" visible="false">
                                                                           <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                               <asp:Label ID="Label62" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px; height: 18px">
                                                                               <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                           <td style="width: 35px; height: 18px">
                                                                           </td>
                                                                           <td style="width: 47px; height: 18px">
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label48" runat="server" Text="g. Jenis Dokument Identitas"></asp:Label>
                                                                                <asp:Label ID="Label270" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 222px"><asp:DropDownList ID="cboNonSwiftOutPengirimNasInd_jenisidentitas" runat="server">
                                                                            </asp:DropDownList></td>
                                                                            <td style="width: 35px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="LabelI49" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                <asp:Label ID="LabelI271" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="height: 18px; width: 222px;">
                                                                                <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Ind_noIdentitas" runat="server" CssClass="searcheditbox" MaxLength="30"
                                                                                    TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                            <td style="width: 35px; height: 18px;">
                                                                            </td>
                                                                            <td style="width: 47px; height: 18px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6; height: 15px;">
                                                                            </td>
                                                                            <td style="height: 15px; width: 222px;">
                                                                            </td>
                                                                            <td style="width: 35px; height: 15px;">
                                                                            </td>
                                                                            <td style="width: 47px; height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                   </table>
                                                              </ajax:AjaxPanel>
                                                            </asp:View>
                                                            <asp:View ID="VwNonSwiftOut_PNG_Corp" runat="server"><table style="width: 288px; height: 31px">
                                                                <tr>
                                                                    <td style="width: 60%; height: 15px; background-color: #fff7e6">
                                                                        <asp:Label ID="LabelI51" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                    <td style="height: 15px; color: #000000; width: 240px;">
                                                                    </td>
                                                                    <td style="height: 15px; width: 2977px; color: #000000;">
                                                                    </td>
                                                                    <td style="width: 2977px; color: #000000; height: 15px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                        <asp:Label ID="Label9306" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label>
                                                                    </td>
                                                                    <td style="width: 240px">
                                                                        <asp:DropDownList ID="cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha" 
                                                                            runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                        &nbsp;</td>
                                                                    <td style="width: 2977px">
                                                                        &nbsp;</td>
                                                                </tr>
                                                                <tr ID="trNonSwiftOutPengirimNas_Corp_BentukBadanUsahaLain"  runat="server" visible="false">
                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                        <asp:Label ID="Labelop9307" runat="server" Text="Bentuk Badan Usaha Lainnya " 
                                                                            Width="214px"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 240px">
                                                                        <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsahaLain" runat="server" 
                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                        &nbsp;</td>
                                                                    <td style="width: 2977px">
                                                                        &nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                        <asp:Label ID="Label55" runat="server" Text="b. Nama Korporasi" Width="112px"></asp:Label>
                                                                        <asp:Label ID="Label52" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 240px">
                                                                        <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Corp_NamaKorp" runat="server" 
                                                                            CssClass="searcheditbox" MaxLength="255" TabIndex="2" Width="125px"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                        <asp:Label ID="Labelu9308" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label>
                                                                        <asp:Label ID="Label273" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 240px">
                                                                        &nbsp;
                                                                        <table style="width: 127px">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                        <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp" runat="server" 
                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;</td>
                                                                                    <td style="width: 68px">
                                                                        <asp:ImageButton ID="imgButtonNonSwiftOutPengirimNasCorp_BidangUsaha" runat="server" 
                                                                            ImageUrl="~/Images/button/browse.gif" 
                                                                            OnClientClick="javascript:popWinBidangUsaha();" /></td>
                                                                                    <td style="width: 68px">
                                                                                        <asp:ImageButton ID="RMNonSwiftOutPengirimNasCorp_BidangUsaha" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <asp:HiddenField ID="hfIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp" runat="server" />
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                        &nbsp;</td>
                                                                    <td style="width: 2977px">
                                                                        &nbsp;</td>
                                                                </tr>
                                                                <tr id="trIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorpLainnya" runat="server" visible="false">
                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                        <asp:Label ID="Label20" runat="server" Text="Bidang Usaha Korporasi Lainnya"></asp:Label></td>
                                                                    <td style="width: 240px">
                                                                        <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorpLainnya" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                            TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                        <asp:Label ID="Label58" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                    <td style="width: 240px">
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                        <asp:Label ID="Label59" runat="server" Text="Alamat"></asp:Label>
                                                                        <asp:Label ID="Label274" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                    <td style="width: 240px">
                                                                        <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Corp_alamatkorp" runat="server" CssClass="searcheditbox" MaxLength="100"
                                                                            TabIndex="2" Width="342px"></asp:TextBox></td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                        <asp:Label ID="Label61" runat="server" Text="Kota / Kabupaten "></asp:Label>
                                                                        <asp:Label ID="Label275" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                    <td style="width: 240px">
                                                                        &nbsp;
                                                                        <table style="width: 127px">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                        <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Corp_kotakorp" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;</td>
                                                                                    <td style="width: 68px">
                                                                        <asp:ImageButton ID="imgButtonNonSwiftOutPengirimNasKorp_kotaLengkap" runat="server" ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinKotaKab();" /></td>
                                                                                    <td style="width: 68px">
                                                                                        <asp:ImageButton ID="RMNonSwiftOutPengirimNasKorp_kotaLengkap" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <asp:HiddenField ID="hfIdenNonSwiftOutPengirimNas_Corp_kotakorp" runat="server" />
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                        </td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                </tr>
                                                                <tr id="trIdenNonSwiftOutPengirimNas_Corp_kotakorplain" runat="server" visible="false">
                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                        <asp:Label ID="Label69" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                    <td style="width: 240px">
                                                                        <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Corp_kotakorplain" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                            TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                        <asp:Label ID="Label63" runat="server" Text="Provinsi"></asp:Label>
                                                                        <asp:Label ID="Label276" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                    <td style="width: 240px">
                                                                        &nbsp;
                                                                        <table style="width: 127px">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                                        &nbsp;<asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Corp_provKorp" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 68px">
                                                                        <asp:ImageButton ID="imgButtonNonSwiftOutPengirimNasKorp_ProvinsiLengkap" runat="server" ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinProvinsi();" /></td>
                                                                                    <td style="width: 68px">
                                                                                        <asp:ImageButton ID="RMNonSwiftOutPengirimNasKorp_ProvinsiLengkap" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <asp:HiddenField ID="hfIdenNonSwiftOutPengirimNas_Corp_provKorp" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td style="width: 2977px">
                                                                        &nbsp;</td>
                                                                </tr>
                                                                <tr id="trIdenNonSwiftOutPengirimNas_Corp_provKorpLain" runat="server" visible="false">
                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                        <asp:Label ID="Label78" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                    <td style="width: 222px">
                                                                        <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Corp_provKorpLain" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                            TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                    <td style="width: 2977px; height: 15px">
                                                                    </td>
                                                                    <td style="width: 2977px; height: 15px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                        <asp:Label ID="Label64" runat="server" Text="e. Alamat Korporasi di Luar Negeri"></asp:Label></td>
                                                                    <td style="width: 240px">
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                        <asp:Label ID="Label9258" runat="server" Text="Alamat"></asp:Label></td>
                                                                    <td style="width: 240px">
                                                                        <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Corp_AlamatLuar" runat="server" 
                                                                            CssClass="searcheditbox" MaxLength="100"
                                                                            TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                        <asp:Label ID="Label60" runat="server" Text="f. Alamat Cabang Pengirim Asal"></asp:Label></td>
                                                                    <td style="width: 240px">
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60%; background-color: #fff7e6">
                                                                        <asp:Label ID="Label9259" runat="server" Text="Alamat"></asp:Label></td>
                                                                    <td style="width: 240px">
                                                                        <asp:TextBox ID="TxtIdenNonSwiftOutPengirimNas_Corp_AlamatAsal" runat="server" 
                                                                            CssClass="searcheditbox" MaxLength="100"
                                                                            TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                    <td style="width: 2977px">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            </asp:View>
                                                        </asp:MultiView></td>
                                                    
                                                </tr>
                                                                                             
                                                </table>
                                                </td>
                                        </tr>
                                    </table>
                                                            </asp:View>
                                                            <asp:View ID="ViewNonSwiftOutNonSwiftOutPengirimNonNasabah" runat="server">
                                                                <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 7px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext" style="height: 14px">
                                                            <asp:Label ID="Labeln4" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td style="height: 14px">
                                                            <a href="#" onclick="javascript:ShowHidePanel('B1NonSwiftOutidentitasNonSwiftOutPengirimNonNasabah','Imgg3','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Imgg3" src="Images/search-bar-minimize.gif" border="0" height="12"
                                                                    width="12" /></a>
                                                        </td>
                                                    </tr>
                                       </table>
                                            </td>
                                        </tr>
                                        <tr id="B1NonSwiftOutidentitasNonSwiftOutPengirimNonNasabah">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                             
                                                </table><table style="width: 100%; height: 49px">
                                                    <tr>
                                                        <td style="background-color: #fff7e6" colspan="2">
                                                            <asp:RadioButtonList ID="RbNonSwiftOutPengirimNonNasabah_100Juta" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                                <asp:ListItem Value="1">&lt; 100 Juta (Kurs yang digunakan</asp:ListItem>
                                                                <asp:ListItem Value="2">&gt; 100 Juta (Kurs yang digunakan</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                            <asp:Label ID="Label9311" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                        </td>
                                                        <td style="width: 5%;">
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%; background-color: #fff7e6">
                                                            <asp:Label ID="Label81" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                            <asp:Label ID="Label85" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="width: 35%">
                                                            <asp:TextBox ID="TxtNonSwiftOutPengirimNonNasabah_nama" runat="server" CssClass="searcheditbox" MaxLength="255"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td style="width: 5%">
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%; background-color: #fff7e6">
                                                            <asp:Label ID="Label82" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label>
                                                            </td>
                                                        <td style="width: 35%">
                                                            <asp:TextBox ID="TxtNonSwiftOutPengirimNonNasabah_TanggalLahir" runat="server" 
                                                                CssClass="searcheditbox" TabIndex="2"
                                                                Width="122px" Enabled="False"></asp:TextBox>
                                                            <input id="popUpTanggalLahirNonSwiftOutPengirimNonNasabah" runat="server"
                                                            onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')" style="border-right: #ffffff 0px solid;
                                                            border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                            border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                            height: 17px" title="Click to show calendar" type="button" /></td>
                                                        <td style="width: 5%">
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%; background-color: #fff7e6">
                                                            <asp:Label ID="Label95" runat="server" Text="c. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                        <td style="width: 35%">
                                                        </td>
                                                        <td style="width: 5%">
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%; background-color: #fff7e6">
                                                            <asp:Label ID="Label96" runat="server" Text="Alamat"></asp:Label>
                                                            <asp:Label ID="Label9309" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%">
                                                            <asp:TextBox ID="TxtNonSwiftOutPengirimNonNasabah_alamatiden" runat="server" CssClass="searcheditbox" MaxLength="100"
                                                                TabIndex="2" Width="341px"></asp:TextBox></td>
                                                        <td style="width: 5%">
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%; background-color: #fff7e6">
                                                            <asp:Label ID="Label97" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                            </td>
                                                        <td style="width: 35%">
                                                            &nbsp;
                                                            <table style="width: 127px">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;<asp:TextBox ID="TxtNonSwiftOutPengirimNonNasabah_kotaIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 68px">
                                                            <asp:ImageButton ID="imgButtonNonSwiftOut_PengirimNonNas_Kota" runat="server" ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinKotaKab();" /></td>
                                                                        <td style="width: 68px">
                                                                            <asp:ImageButton ID="ImageButton10" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <asp:HiddenField ID="hfNonSwiftOutPengirimNonNasabah_kotaIden" runat="server" />
                                                        </td>
                                                        <td style="width: 5%">
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr id="trNonSwiftOutPengirimNonNasabah_KoaLainIden" runat="server" visible="false">
                                                        <td style="width: 20%; background-color: #fff7e6">
                                                            <asp:Label ID="Label88" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                        <td style="width: 35%">
                                                            <asp:TextBox ID="TxtNonSwiftOutPengirimNonNasabah_KoaLainIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td style="width: 5%">
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%; background-color: #fff7e6;">
                                                            <asp:Label ID="Label98" runat="server" Text="Provinsi"></asp:Label>
                                                            </td>
                                                        <td>
                                                            &nbsp;
                                                            <table style="width: 127px">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                            <asp:TextBox ID="TxtNonSwiftOutPengirimNonNasabah_ProvIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;</td>
                                                                        <td style="width: 68px">
                                                            <asp:ImageButton ID="imgButtonNonSwiftOut_PengirimNonNas__Provinsi" runat="server" ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinProvinsi();" /></td>
                                                                        <td style="width: 68px">
                                                                            <asp:ImageButton ID="ImageButton11" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <asp:HiddenField ID="hfNonSwiftOutPengirimNonNasabah_ProvIden" runat="server" />
                                                        </td>
                                                        <td>
                                                            &nbsp;</td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr id="trNonSwiftOutPengirimNonNasabah_ProvLainIden" runat="server" visible="false">
                                                        <td style="width: 20%; background-color: #fff7e6">
                                                            <asp:Label ID="Label99" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                        <td style="width: 35%;">
                                                            <asp:TextBox ID="TxtNonSwiftOutPengirimNonNasabah_ProvLainIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td style="width: 5%;">
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%; background-color: #fff7e6; height: 24px;">
                                                            <asp:Label ID="Labelso100" runat="server" Text="d. Jenis Dokument Identitas"></asp:Label>
                                                            <asp:Label ID="Mand_JenisDokumen" runat="server" ForeColor="Red" Text="*" Visible="False"></asp:Label></td>
                                                        <td style="width: 35%; height: 24px;">
                                                            <asp:DropDownList ID="CboNonSwiftOutPengirimNonNasabah_JenisDokumen" 
                                                                runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 5%; height: 24px;">
                                                        </td>
                                                        <td style="height: 24px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%; background-color: #fff7e6;">
                                                            <asp:Label ID="Labele101" runat="server" Text="Nomor Identitas"></asp:Label>
                                                            <asp:Label ID="Mand_Nomor" runat="server" ForeColor="Red" Text="*" Visible="False"></asp:Label></td>
                                                        <td style="width: 35%;">
                                                            <asp:TextBox ID="TxtNonSwiftOutPengirimNonNasabah_NomorIden" runat="server" CssClass="searcheditbox" MaxLength="30"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td style="width: 5%;">
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%; background-color: #fff7e6;">
                                                        </td>
                                                        <td style="width: 35%;">
                                                        </td>
                                                        <td style="width: 5%;">
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                                            </asp:View>
                                                        </asp:MultiView>
                                               
                                                </ajax:AjaxPanel>

                                                </td>
                                        </tr>
                                    </table>
                                                </td>
                                           </tr>
                                          
                                   </table>                                    
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Labelas7" runat="server" Text="C. Beneficiary Owner " Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('CNonSwiftOutBeneficiaryOwner','Imgg5','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Imgg5" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="CNonSwiftOutBeneficiaryOwner">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                            <table style="width: 725px; height: 60px">
                                                                <tr>
                                                                    <td colspan="1" style="width: 33%; height: 24px; background-color: #fff7e6">
                                                                        <asp:Label ID="Label3" runat="server" Text="Apakah transaksi melibatkan Beneficial Owner?"
                                                                            Width="298px"></asp:Label>
                                                                        <asp:Label ID="Label22" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                    <td colspan="1" style="width: 124px; height: 24px">
                                                                        <asp:RadioButtonList ID="Rb_BOwnerNas_ApaMelibatkan" runat="server" AutoPostBack="True"
                                                                            RepeatDirection="Horizontal" Width="189px">
                                                                            <asp:ListItem Value="1">Ya</asp:ListItem>
                                                                            <asp:ListItem Value="2">Tidak</asp:ListItem>
                                                                        </asp:RadioButtonList></td>
                                                                </tr>
                                                                <tr id="BenfOwnerHubungan" runat="server" visible="false">
                                                                    <td style="width: 33%; background-color: #fff7e6">
                                                                        <asp:Label ID="Label162" runat="server" Text="Hubungan dengan Pemilik Dana (Beneficial Owner)"
                                                                            Width="291px"></asp:Label>
                                                                        <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                    <td style="width: 124px">
                                                                        <asp:TextBox ID="BenfOwnerNonSwiftOutHubunganPemilikDana" runat="server" 
                                                                            CssClass="searcheditbox" MaxLength="50"
                                                                            TabIndex="2" Width="343px"></asp:TextBox></td>
                                                                </tr>
                                                                <tr id="BenfOwnerTipePengirim" runat="server" visible="false">
                                                                    <td style="width: 33%; background-color: #fff7e6; height: 82px;">
                                                                                    <asp:Label ID="Label9255" runat="server" Text="       Tipe Pengirim " Width="87px"></asp:Label>
                                                                                    <asp:Label ID="Mand_lbh100jt0" runat="server" ForeColor="Red" Text="*" 
                                                                                        Visible="False"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 124px; height: 82px;">
                                                                                    <ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                                                                        <asp:RadioButtonList ID="Rb_NonSwiftOutBOwnerNas_TipePengirim" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" Width="189px">
                                                                                            <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                                            <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                    </ajax:AjaxPanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                              
                                                <asp:MultiView ID="MultiViewNonSwiftOutBOwner" runat="server">
                                                    <asp:View ID="ViewNonSwiftOutBOwnerNasabah" runat="server">
                                                         <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2" id="TableNonSwiftOutBOwnerNas">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Labelf8" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('BNonSwiftOutOwner1nasabah','Imgf6','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Imgf6" src="Images/search-bar-minimize.gif" border="0" height="12"
                                                                    width="12" /></a>
                                                        </td>
                                                    </tr>
                                       </table>
                                            </td>
                                        </tr>
                                        <tr id="BNonSwiftOutOwner1nasabah">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                             
                                                </table><table style="width: 98%; height: 49px">
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6">
                                                            <asp:Label ID="Label1163" runat="server" Text="a. No. Rek "></asp:Label>
                                                            <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="width: 204px">
                                                            <asp:TextBox ID="BenfOwnerNonSwiftOutNasabah_rekening" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6">
                                                            <asp:Label ID="Label146" runat="server" Text="b. Nama Lengkap"></asp:Label>
                                                            <asp:Label ID="Label6" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="width: 204px">
                                                            <asp:TextBox ID="BenfOwnerNonSwiftOutNasabah_Nama" runat="server" CssClass="searcheditbox" MaxLength="255"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6">
                                                            <asp:Label ID="Label1r48" runat="server" Text="c. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label>
                                                            <asp:Label ID="Label7" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        <td style="width: 204px">
                                                            <asp:TextBox ID="BenfOwnerNonSwiftOutNasabah_tanggalLahir" runat="server" 
                                                                CssClass="searcheditbox" TabIndex="2"
                                                                Width="122px" Enabled="False"></asp:TextBox>
                                                            <input id="popUpTanggalLahirNonSwiftOutBOwnerNasabah" runat="server"
                                                            onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')" style="border-right: #ffffff 0px solid;
                                                            border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                            border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                            height: 17px" title="Click to show calendar" type="button" /></td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6">
                                                            <asp:Label ID="Label150" runat="server" Text="d. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                        <td style="width: 204px">
                                                        </td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6">
                                                            <asp:Label ID="Label151" runat="server" Text="Alamat"></asp:Label>
                                                            <asp:Label ID="Label8" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="width: 204px">
                                                            <asp:TextBox ID="BenfOwnerNonSwiftOutNasabah_AlamatIden" runat="server" CssClass="searcheditbox" MaxLength="100"
                                                                TabIndex="2" Width="341px"></asp:TextBox></td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6">
                                                            <asp:Label ID="Label152" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                            <asp:Label ID="Label11" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="width: 204px">
                                                            &nbsp;
                                                            <table style="width: 127px">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;<asp:TextBox ID="BenfOwnerNonSwiftOutNasabah_KotaIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 68px">
                                                            <asp:ImageButton ID="imgButton_NonSwiftOutBOwnerNas_KotaIden" runat="server" ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinKotaKab();" /></td>
                                                                        <td style="width: 68px">
                                                                            <asp:ImageButton ID="ImageButton12" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <asp:HiddenField ID="hfBenfOwnerNonSwiftOutNasabah_KotaIden" runat="server" />
                                                        </td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr id="trBenfOwnerNonSwiftOutNasabah_kotaLainIden" runat="server" visible="false">
                                                        <td style="width: 25%; background-color: #fff7e6">
                                                            <asp:Label ID="Label154" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                        <td style="width: 204px">
                                                            <asp:TextBox ID="BenfOwnerNonSwiftOutNasabah_kotaLainIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Label155" runat="server" Text="Provinsi"></asp:Label>
                                                            <asp:Label ID="Label12" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="height: 18px; width: 204px;">
                                                            &nbsp;
                                                            <table style="width: 127px">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;<asp:TextBox ID="BenfOwnerNonSwiftOutNasabah_ProvinsiIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 68px">
                                                            <asp:ImageButton ID="imgButton_NonSwiftOutBOwnerNas_ProvinsiIden" runat="server" ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinProvinsi();" /></td>
                                                                        <td style="width: 68px">
                                                                            <asp:ImageButton ID="ImageButton13" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <asp:HiddenField ID="hfBenfOwnerNonSwiftOutNasabah_ProvinsiIden" runat="server" />
                                                        </td>
                                                        <td>
                                                            &nbsp;</td>
                                                        <td style="width: 47px; height: 18px">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr id="trBenfOwnerNonSwiftOutNasabah_ProvinsiLainIden" runat="server" visible="false">
                                                        <td style="width: 25%; height: 18px; background-color: #fff7e6">
                                                            <asp:Label ID="Label156" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                        <td style="width: 204px; height: 18px">
                                                            <asp:TextBox ID="BenfOwnerNonSwiftOutNasabah_ProvinsiLainIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td style="width: 35px; height: 18px">
                                                        </td>
                                                        <td style="width: 47px; height: 18px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6">
                                                            <asp:Label ID="Label158" runat="server" Text="e. Jenis Dokument Identitas"></asp:Label>
                                                            <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="width: 204px">
                                                            <asp:DropDownList ID="CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen" runat="server">
                                                            </asp:DropDownList></td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Label160" runat="server" Text="Nomor Identitas"></asp:Label>
                                                            <asp:Label ID="Label14" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="height: 18px; width: 204px;">
                                                            <asp:TextBox ID="BenfOwnerNonSwiftOutNasabah_NomorIden" runat="server" CssClass="searcheditbox" MaxLength="30"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td style="width: 35px; height: 18px;">
                                                        </td>
                                                        <td style="width: 47px; height: 18px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6; height: 15px;">
                                                        </td>
                                                        <td style="height: 15px; width: 204px;">
                                                        </td>
                                                        <td style="width: 35px; height: 15px;">
                                                        </td>
                                                        <td style="width: 47px; height: 15px">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                                    </asp:View>
                                                    <asp:View ID="ViewNonSwiftOutBOwnerNonNasabah" runat="server">
                                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none" id="TableSwiftOutBOwnerNonNas">
                                                                                                
                                             <tr>
                                                <td>
                                                     <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext" style="height: 14px">
                                                            <asp:Label ID="Label9" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td style="height: 14px">
                                                            <a href="#" onclick="javascript:ShowHidePanel('BNonSwiftOutOwnerNonNasabah','Imgf7','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Imgf7" src="Images/search-bar-minimize.gif" border="0" height="12"
                                                                    width="12" /></a>
                                                        </td>
                                                    </tr>
                                       </table>
                                            </td>
                                        </tr>
                                        <tr id="BNonSwiftOutOwnerNonNasabah">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                             
                                                </table><table style="width: 98%; height: 49px">
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6">
                                                            <asp:Label ID="Label157" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                            <asp:Label ID="Label15" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="width: 222px">
                                                            <asp:TextBox ID="BenfOwnerNonSwiftOutNonNasabah_Nama" runat="server" CssClass="searcheditbox" MaxLength="255"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6">
                                                            <asp:Label ID="Label166" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label>
                                                            </td>
                                                        <td style="width: 222px">
                                                            <asp:TextBox ID="BenfOwnerNonSwiftOutNonNasabah_TanggalLahir" runat="server" 
                                                                CssClass="searcheditbox" TabIndex="2"
                                                                Width="122px" Enabled="False"></asp:TextBox>
                                                            <input id="popUpTanggalLahirBownerNonNasabah" runat="server"
                                                            onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')" style="border-right: #ffffff 0px solid;
                                                            border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                            border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                            height: 17px" title="Click to show calendar" type="button" /></td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6">
                                                            <asp:Label ID="Label168" runat="server" Text="c. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                        <td style="width: 222px">
                                                        </td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6">
                                                            <asp:Label ID="Label169" runat="server" Text="Alamat"></asp:Label>
                                                            <asp:Label ID="Label16" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="width: 222px">
                                                            <asp:TextBox ID="BenfOwnerNonSwiftOutNonNasabah_AlamatIden" runat="server" CssClass="searcheditbox" MaxLength="100"
                                                                TabIndex="2" Width="341px"></asp:TextBox></td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6">
                                                            <asp:Label ID="Label170" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                            <asp:Label ID="Label17" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="width: 222px">
                                                            &nbsp;
                                                            <table style="width: 127px">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;<asp:TextBox ID="BenfOwnerNonSwiftOutNonNasabah_KotaIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 68px">
                                                            <asp:ImageButton ID="imgButton_NonSwiftOutBOwnerNonNas_kota" runat="server" ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinKotaKab();" /></td>
                                                                        <td style="width: 68px">
                                                                            <asp:ImageButton ID="ImageButton14" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <asp:HiddenField ID="hfBenfOwnerNonSwiftOutNonNasabah_KotaIden" runat="server" />
                                                        </td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr id="trBenfOwnerNonSwiftOutNonNasabah_KotaLainIden" runat="server" visible="false">
                                                        <td style="width: 25%; background-color: #fff7e6">
                                                            <asp:Label ID="Label172" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                        <td style="width: 222px">
                                                            <asp:TextBox ID="BenfOwnerNonSwiftOutNonNasabah_KotaLainIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Label173" runat="server" Text="Provinsi"></asp:Label>
                                                            <asp:Label ID="Label18" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="height: 18px; width: 222px;">
                                                            &nbsp;
                                                            <table style="width: 127px">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;<asp:TextBox ID="BenfOwnerNonSwiftOutNonNasabah_ProvinsiIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 68px">
                                                            <asp:ImageButton ID="imgButton_NonSwiftOutBOwnerNonNas_Provinsi" runat="server" ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinProvinsi();" /></td>
                                                                        <td style="width: 68px">
                                                                            <asp:ImageButton ID="ImageButton15" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <asp:HiddenField ID="hfBenfOwnerNonSwiftOutNonNasabah_ProvinsiIden" runat="server" />
                                                        </td>
                                                        <td>
                                                            &nbsp;</td>
                                                        <td style="width: 47px; height: 18px">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr id="trBenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden" runat="server" visible="false">
                                                        <td style="width: 25%; height: 18px; background-color: #fff7e6">
                                                            <asp:Label ID="Label174" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                        <td style="width: 222px; height: 18px">
                                                            <asp:TextBox ID="BenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td style="width: 35px; height: 18px">
                                                        </td>
                                                        <td style="width: 47px; height: 18px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6">
                                                            <asp:Label ID="Label176" runat="server" Text="d. Jenis Dokument Identitas"></asp:Label>
                                                            </td>
                                                        <td style="width: 222px">
                                                            <asp:DropDownList ID="CBOBenfOwnerNonSwiftOutNonNasabah_JenisDokumen" runat="server" CssClass="combobox">
                                                            </asp:DropDownList></td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 25%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Label178" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                        <td style="height: 18px; width: 222px;">
                                                            <asp:TextBox ID="BenfOwnerNonSwiftOutNonNasabah_NomorIdentitas" runat="server" CssClass="searcheditbox" MaxLength="30"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td style="width: 35px; height: 18px;">
                                                        </td>
                                                        <td style="width: 47px; height: 18px">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                                </td>
                                             </tr>
                                                </table>
                                                    </asp:View>
                                                </asp:MultiView></td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Label10" runat="server" Text="D. Identitas Penerima" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('DNonSwiftOutIdentitasPenerima','Imgg8','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Imgg8" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="DNonSwiftOutIdentitasPenerima">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                             
                                                </table>
                                                            <table style="width: 725px; height: 60px">
                                                                <tr id="trNonSwiftOutPenerimaTipePengirim" runat="server">
                                                                    <td style="width: 34%; background-color: #fff7e6; height: 34px;">
                                                                        <asp:Label ID="Label192" runat="server" Text="       "></asp:Label>
                                                                        <asp:Label ID="Label193" runat="server" Text="       Tipe Penerima "></asp:Label>
                                                                        <asp:Label ID="Label194" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 124px; height: 34px;"><ajax:AjaxPanel ID="AjaxPanelnonSO5" 
                                                                            runat="server" Width="93px">
                                                                        &nbsp;<asp:RadioButtonList ID="RBNonSwiftOutPenerimaNasabah_TipePengirim" runat="server" 
                                                                            RepeatDirection="Horizontal" AutoPostBack="True" Width="201px" Enabled="False">
                                                                            <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                            <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                        </asp:RadioButtonList></ajax:AjaxPanel></td>
                                                                    <td style="width: 53px; height: 34px;">
                                                                    </td>
                                                                </tr>
                                                                <tr id="trNonSwiftOutPenerimaTipeNasabah" runat="server" visible="False">
                                                                    <td style="width: 34%; height: 34px; background-color: #fff7e6">
                                                                        <asp:Label ID="Label9256" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                                        <asp:Label ID="Label9257" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 124px; height: 34px">
                                                                        <ajax:AjaxPanel ID="AjaxPanel779" runat="server">
                                                                            <asp:RadioButtonList ID="Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah" runat="server" 
                                                                                AutoPostBack="True" RepeatDirection="Horizontal">
                                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </ajax:AjaxPanel>
                                                                    </td>
                                                                    <td style="width: 53px; height: 34px">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                <asp:MultiView ID="MultiViewNonSwiftOutIdenPenerima" runat="server">
                                                    <asp:View ID="ViewNonSwiftOutIdenPenerimaNasabah" runat="server">
                                                         <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Labelasf11" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('DNonSwiftOUtIdentitasPenerima1nasabah','Imgg9','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Imgg9" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" /></a>
                                                        </td>
                                                    </tr>
                                       </table>
                                            </td>
                                        </tr>
                                        <tr id="DNonSwiftOUtIdentitasPenerima1nasabah">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                             
                                                </table><table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                    <tr>
                                                        <td style="width: 100%"><table style="width: 725px; height: 60px">
                                                            <tr>
                                                                    <td style="width: 34%; background-color: #fff7e6; height: 15px;">
                                                                        <asp:Label ID="Label190" runat="server" Text="No. Rek " Width="84px"></asp:Label>
                                                                        <asp:Label ID="Label191" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                    <td style="width: 124px; height: 15px;">
                                                                        <asp:TextBox ID="txtPenerimaNonSwiftOut_rekening" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                            TabIndex="2" Width="125px"></asp:TextBox></td>
                                                            </tr>
                                                        </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 610px">
                                                            <asp:MultiView ID="MultiViewNonSwiftOutPenerimaNasabah" runat="server" ActiveViewIndex="0">
                                                                <asp:View ID="ViewNonSwiftOutPenerimaNasabah_IND" runat="server">
                                                                    <table style="width: 98%; height: 49px">
                                                                        <tr>
                                                                            <td style="width: 48%; height: 15px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label195" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; width: 222px;">
                                                                            </td>
                                                                            <td style="height: 15px; width: 36px;">
                                                                            </td>
                                                                            <td style="width: 47px; height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 48%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label197" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                <asp:Label ID="Label125" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_IND_nama" runat="server" CssClass="searcheditbox" MaxLength="255"
                                                                                    TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                            <td style="width: 36px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 48%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label198" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_IND_TanggalLahir" runat="server" 
                                                                                    CssClass="searcheditbox" TabIndex="2"
                                                                                    Width="122px" Enabled="False"></asp:TextBox>
                                                                                <input id="popUpNonSwiftOutTanggalLahirPenerimaNasabah_Ind" runat="server"
                                                            onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')" style="border-right: #ffffff 0px solid;
                                                            border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                            border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                            height: 17px" title="Click to show calendar" type="button" /></td>
                                                                            <td style="width: 36px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 48%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label200" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                    Width="223px"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:RadioButtonList ID="RbNonSwiftOutPenerimaNasabah_IND_Warganegara" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                </asp:RadioButtonList></td>
                                                                            <td style="width: 36px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="NegaraNonSwift_penerimaPerorangan" runat="server" visible="false">
                                                                            <td style="width: 48%; background-color: #fff7e6" >
                                                                                <asp:Label ID="Labels201" runat="server" Text="Negara "></asp:Label>
                                                                                <br />
                                                                                <asp:Label ID="Labels202" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                    Width="258px"></asp:Label></td>
                                                                            <td style="width: 222px" >
                                                                                &nbsp;
                                                                                <table style="width: 127px">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                                <asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_IND_negara" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;</td>
                                                                                            <td style="width: 68px">
                                                                                <asp:ImageButton ID="imgButton_NonSwiftOutPengirimNas_IND_negaraWarga" runat="server" ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinNegara();" /></td>
                                                                                            <td style="width: 68px">
                                                                                                <asp:ImageButton ID="ImageButton16" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <asp:HiddenField ID="hfNonSwiftOutPengirimNasabah_IND_negara" runat="server" />
                                                                            </td>
                                                                            <td style="width: 36px" >
                                                                            </td>
                                                                            <td style="width: 47px" >
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="NegaraLainNonSwift_penerimaPerorangan" runat="server" visible="false">
                                                                            <td style="width: 48%; background-color: #fff7e6" >
                                                                                <asp:Label ID="Labels203" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_IND_negaraLain" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                            <td style="width: 36px" >
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 48%; background-color: #fff7e6; height: 22px;">
                                                                                <asp:Label ID="Labels213" runat="server" Text="d. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                            <td style="width: 222px; height: 22px;">
                                                                                &nbsp;</td>
                                                                            <td style="width: 36px; height: 22px;">
                                                                            </td>
                                                                            <td style="width: 47px; height: 22px;">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 48%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labels214" runat="server" Text="Alamat"></asp:Label>
                                                                                <asp:Label ID="Label9310" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_IND_alamatIden" runat="server" CssClass="searcheditbox" MaxLength="100"
                                                                                    TabIndex="2" Width="341px"></asp:TextBox></td>
                                                                            <td style="width: 36px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 48%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labels205" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_IND_negaraBagian" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 36px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 48%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="Labels204" runat="server" Text="Negara"></asp:Label>
                                                                                <asp:Label ID="Labels147" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 222px; height: 18px">
                                                                                &nbsp;
                                                                                <table style="width: 127px">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                                                &nbsp;<asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_IND_negaraIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <td style="width: 68px">
                                                                                <asp:ImageButton ID="imgButton_NonSwiftOutPengirimNas_IND_negaraIden" runat="server" ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinNegara();" /></td>
                                                                                            <td style="width: 68px">
                                                                                                <asp:ImageButton ID="RMNonSwiftOutPengirimNas_IND_negaraIden" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <asp:HiddenField ID="hfNonSwiftOutPengirimNasabah_IND_negaraIden" runat="server" />
                                                                            </td>
                                                                            <td style="width: 36px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trPenerimaNonSwiftOutNasabah_IND_negaraLainIden" runat="server" visible="false">
                                                                            <td style="width: 48%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Labels206" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                            <td style="height: 18px; width: 222px;">
                                                                                <asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_IND_negaraLainIden" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                            <td style="width: 47px; height: 18px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 48%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labels220" runat="server" Text="e. Jenis Dokument Identitas"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:DropDownList ID="CBONonSwiftOutPenerimaNasabah_IND_JenisIden" runat="server">
                                                                                </asp:DropDownList></td>
                                                                            <td style="width: 36px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 48%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Labels221" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                            <td style="height: 18px; width: 222px;">
                                                                                <asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_IND_NomorIden" runat="server" CssClass="searcheditbox" MaxLength="30"
                                                                                    TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                            <td style="width: 36px; height: 18px;">
                                                                            </td>
                                                                            <td style="width: 47px; height: 18px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 48%; background-color: #fff7e6; height: 15px;">
                                                                            </td>
                                                                            <td style="height: 15px; width: 222px;">
                                                                            </td>
                                                                            <td style="width: 36px; height: 15px;">
                                                                            </td>
                                                                            <td style="width: 47px; height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                                <asp:View ID="ViewNonSwiftOutPenerimaNasabah_Korp" runat="server">
                                                                    <table style="width: 288px; height: 31px">
                                                                        <tr>
                                                                            <td style="background-color: #fff7e6;">
                                                                                <asp:Label ID="Labeler223" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; color: #000000; width: 240px;">
                                                                            </td>
                                                                            <td style="width: 240px; color: #000000; height: 15px">
                                                                            </td>
                                                                            <td style="height: 15px; width: 2977px; color: #000000;">
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="color: #000000">
                                                                            <td style="background-color: #fff7e6;">
                                                                                <asp:Label ID="Labelwe225" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label></td>
                                                                            <td style="width: 240px">
                                                                                <asp:DropDownList ID="cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain" 
                                                                                    runat="server">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td style="width: 240px">
                                                                            </td>
                                                                            <td style="width: 2977px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="background-color: #fff7e6;">
                                                                                <asp:Label ID="Labelwe226" runat="server" Text="Bentuk Badan Usaha Lainnya " 
                                                                                    Width="167px"></asp:Label></td>
                                                                            <td style="width: 240px">
                                                                                <asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLainnya" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                            <td style="width: 240px">
                                                                            </td>
                                                                            <td style="width: 2977px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="background-color: #fff7e6;">
                                                                                <asp:Label ID="Labelwe227" runat="server" Text="b. Nama Korporasi" Width="110px"></asp:Label>
                                                                                <asp:Label ID="Labelwe149" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 240px">
                                                                                <asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_Korp_namaKorp" runat="server" CssClass="searcheditbox" MaxLength="255"
                                                                                    TabIndex="2" Width="125px"></asp:TextBox></td>
                                                                            <td style="width: 240px">
                                                                            </td>
                                                                            <td style="width: 2977px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="background-color: #fff7e6;">
                                                                                <asp:Label ID="Labelwe228" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label></td>
                                                                            <td style="width: 240px">
                                                                                &nbsp;
                                                                                <table style="width: 127px">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                                <asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_Korp_BidangUsahaKorp" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;</td>
                                                                                            <td style="width: 68px">
                                                                                
                                                                                <asp:ImageButton ID="imgButton_NonSwiftOutPengirimNas_Korp_bidangUsaha" runat="server" 
                                                                                    ImageUrl="~/Images/button/browse.gif" 
                                                                                    OnClientClick="javascript:popWinBidangUsaha();" /></td>
                                                                                            <td style="width: 68px">
                                                                                                <asp:ImageButton ID="RMNonSwiftOutPengirimNas_Korp_bidangUsaha" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <asp:HiddenField ID="hfNonSwiftOutPengirimNasabah_Korp_BidangUsahaKorp" runat="server" />
                                                                                
                                                                            </td>
                                                                            <td style="width: 240px">
                                                                            </td>
                                                                            <td style="width: 2977px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trPenerimaNasabah_Korp_BidangUsahaKorpLainnya" runat="server" visible="false">
                                                                            <td style="background-color: #fff7e6">
                                                                                <asp:Label ID="Labelb9308" runat="server" Text="Bidang Usaha Korporasi Lainnya" Width="200px"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 240px">
                                                                                <asp:TextBox ID="txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya" runat="server" CssClass="textbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 240px">
                                                                            </td>
                                                                            <td style="width: 2977px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="background-color: #fff7e6;">
                                                                                <asp:Label ID="Labelwe229" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                            <td style="width: 240px">
                                                                            </td>
                                                                            <td style="width: 240px">
                                                                            </td>
                                                                            <td style="width: 2977px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="background-color: #fff7e6;">
                                                                                <asp:Label ID="Labelwe199" runat="server" Text="Alamat"></asp:Label>
                                                                                <asp:Label ID="Labeltwe153" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_Korp_AlamatKorp" runat="server" CssClass="searcheditbox" MaxLength="100"
                                                                                    TabIndex="2" Width="341px"></asp:TextBox></td>
                                                                            <td style="width: 36px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="background-color: #fff7e6;">
                                                                                <asp:Label ID="Labelwe207" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_Korp_Kota" runat="server" CssClass="searcheditbox" MaxLength="30"
                                                                                    TabIndex="2" Width="125px"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 36px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="background-color: #fff7e6;">
                                                                                <asp:Label ID="Labelwe208" runat="server" Text="Negara"></asp:Label>
                                                                                <asp:Label ID="Labelwe159" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 222px; height: 18px">
                                                                                &nbsp;
                                                                                <table style="width: 127px">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                                                &nbsp;<asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_Korp_NegaraKorp" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <td style="width: 68px">
                                                                                
                                                                                                                                                          <asp:ImageButton ID="imgButton_PenerimaNonSwiftOutNasabah_Korp_NegaraKorp" 
                                                                                    runat="server" ImageUrl="~/Images/button/browse.gif" 
                                                                                    OnClientClick="javascript:popWinNegara();" /></td>
                                                                                            <td style="width: 68px">
                                                                                                <asp:ImageButton ID="RMPenerimaNonSwiftOutNasabah_Korp_NegaraKorp" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <asp:HiddenField ID="hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp" 
                                                                                    runat="server" />
                                                                                
                                                                                                                                                          </td>
                                                                            <td style="width: 36px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trPenerimaNonSwiftOutNasabah_Korp_NegaraLainnyaKorp" runat="server" visible="false">
                                                                            <td style="background-color: #fff7e6;">
                                                                                <asp:Label ID="Label209" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                            <td style="height: 18px; width: 222px;">
                                                                                <asp:TextBox ID="txtPenerimaNonSwiftOutNasabah_Korp_NegaraLainnyaKorp" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                            <td style="width: 47px; height: 18px">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                            </asp:MultiView></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                                    </asp:View>
                                                    <asp:View ID="ViewNonSwiftOutIdenPenerimaNonNasabah" runat="server">
                                                           <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Labeler12" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('DNonSwiftOutIdentitasPenerimaNonNasabah','Imgg10','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Imgg10" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" /></a>
                                                        </td>
                                                    </tr>
                                       </table>
                                            </td>
                                        </tr>
                                        <tr id="DNonSwiftOutIdentitasPenerimaNonNasabah">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                             
                                                </table><table style="width: 98%; height: 49px">
                                                    <tr>
                                                        <td style="width: 30%; background-color: #fff7e6">
                                                            <asp:Label ID="Labelt9311" runat="server" Text="a. Kode Rahasia "></asp:Label>
                                                        </td>
                                                        <td style="width: 204px">
                                                            <asp:TextBox ID="txtPenerimaNonSwiftOutNonNasabah_KodeRahasia" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 35px">
                                                            &nbsp;</td>
                                                        <td style="width: 47px">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; background-color: #fff7e6">
                                                            <asp:Label ID="Label163" runat="server" Text="b. No. Rek "></asp:Label>
                                                        </td>
                                                        <td style="width: 204px">
                                                            <asp:TextBox ID="txtPenerimaNonSwiftOutNonNasabah_rekening" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; background-color: #fff7e6">
                                                            <asp:Label ID="Label9272" runat="server" Text="c. Nama Bank"></asp:Label>
                                                        </td>
                                                        <td style="width: 204px">
                                                            <asp:TextBox ID="txtPenerimaNonSwiftOutNonNasabah_namabank" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="255" TabIndex="2" Width="125px"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 35px">
                                                            &nbsp;</td>
                                                        <td style="width: 47px">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; background-color: #fff7e6">
                                                            <asp:Label ID="Label1146" runat="server" Text="d. Nama Lengkap"></asp:Label>
                                                            <asp:Label ID="Label1147" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="width: 204px">
                                                            <asp:TextBox ID="txtPenerimaNonSwiftOutNonNasabah_Nama" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; background-color: #fff7e6">
                                                            <asp:Label ID="Label210" runat="server" 
                                                                Text="e. Alamat Sesuai Bukti Identitas/ Voucher" Height="14px" 
                                                                Width="242px"></asp:Label>
                                                            </td>
                                                        <td style="width: 204px">
                                                            &nbsp;</td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; background-color: #fff7e6">
                                                            <asp:Label ID="Label1150" runat="server" Text="Alamat"></asp:Label></td>
                                                        <td style="width: 204px">
                                                            <asp:TextBox ID="txtPenerimaNonSwiftOutNonNasabah_Alamat" runat="server" CssClass="searcheditbox" MaxLength="100"
                                                                TabIndex="2" Width="421px"></asp:TextBox></td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; background-color: #fff7e6">
                                                            <asp:Label ID="Label2g11" runat="server" Text="Negara"></asp:Label>
                                                            <asp:Label ID="Label165" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="width: 204px">
                                                            &nbsp;<table style="width: 127px">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;<asp:TextBox ID="txtPenerimaNonSwiftOutNonNasabah_Negara" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 68px">
                                                                            <asp:ImageButton ID="ImageButton_PenerimaNonSwiftOutNonNasabah_Negara" runat="server" ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinNegara();" /></td>
                                                                        <td style="width: 68px">
                                                                            <asp:ImageButton ID="RMPenerimaNonSwiftOutNonNasabah_Negara" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <asp:HiddenField ID="hfSwiftInNonSwiftOutPengirimNonNasabah_Negara" runat="server" />
                                                        </td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr id="trPenerimaNonSwiftOutNonNasabah_negaraLain" runat="server" visible="false">
                                                        <td style="width: 30%; background-color: #fff7e6">
                                                            <asp:Label ID="Labelg212" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                        <td style="width: 204px">
                                                            <asp:TextBox ID="txtPenerimaNonSwiftOutNonNasabah_negaraLain" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                        <td style="width: 35px">
                                                        </td>
                                                        <td style="width: 47px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; background-color: #fff7e6">
                                                            &nbsp;</td>
                                                        <td style="width: 204px">
                                                            &nbsp;</td>
                                                        <td style="width: 35px">
                                                            &nbsp;</td>
                                                        <td style="width: 47px">
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                                    </asp:View>
                                                </asp:MultiView></td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" align="left"
                                                style="height: 6px; width: 100%;">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Labelkl13" runat="server" Text="E. Transaksi" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('ENonSwiftOuttransaksi','Imgtt11','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Imgtt11" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:Label ID="Label215" runat="server" Font-Italic="True" Text="(Diisi oleh PJK Bank  yang bertindak sebagai Penyelenggara Pengirim Asal atau sebagai Penyelenggara Penerus)"></asp:Label></td>
                                        </tr>
                                        <tr id="ENonSwiftOuttransaksi">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                             
                                                </table><table style="width: 100%; height: 49px">
                                                    <tr>
                                                        <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                            <asp:Label ID="Labeld216" runat="server" Text="a. Tanggal Transaksi (tgl/bln/thn) "></asp:Label>
                                                            <asp:Label ID="Labeld217" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="Transaksi_NonSwiftOut_tanggal" runat="server" 
                                                                CssClass="searcheditbox" TabIndex="2"
                                                                Width="122px" Enabled="False"></asp:TextBox>
                                                            <input id="popUpTanggalTransaksi_NonSwiftOut_Transaksi" runat="server"
                                                            onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')" style="border-right: #ffffff 0px solid;
                                                            border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                            border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                            height: 17px" title="Click to show calendar" type="button" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Labelt235" runat="server" 
                                                                Text="b. Kantor Cabang Penyelenggara Pengirim Asal"></asp:Label>
                                                            </td>
                                                        <td>
                                                            <asp:TextBox ID="Transaksi_NonSwiftOut_kantorCabangPengirim" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Labelt237" runat="server" 
                                                                Text=" c.  Value Date/Currency/Interbank Settled Amount"></asp:Label></td>
                                                        <td>
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Labelt238" runat="server" Text="Tanggal Transaksi (Value Date)"></asp:Label>
                                                            <asp:Label ID="Labelt280" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="Transaksi_NonSwiftOut_ValueTanggalTransaksi" runat="server" 
                                                                CssClass="searcheditbox" TabIndex="2"
                                                                Width="122px" Enabled="False"></asp:TextBox>
                                                            <input id="popUpTanggalTransaksi_NonSwiftOut_ValueDate" runat="server"
                                                            onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')" style="border-right: #ffffff 0px solid;
                                                            border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                            border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                            height: 17px" title="Click to show calendar" type="button" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                            <asp:Label ID="Labelt239" runat="server" Text="Nilai Transaksi (Amount of The EFT)"></asp:Label>
                                                            <asp:Label ID="Mand_lbh100jt1" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="Transaksi_NonSwiftOut_nilaitransaksi" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                            <asp:Label ID="Labelt240" runat="server" Text="Mata Uang Transaksi (currency of the EFT)"></asp:Label>
                                                            <asp:Label ID="Labelt281" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td>
                                                            &nbsp;
                                                            <table style="width: 127px">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                            <asp:TextBox ID="Transaksi_NonSwiftOut_MataUangTransaksi" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;</td>
                                                                        <td style="width: 68px">
                                                            <asp:ImageButton ID="ImageButton_Transaksi_NonSwiftOut_MataUangTransaksi" 
                                                                runat="server" ImageUrl="~/Images/button/browse.gif" 
                                                                OnClientClick="javascript:popWinMataUang();" /></td>
                                                                        <td style="width: 68px">
                                                                            <asp:ImageButton ID="RMTransaksi_NonSwiftOut_MataUangTransaksi" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <asp:HiddenField ID="hfTransaksi_NonSwiftOut_MataUangTransaksi" 
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr ID="trNonSwiftOut_MataUangTransaksiLain"  runat="server" visible="false">
                                                        <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                            <asp:Label ID="Label1" runat="server" Text="Mata Uang Transaksi Lainnya"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="Transaksi_NonSwiftOut_MataUangTransaksiLain" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Labelt243" runat="server" Text="Amount dalam Rupiah (Kurs yang digunakan Bank) "></asp:Label>
                                                            <asp:Label ID="Mand_lbh100jt2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="Transaksi_NonSwiftOut_AmountdalamRupiah" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Labelt244" runat="server" Text="d. Currency/Instructed Amount"></asp:Label></td>
                                                        <td>
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Label9298" runat="server" Text="Mata Uang yang diinstruksikan"></asp:Label></td>
                                                        <td>
                                                            &nbsp;
                                                            <table style="width: 127px">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                            <asp:TextBox ID="Transaksi_NonSwiftOut_currency" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;</td>
                                                                        <td style="width: 68px">
                                                            <asp:ImageButton ID="ImageButton_Transaksi_NonSwiftOut_currency" runat="server" 
                                                                ImageUrl="~/Images/button/browse.gif" 
                                                                OnClientClick="javascript:popWinMataUang();" /></td>
                                                                        <td style="width: 68px">
                                                                            <asp:ImageButton ID="RMTransaksi_NonSwiftOut_currency" runat="server" ImageUrl="~/Images/button/remove.gif" /></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <asp:HiddenField ID="hfTransaksi_NonSwiftOut_currency" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr ID="trNonSwiftOut_currencyLain"  runat="server" visible="false">
                                                        <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                            <asp:Label ID="Label2" runat="server" Text="Mata Uang Lainnya"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="Transaksi_NonSwiftOut_currencyLain" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Labelt246" runat="server" Text="Nilai Transaksi Keuangan yang diinstruksikan"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="Transaksi_NonSwiftOut_instructedAmount" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox>
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                            <asp:Label ID="Labelt251" runat="server" Text="e. Tujuan Transaksi "></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="Transaksi_NonSwiftOut_TujuanTransaksi" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="100"
                                                                TabIndex="2" Width="295px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 38%; background-color: #fff7e6;">
                                                            <asp:Label ID="Labelt252" runat="server" Text="f. Sumber Penggunaan Dana "></asp:Label>
                                                            <asp:Label ID="Label19" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="Transaksi_NonSwiftOut_SumberPenggunaanDana" runat="server" CssClass="searcheditbox" MaxLength="100"
                                                                TabIndex="2" Width="296px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>  
                                    </asp:View>
                                    <asp:View ID="vwMessage" runat="server">
                                            <table width="100%" >
                                                <tr>
                                                    <td class="formtext" align="center" style="height: 15px">
                                                        <asp:Label runat="server" ID="lblMsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="formtext" align="center">
                                                        <asp:ImageButton ID="imgOKMsg" runat="server"  ImageUrl="~/Images/button/Ok.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </asp:MultiView>                                                                 
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <%-- <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="Images/blank.gif" width="5" height="1" /></td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="images/arrow.gif" width="15" height="15" /></td>
                        <td width="99%" background="Images/button-bground.gif">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td>
                            </td>
                    </tr>
                </table>--%>
                 <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator><ajax:AjaxPanel ID="AjaxPanel3" runat="server"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" background="Images/button-bground.gif" valign="middle">
                                <img height="1" src="Images/blank.gif" width="5" /></td>
                            <td align="left" background="Images/button-bground.gif" valign="middle">
                                <img height="15" src="images/arrow.gif" width="15" />
                            </td>
                            <td background="Images/button-bground.gif" style="width: 5px">
                                </td>
                            <td background="Images/button-bground.gif">
                                <asp:ImageButton ID="ImageButtonSave" runat="server" ImageUrl="~/Images/Button/Save.gif"
                                    />
                            </td>
                            <td background="Images/button-bground.gif" style="width: 62px">
                                <asp:ImageButton ID="ImageButtonCancel" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/Cancel.gif"
                                     />
                            </td>
                            <td background="Images/button-bground.gif" width="99%">
                                <img height="1" src="Images/blank.gif" width="1" /></td>
                            <td>
                                </td>
                        </tr>
                   
                        </ajax:AjaxPanel>
            </td>
        </tr>
    </table>
    <%-- <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator><ajax:AjaxPanel ID="AjaxPanel2" runat="server"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" background="Images/button-bground.gif" valign="middle">
                                <img height="1" src="Images/blank.gif" width="5" /></td>
                            <td align="left" background="Images/button-bground.gif" valign="middle">
                                <img height="15" src="images/arrow.gif" width="15" />
                            </td>
                            <td background="Images/button-bground.gif" style="width: 5px">
                                </td>
                            <td background="Images/button-bground.gif">
                                <asp:ImageButton ID="ImageButtonSave" runat="server" ImageUrl="~/Images/Button/Save.gif"
                                    SkinID="AddButton" />
                            </td>
                            <td background="Images/button-bground.gif" style="width: 62px">
                                <asp:ImageButton ID="ImageButtonCancel" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/Cancel.gif"
                                    SkinID="CancelButton" />
                            </td>
                            <td background="Images/button-bground.gif" width="99%">
                                <img height="1" src="Images/blank.gif" width="1" /></td>
                            <td>
                                </td>
                        </tr>
                   
                        </ajax:AjaxPanel>--%>
</asp:Content>

