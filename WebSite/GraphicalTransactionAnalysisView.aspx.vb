
Imports System.Data

Public Class AccountGraphicShape
    Public strBackgroundColor As String  '#ff00ff
    Public strFontColor As String
    Public strBorderShape As String
End Class

Public Class AccountNode

    Public AccountNo As String
    Public AccountName As String
    Public BankName As String
    Public CIFNo As String

    Public TransactionDateForRank As Date
    Public Shape As AccountGraphicShape

    Public DebitOrCredit As String

    Public IsAccountNo As Boolean = True
End Class
Public Class TransactionDetail

    Public IsSingleEntity As Boolean = False
    Public AccountSource As AccountNode
    Public AccountDestination As AccountNode

    Public Amount As Double
    Public TransactionTimestamp As Date
    Public TransactionCode As String
    Public TransactionDescription As String
    Public StrTicketNo As String


    Public Function ShallowCopy() As TransactionDetail
        Return CType(Me.MemberwiseClone, TransactionDetail)
    End Function

End Class

Partial Class GraphicalTransactionAnalysisView
    Inherits System.Web.UI.Page

    Private arColor As String() = {"FFAD9D", "ACB4C0", "8D97DA", "C7CCED", "9afd84", "d8f6ae", "4ec189", "a7e1c5", "a7b39d", "a687db", "c4b0e7", "9f9466", "c0b899", "dfdbcc", "f9ff40", "fbff80", "67fb46", "c3c257", "d7d78f"}
    Private arBorderStyle As String() = {"filled"}
    Private arFontColor As String() = {"black", "white"}

    'Protected Sub FillAuxCode()
    '    Using adapter As New AMLDAL.TransactionAnalysisTableAdapters.TLTXTableAdapter
    '        Using table As AMLDAL.TransactionAnalysis.TLTXDataTable = adapter.GetData
    '            lstAllTrxCode.DataSource = table
    '            lstAllTrxCode.DataValueField = "TLTXCD"
    '            lstAllTrxCode.DataTextField = "TLTXDS"
    '            lstAllTrxCode.DataBind()
    '        End Using
    '    End Using
    'End Sub

    Protected Function ConstructAccountNodeIndex(ByVal oAccountNode As AccountNode) As String
        Return ConstructAccountNodeIndex(oAccountNode.AccountNo, oAccountNode.TransactionDateForRank)
    End Function
    Protected Function ConstructAccountNodeIndex(ByVal strAccountNo As String, ByVal dtTransactionDate As Date) As String
        Return "Acc" & CStr(strAccountNo).Trim & "_" & dtTransactionDate.ToString("yyyyMMdd")
    End Function

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("GraphicalTransactionAnalysisView.SetnGetCurrentPage") Is Nothing, 0, Session("GraphicalTransactionAnalysisView.SetnGetCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("GraphicalTransactionAnalysisView.SetnGetCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Math.Ceiling(Me.SetnGetRowTotal / 10)

        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("GraphicalTransactionAnalysisView.SetnGetRowTotal") Is Nothing, 0, Session("GraphicalTransactionAnalysisView.SetnGetRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("GraphicalTransactionAnalysisView.SetnGetRowTotal") = Value
        End Set
    End Property
    Protected Function BuildGraphLanguange(ByVal AllTransactions As ArrayList, ByVal AllAccountNode As Hashtable) As String
        Dim sbCommand As New StringBuilder
        sbCommand.Append("Digraph LinkAnalysis {" & vbCrLf)
        sbCommand.Append("  ranksep=1.0; size = ""7.5,7.5""; " & vbCrLf)

        Dim dtTrx As New DataTable
        Dim dc As DataColumn
        Dim dckey(1) As DataColumn
        dc = dtTrx.Columns.Add("TrxCode", GetType(System.String))
        dckey(0) = dc
        dtTrx.Columns.Add("TrxDesc", GetType(System.String))
        dtTrx.PrimaryKey = dckey
        'Dim ArrTrx As New Hashtable

        Dim sbTrx As New StringBuilder
        Dim sbTrxRank As New StringBuilder
        Dim arTmpDate As New ArrayList(AllTransactions.Count)
        For i As Integer = 0 To AllTransactions.Count - 1
            Dim oTransactionDetail As TransactionDetail = CType(AllTransactions.Item(i), TransactionDetail)

            If dtTrx.Rows.Find(oTransactionDetail.TransactionCode) Is Nothing Then
                Dim dr As DataRow = dtTrx.NewRow
                dr.Item("TrxCode") = oTransactionDetail.TransactionCode
                dr.Item("TrxDesc") = oTransactionDetail.TransactionDescription
                dtTrx.Rows.Add(dr)
            End If
            'If Not ArrTrx.ContainsKey(oTransactionDetail.TransactionCode) Then
            '    ArrTrx.Add(oTransactionDetail.TransactionCode, oTransactionDetail.TransactionDescription)
            'End If

            Dim Amount As Double = oTransactionDetail.Amount / 1000000.0
            sbTrx.Append("     " & ConstructAccountNodeIndex(oTransactionDetail.AccountSource) & " -> " & ConstructAccountNodeIndex(oTransactionDetail.AccountDestination) & " [font=""Arial Narrow"",fontsize=""10"",label=""Trx: " & oTransactionDetail.TransactionCode & "\n" & Format(Amount, "0.0") & """]; " & vbCrLf)

            sbTrxRank.Append("       { rank = same; " & oTransactionDetail.AccountSource.TransactionDateForRank.ToString("yyyyMMdd") & "; " & ConstructAccountNodeIndex(oTransactionDetail.AccountSource) & "}" & vbCrLf)
            sbTrxRank.Append("       { rank = same; " & oTransactionDetail.AccountDestination.TransactionDateForRank.ToString("yyyyMMdd") & "; " & ConstructAccountNodeIndex(oTransactionDetail.AccountDestination) & "}" & vbCrLf)
            If arTmpDate.IndexOf(oTransactionDetail.AccountSource.TransactionDateForRank) < 0 Then
                arTmpDate.Add(oTransactionDetail.AccountSource.TransactionDateForRank)
            End If
            If arTmpDate.IndexOf(oTransactionDetail.AccountDestination.TransactionDateForRank) < 0 Then
                arTmpDate.Add(oTransactionDetail.AccountDestination.TransactionDateForRank)
            End If
        Next
        arTmpDate.Sort()

        'gambar tanggal
        For i As Integer = 0 To arTmpDate.Count - 1
            Dim dt As Date = CDate(arTmpDate.Item(i))
            If dt.Year = 1900 OrElse dt.Year = 2100 Then
                sbCommand.Append("   " & dt.ToString("yyyyMMdd") & " [shape=""plaintext"",font=""Arial Narrow"",fontsize=""10"",label=""""];" & vbCrLf)
            Else
                sbCommand.Append("   " & dt.ToString("yyyyMMdd") & " [shape=""plaintext"",font=""Arial Narrow"",fontsize=""10"",label=""" & dt.ToString("dd-MMM-yyyy") & """];" & vbCrLf)
            End If

        Next
        If arTmpDate.Count = 1 Then
            Dim dt As Date = CDate(arTmpDate.Item(0))
            sbCommand.Append("    " & dt.ToString("yyyyMMdd"))
            sbCommand.Append(";" & vbCrLf)
        Else
            For i As Integer = 0 To arTmpDate.Count - 2
                Dim dt As Date = CDate(arTmpDate.Item(i))
                If (arTmpDate.Count - 1 > i) Then
                    sbCommand.Append("    " & dt.ToString("yyyyMMdd"))
                    sbCommand.Append(" -> ")
                    sbCommand.Append(CDate(arTmpDate.Item(i + 1)).ToString("yyyyMMdd"))
                    If (i = 0) AndAlso dt.Year = 1900 Then
                        sbCommand.Append(" [color=white]")
                    ElseIf CDate(arTmpDate.Item(i + 1)).Year = 2100 Then
                        sbCommand.Append(" [color=white]")
                    End If
                    sbCommand.Append(";" & vbCrLf)
                End If
            Next
        End If
        

        'gambar seluruh account node
        'Dim ArrAccounts As New Hashtable
        Dim dtAccounts As New DataTable
        dc = dtAccounts.Columns.Add("AccountNo", GetType(System.String))
        dckey(0) = dc
        dtAccounts.PrimaryKey = dckey
        dtAccounts.Columns.Add("AccountName", GetType(System.String))


        Dim oEnumerator As IDictionaryEnumerator = AllAccountNode.GetEnumerator()
        While (oEnumerator.MoveNext)
            Dim oAccountNode As AccountNode = CType(oEnumerator.Value, AccountNode)
            'nama index nya = AccountNo + date
            sbCommand.Append("     " & ConstructAccountNodeIndex(oAccountNode))
            If oAccountNode.IsAccountNo Then
                sbCommand.Append(" [font=""Arial Narrow"",fontsize=""10"",color=""#" & oAccountNode.Shape.strBackgroundColor & """ fillcolor=""#" & oAccountNode.Shape.strBackgroundColor & """,style=""" & oAccountNode.Shape.strBorderShape & """,fontcolor=""" & oAccountNode.Shape.strFontColor & """,label=""" & (oAccountNode.BankName & vbCrLf & oAccountNode.CIFNo & vbCrLf & oAccountNode.AccountNo).Trim.Replace(vbCrLf, "\n") & """,URL=""javascript:ShowMenu('" & ConstructAccountNodeIndex(oAccountNode) & "')""];" & vbCrLf)
                If dtAccounts.Rows.Find(oAccountNode.AccountNo) Is Nothing Then
                    Dim dr As DataRow = dtAccounts.NewRow
                    dr.Item("AccountNo") = oAccountNode.AccountNo
                    dr.Item("AccountName") = oAccountNode.AccountName
                    dtAccounts.Rows.Add(dr)
                End If
                'If Not ArrAccounts.ContainsKey(oAccountNode.AccountNo) Then
                '    ArrAccounts.Add(oAccountNode.AccountNo, oAccountNode.AccountName)
                'End If
            Else
                sbCommand.Append(" [font=""Arial Narrow"",fontsize=""10"",color=""#" & oAccountNode.Shape.strBackgroundColor & """ fillcolor=""#" & oAccountNode.Shape.strBackgroundColor & """,style=""" & oAccountNode.Shape.strBorderShape & """,fontcolor=""" & oAccountNode.Shape.strFontColor & """,label=""" & (oAccountNode.BankName & vbCrLf & oAccountNode.CIFNo & vbCrLf & oAccountNode.AccountNo).Trim.Replace(vbCrLf, "\n") & """];" & vbCrLf)
            End If
            'sbCommand.Append(" [font=""Arial Narrow"",fontsize=""10"",color=""#" & oAccountNode.Shape.strBackgroundColor & """ fillcolor=""#" & oAccountNode.Shape.strBackgroundColor & """,style=""" & oAccountNode.Shape.strBorderShape & """,fontcolor=""" & oAccountNode.Shape.strFontColor & """,label=""" & (oAccountNode.BankName & vbCrLf & oAccountNode.CIFNo & vbCrLf & oAccountNode.AccountNo).Trim.Replace(vbCrLf, "\n") & """,URL=""javascript:ShowMenu('" & ConstructAccountNodeIndex(oAccountNode) & "')""];" & vbCrLf)
        End While
        GridViewAccountLegend.DataSource = dtAccounts
        GridViewAccountLegend.DataBind()
        GridViewAuxTransactionCodeLegend.DataSource = dtTrx
        GridViewAuxTransactionCodeLegend.DataBind()

        sbCommand.Append(sbTrx.ToString)
        sbCommand.Append(sbTrxRank.ToString)
        sbCommand.Append("}" & vbCrLf)
        Return sbCommand.ToString
    End Function


    Protected Function BuildData(ByVal strAccountNo As String, ByVal ExpandLink As Boolean, ByVal dtStartDate As Date, ByVal dtEndDate As Date, ByVal arAuxCode As String(), ByVal lngAmountStart As Long, ByVal lngAmountEnd As Long, ByRef bolDataExists As Boolean, ByVal intPageIndex As Integer, ByVal intPageSize As Integer) As String
        Dim sbQuery As New StringBuilder

        Dim arAllTransactions As ArrayList
        Dim htAccountGraphicShape As Hashtable 'buat gambar di grafik nya
        Dim htAccountNode As Hashtable 'node asli nya, index: AccountNo + date
        Dim oTransactionDetail As TransactionDetail
        Dim dsResult As DataSet
        Dim intpagelow As Integer
        Dim intpagehigh As Integer

        intpagelow = intPageSize * intPageIndex
        intpagehigh = intpagelow + intPageSize

        arAllTransactions = CType(Session("GraphicalTransactionAnalysisView.arAllTransactions"), ArrayList)
        If arAllTransactions Is Nothing Then
            arAllTransactions = New ArrayList
        End If

        htAccountGraphicShape = CType(Session("GraphicalTransactionAnalysisView.htAccountGraphicShape"), Hashtable)
        If htAccountGraphicShape Is Nothing Then
            htAccountGraphicShape = New Hashtable
        End If

        htAccountNode = CType(Session("GraphicalTransactionAnalysisView.htAccountNode"), Hashtable)
        If htAccountNode Is Nothing Then
            htAccountNode = New Hashtable
        End If

        'build sqlquery
        Dim strqueryTotal As New StringBuilder

        Try

            sbQuery.Append("SELECT distinct TransactionDetail_1.TransactionDetailId, TrxTicketNo.TransactionTicketNo, TransactionDetail_1.TransactionDate, TransactionDetail_1.BankName, " & vbCrLf)
            sbQuery.Append("        TransactionDetail_1.CIFNo, TransactionDetail_1.AccountName, CASE WHEN  AccountNo =0 THEN convert(varchar,AccountNo)+'_'+ REPLACE( ltrim(rtrim(AccountName)),' ','_') ELSE convert(Varchar,AccountNo) END AS AccountNo   , TransactionDetail_1.TransactionLocalEquivalent, " & vbCrLf)
            sbQuery.Append("       TransactionDetail_1.DebitORCredit, TransactionDetail_1.AuxiliaryTransactionCode, vw_AuxTransactionCode.TransactionCode AS TLTXCD, vw_AuxTransactionCode.TransactionDescription AS TLTXDS " & vbCrLf)
            sbQuery.Append("FROM    ( SELECT top  " & intpagehigh & " alldata.RowIndex,alldata.TransactionTicketNo    FROM (  SELECT  ROW_NUMBER() OVER (ORDER BY transactiondate) as RowIndex,    TransactionTicketNo " & vbCrLf)

            strqueryTotal.Append("SELECT count(1) " & vbCrLf)


            sbQuery.Append("        FROM          TransactionDetail " & vbCrLf)
            strqueryTotal.Append("        FROM          TransactionDetail " & vbCrLf)
            If ExpandLink Then
                strAccountNo = strAccountNo.Substring(3, strAccountNo.IndexOf("_") - 3) 'Acc9999999999991_20070914
            End If
            sbQuery.Append("        WHERE      (Cast(AccountNo as Varchar(50)) = '" & strAccountNo & "') AND (TransactionDate BETWEEN '" & dtStartDate.ToString("yyyy-MM-dd") & "' AND '" & dtEndDate.ToString("yyyy-MM-dd") & "') " & vbCrLf)
            strqueryTotal.Append("  WHERE      (Cast(AccountNo as Varchar(50)) = '" & strAccountNo & "') AND (TransactionDate BETWEEN '" & dtStartDate.ToString("yyyy-MM-dd") & "' AND '" & dtEndDate.ToString("yyyy-MM-dd") & "') " & vbCrLf)

            If arAuxCode IsNot Nothing AndAlso arAuxCode.Length > 0 Then
                sbQuery.Append(" AND (AuxiliaryTransactionCode IN (")
                strqueryTotal.Append(" AND (AuxiliaryTransactionCode IN (")

                For i As Integer = 0 To arAuxCode.Length - 1
                    sbQuery.Append("'" & arAuxCode(i).Replace("'", "''") & "',")
                    strqueryTotal.Append("'" & arAuxCode(i).Replace("'", "''") & "',")
                Next
                sbQuery.Remove(sbQuery.Length - 1, 1)
                strqueryTotal.Remove(sbQuery.Length - 1, 1)

                sbQuery.Append(")) " & vbCrLf)
                strqueryTotal.Append(")) " & vbCrLf)
            End If

            If lngAmountStart <> Nothing AndAlso lngAmountEnd <> Nothing AndAlso lngAmountStart <= lngAmountStart Then
                sbQuery.Append(" AND ( TransactionLocalEquivalent BETWEEN " & lngAmountStart & " and " & lngAmountEnd & " ) ")
                strqueryTotal.Append(" AND ( TransactionLocalEquivalent BETWEEN " & lngAmountStart & " and " & lngAmountEnd & " ) ")
            End If

            If ExpandLink AndAlso arAllTransactions.Count > 0 Then
                sbQuery.Append("    AND ( TransactionTicketNo NOT IN( ")
                strqueryTotal.Append("    AND ( TransactionTicketNo NOT IN( ")
                For i As Integer = 0 To arAllTransactions.Count - 1
                    Dim oTrxDetail As TransactionDetail = CType(arAllTransactions.Item(i), TransactionDetail)
                    sbQuery.Append("'" & oTrxDetail.StrTicketNo & "',")
                    strqueryTotal.Append("'" & oTrxDetail.StrTicketNo & "',")
                Next
                sbQuery.Remove(sbQuery.Length - 1, 1)
                strqueryTotal.Remove(sbQuery.Length - 1, 1)
                sbQuery.Append("))" & vbCrLf)
                strqueryTotal.Append("))" & vbCrLf)
                'AND (TransactionTicketNo NOT IN('2333','333')
            End If

            sbQuery.Append("        )alldata WHERE alldata.RowIndex>" & intpagelow & " AND alldata.RowIndex<=" & intpagehigh & "" & vbCrLf)
            sbQuery.Append("        ) " & vbCrLf)
            sbQuery.Append("       AS TrxTicketNo INNER JOIN " & vbCrLf)
            sbQuery.Append("       TransactionDetail AS TransactionDetail_1 ON TrxTicketNo.TransactionTicketNo = TransactionDetail_1.TransactionTicketNo INNER JOIN " & vbCrLf)
            sbQuery.Append("       vw_AuxTransactionCode ON TransactionDetail_1.AuxiliaryTransactionCode = vw_AuxTransactionCode.TransactionCode " & vbCrLf)
            'sbQuery.Append("    where TransactionDetail_1.AccountNo=" & strAccountNo)
            sbQuery.Append("       ORDER BY TransactionDetail_1.TransactionDate, TransactionDetail_1.DebitORCredit DESC ")

            Me.SetnGetRowTotal = SahassaNettier.Data.DataRepository.Provider.ExecuteScalar(CommandType.Text, strqueryTotal.ToString)

            'Using adapter As New AMLDAL.TransactionAnalysisTableAdapters.SelectTransactionTableAdapter
            dsResult = Sahassa.AML.Commonly.ExecuteDataset(sbQuery.ToString)

            Dim intTipeTransaksi As TransactionType
            Dim counter As Integer = 0
            If dsResult.Tables.Count > 0 AndAlso dsResult.Tables(0).Rows.Count > 0 Then
                Dim currentTicketNo As String = dsResult.Tables(0).Rows(counter).Item("TransactionTicketNo").ToString
                While counter < dsResult.Tables(0).Rows.Count

                    'itung dulu credit n debit nya
                    Dim counterLast As Integer = counter
                    Dim intTotalCredit As Integer = 0
                    Dim intTotalDebit As Integer = 0
                    While counterLast < dsResult.Tables(0).Rows.Count
                        If dsResult.Tables(0).Rows(counterLast).Item("DebitORCredit").ToString() = "C" Then
                            intTotalCredit += 1
                        Else
                            intTotalDebit += 1
                        End If
                        counterLast += 1
                        If counterLast < dsResult.Tables(0).Rows.Count AndAlso dsResult.Tables(0).Rows(counterLast).Item("TransactionTicketNo").ToString() <> currentTicketNo Then
                            Exit While
                        End If
                    End While
                    counterLast -= 1

                    If intTotalCredit >= 1 AndAlso intTotalDebit = 0 Then
                        intTipeTransaksi = TransactionType.CreditOnly
                    ElseIf intTotalCredit = 0 AndAlso intTotalDebit >= 1 Then
                        intTipeTransaksi = TransactionType.DebitOnly
                    ElseIf intTotalCredit > 1 AndAlso intTotalDebit = 1 Then
                        intTipeTransaksi = TransactionType.SingleDebitMultiCredit
                    ElseIf intTotalCredit = 1 AndAlso intTotalDebit > 1 Then
                        intTipeTransaksi = TransactionType.MultiDebitSingleCredit
                    Else
                        intTipeTransaksi = TransactionType.SingleDebitSingleCredit
                    End If

                    If intTipeTransaksi = TransactionType.MultiDebitSingleCredit OrElse intTipeTransaksi = TransactionType.DebitOnly Then
                        Dim counterDownTo As Integer = counterLast
                        Dim intPositionToInsert As Integer = -1
                        '-1 means append to arTransactionDetail
                        'non -1 means insert to arTransactionDetail
                        'karena diposisi mana kita insert itu tergantung kepada credit nya (node account penerima)
                        While counterDownTo >= counter
                            Dim row As DataRow = dsResult.Tables(0).Rows(counterDownTo)
                            'D
                            'D
                            'C --> kita looping nya dari bawah
                            If (htAccountGraphicShape.Item(CStr(row("AccountNo")).Trim) Is Nothing) Then
                                Dim oAccountGraphicShape As New AccountGraphicShape

                                Dim intBorderStyleIndex As Integer = CInt(Math.Floor(htAccountGraphicShape.Count / arColor.Length))
                                intBorderStyleIndex = intBorderStyleIndex Mod arBorderStyle.Length
                                oAccountGraphicShape.strBorderShape = arBorderStyle(intBorderStyleIndex)

                                Dim intColorIndex As Integer = htAccountGraphicShape.Count Mod arColor.Length
                                oAccountGraphicShape.strBackgroundColor = arColor(intColorIndex)

                                oAccountGraphicShape.strFontColor = arFontColor(0)

                                htAccountGraphicShape.Add(CStr(row("AccountNo")).Trim, oAccountGraphicShape)
                            End If

                            If row("DebitORCredit").ToString = "D" Then

                                If oTransactionDetail Is Nothing Then
                                    oTransactionDetail = New TransactionDetail
                                End If
                                Dim oTransDetailToSave As TransactionDetail = oTransactionDetail.ShallowCopy()
                                If oTransDetailToSave.AccountDestination Is Nothing Then
                                    oTransDetailToSave.IsSingleEntity = True
                                    'tarik tunai

                                    Dim oAccountNode As New AccountNode
                                    oAccountNode.IsAccountNo = False
                                    oAccountNode.AccountNo = row("TLTXDS").ToString.Trim
                                    oAccountNode.AccountNo = oAccountNode.AccountNo.Replace("/", "")
                                    oAccountNode.AccountNo = Regex.Replace(oAccountNode.AccountNo, "\W", " ")
                                    oAccountNode.AccountNo = Regex.Replace(oAccountNode.AccountNo, "\s+", " ")
                                    oAccountNode.AccountNo = Regex.Replace(oAccountNode.AccountNo, "\s", "_")
                                    oAccountNode.TransactionDateForRank = New Date(2100, 1, 1)
                                    oAccountNode.DebitOrCredit = "D"

                                    If htAccountGraphicShape.Item(oAccountNode.AccountNo) Is Nothing Then
                                        Dim oAccountGraphicShape As New AccountGraphicShape

                                        Dim intBorderStyleIndex As Integer = CInt(Math.Floor(htAccountGraphicShape.Count / arColor.Length))
                                        intBorderStyleIndex = intBorderStyleIndex Mod arBorderStyle.Length
                                        oAccountGraphicShape.strBorderShape = arBorderStyle(intBorderStyleIndex)

                                        Dim intColorIndex As Integer = htAccountGraphicShape.Count Mod arColor.Length
                                        oAccountGraphicShape.strBackgroundColor = arColor(intColorIndex)

                                        oAccountGraphicShape.strFontColor = arFontColor(0)

                                        htAccountGraphicShape.Add(oAccountNode.AccountNo, oAccountGraphicShape)

                                        htAccountNode.Add(ConstructAccountNodeIndex(oAccountNode), oAccountNode)
                                    End If
                                    oAccountNode.Shape = CType(htAccountGraphicShape.Item(oAccountNode.AccountNo), AccountGraphicShape)   'ga mungkin nothin
                                    oTransDetailToSave.AccountDestination = oAccountNode
                                End If

                                oTransDetailToSave.Amount = CType(row("TransactionLocalEquivalent"), Double)
                                oTransDetailToSave.TransactionCode = row("AuxiliaryTransactionCode").ToString
                                oTransDetailToSave.TransactionDescription = row("TLTXDS").ToString
                                oTransDetailToSave.TransactionTimestamp = CDate(row("TransactionDate"))
                                oTransDetailToSave.StrTicketNo = CStr(row("TransactionTicketNo"))

                                Dim i As Integer = arAllTransactions.Count - 1
                                Dim bolFound As Boolean = False
                                While i >= 0
                                    Dim existingTrxNode As TransactionDetail = CType(arAllTransactions.Item(i), TransactionDetail)
                                    Dim dtTrxDate As Date = CDate(row("TransactionDate"))
                                    Dim dtForRank As New Date(dtTrxDate.Year, dtTrxDate.Month, dtTrxDate.Day)
                                    If oTransDetailToSave.IsSingleEntity Then
                                        If existingTrxNode.AccountSource.AccountNo = CStr(row("AccountNo")).ToString AndAlso existingTrxNode.AccountSource.TransactionDateForRank = dtForRank Then
                                            oTransDetailToSave.AccountSource = existingTrxNode.AccountSource
                                            bolFound = True
                                            Exit While
                                        ElseIf existingTrxNode.AccountDestination.AccountNo = CStr(row("AccountNo")).ToString AndAlso existingTrxNode.AccountDestination.TransactionDateForRank = dtForRank Then
                                            oTransDetailToSave.AccountSource = existingTrxNode.AccountDestination
                                            bolFound = True
                                            Exit While
                                        End If
                                    Else
                                        If existingTrxNode.AccountSource.AccountNo = CStr(row("AccountNo")).ToString AndAlso existingTrxNode.AccountSource.TransactionDateForRank < dtForRank Then
                                            oTransDetailToSave.AccountSource = existingTrxNode.AccountSource
                                            bolFound = True
                                            Exit While
                                        ElseIf existingTrxNode.AccountDestination.AccountNo = CStr(row("AccountNo")).ToString AndAlso existingTrxNode.AccountDestination.TransactionDateForRank < dtForRank Then
                                            oTransDetailToSave.AccountSource = existingTrxNode.AccountDestination
                                            bolFound = True
                                            Exit While
                                        End If
                                    End If

                                    i -= 1
                                End While
                                If Not bolFound Then
                                    If oTransDetailToSave.IsSingleEntity Then
                                        'gambar baru di posisi rank tanggal transaksi (tarik tunai)
                                        Dim oAccountNode As New AccountNode
                                        oAccountNode.AccountNo = CStr(row("AccountNo")).ToString.Trim
                                        oAccountNode.AccountName = row("AccountName").ToString
                                        oAccountNode.CIFNo = row("CIFNo").ToString
                                        oAccountNode.BankName = row("BankName").ToString
                                        Dim dtTrxDate As Date = CDate(row("TransactionDate"))
                                        oAccountNode.TransactionDateForRank = New Date(dtTrxDate.Year, dtTrxDate.Month, dtTrxDate.Day)
                                        oAccountNode.DebitOrCredit = row("DebitORCredit").ToString

                                        oAccountNode.Shape = CType(htAccountGraphicShape.Item(CStr(row("AccountNo")).ToString.Trim), AccountGraphicShape)   'ga mungkin nothing
                                        oTransDetailToSave.AccountSource = oAccountNode
                                        htAccountNode.Add(ConstructAccountNodeIndex(oAccountNode), oAccountNode)
                                    Else
                                        'gambar baru di level rank 0, karena account destination nya dah punya rank
                                        Dim oAccountNode As New AccountNode
                                        oAccountNode.AccountNo = CStr(row("AccountNo")).ToString.Trim
                                        oAccountNode.AccountName = row("AccountName").ToString
                                        oAccountNode.CIFNo = row("CIFNo").ToString
                                        oAccountNode.BankName = row("BankName").ToString
                                        oAccountNode.TransactionDateForRank = New Date(1900, 1, 1)
                                        oAccountNode.DebitOrCredit = row("DebitORCredit").ToString

                                        oAccountNode.Shape = CType(htAccountGraphicShape.Item(CStr(row("AccountNo")).ToString.Trim), AccountGraphicShape)   'ga mungkin nothing
                                        oTransDetailToSave.AccountSource = oAccountNode
                                        htAccountNode.Add(ConstructAccountNodeIndex(oAccountNode), oAccountNode)

                                    End If
                                End If
                                If intPositionToInsert = -1 Then
                                    arAllTransactions.Add(oTransDetailToSave)
                                Else
                                    arAllTransactions.Insert(intPositionToInsert, oTransDetailToSave)
                                End If
                            Else
                                'posisi Credit --> terima uang
                                If oTransactionDetail Is Nothing Then
                                    oTransactionDetail = New TransactionDetail
                                End If


                                Dim i As Integer = arAllTransactions.Count - 1
                                Dim bolFound As Boolean = False
                                While i >= 0
                                    Dim existingTrxNode As TransactionDetail = CType(arAllTransactions.Item(i), TransactionDetail)
                                    Dim dtExistingTrxDate As Date = New Date(existingTrxNode.TransactionTimestamp.Year, existingTrxNode.TransactionTimestamp.Month, existingTrxNode.TransactionTimestamp.Day)

                                    Dim dtTrxDate As Date = CDate(row("TransactionDate"))
                                    Dim dtTrxRank As New Date(dtTrxDate.Year, dtTrxDate.Month, dtTrxDate.Day)
                                    '##### if no account nya sama
                                    'dan date asli dari timstamp object transaction detail = dtTrxDate
                                    'maka diset ke dia, kemudian date dtForRank object transactiondetail diupdate u/ sama dengan dttimestamp
                                    'ini hanya bisa diupdate kalo dt timestamp < dt transaction date dari row
                                    If existingTrxNode.AccountDestination.AccountNo = CStr(row("AccountNo")).Trim AndAlso dtExistingTrxDate = dtTrxRank Then
                                        oTransactionDetail.AccountDestination = existingTrxNode.AccountDestination
                                        bolFound = True
                                        'arAllTransactions.Insert(i, oTransDetailToSave)
                                        intPositionToInsert = i
                                        existingTrxNode.AccountDestination.TransactionDateForRank = New Date(dtTrxRank.Year, dtTrxRank.Month, dtTrxRank.Day)
                                        Exit While
                                    End If
                                    i -= 1
                                End While

                                If Not bolFound Then
                                    'gambar baru di tanggal transactionDate
                                    Dim oAccountNode As New AccountNode
                                    oAccountNode.AccountNo = CStr(row("AccountNo")).Trim
                                    oAccountNode.AccountName = row("AccountName").ToString
                                    oAccountNode.CIFNo = row("CIFNo").ToString
                                    oAccountNode.BankName = row("BankName").ToString
                                    Dim dtTrxDate As Date = CDate(row("TransactionDate"))
                                    oAccountNode.TransactionDateForRank = New Date(dtTrxDate.Year, dtTrxDate.Month, dtTrxDate.Day)
                                    oAccountNode.DebitOrCredit = row("DebitORCredit").ToString

                                    oAccountNode.Shape = CType(htAccountGraphicShape.Item(CStr(row("AccountNo")).Trim), AccountGraphicShape)   'ga mungkin nothing
                                    oTransactionDetail.AccountDestination = oAccountNode

                                    htAccountNode.Add(ConstructAccountNodeIndex(oAccountNode), oAccountNode)
                                    intPositionToInsert = -1
                                    'arAllTransactions.Add(oTransDetailToSave)
                                End If


                                'Dim oTransDetailToSave As TransactionDetail = oTransactionDetail.ShallowCopy()
                                'If oTransDetailToSave.AccountSource Is Nothing Then
                                '    oTransDetailToSave.IsSingleEntity = True
                                '    'oTransDetailToSave.AccountSource = New AccountNode
                                '    'oTransDetailToSave.AccountSource.AccountNo = row("TLTXDS").ToString.Replace(" ", "_")

                                '    Dim oAccountNode As New AccountNode
                                '    oAccountNode.AccountNo = row("TLTXDS").ToString.Trim
                                '    oAccountNode.AccountNo = oAccountNode.AccountNo.Replace("/", "")
                                '    oAccountNode.AccountNo = Regex.Replace(oAccountNode.AccountNo, "\W", " ")
                                '    oAccountNode.AccountNo = Regex.Replace(oAccountNode.AccountNo, "\s+", " ")
                                '    oAccountNode.AccountNo = Regex.Replace(oAccountNode.AccountNo, "\s", "_")
                                '    'oAccountNode.AccountNo = row("TLTXDS").ToString.Trim.Replace(" ", "_")
                                '    oAccountNode.TransactionDateForRank = New Date(1900, 1, 1)
                                '    oAccountNode.DebitOrCredit = "D"

                                '    If htAccountGraphicShape.Item(oAccountNode.AccountNo) Is Nothing Then
                                '        Dim oAccountGraphicShape As New AccountGraphicShape

                                '        Dim intBorderStyleIndex As Integer = CInt(Math.Floor(htAccountGraphicShape.Count / arColor.Length))
                                '        intBorderStyleIndex = intBorderStyleIndex Mod arBorderStyle.Length
                                '        oAccountGraphicShape.strBorderShape = arBorderStyle(intBorderStyleIndex)

                                '        Dim intColorIndex As Integer = htAccountGraphicShape.Count Mod arColor.Length
                                '        oAccountGraphicShape.strBackgroundColor = arColor(intColorIndex)

                                '        oAccountGraphicShape.strFontColor = arFontColor(0)

                                '        htAccountGraphicShape.Add(oAccountNode.AccountNo, oAccountGraphicShape)

                                '        htAccountNode.Add(ConstructAccountNodeIndex(oAccountNode), oAccountNode)
                                '    End If
                                '    oAccountNode.Shape = CType(htAccountGraphicShape.Item(oAccountNode.AccountNo), AccountGraphicShape)   'ga mungkin nothin
                                '    oTransDetailToSave.AccountSource = oAccountNode
                                'End If

                                'oTransDetailToSave.Amount = CType(row("TransactionLocalEquivalent"), Double)
                                'oTransDetailToSave.TransactionCode = row("AuxiliaryTransactionCode").ToString
                                'oTransDetailToSave.TransactionTimestamp = CDate(row("TransactionDate"))
                                'oTransDetailToSave.StrTicketNo = CStr(row("TransactionTicketNo"))

                                'Dim i As Integer = arAllTransactions.Count - 1
                                'Dim bolFound As Boolean = False
                                'While i >= 0
                                '    Dim existingTrxNode As TransactionDetail = CType(arAllTransactions.Item(i), TransactionDetail)
                                '    Dim dtExistingTrxDate As Date = New Date(existingTrxNode.TransactionTimestamp.Year, existingTrxNode.TransactionTimestamp.Month, existingTrxNode.TransactionTimestamp.Day)

                                '    Dim dtTrxDate As Date = CDate(row("TransactionDate"))
                                '    Dim dtTrxRank As New Date(dtTrxDate.Year, dtTrxDate.Month, dtTrxDate.Day)
                                '    '##### if no account nya sama
                                '    'dan date asli dari timstamp object transaction detail = dtTrxDate
                                '    'maka diset ke dia, kemudian date dtForRank object transactiondetail diupdate u/ sama dengan dttimestamp
                                '    'ini hanya bisa diupdate kalo dt timestamp < dt transaction date dari row
                                '    If existingTrxNode.AccountDestination.AccountNo = CStr(row("AccountNo")).Trim AndAlso dtExistingTrxDate = dtTrxRank Then
                                '        oTransDetailToSave.AccountDestination = existingTrxNode.AccountDestination
                                '        bolFound = True
                                '        arAllTransactions.Insert(i, oTransDetailToSave)
                                '        existingTrxNode.AccountDestination.TransactionDateForRank = New Date(dtTrxRank.Year, dtTrxRank.Month, dtTrxRank.Day)
                                '        Exit While
                                '    End If
                                '    i -= 1
                                'End While

                                'If Not bolFound Then
                                '    'gambar baru di tanggal transactionDate
                                '    Dim oAccountNode As New AccountNode
                                '    oAccountNode.AccountNo = CStr(row("AccountNo")).Trim
                                '    oAccountNode.AccountName = row("AccountName").ToString
                                '    oAccountNode.CIFNo = row("CIFNo").ToString
                                '    oAccountNode.BankName = row("BankName").ToString
                                '    Dim dtTrxDate As Date = CDate(row("TransactionDate"))
                                '    oAccountNode.TransactionDateForRank = New Date(dtTrxDate.Year, dtTrxDate.Month, dtTrxDate.Day)
                                '    oAccountNode.DebitOrCredit = row("DebitORCredit").ToString

                                '    oAccountNode.Shape = CType(htAccountGraphicShape.Item(CStr(row("AccountNo")).Trim), AccountGraphicShape)   'ga mungkin nothing
                                '    oTransDetailToSave.AccountDestination = oAccountNode

                                '    htAccountNode.Add(ConstructAccountNodeIndex(oAccountNode), oAccountNode)

                                '    arAllTransactions.Add(oTransDetailToSave)
                                'End If
                            End If
                            counterDownTo = -1
                        End While
                        counter = counterLast + 1
                    Else    'sisanya sama
                        While counter < dsResult.Tables(0).Rows.Count
                            Dim row As DataRow = dsResult.Tables(0).Rows(counter)
                            If (htAccountGraphicShape.Item(CStr(row("AccountNo")).Trim) Is Nothing) Then
                                Dim oAccountGraphicShape As New AccountGraphicShape

                                Dim intBorderStyleIndex As Integer = CInt(Math.Floor(htAccountGraphicShape.Count / arColor.Length))
                                intBorderStyleIndex = intBorderStyleIndex Mod arBorderStyle.Length
                                oAccountGraphicShape.strBorderShape = arBorderStyle(intBorderStyleIndex)

                                Dim intColorIndex As Integer = htAccountGraphicShape.Count Mod arColor.Length
                                oAccountGraphicShape.strBackgroundColor = arColor(intColorIndex)

                                oAccountGraphicShape.strFontColor = arFontColor(0)

                                htAccountGraphicShape.Add(CStr(row("AccountNo")).Trim, oAccountGraphicShape)
                            End If

                            If row("DebitORCredit").ToString = "D" Then
                                'kalo data baru ini adalah debet (uang keluar), 
                                'maka gw cari transaction detail yg account no = row.account no dan sifat nya Debet or Credit
                                'if ga ada, maka gw gambar node ini sebagai node di level rank=0 (tgl 1/1/1900 tapi dengan label kosong
                                'kalo ada, tapi ketemunya yg node level rank=0, gak masalah jg, pake yg level 0 ini

                                If oTransactionDetail IsNot Nothing Then
                                    'ada lebih dari 1 Debet dalam 1 ticket no
                                    'ignore
                                Else
                                    oTransactionDetail = New TransactionDetail
                                End If

                                Dim i As Integer = arAllTransactions.Count - 1
                                Dim bolFound As Boolean = False
                                While i >= 0
                                    Dim existingTrxNode As TransactionDetail = CType(arAllTransactions.Item(i), TransactionDetail)
                                    Dim dtTrxDate As Date = CDate(row("TransactionDate"))
                                    Dim dtForRank As New Date(dtTrxDate.Year, dtTrxDate.Month, dtTrxDate.Day)
                                    If existingTrxNode.AccountSource.AccountNo = CStr(row("AccountNo")).ToString AndAlso existingTrxNode.AccountSource.TransactionDateForRank < dtForRank Then
                                        oTransactionDetail.AccountSource = existingTrxNode.AccountSource
                                        bolFound = True
                                        'arAllTransactions.Insert(i, oTransactionDetail)
                                        Exit While
                                    ElseIf existingTrxNode.AccountDestination.AccountNo = CStr(row("AccountNo")).ToString AndAlso existingTrxNode.AccountDestination.TransactionDateForRank < dtForRank Then
                                        oTransactionDetail.AccountSource = existingTrxNode.AccountDestination
                                        bolFound = True
                                        'arAllTransactions.Insert(i, oTransactionDetail)
                                        Exit While
                                    End If
                                    i -= 1
                                End While
                                If Not bolFound Then
                                    'gambar baru di level rank 0
                                    Dim oAccountNode As New AccountNode
                                    oAccountNode.AccountNo = CStr(row("AccountNo")).ToString.Trim
                                    oAccountNode.AccountName = row("AccountName").ToString
                                    oAccountNode.CIFNo = row("CIFNo").ToString
                                    oAccountNode.BankName = row("BankName").ToString
                                    oAccountNode.TransactionDateForRank = New Date(1900, 1, 1)
                                    oAccountNode.DebitOrCredit = row("DebitORCredit").ToString

                                    oAccountNode.Shape = CType(htAccountGraphicShape.Item(CStr(row("AccountNo")).ToString), AccountGraphicShape)   'ga mungkin nothing
                                    oTransactionDetail.AccountSource = oAccountNode
                                    If Not htAccountNode.ContainsKey(ConstructAccountNodeIndex(oAccountNode)) Then
                                        htAccountNode.Add(ConstructAccountNodeIndex(oAccountNode), oAccountNode)
                                    End If


                                End If
                            Else
                                'posisi Credit
                                If oTransactionDetail Is Nothing Then
                                    oTransactionDetail = New TransactionDetail
                                End If
                                Dim oTransDetailToSave As TransactionDetail = oTransactionDetail.ShallowCopy()
                                If oTransDetailToSave.AccountSource Is Nothing Then
                                    oTransDetailToSave.IsSingleEntity = True
                                    'oTransDetailToSave.AccountSource = New AccountNode
                                    'oTransDetailToSave.AccountSource.AccountNo = row("TLTXDS").ToString.Replace(" ", "_")

                                    Dim oAccountNode As New AccountNode
                                    oAccountNode.IsAccountNo = False
                                    oAccountNode.AccountNo = row("TLTXDS").ToString.Trim
                                    oAccountNode.AccountNo = oAccountNode.AccountNo.Replace("/", "")
                                    oAccountNode.AccountNo = Regex.Replace(oAccountNode.AccountNo, "\W", " ")
                                    oAccountNode.AccountNo = Regex.Replace(oAccountNode.AccountNo, "\s+", " ")
                                    oAccountNode.AccountNo = Regex.Replace(oAccountNode.AccountNo, "\s", "_")
                                    'oAccountNode.AccountNo = row("TLTXDS").ToString.Trim.Replace(" ", "_")
                                    oAccountNode.TransactionDateForRank = New Date(1900, 1, 1)
                                    oAccountNode.DebitOrCredit = "D"

                                    If htAccountGraphicShape.Item(oAccountNode.AccountNo) Is Nothing Then
                                        Dim oAccountGraphicShape As New AccountGraphicShape

                                        Dim intBorderStyleIndex As Integer = CInt(Math.Floor(htAccountGraphicShape.Count / arColor.Length))
                                        intBorderStyleIndex = intBorderStyleIndex Mod arBorderStyle.Length
                                        oAccountGraphicShape.strBorderShape = arBorderStyle(intBorderStyleIndex)

                                        Dim intColorIndex As Integer = htAccountGraphicShape.Count Mod arColor.Length
                                        oAccountGraphicShape.strBackgroundColor = arColor(intColorIndex)

                                        oAccountGraphicShape.strFontColor = arFontColor(0)

                                        htAccountGraphicShape.Add(oAccountNode.AccountNo, oAccountGraphicShape)

                                        htAccountNode.Add(ConstructAccountNodeIndex(oAccountNode), oAccountNode)
                                    End If
                                    oAccountNode.Shape = CType(htAccountGraphicShape.Item(oAccountNode.AccountNo), AccountGraphicShape)   'ga mungkin nothin
                                    oTransDetailToSave.AccountSource = oAccountNode
                                End If

                                oTransDetailToSave.Amount = CType(row("TransactionLocalEquivalent"), Double)
                                oTransDetailToSave.TransactionCode = row("AuxiliaryTransactionCode").ToString
                                oTransDetailToSave.TransactionDescription = row("TLTXDS").ToString
                                oTransDetailToSave.TransactionTimestamp = CDate(row("TransactionDate"))
                                oTransDetailToSave.StrTicketNo = CStr(row("TransactionTicketNo"))

                                Dim i As Integer = arAllTransactions.Count - 1
                                Dim bolFound As Boolean = False
                                While i >= 0
                                    Dim existingTrxNode As TransactionDetail = CType(arAllTransactions.Item(i), TransactionDetail)
                                    Dim dtExistingTrxDate As Date = New Date(existingTrxNode.TransactionTimestamp.Year, existingTrxNode.TransactionTimestamp.Month, existingTrxNode.TransactionTimestamp.Day)

                                    Dim dtTrxDate As Date = CDate(row("TransactionDate"))
                                    Dim dtTrxRank As New Date(dtTrxDate.Year, dtTrxDate.Month, dtTrxDate.Day)
                                    '##### if no account nya sama
                                    'dan date asli dari timstamp object transaction detail = dtTrxDate
                                    'maka diset ke dia, kemudian date dtForRank object transactiondetail diupdate u/ sama dengan dttimestamp
                                    'ini hanya bisa diupdate kalo dt timestamp < dt transaction date dari row
                                    If existingTrxNode.AccountSource.AccountNo = CStr(row("AccountNo")).ToString AndAlso dtExistingTrxDate = dtTrxRank Then
                                        oTransDetailToSave.AccountDestination = existingTrxNode.AccountSource
                                        bolFound = True
                                        arAllTransactions.Insert(i, oTransDetailToSave)
                                        'existingTrxNode.AccountDestination.TransactionDateForRank = New Date(dtTrxRank.Year, dtTrxRank.Month, dtTrxRank.Day)
                                        Exit While
                                    ElseIf existingTrxNode.AccountDestination.AccountNo = CStr(row("AccountNo")).ToString AndAlso dtExistingTrxDate = dtTrxRank Then
                                        oTransDetailToSave.AccountDestination = existingTrxNode.AccountDestination
                                        bolFound = True
                                        arAllTransactions.Insert(i, oTransDetailToSave)
                                        'existingTrxNode.AccountDestination.TransactionDateForRank = New Date(dtTrxRank.Year, dtTrxRank.Month, dtTrxRank.Day)
                                        Exit While
                                    End If
                                    'If (existingTrxNode.AccountDestination.AccountNo = CStr(row("AccountNo")).Trim OrElse existingTrxNode.AccountSource.AccountNo = CStr(row("AccountNo")).Trim) AndAlso dtExistingTrxDate = dtTrxRank Then
                                    '    oTransDetailToSave.AccountDestination = existingTrxNode.AccountDestination
                                    '    bolFound = True
                                    '    arAllTransactions.Insert(i, oTransDetailToSave)
                                    '    existingTrxNode.AccountDestination.TransactionDateForRank = New Date(dtTrxRank.Year, dtTrxRank.Month, dtTrxRank.Day)
                                    '    Exit While
                                    'End If
                                    i -= 1
                                End While

                                If Not bolFound Then
                                    'gambar baru di tanggal transactionDate
                                    Dim oAccountNode As New AccountNode
                                    oAccountNode.AccountNo = CStr(row("AccountNo")).Trim
                                    oAccountNode.AccountName = row("AccountName").ToString
                                    oAccountNode.CIFNo = row("CIFNo").ToString
                                    oAccountNode.BankName = row("BankName").ToString
                                    Dim dtTrxDate As Date = CDate(row("TransactionDate"))
                                    oAccountNode.TransactionDateForRank = New Date(dtTrxDate.Year, dtTrxDate.Month, dtTrxDate.Day)
                                    oAccountNode.DebitOrCredit = row("DebitORCredit").ToString

                                    oAccountNode.Shape = CType(htAccountGraphicShape.Item(CStr(row("AccountNo")).Trim), AccountGraphicShape)   'ga mungkin nothing
                                    oTransDetailToSave.AccountDestination = oAccountNode

                                    If Not htAccountNode.Contains(ConstructAccountNodeIndex(oAccountNode)) Then
                                        htAccountNode.Add(ConstructAccountNodeIndex(oAccountNode), oAccountNode)
                                    End If


                                    arAllTransactions.Add(oTransDetailToSave)
                                End If
                            End If

                            counter += 1
                            If counter < dsResult.Tables(0).Rows.Count AndAlso dsResult.Tables(0).Rows(counter).Item("TransactionTicketNo").ToString() <> currentTicketNo Then
                                currentTicketNo = dsResult.Tables(0).Rows(counter).Item("TransactionTicketNo").ToString
                                Exit While
                            End If
                        End While
                    End If





                    'If row("TransactionTicketNo").ToString() = currentTicketNo Then

                    '    If (htAccountGraphicShape.Item(CStr(row("AccountNo")).Trim) Is Nothing) Then
                    '        Dim oAccountGraphicShape As New AccountGraphicShape

                    '        Dim intBorderStyleIndex As Integer = CInt(Math.Floor(htAccountGraphicShape.Count / arColor.Length))
                    '        intBorderStyleIndex = intBorderStyleIndex Mod arBorderStyle.Length
                    '        oAccountGraphicShape.strBorderShape = arBorderStyle(intBorderStyleIndex)

                    '        Dim intColorIndex As Integer = htAccountGraphicShape.Count Mod arColor.Length
                    '        oAccountGraphicShape.strBackgroundColor = arColor(intColorIndex)

                    '        oAccountGraphicShape.strFontColor = arFontColor(0)

                    '        htAccountGraphicShape.Add(CStr(row("AccountNo")).Trim, oAccountGraphicShape)
                    '    End If

                    '    If row("DebitORCredit").ToString = "D" Then
                    '        'kalo data baru ini adalah debet (uang keluar), 
                    '        'maka gw cari transaction detail yg account no = row.account no dan sifat nya Debet or Credit
                    '        'if ga ada, maka gw gambar node ini sebagai node di level rank=0 (tgl 1/1/1900 tapi dengan label kosong
                    '        'kalo ada, tapi ketemunya yg node level rank=0, gak masalah jg, pake yg level 0 ini

                    '        If oTransactionDetail IsNot Nothing Then
                    '            'ada lebih dari 1 Debet dalam 1 ticket no
                    '            'ignore
                    '        Else
                    '            oTransactionDetail = New TransactionDetail
                    '        End If

                    '        Dim i As Integer = arAllTransactions.Count - 1
                    '        Dim bolFound As Boolean = False
                    '        While i >= 0
                    '            Dim existingTrxNode As TransactionDetail = CType(arAllTransactions.Item(i), TransactionDetail)
                    '            Dim dtTrxDate As Date = CDate(row("TransactionDate"))
                    '            Dim dtForRank As New Date(dtTrxDate.Year, dtTrxDate.Month, dtTrxDate.Day)
                    '            If existingTrxNode.AccountSource.AccountNo = CStr(row("AccountNo")).ToString AndAlso existingTrxNode.AccountSource.TransactionDateForRank < dtForRank Then
                    '                oTransactionDetail.AccountSource = existingTrxNode.AccountSource
                    '                bolFound = True
                    '                'arAllTransactions.Insert(i, oTransactionDetail)
                    '                Exit While
                    '            ElseIf existingTrxNode.AccountDestination.AccountNo = CStr(row("AccountNo")).ToString AndAlso existingTrxNode.AccountDestination.TransactionDateForRank < dtForRank Then
                    '                oTransactionDetail.AccountSource = existingTrxNode.AccountDestination
                    '                bolFound = True
                    '                'arAllTransactions.Insert(i, oTransactionDetail)
                    '                Exit While
                    '            End If
                    '            i -= 1
                    '        End While
                    '        If Not bolFound Then
                    '            'gambar baru di level rank 0
                    '            Dim oAccountNode As New AccountNode
                    '            oAccountNode.AccountNo = CStr(row("AccountNo")).ToString.Trim
                    '            oAccountNode.AccountName = row("AccountName").ToString
                    '            oAccountNode.CIFNo = row("CIFNo").ToString
                    '            oAccountNode.BankName = row("BankName").ToString
                    '            oAccountNode.TransactionDateForRank = New Date(1900, 1, 1)
                    '            oAccountNode.DebitOrCredit = row("DebitORCredit").ToString

                    '            oAccountNode.Shape = CType(htAccountGraphicShape.Item(CStr(row("AccountNo")).ToString), AccountGraphicShape)   'ga mungkin nothing
                    '            oTransactionDetail.AccountSource = oAccountNode

                    '            htAccountNode.Add(ConstructAccountNodeIndex(oAccountNode), oAccountNode)

                    '        End If
                    '    Else
                    '        'posisi Credit
                    '        If oTransactionDetail Is Nothing Then
                    '            oTransactionDetail = New TransactionDetail
                    '        End If
                    '        Dim oTransDetailToSave As TransactionDetail = oTransactionDetail.ShallowCopy()
                    '        If oTransDetailToSave.AccountSource Is Nothing Then
                    '            oTransDetailToSave.IsSingleEntity = True
                    '            'oTransDetailToSave.AccountSource = New AccountNode
                    '            'oTransDetailToSave.AccountSource.AccountNo = row("TLTXDS").ToString.Replace(" ", "_")

                    '            Dim oAccountNode As New AccountNode
                    '            oAccountNode.AccountNo = row("TLTXDS").ToString.Trim
                    '            oAccountNode.AccountNo = oAccountNode.AccountNo.Replace("/", "")
                    '            oAccountNode.AccountNo = Regex.Replace(oAccountNode.AccountNo, "\W", " ")
                    '            oAccountNode.AccountNo = Regex.Replace(oAccountNode.AccountNo, "\s+", " ")
                    '            oAccountNode.AccountNo = Regex.Replace(oAccountNode.AccountNo, "\s", "_")
                    '            'oAccountNode.AccountNo = row("TLTXDS").ToString.Trim.Replace(" ", "_")
                    '            oAccountNode.TransactionDateForRank = New Date(1900, 1, 1)
                    '            oAccountNode.DebitOrCredit = "D"

                    '            If htAccountGraphicShape.Item(oAccountNode.AccountNo) Is Nothing Then
                    '                Dim oAccountGraphicShape As New AccountGraphicShape

                    '                Dim intBorderStyleIndex As Integer = CInt(Math.Floor(htAccountGraphicShape.Count / arColor.Length))
                    '                intBorderStyleIndex = intBorderStyleIndex Mod arBorderStyle.Length
                    '                oAccountGraphicShape.strBorderShape = arBorderStyle(intBorderStyleIndex)

                    '                Dim intColorIndex As Integer = htAccountGraphicShape.Count Mod arColor.Length
                    '                oAccountGraphicShape.strBackgroundColor = arColor(intColorIndex)

                    '                oAccountGraphicShape.strFontColor = arFontColor(0)

                    '                htAccountGraphicShape.Add(oAccountNode.AccountNo, oAccountGraphicShape)

                    '                htAccountNode.Add(ConstructAccountNodeIndex(oAccountNode), oAccountNode)
                    '            End If
                    '            oAccountNode.Shape = CType(htAccountGraphicShape.Item(oAccountNode.AccountNo), AccountGraphicShape)   'ga mungkin nothin
                    '            oTransDetailToSave.AccountSource = oAccountNode
                    '        End If

                    '        oTransDetailToSave.Amount = CType(row("TransactionLocalEquivalent"), Double)
                    '        oTransDetailToSave.TransactionCode = row("AuxiliaryTransactionCode").ToString
                    '        oTransDetailToSave.TransactionTimestamp = CDate(row("TransactionDate"))
                    '        oTransDetailToSave.StrTicketNo = CStr(row("TransactionTicketNo"))

                    '        Dim i As Integer = arAllTransactions.Count - 1
                    '        Dim bolFound As Boolean = False
                    '        While i >= 0
                    '            Dim existingTrxNode As TransactionDetail = CType(arAllTransactions.Item(i), TransactionDetail)
                    '            Dim dtExistingTrxDate As Date = New Date(existingTrxNode.TransactionTimestamp.Year, existingTrxNode.TransactionTimestamp.Month, existingTrxNode.TransactionTimestamp.Day)

                    '            Dim dtTrxDate As Date = CDate(row("TransactionDate"))
                    '            Dim dtTrxRank As New Date(dtTrxDate.Year, dtTrxDate.Month, dtTrxDate.Day)
                    '            '##### if no account nya sama
                    '            'dan date asli dari timstamp object transaction detail = dtTrxDate
                    '            'maka diset ke dia, kemudian date dtForRank object transactiondetail diupdate u/ sama dengan dttimestamp
                    '            'ini hanya bisa diupdate kalo dt timestamp < dt transaction date dari row
                    '            If existingTrxNode.AccountDestination.AccountNo = CStr(row("AccountNo")).Trim AndAlso dtExistingTrxDate = dtTrxRank Then
                    '                oTransDetailToSave.AccountDestination = existingTrxNode.AccountDestination
                    '                bolFound = True
                    '                arAllTransactions.Insert(i, oTransDetailToSave)
                    '                existingTrxNode.AccountDestination.TransactionDateForRank = New Date(dtTrxRank.Year, dtTrxRank.Month, dtTrxRank.Day)
                    '                Exit While
                    '            End If
                    '            i -= 1
                    '        End While

                    '        If Not bolFound Then
                    '            'gambar baru di tanggal transactionDate
                    '            Dim oAccountNode As New AccountNode
                    '            oAccountNode.AccountNo = CStr(row("AccountNo")).Trim
                    '            oAccountNode.AccountName = row("AccountName").ToString
                    '            oAccountNode.CIFNo = row("CIFNo").ToString
                    '            oAccountNode.BankName = row("BankName").ToString
                    '            Dim dtTrxDate As Date = CDate(row("TransactionDate"))
                    '            oAccountNode.TransactionDateForRank = New Date(dtTrxDate.Year, dtTrxDate.Month, dtTrxDate.Day)
                    '            oAccountNode.DebitOrCredit = row("DebitORCredit").ToString

                    '            oAccountNode.Shape = CType(htAccountGraphicShape.Item(CStr(row("AccountNo")).Trim), AccountGraphicShape)   'ga mungkin nothing
                    '            oTransDetailToSave.AccountDestination = oAccountNode

                    '            htAccountNode.Add(ConstructAccountNodeIndex(oAccountNode), oAccountNode)

                    '            arAllTransactions.Add(oTransDetailToSave)
                    '        End If
                    '    End If
                    '    counter += 1
                    'Else
                    '    oTransactionDetail = Nothing
                    '    currentTicketNo = row("TransactionTicketNo").ToString
                    'End If

                End While
                'sekarang kita update untuk semua account node pengirim yang daterank nya = 1900 untuk di cariin pasangan nya
                'dan digambar ulang (hapus dari account node)
                For i As Integer = 0 To arAllTransactions.Count - 1
                    Dim oTrxDetailToBeUpdated As TransactionDetail = CType(arAllTransactions.Item(i), TransactionDetail)
                    If oTrxDetailToBeUpdated.AccountSource.TransactionDateForRank.Year = 1900 Then
                        For j As Integer = arAllTransactions.Count - 1 To 0 Step -1
                            If i = j Then
                                Continue For
                            End If
                            Dim oTrxDetailMatcher As TransactionDetail = CType(arAllTransactions.Item(j), TransactionDetail)
                            If (oTrxDetailToBeUpdated.AccountSource.AccountNo = oTrxDetailMatcher.AccountSource.AccountNo AndAlso oTrxDetailToBeUpdated.AccountDestination.TransactionDateForRank > oTrxDetailMatcher.AccountSource.TransactionDateForRank AndAlso oTrxDetailMatcher.AccountSource.TransactionDateForRank.Year <> 1900) Then
                                htAccountNode.Remove(ConstructAccountNodeIndex(oTrxDetailToBeUpdated.AccountSource))
                                oTrxDetailToBeUpdated.AccountSource = oTrxDetailMatcher.AccountSource
                            ElseIf (oTrxDetailToBeUpdated.AccountSource.AccountNo = oTrxDetailMatcher.AccountDestination.AccountNo AndAlso oTrxDetailToBeUpdated.AccountDestination.TransactionDateForRank > oTrxDetailMatcher.AccountDestination.TransactionDateForRank AndAlso oTrxDetailMatcher.AccountDestination.TransactionDateForRank.Year <> 1900) Then
                                htAccountNode.Remove(ConstructAccountNodeIndex(oTrxDetailToBeUpdated.AccountSource))
                                oTrxDetailToBeUpdated.AccountSource = oTrxDetailMatcher.AccountDestination
                            End If

                        Next
                    End If
                Next
            End If

            Session("GraphicalTransactionAnalysisView.arAllTransactions") = arAllTransactions
            Session("GraphicalTransactionAnalysisView.htAccountGraphicShape") = htAccountGraphicShape
            Session("GraphicalTransactionAnalysisView.htAccountNode") = htAccountNode
            Return BuildGraphLanguange(arAllTransactions, htAccountNode)
        Catch ex As Exception
            Throw
        Finally
            bolDataExists = arAllTransactions.Count > 0
            If dsResult IsNot Nothing Then
                dsResult.Dispose()
                dsResult = Nothing
            End If
        End Try

    End Function

    'Private Function BuildGetTransactionDate(ByVal StrAccountNo As String, ByVal TransactionDateStart As Date, ByVal TransactionDateEnd As Date, ByVal AuxTransactionCode As String, ByVal TransactionAmountFrom As Decimal, ByVal TransactionAmountTo As Decimal) As Boolean
    '    Using TransactionLinkAnalysisAdapter As New AMLDAL.TransactionLinkAnalysisDataSetTableAdapters.TransactionDetailTableAdapter
    '        ' The command collection property modified from protected to pubic
    '        ' Change Protected ReadOnly Property CommandCollection() As System.Data.SqlClient.SqlCommand() in TransactionLinkAnalysisDataSet.Designer.vb
    '        ' insert order to set select command (command collection 0)
    '        TransactionLinkAnalysisAdapter.CommandCollection(0).CommandText = "SELECT TransactionDetailId, BankCode, BankName, AccountNo, AccountName, " & _
    '        " TransactionAmount, DebitORCredit, TransactionDate, AuxiliaryTransactionCode, TransactionLocalEquivalent, TransactionTicketNo " & _
    '        " FROM TransactionDetail" & _
    '        " WHERE AccountNo=@AccountNo" & _
    '        " AND DATEDIFF(DAY,TransactionDate,@TransactionDateStart)<=0" & _
    '        " AND DATEDIFF(DAY,TransactionDate,@TransactionDateEnd)>=0" & _
    '        " AND AuxiliaryTransactionCode=@AuxiliaryTransactionCode" & _
    '        " AND TransactionAmount BETWEEN @TransactionAmountFrom AND @TransactionAmountTo"
    '        TransactionLinkAnalysisAdapter.CommandCollection(0).Parameters.Clear()
    '        TransactionLinkAnalysisAdapter.CommandCollection(0).Parameters.Add("@AccountNo", Data.SqlDbType.VarChar, 20)
    '        TransactionLinkAnalysisAdapter.CommandCollection(0).Parameters.Add("@TransactionDateStart", Data.SqlDbType.DateTime)
    '        TransactionLinkAnalysisAdapter.CommandCollection(0).Parameters.Add("@TransactionDateEnd", Data.SqlDbType.DateTime)
    '        TransactionLinkAnalysisAdapter.CommandCollection(0).Parameters.Add("@AuxiliaryTransactionCode", Data.SqlDbType.VarChar, 50)
    '        TransactionLinkAnalysisAdapter.CommandCollection(0).Parameters.Add("@TransactionAmountFrom", Data.SqlDbType.Money)
    '        TransactionLinkAnalysisAdapter.CommandCollection(0).Parameters.Add("@TransactionAmountTo", Data.SqlDbType.Money)
    '        TransactionLinkAnalysisAdapter.CommandCollection(0).Parameters.Item("@TransactionDateStart").Value = TransactionDateStart
    '    End Using
    'End Function

    Protected Sub ClearSession()
        Session("GraphicalTransactionAnalysisView.SetnGetRowTotal") = Nothing
        Session("GraphicalTransactionAnalysisView.SetnGetCurrentPage") = Nothing
        Session("TransactionGraphCommand") = Nothing
        Session("GraphicalTransactionAnalysisView.arAllTransactions") = Nothing
        Session("GraphicalTransactionAnalysisView.htAccountGraphicShape") = Nothing
        Session("GraphicalTransactionAnalysisView.htAccountNode") = Nothing

    End Sub

    Sub BindGrid()
        Session("GraphicalTransactionAnalysisView.arAllTransactions") = Nothing
        Session("GraphicalTransactionAnalysisView.htAccountGraphicShape") = Nothing
        Session("GraphicalTransactionAnalysisView.htAccountNode") = Nothing
        Dim dtStartDate As Date
        Dim dtEndDate As Date

        dtStartDate = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", StartDateTextBox.Text)
        dtEndDate = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", EndDateTextBox.Text)

        Dim AccountNo As String
        If chkExpandLink.Checked Then
            AccountNo = hidSelectedNode.Value
        Else
            AccountNo = Regex.Replace(Me.Request.Params.Item("AccountNo"), "/\D/", "")
        End If

        If AccountNo <> "" AndAlso ((chkExpandLink.Checked) OrElse (dtStartDate <> Nothing AndAlso dtEndDate <> Nothing)) Then
            Dim strCommand As String
            Dim bolDataExists As Boolean = False
            strCommand = BuildData(AccountNo, chkExpandLink.Checked, dtStartDate, dtEndDate, Nothing, Nothing, Nothing, bolDataExists, Me.SetnGetCurrentPage, 10)
            SetInfoNavigate()

            If Not strCommand Is Nothing AndAlso bolDataExists Then
                Session("TransactionGraphCommand") = strCommand

                'dot = New WINGRAPHVIZLib.DOT

                ''Generate Client-side image map(CMAP)
                'Dim strCMAP As String
                'strCMAP = dot.ToCMAP(strCommand)
                'strCMAP = strCMAP.Replace("href", "href=""#"" onclick")
                'strCMAP = strCMAP.Replace("\n", " - ")
                'Me.GraphicalTransactionAnalysisMap.InnerHtml = strCMAP

                'Me.GraphicalTransactionAnalysisImage.Src = "GraphicalTransactionAnalysis.aspx?id=" & Now.Ticks & Now.Second
            End If
        End If
        If Session("TransactionGraphCommand") Is Nothing Then
            Me.GraphicalTransactionAnalysisImage.Visible = True
        Else
            Me.GraphicalTransactionAnalysisImage.Visible = False
        End If
        chkExpandLink.Checked = False
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim dtStartDate As Date
            Dim dtEndDate As Date
            Me.StartDateTextBox.Attributes.Add("readonly", "readonly")
            Me.EndDateTextBox.Attributes.Add("readonly", "readonly")

            If Not HttpContext.Current.Request(Me.ImageButtonClear.UniqueID & ".x") Is Nothing Then
                Me.ClearSession()
            End If

            If Not Page.IsPostBack Then
                ClearSession()
                'FillAuxCode()

                dtStartDate = Sahassa.AML.Commonly.ConvertToDate("yyyy-MM-dd", Me.Request.Params.Item("StartDate"))
                If dtStartDate <> Nothing Then
                    StartDateTextBox.Text = dtStartDate.ToString("dd-MMM-yyyy")
                End If

                dtEndDate = Sahassa.AML.Commonly.ConvertToDate("yyyy-MM-dd", Me.Request.Params.Item("EndDate"))
                If dtEndDate <> Nothing Then
                    EndDateTextBox.Text = dtEndDate.ToString("dd-MMM-yyyy")
                End If

                'Tambahkan event handler OnClick yang akan menampilkan kalender javascript
                Me.cmdStartDatePicker.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.StartDateTextBox.ClientID & "'), 'dd-mmm-yyyy')")
                Me.cmdEndDatePicker.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.EndDateTextBox.ClientID & "'), 'dd-mmm-yyyy')")
                chkExpandLink.Attributes.Add("style", "display:none")
            End If

            'dtStartDate = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", StartDateTextBox.Text)
            'dtEndDate = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", EndDateTextBox.Text)

            'Dim AccountNo As String
            'If chkExpandLink.Checked Then
            '    AccountNo = hidSelectedNode.Value
            'Else
            '    AccountNo = Regex.Replace(Me.Request.Params.Item("AccountNo"), "/\D/", "")
            'End If

            'If AccountNo <> "" AndAlso ((chkExpandLink.Checked) OrElse (dtStartDate <> Nothing AndAlso dtEndDate <> Nothing)) Then
            '    Dim strCommand As String
            '    Dim bolDataExists As Boolean = False
            '    strCommand = BuildData(AccountNo, chkExpandLink.Checked, dtStartDate, dtEndDate, Nothing, Nothing, Nothing, bolDataExists, Me.SetnGetCurrentPage, 10)
            '    SetInfoNavigate()

            '    If Not strCommand Is Nothing AndAlso bolDataExists Then
            '        Session("TransactionGraphCommand") = strCommand

            '        'dot = New WINGRAPHVIZLib.DOT

            '        ''Generate Client-side image map(CMAP)
            '        'Dim strCMAP As String
            '        'strCMAP = dot.ToCMAP(strCommand)
            '        'strCMAP = strCMAP.Replace("href", "href=""#"" onclick")
            '        'strCMAP = strCMAP.Replace("\n", " - ")
            '        'Me.GraphicalTransactionAnalysisMap.InnerHtml = strCMAP

            '        'Me.GraphicalTransactionAnalysisImage.Src = "GraphicalTransactionAnalysis.aspx?id=" & Now.Ticks & Now.Second
            '    End If
            'End If
            'If Session("TransactionGraphCommand") Is Nothing Then
            '    Me.GraphicalTransactionAnalysisImage.Visible = True
            'Else
            '    Me.GraphicalTransactionAnalysisImage.Visible = False
            'End If
            'chkExpandLink.Checked = False
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            
        Catch
            Throw
        End Try
    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
            ' BindGrid()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message

        End Try
    End Sub
    'Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAdd.Click
    '    Dim i As Integer = 0
    '    While i < lstAllTrxCode.Items.Count
    '        If lstAllTrxCode.Items(i).Selected = True Then
    '            lstSelectedTrxCode.Items.Add(lstAllTrxCode.Items(i))
    '            lstAllTrxCode.Items.RemoveAt(i)
    '        Else
    '            i += 1
    '        End If
    '    End While
    '    SortListBox(lstAllTrxCode)
    '    SortListBox(lstSelectedTrxCode)
    'End Sub
    'Protected Sub SortListBox(ByVal lstBox As ListBox)
    '    Dim sl As New SortedList
    '    For Each oItem As ListItem In lstBox.Items
    '        sl.Add(oItem.Value, oItem.Text)
    '    Next
    '    lstBox.Items.Clear()

    '    lstBox.DataSource = sl
    '    lstBox.DataTextField = "Value"
    '    lstBox.DataValueField = "Key"
    '    lstBox.DataBind()
    'End Sub
    Protected Sub imgGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgGo.Click
        Try
            Dim dtStartDate As Date
            Dim dtEndDate As Date
            dtStartDate = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", StartDateTextBox.Text)
            dtEndDate = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", EndDateTextBox.Text)
            If DateDiff(DateInterval.Day, dtStartDate, dtEndDate) < 0 Then
                Throw New Exception("End Date must greater than or equal Start Date")
            End If
            If DateDiff(DateInterval.Day, dtStartDate, dtEndDate) > 31 Then
                Throw New Exception("Maximum transaction date is 31 days")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    'Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRemove.Click
    '    Dim i As Integer = 0
    '    While i < lstSelectedTrxCode.Items.Count
    '        If lstSelectedTrxCode.Items(i).Selected = True Then
    '            lstAllTrxCode.Items.Add(lstSelectedTrxCode.Items(i))
    '            lstSelectedTrxCode.Items.RemoveAt(i)
    '        Else
    '            i += 1
    '        End If
    '    End While
    '    SortListBox(lstAllTrxCode)
    '    SortListBox(lstSelectedTrxCode)
    'End Sub

    Protected Sub GraphicalTransactionAnalysisView_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Try

            BindGrid()
        Catch ex As Exception

            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try            
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If

        Catch ex As Exception

            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub
End Class

Public Enum TransactionType
    DebitOnly = 0
    CreditOnly = 1
    SingleDebitMultiCredit = 2
    MultiDebitSingleCredit = 3
    SingleDebitSingleCredit = 4
End Enum