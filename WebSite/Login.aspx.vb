Imports System.Web.Security
Imports System.Configuration
Imports SahassaNettier.Entities
Imports SahassaNettier.Data

Partial Class Login
    Inherits System.Web.UI.Page

#Region "Property"
    Public ReadOnly Property GetUserElemetId() As String
        Get
            Return Me.TextUsername.ClientID
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' ambil userid yg sedang login
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	01/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Property GetUserId() As String
        Get
            Return Me.Session("UserName")
        End Get
        Set(ByVal Value As String)
            Me.Session("UserName") = Value
        End Set
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set login pk_userId
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	29/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private WriteOnly Property SetPkUserId() As Long
        Set(ByVal Value As Long)
            Sahassa.AML.Commonly.SessionPkUserId = Value
            'Dim Cookies As New HttpCookie("UserIdPk", Value)
            'Cookies.Expires = Now.AddMinutes(Commonly.GetLoginExpire)
            'Me.Response.SetCookie(Cookies)
        End Set
    End Property


    ''' -----------------------------------------------------------------------------
    '''     ''' <summary>
    ''' ambil password retry
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	01/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Property GetPasswordRetry(ByVal strUserId As String) As Int32
        Get
            If strUserId <> Me.GetUserId Then
                Me.GetUserId = strUserId
                Me.Session("PasswordRetry") = 0
            End If
            Return Me.Session("PasswordRetry")
        End Get
        Set(ByVal Value As Int32)
            Me.Session("PasswordRetry") = Value
        End Set
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set group name
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	23/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private WriteOnly Property SetGroupName() As String
        Set(ByVal Value As String)
            Sahassa.AML.Commonly.SessionGroupName = Value
            'Dim Cookies As New HttpCookie("GroupName", Value)
            'Cookies.Expires = Now.AddMinutes(Commonly.GetLoginExpire)
            'Me.Response.SetCookie(Cookies)
        End Set
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set login cookie
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	02/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private WriteOnly Property SetLoginCookie() As String
        Set(ByVal Value As String)
            Sahassa.AML.Commonly.SessionUserId = Value
            'Dim Cookies As New HttpCookie("UserId", Value)
            'Cookies.Expires = Now.AddMinutes(Commonly.GetLoginExpire)
            'Me.Response.SetCookie(Cookies)
            FormsAuthentication.SetAuthCookie(Value, True)
        End Set
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set branchid cookies
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	23/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private WriteOnly Property SetBranchCookie() As Int32
        Set(ByVal Value As Int32)
            'Sahassa.AML.Commonly.SessionBranchId = Value

            'Dim Cookies As New HttpCookie("BranchId", Value)
            'Cookies.Expires = Now.AddMinutes(Commonly.GetLoginExpire)
            'Me.Response.SetCookie(Cookies)
        End Set
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' write to audit login
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	01/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private WriteOnly Property AuditLogin() As String
        Set(ByVal Value As String)
            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                AccessAudit.Insert(Value, Now, "Login")

                'Transcope.Complete()
                'End Using
            End Using
        End Set
    End Property

    Private Function IsAssignedToWorkingUnit() As Boolean
        'Periksa apakah UserID tsb sdh diassign ke suatu WorkingUnit atau belum
        Using AccessUserWorkingUnitAssignment As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignmentTableAdapter
            Dim objTable As AMLDAL.AMLDataSet.UserWorkingUnitAssignmentDataTable = AccessUserWorkingUnitAssignment.GetWorkingUnitsByUserID(Me.TextUsername.Text)

            'Jika objTable.Rows.Count = 0 berarti UserID tsb blm diassign ke suatu WorkingUnit 
            If objTable.Rows.Count = 0 Then
                Return False
            Else 'Jika objTable.Rows.Count != 0 berarti UserID tsb sdh diassign ke suatu WorkingUnit
                Return True
            End If
        End Using
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' ambil password expire day
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	01/06/2006	Created
    '''     [Hendry] 05/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property GetExpiredPasswordDay() As Int32
        Get
            Using AccessLoginParameter As New AMLDAL.AMLDataSetTableAdapters.LoginParameterTableAdapter
                Using TableLoginParameter As AMLDAL.AMLDataSet.LoginParameterDataTable = AccessLoginParameter.GetData
                    If TableLoginParameter.Rows.Count > 0 Then
                        Dim RowLoginParameter As AMLDAL.AMLDataSet.LoginParameterRow = TableLoginParameter.Rows(0)

                        Return RowLoginParameter.PasswordExpiredPeriod
                    Else
                        Return 15 'Nilai default password expiration day = 15 hari
                    End If
                End Using
            End Using
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' ambil password expire day
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	01/06/2006	Created
    '''     [Hendry] 15/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property GetAccountLockout() As Int32
        Get
            Using AccessLoginParameter As New AMLDAL.AMLDataSetTableAdapters.LoginParameterTableAdapter
                Using TableLoginParameter As AMLDAL.AMLDataSet.LoginParameterDataTable = AccessLoginParameter.GetData
                    If TableLoginParameter.Rows.Count > 0 Then
                        Dim RowLoginParameter As AMLDAL.AMLDataSet.LoginParameterRow = TableLoginParameter.Rows(0)

                        Return RowLoginParameter.AccountLockout
                    Else
                        Return 3 'Nilai default account lockout = 3 kali
                    End If
                End Using
            End Using
        End Get
    End Property
    Public Shared Sub CekPasswordChar(ByVal strPassword As String)
        Dim ObjLoginParameter As TList(Of LoginParameter)
        Try
            Dim dataParameter As LoginParameter = DataRepository.LoginParameterProvider.GetPaged("PK_LoginParameter_ID = 1", "", 0, Integer.MaxValue, 0)(0)
            ObjLoginParameter = DataRepository.LoginParameterProvider.GetPaged("PK_LoginParameter_ID = 1", Nothing, 0, 1, 1)
            ' Alphanumeric & Symbol
            If ObjLoginParameter.Count > 0 Then
                If dataParameter.PasswordChar.ToString = True Then
                    If Not (System.Text.RegularExpressions.Regex.IsMatch(strPassword, "^((?=.*[a-z])|(?=.*[A-Z]))(?=.*\d)(?=.*[\W]).*$")) Then
                        Throw New Exception("Password must has alphanumeric and symbol character")
                    End If
                End If
            End If
        Finally
            ObjLoginParameter = Nothing
        End Try
    End Sub

    Public Shared Function IsChangePasswordOnFirstLoginEnabled() As Boolean
        Dim ObjLoginParameter As TList(Of LoginParameter)
        Try
            Dim dataParameter As LoginParameter = DataRepository.LoginParameterProvider.GetPaged("PK_LoginParameter_ID = 1", "", 0, Integer.MaxValue, 0)(0)
            ObjLoginParameter = DataRepository.LoginParameterProvider.GetPaged("PK_LoginParameter_ID = 1", Nothing, 0, 1, 1)
            ' changepasswordonfirstlogin
            If ObjLoginParameter.Count > 0 Then
                If dataParameter.ChangePasswordLogin.ToString = True Then
                    Return True
                Else
                    Return False
                End If
            End If
        Finally
            ObjLoginParameter = Nothing
        End Try
    End Function
#End Region

    Protected Sub LoginButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles LoginButton.Click

        If Me.TextUsername.Text.Length = 0 And Me.TextPassword.Text.Length <> 0 Then
            Me.CustomValidatorLogin.IsValid = False
            Me.CustomValidatorLogin.Text = "* Username is required."
        ElseIf Me.TextPassword.Text.Length = 0 And Me.TextUsername.Text.Length <> 0 Then
            Me.CustomValidatorLogin.IsValid = False
            Me.CustomValidatorLogin.Text = "* Password is required."
        ElseIf Me.TextUsername.Text.Length = 0 And Me.TextPassword.Text.Length = 0 Then
            Me.CustomValidatorLogin.IsValid = False
            Me.CustomValidatorLogin.Text = "* Both Username and Password are required."
        Else
            Try
                Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Using dtUser As AMLDAL.AMLDataSet.UserDataTable = AccessUser.GetUserLoginInfo(Me.TextUsername.Text)
                        If dtUser.Rows.Count > 0 Then
                            'Periksa apakah ada User dgn Username dan Password yang sesuai
                            Dim RowUser As AMLDAL.AMLDataSet.UserRow = dtUser.Rows(0)

                            If RowUser.UserIsDisabled = False Then
                                If RowUser.pkUserID = 1 OrElse Me.GetPasswordRetry(RowUser.UserID) <= Me.GetAccountLockout Then
                                    Dim Salt As String = RowUser.UserPasswordSalt 'Ambil nilai Salt dari tabel User tsb

                                    'Jika Password yg diinput (setelah dienkripsi) sesuai dgn password yg ada dalam tabel User tsb
                                    If RowUser.UserPassword = Sahassa.AML.Commonly.Encrypt(Me.TextPassword.Text, Salt) Then
                                        'If RowUser.pkUserID = 1 OrElse Not Me.IsLoginCanExpire OrElse (Now.Subtract(RowUser.UserLastChangedPassword).Days <= Me.GetExpiredPasswordDay) Then
                                        If RowUser.pkUserID = 1 OrElse (Now.Date.Subtract(Convert.ToDateTime(RowUser.UserLastChangedPassword).Date).Days <= Me.GetExpiredPasswordDay) Then
                                            If RowUser.pkUserID = 1 Or ((RowUser.UserInUsed And RowUser.UserIPAddress = Me.Request.UserHostAddress) Or Not RowUser.UserInUsed) Then
                                                Me.SetLoginCookie = RowUser.UserID
                                                Me.SetPkUserId = RowUser.pkUserID
                                                'Me.SetBranchCookie = RowUser.Fk_MsBranch_id

                                                'Hanya utk Non-SuperUser, periksa apakah User tsb sdh diassign ke suatu WorkingUnit atau belum
                                                If RowUser.pkUserID <> 1 AndAlso Me.IsAssignedToWorkingUnit = False Then
                                                    FormsAuthentication.SetAuthCookie("NoWorkingUnitAssignedRequest", False)

                                                    Sahassa.AML.Commonly.SessionIntendedPage = "NoWorkingUnitAssignedRequest.aspx?UId=" & Me.TextUsername.Text

                                                    Me.Response.Redirect("NoWorkingUnitAssignedRequest.aspx?UId=" & Me.TextUsername.Text, False)
                                                Else
                                                    Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                                                        Me.SetGroupName = AccessGroup.GetGroupNameByGroupID(RowUser.UserGroupId)
                                                    End Using
                                                    Using AccessUserSettings As New AMLDAL.UserSettingsTableAdapters.SelectUserSettingsTableAdapter
                                                        Dim UserSettingsTable As New AMLDAL.UserSettings.SelectUserSettingsDataTable
                                                        Dim UserSettingsTableRow As AMLDAL.UserSettings.SelectUserSettingsRow
                                                        UserSettingsTable = AccessUserSettings.GetData(RowUser.UserID)
                                                        If UserSettingsTable.Rows.Count > 0 Then
                                                            UserSettingsTableRow = UserSettingsTable.Rows(0)
                                                            Sahassa.AML.Commonly.SessionPagingLimit = UserSettingsTableRow.RecordPerPage
                                                        Else
                                                            Sahassa.AML.Commonly.SessionPagingLimit = 10
                                                        End If
                                                    End Using
                                                    AccessUser.UpdateUserLoginInfo(RowUser.pkUserID, Me.Request.UserHostAddress, Now, True)

                                                    Me.AuditLogin = RowUser.UserID

                                                    Dim GroupId As Integer = RowUser.UserGroupId
                                                    Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                                                        Using dtGroup As AMLDAL.AMLDataSet.GroupDataTable = AccessGroup.GetDataByGroupID(GroupId)
                                                            Dim RowGroup As AMLDAL.AMLDataSet.GroupRow = dtGroup.Rows(0)
                                                            If dtGroup.Rows.Count > 0 Then
                                                                Dim GroupName As String = RowGroup.GroupName

                                                                Dim StrRedirect As String = FormsAuthentication.GetRedirectUrl(RowUser.UserID, True)
                                                                Using AccessFileSecurity As New AMLDAL.AMLDataSetTableAdapters.FileSecurityTableAdapter
                                                                    Using dtFileSecurity As AMLDAL.AMLDataSet.FileSecurityDataTable = AccessFileSecurity.GetData
                                                                        Dim Row() As AMLDAL.AMLDataSet.FileSecurityRow = dtFileSecurity.Select("FileSecurityFileName Like '%" & StrRedirect.Split("/"c)(2) & "' and FileSecurityGroupId = " & RowGroup.GroupID)
                                                                        Dim TotalFileSecurity As Integer = Row.Length
                                                                        If TotalFileSecurity > 0 AndAlso StrRedirect.Split("/"c).Length > 0 Then
                                                                            StrRedirect = StrRedirect.Split("/"c)(2)
                                                                        Else
                                                                            StrRedirect = "Default.aspx"
                                                                        End If
                                                                    End Using
                                                                End Using
                                                                FormsAuthentication.SetAuthCookie(RowUser.UserID, False)

                                                                Sahassa.AML.Commonly.SessionIntendedPage = "Default.aspx"

                                                                Me.Response.Redirect(StrRedirect, False)
                                                            End If
                                                        End Using
                                                    End Using
                                                End If
                                            Else
                                                Throw New Exception("Login failed. User Id is being used on a computer with the following IP Address : " & RowUser.UserIPAddress)
                                            End If
                                        Else
                                            FormsAuthentication.SetAuthCookie("ForceChangePassword", False)

                                            Sahassa.AML.Commonly.SessionIntendedPage = "ForceChangePassword.aspx?UId=" & RowUser.pkUserID

                                            Me.Response.Redirect("ForceChangePassword.aspx?UId=" & RowUser.pkUserID, False)
                                        End If
                                    Else
                                        Me.GetPasswordRetry(RowUser.UserID) += 1
                                        Me.CustomValidatorLogin.IsValid = False
                                        If RowUser.pkUserID <> 1 AndAlso Me.GetPasswordRetry(RowUser.UserID) > Me.GetAccountLockout Then
                                            AccessUser.DisableUser(RowUser.UserID)
                                            Me.GetPasswordRetry(RowUser.UserID) = 0
                                            Throw New Exception("Password retry limit exceeded. The following User : " & Me.TextUsername.Text & " has been disabled. Please contact your administrator.")
                                        End If
                                        Throw New Exception("Incorrect Username and/or Password.")
                                    End If
                                Else
                                    Me.GetPasswordRetry(RowUser.UserID) = 0
                                    Throw New Exception("Password retry limit exceeded. The following User : " & Me.TextUsername.Text & " has been disabled. Please contact your administrator.")
                                End If
                            Else
                                Throw New Exception("Unable to login because the following User : " & Me.TextUsername.Text & " is currently disabled.")
                            End If
                        Else
                            Throw New Exception("Incorrect Username and/or Password.")
                        End If
                    End Using
                End Using
            Catch ex As Exception
                Me.CustomValidatorLogin.IsValid = False
                Me.CustomValidatorLogin.ErrorMessage = ex.Message
                Me.LogError(ex)
            End Try
        End If
    End Sub


    Sub Loginwithtoken(struser As String, strtokenuser As String)

        Try



            Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                Using dtUser As AMLDAL.AMLDataSet.UserDataTable = AccessUser.GetUserLoginInfo(struser)
                    If dtUser.Rows.Count > 0 Then
                        'Periksa apakah ada User dgn Username dan Password yang sesuai
                        Dim RowUser As AMLDAL.AMLDataSet.UserRow = dtUser.Rows(0)

                        If RowUser.UserIsDisabled = False Then
                            If RowUser.pkUserID = 1 OrElse Me.GetPasswordRetry(RowUser.UserID) <= Me.GetAccountLockout Then
                                Dim Salt As String = RowUser.UserPasswordSalt 'Ambil nilai Salt dari tabel User tsb

                                'Jika Password yg diinput (setelah dienkripsi) sesuai dgn password yg ada dalam tabel User tsb
                                If RowUser.UserPassword.Replace("+", "") = strtokenuser.Replace("+", "") Then
                                    'If RowUser.pkUserID = 1 OrElse Not Me.IsLoginCanExpire OrElse (Now.Subtract(RowUser.UserLastChangedPassword).Days <= Me.GetExpiredPasswordDay) Then
                                    If RowUser.pkUserID = 1 OrElse (Now.Subtract(RowUser.UserLastChangedPassword).Days <= Me.GetExpiredPasswordDay) Then
                                        If RowUser.pkUserID = 1 Or ((RowUser.UserInUsed And RowUser.UserIPAddress = Me.Request.UserHostAddress) Or Not RowUser.UserInUsed) Then
                                            Me.SetLoginCookie = RowUser.UserID
                                            Me.SetPkUserId = RowUser.pkUserID
                                            'Me.SetBranchCookie = RowUser.Fk_MsBranch_id

                                            'Hanya utk Non-SuperUser, periksa apakah User tsb sdh diassign ke suatu WorkingUnit atau belum
                                            If RowUser.pkUserID <> 1 AndAlso Me.IsAssignedToWorkingUnit = False Then
                                                FormsAuthentication.SetAuthCookie("NoWorkingUnitAssignedRequest", False)

                                                Sahassa.AML.Commonly.SessionIntendedPage = "NoWorkingUnitAssignedRequest.aspx?UId=" & Me.TextUsername.Text

                                                Me.Response.Redirect("NoWorkingUnitAssignedRequest.aspx?UId=" & Me.TextUsername.Text, False)
                                            Else
                                                Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                                                    Me.SetGroupName = AccessGroup.GetGroupNameByGroupID(RowUser.UserGroupId)
                                                End Using
                                                Using AccessUserSettings As New AMLDAL.UserSettingsTableAdapters.SelectUserSettingsTableAdapter
                                                    Dim UserSettingsTable As New AMLDAL.UserSettings.SelectUserSettingsDataTable
                                                    Dim UserSettingsTableRow As AMLDAL.UserSettings.SelectUserSettingsRow
                                                    UserSettingsTable = AccessUserSettings.GetData(RowUser.UserID)
                                                    If UserSettingsTable.Rows.Count > 0 Then
                                                        UserSettingsTableRow = UserSettingsTable.Rows(0)
                                                        Sahassa.AML.Commonly.SessionPagingLimit = UserSettingsTableRow.RecordPerPage
                                                    Else
                                                        Sahassa.AML.Commonly.SessionPagingLimit = 10
                                                    End If
                                                End Using
                                                AccessUser.UpdateUserLoginInfo(RowUser.pkUserID, Me.Request.UserHostAddress, Now, True)

                                                Me.AuditLogin = RowUser.UserID

                                                Dim GroupId As Integer = RowUser.UserGroupId
                                                Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                                                    Using dtGroup As AMLDAL.AMLDataSet.GroupDataTable = AccessGroup.GetDataByGroupID(GroupId)
                                                        Dim RowGroup As AMLDAL.AMLDataSet.GroupRow = dtGroup.Rows(0)
                                                        If dtGroup.Rows.Count > 0 Then
                                                            Dim GroupName As String = RowGroup.GroupName

                                                            Dim StrRedirect As String = FormsAuthentication.GetRedirectUrl(RowUser.UserID, True)
                                                            Using AccessFileSecurity As New AMLDAL.AMLDataSetTableAdapters.FileSecurityTableAdapter
                                                                Using dtFileSecurity As AMLDAL.AMLDataSet.FileSecurityDataTable = AccessFileSecurity.GetData
                                                                    Dim Row() As AMLDAL.AMLDataSet.FileSecurityRow = dtFileSecurity.Select("FileSecurityFileName Like '%" & StrRedirect.Split("/"c)(2) & "' and FileSecurityGroupId = " & RowGroup.GroupID)
                                                                    Dim TotalFileSecurity As Integer = Row.Length
                                                                    If TotalFileSecurity > 0 AndAlso StrRedirect.Split("/"c).Length > 0 Then
                                                                        StrRedirect = StrRedirect.Split("/"c)(2)
                                                                    Else
                                                                        StrRedirect = "Default.aspx"
                                                                    End If
                                                                End Using
                                                            End Using
                                                            FormsAuthentication.SetAuthCookie(RowUser.UserID, False)

                                                            Sahassa.AML.Commonly.SessionIntendedPage = "Default.aspx"

                                                            Me.Response.Redirect(StrRedirect, False)
                                                        End If
                                                    End Using
                                                End Using
                                            End If
                                        Else
                                            Throw New Exception("Login failed. User Id is being used on a computer with the following IP Address : " & RowUser.UserIPAddress)
                                        End If
                                    Else
                                        FormsAuthentication.SetAuthCookie("ForceChangePassword", False)

                                        Sahassa.AML.Commonly.SessionIntendedPage = "ForceChangePassword.aspx?UId=" & RowUser.pkUserID

                                        Me.Response.Redirect("ForceChangePassword.aspx?UId=" & RowUser.pkUserID, False)
                                    End If
                                Else


                                    EKABLL.ScreeningBLL.SetUserAuditInfoLoginNewframework(RowUser.UserID, "Login", Me.Request.UserHostAddress, "Invalid Credential.Please Contact Your Administrator", 2)
                                    Me.GetPasswordRetry(RowUser.UserID) += 1
                                    Me.CustomValidatorLogin.IsValid = False
                                    If RowUser.pkUserID <> 1 AndAlso Me.GetPasswordRetry(RowUser.UserID) > Me.GetAccountLockout Then
                                        AccessUser.DisableUser(RowUser.UserID)
                                        Me.GetPasswordRetry(RowUser.UserID) = 0
                                        Throw New Exception("Password retry limit exceeded. The following User : " & Me.TextUsername.Text & " has been disabled. Please contact your administrator.")
                                    End If
                                    Throw New Exception("Incorrect Username and/or Password.")
                                End If
                            Else
                                Me.GetPasswordRetry(RowUser.UserID) = 0
                                Throw New Exception("Password retry limit exceeded. The following User : " & Me.TextUsername.Text & " has been disabled. Please contact your administrator.")
                            End If
                        Else
                            Throw New Exception("Unable to login because the following User : " & Me.TextUsername.Text & " is currently disabled.")
                        End If
                    Else
                        Throw New Exception("Incorrect Username and/or Password.")
                    End If
                End Using
            End Using
        Catch ex As Exception
            Me.CustomValidatorLogin.IsValid = False
            Me.CustomValidatorLogin.ErrorMessage = ex.Message
            Me.LogError(ex)
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

                Dim struser As String = Request.Params("userid")
                Dim strtokenuser As String = Request.Params("tokenuser")

                If struser <> "" And strtokenuser <> "" Then

                    Loginwithtoken(struser, strtokenuser)

                End If

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                If Sahassa.AML.Commonly.SessionIntendedPage = "" Then
                    'berarti User baru pertama kali masuk ke Login page
                Else
                    'berati User tadinya hendak menuju ke halaman lain tapi krn tdk ada akses authority atau sesuatu hal maka dilempar balik ke Login page.
                    'Using Transcope As New Transactions.TransactionScope
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                        Dim UserAccessAction As String = "Accessing page " & Sahassa.AML.Commonly.SessionIntendedPage & " failed"
                        AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                        'Transcope.Complete()
                    End Using
                    'End Using
                End If

                Using NewsCount As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
                    Dim count As Int32 = NewsCount.CountNewsAtLoginPage()

                    'Kalau count = 0 berarti tidak ada News yang siap ditampilkan
                    If count > 0 Then
                        Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
                            'Dim Rows() As AMLDAL.AMLDataSet.NewsRow = AccessNews.GetNewsforLoginPage(DateTime.Now).Select
                            Dim dtNews As AMLDAL.AMLDataSet.NewsDataTable = AccessNews.GetNewsforLoginPage()

                            Me.NewsRepeater.DataSource = dtNews
                            Me.NewsRepeater.DataBind()
                        End Using
                        Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
                            'Dim Rows() As AMLDAL.AMLDataSet.NewsRow = AccessNews.GetNewsforLoginPage(DateTime.Now).Select
                            Dim dtNews As AMLDAL.AMLDataSet.NewsDataTable = AccessNews.GetNewsforLoginPageColumn2()

                            Me.NewsRepeater1.DataSource = dtNews
                            Me.NewsRepeater1.DataBind()
                        End Using


                        'Me.NewsRepeater.Visible = True
                        Me.tableNews.Visible = True
                    Else
                        'Me.NewsRepeater.Visible = False
                        Me.tableNews.Visible = False
                    End If
                End Using
            End If
        Catch ex As Exception
            Me.CustomValidatorLogin.IsValid = False
            Me.CustomValidatorLogin.ErrorMessage = ex.Message
            Me.LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
        Catch ex As Exception
            Me.CustomValidatorLogin.IsValid = False
            Me.CustomValidatorLogin.ErrorMessage = ex.Message
            Me.LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
            Me.NewsRepeater.DataSource = AccessNews.GetNewsforLoginPage
            Me.NewsRepeater.DataBind()
        End Using
        Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
            Me.NewsRepeater1.DataSource = AccessNews.GetNewsforLoginPageColumn2
            Me.NewsRepeater1.DataBind()
        End Using
    End Sub


    Protected Sub NewsRepeater_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles NewsRepeater.ItemCommand
        If e.CommandName = "ShowMoreNews" Then
            'Ambil nilai NewsID
            Dim NewsID As Integer = Convert.ToInt32(e.CommandArgument)

            'Redirect ke halaman detail News dgn NewsID tersebut
            Me.Response.Redirect("DisplayNews.aspx?NewsID=" & NewsID, False)
        End If
    End Sub
    Protected Sub NewsRepeater1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles NewsRepeater1.ItemCommand
        If e.CommandName = "ShowMoreNews" Then
            'Ambil nilai NewsID
            Dim NewsID As Integer = Convert.ToInt32(e.CommandArgument)

            'Redirect ke halaman detail News dgn NewsID tersebut
            Me.Response.Redirect("DisplayNews.aspx?NewsID=" & NewsID, False)
        End If
    End Sub

    ''' <summary>
    ''' Logger
    ''' </summary>
    ''' <param name="ex"></param>
    ''' <remarks></remarks>
    Private Sub LogError(ByVal ex As Exception)
        Dim Logger As log4net.ILog = log4net.LogManager.GetLogger("AMLError")
        Logger.Error("Exception has occured", ex)
    End Sub
End Class
