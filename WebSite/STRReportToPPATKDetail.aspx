﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="STRReportToPPATKDetail.aspx.vb" Inherits="STRReportToPPATKDetail" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <script src="script/popcalendar2.js"></script>
    <ajax:AjaxPanel ID="lbl" runat="server" Width="100%">
        <table id="title" border="2" bgcolor="#ffffff" bordercolor="#ffffff" cellpadding="2"
            cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
            border-left-style: none; border-bottom-style: none" width="100%">
            <tr>
                <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                    border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                    <strong>STR Reported To PPATK - Detail&nbsp; </strong>
                </td>
                <tr>
                    <td>
                        <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                            Width="100%"></asp:Label>
                    </td>
                </tr>
            </tr>
        </table>
    </ajax:AjaxPanel>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2">
        <tr class="formText">
            <td bgcolor="#ffffff" style="height: 24px" colspan="4">
                <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">
                    <asp:MultiView ID="mtvSTRReporedToPPATKDetail" runat="server">
                        <asp:View runat="server" ID="detail">
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 81px">
                                <tr>
                                    <td style="width: 24px; height: 35px">
                                    </td>
                                    <td style="width: 16%; height: 35px">
                                        Case ID
                                    </td>
                                    <td align="left" style="width: 45px; height: 35px">
                                        :
                                    </td>
                                    <td style="width: 2%; height: 35px">
                                    </td>
                                    <td style="width: 80%; height: 35px">
                                        <asp:Label ID="lblCASEID" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 24px; height: 35px">
                                    </td>
                                    <td style="width: 16%; height: 35px">
                                        Case Description
                                    </td>
                                    <td align="left" style="width: 45px; height: 35px">
                                        :
                                    </td>
                                    <td style="width: 2%; height: 35px">
                                    </td>
                                    <td style="width: 80%; height: 35px">
                                        <asp:Label ID="lblCaseDescription" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 24px; height: 35px">
                                    </td>
                                    <td style="width: 16%; height: 35px">
                                        Status
                                    </td>
                                    <td align="left" style="width: 45px; height: 35px">
                                        :
                                    </td>
                                    <td style="width: 2%; height: 35px">
                                    </td>
                                    <td style="width: 80%; height: 35px">
                                        <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 24px; height: 35px">
                                    </td>
                                    <td style="width: 16%; height: 35px">
                                        Created Date
                                    </td>
                                    <td align="left" style="width: 45px; height: 35px">
                                        :
                                    </td>
                                    <td style="width: 2%; height: 35px">
                                    </td>
                                    <td style="width: 80%; height: 35px">
                                        <asp:Label ID="lblCreatedDate" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 24px; height: 35px">
                                    </td>
                                    <td style="width: 16%; height: 35px">
                                        Last Update Date
                                    </td>
                                    <td align="left" style="width: 45px; height: 35px">
                                        :
                                    </td>
                                    <td style="width: 2%; height: 35px">
                                    </td>
                                    <td style="width: 80%; height: 35px">
                                        <asp:Label ID="lblLastUpdateDate" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 24px; height: 35px">
                                    </td>
                                    <td style="width: 16%; height: 35px">
                                        Assigned Branch
                                    </td>
                                    <td align="left" style="width: 45px; height: 35px">
                                        :
                                    </td>
                                    <td style="width: 2%; height: 35px">
                                    </td>
                                    <td style="width: 80%; height: 35px">
                                        <asp:Label ID="lblAssignBranch" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 24px; height: 35px">
                                    </td>
                                    <td style="width: 16%; height: 35px">
                                        PIC
                                    </td>
                                    <td align="left" style="width: 45px; height: 35px">
                                        :
                                    </td>
                                    <td style="width: 2%; height: 35px">
                                    </td>
                                    <td style="width: 80%; height: 35px">
                                        <asp:Label ID="lblPIC" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 24px; height: 35px">
                                    </td>
                                    <td style="width: 16%; height: 35px">
                                        Aging
                                    </td>
                                    <td align="left" style="width: 45px; height: 35px">
                                        :
                                    </td>
                                    <td style="width: 2%; height: 35px">
                                    </td>
                                    <td style="width: 80%; height: 35px">
                                        <asp:Label ID="lblAging" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 24px; height: 35px">
                                    </td>
                                    <td style="width: 16%; height: 35px">
                                        Reported To PPATK
                                    </td>
                                    <td align="left" style="width: 45px; height: 35px">
                                        :
                                    </td>
                                    <td style="width: 2%; height: 35px">
                                    </td>
                                    <td style="width: 80%; height: 35px">
                                        <asp:RadioButtonList ID="rblIsReportedToPPATK" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Value="0">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 24px; height: 35px">
                                    </td>
                                    <td style="width: 16%; height: 35px">
                                        Reporting Date
                                    </td>
                                    <td align="left" style="width: 45px; height: 35px">
                                        :
                                    </td>
                                    <td style="width: 2%; height: 35px">
                                    </td>
                                    <td style="width: 80%; height: 35px">
                                        <asp:TextBox ID="txtReportingDate" runat="server"></asp:TextBox>
                                        <strong><span style="color: #ff0000">
                                            <input id="popUpReportingDate" runat="server" name="popUpCalc0" style="border-right: #ffffff 0px solid;
                                                border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                width: 16px;" title="Click to show calendar" type="button" />
                                        </span></strong></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 24px; height: 35px">
                                    </td>
                                    <td style="width: 16%; height: 35px">
                                        No STR PPATK
                                    </td>
                                    <td align="left" style="width: 45px; height: 35px">
                                        :
                                    </td>
                                    <td style="width: 2%; height: 35px">
                                    </td>
                                    <td style="width: 80%; height: 35px">
                                        <asp:TextBox ID="txtNoSTRPPATK" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="formText" bgcolor="#dddddd" height="30">
                                    <td style="width: 24px">
                                        <img height="15" src="images/arrow.gif" width="15">
                                    </td>
                                    <td style="height: 35px" colspan="4">
                                        <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="True" SkinID="SaveButton"
                                            ImageUrl="~/Images/button/save.gif"></asp:ImageButton>
                                        <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"
                                            ImageUrl="~/Images/button/cancel.gif"></asp:ImageButton><br />
                                        <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
                                    </td>
                                </tr>
                            </table>
                            <strong><span style="color: #ff0000"></span></strong>
                        </asp:View>
                        <asp:View runat="server" ID="dialog">
                            <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                border="2">
                                <tr align="center">
                                    <td style="height: 20px">
                                        Data saved
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td>
                                        <asp:ImageButton ID="btnOK" runat="server" ImageUrl="~/Images/button/ok.gif" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                    </asp:MultiView>
                </ajax:AjaxPanel>
            </td>
        </tr>
    </table>
</asp:Content>
