<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" 
CodeFile="IFTI_Detail_NonSwift_Incoming.aspx.vb" Inherits="IFTI_Detail_NonSwift_Incoming" title="Untitled Page" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
 <script src="Script/popcalendar.js"></script>
 <script language="javascript" type="text/javascript">
     function hidePanel(objhide, objpanel, imgmin, imgmax) {
         document.getElementById(objhide).style.display = 'none';
         document.getElementById(objpanel).src = imgmax;
     }
     // JScript File


     function popWinNegara() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerNegara.aspx", "#3", winSetting);
     }

     function popWinProvinsi() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerProvinsi.aspx", "#5", winSetting);
     }
     function popWinPekerjaan() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerPekerjaan.aspx", "#6", winSetting);
     }
     function popWinKotaKab() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerKotaKab.aspx", "#7", winSetting);
     }
     function popWinBidangUsaha() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerBidangUsaha.aspx", "#8", winSetting);
     }
     function popWinMataUang() {
         var height = '600px';
         var width = '550px';
         var left = (screen.availWidth - width) / 2;
         var top = (screen.availHeight - height) / 2;
         var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

         window.showModalDialog("PickerMataUang.aspx", "#4", winSetting);

     }

	</script>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        
                        <td width="99%" bgcolor="#FFFFFF" style="height: 20px">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td bgcolor="#FFFFFF" style="height: 20px">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="10" height="100%" />
                            </td>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="a" runat="server">
                                    
                                </ajax:AjaxPanel>
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <asp:MultiView ID="MultiViewEditNonSwiftIn" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="ViewNonSwiftIn" runat="server">
                                    <table id="Table4" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                           style="border-top-style: none; border-right-style: none; border-left-style: none;
                                           border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <img src="Images/dot_title.gif" width="17" height="17" />
                                                <strong>
                                                    <asp:Label ID="Labeld1" runat="server" 
                                                    Text="IFTI Non Swift Incoming - Detail "></asp:Label>
                                                </strong>
                                                <hr />
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" align="left"
                                                style="height: 6px; width: 100%;">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Labelrt16" runat="server" Text="A. Umum" Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('SwiftIN_Umum','Img13','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                               title="click to minimize or maximize">
                                                            <img id="Img15" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                 width="12px" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="NonSwiftIn_Umum">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                             <tr>
                                                <td style="width: 257px; background-color: #fff7e6; height: 22px;">
                                                    <asp:Label ID="Label18" runat="server" Text="a. No. LTDLN "></asp:Label>
                                                </td>
                                                <td style="height: 22px">
                                                    <asp:TextBox ID="NonSwiftInUmum_LTDN" runat="server" CssClass="searcheditbox" MaxLength="30"
                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                </td>                                                                                            
                                             </tr>
                                             <tr>
                                                <td style="width: 257px; background-color: #fff7e6">
                                                    <asp:Label ID="Label20" runat="server" Text="b. No. LTDLN Koreksi " Width="132px"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="NonSwiftInUmum_LtdnKoreksi" runat="server" CssClass="searcheditbox" MaxLength="30"
                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                </td>                                                                                            
                                             </tr>
                                             <tr>
                                                <td style="width: 257px; background-color: #fff7e6">
                                                    <asp:Label ID="Labelo101" runat="server" Text="c. Tanggal Laporan "></asp:Label>
                                                    <asp:Label ID="Labelo149" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="NonSwiftInUmum_TanggalLaporan" runat="server" CssClass="searcheditbox"
                                                        TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>&nbsp;
                                                </td>                                                                                            
                                             </tr>
                                             <tr>
                                                <td style="width: 257px; background-color: #fff7e6">
                                                    <asp:Label ID="Labelo159" runat="server" Text="d. Nama PJK Bank Pelapor " Width="162px"></asp:Label>
                                                    <asp:Label ID="Labelo196" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="NonSwiftInUmum_NamaPJKBank" runat="server" 
                                                        CssClass="searcheditbox" MaxLength="100" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                </td>                                                                                            
                                              </tr>
                                             <tr>
                                                <td style="width: 257px; background-color: #fff7e6; height: 22px;">
                                                    <asp:Label ID="Labeloo199" runat="server" Text="e. Nama Pejabat PJK Bank Pelapor" Width="204px"></asp:Label>
                                                    <asp:Label ID="Labelo207" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                                <td style="height: 22px">
                                                    <asp:TextBox ID="NonSwiftInUmum_NamaPejabatPJKBank" runat="server" CssClass="searcheditbox" MaxLength="100"
                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                </td>                                                                                            
                                             </tr>
                                             <tr>
                                                <td style="width: 257px; background-color: #fff7e6">
                                                    <asp:Label ID="Labelo208" runat="server" Text="f. Jenis Laporan"></asp:Label>
                                                    <asp:Label ID="Labelo211" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:RadioButtonList ID="RbNonSwiftInUmum_JenisLaporan" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" Enabled="False">
                                                        <asp:ListItem Value="1">Baru</asp:ListItem>
                                                        <asp:ListItem Value="2">Recall</asp:ListItem>
                                                        <asp:ListItem Value="3">Koreksi</asp:ListItem>
                                                        <asp:ListItem Value="4">Reject</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>                                                                                            
                                             </tr>
                                             <tr>
                                                <td style="width: 257px">
                                                </td>
                                                <td>
                                                </td>                                                                                            
                                             </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table id= "Table6" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                               bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" align="left"
                                                style="height: 11px; width: 100%;">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Labelo216" runat="server" Text="B.  Identitas Pengirim" 
                                                                 Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('B1SwiftInidentitasPENERIMA','Img1E4','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                            <img id="Img16" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="B1NonSwiftInIdentitasPENGIRIM">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;">
                                                <table style="width: 100%">
                                                    <tr runat="server" visible="false">
                                                        <td style="width: 25%">
                                                        </td>
                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                            <asp:Label ID="Label217" runat="server" Text="       Tipe Pengirim "></asp:Label>
                                                            <asp:Label ID="Label22q5" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                            <ajax:AjaxPanel ID="AjaxPanel14" runat="server">
                                                                <asp:RadioButtonList ID="rbrbNonSwiftIn_TipePengirim" runat="server" 
                                                                    RepeatDirection="Horizontal" AutoPostBack="True" Width="189px" Enabled="False">
                                                                    <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                    <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                        <td style="width: 25%">
                                                        </td>
                                                    </tr>
                                                    <tr ID="trNonSwiftInTipeNasabah" runat="server" visible="false">
                                                        <td style="width: 25%">
                                                            &nbsp;</td>
                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                            <asp:Label ID="Label2w26" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                            <asp:Label ID="Label2c27" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                            <ajax:AjaxPanel ID="AjaxPanel16" runat="server">
                                                                <asp:RadioButtonList ID="RB_NonSwiftIn_TipeNasabah" runat="server" 
                                                                    AutoPostBack="True" RepeatDirection="Horizontal" Enabled="False">
                                                                    <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                    <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                        <td style="width: 25%">
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
												<ajax:AjaxPanel ID="AjaxPanel15" runat="server" Width="100%">
                  <asp:MultiView ID="MultiViewNonSwiftInPengirim" runat="server">
                     <asp:View ID="ViewNonSwiftInPengirimAkhir" runat="server">
                        <asp:MultiView ID="MultiViewNonSwiftInPengirimAkhir" runat="server">
                           <asp:View ID="ViewNonSwiftInPengirimNasabah" runat="server">
                            <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2" id="TABLE7" >
                                        <tr>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext" style="height: 14px">
                                                            <asp:Label ID="Labelqq223" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td style="height: 14px">
                                                            <a href="#" onclick="javascript:ShowHidePanel('B1sWIFTiNidentitasPENerima1nasabah','Img1e5','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                               title="click to minimize or maximize">
                                                            <img id="Img17" src="Images/search-bar-minimize.gif" border="0" height="12"
                                                                 width="12" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="Tr5">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <tr>
                                                    <td style="width: 100%">
                                                        <table>
                                                            <tr>
                                                                <td style="width: 20%; background-color: #fff7e6">
                                                                    <asp:Label ID="Labelq224" runat="server" Text="a. No. Rek " Width="84px"></asp:Label>
                                                                 </td>
                                                                 <td style="width: 124px">
                                                                     <asp:TextBox ID="txtNonSwiftInPengirimNasabah_IND_Rekening" runat="server" 
                                                                          CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="154px" Enabled="False"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 53px">
                                                                     &nbsp;
                                                                </td>                                                                
                                                            </tr>
                                                        </table>
                                                    </td>                                                    
                                                </tr>
                                                <tr>
                                                    <td style="width: 610px">
                                                        <asp:MultiView ID="MultiViewPengirimAkhirNasabah" runat="server" 
                                                            ActiveViewIndex="0">                                                       
                                                            <asp:View ID="VwNonSwiftIn_PENG_IND" runat="server">
                                                             <ajax:AjaxPanel ID="AjaxPanel17" runat="server">
                                                                   <table style="width: 98%; height: 49px">
                                                                       <tr>
                                                                            <td style="width: 50%; height: 15px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label2s28" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; width: 222px;">
                                                                            </td>
                                                                            <td style="height: 15px; width: 35px;">
                                                                            </td>
                                                                            <td style="width: 47px; height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                       <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label233" runat="server" Text="b. Nama Lengkap"></asp:Label>
                                                                                <asp:Label ID="Label2d35" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="TxtNonSwiftInIdenPengirimNas_Ind_NamaLengkap" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="255"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 35px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                       <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label2s37" runat="server" Text="c. Tanggal lahir (tgl/bln/thn)" 
                                                                                    Width="183px"></asp:Label>
                                                                                </td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="TxtNonSwiftInIdenPengirimNas_Ind_TglLahir" runat="server" 
                                                                                    CssClass="searcheditbox" TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                </td>
                                                                            <td style="width: 35px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                                </td>
                                                                        </tr>
                                                                       <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label239" runat="server" Text="d. Kewarganegaraan (Pilih salah satu) "
                                                                                    Width="223px"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:RadioButtonList ID="RbNonSwiftInIdenPengirimNas_Ind_Kewarganegaraan" 
                                                                                    runat="server" RepeatDirection="Horizontal" AutoPostBack="True" Enabled="False">
                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                </asp:RadioButtonList></td>
                                                                            <td style="width: 35px">
                                                                            </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                       <tr id ="Negara_warganegaraPengirim" runat="server" visible="false">
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label240" runat="server" Text=" Negara "></asp:Label>
                                                                                <br />
                                                                                <asp:Label ID="Label244" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                    Width="258px"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="TxtNonSwiftInIdenPengirimNas_Ind_Negara" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                <asp:HiddenField ID="hfNonSwiftInIdenPengirimNas_Ind_Negara" 
                                                                                    runat="server" />
                                                                            </td>
                                                                            <td style="width: 35px">
                                                                                </td>
                                                                            <td style="width: 47px">
                                                                            </td>
                                                                        </tr>
                                                                       <tr id ="NegaraLain_warganegaraPengirim" runat="server" visible="false">
                                                                           <td style="width: 50%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label245" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px">
                                                                               <asp:TextBox ID="TxtNonSwiftInIdenPengirimNas_Ind_NegaraLainnya" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           <td style="width: 35px">
                                                                           </td>
                                                                           <td style="width: 47px">
                                                                           </td>
                                                                       </tr>
                                                                       <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6; ">
                                                                                <asp:Label ID="Labelfg88" runat="server" 
                                                                                    Text="e.  Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                            <td style="width: 222px; ">
                                                                                <%-- <ajax:AjaxPanel ID="AjaxPanelCheckPengirimNas_Ind_alamat" runat="server">--%>
                                                                                    
                                                                                   <%--</ajax:AjaxPanel>--%></td>
                                                                            <td style="width: 35px; ">
                                                                            </td>
                                                                            <td style="width: 47px; ">
                                                                            </td>
                                                                        </tr>
                                                                       <tr>
                                                                           <td style="width: 50%; background-color: #fff7e6; height: 40px;">
                                                                               <asp:Label ID="Labelfg89" runat="server" Text="Alamat"></asp:Label>
                                                                               </td>
                                                                           <td style="width: 222px; height: 40px;">
                                                                               <asp:TextBox ID="TxtNonSwiftInIdenPengirimNas_Ind_alamat" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="100"
                                                                                   TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                           <td style="width: 35px; height: 40px;">
                                                                           </td>
                                                                           <td style="width: 47px; height: 40px;">
                                                                           </td>
                                                                       </tr>
                                                                       <tr>
                                                                           <td style="width: 50%; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelfg91" runat="server" Text="Negara Bagian/Kota"></asp:Label>
                                                                               </td>
                                                                           <td style="width: 222px">
                                                                               <asp:TextBox ID="TxtNonSwiftInIdenPengirimNas_Ind_NegaraBagian" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="30"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                               
                                                                           </td>
                                                                           <td style="width: 35px">
                                                                           </td>
                                                                           <td style="width: 47px">
                                                                           </td>
                                                                       </tr>
                                                                       <tr>
                                                                           <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelprovinsi" runat="server" Text="Negara"></asp:Label>
                                                                               <asp:Label ID="Labels269" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                           </td>
                                                                           <td style="width: 222px; height: 18px">
                                                                               <asp:TextBox ID="TxtNonSwiftInIdenPengirimNas_Ind_NegaraIden" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                               <asp:HiddenField ID="hfNonSwiftInIdenPengirimNas_Ind_NegaraIden" runat="server" />
                                                                           </td>
                                                                           <td>
                                                                               &nbsp;</td>
                                                                           <td style="width: 47px; height: 18px;">
                                                                               &nbsp;</td>
                                                                       </tr>
                                                                       <tr>
                                                                           <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelqa62" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px; height: 18px">
                                                                               <asp:TextBox ID="TxtNonSwiftInIdenPengirimNas_Ind_NegaralainIden" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           <td style="width: 35px; height: 18px">
                                                                           </td>
                                                                           <td style="width: 47px; height: 18px">
                                                                           </td>
                                                                       </tr>
                                                                       <tr>
                                                                            
                                                                            <td style="width: 54%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label9321" runat="server" Text="f. No. Telepon"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="TxtNonSwiftInIdenPengirimNas_Ind_NoTelp" runat="server" CssClass="searcheditbox" MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 35px">
                                                                                &nbsp;</td>
                                                                            <td style="width: 47px">
                                                                                &nbsp;</td>
                                                                            
                                                                        </tr>
                                                                       <tr>
                                                                           <td style="width: 50%; background-color: #fff7e6">
                                                                               <asp:Label ID="Labela33" runat="server" Text="g. Jenis Dokument Identitas"></asp:Label>
                                                                           </td>
                                                                           <td style="width: 222px">
                                                                               <asp:DropDownList ID="cbo_NonSwiftInIdenPengirimNas_Ind_jenisidentitas" runat="server" CssClass="combobox" Enabled="False">
                                                                               </asp:DropDownList>
                                                                           </td>
                                                                           <td style="width: 35px"></td>
                                                                           <td style="width: 47px"></td>
                                                                       </tr>
                                                                       <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Labela35" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 18px; width: 222px;">
                                                                                <asp:TextBox ID="TxtNonSwiftInIdenPengirimNas_Ind_noIdentitas" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="30"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 35px; height: 18px;">
                                                                            </td>
                                                                            <td style="width: 47px; height: 18px">
                                                                            </td>
                                                                        </tr>
                                                                       <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6; height: 15px;">
                                                                            </td>
                                                                            <td style="height: 15px; width: 222px;">
                                                                            </td>
                                                                            <td style="width: 35px; height: 15px;">
                                                                            </td>
                                                                            <td style="width: 47px; height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                   </table>
                                                             </ajax:AjaxPanel>
                                                            </asp:View>
                                                            <asp:View ID="VwNonSwiftIn_PENG_Corp" runat="server">
                                                            <table style="width: 100%; height: 31px">
                                                                        <tr>
                                                                            <td style="width: 54%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label1" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; color: #000000; width: 240px;">
                                                                            </td>
                                                                            <td style="width: 240px; color: #000000; height: 15px">
                                                                            </td>
                                                                            <td style="height: 15px; width: 2977px; color: #000000;">
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="color: #000000">
                                                                            <td style="width: 54%; background-color: #fff7e6; height: 20px;">
                                                                                <asp:Label ID="Label4" runat="server" Text="b. Nama Korporasi"></asp:Label>
                                                                                <asp:Label ID="Label9317" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 240px; height: 20px;">
                                                                                <asp:TextBox ID="TxtNonSwiftInIdenPengirimNas_Korporasi_NamaKorporasi" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="255" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 240px; height: 20px;">
                                                                            </td>
                                                                            <td style="width: 2977px; height: 20px;">
                                                                            </td>
                                                                        </tr>                                                                                                                                               
                                                                        <tr>
                                                                            <td style="width: 54%; background-color: #fff7e6; ">
                                                                                <asp:Label ID="Label100" runat="server" Text="c. Alamat"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 222px; height: 20px;">
                                                                                <asp:TextBox ID="TxtNonSwiftInIdenPengirimNas_Korporasi_alamat" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="100"
                                                                                    TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 35px; height: 20px;">
                                                                            </td>
                                                                            <td style="width: 47px; height: 20px;">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 54%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label101" runat="server" Text="Negara Bagian / Kota"></asp:Label>
                                                                            </td>
                                                                            <td style="height: 18px; width: 222px;">
                                                                                <asp:TextBox ID="TxtNonSwiftInIdenPengirimNas_Korporasi_NegaraBagian" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="30"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            <td style="height: 18px; width: 35px;">
                                                                                </td>
                                                                            <td style="width: 47px; height: 18px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 54%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label102" runat="server" Text="Negara"></asp:Label>
                                                                               <asp:Label ID="Label9323" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                           <td style="width: 222px; height: 18px">
                                                                                <asp:TextBox ID="TxtNonSwiftInIdenPengirimNas_Korporasi_Negara" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                <asp:HiddenField ID="HfNonSwiftInIdenPengirimNas_Korporasi_Negara" 
                                                                                    runat="server" />
                                                                           </td>
                                                                           <td style="width: 35px; height: 18px">
                                                                           </td>
                                                                           <td style="width: 47px; height: 18px">
                                                                           </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 54%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label103" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                               <asp:TextBox ID="txtNonSwiftInIdenPengirimNas_Korporasi_NegaraLainnya" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;</td>
                                                                            <td style="width: 47px">
                                                                                &nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 54%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label104" runat="server" Text="d. No. Telepon"></asp:Label></td>
                                                                           <td style="width: 222px">
                                                                               <asp:TextBox ID="txtNonSwiftInIdenPengirimNas_Korporasi_NoTelp" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="30"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           <td style="width: 35px">
                                                                           </td>
                                                                           <td style="width: 47px">
                                                                           </td>
                                                                       </tr>                                                                       
                                                            </table>
                                                            </asp:View>
                                                        </asp:MultiView>
                                                    </td>                                                    
                                                </tr>                                                                                             
                                                </table>
                                            </td>
                                        </tr>
                            </table>
                           </asp:View>
                           <asp:View ID="ViewNonSwiftInPengirimNonNasabah" runat="server">
                               <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 7px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext" style="height: 14px">
                                                            <asp:Label ID="LabelNonSwiftIna4" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td style="height: 14px">
                                                            <a href="#" onclick="javascript:ShowHidePanel('B1SwiftInidentitasPENerimaonNasabah','Img23','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                               title="click to minimize or maximize">
                                                            <img id="Img23" src="Images/search-bar-minimize.gif" border="0" height="12"
                                                                 width="12" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="B1NonSwiftInidentitasPengirimNonNasabah">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">                                               
                                                <table style="width: 100%; height: 49px">
                                                    <tr>
                                                        <td style="width: 20%; background-color: #fff7e6">
                                                            <asp:Label ID="Labelq81" runat="server" Text="a. No. Rek"></asp:Label>
                                                            </td>
                                                        <td style="width: 35%">
                                                            <asp:TextBox ID="TxtNonSwiftInPengirimNonNasabah_NomorRek" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                        <td style="width: 5%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%; background-color: #fff7e6">
                                                            <asp:Label ID="LabelNonSwiftIn82" runat="server" Text="b. Nama Bank" 
                                                                Width="87px"></asp:Label>
                                                            </td>
                                                        <td style="width: 35%">
                                                            <asp:TextBox ID="TxtNonSwiftInPengirimNonNasabah_NamaBank" runat="server" 
                                                                CssClass="searcheditbox" TabIndex="2"
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                            &nbsp;</td>
                                                        <td style="width: 5%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%; background-color: #fff7e6">
                                                            <asp:Label ID="LabelNonSwiftIn95" runat="server" Text="c. Nama Lengkap"></asp:Label>
                                                            <asp:Label ID="Labelq86" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%">
                                                            <asp:TextBox ID="TxtNonSwiftInPengirimNonNasabah_NamaLengkap" runat="server" 
                                                             CssClass="searcheditbox" MaxLength="255" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 5%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%; background-color: #fff7e6">
                                                            <asp:Label ID="LabelNonSwiftIn84" runat="server" 
                                                                Text="d. Tanggal Lahir (tgl/bln/thn)"></asp:Label></td>
                                                        <td style="width: 35%;">
                                                            <asp:TextBox ID="TxtNonSwiftInPengirimNonNasabah_TanggalLhr" runat="server" 
                                                             CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                        </td>
                                                        <td style="width: 5%;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%; background-color: #fff7e6; height: 24px;">
                                                            <asp:Label ID="LabelNonSwiftIn100" runat="server" Text="e. Alamat"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%; height: 24px;">
                                                            <asp:TextBox ID="TxtNonSwiftInPengirimNonNasabah_Alamat" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 5%; height: 24px;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20%; background-color: #fff7e6;">
                                                            <asp:Label ID="Label9316" runat="server" Text="Negara Bagian / Kota"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%;">
                                                            <asp:TextBox ID="TxtNonSwiftInPengirimNonNasabah_Kota" runat="server" 
                                                                 CssClass="searcheditbox" MaxLength="30"
                                                                 TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 5%;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%; background-color: #fff7e6">
                                                            <asp:Label ID="Label7" runat="server" Text="Negara"></asp:Label>
                                                            <asp:Label ID="Labelq88" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                        </td>
                                                        <td style="width: 222px">
                                                            <asp:TextBox ID="TxtNonSwiftInPengirimNonNasabah_Negara" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                             <asp:HiddenField ID="hfNonSwiftInPengirimNonNasabah_Negara" 
                                                                  runat="server" />
                                                        </td>
                                                        <td style="width: 5%;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%; background-color: #fff7e6">
                                                            <asp:Label ID="Label3" runat="server" Text="Negara Lainnya"></asp:Label>
                                                        </td>
                                                        <td style="width: 222px">
                                                            <asp:TextBox ID="TxtNonSwiftInPengirimNonNasabah_NegaraLainnya" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 5%;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 54%; background-color: #fff7e6">
                                                            <asp:Label ID="Label9322" runat="server" Text="f. No. Telepon"></asp:Label>
                                                        </td>
                                                        <td style="width: 222px">
                                                            <asp:TextBox ID="TxtNonSwiftInPengirimNonNasabah_NoTelp" runat="server" CssClass="searcheditbox" MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 5%;">&nbsp;</td>
                                                    </tr>
                                                 </table>
                                            </td>
                                        </tr>
                                </table>
                            </asp:View>
                        </asp:MultiView>
                     </asp:View>
                  </asp:MultiView>
                                                </ajax:AjaxPanel>
                                            </td>
                                        </tr>
                                    </table>
                                    <table id= "Table4" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" align="left"
                                                style="height: 11px; width: 100%;">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Label2" runat="server" Text="C.  Identitas Penerima" 
                                                                Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('B1SwiftInidentitasPENERIMA','Img1E4','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Img1" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="B1SwiftInidentitasPENERIMA">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;">
                                                <table style="width: 100%">                                                        
                                                    <tr id="trNonSwiftInTipepengirim" runat="server" visible="false">
                                                        <td style="width: 25%">
                                                        </td>
                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                            <asp:Label ID="Label53" runat="server" Text="       Tipe Penerima "></asp:Label></td>
                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                                                <asp:RadioButtonList ID="Rb_IdenPenerima_TipePenerima" runat="server" 
                                                                    RepeatDirection="Horizontal" AutoPostBack="True" Width="189px" Enabled="False">
                                                                    <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                    <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                        <td style="width: 25%">
                                                        </td>
                                                    </tr>
                                                    <tr ID="trNonSwiftInPenerimaTipeNasabah" runat="server" visible="false">
                                                        <td style="width: 25%">
                                                            &nbsp;</td>
                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                            <asp:Label ID="Label22" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                        </td>
                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;">
                                                            <ajax:AjaxPanel ID="AjaxPanel5" runat="server" style="text-align: left">
                                                                <asp:RadioButtonList ID="Rb_IdenPenerima_TipeNasabah" runat="server" 
                                                                    AutoPostBack="True" RepeatDirection="Horizontal" Enabled="False">
                                                                    <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                    <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                        <td style="width: 25%">
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
												<ajax:AjaxPanel ID="AjaxPanel3" runat="server" Width="100%">

                                                <asp:MultiView ID="MultiViewNonSwiftInPenerima" runat="server">
                                                    <asp:View ID="ViewNonSwiftInPenerimaAkhir" runat="server">
                                                        <asp:MultiView ID="MultiViewNonSwiftInPenerimaAkhir" runat="server">
                                                            <asp:View ID="ViewNonSwiftInPenerimaNasabah" runat="server">
                                                             <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                                    bgcolor="#dddddd" border="2" id="TABLE5" >
                                         <tr>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext" style="height: 14px">
                                                            <asp:Label ID="Labelrf8" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td style="height: 14px">
                                                            <a href="#" onclick="javascript:ShowHidePanel('B1NonsWIFTiNidentitasPENerima1nasabah','Imgnon2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                               title="click to minimize or maximize">
                                                            <img id="Imgnon2" src="Images/search-bar-minimize.gif" border="0" height="12"
                                                                 width="12" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="B1NonsWIFTiNidentitasPENerima1nasabah">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                 <tr>
                                                    <td style="width: 100%">
                                                        <table>
                                                            <tr>
                                                                <td style="width: 50%; background-color: #fff7e6">
                                                                    <asp:Label ID="Labeld9" runat="server" Text="No. Rek " Width="47px"></asp:Label>
                                                                    <asp:Label ID="Labeld10" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                 <td style="width: 222px">
                                                                     <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_Rekening" runat="server" 
                                                                         CssClass="searcheditbox" MaxLength="50"
                                                                         TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                 <td>
                                                                </td>
                                                                <td>
                                                                    &nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    
                                                </tr>
                                                 <tr>
                                                    <td style="width: 610px">
                                                        <asp:MultiView ID="MultiViewPenerimaAkhirNasabah" runat="server" 
                                                            ActiveViewIndex="0">
                                                       
                                                            <asp:View ID="VwNonSwiftIn_PEN_IND" runat="server">
                                                             <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                                                                   <table style="width: 98%; height: 49px">
                                                                        <tr>
                                                                            <td style="width: 50%; height: 15px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label11" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; width: 222px;">
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label13" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                <asp:Label ID="Label14" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_IND_nama" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="255"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label123" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label>
                                                                                <asp:Label ID="Label137" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_IND_TanggalLahir" runat="server" 
                                                                                    CssClass="searcheditbox" TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                </td>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label138" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                    Width="223px"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:RadioButtonList ID="RbNonSwiftInPenerimaNasabah_IND_Kewarganegaraan" 
                                                                                    runat="server" RepeatDirection="Horizontal" AutoPostBack="True" Enabled="False">
                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                </asp:RadioButtonList></td>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id ="Negara_warganegaraPenerima" runat="server" visible="false">
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label139" runat="server" Text="Negara "></asp:Label>
                                                                                <br />
                                                                                <asp:Label ID="Label142" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                    Width="258px"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_IND_negara" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                <asp:HiddenField ID="hfNonSwiftInPenerimaNasabah_IND_negara" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id ="NegaraLain_warganegaraPenerima" runat="server" visible="false">
                                                                           <td style="width: 50%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label143" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px">
                                                                               <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_IND_negaralain" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           <td>
                                                                           </td>
                                                                           <td>
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labelq144" runat="server" Text="d.  Pekerjaan"></asp:Label>
                                                                                <asp:Label ID="Labelq145" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_IND_pekerjaan" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                <asp:HiddenField ID="hfNonSwiftInIdenPenerimaNas_Ind_pekerjaan" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 50%; background-color: #fff7e6; height: 20px;">
                                                                               <asp:Label ID="Labelq45" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px; height: 20px;">
                                                                                <asp:TextBox ID="TxtNonSwiftInIdenPenerimaNas_Ind_Pekerjaanlain" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           <td style="height: 20px">
                                                                           </td>
                                                                           <td style="height: 20px">
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labelq37" runat="server" Text="e. Alamat Domisili"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                </td>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6; height: 20px;">
                                                                                <asp:Label ID="Labelq38" runat="server" Text="Alamat"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 222px; height: 20px;">
                                                                                <asp:TextBox ID="TxtNonSwiftInIdenPenerimaNas_Ind_Alamat" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="100"
                                                                                    TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="Labelq39" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                            <td style="height: 18px; width: 222px;">
                                                                                <asp:TextBox ID="TxtNonSwiftInIdenPenerimaNas_Ind_Kota" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                <asp:HiddenField ID="hfNonSwiftInIdenPenerimaNas_Ind_Kota_dom" runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelq57" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px; height: 18px">
                                                                                <asp:TextBox ID="TxtNonSwiftInIdenPenerimaNas_Ind_kotalain" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           <td>
                                                                           </td>
                                                                           <td>
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labelq41" runat="server" Text="Provinsi"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                               <asp:TextBox ID="TxtNonSwiftInIdenPenerimaNas_Ind_provinsi" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                <asp:HiddenField ID="hfNonSwiftInIdenPenerimaNas_Ind_provinsi_dom" 
                                                                                    runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;</td>
                                                                            <td>
                                                                                &nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 50%; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelq186" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px">
                                                                               <asp:TextBox ID="TxtNonSwiftInIdenPenerimaNas_Ind_provinsiLain" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           <td>
                                                                           </td>
                                                                           <td>
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6; height: 22px;">
                                                                                <asp:Label ID="Labelq42" runat="server" Text="f. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                            <td style="width: 222px; height: 22px;">
                                                                                <%-- <ajax:AjaxPanel ID="AjaxPanelCheckPengirimNas_Ind_alamat" runat="server">--%>
                                                                                &nbsp;<%--</ajax:AjaxPanel>--%></td>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 50%; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelq43" runat="server" Text="Alamat"></asp:Label>
                                                                               <asp:Label ID="Labelq267" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                           <td style="width: 222px">
                                                                               <asp:TextBox ID="TxtNonSwiftInIdenPenerimaNas_Ind_alamatIdentitas" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="100"
                                                                                   TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                           <td>
                                                                           </td>
                                                                           <td>
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                           <td style="width: 50%; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelq44" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                               <asp:Label ID="Labelq268" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                           <td style="width: 222px">
                                                                               <asp:TextBox ID="TxtNonSwiftInIdenPenerimaNas_Ind_kotaIdentitas" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                               <asp:HiddenField ID="hfNonSwiftInIdenPenerimaNas_Ind_kotaIdentitas" 
                                                                                   runat="server" />
                                                                           </td>
                                                                           <td>
                                                                           </td>
                                                                           <td>
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                           <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                               <asp:Label ID="Labeql83" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px; height: 18px">
                                                                               <asp:TextBox ID="TxtNonSwiftInIdenPenerimaNas_Ind_KotaLainIden" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           <td>
                                                                           </td>
                                                                           <td>
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Labeql46" runat="server" Text="Provinsi"></asp:Label>
                                                                                <asp:Label ID="Labelq269" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="height: 18px; width: 222px;">
                                                                                <asp:TextBox ID="TxtNonSwiftInIdenPenerimaNas_Ind_ProvinsiIden" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                <asp:HiddenField ID="HfNonSwiftInIdenPenerimaNas_Ind_ProvinsiIden" 
                                                                                    runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;</td>
                                                                            <td>
                                                                                &nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelq62" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px; height: 18px">
                                                                               <asp:TextBox ID="TxtNonSwiftInIdenPenerimaNas_Ind_ProvinsilainIden" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           <td>
                                                                           </td>
                                                                           <td>
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label9307" runat="server" Text="g. No Telp"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 222px; height: 18px">
                                                                                <asp:TextBox ID="TxtINonSwiftIndenPenerimaNas_Ind_noTelp" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;</td>
                                                                            <td>
                                                                                &nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labelq48" runat="server" Text="h. Jenis Dokument Identitas"></asp:Label>
                                                                                <asp:Label ID="Labelq270" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 222px">
                                                                                <asp:DropDownList ID="cbo_NonSwiftInpenerimaNas_Ind_jenisidentitas" runat="server" Enabled="False">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Labelq49" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                <asp:Label ID="LabelIq271" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="height: 18px; width: 222px;">
                                                                                <asp:TextBox ID="TxtINonSwiftIndenPenerimaNas_Ind_noIdentitas" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="30"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6; height: 15px;">
                                                                            </td>
                                                                            <td style="height: 15px; width: 222px;">
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                   </table>
                                                              </ajax:AjaxPanel>
                                                            </asp:View>
                                                            <asp:View ID="VwNonSwiftIn_PEN_Corp" runat="server">
                                                                    <table style="width: 288px; height: 31px">
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label223" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; color: #000000; width: 240px;">
                                                                            </td>
                                                                            <td style="width: 240px; color: #000000; height: 15px">
                                                                            </td>
                                                                            <td style="height: 15px; width: 2977px; color: #000000;">
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="color: #000000">
                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label225" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label></td>
                                                                            <td style="width: 2977px">
                                                                                <asp:DropDownList ID="cbo_NonSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLain" 
                                                                                    runat="server" Enabled="False">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label226" runat="server" Text="Bentuk Badan Usaha Lainnya " 
                                                                                    Width="167px"></asp:Label></td>
                                                                            <td style="width: 240px">
                                                                                <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 240px">
                                                                            </td>
                                                                            <td style="width: 2977px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label227" runat="server" Text="b. Nama Korporasi" Width="110px"></asp:Label>
                                                                                <asp:Label ID="Label149" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 240px">
                                                                                <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_Korp_namaKorp" runat="server" CssClass="searcheditbox" MaxLength="255"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 240px">
                                                                            </td>
                                                                            <td style="width: 2977px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label228" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label>
                                                                                <asp:Label ID="Label9308" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 240px">
                                                                                <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_Korp_BidangUsahaKorp" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                <asp:HiddenField ID="hfNonSwiftInPenerimaNasabah_Korp_BidangUsahaKorp" runat="server" />
                                                                                
                                                                            </td>
                                                                            <td style="width: 240px">
                                                                            </td>
                                                                            <td style="width: 2977px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label9324" runat="server" Text="Bidang Usaha Korporasi Lainnya"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 240px">
                                                                                <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                                                    Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 240px">
                                                                                &nbsp;</td>
                                                                            <td style="width: 2977px">
                                                                                &nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label229" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                            <td style="width: 240px">
                                                                            </td>
                                                                            <td style="width: 240px">
                                                                            </td>
                                                                            <td style="width: 2977px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6; ">
                                                                                <asp:Label ID="Label15" runat="server" Text="Alamat"></asp:Label>
                                                                                <asp:Label ID="Label9318" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 222px; height: 20px;">
                                                                                <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_Korp_AlamatKorp" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="100"
                                                                                    TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 35px; height: 20px;">
                                                                            </td>
                                                                            <td style="width: 47px; height: 20px;">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6; height: 64px;">
                                                                                <asp:Label ID="Label16" runat="server" Text="Kota / Kabupaten "></asp:Label>
                                                                                <asp:Label ID="Label9319" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="height: 64px; width: 222px;">
                                                                                <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_Korp_Kotakab" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                <asp:HiddenField ID="hfNonSwiftInPenerimaNasabah_Korp_kotakab" runat="server" />
                                                                            </td>
                                                                            <td style="height: 64px; width: 35px;">
                                                                                </td>
                                                                            <td style="width: 47px; height: 64px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 50%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label17" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px; height: 18px">
                                                                                <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_Korp_kotaLain" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           <td style="width: 35px; height: 18px">
                                                                           </td>
                                                                           <td style="width: 47px; height: 18px">
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labela18" runat="server" Text="Provinsi"></asp:Label>
                                                                                <asp:Label ID="Label9320" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 222px">
                                                                               <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_Korp_propinsi" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                                <asp:HiddenField ID="hfNonSwiftInPenerimaNasabah_Korp_propinsi" 
                                                                                    runat="server" />
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;</td>
                                                                            <td style="width: 47px">
                                                                                &nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 50%; background-color: #fff7e6">
                                                                               <asp:Label ID="Labels19" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                           <td style="width: 222px">
                                                                               <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_Korp_propinsilain" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           <td style="width: 35px">
                                                                           </td>
                                                                           <td style="width: 47px">
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 50%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labeld9310" runat="server" Text="e. No. Telp"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 222px">
                                                                                <asp:TextBox ID="TxtNonSwiftInPenerimaNasabah_Korp_NoTelp" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 35px">
                                                                                &nbsp;</td>
                                                                            <td style="width: 47px">
                                                                                &nbsp;</td>
                                                                        </tr>
                                                                    </table>
                                                            </asp:View>
                                                        </asp:MultiView>
                                                    </td>
                                                    
                                                </tr>
                                                                                             
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                                            </asp:View>
                                                            <asp:View ID="ViewNonSwiftInPenerimaNonNasabah" runat="server">
                                                                <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                       bgcolor="#dddddd" border="2">
                                                                    <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 7px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext" style="height: 14px">
                                                            <asp:Label ID="LabelSdcwiftIn4" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td style="height: 14px">
                                                            <a href="#" onclick="javascript:ShowHidePanel('B1NonSwiftInidentitasPENerimaonNasabah','Imgfg3','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Imgfg3" src="Images/search-bar-minimize.gif" border="0" height="12"
                                                                    width="12" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                                                    <tr id="B1NonSwiftInidentitasPENerimaonNasabah">
                                                                        <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                   border-right-style: none; border-left-style: none; border-bottom-style: none">                                             
                                                                            </table>
                                                                            <table style="width: 100%; height: 49px">
                                                                            <tr>
                                                                                <td style="width: 20%; background-color: #fff7e6">
                                                                                    <asp:Label ID="Labelas29" runat="server" Text="a. Kode Rahasia"></asp:Label>
                                                                                    </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="TxtNonSwiftInPenerimaNonNasabah_KodeRahasia" runat="server" 
                                                                                         CssClass="searcheditbox" MaxLength="50"
                                                                                         TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                </td>
                                                                                <td style="width: 5%">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 20%; background-color: #fff7e6">
                                                                                    <asp:Label ID="Labelas6" runat="server" Text="b. No. Rek"></asp:Label>
                                                                                    </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="TxtNonSwiftInPenerimaNonNasabah_NomorRek" runat="server" 
                                                                                         CssClass="searcheditbox" MaxLength="50"
                                                                                         TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                <td style="width: 5%">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 20%; background-color: #fff7e6">
                                                                                    <asp:Label ID="Labelas24" runat="server" Text="c. Nama Bank" 
                                                                                               Width="87px"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="TxtNonSwiftInPenerimaNonNasabah_NamaBank" runat="server" 
                                                                                         CssClass="searcheditbox" TabIndex="2"
                                                                                         Width="125px" MaxLength="50" Enabled="False"></asp:TextBox>
                                                                                         &nbsp;</td>
                                                                                <td style="width: 5%">
                                                                                </td>
                                                                            </tr>
                                                                                 <tr>
                                                                                    <td style="width: 20%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelas20" runat="server" Text="d. Nama Lengkap"></asp:Label>
                                                                                        <asp:Label ID="Labelas21" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                    <td style="width: 35%">
                                                                                        <asp:TextBox ID="TxtNonSwiftInPenerimaNonNasabah_nama" runat="server" CssClass="searcheditbox" MaxLength="100"
                                                                                                     TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 5%">
                                                                                    </td>
                                                                                 </tr>
                                                                                 <tr>
                                                                                    <td style="width: 20%; background-color: #fff7e6">
                                                                                        <asp:Label ID="LabelSwiftIn82" runat="server" Text="e. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 35%">
                                                                                        <asp:TextBox ID="TxtNonSwiftInPenerimaNonNasabah_TanggalLahir" runat="server" CssClass="searcheditbox" TabIndex="2"
                                                                                                     Width="122px" Enabled="False"></asp:TextBox>
                                                                                        </td>
                                                                                    <td style="width: 5%">
                                                                                    </td>
                                                                                 </tr>
                                                                                 <tr>
                                                                                    <td style="width: 20%; background-color: #fff7e6">
                                                                                        <asp:Label ID="LabelSwiftIn95" runat="server" Text="f. Alamat"></asp:Label></td>
                                                                                    <td style="width: 35%">
                                                                                        <asp:TextBox ID="TxtNonSwiftInPenerimaNonNasabah_alamatiden" runat="server" 
                                                                                                     CssClass="searcheditbox" MaxLength="100" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 5%">
                                                                                    </td>
                                                                                 </tr>
                                                                                 <tr>
                                                                                    <td style="width: 20%; background-color: #fff7e6">
                                                                                        <asp:Label ID="LabelSwiftIn84" runat="server" Text="g. No Telepon"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 35%;">
                                                                                        <asp:TextBox ID="TxtNonSwiftInPenerimaNonNasabah_NoTelepon" runat="server" 
                                                                                                     CssClass="searcheditbox" MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 5%;">
                                                                                    </td>
                                                                                 </tr>
                                                                                 <tr>
                                                        <td style="width: 20%; background-color: #fff7e6; height: 24px;">
                                                            <asp:Label ID="LabelSwiftIn100" runat="server" 
                                                                Text="h. Jenis Dokument Identitas"></asp:Label>
                                                            </td>
                                                        <td style="width: 35%; height: 24px;">
                                                            <asp:DropDownList ID="CboNonSwiftInPenerimaNonNasabah_JenisDokumen" runat="server" Enabled="False">
                                                            </asp:DropDownList></td>
                                                        <td style="width: 5%; height: 24px;">
                                                        </td>
                                                    </tr>                                                                                 
                                                                                <tr>
                                                                                    <td style="width: 20%; background-color: #fff7e6; height: 24px;">
                                                                                        <asp:Label ID="Labela37" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 35%; height: 24px;">
                                                                                        <asp:TextBox ID="TxtNonSwiftInPenerimaNonNasabah_NomorIden" runat="server" 
                                                                                            CssClass="searcheditbox" MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 5%; height: 24px;">
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:View>
                                                        </asp:MultiView>
                                                        </asp:View>                                                    
                                                </asp:MultiView>
                                                </ajax:AjaxPanel>
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" bgcolor="#dddddd" border="2" bordercolor="#ffffff" 
                                                         cellpadding="2" cellspacing="1" width="99%">
                                                         <tr>
                                                             <td align="left" background="Images/search-bar-background.gif" 
                                                                 style="height: 6px; width: 100%;" valign="middle">
                                                                 <table border="0" cellpadding="0" cellspacing="0">
                                                                     <tr>
                                                                         <td class="formtext">
                                                                             <asp:Label ID="Label9d75" runat="server" Font-Bold="True" Text="D. Transaksi"></asp:Label>
                                                                         </td>
                                                                         <td>
                                                                             <a href="#" 
                                                                                 onclick="javascript:ShowHidePanel('Etransaksi','Img11','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')" 
                                                                                 title="click to minimize or maximize">
                                                                             <img id="Img25" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" />
                                                                             </a>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                                 <asp:Label ID="Label9g276" runat="server" Font-Italic="True" 
                                                                     Text="(Diisi oleh PJK Bank  yang bertindak sebagai Penyelenggara Pengirim Asal atau sebagai Penyelenggara Penerus)"></asp:Label>
                                                             </td>
                                                         </tr>
                                                         <tr ID="Etransaksi0">
                                                             <td bgcolor="#ffffff" style="height: 125px; width: 100%;" valign="top">
                                                                 <table border="0" cellpadding="2" cellspacing="1" style="border-top-style: none;
                                                                        border-right-style: none; border-left-style: none; border-bottom-style: none" width="100%">
                                                                 </table>
                                                                 <table style="width: 100%; height: 49px">
                                                                     <tr>
                                                                         <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                                             <asp:Label ID="Label92g77" runat="server" 
                                                                                 Text="a. Tanggal Transaksi (tgl/bln/thn) "></asp:Label>
                                                                             <asp:Label ID="Label9g278" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                         </td>
                                                                         <td>
                                                                             <asp:TextBox ID="TxtTransaksi_NonSwiftIntanggal0" runat="server" CssClass="searcheditbox" 
                                                                                 TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>&nbsp;
                                                                         </td>
                                                                     </tr>                                                                     
                                                                     <tr>
                                                                         <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                             <asp:Label ID="Label9d287" runat="server" 
                                                                                 Text="b. Kantor Cabang Penyelenggara Pengirim Asal"></asp:Label>
                                                                         </td>
                                                                         <td>
                                                                             <asp:TextBox ID="TxtTransaksi_NonSwiftInkantorCabangPengirim" runat="server" 
                                                                                 CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                         </td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                             <asp:Label ID="Label92gf90" runat="server" 
                                                                                 Text=" c.  Value Date/Currency/Interbank Settled Amount"></asp:Label>
                                                                         </td>
                                                                         <td>
                                                                         </td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                             <asp:Label ID="Label92df91" runat="server" Text="Tanggal Transaksi (Value Date)"></asp:Label>
                                                                             <asp:Label ID="Label9ds292" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                         </td>
                                                                         <td>
                                                                             <asp:TextBox ID="TxtTransaksi_NonSwiftInValueTanggalTransaksi" runat="server" 
                                                                                          CssClass="searcheditbox" TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>&nbsp;
                                                                         </td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                                             <asp:Label ID="Label92g93" runat="server" 
                                                                                 Text="Nilai Transaksi (Amount of The EFT)"></asp:Label>
                                                                             <asp:Label ID="Label23" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                         </td>
                                                                         <td>
                                                                             <asp:TextBox ID="Transaksi_NonSwiftInnilaitransaksi0" runat="server" 
                                                                                 CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                         </td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                                             <asp:Label ID="Label9s294" runat="server" 
                                                                                 Text="Mata Uang Transaksi (currency of the EFT)"></asp:Label>
                                                                             <asp:Label ID="Label92d95" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                         </td>
                                                                         <td>
                                                                             <asp:TextBox ID="TxtTransaksi_NonSwiftInMataUangTransaksi" runat="server" 
                                                                                 CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                             <asp:HiddenField ID="hfTransaksi_NonSwiftInMataUangTransaksi" runat="server" />
                                                                         </td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                                             <asp:Label ID="Label6" runat="server" Text="Mata Uang Lainnya"></asp:Label></td>
                                                                         <td>
                                                                             <asp:TextBox ID="TxtTransaksi_NonSwiftInMataUangTransaksiLainnya" runat="server" CssClass="searcheditbox"
                                                                                 MaxLength="30" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                             <asp:Label ID="Label9296" runat="server" Text="Nilai Transaksi Keuangan yang diterima dalam rupiah"></asp:Label>
                                                                             <asp:Label ID="Label92d96" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                         </td>
                                                                         <td>
                                                                             <asp:TextBox ID="TxtTransaksi_NonSwiftInAmountdalamRupiah" runat="server" 
                                                                                 CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                         </td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                             <asp:Label ID="Label92s97" runat="server" Text="d. Currency/Instructed Amount"></asp:Label>
                                                                         </td>
                                                                         <td>
                                                                         </td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                             <asp:Label ID="Label9298" runat="server" Text="Mata Uang yang diinstruksikan"></asp:Label></td>
                                                                         <td>
                                                                             <asp:TextBox ID="TxtTransaksi_NonSwiftIncurrency" runat="server" CssClass="searcheditbox" 
                                                                                 MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>&nbsp;
                                                                             <asp:HiddenField ID="hfTransaksi_NonSwiftIncurrency" runat="server" />
                                                                         </td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td style="width: 38%; height: 18px; background-color: #fff7e6">
                                                                             <asp:Label ID="Label5" runat="server" Text="Mata Uang Lainnya"></asp:Label></td>
                                                                         <td>
                                                                             <asp:TextBox ID="TxtTransaksi_NonSwiftIncurrencyLainnya" runat="server" CssClass="searcheditbox" MaxLength="30"
                                                                                 TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                             <asp:Label ID="Label9df299" runat="server" Text="Nilai Transaksi Keuangan yang diinstruksikan"></asp:Label></td>
                                                                         <td>
                                                                             <asp:TextBox ID="TxtTransaksi_NonSwiftIninstructedAmount" runat="server" 
                                                                                 CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                         </td>
                                                                     </tr>                                                                     
                                                                     <tr>
                                                                         <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                             <asp:Label ID="Label93dd04" runat="server" Text="e. Tujuan Transaksi "></asp:Label>
                                                                         </td>
                                                                         <td>
                                                                             <asp:TextBox ID="TxtTransaksi_NonSwiftInTujuanTransaksi" runat="server" 
                                                                                 CssClass="searcheditbox" MaxLength="100" TabIndex="2" Width="297px" Enabled="False"></asp:TextBox>
                                                                         </td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td style="width: 38%; background-color: #fff7e6;">
                                                                             <asp:Label ID="Label9305" runat="server" Text="f. Sumber Penggunaan Dana "></asp:Label>
                                                                         </td>
                                                                         <td>
                                                                             <asp:TextBox ID="TxtTransaksi_NonSwiftInSumberPenggunaanDana" runat="server" 
                                                                                 CssClass="searcheditbox" MaxLength="100" TabIndex="2" Width="296px" Enabled="False"></asp:TextBox>
                                                                         </td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td style="width: 38%; background-color: #fff7e6; height: 18px;">
                                                                         </td>
                                                                         <td>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                         </tr>
                                                     </table>
                                        </asp:View>
                                          <asp:View ID="vwMessage" runat="server">
                                            <table width="100%" >
                                                <tr>
                                                    <td class="formtext" align="center" style="height: 15px">
                                                        <asp:Label runat="server" ID="lblMsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="formtext" align="center">
                                                        <asp:ImageButton ID="imgOKMsg" runat="server"  ImageUrl="~/Images/button/Ok.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </asp:MultiView>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                         <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator><ajax:AjaxPanel ID="AjaxPanel2" runat="server"><table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" background="Images/button-bground.gif" valign="middle">
                                <img height="1" src="Images/blank.gif" width="5" /></td>
                            <td align="left" background="Images/button-bground.gif" valign="middle">
                                <img height="15" src="images/arrow.gif" width="15" />
                            </td>
                            <td background="Images/button-bground.gif" style="width: 5px">
                                </td>
                            
                            <td background="Images/button-bground.gif" style="width: 62px">
                                <asp:ImageButton ID="ImageButtonCancel" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/back.gif"
                                     />
                            </td>
                            <td background="Images/button-bground.gif" width="99%">
                                <img height="1" src="Images/blank.gif" width="1" /></td>
                            <td>
                                </td>
                        </tr>
                   
                        </ajax:AjaxPanel>
                    </table>
                </div>
            </td>
        </tr>
    </table>

</asp:Content>
