Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class VerificationListEdit
    Inherits Parent

#Region " Insert Verification List dan Audit trail"
    Private Sub InsertVerificationListBySU()
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            Dim VerificationListId As Int64 = Me.LabelVerificationListID.Text

            Dim objTableAliases As Data.DataTable = CType(Session("AliasesDatatable_Edit"), DataTable)
            If objTableAliases.Rows.Count = 0 Then
                Throw New Exception("You need to enter at least one Name/Alias for the Verification List.")
            End If

            Dim objTableAddresses As Data.DataTable = CType(Session("AddressesDatatable_Edit"), DataTable)
            Dim objTableIDNos As Data.DataTable = CType(Session("IDNosDatatable_Edit"), DataTable)

            Dim objTableCustomRemarksOld As Data.DataTable = CType(Session("CustomRemarksDatatable_Edit_Old"), DataTable)
            Dim objTableCustomRemarks As Data.DataTable = CType(Session("CustomRemarksDatatable_Edit"), DataTable)

            Dim DisplayName As String = objTableAliases.Rows(0)("Aliases").ToString

            Dim DateUpdated As DateTime = DateTime.Now

            Dim DateOfData As Nullable(Of DateTime)
            If Me.TextDateOfData.Text.Trim.Length <> 0 Then
                DateOfData = CDate(Me.TextDateOfData.Text)
            End If

            Dim DateEntered As Nullable(Of DateTime)
            If Session("DateEntered_Old") <> "01-Jan-1900" Then
                DateEntered = CDate(Session("DateEntered_Old"))
            End If

            Dim DateOfBirth As Nullable(Of DateTime)
            If Me.TextCustomerDOB.Text.Trim.Length <> 0 Then
                DateOfBirth = CDate(Me.TextCustomerDOB.Text)
            End If

            Dim ListType As String = Me.DropDownListType.SelectedValue
            Dim CategoryID As Int32 = Me.DropDownCategoryID.SelectedValue
            Dim RefId As String = Session("RefId_Old")

            Dim CustomerRemarks As String() = New String(4) {"", "", "", "", ""}
            Dim i As Integer = 0

            For Each row As Data.DataRow In objTableCustomRemarks.Rows
                CustomerRemarks(i) = CType(row("CustomRemarks"), String)
                i += 1
            Next

            'Using TransScope As New Transactions.TransactionScope
            Using AccessVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_MasterTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessVerificationList, Data.IsolationLevel.ReadUncommitted)
                AccessVerificationList.UpdateVerificationList(VerificationListId, DateEntered, DateUpdated, DateOfData, DisplayName, DateOfBirth, ListType, CategoryID, CustomerRemarks(0), CustomerRemarks(1), CustomerRemarks(2), CustomerRemarks(3), CustomerRemarks(4), RefId, TxtBirthPlace.Text.Trim, TxtNationality.Text.Trim)
            End Using

            Using AccessVerificationListAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AliasTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAlias, oSQLTrans)
                Dim objTable As Data.DataTable = AccessVerificationListAlias.GetVerificationListAliasByVerificationListID(VerificationListId)
                If objTable.Rows.Count > 0 Then
                    AccessVerificationListAlias.DeleteVerificationList_AliasByVerificationListID(VerificationListId)
                End If

                For Each row As Data.DataRow In objTableAliases.Rows
                    AccessVerificationListAlias.Insert(VerificationListId, row("Aliases"))
                Next
            End Using

            Using AccessVerificationListAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AddressTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAddress, oSQLTrans)
                Dim objTable As Data.DataTable = AccessVerificationListAddress.GetVerificationListAddressByVerificationListID(VerificationListId)
                If objTable.Rows.Count > 0 Then
                    AccessVerificationListAddress.DeleteVerificationList_AddressByVerificationListID(VerificationListId)
                End If

                For Each row As Data.DataRow In objTableAddresses.Rows
                    AccessVerificationListAddress.Insert(VerificationListId, row("Addresses"), CInt(row("AddressType")), CBool(row("IsLocalAddress")))
                Next
            End Using

            Using AccessVerificationListIDNumber As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumberTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListIDNumber, oSQLTrans)
                Dim objTable As Data.DataTable = AccessVerificationListIDNumber.GetVerificationIDNumberByVerificationListID(VerificationListId)

                If objTable.Rows.Count > 0 Then
                    AccessVerificationListIDNumber.DeleteVerificationList_IDNumberByVerificationListID(VerificationListId)
                End If

                For Each row As Data.DataRow In objTableIDNos.Rows
                    AccessVerificationListIDNumber.Insert(VerificationListId, row("IDNos"))
                Next
            End Using

            oSQLTrans.Commit()
            'End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    Private Sub InsertAuditTrail()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Dim objTableAliasesOld As Data.DataTable = CType(Session("AliasesDatatable_Edit_Old"), DataTable)
            Dim objTableAliases As Data.DataTable = CType(Session("AliasesDatatable_Edit"), DataTable)
            If objTableAliases.Rows.Count = 0 Then
                Throw New Exception("You need to enter at least one Name/Alias for the Verification List.")
            End If

            Dim objTableAddressesOld As Data.DataTable = CType(Session("AddressesDatatable_Edit_Old"), DataTable)
            Dim objTableAddresses As Data.DataTable = CType(Session("AddressesDatatable_Edit"), DataTable)

            Dim objTableIDNosOld As Data.DataTable = CType(Session("IDNosDatatable_Edit_Old"), DataTable)
            Dim objTableIDNos As Data.DataTable = CType(Session("IDNosDatatable_Edit"), DataTable)

            Dim objTableCustomRemarksOld As Data.DataTable = CType(Session("CustomRemarksDatatable_Edit_Old"), DataTable)
            Dim objTableCustomRemarks As Data.DataTable = CType(Session("CustomRemarksDatatable_Edit"), DataTable)

            'Buat Variabel2 utk menampung Informasi2 baru
            Dim VerificationListId As Int64 = Me.LabelVerificationListID.Text

            Dim DateEntered As String = "N/A"
            If Session("DateEntered_Old") <> "01-Jan-1900" Then
                DateEntered = Session("DateEntered_Old")
            End If

            Dim DateUpdated As String = DateTime.Now.ToString("dd-MMMM-yyyy HH:mm")

            Dim DateOfData As String = "N/A"
            If Me.TextDateOfData.Text.Trim.Length > 0 Then
                DateOfData = Me.TextDateOfData.Text
            End If

            Dim DisplayName As String = objTableAliases.Rows(0)("Aliases").ToString

            Dim DateOfBirth As String = "N/A"
            If Me.TextCustomerDOB.Text.Trim.Length > 0 Then
                DateOfBirth = Me.TextCustomerDOB.Text
            End If

            Dim VerificationListTypeID As Int32 = Me.DropDownListType.SelectedValue
            Dim VerificationListCategoryID As Int32 = Me.DropDownCategoryID.SelectedValue

            Dim i As Integer = 0
            Dim CustomRemarks As String() = New String(4) {"", "", "", "", ""}
            For Each row As Data.DataRow In objTableCustomRemarks.Rows
                CustomRemarks(i) = CType(row("CustomRemarks"), String)
                i += 1
            Next

            Dim RefId As String = Session("RefId_Old")

            'Buat Variabel2 utk menampung Informasi2 lama
            Dim VerificationListId_Old As Int64 = Session("VerificationListId_Old")

            Dim DateEntered_Old As String = "N/A"
            If Session("DateEntered_Old") <> "01-Jan-1900" Then
                DateEntered_Old = Session("DateEntered_Old")
            End If

            Dim DateUpdated_Old As String = "N/A"
            If Session("DateUpdated_Old") <> "01-Jan-1900" Then
                DateUpdated_Old = Session("DateUpdated_Old")
            End If

            Dim DateOfData_Old As String = "N/A"
            If Session("DateOfData_Old") <> "01-Jan-1900" Then
                DateOfData_Old = Session("DateOfData_Old")
            End If

            Dim DateOfBirth_Old As String = "N/A"
            If Session("DateOfBirth_Old") <> "01-Jan-1900" Then
                DateOfBirth_Old = Session("DateOfBirth_Old")
            End If

            Dim DisplayName_Old As String = Session("DisplayName_Old")

            Dim VerificationListTypeId_Old As Int16 = Session("VerificationListTypeId_Old")

            Dim VerificationListCategoryId_Old As Int32 = Session("VerificationListCategoryId_Old")

            i = 0
            Dim CustomRemarks_Old As String() = New String(4) {"", "", "", "", ""}
            For Each row As Data.DataRow In objTableCustomRemarksOld.Rows
                CustomRemarks_Old(i) = CType(row("CustomRemarks"), String)
                i += 1
            Next

            Dim RefId_Old As String = Session("RefId_Old")

            Dim counter As Int32 = 14 + objTableAliases.Rows.Count + (3 * objTableAddresses.Rows.Count) + objTableIDNos.Rows.Count

            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(counter)
            'Using TransScope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "VerificationListId", "Edit", VerificationListId_Old, VerificationListId, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DateEntered", "Edit", DateEntered_Old, DateEntered, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DateUpdated", "Edit", DateUpdated_Old, DateUpdated, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DateOfData", "Edit", DateOfData_Old, DateOfData, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DisplayName", "Edit", DisplayName_Old, DisplayName, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DateOfBirth", "Edit", DateOfBirth_Old, DateOfBirth, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "VerificationListTypeId", "Edit", VerificationListTypeId_Old, VerificationListTypeID, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "VerificationListCategoryId", "Edit", VerificationListCategoryId_Old, VerificationListCategoryID, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "CustomRemark1", "Edit", CustomRemarks_Old(0), CustomRemarks(0), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "CustomRemark2", "Edit", CustomRemarks_Old(1), CustomRemarks(1), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "CustomRemark3", "Edit", CustomRemarks_Old(2), CustomRemarks(2), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "CustomRemark4", "Edit", CustomRemarks_Old(3), CustomRemarks(3), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "CustomRemark5", "Edit", CustomRemarks_Old(4), CustomRemarks(4), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "RefId", "Edit", RefId_Old, RefId, "Accepted")

                For Each row As Data.DataRow In objTableAliases.Rows
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List Alias", "Name", "Edit", "", row("Aliases"), "Accepted")
                Next

                For Each row As Data.DataRow In objTableAddresses.Rows
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List Address", "Address", "Edit", "", row("Addresses"), "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List Address", "AddressTypeId", "Edit", "", row("AddressType"), "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List Address", "IsLocalAddress", "Edit", "", row("IsLocalAddress"), "Accepted")
                Next

                For Each row As Data.DataRow In objTableIDNos.Rows
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List ID Number", "Name", "Edit", "", row("IDNos"), "Accepted")
                Next

                oSQLTrans.Commit()
            End Using
            'End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetListId() As String
        Get
            Return Me.Request.Params("VerificationListId")
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListView.aspx"

        Me.Response.Redirect("VerificationListView.aspx", False)
    End Sub

    Private Function CheckInApproval() As Boolean
        Using AccessApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
            Dim count As Int32 = AccessApproval.CountVerificationList_MasterApproval(Me.LabelVerificationListID.Text)

            'Jika count > 0 berarti VerificationListID tsb dlm status Pending Approval
            If count > 0 Then
                Return True
            Else 'Jika count = 0 berarti VerificationListID tsb tdk dlm status Pending Approval
                Return False
            End If
        End Using
    End Function

    Private Property NewAliases() As ArrayList
        Get
            Return IIf(Session("NewAliases") Is Nothing, New ArrayList, Session("NewAliases"))
        End Get
        Set(ByVal value As ArrayList)
            Session("NewAliases") = value
        End Set
    End Property

    Private Property OldAliases() As ArrayList
        Get
            Return IIf(Session("OldAliases") Is Nothing, New ArrayList, Session("OldAliases"))
        End Get
        Set(ByVal value As ArrayList)
            Session("OldAliases") = value
        End Set
    End Property

    Private Function CompareAliasDataTables(ByRef AliasDataTableNew As Data.DataTable, ByRef AliasDataTableOld As Data.DataTable) As Boolean
        Dim i As Integer
        Dim AliasNamesNew, AliasNamesOld As New ArrayList

        'Simpan masing2 nilai dari kolom Aliases(tabel AliasDataTable1) ke dlm AliasNames1 ArrayList
        For Each row As Data.DataRow In AliasDataTableNew.Rows
            AliasNamesNew.Add(row("Aliases"))
        Next

        Me.NewAliases = AliasNamesNew

        'Simpan masing2 nilai dari kolom Aliases(tabel AliasDataTable2) ke dlm AliasNames2 ArrayList
        For Each row As Data.DataRow In AliasDataTableOld.Rows
            AliasNamesOld.Add(row("Aliases"))
        Next

        Me.OldAliases = AliasNamesOld

        If AliasDataTableNew.Rows.Count = AliasDataTableOld.Rows.Count Then
            For i = 0 To AliasNamesNew.Count - 1
                If AliasNamesNew(i) <> AliasNamesOld(i) Then
                    Return False
                End If
            Next
        Else
            Return False
        End If

        Return True
    End Function

    Private Property NewAddress() As ArrayList
        Get
            Return IIf(Session("NewAddress") Is Nothing, New ArrayList, Session("NewAddress"))
        End Get
        Set(ByVal value As ArrayList)
            Session("NewAddress") = value
        End Set
    End Property

    Private Property OldAddress() As ArrayList
        Get
            Return IIf(Session("OldAddress") Is Nothing, New ArrayList, Session("OldAddress"))
        End Get
        Set(ByVal value As ArrayList)
            Session("OldAddress") = value
        End Set
    End Property

    Private Property NewAddressType() As ArrayList
        Get
            Return IIf(Session("NewAddressType") Is Nothing, New ArrayList, Session("NewAddressType"))
        End Get
        Set(ByVal value As ArrayList)
            Session("NewAddressType") = value
        End Set
    End Property

    Private Property OldAddressType() As ArrayList
        Get
            Return IIf(Session("OldAddressType") Is Nothing, New ArrayList, Session("OldAddressType"))
        End Get
        Set(ByVal value As ArrayList)
            Session("OldAddressType") = value
        End Set
    End Property

    Private Property NewIsLocalAddress() As ArrayList
        Get
            Return IIf(Session("NewIsLocalAddress") Is Nothing, New ArrayList, Session("NewIsLocalAddress"))
        End Get
        Set(ByVal value As ArrayList)
            Session("NewIsLocalAddress") = value
        End Set
    End Property

    Private Property OldIsLocalAddress() As ArrayList
        Get
            Return IIf(Session("OldIsLocalAddress") Is Nothing, New ArrayList, Session("OldIsLocalAddress"))
        End Get
        Set(ByVal value As ArrayList)
            Session("OldIsLocalAddress") = value
        End Set
    End Property

    Private Function CompareAddressDataTables(ByRef AddressDataTableNew As Data.DataTable, ByRef AddressDataTableOld As Data.DataTable) As Boolean
        Dim i As Integer

        Dim AddressesNew, AddressesOld, AddressTypeNew, AddressTypeOld, _
               IsLocalAddressNew, IsLocalAddressOld As New ArrayList

        'Simpan masing2 nilai dari kolom Addresses(tabel AddressDataTable1) ke dlm Addresses1 ArrayList
        For Each row As Data.DataRow In AddressDataTableNew.Rows
            AddressesNew.Add(row("Addresses"))
            AddressTypeNew.Add(row("AddressType"))
            IsLocalAddressNew.Add(row("IsLocalAddress"))
        Next

        Me.NewAddress = AddressesNew
        Me.NewAddressType = AddressTypeNew
        Me.NewIsLocalAddress = IsLocalAddressNew

        'Simpan masing2 nilai dari kolom Addresses(tabel AddressDataTable2) ke dlm Addresses2 ArrayList
        For Each row As Data.DataRow In AddressDataTableOld.Rows
            AddressesOld.Add(row("Addresses"))
            AddressTypeOld.Add(row("AddressType"))
            IsLocalAddressOld.Add(row("IsLocalAddress"))
        Next

        Me.OldAddress = AddressesOld
        Me.OldAddressType = AddressTypeOld
        Me.OldIsLocalAddress = IsLocalAddressOld

        If AddressDataTableNew.Rows.Count = AddressDataTableOld.Rows.Count Then

            For i = 0 To AddressesNew.Count - 1
                If AddressesNew(i) <> AddressesOld(i) Then
                    Return False
                End If
            Next

            '==============================================

            For i = 0 To AddressTypeNew.Count - 1
                If AddressTypeNew(i) <> AddressTypeOld(i) Then
                    Return False
                End If
            Next

            '================================================

            For i = 0 To IsLocalAddressNew.Count - 1
                If IsLocalAddressNew(i) <> IsLocalAddressOld(i) Then
                    Return False
                End If
            Next
        Else
            Return False
        End If

        Return True
    End Function

    Private Property NewIDNos() As ArrayList
        Get
            Return IIf(Session("NewIDNos") Is Nothing, New ArrayList, Session("NewIDNos"))
        End Get
        Set(ByVal value As ArrayList)
            Session("NewIDNos") = value
        End Set
    End Property

    Private Property OldIDNos() As ArrayList
        Get
            Return IIf(Session("OldIDNos") Is Nothing, New ArrayList, Session("OldIDNos"))
        End Get
        Set(ByVal value As ArrayList)
            Session("OldIDNos") = value
        End Set
    End Property

    Private Function CompareIDNumberDataTables(ByRef IDNumberDataTableNew As Data.DataTable, ByRef IDNumberDataTableOld As Data.DataTable) As Boolean
        Dim i As Integer

        Dim IDNosNew, IDNosOld As New ArrayList

        'Simpan masing2 nilai dari kolom IDNos(IDNumberDataTable1) ke dlm IDNos1 ArrayList
        For Each row As Data.DataRow In IDNumberDataTableNew.Rows
            IDNosNew.Add(row("IDNos"))
        Next

        Me.NewIDNos = IDNosNew

        'Simpan masing2 nilai dari kolom IDNos(tabel IDNumberDataTable2) ke dlm IDNos2 ArrayList
        For Each row As Data.DataRow In IDNumberDataTableOld.Rows
            IDNosOld.Add(row("IDNos"))
        Next

        Me.OldIDNos = IDNosOld

        If IDNumberDataTableNew.Rows.Count = IDNumberDataTableOld.Rows.Count Then
            For i = 0 To IDNosNew.Count - 1
                If IDNosNew(i) <> IDNosOld(i) Then
                    Return False
                End If
            Next
        Else
            Return False
        End If

        Return True
    End Function

    Private Property NewCustomRemarks() As ArrayList
        Get
            Return IIf(Session("NewCustomRemarks") Is Nothing, New ArrayList, Session("NewCustomRemarks"))
        End Get
        Set(ByVal value As ArrayList)
            Session("NewCustomRemarks") = value
        End Set
    End Property

    Private Property OldCustomRemarks() As ArrayList
        Get
            Return IIf(Session("OldCustomRemarks") Is Nothing, New ArrayList, Session("OldCustomRemarks"))
        End Get
        Set(ByVal value As ArrayList)
            Session("OldCustomRemarks") = value
        End Set
    End Property

    Private Function CompareCustomRemarksDatatable_Edits(ByRef CustomRemarksDatatable_EditNew As Data.DataTable, ByRef CustomRemarksDatatable_EditOld As Data.DataTable) As Boolean
        Dim i As Integer

        If CustomRemarksDatatable_EditNew.Rows.Count = CustomRemarksDatatable_EditOld.Rows.Count Then
            Dim CustomRemarksNew, CustomRemarksOld As New ArrayList

            'Simpan masing2 nilai dari kolom CustomRemarks(CustomRemarksDatatable_Edit1) ke dlm CustomRemarks1 ArrayList
            For Each row As Data.DataRow In CustomRemarksDatatable_EditNew.Rows
                CustomRemarksNew.Add(row("CustomRemarks"))
            Next

            'Simpan masing2 nilai dari kolom CustomRemarks(tabel CustomRemarksDatatable_Edit2) ke dlm CustomRemarks2 ArrayList
            For Each row As Data.DataRow In CustomRemarksDatatable_EditOld.Rows
                CustomRemarksOld.Add(row("CustomRemarks"))
            Next

            For i = 0 To CustomRemarksNew.Count - 1
                If CustomRemarksNew(i) <> CustomRemarksOld(i) Then
                    Return False
                End If
            Next
        Else
            Return False
        End If

        Return True
    End Function
#End Region

    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            Dim objTableAliasesOld As Data.DataTable = CType(Session("AliasesDatatable_Edit_Old"), DataTable)
            Dim objTableAliases As Data.DataTable = CType(Session("AliasesDatatable_Edit"), DataTable)
            If objTableAliases.Rows.Count = 0 Then
                Throw New Exception("You need to enter at least one Name/Alias for the Verification List.")
            End If

            Dim objTableAddressesOld As Data.DataTable = CType(Session("AddressesDatatable_Edit_Old"), DataTable)
            Dim objTableAddresses As Data.DataTable = CType(Session("AddressesDatatable_Edit"), DataTable)

            Dim objTableIDNosOld As Data.DataTable = CType(Session("IDNosDatatable_Edit_Old"), DataTable)
            Dim objTableIDNos As Data.DataTable = CType(Session("IDNosDatatable_Edit"), DataTable)

            Dim objTableCustomRemarksOld As Data.DataTable = CType(Session("CustomRemarksDatatable_Edit_Old"), DataTable)
            Dim objTableCustomRemarks As Data.DataTable = CType(Session("CustomRemarksDatatable_Edit"), DataTable)

            Dim AliasesNotChanged As Boolean = Me.CompareAliasDataTables(objTableAliases, objTableAliasesOld)
            Dim AddressesNotChanged As Boolean = Me.CompareAddressDataTables(objTableAddresses, objTableAddressesOld)
            Dim IDNosNotChanged As Boolean = Me.CompareIDNumberDataTables(objTableIDNos, objTableIDNosOld)
            Dim CustomRemarksNotChanged As Boolean = Me.CompareCustomRemarksDatatable_Edits(objTableCustomRemarks, objTableCustomRemarksOld)

            'Periksa apakah User melakukan perubahan atau tdk
            If (Me.TextDateOfData.Text = Session("DateOfData_Old")) AndAlso _
            (Me.TextCustomerDOB.Text = Session("DateOfBirth_Old")) AndAlso _
            (Me.DropDownListType.SelectedValue = Session("VerificationListTypeId_Old")) AndAlso _
            (Me.DropDownCategoryID.SelectedValue = Session("VerificationListCategoryId_Old")) AndAlso _
            (AliasesNotChanged = True) AndAlso (AddressesNotChanged = True) AndAlso _
            (IDNosNotChanged = True) AndAlso (CustomRemarksNotChanged = True) AndAlso TxtBirthPlace.Text = Session("BirthPlace_Old") AndAlso TxtNationality.Text = Session("nationality_Old") Then



                'do nothing
            Else 'Berarti User melakukan suatu perubahan
                'Jika Me.CheckInApproval = False berarti VerificationListID tsb tdk dlm status PendingApproval dan boleh dihapus
                If Me.CheckInApproval = False Then
                    Using AccessVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_MasterTableAdapter
                        'Periksa apakah VerificationList tsb msh ada dlm tabel VerificationList_Master
                        Using TableList As AMLDAL.AMLDataSet.VerificationList_MasterDataTable = AccessVerificationList.GetVerificationListMasterDataByListID(Me.GetListId)

                            'Jika TableList.rows.count = 0 berarti VerificationList tsb sdh tdk ada dlm tabel VerificationList_Master
                            If TableList.Rows.Count = 0 Then
                                Throw New Exception("Cannot edit the Verification List because it is no longer in the database.")
                            Else 'Jika TableList.rows.count != 0 berarti VerificationList tsb msh ada dlm tabel VerificationList_Master dan boleh diedit
                                'Jika User yg melakukan perubahan adalah SuperUser
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertAuditTrail()
                                    Me.InsertVerificationListBySU()
                                    Me.LblSuccess.Text = "The Verification List has been edited."
                                    Me.LblSuccess.Visible = True
                                Else 'Jika User yg melakukan perubahan bukan SuperUser
                                    Dim i As Integer = 0

                                    'Buat Variabel2 utk menampung Informasi2 baru
                                    Dim VerificationListId As Int64 = Me.LabelVerificationListID.Text

                                    Dim DateEntered As DateTime = Session("DateEntered_Old")

                                    Dim DateUpdated As DateTime = DateTime.Now

                                    'Dim DateOfData As DateTime = Session("DateOfData_Old")
                                    Dim DateOfData As Nullable(Of DateTime)
                                    If Me.TextDateOfData.Text.Trim.Length > 0 Then
                                        DateOfData = CDate(Me.TextDateOfData.Text)
                                    End If

                                    Dim DisplayName As String = objTableAliases.Rows(0)("Aliases").ToString

                                    Dim DateOfBirth As Nullable(Of DateTime)
                                    If Me.TextCustomerDOB.Text.Trim.Length > 0 Then
                                        DateOfBirth = CDate(Me.TextCustomerDOB.Text)
                                    End If

                                    Dim VerificationListTypeId As Int16 = Me.DropDownListType.SelectedValue
                                    Dim VerificationListCategoryId As Int32 = Me.DropDownCategoryID.SelectedValue
                                    Dim RefId As String = Session("RefId_Old")
                                    Dim CustomRemarks As String() = New String(4) {"", "", "", "", ""}

                                    'Buat Variabel2 utk menampung Informasi2 lama
                                    Dim VerificationListId_Old As Int64 = Session("VerificationListId_Old")

                                    Dim DateEntered_Old As Nullable(Of DateTime)
                                    If Session("DateEntered_Old") <> "01-Jan-1900" Then
                                        DateEntered_Old = CDate(Session("DateEntered_Old"))
                                    End If

                                    Dim DateUpdated_Old As Nullable(Of DateTime)
                                    If Session("DateUpdated_Old") <> "01-Jan-1900" Then
                                        DateUpdated_Old = CDate(Session("DateUpdated_Old"))
                                    End If

                                    Dim DateOfData_Old As Nullable(Of DateTime)
                                    If Session("DateOfData_Old") <> "01-Jan-1900" Then
                                        DateOfData_Old = CDate(Session("DateOfData_Old"))
                                    End If

                                    Dim DateOfBirth_Old As Nullable(Of DateTime)
                                    If Session("DateOfBirth_Old") <> "01-Jan-1900" Then
                                        DateOfBirth_Old = CDate(Session("DateOfBirth_Old"))
                                    End If

                                    Dim DisplayName_Old As String = Session("DisplayName_Old")

                                    Dim VerificationListTypeId_Old As Int16 = Session("VerificationListTypeId_Old")
                                    Dim VerificationListCategoryId_Old As Int32 = Session("VerificationListCategoryId_Old")
                                    Dim RefId_Old As String = Session("RefId_Old")
                                    Dim CustomRemarks_Old As String() = New String(4) {"", "", "", "", ""}

                                    If CustomRemarksNotChanged = True Then
                                        For Each row As Data.DataRow In objTableCustomRemarksOld.Rows
                                            CustomRemarks(i) = CType(row("CustomRemarks"), String)
                                            CustomRemarks_Old(i) = CType(row("CustomRemarks"), String)
                                            i += 1
                                        Next
                                    Else 'Berarti ada perubahan pd CustomRemarks Datatable
                                        'Catat CustomRemarks2 baru
                                        For Each row As Data.DataRow In objTableCustomRemarks.Rows
                                            CustomRemarks(i) = CType(row("CustomRemarks"), String)
                                            i += 1
                                        Next

                                        i = 0
                                        'Catat CustomRemarks2 lama
                                        Dim OldCustomRemarks_ As ArrayList = Me.OldCustomRemarks

                                        For i = 0 To OldCustomRemarks_.Count - 1
                                            CustomRemarks_Old(i) = OldCustomRemarks_(i)
                                        Next
                                    End If

                                    'Using TransScope As New Transactions.TransactionScope
                                    Using AccessPendingVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_PendingApprovalTableAdapter
                                        oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessPendingVerificationList, Data.IsolationLevel.ReadUncommitted)
                                        Using AccessApprovalVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
                                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalVerificationList, oSQLTrans)
                                            'Catat kegiatan Edit Verification List tsb ke dlm tabel Pending Approval
                                            Dim PendingApprovalID As Int64 = AccessPendingVerificationList.InsertVerificationList_Master_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Verification List Edit", 2)
                                            AccessApprovalVerificationList.Insert(PendingApprovalID, 2, VerificationListId, DateEntered, DateUpdated, DateOfData, DisplayName, DateOfBirth, VerificationListTypeId, VerificationListCategoryId, CustomRemarks(0), CustomRemarks(1), CustomRemarks(2), CustomRemarks(3), CustomRemarks(4), _
                                                                                  VerificationListId_Old, DateEntered_Old, DateUpdated_Old, DateOfData_Old, DisplayName_Old, DateOfBirth_Old, VerificationListTypeId_Old, VerificationListCategoryId_Old, CustomRemarks_Old(0), CustomRemarks_Old(1), CustomRemarks_Old(2), CustomRemarks_Old(3), CustomRemarks_Old(4), RefId, RefId_Old, TxtBirthPlace.Text.Trim, Session("BirthPlace_Old"), TxtNationality.Text.Trim, Session("nationality_Old"))


                                            Using AccessApprovalAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAlias, oSQLTrans)
                                                ' edited by johan; insert semua alias yang ada ke approval alias
                                                For Each row As Data.DataRow In objTableAliases.Rows
                                                    AccessApprovalAlias.Insert(PendingApprovalID, 2, 0, VerificationListId, row("Aliases"), Nothing, Nothing, Nothing)
                                                Next
                                            End Using

                                            ''===================================================================================                                                                                      VerificationListId_Old, DateEntered_Old, DateUpdated_Old, DateOfData_Old, DisplayName_Old, DateOfBirth_Old, VerificationListTypeId_Old, VerificationListCategoryId_Old, CustomRemarks_Old(0), CustomRemarks_Old(1), CustomRemarks_Old(2), CustomRemarks_Old(3), CustomRemarks_Old(4), RefId, RefId_Old)
                                            Using AccessApprovalAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAddress, oSQLTrans)
                                                For Each row As Data.DataRow In objTableAddresses.Rows
                                                    AccessApprovalAddress.Insert(PendingApprovalID, 2, 0, VerificationListId, row("Addresses"), CInt(row("AddressType")), CBool(row("IsLocalAddress")), Nothing, Nothing, Nothing, Nothing, True)
                                                Next
                                            End Using

                                            ''===================================================================================
                                            Using AccessApprovalIDNo As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalIDNo, oSQLTrans)
                                                For Each row As Data.DataRow In objTableIDNos.Rows
                                                    AccessApprovalIDNo.Insert(PendingApprovalID, 2, 0, VerificationListId, row("IDNos"), Nothing, Nothing, Nothing)
                                                Next
                                            End Using

                                            oSQLTrans.Commit()

                                            Dim MessagePendingID As Integer = 81002 'MessagePendingID 81002 = Verification List Edit 

                                            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & DisplayName_Old
                                            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & DisplayName_Old, False)
                                        End Using
                                    End Using
                                    'End Using
                                End If
                            End If
                        End Using
                    End Using
                Else 'Jika Me.CheckInApproval = True berarti VerificationListID tsb dlm status PendingApproval dan tidak boleh dihapus
                    Throw New Exception("Cannot edit the Verification List because it is currently waiting for approval.")
                End If
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' fill group
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroupAddressType()
        Using AccessAddressType As New AMLDAL.AMLDataSetTableAdapters.AddressTypeTableAdapter
            Me.DropDownListAddressType.DataSource = AccessAddressType.GetData
            Me.DropDownListAddressType.DataTextField = "AddressTypeDescription"
            Me.DropDownListAddressType.DataValueField = "AddressTypeId"
            Me.DropDownListAddressType.DataBind()
        End Using
    End Sub

    ''' <summary>
    ''' fill group
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroupCategory()
        Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
            Me.DropDownCategoryID.DataSource = AccessCategory.GetData
            Me.DropDownCategoryID.DataTextField = "CategoryName"
            Me.DropDownCategoryID.DataValueField = "CategoryID"
            Me.DropDownCategoryID.DataBind()
        End Using
    End Sub

    ''' <summary>
    ''' fill list type
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroupListType()
        Using AccessVerificationListType As New AMLDAL.AMLDataSetTableAdapters.VerificationListTypeTableAdapter
            Me.DropDownListType.DataSource = AccessVerificationListType.GetData
            Me.DropDownListType.DataTextField = "ListTypeName"
            Me.DropDownListType.DataValueField = "pk_Verification_List_Type_Id"
            Me.DropDownListType.DataBind()
        End Using
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_MasterTableAdapter
            Using TableList As AMLDAL.AMLDataSet.VerificationList_MasterDataTable = AccessList.GetVerificationListMasterDataByListID(Me.GetListId)
                If TableList.Rows.Count > 0 Then
                    Dim i As Integer = 0
                    Dim TableRowList As AMLDAL.AMLDataSet.VerificationList_MasterRow = TableList.Rows(0)

                    Session("VerificationListId_Old") = TableRowList.VerificationListId
                    Me.LabelVerificationListID.Text = Session("VerificationListId_Old")

                    Dim DateEntered As String = String.Format("{0:dd-MMM-yyyy}", TableRowList.DateEntered)
                    Session("DateEntered_Old") = DateEntered

                    Dim DateUpdated As String = String.Format("{0:dd-MMM-yyyy}", TableRowList.DateUpdated)
                    Session("DateUpdated_Old") = DateUpdated

                    Dim DateOfData As String = String.Format("{0:dd-MMM-yyyy}", TableRowList.DateOfData)
                    Session("DateOfData_Old") = DateOfData
                    If DateOfData = "01-Jan-1900" Then
                        Me.TextDateOfData.Text = ""
                    Else
                        Me.TextDateOfData.Text = DateOfData
                    End If

                    Dim DOB As String = String.Format("{0:dd-MMM-yyyy}", TableRowList.DateOfBirth)
                    Session("DateOfBirth_Old") = DOB
                    If DOB = "01-Jan-1900" Then
                        Me.TextCustomerDOB.Text = ""
                    Else
                        Me.TextCustomerDOB.Text = DOB
                    End If

                    Session("BirthPlace_Old") = TableRowList.BirthPlace
                    TxtBirthPlace.Text = Session("BirthPlace_Old")

                    Session("nationality_Old") = TableRowList.Nationality
                    TxtNationality.Text = TableRowList.Nationality

                    Session("DisplayName_Old") = TableRowList.DisplayName

                    Session("VerificationListTypeId_Old") = TableRowList.VerificationListTypeId
                    Me.DropDownListType.SelectedValue = Session("VerificationListTypeId_Old")

                    Session("VerificationListCategoryId_Old") = TableRowList.VerificationListCategoryId
                    Me.DropDownCategoryID.SelectedValue = Session("VerificationListCategoryId_Old")

                    Session("RefId_Old") = TableRowList.RefId

                    '==========================================

                    'Tarik semua alias utk VerificationListID tsb
                    Dim myDt As New DataTable()
                    myDt = CreateDataTableAliases()
                    i = 1
                    Dim OldAliases_ As New ArrayList
                    Using AccessAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AliasTableAdapter
                        Using AliasTable As AMLDAL.AMLDataSet.VerificationList_AliasDataTable = AccessAlias.GetVerificationListAliasByVerificationListID(Me.GetListId)
                            If AliasTable.Rows.Count > 0 Then
                                For Each AliasRow As AMLDAL.AMLDataSet.VerificationList_AliasRow In AliasTable.Rows
                                    AddAliasesToTable(i, AliasRow.Name, myDt)
                                    OldAliases_.Add(AliasRow.Name)
                                    i += 1
                                Next
                            End If
                            Me.OldAliases = OldAliases_

                        End Using
                    End Using

                    Session("AliasesDatatable_Edit_Old") = myDt

                    Dim myDtNew As New DataTable()
                    myDtNew = CreateDataTableAliases()
                    myDtNew.Merge(myDt)
                    Session("AliasesDatatable_Edit") = myDtNew
                    Me.GridViewAliases.DataSource = myDt
                    Me.GridViewAliases.DataBind()

                    If myDt.Rows.Count = 0 Then
                        Me.GridViewAliasesRow.Visible = False
                        Me.SpacerAliases.Visible = False
                    End If

                    '==========================================
                    'Tarik semua address utk VerificationListID tsb
                    Dim myDt2 As New DataTable()
                    myDt2 = CreateDataTableAddresses()
                    i = 1
                    Dim OldAddress_ As New ArrayList
                    Dim OldAddressType_ As New ArrayList
                    Dim OldIsLocalAddress_ As New ArrayList

                    Using AccessAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AddressTableAdapter
                        Using AddressTable As AMLDAL.AMLDataSet.VerificationList_AddressDataTable = AccessAddress.GetVerificationListAddressByVerificationListID(Me.GetListId)
                            For Each AddressRow As AMLDAL.AMLDataSet.VerificationList_AddressRow In AddressTable.Rows
                                'AddAliasesToTable(AliasRow.VerificationList_Alias_ID, AliasRow.Name, "(Alias " & i & ")", myDt2)
                                Dim AddressTypeLabel As String

                                Using AccessAddressType As New AMLDAL.AMLDataSetTableAdapters.AddressTypeTableAdapter
                                    AddressTypeLabel = AccessAddressType.GetAddressTypeDescriptionByAddressTypeID(AddressRow.AddressTypeId)
                                End Using

                                AddAddressesToTable(i, AddressRow.Address, AddressRow.AddressTypeId, "(" & AddressTypeLabel & ")", AddressRow.IsLocalAddress, myDt2)
                                OldAddress_.Add(AddressRow.Address)
                                OldAddressType_.Add(AddressRow.AddressTypeId)
                                OldIsLocalAddress_.Add(AddressRow.IsLocalAddress)
                                i += 1
                            Next

                            Me.OldAddress = OldAddress_
                            Me.OldAddressType = OldAddressType_
                            Me.OldIsLocalAddress = OldIsLocalAddress_
                        End Using
                    End Using
                    Session("AddressesDatatable_Edit_Old") = myDt2

                    Dim myDt2New As New DataTable()
                    myDt2New = CreateDataTableAddresses()
                    myDt2New.Merge(myDt2)
                    Session("AddressesDatatable_Edit") = myDt2New

                    Me.GridViewAddresses.DataSource = myDt2
                    Me.GridViewAddresses.DataBind()

                    If myDt2.Rows.Count = 0 Then
                        Me.GridViewAddressesRow.Visible = False
                        Me.SpacerAddresses.Visible = False
                    End If

                    '==========================================

                    'Tarik semua id number utk VerificationListID tsb
                    Dim myDt3 As New DataTable()
                    myDt3 = CreateDataTableIDNos()
                    i = 1
                    Dim OldIDNos_ As New ArrayList

                    Using AccessIDNo As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumberTableAdapter
                        Using IDNoTable As AMLDAL.AMLDataSet.VerificationList_IDNumberDataTable = AccessIDNo.GetVerificationIDNumberByVerificationListID(Me.GetListId)
                            For Each IDNoRow As AMLDAL.AMLDataSet.VerificationList_IDNumberRow In IDNoTable.Rows
                                AddIDNosToTable(i, IDNoRow.IDNumber, myDt3)
                                OldIDNos_.Add(IDNoRow.IDNumber)
                                i += 1
                            Next

                            Me.OldIDNos = OldIDNos_
                        End Using
                    End Using
                    Session("IDNosDatatable_Edit_Old") = myDt3

                    Dim myDt3New As New DataTable()
                    myDt3New = CreateDataTableIDNos()
                    myDt3New.Merge(myDt3)
                    Session("IDNosDatatable_Edit") = myDt3New

                    Me.GridViewIDNos.DataSource = myDt3
                    Me.GridViewIDNos.DataBind()

                    If myDt3.Rows.Count = 0 Then
                        Me.GridViewIDNosRow.Visible = False
                        Me.SpacerIDNos.Visible = False
                    End If

                    '============================================
                    Dim myDt4 As New DataTable()
                    myDt4 = CreateDataTableCustomRemarks()
                    Dim OldCustomRemarks_ As New ArrayList
                    i = 1

                    If TableRowList.CustomRemark1 <> "" Then
                        AddCustomRemarksToTable(i, TableRowList.CustomRemark1, myDt4)
                        OldCustomRemarks_.Add(TableRowList.CustomRemark1)
                        i += 1
                    Else
                        OldCustomRemarks_.Add("")
                    End If

                    If TableRowList.CustomRemark2 <> "" Then
                        AddCustomRemarksToTable(i, TableRowList.CustomRemark2, myDt4)
                        OldCustomRemarks_.Add(TableRowList.CustomRemark2)
                        i += 1
                    Else
                        OldCustomRemarks_.Add("")
                    End If

                    If TableRowList.CustomRemark3 <> "" Then
                        AddCustomRemarksToTable(i, TableRowList.CustomRemark3, myDt4)
                        OldCustomRemarks_.Add(TableRowList.CustomRemark3)
                        i += 1
                    Else
                        OldCustomRemarks_.Add("")
                    End If

                    If TableRowList.CustomRemark4 <> "" Then
                        AddCustomRemarksToTable(i, TableRowList.CustomRemark4, myDt4)
                        OldCustomRemarks_.Add(TableRowList.CustomRemark4)
                        i += 1
                    Else
                        OldCustomRemarks_.Add("")
                    End If

                    If TableRowList.CustomRemark5 <> "" Then
                        AddCustomRemarksToTable(i, TableRowList.CustomRemark5, myDt4)
                        OldCustomRemarks_.Add(TableRowList.CustomRemark5)
                        i += 1
                    Else
                        OldCustomRemarks_.Add("")
                    End If

                    OldCustomRemarks = OldCustomRemarks_

                    Session("CustomRemarksDatatable_Edit_Old") = myDt4

                    Dim myDt4New As New DataTable()
                    myDt4New = CreateDataTableCustomRemarks()
                    myDt4New.Merge(myDt4)
                    Session("CustomRemarksDatatable_Edit") = myDt4New

                    Me.GridViewCustomRemarks.DataSource = myDt4
                    Me.GridViewCustomRemarks.DataBind()
                    If myDt4.Rows.Count = 0 Then
                        Me.GridViewCustomRemarksRow.Visible = False
                    End If
                Else
                    Throw New Exception("Cannot edit the Verification List because it is no longer in the database.")
                End If
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillGroupAddressType()
                Me.FillGroupCategory()
                Me.FillGroupListType()

                Me.FillEditData()

                'Tambahkan event handler OnClick yang akan menampilkan kalender javascript
                Me.cmdDODDatePicker.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextDateOfData.ClientID & "'), 'dd-mmm-yyyy')")
                Me.cmdDOBDatePicker.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextCustomerDOB.ClientID & "'), 'dd-mmm-yyyy')")

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '        Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Function CreateDataTableCustomRemarks() As DataTable
        Dim CustomRemarksDatatable_Edit As DataTable = New DataTable()

        Dim CustomRemarksDataColumn As DataColumn

        CustomRemarksDataColumn = New DataColumn()
        CustomRemarksDataColumn.DataType = Type.GetType("System.String")
        CustomRemarksDataColumn.ColumnName = "id"
        CustomRemarksDatatable_Edit.Columns.Add(CustomRemarksDataColumn)

        CustomRemarksDataColumn = New DataColumn()
        CustomRemarksDataColumn.DataType = Type.GetType("System.String")
        CustomRemarksDataColumn.ColumnName = "CustomRemarks"
        CustomRemarksDatatable_Edit.Columns.Add(CustomRemarksDataColumn)

        Return CustomRemarksDatatable_Edit
    End Function

    Private Sub AddCustomRemarksToTable(ByVal id As String, ByVal CustomRemarks As String, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("CustomRemarks") = CustomRemarks

        myTable.Rows.Add(row)
    End Sub

    Private Function CreateDataTableAliases() As DataTable
        Dim AliasesDatatable_Edit As DataTable = New DataTable()

        Dim AliasesDataColumn As DataColumn

        AliasesDataColumn = New DataColumn()
        AliasesDataColumn.DataType = Type.GetType("System.String")
        AliasesDataColumn.ColumnName = "id"
        AliasesDatatable_Edit.Columns.Add(AliasesDataColumn)

        AliasesDataColumn = New DataColumn()
        AliasesDataColumn.DataType = Type.GetType("System.String")
        AliasesDataColumn.ColumnName = "Aliases"
        AliasesDatatable_Edit.Columns.Add(AliasesDataColumn)

        Return AliasesDatatable_Edit
    End Function

    Private Sub AddAliasesToTable(ByVal id As String, ByVal aliases As String, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("Aliases") = aliases

        myTable.Rows.Add(row)
    End Sub

    Private Function CreateDataTableAddresses() As DataTable
        Dim AddressesDatatable_Edit As DataTable = New DataTable()

        Dim AddressesDataColumn As DataColumn

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.String")
        AddressesDataColumn.ColumnName = "id"
        AddressesDatatable_Edit.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.String")
        AddressesDataColumn.ColumnName = "Addresses"
        AddressesDatatable_Edit.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.Int16")
        AddressesDataColumn.ColumnName = "AddressType"
        AddressesDatatable_Edit.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.String")
        AddressesDataColumn.ColumnName = "AddressTypeLabel"
        AddressesDatatable_Edit.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.Boolean")
        AddressesDataColumn.ColumnName = "IsLocalAddress"
        AddressesDatatable_Edit.Columns.Add(AddressesDataColumn)

        Return AddressesDatatable_Edit
    End Function

    Private Sub AddAddressesToTable(ByVal id As String, ByVal addresses As String, ByVal addresstype As Int16, ByVal AddressTypeLabel As String, ByVal islocaladdress As Boolean, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("Addresses") = addresses
        row("AddressType") = addresstype
        row("AddressTypeLabel") = AddressTypeLabel
        row("IsLocalAddress") = islocaladdress

        myTable.Rows.Add(row)
    End Sub

    Private Function CreateDataTableIDNos() As DataTable
        Dim IDNosDatatable_Edit As DataTable = New DataTable()

        Dim IDNosDataColumn As DataColumn

        IDNosDataColumn = New DataColumn()
        IDNosDataColumn.DataType = Type.GetType("System.String")
        IDNosDataColumn.ColumnName = "id"
        IDNosDatatable_Edit.Columns.Add(IDNosDataColumn)

        IDNosDataColumn = New DataColumn()
        IDNosDataColumn.DataType = Type.GetType("System.String")
        IDNosDataColumn.ColumnName = "IDNos"
        IDNosDatatable_Edit.Columns.Add(IDNosDataColumn)

        Return IDNosDatatable_Edit
    End Function

    Private Sub AddIDNosToTable(ByVal id As String, ByVal idnos As String, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("IDNos") = idnos

        myTable.Rows.Add(row)
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid delete event handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    '''     [Hendry] 15/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridViewAliases_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewAliases.DeleteCommand
        Dim id As String = e.Item.Cells(0).Text

        Try
            Dim objTable As Data.DataTable = CType(Session("AliasesDatatable_Edit"), DataTable)

            'hapus row tsb
            objTable.Rows.RemoveAt(e.Item.ItemIndex)
            Dim i As Integer = 1

            For Each Row As Data.DataRow In objTable.Rows
                Row("id") = i
                i += 1
            Next

            Session("AliasesDatatable_Edit") = objTable
            Me.GridViewAliases.DataSource = objTable

            Me.GridViewAliases.DataBind()

            If objTable.Rows.Count = 0 Then
                Me.GridViewAliasesRow.Visible = False
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid delete event handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    '''     [Hendry] 15/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridViewAddAddress_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewAddresses.DeleteCommand
        Dim id As String = e.Item.Cells(0).Text

        Try
            Dim objTable As Data.DataTable = CType(Session("AddressesDatatable_Edit"), DataTable)

            'hapus row tsb
            objTable.Rows.RemoveAt(e.Item.ItemIndex)
            Dim i As Integer = 1

            For Each Row As Data.DataRow In objTable.Rows
                Row("id") = i
                i += 1
            Next

            Session("AddressesDatatable_Edit") = objTable

            Me.GridViewAddresses.DataSource = objTable
            Me.GridViewAddresses.DataBind()

            If objTable.Rows.Count = 0 Then
                Me.GridViewAddressesRow.Visible = False
                Me.SpacerAddresses.Visible = False
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid delete event handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    '''     [Hendry] 15/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridViewIDNos_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewIDNos.DeleteCommand
        Dim id As String = e.Item.Cells(0).Text

        Try
            Dim objTable As Data.DataTable = CType(Session("IDNosDatatable_Edit"), DataTable)

            'hapus row tsb
            objTable.Rows.RemoveAt(e.Item.ItemIndex)
            Dim i As Integer = 1

            For Each Row As Data.DataRow In objTable.Rows
                Row("id") = i
                i += 1
            Next

            Session("IDNosDatatable_Edit") = objTable

            Me.GridViewIDNos.DataSource = objTable
            Me.GridViewIDNos.DataBind()

            If objTable.Rows.Count = 0 Then
                Me.GridViewIDNosRow.Visible = False
                Me.SpacerIDNos.Visible = False
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid delete event handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    '''     [Hendry] 15/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridViewCustomRemarks_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewCustomRemarks.DeleteCommand
        Dim id As String = e.Item.Cells(0).Text

        Try
            Dim objTable As Data.DataTable = CType(Session("CustomRemarksDatatable_Edit"), DataTable)

            'hapus row tsb
            objTable.Rows.RemoveAt(e.Item.ItemIndex)
            Dim i As Integer = 1

            For Each Row As Data.DataRow In objTable.Rows
                Row("id") = i
                i += 1
            Next

            Session("CustomRemarksDatatable_Edit") = objTable

            Me.GridViewCustomRemarks.DataSource = objTable
            Me.GridViewCustomRemarks.DataBind()

            If objTable.Rows.Count = 0 Then
                Me.GridViewCustomRemarksRow.Visible = False
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonAddAlias_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddAlias.Click
        If Me.TextBoxAlias.Text.Trim.Length <> 0 Then
            Try
                Me.GridViewAliasesRow.Visible = True
                Me.SpacerAliases.Visible = True

                'Proses semua item2 lama dlm DataGridAliases
                Dim objTable As Data.DataTable = CType(Session("AliasesDatatable_Edit"), DataTable)

                AddAliasesToTable(objTable.Rows.Count + 1, Me.TextBoxAlias.Text.Trim(), objTable)

                Session("AliasesDatatable_Edit") = objTable

                Me.GridViewAliases.DataSource = objTable

                Me.GridViewAliases.DataBind()

                Me.TextBoxAlias.Text = ""
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub

    Protected Sub ImageButtonAddAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddAddress.Click
        If Me.TextAddress.Text.Trim.Length <> 0 Then
            Try
                Me.GridViewAddressesRow.Visible = True
                Me.SpacerAddresses.Visible = True

                'Proses semua item2 lama dlm DataGridAliases
                Dim objTable As Data.DataTable = CType(Session("AddressesDatatable_Edit"), DataTable)

                Dim Address As String
                If Me.TextAddress.Text.Length <= 255 Then
                    Address = Me.TextAddress.Text
                Else
                    Address = Me.TextAddress.Text.Substring(0, 255)
                End If

                AddAddressesToTable((objTable.Rows.Count + 1), Address, Me.DropDownListAddressType.SelectedValue, "(" & Me.DropDownListAddressType.SelectedItem.Text & ")", Me.CheckBoxIsLocalAddress.Checked, objTable)

                Session("AddressesDatatable_Edit") = objTable

                Me.GridViewAddresses.DataSource = objTable

                Me.GridViewAddresses.DataBind()

                Me.TextAddress.Text = ""
                Me.DropDownListAddressType.SelectedIndex = 0
                Me.CheckBoxIsLocalAddress.Checked = True
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub

    Protected Sub ImageButtonAddIDNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddIDNumber.Click
        If Me.TextBoxIDNo.Text.Trim.Length <> 0 Then
            Try
                Me.GridViewIDNosRow.Visible = True
                Me.SpacerIDNos.Visible = True

                'Proses semua item2 lama dlm DataGridIDNos
                Dim objTable As Data.DataTable = CType(Session("IDNosDatatable_Edit"), DataTable)
                AddIDNosToTable((objTable.Rows.Count + 1), Me.TextBoxIDNo.Text.Trim(), objTable)
                Session("IDNosDatatable_Edit") = objTable

                Me.GridViewIDNos.DataSource = objTable
                Me.GridViewIDNos.DataBind()

                Me.TextBoxIDNo.Text = ""
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub

    Protected Sub ImageButtonAddCustomRemarks_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddCustomRemarks.Click
        If Me.TextboxCustomRemarks.Text.Trim.Length <> 0 Then
            Try
                Dim objTable As Data.DataTable = CType(Session("CustomRemarksDatatable_Edit"), DataTable)
                If objTable.Rows.Count < 5 Then
                    Me.GridViewCustomRemarksRow.Visible = True

                    Dim CustomRemarks As String
                    If Me.TextboxCustomRemarks.Text.Length <= 255 Then
                        CustomRemarks = Me.TextboxCustomRemarks.Text
                    Else
                        CustomRemarks = Me.TextboxCustomRemarks.Text.Substring(0, 255)
                    End If

                    'Proses semua item2 lama dlm DataGridIDNos
                    AddCustomRemarksToTable((objTable.Rows.Count + 1), CustomRemarks, objTable)

                    Session("CustomRemarksDatatable_Edit") = objTable

                    Me.GridViewCustomRemarks.DataSource = objTable
                    Me.GridViewCustomRemarks.DataBind()

                    Me.TextboxCustomRemarks.Text = ""
                Else
                    Me.TextboxCustomRemarks.Text = ""
                    Throw New Exception("Unable to add more than 5 Custom Remarks for this Verification List.")
                End If
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub
End Class