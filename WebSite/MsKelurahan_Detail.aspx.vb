#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
Imports Sahassa.AML
#End Region

Partial Class MsKelurahan_Detail
    Inherits Parent

#Region "Function"

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub



    Private Sub LoadData()
        Dim ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetPaged(MsKelurahanColumn.IDKelurahan.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsKelurahan Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsKelurahan
            SafeDefaultValue = "-"
            HFKecamatan.Value = Safe(.IDKecamatan)
            Dim OKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(.IDKecamatan.GetValueOrDefault)
            If OKecamatan IsNot Nothing Then LBSearchNamaKecamatan.Text = Safe(OKecamatan.NamaKecamatan)

            lblIDKelurahan.Text = .IDKelurahan
            lblNamaKelurahan.Text = Safe(.NamaKelurahan)

            'other info
            Dim Omsuser As TList(Of User)
            Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
            If Omsuser.Count > 0 Then
                lblCreatedBy.Text = Omsuser(0).UserName
            End If
            lblCreatedDate.Text = FormatDate(.CreatedDate)
            Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
            If Omsuser.Count > 0 Then
                lblUpdatedby.Text = Omsuser(0).UserName
            End If
            lblUpdatedDate.Text = FormatDate(.LastUpdatedDate)
            lblActivation.Text = SafeActiveInactive(.Activation)
            Dim L_objMappingMsKelurahanNCBSPPATK As TList(Of MappingMsKelurahanNCBSPPATK)
            L_objMappingMsKelurahanNCBSPPATK = DataRepository.MappingMsKelurahanNCBSPPATKProvider.GetPaged(MappingMsKelurahanNCBSPPATKColumn.IDKelurahan.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            Listmaping.AddRange(L_objMappingMsKelurahanNCBSPPATK)

        End With
    End Sub


#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsKelurahan_View.aspx")
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
                Listmaping = New TList(Of MappingMsKelurahanNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub
#End Region

#Region "Property..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsKelurahanNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKelurahanNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKelurahanNCBSPPATK In Listmaping.FindAllDistinct("PK_MappingMsKelurahanNCBSPPATK_Id")
                Temp.Add(i.IDKelurahanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property
#End Region

End Class



