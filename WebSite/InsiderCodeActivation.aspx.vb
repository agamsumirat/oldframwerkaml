﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class InsiderCodeActivation
    Inherits Parent


    ReadOnly Property GetPk_InsiderCode As Long
        Get
            If IsNumeric(Request.Params("InsiderCodeID")) Then
                If Not IsNothing(Session("InsiderCodeActivation.PK")) Then
                    Return CLng(Session("InsiderCodeActivation.PK"))
                Else
                    Session("InsiderCodeActivation.PK") = Request.Params("InsiderCodeID")
                    Return CLng(Session("InsiderCodeActivation.PK"))
                End If
            End If
            Return 0
        End Get
    End Property

    Sub clearSession()
        Session("InsiderCodeActivation.PK") = Nothing
    End Sub


    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "InsiderCodeView.aspx"

            Me.Response.Redirect("InsiderCodeView.aspx", False)
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImageDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageDelete.Click
        Try

            If Sahassa.AML.Commonly.SessionGroupName.ToLower = "superuser" Then
                If InsiderCodeBLL.Activation(GetPk_InsiderCode) Then
                    lblMessage.Text = lblActiveOrInactive.Text & " Data Success"
                    mtvInsiderCodeAdd.ActiveViewIndex = 1
                End If
            Else
                If InsiderCodeBLL.ActivationApproval(GetPk_InsiderCode) Then
                    lblMessage.Text = lblActiveOrInactive.Text & " Data has been inserted to Approval"
                    mtvInsiderCodeAdd.ActiveViewIndex = 1
                End If
            End If


        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub LoadData()
        Using objInsiderCode As InsiderCode = InsiderCodeBLL.getInsiderCodeByPk(GetPk_InsiderCode)
            If Not IsNothing(objInsiderCode) Then
                txtInsiderCodeName.Text = objInsiderCode.InsiderCodeName.ToString
                txtInsiderCodeID.Text = objInsiderCode.InsiderCodeId.ToString
                If CBool(objInsiderCode.Activation) Then
                    lblActiveOrInactive.Text = "Deactivated"
                Else
                    lblActiveOrInactive.Text = "Activated"
                End If
            Else
                ImageDelete.Visible = False
            End If
        End Using
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                mtvInsiderCodeAdd.ActiveViewIndex = 0
                clearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                LoadData()
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Response.Redirect("InsiderCodeView.aspx")
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class


