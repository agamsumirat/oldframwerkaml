Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class VerificationListManagementApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param pk user management
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamPkListManagement() As Int64
        Get
            Return Me.Request.Params("PendingApprovalID")
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserID() As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_PendingApprovalTableAdapter
                Return AccessPending.GetVerificationListMaster_PendingApprovalUserID(Me.ParamPkListManagement)
            End Using
        End Get
    End Property

    Private Function CreateDataTableCustomRemarks() As DataTable
        Dim CustomRemarksDataTable As DataTable = New DataTable()

        Dim CustomRemarksDataColumn As DataColumn

        CustomRemarksDataColumn = New DataColumn()
        CustomRemarksDataColumn.DataType = Type.GetType("System.String")
        CustomRemarksDataColumn.ColumnName = "id"
        CustomRemarksDataTable.Columns.Add(CustomRemarksDataColumn)

        CustomRemarksDataColumn = New DataColumn()
        CustomRemarksDataColumn.DataType = Type.GetType("System.String")
        CustomRemarksDataColumn.ColumnName = "CustomRemarks"
        CustomRemarksDataTable.Columns.Add(CustomRemarksDataColumn)

        Return CustomRemarksDataTable
    End Function

    Private Sub AddCustomRemarksToTable(ByVal id As String, ByVal CustomRemarks As String, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("CustomRemarks") = CustomRemarks
       
        myTable.Rows.Add(row)
    End Sub

    Private Function CreateDataTableAliases() As DataTable
        Dim AliasesDataTable As DataTable = New DataTable()

        Dim AliasesDataColumn As DataColumn

        AliasesDataColumn = New DataColumn()
        AliasesDataColumn.DataType = Type.GetType("System.String")
        AliasesDataColumn.ColumnName = "id"
        AliasesDataTable.Columns.Add(AliasesDataColumn)

        AliasesDataColumn = New DataColumn()
        AliasesDataColumn.DataType = Type.GetType("System.String")
        AliasesDataColumn.ColumnName = "Aliases"
        AliasesDataTable.Columns.Add(AliasesDataColumn)

        Return AliasesDataTable
    End Function

    Private Sub AddAliasesToTable(ByVal id As String, ByVal aliases As String, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("Aliases") = aliases
     
        myTable.Rows.Add(row)
    End Sub

    Private Function CreateDataTableAddresses() As DataTable
        Dim AddressesDataTable As DataTable = New DataTable()

        Dim AddressesDataColumn As DataColumn

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.String")
        AddressesDataColumn.ColumnName = "id"
        AddressesDataTable.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.String")
        AddressesDataColumn.ColumnName = "Addresses"
        AddressesDataTable.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.Int16")
        AddressesDataColumn.ColumnName = "AddressType"
        AddressesDataTable.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.String")
        AddressesDataColumn.ColumnName = "AddressTypeLabel"
        AddressesDataTable.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.Boolean")
        AddressesDataColumn.ColumnName = "IsLocalAddress"
        AddressesDataTable.Columns.Add(AddressesDataColumn)

        Return AddressesDataTable
    End Function

    Private Sub AddAddressesToTable(ByVal id As String, ByVal addresses As String, ByVal addresstype As Int16, ByVal AddressTypeLabel As String, ByVal islocaladdress As Boolean, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("Addresses") = addresses
        row("AddressType") = addresstype
        row("AddressTypeLabel") = AddressTypeLabel
        row("IsLocalAddress") = islocaladdress
  
        myTable.Rows.Add(row)

    End Sub

    Private Function CreateDataTableIDNos() As DataTable
        Dim IDNosDataTable As DataTable = New DataTable()

        Dim IDNosDataColumn As DataColumn

        IDNosDataColumn = New DataColumn()
        IDNosDataColumn.DataType = Type.GetType("System.String")
        IDNosDataColumn.ColumnName = "id"
        IDNosDataTable.Columns.Add(IDNosDataColumn)

        IDNosDataColumn = New DataColumn()
        IDNosDataColumn.DataType = Type.GetType("System.String")
        IDNosDataColumn.ColumnName = "IDNos"
        IDNosDataTable.Columns.Add(IDNosDataColumn)

        Return IDNosDataTable
    End Function

    Private Sub AddIDNosToTable(ByVal id As String, ByVal idnos As String, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("IDNos") = idnos

        myTable.Rows.Add(row)
    End Sub

    Private Sub FillAliasAddressIDNoGridViews(ByRef pk As Int64, ByRef rowdata As AMLDAL.AMLDataSet.VerificationList_Master_ApprovalRow)
        Dim i As Integer = 1
        Dim VerificationListId As Int64 = rowdata.VerificationListId
        Dim PendingApprovalID As Int64 = rowdata.PendingApprovalID

        Dim myDt, myDtEditOld, myDtEditNew As New DataTable()
        myDt = CreateDataTableAliases()
        myDtEditOld = CreateDataTableAliases()
        myDtEditNew = CreateDataTableAliases()

        Dim myDt2, myDt2EditOld, myDt2EditNew As New DataTable()
        myDt2 = CreateDataTableAddresses()
        myDt2EditOld = CreateDataTableAddresses()
        myDt2EditNew = CreateDataTableAddresses()

        Dim myDt3, myDt3EditOld, myDt3EditNew As New DataTable()
        myDt3 = CreateDataTableIDNos()
        myDt3EditOld = CreateDataTableIDNos()
        myDt3EditNew = CreateDataTableIDNos()

        Dim myDt4, myDt4EditOld, myDt4EditNew As New DataTable()
        myDt4 = CreateDataTableCustomRemarks()
        myDt4EditOld = CreateDataTableCustomRemarks()
        myDt4EditNew = CreateDataTableCustomRemarks()
        '==========================================
        'Tarik Informasi Alias 
        Select Case ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListEdit
                'Ambil informasi Alias2 lama untuk VerificationListID tsb
                Using AccessVerificationListAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AliasTableAdapter
                    Dim objTable As AMLDAL.AMLDataSet.VerificationList_AliasDataTable = AccessVerificationListAlias.GetVerificationListAliasByVerificationListID(VerificationListId)
                    'Jika objTable.Rows.Count > 0 berarti VerificationList tsb memiliki Alias2 dlm tabel VerificationList_Alias
                    If objTable.Rows.Count > 0 Then
                        For Each AliasRow As AMLDAL.AMLDataSet.VerificationList_AliasRow In objTable.Rows
                            AddAliasesToTable(i, AliasRow.Name, myDtEditOld)
                            i += 1
                        Next
                    End If
                End Using

                i = 1

                'Ambil informasi Alias2 baru untuk VerificationListID tsb
                Using AccessVerificationListAliasApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                    Dim objtable As AMLDAL.AMLDataSet.VerificationList_Alias_ApprovalDataTable = AccessVerificationListAliasApproval.GetVerificationList_Alias_ApprovalIDByPendingApprovalID(PendingApprovalID)
                    If objtable.Rows.Count > 0 Then
                        For Each AliasRow As AMLDAL.AMLDataSet.VerificationList_Alias_ApprovalRow In objtable.Rows
                            AddAliasesToTable(i, AliasRow.Name, myDtEditNew)
                            i += 1
                        Next
                    End If
                End Using

                'Bila dlm OldAlias dan NewAlias tdk ada entry maka sembunyikan row CustomRemarks
                If myDtEditNew.Rows.Count = 0 AndAlso myDtEditOld.Rows.Count = 0 Then
                    For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
                        If ObjRow.ID = "UserEdi3" OrElse ObjRow.ID = "UserEdi4" Then
                            ObjRow.Visible = False
                        End If
                    Next
                Else
                    Me.GridViewAliasesOld.DataSource = myDtEditOld
                    Me.GridViewAliasesOld.DataBind()
                    If myDtEditOld.Rows.Count = 0 Then
                        Me.GridViewAliasesOld.Visible = False
                    End If

                    Me.GridViewAliasesNew.DataSource = myDtEditNew
                    Me.GridViewAliasesNew.DataBind()
                    If myDtEditNew.Rows.Count = 0 Then
                        Me.GridViewAliasesNew.Visible = False
                    End If
                End If
                '======================================================
            Case Else
                Using AccessAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                    Using AliasTable As AMLDAL.AMLDataSet.VerificationList_Alias_ApprovalDataTable = AccessAlias.GetVerificationList_Alias_ApprovalIDByPendingApprovalID(PendingApprovalID)
                        For Each AliasRow As AMLDAL.AMLDataSet.VerificationList_Alias_ApprovalRow In AliasTable.Rows
                            AddAliasesToTable(i, AliasRow.Name, myDt)
                            i += 1
                        Next
                    End Using
                End Using

                If myDt.Rows.Count > 0 Then
                    Select Case ParamType
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListAdd
                            Me.GridViewAliasesAdd.DataSource = myDt
                            Me.GridViewAliasesAdd.DataBind()
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListDelete
                            Me.GridViewAliasesDelete.DataSource = myDt
                            Me.GridViewAliasesDelete.DataBind()
                        Case Else
                            Throw New Exception("Unknown Type : " & ParamType)
                    End Select
                Else
                    Select Case ParamType
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListAdd
                            For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
                                If ObjRow.ID = "UserAdd2" OrElse ObjRow.ID = "UserAdd3" Then
                                    ObjRow.Visible = False
                                End If
                            Next
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListDelete
                            For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
                                If ObjRow.ID = "UserDel3" OrElse ObjRow.ID = "UserDel4" Then
                                    ObjRow.Visible = False
                                End If
                            Next
                        Case Else
                            Throw New Exception("Unknown Type : " & ParamType)
                    End Select
                End If
        End Select

        '==========================================
        'Tarik Informasi Address

        i = 1

        Select Case ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListEdit
                'Ambil informasi Address2 lama utk VerificationListID tsb
                Using AccessVerificationListAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AddressTableAdapter
                    Dim objTable As AMLDAL.AMLDataSet.VerificationList_AddressDataTable = AccessVerificationListAddress.GetVerificationListAddressByVerificationListID(VerificationListId)

                    If objTable.Rows.Count > 0 Then
                        For Each AddressRow As AMLDAL.AMLDataSet.VerificationList_AddressRow In objTable.Rows
                            Dim AddressTypeLabel As String

                            Using AccessAddressType As New AMLDAL.AMLDataSetTableAdapters.AddressTypeTableAdapter
                                AddressTypeLabel = AccessAddressType.GetAddressTypeDescriptionByAddressTypeID(AddressRow.AddressTypeId)
                            End Using

                            AddAddressesToTable(i, AddressRow.Address, AddressRow.AddressTypeId, "(" & AddressTypeLabel & ")", AddressRow.IsLocalAddress, myDt2EditOld)
                            i += 1
                        Next
                    End If
                End Using

                i = 1

                'Ambil informasi Address2 baru utk VerificationListID tsb
                Using AccessVerificationListAddressApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                    Dim objTable As AMLDAL.AMLDataSet.VerificationList_Address_ApprovalDataTable = AccessVerificationListAddressApproval.GetVerificationList_Address_ApprovalIDByPendingApprovalID(PendingApprovalID)

                    If objTable.Rows.Count > 0 Then
                        For Each AddressRow As AMLDAL.AMLDataSet.VerificationList_Address_ApprovalRow In objTable.Rows
                            Dim AddressTypeLabel As String

                            Using AccessAddressType As New AMLDAL.AMLDataSetTableAdapters.AddressTypeTableAdapter
                                AddressTypeLabel = AccessAddressType.GetAddressTypeDescriptionByAddressTypeID(AddressRow.AddressTypeId)
                            End Using

                            AddAddressesToTable(i, AddressRow.Address, AddressRow.AddressTypeId, "(" & AddressTypeLabel & ")", AddressRow.IsLocalAddress, myDt2EditNew)
                            i += 1
                        Next
                    End If
                End Using

                
                '======================================================
                'Bila dlm OldAddress dan NewAddress tdk ada entry maka sembunyikan row Address
                If myDt2EditNew.Rows.Count = 0 AndAlso myDt2EditOld.Rows.Count = 0 Then
                    For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
                        If ObjRow.ID = "UserEdi6" OrElse ObjRow.ID = "UserEdi7" Then
                            ObjRow.Visible = False
                        End If
                    Next
                Else
                    Me.GridViewAddressesOld.DataSource = myDt2EditOld
                    Me.GridViewAddressesOld.DataBind()
                    If myDt2EditOld.Rows.Count = 0 Then
                        Me.GridViewAddressesOld.Visible = False
                    End If

                    Me.GridViewAddressesNew.DataSource = myDt2EditNew
                    Me.GridViewAddressesNew.DataBind()
                    If myDt2EditNew.Rows.Count = 0 Then
                        Me.GridViewAddressesNew.Visible = False
                    End If
                End If
            Case Else
                Using AccessAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                    Using AddressTable As AMLDAL.AMLDataSet.VerificationList_Address_ApprovalDataTable = AccessAddress.GetVerificationList_Address_ApprovalIDByPendingApprovalID(PendingApprovalID)
                        For Each AddressRow As AMLDAL.AMLDataSet.VerificationList_Address_ApprovalRow In AddressTable.Rows
                            Dim AddressTypeLabel As String

                            Using AccessAddressType As New AMLDAL.AMLDataSetTableAdapters.AddressTypeTableAdapter
                                AddressTypeLabel = AccessAddressType.GetAddressTypeDescriptionByAddressTypeID(AddressRow.AddressTypeId)
                            End Using

                            AddAddressesToTable(i, AddressRow.Address, AddressRow.AddressTypeId, "(" & AddressTypeLabel & ")", AddressRow.IsLocalAddress, myDt2)
                            i += 1
                        Next
                    End Using
                End Using

                If myDt2.Rows.Count > 0 Then
                    Select Case ParamType
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListAdd
                            Me.GridViewAddressesAdd.DataSource = myDt2
                            Me.GridViewAddressesAdd.DataBind()
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListDelete
                            Me.GridViewAddressesDelete.DataSource = myDt2
                            Me.GridViewAddressesDelete.DataBind()
                        Case Else
                            Throw New Exception("Unknown Type : " & ParamType)
                    End Select
                Else
                    Select Case ParamType
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListAdd
                            For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
                                If ObjRow.ID = "UserAdd5" Then
                                    ObjRow.Visible = False
                                End If
                            Next
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListDelete
                            For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
                                'If ObjRow.ID = "UserDel6" Then
                                '    ObjRow.Visible = False
                                'End If
                            Next
                        Case Else
                            Throw New Exception("Unknown Type : " & ParamType)
                    End Select
                End If
        End Select
        '==========================================
        'Tarik Informasi ID Number      
        i = 1

        'Tarik Informasi IDNumber 
        Select Case ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListEdit
                'Ambil informasi ID Number2 lama
                Using AccessVerificationListIDNumber As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumberTableAdapter
                    Dim objTable As AMLDAL.AMLDataSet.VerificationList_IDNumberDataTable = AccessVerificationListIDNumber.GetVerificationIDNumberByVerificationListID(VerificationListId)
                    If objTable.Rows.Count > 0 Then
                        For Each IDNumberRow As AMLDAL.AMLDataSet.VerificationList_IDNumberRow In objTable.Rows
                            AddIDNosToTable(i, IDNumberRow.IDNumber, myDt3EditOld)
                            i += 1
                        Next
                    End If
                End Using

                'Ambil informasi ID Number2 baru
                Using AccessVerificationListIDNumberApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                    Dim objTable As AMLDAL.AMLDataSet.VerificationList_IDNumber_ApprovalDataTable = AccessVerificationListIDNumberApproval.GetVerificationList_IDNumber_ApprovalIDByPendingApprovalID(PendingApprovalID)
                    If objTable.Rows.Count > 0 Then
                        For Each IDNumberRow As AMLDAL.AMLDataSet.VerificationList_IDNumber_ApprovalRow In objTable.Rows
                            AddIDNosToTable(i, IDNumberRow.IDNumber, myDt3EditNew)
                            i += 1
                        Next
                    End If
                End Using

                'Bila dlm OldIDNumber dan NewIDNumber tdk ada entry maka sembunyikan row IDNumber
                If myDt3EditNew.Rows.Count = 0 AndAlso myDt3EditOld.Rows.Count = 0 Then
                    For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
                        If ObjRow.ID = "UserEdi8" OrElse ObjRow.ID = "UserEdi9" Then
                            ObjRow.Visible = False
                        End If
                    Next
                Else
                    Me.GridViewIDNosOld.DataSource = myDt3EditOld
                    Me.GridViewIDNosOld.DataBind()
                    If myDt3EditOld.Rows.Count = 0 Then
                        Me.GridViewIDNosOld.Visible = False
                    End If

                    Me.GridViewIDNosNew.DataSource = myDt3EditNew
                    Me.GridViewIDNosNew.DataBind()
                    If myDt3EditNew.Rows.Count = 0 Then
                        Me.GridViewIDNosNew.Visible = False
                    End If
                End If
            Case Else
                Using AccessIDNumber As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                    Using IDNumberTable As AMLDAL.AMLDataSet.VerificationList_IDNumber_ApprovalDataTable = AccessIDNumber.GetVerificationList_IDNumber_ApprovalIDByPendingApprovalID(PendingApprovalID)
                        For Each IDNumberRow As AMLDAL.AMLDataSet.VerificationList_IDNumber_ApprovalRow In IDNumberTable.Rows
                            AddIDNosToTable(i, IDNumberRow.IDNumber, myDt3)
                            i += 1
                        Next
                    End Using
                End Using

                If myDt3.Rows.Count > 0 Then
                    Select Case ParamType
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListAdd
                            Me.GridViewIDNosAdd.DataSource = myDt3
                            Me.GridViewIDNosAdd.DataBind()
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListDelete
                            Me.GridViewIDNosDelete.DataSource = myDt3
                            Me.GridViewIDNosDelete.DataBind()
                        Case Else
                            Throw New Exception("Unknown Type : " & ParamType)
                    End Select
                Else
                    Select Case ParamType
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListAdd
                            For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
                                If ObjRow.ID = "UserAdd6" OrElse ObjRow.ID = "UserAdd7" Then
                                    ObjRow.Visible = False
                                End If
                            Next
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListDelete
                            For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
                                If ObjRow.ID = "UserDel7" OrElse ObjRow.ID = "UserDel8" Then
                                    ObjRow.Visible = False
                                End If
                            Next
                        Case Else
                            Throw New Exception("Unknown Type : " & ParamType)
                    End Select
                End If
        End Select

        '==========================================
        'Tarik Informasi Custom Remarks     
        i = 1

        Select Case ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListEdit
                '================================
                'New CustomRemarks
                If rowdata.CustomRemark1 <> "" Then
                    AddCustomRemarksToTable(i, rowdata.CustomRemark1, myDt4EditNew)
                    i += 1
                End If

                If rowdata.CustomRemark2 <> "" Then
                    AddCustomRemarksToTable(i, rowdata.CustomRemark2, myDt4EditNew)
                    i += 1
                End If

                If rowdata.CustomRemark3 <> "" Then
                    AddCustomRemarksToTable(i, rowdata.CustomRemark3, myDt4EditNew)
                    i += 1
                End If

                If rowdata.CustomRemark4 <> "" Then
                    AddCustomRemarksToTable(i, rowdata.CustomRemark4, myDt4EditNew)
                    i += 1
                End If

                If rowdata.CustomRemark5 <> "" Then
                    AddCustomRemarksToTable(i, rowdata.CustomRemark5, myDt4EditNew)
                    i += 1
                End If

                i = 1

                '================================
                'Old CustoRemarks
                If rowdata.CustomRemark1_Old <> "" Then
                    AddCustomRemarksToTable(i, rowdata.CustomRemark1, myDt4EditOld)
                    i += 1
                End If

                If rowdata.CustomRemark2_Old <> "" Then
                    AddCustomRemarksToTable(i, rowdata.CustomRemark2, myDt4EditOld)
                    i += 1
                End If

                If rowdata.CustomRemark3_Old <> "" Then
                    AddCustomRemarksToTable(i, rowdata.CustomRemark3, myDt4EditOld)
                    i += 1
                End If

                If rowdata.CustomRemark4_Old <> "" Then
                    AddCustomRemarksToTable(i, rowdata.CustomRemark4, myDt4EditOld)
                    i += 1
                End If

                If rowdata.CustomRemark5_Old <> "" Then
                    AddCustomRemarksToTable(i, rowdata.CustomRemark5, myDt4EditOld)
                    i += 1
                End If

                'Bila dlm OldCustomRemarks dan NewCustomRemarks tdk ada entry maka sembunyikan row CustomRemarks
                If myDt4EditNew.Rows.Count = 0 AndAlso myDt4EditOld.Rows.Count = 0 Then
                    For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
                        If ObjRow.ID = "UserEdi12" Then
                            ObjRow.Visible = False
                            Exit For
                        End If
                    Next
                Else
                    Me.GridViewCustomRemarksOld.DataSource = myDt4EditOld
                    Me.GridViewCustomRemarksOld.DataBind()
                    If myDt4EditOld.Rows.Count = 0 Then
                        Me.GridViewCustomRemarksOld.Visible = False
                    End If

                    Me.GridViewCustomRemarksNew.DataSource = myDt4EditNew
                    Me.GridViewCustomRemarksNew.DataBind()
                    If myDt4EditNew.Rows.Count = 0 Then
                        Me.GridViewCustomRemarksNew.Visible = False
                    End If
                End If
            Case Else
                If rowdata.CustomRemark1 <> "" Then
                    AddCustomRemarksToTable(i, rowdata.CustomRemark1, myDt4)
                    i += 1
                End If

                If rowdata.CustomRemark2 <> "" Then
                    AddCustomRemarksToTable(i, rowdata.CustomRemark2, myDt4)
                    i += 1
                End If

                If rowdata.CustomRemark3 <> "" Then
                    AddCustomRemarksToTable(i, rowdata.CustomRemark3, myDt4)
                    i += 1
                End If

                If rowdata.CustomRemark4 <> "" Then
                    AddCustomRemarksToTable(i, rowdata.CustomRemark4, myDt4)
                    i += 1
                End If

                If rowdata.CustomRemark5 <> "" Then
                    AddCustomRemarksToTable(i, rowdata.CustomRemark5, myDt4)
                    i += 1
                End If

                If myDt4.Rows.Count > 0 Then
                    Select Case ParamType
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListAdd
                            Me.GridViewCustomRemarksAdd.DataSource = myDt4
                            Me.GridViewCustomRemarksAdd.DataBind()
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListDelete
                            Me.GridViewCustomRemarksDelete.DataSource = myDt4
                            Me.GridViewCustomRemarksDelete.DataBind()
                        Case Else
                            Throw New Exception("Unknown Type : " & ParamType)
                    End Select
                Else
                    Select Case ParamType
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListAdd
                            For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
                                If ObjRow.ID = "UserAdd10" Then
                                    ObjRow.Visible = False
                                    Exit For
                                End If
                            Next
                        Case Sahassa.AML.Commonly.TypeConfirm.VerificationListDelete
                            For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
                                If ObjRow.ID = "UserDel11" Then
                                    ObjRow.Visible = False
                                    Exit For
                                End If
                            Next
                        Case Else
                            Throw New Exception("Unknown Type : " & ParamType)
                    End Select
                End If
        End Select
    End Sub

#End Region

#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    02/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HideAll()
        Dim StrId As String = ""
        Select Case Me.ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListAdd
                StrId = "UserAdd"
            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListEdit
                StrId = "UserEdi"
            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListDelete
                StrId = "UserDel"
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select
        For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
            ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID = "Button12" Or ObjRow.ID = "Button13" Or ObjRow.ID.Substring(0, 7) = StrId
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load VerificationList add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    20/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadVerificationListAdd()
        Me.LabelTitle.Text = "Activity: Add Verification List"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetVerificationListMasterApprovalDetail(Me.ParamPkListManagement)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.VerificationList_Master_ApprovalRow = ObjTable.Rows(0)
                    Dim DateOfData As String = String.Format("{0:dd-MMM-yyyy}", rowData.DateOfData)
                    If DateOfData = "01-Jan-1900" Then
                        Me.LabelDateOfData.Text = "N/A"
                    Else
                        Me.LabelDateOfData.Text = DateOfData
                    End If

                    If Not rowData.IsBirthPlaceNull Then
                        LblBirthPlace.Text = rowData.BirthPlace
                    End If


                    Dim DOB As String = String.Format("{0:dd-MMM-yyyy}", rowData.DateOfBirth)
                    If DOB = "01-Jan-1900" Then
                        Me.LabelDOBAdd.Text = "N/A"
                    Else
                        Me.LabelDOBAdd.Text = DOB
                    End If
                    If Not rowData.IsnationalityNull Then
                        Me.LabelNationalityAdd.Text = rowData.nationality
                    End If


                    Using AccessListType As New AMLDAL.AMLDataSetTableAdapters.VerificationListTypeTableAdapter
                        Me.LabelListTypeAdd.Text = AccessListType.GetListTypeNameByListTypeID(rowData.VerificationListTypeId)
                    End Using

                    Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
                        Me.LabelCategoryAdd.Text = AccessCategory.GetCategoryNameByCategoryID(rowData.VerificationListCategoryId)
                    End Using

                    Dim ApprovalID As Int64 = rowData.ApprovalID
                    Me.FillAliasAddressIDNoGridViews(ApprovalID, rowData)
                Else
                    Throw New Exception("Cannot load Verification List data because it is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load VerificationList edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    02/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadVerificationListEdit()
        Me.LabelTitle.Text = "Activity: Edit Verification List"

        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetVerificationListMasterApprovalDetail(Me.ParamPkListManagement)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.VerificationList_Master_ApprovalRow = ObjTable.Rows(0)

                    LabelNewListID.Text = rowData.VerificationListId
                    Dim NewDateOfData As String = String.Format("{0:dd-MMM-yyyy}", rowData.DateOfData)
                    If NewDateOfData = "01-Jan-1900" Then
                        Me.LabelDateOfDataNew.Text = "N/A"
                    Else
                        Me.LabelDateOfDataNew.Text = NewDateOfData
                    End If
                    If Not rowData.IsBirthPlaceNull Then
                        LblBirthPlaceNew.Text = rowData.BirthPlace
                    End If
                    If Not rowData.IsDateOfBirth_OldNull Then
                        LblBirthPlaceOld.Text = rowData.BirthPlace_Old
                    End If

                    Dim NewDOB As String = String.Format("{0:dd-MMM-yyyy}", rowData.DateOfBirth)
                    If NewDOB = "01-Jan-1900" Then
                        Me.LabelNewDOB.Text = "N/A"
                    Else
                        Me.LabelNewDOB.Text = NewDOB
                    End If

                    Using AccessNewListType As New AMLDAL.AMLDataSetTableAdapters.VerificationListTypeTableAdapter
                        Me.LabelNewListType.Text = AccessNewListType.GetListTypeNameByListTypeID(rowData.VerificationListTypeId)
                    End Using

                    Using AccessNewCategoryName As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
                        Me.LabelNewCategory.Text = AccessNewCategoryName.GetCategoryNameByCategoryID(rowData.VerificationListCategoryId)
                    End Using

                    LabelOldListID.Text = rowData.VerificationListId_Old

                    Dim OldDateOfData As String = String.Format("{0:dd-MMM-yyyy}", rowData.DateOfData_Old)
                    If OldDateOfData = "01-Jan-1900" Then
                        Me.LabelDateOfDataOld.Text = "N/A"
                    Else
                        Me.LabelDateOfDataOld.Text = OldDateOfData
                    End If

                    Dim OldDOB As String = String.Format("{0:dd-MMM-yyyy}", rowData.DateOfBirth_Old)
                    If OldDOB = "01-Jan-1900" Then
                        Me.LabelOldDOB.Text = "N/A"
                    Else
                        Me.LabelOldDOB.Text = OldDOB
                    End If

                    Using AccessOldListType As New AMLDAL.AMLDataSetTableAdapters.VerificationListTypeTableAdapter
                        Me.LabelOldListType.Text = AccessOldListType.GetListTypeNameByListTypeID(rowData.VerificationListTypeId_Old)
                    End Using

                    Using AccessOldCategoryName As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
                        Me.LabelOldCategory.Text = AccessOldCategoryName.GetCategoryNameByCategoryID(rowData.VerificationListCategoryId_Old)
                    End Using
                    If Not rowData.Isnationality_oldNull Then
                        Me.LabelOldNationality.Text = rowData.nationality_old
                    End If

                    If Not rowData.IsnationalityNull Then
                        Me.LabelNewNationality.Text = rowData.nationality
                    End If



                    Dim ApprovalID As Int64 = rowData.ApprovalID
                    Me.FillAliasAddressIDNoGridViews(ApprovalID, rowData)
                Else
                    Throw New Exception("Cannot load Verification List data because it is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load VerificationList delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadVerificationListDelete()
        Me.LabelTitle.Text = "Activity: Delete Verification List"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetVerificationListMasterApprovalDetail(Me.ParamPkListManagement)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.VerificationList_Master_ApprovalRow = ObjTable.Rows(0)

                    Me.LabelListIDDelete.Text = rowData.VerificationListId
                    Dim DateOfData As String = String.Format("{0:dd-MMM-yyyy}", rowData.DateOfData)
                    If DateOfData = "01-Jan-1900" Then
                        Me.LabelDateOfDataDeleted.Text = "N/A"
                    Else
                        Me.LabelDateOfDataDeleted.Text = DateOfData
                    End If
                    If rowData.IsBirthPlaceNull Then
                        LblBirthPlaceDelete.Text = rowData.BirthPlace
                    End If

                    Dim DOB As String = String.Format("{0:dd-MMM-yyyy}", rowData.DateOfBirth)
                    If DOB = "01-Jan-1900" Then
                        Me.LabelDOBDelete.Text = "N/A"
                    Else
                        Me.LabelDOBDelete.Text = DOB
                    End If

                    Using AccessListType As New AMLDAL.AMLDataSetTableAdapters.VerificationListTypeTableAdapter
                        Me.LabelListTypeDelete.Text = AccessListType.GetListTypeNameByListTypeID(rowData.VerificationListTypeId)
                    End Using

                    Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
                        Me.LabelCategoryDelete.Text = AccessCategory.GetCategoryNameByCategoryID(rowData.VerificationListCategoryId)
                    End Using
                    Me.LabelNationalityDel.Text = rowData.nationality

                    Dim ApprovalID As Int64 = rowData.ApprovalID
                    Me.FillAliasAddressIDNoGridViews(ApprovalID, rowData)
                Else
                    Throw New Exception("Cannot load Verification List data because it is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub
#End Region
#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' VerificationList add accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    '''     [Hendry]    20/06/2007 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptVerificationListAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_MasterTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessVerificationList, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetVerificationListMasterApprovalDetail(Me.ParamPkListManagement)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.VerificationList_Master_ApprovalRow = ObjTable.Rows(0)

                    'tambahkan item tersebut dalam tabel VerificationList_Master

                    Dim ddateob As Nullable(Of Date)

                    If rowData.DateOfBirth.ToString("dd-MM-yyyy") = "01-01-1900" Then
                        ddateob = Nothing
                    Else
                        ddateob = rowData.DateOfBirth
                    End If

                    Dim strnationality As String
                    If rowData.IsnationalityNull Then
                        strnationality = ""
                    Else
                        strnationality = rowData.nationality
                    End If
                    Dim VerificationListID As Int64 = AccessVerificationList.InsertVerificationListMaster(rowData.DateEntered, rowData.DateUpdated, rowData.DateOfData, rowData.DisplayName, ddateob, rowData.VerificationListTypeId, rowData.VerificationListCategoryId, rowData.CustomRemark1, rowData.CustomRemark2, rowData.CustomRemark3, rowData.CustomRemark4, rowData.CustomRemark5, rowData.RefId, rowData.BirthPlace, strnationality)
                    Dim PendingApprovalID As Int64 = rowData.PendingApprovalID

                    Dim TotalAliasesPending, TotalAddressesPending, TotalIDNumbersPending As Int32

                    Using AccessVerificationListAliasApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAliasApproval, oSQLTrans)
                        TotalAliasesPending = AccessVerificationListAliasApproval.CountAliasesInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Using AccessVerificationListAddressApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAddressApproval, oSQLTrans)
                        TotalAddressesPending = AccessVerificationListAddressApproval.CountAddressesInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Using AccessVerificationListIDNumberApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListIDNumberApproval, oSQLTrans)
                        TotalIDNumbersPending = AccessVerificationListIDNumberApproval.CountIDNumbersInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Dim counter As Int32 = 14 + (2 * TotalAliasesPending) + (4 * TotalAddressesPending) + (2 * TotalIDNumbersPending)

                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(counter)

                    'catat aktifitas dalam tabel Audit Trail
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListID", "Add", "", VerificationListID, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateEntered", "Add", "", rowData.DateEntered.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateUpdated", "Add", "", rowData.DateUpdated.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateOfData", "Add", "", rowData.DateOfData.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DisplayName", "Add", "", rowData.DisplayName, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateOfBirth", "Add", "", rowData.DateOfBirth.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "BirthPlace", "Add", "", rowData.BirthPlace, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListTypeId", "Add", "", rowData.VerificationListTypeId, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListCategoryId", "Add", "", rowData.VerificationListCategoryId, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark1", "Add", "", rowData.CustomRemark1, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark2", "Add", "", rowData.CustomRemark2, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark3", "Add", "", rowData.CustomRemark3, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark4", "Add", "", rowData.CustomRemark4, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark5", "Add", "", rowData.CustomRemark5, "Accepted")

                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "RefId", "Add", "", rowData.RefId, "Accepted")

                        'tambahkan Alias2 utk VerificationList baru tersebut dalam tabel VerificationList_Alias
                        Using AccessAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AliasTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAlias, oSQLTrans)
                            Using AccessApprovalAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAlias, oSQLTrans)
                                Dim AliasApprovalTable As Data.DataTable = AccessApprovalAlias.GetVerificationList_Alias_ApprovalIDByPendingApprovalID(PendingApprovalID)

                                If AliasApprovalTable.Rows.Count > 0 Then
                                    For Each AliasApprovalRow As AMLDAL.AMLDataSet.VerificationList_Alias_ApprovalRow In AliasApprovalTable.Rows
                                        AccessAlias.Insert(VerificationListID, AliasApprovalRow.Name)
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Alias", "VerificationListId", "Add", "", VerificationListID, "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Alias", "Name", "Add", "", AliasApprovalRow.Name, "Accepted")
                                    Next

                                    'hapus Alias yg sdh dimasukkan ke dlm tabel Verification_Alias tsb dr tabel Approvalnya
                                    AccessApprovalAlias.DeleteVerificationListAliasMasterApproval(PendingApprovalID)
                                End If
                            End Using
                        End Using

                        '=====================================================================
                        'tambahkan Address2 utk VerificationList baru tersebut dalam tabel VerificationList_Address
                        Using AccessAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AddressTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAddress, oSQLTrans)
                            Using AccessApprovalAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAddress, oSQLTrans)
                                Dim AddressApprovalTable As Data.DataTable = AccessApprovalAddress.GetVerificationList_Address_ApprovalIDByPendingApprovalID(PendingApprovalID)

                                If AddressApprovalTable.Rows.Count > 0 Then
                                    For Each AddressApprovalRow As AMLDAL.AMLDataSet.VerificationList_Address_ApprovalRow In AddressApprovalTable.Rows
                                        AccessAddress.Insert(VerificationListID, AddressApprovalRow.Address, AddressApprovalRow.AddressTypeId, AddressApprovalRow.IsLocalAddress)
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "VerificationListId", "Add", "", VerificationListID, "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "Address", "Add", "", AddressApprovalRow.Address, "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "AddressTypeId", "Add", "", AddressApprovalRow.AddressTypeId, "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "IsLocalAddress", "Add", "", AddressApprovalRow.IsLocalAddress, "Accepted")
                                    Next
                                End If

                                'hapus Address yg sdh dimasukkan ke dlm tabel Verification_Address tsb dr tabel Approvalnya
                                AccessApprovalAddress.DeleteVerificationListAddressMasterApproval(PendingApprovalID)
                            End Using
                        End Using

                        '======================================================================
                        'tambahkan IDNo2 utk VerificationList baru tersebut dalam tabel VerificationList_IDNumber
                        Using AccessIDNo As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumberTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessIDNo, oSQLTrans)
                            Using AccessApprovalIDNo As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalIDNo, oSQLTrans)
                                Dim IDNoApprovalTable As Data.DataTable = AccessApprovalIDNo.GetVerificationList_IDNumber_ApprovalIDByPendingApprovalID(PendingApprovalID)

                                If IDNoApprovalTable.Rows.Count > 0 Then
                                    For Each IDNoApprovalRow As AMLDAL.AMLDataSet.VerificationList_IDNumber_ApprovalRow In IDNoApprovalTable.Rows
                                        AccessIDNo.Insert(VerificationListID, IDNoApprovalRow.IDNumber)
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_IDNumber", "VerificationListId", "Add", "", VerificationListID, "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_IDNumber", "IDNumber", "Add", "", IDNoApprovalRow.IDNumber, "Accepted")
                                    Next

                                    'hapus IDNo yg sdh dimasukkan ke dlm tabel Verification_IDNumber tsb dr tabel Approvalnya
                                    AccessApprovalIDNo.DeleteVerificationListIDNoMasterApproval(PendingApprovalID)
                                End If
                            End Using
                        End Using

                        '======================================================================

                        'hapus item tersebut dalam tabel VerificationList_Approval
                        AccessPending.DeleteVerificationListMasterApproval(rowData.PendingApprovalID)

                        'hapus item tersebut dalam tabel VerificationList_PendingApproval
                        Using AccessPendingVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingVerificationList, oSQLTrans)
                            AccessPendingVerificationList.DeleteVerificationListMasterPendingApproval(rowData.PendingApprovalID)
                        End Using

                        oSQLTrans.Commit()
                    End Using
                Else
                    Throw New Exception("Operation failed. The Verification List is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' VerificationList edit accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    02/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptVerificationListEdit()
        Dim oSQLTrans As SqlTransaction = Nothing

        'Using TranScope As New Transactions.TransactionScope
        Using AccessVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_MasterTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessVerificationList, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As AMLDAL.AMLDataSet.VerificationList_Master_ApprovalDataTable = AccessPending.GetVerificationListMasterApprovalDetail(Me.ParamPkListManagement)

                'Jika ObjTable.Rows.Count > 0 berarti VerificationList tsb msh dlm status Pending Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.VerificationList_Master_ApprovalRow = ObjTable.Rows(0)

                    Dim TotalAliasesPending, TotalAddressesPending, TotalIDNumbersPending As Int32

                    Using AccessVerificationListAliasApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAliasApproval, oSQLTrans)
                        TotalAliasesPending = AccessVerificationListAliasApproval.CountAliasesInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Using AccessVerificationListAddressApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAddressApproval, oSQLTrans)
                        TotalAddressesPending = AccessVerificationListAddressApproval.CountAddressesInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Using AccessVerificationListIDNumberApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListIDNumberApproval, oSQLTrans)
                        TotalIDNumbersPending = AccessVerificationListIDNumberApproval.CountIDNumbersInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Dim VerificationListID As Int64 = rowData.VerificationListId

                    Dim counter As Int32 = 14 + (2 * TotalAliasesPending) + (4 * TotalAddressesPending) + (2 * TotalIDNumbersPending)

                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(counter)

                    'catat aktifitas dalam tabel Audit Trail
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)

                        Dim ddateob As Nullable(Of Date)
                        If rowData.DateOfBirth.ToString("dd-MM-yyyy") = "01-01-1900" Then
                            ddateob = Nothing
                        Else
                            ddateob = rowData.DateOfBirth
                        End If
                        Dim strnationality As String
                        If rowData.IsnationalityNull Then
                            strnationality = ""
                        Else
                            strnationality = rowData.nationality
                        End If

                        AccessVerificationList.UpdateVerificationList(rowData.VerificationListId, rowData.DateEntered, rowData.DateUpdated, rowData.DateOfData, rowData.DisplayName, ddateob, rowData.VerificationListTypeId, rowData.VerificationListCategoryId, rowData.CustomRemark1, rowData.CustomRemark2, rowData.CustomRemark3, rowData.CustomRemark4, rowData.CustomRemark5, rowData.RefId, rowData.BirthPlace, strnationality)

                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListID", "Edit", rowData.VerificationListId_Old, rowData.VerificationListId, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateEntered", "Edit", rowData.DateEntered_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.DateEntered.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateUpdated", "Edit", rowData.DateUpdated_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.DateUpdated.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateOfData", "Edit", rowData.DateOfData_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.DateOfData.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DisplayName", "Edit", rowData.DisplayName_Old, rowData.DisplayName, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateOfBirth", "Edit", rowData.DateOfBirth_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.DateOfBirth.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListTypeId", "Edit", rowData.VerificationListTypeId_Old, rowData.VerificationListTypeId, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListCategoryId", "Edit", rowData.VerificationListCategoryId_Old, rowData.VerificationListCategoryId, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark1", "Edit", rowData.CustomRemark1_Old, rowData.CustomRemark1, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark2", "Edit", rowData.CustomRemark2_Old, rowData.CustomRemark2, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark3", "Edit", rowData.CustomRemark3_Old, rowData.CustomRemark3, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark4", "Edit", rowData.CustomRemark4_Old, rowData.CustomRemark4, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark5", "Edit", rowData.CustomRemark5_Old, rowData.CustomRemark5, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "RefId", "Edit", rowData.RefId_Old, rowData.RefId, "Rejected")

                        'tambahkan Alias2 utk VerificationList baru tersebut dalam tabel VerificationList_Alias
                        Using AccessApprovalAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAlias, oSQLTrans)
                            Using AccessAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AliasTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAlias, oSQLTrans)
                                AccessAlias.DeleteVerificationList_AliasByVerificationListID(VerificationListID)

                                Dim AliasApprovalTable As Data.DataTable = AccessApprovalAlias.GetVerificationList_Alias_ApprovalIDByPendingApprovalID(rowData.PendingApprovalID)

                                If AliasApprovalTable.Rows.Count > 0 Then
                                    For Each AliasApprovalRow As AMLDAL.AMLDataSet.VerificationList_Alias_ApprovalRow In AliasApprovalTable.Rows
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Alias", "VerificationListId", "Edit", "", rowData.VerificationListId, "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Alias", "Name", "Edit", "", AliasApprovalRow.Name, "Accepted")

                                        'masukkan Alias yg baru ke dlm tabel VerificationList_Alias
                                        AccessAlias.Insert(VerificationListID, AliasApprovalRow.Name)
                                    Next

                                    'hapus Alias yg sdh dimasukkan ke dlm tabel Verification_Alias tsb dr tabel Approvalnya
                                    AccessApprovalAlias.DeleteVerificationListAliasMasterApproval(rowData.PendingApprovalID)
                                End If
                            End Using
                        End Using

                        '=====================================================================
                        'tambahkan Address2 utk VerificationList baru tersebut dalam tabel VerificationList_Address
                        Using AccessApprovalAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAddress, oSQLTrans)
                            Using AccessAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AddressTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAddress, oSQLTrans)
                                AccessAddress.DeleteVerificationList_AddressByVerificationListID(VerificationListID)

                                Dim AddressApprovalTable As Data.DataTable = AccessApprovalAddress.GetVerificationList_Address_ApprovalIDByPendingApprovalID(rowData.PendingApprovalID)

                                If AddressApprovalTable.Rows.Count > 0 Then
                                    For Each AddressApprovalRow As AMLDAL.AMLDataSet.VerificationList_Address_ApprovalRow In AddressApprovalTable.Rows
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "VerificationListId", "Edit", "", rowData.VerificationListId, "Rejected")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "Address", "Edit", "", AddressApprovalRow.Address, "Rejected")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "AddressTypeId", "Edit", "", AddressApprovalRow.AddressTypeId, "Rejected")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "IsLocalAddress", "Edit", "", AddressApprovalRow.IsLocalAddress, "Rejected")

                                        AccessAddress.Insert(VerificationListID, AddressApprovalRow.Address, AddressApprovalRow.AddressTypeId, AddressApprovalRow.IsLocalAddress)
                                    Next

                                    'hapus Address yg sdh dimasukkan ke dlm tabel Verification_Address tsb dr tabel Approvalnya
                                    AccessApprovalAddress.DeleteVerificationListAddressMasterApproval(rowData.PendingApprovalID)
                                End If
                            End Using
                        End Using

                        '======================================================================
                        'tambahkan IDNo2 utk VerificationList baru tersebut dalam tabel VerificationList_IDNumber
                        Using AccessApprovalIDNo As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalIDNo, oSQLTrans)
                            Using AccessIDNo As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumberTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessIDNo, oSQLTrans)
                                AccessIDNo.DeleteVerificationList_IDNumberByVerificationListID(VerificationListID)

                                Dim IDNoApprovalTable As Data.DataTable = AccessApprovalIDNo.GetVerificationList_IDNumber_ApprovalIDByPendingApprovalID(rowData.PendingApprovalID)

                                If IDNoApprovalTable.Rows.Count > 0 Then
                                    For Each IDNoApprovalRow As AMLDAL.AMLDataSet.VerificationList_IDNumber_ApprovalRow In IDNoApprovalTable.Rows
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_IDNumber", "VerificationListId", "Edit", "", rowData.VerificationListId, "Rejected")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_IDNumber", "IDNumber", "Edit", "", IDNoApprovalRow.IDNumber, "Rejected")

                                        AccessIDNo.Insert(VerificationListID, IDNoApprovalRow.IDNumber)
                                    Next
                                End If

                                'hapus IDNo yg sdh dimasukkan ke dlm tabel Verification_IDNumber tsb dr tabel Approvalnya
                                AccessApprovalIDNo.DeleteVerificationListIDNoMasterApproval(rowData.PendingApprovalID)
                            End Using
                        End Using

                        'hapus item tersebut dalam tabel VerificationList_Approval
                        AccessPending.DeleteVerificationListMasterApproval(rowData.PendingApprovalID)

                        'hapus item tersebut dalam tabel VerificationList_PendingApproval
                        Using AccessPendingVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingVerificationList, oSQLTrans)
                            AccessPendingVerificationList.DeleteVerificationListMasterPendingApproval(rowData.PendingApprovalID)
                        End Using

                        oSQLTrans.Commit()
                    End Using
                Else 'Berarti Verification List tsb sdh tdk lg dlm status Pending Approval
                    Throw New Exception("Operation failed. The Verification List is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' VerificationList delete accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    '''     [Hendry]    21/06/2007 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptVerificationListDelete()
        Dim oSQLTrans As SqlTransaction = Nothing

        'Using TranScope As New Transactions.TransactionScope
        Using AccessVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_MasterTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessVerificationList, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetVerificationListMasterApprovalDetail(Me.ParamPkListManagement)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.VerificationList_Master_ApprovalRow = ObjTable.Rows(0)

                    Dim TotalAliasesPending, TotalAddressesPending, TotalIDNumbersPending As Int32

                    Using AccessVerificationListAliasApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAliasApproval, oSQLTrans)
                        TotalAliasesPending = AccessVerificationListAliasApproval.CountAliasesInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Using AccessVerificationListAddressApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAddressApproval, oSQLTrans)
                        TotalAddressesPending = AccessVerificationListAddressApproval.CountAddressesInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Using AccessVerificationListIDNumberApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListIDNumberApproval, oSQLTrans)
                        TotalIDNumbersPending = AccessVerificationListIDNumberApproval.CountIDNumbersInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Dim counter As Int32 = 14 + (3 * TotalAliasesPending) + (5 * TotalAddressesPending) + (3 * TotalIDNumbersPending)

                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(counter)

                    'hapus item tersebut dr tabel VerificationList_Master
                    AccessVerificationList.DeleteVerificationListMasterByVerificationListID(rowData.VerificationListId)

                    'catat aktifitas dalam tabel Audit Trail
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                        Dim PendingApprovalID As Int64 = rowData.PendingApprovalID
                        Dim VerificationListID As Int64 = rowData.VerificationListId

                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListID", "Delete", rowData.VerificationListId, "", "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateEntered", "Delete", rowData.DateEntered.ToString("dd-MMMM-yyyy HH:mm"), "", "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateUpdated", "Delete", rowData.DateUpdated.ToString("dd-MMMM-yyyy HH:mm"), "", "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateOfData", "Delete", rowData.DateOfData.ToString("dd-MMMM-yyyy HH:mm"), "", "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DisplayName", "Delete", rowData.DisplayName, "", "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateOfBirth", "Delete", rowData.DateOfBirth.ToString("dd-MMMM-yyyy HH:mm"), "", "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "BirhtPlace", "Delete", rowData.BirthPlace.ToString, "", "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListTypeId", "Delete", rowData.VerificationListTypeId, "", "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListCategoryId", "Delete", rowData.VerificationListCategoryId, "", "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark1", "Delete", rowData.CustomRemark1, "", "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark2", "Delete", rowData.CustomRemark2, "", "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark3", "Delete", rowData.CustomRemark3, "", "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark4", "Delete", rowData.CustomRemark4, "", "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark5", "Delete", rowData.CustomRemark5, "", "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "RefId", "Delete", rowData.RefId, "", "Accepted")

                        'tambahkan Alias2 utk VerificationList baru tersebut dalam tabel VerificationList_Alias
                        Using AccessAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AliasTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAlias, oSQLTrans)
                            Using AccessApprovalAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAlias, oSQLTrans)
                                Dim AliasApprovalTable As Data.DataTable = AccessApprovalAlias.GetVerificationList_Alias_ApprovalIDByPendingApprovalID(PendingApprovalID)

                                If AliasApprovalTable.Rows.Count > 0 Then
                                    For Each AliasApprovalRow As AMLDAL.AMLDataSet.VerificationList_Alias_ApprovalRow In AliasApprovalTable.Rows
                                        AccessAlias.DeleteVerificationList_AliasByVerificationListID(VerificationListID)

                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Alias", "VerificationList_Alias_ID", "Delete", AliasApprovalRow.VerificationList_Alias_ID, "", "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Alias", "VerificationListId", "Delete", AliasApprovalRow.VerificationListId, "", "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Alias", "Name", "Delete", AliasApprovalRow.Name, "", "Accepted")
                                    Next

                                    'hapus Alias yg sdh dimasukkan ke dlm tabel Verification_Alias tsb dr tabel Approvalnya
                                    AccessApprovalAlias.DeleteVerificationListAliasMasterApproval(PendingApprovalID)
                                End If
                            End Using
                        End Using

                        '=====================================================================
                        'tambahkan Address2 utk VerificationList baru tersebut dalam tabel VerificationList_Address
                        Using AccessAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AddressTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAddress, oSQLTrans)
                            Using AccessApprovalAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAddress, oSQLTrans)
                                Dim AddressApprovalTable As Data.DataTable = AccessApprovalAddress.GetVerificationList_Address_ApprovalIDByPendingApprovalID(PendingApprovalID)

                                If AddressApprovalTable.Rows.Count > 0 Then
                                    For Each AddressApprovalRow As AMLDAL.AMLDataSet.VerificationList_Address_ApprovalRow In AddressApprovalTable.Rows
                                        AccessAddress.DeleteVerificationList_AddressByVerificationListID(VerificationListID)

                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "VerificationList_Address_Id", "Delete", AddressApprovalRow.VerificationList_Address_Id, "", "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "VerificationListId", "Delete", AddressApprovalRow.VerificationListId, "", "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "Address", "Delete", AddressApprovalRow.Address, "", "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "AddressTypeId", "Delete", AddressApprovalRow.AddressTypeId, "", "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "IsLocalAddress", "Delete", AddressApprovalRow.IsLocalAddress, "", "Accepted")
                                    Next

                                    'hapus Address yg sdh dimasukkan ke dlm tabel Verification_Address tsb dr tabel Approvalnya
                                    AccessApprovalAddress.DeleteVerificationListAddressMasterApproval(PendingApprovalID)
                                End If
                            End Using
                        End Using

                        '======================================================================
                        'tambahkan IDNo2 utk VerificationList baru tersebut dalam tabel VerificationList_IDNumber
                        Using AccessIDNo As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumberTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessIDNo, oSQLTrans)
                            Using AccessApprovalIDNo As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalIDNo, oSQLTrans)
                                Dim IDNoApprovalTable As Data.DataTable = AccessApprovalIDNo.GetVerificationList_IDNumber_ApprovalIDByPendingApprovalID(PendingApprovalID)

                                If IDNoApprovalTable.Rows.Count > 0 Then
                                    For Each IDNoApprovalRow As AMLDAL.AMLDataSet.VerificationList_IDNumber_ApprovalRow In IDNoApprovalTable.Rows
                                        AccessIDNo.DeleteVerificationList_IDNumberByVerificationListID(VerificationListID)

                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_IDNumber", "VerificationList_IDNumber_ID", "Delete", IDNoApprovalRow.VerificationList_IDNumber_ID, "", "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_IDNumber", "VerificationListId", "Delete", IDNoApprovalRow.VerificationListId, "", "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_IDNumber", "IDNumber", "Delete", IDNoApprovalRow.IDNumber, "", "Accepted")
                                    Next

                                    'hapus IDNo yg sdh dimasukkan ke dlm tabel Verification_IDNumber tsb dr tabel Approvalnya
                                    AccessApprovalIDNo.DeleteVerificationListIDNoMasterApproval(PendingApprovalID)
                                End If
                            End Using
                        End Using

                        '======================================================================

                        'hapus item tersebut dalam tabel VerificationList_Approval
                        AccessPending.DeleteVerificationListMasterApproval(rowData.PendingApprovalID)

                        'hapus item tersebut dalam tabel VerificationList_PendingApproval
                        Using AccessPendingVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingVerificationList, oSQLTrans)
                            AccessPendingVerificationList.DeleteVerificationListMasterPendingApproval(rowData.PendingApprovalID)
                        End Using

                        oSQLTrans.Commit()
                    End Using
                Else
                    Throw New Exception("Operation failed. The Verification List is no longer in the approval table.")
                End If '
            End Using
        End Using
        'End Using
    End Sub

#End Region
#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject VerificationList add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectVerificationListAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_MasterTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessVerificationList, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetVerificationListMasterApprovalDetail(Me.ParamPkListManagement)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.VerificationList_Master_ApprovalRow = ObjTable.Rows(0)

                    Dim TotalAliasesPending, TotalAddressesPending, TotalIDNumbersPending As Int32

                    Using AccessVerificationListAliasApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAliasApproval, oSQLTrans)
                        TotalAliasesPending = AccessVerificationListAliasApproval.CountAliasesInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Using AccessVerificationListAddressApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAddressApproval, oSQLTrans)
                        TotalAddressesPending = AccessVerificationListAddressApproval.CountAddressesInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Using AccessVerificationListIDNumberApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListIDNumberApproval, oSQLTrans)
                        TotalIDNumbersPending = AccessVerificationListIDNumberApproval.CountIDNumbersInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Dim counter As Int32 = 13 + TotalAliasesPending + (3 * TotalAddressesPending) + TotalIDNumbersPending

                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(counter)

                    'catat aktifitas dalam tabel Audit Trail
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                        Dim PendingApprovalID As Int64 = rowData.PendingApprovalID

                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateEntered", "Add", "", rowData.DateEntered.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateUpdated", "Add", "", rowData.DateUpdated.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateOfData", "Add", "", rowData.DateOfData.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DisplayName", "Add", "", rowData.DisplayName, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateOfBirth", "Add", "", rowData.DateOfBirth.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "BirthPlace", "Add", "", rowData.BirthPlace, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListTypeId", "Add", "", rowData.VerificationListTypeId, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListCategoryId", "Add", "", rowData.VerificationListCategoryId, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark1", "Add", "", rowData.CustomRemark1, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark2", "Add", "", rowData.CustomRemark2, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark3", "Add", "", rowData.CustomRemark3, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark4", "Add", "", rowData.CustomRemark4, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark5", "Add", "", rowData.CustomRemark5, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "RefId", "Add", "", rowData.RefId, "Rejected")

                        'hapus Alias2 utk VerificationList baru tersebut dr tabel VerificationList_Alias_Approval
                        Using AccessApprovalAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAlias, oSQLTrans)
                            Dim AliasApprovalTable As Data.DataTable = AccessApprovalAlias.GetVerificationList_Alias_ApprovalIDByPendingApprovalID(PendingApprovalID)

                            If AliasApprovalTable.Rows.Count > 0 Then
                                For Each AliasApprovalRow As AMLDAL.AMLDataSet.VerificationList_Alias_ApprovalRow In AliasApprovalTable.Rows
                                    'catat kegiatan penghapusan Alias2 tsb dlm tabel Audit Trail
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Alias", "Name", "Add", "", AliasApprovalRow.Name, "Rejected")
                                Next

                                'hapus Alias tsb dr tabel Verification_Alias tsb dr tabel Approvalnya
                                AccessApprovalAlias.DeleteVerificationListAliasMasterApproval(PendingApprovalID)
                            End If

                        End Using

                        '=====================================================================
                        'tambahkan Address2 utk VerificationList baru tersebut dalam tabel VerificationList_Address
                        Using AccessApprovalAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAddress, oSQLTrans)
                            Dim AddressApprovalTable As Data.DataTable = AccessApprovalAddress.GetVerificationList_Address_ApprovalIDByPendingApprovalID(PendingApprovalID)

                            If AddressApprovalTable.Rows.Count > 0 Then
                                For Each AddressApprovalRow As AMLDAL.AMLDataSet.VerificationList_Address_ApprovalRow In AddressApprovalTable.Rows
                                    'catat kegiatan penghapusan Address2 tsb dlm tabel Audit Trail
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "Address", "Add", "", AddressApprovalRow.Address, "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "AddressTypeId", "Add", "", AddressApprovalRow.AddressTypeId, "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "IsLocalAddress", "Add", "", AddressApprovalRow.IsLocalAddress, "Rejected")
                                Next

                                'hapus Address yg sdh dimasukkan ke dlm tabel Verification_Address tsb dr tabel Approvalnya
                                AccessApprovalAddress.DeleteVerificationListAddressMasterApproval(PendingApprovalID)
                            End If
                        End Using

                        '======================================================================
                        'tambahkan IDNo2 utk VerificationList baru tersebut dalam tabel VerificationList_IDNumber
                        Using AccessApprovalIDNo As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalIDNo, oSQLTrans)
                            Dim IDNoApprovalTable As Data.DataTable = AccessApprovalIDNo.GetVerificationList_IDNumber_ApprovalIDByPendingApprovalID(PendingApprovalID)

                            If IDNoApprovalTable.Rows.Count > 0 Then
                                For Each IDNoApprovalRow As AMLDAL.AMLDataSet.VerificationList_IDNumber_ApprovalRow In IDNoApprovalTable.Rows
                                    'catat kegiatan penghapusan Address2 tsb dlm tabel Audit Trail
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_IDNumber", "IDNumber", "Add", "", IDNoApprovalRow.IDNumber, "Rejected")
                                Next

                                'hapus IDNo yg sdh dimasukkan ke dlm tabel Verification_IDNumber tsb dr tabel Approvalnya
                                AccessApprovalIDNo.DeleteVerificationListIDNoMasterApproval(PendingApprovalID)
                            End If
                        End Using

                        'hapus item tersebut dalam tabel VerificationList_Approval
                        AccessPending.DeleteVerificationListMasterApproval(rowData.PendingApprovalID)

                        'hapus item tersebut dalam tabel VerificationList_PendingApproval
                        Using AccessPendingVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingVerificationList, oSQLTrans)
                            AccessPendingVerificationList.DeleteVerificationListMasterPendingApproval(rowData.PendingApprovalID)
                        End Using

                        oSQLTrans.Commit()
                    End Using
                Else
                    Throw New Exception("Operation failed. The Verification List is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject VerificationList edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    02/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectVerificationListEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_MasterTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessVerificationList, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetVerificationListMasterApprovalDetail(Me.ParamPkListManagement)

                'Jika ObjTable.Rows.Count > 0 berarti VerificationList tsb msh dlm status Pending Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.VerificationList_Master_ApprovalRow = ObjTable.Rows(0)

                    Dim TotalAliasesPending, TotalAddressesPending, TotalIDNumbersPending As Int32

                    Using AccessVerificationListAliasApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAliasApproval, oSQLTrans)
                        TotalAliasesPending = AccessVerificationListAliasApproval.CountAliasesInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Using AccessVerificationListAddressApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAddressApproval, oSQLTrans)
                        TotalAddressesPending = AccessVerificationListAddressApproval.CountAddressesInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Using AccessVerificationListIDNumberApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListIDNumberApproval, oSQLTrans)
                        TotalIDNumbersPending = AccessVerificationListIDNumberApproval.CountIDNumbersInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Dim counter As Int32 = 14 + (2 * TotalAliasesPending) + (4 * TotalAddressesPending) + (2 * TotalIDNumbersPending)

                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(counter)

                    'catat aktifitas dalam tabel Audit Trail
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                        Dim PendingApprovalID As Int64 = rowData.PendingApprovalID

                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListID", "Edit", rowData.VerificationListId_Old, rowData.VerificationListId, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateEntered", "Edit", rowData.DateEntered_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.DateEntered.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateUpdated", "Edit", rowData.DateUpdated_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.DateUpdated.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateOfData", "Edit", rowData.DateOfData_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.DateOfData.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DisplayName", "Edit", rowData.DisplayName_Old, rowData.DisplayName, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateOfBirth", "Edit", rowData.DateOfBirth_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.DateOfBirth.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "BirthPlace", "Edit", rowData.BirthPlace_Old, rowData.BirthPlace, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListTypeId", "Edit", rowData.VerificationListTypeId_Old, rowData.VerificationListTypeId, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListCategoryId", "Edit", rowData.VerificationListCategoryId_Old, rowData.VerificationListCategoryId, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark1", "Edit", rowData.CustomRemark1_Old, rowData.CustomRemark1, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark2", "Edit", rowData.CustomRemark2_Old, rowData.CustomRemark2, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark3", "Edit", rowData.CustomRemark3_Old, rowData.CustomRemark3, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark4", "Edit", rowData.CustomRemark4_Old, rowData.CustomRemark4, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark5", "Edit", rowData.CustomRemark5_Old, rowData.CustomRemark5, "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "RefId", "Edit", rowData.RefId_Old, rowData.RefId, "Rejected")

                        'tambahkan Alias2 utk VerificationList baru tersebut dalam tabel VerificationList_Alias
                        Using AccessApprovalAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAlias, oSQLTrans)
                            Dim AliasApprovalTable As Data.DataTable = AccessApprovalAlias.GetVerificationList_Alias_ApprovalIDByPendingApprovalID(PendingApprovalID)

                            If AliasApprovalTable.Rows.Count > 0 Then
                                For Each AliasApprovalRow As AMLDAL.AMLDataSet.VerificationList_Alias_ApprovalRow In AliasApprovalTable.Rows
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Alias", "VerificationListId", "Edit", "", rowData.VerificationListId, "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Alias", "Name", "Edit", "", AliasApprovalRow.Name, "Rejected")
                                Next
                                'hapus Alias yg sdh dimasukkan ke dlm tabel Verification_Alias tsb dr tabel Approvalnya
                                AccessApprovalAlias.DeleteVerificationListAliasMasterApproval(rowData.PendingApprovalID)
                            End If
                        End Using

                        '=====================================================================
                        'tambahkan Address2 utk VerificationList baru tersebut dalam tabel VerificationList_Address
                        Using AccessApprovalAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAddress, oSQLTrans)
                            Dim AddressApprovalTable As Data.DataTable = AccessApprovalAddress.GetVerificationList_Address_ApprovalIDByPendingApprovalID(PendingApprovalID)

                            If AddressApprovalTable.Rows.Count > 0 Then
                                For Each AddressApprovalRow As AMLDAL.AMLDataSet.VerificationList_Address_ApprovalRow In AddressApprovalTable.Rows
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "VerificationListId", "Edit", "", rowData.VerificationListId, "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "Address", "Edit", "", AddressApprovalRow.Address, "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "AddressTypeId", "Edit", "", AddressApprovalRow.AddressTypeId, "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "IsLocalAddress", "Edit", "", AddressApprovalRow.IsLocalAddress, "Rejected")
                                Next
                                'hapus Address yg sdh dimasukkan ke dlm tabel Verification_Address tsb dr tabel Approvalnya
                                AccessApprovalAddress.DeleteVerificationListAddressMasterApproval(rowData.PendingApprovalID)
                            End If
                        End Using

                        '======================================================================
                        'tambahkan IDNo2 utk VerificationList baru tersebut dalam tabel VerificationList_IDNumber
                        Using AccessApprovalIDNo As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalIDNo, oSQLTrans)
                            Dim IDNoApprovalTable As Data.DataTable = AccessApprovalIDNo.GetVerificationList_IDNumber_ApprovalIDByPendingApprovalID(PendingApprovalID)

                            If IDNoApprovalTable.Rows.Count > 0 Then
                                For Each IDNoApprovalRow As AMLDAL.AMLDataSet.VerificationList_IDNumber_ApprovalRow In IDNoApprovalTable.Rows
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_IDNumber", "VerificationListId", "Edit", "", rowData.VerificationListId, "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_IDNumber", "IDNumber", "Edit", "", IDNoApprovalRow.IDNumber, "Rejected")
                                Next
                                'hapus IDNo yg sdh dimasukkan ke dlm tabel Verification_IDNumber tsb dr tabel Approvalnya
                                AccessApprovalIDNo.DeleteVerificationListIDNoMasterApproval(rowData.PendingApprovalID)
                            End If
                        End Using

                        'hapus item tersebut dalam tabel VerificationList_Approval
                        AccessPending.DeleteVerificationListMasterApproval(rowData.PendingApprovalID)

                        'hapus item tersebut dalam tabel VerificationList_PendingApproval
                        Using AccessPendingVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingVerificationList, oSQLTrans)
                            AccessPendingVerificationList.DeleteVerificationListMasterPendingApproval(rowData.PendingApprovalID)
                        End Using

                        oSQLTrans.Commit()
                    End Using
                Else 'Berarti Verification List tsb sdh tdk lg dlm status Pending Approval
                    Throw New Exception("Operation failed. The Verification List is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject VerificationList delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectVerificationListDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_MasterTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessVerificationList, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetVerificationListMasterApprovalDetail(Me.ParamPkListManagement)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.VerificationList_Master_ApprovalRow = ObjTable.Rows(0)

                    Dim TotalAliasesPending, TotalAddressesPending, TotalIDNumbersPending As Int32

                    Using AccessVerificationListAliasApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAliasApproval, oSQLTrans)
                        TotalAliasesPending = AccessVerificationListAliasApproval.CountAliasesInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Using AccessVerificationListAddressApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAddressApproval, oSQLTrans)
                        TotalAddressesPending = AccessVerificationListAddressApproval.CountAddressesInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Using AccessVerificationListIDNumberApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListIDNumberApproval, oSQLTrans)
                        TotalIDNumbersPending = AccessVerificationListIDNumberApproval.CountIDNumbersInApprovalByPendingApprovalID(Me.ParamPkListManagement)
                    End Using

                    Dim counter As Int32 = 14 + (3 * TotalAliasesPending) + (5 * TotalAddressesPending) + (3 * TotalIDNumbersPending)

                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(counter)

                    'catat aktifitas dalam tabel Audit Trail
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                        Dim PendingApprovalID As Int64 = rowData.PendingApprovalID

                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListID", "Delete", rowData.VerificationListId, "", "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateEntered", "Delete", rowData.DateEntered.ToString("dd-MMMM-yyyy HH:mm"), "", "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateUpdated", "Delete", rowData.DateUpdated.ToString("dd-MMMM-yyyy HH:mm"), "", "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateOfData", "Delete", rowData.DateOfData.ToString("dd-MMMM-yyyy HH:mm"), "", "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DisplayName", "Delete", rowData.DisplayName, "", "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "DateOfBirth", "Delete", rowData.DateOfBirth.ToString("dd-MMMM-yyyy HH:mm"), "", "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "BirthPlace", "Delete", rowData.BirthPlace, "", "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListTypeId", "Delete", rowData.VerificationListTypeId, "", "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "VerificationListCategoryId", "Delete", rowData.VerificationListCategoryId, "", "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark1", "Delete", rowData.CustomRemark1, "", "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark2", "Delete", rowData.CustomRemark2, "", "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark3", "Delete", rowData.CustomRemark3, "", "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark4", "Delete", rowData.CustomRemark4, "", "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "CustomRemark5", "Delete", rowData.CustomRemark5, "", "Rejected")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "RefId", "Delete", rowData.RefId, "", "Rejected")

                        'hapus Alias2 utk VerificationList baru tersebut dr tabel VerificationList_Alias_Approval
                        'Using AccessAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AliasTableAdapter
                        Using AccessApprovalAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAlias, oSQLTrans)
                            Dim AliasApprovalTable As Data.DataTable = AccessApprovalAlias.GetVerificationList_Alias_ApprovalIDByPendingApprovalID(PendingApprovalID)

                            If AliasApprovalTable.Rows.Count > 0 Then
                                For Each AliasApprovalRow As AMLDAL.AMLDataSet.VerificationList_Alias_ApprovalRow In AliasApprovalTable.Rows
                                    'catat kegiatan penghapusan Alias2 tsb dlm tabel Audit Trail
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Alias", "VerificationList_Alias_ID", "Delete", AliasApprovalRow.VerificationList_Alias_ID, "", "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Alias", "VerificationListId", "Delete", AliasApprovalRow.VerificationListId, "", "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Alias", "Name", "Delete", AliasApprovalRow.Name, "", "Rejected")
                                Next

                                'hapus Alias tsb dr tabel Verification_Alias tsb dr tabel Approvalnya
                                AccessApprovalAlias.DeleteVerificationListAliasMasterApproval(Me.ParamPkListManagement)
                            End If
                        End Using
                        'End Using

                        '=====================================================================
                        'tambahkan Address2 utk VerificationList baru tersebut dalam tabel VerificationList_Address
                        'Using AccessAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AddressTableAdapter
                        Using AccessApprovalAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAddress, oSQLTrans)
                            Dim AddressApprovalTable As Data.DataTable = AccessApprovalAddress.GetVerificationList_Address_ApprovalIDByPendingApprovalID(PendingApprovalID)

                            If AddressApprovalTable.Rows.Count > 0 Then
                                For Each AddressApprovalRow As AMLDAL.AMLDataSet.VerificationList_Address_ApprovalRow In AddressApprovalTable.Rows
                                    'catat kegiatan penghapusan Address2 tsb dlm tabel Audit Trail
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "VerificationList_Address_Id", "Delete", AddressApprovalRow.VerificationList_Address_Id, "", "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "VerificationListId", "Delete", AddressApprovalRow.VerificationListId, "", "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "Address", "Delete", AddressApprovalRow.Address, "", "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "AddressTypeId", "Delete", AddressApprovalRow.AddressTypeId, "", "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_Address", "IsLocalAddress", "Delete", AddressApprovalRow.IsLocalAddress, "", "Rejected")
                                Next
                                'hapus Address yg sdh dimasukkan ke dlm tabel Verification_Address tsb dr tabel Approvalnya
                                AccessApprovalAddress.DeleteVerificationListAddressMasterApproval(Me.ParamPkListManagement)
                            End If
                        End Using
                        'End Using

                        '======================================================================
                        'tambahkan IDNo2 utk VerificationList baru tersebut dalam tabel VerificationList_IDNumber
                        'Using AccessIDNo As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumberTableAdapter
                        Using AccessApprovalIDNo As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalIDNo, oSQLTrans)
                            Dim IDNoApprovalTable As Data.DataTable = AccessApprovalIDNo.GetVerificationList_IDNumber_ApprovalIDByPendingApprovalID(PendingApprovalID)

                            If IDNoApprovalTable.Rows.Count > 0 Then
                                For Each IDNoApprovalRow As AMLDAL.AMLDataSet.VerificationList_IDNumber_ApprovalRow In IDNoApprovalTable.Rows
                                    'catat kegiatan penghapusan Address2 tsb dlm tabel Audit Trail
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_IDNumber", "VerificationList_IDNumber_ID", "Delete", IDNoApprovalRow.VerificationList_IDNumber_ID, "", "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_IDNumber", "VerificationListId", "Delete", IDNoApprovalRow.VerificationListId, "", "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationList_IDNumber", "IDNumber", "Delete", IDNoApprovalRow.IDNumber, "", "Rejected")
                                Next

                                'hapus IDNo yg sdh dimasukkan ke dlm tabel Verification_IDNumber tsb dr tabel Approvalnya
                                AccessApprovalIDNo.DeleteVerificationListIDNoMasterApproval(Me.ParamPkListManagement)
                            End If
                        End Using
                        'End Using

                        '======================================================================

                        'hapus item tersebut dalam tabel VerificationList_Approval
                        AccessPending.DeleteVerificationListMasterApproval(rowData.PendingApprovalID)

                        'hapus item tersebut dalam tabel VerificationList_PendingApproval
                        Using AccessPendingVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingVerificationList, oSQLTrans)
                            AccessPendingVerificationList.DeleteVerificationListMasterPendingApproval(rowData.PendingApprovalID)
                        End Using

                        oSQLTrans.Commit()
                    End Using
                Else
                    Throw New Exception("Operation failed. The Verification List is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub
#End Region

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                '  Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                ' Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HideAll()
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.VerificationListAdd
                        Me.LoadVerificationListAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.VerificationListEdit
                        Me.LoadVerificationListEdit()
                    Case Sahassa.AML.Commonly.TypeConfirm.VerificationListDelete
                        Me.LoadVerificationListDelete()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    'Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
    '    Try
    '        Select Case Me.ParamType
    '            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListAdd
    '                Me.AcceptVerificationListAdd()
    '            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListEdit
    '                Me.AcceptVerificationListEdit()
    '            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListDelete
    '                Me.AcceptVerificationListDelete()
    '            Case Else
    '                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
    '        End Select

    '        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListManagementApproval.aspx"

    '        Me.Response.Redirect("VerificationListManagementApproval.aspx", False)
    '    Catch ex As Exception
    '        Me.cvalPageErr.IsValid = False
    '        Me.cvalPageErr.ErrorMessage = ex.Message
    '        LogError(ex)
    '    End Try
    'End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    'Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
    '    Try
    '        Select Case Me.ParamType
    '            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListAdd
    '                Me.RejectVerificationListAdd()
    '            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListEdit
    '                Me.RejectVerificationListEdit()
    '            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListDelete
    '                Me.RejectVerificationListDelete()
    '            Case Else
    '                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
    '        End Select

    '        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListManagementApproval.aspx"

    '        Me.Response.Redirect("VerificationListManagementApproval.aspx", False)
    '    Catch ex As Exception
    '        Me.cvalPageErr.IsValid = False
    '        Me.cvalPageErr.ErrorMessage = ex.Message
    '        LogError(ex)
    '    End Try
    'End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListManagementApproval.aspx"

        Me.Response.Redirect("VerificationListManagementApproval.aspx", False)
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click

        Try

            If cboStatusApproval.SelectedValue = Sahassa.AML.CommonlyEnum.StatusApproval.None Then
                Throw New Exception("Please Status Approval Value.")
            ElseIf cboStatusApproval.SelectedValue = Sahassa.AML.CommonlyEnum.StatusApproval.Accept Then
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.VerificationListAdd
                        Me.AcceptVerificationListAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.VerificationListEdit
                        Me.AcceptVerificationListEdit()
                    Case Sahassa.AML.Commonly.TypeConfirm.VerificationListDelete
                        Me.AcceptVerificationListDelete()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListManagementApproval.aspx"

                Me.Response.Redirect("VerificationListManagementApproval.aspx", False)
            ElseIf cboStatusApproval.SelectedValue = Sahassa.AML.CommonlyEnum.StatusApproval.Reject Then
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.VerificationListAdd
                        Me.RejectVerificationListAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.VerificationListEdit
                        Me.RejectVerificationListEdit()
                    Case Sahassa.AML.Commonly.TypeConfirm.VerificationListDelete
                        Me.RejectVerificationListDelete()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListManagementApproval.aspx"

                Me.Response.Redirect("VerificationListManagementApproval.aspx", False)
            ElseIf cboStatusApproval.SelectedValue = Sahassa.AML.CommonlyEnum.StatusApproval.Correction Then
                If txtReviewNotes.Text.Trim = "" Then
                    Throw New Exception("Please Enter Review Notes For This Verification List.")
                End If
                AMLBLL.VerificationListBLL.SetCorrection(Me.ParamPkListManagement, txtReviewNotes.Text.Trim)
                Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListManagementApproval.aspx"
                Me.Response.Redirect("VerificationListManagementApproval.aspx", False)
            End If

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub cboStatusApproval_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboStatusApproval.SelectedIndexChanged
        Try
            If cboStatusApproval.SelectedValue = Sahassa.AML.CommonlyEnum.StatusApproval.Correction Then
                PanelReviewNotes.Visible = True
            Else
                PanelReviewNotes.Visible = False
                txtReviewNotes.Text = ""
            End If

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub
End Class