Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Excel
Imports Sahassa.AML.Commonly
Imports System.Data.SqlClient
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports AMLBLL.DataType
Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports NPOI.HSSF.UserModel
Imports NPOI.HPSF
Imports NPOI.POIFS.FileSystem
Imports NPOI.SS.UserModel

Partial Class CDDLNPWorkflowUpload
    Inherits Parent

    Private hssfworkbook As HSSFWorkbook
#Region "Property"

    Private ReadOnly Property ModuleName() As String
        Get
            Return "CDDLNPWorkflowUpload"
        End Get
    End Property
    Private ReadOnly Property ModuleName2() As String
        Get
            Return "CDDLNPReassignment"
        End Get
    End Property

    'Private ReadOnly Property objAccountOwner() As SahassaNettier.Entities.TList(Of SahassaNettier.Entities.AccountOwner)
    '    Get
    '        Session("Match.AccountOwner") = SahassaNettier.Data.DataRepository.AccountOwnerProvider.GetAll
    '        Return CType(Session("Match.AccountOwner"), SahassaNettier.Entities.TList(Of SahassaNettier.Entities.AccountOwner))
    '    End Get
    'End Property
    'Private StrAccountOwnerId As String
    'Private Function FindAccountOwnerID(ByVal bk As SahassaNettier.Entities.AccountOwner) As Boolean
    '    If bk.AccountOwnerId = StrAccountOwnerId Then
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function
    Private StrUserId As String
    Private Property objUser() As SahassaNettier.Entities.TList(Of SahassaNettier.Entities.User)
        Get
            Return CType(Session("CDDLNPWorkflowUpload.MatchUser"), SahassaNettier.Entities.TList(Of SahassaNettier.Entities.User))
        End Get
        Set(ByVal value As SahassaNettier.Entities.TList(Of SahassaNettier.Entities.User))
            Session("CDDLNPWorkflowUpload.MatchUser") = value
        End Set
    End Property
    Private Function FindUserID(ByVal bk As SahassaNettier.Entities.User) As Boolean
        If bk.UserID.ToLower = StrUserId.ToLower Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Property intNotListedRM() As Int32
        Get
            Return Session("CDDLNPWorkflowUpload.intNotListedRM")
        End Get
        Set(ByVal value As Int32)
            Session("CDDLNPWorkflowUpload.intNotListedRM") = value
        End Set
    End Property
    Private Property objUserRM() As List(Of String)
        Get
            Return Session("CDDLNPWorkflowUpload.MatchUserRM")
        End Get
        Set(ByVal value As List(Of String))
            Session("CDDLNPWorkflowUpload.MatchUserRM") = value
        End Set
    End Property
    Private Function FindUserRMID(ByVal bk As String) As Boolean
        If bk.ToLower = StrUserId.ToLower Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Property objUserReassign() As List(Of String)
        Get
            Return Session("CDDLNPWorkflowUpload.MatchUserReassign")
        End Get
        Set(ByVal value As List(Of String))
            Session("CDDLNPWorkflowUpload.MatchUserReassign") = value
        End Set
    End Property
    Private Function FindUserReassign(ByVal bk As String) As Boolean
        If bk.ToLower = StrUserId.ToLower Then
            Return True
        Else
            Return False
        End If
    End Function

    'Private StrAccountOwnerId As String
    'Private Property objAccountOwnerID() As List(Of String)
    '    Get
    '        Return Session("CDDLNPWorkflowUpload.MatchAccountOwner")
    '    End Get
    '    Set(ByVal value As List(Of String))
    '        Session("CDDLNPWorkflowUpload.MatchAccountOwner") = value
    '    End Set
    'End Property
    'Private Function FindAccountOwnerID(ByVal bk As String) As Boolean
    '    If bk.ToLower = StrAccountOwnerId.ToLower Then
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function

    Private Property GroupTableSuccessData() As TList(Of CDDLNP_WorkflowUpload_ApprovalDetail)
        Get
            Return Session("CDDLNP_WorkflowUpload.Success")
        End Get
        Set(ByVal value As TList(Of CDDLNP_WorkflowUpload_ApprovalDetail))
            Session("CDDLNP_WorkflowUpload.Success") = value
        End Set
    End Property

    Private Property GroupTableDataUserAssignment() As TList(Of CDDLNP_UserAssignment)
        Get
            Return Session("CDDLNP_WorkflowUpload.UserAssignment")
        End Get
        Set(ByVal value As TList(Of CDDLNP_UserAssignment))
            Session("CDDLNP_WorkflowUpload.UserAssignment") = value
        End Set
    End Property

    Private Property GroupTableAnomalyData() As TList(Of CDDLNP_WorkflowUpload_ApprovalDetail)
        Get
            Return Session("CDDLNP_WorkflowUpload.Anomaly")
        End Get
        Set(ByVal value As TList(Of CDDLNP_WorkflowUpload_ApprovalDetail))
            Session("CDDLNP_WorkflowUpload.Anomaly") = value
        End Set
    End Property

    Private Property SetnGetValidDataTable() As TList(Of CDDLNP_WorkflowUpload_ApprovalDetail)
        Get
            Return CType(IIf(Session("CDDLNPWorkflowUpload.ValidData") Is Nothing, Nothing, Session("CDDLNPWorkflowUpload.ValidData")), TList(Of CDDLNP_WorkflowUpload_ApprovalDetail))
        End Get
        Set(ByVal value As TList(Of CDDLNP_WorkflowUpload_ApprovalDetail))
            Session("CDDLNPWorkflowUpload.ValidData") = value
        End Set
    End Property

    Private Property SetnGetAnomalyDataTable() As Data.DataTable
        Get
            Return CType(IIf(Session("CDDLNPWorkflowUpload.InvalidData") Is Nothing, Nothing, Session("CDDLNPWorkflowUpload.InvalidData")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CDDLNPWorkflowUpload.InvalidData") = value
        End Set
    End Property

    Private Property DS_UploadData() As Data.DataSet
        Get
            Return CType(IIf(Session("CDDLNPWorkflowUpload.DS_UploadData") Is Nothing, Nothing, Session("CDDLNPWorkflowUpload.DS_UploadData")), DataSet)
        End Get
        Set(ByVal value As Data.DataSet)
            Session("CDDLNPWorkflowUpload.DS_UploadData") = value
        End Set
    End Property

    Private Property DT_UploadData() As Data.DataTable
        Get
            Return CType(IIf(Session("CDDLNPWorkflowUpload.DT_UploadData") Is Nothing, Nothing, Session("CDDLNPWorkflowUpload.DT_UploadData")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CDDLNPWorkflowUpload.DT_UploadData") = value
        End Set
    End Property

    Private Property DT_UploadData2() As Data.DataTable
        Get
            Return CType(IIf(Session("CDDLNPWorkflowUpload.DT_UploadData2") Is Nothing, Nothing, Session("CDDLNPWorkflowUpload.DT_UploadData2")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CDDLNPWorkflowUpload.DT_UploadData2") = value
        End Set
    End Property

#End Region

    Private Function ValidasiDataWorkflow(ByVal RowData As DataRow) As List(Of String)
        Dim temp As New List(Of String)
        Dim msgError As New StringBuilder
        Dim errorline As New StringBuilder
        Try
            'Validasi Account Owner
            'Me.StrAccountOwnerId = RowData("AccountOwnerId").ToString
            'If objAccountOwner.Find(AddressOf FindAccountOwnerID) Is Nothing Then
            '    msgError.AppendLine(" &bull;Account Owner ID '" & Me.StrAccountOwnerId & "' is not exist <BR/>")
            '    errorline.AppendLine(" &bull;CDDLNPWorkflowUpload : Line " & DT_UploadData.Rows.IndexOf(RowData) + 2 & "<BR/>")
            'End If
            'Me.StrAccountOwnerId = RowData("AccountOwnerId").ToString
            'If objAccountOwnerID.Find(AddressOf FindAccountOwnerID) IsNot Nothing Then
            '    msgError.AppendLine(" &bull;Account Owner ID '" & Me.StrAccountOwnerId & "' has registered on previous line. Account Owner must be unique. <BR/>")
            '    errorline.AppendLine(" &bull;CDDLNPWorkflowUpload : Line " & DT_UploadData.Rows.IndexOf(RowData) + 2 & "<BR/>")
            'Else
            '    objAccountOwnerID.Add(Me.StrUserId)
            'End If

            'Validasi user RM
            For Each id As String In RowData("List_RM").ToString.Split(";")
                Me.StrUserId = id
                If objUser.Find(AddressOf FindUserID) Is Nothing Then
                    msgError.AppendLine(" &bull;User ID '" & Me.StrUserId & "' is not exist <BR/>")
                    errorline.AppendLine(" &bull;CDDLNPWorkflowUpload : Line " & DT_UploadData.Rows.IndexOf(RowData) + 2 & "<BR/>")
                Else
                    If objUserRM.Find(AddressOf FindUserRMID) IsNot Nothing Then
                        msgError.AppendLine(" &bull;User ID '" & Me.StrUserId & "' has registered on previous line. User ID in List_RM must be unique. <BR/>")
                        errorline.AppendLine(" &bull;CDDLNPWorkflowUpload : Line " & DT_UploadData.Rows.IndexOf(RowData) + 2 & "<BR/>")
                    Else
                        objUserRM.Add(Me.StrUserId)
                    End If

                End If
            Next

            'Validasi user Team Head
            For Each id As String In RowData("List_TeamHead").ToString.Split(";")
                Me.StrUserId = id
                If objUser.Find(AddressOf FindUserID) Is Nothing Then
                    msgError.AppendLine(" &bull;User ID '" & Me.StrUserId & "' is not exist <BR/>")
                    errorline.AppendLine(" &bull;CDDLNPWorkflowUpload : Line " & DT_UploadData.Rows.IndexOf(RowData) + 2 & "<BR/>")
                End If
            Next

            'Validasi user Group Head
            For Each id As String In RowData("List_GroupHead").ToString.Split(";")
                Me.StrUserId = id
                If objUser.Find(AddressOf FindUserID) Is Nothing Then
                    msgError.AppendLine(" &bull;User ID '" & Me.StrUserId & "' is not exist <BR/>")
                    errorline.AppendLine(" &bull;CDDLNPWorkflowUpload : Line " & DT_UploadData.Rows.IndexOf(RowData) + 2 & "<BR/>")
                End If
            Next

            'Validasi user Head Office
            For Each id As String In RowData("List_HeadOff").ToString.Split(";")
                Me.StrUserId = id
                If objUser.Find(AddressOf FindUserID) Is Nothing Then
                    msgError.AppendLine(" &bull;User ID '" & Me.StrUserId & "' is not exist <BR/>")
                    errorline.AppendLine(" &bull;CDDLNPWorkflowUpload : Line " & DT_UploadData.Rows.IndexOf(RowData) + 2 & "<BR/>")
                End If
            Next


            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        Catch ex As Exception
            msgError.Append(ex.Message)
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        End Try
        Return temp
    End Function

    Private Function ValidasiDataReassignment(ByVal RowData As DataRow) As List(Of String)
        Dim temp As New List(Of String)
        Dim msgError As New StringBuilder
        Dim errorline As New StringBuilder
        Try
            

            'Validasi OldUser: OldUser harus unique
            Me.StrUserId = RowData("OldUser")
            If objUserReassign.Find(AddressOf FindUserReassign) IsNot Nothing Then
                msgError.AppendLine(" &bull;User ID '" & Me.StrUserId & "' has registered on previous line. OldUser must be unique. <BR/>")
                errorline.AppendLine(" &bull;CDDLNPReassignment : Line " & DT_UploadData2.Rows.IndexOf(RowData) + 2 & "<BR/>")
            Else
                objUserReassign.Add(Me.StrUserId)
            End If

            'Validasi NewUser: NewUser harus terdaftar di system
            Me.StrUserId = RowData("NewUser")
            If objUser.Find(AddressOf FindUserID) Is Nothing Then
                msgError.AppendLine(" &bull;User ID '" & Me.StrUserId & "' is not exist <BR/>")
                errorline.AppendLine(" &bull;CDDLNPReassignment : Line " & DT_UploadData2.Rows.IndexOf(RowData) + 2 & "<BR/>")
            End If

            'If objUserReassign.Find(AddressOf FindUserReassign) Is Nothing Then
            '    msgError.AppendLine(" &bull;User ID '" & Me.StrUserId & "' registered in OldUser list. <BR/>")
            '    errorline.AppendLine(" &bull;CDDLNPReassignment : Line " & DT_UploadData2.Rows.IndexOf(RowData) + 2 & "<BR/>")
            'End If

            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        Catch ex As Exception
            msgError.Append(ex.Message)
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        End Try
        Return temp
    End Function

    Private Function ValidasiDataInfiniteReassignment(ByVal RowData As DataRow) As List(Of String)
        Dim temp As New List(Of String)
        Dim msgError As New StringBuilder
        Dim errorline As New StringBuilder
        Try

            'Validasi NewUser: NewUser tidak boleh menggunakan user yang ada di OldUser
            Me.StrUserId = RowData("NewUser")
            If objUserReassign.Find(AddressOf FindUserReassign) IsNot Nothing Then
                msgError.AppendLine(" &bull;User ID '" & Me.StrUserId & "' registered in OldUser list. <BR/>")
                errorline.AppendLine(" &bull;CDDLNPReassignment : Line " & DT_UploadData2.Rows.IndexOf(RowData) + 2 & "<BR/>")
            End If

            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        Catch ex As Exception
            msgError.Append(ex.Message)
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        End Try
        Return temp
    End Function

#Region "Upload File"
    Public Function SaveFileImportToSave() As String
        Dim filePath As String
        Dim FileNameToGetData As String = ""
        Try
            If FileUploadKPI.PostedFile.ContentLength = 0 Then
                Return FileNameToGetData
            End If

            If Not FileUploadKPI.HasFile Then
                Return FileNameToGetData
            End If

            filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName)

            FileNameToGetData = FileUploadKPI.FileName
            If System.IO.File.Exists(filePath) Then
                Dim bexist As Boolean = True
                Dim i As Integer = 1
                While bexist
                    filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & FileUploadKPI.FileName.Split(CChar("."))(1))
                    FileNameToGetData = FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & Me.FileUploadKPI.FileName.Split(CChar("."))(1)
                    If Not System.IO.File.Exists(filePath) Then
                        bexist = False
                        Exit While
                    End If
                    i = i + 1
                End While
            End If
            Me.FileUploadKPI.PostedFile.SaveAs(filePath)
            Session("Filepath") = filePath
            Return FileNameToGetData
        Catch
            Throw
        End Try
    End Function

    Private Sub ProcessExcelFile(ByVal FullPathFileNameToExportExcel As String)
        Using stream As FileStream = File.Open(FullPathFileNameToExportExcel, FileMode.Open, FileAccess.Read)
            Using excelReader As Excel.IExcelDataReader = ExcelReaderFactory.CreateBinaryReader(stream)
                excelReader.IsFirstRowAsColumnNames = True
                DS_UploadData = excelReader.AsDataSet()
                If DS_UploadData.Tables(Me.ModuleName).Columns(0).ColumnName Like "*Column*" Then
                    DS_UploadData.Tables(Me.ModuleName).Rows.Clear()
                End If
                If DS_UploadData.Tables(Me.ModuleName2).Columns(0).ColumnName Like "*Column*" Then
                    DS_UploadData.Tables(Me.ModuleName2).Rows.Clear()
                End If
            End Using
        End Using
    End Sub
#End Region

#Region "Function..."
    Private Sub ImportExcelTempToDTproperty()
        Dim DT As New Data.DataTable
        Dim DV As New DataView(DT)
        Dim InvalidRows() As DataRow

        'CDDLNPWorkflowUpload
        'Convert excel ke datatable
        DT = DS_UploadData.Tables(Me.ModuleName)
        DV = New DataView(DT)
        If DT.Rows.Count > 0 Then
            DT_UploadData = DV.ToTable(True, "AccountOwnerId", "List_RM", "List_TeamHead", "List_GroupHead", "List_HeadOff")
            'Cari datarow yang tidak valid
            For Each objCDDLNP_Workflow As DataRow In DT_UploadData.Rows
                objCDDLNP_Workflow("AccountOwnerId") = objCDDLNP_Workflow("AccountOwnerId").ToString.Replace(" ", String.Empty)
                objCDDLNP_Workflow("List_RM") = objCDDLNP_Workflow("List_RM").ToString.Replace(" ", String.Empty)
                objCDDLNP_Workflow("List_TeamHead") = objCDDLNP_Workflow("List_TeamHead").ToString.Replace(" ", String.Empty)
                objCDDLNP_Workflow("List_GroupHead") = objCDDLNP_Workflow("List_GroupHead").ToString.Replace(" ", String.Empty)
                objCDDLNP_Workflow("List_HeadOff") = objCDDLNP_Workflow("List_HeadOff").ToString.Replace(" ", String.Empty)
            Next

            'Hapus datarow yang tidak valid
            InvalidRows = DT_UploadData.Select("AccountOwnerId = '' AND List_RM = '' AND List_TeamHead = '' AND List_HeadOff = ''")
            For Each row As DataRow In InvalidRows
                DT_UploadData.Rows.Remove(row)
            Next
        Else
            DT_UploadData = DV.ToTable(True, "Column1", "Column2", "Column3", "Column4", "Column5")
        End If
        


        'CDDLNPReassignment
        'Convert excel ke datatable
        DT = DS_UploadData.Tables(Me.ModuleName2)
        DV = New DataView(DT)
        If DT.Rows.Count > 0 Then
            DT_UploadData2 = DV.ToTable(True, "OldUser", "NewUser")
            'Cari datarow yang tidak valid
            For Each objCDDLNP_UserAssigment As DataRow In DT_UploadData2.Rows
                objCDDLNP_UserAssigment("OldUser") = objCDDLNP_UserAssigment("OldUser").ToString.Replace(" ", String.Empty)
                objCDDLNP_UserAssigment("NewUser") = objCDDLNP_UserAssigment("NewUser").ToString.Replace(" ", String.Empty)
            Next

            'Hapus datarow yang tidak valid
            InvalidRows = DT_UploadData2.Select("OldUser = '' AND NewUser = ''")
            For Each row As DataRow In InvalidRows
                DT_UploadData2.Rows.Remove(row)
            Next

            'Hapus datarow yang nilai OldUser = NewUser
            InvalidRows = DT_UploadData2.Select("OldUser = NewUser")
            For Each row As DataRow In InvalidRows
                DT_UploadData2.Rows.Remove(row)
            Next
        Else
            DT_UploadData2 = DV.ToTable(True, "Column1", "Column2")
        End If
    End Sub

    Private Sub ClearThisPageSessions()
        DT_UploadData = Nothing
        GroupTableSuccessData = Nothing
        GroupTableAnomalyData = Nothing
        GroupTableDataUserAssignment = Nothing
        DS_UploadData = Nothing
        SetnGetAnomalyDataTable = Nothing
        SetnGetValidDataTable = Nothing
    End Sub

    Private Sub ChangeMultiView(ByVal index As Integer)
        MultiViewKPIOvertimeUpload.ActiveViewIndex = index
        If index = 0 Then
            ImageSave.Visible = False
            ImageCancel.Visible = False
        ElseIf index = 1 Then
            ImageCancel.Visible = True
        ElseIf index = 2 Then
            ImageCancel.Visible = False
            ImageSave.Visible = False
        End If
    End Sub

    Public Sub BindGrid()
        lblValueFailedData.Text = SetnGetAnomalyDataTable.Rows.Count - intNotListedRM
        GridAnomalyList.DataSource = SetnGetAnomalyDataTable
        GridAnomalyList.DataBind()

        lblValueSuccessData.Text = SetnGetValidDataTable.Count + GroupTableDataUserAssignment.Count
        GridSuccessList.DataSource = SetnGetValidDataTable
        GridSuccessList.DataBind()
        GridReassignment.DataSource = GroupTableDataUserAssignment
        GridReassignment.DataBind()

        lblValueuploadeddata.Text = DT_UploadData.Rows.Count + DT_UploadData2.Rows.Count
    End Sub
#End Region

#Region "events..."

    'Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
    '    ClearThisPageSessions()
    '    ChangeMultiView(0)
    '    LinkButtonExportExcel.Visible = True
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                ClearThisPageSessions()
                ChangeMultiView(0)
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageOK.Click, ImageCancel.Click
        'ChangeMultiView(0)
        Me.Response.Redirect("CDDLNPWorkflowUpload.aspx", True)
    End Sub

    Protected Sub GridSuccessList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridSuccessList.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridSuccessList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridSuccessList.PageIndexChanged, GridAnomalyList.PageIndexChanged
        Dim GridTarget As DataGrid = CType(source, DataGrid)
        Try
            GridTarget.CurrentPageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim objApproval As New CDDLNP_WorkflowUpload_Approval

        Try
            objTransManager.BeginTransaction()
            'File belum diupload
            If SetnGetValidDataTable.Count = 0 Then
                Throw New Exception("Load data failed")
            End If

            'Save Approval Header
            objApproval.RequestedBy = SessionPkUserId
            objApproval.RequestedDate = Date.Now
            objApproval.FK_MsMode_Id = 1
            DataRepository.CDDLNP_WorkflowUpload_ApprovalProvider.Save(objTransManager, objApproval)

            For Each objApprovalDetail As CDDLNP_WorkflowUpload_ApprovalDetail In GroupTableSuccessData
                objApprovalDetail.FK_CDDLNP_WorkflowUpload_Approval_Id = objApproval.PK_CDDLNP_WorkflowUpload_Approval_Id
            Next
            DataRepository.CDDLNP_WorkflowUpload_ApprovalDetailProvider.Save(objTransManager, GroupTableSuccessData)

            For Each objUserAssignment As CDDLNP_UserAssignment In GroupTableDataUserAssignment
                objUserAssignment.FK_CDDLNP_WorkflowUpload_Approval_Id = objApproval.PK_CDDLNP_WorkflowUpload_Approval_Id
            Next
            DataRepository.CDDLNP_UserAssignmentProvider.Save(objTransManager, GroupTableDataUserAssignment)

            objTransManager.Commit()
            LblConfirmation.Text = "CDDLNP Workflow data has been uploaded (" & GroupTableSuccessData.Count & " Rows) and it is currently waiting for approval"
            ChangeMultiView(2)

        Catch ex As Exception
            objTransManager.Rollback()
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
            If Not objApproval Is Nothing Then
                objApproval.Dispose()
                objApproval = Nothing
            End If
        End Try
    End Sub

    Protected Sub imgUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgUpload.Click
        Dim CombinedFileName As String
        GroupTableSuccessData = New TList(Of CDDLNP_WorkflowUpload_ApprovalDetail)
        GroupTableAnomalyData = New TList(Of CDDLNP_WorkflowUpload_ApprovalDetail)
        GroupTableDataUserAssignment = New TList(Of CDDLNP_UserAssignment)

        objUser = SahassaNettier.Data.DataRepository.UserProvider.GetAll
        Try
            'validasi File
            If FileUploadKPI.FileName = "" Then
                Throw New Exception("Data required")
            End If

            If Path.GetExtension(FileUploadKPI.FileName) <> ".xls" Then
                Throw New Exception("Invalid File Upload Extension")
            End If

            CombinedFileName = SaveFileImportToSave()
            If String.Compare(CombinedFileName, "", False) <> 0 Then
                Try
                    ProcessExcelFile(Session("FilePath"))
                Catch ex As Exception
                    Throw New Exception("Cannot find sheet '" & Me.ModuleName & "'")
                End Try
            Else
                Throw New Exception("Invalid File Format")
            End If

            'Import from Excell to property Global
            ImportExcelTempToDTproperty()

            'tambahkan kolom untuk tampilan di grid anomaly
            DT_UploadData.Columns.Add("Description")
            DT_UploadData.Columns.Add("errorline")

            'Membuat tempory untuk yang anomali
            Dim DTAnomaly As Data.DataTable = DT_UploadData.Clone

            'Buat List RM yang sudah masuk validasi
            'objAccountOwnerID = New List(Of String)
            objUserRM = New List(Of String)
            objUserReassign = New List(Of String)

            'Validasi CDDLNPWorkflowUpload
            For Each objCDDLNP_Workflow As DataRow In DT_UploadData.Rows
                Dim Lerror As List(Of String) = ValidasiDataWorkflow(objCDDLNP_Workflow)
                Dim MSG As String = Lerror(0)
                Dim LINE As String = Lerror(1)
                If MSG = String.Empty Then

                    GroupTableSuccessData.Add(CDDLNP_WorkflowUpload_ApprovalDetail.CreateCDDLNP_WorkflowUpload_ApprovalDetail( _
                            Nothing, _
                            objCDDLNP_Workflow("AccountOwnerId"), _
                            objCDDLNP_Workflow("List_RM"), _
                            objCDDLNP_Workflow("List_TeamHead"), _
                            objCDDLNP_Workflow("List_GroupHead"), _
                            objCDDLNP_Workflow("List_HeadOff")))

                Else
                    objCDDLNP_Workflow("Description") = MSG
                    objCDDLNP_Workflow("errorline") = LINE
                    DTAnomaly.ImportRow(objCDDLNP_Workflow)

                    GroupTableAnomalyData.Add(CDDLNP_WorkflowUpload_ApprovalDetail.CreateCDDLNP_WorkflowUpload_ApprovalDetail( _
                            DT_UploadData.Rows.IndexOf(objCDDLNP_Workflow) + 2, _
                            objCDDLNP_Workflow("AccountOwnerId").ToString, _
                            objCDDLNP_Workflow("List_RM").ToString, _
                            objCDDLNP_Workflow("List_TeamHead").ToString, _
                            objCDDLNP_Workflow("List_GroupHead").ToString, _
                            objCDDLNP_Workflow("List_HeadOff").ToString))
                End If
            Next

            For Each objCDDLNP_Reassignment As DataRow In DT_UploadData2.Rows
                Dim Lerror As List(Of String) = ValidasiDataReassignment(objCDDLNP_Reassignment)
                Dim MSG As String = Lerror(0)
                Dim LINE As String = Lerror(1)
                If MSG = String.Empty Then
                    GroupTableDataUserAssignment.Add(CDDLNP_UserAssignment.CreateCDDLNP_UserAssignment( _
                            Nothing, _
                            objCDDLNP_Reassignment("OldUser").ToString, _
                            objCDDLNP_Reassignment("NewUser").ToString, _
                            Nothing))

                Else
                    Dim objCDDLNP_Workflow As DataRow = DTAnomaly.NewRow
                    objCDDLNP_Workflow("Description") = MSG
                    objCDDLNP_Workflow("errorline") = LINE
                    DTAnomaly.Rows.Add(objCDDLNP_Workflow)

                End If

            Next

            For Each objCDDLNP_Reassignment As DataRow In DT_UploadData2.Rows
                Dim Lerror As List(Of String) = ValidasiDataInfiniteReassignment(objCDDLNP_Reassignment)
                Dim MSG As String = Lerror(0)
                Dim LINE As String = Lerror(1)
                If MSG = String.Empty Then
                    'Do nothing
                Else
                    Dim objCDDLNP_Workflow As DataRow = DTAnomaly.NewRow
                    objCDDLNP_Workflow("Description") = MSG
                    objCDDLNP_Workflow("errorline") = LINE
                    DTAnomaly.Rows.Add(objCDDLNP_Workflow)

                End If

            Next

            ''Cek apakah masih ada user yang dalam proses approval tetapi tidak ada dalam workflow yang baru
            'intNotListedRM = 0
            'For Each objApproval As vw_CDDLNP_Approval In DataRepository.vw_CDDLNP_ApprovalProvider.GetAll.FindAllDistinct("RequestedBy")
            '    Me.StrUserId = objApproval.RequestedBy
            '    If objUserRM.Find(AddressOf FindUserRMID) Is Nothing Then
            '        Dim strBranch As String = String.Empty
            '        Using objExistingWorkflow As TList(Of CDDLNP_WorkflowUpload) = DataRepository.CDDLNP_WorkflowUploadProvider.GetPaged("List_RM LIKE '%" & Me.StrUserId & "%'")
            '            If objExistingWorkflow.Count > 0 Then
            '                strBranch = objExistingWorkflow(0).AccountOwnerId
            '            End If
            '        End Using

            '        Dim objCDDLNP_Workflow As DataRow = DTAnomaly.NewRow
            '        objCDDLNP_Workflow("Description") = " &bull;User ID '" & Me.StrUserId & "' (branch " & strBranch & ") has approval on progress but not register in the uploaded workflow <BR/>"
            '        objCDDLNP_Workflow("errorline") = ""
            '        DTAnomaly.Rows.Add(objCDDLNP_Workflow)
            '        intNotListedRM += 1
            '    End If
            'Next

            intNotListedRM = 0
            For Each objCDDLNP As CDDLNP In DataRepository.CDDLNPProvider.GetAll.FindAllDistinct("CreatedBy")
                Me.StrUserId = objCDDLNP.CreatedBy
                If objUserRM.Find(AddressOf FindUserRMID) Is Nothing Then 'Jika tidak ditemukan list workflowupload

                    If objUserReassign.Find(AddressOf FindUserReassign) Is Nothing Then 'Jika tidak ditemukan di list reassignment
                        Dim strBranch As String = String.Empty 'Ambil branch
                        Using objExistingWorkflow As TList(Of CDDLNP_WorkflowUpload) = DataRepository.CDDLNP_WorkflowUploadProvider.GetPaged("List_RM LIKE '%" & Me.StrUserId & "%'", String.Empty, 0, 1, 0)
                            If objExistingWorkflow.Count > 0 Then
                                strBranch = objExistingWorkflow(0).AccountOwnerId
                            End If
                        End Using

                        Dim objCDDLNP_Workflow As DataRow = DTAnomaly.NewRow
                        objCDDLNP_Workflow("Description") = " &bull;User ID '" & Me.StrUserId & "' (branch " & strBranch & ") is assigned on CDDLNP but not register in the uploaded workflow. Please fix the workflow or fill CDDLNPreassignment if the user need to be reassign to another user<BR/>"
                        objCDDLNP_Workflow("errorline") = ""
                        DTAnomaly.Rows.Add(objCDDLNP_Workflow)
                        intNotListedRM += 1

                    End If

                End If
            Next

            SetnGetValidDataTable = GroupTableSuccessData
            SetnGetAnomalyDataTable = DTAnomaly

            If SetnGetAnomalyDataTable.Rows.Count > 0 Then
                ImageSave.Visible = False
            Else
                ImageSave.Visible = True
            End If

            BindGrid()

            If GridAnomalyList.Items.Count < 1 Then
                tableanomaly.Visible = False
            Else
                tableanomaly.Visible = True
            End If
            ChangeMultiView(1)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Response.Clear()
            Response.ClearHeaders()
            Response.Buffer = False
            Response.BufferOutput = True

            Response.AddHeader("Pragma", "public")
            Response.Expires = 0

            Response.AddHeader("Cache-Control", "no-cache, must-revalidate")
            Response.AddHeader("Cache-Control", "public")
            Response.AddHeader("Content-Description", "File Transfer")
            Response.AddHeader("cache-control", "max-age=0")

            Response.AddHeader("content-disposition", "attachment; filename=CDDLNPWorkflowAnomaly.csv")
            Response.AddHeader("Content-Transfer-Encoding", "binary")
            Response.Charset = ""

            Response.ContentType = "text/plain"
            Me.EnableViewState = False

            Context.Response.Write("""Line"",")
            Context.Response.Write("""AccountOwnerId"",")
            Context.Response.Write("""List_RM"",")
            Context.Response.Write("""List_TeamHead"",")
            Context.Response.Write("""List_GroupHead"",")
            Context.Response.Write("""List_HeadOff""" & vbCrLf)
            For Each RowAnomaly As CDDLNP_WorkflowUpload_ApprovalDetail In GroupTableAnomalyData
                Context.Response.Write("""" & RowAnomaly.FK_CDDLNP_WorkflowUpload_Approval_Id & """,")
                Context.Response.Write("""" & RowAnomaly.AccountOwnerId & """,")
                Context.Response.Write("""" & RowAnomaly.List_RM & """,")
                Context.Response.Write("""" & RowAnomaly.List_TeamHead & """,")
                Context.Response.Write("""" & RowAnomaly.List_GroupHead & """,")
                Context.Response.Write("""" & RowAnomaly.List_HeadOff & """" & vbCrLf)
            Next

        Catch ex As Exception
            Context.Response.Write(vbCrLf & vbCrLf)
            Context.Response.Write("""There is error while exporting data - """ & ex.Message & vbCrLf)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            Response.End()
        End Try
    End Sub

    Private Sub InitializeWorkbook(ByVal strPath As String)
        Dim objFs As New FileStream(strPath, FileMode.Open, FileAccess.Read)
        hssfworkbook = New HSSFWorkbook(objFs)
        'Create a entry of DocumentSummaryInformation
        Dim dsi As DocumentSummaryInformation = PropertySetFactory.CreateDocumentSummaryInformation()
        dsi.Company = "PT Bank CIMB Niaga"
        hssfworkbook.DocumentSummaryInformation = dsi
        'Create a entry of SummaryInformation
        Dim si As SummaryInformation = PropertySetFactory.CreateSummaryInformation()
        si.Subject = "CDDLNPWorkflowUpload"
        hssfworkbook.SummaryInformation = si
    End Sub

    Private Sub Create(ByVal Path As String, ByRef returnPath As String)
        InitializeWorkbook(Path)
        Dim sheet1 As Sheet = hssfworkbook.GetSheet("CDDLNPWorkflowUpload")
        Dim rows As Row
        Dim cell As Cell

        Using objWorkflow As TList(Of CDDLNP_WorkflowUpload) = DataRepository.CDDLNP_WorkflowUploadProvider.GetAll
            If objWorkflow.Count > 0 Then
                For i As Int32 = 0 To objWorkflow.Count - 1
                    If sheet1.GetRow(i + 1) Is Nothing Then
                        rows = sheet1.CreateRow(i + 1)
                    Else
                        rows = sheet1.GetRow(i + 1)
                    End If

                    Dim CDDLNPWorkflowUpload() As String = {objWorkflow(i).AccountOwnerId, objWorkflow(i).List_RM.TrimEnd(";"), objWorkflow(i).List_TeamHead.TrimEnd(";"), objWorkflow(i).List_GroupHead.TrimEnd(";"), objWorkflow(i).List_HeadOff.TrimEnd(";")}
                    For j As Integer = 0 To 4
                        If rows.GetCell(j) Is Nothing Then
                            cell = rows.CreateCell(j)
                        Else
                            cell = rows.GetCell(j)
                        End If
                        cell.SetCellValue(CDDLNPWorkflowUpload(j))
                    Next
                Next
            End If
        End Using

        Dim strNewTemplate As String = System.IO.Path.GetTempFileName()
        Dim NewFile As String = strNewTemplate.Replace(IO.Path.GetExtension(strNewTemplate), ".xls")
        returnPath = NewFile
        WriteToFile(returnPath)
    End Sub

    Private Sub WriteToFile(ByVal returnPath As String)
        'Write the stream data of workbook to the root directory
        Dim file As New FileStream(returnPath, FileMode.Create)
        hssfworkbook.Write(file)
        file.Close()
    End Sub

    Protected Sub LnkDownloadTemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkDownloadTemplate.Click
        Try
            Dim strReturnPath As String = String.Empty
            Me.Create(Server.MapPath("~\FolderTemplate\CDDLNPWorkflowTemplate.xls"), strReturnPath)

            Dim fileContents As Byte()
            fileContents = My.Computer.FileSystem.ReadAllBytes(strReturnPath)

            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CDDLNPWorkflowUpload.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            Response.ContentType = "application/vnd.xls"
            Response.BinaryWrite(fileContents)
            Response.End()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class