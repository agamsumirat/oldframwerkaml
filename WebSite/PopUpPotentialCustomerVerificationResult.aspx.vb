'Imports SahassaNettier.Data
'Imports SahassaNettier.Entities
'Imports AMLBLL.CDDLNPBLL

Partial Class PotentialCustomerVerificationResult
    Inherits Parent

    Public ReadOnly Property GetPkSearch As Int64
        Get
            Return Me.Request.Params("ScreeningCustomerRequestId")
        End Get
    End Property

    Public Property ScreeningCustomerPK As Int64
        Get
           Return Session("ScreeningCustomerPK")
        End Get
        Set
            Session("ScreeningCustomerPK") = Value
        End Set
    End Property

    ''' <summary>
    ''' Load Data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            BindGrid()

            If GrdResult.Rows.Count > 0 Then
                lblSearching.Text = ""
                LoadDataSearching()
                'Response.Redirect("PotentialCustomerVerification.aspx", False)

                'MagicAjax.AjaxCallHelper.Write("AJAXCbo.ClearIntervalForAjaxCall();")



                'MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(Integer.MaxValue)
                Response.AppendHeader("Refresh", -1)


            Else
                lblSearching.Text = "Please Wait Until Data Loading Finish..."
                LoadDataSearching()
                ClearWatchlist()

                Response.AppendHeader("Refresh", 5)


                'MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(5000)
            End If

            'Hanya simpan jika PK diatas 0
            'Takutnya user membuka page result tanpa melakukan pencarian terlebih dahulu
            'Jika demikian, maka PPK akan menjadi 0
            If GetPkSearch > 0 Then
                ScreeningCustomerPK = GetPkSearch
            End If

            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
            End Using
            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' BindGrid
    ''' </summary>
    ''' <remarks></remarks>
    'Private Sub BindGrid()
    '    Try
    '        If SessionCDDLNPAuditTrailId = 0 Then
    '            Dim Dt As New Data.DataTable
    '            Dim p As New Sahassa.AML.PotentialCustomerVerification
    '            Dt = p.DoPotentialCustomerVerification(CInt(Me.GetPkSearch)).Tables(0)
    '            Dim ArrAlias, ArrAddress, ArrIdNumber As ArrayList
    '            ArrAlias = Session("PotentialCustomerName")
    '            ArrAddress = Session("PotentialCustomerAddress")
    '            ArrIdNumber = Session("PotentialCustomerIdNumber")

    '            Using objaudittrail As New SahassaNettier.Entities.AuditTrail_Potensial_Screening_Customer
    '                With objaudittrail

    '                    .UserID = Sahassa.AML.Commonly.SessionUserId
    '                    Using objuser As SahassaNettier.Entities.User = SahassaNettier.Data.DataRepository.UserProvider.GetBypkUserID(Sahassa.AML.Commonly.SessionPkUserId)
    '                        If Not objuser Is Nothing Then
    '                            .UserName = objuser.UserName
    '                        End If
    '                    End Using

    '                    .ScreeningDate = Now

    '                    If ArrAlias.Count > 0 Then
    '                        .Nama = ArrAlias(0)
    '                    End If

    '                    For i As Integer = 0 To ArrAlias.Count - 1

    '                    Next

    '                    If ArrAddress.Count > 0 Then
    '                        .[Address] = ArrAddress(0)
    '                    End If

    '                    If ArrIdNumber.Count > 0 Then
    '                        .IDNumber = ArrIdNumber(0)
    '                    End If
    '                    .SuspectMatch = Dt.Rows.Count

    '                    Dim dob As String = CStr(Session("DOB"))
    '                    If Equals(dob, "") Then
    '                        dob = ""
    '                    Else
    '                        dob = CDate(Session("DOB")).ToString("dd-MMMM-yyyy")
    '                    End If
    '                    If IsDate(dob) Then
    '                        .DateOfBirthDay = dob
    '                    End If
    '                End With
    '                SahassaNettier.Data.DataRepository.AuditTrail_Potensial_Screening_CustomerProvider.Save(objaudittrail)
    '                SessionCDDLNPAuditTrailId = objaudittrail.PK_AuditTrail_Potensial_Screening_Customer_ID

    '                If Dt.Rows.Count > 0 Then
    '                    Using objdetail As New TList(Of AuditTrail_Potensial_Screening_CustomerDetail)
    '                        For Each item As Data.DataRow In Dt.Rows
    '                            Using objnew As AuditTrail_Potensial_Screening_CustomerDetail = objdetail.AddNew()

    '                                objnew.FK_AuditTrail_Potensial_Screening_Customer_ID = objaudittrail.PK_AuditTrail_Potensial_Screening_Customer_ID

    '                                Using objnewverification As VerificationList_Master = DataRepository.VerificationList_MasterProvider.GetByVerificationListId(item("VerificationListId"))
    '                                    If Not objnewverification Is Nothing Then

    '                                        objnew.ResultDisplayName = objnewverification.DisplayName
    '                                        objnew.PercentMatch = item("MatchPercentage")
    '                                        objnew.DateOfBirth = objnewverification.DateOfBirth

    '                                        Using objaddress As TList(Of VerificationList_Address) = DataRepository.VerificationList_AddressProvider.GetPaged(VerificationList_AddressColumn.VerificationListId.ToString & "=" & item("VerificationListId"), "", 0, Integer.MaxValue, 0)
    '                                            If objaddress.Count > 0 Then
    '                                                objnew.Address = objaddress(0).Address
    '                                            End If
    '                                        End Using

    '                                        Using objid As TList(Of VerificationList_IDNumber) = DataRepository.VerificationList_IDNumberProvider.GetPaged(VerificationList_IDNumberColumn.VerificationListId.ToString & "=" & item("VerificationListId"), "", 0, Integer.MaxValue, 0)
    '                                            If objid.Count > 0 Then
    '                                                objnew.IdentityNumber = objid(0).IDNumber
    '                                            End If

    '                                        End Using

    '                                        objnew.ListType = AMLBLL.VerificationListBLL.GetListName(objnewverification.VerificationListTypeId)
    '                                        objnew.ListCategory = AMLBLL.VerificationListBLL.GetListCategory(objnewverification.VerificationListCategoryId)
    '                                        objnew.CustomRemarks1 = objnewverification.CustomRemark1
    '                                        objnew.CustomRemarks2 = objnewverification.CustomRemark2
    '                                        objnew.CustomRemarks3 = objnewverification.CustomRemark3
    '                                        objnew.CustomRemarks4 = objnewverification.CustomRemark4
    '                                        objnew.CustomRemarks5 = objnewverification.CustomRemark5
    '                                        objnew.DateOfData = objnewverification.DateOfData
    '                                    End If

    '                                End Using

    '                            End Using
    '                        Next

    '                        DataRepository.AuditTrail_Potensial_Screening_CustomerDetailProvider.Save(objdetail)
    '                    End Using
    '                End If
    '            End Using
    '        End If

    '        Dim ObjAuditDetail As TList(Of AuditTrail_Potensial_Screening_CustomerDetail) = DataRepository.AuditTrail_Potensial_Screening_CustomerDetailProvider.GetPaged("FK_AuditTrail_Potensial_Screening_Customer_ID = " & SessionCDDLNPAuditTrailId, "", 0, Int32.MaxValue, 0)
    '        If ObjAuditDetail.Count = 0 Then
    '            Me.tableNotFound.Visible = True
    '            Me.tableFound.Visible = False
    '        Else
    '            Me.tableNotFound.Visible = False
    '            Me.tableFound.Visible = True
    '            Me.GridPotentialCustomerVerification.PageSize = ObjAuditDetail.Count
    '            Me.GridPotentialCustomerVerification.DataSource = ObjAuditDetail 'p.DoPotentialCustomerVerification(CInt(Me.GetPkSearch)).Tables(0)
    '            Me.GridPotentialCustomerVerification.DataBind()
    '        End If
    '    Catch
    '        Throw
    '    End Try
    'End Sub
    ''' <summary>
    ''' Detail
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    'Protected Sub GridPotentialCustomerVerification_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridPotentialCustomerVerification.EditCommand
    '    Try
    '        Me.LblDateOfBirthSuspect.Text = ""
    '        Me.LblBirthPlace.Text = ""
    '        Me.LblListTypeSuspect.Text = ""
    '        Me.LblListCategorySuspect.Text = ""
    '        Me.TxtCustomRemark1.Text = ""
    '        Me.TxtCustomRemark2.Text = ""
    '        Me.TxtCustomRemark3.Text = ""
    '        Me.TxtCustomRemark4.Text = ""
    '        Me.TxtCustomRemark5.Text = ""
    '        Me.ListAddressMatch.Items.Clear()
    '        Me.ListAliasMatch.Items.Clear()
    '        Me.ListIdentityNumberMatch.Items.Clear()
    '        Me.ShowDetail.Visible = True

    '        Using AccessDetailPotentialCustomerVerification As New AMLDAL.PotentialCustomerVerificationTableAdapters.SelectPotentialCustomerVerificationMatchDetailTableAdapter
    '            Using dtDetailPotentialCustomerVerification As Data.DataTable = AccessDetailPotentialCustomerVerification.GetData(CInt(e.Item.Cells(0).Text))
    '                Dim RowDetailPotentialCustomerVerification As AMLDAL.PotentialCustomerVerification.SelectPotentialCustomerVerificationMatchDetailRow = dtDetailPotentialCustomerVerification.Rows(0)
    '                Dim dob As String = CStr(RowDetailPotentialCustomerVerification.DateOfBirth)
    '                If Equals(dob, "") Then
    '                    dob = ""
    '                Else
    '                    dob = CDate(RowDetailPotentialCustomerVerification.DateOfBirth).ToString("dd-MMMM-yyyy")
    '                End If
    '                Me.LblDateOfBirthSuspect.Text = dob
    '                LblBirthPlace.Text = RowDetailPotentialCustomerVerification.BirthPlace
    '                Me.LblListTypeSuspect.Text = RowDetailPotentialCustomerVerification.ListTypeName
    '                Me.LblListCategorySuspect.Text = RowDetailPotentialCustomerVerification.CategoryName
    '                Me.TxtCustomRemark1.Text = RowDetailPotentialCustomerVerification.CustomRemark1
    '                Me.TxtCustomRemark2.Text = RowDetailPotentialCustomerVerification.CustomRemark2
    '                Me.TxtCustomRemark3.Text = RowDetailPotentialCustomerVerification.CustomRemark3
    '                Me.TxtCustomRemark4.Text = RowDetailPotentialCustomerVerification.CustomRemark4
    '                Me.TxtCustomRemark5.Text = RowDetailPotentialCustomerVerification.CustomRemark5
    '            End Using
    '        End Using
    '        ' Address
    '        Using AccessDetailAddressPotentialCustomerVerification As New AMLDAL.PotentialCustomerVerificationTableAdapters.SelectAddressPotentialCustomerMatchTableAdapter
    '            Using dtDetailAddressPotentialCustomerVerification As Data.DataTable = AccessDetailAddressPotentialCustomerVerification.GetData(CInt(e.Item.Cells(0).Text))
    '                For Each RowDetailAddressPotentialCustomerVerification As AMLDAL.PotentialCustomerVerification.SelectAddressPotentialCustomerMatchRow In dtDetailAddressPotentialCustomerVerification.Rows
    '                    Me.ListAddressMatch.Items.Add(RowDetailAddressPotentialCustomerVerification.Address)
    '                Next
    '            End Using
    '        End Using
    '        ' Alias
    '        Using AccessDetailAliasPotentialCustomerVerification As New AMLDAL.PotentialCustomerVerificationTableAdapters.SelectVerificationList_AliasTableAdapter
    '            Using dtDetailAliasPotentialCustomerVerification As Data.DataTable = AccessDetailAliasPotentialCustomerVerification.GetData(CInt(e.Item.Cells(0).Text))
    '                For Each RowDetailAliasPotentialCustomerVerification As AMLDAL.PotentialCustomerVerification.SelectVerificationList_AliasRow In dtDetailAliasPotentialCustomerVerification.Rows
    '                    Me.ListAliasMatch.Items.Add(RowDetailAliasPotentialCustomerVerification.Name)
    '                Next
    '            End Using
    '        End Using
    '        ' Id Number
    '        Using AccessDetailIdNumberPotentialCustomerVerification As New AMLDAL.PotentialCustomerVerificationTableAdapters.SelectIDNumberPotentialCustomerMatchTableAdapter
    '            Using dtDetailIdNumberPotentialCustomerVerification As Data.DataTable = AccessDetailIdNumberPotentialCustomerVerification.GetData(CInt(e.Item.Cells(0).Text))
    '                For Each RowDetailIdNumberPotentialCustomerVerification As AMLDAL.PotentialCustomerVerification.SelectIDNumberPotentialCustomerMatchRow In dtDetailIdNumberPotentialCustomerVerification.Rows
    '                    Me.ListIdentityNumberMatch.Items.Add(RowDetailIdNumberPotentialCustomerVerification.IDNumber)
    '                Next
    '            End Using
    '        End Using
    '    Catch ex As Exception
    '        Me.cvalPageError.IsValid = False
    '        Me.cvalPageError.ErrorMessage = ex.Message
    '        LogError(ex)
    '    End Try
    'End Sub

    Sub BindGrid()

        Dim currentPK As Long

        If GetPkSearch > 0 Then
            currentPK = GetPkSearch
        Else
            currentPK = ScreeningCustomerPK
        End If

        'Isi SessionCDDLNPAuditTrailId dengan PK
        'Sebenarnya ini diisi dengan PK untuk tabel audit trail potential
        'Namun karena sudah tidak dipakai lagi (sekarang memakai table AML_Screening_Customer_Request_Result), maka kita ambil saja PK nya
        'Hanya untuk memancing saja, karena di kode asalnya, jika nilainya 0 maka RadioGroup tetap di disable (tidak bisa diklik, yang berisi "No Match", "True Match", dan "False Match")
        'Jadi isi apa saja yang penting angka dan bukan 0
        'SessionCDDLNPAuditTrailId akan digunakan di fungsi HideUncheck() pada CDDLNPBLL
        AMLBLL.CDDLNPBLL.SessionCDDLNPAuditTrailId = currentPK

        'GrdResult.DataSource = EKABLL.ScreeningBLL.GetResult(GetPkSearch)
        GrdResult.DataSource = EKABLL.ScreeningBLL.GetResult(currentPK)
        GrdResult.DataBind()
        
    End Sub

    Private Sub LoadDataSearching()

        Dim valuePK As Long = GetPkSearch

        If Not valuePK < 0 Then
            Using objresult As EkaDataNettier.Entities.AML_Screening_Customer_Request_Detail = EKABLL.ScreeningBLL.GetScreeningResultByPk(valuePK)
                If Not objresult Is Nothing Then
                    LblName.Text = objresult.Nama
                    If objresult.DOB.HasValue Then
                        LblDOB.Text = objresult.DOB.GetValueOrDefault.ToString("dd-MMM-yyyy")
                    Else
                        LblDOB.Text = ""
                    End If

                    LblNationality.Text = objresult.Nationality
                End If

            End Using
        End If

    End Sub

    Private Sub ClearWatchlist()
        LblBirthPlaceWatchList.Text = ""
        LblDOBWatchList.Text = ""
        LblListTypeWatchList.Text = ""
        LblNameWatchList.Text = ""
        LblNationalityWatchList.Text = ""
        LblYOBWatchlist.Text = ""
        LblCustomRemarks1.Text = ""
        LblCustomRemarks2.Text = ""
        LblCustomRemarks3.Text = ""
        LblCustomRemarks4.Text = ""
        LblCustomRemarks5.Text = ""
        LblCategoryType.Text = ""
        linkcategory.Text = ""
    End Sub

    Private Sub GrdResult_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GrdResult.SelectedIndexChanged
        Try
            Dim resultid As String = GrdResult.SelectedRow.Cells(2).Text
            'MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(0)

            Response.AppendHeader("Refresh", -1)

            If Integer.TryParse(resultid, 0) Then
                LoadWatchlist(resultid)
            End If


        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GrdResult_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GrdResult.PageIndexChanging
        Try
            GrdResult.PageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Sub LoadWatchlist(intverificationlist As Long)
        Dim objdt As Data.DataTable = EKABLL.ScreeningBLL.GetWatchListData(intverificationlist)
        If Not objdt Is Nothing Then
            If objdt.Rows.Count > 0 Then


                LblBirthPlaceWatchList.Text = objdt.Rows(0).Item("BirthPlace").ToString

                Dim strdob As String = ""
                Try
                    strdob = Convert.ToDateTime(objdt.Rows(0).Item("DateOfBirth")).ToString("dd-MMM-yyyy")
                Catch ex As Exception
                    strdob = objdt.Rows(0).Item("DateOfBirth").ToString
                End Try

                LblDOBWatchList.Text = strdob

                LblListTypeWatchList.Text = objdt.Rows(0).Item("ListTypeName").ToString
                LblCategoryType.Text = objdt.Rows(0).Item("CategoryName").ToString
                linkcategory.Text = objdt.Rows(0).Item("CategoryName").ToString
                LblNameWatchList.Text = objdt.Rows(0).Item("DisplayName").ToString
                LblNationalityWatchList.Text = objdt.Rows(0).Item("Nationality").ToString
                LblYOBWatchlist.Text = objdt.Rows(0).Item("YOB").ToString
                LblCustomRemarks1.Text = objdt.Rows(0).Item("CustomRemark1").ToString
                LblCustomRemarks2.Text = objdt.Rows(0).Item("CustomRemark2").ToString
                LblCustomRemarks3.Text = objdt.Rows(0).Item("CustomRemark3").ToString
                LblCustomRemarks4.Text = objdt.Rows(0).Item("CustomRemark4").ToString
                LblCustomRemarks5.Text = objdt.Rows(0).Item("CustomRemark5").ToString
                Dim intVerificationCategoryid As Integer = objdt.Rows(0).Item("VerificationListCategoryId").ToString

                Dim intcategoryworldcheck As String = EKABLL.ScreeningBLL.GetSystemParameterByValueByID(4000)
                Dim strreffid As String = objdt.Rows(0).Item("RefId").ToString
                If intVerificationCategoryid = intcategoryworldcheck Then
                    linkcategory.Visible = True
                    LblCategoryType.Visible = False
                Else
                    linkcategory.Visible = False
                    LblCategoryType.Visible = True
                End If

                linkcategory.OnClientClick = "window.open('showwatchlist.aspx?uid=" & strreffid & "', 'ca', 'width=500,height=250,left=500,top=500,resizable=yes,scrollbars=yes')"
            Else
                LblBirthPlaceWatchList.Text = ""
                LblDOBWatchList.Text = ""
                LblListTypeWatchList.Text = ""
                LblNameWatchList.Text = ""
                LblNationalityWatchList.Text = ""
                LblYOBWatchlist.Text = ""
                LblCustomRemarks1.Text = ""
                LblCustomRemarks2.Text = ""
                LblCustomRemarks3.Text = ""
                LblCustomRemarks4.Text = ""
                LblCustomRemarks5.Text = ""
                LblCategoryType.Text = ""
                linkcategory.Text = ""

            End If
        End If

    End Sub
    
End Class
