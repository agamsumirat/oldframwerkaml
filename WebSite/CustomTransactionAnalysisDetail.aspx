<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CustomTransactionAnalysisDetail.aspx.vb" Inherits="CustomTransactionAnalysisDetail" title="Custom Transaction Analysis Detail" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
   <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Custom Transaction Analysis - Detail
                </strong>
            </td>
        </tr>
    </table>
    <TABLE id="SearchBar" runat="server" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="99%" bgColor="#dddddd"
		border="2">
        <TR>
            <TD align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></TD>
				<TD vAlign="top" width="98%" bgColor="#ffffff"><TABLE cellSpacing="4" cellPadding="0" width="100%" border="0">
						<TR>
							<TD class="Regtext" noWrap>Search By :
							</TD>
							<TD vAlign="middle" noWrap><ajax:ajaxpanel id="AjaxPanel1" runat="server">
                                <asp:dropdownlist id="ComboSearch" tabIndex="1" runat="server" Width="120px" CssClass="searcheditcbo"></asp:dropdownlist>&nbsp; 
                                <asp:textbox id="TextSearch" tabIndex="2" runat="server" CssClass="searcheditbox"></asp:textbox><input id="popUp" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                    border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                    border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif); width: 16px;" title="Click to show calendar"
                                    type="button" /></ajax:ajaxpanel></TD>
							<TD vAlign="middle" width="99%"><ajax:ajaxpanel id="AjaxPanel2" runat="server">
									<asp:imagebutton id="ImageButtonSearch" tabIndex="3" runat="server" SkinID="SearchButton"></asp:imagebutton></ajax:ajaxpanel>
								</TD>
						</TR>
					</TABLE><ajax:ajaxpanel id="AjaxPanel3" runat="server">
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:ajaxpanel></TD>
			</TR>
	</TABLE>
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="99%" bgColor="#dddddd"
		border="2">
		<tr>
			<td bgColor="#ffffff">
			    <ajax:ajaxpanel id="AjaxPanel4" runat="server">
					<asp:datagrid id="GridMSUserView" runat="server" AutoGenerateColumns="False"
						Font-Size="XX-Small" BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None"
						AllowPaging="True" width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE"
						ForeColor="Black">
						<FooterStyle BackColor="#CCCC99"></FooterStyle>
						<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
						<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
						<ItemStyle BackColor="#F7F7DE"></ItemStyle>
						<HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B"></HeaderStyle>
						<Columns>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="TransactionDetailId" HeaderText="TransactionDetailId" Visible="False" >
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionDate" HeaderText="Transaction Date" SortExpression="TransactionDate desc" DataFormatString="{0:dd-MMM-yyyy}">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountOwner" HeaderText="AccountOwner" SortExpression="(CAST(TransactionDetail.AccountOwnerId AS VARCHAR)  + ' - ' + AccountOwner.AccountOwnerName) desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionChannelTypeName" HeaderText="TransactionChannelType" SortExpression="TransactionChannelTypeName desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Country" HeaderText="Country" SortExpression="TransactionDetail.CountryCode + ' - ' + ISNULL(JHCOUN.JHCNAM,'') desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BankCode" HeaderText="BankCode" SortExpression="BankCode desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BankName" HeaderText="Bank Name" SortExpression="BankName desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CIFNo" HeaderText="CIF No" SortExpression="CIFNo desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountNo" HeaderText="Account Number" SortExpression="AccountNo Desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountName" HeaderText="Account Name" SortExpression="AccountName desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AuxiliaryTransactionCode" HeaderText="AuxiliaryTransactionCode"
                                SortExpression="TransactionDetail.AuxiliaryTransactionCode + ' - ' + vw_AuxTransactionCode.TransactionDescription desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionAmount" HeaderText="Transaction Amount" SortExpression="TransactionAmount desc" DataFormatString="{0:#,#.00}">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CurrencyType" HeaderText="CurrencyType" SortExpression="CurrencyType desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="DebitOrCredit" HeaderText="Debit / Credit" SortExpression="DebitOrCredit desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MemoRemark" HeaderText="Memo Remark" SortExpression="MemoRemark desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionRemark" HeaderText="Transaction Remark" SortExpression="TransactionRemark desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionExchangeRate" DataFormatString="{0:#,#.00}"
                                HeaderText="TransactionExchangeRate" SortExpression="TransactionExchangeRate desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionLocalEquivalent" DataFormatString="{0:#,#.00}"
                                HeaderText="TransactionLocalEquivalent" SortExpression="TransactionLocalEquivalent desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionTicketNo" HeaderText="Transaction Ticket #" SortExpression="TransactionTicketNo desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="UserId" HeaderText="UserId" SortExpression="UserId desc">
                            </asp:BoundColumn>
						</Columns>
						<PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages"></PagerStyle>
					</asp:datagrid>
                </ajax:ajaxpanel>
            </td>
		</tr>
		<tr>
		    <td style="background-color:#ffffff"><table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td nowrap><ajax:ajaxpanel id="AjaxPanel5" runat="server">&nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
			  &nbsp; </ajax:ajaxpanel> </td>
			 <td width="99%">&nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" runat="server">Export 
		        to Excel</asp:LinkButton>&nbsp;&nbsp;
                 <asp:LinkButton ID="LnkBtnExportAllToExcel" runat="server">Export All to Excel</asp:LinkButton></td>
			<td align="right" nowrap>
                <asp:LinkButton ID="LinkAddNewCase" runat="server">Add New Case for Selected Item</asp:LinkButton>
			&nbsp;&nbsp;</td>
		</tr>
      </table></td>
		</tr>
		<tr>
			<td bgColor="#ffffff">
				<TABLE id="Table3" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
					<TR class="regtext" align="center" bgColor="#dddddd">
						<TD vAlign="top" align="left" width="50%" bgColor="#ffffff">Page&nbsp;<ajax:ajaxpanel id="AjaxPanel6" runat="server"><asp:label id="PageCurrentPage" runat="server" CssClass="regtext">0</asp:label>&nbsp;of&nbsp;
							<asp:label id="PageTotalPages" runat="server" CssClass="regtext">0</asp:label></ajax:ajaxpanel></TD>
						<TD vAlign="top" align="right" width="50%" bgColor="#ffffff">Total Records&nbsp;
							<ajax:ajaxpanel id="AjaxPanel7" runat="server"><asp:label id="PageTotalRows" runat="server">0</asp:label></ajax:ajaxpanel></TD>
					</TR>
				</TABLE>
				<TABLE id="Table4" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
					<TR bgColor="#ffffff">
						<TD class="regtext" vAlign="middle" align="left" colSpan="11" height="7">
							<HR color="#f40101" noShade SIZE="1">
						</TD>
					</TR>
					<TR>
						<TD class="regtext" vAlign="middle" align="left" width="63" bgColor="#ffffff">Go to 
							page</TD>
						<TD class="regtext" vAlign="middle" align="left" width="5" bgColor="#ffffff"><FONT face="Verdana, Arial, Helvetica, sans-serif" size="1">
									<ajax:ajaxpanel id="AjaxPanel8" runat="server"><asp:textbox id="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:textbox></ajax:ajaxpanel>
								</FONT></TD>
						<TD class="regtext" vAlign="middle" align="left" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel9" runat="server">
								<asp:imagebutton id="ImageButtonGo" runat="server" SkinID="GoButton"></asp:imagebutton></ajax:ajaxpanel>
							</TD>
						<TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/first.gif" width="6">
						</TD>
						<TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel10" runat="server">
								<asp:linkbutton id="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First" OnCommand="PageNavigate">First</asp:linkbutton></ajax:ajaxpanel>
							</TD>
						<TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><IMG height="5" src="images/prev.gif" width="6"></TD>
						<TD class="regtext" vAlign="middle" align="right" width="14" bgColor="#ffffff">
						<ajax:ajaxpanel id="AjaxPanel11" runat="server">
								<asp:linkbutton id="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev" OnCommand="PageNavigate">Previous</asp:linkbutton></ajax:ajaxpanel>
							</TD>
						<TD class="regtext" vAlign="middle" align="right" width="60" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel12" runat="server"><A class="pageNav" href="#">
									<asp:linkbutton id="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next" OnCommand="PageNavigate">Next</asp:linkbutton></A></ajax:ajaxpanel></TD>
						<TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/next.gif" width="6"></TD>
						<TD class="regtext" vAlign="middle" align="left" width="25" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel13" runat="server">
								<asp:linkbutton id="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last" OnCommand="PageNavigate">Last</asp:linkbutton></ajax:ajaxpanel>
							</TD>
						<TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/last.gif" width="6"></TD>
					</TR>
				</TABLE>
			</td>
		</tr>
	</table>
</asp:Content>