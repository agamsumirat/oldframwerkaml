<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="MappingUserIdHiportJiveEdit.aspx.vb" Inherits="MappingUserIdHiportJiveEdit"
     %>
    
<%@ Register Src="webcontrol/PopUpUser.ascx" TagName="PopUpUser" TagPrefix="uc1" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
<ajax:AjaxPanel ID="AjaxPanel2" runat="server" >
    <uc1:PopUpUser ID="PopUpUser1" runat="server" />
</ajax:AjaxPanel> 
    <ajax:AjaxPanel runat="server" ID="Ajax1">
        <table id="Table1" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
            height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
            border-bottom-style: none" width="100%">
            <tr>
                <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                    border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                    <strong>Mapping UserID Hiport Jive - Edit&nbsp;
                        <hr />
                    </strong>
                    <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                        Width="100%"></asp:Label>
                </td>
            </tr>
        </table>
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="view1" runat="server">
                <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                    height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
                    border-bottom-style: none" width="100%">
                    <tr class="formText">
                        <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                            border-left-style: none; height: 24px; border-bottom-style: none">
                            <span style="color: #ff0000">* Required</span>
                        </td>
                    </tr>
                </table>
                <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                    border="2" height="72" style="border-top-style: none; border-right-style: none;
                    border-left-style: none; border-bottom-style: none">
                    <tr class="formText">
                        <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                        </td>
                        <td width="20%" bgcolor="#ffffff" style="height: 24px">
                            User ID</td>
                        <td width="5" bgcolor="#ffffff" style="height: 24px">
                            :
                        </td>
                        <td width="80%" bgcolor="#ffffff" style="height: 24px">
                            <strong><span>
                            <asp:HiddenField ID="HUserID" runat="server" />
                            <asp:Label ID="LblUserID" runat="server">Click Browse Button To Choose User</asp:Label>
                            </span></strong>
                            &nbsp;<strong><span style="color: #ff0000; width: 80%;"><asp:LinkButton 
                                ID="btnBrowse" runat="server" CausesValidation="False" CssClass="ovalbutton"><span>Browse</span></asp:LinkButton>
                            </span></strong>&nbsp;
                        </td>
                    </tr>
                    <tr class="formText">
                        <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                            &nbsp;</td>
                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                            User Name</td>
                        <td bgcolor="#ffffff" style="height: 24px" width="5">
                            :</td>
                        <td bgcolor="#ffffff" style="height: 24px" width="80%">
                            <strong><span>
                            <asp:Label ID="LabelUserName" runat="server" Width="173px"></asp:Label>
                            </span></strong>
                        </td>
                    </tr>
                    <tr class="formText">
                        <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                        </td>
                        <td width="20%" bgcolor="#ffffff" style="height: 24px">
                            Kode AO
                        </td>
                        <td width="5" bgcolor="#ffffff" style="height: 24px">
                            :
                        </td>
                        <td width="80%" bgcolor="#ffffff" style="height: 24px">
                            &nbsp;<asp:TextBox ID="TxtKodeAO" runat="server" Width="173px"></asp:TextBox><strong><span
                                style="color: #ff0000"></span></strong>
                        </td>
                    </tr>
                    <tr class="formText" bgcolor="#dddddd" height="30">
                        <td style="width: 24px">
                            <img height="15" src="images/arrow.gif" width="15">
                        </td>
                        <td colspan="3" style="height: 9px">
                            <table cellspacing="0" cellpadding="3" border="0">
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="True" 
                                            ImageUrl="~/Images/button/edit.gif">
                                        </asp:ImageButton>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton">
                                        </asp:ImageButton>
                                    </td>
                                </tr>
                            </table>
                            <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
                        </td>
                    </tr>
                </table>
            </asp:View>
            <asp:View ID="dialog" runat="server">
                <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#ffffff"
                    border="2" id="TABLE2">
                    <tr class="formText" align="center">
                        <td>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="formText" align="center">
                        <td>
                            <asp:ImageButton ID="btnOK" runat="server" ImageUrl="~/Images/button/ok.gif" />
                        </td>
                    </tr>
                </table>
            </asp:View>
        </asp:MultiView>
    </ajax:AjaxPanel>
</asp:Content>
