#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
#End Region

Partial Class MsProvince_add
	Inherits Parent



#Region "Function"

	''' <summary>
	''' Validasi men-handle seluruh syarat2 data valid
	''' data yang lolos dari validasi dipastikan lolos ke database 
	''' karena menggunakan FillorNothing
	''' </summary>
	''' <remarks></remarks>
	Private Sub ValidasiControl()
	
	  '======================== Validasi textbox :  ID canot Null ====================================================
 If ObjectAntiNull(txtIdProvince.Text) = False Then Throw New Exception("ID  must be filled  ")
        If Not IsNumeric(Me.txtIdProvince.Text) Then
            Throw New Exception("ID must be in number format")
        End If
  '======================== Validasi textbox :  Code canot Null ====================================================
 If ObjectAntiNull(txtCode.Text) = False Then Throw New Exception("Code  must be filled  ")
  '======================== Validasi textbox :  Nama canot Null ====================================================
 If ObjectAntiNull(txtNama.Text) = False Then Throw New Exception("Nama  must be filled  ")
  '======================== Validasi textbox :  Description canot Null ====================================================
 If ObjectAntiNull(txtDescription.Text) = False Then Throw New Exception("Description  must be filled  ")


		If Not DataRepository.MsProvinceProvider.GetByIdProvince(txtIdProvince.Text) Is Nothing Then
			Throw New Exception("ID already exist in the database")
		End If

		If DataRepository.MsProvince_ApprovalDetailProvider.GetPaged(MsProvince_ApprovalDetailColumn.IdProvince.ToString & "=" & txtIdProvince.Text, "", 0, 1, Nothing).Count > 0 Then
			Throw New Exception("Data exist in List Waiting Approval")
		End If

	   
		'If LBMapping.Items.Count = 0 Then Throw New Exception("data doesnt have list Mapping")
	End Sub

#End Region

#Region "events..."
    Protected Sub ImgBack_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Response.Redirect("MsProvince_View.aspx")
    End Sub
	Protected Sub imgBrowse_click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgBrowse.Click
		Try
			If Not Get_DataFromPicker Is Nothing Then
				Listmaping.AddRange(Get_DataFromPicker)
			End If
		Catch ex As Exception
			LogError(ex)
			Me.CvalPageErr.IsValid = False
			Me.CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

	Protected Sub imgDeleteItem_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgDeleteItem.Click
		Try
			If LBMapping.SelectedIndex < 0 Then
				Throw New Exception("No data Selected")
			End If
			Listmaping.RemoveAt(LBMapping.SelectedIndex)

		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try

	End Sub

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(Sahassa.CommonCTRWeb.Commonly.SessionCurrentPage)
				Listmaping = New TList(Of MappingMsProvinceNCBSPPATK)
			Catch ex As Exception
				LogError(ex)
				CvalPageErr.IsValid = False
				CvalPageErr.ErrorMessage = ex.Message
			End Try
		End If

	End Sub

	

	Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
		Try
			Page.Validate("handle")
			If Page.IsValid Then
				ValidasiControl()
				'create audittrailtype
                'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
				' =========== Insert Header Approval
				Dim KeyHeaderApproval As Integer
				Using ObjMsProvince_Approval As New MsProvince_Approval
					With ObjMsProvince_Approval
						FillOrNothing(.FK_MsMode_Id, 1,true,Oint)
                        FillOrNothing(.Fk_MsUser_Id, SessionPkUserId)
						FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
					End With
					DataRepository.MsProvince_ApprovalProvider.Save(ObjMsProvince_Approval)
					KeyHeaderApproval = ObjMsProvince_Approval.PK_MsProvince_Approval_Id
				End Using
				'============ Insert Detail Approval
				Using objMsProvince_ApprovalDetail As New MsProvince_ApprovalDetail()
					With objMsProvince_ApprovalDetail
						FillOrNothing(.IdProvince, txtIdProvince.Text,true,Oint)
FillOrNothing(.Code, txtCode.Text,true,Ovarchar)
FillOrNothing(.Nama, txtNama.Text,true,Ovarchar)
FillOrNothing(.Description, txtDescription.Text,true,Ovarchar)

                        FillOrNothing(.Activation, 1, True, oBit)
                       	FillOrNothing(.CreatedDate, Date.Now)
						FillOrNothing(.CreatedBy, SessionUserId)
						FillOrNothing(.FK_MsProvince_Approval_Id, KeyHeaderApproval)
					End With
					DataRepository.MsProvince_ApprovalDetailProvider.Save(objMsProvince_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsProvince_ApprovalDetail.Pk_MsProvince_Approval_Detail_Id
					'Insert auditrail
                    'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "Request for approval inserted  data MsProvince", objMsProvince_ApprovalDetail, Nothing)
					'========= Insert mapping item 
					Dim LobjMappingMsProvinceNCBSPPATK_Approval_Detail As New TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail)
					For Each objMappingMsProvinceNCBSPPATK As MappingMsProvinceNCBSPPATK In Listmaping
						Dim Mapping As New MappingMsProvinceNCBSPPATK_Approval_Detail
						With Mapping
							FillOrNothing(.IdProvince, objMsProvince_ApprovalDetail.IdProvince)
                            FillOrNothing(.IDProvinceNCBS, objMappingMsProvinceNCBSPPATK.Pk_MsProvinceNCBS_Id)
							FillOrNothing(.PK_MsProvince_Approval_Id, KeyHeaderApproval)
							FillOrNothing(.PK_MappingMsProvinceNCBSPPATK_approval_detail_Id, keyHeaderApprovalDetail)
						    FillOrNothing(.Nama, objMappingMsProvinceNCBSPPATK.Nama)
							LobjMappingMsProvinceNCBSPPATK_Approval_Detail.Add(Mapping)
						End With
					Next
					DataRepository.MappingMsProvinceNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsProvinceNCBSPPATK_Approval_Detail)
					'session Maping item Clear
					Listmaping.Clear()
					LblConfirmation.Text = "MsProvince data is waiting approval for Insert"
					MtvMsUser.ActiveViewIndex = 1
					ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
				End Using
			End If
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try

	End Sub

	Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
		Try
			ClearAllControl(VwAdd)
			MtvMsUser.ActiveViewIndex = 0
            ImgBtnSave.Visible = True
            ImgBackAdd.Visible = True
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

	Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
		Response.Redirect("MsProvince_View.aspx")
	End Sub

	Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
		Try
			BindListMapping()
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub

#End Region

#Region "Property..."
    ''' <summary>
    ''' Mengambil Data dari Picker
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Get_DataFromPicker As TList(Of MappingMsProvinceNCBSPPATK)
        Get
            Dim Temp As New TList(Of MappingMsProvinceNCBSPPATK)
            Dim L_MsProvinceNCBS As TList(Of MsProvinceNCBS)
            Try
                If Session("PickerMsProvinceNCBS.Data") Is Nothing Then
                    Throw New Exception
                End If
                L_MsProvinceNCBS = Session("PickerMsProvinceNCBS.Data")
                For Each i As MsProvinceNCBS In L_MsProvinceNCBS
                    Dim Tempmapping As New MappingMsProvinceNCBSPPATK
                    With Tempmapping
                        .Pk_MsProvinceNCBS_Id = i.IdProvinceNCBS
                        .Nama = i.Nama
                    End With
                    Temp.Add(Tempmapping)
                Next
            Catch ex As Exception
                L_MsProvinceNCBS = Nothing
            End Try
            Return Temp
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping As TList(Of MappingMsProvinceNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(value As TList(Of MappingMsProvinceNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsProvinceNCBSPPATK In Listmaping.FindAllDistinct("Pk_MsProvinceNCBS_Id")
                Temp.Add(i.Pk_MsProvinceNCBS_Id.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

End Class


