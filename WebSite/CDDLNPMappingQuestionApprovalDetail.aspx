﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CDDLNPMappingQuestionApprovalDetail.aspx.vb" Inherits="CDDLNPMappingQuestionApprovalDetail" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>
                    <img height="17" src="Images/dot_title.gif" width="17" />
                    <asp:Label ID="lblHeader" runat="server" Text="EDD Mapping Question Approval"></asp:Label>&nbsp;
                    <hr />
                </strong>
            </td>
        </tr>
    </table>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none">
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" width="20%">
                CDD Type</td>
            <td bgcolor="#ffffff" width="5px">
                :</td>
            <td bgcolor="#ffffff" colspan="2" width="80%">
                <asp:Label ID="lblCDDType" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                CDD Nasabah Type</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:Label ID="lblCDDNasabahType" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" height="20">
            <td bgcolor="#ffffff">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                Requested By</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:Label ID="lblRequestedBy" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" height="20">
            <td bgcolor="#ffffff">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                Requested Date</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:Label ID="lblRequestedDate" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" height="20">
            <td bgcolor="#ffffff">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                Action</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:Label ID="lblAction" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff" colspan="5">
                &nbsp;</td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" colspan="4" width="50%">
                <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                    border="2" height="72" style="border-top-style: none; border-right-style: none;
                    border-left-style: none; border-bottom-style: none">
                    <tr>
                        <td valign="top" bgcolor="white">
                            <strong>Old Mapping</strong>
                        </td>
                        <td valign="top" bgcolor="white">
                            <strong>New Mapping</strong>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" width="50%">
                            <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                <asp:DataGrid ID="GridViewCDDQuestionOld" runat="server" AutoGenerateColumns="False"
                                    Font-Size="XX-Small" BackColor="White" CellPadding="4" Width="100%" GridLines="Vertical"
                                    BorderColor="#DEDFDE" ForeColor="Black" AllowPaging="True">
                                    <FooterStyle BackColor="#CCCC99" />
                                    <AlternatingItemStyle BackColor="White" />
                                    <ItemStyle BackColor="#F7F7DE" />
                                    <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Visible="false" />
                                    <Columns>
                                        <asp:BoundColumn DataField="FK_MappingSettingCDDBagian_ID" Visible="False" />
                                        <asp:BoundColumn HeaderText="CDD Bagian Name" HeaderStyle-ForeColor="white">
                                            <ItemStyle Width="20%" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FK_CDD_Question_ID" Visible="False" />
                                        <asp:BoundColumn HeaderText="CDD Question" HeaderStyle-ForeColor="white">
                                            <ItemStyle Width="80%" />
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </ajax:AjaxPanel>
                        </td>
                        <td valign="top" width="50%">
                            <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                                <asp:DataGrid ID="GridViewCDDQuestionNew" runat="server" AutoGenerateColumns="False"
                                    Font-Size="XX-Small" BackColor="White" CellPadding="4" Width="100%" GridLines="Vertical"
                                    BorderColor="#DEDFDE" ForeColor="Black" AllowPaging="True">
                                    <FooterStyle BackColor="#CCCC99" />
                                    <AlternatingItemStyle BackColor="White" />
                                    <ItemStyle BackColor="#F7F7DE" />
                                    <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Visible="false" />
                                    <Columns>
                                        <asp:BoundColumn DataField="FK_MappingSettingCDDBagian_ID" Visible="False" />
                                        <asp:BoundColumn HeaderText="CDD Bagian Name" HeaderStyle-ForeColor="white">
                                            <ItemStyle Width="20%" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FK_CDD_Question_ID" Visible="False" />
                                        <asp:BoundColumn HeaderText="CDD Question" HeaderStyle-ForeColor="white">
                                            <ItemStyle Width="80%" />
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </ajax:AjaxPanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel2" runat="server">
                                <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                    bgcolor="#ffffff" border="2">
                                    <tr>
                                        <td class="regtext" valign="middle" align="left" bgcolor="#ffffff" style="width: 175px;
                                            height: 15px">
                                            Page&nbsp;<asp:Label ID="PageCurrentPageOld" runat="server" CssClass="regtext">0</asp:Label>&nbsp;of
                                            <asp:Label ID="PageTotalPagesOld" runat="server" CssClass="regtext">0</asp:Label>
                                            (<asp:Label ID="PageTotalRowsOld" runat="server">0</asp:Label>
                                            Records)</td>
                                        <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff" style="height: 15px">
                                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1"></font>
                                        </td>
                                        <td class="regtext" valign="middle" align="left" bgcolor="#ffffff" style="height: 15px">
                                        </td>
                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 15px">
                                            <img height="5" src="images/first.gif" width="6">
                                        </td>
                                        <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff" style="height: 15px">
                                            <asp:LinkButton ID="LinkButtonFirstOld" runat="server" CssClass="regtext" CommandName="First"
                                                OnCommand="PageNavigateOld">First</asp:LinkButton>
                                        </td>
                                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff" style="height: 15px">
                                            <img height="5" src="images/prev.gif" width="6"></td>
                                        <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff" style="height: 15px">
                                            <asp:LinkButton ID="LinkButtonPreviousOld" runat="server" CssClass="regtext" CommandName="Prev"
                                                OnCommand="PageNavigateOld">Previous</asp:LinkButton>
                                        </td>
                                        <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff" style="height: 15px">
                                            <asp:LinkButton ID="LinkButtonNextOld" runat="server" CssClass="regtext" CommandName="Next"
                                                OnCommand="PageNavigateOld">Next</asp:LinkButton></td>
                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 15px">
                                            <img height="5" src="images/next.gif" width="6"></td>
                                        <td class="regtext" valign="middle" align="left" width="50" bgcolor="#ffffff" style="height: 15px">
                                            <asp:LinkButton ID="LinkButtonLastOld" runat="server" CssClass="regtext" CommandName="Last"
                                                OnCommand="PageNavigateOld">Last</asp:LinkButton>
                                        </td>
                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 15px">
                                            <img height="5" src="images/last.gif" width="6"></td>
                                    </tr>
                                </table>
                            </ajax:AjaxPanel>
                        </td>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                                <table id="Table1" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                    bgcolor="#ffffff" border="2">
                                    <tr>
                                        <td class="regtext" valign="middle" align="left" bgcolor="#ffffff" style="width: 175px;
                                            height: 15px">
                                            Page&nbsp;<asp:Label ID="PageCurrentPageNew" runat="server" CssClass="regtext">0</asp:Label>&nbsp;of
                                            <asp:Label ID="PageTotalPagesNew" runat="server" CssClass="regtext">0</asp:Label>
                                            (<asp:Label ID="PageTotalRowsNew" runat="server">0</asp:Label>
                                            Records)</td>
                                        <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff" style="height: 15px">
                                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1"></font>
                                        </td>
                                        <td class="regtext" valign="middle" align="left" bgcolor="#ffffff" style="height: 15px">
                                        </td>
                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 15px">
                                            <img height="5" src="images/first.gif" width="6">
                                        </td>
                                        <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff" style="height: 15px">
                                            <asp:LinkButton ID="LinkButtonFirstNew" runat="server" CssClass="regtext" CommandName="First"
                                                OnCommand="PageNavigateNew">First</asp:LinkButton>
                                        </td>
                                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff" style="height: 15px">
                                            <img height="5" src="images/prev.gif" width="6"></td>
                                        <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff" style="height: 15px">
                                            <asp:LinkButton ID="LinkButtonPreviousNew" runat="server" CssClass="regtext" CommandName="Prev"
                                                OnCommand="PageNavigateNew">Previous</asp:LinkButton>
                                        </td>
                                        <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff" style="height: 15px">
                                            <asp:LinkButton ID="LinkButtonNextNew" runat="server" CssClass="regtext" CommandName="Next"
                                                OnCommand="PageNavigateNew">Next</asp:LinkButton></td>
                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 15px">
                                            <img height="5" src="images/next.gif" width="6"></td>
                                        <td class="regtext" valign="middle" align="left" width="50" bgcolor="#ffffff" style="height: 15px">
                                            <asp:LinkButton ID="LinkButtonLastNew" runat="server" CssClass="regtext" CommandName="Last"
                                                OnCommand="PageNavigateNew">Last</asp:LinkButton>
                                        </td>
                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 15px">
                                            <img height="5" src="images/last.gif" width="6"></td>
                                    </tr>
                                </table>
                            </ajax:AjaxPanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="formText" bgcolor="#dddddd" height="30">
            <td style="width: 5px">
                <img height="15" src="images/arrow.gif" width="15"></td>
            <td colspan="6" style="height: 9px">
                <table cellspacing="0" cellpadding="3" border="0">
                    <tr>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                                <asp:ImageButton ID="ImageAccept" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Accept.gif" /></ajax:AjaxPanel></td>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                <asp:ImageButton ID="ImageReject" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Reject.gif">
                                </asp:ImageButton></ajax:AjaxPanel></td>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                <asp:ImageButton ID="ImageCancel" runat="server" ImageUrl="~/Images/Button/Cancel.gif">
                                </asp:ImageButton></ajax:AjaxPanel></td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
        </tr>
    </table>
</asp:Content>
