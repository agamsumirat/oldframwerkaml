﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports System.Data

Partial Class CDDLNPHeaderApprovalDetail
    Inherits Parent

    Private ReadOnly Property GetPK_CDD_Bagian_Approval_Id() As Int32
        Get
            Return IIf(Request.QueryString("ID") = String.Empty, 0, Request.QueryString("ID"))
        End Get
    End Property

    Private Property SetnGetPK_CDD_Bagian_id() As Int32
        Get
            Return CType(Session("CDDLNPHeaderApprovalDetail.PK_CDD_Bagian_id"), Int32)
        End Get
        Set(ByVal value As Int32)
            Session("CDDLNPHeaderApprovalDetail.PK_CDD_Bagian_id") = value
        End Set
    End Property

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageError.PreRender
        If cvalPageError.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("CDDLNPHeaderApproval.aspx")
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                'Load data Approval Header
                Using objApproval As VList(Of vw_CDD_Bagian_Approval) = DataRepository.vw_CDD_Bagian_ApprovalProvider.GetPaged("PK_CDD_Bagian_Approval_Id = " & Me.GetPK_CDD_Bagian_Approval_Id, String.Empty, 0, Int32.MaxValue, 0)
                    If objApproval.Count > 0 Then
                        lblRequestedBy.Text = objApproval(0).UserName
                        lblRequestedDate.Text = objApproval(0).RequestedDate.GetValueOrDefault.ToString("dd-MMM-yyyy hh:mm")
                        lblAction.Text = objApproval(0).Mode

                        'Load new data
                        Using objApprovalDetail As TList(Of CDD_Bagian_ApprovalDetail) = DataRepository.CDD_Bagian_ApprovalDetailProvider.GetPaged("FK_CDD_Bagian_Approval_Id = " & Me.GetPK_CDD_Bagian_Approval_Id, String.Empty, 0, Int32.MaxValue, 0)
                            If objApprovalDetail.Count > 0 Then
                                lblNewCDDBagian.Text = objApprovalDetail(0).CDDBagianName
                                Me.SetnGetPK_CDD_Bagian_id = objApprovalDetail(0).PK_CDD_Bagian_id
                            End If
                        End Using

                        'Load old data
                        Using objCDD_Bagian As CDD_Bagian = DataRepository.CDD_BagianProvider.GetByPK_CDD_Bagian_ID(Me.SetnGetPK_CDD_Bagian_id)
                            If objCDD_Bagian IsNot Nothing Then
                                lblOldCDDBagian.Text = objCDD_Bagian.CDDBagianName
                            End If
                        End Using

                        If lblNewCDDBagian.Text <> lblOldCDDBagian.Text Then
                            lblNewCDDBagian.ForeColor = Drawing.Color.Red
                            lblOldCDDBagian.ForeColor = Drawing.Color.Red
                        End If

                        Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                            Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                            AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                        End Using

                    Else 'Handle jika ID tidak ditemukan
                        Me.Response.Redirect("CDDLNPHeaderApproval.aspx", False)
                    End If
                End Using
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)

        Try
            objTransManager.BeginTransaction()

            'Delete Approval
            DataRepository.CDD_Bagian_ApprovalProvider.Delete(objTransManager, Me.GetPK_CDD_Bagian_Approval_Id)
            'Delete Approval Detail
            DataRepository.CDD_Bagian_ApprovalDetailProvider.Delete(objTransManager, DataRepository.CDD_Bagian_ApprovalDetailProvider.GetPaged("FK_CDD_Bagian_Approval_Id = " & Me.GetPK_CDD_Bagian_Approval_Id, String.Empty, 0, Int32.MaxValue, 0))

            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10522", False)
        Catch ex As Exception
            objTransManager.Rollback()
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)

        Try
            objTransManager.BeginTransaction()

            Using objCDD_Bagian As CDD_Bagian = DataRepository.CDD_BagianProvider.GetByPK_CDD_Bagian_ID(Me.SetnGetPK_CDD_Bagian_id)
                If objCDD_Bagian IsNot Nothing Then
                    objCDD_Bagian.CDDBagianName = lblNewCDDBagian.Text

                    'Save New Data
                    DataRepository.CDD_BagianProvider.Save(objTransManager, objCDD_Bagian)
                End If
            End Using

            'Delete Approval
            DataRepository.CDD_Bagian_ApprovalProvider.Delete(objTransManager, Me.GetPK_CDD_Bagian_Approval_Id)
            'Delete Approval Detail
            DataRepository.CDD_Bagian_ApprovalDetailProvider.Delete(objTransManager, DataRepository.CDD_Bagian_ApprovalDetailProvider.GetPaged("FK_CDD_Bagian_Approval_Id = " & Me.GetPK_CDD_Bagian_Approval_Id, String.Empty, 0, Int32.MaxValue, 0))

            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10523", False)
        Catch ex As Exception
            objTransManager.Rollback()
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub
End Class