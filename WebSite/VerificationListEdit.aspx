<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="VerificationListEdit.aspx.vb" Inherits="VerificationListEdit" title="Verification List Edit" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <ajax:AjaxPanel ID="lbl" runat="server" Width="100%">
	<table id="title"  border="2" bgcolor="#ffffff" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Verification List - Edit&nbsp;
                </strong>
                 <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label>
            </td>
        </tr>
    </table>	
    </ajax:AjaxPanel>
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72">       
        <tr class="formText">
            <td bgcolor="#ffffff" style="height: 24px" colspan="4">
                <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 81px">
                        <tr>
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                                Verification List ID</td>
                            <td align="left" style="width: 45px; height: 35px">
                                :</td>
                            <td style="width: 2%; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <asp:Label ID="LabelVerificationListID" runat="server"></asp:Label></td>
                        </tr>
                          
                        <tr>
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                                Name/Alias</td>
                            <td align="left" style="width: 45px; height: 35px">                                :</td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <asp:TextBox ID="TextBoxAlias" runat="server" CssClass="textBox" MaxLength="150" Width="300px"></asp:TextBox>
                                &nbsp; &nbsp;&nbsp;
                                <asp:LinkButton ID="LinkButtonAddAlias" runat="server" CausesValidation="False" Font-Underline="True"
                                    ForeColor="Blue">Add Alias</asp:LinkButton>&nbsp;</td>
                        </tr>
                        <tr id="GridViewAliasesRow" runat="server">
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                            </td>
                             <td align="left" style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">    
                                <div id="divAliases" runat="server" style="overflow: auto; height: 92px">
                                    <asp:DataGrid ID="GridViewAliases" runat="server" AutoGenerateColumns="False" BackColor="White"
                                        BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2" Font-Size="XX-Small"
                                        ForeColor="Black" HorizontalAlign="Left" Width="300px">
                                        <Columns>
                                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Aliases" HeaderText="Alias ">
                                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                            </asp:BoundColumn>
                                            <asp:ButtonColumn CommandName="Delete" Text="Delete">
                                                <HeaderStyle BackColor="Gray" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" />
                                            </asp:ButtonColumn>
                                        </Columns>
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                                            Visible="False" />
                                        <AlternatingItemStyle BackColor="AliceBlue" />
                                    </asp:DataGrid></div>
                            </td>
                        </tr>
                        <tr id="SpacerAliases" runat="server">
                            <td style="width: 24px; height: 15px">
                            </td>
                            <td style="width: 16%; height: 15px">
                            </td>
                             <td align="left" style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 80%; height: 15px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 24px; height: 35px">
                                &nbsp;</td>
                            <td style="width: 16%; height: 35px">
                Date of Data</td>
                             <td align="left" style="width: 45px; height: 35px">
                                :
                            </td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <asp:TextBox ID="TextDateOfData" runat="server" CssClass="textBox" MaxLength="20" Width="200px"></asp:TextBox>&nbsp;<strong><span style="color: #ff0000">
                    <input id="cmdDODDatePicker" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                        border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                        border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif); width: 16px;" title="Click to show calendar"
                        type="button" />&nbsp;</span></strong>
                                <strong><span style="color: #ff0000"></span></strong></td>
                        </tr>                          
                        <tr>
                            <td style="width: 24px; height: 35px">
                                &nbsp;</td>
                            <td style="width: 16%; height: 35px">
                Date of Birth</td>
                             <td align="left" style="width: 45px; height: 35px">
                                :
                            </td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <asp:TextBox ID="TextCustomerDOB" runat="server" CssClass="textBox" MaxLength="20" Width="200px"></asp:TextBox>&nbsp;<strong><span style="color: #ff0000">
                    <input id="cmdDOBDatePicker" runat="server" name="popUpCalc"  style="border-right: #ffffff 0px solid;
                        border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                        border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif); width: 16px;" title="Click to show calendar"
                        type="button"  />&nbsp;</span></strong>
                                <strong><span style="color: #ff0000"></span></strong></td>
                        </tr>
						<tr>
							<td style="width: 24px; height: 35px">
							</td>
							<td style="width: 16%; height: 35px">
								Birth Place</td>
							<td align="left" style="width: 45px; height: 35px">
								:</td>
							<td style="width: 45px; height: 35px">
							</td>
							<td style="width: 80%; height: 35px">
								<asp:TextBox ID="TxtBirthPlace" runat="server" CssClass="textBox" MaxLength="20" Width="200px"></asp:TextBox></td>
						</tr>
                        <tr>
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                                Address<br />
                                <br />
                                <br />
                                <br />
                                <br />
                            </td>
                             <td align="left" style="width: 45px; height: 35px">
                                :<br />
                                <br />
                                <br />
                                 <br />
                                 <br />
                            </td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <asp:textbox id="TextAddress" runat="server" CssClass="textBox" MaxLength="255" Width="300px" Height="42px" TextMode="MultiLine"></asp:textbox>
                                &nbsp;&nbsp;
                                <asp:LinkButton ID="LinkButtonAddAddress" runat="server" CausesValidation="False"
                                    Font-Underline="True" ForeColor="Blue">Add Address</asp:LinkButton>&nbsp;
                                <br />
                                Address Type :
                                <asp:DropDownList ID="DropDownListAddressType" runat="server">
                                    <asp:ListItem Value="0">Home</asp:ListItem>
                                    <asp:ListItem Value="1">Office</asp:ListItem>
                                    <asp:ListItem Value="2">Other</asp:ListItem>
                                </asp:DropDownList>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;
                                <asp:CheckBox ID="CheckBoxIsLocalAddress" runat="server" Text="Local Address?" Checked="True" />
                                <br />
                            </td>
                        </tr>
                         <tr id="GridViewAddressesRow" runat="server">
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                            </td>
                             <td align="left" style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <div style="overflow: auto; height: 92px" id="DIV1">
                                    <asp:DataGrid ID="GridViewAddresses" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                                        Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                                        <Columns>
                                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Addresses" HeaderText="Address">
                                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="AddressType" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="AddressTypeLabel" HeaderText="Address Type">
                                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="IsLocalAddress" HeaderText="Local Address?">
                                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:ButtonColumn CommandName="Delete" Text="Delete">
                                                <HeaderStyle BackColor="Gray" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                            </asp:ButtonColumn>
                                        </Columns>
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                                            Visible="False" />
                                        <AlternatingItemStyle BackColor="AliceBlue" />
                                    </asp:DataGrid></div>
                            </td>
                        </tr>
                          <tr id="SpacerAddresses" runat="server">
                            <td style="width: 24px; height: 15px">
                            </td>
                            <td style="width: 16%; height: 15px">
                            </td>
                             <td align="left" style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 80%; height: 15px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                                ID Number</td>
                             <td align="left" style="width: 45px; height: 35px">
                                :
                            </td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <asp:TextBox ID="TextBoxIDNo" runat="server" CssClass="textBox" MaxLength="50" Width="200px"></asp:TextBox>
                                &nbsp;&nbsp; &nbsp;
                                <asp:LinkButton ID="LinkButtonAddIDNumber" runat="server" CausesValidation="False"
                                    Font-Underline="True" ForeColor="Blue">Add ID Number</asp:LinkButton>
                                &nbsp; &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr id="GridViewIDNosRow" runat="server">
                            <td style="width: 24px; height: 35px">
                                &nbsp; &nbsp; &nbsp; &nbsp;
                            </td>
                            <td style="width: 16%; height: 35px">
                            </td>
                             <td align="left" style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <div style="overflow: auto; height: 92px">
                                    <asp:DataGrid ID="GridViewIDNos" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                                        Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                                        <Columns>
                                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="IDNos" HeaderText="ID Number">
                                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                            </asp:BoundColumn>
                                            <asp:ButtonColumn CommandName="Delete" Text="Delete">
                                                <HeaderStyle BackColor="Gray" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" />
                                            </asp:ButtonColumn>
                                        </Columns>
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                                            Visible="False" />
                                        <AlternatingItemStyle BackColor="AliceBlue" />
                                    </asp:DataGrid></div>
                            </td>
                        </tr>
                         <tr id="SpacerIDNos" runat="server">
                            <td style="width: 24px; height: 15px">
                            </td>
                            <td style="width: 16%; height: 15px">
                            </td>
                             <td align="left" style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 80%; height: 15px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                Verification List Type</td>
                             <td align="left" style="width: 45px; height: 35px">
                                :
                            </td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <asp:DropDownList ID="DropDownListType" runat="server" Width="200px">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                Category</td>
                             <td align="left" style="width: 45px; height: 35px">
                                :</td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <asp:DropDownList ID="DropDownCategoryID" runat="server" Width="200px">
                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                                Custom Remark(s)<br />
                                <br />
                                <br />
                            </td>
                            <td align="left" style="width: 45px; height: 35px">
                                :<br />
                                <br />
                                <br />
                            </td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <asp:TextBox ID="TextboxCustomRemarks" runat="server" CssClass="textBox" Height="42px" MaxLength="255"
                                    TextMode="MultiLine" Width="300px"></asp:TextBox>
                                &nbsp;&nbsp;&nbsp;
                                <asp:LinkButton ID="LinkButtonAddCustomRemarks" runat="server" CausesValidation="False"
                                    Font-Underline="True" ForeColor="Blue">Add Custom Remark</asp:LinkButton></td>
                        </tr>
                        <tr id="GridViewCustomRemarksRow" runat="server">
                            <td style="width: 24px; height: 35px">
                            </td>
                            <td style="width: 16%; height: 35px">
                            </td>
                            <td align="left" style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 45px; height: 35px">
                            </td>
                            <td style="width: 80%; height: 35px">
                                <div style="overflow: auto; height: 92px">
                                    <asp:DataGrid ID="GridViewCustomRemarks" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="1px" CellPadding="2"
                                        Font-Size="XX-Small" ForeColor="Black" HorizontalAlign="Left">
                                        <Columns>
                                            <asp:BoundColumn DataField="id" HeaderText="No.">
                                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="CustomRemarks" HeaderText="Custom Remark">
                                                <HeaderStyle BackColor="Gray" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                            </asp:BoundColumn>
                                            <asp:ButtonColumn CommandName="Delete" Text="Delete">
                                                <HeaderStyle BackColor="Gray" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" />
                                            </asp:ButtonColumn>
                                        </Columns>
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                                            Visible="False" />
                                        <AlternatingItemStyle BackColor="AliceBlue" />
                                    </asp:DataGrid></div>
                            </td>
                        </tr>
                          <tr id="GridViewCustomNationalityRow" runat="server">
                              <td style="width: 24px; height: 35px">&nbsp;</td>
                              <td style="width: 16%; height: 35px">Nationality</td>
                              <td align="left" style="width: 45px; height: 35px">:</td>
                              <td style="width: 45px; height: 35px">&nbsp;</td>
                              <td style="width: 80%; height: 35px">
                                  <asp:TextBox ID="TxtNationality" runat="server" CssClass="textBox" MaxLength="20" Width="200px"></asp:TextBox>
                              </td>
                        </tr>
                          <tr id="SpacerCustomRemarks" runat="server">
                            <td style="width: 24px; height: 15px">
                            </td>
                            <td style="width: 16%; height: 15px">
                            </td>
                             <td align="left" style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 45px; height: 15px">
                            </td>
                            <td style="width: 80%; height: 15px">
                            </td>
                        </tr>
                        <tr class="formText" bgColor="#dddddd" height="30">
                            <td style="width: 24px">
                                <IMG height="15" src="images/arrow.gif" width="15"></td>
                            <td style="height: 35px" colspan="4">
                            <asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="updateButton"></asp:imagebutton>
                                <asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton><br />
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
                        </tr>
                    </table>
                    <strong><span style="color: #ff0000"></span></strong>
                </ajax:AjaxPanel>
            </td>
        </tr>
	</table>
	
</asp:Content>

