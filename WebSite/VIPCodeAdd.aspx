﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="VIPCodeAdd.aspx.vb" Inherits="VIPCodeAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>VIPCode - Add New&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:MultiView ID="mtvVIPCodeAdd" runat="server">
        <asp:View ID="View1" runat="server">
            <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                border="2" height="72" id="TABLE1">
                <tr class="formText">
                    <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                        border-left-style: none; height: 24px; border-bottom-style: none" colspan="4">
                        <span style="color: #ff0000">* Required</span>
                    </td>
                </tr>
                <tr class="formText">
                    <td bgcolor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none;
                        border-left-style: none; text-align: center; border-bottom-style: none;">
                        &nbsp;
                    </td>
                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                        border-left-style: none; border-bottom-style: none;" width="23%">
                        VIPCode Code
                    </td>
                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                        border-left-style: none; border-bottom-style: none;">
                        :
                    </td>
                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                        border-left-style: none; border-bottom-style: none;" width="80%">
                        <asp:TextBox ID="txtVIPCodeCode" runat="server" CssClass="textBox" MaxLength="50"
                            Width="190px"></asp:TextBox>
                        <strong><span style="color: #ff0000">*</span></strong>
                    </td>
                </tr>
                <tr class="formText">
                    <td bgcolor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none;
                        border-left-style: none; text-align: center; border-bottom-style: none;">
                        &nbsp;
                    </td>
                    <td width="23%" bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                        border-left-style: none; border-bottom-style: none;">
                        VIPCode Description
                    </td>
                    <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                        border-left-style: none; border-bottom-style: none;">
                        :
                    </td>
                    <td width="77%" bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none;
                        border-left-style: none; border-bottom-style: none;">
                        <asp:TextBox ID="txtDescription" runat="server" CssClass="textBox" MaxLength="255"
                            Width="200px" Height="63px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr class="formText" bgcolor="#dddddd" height="30">
                    <td style="width: 24px">
                        <img height="15" src="images/arrow.gif" width="15">
                    </td>
                    <td colspan="3" style="height: 9px">
                        <table cellspacing="0" cellpadding="3" border="0">
                            <tr>
                                <td>
                                    <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="True" SkinID="AddButton"
                                        ImageUrl="~/Images/button/add.gif"></asp:ImageButton>
                                </td>
                                <td>
                                    <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"
                                        ImageUrl="~/Images/button/cancel.gif"></asp:ImageButton>
                                </td>
                            </tr>
                        </table>
                        <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="dialog" runat="server">
            <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#ffffff"
                border="2" id="TABLE2">
                <tr class="formText" align="center">
                    <td>
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr class="formText" align="center">
                    <td>
                        <asp:ImageButton ID="btnBack" runat="server" 
                            ImageUrl="~/Images/button/back.gif" />
                        <asp:ImageButton ID="btnOK" runat="server" ImageUrl="~/Images/button/ok.gif" />
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</asp:Content>

