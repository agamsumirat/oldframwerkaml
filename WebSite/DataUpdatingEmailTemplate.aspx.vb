Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Public Class DataUpdatingEmailTemplate
    Inherits Parent

#Region " Property "
    ''' <summary>
    ''' get focus id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TextSubject.ClientID
        End Get
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cek approved
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Function CekApproved() As Boolean
        Using AccessParameters As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
            Dim count As Int32 = AccessParameters.CountParametersApproval(5)
            If count > 0 Then
                Return False
            Else
                Return True
            End If
        End Using
    End Function

    ''' <summary>
    ''' fill group
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroupLevelType()
        Try
            Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                'Bind DropDrownListTO webcontrol
                Me.DropDownListTO.DataSource = AccessGroup.GetData
                Me.DropDownListTO.DataTextField = "LevelTypeName"
                Me.DropDownListTO.DataValueField = "LevelTypeID"
                Me.DropDownListTO.DataBind()

                'Bind DropDownListCC webcontrol
                Me.DropDownListCC.DataSource = AccessGroup.GetData
                Me.DropDownListCC.DataTextField = "LevelTypeName"
                Me.DropDownListCC.DataValueField = "LevelTypeID"
                Me.DropDownListCC.DataBind()

                'Bind DropDownListBCC webcontrol
                Me.DropDownListBCC.DataSource = AccessGroup.GetData
                Me.DropDownListBCC.DataTextField = "LevelTypeName"
                Me.DropDownListBCC.DataValueField = "LevelTypeID"
                Me.DropDownListBCC.DataBind()
            End Using

        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get data login parameter
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	03/07/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GetData()
        Using AccessDataUpdatingEmailTemplate As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingEmailTemplateTableAdapter
            Using TableDataUpdatingEmailTemplate As AMLDAL.AMLDataSet.DataUpdatingEmailTemplateDataTable = AccessDataUpdatingEmailTemplate.GetData
                'Bila tabel DataUpdatingEmailTemplate tidak kosong, maka isi web control dengan informasi dlm tabel tsb
                If TableDataUpdatingEmailTemplate.Rows.Count > 0 Then
                    Dim TableRowDataUpdatingEmailTemplate As AMLDAL.AMLDataSet.DataUpdatingEmailTemplateRow = TableDataUpdatingEmailTemplate.Rows(0)

                    ViewState("ToLevelTypeID_Old") = CType(TableRowDataUpdatingEmailTemplate.ToLevelTypeID, Int32)
                    Me.DropDownListTO.SelectedValue = ViewState("ToLevelTypeID_Old")

                    ViewState("CCLevelTypeID_Old") = CType(TableRowDataUpdatingEmailTemplate.CCLevelTypeID, Int32)
                    Me.DropDownListCC.Text = ViewState("CCLevelTypeID_Old")

                    ViewState("BCCLevelTypeID_Old") = CType(TableRowDataUpdatingEmailTemplate.BCCLevelTypeID, Int32)
                    Me.DropDownListBCC.Text = ViewState("BCCLevelTypeID_Old")

                    ViewState("Subject_Old") = CType(TableRowDataUpdatingEmailTemplate.Subject, String)
                    Me.TextSubject.Text = ViewState("Subject_Old")

                    ViewState("Body_Old") = CType(TableRowDataUpdatingEmailTemplate.Body, String)
                    Me.TextBody.Text = ViewState("Body_Old")

                    ViewState("IncludeAttachment_Old") = CType(TableRowDataUpdatingEmailTemplate.IncludeAttachment, Boolean)
                    Me.CheckIncludeAttachmentData.Checked = ViewState("IncludeAttachment_Old")

                    ViewState("CreatedDate_Old") = CType(TableRowDataUpdatingEmailTemplate.CreatedDate, Date)
                    ViewState("LastUpdateDate_Old") = CType(TableRowDataUpdatingEmailTemplate.LastUpdateDate, Date)
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load page handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Me.CekApproved() Then
                If Not Me.IsPostBack Then
                    Me.FillGroupLevelType()
                    Me.GetData()

                    Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                    'Using Transcope As New Transactions.TransactionScope
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                        Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                        AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                        'Transcope.Complete()
                    End Using
                    'End Using
                End If
            Else
                Throw New Exception("Cannot update Data Updating Email Template because it is currently waiting for approval")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cancel event handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "Default.aspx"

        Me.Response.Redirect("Default.aspx", False)
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' save handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>    
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry] 14/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            If Me.CekApproved Then

                Page.Validate()

                If Page.IsValid Then

                    Using ParametersPending As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                        oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(ParametersPending, Data.IsolationLevel.ReadUncommitted)
                        'Perika apakah sudah ada entry dalam tabel DataUpdatingEmailTemplate, bila belum ada, maka buat entry baru dlm tabel DataUpdatingEmailTemplate,
                        'tapi bila entry sudah ada dalam tabel DataUpdatingEmailTemplate, maka update entry tsb
                        Using AccessDataUpdatingEmailTemplate As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingEmailTemplateTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessDataUpdatingEmailTemplate, oSQLTrans)
                            Using TableDataUpdatingEmailTemplate As AMLDAL.AMLDataSet.DataUpdatingEmailTemplateDataTable = AccessDataUpdatingEmailTemplate.GetData                                
                                Using DataUpdatingEmailTemplateApproval As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingEmailTemplate_ApprovalTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(DataUpdatingEmailTemplateApproval, oSQLTrans)
                                    'Buat variabel untuk menampung nilai-nilai baru
                                    Dim ToLevelTypeID As Int32 = Me.DropDownListTO.SelectedValue
                                    Dim CCLevelTypeID As Int32 = Me.DropDownListCC.SelectedValue
                                    Dim BCCLevelTypeID As Int32 = Me.DropDownListBCC.SelectedValue
                                    Dim Subject As String = Me.TextSubject.Text
                                    Dim Body As String
                                    If Me.TextBody.Text.Length <= 1000 Then
                                        Body = Me.TextBody.Text
                                    Else
                                        Body = Me.TextBody.Text.Substring(0, 1000)
                                    End If
                                    Dim IncludeAttachment As Boolean = Me.CheckIncludeAttachmentData.Checked

                                    Dim ModeID As Int16
                                    Dim DataUpdatingEmailTemplateApprovalID As Int64

                                    If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                        'Using TranScope As New Transactions.TransactionScope
                                        If TableDataUpdatingEmailTemplate.Rows.Count > 0 Then
                                            InsertAuditTrail("Edit", "Accept")
                                            If AccessDataUpdatingEmailTemplate.UpdateDataUpdatingEmailTemplate(ToLevelTypeID, CCLevelTypeID, BCCLevelTypeID, Subject, Body, IncludeAttachment, Now) Then
                                                Me.LblSuccess.Visible = True
                                                Me.LblSuccess.Text = "Succes to Update Data Updating Email Template"
                                                'TranScope.Complete()
                                            Else
                                                Throw New Exception("Failed to Update Update Data Updating Email Template")
                                            End If
                                        Else
                                            If AccessDataUpdatingEmailTemplate.Insert(ToLevelTypeID, CCLevelTypeID, BCCLevelTypeID, Subject, Body, IncludeAttachment, Now, Now) Then
                                                Me.LblSuccess.Visible = True
                                                Me.LblSuccess.Text = "Succes to Update Data Updating Email Template"
                                                Me.InsertAuditTrail("Add", "Accept")
                                                'TranScope.Complete()
                                            Else
                                                Throw New Exception("Failed to Insert data updating email template.")
                                            End If
                                        End If
                                        'End Using
                                    Else
                                        'Using TranScope As New Transactions.TransactionScope
                                        'Bila tabel DataUpdatingEmailTemplate tidak kosong, maka update entry tsb & set ModeID = 2 (Edit)
                                        If TableDataUpdatingEmailTemplate.Rows.Count > 0 Then
                                            'Buat variabel untuk menampung nilai-nilai lama
                                            Dim ToLevelTypeID_Old As Int32 = ViewState("ToLevelTypeID_Old")
                                            Dim CCLevelTypeID_Old As Int32 = ViewState("CCLevelTypeID_Old")
                                            Dim BCCLevelTypeID_Old As Int32 = ViewState("BCCLevelTypeID_Old")
                                            Dim Subject_Old As String = ViewState("Subject_Old")
                                            Dim Body_Old As String = ViewState("Body_Old")
                                            Dim IncludeAttachment_Old As Boolean = ViewState("IncludeAttachment_Old")
                                            Dim CreatedDate_Old As DateTime = ViewState("CreatedDate_Old")
                                            Dim LastUpdate_Old As DateTime = ViewState("LastUpdateDate_Old")
                                            ModeID = 2 'Edit

                                            'DataUpdatingEmailTemplateApprovalID adalah primary key dari tabel DataUpdatingEmailTemplate_Approval yang sifatnya identity dan baru dibentuk setelah row baru dibuat
                                            DataUpdatingEmailTemplateApprovalID = DataUpdatingEmailTemplateApproval.InsertDataUpdatingEmailTemplate_Approval(ToLevelTypeID, CCLevelTypeID, BCCLevelTypeID, Subject, Body, IncludeAttachment, CreatedDate_Old, Now, ToLevelTypeID_Old, CCLevelTypeID_Old, BCCLevelTypeID_Old, Subject_Old, Body_Old, IncludeAttachment_Old, CreatedDate_Old, LastUpdate_Old)
                                        Else 'Bila tabel DataUpdatingEmailTemplate kosong, maka buat entry baru & set ModeID = 1 (Add)
                                            ModeID = 1 'Add

                                            'DataUpdatingEmailTemplateApprovalID adalah primary key dari tabel DataUpdatingEmailTemplate_Approval yang sifatnya identity dan baru dibentuk setelah row baru dibuat
                                            DataUpdatingEmailTemplateApprovalID = DataUpdatingEmailTemplateApproval.InsertDataUpdatingEmailTemplate_Approval(ToLevelTypeID, CCLevelTypeID, BCCLevelTypeID, Subject, Body, IncludeAttachment, Now, Now, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
                                        End If

                                        'ParametersPendingApprovalID adalah primary key dari tabel Parameters_PendingApproval yang sifatnya identity dan baru dibentuk setelah row baru dibuat 
                                        Dim ParametersPendingApprovalID As Int64 = ParametersPending.InsertParameters_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, ModeID)

                                        Using ParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                                            Sahassa.AML.TableAdapterHelper.SetTransaction(ParametersApproval, oSQLTrans)
                                            'ParameterItemID 5 = DataUpdatingEmailTemplate
                                            ParametersApproval.Insert(ParametersPendingApprovalID, ModeID, 5, DataUpdatingEmailTemplateApprovalID)
                                        End Using

                                        oSQLTrans.Commit()

                                        Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=8851"
                                        Me.Response.Redirect("MessagePending.aspx?MessagePendingID=8851", False)
                                        'End Using
                                    End If
                                End Using
                            End Using
                        End Using
                    End Using
                End If
            Else
                Throw New Exception("Cannot update Data Updating Email Template because it is currently waiting for approval")
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    ''' <summary>
    ''' Insert Audit Trail
    ''' </summary>
    ''' <param name="mode"></param>
    ''' <param name="Action"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function InsertAuditTrail(ByVal mode As String, ByVal Action As String) As Boolean
        Try
            Using AccessDataEmailTemplate As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingEmailTemplateTableAdapter
                Dim rowData() As AMLDAL.AMLDataSet.DataUpdatingEmailTemplateRow = AccessDataEmailTemplate.GetData.Select
                Dim ToLevelTypeID As Int32 = Me.DropDownListTO.SelectedValue
                Dim CCLevelTypeID As Int32 = Me.DropDownListCC.SelectedValue
                Dim BCCLevelTypeID As Int32 = Me.DropDownListBCC.SelectedValue
                Dim Subject As String = Me.TextSubject.Text
                Dim Body As String
                If Me.TextBody.Text.Length <= 1000 Then
                    Body = Me.TextBody.Text
                Else
                    Body = Me.TextBody.Text.Substring(0, 1000)
                End If
                Dim IncludeAttachment As Boolean = Me.CheckIncludeAttachmentData.Checked
                Sahassa.AML.AuditTrailAlert.AuditTrailChecking(8)
                If mode.ToLower = "add" Then
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "ToLevelTypeID", mode, "", ToLevelTypeID, Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "CCLevelTypeID", mode, "", CCLevelTypeID, Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "BCCLevelTypeID", mode, "", BCCLevelTypeID, Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "Subject", mode, "", Subject, Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "Body", mode, "", Body, Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "IncludeAttachment", mode, "", IncludeAttachment, Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "CreatedDate", mode, "", Now.ToString("dd-MMMM-yyyy HH:mm"), Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "LastUpdateDate", mode, "", Now.ToString("dd-MMMM-yyyy HH:mm"), Action)
                    End Using
                Else
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "ToLevelTypeID", mode, rowData(0).ToLevelTypeID, ToLevelTypeID, Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "CCLevelTypeID", mode, rowData(0).CCLevelTypeID, CCLevelTypeID, Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "BCCLevelTypeID", mode, rowData(0).BCCLevelTypeID, BCCLevelTypeID, Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "Subject", mode, rowData(0).Subject, Subject, Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "Body", mode, rowData(0).Body, Body, Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "IncludeAttachment", mode, rowData(0).IncludeAttachment, IncludeAttachment, Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "CreatedDate", mode, rowData(0).CreatedDate, Now.ToString("dd-MMMM-yyyy HH:mm"), Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "LastUpdateDate", mode, rowData(0).LastUpdateDate, Now.ToString("dd-MMMM-yyyy HH:mm"), Action)
                    End Using
                End If
            End Using
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class