#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsBidangUsaha_APPROVAL_Detail
    Inherits Parent

#Region "Function"

    Sub HideControl()
        imgReject.Visible = False
        imgApprove.Visible = False
        ImageCancel.Visible = False
    End Sub

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsBidangUsaha_Approval_View.aspx")
    End Sub

    Protected Sub imgReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgReject.Click
        Try
            Rejected()
            ChangeMultiView(1)
            LblConfirmation.Text = "Data has been rejected"
            HideControl()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgApprove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgApprove.Click
        Try
            Using ObjMsBidangUsaha_Approval As MsBidangUsaha_Approval = DataRepository.MsBidangUsaha_ApprovalProvider.GetByPK_MsBidangUsaha_Approval_Id(parID)
                If ObjMsBidangUsaha_Approval.FK_MsMode_Id.GetValueOrDefault = 1 Then
                    Inserdata(ObjMsBidangUsaha_Approval)
                ElseIf ObjMsBidangUsaha_Approval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                    UpdateData(ObjMsBidangUsaha_Approval)
                ElseIf ObjMsBidangUsaha_Approval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                    DeleteData(ObjMsBidangUsaha_Approval)
                End If
            End Using
            ChangeMultiView(1)
            LblConfirmation.Text = "Data approved successful"
            HideControl()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                ListmapingNew = New TList(Of MappingBidangUsahaNCBS_Approval_Detail)
                ListmapingOld = New TList(Of MappingBidangUsahaNCBS)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim PkObject As String
        'Load new data
        Using ObjMsBidangUsaha_ApprovalDetail As MsBidangUsaha_ApprovalDetail = DataRepository.MsBidangUsaha_ApprovalDetailProvider.GetPaged(MsBidangUsaha_ApprovalDetailColumn.FK_MsBidangUsaha_Approval_Id.ToString & _
         "=" & _
         parID, "", 0, 1, Nothing)(0)
            Dim CekMode As MsBidangUsaha_Approval = DataRepository.MsBidangUsaha_ApprovalProvider.GetByPK_MsBidangUsaha_Approval_Id(ObjMsBidangUsaha_ApprovalDetail.FK_MsBidangUsaha_Approval_Id)
            Dim nama As String = CekMode.FK_MsMode_Id
            If nama = 1 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 2 Then
                baru.Visible = True
                lama.Visible = True
                baruNew.Visible = True
                lamaOld.Visible = True
            ElseIf nama = 3 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 4 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            End If
            With ObjMsBidangUsaha_ApprovalDetail
                SafeDefaultValue = "-"
                txtIdBidangUsahanew.Text = Safe(.IdBidangUsaha)
                txtNamaBidangUsahanew.Text = Safe(.NamaBidangUsaha)


                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByNew.Text = Omsuser(0).UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyNew.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
                lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
                txtActivationNew.Text = SafeActiveInactive(.Activation)
                Dim L_objMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail As TList(Of MappingBidangUsahaNCBS_Approval_Detail)
                L_objMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail = DataRepository.MappingBidangUsahaNCBS_Approval_DetailProvider.GetPaged(MappingBidangUsahaNCBS_Approval_DetailColumn.PK_MsBidangUsaha_Approval_id.ToString & _
     "=" & _
     parID, "", 0, Integer.MaxValue, Nothing)
                'Add mapping
                ListmapingNew.AddRange(L_objMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail)

            End With
            PkObject = ObjMsBidangUsaha_ApprovalDetail.IdBidangUsaha
        End Using

        'Load Old Data
        Using ObjMsBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(PkObject)
            If ObjMsBidangUsaha Is Nothing Then Return
            With ObjMsBidangUsaha
                SafeDefaultValue = "-"
                txtIdBidangUsahaOld.Text = Safe(.IdBidangUsaha)
                txtNamaBidangUsahaOld.Text = Safe(.NamaBidangUsaha)


                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByOld.Text = Omsuser(0).UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyOld.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
                lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
                Dim L_objMappingBidangUsahaNCBS As TList(Of MappingBidangUsahaNCBS)
                txtActivationOld.Text = SafeActiveInactive(.Activation)
                L_objMappingBidangUsahaNCBS = DataRepository.MappingBidangUsahaNCBSProvider.GetPaged(MappingBidangUsahaNCBSColumn.BidangUsahaId.ToString & _
                 "=" & _
                 PkObject, "", 0, Integer.MaxValue, Nothing)
                ListmapingOld.AddRange(L_objMappingBidangUsahaNCBS)
            End With
        End Using
    End Sub



    Protected Sub imgBtnConfirmation_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnConfirmation.Click
        Try
            Response.Redirect("MsBidangUsaha_approval_view.aspx")
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping New sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingNew() As TList(Of MappingBidangUsahaNCBS_Approval_Detail)
        Get
            Return Session("ListmapingNew.data")
        End Get
        Set(ByVal value As TList(Of MappingBidangUsahaNCBS_Approval_Detail))
            Session("ListmapingNew.data") = value
        End Set
    End Property

    ''' <summary>
    ''' Menyimpan Item untuk mapping Old sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingOld() As TList(Of MappingBidangUsahaNCBS)
        Get
            Return Session("ListmapingOld.data")
        End Get
        Set(ByVal value As TList(Of MappingBidangUsahaNCBS))
            Session("ListmapingOld.data") = value
        End Set
    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingBidangUsahaNCBS_Approval_Detail In ListmapingNew.FindAllDistinct("IDBidangUsahaNCBS")
                Temp.Add(i.IDBidangUsahaNCBS.ToString & "-" & i.NAMA)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingBidangUsahaNCBS In ListmapingOld.FindAllDistinct("IDBidangUsahaNCBS")
                Temp.Add(i.IDBidangUsahaNCBS.ToString & "-" & i.nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

#Region "Action Approval..."

    Sub Rejected()
        Dim ObjMsBidangUsaha_Approval As MsBidangUsaha_Approval = DataRepository.MsBidangUsaha_ApprovalProvider.GetByPK_MsBidangUsaha_Approval_Id(parID)
        Dim ObjMsBidangUsaha_ApprovalDetail As MsBidangUsaha_ApprovalDetail = DataRepository.MsBidangUsaha_ApprovalDetailProvider.GetPaged(MsBidangUsaha_ApprovalDetailColumn.FK_MsBidangUsaha_Approval_Id.ToString & "=" & ObjMsBidangUsaha_Approval.PK_MsBidangUsaha_Approval_Id, "", 0, 1, Nothing)(0)
        Dim L_ObjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail As TList(Of MappingBidangUsahaNCBS_Approval_Detail) = DataRepository.MappingBidangUsahaNCBS_Approval_DetailProvider.GetPaged(MappingBidangUsahaNCBS_Approval_DetailColumn.PK_MsBidangUsaha_Approval_id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)
        DeleteApproval(ObjMsBidangUsaha_Approval, ObjMsBidangUsaha_ApprovalDetail, L_ObjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail)
    End Sub

    Sub DeleteApproval(ByRef objMsBidangUsaha_Approval As MsBidangUsaha_Approval, ByRef ObjMsBidangUsaha_ApprovalDetail As MsBidangUsaha_ApprovalDetail, ByRef L_ObjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail As TList(Of MappingBidangUsahaNCBS_Approval_Detail))
        DataRepository.MsBidangUsaha_ApprovalProvider.Delete(objMsBidangUsaha_Approval)
        DataRepository.MsBidangUsaha_ApprovalDetailProvider.Delete(ObjMsBidangUsaha_ApprovalDetail)
        DataRepository.MappingBidangUsahaNCBS_Approval_DetailProvider.Delete(L_ObjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail)
    End Sub

    ''' <summary>
    ''' Delete Data dari  asli dari approval
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DeleteData(ByVal objMsBidangUsaha_Approval As MsBidangUsaha_Approval)
        '   '================= Delete Header ===========================================================
        Dim LObjMsBidangUsaha_ApprovalDetail As TList(Of MsBidangUsaha_ApprovalDetail) = DataRepository.MsBidangUsaha_ApprovalDetailProvider.GetPaged(MsBidangUsaha_ApprovalDetailColumn.FK_MsBidangUsaha_Approval_Id.ToString & "=" & objMsBidangUsaha_Approval.PK_MsBidangUsaha_Approval_Id, "", 0, 1, Nothing)
        If LObjMsBidangUsaha_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        Dim ObjMsBidangUsaha_ApprovalDetail As MsBidangUsaha_ApprovalDetail = LObjMsBidangUsaha_ApprovalDetail(0)
        Using ObjNewMsBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(ObjMsBidangUsaha_ApprovalDetail.IdBidangUsaha)
            DataRepository.MsBidangUsahaProvider.Delete(ObjNewMsBidangUsaha)
        End Using

        '======== Delete MAPPING =========================================
        Dim L_ObjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail As TList(Of MappingBidangUsahaNCBS_Approval_Detail) = DataRepository.MappingBidangUsahaNCBS_Approval_DetailProvider.GetPaged(MappingBidangUsahaNCBS_Approval_DetailColumn.PK_MsBidangUsaha_Approval_id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


        For Each c As MappingBidangUsahaNCBS_Approval_Detail In L_ObjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail
            Using ObjNewMappingBidangUsahaNCBS_Approval_Detail As MappingBidangUsahaNCBS_Approval_Detail = DataRepository.MappingBidangUsahaNCBS_Approval_DetailProvider.GetByPK_MappingBidangUsahaNCBS_Approval_detail_id(c.PK_MappingBidangUsahaNCBS_Approval_detail_id)
                DataRepository.MappingBidangUsahaNCBS_Approval_DetailProvider.Delete(ObjNewMappingBidangUsahaNCBS_Approval_Detail)
            End Using
        Next

        '======== Delete Approval data ======================================
        DeleteApproval(objMsBidangUsaha_Approval, ObjMsBidangUsaha_ApprovalDetail, L_ObjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail)
    End Sub

    ''' <summary>
    ''' Update Data dari approval ke table asli 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateData(ByVal objMsBidangUsaha_Approval As MsBidangUsaha_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
        '   '================= Update Header ===========================================================
        Dim LObjMsBidangUsaha_ApprovalDetail As TList(Of MsBidangUsaha_ApprovalDetail) = DataRepository.MsBidangUsaha_ApprovalDetailProvider.GetPaged(MsBidangUsaha_ApprovalDetailColumn.FK_MsBidangUsaha_Approval_Id.ToString & "=" & objMsBidangUsaha_Approval.PK_MsBidangUsaha_Approval_Id, "", 0, 1, Nothing)
        If LObjMsBidangUsaha_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        Dim ObjMsBidangUsaha_ApprovalDetail As MsBidangUsaha_ApprovalDetail = LObjMsBidangUsaha_ApprovalDetail(0)
        Dim ObjMsBidangUsahaNew As MsBidangUsaha
        Using ObjMsBidangUsahaOld As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(ObjMsBidangUsaha_ApprovalDetail.IdBidangUsaha)
            If ObjMsBidangUsahaOld Is Nothing Then Throw New Exception("Data Not Found")
            ObjMsBidangUsahaNew = ObjMsBidangUsahaOld.Clone
            With ObjMsBidangUsahaNew
                FillOrNothing(.NamaBidangUsaha, ObjMsBidangUsaha_ApprovalDetail.NamaBidangUsaha)
                FillOrNothing(.CreatedBy, ObjMsBidangUsaha_ApprovalDetail.CreatedBy)
                FillOrNothing(.CreatedDate, ObjMsBidangUsaha_ApprovalDetail.CreatedDate)
                FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
                FillOrNothing(.ApprovedDate, Date.Now)
            End With
            DataRepository.MsBidangUsahaProvider.Save(ObjMsBidangUsahaNew)
            'Insert auditrail
            InsertAuditTrail()
            '======== Update MAPPING =========================================
            'delete mapping item
            Using L_ObjMappingBidangUsahaNCBS As TList(Of MappingBidangUsahaNCBS) = DataRepository.MappingBidangUsahaNCBSProvider.GetPaged(MappingBidangUsahaNCBSColumn.BidangUsahaId.ToString & _
    "=" & _
    ObjMsBidangUsahaNew.IdBidangUsaha, "", 0, Integer.MaxValue, Nothing)
                DataRepository.MappingBidangUsahaNCBSProvider.Delete(L_ObjMappingBidangUsahaNCBS)
            End Using
            'Insert mapping item
            Dim L_ObjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail As TList(Of MappingBidangUsahaNCBS_Approval_Detail) = DataRepository.MappingBidangUsahaNCBS_Approval_DetailProvider.GetPaged(MappingBidangUsahaNCBS_Approval_DetailColumn.PK_MsBidangUsaha_Approval_id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            Dim L_ObjNewMappingBidangUsahaNCBS As New TList(Of MappingBidangUsahaNCBS)
            For Each c As MappingBidangUsahaNCBS_Approval_Detail In L_ObjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail
                Dim ObjNewMappingBidangUsahaNCBS As New MappingBidangUsahaNCBS
                With ObjNewMappingBidangUsahaNCBS
                    FillOrNothing(.BidangUsahaId, ObjMsBidangUsahaNew.IdBidangUsaha)
                    FillOrNothing(.Nama, c.NAMA)
                    FillOrNothing(.IDBidangUsahaNCBS, c.IDBidangUsahaNCBS)
                    L_ObjNewMappingBidangUsahaNCBS.Add(ObjNewMappingBidangUsahaNCBS)
                End With
            Next
            DataRepository.MappingBidangUsahaNCBSProvider.Save(L_ObjNewMappingBidangUsahaNCBS)
            '======== Delete Approval data ======================================
            DeleteApproval(objMsBidangUsaha_Approval, ObjMsBidangUsaha_ApprovalDetail, L_ObjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail)
        End Using
    End Sub
    ''' <summary>
    ''' Insert Data dari approval ke table asli 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Inserdata(ByVal objMsBidangUsaha_Approval As MsBidangUsaha_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
        '   '================= Save Header ===========================================================
        Dim LObjMsBidangUsaha_ApprovalDetail As TList(Of MsBidangUsaha_ApprovalDetail) = DataRepository.MsBidangUsaha_ApprovalDetailProvider.GetPaged(MsBidangUsaha_ApprovalDetailColumn.FK_MsBidangUsaha_Approval_Id.ToString & "=" & objMsBidangUsaha_Approval.PK_MsBidangUsaha_Approval_Id, "", 0, 1, Nothing)
        'jika data tidak ada di database
        If LObjMsBidangUsaha_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        'binding approval ke table asli
        Dim ObjMsBidangUsaha_ApprovalDetail As MsBidangUsaha_ApprovalDetail = LObjMsBidangUsaha_ApprovalDetail(0)
        Using ObjNewMsBidangUsaha As New MsBidangUsaha()
            With ObjNewMsBidangUsaha
                FillOrNothing(.IdBidangUsaha, ObjMsBidangUsaha_ApprovalDetail.IdBidangUsaha)
                FillOrNothing(.NamaBidangUsaha, ObjMsBidangUsaha_ApprovalDetail.NamaBidangUsaha)
                FillOrNothing(.CreatedBy, ObjMsBidangUsaha_ApprovalDetail.CreatedBy)
                FillOrNothing(.CreatedDate, ObjMsBidangUsaha_ApprovalDetail.CreatedDate)
                FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
                FillOrNothing(.ApprovedDate, Date.Now)
                .Activation = True
            End With
            DataRepository.MsBidangUsahaProvider.Save(ObjNewMsBidangUsaha)
            'Insert auditrail
            InsertAuditTrail()
            '======== Insert MAPPING =========================================
            Dim L_ObjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail As TList(Of MappingBidangUsahaNCBS_Approval_Detail) = DataRepository.MappingBidangUsahaNCBS_Approval_DetailProvider.GetPaged(MappingBidangUsahaNCBS_Approval_DetailColumn.PK_MsBidangUsaha_Approval_id.ToString & _
    "=" & _
    parID, "", 0, Integer.MaxValue, Nothing)
            Dim L_ObjNewMappingBidangUsahaNCBS As New TList(Of MappingBidangUsahaNCBS)
            For Each c As MappingBidangUsahaNCBS_Approval_Detail In L_ObjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail
                Dim ObjNewMappingBidangUsahaNCBS As New MappingBidangUsahaNCBS
                With ObjNewMappingBidangUsahaNCBS
                    FillOrNothing(.BidangUsahaId, ObjNewMsBidangUsaha.IdBidangUsaha)
                    FillOrNothing(.Nama, c.NAMA)
                    FillOrNothing(.IDBidangUsahaNCBS, c.IDBidangUsahaNCBS)
                    L_ObjNewMappingBidangUsahaNCBS.Add(ObjNewMappingBidangUsahaNCBS)
                End With
            Next
            DataRepository.MappingBidangUsahaNCBSProvider.Save(L_ObjNewMappingBidangUsahaNCBS)
            '======== Delete Approval data ======================================
            DeleteApproval(objMsBidangUsaha_Approval, ObjMsBidangUsaha_ApprovalDetail, L_ObjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail)
        End Using
    End Sub

#End Region

    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(8)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBidangUsaha", "ID", "Add", txtIdBidangUsahaOld.Text, txtIdBidangUsahanew.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBidangUsaha", "Nama", "Add", txtNamaBidangUsahaOld.Text, txtNamaBidangUsahanew.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBidangUsaha", "Activation", "Add", txtActivationOld.Text, txtActivationNew.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBidangUsaha", "Mapping Item", "Add", LBmappingOld.Text, LBMappingNew.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBidangUsaha", "Created Date", "Add", lblCreatedDateOld.Text, lblCreatedDateNew.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBidangUsaha", "Created By", "Add", lblCreatedByOld.Text, lblCreatedByNew.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBidangUsaha", "Updated Date", "Add", lblUpdatedDateOld.Text, lblUpdatedDateNew.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBidangUsaha", "Updated By", "Add", lblUpdatedbyOld.Text, lblUpdatedbyNew.Text, "Accepted")
            End Using
        Catch
            Throw
        End Try
    End Sub

End Class


