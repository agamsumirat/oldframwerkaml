<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CDDLNPMessage.aspx.vb" Inherits="CDDLNPMessage" Title="CDDLNP Message" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table id="Table2" bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" style="position: static; width: 100%; border-top-style: none;
        border-right-style: none; border-left-style: none; border-bottom-style: none;">
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="2" align="center">
                <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                    style="position: static" width="100%">
                    <tr class="formText">
                        <td align="center" bgcolor="#ffffff" rowspan="2">
                            <asp:Label ID="lblMessageID" runat="server"></asp:Label><br />
                            <br />
                            <asp:Label ID="lblMessage" runat="server"></asp:Label><br />
                            <br />
                        </td>
                    </tr>
                    <tr class="formText">
                    </tr>
                    <tr bgcolor="#dddddd" class="formText" height="30" align="center">
                        <td colspan="3" style="width: 100%" align="center">
                            <asp:ImageButton ID="ImageButtonOK" runat="server" SkinID="OKButton" />
                            <br />
                            <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
