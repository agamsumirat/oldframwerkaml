Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Public Class AccountOwnerWorkingUnitMappingDelete
    Inherits Parent

    Private AccountOwnerName As String

#Region " Property "
    ''' <summary>
    ''' get focus id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.LabelAccountOwnerName.ClientID
        End Get
    End Property

    ''' <summary>
    ''' GetAccountOwnerId()
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetAccountOwnerId() As String
        Get
            Return Me.Request.Params("AccountOwnerId")
        End Get
    End Property
#End Region

#Region " Delete Account Owner Working Unit Mapping dan Audit trail"
    Private Sub DeleteAccountOwnerWorkingUnitMappingBySU()
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            Using AccessAccountOwnerWorkingUnitMapping As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMappingTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAccountOwnerWorkingUnitMapping, Data.IsolationLevel.ReadUncommitted)
                AccessAccountOwnerWorkingUnitMapping.DeleteAccountOwnerWorkingUnitMapping(Me.GetAccountOwnerId)
            End Using

            'Ubah AccountOwnerWorkingUnitMappingStatus dari True menjadi False 
            Using AccessAccountOwner As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAccountOwner, oSQLTrans)
                AccessAccountOwner.UpdateAccountOwnerWorkingUnitMappingStatus(Me.GetAccountOwnerId, "False")
            End Using

            oSQLTrans.Commit()
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    Private Sub InsertAuditTrail()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using AccessAccountOwnerWorkingUnitMapping As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMappingTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAccountOwnerWorkingUnitMapping, Data.IsolationLevel.ReadUncommitted)
                Using dt As Data.DataTable = AccessAccountOwnerWorkingUnitMapping.GetDataByAccountOwnerId(Me.GetAccountOwnerId)
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
                    Dim RowData() As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMappingRow = dt.Select("AccountOwnerId='" & CInt(Me.GetAccountOwnerId) & "'")
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "AccountOwnerID", "Delete", "", RowData(0).AccountOwnerID, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "WorkingUnitID", "Delete", "", RowData(0).WorkingUnitID, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "CreatedDate", "Delete", "", RowData(0).CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AccountOwnerWorkingUnitMapping", "LastUpdateDate", "Delete", "", RowData(0).LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "AccountOwner", "AccountOwnerWorkingUnitMappingStatus", "Edit", "True", "False", "Accepted")
                    End Using
                    oSQLTrans.Commit()
                End Using
            End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
#End Region


    ''' <summary>
    ''' fill group
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroupWorkingUnit()
        Using AccessAccountOwnerWorkingUnitMapping As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMappingTableAdapter
            Dim objTable As Data.DataTable = AccessAccountOwnerWorkingUnitMapping.GetDataByAccountOwnerId(Me.GetAccountOwnerId)
            Dim objRow As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMappingRow = objTable.Rows(0)

            Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                Dim objTable2 As Data.DataTable = AccessWorkingUnit.GetDataByWorkingUnitID(objRow.WorkingUnitID)
                Dim objRow2 As AMLDAL.AMLDataSet.WorkingUnitRow = objTable2.Rows(0)

                Me.LabelWorkingUnitName.Text = objRow2.WorkingUnitID & " - " & objRow2.WorkingUnitName
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get data login parameter
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	03/07/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GetData()
        Try
            Me.LabelAccountOwnerName.Text = Me.GetAccountOwnerId
            Using AccessAccountOwner As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
                Dim objAccountOwnerTable As Data.DataTable = AccessAccountOwner.GetDataByAccountOwnerId(Me.GetAccountOwnerId)
                Dim objAccountOwnerRow As AMLDAL.AMLDataSet.AccountOwnerRow = objAccountOwnerTable.Rows(0)
                Me.AccountOwnerName = objAccountOwnerRow.AccountOwnerName
                Me.LabelAccountOwnerName.Text &= " - " & objAccountOwnerRow.AccountOwnerName

                If objAccountOwnerRow.AccountOwnerWorkingUnitMappingStatus Then
                    Using AccessAccountOwnerWorkingUnitMapping As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMappingTableAdapter
                        Using objtable As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMappingDataTable = AccessAccountOwnerWorkingUnitMapping.GetDataByAccountOwnerId(Me.GetAccountOwnerId)
                            Dim objrow As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMappingRow = objtable.Rows(0)

                            ViewState("AccountOwnerID_Old") = CType(objrow.AccountOwnerID, String)
                            ViewState("WorkingUnitID_Old") = CType(objrow.WorkingUnitID, Integer)
                            ViewState("CreatedDate_Old") = CType(objrow.CreatedDate, Date)
                            ViewState("LastUpdateDate_Old") = CType(objrow.LastUpdateDate, Date)
                        End Using
                    End Using
                Else
                    'do nothing
                    'ViewState("WorkingUnitID_Old") = Me.DropDownListWorkingUnit.SelectedValue
                End If
            End Using
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' CheckCanBeDeleted()
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckCanBeDeleted() As Boolean
        Using AccountOwner As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
            Dim objTable As Data.DataTable = AccountOwner.GetDataByAccountOwnerId(Me.GetAccountOwnerId)

            If objTable.Rows.Count > 0 Then
                Dim objRow As AMLDAL.AMLDataSet.AccountOwnerRow = objTable.Rows(0)

                'Bila AccountOwnerWorkingUnitMappingStatus = True berarti bisa didelete 
                If objRow.AccountOwnerWorkingUnitMappingStatus Then
                    Return True
                Else 'Bila AccountOwnerWorkingUnitMappingStatus = False berarti tidak bisa didelete 
                    Return False
                End If
            Else
                Throw New Exception("Cannot load data because it is no longer in the database.")
            End If
        End Using
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load page handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Me.CheckCanBeDeleted Then
                If Not Me.IsPostBack Then
                    Me.FillGroupWorkingUnit()
                    Me.GetData()
                End If

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            Else
                Throw New Exception("Cannot delete Account Owner Working Unit Mapping for the following Account Owner : " & Me.AccountOwnerName & " because it hasn't been mapped to any Working Unit yet.")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cancel event handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "AccountOwnerWorkingUnitMappingView.aspx"

        Me.Response.Redirect("AccountOwnerWorkingUnitMappingView.aspx", False)
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' save handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>    
    ''' </remarks>
    ''' <history>
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Periksa apakah User melakukan perubahan Working Unit Mapping
            'If Me.DropDownListWorkingUnit.SelectedValue <> ViewState("WorkingUnitID_Old") Then
            'Periksa apakah Account Owner tsb statusnya dlm pending approval atau tdk
            If Me.CekIsApproval(Me.GetAccountOwnerId) Then
                Throw New Exception("Cannot add Account Owner Working Unit Mapping for the following Account Owner : " & Me.AccountOwnerName & " because it is currently waiting for approval.")
            Else 'Jika tdk dlm status pending approval
                'Periksa apakah AccountOwnerWorkingUnitMapping untuk AccountOwner tsb bisa didelete atau tdk
                If Me.CheckCanBeDeleted() Then
                    'Jika SuperUser yg melakukan penambahan/perubahan
                    If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                        Me.InsertAuditTrail()
                        Me.DeleteAccountOwnerWorkingUnitMappingBySU()
                        Me.LblSuccess.Text = "The Account Working Unit Mapping has been deleted."
                        Me.LblSuccess.Visible = True
                    Else 'Jika bukan SuperUser yg melakukan penghapusan
                        Using AccessAccountOwner As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
                            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAccountOwner, Data.IsolationLevel.ReadUncommitted)
                            Using AccountOwnerTable As AMLDAL.AMLDataSet.AccountOwnerDataTable = AccessAccountOwner.GetDataByAccountOwnerId(Me.GetAccountOwnerId)
                                Dim AccountOwnerRow As AMLDAL.AMLDataSet.AccountOwnerRow = AccountOwnerTable.Rows(0)

                                Using AccessAccountOwnerWorkingUnitMapping As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMappingTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAccountOwnerWorkingUnitMapping, oSQLTrans)
                                    Using AccessApproval As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMapping_ApprovalTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApproval, oSQLTrans)
                                        'Dim AccountOwnerWorkingUnitMappingRow As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMappingRow = AccessAccountOwnerWorkingUnitMapping.GetDataByAccountOwnerId(Me.GetAccountOwnerId).Rows(0)

                                        Dim AccountWorkingUnitMappingApprovalID As Int64
                                        Dim ModeID As Int16 = 3 'Mode = 3 (Delete)

                                        Dim objTable As Data.DataTable = AccessAccountOwnerWorkingUnitMapping.GetDataByAccountOwnerId(Me.GetAccountOwnerId)

                                        'Jika objTable.Rows.Count > 0 berarti AccountOwnerWorkingUnitMapping utk AccountOwner tsb memang masih ada dlm tabel AccountOwnerWorkingUnitMapping
                                        If objTable.Rows.Count > 0 Then
                                            'Dim objRow As AMLDAL.AMLDataSet.AccountOwnerWorkingUnitMappingRow = objTable.Rows(0)
                                            Dim AccountOwnerID_Old = ViewState("AccountOwnerID_Old")
                                            Dim WorkingUnitID_Old As Int32 = ViewState("WorkingUnitID_Old")
                                            Dim CreatedDate_Old As DateTime = ViewState("CreatedDate_Old")
                                            Dim LastUpdate_Old As DateTime = ViewState("LastUpdateDate_Old")

                                            AccountWorkingUnitMappingApprovalID = AccessApproval.InsertAccountOwnerWorkingUnitMapping_Approval(AccountOwnerID_Old, WorkingUnitID_Old, CreatedDate_Old, Now, AccountOwnerID_Old, WorkingUnitID_Old, CreatedDate_Old, LastUpdate_Old)

                                            Using ParametersPending As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                                                Sahassa.AML.TableAdapterHelper.SetTransaction(ParametersPending, oSQLTrans)
                                                'ParametersPendingApprovalID adalah primary key dari tabel Parameters_PendingApproval yang sifatnya identity dan baru dibentuk setelah row baru dibuat 
                                                Dim ParametersPendingApprovalID As Int64 = ParametersPending.InsertParameters_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, ModeID)

                                                Using ParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                                                    Sahassa.AML.TableAdapterHelper.SetTransaction(ParametersApproval, oSQLTrans)
                                                    'ParameterItemID 9 = AccountOwnerWorkingUnitMapping
                                                    ParametersApproval.Insert(ParametersPendingApprovalID, ModeID, 9, AccountWorkingUnitMappingApprovalID)
                                                End Using
                                            End Using
                                        Else 'Jika objTable.Rows.Count = 0 berarti AccountOwnerWorkingUnitMapping utk AccountOwner tsb memang sdh tdk ada lg dlm tabel AccountOwnerWorkingUnitMapping
                                            Throw New Exception("Cannot delete the Account Owner Working Unit Mapping for the following Account Owner : " & Me.AccountOwnerName & " because it is no longer in the database.")
                                        End If
                                    End Using
                                End Using
                            End Using
                        End Using

                        oSQLTrans.Commit()

                        Dim MessagePendingID As Integer = 8873 'MessagePendingID 8873 = AccountOwnerWorkingUnitMapping Delete 

                        Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & AccountOwnerName

                        Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & AccountOwnerName, False)
                        'End Using
                    End If
                Else
                    Throw New Exception("Cannot delete Account Owner Working Unit Mapping for the following Account Owner : " & Me.AccountOwnerName & " because it hasn't been mapped to any Working Unit yet.")
                End If
            End If
            'Else 'Jika User tidak melakukan perubahan Working Unit Mapping
            'do nothing
            'End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub


    ''' <summary>
    ''' cek is approval
    ''' </summary>
    ''' <param name="strUserId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CekIsApproval(ByVal AccountOwnerId As String) As Boolean
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerWorkingUnitMapping_ApprovalTableAdapter
            Dim count As Int32 = AccessPending.CountAccountOwnerWorkingUnitMappingApproval(AccountOwnerId)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function
End Class