
Partial Class TransactionTypeAuxiliaryMapping
    Inherits Parent

#Region "Set Session"

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("TTAMappingPageSelected") Is Nothing, New ArrayList, Session("TTAMappingPageSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("TTAMappingPageSelected") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("TTAMappingPageSort") Is Nothing, "vw_AuxTransactionCode.TransactionCode asc", Session("TTAMappingPageSort"))
        End Get
        Set(ByVal Value As String)
            Session("TTAMappingPageSort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("TTAMappingCurrentPage") Is Nothing, 0, Session("TTAMappingCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("TTAMappingCurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("TTAMappingRowTotal") Is Nothing, 0, Session("TTAMappingRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("TTAMappingRowTotal") = Value
        End Set
    End Property
    Private Property SetAndGetSearchingCriteria() As String
        Get
            Return IIf(Session("TTAMappingSearchCriteria") Is Nothing, "", Session("TTAMappingSearchCriteria"))
        End Get
        Set(ByVal Value As String)
            Session("TTAMappingSearchCriteria") = Value
        End Set
    End Property
    Private Function SetAllSearchingCriteria() As String
        'Dim objErrorMessage As New ORMsController.ErrorMessage
        Dim StrSearch As String = ""
        Try
            If Me.TxtAuxTransCode.Text <> "" Then
                If StrSearch = "" Then
                    StrSearch = StrSearch & " TransactionCode like '%" & Me.TxtAuxTransCode.Text.Replace("'", "''") & "%' "
                Else
                    StrSearch = StrSearch & " And TransactionCode like '%" & Me.TxtAuxTransCode.Text.Replace("'", "''") & "%' "
                End If
            End If

            If Me.TxtAuxTransDescription.Text <> "" Then
                If StrSearch = "" Then
                    StrSearch = StrSearch & " TransactionDescription like '%" & Me.TxtAuxTransDescription.Text.Replace("'", "''") & "%' "
                Else
                    StrSearch = StrSearch & " And TransactionDescription like '%" & Me.TxtAuxTransDescription.Text.Replace("'", "''") & "%' "
                End If
            End If

            If Me.TxtTransType.Text <> "" Then
                If StrSearch = "" Then
                    StrSearch = StrSearch & " TransactionTypeName like '%" & Me.TxtTransType.Text.Replace("'", "''") & "%' "
                Else
                    StrSearch = StrSearch & " And TransactionTypeName like '%" & Me.TxtTransType.Text.Replace("'", "''") & "%' "
                End If
            End If
            Return StrSearch
        Catch
            Throw
            Return ""
        End Try
    End Function
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("TTAMappingViewData") Is Nothing, Sahassa.AML.Commonly.GetDatasetPaging(Me.TablesRelated, Me.Pk, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, 10, Me.Fields, Me.SetAndGetSearchingCriteria, ""), Session("TTAMappingViewData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TTAMappingViewData") = value
        End Set
    End Property
#End Region

    Private Sub ClearThisPageSessions()
        Session("TTAMappingPageSelected") = Nothing
        Session("TTAMappingPageSort") = Nothing
        Session("TTAMappingCurrentPage") = Nothing
        Session("TTAMappingRowTotal") = Nothing
        Session("TTAMappingSearchCriteria") = Nothing
        Session("TTAMappingViewData") = Nothing
    End Sub

#Region "Set Property For Table Bind Grid"
    Private ReadOnly Property TablesRelated() As String
        Get
            Return "vw_AuxTransactionCode left join TransactionTypeAuxiliaryTransactionCodeMapping " _
                 & "ON vw_AuxTransactionCode.TransactionCode=TransactionTypeAuxiliaryTransactionCodeMapping.AuxiliaryTransactionCode " _
                 & "left join TransactionType ON TransactionTypeAuxiliaryTransactionCodeMapping.TransactionTypeId=TransactionType.TransactionTypeId"
        End Get
    End Property
    Private ReadOnly Property Pk() As String
        Get
            Return "vw_AuxTransactionCode.TransactionCode"
        End Get
    End Property
    Private ReadOnly Property Fields() As String
        Get
            Return "vw_AuxTransactionCode.TransactionCode, vw_AuxTransactionCode.TransactionDescription, TransactionType.TransactionTypeID, " _
                 & "TransactionType.TransactionTypeName"
        End Get
    End Property
#End Region

#Region "Bind Grid View"
    Public Sub BindGrid()
        Me.SetnGetBindTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.TablesRelated, Me.Pk, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.Fields, Me.SetAndGetSearchingCriteria, "")
        Me.GridView.DataSource = Me.SetnGetBindTable
        Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCount(Me.TablesRelated, Me.SetAndGetSearchingCriteria)
        Me.GridView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridView.DataBind()
    End Sub
#End Region

#Region "Paging Event"
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
#Region "Sorting Event"

    Public Function CheckDataPendingApprovalStatus(ByVal StrAuxTransCode As String) As Boolean
        Using ApprovalTableAdapter As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.QueriesTableAdapter
            Dim count As Int32 = ApprovalTableAdapter.CekStatusTransactionTypeAuxiliaryTransactionCodeMapping_PendingApproval(StrAuxTransCode)

            If count = 0 Then
                Return False
            Else
                Return True
            End If
        End Using
    End Function

    Protected Sub GridView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridView.DeleteCommand
        Dim StrAuxTrxCode As String = e.Item.Cells(1).Text
        Dim StrAuxTrxDescription As String = e.Item.Cells(2).Text
        Dim StrTransId As String = e.Item.Cells(3).Text
        Dim StrTransName As String = e.Item.Cells(4).Text

        Try
            If StrTransId = "&nbsp;" Then
                Throw New Exception("Cannot delete the following data: '" & StrAuxTrxCode & " - " & StrAuxTrxDescription & "' because it has not been mapped.")
            End If
            If Me.CheckDataPendingApprovalStatus(StrAuxTrxCode) = True Then
                Throw New Exception("Cannot delete the following data: '" & StrAuxTrxCode & " - " & StrAuxTrxDescription & "' because it is currently waiting for approval.")
            End If
            Sahassa.AML.Commonly.SessionIntendedPage = "TransactionTypeAuxiliaryMappingDelete.aspx"
            Response.Redirect("TransactionTypeAuxiliaryMappingDelete.aspx?AuxTransCode=" & StrAuxTrxCode & "&AuxTransCodeDesc=" & StrAuxTrxDescription & "&TransactionTypeId=" & StrTransId & "&TransactionTypeName=" & StrTransName, False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridView.EditCommand
        Dim StrAuxTrxCode As String = e.Item.Cells(1).Text
        Dim StrAuxTrxDescription As String = e.Item.Cells(2).Text
        Dim StrTransId As String = e.Item.Cells(3).Text

        Try
            If Me.CheckDataPendingApprovalStatus(StrAuxTrxCode) = True Then
                Throw New Exception("Cannot edit the following data: '" & StrAuxTrxCode & " - " & StrAuxTrxDescription & "' because it is currently waiting for approval.")
            End If
            Sahassa.AML.Commonly.SessionIntendedPage = "TransactionTypeAuxiliaryMappingEdit.aspx"
            Response.Redirect("TransactionTypeAuxiliaryMappingEdit.aspx?AuxTransCode=" & StrAuxTrxCode & "&AuxTransCodeDesc=" & StrAuxTrxDescription & "&TransactionTypeId=" & StrTransId, False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridView.SortCommand
        Dim GridRETBaselViewPage As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridRETBaselViewPage.Columns(Sahassa.AML.Commonly.IndexSort(GridView, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
#Region "Excel Export Event"
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim RETBaselPkId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(RETBaselPkId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(RETBaselPkId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(RETBaselPkId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls
    Private Sub BindSelected()
        Dim Rows As New ArrayList
        Dim oTableTrans As New Data.DataTable
        Dim oRowTrans As Data.DataRow
        oTableTrans.Columns.Add("TransactionCode", GetType(String))
        oTableTrans.Columns.Add("TransactionDescription", GetType(String))
        oTableTrans.Columns.Add("TransactionTypeID", GetType(String))
        oTableTrans.Columns.Add("TransactionTypeName", GetType(String))
        For Each IdPk As String In Me.SetnGetSelectedItem
            Dim rowData() As Data.DataRow = Me.SetnGetBindTable.Select("TransactionCode = '" & IdPk & "'")
            If rowData.Length > 0 Then
                oRowTrans = oTableTrans.NewRow
                oRowTrans(0) = rowData(0)("TransactionCode")
                oRowTrans(1) = rowData(0)("TransactionDescription")
                oRowTrans(2) = rowData(0)("TransactionTypeID")
                oRowTrans(3) = rowData(0)("TransactionTypeName")
                oTableTrans.Rows.Add(oRowTrans)
            End If
        Next
        Me.GridView.DataSource = oTableTrans
        Me.GridView.AllowPaging = False
        Me.GridView.DataBind()

        'Sembunyikan kolom agar tidak ikut diekspor ke excel
        Me.GridView.Columns(0).Visible = False
        Me.GridView.Columns(3).Visible = False
        Me.GridView.Columns(5).Visible = False
        Me.GridView.Columns(6).Visible = False
    End Sub
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=TransactionTypeAuxiliaryMapping.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridView)
            GridView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub
#End Region

    Protected Sub GridView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridView.ItemDataBound
        'bikin indexing
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                'e.Item.Cells(1).Text = e.Item.ItemIndex + 1
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Me.GridView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                Me.SetAndGetSearchingCriteria = Me.SetAllSearchingCriteria

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.SetAndGetSearchingCriteria = Me.SetAllSearchingCriteria
            Me.SetnGetCurrentPage = 0

            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
