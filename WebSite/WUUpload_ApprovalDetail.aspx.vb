﻿#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports AMLBLL.AuditTrailBLL
Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
Imports System.Data.SqlClient
#End Region

Partial Class WUUpload_ApprovalDetail
    Inherits Parent

#Region "Property"

    Public ReadOnly Property SetnGetUserID() As String
        Get
            Return SessionPkUserId
        End Get

    End Property

    Private Property GetTotalInsert() As Double
        Get
            Return Session("GetTotalInsert")
        End Get
        Set(ByVal value As Double)
            Session("GetTotalInsert") = value
        End Set
    End Property

    Private Property GetTotalUpdate() As Double
        Get
            Return Session("GetTotalUpdate")
        End Get
        Set(ByVal value As Double)
            Session("GetTotalUpdate") = value
        End Set
    End Property

    Public ReadOnly Property parID As String
        Get
            'If Request.Item("ID") = Nothing Then Throw New Exception("Data Was Not Found")
            Return Request.Params("PK_WO_Approval_Id")
        End Get
    End Property

    Public Property SetIgnoreID() As String
        Get
            If Not Session("WU_ApprovalDetail.setPK_WUUpload_id") Is Nothing Then
                Return CStr(Session("WU_ApprovalDetail.setPK_WUUpload_id"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("WU_ApprovalDetail.setPK_WUUpload_id") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("WU_ApprovalDetail.Sort") Is Nothing, " FK_WO_Approval_Id  asc", Session("WU_ApprovalDetail.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("WU_ApprovalDetail.Sort") = Value
        End Set
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("WU_ApprovalDetail.RowTotal") Is Nothing, 0, Session("WU_ApprovalDetail.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("WU_ApprovalDetail.RowTotal") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return iTotalPages(SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("WU_ApprovalDetail.CurrentPage") Is Nothing, 0, Session("WU_ApprovalDetail.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("WU_ApprovalDetail.CurrentPage") = Value
        End Set
    End Property

    Public ReadOnly Property SetnGetBindTable(Optional ByVal AllRecord As Boolean = False) As TList(Of WU_Approval_detail)
        Get
            Dim TotalDisplay As Integer = 0
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
                SetnGetCurrentPage = GetPageTotal - 1
            End If

            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = "FK_WO_Approval_id=" & parID

            If AllRecord = False Then
                TotalDisplay = GetDisplayedTotalRow
            Else
                TotalDisplay = Integer.MaxValue
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            Return DataRepository.WU_Approval_detailProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, TotalDisplay, SetnGetRowTotal)

        End Get

    End Property

    Private Sub ClearThisPageSessions()

        LblMessage.Visible = False
        LblMessage.Text = ""
        SetnGetRowTotal = Nothing
        SetnGetSort = Nothing
        SetnGetCurrentPage = 0
    End Sub

    Private Sub bindgrid()

        DataGrid1.DataSource = SetnGetBindTable
        DataGrid1.CurrentPageIndex = SetnGetCurrentPage
        DataGrid1.VirtualItemCount = SetnGetRowTotal
        DataGrid1.DataBind()

    End Sub
    Private Sub ShowButonApproveReject(ByVal Show As Boolean)
        BtnReject.Visible = Show
        BtnAccept.Visible = Show
    End Sub
    Sub HideControl()
        BtnCancel.Visible = False
    End Sub
#End Region

#Region "Event"
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            bindgrid()
            LoadData()
            LblMessage.Text = ""
            LblMessage.Visible = False
            If Not Page.IsPostBack Then
                ClearThisPageSessions()
                AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '    Try
    '        bindgrid()
    '    Catch ex As Exception
    '        LogError(ex)
    '        CvalPageErr.IsValid = False
    '        CvalPageErr.ErrorMessage = ex.Message
    '    End Try
    'End Sub
    Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
        If CvalHandleErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub
    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalPageErr.PreRender
        If CvalPageErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub
    Private Sub LoadData()

        Dim ObjMsIgnoreList_ApprovalDetail As WU_Approval = DataRepository.WU_ApprovalProvider.GetPaged("PK_WO_Approval_Id = " & parID, "", 0, Integer.MaxValue, 0)(0)
        With ObjMsIgnoreList_ApprovalDetail
            SafeDefaultValue = "-"
            txtMsUser_StaffName.Text = .RequestedBy
            txtRequestedDate.Text = .RequestedDate.GetValueOrDefault().ToString("dd-MMM-yyyy HH:mm:ss")


            'other info
            Dim Omsuser As User
            Omsuser = DataRepository.UserProvider.GetBypkUserID(.RequestedBy)
            If Omsuser IsNot Nothing Then
                txtMsUser_StaffName.Text = Omsuser.UserName
            Else
                txtMsUser_StaffName.Text = SafeDefaultValue
            End If
        End With

    End Sub

    Protected Sub DataGrid1_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DataGrid1.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            SetnGetSort = ChangeSortCommand(e.SortExpression)
            GridUser.Columns(IndexSort(GridUser, e.SortExpression)).SortExpression = SetnGetSort
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub ProcesDeleteApprovalDetail(ByVal objTransManager As TransactionManager)
        'Dim wulist_ApprovalDetail As WU_Approval_detail = DataRepository.WU_Approval_detailProvider.GetPaged("FK_WO_Approval_id = " & parID, "", 0, Integer.MaxValue, Nothing)(0)
        Using cmd As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("USP_WU_delete")
            ' Create a SqlParameter for each parameter in the stored procedure.
            'Dim IDParam As New SqlParameter("@WuId", CStr(wulist_ApprovalDetail.FK_WO_Approval_id))
            Dim IDParam As New SqlParameter("@WuId", parID)
            cmd.Parameters.Add(IDParam)
            DataRepository.Provider.ExecuteNonQuery(objTransManager, cmd)
        End Using
    End Sub
    Private Sub ProcesAcceptApprovalDetail(ByVal objTransManager As TransactionManager)
        'Dim wulist_ApprovalDetail As WU_Approval_detail = DataRepository.WU_Approval_detailProvider.GetPaged("FK_WO_Approval_id = " & parID, "", 0, Integer.MaxValue, Nothing)(0)
        Using cmd As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("USP_WU_accept")
            ' Create a SqlParameter for each parameter in the stored procedure.
            'Dim customerIDParam As New SqlParameter("@pkuploadid", wulist_ApprovalDetail.FK_WO_Approval_id)
            Dim customerIDParam As New SqlParameter("@pkuploadid", parID)
            cmd.Parameters.Add(customerIDParam)

            DataRepository.Provider.ExecuteNonQuery(objTransManager, cmd)
        End Using
    End Sub
#End Region

#Region "Accept Reject"
    Protected Sub BtnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReject.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Try
            'Dim Key_ApprovalID As String = "0"
            'Dim Idx As WU_Approval_Detail = DataRepository.WU_Approval_DetailProvider.GetPaged("FK_WO_Approval_id = " & parID, "", 0, Integer.MaxValue, Nothing)(0)
            ''----------------------------------------------------------------------------------------------------------------------------------------------------------------

            'Dim wuList_Approval As WU_Approval = DataRepository.WU_ApprovalProvider.GetPaged("PK_WO_Approval_id = " & parID, "", 0, Integer.MaxValue, Nothing)(0)

            'If Not wuList_Approval.PK_WO_Approval_Id.ToString Is Nothing Then
            '    wuList_Approval = DataRepository.WU_ApprovalProvider.GetPaged( _
            '        WU_ApprovalColumn.PK_WO_Approval_Id.ToString & "=" & Idx.FK_WO_Approval_id.ToString, _
            '        "", 0, Integer.MaxValue, Nothing)(0)
            'End If
            'DataRepository.WU_ApprovalProvider.Delete(wuList_Approval)
            'ProcesDeleteApprovalDetail()

            objTransManager.BeginTransaction()
            DataRepository.WU_ApprovalProvider.Delete(objTransManager, parID)
            ProcesDeleteApprovalDetail(objTransManager)
            objTransManager.Commit()

            BtnCancel.Visible = False
            'ShowButonApproveReject(False)
            'CvalHandleErr.IsValid = False
            LblConfirmation.Text = " Rejected Success "
            MultiViewUpload.ActiveViewIndex = 1

        Catch ex As Exception
            objTransManager.Rollback()
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub BtnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAccept.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Try
            'Dim Key_ApprovalID As String = ""
            'Dim Idx As WU_Approval_Detail = DataRepository.WU_Approval_DetailProvider.GetPaged("FK_WO_Approval_id=" & parID, "", 0, Integer.MaxValue, Nothing)(0)
            ''================================================== Deklaration ===================================================

            ''----------------------------------------------------------------------------------------------------------------------------------------------------------------
            'Dim wuList_Approval As WU_Approval = DataRepository.WU_ApprovalProvider.GetPaged("PK_WO_Approval_id = " & parID, "", 0, Integer.MaxValue, Nothing)(0)

            'If Not wuList_Approval.PK_WO_Approval_Id.ToString Is Nothing Then
            '    wuList_Approval = DataRepository.WU_ApprovalProvider.GetPaged( _
            '        WU_ApprovalColumn.PK_WO_Approval_Id.ToString & "=" & Idx.FK_WO_Approval_id.ToString, _
            '        "", 0, Integer.MaxValue, Nothing)(0)
            'End If

            'ProcesAcceptApprovalDetail()
            'DataRepository.WU_ApprovalProvider.Delete(wuList_Approval)

            objTransManager.BeginTransaction()
            ProcesAcceptApprovalDetail(objTransManager)
            DataRepository.WU_ApprovalProvider.Delete(objTransManager, parID)
            objTransManager.Commit()

            Dim ket As String = ""
            'CvalHandleErr.IsValid = False
            If GetTotalInsert > 0 Then
                ket = "(Insert :" & GetTotalInsert & " )"
            End If
            LblConfirmation.Text = "Confirmed Success " & ket
            MultiViewUpload.ActiveViewIndex = 1

            ShowButonApproveReject(False)
        Catch ex As Exception
            objTransManager.Rollback()
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("WUUpload_Approval.aspx")
    End Sub
    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Response.Redirect("WUUpload_approval.aspx")
    End Sub
#End Region

End Class
