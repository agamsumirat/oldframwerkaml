<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="VerificationListUploadPEPApproval.aspx.vb" Inherits="VerificationListUploadPEPApproval" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpcontent" Runat="Server">
	 <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Pending Approval Verification List - PEP List
                </strong>
            </td>
        </tr>
    </table>
		<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" id="TableUploadData" runat="server">
            <tr>
                <td bgcolor="#ffffff">
                            <asp:CustomValidator ID="cvalPageError" runat="server" Display="Dynamic"></asp:CustomValidator></td>
            </tr>
            <tr>
                <td bgcolor="#ffffff" style="height: 19px">
                    There is an upload for PEPList that need for approval with following data
                    supplied:</td>
            </tr>
            <tr>
                <td bgcolor="#ffffff">
                    </td>
            </tr>
		<tr>
			<td bgColor="#ffffff" style="height: 249px">
       
                <ajax:ajaxpanel id="AjaxPanel4" runat="server">       
                    <div style="overflow:auto;height:200px;width:800px">
                    <asp:GridView ID="GridPendingApprovalPEPList" runat="server" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                        CellPadding="4" ForeColor="Black" GridLines="Vertical">
                        <FooterStyle BackColor="#CCCC99" />
                        <Columns>
                            <asp:BoundField DataField="NoReference" HeaderText="NoReference" SortExpression="NoReference" />
                            <asp:BoundField DataField="Nama" HeaderText="Nama" SortExpression="Nama" />
                            <asp:BoundField DataField="GelarDepan" HeaderText="GelarDepan" SortExpression="GelarDepan" />
                            <asp:BoundField DataField="GelarBelakang" HeaderText="GelarBelakang" SortExpression="GelarBelakang" />
                            <asp:BoundField DataField="LembagaInstansi" HeaderText="LembagaInstansi" SortExpression="LembagaInstansi" />
                            <asp:BoundField DataField="Jabatan" HeaderText="Jabatan" SortExpression="Jabatan" />
                            <asp:BoundField DataField="JalanKantor" HeaderText="JalanKantor" SortExpression="JalanKantor" />
                            <asp:BoundField DataField="AlamatKantor" HeaderText="AlamatKantor" SortExpression="AlamatKantor" />
                            <asp:BoundField DataField="AlamatKantor2" HeaderText="AlamatKantor2" SortExpression="AlamatKantor2" />
                            <asp:BoundField DataField="PropinsiKantor" HeaderText="PropinsiKantor" SortExpression="PropinsiKantor" />
                            <asp:BoundField DataField="TeleponKantor" HeaderText="TeleponKantor" SortExpression="TeleponKantor" />
                            <asp:BoundField DataField="JalanRumah" HeaderText="JalanRumah" SortExpression="JalanRumah" />
                            <asp:BoundField DataField="AlamatRumah" HeaderText="AlamatRumah" SortExpression="AlamatRumah" />
                            <asp:BoundField DataField="PropinsiRumah" HeaderText="PropinsiRumah" SortExpression="PropinsiRumah" />
                            <asp:BoundField DataField="KodePosRumah" HeaderText="KodePosRumah" SortExpression="KodePosRumah" />
                            <asp:BoundField DataField="TeleponRumah" HeaderText="TeleponRumah" SortExpression="TeleponRumah" />
                        </Columns>
                        <RowStyle BackColor="#F7F7DE" />
                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </ajax:ajaxpanel>
				</td>
				
		</tr>
            
            <tr>
                <td bgcolor="#ffffff">
                </td>
            </tr>		
			</table>
		<table id="TableNoPendingApproval" runat=server visible=false>
	        <tr>
	            <td>There is no pending approval for Verification List PEPList</td>
	        </tr>
	    </table>
		   <table cellpadding="0" cellspacing="0" border="0" width="100%">
		    <tr>
			<td nowrap style="height: 93px"><ajax:ajaxpanel id="AjaxPanel5" runat="server">&nbsp;<asp:imagebutton id="ImageAccept" runat="server" CausesValidation="False" SkinID="AcceptButton"></asp:imagebutton>
			  &nbsp; </ajax:ajaxpanel> </td>
			 <td style="height: 93px" nowrap="noWrap">
			 <ajax:ajaxpanel id="AjaxPanel1" runat="server">&nbsp;
			    <asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton">
			    </asp:imagebutton>&nbsp; </ajax:ajaxpanel></td>
            <td nowrap="nowrap" style="height: 93px" width="99%">
                <ajax:ajaxpanel id="Ajaxpanel14" runat="server">&nbsp;
                    <asp:ImageButton ID="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton" />&nbsp;</ajax:AjaxPanel></td>
		    </tr>
            </table>
			

</asp:Content>
