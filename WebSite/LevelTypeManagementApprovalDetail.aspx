<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="LevelTypeManagementApprovalDetail.aspx.vb" Inherits="LevelTypeManagementApprovalDetail" title="Level Type Management Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:Label id="LabelTitle" runat="server"></asp:Label></td>
		</tr>
	</table>
    <TABLE id="TableDetail" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="2" runat="server">
	    <TR class="formText" id="UserAdd1">
		    <TD bgColor="#ffffff" height="24" style="width: 22px">&nbsp;</TD>
		    <TD bgColor="#ffffff">
                Level Type Name</TD>
		    <TD bgColor="#ffffff" style="width: 44px">:</TD>
		    <TD width="80%" bgColor="#ffffff"><asp:Label ID="LabelLevelTypeNameAdd" runat="server"></asp:Label></TD>
	    </TR>
	    <tr class="formText" id="UserAdd2">
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Description<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff" style="width: 44px">:<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff">
                <asp:TextBox ID="TextLevelTypeDescriptionAdd" runat="server" MaxLength="255" ReadOnly="True"
                    Rows="5" TextMode="MultiLine" Width="312px"></asp:TextBox></td>
	    </tr>
	    <tr class="formText" id="UserEdi1">
		    <td height="24" colspan="4" bgcolor="#ffffcc">&nbsp;Old Values</td>
		    <td colspan="4" bgcolor="#ffffcc">&nbsp;New Values</td>
	    </tr>
	    <tr class="formText" id="UserEdi2">
		    <td bgcolor="#ffffff" style="width: 22px; height: 32px">&nbsp;</td>
		    <td bgcolor="#ffffff" style="height: 32px;">
                Level Type ID</td>
		    <td bgcolor="#ffffff" style="width: 44px; height: 32px;">:</td>
		    <td width="40%" bgcolor="#ffffff" style="height: 32px"><asp:Label id="LabelLevelTypeIDEditOldLevelTypeID" runat="server"></asp:Label></td>
		    <td width="5" bgcolor="#ffffff" style="height: 32px">&nbsp;</td>
		    <td width="10%" bgcolor="#ffffff" style="height: 32px">
                Level Type ID</td>
		    <td width="5" bgcolor="#ffffff" style="height: 32px">:</td>
		    <td width="40%" bgcolor="#ffffff" style="height: 32px"><asp:Label id="LabelLevelTypeIDEditNewLevelTypeID" runat="server"></asp:Label></td>
	    </tr>
	    <tr class="formText" id="UserEdi3">
		    <td bgcolor="#ffffff" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Level Type Name</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td bgcolor="#ffffff"><asp:Label id="LabelLevelTypeNameEditOldLevelTypeName" runat="server"></asp:Label></td>
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Level Type Name</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:Label id="LabelLevelTypeEditNewLevelTypeName" runat="server"></asp:Label></td>
	    </tr>
	    <tr class="formText" id="UserEdi4">
		    <td bgcolor="#ffffff" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Description<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff" style="width: 44px">:<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff">
                <asp:TextBox ID="TextLevelTypeDescriptionEditOldLevelTypeDescription" runat="server"
                    MaxLength="255" ReadOnly="True" Rows="5" TextMode="MultiLine" Width="312px"></asp:TextBox></td>
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Description<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff">:<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff">
                <asp:TextBox ID="TextLevelTypeEditNewDescription" runat="server" MaxLength="255"
                    ReadOnly="True" Rows="5" TextMode="MultiLine" Width="312px"></asp:TextBox></td>
	    </tr>
	    <TR class="formText" id="UserDel1">
		    <TD bgColor="#ffffff" style="height: 23px; width: 22px;">&nbsp;</TD>
		    <TD bgColor="#ffffff" style="height: 23px;">
                Level Type ID</TD>
		    <TD bgColor="#ffffff" style="height: 23px; width: 44px;">:</TD>
		    <TD width="80%" bgColor="#ffffff" style="height: 23px"><asp:Label id="LabelLevelTypeDeleteLevelTypeID" runat="server"></asp:Label></TD>
	    </TR>
	    <tr class="formText" id="UserDel2">
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Level Type Name</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td bgcolor="#ffffff"><asp:Label id="LabelLevelTypeDeleteLevelTypeName" runat="server"></asp:Label></td>
	    </tr>
	    <tr class="formText" id="UserDel3">
		    <td bgcolor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Description<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff" style="width: 44px">:<br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff">
                <asp:TextBox ID="TextLevelTypeDeleteDescription" runat="server" MaxLength="255" ReadOnly="True" Rows="5"
                    TextMode="MultiLine" Width="312px"></asp:TextBox></td>
	    </tr>
	    <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="False" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="none"></asp:CustomValidator></TD>
	    </TR>
    </TABLE>
</asp:Content>