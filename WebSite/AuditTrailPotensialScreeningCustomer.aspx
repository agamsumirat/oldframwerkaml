<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AuditTrailPotensialScreeningCustomer.aspx.vb" Inherits="AuditTrailPotensialScreeningCustomer"  %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">

<script src="script/popcalendar.js"></script>

    <span defaultbutton="ImgBtnSearch">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td>
                </td>
                <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                    <div id="divcontent" class="divcontent">
                        <table id="tblpenampung" cellpadding="0" cellspac   ing="0" border="0" width="100%">
                            <tr>
                                <td>
                                    <img src="Images/blank.gif" width="20" height="100%" /></td>
                                <td class="divcontentinside" bgcolor="#FFFFFF">
                                    <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%">
                                        <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                            height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
                                            border-bottom-style: none" bgcolor="#dddddd" width="100%">
                                            <tr bgcolor="#ffffff">
                                                <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                    <strong>
                                                        <img src="Images/dot_title.gif" width="17" height="17">
                                                        <asp:Label ID="Label7" runat="server" 
                                                        Text="Potential Customer Verification"></asp:Label>
                                                        <hr />
                                                    </strong>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ajax:AjaxPanel ID="AjxMessage" runat="server">
                                                        <asp:Label ID="LblMessage" background="images/validationbground.gif" runat="server"
                                                            CssClass="validationok" Width="100%"></asp:Label>
                                                    </ajax:AjaxPanel>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="ImgBtnSearch" Width="100%">
                                            <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                                border="2">
                                                <tr id="searchbox">
                                                    <td colspan="2" valign="top" width="98%" bgcolor="#ffffff">
                                                        <table cellspacing="4" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td class="Regtext" nowrap valign="top">
                                                                    &nbsp;Search By :</td>
                                                                <td valign="middle" nowrap>
                                                                    <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="800px">
                                                                        <table style="height: 100%">
                                                                            <tr>
                                                                                <td colspan="3" style="height: 26px">
                                                                                </td>
                                                                                 <td colspan="3" style="height: 26px">
                                                                                </td>
                                                                            </tr>
                                                                           <tr>
                                                                                <td nowrap style="height: 26px">
                                                                                    <asp:Label ID="LblUserID" runat="server" Text="User ID"></asp:Label></td>
                                                                                <td style="width: 1px;">
                                                                                    :</td>
                                                                                <td style="height: 26px; width: 205px;">
                                                                                    <asp:TextBox ID="TxtUserID" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                                                                    <td nowrap style="height: 26px">
                                                                                    <asp:Label ID="Label4" runat="server" Text="Date of Birth"></asp:Label></td>
                                                                                <td style="width: 1px;">
                                                                                    :</td>
                                                                                <td style="height: 26px; width: 200px;">
                                                                                    <asp:TextBox ID="TxtEntryDateBirth" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox><input
                                                                                        id="popUpEntryDateBirth" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                                        border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                                        border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                                        height: 17px" title="Click to show calendar" type="button" /></td>
                                                                            </tr>
                                                                             <tr>
                                                                                <td nowrap style="height: 26px">
                                                                                    <asp:Label ID="Label1" runat="server" Text="User Name"></asp:Label></td>
                                                                                <td style="width: 1px;">
                                                                                    :</td>
                                                                                <td style="height: 26px; width: 205px;">
                                                                                    <asp:TextBox ID="TxtUserName" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                                                            <td nowrap style="height: 26px">
                                                                                    Nationality</td>
                                                                                <td style="width: 1px;">
                                                                                    :</td>
                                                                                <td style="height: 26px; width: 200px;">
                                                                                    <asp:TextBox ID="TxtNationality" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                                                            </tr>
                                                                             <tr>
                                                                                <td nowrap style="height: 26px">
                                                                                    <asp:Label ID="Label3" runat="server" Text="Screening Date"></asp:Label></td>
                                                                                <td style="width: 1px;">
                                                                                    :</td>
                                                                                <td style="height: 26px; width: 205px;">
                                                                                    <asp:TextBox ID="TxtEntryDateScreen" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox><input
                                                                                        id="popUpEntryDateScreen" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                                        border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                                        border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                                        height: 17px" title="Click to show calendar" type="button" /></td>
                                                                           <td nowrap style="height: 26px">
                                                                                    <span defaultbutton="ImgBtnSearch">
                                                                                    <asp:Label ID="Label10" runat="server" Text="Suspect Match"></asp:Label>
                                                                                    </span></td>
                                                                                <td style="width: 1px;">
                                                                                    :</td>
                                                                                <td style="height: 26px; width: 200px;">
                                                                                    <span defaultbutton="ImgBtnSearch">
                                                                                    <asp:TextBox ID="TxtSuspectMatch" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox>
                                                                                    </span></td>
                                                                            </tr>
                                                                             <tr>
                                                                                <td nowrap style="height: 26px">
                                                                                    <asp:Label ID="Label8" runat="server" Text="Nama"></asp:Label></td>
                                                                                <td style="width: 1px;">
                                                                                    :</td>
                                                                                <td style="height: 26px; width: 205px;">
                                                                                    <asp:TextBox ID="TxtNama" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                                                            <td nowrap style="height: 26px">
                                                                                    &nbsp;</td>
                                                                                <td style="width: 1px;">
                                                                                    &nbsp;</td>
                                                                                <td style="height: 26px; width: 200px;">
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                </td>
                                                                                 <td colspan="3">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                    <asp:ImageButton ID="ImgBtnSearch" TabIndex="3" runat="server" SkinID="SearchButton"
                                                                                        ImageUrl="~/Images/button/search.gif"></asp:ImageButton>
                                                                                    <asp:ImageButton ID="ImgBtnClearSearch" runat="server" ImageUrl="~/Images/button/clearsearch.gif" /></td>
                                                                            </tr>
                                                                        </table>
                                                                        &nbsp;&nbsp;&nbsp;
                                                                    </ajax:AjaxPanel>
                                                                </td>
                                                                <td valign="middle" width="99%">
                                                                    &nbsp;</td>
                                                            </tr>
                                                        </table>
                                                        <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                                                            <asp:CustomValidator ID="CvalPageErr" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
                                                        <ajax:AjaxPanel ID="Ajaxpanel2" runat="server">
                                                            <asp:CustomValidator ID="CvalHandleErr" runat="server" ValidationGroup="handle" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                            border="2">
                                            <tr>
                                                <td bgcolor="#ffffff">
                                                    &nbsp;<ajax:AjaxPanel ID="AjaxPanel14" runat="server"><asp:DataGrid ID="GridAuditTrailPotensialScreeningCustomer"
                                                        runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                                        CellPadding="4" Font-Size="XX-Small" ForeColor="Black" GridLines="Vertical" 
                                                            Width="100%" AllowCustomPaging="True">
                                                        <FooterStyle BackColor="#CCCC99" />
                                                        <PagerStyle Visible="False" />
                                                        <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                        <AlternatingItemStyle BackColor="White" />
                                                        <ItemStyle BackColor="#F7F7DE" />
                                                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="XX-Small" ForeColor="White" />
                                                        <Columns>
                                                            <asp:TemplateColumn>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="10px" />
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="PK_AML_Screening_Customer_Request_Detail_ID" Visible="False">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="UserID" HeaderText="User ID" SortExpression="UserID desc">
                                                                <HeaderStyle Width="20px" />
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="UserName" HeaderText="User Name" SortExpression="UserName desc">
                                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" Width="10%" Wrap="False" />
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="CreatedDate" HeaderText="Screening Date" 
                                                                SortExpression="CreatedDate desc" DataFormatString="{0:dd MMM yyyy HH:mm:ss}">
                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" Wrap="False" />
                                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" Width="24%" Wrap="False" />
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Nama" DataFormatString="{0:dd-MM-yyyy}" HeaderText="Nama"
                                                                SortExpression="Nama Desc">
                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" Wrap="False" />
                                                                <HeaderStyle Width="24%" />
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="DOB" HeaderText="Date Of Birth" 
                                                                SortExpression="DOB Desc" DataFormatString="{0:dd MMM yyyy}">
                                                                <HeaderStyle Width="50px" />
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Nationality" HeaderText="Nationality" SortExpression="Nationality Desc">
                                                                <HeaderStyle Width="30%" />
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="SuspectMatch" HeaderText="Suspect Match" SortExpression="SuspectMatch Desc">
                                                            </asp:BoundColumn>
                                                            <asp:EditCommandColumn CancelText="Cancel" EditText="Detail" UpdateText="Update">
                                                                <HeaderStyle Width="24%" />
                                                            </asp:EditCommandColumn>
                                                            <asp:TemplateColumn>
                                                                <ItemTemplate>
                                                                    &nbsp;
                                                                    <asp:CheckBox ID="Verify" runat="server" Enabled="False" />
                                                                    <asp:LinkButton ID="LnkVerify" runat="server" CausesValidation="False" CommandName="Verify"
                                                                        meta:resourcekey="LnkActiveResource1">Verify</asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="VerifyUser" HeaderText="Verify User" SortExpression="VerifyUser Desc">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="VerifyDate" DataFormatString="{0:dd-MMM-yyyy}"
                                                                HeaderText="Verify Date" SortExpression="VerifyDate  desc"></asp:BoundColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                        <asp:Label ID="LabelNoRecordFound" runat="server" CssClass="text" Text="No record match with your criteria"
                                                            Visible="False"></asp:Label></ajax:AjaxPanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #ffffff">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td nowrap>
                                                                <ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                                                    &nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
                                                                    &nbsp;
                                                                </ajax:AjaxPanel>
                                                            </td>
                                                            <td width="99%">
                                                                &nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" ajaxcall="none" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                    runat="server">Export 
		        to Excel</asp:LinkButton>&nbsp;
                                                                <asp:LinkButton ID="lnkExportAllData" runat="server" ajaxcall="none" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                    Text="Export All to Excel"></asp:LinkButton></td>
                                                            <td align="right" nowrap>
                                                                &nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#ffffff">
                                                    <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                        bgcolor="#ffffff" border="2">
                                                        <tr class="regtext" align="center" bgcolor="#dddddd">
                                                            <td valign="top" align="left" width="50%" bgcolor="#ffffff">
                                                                Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel7" runat="server"><asp:Label ID="PageCurrentPage"
                                                                    runat="server" CssClass="regtext">0</asp:Label>&nbsp;of&nbsp;
                                                                    <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label></ajax:AjaxPanel></td>
                                                            <td valign="top" align="right" width="50%" bgcolor="#ffffff">
                                                                Total Records&nbsp;
                                                                <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                                                    <asp:Label ID="PageTotalRows" runat="server">0</asp:Label></ajax:AjaxPanel></td>
                                                        </tr>
                                                    </table>
                                                    <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                        bgcolor="#ffffff" border="2">
                                                        <tr bgcolor="#ffffff">
                                                            <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                                                                <hr color="#f40101" noshade size="1">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                                                                Go to page</td>
                                                            <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                                                                <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                                    <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                                                        <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox></ajax:AjaxPanel>
                                                                </font>
                                                            </td>
                                                            <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                                                                <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                                                    <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton" ImageUrl="~/Images/button/go.gif">
                                                                    </asp:ImageButton></ajax:AjaxPanel>
                                                            </td>
                                                            <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                                <img height="5" src="images/first.gif" width="6">
                                                            </td>
                                                            <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                                                <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                                                    <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                                                        OnCommand="PageNavigate">First</asp:LinkButton></ajax:AjaxPanel>
                                                            </td>
                                                            <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                                                <img height="5" src="images/prev.gif" width="6"></td>
                                                            <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
                                                                <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                                                    <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                                                        OnCommand="PageNavigate">Previous</asp:LinkButton></ajax:AjaxPanel>
                                                            </td>
                                                            <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
                                                                <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                                                    <a class="pageNav" href="#">
                                                                        <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                                                            OnCommand="PageNavigate">Next</asp:LinkButton></a></ajax:AjaxPanel></td>
                                                            <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                                <img height="5" src="images/next.gif" width="6"></td>
                                                            <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
                                                                <ajax:AjaxPanel ID="AjaxPanel16" runat="server">
                                                                    <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                                                        OnCommand="PageNavigate">Last</asp:LinkButton></ajax:AjaxPanel>
                                                            </td>
                                                            <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                                <img height="5" src="images/last.gif" width="6"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </ajax:AjaxPanel>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
</asp:Content>


