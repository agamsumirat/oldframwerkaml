Partial Class CustomerVerificationJudgementByRequestIDWebAPIViewDetail
    Inherits Parent

    ''' <summary>
    ''' Get CustomerVerificationJudgementId
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property RequestID As String
        Get
            Return Me.Request.Params.Get("requestID")
        End Get
    End Property

    Private ReadOnly Property PrimaryKey As String
        Get
            Return Me.Request.Params.Get("key")
        End Get
    End Property

    Public Property JudgementCustomer() As Data.DataTable
        Get
            Return Session("CustomerVerificationJudgementViewDetailWebAPI.JudgementCustomer")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CustomerVerificationJudgementViewDetailWebAPI.JudgementCustomer") = value
        End Set
    End Property

    Public Property Aml_Customer_Judgement() As Data.DataTable
        Get
            Return Session("CustomerVerificationJudgementViewDetailWebAPI.Aml_Customer_Judgement")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CustomerVerificationJudgementViewDetailWebAPI.Aml_Customer_Judgement") = value
        End Set
    End Property

    Public Property Aml_Watchlist_JudgementSatuan() As Data.DataTable
        Get
            Return Session("CustomerVerificationJudgementViewDetailWebAPI.Aml_Watchlist_JudgementSatuan")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CustomerVerificationJudgementViewDetailWebAPI.Aml_Watchlist_JudgementSatuan") = value
        End Set
    End Property

    'Public Property Aml_Watchlist_JudgementFilter() As Data.DataTable
    '    Get
    '        Return Session("CustomerVerificationJudgementViewDetailWebAPI.Aml_Watchlist_JudgementFilter")
    '    End Get
    '    Set(ByVal value As Data.DataTable)
    '        Session("CustomerVerificationJudgementViewDetailWebAPI.Aml_Watchlist_JudgementFilter") = value
    '    End Set
    'End Property

    Public Property AMLScreeningResultDetail() As Data.DataTable
        Get
            Return Session("CustomerVerificationJudgementViewDetailWebAPI.AMLScreeningResultDetail")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CustomerVerificationJudgementViewDetailWebAPI.AMLScreeningResultDetail") = value
        End Set
    End Property

    Private Property AMLScreeningCustomerPK As String
        Get
            Return Session("CustomerVerificationJudgementViewDetailWebAPI.AMLScreeningCustomerPK")
        End Get
        Set(ByVal value As String)
            Session("CustomerVerificationJudgementViewDetailWebAPI.AMLScreeningCustomerPK") = value
        End Set
    End Property

    Private Sub LoadDataMatchDetail(requestID As String)

        'Dim objdr As Data.DataRow() = AMLScreeningResultDetail.Select("Request_ID=" & requestID)
        Dim builder As New StringBuilder()
        Dim selectFilter As String

        With builder
            .Append("Request_ID='")
            .Append(requestID)
            .Append("'")
        End With

        selectFilter = builder.ToString()

        Dim objdr As Data.DataRow() = JudgementCustomer.Select(selectFilter)

        If objdr.Length = 0 Then Exit Sub

        'LblCIFNo.Text = objdr(0).Item("CIFNo")
        lblRequestID.Text = objdr(0).Item("Request_ID").ToString()
        LblName.Text = objdr(0).Item("Name").ToString()

        LblRelatedCustomerType.Text = objdr(0).Item("RelatedCustomerType").ToString()
        'LblBranch.Text = objdr(0).Item("AccountOwnerID")
        If objdr(0).Item("DOB").ToString <> "" Then
            LblDateOfBirth.Text = Convert.ToDateTime(objdr(0).Item("DOB")).ToString("dd-MMM-yyyy")
        End If

        'If objdr(0).Item("OpeningCIF").ToString <> "" Then
        '    LblOpeningDate.Text = Convert.ToDateTime(objdr(0).Item("OpeningCIF")).ToString("dd-MMM-yyyy")
        'End If

        'If objdr(0).Item("CIFNo").ToString <> "" Then
        '    lblBirthPlaceCIF.Text = AMLBLL.AccountInformationBLL.GetBirthPlace(objdr(0).Item("CIFNo"))
        'End if

        'AddressTextBox.Text = objdr(0).Item("Address").ToString
        'IdentityNumberTextBox.Text = objdr(0).Item("IdentityNumber").ToString
        'TxtPhoneNumberCustomer.Text = objdr(0).Item("PhoneNumber").ToString

    End Sub

    Sub ClearSession()

        Session("JudgementCommentDetailWebAPI") = Nothing
        JudgementCustomer = Nothing
        Aml_Customer_Judgement = Nothing
        'Aml_Watchlist_JudgementFilter = Nothing
        AMLScreeningResultDetail = Nothing
        Aml_Watchlist_JudgementSatuan = Nothing
        AMLScreeningCustomerPK = Nothing

    End Sub

    Sub BindgrdiCustomerDistinct()
        GridCustomer.DataSource = JudgementCustomer
        GridCustomer.DataBind()
    End Sub

    ''' <summary>
    '''  Load Data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then

                ClearSession()

                'JudgementCustomer = EKABLL.ScreeningBLL.getAMLCustomerJudgementDistinctCIF(CIFNo)

                JudgementCustomer = EKABLL.ScreeningBLL.GetAMLCustomerJudgementWebAPIByRequestID(RequestID)
                BindgrdiCustomerDistinct()

                Aml_Customer_Judgement = EKABLL.ScreeningBLL.GetAMLCustomerJudgementWebAPIByRequestID(RequestID)

                AMLScreeningResultDetail = EKABLL.ScreeningBLL.GetAMLScreeningResultDetailWebAPI(RequestID)

                Me.LabelUserJudgement.Text = Sahassa.AML.Commonly.SessionUserId
                Me.LabelTglJudgement.Text = Now.ToLongDateString()

                'Using AccessCustomerVerificationDetail As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectCustomerVerificationJudgementFilterTableAdapter
                '    ' Get Detail for data Customer
                '    Using dtCustomerFilter As AMLDAL.CustomerVerificationJudgement.SelectCustomerVerificationJudgementFilterDataTable = AccessCustomerVerificationDetail.GetData(Me.CustomerVerificationJudgementId)
                '        Dim RowCustomerFilter As AMLDAL.CustomerVerificationJudgement.SelectCustomerVerificationJudgementFilterRow = dtCustomerFilter.Rows(0)

                '        Me.LblCIFNo.Text = RowCustomerFilter.CIFNo
                '        Me.LblName.Text = RowCustomerFilter.Name
                '        Try
                '            Me.LblDateOfBirth.Text = RowCustomerFilter.DateOfBirth.ToString("dd-MMMM-yyyy")
                '        Catch ex As Exception
                '            Me.LblDateOfBirth.Text = ""
                '        End Try
                '        lblBirthPlaceCIF.Text = AMLBLL.AccountInformationBLL.GetBirthPlace(RowCustomerFilter.CIFNo)

                '        Me.LblBranch.Text = RowCustomerFilter.BranchName
                '        Me.LabelUserJudgement.Text = Sahassa.AML.Commonly.SessionUserId
                '        Me.LabelTglJudgement.Text = Now.ToLongDateString
                '        Try
                '            Me.LblOpeningDate.Text = RowCustomerFilter.OpeningDate.ToString("dd-MMMM-yyyy")
                '        Catch ex As Exception
                '            Me.LblOpeningDate.Text = ""
                '        End Try
                '        Me.GetCCDAddressCustomer(RowCustomerFilter.CIFNo)
                '        Me.GetIdentityNumberCustomer(RowCustomerFilter.CIFNo)
                '        Me.GetPhoneNumberCustomer(RowCustomerFilter.CIFNo)
                '        Me.LblBeneficialOwner.Text = GetBeneficialOwner(RowCustomerFilter.CIFNo, RowCustomerFilter.Name)
                '    End Using
                'End Using

                '' Get All Detail Suspect Customer
                'Dim StrAddressSuspect As String = ""
                'Dim StrIdentitySuspect As String = ""
                'Using AccessCustomerVerification As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectCustomerVerificationJudgementDetailTableAdapter
                '    Using dt As AMLDAL.CustomerVerificationJudgement.SelectCustomerVerificationJudgementDetailDataTable = AccessCustomerVerification.GetData(Me.CustomerVerificationJudgementId)
                '        Me.GridMSUserView.DataSource = dt
                '        Me.GridMSUserView.PageSize = Int32.MaxValue
                '        Me.GridMSUserView.DataBind()

                '        For Each row As AMLDAL.CustomerVerificationJudgement.SelectCustomerVerificationJudgementDetailRow In dt.Rows

                '            StrAddressSuspect = Me.GetAddressSuspect(CInt(row.VerificationListId))
                '            StrIdentitySuspect = Me.GetIdentityNumberSuspect(CInt(row.VerificationListId))

                '            ' Tampilkan
                '            Try
                '                arrlistvalue.Add(New ListItem("Match with Name " & row.DisplayName & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB '" & row.DateOfBirth.ToString("dd-MMMM-yyyy") & "';" & row.ListTypeName & ";" & row.CategoryName & vbCrLf, row.CustomerVerificationJudgementDetailId))
                '            Catch ex As Exception
                '                arrlistvalue.Add(New ListItem("Match with Name " & row.DisplayName & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB '';" & row.ListTypeName & ";" & row.CategoryName & vbCrLf, row.CustomerVerificationJudgementDetailId))
                '            End Try

                '            Try
                '                Me.TxtJudgementComment.Text = Me.TxtJudgementComment.Text & "Match with Name " & row.DisplayName & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB" & row.DateOfBirth.ToString("dd-MMMM-yyyy") & ";" & row.ListTypeName & ";" & row.CategoryName & vbCrLf
                '            Catch ex As Exception
                '                Me.TxtJudgementComment.Text = Me.TxtJudgementComment.Text & "Match with Name " & row.DisplayName & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB;" & row.ListTypeName & ";" & row.CategoryName & vbCrLf
                '            End Try

                '        Next
                '        Session("JudgementCommentDetail") = arrlistvalue
                '    End Using
                'End Using

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' GetIdentityNumberCustomer
    ''' </summary>
    ''' <param name="CIFNo"></param>
    ''' <remarks></remarks>
    'Private Sub GetIdentityNumberCustomer(ByVal CIFNo As String)
    '    Try
    '        'Me.ListIdentityNumber.Items.Clear()
    '        Me.IdentityNumberTextBox.Text = ""
    '        ' Get Detail Identity Number Customer
    '        Using AccessCCDIdentity As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectCDD_CIF_IdNumberTableAdapter
    '            Using dtCCDIdentity As Data.DataTable = AccessCCDIdentity.GetData(CIFNo)
    '                For Each itemCCDIdentity As AMLDAL.CustomerVerificationJudgement.SelectCDD_CIF_IdNumberRow In dtCCDIdentity.Rows
    '                    'Me.ListIdentityNumber.Items.Add(itemCCDIdentity.IDNumber)
    '                    Me.IdentityNumberTextBox.Text = Me.IdentityNumberTextBox.Text & itemCCDIdentity.IDNumber & vbCrLf
    '                Next
    '            End Using
    '        End Using
    '    Catch
    '        Throw
    '    End Try
    'End Sub

    ''' <summary>
    ''' GetCCDAddressCustomer
    ''' </summary>
    ''' <param name="CIFNo"></param>
    ''' <remarks></remarks>
    'Private Sub GetCCDAddressCustomer(ByVal CIFNo As String)
    '    Try
    '        'Me.ListAddress.Items.Clear()
    '        Me.AddressTextBox.Text = ""
    '        ' Get Detail Address Customer
    '        Using AccessCCDAddress As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectCDD_CIF_AddressTableAdapter
    '            Using dtCCDAddress As Data.DataTable = AccessCCDAddress.GetData(CIFNo)
    '                For Each itemCCDAddress As AMLDAL.CustomerVerificationJudgement.SelectCDD_CIF_AddressRow In dtCCDAddress.Rows
    '                    'Me.ListAddress.Items.Add(itemCCDAddress.Address)
    '                    Me.AddressTextBox.Text = Me.AddressTextBox.Text & itemCCDAddress.Address & vbCrLf
    '                Next
    '            End Using
    '        End Using
    '    Catch
    '        Throw
    '    End Try
    'End Sub

    'Private Sub GetPhoneNumberCustomer(ByVal CIFNo As String)
    '    Me.TxtPhoneNumberCustomer.Text = ""
    '    ' Get Detail Customer Phone Number
    '    Using AccessCustomerInformation As New AMLDAL.CustomerInformationTableAdapters.SelectCustomerInformationPhoneDetailTableAdapter
    '        Using dtPhone As Data.DataTable = AccessCustomerInformation.GetData(CIFNo)
    '            For Each itemPhone As AMLDAL.CustomerInformation.SelectCustomerInformationPhoneDetailRow In dtPhone.Rows
    '                Me.TxtPhoneNumberCustomer.Text = Me.TxtPhoneNumberCustomer.Text & itemPhone.Type & " - " & itemPhone.Description & vbCrLf
    '            Next
    '        End Using
    '    End Using
    'End Sub

    'Function GetBeneficialOwner(strcif As String, strName As String) As String
    '    Using objCommand As System.Data.SqlClient.SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_GetBeneficialOwner")
    '        objCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CIFNo", strcif))
    '        objCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NamaCIF", strName))
    '        Return SahassaNettier.Data.DataRepository.Provider.ExecuteScalar(objCommand)
    '    End Using
    'End Function

    ''' <summary>
    ''' Get Identity Number Suspect
    ''' </summary>
    ''' <param name="VerificationListId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Private Function GetIdentityNumberSuspect(ByVal VerificationListId As Int64) As String
    '    Try
    '        Dim StrIdentitySuspect As String = ""
    '        ' Identity Suspect
    '        Using AccessIdentitySuspect As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectIDNumberSuspectTableAdapter
    '            Using dtIdentityNumber As Data.DataTable = AccessIdentitySuspect.GetData(CInt(VerificationListId))
    '                For Each RowIdentitySuspect As AMLDAL.CustomerVerificationJudgement.SelectIDNumberSuspectRow In dtIdentityNumber.Rows
    '                    StrIdentitySuspect &= RowIdentitySuspect.IDNumber & ","
    '                Next
    '                If StrIdentitySuspect.Length > 0 Then
    '                    StrIdentitySuspect = StrIdentitySuspect.Substring(0, StrIdentitySuspect.Length - 1)
    '                End If
    '            End Using
    '        End Using
    '        Return StrIdentitySuspect
    '    Catch
    '        Throw
    '    End Try
    'End Function

    ''' <summary>
    ''' Get Address Suspect
    ''' </summary>
    ''' <param name="VerificationListId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Private Function GetAddressSuspect(ByVal VerificationListId As Int64) As String
    '    Try
    '        Dim StrAddressSuspect As String = ""
    '        Using AccessAddressSuspect As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectAddressSuspectTableAdapter
    '            Using dtAddressSuspect As Data.DataTable = AccessAddressSuspect.GetData(CInt(VerificationListId))
    '                For Each RowAddressSuspect As AMLDAL.CustomerVerificationJudgement.SelectAddressSuspectRow In dtAddressSuspect.Rows
    '                    StrAddressSuspect &= RowAddressSuspect.Address & ","
    '                Next
    '                If StrAddressSuspect.Length > 0 Then
    '                    StrAddressSuspect = StrAddressSuspect.Substring(0, StrAddressSuspect.Length - 1)
    '                End If

    '            End Using
    '        End Using
    '        Return StrAddressSuspect
    '    Catch
    '        Throw
    '    End Try
    'End Function

    'Private Sub GetVerificationListAlias(ByVal VerificationListId As Int64)
    '    ' Alias
    '    Me.NameSuspectTextBox.Text = ""
    '    Using AccessDetailAliasPotentialCustomerVerification As New AMLDAL.PotentialCustomerVerificationTableAdapters.SelectVerificationList_AliasTableAdapter
    '        Using dtDetailAliasPotentialCustomerVerification As Data.DataTable = AccessDetailAliasPotentialCustomerVerification.GetData(VerificationListId)
    '            For Each RowDetailAliasPotentialCustomerVerification As AMLDAL.PotentialCustomerVerification.SelectVerificationList_AliasRow In dtDetailAliasPotentialCustomerVerification.Rows
    '                Me.NameSuspectTextBox.Text = Me.NameSuspectTextBox.Text & RowDetailAliasPotentialCustomerVerification.Name & vbCrLf
    '            Next
    '        End Using
    '    End Using
    'End Sub

    ''' <summary>
    ''' GetIdentityNumberSuspectDetail
    ''' </summary>
    ''' <param name="VerificationListId"></param>
    ''' <remarks></remarks>
    'Private Sub GetIdentityNumberSuspectDetail(ByVal VerificationListId As Int64)
    '    Try
    '        'Me.ListIndetityNumberSuspect.Items.Clear()
    '        Me.IdentityNumberSuspectTextBox.Text = ""
    '        Dim StrIdentitySuspect As String = ""
    '        ' Identity Suspect
    '        Using AccessIdentitySuspect As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectIDNumberSuspectTableAdapter
    '            Using dtIdentityNumber As Data.DataTable = AccessIdentitySuspect.GetData(CInt(VerificationListId))
    '                For Each RowIdentitySuspect As AMLDAL.CustomerVerificationJudgement.SelectIDNumberSuspectRow In dtIdentityNumber.Rows
    '                    'Me.ListIndetityNumberSuspect.Items.Add(RowIdentitySuspect.IDNumber)
    '                    Me.IdentityNumberSuspectTextBox.Text = Me.IdentityNumberSuspectTextBox.Text & RowIdentitySuspect.IDNumber & vbCrLf
    '                Next
    '            End Using
    '        End Using
    '    Catch
    '        Throw
    '    End Try
    'End Sub

    ''' <summary>
    ''' Get Address Suspect Detail
    ''' </summary>
    ''' <param name="VerificationListId"></param>
    ''' <remarks></remarks>
    'Private Sub GetAddressSuspectDetail(ByVal VerificationListId As Int64)
    '    Try
    '        'Me.ListAddressSuspect.Items.Clear()
    '        Me.AddressSuspectTextBox.Text = ""
    '        Dim StrAddressSuspect As String = ""
    '        Using AccessAddressSuspect As New AMLDAL.CustomerVerificationJudgementTableAdapters.SelectAddressSuspectTableAdapter
    '            Using dtAddressSuspect As Data.DataTable = AccessAddressSuspect.GetData(CInt(VerificationListId))
    '                For Each RowAddressSuspect As AMLDAL.CustomerVerificationJudgement.SelectAddressSuspectRow In dtAddressSuspect.Rows
    '                    'Me.ListAddressSuspect.Items.Add(RowAddressSuspect.Address)
    '                    Me.AddressSuspectTextBox.Text = Me.AddressSuspectTextBox.Text & RowAddressSuspect.Address & vbCrLf
    '                Next
    '            End Using
    '        End Using
    '    Catch
    '        Throw
    '    End Try
    'End Sub

    ''' <summary>
    ''' Radio Match dan UnMatch
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub RadioButtonList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Me.TxtJudgementComment.Text = ""
            Dim RadioListMatch As RadioButtonList = CType(sender, RadioButtonList)
            Dim dgItem As DataGridItem = CType(RadioListMatch.NamingContainer, DataGridItem)
            Dim VerificationListId As Integer = dgItem.Cells(1).Text
            Dim PK_ID As String = dgItem.Cells(0).Text
            'Dim PK_Aml_Customer_Judgement_ID As Integer = dgItem.Cells(0).Text

            'Aml_Watchlist_JudgementSatuan = EKABLL.ScreeningBLL.GetWatchListData(VerificationListId, PK_Aml_Customer_Judgement_ID)
            Aml_Watchlist_JudgementSatuan = EKABLL.ScreeningBLL.GetAMLWatchListSuspectDataDetail(VerificationListId)



            'Dim arrlist As ArrayList = Session("JudgementCommentDetail")
            'If arrlist Is Nothing Then
            '    arrlist = New ArrayList
            'End If
            
            'Dim listitem As ListItem
            

            Dim dr As Data.DataRow() = AMLScreeningResultDetail.Select("PK_AML_WebAPIScreening_Result_Detail_ID = " & PK_ID)
            If dr.Length > 0 Then
                If RadioListMatch.SelectedIndex = 0 Then
                    dr(0).Item("JudgementResult") = True
                Else
                    dr(0).Item("JudgementResult") = False
                End If
            End If



            'If RadioListMatch.SelectedIndex = 0 Then
            '    If arrlist.Count <> 0 Then
            '        For i As Integer = 0 To arrlist.Count - 1
            '            listitem = arrlist(i)
            '            If listitem.Value = dgItem.Cells(0).Text Then
            '                listitem.Text = listitem.Text.Replace("UnMatch", "Match")
            '                arrlist(i) = listitem
            '            Else

            '            End If
            '        Next
            '    End If
            'Else
            '    If arrlist.Count <> 0 Then
            '        For i As Integer = 0 To arrlist.Count - 1
            '            listitem = arrlist(i)
            '            If listitem.Value = dgItem.Cells(0).Text Then
            '                listitem.Text = listitem.Text.Replace("Match", "UnMatch")
            '                arrlist(i) = listitem
            '            End If
            '        Next
            '    End If
            'End If
            'Session("JudgementCommentDetail") = arrlist

            'For i As Integer = 0 To arrlist.Count - 1
            '    listitem = arrlist(i)
            '    Me.TxtJudgementComment.Text = Me.TxtJudgementComment.Text & listitem.Text
            'Next
            Session("JudgementCommentDetailWebAPI") = GetStringJudgement()

            Me.TxtJudgementComment.Text = Session("JudgementCommentDetailWebAPI")


        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' Get Detail Suspect
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Try

            Dim arrlistvalue As New ArrayList
            'Dim PK_ID As Integer = GridCustomer.Items(e.Item.ItemIndex).Cells(0).Text
            Dim dataGridItem As DataGridItem = GridMSUserView.Items(e.Item.ItemIndex)

            Dim primaryKey As Integer = dataGridItem.Cells(0).Text
            Dim verificationListID As Integer = dataGridItem.Cells(1).Text
            Dim suspectName As String = dataGridItem.Cells(2).Text
            
            Dim objradio As RadioButtonList = e.Item.FindControl("RadioButtonList1")
            
            If Not Aml_Customer_Judgement Is Nothing Then
                'LoadDatajudgement(PK_Aml_Customer_Judgement_ID)
                LoadDataMatchDetail(AMLScreeningCustomerPK)

                Aml_Watchlist_JudgementSatuan = EKABLL.ScreeningBLL.GetAMLWatchListSuspectDataDetail(verificationListID)

                If Aml_Watchlist_JudgementSatuan.Rows.Count > 0 Then
                    
                    'Nama diambil dari daftar, bukan dari tabel verificationlist_master
                    'Dikarenakan ada beberapa nama yang mengarah ke verificationlistid yang sama
                    'Sebagai contoh : umar patek
                    NameSuspectTextBox.Text = suspectName
                    'NameSuspectTextBox.Text = Aml_Watchlist_JudgementSatuan.Rows(0).Item("DisplayName")
                    LblListTypeSuspect.Text = Aml_Watchlist_JudgementSatuan.Rows(0).Item("ListTypeName")
                    LblListCategorySuspect.Text = Aml_Watchlist_JudgementSatuan.Rows(0).Item("CategoryName")
                    linkcategory.Text = Aml_Watchlist_JudgementSatuan.Rows(0).Item("CategoryName")
                    Dim strdate As String = ""
                    Try
                        strdate = Convert.ToDateTime(Aml_Watchlist_JudgementSatuan.Rows(0).Item("DateOfBirth")).ToString("dd-MMM-yyyy")
                    Catch ex As Exception
                        strdate = Aml_Watchlist_JudgementSatuan.Rows(0).Item("DateOfBirth").ToString
                    End Try

                    LblDateOfBirthSuspect.Text = strdate
                    LblBirthPlace.Text = Aml_Watchlist_JudgementSatuan.Rows(0).Item("BirthPlace").ToString()
                    AddressSuspectTextBox.Text = Aml_Watchlist_JudgementSatuan.Rows(0).Item("Alamat").ToString()
                    IdentityNumberSuspectTextBox.Text = Aml_Watchlist_JudgementSatuan.Rows(0).Item("identitynumber").ToString()
                    TxtCustomRemark1.Text = Aml_Watchlist_JudgementSatuan.Rows(0).Item("CustomRemark1").ToString()
                    TxtCustomRemark2.Text = Aml_Watchlist_JudgementSatuan.Rows(0).Item("CustomRemark2").ToString()
                    TxtCustomRemark3.Text = Aml_Watchlist_JudgementSatuan.Rows(0).Item("CustomRemark3").ToString()
                    TxtCustomRemark4.Text = Aml_Watchlist_JudgementSatuan.Rows(0).Item("CustomRemark4").ToString()
                    TxtCustomRemark5.Text = Aml_Watchlist_JudgementSatuan.Rows(0).Item("CustomRemark5").ToString()
                    Dim intcategoryworldcheck As String = EKABLL.ScreeningBLL.GetSystemParameterByValueByID(4000)
                    Dim strreffid As String = Aml_Watchlist_JudgementSatuan.Rows(0).Item("RefId").ToString
                    If Aml_Watchlist_JudgementSatuan.Rows(0).Item("CategoryID") = intcategoryworldcheck Then
                        linkcategory.Visible = True

                        LblListCategorySuspect.Visible = False
                    Else
                        linkcategory.Visible = False

                        LblListCategorySuspect.Visible = True
                    End If

                    linkcategory.OnClientClick = "window.open('showwatchlist.aspx?uid=" & strreffid & "', 'ca', 'width=500,height=550,left=500,top=500,resizable=yes,scrollbars=yes')"

                    'Dim StrAddressSuspect As String
                    'Dim StrIdentitySuspect As String

                    'For Each row As Data.DataRow In Aml_Watchlist_JudgementSatuan.Rows

                    '    StrAddressSuspect = row.Item("Alamat")
                    '    StrIdentitySuspect = row.Item("identitynumber")

                    '    Dim strresult As String = ""
                    '    If objradio.SelectedIndex = 0 Then
                    '        strresult = "Match with Name "
                    '    ElseIf objradio.SelectedIndex = 1 Then
                    '        strresult = "UnMatch with Name "
                    '    End If
                    '    ' Tampilkan
                    '    Try
                    '        arrlistvalue.Add(New ListItem(strresult & row.Item("DisplayName") & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB '" & Convert.ToDateTime(row.Item("DateOfBirth")).ToString("dd-MMMM-yyyy") & "';" & row.Item("ListTypeName") & ";" & row.Item("CategoryName") & vbCrLf, Aml_Customer_Judgement.Rows(0).Item("PK_Aml_Customer_Judgement_ID")))
                    '    Catch ex As Exception
                    '        arrlistvalue.Add(New ListItem(strresult & row.Item("DisplayName") & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB '';" & row.Item("ListTypeName") & ";" & row.Item("CategoryName") & vbCrLf, Aml_Customer_Judgement.Rows(0).Item("PK_Aml_Customer_Judgement_ID")))
                    '    End Try

                    '    Try
                    '        Me.TxtJudgementComment.Text = Me.TxtJudgementComment.Text & strresult & row.Item("DisplayName") & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB" & row.Item("DateOfBirth").ToString("dd-MMMM-yyyy") & ";" & row.Item("ListTypeName") & ";" & row.Item("CategoryName") & vbCrLf
                    '    Catch ex As Exception
                    '        Me.TxtJudgementComment.Text = Me.TxtJudgementComment.Text & strresult & row.Item("DisplayName") & ";Address [" & StrAddressSuspect & "] ;IdentityNumber [" & StrIdentitySuspect & "];DOB;" & row.Item("ListTypeName") & ";" & row.Item("CategoryName") & vbCrLf
                    '    End Try

                    'Next
                    'Session("JudgementCommentDetail") = arrlistvalue

                    Session("JudgementCommentDetailWebAPI") = GetStringJudgement()

                    Me.TxtJudgementComment.Text = Session("JudgementCommentDetailWebAPI")


                End If

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Function IsDataValid() As Boolean
        Dim strErrorMessage As New StringBuilder


        'For Each item As Data.DataRow In Aml_Customer_Judgement.Rows
        For Each item As Data.DataRow In AMLScreeningResultDetail.Rows
            If IsDBNull(item("JudgementResult")) Then
                strErrorMessage.Append("Please Select Match or UnMatch for Name " & item("DisplayName") & " and category " & item("CategoryName") & "<br/>")

            End If

        Next

        'For Each gridRow As DataGridItem In Me.GridMSUserView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim radioMatch As RadioButtonList = gridRow.FindControl("RadioButtonList1")

        '        If radioMatch.SelectedValue = "" Then
        '            strErrorMessage.Append("Please Select Match or UnMatch for Name " & gridRow.Cells(2).Text & "<br/>")
        '        End If
        '    End If
        'Next

        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New Exception(strErrorMessage.ToString)
        Else
            Return True
        End If

    End Function

    Function GetStringJudgement() As String

        'Dim strjudgement As String = ""
        Dim builder As New StringBuilder()
        Dim VerificationListId As Integer
        'Dim PK_Aml_Customer_Judgement_ID As Long
        'For Each item As Data.DataRow In Aml_Customer_Judgement.Rows
        For Each item As Data.DataRow In AMLScreeningResultDetail.Rows

            'Lanjutkan looping, tidak perlu diproses jika judgement result null
            If IsDBNull(item("JudgementResult")) Then
                Continue For
            End If

            VerificationListId = item("VerificationListId")
            'PK_Aml_Customer_Judgement_ID = item("PK_Aml_Customer_Judgement_ID")

            'Aml_Watchlist_JudgementSatuan = EKABLL.ScreeningBLL.GetWatchListData(VerificationListId, PK_Aml_Customer_Judgement_ID)
            Aml_Watchlist_JudgementSatuan = EKABLL.ScreeningBLL.GetAMLWatchListSuspectDataDetail(VerificationListId)

            If Aml_Watchlist_JudgementSatuan.Rows.Count = 0 Then
                Continue For
            End If

            If item("JudgementResult") = True Then
                builder.Append("Match with name ")
                'strjudgement += "Match with Name " & Aml_Watchlist_JudgementSatuan.Rows(0).Item("DisplayName") & ";Address [" & Aml_Watchlist_JudgementSatuan.Rows(0).Item("Alamat") & "] ;IdentityNumber [" & Aml_Watchlist_JudgementSatuan.Rows(0).Item("identitynumber") & "];DOB" & Aml_Watchlist_JudgementSatuan.Rows(0).Item("DateOfBirth").ToString & ";" & Aml_Watchlist_JudgementSatuan.Rows(0).Item("ListTypeName") & ";" & Aml_Watchlist_JudgementSatuan.Rows(0).Item("CategoryName") & vbCrLf
            Else
                builder.Append("UnMatch with name ")
                'strjudgement += "UnMatch with Name " & Aml_Watchlist_JudgementSatuan.Rows(0).Item("DisplayName") & ";Address [" & Aml_Watchlist_JudgementSatuan.Rows(0).Item("Alamat") & "] ;IdentityNumber [" & Aml_Watchlist_JudgementSatuan.Rows(0).Item("identitynumber") & "];DOB" & Aml_Watchlist_JudgementSatuan.Rows(0).Item("DateOfBirth").ToString & ";" & Aml_Watchlist_JudgementSatuan.Rows(0).Item("ListTypeName") & ";" & Aml_Watchlist_JudgementSatuan.Rows(0).Item("CategoryName") & vbCrLf
            End If

            builder.Append(Aml_Watchlist_JudgementSatuan.Rows(0).Item("DisplayName"))
            builder.Append(";Address [")
            builder.Append(Aml_Watchlist_JudgementSatuan.Rows(0).Item("Alamat"))
            builder.Append("] ;IdentityNumber [")
            builder.Append(Aml_Watchlist_JudgementSatuan.Rows(0).Item("identitynumber"))
            builder.Append("];DOB")
            builder.Append(Aml_Watchlist_JudgementSatuan.Rows(0).Item("DateOfBirth").ToString) 
            builder.Append(";")
            builder.Append(Aml_Watchlist_JudgementSatuan.Rows(0).Item("ListTypeName"))
            builder.Append(";")
            builder.AppendLine(Aml_Watchlist_JudgementSatuan.Rows(0).Item("CategoryName"))

        Next

        'Return strjudgement
        Return builder.ToString()
    End Function

    'Private Function IsApproval(ByVal PK_Aml_Customer_Judgement_ID As Long)

    '    Dim intcountApproval As Integer = 0
    '    Using objCommand As SqlCommand = Commonly.GetSQLCommandStoreProcedure("AML_usp_IsCustomerVerificationJudgementInApproval")
    '        objCommand.Parameters.Add(New SqlParameter("@PK_Aml_Customer_Judgement_ID", PK_Aml_Customer_Judgement_ID))
    '        intcountApproval = EkaDataNettier.Data.DataRepository.Provider.ExecuteScalar(objCommand)

    '        If intcountApproval > 0 Then
    '            Return True
    '        Else
    '            Return False
    '        End If
    '    End Using

    'End Function

    ''' <summary>
    ''' Save to Approval
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try

            If IsDataValid() Then

                'For Each item As Data.DataRow In Aml_Customer_Judgement.Rows
                For Each item As Data.DataRow In AMLScreeningResultDetail.Rows

                    Dim objdt As Data.DataTable = AMLScreeningResultDetail.Clone
                    objdt.ImportRow(item)

                    objdt.Rows(0).Item("JudgementBy") = Sahassa.AML.Commonly.SessionUserId
                    objdt.Rows(0).Item("JudgementComment") = TxtJudgementComment.Text.Trim
                    'objdt.Rows(0).Item("JudgementDate") = Now

                    'EKABLL.ScreeningBLL.saveScreening(objdt)
                    EKABLL.ScreeningBLL.SaveAMLScreeningJudgementWebAPI(objdt)
                Next




                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=8893&Identifier=" & Me.LblName.Text, False)

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    'Private Sub DeleteAllApproval(ByVal appid As Long)
    '    Try
    '        Using AccessDeleteQuery As New AMLDAL.CustomerVerificationJudgementTableAdapters.QueriesCustomerVerificationJudgement
    '            AccessDeleteQuery.DeleteCustomerVerificationJudgement_Approval(appid)
    '            AccessDeleteQuery.DeleteCustomerVerificationJudgementDetail_Approval(appid)
    '        End Using
    '    Catch
    '        Throw
    '    End Try
    'End Sub

    'Private Sub InsertHistoryApproval(ByVal appid As Long)
    '    Try
    '        'Using TransScope As New Transactions.TransactionScope
    '        Using QueryInsertHistory As New AMLDAL.CustomerVerificationJudgementTableAdapters.QueriesCustomerVerificationJudgement
    '            QueryInsertHistory.InsertCustomerVerificationJudgement_Approval_History(appid, CDate(Me.LabelTglJudgement.Text).ToShortDateString, Me.LblCIFNo.Text, Me.LabelUserJudgement.Text, Sahassa.AML.Commonly.SessionUserId, Me.TxtJudgementComment.Text)
    '            For Each gridRow As DataGridItem In Me.GridMSUserView.Items
    '                If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
    '                    Dim radiolist As RadioButtonList = gridRow.FindControl("RadioButtonList1")
    '                    Dim IsMatch As Boolean = True
    '                    If radiolist.SelectedIndex = 1 Then
    '                        IsMatch = False
    '                    End If
    '                    QueryInsertHistory.InsertCustomerVerificationJudgementDetail_Approval_History(gridRow.Cells(0).Text, appid, gridRow.Cells(8).Text, gridRow.Cells(6).Text, IsMatch)
    '                End If
    '            Next
    '        End Using
    '        'TransScope.Complete()
    '        'End Using
    '    Catch
    '        Throw
    '    End Try
    'End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "CustomerVerificationJudgementByRequestIDWebAPIView.aspx"

        Response.Redirect("CustomerVerificationJudgementByRequestIDWebAPIView.aspx")
    End Sub

    Protected Sub GridCustomer_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles GridCustomer.RowEditing
        Try

            'Dim strcifno As String = GridCustomer.Rows(e.NewEditIndex).Cells(0).Text
            'Dim strrelatedtype As String = GridCustomer.Rows(e.NewEditIndex).Cells(1).Text

            'Dim intrelatedtype As Integer = EKABLL.ScreeningBLL.Getrelatetypeidbyname(strrelatedtype)

            'Dim strname As String = GridCustomer.Rows(e.NewEditIndex).Cells(2).Text

            'Dim objarr As Data.DataRow()
            'objarr = Aml_Watchlist_JudgementALL.Select("CIFNo='" & strcifno & "' and FK_AML_RelatedCustomerType_ID=" & intrelatedtype & " and Nama='" & strname.Replace("'", "''") & "'")

            'Aml_Watchlist_JudgementFilter = New Data.DataTable
            'Aml_Watchlist_JudgementFilter = Aml_Watchlist_JudgementALL.Clone
            'For Each objdata As Data.DataRow In objarr
            '    Aml_Watchlist_JudgementFilter.ImportRow(objdata)
            'Next

            'Me.GridMSUserView.DataSource = Aml_Watchlist_JudgementFilter

            AMLScreeningCustomerPK = GridCustomer.Rows(e.NewEditIndex).Cells(0).Text

            Me.GridMSUserView.DataSource = AMLScreeningResultDetail
            Me.GridMSUserView.PageSize = Int32.MaxValue
            Me.GridMSUserView.DataBind()


            ClearInput()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Sub ClearInput()
        'LblCIFNo.Text = ""
        lblRequestID.Text = ""
        LblName.Text = ""
        LblRelatedCustomerType.Text = ""
        LblBranch.Text = ""
        LblOpeningDate.Text = ""
        LblDateOfBirth.Text = ""
        lblBirthPlaceCIF.Text = ""
        AddressTextBox.Text = ""
        IdentityNumberTextBox.Text = ""
        TxtPhoneNumberCustomer.Text = ""
        NameSuspectTextBox.Text = ""
        LblListTypeSuspect.Text = ""
        LblListCategorySuspect.Text = ""
        LblBirthPlace.Text = ""
        AddressSuspectTextBox.Text = ""
        IdentityNumberSuspectTextBox.Text = ""
        TxtCustomRemark1.Text = ""
        TxtCustomRemark2.Text = ""
        TxtCustomRemark3.Text = ""
        TxtCustomRemark4.Text = ""
        TxtCustomRemark5.Text = ""
        linkcategory.Visible = False

    End Sub
    Private Sub GridMSUserView_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim currentPK As Integer = e.Item.Cells(0).Text
                'Dim VerificationListId As Integer = e.Item.Cells(1).Text


                Dim objradio As RadioButtonList = e.Item.FindControl("RadioButtonList1")


                'Dim dr As Data.DataRow() = Aml_Customer_Judgement.Select("Request_ID=" & requestID)
                Dim dr As Data.DataRow() = AMLScreeningResultDetail.Select("PK_AML_WebAPIScreening_Result_Detail_ID=" & currentPK)

                If dr.Length = 0 Then
                    Exit Sub
                End If

                If Not IsDBNull(dr(0).Item("JudgementResult")) Then
                    If dr(0).Item("JudgementResult").ToString.ToLower = "true" Then
                        objradio.Items(0).Selected = True
                    Else
                        objradio.Items(1).Selected = True
                    End If
                Else
                    objradio.ClearSelection()


                End If
            End If
            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class