<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TransactionTypeAuxiliaryMappingApprovalDetail.aspx.vb" Inherits="TransactionTypeAuxiliaryMappingApprovalDetail" MasterPageFile="~/MasterPage.master" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">

  <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/dot_title.gif" />
                <strong>Transaction Type Auxiliary Transaction Code Mapping Approval - Detail&nbsp; </strong>
            </td>
        </tr>
    </table>
    <TABLE borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
        <TR>
            <TD align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></TD>
				<TD vAlign="top" width="98%" bgColor="#ffffff">
				

                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                            <asp:View ID="ViewEdit" runat="server">
            				
                                <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="1" cellspacing="1"
                                    width="100%">
                                    <tr style="background-color: #ffffff">
                                        <td colspan="6">
                                            Activity : Edit</td>
                                    </tr>
                                    <tr style="background-color: #ffffff">
                                        <td colspan="3"><strong>Old Value</strong></td>
                                        <td colspan="3"><strong>New Value</strong></td>
                                    </tr>
                                    <tr style="background-color: #ffffff">
                                        <td style="width: 113px">
                                            Auxiliary Transaction</td>
                                        <td style="width: 1%">
                                            :</td>
                                        <td style="width: 30%">
                                            &nbsp;<asp:Label ID="TxtAuxTransOld" runat="server"></asp:Label>
                                        </td>

                                        <td style="width: 113px">
                                            Auxiliary Transaction</td>
                                        <td style="width: 1%">
                                            :</td>
                                        <td style="width: 75%">
                                            &nbsp;<asp:Label ID="TxtAuxTransNew" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr style="background-color: #ffffff">
                                        <td style="width: 113px">
                                            Transaction Type</td>
                                        <td style="width: 1%">
                                            :</td>
                                        <td style="width: 30%">
                                            &nbsp;<asp:Label ID="TxtTransTypeOld" runat="server"></asp:Label>
                                        </td>

                                        <td style="width: 113px">
                                            Transaction Type</td>
                                        <td style="width: 1%">
                                            :</td>
                                        <td style="width: 75%">
                                            &nbsp;<asp:Label ID="TxtTransTypeNew" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    
                                    <td colspan="6">
                                        <asp:ImageButton ID="ImgBtnAcceptEdit" runat="server" ImageUrl="~/Images/button/accept.gif"/>
                                        &nbsp;<asp:ImageButton ID="ImgBtnRejectEdit" runat="server" ImageUrl="~/Images/button/reject.gif" EnableViewState="False" />
                                        &nbsp;<asp:ImageButton ID="ImgBtnBackEdit" runat="server" CausesValidation="False" ImageUrl="~/Images/button/back.gif" EnableViewState="False" />
                                    </td>                        
                                </table>                				
			                </asp:View>


                            <asp:View ID="ViewDelete" runat="server">
            				
                                <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="1" cellspacing="1"
                                    width="100%">
                                    <tr style="background-color: #ffffff">
                                        <td colspan="3">
                                            Activity : Delete</td>
                                    </tr>
                                    <tr style="background-color: #ffffff">
                                        <td style="width: 113px">
                                            Auxiliary Transaction</td>
                                        <td style="width: 1%">
                                            :</td>
                                        <td style="width: 75%">
                                            &nbsp;<asp:Label ID="TxtAuxTranDel" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr style="background-color: #ffffff">
                                        <td style="width: 113px">
                                            Transaction Type</td>
                                        <td style="width: 1%">
                                            :</td>
                                        <td style="width: 75%">
                                            &nbsp;<asp:Label ID="TxtTransTypeDel" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    
                                    <td colspan="3">
                                        <asp:ImageButton ID="ImgBtnAcceptDelete" runat="server" ImageUrl="~/Images/button/accept.gif"/>
                                        &nbsp;<asp:ImageButton ID="ImgBtnRejectDelete" runat="server" ImageUrl="~/Images/button/reject.gif" EnableViewState="False" />
                                        &nbsp;<asp:ImageButton ID="ImgBtnBackDelete" runat="server" CausesValidation="False" ImageUrl="~/Images/button/back.gif" EnableViewState="False" />
                                    </td>                        
                                    
                                </table>                				
			                </asp:View>
		                </asp:MultiView>&nbsp;
                    <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                        <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
                    </ajax:AjaxPanel>
                </ajax:AjaxPanel>


				</TD>
			</TR>
	</TABLE>
</asp:Content>