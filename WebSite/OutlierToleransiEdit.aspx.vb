﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class OutlierToleransiEdit
    Inherits Parent

    ReadOnly Property GetPk_OutlierToleransi As Long
        Get
            If IsNumeric(Request.Params("OutlierToleransiID")) Then
                If Not IsNothing(Session("OutlierToleransiEdit.PK")) Then
                    Return CLng(Session("OutlierToleransiEdit.PK"))
                Else
                    Session("OutlierToleransiEdit.PK") = Request.Params("OutlierToleransiID")
                    Return CLng(Session("OutlierToleransiEdit.PK"))
                End If
            End If
            Return 0
        End Get
    End Property

    Sub ClearSession()
        Session("OutlierToleransiEdit.PK") = Nothing
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Not Page.ClientScript.IsClientScriptIncludeRegistered("idpopup1") Then
                Page.ClientScript.RegisterClientScriptInclude(Me.GetType(), "idpopup1", ResolveClientUrl("script/popupdrag.js"))
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Sub SelectUser(ByVal IntUserid As String)
        haccountowner.Value = IntUserid
        LblAccountOwner.Text = AMLBLL.SuspiciusPersonBLL.GetAccountOwnerIDbyPk(IntUserid)
    End Sub


    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "OutlierToleransiView.aspx"

            Me.Response.Redirect("OutlierToleransiView.aspx", False)
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Function isValidData() As Boolean
        Try
            If haccountowner.Value.ToString.Trim.Length = 0 Then
                Throw New Exception("Account Owner must be choosen")
            End If
            If cboSegment.SelectedValue = "0" Then
                Throw New Exception("Segment must be choosen")
            End If
            If txtToleransiDebet.Text.Trim.Length = 0 Then
                Throw New Exception("Toleransi Debet must be filled")
            End If
            If Not IsNumeric(txtToleransiDebet.Text) Then
                Throw New Exception("Toleransi Debet must be filled by number")
            End If
            If txtToleransiKredit.Text.Trim.Length = 0 Then
                Throw New Exception("Toleransi Kredit must be filled")
            End If
            If Not IsNumeric(txtToleransiKredit.Text) Then
                Throw New Exception("Toleransi Kredit must be filled by number")
            End If
            Return True
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
    End Function

    Sub ClearControl()
        haccountowner.Value = ""
        LblAccountOwner.Text = "Please Select Account Owner"
        cboSegment.SelectedIndex = 0
        txtToleransiDebet.Text = ""
        txtToleransiKredit.Text = ""
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        If isValidData() Then
            Try

                Using objUser As User = AMLBLL.UserBLL.GetUserByPkUserID(Sahassa.AML.Commonly.SessionPkUserId)
                    If Not IsNothing(objUser) Then
                        Using objOutlierToleransi As OutlierToleransi = OutlierToleransiBLL.getOutlierToleransiByPk(GetPk_OutlierToleransi)
                            If Not IsNothing(objOutlierToleransi) Then
                                objOutlierToleransi.AccountOwnerID = haccountowner.Value
                                objOutlierToleransi.Segment = cboSegment.SelectedValue
                                objOutlierToleransi.ToleransiDebet = txtToleransiDebet.Text
                                objOutlierToleransi.ToleransiKredit = txtToleransiKredit.Text
                                objOutlierToleransi.Activation = True
                                'objOutlierToleransi.CreatedDate = Now
                                'objOutlierToleransi.CreatedBy = objUser.UserName
                                objOutlierToleransi.LastUpdateDate = Now
                                objOutlierToleransi.LastUpdateBy = objUser.UserName



                                If Sahassa.AML.Commonly.SessionGroupName.ToLower = "superuser" Then
                                    If OutlierToleransiBLL.SaveEdit(objOutlierToleransi) Then
                                        lblMessage.Text = "Edit Data Success"
                                        mtvOutLierToleransiEdit.ActiveViewIndex = 1
                                    End If
                                Else
                                    If OutlierToleransiBLL.SaveEditApproval(objOutlierToleransi) Then
                                        lblMessage.Text = "Edited Data has been inserted to Approval"
                                        mtvOutLierToleransiEdit.ActiveViewIndex = 1
                                    End If
                                End If
                            End If
                        End Using
                    End If
                End Using

            Catch ex As Exception
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub

    Sub LoadSegment()
        cboSegment.Items.Clear()
        cboSegment.AppendDataBoundItems = True
        cboSegment.Items.Add(New ListItem("--None--", 0))
        cboSegment.DataSource = SegmentBLL.GetSegment(SegmentColumn.Activation.ToString & "=1", SegmentColumn.SegmentCode.ToString, 0, Integer.MaxValue, 0)
        cboSegment.DataTextField = SegmentColumn.SegmentCode.ToString
        cboSegment.DataValueField = SegmentColumn.SegmentCode.ToString
        cboSegment.DataBind()
    End Sub

    Sub LoadData()
        Using objOutlierToleransi As OutlierToleransi = OutlierToleransiBLL.getOutlierToleransiByPk(GetPk_OutlierToleransi)
            If Not IsNothing(objOutlierToleransi) Then
                haccountowner.Value = objOutlierToleransi.AccountOwnerID.ToString
                'LblAccountOwner.Text = objOutlierToleransi..ToString
                cboSegment.SelectedValue = objOutlierToleransi.Segment
                SelectUser(objOutlierToleransi.AccountOwnerID)
                txtToleransiDebet.Text = objOutlierToleransi.ToleransiDebet.ToString
                txtToleransiKredit.Text = objOutlierToleransi.ToleransiKredit.ToString
            Else
                ImageSave.Visible = False
            End If
        End Using
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            AddHandler PopUpAccountOwner1.SelectUser, AddressOf SelectUser
            If Not Page.IsPostBack Then
                ClearSession()
                mtvOutLierToleransiEdit.ActiveViewIndex = 0

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                LoadSegment()
                LoadData()
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            mtvOutLierToleransiEdit.ActiveViewIndex = 0
            ClearControl()
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBack.Click
        Try
            Response.Redirect("OutlierToleransiView.aspx")
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgBrowse_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBrowse.Click
        Try
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "idpopup2", "popUpDrag('" & CType(sender, ImageButton).ClientID & "','divBrowseUser');", True)
            PopUpAccountOwner1.initData()

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgCancel.Click
        haccountowner.Value = ""
        LblAccountOwner.Text = "Please Select Account Owner"
    End Sub
End Class


