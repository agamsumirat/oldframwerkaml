Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL.CDDLNPBLL

Partial Class CDDLNPAttachmentUpload
    Inherits System.Web.UI.Page

    Protected Sub ImageButtonInsert_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonInsert.Click
        Try
            If FileUpload1.HasFile Then
                SessionCDDLNP_Attachment.Add(CDDLNP_Attachment.CreateCDDLNP_Attachment(Nothing, FileUpload1.FileName, FileUpload1.FileBytes, FileUpload1.PostedFile.ContentType))
                BindListAttachment()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub ImageButtonRemove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonRemove.Click
        Try
            If ListAttachment.SelectedIndex <> -1 Then
                SessionCDDLNP_Attachment.RemoveAt(ListAttachment.SelectedValue)
                BindListAttachment()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindListAttachment()
        Try
            ListAttachment.Items.Clear()
            For Each objAttachment As CDDLNP_Attachment In SessionCDDLNP_Attachment
                objAttachment.PK_CDDLNP_AttachmentID = SessionCDDLNP_Attachment.IndexOf(objAttachment)
                ListAttachment.Items.Add(New ListItem(objAttachment.FileName, objAttachment.PK_CDDLNP_AttachmentID))
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                BindListAttachment()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class