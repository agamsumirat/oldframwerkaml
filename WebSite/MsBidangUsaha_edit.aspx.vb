#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
#End Region

Partial Class MsBidangUsaha_edit
    Inherits Parent

#Region "Function"

    Private Sub LoadData()
        Dim ObjMsBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetPaged(MsBidangUsahaColumn.IdBidangUsaha.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsBidangUsaha Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsBidangUsaha
            SafeDefaultValue = "-"
            txtIdBidangUsaha.Text = Safe(.IdBidangUsaha)
            txtNamaBidangUsaha.Text = Safe(.NamaBidangUsaha)

            chkActivation.Checked = SafeBoolean(.Activation)
            Dim L_objMappingBidangUsahaNCBS As TList(Of MappingBidangUsahaNCBS)
            L_objMappingBidangUsahaNCBS = DataRepository.MappingBidangUsahaNCBSProvider.GetPaged(MappingBidangUsahaNCBSColumn.BidangUsahaId.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


            Listmaping.AddRange(L_objMappingBidangUsahaNCBS)

        End With
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub

    ''' <summary>
    ''' Validasi men-handle seluruh syarat2 data valid
    ''' data yang lolos dari validasi dipastikan lolos ke database 
    ''' karena menggunakan FillorNothing
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ValidasiControl()
        '======================== Validasi textbox :  ID canot Null ====================================================
        If ObjectAntiNull(txtIdBidangUsaha.Text) = False Then Throw New Exception("ID  must be filled  ")
        If Not IsNumeric(Me.txtIdBidangUsaha.Text) Then
            Throw New Exception("ID must be in number format")
        End If
        '======================== Validasi textbox :  Nama canot Null ====================================================
        If ObjectAntiNull(txtNamaBidangUsaha.Text) = False Then Throw New Exception("Nama  must be filled  ")

        If DataRepository.MsBidangUsahaProvider.GetPaged("IDBidangUsaha = '" & txtIdBidangUsaha.Text & "' AND IDBidangUsaha <> '" & parID & "'", "", 0, Integer.MaxValue, 0).Count > 0 Then
            Throw New Exception("ID already exist in the database")
        End If

        If DataRepository.MsBidangUsaha_ApprovalDetailProvider.GetPaged(MsBidangUsaha_ApprovalDetailColumn.IdBidangUsaha.ToString & "='" & txtIdBidangUsaha.Text & "' AND IDBidangUsaha <> '" & parID & "'", "", 0, 1, Nothing).Count > 0 Then
            Throw New Exception("Data exist in List Waiting Approval")
        End If
        'If LBMapping.Items.Count = 0 Then Throw New Exception("data tidak punya list Mapping")
    End Sub

#End Region

#Region "events..."

    Protected Sub imgBrowse_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBrowse.Click
        Try
            If Not Get_DataFromPicker Is Nothing Then
                Listmaping.AddRange(Get_DataFromPicker)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgDeleteItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDeleteItem.Click
        Try
            If LBMapping.SelectedIndex < 0 Then
                Throw New Exception("No data Selected")
            End If
            Listmaping.RemoveAt(LBMapping.SelectedIndex)

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                Listmaping = New TList(Of MappingBidangUsahaNCBS)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then
                ValidasiControl()
                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsBidangUsaha_Approval As New MsBidangUsaha_Approval
                    With ObjMsBidangUsaha_Approval
                        FillOrNothing(.FK_MsMode_Id, 2, True, oInt)
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsBidangUsaha_ApprovalProvider.Save(ObjMsBidangUsaha_Approval)
                    KeyHeaderApproval = ObjMsBidangUsaha_Approval.PK_MsBidangUsaha_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsBidangUsaha_ApprovalDetail As New MsBidangUsaha_ApprovalDetail()
                    With objMsBidangUsaha_ApprovalDetail
                        Dim ObjMsBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(parID)
                        FillOrNothing(.IdBidangUsaha, ObjMsBidangUsaha.IdBidangUsaha)
                        FillOrNothing(.NamaBidangUsaha, ObjMsBidangUsaha.NamaBidangUsaha)
                        FillOrNothing(.Activation, ObjMsBidangUsaha.Activation)
                        FillOrNothing(.CreatedDate, ObjMsBidangUsaha.CreatedDate)
                        FillOrNothing(.CreatedBy, ObjMsBidangUsaha.CreatedBy)
                        FillOrNothing(.FK_MsBidangUsaha_Approval_Id, KeyHeaderApproval)

                        '==== Edit data =============
                        FillOrNothing(.IdBidangUsaha, txtIdBidangUsaha.Text, True, oInt)
                        FillOrNothing(.NamaBidangUsaha, txtNamaBidangUsaha.Text, True, Ovarchar)

                        FillOrNothing(.Activation, chkActivation.Checked, True, oBit)
                        FillOrNothing(.LastUpdatedBy, SessionUserId)
                        FillOrNothing(.LastUpdatedDate, Date.Now)

                    End With
                    DataRepository.MsBidangUsaha_ApprovalDetailProvider.Save(objMsBidangUsaha_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsBidangUsaha_ApprovalDetail.PK_MsBidangUsaha_ApprovalDetail_Id
                    '========= Insert mapping item 
                    Dim LobjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail As New TList(Of MappingBidangUsahaNCBS_Approval_Detail)
                    For Each objMappingBidangUsahaNCBS As MappingBidangUsahaNCBS In Listmaping
                        Dim Mapping As New MappingBidangUsahaNCBS_Approval_Detail
                        With Mapping
                            FillOrNothing(.BidangUsahaId, objMsBidangUsaha_ApprovalDetail.IdBidangUsaha)
                            FillOrNothing(.IDBidangUsahaNCBS, objMappingBidangUsahaNCBS.IDBidangUsahaNCBS)
                            FillOrNothing(.PK_MsBidangUsaha_Approval_id, KeyHeaderApproval)
                            FillOrNothing(.NAMA, objMappingBidangUsahaNCBS.Nama)
                            FillOrNothing(.PK_MappingBidangUsahaNCBS_Approval_detail_id, keyHeaderApprovalDetail)
                            LobjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail.Add(Mapping)
                        End With
                    Next
                    DataRepository.MappingBidangUsahaNCBS_Approval_DetailProvider.Save(LobjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail)
                    'session Maping item Clear
                    Listmaping.Clear()
                    ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
                    LblConfirmation.Text = "Data has been added and waiting for approval"
                    MtvMsUser.ActiveViewIndex = 1
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub



    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Response.Redirect("MsBidangUsaha_View.aspx")
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Response.Redirect("MsBidangUsaha_View.aspx")
    End Sub
#End Region

#Region "Property..."
    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Mengambil Data dari Picker
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Get_DataFromPicker() As TList(Of MappingBidangUsahaNCBS)
        Get
            Dim Temp As New TList(Of MappingBidangUsahaNCBS)
            Dim L_MsBidangUsahaNCBS As TList(Of MsBidangUsahaNCBS)
            Try
                If Session("PickerMSBIDANGUSAHANCBS.Data") Is Nothing Then
                    Throw New Exception
                End If
                L_MsBidangUsahaNCBS = Session("PickerMSBIDANGUSAHANCBS.Data")
                For Each i As MsBidangUsahaNCBS In L_MsBidangUsahaNCBS
                    Dim Tempmapping As New MappingBidangUsahaNCBS
                    With Tempmapping
                        .IDBidangUsahaNCBS = i.IDBidangUsahaNCBS
                        .Nama = i.NamaBidangUsahaNCBS
                    End With
                    Temp.Add(Tempmapping)
                Next
            Catch ex As Exception
                L_MsBidangUsahaNCBS = Nothing
            End Try
            Return Temp
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingBidangUsahaNCBS)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingBidangUsahaNCBS))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingBidangUsahaNCBS In Listmaping.FindAllDistinct("IDBidangUsahaNCBS")
                Temp.Add(i.IDBidangUsahaNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property
#End Region

End Class



