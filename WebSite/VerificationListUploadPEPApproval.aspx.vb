Imports System.Data.SqlClient
Partial Class VerificationListUploadPEPApproval
    Inherits Parent

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Using PEPListPendingApprovalAdapter As New AMLDAL.PEPListTableAdapters.PEPList_PendingApprovalTableAdapter
                    Dim PEPListPendingApprovalTable As New AMLDAL.PEPList.PEPList_PendingApprovalDataTable
                    PEPListPendingApprovalTable = PEPListPendingApprovalAdapter.GetData()
                    If PEPListPendingApprovalTable.Rows.Count > 0 Then
                        Me.TableUploadData.Visible = True
                        Me.TableNoPendingApproval.Visible = False
                        Me.ImageAccept.Visible = True
                        Me.ImageReject.Visible = True
                        Me.ImageBack.Visible = True
                        Using PEPListAdapter As New AMLDAL.PEPListTableAdapters.PEPListTableAdapter
                            Me.GridPendingApprovalPEPList.DataSource = PEPListAdapter.GetData
                            Me.GridPendingApprovalPEPList.DataBind()
                        End Using
                    Else
                        Me.TableUploadData.Visible = False
                        Me.TableNoPendingApproval.Visible = True
                        Me.ImageAccept.Visible = False
                        Me.ImageReject.Visible = False
                        Me.ImageBack.Visible = True
                    End If
                End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Private Function ExecNonQuery(ByVal SQLQuery As String) As Integer
        Dim SqlConn As SqlConnection = Nothing
        Dim SqlCmd As SqlCommand = Nothing
        Try
            SqlConn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("AMLConnectionString").ToString)
            SqlConn.Open()
            SqlCmd = New SqlCommand
            SqlCmd.Connection = SqlConn
            SqlCmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings.Get("SQLCommandTimeout")
            SqlCmd.CommandText = SQLQuery
            Return SqlCmd.ExecuteNonQuery()
        Catch tex As Threading.ThreadAbortException
            Throw tex
        Catch ex As Exception
            Throw ex
        Finally
            If Not SqlConn Is Nothing Then
                SqlConn.Close()
                SqlConn.Dispose()
                SqlConn = Nothing
            End If
            If Not SqlCmd Is Nothing Then
                SqlCmd.Dispose()
                SqlCmd = Nothing
            End If
        End Try
    End Function

    Private Function ExecuteProcessPEPList(ByVal ProcessDate As DateTime) As Boolean
        ExecNonQuery("EXEC usp_ProcessPEPList '" & ProcessDate.ToString("yyyy-MM-dd") & "'")
        Return True
    End Function

    Private Sub InsertAuditTrail(ByVal ApprovalStatusDescription As String)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(1)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "ALL", "Update", "", "", "Upload PEPList Approval - " & ApprovalStatusDescription)
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Using PEPListPendingApprovalAdapter As New AMLDAL.PEPListTableAdapters.PEPList_PendingApprovalTableAdapter
                Dim PEPListPendingApprovalTable As New AMLDAL.PEPList.PEPList_PendingApprovalDataTable
                Dim PEPListPendingApprovalTableRow As AMLDAL.PEPList.PEPList_PendingApprovalRow
                PEPListPendingApprovalTable = PEPListPendingApprovalAdapter.GetData()

                If PEPListPendingApprovalTable.Rows.Count > 0 Then
                    PEPListPendingApprovalTableRow = PEPListPendingApprovalTable.Rows(0)
                    ' update sdn list
                    ExecuteProcessPEPList(PEPListPendingApprovalTableRow.PEPList_PendingApprovalEntryDate)
                    ' delete from approval
                    PEPListPendingApprovalAdapter.DeleteAllPendingApproval()
                    ' insert to audit trail
                    InsertAuditTrail("Accepted")
                    ' clear table
                    ExecNonQuery("TRUNCATE TABLE PEPList")
                End If
            End Using
            Dim MessagePendingID As Integer = 8869 'MessagePendingID 8869 = PEP List Approval approved saved successfully
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID, False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Using PEPListPendingApprovalAdapter As New AMLDAL.PEPListTableAdapters.PEPList_PendingApprovalTableAdapter
                Dim PEPListPendingApprovalTable As New AMLDAL.PEPList.PEPList_PendingApprovalDataTable
                PEPListPendingApprovalTable = PEPListPendingApprovalAdapter.GetData()
                If PEPListPendingApprovalTable.Rows.Count > 0 Then
                    ' delete from approval
                    PEPListPendingApprovalAdapter.DeleteAllPendingApproval()
                    ' insert to audit trail
                    InsertAuditTrail("Rejected")
                    ' clear table
                    ExecNonQuery("TRUNCATE TABLE PEPList")
                End If
            End Using
            Dim MessagePendingID As Integer = 8870 'MessagePendingID 8870 = PEP List Approval rejected saved successfully
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID, False)

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Me.Response.Redirect("Default.aspx", False)
    End Sub

End Class
