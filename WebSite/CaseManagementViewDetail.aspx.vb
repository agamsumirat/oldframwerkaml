Imports System.Data.SqlClient
Imports AMLBLL
Imports Sahassa.AML.TableAdapterHelper
Imports EkaDataNettier.Data
Imports EkaDataNettier.Entities

Partial Class CaseManagementViewDetail
    Inherits Parent


    Shared Function GetSuspiciusPersonByPk(ByVal intPkSuspiciusPersonId As Integer) As SuspiciusPerson


        Using objsuspiciusperson As SuspiciusPerson = DataRepository.SuspiciusPersonProvider.GetByPK_SuspiciusPerson_ID(intPkSuspiciusPersonId)
            Return objsuspiciusperson
        End Using

    End Function

    Public Property ObjTMapCaseManagementWorkflowHistoryDraft() As TList(Of MapCaseManagementWorkflowHistory)
        Get
            If Session("CaseManagementViewDetail.ObjTMapCaseManagementWorkflowHistoryDraft") Is Nothing Then
                Session("CaseManagementViewDetail.ObjTMapCaseManagementWorkflowHistoryDraft") = CaseManagementBLL.GetTlistMapCaseManagementWorkflowHistory(MapCaseManagementWorkflowHistoryColumn.FK_EventTypeID.ToString & "=" & Eventtype.TaskDraft & " and " & MapCaseManagementWorkflowHistoryColumn.FK_CaseManagementID.ToString & "=" & Me.PK_CaseManagementID & " and " & MapCaseManagementWorkflowHistoryColumn.UserID.ToString & "='" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "'", "", 0, Integer.MaxValue, 0)
                Return CType(Session("CaseManagementViewDetail.ObjTMapCaseManagementWorkflowHistoryDraft"), TList(Of MapCaseManagementWorkflowHistory))
            Else
                Return CType(Session("CaseManagementViewDetail.ObjTMapCaseManagementWorkflowHistoryDraft"), TList(Of MapCaseManagementWorkflowHistory))
            End If
        End Get
        Set(ByVal value As TList(Of MapCaseManagementWorkflowHistory))
            Session("CaseManagementViewDetail.ObjTMapCaseManagementWorkflowHistoryDraft") = value
        End Set
    End Property

    Private _oRowCaseManagementWorkFlow As AMLDAL.CaseManagement.MapCaseManagementWorkflowRow

    Private ReadOnly Property oRowCaseManagementWorkFlow() As AMLDAL.CaseManagement.MapCaseManagementWorkflowRow
        Get
            Try
                If Not _oRowCaseManagementWorkFlow Is Nothing Then
                    Return _oRowCaseManagementWorkFlow
                Else
                    Using adapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementWorkflowTableAdapter
                        Using oTable As AMLDAL.CaseManagement.MapCaseManagementWorkflowDataTable = adapter.GetCaseManagementWorkFlowByFK(Me.PK_CaseManagementID)
                            If oTable.Rows.Count > 0 Then
                                _oRowCaseManagementWorkFlow = oTable.Rows(0)
                                Return _oRowCaseManagementWorkFlow
                            Else
                                Throw New Exception("There is no data workflow for PKManagementID " & Me.PK_CaseManagementID)
                            End If
                        End Using
                    End Using
                End If
            Catch ex As Exception
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End Get
    End Property

    Public ReadOnly Property objTListCaseManagementSameCase() As TList(Of CaseManagementSameCaseHistory)
        Get
            If Session("CaseManagementViewDetail.objTListCaseManagementSameCase") Is Nothing Then

                Using Resources As TList(Of CaseManagementSameCaseHistory) = DataRepository.CaseManagementSameCaseHistoryProvider.GetPaged(CaseManagementSameCaseHistoryColumn.FK_CaseManagement_ID.ToString & "=" & Me.PK_CaseManagementID, "", 0, Integer.MaxValue, 0)

                    Session("CaseManagementViewDetail.objTListCaseManagementSameCase") = Resources
                End Using
                Return CType(Session("CaseManagementViewDetail.objTListCaseManagementSameCase"), TList(Of CaseManagementSameCaseHistory))
            Else
                Return CType(Session("CaseManagementViewDetail.objTListCaseManagementSameCase"), TList(Of CaseManagementSameCaseHistory))
            End If

        End Get
    End Property

    Public ReadOnly Property ObjDataMapCaseManagementTransactionNew() As Data.DataTable
        Get
            If Session("CaseManagementViewDetail.ObjDataMapCaseManagementTransactionNew") Is Nothing Then

                Dim strxmldata As String = EKABLL.CaseManagementBLL.CaseManagementtransactionNew(Me.PK_CaseManagementID)

                If strxmldata <> "" Then
                    Dim objnewtable As New Data.DataSet
                    Dim odatareader As System.IO.TextReader = New IO.StringReader(strxmldata)

                    objnewtable.ReadXml(odatareader)

                    Session("CaseManagementViewDetail.ObjDataMapCaseManagementTransactionNew") = objnewtable.Tables(0)
                    Return CType(Session("CaseManagementViewDetail.ObjDataMapCaseManagementTransactionNew"), Data.DataTable)
                Else
                    Return Nothing
                End If

            Else
                Return CType(Session("CaseManagementViewDetail.ObjDataMapCaseManagementTransactionNew"), Data.DataTable)
            End If

        End Get

    End Property

    Private _oRowCaseManagementTransaction As Data.DataTable

    Private ReadOnly Property oRowCaseManagementTransaction() As Data.DataTable
        Get
            Try
                If Not _oRowCaseManagementTransaction Is Nothing Then
                    Return _oRowCaseManagementTransaction
                Else

                    Using objdt As Data.DataTable = AMLBLL.CaseManagementBLL.CaseManagementProfileKeuanganTransactionByPK(Me.PK_CaseManagementID)
                        Return objdt
                    End Using

                    'Using adapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementTransactionTableAdapter
                    '    _oRowCaseManagementTransaction = adapter.GetMapCaseManagementTransactionByFK(Me.PK_CaseManagementID)
                    '    Return _oRowCaseManagementTransaction
                    'End Using
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property

    Public ReadOnly Property ObjCaseManagementProfileTopology() As TList(Of CaseManagementProfileTopology)
        Get
            If Session("CaseManagementViewDetail.CaseManagementProfileTopology") Is Nothing Then

                Using Resources As TList(Of CaseManagementProfileTopology) = DataRepository.CaseManagementProfileTopologyProvider.GetPaged(CaseManagementProfileTopologyColumn.FK_CaseManagement_ID.ToString & "=" & Me.PK_CaseManagementID, "", 0, Integer.MaxValue, 0)

                    Session("CaseManagementViewDetail.CaseManagementProfileTopology") = Resources
                End Using
                Return CType(Session("CaseManagementViewDetail.CaseManagementProfileTopology"), TList(Of CaseManagementProfileTopology))
            Else
                Return CType(Session("CaseManagementViewDetail.CaseManagementProfileTopology"), TList(Of CaseManagementProfileTopology))
            End If

        End Get
    End Property

    Public ReadOnly Property ObjCaseManagementProfileKeuangan() As TList(Of CaseManagementProfileKeuangan)
        Get
            If Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuangan") Is Nothing Then

                Using Resources As TList(Of CaseManagementProfileKeuangan) = DataRepository.CaseManagementProfileKeuanganProvider.GetPaged(CaseManagementProfileKeuanganColumn.FK_CaseManagement_ID.ToString & "=" & Me.PK_CaseManagementID, "", 0, Integer.MaxValue, 0)

                    Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuangan") = Resources
                End Using
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuangan"), TList(Of CaseManagementProfileKeuangan))
            Else
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuangan"), TList(Of CaseManagementProfileKeuangan))
            End If

        End Get
    End Property

    Private _oRowCaseManagement As AMLDAL.CaseManagement.SelectCaseManagementRow

    Private ReadOnly Property oRowCaseManagement() As AMLDAL.CaseManagement.SelectCaseManagementRow
        Get
            Try
                If Not _oRowCaseManagement Is Nothing Then
                    Return _oRowCaseManagement
                Else
                    Using adapter As New AMLDAL.CaseManagementTableAdapters.SelectCaseManagementTableAdapter

                        Using oTable As AMLDAL.CaseManagement.SelectCaseManagementDataTable = adapter.GetVw_CaseManagementByPK(Me.PK_CaseManagementID)
                            If oTable.Rows.Count > 0 Then
                                _oRowCaseManagement = oTable.Rows(0)
                                Return _oRowCaseManagement
                            Else
                                Throw New Exception("There is no data for PKManagementID " & Me.PK_CaseManagementID)
                            End If
                        End Using
                    End Using
                End If
            Catch ex As Exception
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End Get

    End Property

    Private ReadOnly Property oRowCaseManagement(ByVal osqltrans As SqlTransaction) As AMLDAL.CaseManagement.SelectCaseManagementRow
        Get
            Try
                If Not _oRowCaseManagement Is Nothing Then
                    Return _oRowCaseManagement
                Else
                    Using adapter As New AMLDAL.CaseManagementTableAdapters.SelectCaseManagementTableAdapter
                        SetTransaction(adapter, osqltrans)
                        Using oTable As AMLDAL.CaseManagement.SelectCaseManagementDataTable = adapter.GetVw_CaseManagementByPK(Me.PK_CaseManagementID)
                            If oTable.Rows.Count > 0 Then
                                _oRowCaseManagement = oTable.Rows(0)
                                Return _oRowCaseManagement
                            Else
                                Throw New Exception("There is no data for PKManagementID " & Me.PK_CaseManagementID)
                            End If
                        End Using
                    End Using
                End If
            Catch ex As Exception
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End Get

    End Property

    Private _oSettiingWorkFlow As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable

    Private ReadOnly Property oSettingWorkFlow() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable
        Get
            Try
                If _oSettiingWorkFlow Is Nothing Then
                    Using adapter As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter
                        If Not Me.oRowCaseManagement.IsFK_AccountOwnerIDNull Then
                            _oSettiingWorkFlow = adapter.GetSettingWorkFlowCaseManagementByAccountOwnerID(Me.oRowCaseManagement.FK_AccountOwnerID, Me.oRowCaseManagement.VIPCode, Me.oRowCaseManagement.InsiderCode, Me.oRowCaseManagement.Segment, oRowCaseManagement.CaseDescription, oRowCaseManagement.SBU, oRowCaseManagement.SUBSBU, oRowCaseManagement.RM)
                            Return _oSettiingWorkFlow
                        Else
                            Throw New Exception("Account Owner Not Valid")
                            Return Nothing
                        End If

                    End Using
                Else
                    Return _oSettiingWorkFlow
                End If
            Catch ex As Exception
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End Get
    End Property

    Private ReadOnly Property oSettingWorkFlow(ByVal osqltrans As SqlTransaction) As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable
        Get
            Try
                If _oSettiingWorkFlow Is Nothing Then
                    Using adapter As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter
                        If Not Me.oRowCaseManagement(osqltrans).IsFK_AccountOwnerIDNull Then
                            _oSettiingWorkFlow = adapter.GetSettingWorkFlowCaseManagementByAccountOwnerID(Me.oRowCaseManagement.FK_AccountOwnerID, Me.oRowCaseManagement.VIPCode, Me.oRowCaseManagement.InsiderCode, Me.oRowCaseManagement.Segment, oRowCaseManagement.CaseDescription, oRowCaseManagement.SBU, oRowCaseManagement.SUBSBU, oRowCaseManagement.RM)
                            Return _oSettiingWorkFlow
                        Else
                            Throw New Exception("Account Owner Not Valid")
                            Return Nothing
                        End If

                    End Using
                Else
                    Return _oSettiingWorkFlow
                End If
            Catch ex As Exception
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End Get
    End Property

    Public Property ObjTMapCaseManagementWorkflowHistory() As TList(Of MapCaseManagementWorkflowHistory)
        Get
            If Session("CaseManagementViewDetail.ObjTMapCaseManagementWorkflowHistory") Is Nothing Then

                Dim intacocuntowner As Integer = ObjCaseManagement.FK_AccountOwnerID
                Dim strcurrentworkflow As String = ""
                Using objdt As Data.DataTable = CaseManagementBLL.GetCurrentWorkflowStepForUser(intacocuntowner, Sahassa.AML.Commonly.SessionPkUserId)
                    For Each item As Data.DataRow In objdt.Rows
                        strcurrentworkflow += "'" & item("workflowstep") & "',"
                    Next
                    If strcurrentworkflow.Length > 0 Then strcurrentworkflow = strcurrentworkflow.Substring(0, strcurrentworkflow.Length - 1)
                    If strcurrentworkflow.Length > 0 Then
                        Dim straccessworflow As String = ""
                        Using objaccess As Data.DataTable = CaseManagementBLL.GetWorflowCanBeAccessinCaseManagement(strcurrentworkflow)
                            For Each item As Data.DataRow In objaccess.Rows
                                straccessworflow += item("AccessViewOtherWorkflow")
                            Next
                            'todohendra:sampesini 4 oktober 2011
                        End Using
                    Else

                    End If

                End Using

                Session("CaseManagementViewDetail.ObjTMapCaseManagementWorkflowHistory") = CaseManagementBLL.GetTlistMapCaseManagementWorkflowHistory("", "", 0, Integer.MaxValue, 0)
                Return CType(Session("CaseManagementViewDetail.ObjTMapCaseManagementWorkflowHistory"), TList(Of MapCaseManagementWorkflowHistory))
            Else
                Return CType(Session("CaseManagementViewDetail.ObjTMapCaseManagementWorkflowHistory"), TList(Of MapCaseManagementWorkflowHistory))
            End If
        End Get
        Set(ByVal value As TList(Of MapCaseManagementWorkflowHistory))
            Session("CaseManagementViewDetail.ObjTMapCaseManagementWorkflowHistory") = value
        End Set
    End Property

    Private _oRowCasemanagementWorkFlowHistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryDataTable

    Private ReadOnly Property oRowCasemanagementWorkFlowHistory() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryDataTable
        Get
            If Not _oRowCasemanagementWorkFlowHistory Is Nothing Then
                Return _oRowCasemanagementWorkFlowHistory
            Else
                Using adapter As New AMLDAL.CaseManagementTableAdapters.SelectMapCaseManagementWorkflowHistoryTableAdapter
                    _oRowCasemanagementWorkFlowHistory = adapter.SelectMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID)
                    Return _oRowCasemanagementWorkFlowHistory
                End Using
            End If

        End Get
    End Property

    Public ReadOnly Property objCaseManagementProfileKeuanganTransaction() As System.Data.DataTable
        Get
            If Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuanganTransaction") Is Nothing Then

                Using Resources As System.Data.DataTable = AMLBLL.CaseManagementBLL.CaseManagementProfileKeuanganTransactionByPK(Me.PK_CaseManagementID, Me.SetnGetCurrentPagetransKeuanganAmount, 10, Me.SetnGetRowTotaltransKeuanganAmount)

                    Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuanganTransaction") = Resources
                End Using
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuanganTransaction"), System.Data.DataTable)
            Else
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuanganTransaction"), System.Data.DataTable)
            End If

        End Get
    End Property

    Public ReadOnly Property objCaseManagementProfileKeuanganTransactionFreq() As System.Data.DataTable
        Get
            If Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuanganTransactionFreq") Is Nothing Then

                Using Resources As System.Data.DataTable = AMLBLL.CaseManagementBLL.CaseManagementProfileKeuanganTransactionFreqByPK(Me.PK_CaseManagementID, Me.SetnGetCurrentPagetransKeuanganFreq, 10, Me.SetnGetRowTotaltransKeuanganFreq)

                    Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuanganTransactionFreq") = Resources
                End Using
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuanganTransactionFreq"), System.Data.DataTable)
            Else
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuanganTransactionFreq"), System.Data.DataTable)
            End If

        End Get
    End Property

    Private Property SetnGetRowTotaltransKeuanganFreq() As Int32
        Get
            Return CType(IIf(Session("CaseManagementViewDetail.RowTotaltransKeuanganFreq") Is Nothing, 0, Session("CaseManagementViewDetail.RowTotaltransKeuanganFreq")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("CaseManagementViewDetail.RowTotaltransKeuanganFreq") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotaltransKeuanganFreq() As Int32
        Get

            Return CInt(Math.Ceiling(Me.SetnGetRowTotaltransKeuanganFreq / 10))

        End Get
    End Property

    Private Property SetnGetCurrentPagetransKeuanganFreq() As Int32
        Get
            Return CType(IIf(Session("CaseManagementViewDetail.CurrentPagetransKeuanganFreq") Is Nothing, 0, Session("CaseManagementViewDetail.CurrentPagetransKeuanganFreq")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("CaseManagementViewDetail.CurrentPagetransKeuanganFreq") = Value
        End Set
    End Property

    Private Property SetnGetCurrentPagetransKeuanganAmount() As Int32
        Get
            Return CType(IIf(Session("CaseManagementViewDetail.CurrentPagetransKeuanganAmount") Is Nothing, 0, Session("CaseManagementViewDetail.CurrentPagetransKeuanganAmount")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("CaseManagementViewDetail.CurrentPagetransKeuanganAmount") = Value
        End Set
    End Property

    Public ReadOnly Property PK_CaseManagementID() As Long
        Get
            Dim strtemp As String = Request.Params("PK_CaseManagementID")
            Dim intresult As Integer
            If Integer.TryParse(strtemp, intresult) Then
                Return intresult
            Else
                Throw New Exception("Please Request Valid Pk_CaseManagement_ID.")
            End If

        End Get
    End Property

    Public Property ObjCaseManagement() As CaseManagement
        Get
            If Session("CaseManagementViewDetail.ObjCaseManagement") Is Nothing Then
                Session("CaseManagementViewDetail.ObjCaseManagement") = CaseManagementBLL.GetCaseManagementByPk(Me.PK_CaseManagementID)
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagement"), CaseManagement)
            Else
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagement"), CaseManagement)
            End If
        End Get
        Set(ByVal value As CaseManagement)
            Session("CaseManagementViewDetail.ObjCaseManagement") = value
        End Set
    End Property

    Sub ClearSession()
        ObjTMapCaseManagementWorkflowHistoryDraft = Nothing
        Session("CaseManagementViewDetail.ObjDataMapCaseManagementTransactionNew") = Nothing
        Session("CaseManagementViewDetail.ShowCounterParttrans") = "false"
        Session("CaseManagementViewDetail.ShowCounterPart") = "false"
        Session("CaseManagementViewDetail.ShowCounterPartfreq") = "false"
        ObjTMapCaseManagementWorkflowHistory = Nothing
        ObjTMapCaseManagementWorkflowHistoryDraft = Nothing
        Session("CaseManagementViewDetail.ObjSuspiciusPerson") = Nothing
        Session("CaseManagementViewDetail.ObjCaseManagementSuspiciusPerson") = Nothing
        Session("CaseManagementViewDetail.RowTotaltransKeuanganAmount") = Nothing
        Session("CaseManagementViewDetail.CurrentPagetransKeuanganAmount") = Nothing
        Session("CaseManagementViewDetail.RowTotaltransKeuanganFreq") = Nothing
        Session("CaseManagementViewDetail.CurrentPagetransKeuanganFreq") = Nothing
        Session("CaseManagementViewDetail.CaseManagementProfileTopology") = Nothing
        Session("CaseManagementViewDetail.objTListCaseManagementSameCase") = Nothing
        Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuanganTransaction") = Nothing
        Session("CaseManagementViewDetail.ObjCaseManagementProfileIdentias") = Nothing
        Session("CaseManagementViewDetail.objCaseManagementProfileKeuangan") = Nothing
        Session("CaseManagementViewDetailSelected") = Nothing
        Session("CaseManagementViewDetailFieldSearch") = Nothing
        Session("CaseManagementViewDetailValueSearch") = Nothing
        Session("CaseManagementViewDetailSort") = Nothing
        Session("CaseManagementViewDetailCurrentPage") = Nothing
        Session("CaseManagementViewDetailRowTotal") = Nothing
        Session("CaseManagementViewDetailData") = Nothing
        Session("CaseManagementViewDetail.ObjCaseManagement") = Nothing
        Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuanganTransactionFreq") = Nothing
        Session("CaseManagementViewDetail.ObjCaseManagementPenjeleasan") = Nothing
        Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuanganTransaction") = Nothing
        Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuanganTransactionFreq") = Nothing
        SetnGetRowTotaltransKeuanganAmount = Nothing
        SetnGetRowTotaltransKeuanganFreq = Nothing
        SetnGetCurrentPagetransKeuanganFreq = Nothing
        ObjCaseManagement = Nothing
        SetnGetCurrentPagetransKeuanganAmount = Nothing
    End Sub

    Private Sub FillProposedAction()
        Try


            Dim strworkflowstep As String = ObjCaseManagement.WorkflowStep
            If strworkflowstep Is Nothing Then
                strworkflowstep = ""
            End If
            Dim objresult As Data.DataTable = EKABLL.CaseManagementBLL.GetCaseManagementProposedAction(Me.PK_CaseManagementID, strworkflowstep)






            Dim strWhereClause As String = String.Empty

            Dim strLoopproposedaction As String = String.Empty
            For Each item As Data.DataRow In objresult.Rows
                strLoopproposedaction += item(0).ToString + ","
            Next

            If strLoopproposedaction.Length > 0 Then strLoopproposedaction = strLoopproposedaction.Substring(0, strLoopproposedaction.Length - 1)

            If strLoopproposedaction.Length > 0 Then strLoopproposedaction = " and PK_CaseManagementProposedActionID in (" & strLoopproposedaction & ") "


            If ObjCaseManagement IsNot Nothing Then
                strWhereClause = "ExcludeWorkflowStep NOT LIKE '%" & ObjCaseManagement.WorkflowStep & ";%' and PK_CaseManagementProposedActionID<>8"
            End If
            If strLoopproposedaction.Length > 0 Then
                strWhereClause += strLoopproposedaction
            End If

            For Each objProposedAction As CaseManagementProposedAction In DataRepository.CaseManagementProposedActionProvider.GetPaged(strWhereClause, String.Empty, 0, Int32.MaxValue, 0)
                CboProposedAction.Items.Add(New ListItem(objProposedAction.ProposedAction, objProposedAction.PK_CaseManagementProposedActionID))
            Next



            'Dim strWhereClause As String = String.Empty
            'If ObjCaseManagement IsNot Nothing Then
            '    strWhereClause = "ExcludeWorkflowStep NOT LIKE '%" & ObjCaseManagement.WorkflowStep & ";%' and PK_CaseManagementProposedActionID<>8"
            'End If

            'For Each objProposedAction As CaseManagementProposedAction In DataRepository.CaseManagementProposedActionProvider.GetPaged(strWhereClause, String.Empty, 0, Int32.MaxValue, 0)
            '    CboProposedAction.Items.Add(New ListItem(objProposedAction.ProposedAction, objProposedAction.PK_CaseManagementProposedActionID))
            'Next

            'cek kalau workflow sekarang bukan level terendah maka remove yang  PK_CaseManagementProposedActionID=1

            If strLoopproposedaction.Length = 0 Then


                If IsLevelWorkFlowTerendah() Then
                    CboProposedAction.Items.Remove(New System.Web.UI.WebControls.ListItem("Request Change", 2))
                Else

                End If
                If Not IsLevelWorkFlowTeratas() Then
                    CboProposedAction.Items.Remove(New System.Web.UI.WebControls.ListItem("ReAssign", 5))
                Else
                    CboProposedAction.Items.Remove(New System.Web.UI.WebControls.ListItem("Request Assignment", 1))
                End If
                CboProposedAction.Items.Remove(New System.Web.UI.WebControls.ListItem("Underlying Transaction Confirmation", 6))

            End If

        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub

    Private Function IsLevelWorkFlowTeratas() As Boolean
        Dim oRowWorkflowNotComplete() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim strWorkflowstep As String
        Dim ArrSettingWorkflow() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = Nothing
        Dim intCurrentSerial As Integer = 0
        Dim intMaxSerial As Integer = 0
        Try
            oRowWorkflowNotComplete = oRowCasemanagementWorkFlowHistory.Select("fk_eventtypeid IN (" & Eventtype.TaskStarted & "," & Eventtype.CaseMonitoring & ") and bcomplate=0")
            For i As Integer = 0 To oRowWorkflowNotComplete.Length - 1
                strWorkflowstep = oRowWorkflowNotComplete(i).workflowstep
                ArrSettingWorkflow = oSettingWorkFlow.Select("workflowstep='" & strWorkflowstep.Replace("'", "''") & "'")
                For j As Integer = 0 To ArrSettingWorkflow.Length - 1
                    intCurrentSerial = ArrSettingWorkflow(j).SerialNo
                Next
            Next
            ArrSettingWorkflow = oSettingWorkFlow.Select("", "serialno desc")
            If ArrSettingWorkflow.Length > 0 Then
                intMaxSerial = ArrSettingWorkflow(0).SerialNo
            End If
            If intCurrentSerial = intMaxSerial AndAlso intMaxSerial <> 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        Finally
            If Not oRowWorkflowNotComplete Is Nothing Then
                oRowWorkflowNotComplete = Nothing
            End If
            If Not ArrSettingWorkflow Is Nothing Then
                ArrSettingWorkflow = Nothing
            End If
        End Try
    End Function

    Private Function IsLevelWorkFlowTeratas(ByVal osqltrans As SqlTransaction) As Boolean
        Dim oRowWorkflowNotComplete() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim strWorkflowstep As String
        Dim ArrSettingWorkflow() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = Nothing
        Dim intCurrentSerial As Integer = 0
        Dim intMaxSerial As Integer = 0
        Try
            oRowWorkflowNotComplete = oRowCasemanagementWorkFlowHistory.Select("fk_eventtypeid IN (" & Eventtype.TaskStarted & "," & Eventtype.CaseMonitoring & ") and bcomplate=0")
            For i As Integer = 0 To oRowWorkflowNotComplete.Length - 1
                strWorkflowstep = oRowWorkflowNotComplete(i).workflowstep
                ArrSettingWorkflow = oSettingWorkFlow.Select("workflowstep='" & strWorkflowstep.Replace("'", "''") & "'")
                For j As Integer = 0 To ArrSettingWorkflow.Length - 1
                    intCurrentSerial = ArrSettingWorkflow(j).SerialNo
                Next
            Next
            ArrSettingWorkflow = oSettingWorkFlow(osqltrans).Select("", "serialno desc")
            If ArrSettingWorkflow.Length > 0 Then
                intMaxSerial = ArrSettingWorkflow(0).SerialNo
            End If
            If intCurrentSerial = intMaxSerial AndAlso intMaxSerial <> 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        Finally
            If Not oRowWorkflowNotComplete Is Nothing Then
                oRowWorkflowNotComplete = Nothing
            End If
            If Not ArrSettingWorkflow Is Nothing Then
                ArrSettingWorkflow = Nothing
            End If
        End Try
    End Function

    Public ReadOnly Property ObjCaseManagementPenjeleasan() As EkaDataNettier.Entities.CaseManagementPenjelasan

        Get
            If Session("CaseManagementViewDetail.ObjCaseManagementPenjeleasan") Is Nothing Then

                Using Resources As EkaDataNettier.Entities.TList(Of EkaDataNettier.Entities.CaseManagementPenjelasan) = EkaDataNettier.Data.DataRepository.CaseManagementPenjelasanProvider.GetPaged(CaseManagementPenjelasanColumn.FK_CaseManagement_ID.ToString & "=" & Me.PK_CaseManagementID, "", 0, Integer.MaxValue, 0)
                    If Resources.Count > 0 Then
                        Session("CaseManagementViewDetail.ObjCaseManagementPenjeleasan") = Resources(0)
                    Else
                        'buat baru casemanagementpenjelasan karena belum pernah ada.
                        Using objNewdata As New EkaDataNettier.Entities.CaseManagementPenjelasan
                            With objNewdata
                                Dim generator As New Random
                                Dim randomValue As Integer
                                randomValue = generator.Next(1, Integer.MaxValue)

                                .PK_CaseManagementPenjelasan_ID = randomValue
                                .FK_CaseManagement_ID = Me.PK_CaseManagementID
                                .AsalSumberData = ""

                                Dim sql As New StringBuilder
                                sql.Append(" 1. Sebutkan underlying / tujuan transaksi ini dilakukan : " & vbCrLf)
                                sql.Append(" 2. Hubungan nasabah dengan penerima atau pengirim dana : " & vbCrLf)
                                sql.Append(" 3. Jika nasabah adalah pemilik busines (business owner), jelaskan mengenai " & vbCrLf)
                                sql.Append("           a. Jenis komoditas atau servis yang diperdagangkan : " & vbCrLf)
                                sql.Append("           b. Apakah perusahaan tersebut melakukan ekspor atau impor atas barang yang diperdagangkan : " & vbCrLf)
                                sql.Append("           c. Siapa nama nasabah terbesar perusahaan tersebut (jika ada) : " & vbCrLf)
                                sql.Append("           d. Apakah nasabah dan bisnisnya adalah merupakan orang yang terkenal pada daerah dimana nasabah  berusaha : " & vbCrLf)
                                sql.Append(" 4. Berapa lama nasabah ini telah dikenal oleh unit bisnis : " & vbCrLf)
                                sql.Append(" 5. Apakah tempat pekerjaan / usaha nasabah sudah pernah dikunjungi oleh unit bisnis? Jika ya kapan terakhir kunjungan tersebut dilakukan : " & vbCrLf)
                                sql.Append(" 6. Jika nasabah adalah non-independent income seperti ibu rumah tangga atau mahasiswa, darimana sumber dana tersebut berasal? Harap lengkapi form Beneficial Owner terhadap sumber dana tersebut  : " & vbCrLf)
                                sql.Append(" 7. Jika transaksi adalah OB di proses tunai, mohon diberikan alasan mengapa tidak dilakukan secara overbooking : " & vbCrLf)

                                .LatarBelakangTransaksi = sql.ToString

                                .JenisPekerjaan = ""
                                .BidangUsaha = ""
                                .LokasiPekerjaan = ""
                                .Createdby = ""
                                .CreatedDate = Now
                                .LastUpdateBy = ""
                                .LastUpdateDate = Now
                            End With
                            Session("CaseManagementViewDetail.ObjCaseManagementPenjeleasan") = objNewdata
                        End Using
                    End If

                End Using
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagementPenjeleasan"), EkaDataNettier.Entities.CaseManagementPenjelasan)
            Else
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagementPenjeleasan"), EkaDataNettier.Entities.CaseManagementPenjelasan)
            End If

        End Get
    End Property

    Sub LoadDataPenjelasan()
        If Not ObjCaseManagementPenjeleasan Is Nothing Then
            txtSumberDana.Text = ObjCaseManagementPenjeleasan.AsalSumberData
            txtLatarBelakangTransaksi.Text = ObjCaseManagementPenjeleasan.LatarBelakangTransaksi
            txtJenisPekerjaan.Text = ObjCaseManagementPenjeleasan.JenisPekerjaan
            txtBidangPekerjaan.Text = ObjCaseManagementPenjeleasan.BidangUsaha
            txtLokasiPekerjaan.Text = ObjCaseManagementPenjeleasan.LokasiPekerjaan
            txtJabatan.Text = ObjCaseManagementPenjeleasan.Jabatan
            If ObjCaseManagementPenjeleasan.IsJmlOrFregSesuaiKebiasaanNasabah.HasValue Then
                RdlFrequensiSesuaiKebiasaan.SelectedValue = Math.Abs(CInt(ObjCaseManagementPenjeleasan.IsJmlOrFregSesuaiKebiasaanNasabah.GetValueOrDefault(0)))
            End If
            If ObjCaseManagementPenjeleasan.IsTransacactionSesuaiKebiasaanNasabah.HasValue Then
                RdlIsSesuaiKebiasaanNasabah.SelectedValue = Math.Abs(CInt(ObjCaseManagementPenjeleasan.IsTransacactionSesuaiKebiasaanNasabah.GetValueOrDefault(0)))
            End If
            If ObjCaseManagementPenjeleasan.IsTransactionTsbMencurigakan.HasValue Then
                IsTransaksiMencurigakan.SelectedValue = Math.Abs(CInt(ObjCaseManagementPenjeleasan.IsTransactionTsbMencurigakan.GetValueOrDefault(0)))
            End If
            If ObjCaseManagementPenjeleasan.IsTransacactionSesuaiProfileNasabah.HasValue Then
                rdlProfileSesuaiPekerjaan.SelectedValue = Math.Abs(CInt(ObjCaseManagementPenjeleasan.IsTransacactionSesuaiProfileNasabah.GetValueOrDefault(0)))
            End If
            If ObjCaseManagementPenjeleasan.IsAlreadyVerifikasiByPhone.HasValue Then
                RDlVerifikasiTelepon.SelectedValue = Math.Abs(CInt(ObjCaseManagementPenjeleasan.IsAlreadyVerifikasiByPhone.GetValueOrDefault(0)))
            End If

            'RdlFrequensiSesuaiKebiasaan.SelectedValue = Math.Abs(CInt(ObjCaseManagementPenjeleasan.IsJmlOrFregSesuaiKebiasaanNasabah.GetValueOrDefault(0)))
            'RdlIsSesuaiKebiasaanNasabah.SelectedValue = Math.Abs(CInt(ObjCaseManagementPenjeleasan.IsTransacactionSesuaiKebiasaanNasabah.GetValueOrDefault(0)))
            'IsTransaksiMencurigakan.SelectedValue = Math.Abs(CInt(ObjCaseManagementPenjeleasan.IsTransactionTsbMencurigakan.GetValueOrDefault(0)))
            'rdlProfileSesuaiPekerjaan.SelectedValue = Math.Abs(CInt(ObjCaseManagementPenjeleasan.IsTransacactionSesuaiProfileNasabah.GetValueOrDefault(0)))
            'RDlVerifikasiTelepon.SelectedValue = Math.Abs(CInt(ObjCaseManagementPenjeleasan.IsAlreadyVerifikasiByPhone.GetValueOrDefault(0)))
            TxtAlasanOBProcessTunai.Text = ObjCaseManagementPenjeleasan.AlasanObdiProcessTunai
        End If
    End Sub

    Private Function IsLevelWorkFlowTerendah() As Boolean
        Dim oRowWorkflowNotComplete() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim strWorkflowstep As String
        Dim ArrSettingWorkflow() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = Nothing
        Dim strCurrentSerial As Integer = 0
        Try
            oRowWorkflowNotComplete = oRowCasemanagementWorkFlowHistory.Select("fk_eventtypeid IN (" & Eventtype.TaskStarted & "," & Eventtype.CaseMonitoring & ") and bcomplate=0")
            For i As Integer = 0 To oRowWorkflowNotComplete.Length - 1
                strWorkflowstep = oRowWorkflowNotComplete(i).workflowstep
                ArrSettingWorkflow = oSettingWorkFlow.Select("workflowstep='" & strWorkflowstep.Replace("'", "''") & "'")
                For j As Integer = 0 To ArrSettingWorkflow.Length - 1
                    strCurrentSerial = ArrSettingWorkflow(j).SerialNo
                Next
            Next
            If strCurrentSerial = 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        Finally
            If Not oRowWorkflowNotComplete Is Nothing Then
                oRowWorkflowNotComplete = Nothing
            End If
            If Not ArrSettingWorkflow Is Nothing Then
                ArrSettingWorkflow = Nothing
            End If
        End Try
    End Function

    Public ReadOnly Property ObjCaseManagementSuspiciusPerson() As TList(Of CaseManagementSuspiciusPerson)
        Get
            If Session("CaseManagementViewDetail.ObjCaseManagementSuspiciusPerson") Is Nothing Then

                Using Resources As TList(Of CaseManagementSuspiciusPerson) = DataRepository.CaseManagementSuspiciusPersonProvider.GetPaged(CaseManagementSuspiciusPersonColumn.FK_CaseManagement_ID.ToString & "=" & Me.PK_CaseManagementID, "", 0, Integer.MaxValue, 0)

                    Session("CaseManagementViewDetail.ObjCaseManagementSuspiciusPerson") = Resources
                End Using
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagementSuspiciusPerson"), TList(Of CaseManagementSuspiciusPerson))
            Else
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagementSuspiciusPerson"), TList(Of CaseManagementSuspiciusPerson))
            End If

        End Get
    End Property

    Public ReadOnly Property ObjSuspiciusPerson(ByVal intpksuspiciuspersonid As Integer) As SuspiciusPerson
        Get
            If Session("CaseManagementViewDetail.ObjSuspiciusPerson") Is Nothing Then
                Session("CaseManagementViewDetail.ObjSuspiciusPerson") = GetSuspiciusPersonByPk(intpksuspiciuspersonid)
            End If
            Return Session("CaseManagementViewDetail.ObjSuspiciusPerson")
        End Get
    End Property

    Sub LoadDataSuspiciusPerson()
        If ObjCaseManagementSuspiciusPerson.Count > 0 Then

            PanelSuspicius.Visible = True
            If Not ObjSuspiciusPerson(ObjCaseManagementSuspiciusPerson(0).FK_SuspiciusPerson_ID.GetValueOrDefault(0)) Is Nothing Then
                With ObjSuspiciusPerson(ObjCaseManagementSuspiciusPerson(0).FK_SuspiciusPerson_ID.GetValueOrDefault(0))

                    LblCIF.Text = .CIFNo
                    lblName.Text = .Nama
                    lblTglLahir.Text = .TanggalLahir.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy")
                    lblTempatLahir.Text = .TempatLahir
                    lblIdentityNo.Text = .NoIdentitas
                    lblAddress.Text = .Alamat
                    lblKecamatanKelurahan.Text = .KecamatanKeluarahan
                    lblPhoneNumber.Text = .NoTelp
                    lblJob.Text = .Pekerjaan
                    lblworkplace.Text = .AlamatTempatKerja
                    lblnpwp.Text = .NPWP
                    LblDescription.Text = .Description
                    lblcreatedby.Text = .UseridCreator

                End With
            End If

            'Perbaikan case double button 29 Oktober 2014
            If ObjCaseManagement.NewMethod.GetValueOrDefault(0) Then
                PanelQuestionNewTable.Visible = True
                PanelQuestionOldTable.Visible = False
            Else
                PanelQuestionNewTable.Visible = False
                PanelQuestionOldTable.Visible = True
            End If
        End If
    End Sub

    Sub LoadDraftWorklfow()
        If ObjTMapCaseManagementWorkflowHistoryDraft.Count > 0 Then
            TextInvestigationNotes.Text = ObjTMapCaseManagementWorkflowHistoryDraft(0).InvestigationNotes
            Try
                CboProposedAction.SelectedValue = ObjTMapCaseManagementWorkflowHistoryDraft(0).FK_CaseManagementProposedActionID.GetValueOrDefault(0)
            Catch ex As Exception

            End Try

            Try
                CboAccountOwner.SelectedValue = ObjTMapCaseManagementWorkflowHistoryDraft(0).FK_AccountOwnerID
            Catch ex As Exception

            End Try

        End If
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()
            End If
            loadQuestioner()
            If Not IsPostBack Then

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                CboProposedAction.Attributes.Add("onchange", "cekproposedaction('" & CboProposedAction.ClientID & "')")

                FillProposedAction()
                'FillAccountOwner()
                LoadDetailCaseManagement()
                LoadDataSuspiciusPerson()
                LoadProfileIdentitas()
                LoadProfileKeuangan()
                LoadProfileTypology()
                LoadDataPenjelasan()
                LoadWorkFlow()
                LoadDraftWorklfow()
                'loadQuestioner()
                LoadAnswer()
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Private Function IsBolehMendapatReAssign(ByVal strAccontOwnerID As Integer, ByVal strVIPCode As String, ByVal strInsiderCode As String, ByVal strSegment As String, ByVal strdescriptioncasealert As String) As Boolean
        Try
            'todohendra:boleh mendapatkan reassign
            'Using adapter As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter
            '    If adapter.GetSettingWorkFlowCaseManagementByAccountOwnerID(strAccontOwnerID, strVIPCode, strInsiderCode, strSegment, strdescriptioncasealert).Rows.Count = 0 Then
            '        Return False
            '    Else
            '        Return True
            '    End If
            'End Using
        Catch ex As Exception
            Throw

        End Try
    End Function

    Private Sub FillAccountOwner()
        Using adapter As New AMLDAL.AMLDataSetTableAdapters.AccountOwnerTableAdapter
            CboAccountOwner.Items.Clear()

            For Each orows As AMLDAL.AMLDataSet.AccountOwnerRow In adapter.GetData.Select("", "AccountOwnerId  asc")

                'cek kalau reassign yg final baru boleh
                If IsBolehMendapatReAssign(orows.AccountOwnerId, Me.oRowCaseManagement.VIPCode, Me.oRowCaseManagement.InsiderCode, Me.oRowCaseManagement.Segment, Me.oRowCaseManagement.CaseDescription) Then
                    CboAccountOwner.Items.Add(New ListItem(orows.AccountOwnerId & " - " & orows.AccountOwnerName, orows.AccountOwnerId))

                End If
            Next
        End Using
    End Sub

    Public ReadOnly Property ObjCaseManagementProfileIdentias() As TList(Of CaseManagementProfileIdentitas)
        Get
            If Session("CaseManagementViewDetail.ObjCaseManagementProfileIdentias") Is Nothing Then

                Using Resources As TList(Of CaseManagementProfileIdentitas) = DataRepository.CaseManagementProfileIdentitasProvider.GetPaged(CaseManagementProfileIdentitasColumn.FK_CaseManagement_ID.ToString & "=" & Me.PK_CaseManagementID, "", 0, Integer.MaxValue, 0)

                    Session("CaseManagementViewDetail.ObjCaseManagementProfileIdentias") = Resources
                End Using
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagementProfileIdentias"), TList(Of CaseManagementProfileIdentitas))
            Else
                Return CType(Session("CaseManagementViewDetail.ObjCaseManagementProfileIdentias"), TList(Of CaseManagementProfileIdentitas))
            End If

        End Get
    End Property

    Sub LoadWorkFlow()
        'If Not oRowCaseManagementWorkFlow Is Nothing Then
        '    If Not Me.oRowCaseManagementWorkFlow.IsWorkflowInisiatorNull Then
        '        LabelInisiator.Text = Me.oRowCaseManagementWorkFlow.WorkflowInisiator
        '    Else
        '        LabelInisiator.Text = "None"
        '    End If
        '    If Not Me.oRowCaseManagementWorkFlow.IsStartedNull Then
        '        LabelStarted.Text = Me.oRowCaseManagementWorkFlow.Started.ToString("dd MMM yyyy")
        '    Else
        '        LabelStarted.Text = "None"
        '    End If
        '    If Not Me.oRowCaseManagementWorkFlow.IsLastrunNull Then
        '        LabelLastRun.Text = Me.oRowCaseManagementWorkFlow.Lastrun.ToString("dd MMM yyyy")
        '    Else
        '        LabelLastRun.Text = "None"
        '    End If
        'End If
        'Bind Grid
        GridViewWorkflowHistory.DataSource = oRowCasemanagementWorkFlowHistory
        GridViewWorkflowHistory.DataBind()

    End Sub

    Sub LoadProfileKeuangan()
        If Me.ObjCaseManagementProfileKeuangan.Count > 0 Then

            If ObjCaseManagementProfileKeuangan(0).NilaiAkhirResiko.ToLower = "h" Then
                PanelProfileKeuangan.Visible = True
                If ObjCaseManagementProfileKeuangan(0).JenisNasabah.ToLower = "new customer" Then

                    If ObjCaseManagement.NewMethod.GetValueOrDefault(0) Then
                        PanelQuestionNewTable.Visible = True
                        PanelQuestionOldTable.Visible = False
                    Else
                        PanelQuestionNewTable.Visible = False
                        PanelQuestionOldTable.Visible = True
                    End If

                    PanelNewCustomer.Visible = True

                    PanelExistingCustomerOutlier.Visible = False
                    LblJeniscustomerNew.Text = ObjCaseManagementProfileKeuangan(0).JenisNasabah
                    LblPenghasilanNew.Text = ObjCaseManagementProfileKeuangan(0).Penghasilan
                    LblNTNNew.Text = ObjCaseManagementProfileKeuangan(0).NTN
                    LblFinalRiskValueProfileKeuanganNew.Text = AMLBLL.CaseManagementBLL.GetRiskDisplay(ObjCaseManagementProfileKeuangan(0).NilaiAkhirResiko)
                Else

                    'Dim strRange As String = GetRangeModusAmount(ObjCaseManagementProfileKeuangan(0).ModusAmount)
                    If ObjCaseManagement.NewMethod.GetValueOrDefault(0) Then
                        Lblketamount.Text = "Detail Transaction"
                        PanelQuestionNewTable.Visible = True
                        PanelQuestionOldTable.Visible = False
                        PanelExistingCustomerOutlier.Visible = True
                        If ObjCaseManagementProfileKeuangan(0).NilaiResikoOutlier.ToLower = "h" Then
                            Me.SetnGetCurrentPagetransKeuanganAmount = 0
                            PanelTransKeuanganAmount.Visible = True
                            BindTransKeuanganAmount()
                        End If
                        LblNilaiOutlierDebet.Text = ObjCaseManagementProfileKeuangan(0).OutlierDebet.GetValueOrDefault(0).ToString("#,###,###,###.##")
                        LblNilaiOutlierKredit.Text = ObjCaseManagementProfileKeuangan(0).OutlierKredit.GetValueOrDefault(0).ToString("#,###,###,###.##")
                        LblTotalTransDebetPerCIF.Text = ObjCaseManagementProfileKeuangan(0).TotalTransDebet.GetValueOrDefault(0).ToString("#,###,###,###.##")
                        LblTotalTransCreditPerCIF.Text = ObjCaseManagementProfileKeuangan(0).TotalTransKredit.GetValueOrDefault(0).ToString("#,###,###,###.##")
                        LblJeniscustomerExistingOutlier.Text = ObjCaseManagementProfileKeuangan(0).JenisNasabah
                    Else
                        PanelExistingcustomer.Visible = True
                        PanelQuestionNewTable.Visible = False
                        PanelQuestionOldTable.Visible = True
                        PanelExistingCustomerOutlier.Visible = False
                        LblJeniscustomerExisting.Text = ObjCaseManagementProfileKeuangan(0).JenisNasabah
                        LblMOdusAmountDebet.Text = ObjCaseManagementProfileKeuangan(0).ModusAmountDebetDescription
                        LblMOdusAmountKredit.Text = ObjCaseManagementProfileKeuangan(0).ModusAmountKreditDescription
                        LblMOdusFreqPerPeriodDebet.Text = ObjCaseManagementProfileKeuangan(0).ModusFreqDayPeriodDebetDescription
                        LblMOdusFreqPerPeriodKredit.Text = ObjCaseManagementProfileKeuangan(0).ModusFreqDayPeriodKreditDescription
                        LblRiskValueModusAmountDebet.Text = AMLBLL.CaseManagementBLL.GetRiskDisplay(ObjCaseManagementProfileKeuangan(0).NilaiResikoModusAmountDebet)
                        LblRiskValueModusAmountKredit.Text = AMLBLL.CaseManagementBLL.GetRiskDisplay(ObjCaseManagementProfileKeuangan(0).NilaiResikoModusAmountKredit)
                        LblRiskValueModusFreqPerPeriodDebet.Text = AMLBLL.CaseManagementBLL.GetRiskDisplay(ObjCaseManagementProfileKeuangan(0).NilaiResikoModusFreqDayPeriodDebet)
                        LblRiskValueModusFreqPerPeriodKredit.Text = AMLBLL.CaseManagementBLL.GetRiskDisplay(ObjCaseManagementProfileKeuangan(0).NilaiResikoModusFreqDayPeriodKredit)
                        LblFinalRiskValueProfileKeuangan1.Text = AMLBLL.CaseManagementBLL.GetRiskDisplay(ObjCaseManagementProfileKeuangan(0).NilaiAkhirResiko)
                        If ObjCaseManagementProfileKeuangan(0).NilaiResikoModusAmount.ToLower = "h" Then
                            Me.SetnGetCurrentPagetransKeuanganAmount = 0
                            PanelTransKeuanganAmount.Visible = True
                            BindTransKeuanganAmount()
                        End If
                        If ObjCaseManagementProfileKeuangan(0).NilaiResikoModusFreqDayPeriod.ToLower = "h" Then
                            Me.SetnGetCurrentPagetransKeuanganFreq = 0
                            PanelTransKeuanganFreqPeriod.Visible = True
                            BindTransKeuanganFreq()
                        End If
                    End If

                End If
            Else
                PanelProfileKeuangan.Visible = False
            End If

            If ObjCaseManagementProfileKeuangan(0).NilaiAkhirResiko.ToLower = "h" Then
                'PanelFinancial.Visible = True
                LblFinalRiskValueProfileKeuanganNew.ForeColor = Drawing.Color.Red
                LblFinalRiskValueProfileKeuangan1.ForeColor = Drawing.Color.Red

                If objCaseManagementProfileKeuanganTransaction.Rows.Count > 0 Then
                    Me.SetnGetCurrentPagetransKeuanganAmount = 0
                    PanelTransKeuanganAmount.Visible = True
                    BindTransKeuanganAmount()
                End If

                If objCaseManagementProfileKeuanganTransactionFreq.Rows.Count > 0 Then
                    Me.SetnGetCurrentPagetransKeuanganFreq = 0
                    PanelTransKeuanganFreqPeriod.Visible = True
                    BindTransKeuanganFreq()
                End If

            End If

        End If
    End Sub

    Sub LoadProfileTypology()
        If ObjCaseManagementProfileTopology.Count > 0 Then

            If ObjCaseManagementProfileTopology(0).ResikoTopology.ToLower = "h" Then
                PanelTypologi.Visible = True
                If ObjCaseManagement.NewMethod.GetValueOrDefault(0) Then
                    PanelQuestionNewTable.Visible = True
                    PanelQuestionOldTable.Visible = False
                Else

                    If ObjCaseManagementProfileTopology(0).CaseDescription.ToLower = "penipuan" Then
                        PanelPenipuan.Visible = True
                    ElseIf ObjCaseManagementProfileTopology(0).CaseDescription.ToLower = "OB Process Tunai".ToLower Then
                        PanelOBProcessTunai.Visible = True
                    ElseIf ObjCaseManagementProfileTopology(0).CaseDescription.ToLower = "Pass - Through Transaction".ToLower Then
                        PanelPassThrough.Visible = True
                    End If

                    PanelQuestionNewTable.Visible = False
                    PanelQuestionOldTable.Visible = True
                End If

                lblTopology.Text = ObjCaseManagementProfileTopology(0).CaseDescription
                lblciftipology.Text = ObjCaseManagement.CIFNo
                AccountNotypology.Text = ObjCaseManagement.AccountNo
                lblDescTopology.Text = CaseManagementBLL.GetRulesDescriptionByCaseDescription(ObjCaseManagement.CaseDescription)

                If ObjCaseManagementProfileTopology(0).ResikoTopology.ToLower = "h" Then

                    'Berhubungan dengan penjelasan

                    lblFinalTopologyValue.Text = "High"
                    lblFinalTopologyValue.ForeColor = Drawing.Color.Red

                    'Label16.ForeColor = Drawing.Color.Red
                Else
                    lblFinalTopologyValue.Text = "Low"
                End If

                GridViewRelatedTransaction.DataSource = Me.oRowCaseManagementTransaction
                GridViewRelatedTransaction.DataBind()

                'If divTransKeuanganAmount Then
                If oRowCaseManagementTransaction.Rows.Count = 0 Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, " document.getElementById('" & divRelatedTransaction.ClientID & "').style.height=""1px""; ", True)
                ElseIf oRowCaseManagementTransaction.Rows.Count < 5 Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, " document.getElementById('" & divRelatedTransaction.ClientID & "').style.height=""170px""; ", True)
                ElseIf oRowCaseManagementTransaction.Rows.Count >= 5 And oRowCaseManagementTransaction.Rows.Count < 10 Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, " document.getElementById('" & divRelatedTransaction.ClientID & "').style.height=""220px""; ", True)
                Else

                    Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, " document.getElementById('" & divRelatedTransaction.ClientID & "').style.height=""270px""; ", True)
                End If

                If oRowCaseManagementTransaction.Rows.Count = 0 Then
                    'kalau di table mapcasemanagementtransaction kosong artinya ada di new

                    GridViewRelatedTransaction.Visible = False
                    bindGridRelatedTranactionNew()

                    If Not ObjDataMapCaseManagementTransactionNew Is Nothing Then
                        If ObjDataMapCaseManagementTransactionNew.Rows.Count = 0 Then
                            Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, " document.getElementById('" & divRelatedTransaction.ClientID & "').style.height=""1px""; ", True)
                        ElseIf ObjDataMapCaseManagementTransactionNew.Rows.Count < 5 Then
                            Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, " document.getElementById('" & divRelatedTransaction.ClientID & "').style.height=""170px""; ", True)
                        ElseIf ObjDataMapCaseManagementTransactionNew.Rows.Count >= 5 And ObjDataMapCaseManagementTransactionNew.Rows.Count < 10 Then
                            Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, " document.getElementById('" & divRelatedTransaction.ClientID & "').style.height=""220px""; ", True)
                        Else
                            Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, " document.getElementById('" & divRelatedTransaction.ClientID & "').style.height=""270px""; ", True)
                        End If
                    End If
                End If

            Else
                PanelTypologi.Visible = False
            End If

        End If
    End Sub

    Sub bindGridRelatedTranactionNew()
        GridViewRelatedTransactionNew.DataSource = ObjDataMapCaseManagementTransactionNew
        GridViewRelatedTransactionNew.DataBind()
    End Sub

    Private Sub GridViewRelatedTransactionNew_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridViewRelatedTransactionNew.PageIndexChanging
        Try
            GridViewRelatedTransactionNew.PageIndex = e.NewPageIndex
            bindGridRelatedTranactionNew()
        Catch ex As Exception

        End Try
    End Sub

    Sub LoadProfileIdentitas()

        If Me.ObjCaseManagementProfileIdentias.Count > 0 Then

            If ObjCaseManagementProfileIdentias(0).NilaiResikoIdentitas.ToLower = "h" Then
                PanelIdentitas.Visible = True

                If ObjCaseManagementProfileIdentias(0).NilaiResikoIdentitas.ToLower = "h" Then
                    LblResikoIdentitas.Text = "High"
                    LblResikoIdentitas.ForeColor = Drawing.Color.Red
                    'LblKetProfileIdentitas.ForeColor = Drawing.Color.Red
                ElseIf ObjCaseManagementProfileIdentias(0).NilaiResikoIdentitas.ToLower = "m" Then
                    LblResikoIdentitas.Text = "Medium"
                ElseIf ObjCaseManagementProfileIdentias(0).NilaiResikoIdentitas.ToLower = "l" Then
                    LblResikoIdentitas.Text = "Low"
                End If
                If ObjCaseManagementProfileIdentias(0).Fk_Verification_List_Type_Id.HasValue Then
                    LblListType.Text = AMLBLL.CaseManagementBLL.GetVerificationListTypeByPK(ObjCaseManagementProfileIdentias(0).Fk_Verification_List_Type_Id.GetValueOrDefault(0))
                End If
            Else
                PanelIdentitas.Visible = False
            End If

        End If
    End Sub

    Sub LoadDetailCaseManagement()
        If Not Me.ObjCaseManagement Is Nothing Then
            With ObjCaseManagement

                LblCaseNumber.Text = .PK_CaseManagementID
                LblDescription.Text = .CaseDescription
                Using objcasestatus As CaseStatus = DataRepository.CaseStatusProvider.GetByCaseStatusId(.FK_CaseStatusID)
                    If Not objcasestatus Is Nothing Then
                        LblCaseStatus.Text = objcasestatus.CaseStatusDescription
                    End If
                End Using

                LblCreatedDate.Text = .CreatedDate.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy")
                LblLastUpdatedDate.Text = .LastUpdated.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy")
                LblProposedBy.Text = .ProposedBy

                Using objaccountowner As AccountOwner = DataRepository.AccountOwnerProvider.GetByAccountOwnerId(.FK_AccountOwnerID)
                    If Not objaccountowner Is Nothing Then
                        LblAcocuntOwnerName.Text = objaccountowner.AccountOwnerId & "- " & objaccountowner.AccountOwnerName
                    End If
                End Using

                LblPIC.Text = .PIC
                LblHighRiskCustomer.Text = CaseManagementBLL.IsHighRiskCustomer(Me.PK_CaseManagementID)

                Using objCaseManagementProposedAction As CaseManagementProposedAction = DataRepository.CaseManagementProposedActionProvider.GetByPK_CaseManagementProposedActionID(.FK_LastProposedStatusID.GetValueOrDefault(0))
                    If Not objCaseManagementProposedAction Is Nothing Then
                        LblLastProposedAction.Text = objCaseManagementProposedAction.ProposedAction
                    End If
                End Using

                GridViewSameCase.DataSource = objTListCaseManagementSameCase
                GridViewSameCase.DataBind()
            End With
            BindPenghasilan()
            BindEmployeeHistory()

            Using objVW_MappingGroupMenuSTRPPATK As VList(Of vw_MappingGroupMenuSTRPPATK) = AMLBLL.CaseManagementBLL.GetVW_MappingGroupMenuSTRPPATK(Sahassa.AML.Commonly.SessionGroupName)
                If objVW_MappingGroupMenuSTRPPATK.Count > 0 Then

                    BindCTRReportedToPPATK()
                    BindSTReportedToPPATK()
                Else
                    CTRReportedToPPATKROW.Visible = False
                    STRReportedToPPATKROW.Visible = False
                End If
            End Using

        End If
    End Sub

    Protected Sub STRReportedToPPATK_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles STRReportedToPPATK.PageIndexChanging
        STRReportedToPPATK.PageIndex = e.NewPageIndex
        BindSTReportedToPPATK()
    End Sub

    Protected Sub BindCTRReportedToPPATK()
        Dim objVw_CTRReportToPPATK_CASEDETAIL As EkaDataNettier.Entities.VList(Of EkaDataNettier.Entities.Vw_CTRReportToPPATK_CASEDETAIL) = CaseManagementBLL.GetVw_CTRReportedToPPATK_CASEDETAIL("CIFNo='" & ObjCaseManagement.CIFNo.ToString & "' and CreatedDate Between '" & DateAdd(DateInterval.Year, -1, CDate(ObjCaseManagement.CreatedDate)).ToString & "' And '" & ObjCaseManagement.CreatedDate.ToString & "'", "", 0, Integer.MaxValue, 0)

        If objVw_CTRReportToPPATK_CASEDETAIL.Count > 0 Then
            Dim Dt_CTRReportToPPATK_CASEDETAIL As New System.Data.DataTable
            Dt_CTRReportToPPATK_CASEDETAIL.Columns.Add(New Data.DataColumn("Number", GetType(Long)))
            Dt_CTRReportToPPATK_CASEDETAIL.Columns.Add(New Data.DataColumn("Total", GetType(Long)))
            Dt_CTRReportToPPATK_CASEDETAIL.Columns.Add(New Data.DataColumn("Average", GetType(Long)))

            Dim objrow As Data.DataRow = Dt_CTRReportToPPATK_CASEDETAIL.NewRow
            objrow("Number") = 0
            objrow("Total") = 0
            For Each objVw_CTRReportToPPATK_CASEDETAILSingle As EkaDataNettier.Entities.Vw_CTRReportToPPATK_CASEDETAIL In objVw_CTRReportToPPATK_CASEDETAIL
                objrow("Number") = CLng(objrow("Number")) + CLng(objVw_CTRReportToPPATK_CASEDETAILSingle.Number)
                objrow("Total") = CLng(objrow("Total")) + CLng(objVw_CTRReportToPPATK_CASEDETAILSingle.Total)
            Next
            objrow("Average") = objrow("Total") / objrow("Number")
            Dt_CTRReportToPPATK_CASEDETAIL.Rows.Add(objrow)

            CTRReportedToPPATK.DataSource = Dt_CTRReportToPPATK_CASEDETAIL
            CTRReportedToPPATK.DataBind()

        End If
    End Sub

    Protected Sub BindSTReportedToPPATK()
        Dim totalPage As Integer = 0
        Using objSTRReportToPPATK As EkaDataNettier.Entities.TList(Of EkaDataNettier.Entities.CaseManagement) = CaseManagementBLL.GetTlistCaseManagement("CIFNo='" & ObjCaseManagement.CIFNo & "' and IsReportedtoRegulator=1 and PPATKConfirmationno is not NULL", "", 0, Integer.MaxValue, totalPage)
            If objSTRReportToPPATK.Count > 0 Then
                STRReportedToPPATK.AllowPaging = True
                STRReportedToPPATK.PageSize = Sahassa.AML.Commonly.SessionPagingLimit
                STRReportedToPPATK.DataSource = objSTRReportToPPATK
                STRReportedToPPATK.DataBind()

            End If
        End Using
        'Using objSTRReportToPPATK As TList(Of CaseManagement) = CaseManagementBLL.GetTlistCaseManagement("CIFNo='" & ObjCaseManagement.CIFNo & "' ", "", 0, Integer.MaxValue, totalPage)
        '    If objSTRReportToPPATK.Count > 0 Then
        '        STRReportedToPPATK.AllowPaging = True
        '        STRReportedToPPATK.PageSize = Sahassa.AML.Commonly.SessionPagingLimit
        '        STRReportedToPPATK.DataSource = objSTRReportToPPATK
        '        STRReportedToPPATK.DataBind()

        '    End If
        'End Using
    End Sub

    Sub BindPenghasilan()
        GrdPenghasilan.DataSource = CaseManagementBLL.GetTablePenghasilanByCaseManagementID(Me.PK_CaseManagementID)
        GrdPenghasilan.DataBind()
    End Sub

    Sub BindEmployeeHistory()
        GrdEmployeeHistory.DataSource = CaseManagementBLL.GetTableEmployeeHistoryByCaseManagementID(Me.PK_CaseManagementID)
        GrdEmployeeHistory.DataBind()
    End Sub

    Protected Sub GrdPenghasilan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GrdPenghasilan.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim objLabel As Label = CType(e.Row.FindControl("LabelNo"), Label)
                objLabel.Text = e.Row.RowIndex + 1

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GrdEmployeeHistory_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GrdEmployeeHistory.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim objLabel As Label = CType(e.Row.FindControl("LabelNo"), Label)
                objLabel.Text = e.Row.RowIndex + 1

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Function GenerateCIFLink(ByVal strCIF As String) As String
        If Convert.IsDBNull(strCIF) Then
            Return "CIF Data Not Valid"
        Else
            Return String.Format("<a href=""{0}"">{1}</a> ", ResolveUrl("~\CustomerInformationDetail.aspx?CIFNO=" & strCIF), strCIF)
        End If
    End Function

    Protected Function GenerateAccountLink(ByVal strAccountNo As String) As String
        If Convert.IsDBNull(strAccountNo) Then
            Return "Account Data Not Valid"
        Else
            Return String.Format("<a href=""{0}"">{1}</a> ", ResolveUrl("~\AccountInformationDetail.aspx?AccountNo=" & strAccountNo), strAccountNo)
        End If
    End Function

    Protected Function GenerateLink(ByVal PKHistoryWorkflowid As Integer, ByVal Filename As String) As String
        Return String.Format("<a href=""{0}"">{1}</a> ", ResolveUrl("~\CaseManagementDownloadAttachment.aspx?PKWorkflowhistoryAttachmentID=" & PKHistoryWorkflowid), Filename)
    End Function

    Public Sub PageNavigatetransKeuanganFreq(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuanganTransactionFreq") = Nothing
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPagetransKeuanganFreq = 0
                Case "Prev" : Me.SetnGetCurrentPagetransKeuanganFreq -= 1
                Case "Next" : Me.SetnGetCurrentPagetransKeuanganFreq += 1
                Case "Last" : Me.SetnGetCurrentPagetransKeuanganFreq = Me.GetPageTotaltransKeuanganFreq - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            BindTransKeuanganFreq()
        Catch ex As Exception
            CustomValidator1.IsValid = False
            CustomValidator1.ErrorMessage = ex.Message
        End Try
    End Sub

    Public Sub PageNavigatetransKeuanganAmount(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Session("CaseManagementViewDetail.ObjCaseManagementProfileKeuanganTransaction") = Nothing
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPagetransKeuanganAmount = 0
                Case "Prev" : Me.SetnGetCurrentPagetransKeuanganAmount -= 1
                Case "Next" : Me.SetnGetCurrentPagetransKeuanganAmount += 1
                Case "Last" : Me.SetnGetCurrentPagetransKeuanganAmount = Me.GetPageTotaltransKeuanganAmount - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            BindTransKeuanganAmount()
        Catch ex As Exception
            CustomValidator1.IsValid = False
            CustomValidator1.ErrorMessage = ex.Message
        End Try
    End Sub

    Private ReadOnly Property GetPageTotaltransKeuanganAmount() As Int32
        Get

            Return CInt(Math.Ceiling(Me.SetnGetRowTotaltransKeuanganAmount / 10))

        End Get
    End Property

    Private Property SetnGetRowTotaltransKeuanganAmount() As Int32
        Get
            Return CType(IIf(Session("CaseManagementViewDetail.RowTotaltransKeuanganAmount") Is Nothing, 0, Session("CaseManagementViewDetail.RowTotaltransKeuanganAmount")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("CaseManagementViewDetail.RowTotaltransKeuanganAmount") = Value
        End Set
    End Property

    Sub BindTransKeuanganFreq()
        GridViewTransKeuanganFreq.DataSource = objCaseManagementProfileKeuanganTransactionFreq
        GridViewTransKeuanganFreq.DataBind()

        If objCaseManagementProfileKeuanganTransactionFreq.Rows.Count = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, " document.getElementById('" & divTransKeuanganFreqPeriod.ClientID & "').style.height=""1px""; ", True)
        ElseIf objCaseManagementProfileKeuanganTransactionFreq.Rows.Count < 5 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, " document.getElementById('" & divTransKeuanganFreqPeriod.ClientID & "').style.height=""170px""; ", True)
        ElseIf objCaseManagementProfileKeuanganTransactionFreq.Rows.Count >= 5 And objCaseManagementProfileKeuanganTransactionFreq.Rows.Count < 10 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, " document.getElementById('" & divTransKeuanganFreqPeriod.ClientID & "').style.height=""220px""; ", True)

        End If

        SetInfoNavigatetransKeuanganFreq()
    End Sub

    Private Sub SetInfoNavigatetransKeuanganFreq()
        Try
            Me.PageCurrentPagetransKeuanganFreq.Text = CStr(Me.SetnGetCurrentPagetransKeuanganFreq + 1)
            Me.PageTotalPagestransKeuanganFreq.Text = CStr(Me.GetPageTotaltransKeuanganFreq)
            Me.PageTotalRowstransKeuanganFreq.Text = CStr(Me.SetnGetRowTotaltransKeuanganFreq)
            Me.LinkButtonNexttransKeuanganFreq.Enabled = (Not Me.SetnGetCurrentPagetransKeuanganFreq + 1 = Me.GetPageTotaltransKeuanganFreq) AndAlso Me.GetPageTotaltransKeuanganFreq <> 0
            Me.LinkButtonLasttransKeuanganFreq.Enabled = (Not Me.SetnGetCurrentPagetransKeuanganFreq + 1 = Me.GetPageTotaltransKeuanganFreq) AndAlso Me.GetPageTotaltransKeuanganFreq <> 0
            Me.LinkButtonFirsttransKeuanganFreq.Enabled = Not Me.SetnGetCurrentPagetransKeuanganFreq = 0
            Me.LinkButtonPrevioustransKeuanganFreq.Enabled = Not Me.SetnGetCurrentPagetransKeuanganFreq = 0
        Catch ex As Exception
            CustomValidator1.IsValid = False
            CustomValidator1.ErrorMessage = ex.Message
        End Try
    End Sub

    Sub BindTransKeuanganAmount()
        GridViewTransKeuangan.DataSource = objCaseManagementProfileKeuanganTransaction
        GridViewTransKeuangan.DataBind()
        If objCaseManagementProfileKeuanganTransaction.Rows.Count = 0 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, " document.getElementById('" & divTransKeuanganAmount.ClientID & "').style.height=""1px""; ", True)

        ElseIf objCaseManagementProfileKeuanganTransaction.Rows.Count <= 5 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, " document.getElementById('" & divTransKeuanganAmount.ClientID & "').style.height=""170px""; ", True)
        ElseIf objCaseManagementProfileKeuanganTransaction.Rows.Count > 5 And objCaseManagementProfileKeuanganTransaction.Rows.Count < 10 Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, " document.getElementById('" & divTransKeuanganAmount.ClientID & "').style.height=""220px""; ", True)

        End If

        SetInfoNavigatetransKeuanganAmount()
    End Sub

    Private Sub SetInfoNavigatetransKeuanganAmount()
        Try
            Me.PageCurrentPagetransKeuanganAmount.Text = CStr(Me.SetnGetCurrentPagetransKeuanganAmount + 1)
            Me.PageTotalPagestransKeuanganAmount.Text = CStr(Me.GetPageTotaltransKeuanganAmount)
            Me.PageTotalRowstransKeuanganAmount.Text = CStr(Me.SetnGetRowTotaltransKeuanganAmount)
            Me.LinkButtonNexttransKeuanganAmount.Enabled = (Not Me.SetnGetCurrentPagetransKeuanganAmount + 1 = Me.GetPageTotaltransKeuanganAmount) AndAlso Me.GetPageTotaltransKeuanganAmount <> 0
            Me.LinkButtonLasttransKeuanganAmount.Enabled = (Not Me.SetnGetCurrentPagetransKeuanganAmount + 1 = Me.GetPageTotaltransKeuanganAmount) AndAlso Me.GetPageTotaltransKeuanganAmount <> 0
            Me.LinkButtonFirsttransKeuanganAmount.Enabled = Not Me.SetnGetCurrentPagetransKeuanganAmount = 0
            Me.LinkButtonPrevioustransKeuanganAmount.Enabled = Not Me.SetnGetCurrentPagetransKeuanganAmount = 0
        Catch ex As Exception
            CustomValidator1.IsValid = False
            CustomValidator1.ErrorMessage = ex.Message
        End Try
    End Sub

    Public Enum Eventtype
        WorkFlowStarted = 1
        TaskStarted
        TaskComplated
        TaskCanceledBySystem
        Reassign
        WorkFlowFinish
        TaskDraft
        CaseMonitoring
    End Enum

    Public Enum ProposedAction
        RequestAssignment = 1
        RequestChange
        Dropped
        ConfirmAsSTR
        ReAssign
        UnderlyingTransactionConfirmation
        CaseMonitoring
    End Enum

    Public Enum CaseManagementStatus
        bNew = 1
        UnderInvestigate
        Dropped
        Approved
    End Enum

    Protected Sub GridViewSameCase_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewSameCase.RowDataBound

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim objtemp As CaseManagementSameCaseHistory = e.Row.DataItem
                Dim objLblBranch As Label = CType(e.Row.FindControl("LblBranch"), Label)
                Dim objLabelStatus As Label = CType(e.Row.FindControl("LabelStatus"), Label)
                objLblBranch.Text = AMLBLL.CaseManagementBLL.GetAccountOwnerName(objtemp.FK_AccountOwnerID)
                objLabelStatus.Text = AMLBLL.CaseManagementBLL.GetStatusCase(objtemp.FK_CaseStatusID.GetValueOrDefault(0))

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridViewWorkflowHistory_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewWorkflowHistory.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim PK As Integer = GridViewWorkflowHistory.DataKeys(e.Row.RowIndex).Value
            Using adapter As New AMLDAL.CaseManagementTableAdapters.GetMapCaseManagementWorkflowAttachmentByUserIDTableAdapter
                CType(e.Row.FindControl("Datalist1"), System.Web.UI.WebControls.DataList).DataSource = adapter.GetMapCaseManagementWorkflowAttachmentByFK(PK)
                CType(e.Row.FindControl("Datalist1"), System.Web.UI.WebControls.DataList).DataBind()
            End Using

            Dim strUserIdAll As String() = e.Row.Cells(4).Text.Split(";")
            Dim strUserIdBreak As String = String.Empty
            For i As Int32 = 0 To strUserIdAll.Length - 1
                strUserIdBreak &= strUserIdAll(i) & ";"
                If i Mod 7 = 6 Then
                    strUserIdBreak &= "<br>"
                End If
            Next
            e.Row.Cells(4).Text = strUserIdBreak.Remove(strUserIdBreak.Length - 1)
        End If
    End Sub

    Private Sub BindListAttachment()
        Try
            Using adapter As New AMLDAL.CaseManagementTableAdapters.GetMapCaseManagementWorkflowAttachmentByUserIDTableAdapter

                Me.ListAttachment.DataSource = adapter.GetData(Sahassa.AML.Commonly.SessionPkUserId)
                Me.ListAttachment.DataTextField = "filename"
                Me.ListAttachment.DataValueField = "PK_MapCaseManagementWorkflowHistoryAttachmentID"
                Me.ListAttachment.DataBind()
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub

    Protected Sub ButtonAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        Dim WorkFlowHistoryID As System.Nullable(Of Long)
        Try

            If FileUpload1.HasFile Then
                WorkFlowHistoryID = Nothing
                Using adapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementWorkflowHistoryAttachmentTableAdapter
                    adapter.Insert(WorkFlowHistoryID, FileUpload1.FileName, FileUpload1.FileBytes, Sahassa.AML.Commonly.SessionPkUserId, FileUpload1.PostedFile.ContentType)
                End Using
                BindListAttachment()
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ButtonDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonDelete.Click
        Try
            If ListAttachment.SelectedIndex <> -1 Then
                Using adapter As New AMLDAL.CaseManagementTableAdapters.GetMapCaseManagementWorkflowAttachmentByUserIDTableAdapter
                    adapter.DeleteMapCaseManagementWorkflowAttachmentByPK(ListAttachment.SelectedValue)
                End Using
                BindListAttachment()
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Function IsHaveRightToInputCaseManagementPenjelasan() As Boolean
        Dim oRowWorkflowNotComplete() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim ArrUserid() As String = Nothing
        Try
            'cari yang workflowstep=task started dan belum complete
            'fk_eventtypeid=2 and bcomplete=0
            oRowWorkflowNotComplete = oRowCasemanagementWorkFlowHistory.Select("FK_EventTypeID IN (" & Eventtype.TaskStarted & "," & Eventtype.CaseMonitoring & ") AND bComplate=0")
            For i As Integer = 0 To oRowWorkflowNotComplete.Length - 1
                ArrUserid = oRowWorkflowNotComplete(i).UserID.Split(";")
                For j As Integer = 0 To ArrUserid.Length - 1
                    If ArrUserid(j).ToLower = Sahassa.AML.Commonly.SessionUserId.ToLower Then
                        'cek jika workflowstep itu serial yang pertama ngak boleh input investigation notes. tapi input casemanagementpenjelasan
                        If AMLBLL.CaseManagementBLL.GetSerialNo(oRowWorkflowNotComplete(i).workflowstep, ObjCaseManagement.FK_AccountOwnerID) = 1 Then
                            Return True
                        Else
                            Return False
                        End If

                    End If
                Next
            Next
            Return False
        Catch ex As Exception
            LogError(ex)
            Throw
        Finally
            If Not oRowWorkflowNotComplete Is Nothing Then
                oRowWorkflowNotComplete = Nothing
            End If
            If Not ArrUserid Is Nothing Then
                ArrUserid = Nothing
            End If
        End Try

    End Function

    Private Function IsHaveRighttoInputInvestigationNotoesResponse()
        Dim oRowWorkflowNotComplete() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim ArrUserid() As String = Nothing
        Try
            'cari yang workflowstep=task started dan belum complete
            'fk_eventtypeid=2 and bcomplete=0
            oRowWorkflowNotComplete = oRowCasemanagementWorkFlowHistory.Select("fk_eventtypeid IN (" & Eventtype.TaskStarted & ", " & Eventtype.CaseMonitoring & ") and bcomplate=0")
            For i As Integer = 0 To oRowWorkflowNotComplete.Length - 1
                ArrUserid = oRowWorkflowNotComplete(i).UserID.Split(";")
                For j As Integer = 0 To ArrUserid.Length - 1
                    If ArrUserid(j).ToLower = Sahassa.AML.Commonly.SessionUserId.ToLower Then
                        'cek jika workflowstep itu serial yang pertama ngak boleh input investigation notes. tapi input casemanagementpenjelasan
                        If AMLBLL.CaseManagementBLL.GetSerialNo(oRowWorkflowNotComplete(i).workflowstep, ObjCaseManagement.FK_AccountOwnerID) = 1 Then
                            Return False
                        Else
                            Return True
                        End If

                    End If
                Next
            Next
            Return False
        Catch ex As Exception
            LogError(ex)
            Throw
        Finally
            If Not oRowWorkflowNotComplete Is Nothing Then
                oRowWorkflowNotComplete = Nothing
            End If
            If Not ArrUserid Is Nothing Then
                ArrUserid = Nothing
            End If
        End Try

    End Function

    Sub EnableInputPenjelasan(ByVal bEnabled As Boolean)
        txtSumberDana.Enabled = bEnabled
        txtLatarBelakangTransaksi.ReadOnly = Not bEnabled
        txtJenisPekerjaan.Enabled = bEnabled
        txtBidangPekerjaan.Enabled = bEnabled
        txtLokasiPekerjaan.Enabled = bEnabled
        txtJabatan.Enabled = bEnabled
        btnSaveAsDraftPenjelasanOldMethod.Visible = bEnabled
        btnProposedPenjelasanOldMethod.Visible = bEnabled
        RdlIsSesuaiKebiasaanNasabah.Enabled = bEnabled
        IsTransaksiMencurigakan.Enabled = bEnabled
        RdlFrequensiSesuaiKebiasaan.Enabled = bEnabled
        rdlProfileSesuaiPekerjaan.Enabled = bEnabled
        RDlVerifikasiTelepon.Enabled = bEnabled
        TxtAlasanOBProcessTunai.Enabled = bEnabled
        ImagePrintSTR.Visible = Not bEnabled
    End Sub

    Sub EnableInputAnswer(ByVal bEnabled As Boolean)
        For Each QuestionnaireTableRow As TableRow In QuestionnaireTable.Rows
            If QuestionnaireTableRow.Cells(0).Controls.Count > 0 Then
                If TypeOf (QuestionnaireTableRow.Cells(0).Controls(0)) Is TextBox Then
                    CType(QuestionnaireTableRow.Cells(0).Controls(0), TextBox).ReadOnly = Not bEnabled
                ElseIf TypeOf (QuestionnaireTableRow.Cells(0).Controls(0)) Is RadioButtonList Then
                    CType(QuestionnaireTableRow.Cells(0).Controls(0), RadioButtonList).Enabled = bEnabled
                End If
            End If
        Next
        ImagePrintSTR.Visible = Not bEnabled
        btnSaveAsDraftPenjelasan.Visible = bEnabled
        btnProposedPenjelasan.Visible = bEnabled
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

            If IsHaveRightToInputCaseManagementPenjelasan() Then
                EnableInputPenjelasan(True)
                EnableInputAnswer(True)
            Else
                EnableInputPenjelasan(False)
                EnableInputAnswer(False)
            End If
            If Me.IsHaveRighttoInputInvestigationNotoesResponse Then
                Me.Panel1.Visible = True
                BindListAttachment()
            Else
                Me.Panel1.Visible = False

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Function IsValidQuestioner() As Boolean
        Dim strErrorMessage As String = ""
        Dim i As Integer = 0
        For Each QuestionerTableRow As TableRow In QuestionnaireTable.Rows

            If QuestionerTableRow.Cells.Count > 0 Then
                If QuestionerTableRow.Cells(0).HasControls Then
                    i += 1
                    If TypeOf (QuestionerTableRow.Cells(0).Controls(0)) Is TextBox Then

                        If CType(QuestionerTableRow.Cells(0).Controls(0), TextBox).Text.Trim.Length = 0 Then
                            Dim label As Label = QuestionerTableRow.FindControl("Labelquestion" & i)

                            strErrorMessage += label.Text & " must be filled </BR>"
                        End If
                    ElseIf TypeOf (QuestionerTableRow.Cells(0).Controls(0)) Is RadioButtonList Then
                        If CType(QuestionerTableRow.Cells(0).Controls(0), RadioButtonList).SelectedIndex = -1 Then
                            Dim label As Label = QuestionerTableRow.FindControl("Labelquestion" & i)

                            strErrorMessage += label.Text & " must be choosen </BR>"
                        End If
                    End If
                End If

            End If

        Next

        If strErrorMessage.Trim.Length > 0 Then
            Throw New Exception(strErrorMessage)
        End If
        Return True
    End Function

    Private Function GetCurrentWorkflowHistory() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow
        Dim oRowWorkflowNotComplete() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim ArrUserid() As String = Nothing
        Try
            'cari yang status stated dan untuk user yang sedang login
            oRowWorkflowNotComplete = oRowCasemanagementWorkFlowHistory.Select("fk_eventtypeid IN (" & Eventtype.TaskStarted & "," & Eventtype.CaseMonitoring & ") and bcomplate=0")
            For i As Integer = 0 To oRowWorkflowNotComplete.Length - 1
                ArrUserid = oRowWorkflowNotComplete(i).UserID.Split(";")
                For j As Integer = 0 To ArrUserid.Length - 1
                    If ArrUserid(j).ToLower = Sahassa.AML.Commonly.SessionUserId.ToLower Then
                        Return oRowWorkflowNotComplete(i)
                    End If
                Next
            Next
            Return Nothing
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Sub BindTab()
        LoadDetailCaseManagement()
        LoadDataSuspiciusPerson()
        LoadProfileIdentitas()
        LoadProfileKeuangan()
        LoadProfileTypology()
        'LoadDataPenjelasan()
        LoadAnswer()
        LoadWorkFlow()
    End Sub

    Protected Sub btnProposedPenjelasan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProposedPenjelasan.Click
        Dim oCurrentWorkflowhistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Try
            'cek isipenjelasan harus di isi semua
            If IsValidQuestioner() Then
                'Ambil jawaban
                Dim answer As String = ""
                Dim ListNewAnswer As New EkaDataNettier.Entities.TList(Of EkaDataNettier.Entities.CaseManagementQuestionAnswer)

                Dim counter As Integer = 0
                For Each QuestionerTableRow As TableRow In QuestionnaireTable.Rows
                    answer = ""
                    If QuestionerTableRow.Cells(0).Controls.Count > 0 Then
                        If TypeOf (QuestionerTableRow.Cells(0).Controls(0)) Is TextBox Then
                            answer = CType(QuestionerTableRow.Cells(0).Controls(0), TextBox).Text
                        ElseIf TypeOf (QuestionerTableRow.Cells(0).Controls(0)) Is RadioButtonList Then
                            If CType(QuestionerTableRow.Cells(0).Controls(0), RadioButtonList).SelectedIndex <> -1 Then
                                answer = CType(QuestionerTableRow.Cells(0).Controls(0), RadioButtonList).SelectedItem.Value
                            End If
                        End If
                    End If
                    If answer <> "" Then
                        Using objNewAnswer As New EkaDataNettier.Entities.CaseManagementQuestionAnswer
                            objNewAnswer.FK_CaseManagement_ID = ObjCaseManagement.PK_CaseManagementID
                            objNewAnswer.Answer = answer
                            objNewAnswer.QuestionNo = CInt(QuestionnaireTable.Rows(counter - 1).Cells(0).Text)
                            ListNewAnswer.Add(objNewAnswer)
                        End Using
                    End If
                    counter += 1
                Next

                'Ini sisa existing code
                Dim strdescription As String = ""

                oCurrentWorkflowhistory = GetCurrentWorkflowHistory()
                If Not oCurrentWorkflowhistory Is Nothing Then
                    strdescription = GetDescriptionByStep(oCurrentWorkflowhistory.workflowstep)
                End If
                AMLBLL.CaseManagementBLL.SaveQuestionerAnswerFIX(ListNewAnswer, strdescription)
                AMLBLL.CaseManagementBLL.UpdateAging(Me.PK_CaseManagementID)

                'Response.Redirect("CaseManagementview.aspx", False)
                UnloadSession()
                If Me.IsHaveRighttoInputInvestigationNotoesResponse Then
                    Me.Panel1.Visible = True
                    BindListAttachment()
                Else
                    Me.Panel1.Visible = False
                End If
                BindTab()

                If IsHaveRightToInputCaseManagementPenjelasan() Then

                    EnableInputAnswer(True)
                Else

                    EnableInputAnswer(False)
                End If
                'Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, "  window.alert(' Underlying Transaction Confirmation is Waiting For Approval.')", True)
                If Request.Params("source") = "" Then
                    Response.Redirect("CaseManagementView.aspx?msg=Underlying Transaction Confirmation is Waiting For Approval.")
                Else
                    Response.Redirect("CaseManagementViewAll.aspx?msg=Underlying Transaction Confirmation is Waiting For Approval.")
                End If

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Function GetDescriptionByStep(ByVal strWorkFlowStep) As String
        Dim orowSettingworkflow() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = Nothing
        Dim ArrApprover() As String = Nothing
        Try
            orowSettingworkflow = oSettingWorkFlow.Select("workflowstep='" & strWorkFlowStep & "'")
            If orowSettingworkflow.Length > 0 Then
                For i As Integer = 0 To orowSettingworkflow.Length - 1

                    If orowSettingworkflow(i).Paralel > 1 Then
                        ArrApprover = orowSettingworkflow(i).Approvers.Split("|")
                        For j As Integer = 0 To ArrApprover.Length - 1
                            If ArrApprover(j) = Sahassa.AML.Commonly.SessionPkUserId Then
                                Return "Parallel " & i + 1
                            End If
                        Next
                    ElseIf orowSettingworkflow(i).Paralel = 1 Then
                        Return ""
                    End If

                Next
            Else
                Return ""

            End If
        Catch ex As Exception
            LogError(ex)
            Return ""
        Finally
            If Not orowSettingworkflow Is Nothing Then
                orowSettingworkflow = Nothing
            End If
            If Not ArrApprover Is Nothing Then
                ArrApprover = Nothing
            End If
        End Try
    End Function

    Private Sub UnloadSession()
        _oRowCaseManagement = Nothing
        _oRowCaseManagementWorkFlow = Nothing
        _oRowCasemanagementWorkFlowHistory = Nothing

    End Sub

    Protected Sub btnSaveAsDraftPenjelasan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAsDraftPenjelasan.Click
        Dim oCurrentWorkflowhistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Try
            Dim answer As String = ""
            Dim ListNewAnswer As New EkaDataNettier.Entities.TList(Of EkaDataNettier.Entities.CaseManagementQuestionAnswer)

            Dim counter As Integer = 0
            For Each QuestionerTableRow As TableRow In QuestionnaireTable.Rows
                answer = ""
                If QuestionerTableRow.Cells(0).Controls.Count > 0 Then
                    If TypeOf (QuestionerTableRow.Cells(0).Controls(0)) Is TextBox Then
                        answer = CType(QuestionerTableRow.Cells(0).Controls(0), TextBox).Text
                    ElseIf TypeOf (QuestionerTableRow.Cells(0).Controls(0)) Is RadioButtonList Then
                        If CType(QuestionerTableRow.Cells(0).Controls(0), RadioButtonList).SelectedIndex <> -1 Then
                            answer = CType(QuestionerTableRow.Cells(0).Controls(0), RadioButtonList).SelectedItem.Value
                        End If
                    End If
                End If
                If answer <> "" Then
                    Using objNewAnswer As New EkaDataNettier.Entities.CaseManagementQuestionAnswer
                        objNewAnswer.FK_CaseManagement_ID = ObjCaseManagement.PK_CaseManagementID
                        objNewAnswer.Answer = answer
                        objNewAnswer.QuestionNo = CInt(QuestionnaireTable.Rows(counter - 1).Cells(0).Text)
                        ListNewAnswer.Add(objNewAnswer)
                    End Using
                End If
                counter += 1
            Next

            If CaseManagementBLL.SaveQuestionerAnswerDraft(ListNewAnswer) Then
                If Request.Params("source") = "" Then
                    Response.Redirect("CaseManagementView.aspx?msg=Data Penjelasan Saved as Draft.", False)
                Else
                    Response.Redirect("CaseManagementViewAll.aspx?msg=Data Penjelasan Saved as Draft.", False)
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageError.PreRender
        If cvalPageError.IsValid = False Then

            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Private Function IsHaveOtherWorkFlowThatNotComplate(ByVal strworkflowStep As String, ByRef osqltrans As SqlTransaction) As Boolean
        Dim oTableWorkflowHistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryDataTable = Nothing
        Dim ArrWorkflow() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Try
            Using adapterwp As New AMLDAL.CaseManagementTableAdapters.SelectMapCaseManagementWorkflowHistoryTableAdapter
                SetTransaction(adapterwp, osqltrans)
                oTableWorkflowHistory = adapterwp.SelectMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID)
                ArrWorkflow = oTableWorkflowHistory.Select("fk_eventtypeid IN (" & Eventtype.TaskStarted & "," & Eventtype.CaseMonitoring & ") and bcomplate=0 and workflowstep='" & strworkflowStep.Replace("'", "''") & "'")
                If ArrWorkflow.Length = 0 Then
                    Return False
                Else
                    Return True
                End If
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        Finally
            If Not oTableWorkflowHistory Is Nothing Then
                oTableWorkflowHistory = Nothing
            End If
            If Not ArrWorkflow Is Nothing Then
                ArrWorkflow = Nothing
            End If
        End Try

    End Function

    Private Function GetProposedStatusFinal(ByVal StepWorkflow As String, ByRef osqltrans As SqlTransaction) As Integer
        'Ambil workflowhistory yang final
        Dim arrHasil() As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow
        Dim orowProposedAction() As AMLDAL.CaseManagement.CaseManagementProposedActionRow
        Dim arrProposedAction(-1) As Integer
        Try
            'harus query lagi kalau ngak bakal ambil yang belum terupdate
            Using adapter As New AMLDAL.CaseManagementTableAdapters.SelectMapCaseManagementWorkflowHistoryTableAdapter
                SetTransaction(adapter, osqltrans)
                Using oTable As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryDataTable = adapter.SelectMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID)
                    If oTable.Rows.Count > 0 Then
                        'Cari yang eventtype=taskcomplate(fk_eventtypeid=3) dan workflowste=parameter dan
                        arrHasil = oTable.Select("fk_eventtypeid=" & Eventtype.TaskComplated & " and workflowstep='" & StepWorkflow.Replace("'", "''") & "' and baktifworkflow=1")
                        If arrHasil.Length > 0 Then
                            For i As Integer = 0 To arrHasil.Length - 1
                                ReDim Preserve arrProposedAction(arrProposedAction.Length)
                                If Not arrHasil(i).IsFK_CaseManagementProposedActionIDNull Then
                                    arrProposedAction(arrProposedAction.Length - 1) = arrHasil(i).FK_CaseManagementProposedActionID
                                End If
                            Next

                            Dim intMaxPriority As Integer = Integer.MaxValue
                            Dim PKPriority As Integer = 0
                            For i As Integer = 0 To arrProposedAction.Length - 1
                                Using adapterProposedAction As New AMLDAL.CaseManagementTableAdapters.CaseManagementProposedActionTableAdapter
                                    orowProposedAction = adapterProposedAction.GetData.Select("PK_CaseManagementProposedActionID=" & arrProposedAction(i))
                                    If orowProposedAction.Length > 0 Then
                                        If orowProposedAction(0).LevelPriority < intMaxPriority Then
                                            'Cari priority paling rendah
                                            intMaxPriority = orowProposedAction(0).LevelPriority
                                            PKPriority = orowProposedAction(0).PK_CaseManagementProposedActionID
                                        End If
                                        'if intMaxPriority
                                    End If
                                End Using
                            Next

                            Return PKPriority
                        End If
                    End If
                End Using

            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try

    End Function

    Private Function GetUserIDbyPK(ByVal PKUSERID As String) As String
        Try
            If PKUSERID <> "" Then
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Using oTable As AMLDAL.AMLDataSet.UserDataTable = adapter.GetDataByUserID(PKUSERID)
                        If oTable.Rows.Count > 0 Then
                            Return CType(oTable.Rows(0), AMLDAL.AMLDataSet.UserRow).UserID
                        End If
                    End Using
                End Using
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub SendEmail(ByVal StrRecipientTo As String, ByVal StrRecipientCC As String, ByVal StrSubject As String, ByVal strbody As String)
        Dim oEmail As Sahassa.AML.EMail
        Try
            oEmail = New Sahassa.AML.EMail
            oEmail.Sender = System.Configuration.ConfigurationManager.AppSettings("FromMailAddress")
            oEmail.Recipient = StrRecipientTo
            oEmail.RecipientCC = StrRecipientCC
            oEmail.Subject = StrSubject
            oEmail.Body = strbody.Replace(vbLf, "<br>")
            oEmail.SendEmail()
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub


    Protected Sub BtnDoneProposed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDoneProposed.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Dim oCurrentWorkflowhistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim ProposedStatusFinal As Integer
        Dim ProposedStatusPrev As Integer
        Dim strDescription As String = ""
        Dim StrNextWorkflowStep As String = ""
        Try
            If Page.IsValid Then
                Using adapter As New AMLDAL.CaseManagementTableAdapters.SelectMapCaseManagementWorkflowHistoryTableAdapter
                    oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                    'Selesaikan satu workflow

                    oCurrentWorkflowhistory = GetCurrentWorkflowHistory()
                    If Not oCurrentWorkflowhistory Is Nothing Then
                        strDescription = GetDescriptionByStep(oCurrentWorkflowhistory.workflowstep)
                        adapter.UpdateMapCaseManagementWorkfloHistoryBComplate(True, oCurrentWorkflowhistory.PK_MapCaseManagementWorkflowHistoryID)
                        Dim intAccountownerid As Integer = 0
                        If CboProposedAction.SelectedValue = ProposedAction.ReAssign Then
                            intAccountownerid = CboAccountOwner.SelectedValue
                        End If
                        adapter.usp_DeleteTaskDraftMapCaseManagementWorkFlowHistory(Me.PK_CaseManagementID)
                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskComplated, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, Me.TextInvestigationNotes.Text, CboProposedAction.SelectedValue, True, strDescription, intAccountownerid, True)

                    End If

                    Using adapterCaseManagement As New AMLDAL.CaseManagementTableAdapters.SelectCaseManagementTableAdapter
                        SetTransaction(adapterCaseManagement, oSQLTrans)

                        ProposedStatusFinal = GetProposedStatusFinal(oCurrentWorkflowhistory.workflowstep, oSQLTrans)
                        ProposedStatusPrev = ObjCaseManagement.FK_LastProposedStatusID
                        StrNextWorkflowStep = EKABLL.CaseManagementBLL.getnextworkflowstep(oRowCaseManagement.FK_AccountOwnerID, oRowCaseManagement.CaseDescription, oRowCaseManagement.VIPCode, oRowCaseManagement.InsiderCode, oRowCaseManagement.Segment, oRowCaseManagement.SBU, oRowCaseManagement.SUBSBU, oRowCaseManagement.RM, oRowCaseManagement.WorkflowStep, oRowCaseManagement.FK_LastProposedStatusID, ProposedStatusFinal)
                        Dim bStatusWorkflowTeratas As Boolean = Me.IsLevelWorkFlowTeratas(oSQLTrans)

                        If StrNextWorkflowStep = "Finish" Or StrNextWorkflowStep = "Reassign" Then
                            bStatusWorkflowTeratas = True
                        End If

                        adapterCaseManagement.UpdateCaseManagementLastProposedAction(CboProposedAction.SelectedValue, Me.PK_CaseManagementID)
                        'Kalau level teratas dan sudah ngak ada lagi untuk current workflow step yang belum di approve


                        If bStatusWorkflowTeratas Then
                            If Not IsHaveOtherWorkFlowThatNotComplate(oCurrentWorkflowhistory.workflowstep, oSQLTrans) Then

                                'Keputusan status yang dipilih berdasarkan level prioritas paling tinggi(paling kecil dianggap paling tinggi 1-5) dari semua approver


                                Select Case ProposedStatusFinal
                                    Case ProposedAction.Dropped
                                        'insert ke mapcasemanagementworkflowhistory dgn eventtype workflowfinish
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.WorkFlowFinish, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, "", Nothing, True, strDescription, 0, 0)

                                        'update casemanagement set fk_casestastusid=3 (droped)
                                        adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.Dropped, Me.PK_CaseManagementID)

                                    Case ProposedAction.CaseMonitoring
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.CaseMonitoring, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, "", Nothing, False, strDescription, 0, 0)
                                        adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.UnderInvestigate, Me.PK_CaseManagementID)
                                        StrNextWorkflowStep = oCurrentWorkflowhistory.workflowstep

                                    Case ProposedAction.ConfirmAsSTR
                                        'insert ke mapcasemanagementworkflowhistory dgn eventtype workflowfinish
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.WorkFlowFinish, oCurrentWorkflowhistory.workflowstep, Sahassa.AML.Commonly.SessionUserId, "", Nothing, True, strDescription, 0, 0)
                                        'update casemanagement set fk_casestastusid=4 (approved)
                                        adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.Approved, Me.PK_CaseManagementID)

                                    Case ProposedAction.ReAssign
                                        Dim intAccountownerpilihan As Integer = 0
                                        'TODO: Confirm ambil datanya sesuai ngak?
                                        'intAccountownerpilihan = GetPilihanAccontOwner(oCurrentWorkflowhistory.workflowstep, oSQLTrans)
                                        intAccountownerpilihan = CboAccountOwner.SelectedValue
                                        If intAccountownerpilihan > 0 Then
                                            ':Update accountownerid dengan accountowner, pic yang diselect
                                            Dim struserid As String = ""
                                            'Using adapteraccount As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.AccountOwnerUserMappingTableAdapter
                                            '    struserid = CType(adapteraccount.GetUserIDLowestLevelByAccountOwnerID(intAccountownerpilihan).Rows(0), AMLDAL.SettingWorkFlowCaseManagement.AccountOwnerUserMappingRow).UserID
                                            'End Using
                                            Using AdapterCaseManagementWorkflow As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter
                                                Dim CaseManagementWorkflowTable As New AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable
                                                CaseManagementWorkflowTable = AdapterCaseManagementWorkflow.GetSettingWorkFlowCaseManagementByAccountOwnerID(intAccountownerpilihan, Me.oRowCaseManagement.VIPCode, Me.oRowCaseManagement.InsiderCode, Me.oRowCaseManagement.Segment, Me.oRowCaseManagement.CaseDescription, Me.oRowCaseManagement.SBU, Me.oRowCaseManagement.SUBSBU, Me.oRowCaseManagement.RM)
                                                If Not CaseManagementWorkflowTable Is Nothing Then
                                                    If CaseManagementWorkflowTable.Rows.Count > 0 Then
                                                        Dim CaseManagementWorkflowTableRows() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
                                                        CaseManagementWorkflowTableRows = CaseManagementWorkflowTable.Select("SerialNo=1 AND Paralel=1")
                                                        If Not CaseManagementWorkflowTableRows Is Nothing Then
                                                            If CaseManagementWorkflowTableRows.Length > 0 Then
                                                                Dim CaseManagementWorkflowTableRow As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
                                                                CaseManagementWorkflowTableRow = CaseManagementWorkflowTableRows(0)
                                                                If Not CaseManagementWorkflowTableRow.IsWorkflowStepNull Then
                                                                    StrNextWorkflowStep = CaseManagementWorkflowTableRow.WorkflowStep
                                                                End If
                                                                If Not CaseManagementWorkflowTableRow.IsApproversNull Then
                                                                    Dim ArrApprovers() As String = Nothing
                                                                    ArrApprovers = CaseManagementWorkflowTableRow.Approvers.Split("|"c)
                                                                    If Not ArrApprovers Is Nothing Then
                                                                        If ArrApprovers.Length > 0 Then
                                                                            struserid = Me.GetUserIDbyPK(ArrApprovers(0))
                                                                        End If
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End Using

                                            adapterCaseManagement.UpdateCaseManagementAccountOwner(CboAccountOwner.SelectedValue, struserid, Me.PK_CaseManagementID)
                                            adapterCaseManagement.UpdateCaseManagementWorkflowStepByPK(StrNextWorkflowStep, Me.PK_CaseManagementID)
                                            adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.Reassign, "", "", "", Nothing, True, "", Nothing, False)
                                            ':Insert ke mapcasemanagementworkflowhistory dengan eventtype=2 (task started) untuk accountowner yang baru.
                                        End If
                                        Dim oworkflowBaruSerial1() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkFlowStepBaruSerial1(CboAccountOwner.SelectedValue, oRowCaseManagement.VIPCode, oRowCaseManagement.InsiderCode, oRowCaseManagement.Segment, oRowCaseManagement.CaseDescription, oRowCaseManagement.SBU, oRowCaseManagement.SUBSBU, oRowCaseManagement.RM)
                                        If Not oworkflowBaruSerial1 Is Nothing AndAlso oworkflowBaruSerial1.Length > 0 Then
                                            For i As Integer = 0 To oworkflowBaruSerial1.Length - 1
                                                Dim strPKUserBaruSerial1 As String = oworkflowBaruSerial1(i).Approvers
                                                Dim arrPKUserBaruSerial1() As String = strPKUserBaruSerial1.Split("|")

                                                Dim strPKNotifyUser As String = oworkflowBaruSerial1(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")
                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""

                                                Dim strListUserBaruSerial1 As String = ""
                                                Dim strDescriptionBaruSerial1 As String = ""
                                                For j As Integer = 0 To arrPKUserBaruSerial1.Length - 1
                                                    strListUserBaruSerial1 += GetUserIDbyPK(arrPKUserBaruSerial1(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserBaruSerial1(j))
                                                    If j <> arrPKUserBaruSerial1.Length - 1 Then
                                                        strListUserBaruSerial1 += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oworkflowBaruSerial1(i).Pk_CMW_Id, oworkflowBaruSerial1(i).SerialNo)
                                                strEmailBody = GetEmailBody(oworkflowBaruSerial1(i).Pk_CMW_Id, oworkflowBaruSerial1(i).SerialNo)

                                                If oworkflowBaruSerial1.Length > 1 Then
                                                    strDescriptionBaruSerial1 = "Parallel " & i + 1
                                                Else
                                                    strDescriptionBaruSerial1 = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oworkflowBaruSerial1(i).WorkflowStep, strListUserBaruSerial1, "", Nothing, 0, strDescriptionBaruSerial1, 0, 0)
                                                SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next

                                        End If

                                    Case ProposedAction.RequestChange
                                        Dim oWorkFlowStepSebelumnya() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkflowStepSebelumnya(oCurrentWorkflowhistory.workflowstep)
                                        If Not oWorkFlowStepSebelumnya Is Nothing AndAlso oWorkFlowStepSebelumnya.Length > 0 Then
                                            If Not oWorkFlowStepSebelumnya(0).IsWorkflowStepNull Then
                                                StrNextWorkflowStep = oWorkFlowStepSebelumnya(0).WorkflowStep
                                            End If
                                            For i As Integer = 0 To oWorkFlowStepSebelumnya.Length - 1
                                                Dim strPKUserSebelumnya As String = oWorkFlowStepSebelumnya(i).Approvers
                                                Dim arrPKUserSebelumnya() As String = strPKUserSebelumnya.Split("|")

                                                Dim strPKNotifyUser As String = oWorkFlowStepSebelumnya(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")

                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""

                                                Dim strListUserStepSebelumnya As String = ""
                                                Dim strDescriptionStepSebelumnya As String = ""
                                                For j As Integer = 0 To arrPKUserSebelumnya.Length - 1
                                                    strListUserStepSebelumnya += GetUserIDbyPK(arrPKUserSebelumnya(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserSebelumnya(j))
                                                    If j <> arrPKUserSebelumnya.Length - 1 Then
                                                        strListUserStepSebelumnya += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)
                                                strEmailBody = GetEmailBody(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)

                                                Dim strDescriptioinStepSebelumnya As String
                                                If oWorkFlowStepSebelumnya.Length > 1 Then
                                                    strDescriptioinStepSebelumnya = "Parallel " & i + 1
                                                Else
                                                    strDescriptioinStepSebelumnya = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oWorkFlowStepSebelumnya(i).WorkflowStep, strListUserStepSebelumnya, "", Nothing, 0, strDescriptioinStepSebelumnya, 0, 0)
                                                SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next
                                            adapterCaseManagement.UpdateCaseManagementWorkflowStepByPK(StrNextWorkflowStep, Me.PK_CaseManagementID)
                                        End If
                                End Select
                                ':update mapcasemanagementworkflowhistory set baktiveworkflow menjadi false untuk record yang eventtypenya complate untuk workflow step parameter

                                adapter.UpdateMapCaseManagementWorkflowHistoryBAktifWorkflow(Eventtype.TaskComplated, oCurrentWorkflowhistory.workflowstep, Me.PK_CaseManagementID)
                            End If
                        Else
                            If Not IsHaveOtherWorkFlowThatNotComplate(oCurrentWorkflowhistory.workflowstep, oSQLTrans) Then

                                'Bukan level teratas maka buat task baru untuk level berikutnya.
                                ProposedStatusFinal = GetProposedStatusFinal(oCurrentWorkflowhistory.workflowstep, oSQLTrans)
                                Select Case ProposedStatusFinal
                                    Case ProposedAction.ConfirmAsSTR, ProposedAction.Dropped, ProposedAction.RequestAssignment
                                        ': insert ke mapcasemanagementworkflowhistory dengan eventtype =2(taskstarted) untuk user yang terdaftar dalam setting level berikutnya

                                        Dim oWorkFlowStepBerikutnya() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkflowStepBerikutnya(oCurrentWorkflowhistory.workflowstep)
                                        If Not oWorkFlowStepBerikutnya Is Nothing AndAlso oWorkFlowStepBerikutnya.Length > 0 Then

                                            If Not oWorkFlowStepBerikutnya(0).IsWorkflowStepNull Then
                                                StrNextWorkflowStep = oWorkFlowStepBerikutnya(0).WorkflowStep
                                            End If
                                            For i As Integer = 0 To oWorkFlowStepBerikutnya.Length - 1
                                                Dim strPKUserStepBerikutnya As String = oWorkFlowStepBerikutnya(i).Approvers
                                                Dim arrPKUserBerikutnya() As String = strPKUserStepBerikutnya.Split("|")

                                                Dim strPKNotifyUser As String = oWorkFlowStepBerikutnya(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")

                                                Dim strListuserStepBerikutnya As String = ""
                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""

                                                For j As Integer = 0 To arrPKUserBerikutnya.Length - 1
                                                    strListuserStepBerikutnya += GetUserIDbyPK(arrPKUserBerikutnya(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserBerikutnya(j))
                                                    If j <> arrPKUserBerikutnya.Length - 1 Then
                                                        strListuserStepBerikutnya += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oWorkFlowStepBerikutnya(i).Pk_CMW_Id, oWorkFlowStepBerikutnya(i).SerialNo)
                                                strEmailBody = GetEmailBody(oWorkFlowStepBerikutnya(i).Pk_CMW_Id, oWorkFlowStepBerikutnya(i).SerialNo)

                                                Dim strDescriptionStepBerikutnya As String
                                                If oWorkFlowStepBerikutnya.Length > 1 Then
                                                    strDescriptionStepBerikutnya = "Parallel " & i + 1
                                                Else
                                                    strDescriptionStepBerikutnya = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oWorkFlowStepBerikutnya(i).WorkflowStep, strListuserStepBerikutnya, "", Nothing, 0, strDescriptionStepBerikutnya, 0, 0)
                                                SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next

                                        End If

                                    Case ProposedAction.CaseMonitoring
                                        adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oCurrentWorkflowhistory.workflowstep, oCurrentWorkflowhistory.UserID, "", Nothing, 0, "", 0, 0)
                                        StrNextWorkflowStep = oCurrentWorkflowhistory.workflowstep

                                    Case ProposedAction.RequestChange

                                        ':insert ke mapcasemanagementworkflowhistory dengan eventype=2(taskstarted) untuk user yang terdaftar dalam setting level sebelumnnya
                                        Dim oWorkFlowStepSebelumnya() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = GetWorkflowStepSebelumnya(oCurrentWorkflowhistory.workflowstep)

                                        If Not oWorkFlowStepSebelumnya Is Nothing AndAlso oWorkFlowStepSebelumnya.Length > 0 Then
                                            If Not oWorkFlowStepSebelumnya(0).IsWorkflowStepNull Then
                                                StrNextWorkflowStep = oWorkFlowStepSebelumnya(0).WorkflowStep
                                            End If

                                            For i As Integer = 0 To oWorkFlowStepSebelumnya.Length - 1
                                                Dim strPKUserSebelumnya As String = oWorkFlowStepSebelumnya(i).Approvers
                                                Dim arrPKUserSebelumnya() As String = strPKUserSebelumnya.Split("|")

                                                Dim strPKNotifyUser As String = oWorkFlowStepSebelumnya(i).NotifyOthers
                                                Dim arrPKNotifyUser() As String = strPKNotifyUser.Split("|")

                                                Dim strEmailRecepients As String = ""
                                                Dim strEmailCC As String = ""
                                                Dim strEmailSubject As String = ""
                                                Dim strEmailBody As String = ""

                                                Dim strListUserStepSebelumnya As String = ""
                                                Dim strDescriptionStepSebelumnya As String = ""
                                                For j As Integer = 0 To arrPKUserSebelumnya.Length - 1
                                                    strListUserStepSebelumnya += GetUserIDbyPK(arrPKUserSebelumnya(j))
                                                    strEmailRecepients += GetEmailAddress(arrPKUserSebelumnya(j))
                                                    If j <> arrPKUserSebelumnya.Length - 1 Then
                                                        strListUserStepSebelumnya += ";"
                                                        strEmailRecepients += ";"
                                                    End If
                                                Next
                                                For j As Integer = 0 To arrPKNotifyUser.Length - 1
                                                    strEmailCC += GetEmailAddress(arrPKNotifyUser(j))
                                                    If j <> arrPKNotifyUser.Length - 1 Then
                                                        strEmailCC += ";"
                                                    End If
                                                Next
                                                strEmailSubject = GetEmailSubject(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)
                                                strEmailBody = GetEmailBody(oWorkFlowStepSebelumnya(i).Pk_CMW_Id, oWorkFlowStepSebelumnya(i).SerialNo)

                                                Dim strDescriptioinStepSebelumnya As String
                                                If oWorkFlowStepSebelumnya.Length > 1 Then
                                                    strDescriptioinStepSebelumnya = "Parallel " & i + 1
                                                Else
                                                    strDescriptioinStepSebelumnya = ""
                                                End If
                                                adapter.InsertMapCaseManagementWorkflowHistory(Me.PK_CaseManagementID, Now, Eventtype.TaskStarted, oWorkFlowStepSebelumnya(i).WorkflowStep, strListUserStepSebelumnya, "", Nothing, 0, strDescriptioinStepSebelumnya, 0, 0)
                                                SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                                            Next
                                        End If
                                End Select
                                adapter.UpdateMapCaseManagementWorkflowHistoryBAktifWorkflow(Eventtype.TaskComplated, oCurrentWorkflowhistory.workflowstep, Me.PK_CaseManagementID)
                                'Else
                                adapterCaseManagement.UpadateCaseManagementCaseStatus(CaseManagementStatus.UnderInvestigate, Me.PK_CaseManagementID)
                                adapterCaseManagement.UpdateCaseManagementWorkflowStepByPK(StrNextWorkflowStep, Me.PK_CaseManagementID)
                            End If
                        End If
                        adapterCaseManagement.UpdateCaseManagementLastUpdatedByPK(DateTime.Now(), Me.PK_CaseManagementID)
                    End Using

                    'update lastrun dari workflow
                    Using adapterCase As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementWorkflowTableAdapter
                        SetTransaction(adapterCase, oSQLTrans)
                        adapterCase.UpdateMapCaseManagementWorkflowLastRun(Now, Me.PK_CaseManagementID)
                    End Using
                    oSQLTrans.Commit()

                    AMLBLL.CaseManagementBLL.UpdateAging(Me.PK_CaseManagementID)

                    UnloadSession()
                    If Me.IsHaveRighttoInputInvestigationNotoesResponse Then
                        Me.Panel1.Visible = True
                        BindListAttachment()
                    Else
                        Me.Panel1.Visible = False
                    End If
                    BindTab()

                    If Request.Params("source") = "" Then
                        Response.Redirect("CaseManagementView.aspx?msg=Data Investigation Notes  Done And Proposed.", False)
                    Else
                        Response.Redirect("CaseManagementViewAll.aspx?msg=Data Investigation Notes Done And Proposed.", False)
                    End If

                End Using
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
            End If

        End Try
    End Sub

    Private Function GetWorkflowStepBerikutnya(ByVal strcurrentWorkflow As String) As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow()
        Dim orowStepCurrent() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
        Dim SerialNo As Integer
        Dim orowStepBerikut() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
        Try
            orowStepCurrent = oSettingWorkFlow.Select("workflowstep='" & strcurrentWorkflow.Replace("'", "''") & "'")
            If orowStepCurrent.Length > 0 Then
                SerialNo = orowStepCurrent(0).SerialNo
                orowStepBerikut = oSettingWorkFlow.Select("Serialno=" & SerialNo + 1 & "")
                Return orowStepBerikut
            End If
            Return Nothing
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function GetWorkFlowStepBaruSerial1(ByVal AccountOwnerID As Integer, ByVal strViPcode As String, ByVal strInsiderCode As String, ByVal strSegment As String, ByVal strdescriptioncasealert As String, sbu As String, subsbu As String, rm As String) As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow()

        Try
            Using adapter As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter

                Return adapter.GetSettingWorkFlowCaseManagementByAccountOwnerID(AccountOwnerID, strViPcode, strInsiderCode, strSegment, strdescriptioncasealert, sbu, subsbu, rm).Select("serialno=1")

            End Using
        Catch ex As Exception
            LogError(ex)
        End Try
    End Function

    Private Function GetWorkflowStepSebelumnya(ByVal strcurrentWorkflow As String) As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow()
        Dim orowStepCurrent() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
        Dim SerialNo As Integer
        Dim orowStepSebelumnya() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
        Try
            orowStepCurrent = oSettingWorkFlow.Select("workflowstep='" & strcurrentWorkflow.Replace("'", "''") & "'")
            If orowStepCurrent.Length > 0 Then
                SerialNo = orowStepCurrent(0).SerialNo
                If SerialNo > 1 Then
                    orowStepSebelumnya = oSettingWorkFlow.Select("Serialno=" & SerialNo - 1 & "")
                Else
                    orowStepSebelumnya = oSettingWorkFlow.Select("Serialno=" & SerialNo & "")
                End If
                Return orowStepSebelumnya
            End If
            Return Nothing
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function GetEmailAddress(ByVal PKUserID As String) As String
        Try
            If PKUserID <> "" Then
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Using otable As AMLDAL.AMLDataSet.UserDataTable = adapter.GetDataByUserID(PKUserID)
                        If otable.Rows.Count > 0 Then
                            If CType(otable.Rows(0), AMLDAL.AMLDataSet.UserRow).IsUserEmailAddressNull Then
                                Return ""
                            Else
                                Return CType(otable.Rows(0), AMLDAL.AMLDataSet.UserRow).UserEmailAddress
                            End If
                        Else
                            Return ""
                        End If
                    End Using
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Private Function GetEmailSubject(ByVal PKCMWID As Long, ByVal intSerialNo As Integer) As String
        Try
            Using adapter As New AMLDAL.CaseManagementTableAdapters.CaseManagementWorkflowEmailTemplateTableAdapter

                Using otable As AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateDataTable = adapter.GetEmailTemplate(PKCMWID, intSerialNo)
                    If otable.Rows.Count > 0 Then
                        If Not CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).IsSubject_EmailTemplateNull Then
                            Return CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).Subject_EmailTemplate
                        Else
                            Return ""
                        End If
                    Else
                        Return ""
                    End If

                End Using
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Private Function GetEmailBody(ByVal PKCMWID As Long, ByVal intSerialNo As Integer) As String
        Try
            Using adapter As New AMLDAL.CaseManagementTableAdapters.CaseManagementWorkflowEmailTemplateTableAdapter

                Using otable As AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateDataTable = adapter.GetEmailTemplate(PKCMWID, intSerialNo)
                    If otable.Rows.Count > 0 Then
                        If Not CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).IsBody_EmailTemplateNull Then
                            Return CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).Body_EmailTemplate
                        Else
                            Return ""
                        End If
                    Else
                        Return ""
                    End If

                End Using
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Function getCIFtoPrintSub() As String
        If ObjCaseManagementProfileKeuangan.Count > 0 Then
            If ObjCaseManagementProfileKeuangan(0).NilaiAkhirResiko = "H" Then
                If ObjCaseManagementProfileKeuangan(0).NilaiResikoModusAmount = "H" Then
                    If ObjCaseManagementProfileKeuangan(0).NilaiResikoModusAmountDebet = "H" Then
                        'ambil dari d
                        Return AMLBLL.CaseManagementBLL.GetCIFCaseManagementProfileKeuanganTransactionDebet(Me.PK_CaseManagementID)
                    Else
                        'ambil dari c
                        Return AMLBLL.CaseManagementBLL.GetCIFCaseManagementProfileKeuanganTransactionKredit(Me.PK_CaseManagementID)
                    End If
                ElseIf ObjCaseManagementProfileKeuangan(0).NilaiResikoModusFreqDayPeriod = "H" Then
                    If ObjCaseManagementProfileKeuangan(0).NilaiResikoModusFreqDayPeriodDebet = "H" Then
                        'ambil d
                        Return AMLBLL.CaseManagementBLL.GetCIFCaseManagementProfileKeuanganTransactionFreqPerPeriodDebet(Me.PK_CaseManagementID)
                    ElseIf ObjCaseManagementProfileKeuangan(0).NilaiResikoModusFreqDayPeriodKredit = "H" Then
                        'ambil k
                        Return AMLBLL.CaseManagementBLL.GetCIFCaseManagementProfileKeuanganTransactionFreqPerPeriodKredit(Me.PK_CaseManagementID)
                    End If
                End If
            ElseIf ObjCaseManagementProfileTopology(0).ResikoTopology = "H" Then

                Return AMLBLL.CaseManagementBLL.GetCIFMapCaseManagementTransaction(Me.PK_CaseManagementID)

            End If

        End If

    End Function

    Protected Sub ImagePrintSTR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImagePrintSTR.Click
        Response.Redirect("PrintSTR.aspx?CIFNO=" & getCIFtoPrintSub() & "&PKCaseManagementID=" & PK_CaseManagementID & "&asal=2", False)
    End Sub

    Protected Sub BtnSaveAsDraftWorklfow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSaveAsDraftWorklfow.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Dim oCurrentWorkflowhistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Dim ProposedStatusFinal As Integer
        Dim strDescription As String = ""
        Dim StrNextWorkflowStep As String = ""
        Dim intAccountownerid As Integer
        Try
            If Page.IsValid Then
                oCurrentWorkflowhistory = GetCurrentWorkflowHistory()
                If Not oCurrentWorkflowhistory Is Nothing Then
                    If CboProposedAction.SelectedValue = ProposedAction.ReAssign Then
                        intAccountownerid = CboAccountOwner.SelectedValue
                    End If
                    CaseManagementBLL.SaveNotesDraft(oCurrentWorkflowhistory.workflowstep, TextInvestigationNotes.Text.Trim, CboProposedAction.SelectedValue, intAccountownerid, Me.PK_CaseManagementID)

                End If

                'Page.ClientScript.RegisterStartupScript(Me.GetType, "Guid.NewGuid.ToString", "  window.alert(' Data Notes Saved as Draft.')", True)
                If Request.Params("source") = "" Then
                    Response.Redirect("CaseManagementView.aspx?msg=Data Notes Saved as Draft.", False)
                Else
                    Response.Redirect("CaseManagementViewAll.aspx?msg=Data Notes Saved as Draft.", False)
                End If

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Try
            Select Case Request.Params("source")
                Case ""
                    Response.Redirect("CaseManagementView.aspx", False)
                Case "str"
                    Response.Redirect("ProposalSTRCreateView.aspx", False)
                Case "CIFNo"
                    Response.Redirect("CaseManagementViewByCIFDetail.aspx?CIFNo=" & Request.Params("cifno"), False)
                Case Else
                    Response.Redirect("CaseManagementViewAll.aspx", False)

            End Select
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub CTRReportedToPPATK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("CashTransactionReportListFromCaseManagementDetail.aspx?CIfNo=" & ObjCaseManagement.CIFNo.ToString & "&Start=" & DateAdd(DateInterval.Year, -1, CDate(ObjCaseManagement.CreatedDate)) & "&End=" & ObjCaseManagement.CreatedDate.ToString & "")
    End Sub

    Protected Sub LnkDetailSTRRePortedToPPATK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objgridviewrow As GridViewRow = CType(CType(sender, LinkButton).NamingContainer, GridViewRow)

        Response.Redirect("CaseManagementViewDetail.aspx?PK_CaseManagementID=" & objgridviewrow.Cells(0).Text & "&source=all")
    End Sub

    Sub loadQuestioner()
        Using objQuestioner As TList(Of CaseManagementQuestion) = CaseManagementBLL.GetCaseManagementQuestionByPk(ObjCaseManagement.PK_CaseManagementID)
            Dim counter As Integer = 0
            For Each objQuestionerSingle As CaseManagementQuestion In objQuestioner
                QuestionnaireTable.Rows.Add(New TableRow)
                QuestionnaireTable.Rows(counter).Cells.Add(New TableCell)
                QuestionnaireTable.Rows(counter).Cells.Add(New TableCell)
                QuestionnaireTable.Rows(counter).Cells.Add(New TableCell)
                QuestionnaireTable.Rows(counter).Cells(0).RowSpan = 2
                QuestionnaireTable.Rows(counter).Cells(0).VerticalAlign = VerticalAlign.Top
                QuestionnaireTable.Rows(counter).Cells(0).Text = objQuestionerSingle.QuestionNo.ToString
                Dim lblquestion As New Label
                lblquestion.ID = "Labelquestion" & objQuestionerSingle.QuestionNo.GetValueOrDefault(0)
                lblquestion.Text = objQuestionerSingle.Question.ToString

                QuestionnaireTable.Rows(counter).Cells(1).Controls.Add(lblquestion)

                If objQuestionerSingle.FK_QuestionType_ID.GetValueOrDefault(0) = QuestionaireSTRBLL.QuestionTypeEnum.Freetext Then
                    QuestionnaireTable.Rows.Add(New TableRow)
                    counter += 1
                    QuestionnaireTable.Rows(counter).Cells.Add(New TableCell)
                    QuestionnaireTable.Rows(counter).Cells(0).ColumnSpan = 2

                    Dim answerTextBox As New TextBox
                    answerTextBox.ID = "question" & objQuestionerSingle.QuestionNo.GetValueOrDefault(0)
                    answerTextBox.TextMode = TextBoxMode.MultiLine
                    answerTextBox.Columns = 100
                    answerTextBox.Rows = 5
                    QuestionnaireTable.Rows(counter).Cells(0).Controls.Add(answerTextBox)
                ElseIf objQuestionerSingle.FK_QuestionType_ID.GetValueOrDefault(0) = QuestionaireSTRBLL.QuestionTypeEnum.MultipleChoice Then
                    QuestionnaireTable.Rows.Add(New TableRow)
                    counter += 1
                    QuestionnaireTable.Rows(counter).Cells.Add(New TableCell)
                    QuestionnaireTable.Rows(counter).Cells(0).ColumnSpan = 2

                    Dim answerRadioButton As New RadioButtonList
                    Using objMultipleChoice As TList(Of CaseManagementQuestionMulipleChoice) = CaseManagementBLL.GetCaseManagementQuestionMultipleChoiceByPK(ObjCaseManagement.PK_CaseManagementID, objQuestionerSingle.QuestionNo)
                        For Each objMultipleChoiceSingle As CaseManagementQuestionMulipleChoice In objMultipleChoice
                            answerRadioButton.Items.Add(New ListItem(objMultipleChoiceSingle.MultipleChoice, objMultipleChoiceSingle.MultipleChoice))
                        Next
                    End Using
                    QuestionnaireTable.Rows(counter).Cells(0).Controls.Add(answerRadioButton)
                End If
                counter += 1
            Next
        End Using
    End Sub

    Sub LoadAnswer()
        Dim counter As Integer = 0
        Using objAnswer As TList(Of CaseManagementQuestionAnswer) = CaseManagementBLL.Get_CaseManagementQuestionAnswerByFK_CaseManagement_ID(ObjCaseManagement.PK_CaseManagementID)
            If objAnswer.Count > 0 Then
                For Each objAnswerSingle As CaseManagementQuestionAnswer In objAnswer
                    counter = 0
                    For Each QuestionnaireTableROW As TableRow In QuestionnaireTable.Rows

                        If QuestionnaireTableROW.Cells(0).Text = objAnswerSingle.QuestionNo.ToString Then
                            If TypeOf (QuestionnaireTable.Rows(counter + 1).Cells(0).Controls(0)) Is TextBox Then
                                CType(QuestionnaireTable.Rows(counter + 1).Cells(0).Controls(0), TextBox).Text = objAnswerSingle.Answer
                            ElseIf TypeOf (QuestionnaireTable.Rows(counter + 1).Cells(0).Controls(0)) Is RadioButtonList Then
                                Try
                                    CType(QuestionnaireTable.Rows(counter + 1).Cells(0).Controls(0), RadioButtonList).SelectedValue = objAnswerSingle.Answer
                                Catch ex As Exception

                                End Try
                            End If
                            Exit For
                        End If
                        counter += 1
                    Next
                Next
            End If
        End Using
    End Sub

    Protected Sub btnSaveAsDraftPenjelasanOldMethod_Click(sender As Object, e As System.EventArgs) Handles btnSaveAsDraftPenjelasanOldMethod.Click
        Dim oCurrentWorkflowhistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Try
            'cek isipenjelasan harus di isi semua
            '   If IsDataPenjelasanValid() Then
            ObjCaseManagementPenjeleasan.AsalSumberData = txtSumberDana.Text
            ObjCaseManagementPenjeleasan.LatarBelakangTransaksi = txtLatarBelakangTransaksi.Text
            ObjCaseManagementPenjeleasan.JenisPekerjaan = txtJenisPekerjaan.Text
            ObjCaseManagementPenjeleasan.BidangUsaha = txtBidangPekerjaan.Text
            ObjCaseManagementPenjeleasan.LokasiPekerjaan = txtLokasiPekerjaan.Text
            ObjCaseManagementPenjeleasan.Jabatan = txtJabatan.Text

            If ObjCaseManagementProfileTopology.Count > 0 Then
                If ObjCaseManagementProfileTopology(0).ResikoTopology.ToLower = "h" Then
                    If ObjCaseManagementProfileTopology(0).CaseDescription.ToLower = "penipuan" Then
                        If RDlVerifikasiTelepon.SelectedValue <> "" Then
                            ObjCaseManagementPenjeleasan.IsAlreadyVerifikasiByPhone = RDlVerifikasiTelepon.SelectedValue
                        End If

                    ElseIf ObjCaseManagementProfileTopology(0).CaseDescription.ToLower = "OB di Proses Tunai".ToLower Then
                        ObjCaseManagementPenjeleasan.AlasanObdiProcessTunai = TxtAlasanOBProcessTunai.Text.Trim
                    ElseIf ObjCaseManagementProfileTopology(0).CaseDescription.ToLower = "Pass - Through Transaction".ToLower Then
                        If rdlProfileSesuaiPekerjaan.SelectedValue <> "" Then
                            ObjCaseManagementPenjeleasan.IsTransacactionSesuaiProfileNasabah = rdlProfileSesuaiPekerjaan.SelectedValue
                        End If

                    End If

                End If
            End If

            If RdlFrequensiSesuaiKebiasaan.SelectedValue <> "" Then
                ObjCaseManagementPenjeleasan.IsJmlOrFregSesuaiKebiasaanNasabah = RdlFrequensiSesuaiKebiasaan.SelectedValue
            End If

            If RdlIsSesuaiKebiasaanNasabah.SelectedValue <> "" Then
                ObjCaseManagementPenjeleasan.IsTransacactionSesuaiKebiasaanNasabah = RdlIsSesuaiKebiasaanNasabah.SelectedValue
            End If
            If IsTransaksiMencurigakan.SelectedValue <> "" Then
                ObjCaseManagementPenjeleasan.IsTransactionTsbMencurigakan = IsTransaksiMencurigakan.SelectedValue
            End If

            'If ObjCaseManagementProfileKeuangan.Count > 0 Then
            '    If ObjCaseManagementProfileKeuangan(0).NilaiAkhirResiko.ToLower = "h" Then

            '    End If
            'End If

            ObjCaseManagementPenjeleasan.Createdby = Sahassa.AML.Commonly.SessionUserId
            ObjCaseManagementPenjeleasan.LastUpdateBy = Sahassa.AML.Commonly.SessionUserId
            ObjCaseManagementPenjeleasan.LastUpdateDate = Now
            Dim strdescription As String

            'oCurrentWorkflowhistory = GetCurrentWorkflowHistory()
            'If Not oCurrentWorkflowhistory Is Nothing Then
            '    strdescription = GetDescriptionByStep(oCurrentWorkflowhistory.workflowstep)
            'End If

            AMLBLL.CaseManagementBLL.SaveDraftCaseManagement(ObjCaseManagementPenjeleasan, Me.PK_CaseManagementID)

            'If Me.Request.Params("Msg") <> "" Then
            'Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, "  window.alert(' Data Penjelasan Saved as Draft.')", True)

            If Request.Params("source") = "" Then
                Response.Redirect("CaseManagementView.aspx?msg=Data Penjelasan Saved as Draft.", False)
            Else
                Response.Redirect("CaseManagementViewAll.aspx?msg=Data Penjelasan Saved as Draft.", False)
            End If

            'End If

            ' End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Function IsDataPenjelasanValid() As Boolean
        Dim strErrorMessage As String = ""
        If txtSumberDana.Text.Trim = "" Then
            strErrorMessage += "Sumber Dana berasal dari is required </BR>"
        End If

        If txtLatarBelakangTransaksi.Text.Trim = "" Then
            strErrorMessage += "Latar Belakang Transaksi is required </BR>"
        End If
        If txtJenisPekerjaan.Text.Trim = "" Then
            strErrorMessage += "Jenis Pekerjaan is required </BR>"
        End If
        If txtBidangPekerjaan.Text.Trim = "" Then
            strErrorMessage += "Bidang Usaha is required </BR>"
        End If
        If txtLokasiPekerjaan.Text.Trim = "" Then
            strErrorMessage += "Lokasi Pekerjaan is required </BR>"
        End If
        If txtJabatan.Text.Trim = "" Then
            strErrorMessage += "Jabatan is required </BR>"
        End If

        If ObjCaseManagementProfileTopology.Count > 0 Then
            If ObjCaseManagementProfileTopology(0).ResikoTopology.ToLower = "h" Then
                If ObjCaseManagementProfileTopology(0).CaseDescription.ToLower = "penipuan" Then
                    If RDlVerifikasiTelepon.SelectedValue = "" Then
                        strErrorMessage += "Apakah Sudah dilakukan Verifikasi telepon Rumah /Kantor/Tempat Usaha ? is required </BR>"
                    End If

                ElseIf ObjCaseManagementProfileTopology(0).CaseDescription.ToLower = "OB Process Tunai".ToLower Then
                    If TxtAlasanOBProcessTunai.Text.Trim = "" Then
                        strErrorMessage += "Apakah Alasan dilakukan OB Di process Tunai  is required.<br/>"
                    End If

                ElseIf ObjCaseManagementProfileTopology(0).CaseDescription.ToLower = "Pass - Through Transaction".ToLower Then
                    If rdlProfileSesuaiPekerjaan.SelectedValue = "" Then
                        strErrorMessage += "Apakah Transaksi Tersebut sesuai dengan profile pekerjaan/Usaha Nasabah ? is required <br/>"
                    End If

                End If

            End If
        End If

        If RdlFrequensiSesuaiKebiasaan.SelectedValue = "" Then
            strErrorMessage += "a. Profil nasabah (pekerjaan/bidang usaha dan income nasabah) ? is required <br/>"
        End If
        If RdlIsSesuaiKebiasaanNasabah.SelectedValue = "" Then
            strErrorMessage += "b. Kebiasaan nasabah bertransaksi selama ini (dari segi nominal,frekuensi  dan jenis transaksi) ? is required <br/>"
        End If
        If IsTransaksiMencurigakan.SelectedValue = "" Then
            strErrorMessage += "Berdasarkan Jawaban diatas ,apakah menurut Anda transaksi tersebut mencurigakan ? is required <br/>"
        End If

        If strErrorMessage.Trim.Length > 0 Then
            Throw New Exception(strErrorMessage)
        End If
        Return True
    End Function

    Protected Sub btnProposedPenjelasanOldMethod_Click(sender As Object, e As System.EventArgs) Handles btnProposedPenjelasanOldMethod.Click
        Dim oCurrentWorkflowhistory As AMLDAL.CaseManagement.SelectMapCaseManagementWorkflowHistoryRow = Nothing
        Try
            'cek isipenjelasan harus di isi semua
            If IsDataPenjelasanValid() Then
                ObjCaseManagementPenjeleasan.AsalSumberData = txtSumberDana.Text
                ObjCaseManagementPenjeleasan.LatarBelakangTransaksi = txtLatarBelakangTransaksi.Text
                ObjCaseManagementPenjeleasan.JenisPekerjaan = txtJenisPekerjaan.Text
                ObjCaseManagementPenjeleasan.BidangUsaha = txtBidangPekerjaan.Text
                ObjCaseManagementPenjeleasan.LokasiPekerjaan = txtLokasiPekerjaan.Text
                ObjCaseManagementPenjeleasan.Jabatan = txtJabatan.Text

                If ObjCaseManagementProfileTopology.Count > 0 Then
                    If ObjCaseManagementProfileTopology(0).ResikoTopology.ToLower = "h" Then
                        If ObjCaseManagementProfileTopology(0).CaseDescription.ToLower = "penipuan" Then
                            ObjCaseManagementPenjeleasan.IsAlreadyVerifikasiByPhone = RDlVerifikasiTelepon.SelectedValue
                        ElseIf ObjCaseManagementProfileTopology(0).CaseDescription.ToLower = "OB Process Tunai".ToLower Then
                            ObjCaseManagementPenjeleasan.AlasanObdiProcessTunai = TxtAlasanOBProcessTunai.Text.Trim

                        ElseIf ObjCaseManagementProfileTopology(0).CaseDescription.ToLower = "Pass - Through Transaction".ToLower Then
                            ObjCaseManagementPenjeleasan.IsTransacactionSesuaiProfileNasabah = rdlProfileSesuaiPekerjaan.SelectedValue
                        End If

                    End If
                End If

                If RdlFrequensiSesuaiKebiasaan.SelectedValue <> "" Then
                    ObjCaseManagementPenjeleasan.IsJmlOrFregSesuaiKebiasaanNasabah = RdlFrequensiSesuaiKebiasaan.SelectedValue
                End If

                If RdlIsSesuaiKebiasaanNasabah.SelectedValue <> "" Then
                    ObjCaseManagementPenjeleasan.IsTransacactionSesuaiKebiasaanNasabah = RdlIsSesuaiKebiasaanNasabah.SelectedValue
                End If
                If IsTransaksiMencurigakan.SelectedValue <> "" Then
                    ObjCaseManagementPenjeleasan.IsTransactionTsbMencurigakan = IsTransaksiMencurigakan.SelectedValue
                End If

                'If ObjCaseManagementProfileKeuangan.Count > 0 Then
                '    If ObjCaseManagementProfileKeuangan(0).NilaiAkhirResiko.ToLower = "h" Then
                '        ObjCaseManagementPenjeleasan.IsJmlOrFregSesuaiKebiasaanNasabah = RdlFrequensiSesuaiKebiasaan.SelectedValue
                '    End If
                'End If

                ObjCaseManagementPenjeleasan.Createdby = Sahassa.AML.Commonly.SessionUserId
                ObjCaseManagementPenjeleasan.LastUpdateBy = Sahassa.AML.Commonly.SessionUserId
                ObjCaseManagementPenjeleasan.LastUpdateDate = Now
                Dim strdescription As String

                oCurrentWorkflowhistory = GetCurrentWorkflowHistory()
                If Not oCurrentWorkflowhistory Is Nothing Then
                    strdescription = GetDescriptionByStep(oCurrentWorkflowhistory.workflowstep)
                End If
                AMLBLL.CaseManagementBLL.SaveCaseManagementPenjelasan(ObjCaseManagementPenjeleasan, Me.PK_CaseManagementID, strdescription)
                AMLBLL.CaseManagementBLL.UpdateAging(Me.PK_CaseManagementID)

                'Response.Redirect("CaseManagementview.aspx", False)
                UnloadSession()
                If Me.IsHaveRighttoInputInvestigationNotoesResponse Then
                    Me.Panel1.Visible = True
                    BindListAttachment()
                Else
                    Me.Panel1.Visible = False
                End If
                BindTab()

                If IsHaveRightToInputCaseManagementPenjelasan() Then

                    EnableInputPenjelasan(True)
                Else

                    EnableInputPenjelasan(False)
                End If
                'Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, "  window.alert(' Underlying Transaction Confirmation is Waiting For Approval.')", True)
                If Request.Params("source") = "" Then
                    Response.Redirect("CaseManagementView.aspx?msg=Underlying Transaction Confirmation is Waiting For Approval.")
                Else
                    Response.Redirect("CaseManagementViewAll.aspx?msg=Underlying Transaction Confirmation is Waiting For Approval.")
                End If

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridViewTransKeuangan_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewTransKeuangan.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim objlblCredit As Label = CType(e.Row.FindControl("LblTransactionCredit"), Label)
                Dim objlblDebet As Label = CType(e.Row.FindControl("LblTransactionDebet"), Label)
                If CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("DebitORCredit") = "D" Then
                    objlblDebet.Text = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("TransactionLocalEquivalent"), Decimal).ToString("#,##")
                End If
                If CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("DebitORCredit") = "C" Then
                    objlblCredit.Text = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("TransactionLocalEquivalent"), Decimal).ToString("#,##")
                End If

                ' Dim objlblCounterAccount As Label = CType(e.Row.FindControl("LblCounterAccount"), Label)
                'Dim objLblCounterAccountName As Label = CType(e.Row.FindControl("LblCounterAccountName"), Label)
                Dim strTiketno As String = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("TransactionTicketNo"), String).Trim
                Dim strDebetCredit As String = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("DebitORCredit"), String)
                Dim transdate As Date = CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("TransactionDate")

                'objlblCounterAccount.Text = DataRepository.Provider.ExecuteScalar(Sahassa.AML.Commonly.GetSQLCommand("select dbo.ufn_GetAccountLawan('" & strTiketno & "','" & strDebetCredit & "','" & transdate.ToString("yyyy-MM-dd") & "') ")).ToString
                'objLblCounterAccountName.Text = DataRepository.Provider.ExecuteScalar(Sahassa.AML.Commonly.GetSQLCommand("select dbo.ufn_GetAccountNameByAccountNo('" & strTiketno & "','" & strDebetCredit & "','" & transdate.ToString("yyyy-MM-dd") & "') ")).ToString

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridViewTransKeuanganFreq_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewTransKeuanganFreq.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim objlblCredit As Label = CType(e.Row.FindControl("LblTransactionCredit"), Label)
                Dim objlblDebet As Label = CType(e.Row.FindControl("LblTransactionDebet"), Label)
                If CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("DebitORCredit") = "D" Then
                    objlblDebet.Text = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("TransactionLocalEquivalent"), Decimal).ToString("#,##")
                End If
                If CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("DebitORCredit") = "C" Then
                    objlblCredit.Text = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("TransactionLocalEquivalent"), Decimal).ToString("#,##")
                End If

                ' Dim objlblCounterAccount As Label = CType(e.Row.FindControl("LblCounterAccount"), Label)
                'Dim objLblCounterAccountName As Label = CType(e.Row.FindControl("LblCounterAccountName"), Label)
                Dim strTiketno As String = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("TransactionTicketNo"), String).Trim
                Dim strDebetCredit As String = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("DebitORCredit"), String)
                Dim transdate As Date = CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("TransactionDate")

                ' objlblCounterAccount.Text = DataRepository.Provider.ExecuteScalar(Sahassa.AML.Commonly.GetSQLCommand("select dbo.ufn_GetAccountLawan('" & strTiketno & "','" & strDebetCredit & "','" & transdate.ToString("yyyy-MM-dd") & "') ")).ToString
                'objLblCounterAccountName.Text = DataRepository.Provider.ExecuteScalar(Sahassa.AML.Commonly.GetSQLCommand("select dbo.ufn_GetAccountNameByAccountNo('" & strTiketno & "','" & strDebetCredit & "','" & transdate.ToString("yyyy-MM-dd") & "') ")).ToString
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridViewRelatedTransaction_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewRelatedTransaction.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim objlblCredit As Label = CType(e.Row.FindControl("LblTransactionCredit"), Label)
                Dim objlblDebet As Label = CType(e.Row.FindControl("LblTransactionDebet"), Label)
                If CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("DebitORCredit") = "D" Then
                    objlblDebet.Text = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("TransactionLocalEquivalent"), Decimal).ToString("#,##")
                End If
                If CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("DebitORCredit") = "C" Then
                    objlblCredit.Text = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("TransactionLocalEquivalent"), Decimal).ToString("#,##")
                End If

                'Dim objlblCounterAccount As Label = CType(e.Row.FindControl("LblCounterAccount"), Label)
                'Dim objLblCounterAccountName As Label = CType(e.Row.FindControl("LblCounterAccountName"), Label)
                Dim strTiketno As String = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("TransactionTicketNo"), String).Trim
                Dim strDebetCredit As String = CType(CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("DebitORCredit"), String)
                Dim transdate As Date = CType(e.Row.DataItem, System.Data.DataRowView).Row.Item("TransactionDate")

                'objlblCounterAccount.Text = DataRepository.Provider.ExecuteScalar(Sahassa.AML.Commonly.GetSQLCommand("select dbo.ufn_GetAccountLawan('" & strTiketno & "','" & strDebetCredit & "','" & transdate.ToString("yyyy-MM-dd") & "') ")).ToString
                'objLblCounterAccountName.Text = DataRepository.Provider.ExecuteScalar(Sahassa.AML.Commonly.GetSQLCommand("select dbo.ufn_GetAccountNameByAccountNo('" & strTiketno & "','" & strDebetCredit & "','" & transdate.ToString("yyyy-MM-dd") & "') ")).ToString
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub CboProposedAction_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CboProposedAction.SelectedIndexChanged
        Try
            If CboProposedAction.SelectedValue = "5" Then
                FillAccountOwner()
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub lblciftipology_Click(sender As Object, e As System.EventArgs) Handles lblciftipology.Click
        Try
            Response.Redirect("CustomerInformationdetail.aspx?cifno=" & lblciftipology.Text)
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub AccountNotypology_Click(sender As Object, e As System.EventArgs) Handles AccountNotypology.Click
        Try
            Response.Redirect("AccountInformationdetail.aspx?AccountNo=" & AccountNotypology.Text)
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub GrdPenghasilan_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GrdPenghasilan.PageIndexChanging
        Try

            GrdPenghasilan.PageIndex = e.NewPageIndex
            BindPenghasilan()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub GrdEmployeeHistory_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GrdEmployeeHistory.PageIndexChanging
        Try
            GrdEmployeeHistory.PageIndex = e.NewPageIndex
            BindEmployeeHistory()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub GridViewSameCase_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridViewSameCase.PageIndexChanging
        Try
            GridViewSameCase.PageIndex = e.NewPageIndex
            GridViewSameCase.DataSource = objTListCaseManagementSameCase
            GridViewSameCase.DataBind()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class