﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CDDLNPPrintMasterPage.master" AutoEventWireup="false"
    CodeFile="CDDLNPHistory.aspx.vb" Inherits="CDDLNPHistory" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td bgcolor="#ffffff">
                <asp:DataGrid ID="GridViewCDDLNP" runat="server" AutoGenerateColumns="False" Font-Size="XX-Small"
                    BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None" Width="100%"
                    GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE" ForeColor="Black">
                    <FooterStyle BackColor="#CCCC99"></FooterStyle>
                    <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
                    <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                    <ItemStyle BackColor="#F7F7DE"></ItemStyle>
                    <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B"></HeaderStyle>
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Visible="False" />
                    <Columns>
                        <%--<asp:BoundColumn DataField="PK_CDDLNP_ID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" SortExpression="CreatedDate  desc"
                            DataFormatString="{0:dd-MMM-yyyy}" ItemStyle-Width="15%"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CIF" HeaderText="CIF" SortExpression="CIF  asc" ItemStyle-Width="15%" />
                        <asp:BoundColumn DataField="Nama" HeaderText="Nama" SortExpression="Nama  asc" ItemStyle-Width="30%" />
                        <asp:BoundColumn DataField="CDD_Type" HeaderText="CDD_Type" SortExpression="CDD_Type  asc"
                            ItemStyle-Width="10%" />
                        <asp:BoundColumn DataField="CDD_NasabahType_ID" HeaderText="CDD_NasabahType_ID" SortExpression="CDD_NasabahType_ID  asc"
                            ItemStyle-Width="10%" />
                        <asp:BoundColumn DataField="AMLRiskRatingPersonal" HeaderText="AML Risk Rating Personal"
                            SortExpression="AMLRiskRatingPersonal  asc" ItemStyle-Width="5%" />
                        <asp:BoundColumn DataField="AMLRiskRatingGabungan" HeaderText="AML Risk Rating Gabungan"
                            SortExpression="AMLRiskRatingGabungan  asc" ItemStyle-Width="5%" />
                        <asp:BoundColumn DataField="IsPEP" HeaderText="Is PEP" SortExpression="IsPEP  asc"
                            ItemStyle-Width="5%" />--%>
                        <%--0--%>
                        <asp:TemplateColumn Visible="false">
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                            </ItemTemplate>
                            <HeaderStyle Width="2%" />
                        </asp:TemplateColumn>
                        <%--1--%>
                        <asp:BoundColumn DataField="PK_CDDLNP_ID" Visible="false" />
                        <%--2--%>
                        <asp:BoundColumn HeaderText="No" HeaderStyle-ForeColor="White" ItemStyle-Width="3%"
                            Visible="false" />
                        <%--3--%>
                        <asp:BoundColumn DataField="FK_CDD_Type_ID" Visible="false" />
                        <%--4--%>
                        <asp:BoundColumn DataField="CDD_Type" HeaderText="CDD Type" SortExpression="CDD_Type  desc"
                            ItemStyle-Width="9%" Visible="false" />
                        <%--5--%>
                        <asp:BoundColumn DataField="FK_CDD_NasabahType_ID" Visible="false" />
                        <%--6--%>
                        <asp:BoundColumn DataField="CDD_NasabahType_ID" HeaderText="Nasabah Type" SortExpression="CDD_NasabahType_ID  desc"
                            ItemStyle-Width="8%" Visible="false" />
                        <%--7--%>
                        <asp:BoundColumn DataField="Nama" HeaderText="Customer Name" SortExpression="Nama  desc"
                            ItemStyle-Width="10%" />
                        <%--8--%>
                        <asp:BoundColumn DataField="CIF" HeaderText="CIF" SortExpression="CIF  desc" ItemStyle-Width="6%" />
                        <%--9--%>
                        <asp:BoundColumn DataField="AMLRiskRatingPersonal" HeaderText="AML/CFT Risk Rating Personal"
                            SortExpression="AMLRiskRatingPersonal  desc" ItemStyle-Width="5%" Visible="false" />
                        <%--10--%>
                        <asp:BoundColumn DataField="AMLRiskRatingGabungan" HeaderText="AML/CFT Risk Rating Gabungan"
                            SortExpression="AMLRiskRatingGabungan  desc" ItemStyle-Width="5%" Visible="false" />
                        <%--11--%>
                        <asp:BoundColumn DataField="IsPEP" HeaderText="Is PEP" SortExpression="IsPEP  desc"
                            ItemStyle-Width="5%" Visible="false" />
                        <%--12--%>
                        <asp:BoundColumn DataField="ParentBO" HeaderText="Parent BO" SortExpression="ParentBO  desc"
                            ItemStyle-Width="10%" Visible="false" />
                        <%--13--%>
                        <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" SortExpression="CreatedDate  desc"
                            DataFormatString="{0:dd-MMM-yyyy}" ItemStyle-Width="7%" />
                        <%--14--%>
                        <asp:BoundColumn DataField="CreatedBy" HeaderText="Created By" SortExpression="CreatedBy  desc"
                            ItemStyle-Width="6%" Visible="false" />
                        <%--15--%>
                        <asp:BoundColumn DataField="LastWorkflowGroup" HeaderText="Last Approval Group" SortExpression="LastWorkflowGroup  desc"
                            ItemStyle-Width="8%" Visible="false" />
                        <%--16--%>
                        <asp:BoundColumn DataField="LastApprovalStatus" Visible="false" />
                        <%--17--%>
                        <asp:BoundColumn DataField="ApprovalStatus" HeaderText="Status" SortExpression="ApprovalStatus  desc"
                            ItemStyle-Width="8%" Visible="false" />
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton ID="LnkDetail" runat="server" CausesValidation="false" CommandName="Detail"
                                    Text="Detail"></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle Width="5%" />
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
