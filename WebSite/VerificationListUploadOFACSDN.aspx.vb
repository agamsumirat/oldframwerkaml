Imports System.IO
Imports System.Data.SqlClient
Partial Class VerificationListUploadOFACSDN
    Inherits Parent

    Private DsSDN As New Data.DataSet
    Private DsALT As New Data.DataSet
    Private DsADD As New Data.DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextDateofData.ClientID & "'), 'dd-mmm-yyyy')")

                Me.TextDateofData.Text = String.Format("{0:dd-MMM-yyyy}", Now)

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    'Private Function CreateDataTableSDN() As Data.DataTable
    '    Dim SDNDataTable As Data.DataTable = New Data.DataTable()

    '    Dim SDNDataColumn As Data.DataColumn

    '    SDNDataColumn = New Data.DataColumn()
    '    SDNDataColumn.DataType = Type.GetType("System.String")
    '    SDNDataColumn.ColumnName = "ent_num"
    '    SDNDataTable.Columns.Add(SDNDataColumn)

    '    SDNDataColumn = New Data.DataColumn()
    '    SDNDataColumn.DataType = Type.GetType("System.String")
    '    SDNDataColumn.ColumnName = "SDN_Name"
    '    SDNDataTable.Columns.Add(SDNDataColumn)

    '    SDNDataColumn = New Data.DataColumn()
    '    SDNDataColumn.DataType = Type.GetType("System.String")
    '    SDNDataColumn.ColumnName = "SDN_Type"
    '    SDNDataTable.Columns.Add(SDNDataColumn)

    '    SDNDataColumn = New Data.DataColumn()
    '    SDNDataColumn.DataType = Type.GetType("System.String")
    '    SDNDataColumn.ColumnName = "Program"
    '    SDNDataTable.Columns.Add(SDNDataColumn)
    '    SDNDataColumn = New Data.DataColumn()

    '    SDNDataColumn.DataType = Type.GetType("System.String")
    '    SDNDataColumn.ColumnName = "Title"
    '    SDNDataTable.Columns.Add(SDNDataColumn)

    '    SDNDataColumn = New Data.DataColumn()
    '    SDNDataColumn.DataType = Type.GetType("System.String")
    '    SDNDataColumn.ColumnName = "Call_Sign"
    '    SDNDataTable.Columns.Add(SDNDataColumn)

    '    SDNDataColumn = New Data.DataColumn()
    '    SDNDataColumn.DataType = Type.GetType("System.String")
    '    SDNDataColumn.ColumnName = "Vess_type"
    '    SDNDataTable.Columns.Add(SDNDataColumn)

    '    SDNDataColumn = New Data.DataColumn()
    '    SDNDataColumn.DataType = Type.GetType("System.String")
    '    SDNDataColumn.ColumnName = "Tonnage"
    '    SDNDataTable.Columns.Add(SDNDataColumn)

    '    SDNDataColumn = New Data.DataColumn()
    '    SDNDataColumn.DataType = Type.GetType("System.String")
    '    SDNDataColumn.ColumnName = "GRT"
    '    SDNDataTable.Columns.Add(SDNDataColumn)

    '    SDNDataColumn = New Data.DataColumn()
    '    SDNDataColumn.DataType = Type.GetType("System.String")
    '    SDNDataColumn.ColumnName = "Vess_flag"
    '    SDNDataTable.Columns.Add(SDNDataColumn)

    '    SDNDataColumn = New Data.DataColumn()
    '    SDNDataColumn.DataType = Type.GetType("System.String")
    '    SDNDataColumn.ColumnName = "Vess_owner"
    '    SDNDataTable.Columns.Add(SDNDataColumn)

    '    SDNDataColumn = New Data.DataColumn()
    '    SDNDataColumn.DataType = Type.GetType("System.String")
    '    SDNDataColumn.ColumnName = "Remarks"
    '    SDNDataTable.Columns.Add(SDNDataColumn)

    '    Return SDNDataTable
    'End Function

    Private Function getStringFieldValue(ByVal objValue As Object) As String
        Dim strValue As String = ""
        Try
            If Not IsDBNull(objValue) Then
                strValue = CStr(objValue)
            End If
            Return strValue
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function SaveSDNCSVDataToSQLServer() As Boolean
        Dim StrSQL As String
        Dim dr As Data.DataRow
        ExecNonQuery("TRUNCATE TABLE SDN")
        For Each dr In DsSDN.Tables(0).Rows
            If getStringFieldValue(dr.Item("ent_num")).Length > 0 Then
                StrSQL = "INSERT INTO [SDN]([ent_num],[SDN_Name],[SDN_Type],[Program],[Title],[Call_Sign],[Vess_type],[Tonnage],[GRT],[Vess_flag],[Vess_owner],[Remarks])"
                StrSQL = StrSQL & " VALUES ("
                StrSQL = StrSQL & " '" & getStringFieldValue(dr.Item("ent_num")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("SDN_Name")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("SDN_Type")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("Program")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("Title")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("Call_Sign")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("Vess_type")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("Tonnage")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("GRT")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("Vess_flag")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("Vess_owner")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("Remarks")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " )"
                ExecNonQuery(StrSQL)
            End If
        Next
        Return True
    End Function

    Private Function SaveALTCSVDataToSQLServer() As Boolean
        Dim StrSQL As String
        Dim dr As Data.DataRow
        ExecNonQuery("TRUNCATE TABLE SDNALT")
        For Each dr In dsALT.Tables(0).Rows
            If getStringFieldValue(dr.Item("ent_num")).Length > 0 Then
                StrSQL = "INSERT INTO [SDNALT]([ent_num],[alt_num],[alt_type],[alt_name],[alt_remarks])"
                StrSQL = StrSQL & " VALUES ("
                StrSQL = StrSQL & " '" & getStringFieldValue(dr.Item("ent_num")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("alt_num")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("alt_type")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("alt_name")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("alt_remarks")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " )"
                ExecNonQuery(StrSQL)
            End If
        Next
        Return True
    End Function

    Private Function SaveADDCSVDataToSQLServer() As Boolean
        Dim StrSQL As String
        Dim dr As Data.DataRow
        ExecNonQuery("TRUNCATE TABLE SDNADD")
        For Each dr In dsADD.Tables(0).Rows
            If getStringFieldValue(dr.Item("ent_num")).Length > 0 Then
                StrSQL = "INSERT INTO [SDNADD]([Ent_num],[Add_num],[Address],[City],[Country],[Add_remarks])"
                StrSQL = StrSQL & " VALUES ("
                StrSQL = StrSQL & " '" & getStringFieldValue(dr.Item("Ent_num")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("Add_num")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("Address")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("City")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("Country")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " ,'" & getStringFieldValue(dr.Item("Add_remarks")).Replace("'", "''") & "'"
                StrSQL = StrSQL & " )"
                ExecNonQuery(StrSQL)
            End If
        Next
        Return True
    End Function

    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(1)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "ALL", "Update", "", "", "Upload OFAC SDN List")
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Function ExecNonQuery(ByVal SQLQuery As String) As Integer
        Dim SqlConn As SqlConnection = Nothing
        Dim SqlCmd As SqlCommand = Nothing
        Try
            SqlConn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("AMLConnectionString").ToString)
            SqlConn.Open()
            SqlCmd = New SqlCommand
            SqlCmd.Connection = SqlConn
            SqlCmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings.Get("SQLCommandTimeout")
            SqlCmd.CommandText = SQLQuery
            Return SqlCmd.ExecuteNonQuery()
        Catch tex As Threading.ThreadAbortException
            Throw tex
        Catch ex As Exception
            Throw ex
        Finally
            If Not SqlConn Is Nothing Then
                SqlConn.Close()
                SqlConn.Dispose()
                SqlConn = Nothing
            End If
            If Not SqlCmd Is Nothing Then
                SqlCmd.Dispose()
                SqlCmd = Nothing
            End If
        End Try
    End Function

    Private Function ExecuteProcessSDNList(ByVal ProcessDate As DateTime, ByVal DateOfData As DateTime) As Boolean
        ExecNonQuery("EXEC usp_ProcessSDNList '" & ProcessDate.ToString("yyyy-MM-dd") & "','" & DateOfData.ToString("yyyy-MM-dd") & "'")
        Return True
    End Function

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim DateOfData As DateTime
        'Dim StrSQL As String
        Try
            If Me.FileUploadSDN.PostedFile.ContentLength <> 0 And Me.FileUploadSDN.HasFile _
            And Me.FileUploadALT.PostedFile.ContentLength <> 0 And Me.FileUploadALT.HasFile _
            And Me.FileUploadADD.PostedFile.ContentLength <> 0 And Me.FileUploadADD.HasFile Then
                Dim destDir As String = Server.MapPath("Upload")
                Dim fName As String
                Dim destPath As String
                Dim sConnectionString As String
                ' SDN
                fName = Path.GetFileName(Me.FileUploadSDN.PostedFile.FileName)
                destPath = Path.Combine(destDir, fName)
                Me.FileUploadSDN.PostedFile.SaveAs(destPath)

                sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=Text;Data Source=" & destDir
                Dim objConnSDN As New Data.OleDb.OleDbConnection(sConnectionString)
                objConnSDN.Open()
                Dim objCmdSelectSDN As New Data.OleDb.OleDbCommand("SELECT * FROM [" & fName & "]", objConnSDN)
                Dim objAdapterSDN As New Data.OleDb.OleDbDataAdapter
                objAdapterSDN.SelectCommand = objCmdSelectSDN
                objAdapterSDN.Fill(DsSDN)
                objConnSDN.Close()
                SaveSDNCSVDataToSQLServer()

                ' ALT
                fName = Path.GetFileName(Me.FileUploadALT.PostedFile.FileName)
                destPath = Path.Combine(destDir, fName)
                Me.FileUploadALT.PostedFile.SaveAs(destPath)
                sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=Text;Data Source=" & destDir
                Dim objConnSDNALT As New Data.OleDb.OleDbConnection(sConnectionString)
                objConnSDNALT.Open()
                Dim objCmdSelectSDNALT As New Data.OleDb.OleDbCommand("SELECT * FROM [" & fName & "]", objConnSDNALT)
                Dim objAdapterSDNALT As New Data.OleDb.OleDbDataAdapter
                objAdapterSDNALT.SelectCommand = objCmdSelectSDNALT
                objAdapterSDNALT.Fill(DsALT)
                objConnSDNALT.Close()
                SaveALTCSVDataToSQLServer()


                'ExecNonQuery("TRUNCATE TABLE SDNALT")
                'StrSQL = "INSERT INTO [SDNALT]([ent_num],[alt_num],[alt_type],[alt_name],[alt_remarks])"
                'StrSQL = StrSQL & " Select [ent_num],[alt_num],[alt_type],[alt_name],[alt_remarks] FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0','TEXT;Database=" & destDir & ";HDR=YES',"
                'StrSQL = StrSQL & "'SELECT [ent_num],[alt_num],[alt_type],[alt_name],[alt_remarks] FROM [" & fName & "]')"
                'ExecNonQuery(StrSQL)

                ' ADD
                fName = Path.GetFileName(Me.FileUploadADD.PostedFile.FileName)
                destPath = Path.Combine(destDir, fName)
                Me.FileUploadADD.PostedFile.SaveAs(destPath)
                sConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=Text;Data Source=" & destDir
                Dim objConnSDNADD As New Data.OleDb.OleDbConnection(sConnectionString)
                objConnSDNADD.Open()
                Dim objCmdSelectSDNADD As New Data.OleDb.OleDbCommand("SELECT * FROM [" & fName & "]", objConnSDNADD)
                Dim objAdapterSDNADD As New Data.OleDb.OleDbDataAdapter
                objAdapterSDNADD.SelectCommand = objCmdSelectSDNADD
                objAdapterSDNADD.Fill(DsADD)
                objConnSDNADD.Close()
                SaveADDCSVDataToSQLServer()


                'ExecNonQuery("TRUNCATE TABLE SDNADD")
                'StrSQL = "INSERT INTO [SDNADD]([Ent_num],[Add_num],[Address],[City],[Country],[Add_remarks])"
                'StrSQL = StrSQL & " Select [Ent_num],[Add_num],[Address],[City],[Country],[Add_remarks] FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0','TEXT;Database=" & destDir & ";HDR=YES',"
                'StrSQL = StrSQL & "'SELECT [Ent_num],[Add_num],[Address],[City],[Country],[Add_remarks] FROM [" & fName & "]')"
                'ExecNonQuery(StrSQL)

                Try
                    DateOfData = DateTime.Parse(Me.TextDateofData.Text)
                Catch ex As Exception
                    DateOfData = DateTime.Now()
                End Try
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    ' INSERT TO Audit Trail
                    InsertAuditTrail()
                    ' Update Verification List
                    ExecuteProcessSDNList(Now(), DateOfData)
                    Me.LblSuccess.Visible = True
                    Me.LblSuccess.Text = "Success to Update Verification List for Category OFAC SDN."
                Else
                    Using SDNPendingApprovalAdapter As New AMLDAL.OFACSDNListTableAdapters.SDN_PendingApprovalTableAdapter
                        SDNPendingApprovalAdapter.Insert(Sahassa.AML.Commonly.SessionUserId, Now, "Verification List OFAC SDN", 2, DateOfData)
                    End Using
                    Dim MessagePendingID As Integer = 8864 'MessagePendingID 8864 = OFAC SDN List
                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID, False)
                End If
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("Default.aspx", False)
    End Sub

    'Protected Sub ImageButtonSDNView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSDNView.Click
    '    Try
    '        If Me.FileUploadSDN.PostedFile.ContentLength <> 0 Then
    '            If Me.FileUploadSDN.HasFile Then
    '                Dim destDir As String = Server.MapPath("./Upload")
    '                Dim fName As String = Path.GetFileName(Me.FileUploadSDN.PostedFile.FileName)
    '                Dim destPath As String = Path.Combine(destDir, fName)
    '                Me.FileUploadSDN.PostedFile.SaveAs(destpath)

    '                Dim fi As New IO.FileInfo(destPath)
    '                Dim sConnectionString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=Text;Data Source=" & fi.DirectoryName
    '                Dim objConn As New Data.OleDb.OleDbConnection(sConnectionString)
    '                objConn.Open()
    '                Dim objCmdSelect As New Data.OleDb.OleDbCommand("SELECT * FROM [" & fi.Name & "]", objConn)
    '                Dim objAdapter1 As New Data.OleDb.OleDbDataAdapter

    '                'Dim dssdn As Data.DataSet = CType(Session("dsSDN"), Data.DataSet)

    '                objAdapter1.SelectCommand = objCmdSelect
    '                'If Not dssdn Is Nothing Then
    '                '    dssdn.Reset()
    '                'End If

    '                'Dim dtSDN As New Data.DataTable("SDN")
    '                'dtSDN = Me.CreateDataTableSDN

    '                'dssdn.Tables.Add(dtSDN)

    '                objAdapter1.Fill(dssdn)
    '                Me.GridSDNSection.DataSource = dssdn.Tables(0)
    '                Me.GridSDNSection.DataBind()


    '                If dssdn.Tables(0).Rows.Count > 0 Then
    '                    Me.GridSDNSectionRow.Visible = True
    '                Else
    '                    Me.GridSDNSectionRow.Visible = False
    '                End If
    '                objConn.Close()
    '                Session("DsSDN") = DsSDN
    '            Else
    '                Throw New Exception("Choose an SDN.CSV file first.")
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Me.cvalPageError.IsValid = False
    '        Me.cvalPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    'Protected Sub ImageButtonALTView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonALTView.Click
    '    Try
    '        If Me.FileUploadALT.PostedFile.ContentLength <> 0 Then
    '            If Me.FileUploadALT.HasFile Then
    '                Dim destDir As String = Server.MapPath("./Upload")
    '                Dim fName As String = Path.GetFileName(Me.FileUploadALT.PostedFile.FileName)
    '                Dim destPath As String = Path.Combine(destDir, fName)
    '                Me.FileUploadALT.PostedFile.SaveAs(destPath)

    '                Dim fi As New IO.FileInfo(destPath)
    '                Dim sConnectionString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=Text;Data Source=" & fi.DirectoryName
    '                Dim objConn As New Data.OleDb.OleDbConnection(sConnectionString)
    '                objConn.Open()
    '                Dim objCmdSelect As New Data.OleDb.OleDbCommand("SELECT * FROM [" & fi.Name & "]", objConn)
    '                Dim objAdapter1 As New Data.OleDb.OleDbDataAdapter


    '                objAdapter1.SelectCommand = objCmdSelect


    '                objAdapter1.Fill(DsALT)
    '                Me.GridALTSection.DataSource = DsALT.Tables(0)
    '                Me.GridALTSection.DataBind()


    '                If DsALT.Tables(0).Rows.Count > 0 Then
    '                    Me.GridALTSectionRow.Visible = True
    '                Else
    '                    Me.GridALTSectionRow.Visible = False
    '                End If
    '                objConn.Close()
    '                Session("DsALT") = DsALT

    '            Else
    '                Throw New Exception("Choose an ALT.CSV file first.")
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Me.cvalPageError.IsValid = False
    '        Me.cvalPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    'Protected Sub ImageButtonADDView_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonADDView.Click
    '    Try
    '        If Me.FileUploadADD.PostedFile.ContentLength <> 0 Then
    '            If Me.FileUploadADD.HasFile Then
    '                Dim destDir As String = Server.MapPath("./Upload")
    '                Dim fName As String = Path.GetFileName(Me.FileUploadADD.PostedFile.FileName)
    '                Dim destPath As String = Path.Combine(destDir, fName)
    '                Me.FileUploadADD.PostedFile.SaveAs(destPath)

    '                Dim fi As New IO.FileInfo(destPath)
    '                Dim sConnectionString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=Text;Data Source=" & fi.DirectoryName
    '                Dim objConn As New Data.OleDb.OleDbConnection(sConnectionString)
    '                objConn.Open()
    '                Dim objCmdSelect As New Data.OleDb.OleDbCommand("SELECT * FROM [" & fi.Name & "]", objConn)
    '                Dim objAdapter1 As New Data.OleDb.OleDbDataAdapter


    '                objAdapter1.SelectCommand = objCmdSelect


    '                objAdapter1.Fill(DsADD)
    '                Me.GridADDSection.DataSource = DsADD.Tables(0)
    '                Me.GridADDSection.DataBind()


    '                If DsADD.Tables(0).Rows.Count > 0 Then
    '                    Me.GridADDSectionRow.Visible = True
    '                Else
    '                    Me.GridADDSectionRow.Visible = False
    '                End If
    '                objConn.Close()
    '                Session("DsADD") = DsADD

    '            Else
    '                Throw New Exception("Choose an ADD.CSV file first.")
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Me.cvalPageError.IsValid = False
    '        Me.cvalPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub
End Class