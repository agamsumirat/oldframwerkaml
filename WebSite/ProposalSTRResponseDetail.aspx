<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="ProposalSTRResponseDetail.aspx.vb" Inherits="ProposalSTRResponseDetail" title="Proposal STR Response Detail" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<ajax:AjaxPanel ID="PanelAccountInformationDetail" runat="server" Width="749px">
        <table id="TABLE1" bgcolor="#dddddd" border="2" bordercolor="#fffff" cellpadding="0"
            cellspacing="0" width="99%">
            <tr>
                <td style="width: 5px; ">
                </td>
                <td style="width: 466px;  background-color: #ffffff" valign="top">
                    <table width="100%">
                        <tr>
                            <td style="width: 1%">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/dot_title.gif" /></td>
                            <td style="width: 100px">
                                <strong style="font-size: medium; width: 50%">
                                    <asp:Label ID="Label1" runat="server" CssClass="MainTitle" Text="STR Proposal"
                                        Width="400px"></asp:Label></strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 5px">
                </td>
                <td style="background-color: #ffffff; width: 466px;" valign="top">
                    <table cellpadding="0" cellspacing="0" width="99%">
                        <tr>
                            <td style="width: 676px">
                                &nbsp;<table class="tabContents">
                                    <tr>
                                        <td style="width: 714px">
                                            
                                                    <table class="TabArea" width="100%">
                                                        <tr id="Tr14" runat="server">
                                                        </tr>
                                                        <tr id="Tr15" runat="server">
                                                            <td id="TdDataGeneralAvailable" runat="server" style="width: 315px">
                                                                <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="1" cellspacing="1" style="width: 208%">
                                                                    
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                                                            I. Kasus Posisi</td>
                                                                    </tr> 
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                                                            &nbsp;<asp:TextBox ID="TextKasusPosisi" runat="server" Width="500px" CssClass="searcheditbox" Height="50px" TextMode="MultiLine" Enabled="False"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextKasusPosisi"
                                                                                Display="Dynamic" ErrorMessage="Please Fill Kasus Polisi">*</asp:RequiredFieldValidator></td>
                                                                    </tr>

                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                                                            II. Indikator Mencurigakan</td>
                                                                    </tr> 
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                                                            &nbsp;<asp:TextBox ID="TextIndikatorMencurigakan" runat="server" Width="500px" CssClass="searcheditbox" Height="50px" TextMode="MultiLine" Enabled="False"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextIndikatorMencurigakan"
                                                                                Display="Dynamic" ErrorMessage="Please Fill Indicator Mencurigakan">*</asp:RequiredFieldValidator></td>
                                                                    </tr>

                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                                                            III. Unsur Transaksi Keuangan Mencurigakan</td>
                                                                    </tr> 
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                                                            &nbsp;<asp:TextBox ID="TextUnsurTKM" runat="server" Width="500px" CssClass="searcheditbox" Height="50px" TextMode="MultiLine" Enabled="False"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextUnsurTKM"
                                                                                Display="Dynamic" ErrorMessage="Please Fill Unsur Transaksi Keuangan Mencurigakan">*</asp:RequiredFieldValidator></td>
                                                                    </tr>

                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                                                            IV. Lain-lain</td>
                                                                    </tr> 
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                                                            &nbsp;<asp:TextBox ID="TextLainLain" runat="server" Width="500px" CssClass="searcheditbox" Height="50px" TextMode="MultiLine" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>

                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                                                            V. Kesimpulan</td>
                                                                    </tr> 
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                                                            &nbsp;<asp:TextBox ID="TextKesimpulan" runat="server" Width="500px" CssClass="searcheditbox" Height="50px" TextMode="MultiLine" Enabled="False"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextKesimpulan"
                                                                                Display="Dynamic" ErrorMessage="Please fill kesimpulan">*</asp:RequiredFieldValidator></td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                                                            Response :
                                                                            <asp:DropDownList ID="cboProposalSTRResponse" runat="server" CssClass="combobox">
                                                                            </asp:DropDownList></td>
                                                                    </tr>

                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                                                            &nbsp;Comment</td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                                                            <asp:TextBox ID="TextBoxComment" runat="server" CssClass="searcheditbox"
                                                                                Height="50px" TextMode="MultiLine" Width="500px"></asp:TextBox></td>
                                                                    </tr>

                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                             
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td style="width: 466px">
                    &nbsp;<asp:ImageButton ID="ImageSave" runat="server" SkinID="SaveButton" />
                    <asp:ImageButton ID="ImageButtonBack" runat="server" SkinID="BackButton" OnClientClick="javascript:history.back()"/>
                    <asp:CustomValidator ID="CValPageError" runat="server" Display="None"></asp:CustomValidator><br />
                </td>
            </tr>
        </table>
        
    </ajax:AjaxPanel>
</asp:Content>

