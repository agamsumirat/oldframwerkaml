Imports System.Data.SqlClient
Partial Class VerificationListUploadOFACSDNApproval
    Inherits Parent

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Using OFACSDNPendingApprovalAdapter As New AMLDAL.OFACSDNListTableAdapters.SDN_PendingApprovalTableAdapter
                    Dim OFACSDNPendingApprovalTable As New AMLDAL.OFACSDNList.SDN_PendingApprovalDataTable
                    OFACSDNPendingApprovalTable = OFACSDNPendingApprovalAdapter.GetData()
                    If OFACSDNPendingApprovalTable.Rows.Count > 0 Then
                        Me.TableUploadData.Visible = True
                        Me.TableNoPendingApproval.Visible = False
                        Me.ImageAccept.Visible = True
                        Me.ImageReject.Visible = True
                        Me.ImageBack.Visible = True
                        Using OFACSDNAdapter As New AMLDAL.OFACSDNListTableAdapters.SDNTableAdapter
                            Me.GridPendingApprovalSDN.DataSource = OFACSDNAdapter.GetData
                            Me.GridPendingApprovalSDN.DataBind()
                        End Using
                        Using OFACSDNALTAdapter As New AMLDAL.OFACSDNListTableAdapters.SDNALTTableAdapter
                            Me.GridPendingApprovalSDNALT.DataSource = OFACSDNALTAdapter.GetData
                            Me.GridPendingApprovalSDNALT.DataBind()
                        End Using
                        Using OFACSDNADDAdapter As New AMLDAL.OFACSDNListTableAdapters.SDNADDTableAdapter
                            Me.GridPendingApprovalSDNADD.DataSource = OFACSDNADDAdapter.GetData
                            Me.GridPendingApprovalSDNADD.DataBind()
                        End Using
                    Else

                        Me.TableUploadData.Visible = False
                        Me.TableNoPendingApproval.Visible = True
                        Me.ImageAccept.Visible = False
                        Me.ImageReject.Visible = False
                        Me.ImageBack.Visible = True
                    End If
                End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Private Function ExecNonQuery(ByVal SQLQuery As String) As Integer
        Dim SqlConn As SqlConnection = Nothing
        Dim SqlCmd As SqlCommand = Nothing
        Try
            SqlConn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("AMLConnectionString").ToString)
            SqlConn.Open()
            SqlCmd = New SqlCommand
            SqlCmd.Connection = SqlConn
            SqlCmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings.Get("SQLCommandTimeout")
            SqlCmd.CommandText = SQLQuery
            Return SqlCmd.ExecuteNonQuery()
        Catch tex As Threading.ThreadAbortException
            Throw tex
        Catch ex As Exception
            Throw ex
        Finally
            If Not SqlConn Is Nothing Then
                SqlConn.Close()
                SqlConn.Dispose()
                SqlConn = Nothing
            End If
            If Not SqlCmd Is Nothing Then
                SqlCmd.Dispose()
                SqlCmd = Nothing
            End If
        End Try
    End Function

    Private Function ExecuteProcessSDNList(ByVal ProcessDate As DateTime, ByVal DateOfData As DateTime) As Boolean
        ExecNonQuery("EXEC usp_ProcessSDNList '" & ProcessDate.ToString("yyyy-MM-dd") & "','" & DateOfData.ToString("yyyy-MM-dd") & "'")
        Return True
    End Function

    Private Sub InsertAuditTrail(ByVal ApprovalStatusDescription As String, userpreparer As String)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(1)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter

                AccessAudit.Insert(Now, userpreparer, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "ALL", "Update", "", "", "Upload OFAC SDN List Approval - " & ApprovalStatusDescription)
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Using OFACSDNPendingApprovalAdapter As New AMLDAL.OFACSDNListTableAdapters.SDN_PendingApprovalTableAdapter
                Dim OFACSDNPendingApprovalTable As New AMLDAL.OFACSDNList.SDN_PendingApprovalDataTable
                Dim OFACSDNPendingApprovalTableRow As AMLDAL.OFACSDNList.SDN_PendingApprovalRow
                OFACSDNPendingApprovalTable = OFACSDNPendingApprovalAdapter.GetData()

                If OFACSDNPendingApprovalTable.Rows.Count > 0 Then
                    OFACSDNPendingApprovalTableRow = OFACSDNPendingApprovalTable.Rows(0)
                    ' update sdn list
                    ExecuteProcessSDNList(OFACSDNPendingApprovalTableRow.SDN_PendingApprovalEntryDate, OFACSDNPendingApprovalTableRow.SDN_PendingApprovalDateOfData)


                    InsertAuditTrail("Accepted", OFACSDNPendingApprovalTableRow.SDN_PendingApprovalUserID)

                    ' delete from approval
                    OFACSDNPendingApprovalAdapter.DeleteAllPendingApproval()
                    ' insert to audit trail

                    ' clear table
                    ExecNonQuery("TRUNCATE TABLE SDN")
                    ExecNonQuery("TRUNCATE TABLE SDNALT")
                    ExecNonQuery("TRUNCATE TABLE SDNADD")
                End If
            End Using
            Dim MessagePendingID As Integer = 8867 'MessagePendingID 8867 = OFAC SDN List Approval approved saved successfully
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID, False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Using OFACSDNPendingApprovalAdapter As New AMLDAL.OFACSDNListTableAdapters.SDN_PendingApprovalTableAdapter
                Dim OFACSDNPendingApprovalTable As New AMLDAL.OFACSDNList.SDN_PendingApprovalDataTable
                OFACSDNPendingApprovalTable = OFACSDNPendingApprovalAdapter.GetData()
                If OFACSDNPendingApprovalTable.Rows.Count > 0 Then


                    InsertAuditTrail("Rejected", OFACSDNPendingApprovalTable.Rows(0).Item("SDN_PendingApprovalUserID").ToString)

                    ' delete from approval
                    OFACSDNPendingApprovalAdapter.DeleteAllPendingApproval()
                    ' insert to audit trail

                    ' clear table
                    ExecNonQuery("TRUNCATE TABLE SDN")
                    ExecNonQuery("TRUNCATE TABLE SDNALT")
                    ExecNonQuery("TRUNCATE TABLE SDNADD")
                End If
            End Using
            Dim MessagePendingID As Integer = 8868 'MessagePendingID 8868 = OFAC SDN List Approval rejected saved successfully
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID, False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Me.Response.Redirect("Default.aspx", False)
    End Sub
End Class
