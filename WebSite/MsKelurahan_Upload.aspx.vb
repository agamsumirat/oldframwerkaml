#Region "Imports..."
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Excel
Imports Sahassa.AML.Commonly
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports AMLBLL.DataType
#End Region

Partial Class MsKelurahan_upload
    Inherits Parent

#Region "Structure..."

    Structure GrouP_MsKelurahan_success
        Dim MsKelurahan As MsKelurahan_ApprovalDetail
        Dim L_MsKelurahanNCBS As TList(Of MsKelurahanNCBS_ApprovalDetail)

    End Structure

#End Region

#Region "Property umum"
    Private Property GroupTableSuccessData() As List(Of GrouP_MsKelurahan_success)
        Get
            Return Session("GroupTableSuccessData")
        End Get
        Set(ByVal value As List(Of GrouP_MsKelurahan_success))
            Session("GroupTableSuccessData") = value
        End Set
    End Property

    Private Property GroupTableAnomalyData() As List(Of AMLBLL.MsKelurahanBll.DT_Group_MsKelurahan)
        Get
            Return Session("GroupTableAnomalyData")
        End Get
        Set(ByVal value As List(Of AMLBLL.MsKelurahanBll.DT_Group_MsKelurahan))
            Session("GroupTableAnomalyData") = value
        End Set
    End Property

    Public ReadOnly Property GET_PKUserID() As Integer
        Get
            Return SessionPkUserId
        End Get
    End Property

    Public ReadOnly Property GET_UserName() As String
        Get
            'Return SessionNIK
            Return SessionUserId
        End Get
    End Property

    Private Property SetnGetDataTableForSuccessData() As TList(Of MsKelurahan_ApprovalDetail)
        Get
            Return CType(IIf(Session("MsKelurahan_upload.TableSuccessUpload") Is Nothing, Nothing, Session("MsKelurahan_upload.TableSuccessUpload")), TList(Of MsKelurahan_ApprovalDetail))
        End Get
        Set(ByVal value As TList(Of MsKelurahan_ApprovalDetail))
            Session("MsKelurahan_upload.TableSuccessUpload") = value
        End Set
    End Property

    Private Property SetnGetDataTableForAnomalyData() As Data.DataTable
        Get
            Return CType(IIf(Session("MsKelurahan_upload.TableAnomalyUpload") Is Nothing, Nothing, Session("MsKelurahan_upload.TableAnomalyUpload")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsKelurahan_upload.TableAnomalyUpload") = value
        End Set
    End Property

    Private Property DSexcelTemp() As Data.DataSet
        Get
            Return CType(IIf(Session("MsKelurahan_upload.DSexcelTemp") Is Nothing, Nothing, Session("MsKelurahan_upload.DSexcelTemp")), DataSet)
        End Get
        Set(ByVal value As Data.DataSet)
            Session("MsKelurahan_upload.DSexcelTemp") = value
        End Set
    End Property

#End Region

#Region "Property Tabel Temporary"
    Private Property DT_AllMsKelurahan() As Data.DataTable
        Get
            Return CType(IIf(Session("MsKelurahan_upload.DT_MsKelurahan") Is Nothing, Nothing, Session("MsKelurahan_upload.DT_MsKelurahan")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsKelurahan_upload.DT_MsKelurahan") = value
        End Set
    End Property

    Private Property DT_AllMsKelurahanNCBS() As Data.DataTable
        Get
            Return CType(IIf(Session("MsKelurahan_upload.DT_MsKelurahanNCBS") Is Nothing, Nothing, Session("MsKelurahan_upload.DT_MsKelurahanNCBS")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsKelurahan_upload.DT_MsKelurahanNCBS") = value
        End Set
    End Property

#End Region

#Region "Property Table Sukses Data"

    Private Property TableSuccessMsKelurahan() As TList(Of MsKelurahan_ApprovalDetail)
        Get
            Return Session("TableSuccessMsKelurahan")
        End Get
        Set(ByVal value As TList(Of MsKelurahan_ApprovalDetail))
            Session("TableSuccessMsKelurahan") = value
        End Set
    End Property

    Private Property TableSuccessMsKelurahanNCBS() As TList(Of MsKelurahanNCBS)
        Get
            Return Session("TableSuccessMsKelurahanNCBS")
        End Get
        Set(ByVal value As TList(Of MsKelurahanNCBS))
            Session("TableSuccessMsKelurahanNCBS") = value
        End Set
    End Property


#End Region

#Region "Property Table Anomaly Data"

    Private Property TableAnomalyMsKelurahan() As Data.DataTable
        Get
            Return Session("TableAnomalyMsKelurahan")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsKelurahan") = value
        End Set
    End Property

    Private Property TableAnomalyMsKelurahanNCBS() As Data.DataTable
        Get
            Return Session("TableAnomalyMsKelurahanNCBS")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsKelurahanNCBS") = value
        End Set
    End Property

#End Region

#Region "validasi.."

    Private Function ValidateInput(ByVal MsKelurahan As AMLBLL.MsKelurahanBll.DT_Group_MsKelurahan) As List(Of String)
        Dim temp As New List(Of String)
        Dim msgError As New StringBuilder
        Dim errorline As New StringBuilder
        Try
            'validasi
            ValidateMsKelurahan(MsKelurahan.MsKelurahan, msgError, errorline)
            If msgError.ToString = "" Then
                For Each ci As DataRow In MsKelurahan.L_MsKelurahanNCBS.Rows
                    'validasi NCBS
                    ValidateMsKelurahanNCBS(ci, msgError, errorline)
                Next
            End If
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        Catch ex As Exception
            msgError.Append(ex.Message)
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        End Try
        Return temp
    End Function

    Private Shared Sub ValidateMsKelurahanNCBS(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorLine As StringBuilder)
        Try

            '======================== Validasi textbox :  IDKelurahanNCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("IDKelurahanBank")) = False Then
                msgError.AppendLine(" &bull; IDKelurahanBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsKelurahanBank : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  NamaKelurahanNCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("NamaKelurahanBank")) = False Then
                msgError.AppendLine(" &bull; NamaKelurahanBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsKelurahanBank : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorLine.AppendLine(" &bull;MsKelurahanBank : Line " & DTR("NO") & "<BR/>")
        End Try
    End Sub

    Private Shared Sub ValidateMsKelurahan(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorline As StringBuilder)
        Try

            Using checkBlocking As TList(Of MsKelurahan_ApprovalDetail) = DataRepository.MsKelurahan_ApprovalDetailProvider.GetPaged("IDKelurahan=" & DTR("IDKelurahan"), "", 0, 1, Nothing)
                If checkBlocking.Count > 0 Then
                    msgError.AppendLine(" &bull;MsKelurahan is waiting for approval <BR/>")
                    errorline.AppendLine(" &bull;MsKelurahan : Line " & DTR("NO") & "<BR/>")
                End If
            End Using

            '======================== Validasi textbox :  IDKelurahan   canot Null (#)====================================================
            If ObjectAntiNull(DTR("IDKelurahan")) = False Then
                msgError.AppendLine(" &bull; ID must be filled <BR/>")
                errorline.AppendLine(" &bull; MsKelurahan : Line " & DTR("NO") & "<BR/>")
            End If
            If ObjectAntiNull(DTR("IDKelurahan")) Then
                If objectNumOnly(DTR("IDKelurahan")) = False Then
                    msgError.AppendLine(" &bull; ID must be Numeric <BR/>")
                    errorline.AppendLine(" &bull; MsKelurahan : Line " & DTR("NO") & "<BR/>")
                End If
            End If
            '======================== Validasi textbox :  NamaKelurahan   canot Null (#)====================================================
            If ObjectAntiNull(DTR("NamaKelurahan")) = False Then
                msgError.AppendLine(" &bull; Nama must be filled <BR/>")
                errorline.AppendLine(" &bull; MsKelurahan : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorline.AppendLine(" &bull;MsKelurahan  : Line " & DTR("NO") & "<BR/>")
            LogError(ex)
        End Try
    End Sub

#End Region

#Region "Upload File"

    Public Function SaveFileImportToSave() As String
        Dim filePath As String
        Dim FileNameToGetData As String = ""
        Try
            If FileUploadKPI.PostedFile.ContentLength = 0 Then
                Return FileNameToGetData
            End If

            If Not FileUploadKPI.HasFile Then
                Return FileNameToGetData
            End If

            filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName)

            FileNameToGetData = FileUploadKPI.FileName
            If System.IO.File.Exists(filePath) Then
                Dim bexist As Boolean = True
                Dim i As Integer = 1
                While bexist
                    filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & FileUploadKPI.FileName.Split(CChar("."))(1))
                    FileNameToGetData = FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & Me.FileUploadKPI.FileName.Split(CChar("."))(1)
                    If Not System.IO.File.Exists(filePath) Then
                        bexist = False
                        Exit While
                    End If
                    i = i + 1
                End While
            End If
            Me.FileUploadKPI.PostedFile.SaveAs(filePath)
            Session("Filepath") = filePath
            Return FileNameToGetData
        Catch
            Throw
        End Try
    End Function

    ReadOnly Property FilePath() As String
        Get
            Return Session("FilePath")
        End Get
    End Property

    Private Sub ProcessExcelFile(ByVal FullPathFileNameToExportExcel As String)

        Using stream As FileStream = File.Open(FullPathFileNameToExportExcel, FileMode.Open, FileAccess.Read)
            Using excelReader As IExcelDataReader = ExcelReaderFactory.CreateBinaryReader(stream)
                excelReader.IsFirstRowAsColumnNames = True
                DSexcelTemp = excelReader.AsDataSet()
                If DSexcelTemp.Tables("MsKelurahan").Columns(0).ColumnName Like "*Column*" Then
                    DSexcelTemp.Tables("MsKelurahan").Rows.Clear()
                End If

            End Using
        End Using

    End Sub


#End Region

#Region "Function..."

    Function getErrorNo(ByVal Id As String, ByVal nama As String) As String
        Dim temp As String = ""
        Try
            Dim DV As New DataView(DT_AllMsKelurahanNCBS)
            'jika Null maka harus is null bukan kosong disini belum di handle
            DV.RowFilter = "IDKelurahan = '" & Id & "' and NamaKelurahan = '" & nama & "'"

            Dim DT As Data.DataTable = DV.ToTable
            If DT.Rows.Count > 0 Then
                temp = Safe(DT.Rows(0)("NO"))
            End If
        Catch ex As Exception
            temp = ex.Message
        End Try

        Return temp
    End Function
    ''' <summary>
    ''' Merubah excel ke Object Databale sekaligus memecahnya
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ImportExcelTempToDTproperty()
        Dim DT As New Data.DataTable
        'DT_MsKelurahan = DSexcelTemp.Tables("MsKelurahan")
        DT = DSexcelTemp.Tables("MsKelurahan")
        Dim DV As New DataView(DT)
        DT_AllMsKelurahan = DV.ToTable(True, "IDKelurahan", "IDKecamatan", "NamaKelurahan", "activation")
        DT_AllMsKelurahanNCBS = DSexcelTemp.Tables("MsKelurahan")
        DT_AllMsKelurahan.Columns.Add("NO")
        For Each i As DataRow In DT_AllMsKelurahan.Rows
            Dim PK As String = Safe(i("IDKelurahan"))
            Dim nama As String = Safe(i("NamaKelurahan"))
            i("NO") = getErrorNo(PK, nama)
        Next
    End Sub

    Private Sub ClearThisPageSessions()
        DT_AllMsKelurahan = Nothing
        DT_AllMsKelurahanNCBS = Nothing
        GroupTableSuccessData = Nothing
        GroupTableAnomalyData = Nothing
        DSexcelTemp = Nothing
        SetnGetDataTableForAnomalyData = Nothing
        SetnGetDataTableForSuccessData = Nothing
    End Sub


    Function ConvertDTtoMsKelurahanNCBS(ByVal DT As Data.DataTable) As TList(Of MsKelurahanNCBS_ApprovalDetail)
        Dim temp As New TList(Of MsKelurahanNCBS_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsKelurahanNCBS As New MsKelurahanNCBS_ApprovalDetail
                With MsKelurahanNCBS
                    FillOrNothing(.IDKelurahanNCBS, DTR("IDKelurahanBank"), True, oInt)
                    FillOrNothing(.NamaKelurahanNCBS, DTR("NamaKelurahanBank"), True, oString)
                    FillOrNothing(.Activation, 1, True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsKelurahanNCBS)
                End With
            End Using

        Next

        Return temp
    End Function



    Function ConvertDTtoMsKelurahan(ByVal DTR As DataRow) As MsKelurahan_ApprovalDetail
        Dim temp As New MsKelurahan_ApprovalDetail
        Using MsKelurahan As New MsKelurahan_ApprovalDetail()
            With MsKelurahan
                FillOrNothing(.IDKecamatan, DTR("IDKecamatan"), True, oInt)
                FillOrNothing(.IDKelurahan, DTR("IDKelurahan"), True, oInt)
                FillOrNothing(.NamaKelurahan, DTR("NamaKelurahan"), True, Ovarchar)

                FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                FillOrNothing(.CreatedDate, Now, True, oDate)
            End With
            temp = MsKelurahan
        End Using

        Return temp
    End Function

    Function ConvertDTtoMsKelurahan(ByVal DT As Data.DataTable) As TList(Of MsKelurahan_ApprovalDetail)
        Dim temp As New TList(Of MsKelurahan_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsKelurahan As New MsKelurahan_ApprovalDetail()
                With MsKelurahan
                    FillOrNothing(.IDKecamatan, DTR("IDKecamatan"), True, oInt)
                    FillOrNothing(.IDKelurahan, DTR("IDKelurahan"), True, oInt)
                    FillOrNothing(.NamaKelurahan, DTR("NamaKelurahan"), True, Ovarchar)

                    FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsKelurahan)
                End With
            End Using
        Next
        Return temp
    End Function

    Private Sub ChangeMultiView(ByVal index As Integer)
        MultiViewKPIOvertimeUpload.ActiveViewIndex = index
        If index = 0 Then
            Imgbtnsave.Visible = False
            Imgbtncancel.Visible = False
        ElseIf index = 1 Then
            Imgbtncancel.Visible = True
        ElseIf index = 2 Then
            Imgbtncancel.Visible = False
            Imgbtnsave.Visible = False
        End If
    End Sub

    Private Function GenerateGroupingMsKelurahan() As List(Of AMLBLL.MsKelurahanBll.DT_Group_MsKelurahan)
        Dim Ltemp As New List(Of AMLBLL.MsKelurahanBll.DT_Group_MsKelurahan)
        For i As Integer = 0 To DT_AllMsKelurahan.Rows.Count - 1
            Dim MsKelurahan As New AMLBLL.MsKelurahanBll.DT_Group_MsKelurahan
            With MsKelurahan
                .MsKelurahan = DT_AllMsKelurahan.Rows(i)
                .L_MsKelurahanNCBS = DT_AllMsKelurahanNCBS.Clone

                'Insert MsKelurahanNCBS
                Dim MsKelurahan_ID As String = Safe(.MsKelurahan("IDKelurahan"))
                Dim MsKelurahan_nama As String = Safe(.MsKelurahan("NamaKelurahan"))
                Dim MsKelurahan_Activation As String = Safe(.MsKelurahan("Activation"))
                If DT_AllMsKelurahanNCBS.Rows.Count > 0 Then
                    Using DV As New DataView(DT_AllMsKelurahanNCBS)
                        DV.RowFilter = "IDKelurahan='" & MsKelurahan_ID & "' and " & _
                                                  "NamaKelurahan='" & MsKelurahan_nama & "' and " & _
                                                  "activation='" & MsKelurahan_Activation & "'"
                        If DV.Count > 0 Then
                            Dim temp As Data.DataTable = DV.ToTable
                            For Each H As DataRow In temp.Rows
                                .L_MsKelurahanNCBS.ImportRow(H)
                            Next
                        End If
                    End Using
                End If




            End With
            Ltemp.Add(MsKelurahan)
        Next
        Return Ltemp
    End Function

    Public Sub BindGrid()
        lblValueFailedData.Text = SetnGetDataTableForAnomalyData.Rows.Count
        GridAnomalyList.DataSource = SetnGetDataTableForAnomalyData
        GridAnomalyList.DataBind()
        lblValueSuccessData.Text = SetnGetDataTableForSuccessData.Count
        GridSuccessList.DataSource = SetnGetDataTableForSuccessData
        lblValueuploadeddata.Text = DT_AllMsKelurahan.Rows.Count
        GridSuccessList.DataBind()
    End Sub

#End Region

#Region "events..."


    Protected Sub ImgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtncancel.Click
        ClearThisPageSessions()
        ChangeMultiView(0)
        LinkButtonExportExcel.Visible = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If IsPostBack Then
                Return
            End If

            ClearThisPageSessions()
            SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            ChangeMultiView(0)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        ChangeMultiView(0)
    End Sub

    Protected Sub GridAnomalyList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridAnomalyList.PageIndexChanged
        GridAnomalyList.CurrentPageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GridSuccessList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridSuccessList.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                'Show mapping item
                Dim StringMaping As New StringBuilder
                Dim DV As New DataView(DT_AllMsKelurahanNCBS)
                Dim DT As New Data.DataTable
                DV.RowFilter = "IDKelurahan='" & e.Item.Cells(1).Text & "' and " & "NamaKelurahan='" & e.Item.Cells(2).Text & "'"
                DT = DV.ToTable
                If DT.Rows.Count > 0 Then
                    Dim countField As Integer = 7
                    Dim lengthField As Integer = 0
                    For Each idx As DataRow In DT.Rows
                        With idx
                            If lengthField > countField Then
                                StringMaping.AppendLine(idx("NamaKelurahanBank") & "(" & idx("IDKelurahanBank") & ");")
                                lengthField = 0
                            Else
                                StringMaping.AppendLine(idx("NamaKelurahanBank") & "(" & idx("IDKelurahanBank") & ");")
                                lengthField += 1
                            End If

                        End With
                    Next
                Else
                    StringMaping.Append("-")
                End If
                e.Item.Cells(CollCount).Text = StringMaping.ToString

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridSuccessList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridSuccessList.PageIndexChanged
        Dim GridTarget As DataGrid = CType(source, DataGrid)
        Try
            GridSuccessList.CurrentPageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgSaveSuccessData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtnsave.Click
        Try
            'File belum diupload
            If DT_AllMsKelurahan Is Nothing Then
                Throw New Exception("Load data failed")
            End If

            'Save MsKelurahan Approval (Header)
            Dim KeyApp As String
            Using Aproval As New MsKelurahan_Approval()
                With Aproval
                    .RequestedBy = GET_PKUserID
                    .RequestedDate = Date.Now
                    .FK_MsMode_Id = 2 'Edit
                    .IsUpload = True
                End With
                DataRepository.MsKelurahan_ApprovalProvider.Save(Aproval)
                'ambil key approval
                KeyApp = Aproval.PK_MsKelurahan_Approval_Id
            End Using

            For k As Integer = 0 To GroupTableSuccessData.Count - 1

                Dim MsKelurahan As GrouP_MsKelurahan_success = GroupTableSuccessData(k)


                With MsKelurahan
                    'save MsKelurahan approval detil
                    .MsKelurahan.FK_MsKelurahan_Approval_Id = KeyApp
                    DataRepository.MsKelurahan_ApprovalDetailProvider.Save(.MsKelurahan)
                    'ambil key MsKelurahan approval detil
                    Dim IdMsKelurahan As String = .MsKelurahan.IDKelurahan
                    Dim L_MappingMsKelurahanNCBSPPATK_Approval_Detail As New TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail)
                    'save mapping dan NCBS
                    For Each OMsKelurahanNCBS_ApprovalDetail As MsKelurahanNCBS_ApprovalDetail In .L_MsKelurahanNCBS
                        OMsKelurahanNCBS_ApprovalDetail.FK_MsKelurahan_Approval_Id = KeyApp
                        Using OMappingMsKelurahanNCBSPPATK_Approval_Detail As New MappingMsKelurahanNCBSPPATK_Approval_Detail
                            With OMappingMsKelurahanNCBSPPATK_Approval_Detail
                                FillOrNothing(.IDKelurahanNCBS, OMsKelurahanNCBS_ApprovalDetail.IDKelurahanNCBS, True, oInt)
                                FillOrNothing(.IDKelurahan, IdMsKelurahan, True, oInt)
                                FillOrNothing(.PK_MsKelurahan_Approval_Id, KeyApp, True, oInt)
                                FillOrNothing(.Nama, OMsKelurahanNCBS_ApprovalDetail.NamaKelurahanNCBS, True, oString)
                            End With
                            L_MappingMsKelurahanNCBSPPATK_Approval_Detail.Add(OMappingMsKelurahanNCBSPPATK_Approval_Detail)
                        End Using
                    Next
                    'save maping dan NCBS
                    DataRepository.MappingMsKelurahanNCBSPPATK_Approval_DetailProvider.Save(L_MappingMsKelurahanNCBSPPATK_Approval_Detail)
                    DataRepository.MsKelurahanNCBS_ApprovalDetailProvider.Save(.L_MsKelurahanNCBS)

                End With
            Next

            LblConfirmation.Text = "MsKelurahan data has edited (" & SetnGetDataTableForSuccessData.Count & " Rows / MsKelurahan)"
            ChangeMultiView(2)

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgUpload.Click
        Dim CombinedFileName As String
        GroupTableSuccessData = New List(Of GrouP_MsKelurahan_success)
        GroupTableAnomalyData = New List(Of AMLBLL.MsKelurahanBll.DT_Group_MsKelurahan)
        Try
            'validasi File
            If FileUploadKPI.FileName = "" Then
                Throw New Exception("Data required")
            End If

            If Path.GetExtension(FileUploadKPI.FileName) <> ".xls" Then
                Throw New Exception("Invalid File Upload Extension")
            End If

            CombinedFileName = SaveFileImportToSave()
            If String.Compare(CombinedFileName, "", False) <> 0 Then
                Try
                    ProcessExcelFile(FilePath)
                Catch ex As Exception
                    Throw New Exception("Invalid File Format")
                End Try
            Else
                Throw New Exception("Invalid File Format")
            End If

            'Import from Excell to property Global
            ImportExcelTempToDTproperty()

            Dim LG_MsKelurahan As List(Of AMLBLL.MsKelurahanBll.DT_Group_MsKelurahan) = GenerateGroupingMsKelurahan()
            'tambahkan kolom untuk tampilan di grid anomaly
            DT_AllMsKelurahan.Columns.Add("Description")
            DT_AllMsKelurahan.Columns.Add("errorline")

            'Membuat tempory untuk yang anomali dan yang sukses
            Dim DTnormal As Data.DataTable = DT_AllMsKelurahan.Clone
            Dim DTAnomaly As Data.DataTable = DT_AllMsKelurahan.Clone

            'proses pemilahan data anomaly dan data sukses melalui validasi 
            For i As Integer = 0 To LG_MsKelurahan.Count - 1
                Dim Lerror As List(Of String) = ValidateInput(LG_MsKelurahan(i))
                Dim MSG As String = Lerror(0)
                Dim LINE As String = Lerror(1)
                If MSG = "" Then
                    DTnormal.ImportRow(LG_MsKelurahan(i).MsKelurahan)
                    Dim GroupMsKelurahan As New GrouP_MsKelurahan_success
                    With GroupMsKelurahan
                        'konversi dari datatable ke Object Nettier
                        .MsKelurahan = ConvertDTtoMsKelurahan(LG_MsKelurahan(i).MsKelurahan)
                        .L_MsKelurahanNCBS = ConvertDTtoMsKelurahanNCBS(LG_MsKelurahan(i).L_MsKelurahanNCBS)
                    End With
                    'masukan ke group sukses
                    GroupTableSuccessData.Add(GroupMsKelurahan)
                Else
                    DT_AllMsKelurahan.Rows(i)("Description") = MSG
                    DT_AllMsKelurahan.Rows(i)("errorline") = LINE
                    DTAnomaly.ImportRow(DT_AllMsKelurahan.Rows(i))
                    GroupTableAnomalyData.Add(LG_MsKelurahan(i))
                End If

            Next

            SetnGetDataTableForSuccessData = ConvertDTtoMsKelurahan(DTnormal)
            SetnGetDataTableForAnomalyData = DTAnomaly

            If DTAnomaly.Rows.Count > 0 Then
                Imgbtnsave.Visible = False
                Imgbtncancel.Visible = True
            Else
                Imgbtnsave.Visible = True
                Imgbtncancel.Visible = True
            End If

            BindGrid()
            If GridAnomalyList.Items.Count < 1 Then
                LinkButtonExportExcel.Visible = False
            Else
                LinkButtonExportExcel.Visible = True
            End If
            ChangeMultiView(1)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Dim Exp As New AMLBLL.MsKelurahanBll
            Dim FolderPath As String = Server.MapPath("FolderTemplate")

            Dim buffer As Byte() = Exp.ExportDTtoExcel(GroupTableAnomalyData, FolderPath)
            Response.AddHeader("Content-type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment; filename=MsKelurahanUpload.xls")
            Response.BinaryWrite(buffer)
            Response.Flush()
            Response.End()

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered 

    End Sub

    Protected Sub GridSuccessList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridSuccessList.SelectedIndexChanged

    End Sub
End Class





