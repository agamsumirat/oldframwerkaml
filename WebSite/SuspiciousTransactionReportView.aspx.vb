
Partial Class SuspiciousTransactionReportView
    Inherits Parent
#Region " Property "
    Private Property SearchCaseID() As String
        Get
            If Not Session("SuspeciousTransactionViewCaseID") Is Nothing Then
                Return Session("SuspeciousTransactionViewCaseID")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewCaseID") = value
        End Set
    End Property
    Private Property SearchCaseDescription() As String
        Get
            If Not Session("SuspeciousTransactionViewCaseDescription") Is Nothing Then
                Return Session("SuspeciousTransactionViewCaseDescription")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewCaseDescription") = value
        End Set
    End Property
    Private Property SearchReportedDateFirst() As String
        Get
            If Not Session("SuspeciousTransactionViewReportedDateFirst") Is Nothing Then
                Return Session("SuspeciousTransactionViewReportedDateFirst")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewReportedDateFirst") = value
        End Set
    End Property
    Private Property SearchReportedDateLast() As String
        Get
            If Not Session("SuspeciousTransactionViewReportedDateLast") Is Nothing Then
                Return Session("SuspeciousTransactionViewReportedDateLast")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewReportedDateLast") = value
        End Set
    End Property
    Private Property SearchCaseStatus() As String
        Get
            If Not Session("SuspeciousTransactionViewCaseStatus") Is Nothing Then
                Return Session("SuspeciousTransactionViewCaseStatus")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewCaseStatus") = value
        End Set
    End Property
    Private Property SearchWorkFlowStep() As String
        Get
            If Not Session("SuspeciousTransactionViewWorkFlowStep") Is Nothing Then
                Return Session("SuspeciousTransactionViewWorkFlowStep")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewWorkFlowStep") = value
        End Set
    End Property
    Private Property SearchAccountOwnerName() As String
        Get
            If Not Session("SuspeciousTransactionViewAccountOwnerName") Is Nothing Then
                Return Session("SuspeciousTransactionViewAccountOwnerName")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewAccountOwnerName") = value
        End Set
    End Property
    Private Property SearchLastProposedAction() As String
        Get
            If Not Session("SuspeciousTransactionViewLastProposedAction") Is Nothing Then
                Return Session("SuspeciousTransactionViewLastProposedAction")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewLastProposedAction") = value
        End Set
    End Property
    Private Property SearchCreateDateFirst() As String
        Get
            If Not Session("SuspeciousTransactionViewCreatedDateFirst") Is Nothing Then
                Return Session("SuspeciousTransactionViewCreatedDateFirst")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewCreatedDateFirst") = value
        End Set
    End Property
    Private Property SearchCreateDateLast() As String
        Get
            If Not Session("SuspeciousTransactionViewCreatedDateLast") Is Nothing Then
                Return Session("SuspeciousTransactionViewCreatedDateLast")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewCreatedDateLast") = value
        End Set
    End Property
    Private Property SearchLastUpdatedDateFirst() As String
        Get
            If Not Session("SuspeciousTransactionViewLastUpdatedDateFirst") Is Nothing Then
                Return Session("SuspeciousTransactionViewLastUpdatedDateFirst")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewLastUpdatedDateFirst") = value
        End Set
    End Property
    Private Property SearchLastUpdatedDateLast() As String
        Get
            If Not Session("SuspeciousTransactionViewLastUpdatedDateLast") Is Nothing Then
                Return Session("SuspeciousTransactionViewLastUpdatedDateLast")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewLastUpdatedDateLast") = value
        End Set
    End Property
    Private Property SearchReportedBy() As String
        Get
            If Not Session("SuspeciousTransactionViewReportedBy") Is Nothing Then
                Return Session("SuspeciousTransactionViewReportedBy")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewReportedBy") = value
        End Set
    End Property
    Private Property SearchPIC() As String
        Get
            If Not Session("SuspeciousTransactionViewPIC") Is Nothing Then
                Return Session("SuspeciousTransactionViewPIC")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewPIC") = value
        End Set
    End Property
    Private Property SearchHasOpenIssue() As String
        Get
            If Not Session("SuspeciousTransactionViewHasOpenIssue") Is Nothing Then
                Return Session("SuspeciousTransactionViewHasOpenIssue")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewHasOpenIssue") = value
        End Set
    End Property
    Private Property SearchAging() As String
        Get
            If Not Session("SuspeciousTransactionViewAging") Is Nothing Then
                Return Session("SuspeciousTransactionViewAging")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("SuspeciousTransactionViewAging") = value
        End Set
    End Property
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("SuspeciousTransactionViewSelected") Is Nothing, New ArrayList, Session("SuspeciousTransactionViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("SuspeciousTransactionViewSelected") = value
        End Set
    End Property


    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("SuspeciousTransactionViewSort") Is Nothing, "PK_CaseManagementID asc", Session("SuspeciousTransactionViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("SuspeciousTransactionViewSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("SuspeciousTransactionViewCurrentPage") Is Nothing, 0, Session("SuspeciousTransactionViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("SuspeciousTransactionViewCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("SuspeciousTransactionViewRowTotal") Is Nothing, 0, Session("SuspeciousTransactionViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("SuspeciousTransactionViewRowTotal") = Value
        End Set
    End Property
    Private ReadOnly Property Tables() As String
        Get
            Return " CaseManagement INNER JOIN CaseStatus ON CaseManagement.FK_CaseStatusID = CaseStatus.CaseStatusId INNER JOIN AccountOwner ON CaseManagement.FK_AccountOwnerID = AccountOwner.AccountOwnerId LEFT OUTER JOIN CaseManagementProposedAction ON CaseManagement.FK_LastProposedStatusID = CaseManagementProposedAction.PK_CaseManagementProposedActionID"

        End Get

    End Property
    Private ReadOnly Property PK() As String
        Get
            Return "CaseManagement.PK_CaseManagementID"
        End Get
    End Property
    Private ReadOnly Property Fields() As String

        Get
            Dim strField As New StringBuilder
            strField.Append("CaseManagement.PK_CaseManagementID, CaseManagement.CaseDescription, CaseManagement.FK_CaseStatusID,")
            strField.Append("CaseStatus.CaseStatusDescription, CaseManagement.WorkflowStep, CaseManagement.CreatedDate, CaseManagement.LastUpdated,")
            strField.Append("CaseManagement.FK_AccountOwnerID, AccountOwner.AccountOwnerName, CaseManagement.PIC, CaseManagement.HasOpenIssue,")
            strField.Append("CaseManagement.Aging, CaseManagement.IsReportedtoRegulator, CaseManagement.PPATKConfirmationno,CaseManagement.UserIDReportedBy,CaseManagement.ReportedDate,")
            strField.Append("CaseManagementProposedAction.ProposedAction")
            Return strField.ToString
        End Get
    End Property

    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property SetnGetBindTable() As Data.DataTable
        Get
            ' Return IIf(Session("SuspeciousTransactionViewData") Is Nothing, New AMLDAL.CaseManagement.CaseManagementDataTable, Session("SuspeciousTransactionViewData"))

            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            If SearchCaseID.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.PK_CaseManagementID like'%" & SearchCaseID.Replace("'", "''") & "%' "
            End If
            If SearchCaseDescription.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.CaseDescription like'%" & SearchCaseDescription.Replace("'", "''") & "%' "
            End If
            If SearchCaseStatus.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseStatus.CaseStatusDescription like'%" & SearchCaseStatus.Replace("'", "''") & "%' "
            End If
            If SearchWorkFlowStep.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.WorkflowStep like'%" & SearchWorkFlowStep.Replace("'", "''") & "%' "
            End If
            If SearchAccountOwnerName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "accountowner.AccountOwnerName like'%" & SearchAccountOwnerName.Replace("'", "''") & "%' "
            End If
            If SearchLastProposedAction.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagementProposedAction.ProposedAction like'%" & SearchLastProposedAction.Replace("'", "''") & "%' "
            End If
            If SearchCreateDateFirst.Trim.Length > 0 And SearchCreateDateLast.Trim.Length = 0 Then
                Throw New Exception("Created Date until must be filled")
            End If
            If SearchCreateDateFirst.Trim.Length = 0 And SearchCreateDateLast.Trim.Length > 0 Then
                Throw New Exception("Created Date from must be filled")
            End If
            If SearchCreateDateFirst.Trim.Length > 0 And SearchCreateDateLast.Trim.Length > 0 Then
                Dim DCreatedFirst As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SearchCreateDateFirst.Trim)
                Dim DCreatedLast As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SearchCreateDateLast.Trim)
                If DCreatedFirst.CompareTo(DateTime.MinValue) > 0 And DCreatedLast.CompareTo(DateTime.MinValue) > 0 Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "CaseManagement.CreatedDate between '" & Format(DCreatedFirst, "yyyy-MM-dd").Replace("'", "''") & " 00:00:00' and '" & Format(DCreatedLast, "yyyy-MM-dd") & " 23:59:59'"
                End If
            End If
            If SearchLastUpdatedDateFirst.Trim.Length > 0 And SearchLastUpdatedDateLast.Trim.Length > 0 Then
                Dim DUpdatedFirst As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SearchLastUpdatedDateFirst.Trim)
                Dim DUpdatedLast As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SearchLastUpdatedDateLast.Trim)
                If DUpdatedFirst.CompareTo(DateTime.MinValue) > 0 And DUpdatedLast.CompareTo(DateTime.MinValue) > 0 Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "CaseManagement.LastUpdated between '" & Format(DUpdatedFirst, "yyyy-MM-dd").Replace("'", "''") & " 00:00:00' and '" & Format(DUpdatedLast, "yyyy-MM-dd") & " 23:59:59'"
                End If
            End If
            If SearchReportedDateFirst.Trim.Length > 0 And SearchReportedDateLast.Trim.Length > 0 Then
                Dim DReportFirst As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SearchReportedDateFirst.Trim)
                Dim DReportLast As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SearchReportedDateLast.Trim)
                If DReportFirst.CompareTo(DateTime.MinValue) > 0 And DReportLast.CompareTo(DateTime.MinValue) > 0 Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "CaseManagement.ReportedDate between '" & Format(DReportFirst, "yyyy-MM-dd").Replace("'", "''") & " 00:00:00' and '" & Format(DReportLast, "yyyy-MM-dd") & " 23:59:59'"
                End If
            End If
            If SearchPIC.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.PIC like'%" & SearchPIC.Replace("'", "''") & "%' "
            End If
            If SearchReportedBy.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CaseManagement.UserIDReportedBy like'%" & SearchReportedBy.Replace("'", "''") & "%' "
            End If
            If SearchHasOpenIssue.Trim.Length > 0 Then
                'Dim TempSearchHasOpenIssue As Integer
                'If SearchHasOpenIssue = "true" Then
                '    TempSearchHasOpenIssue = 1
                'Else
                '    TempSearchHasOpenIssue = 0
                'End If
                ReDim Preserve strWhereClause(strWhereClause.Length)
                'strWhereClause(strWhereClause.Length - 1) = "CASE WHEN CaseManagement.HasOpenIssue=1 THEN 'TRUE' ELSE 'FALSE' END LIKE '%" & TempSearchHasOpenIssue & "%' "
                strWhereClause(strWhereClause.Length - 1) = "CASE WHEN CaseManagement.HasOpenIssue=1 THEN 'TRUE' ELSE 'FALSE' END LIKE '%" & SearchHasOpenIssue.Replace("'", "''") & "%' "
            End If
            If SearchAging.Trim.Length > 0 Then
                If IsNumeric(SearchAging) Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "CaseManagement.Aging =" & SearchAging.Replace("'", "''") & " "
                Else
                    Throw New Exception("Aging must be numeric.")
                End If
            End If
            strAllWhereClause = String.Join(" and ", strWhereClause)
            If strAllWhereClause.Trim = "" Then
                strAllWhereClause += " IsReportedtoRegulator=1 "
                'strAllWhereClause += " fk_accountownerid in (select accountownerid from AccountOwnerUserMapping where workingunitid in( select * from ufn_GetHierarkiWorkingUnitByUserID('" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "'))) "
            Else
                strAllWhereClause += " and IsReportedtoRegulator=1 "
                'strAllWhereClause += " and  fk_accountownerid in (select accountownerid from AccountOwnerUserMapping where workingunitid in( select * from ufn_GetHierarkiWorkingUnitByUserID('" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "'))) "
            End If

            Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCount(Me.Tables, strAllWhereClause)
            Dim pageSize As Long
            If Sahassa.AML.Commonly.SessionPagingLimit = "" Then
                pageSize = 10
            Else
                pageSize = Sahassa.AML.Commonly.SessionPagingLimit
            End If

            Return Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, pageSize, Me.Fields, strAllWhereClause, "")

        End Get

    End Property
#End Region




    Private Sub ClearThisPageSessions()
        Session("SuspeciousTransactionViewSelected") = Nothing
        Session("SuspeciousTransactionViewFieldSearch") = Nothing
        Session("SuspeciousTransactionViewValueSearch") = Nothing
        Session("SuspeciousTransactionViewSort") = Nothing
        Session("SuspeciousTransactionViewCurrentPage") = Nothing
        Session("SuspeciousTransactionViewRowTotal") = Nothing
        Session("SuspeciousTransactionViewData") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()

                Me.popupCreatedFirst.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtCreatedDate1.ClientID & "'), 'dd-mm-yyyy')")
                Me.popupCreatedLast.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtCreatedDate2.ClientID & "'), 'dd-mm-yyyy')")
                Me.popupLastUpdatedFirst.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtLastUpdatedDate1.ClientID & "'), 'dd-mm-yyyy')")
                Me.popupLastUpdatedLast.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtLastUpdatedDate2.ClientID & "'), 'dd-mm-yyyy')")
                Me.popupReportedDateFirst.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtReportedDateAwal.ClientID & "'), 'dd-mm-yyyy')")
                Me.popupReportedDatelast.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtReportedDateAkhir.ClientID & "'), 'dd-mm-yyyy')")


                Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)


                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            SettingControlSearch()
            Me.GridMSUserView.DataSource = Me.SetnGetBindTable
            'Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
            Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridMSUserView.DataBind()
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0


        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.SettingPropertySearch()
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid edit command handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Dim strCaseManagementID As String = e.Item.Cells(1).Text
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "CaseManagementViewDetail.aspx?PK_CaseManagementID=" & strCaseManagementID
            Me.Response.Redirect("CaseManagementViewDetail.aspx?PK_CaseManagementID=" & strCaseManagementID, False)
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 14/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim PKID As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PKID) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()
        Try
            Dim Rows As New ArrayList
            For Each IdPk As Int32 In Me.SetnGetSelectedItem
                'Dim rowData() As AMLDAL.CaseManagementTableAdapters.vw_CaseManagementTableAdapter = Me.SetnGetBindTable.Select("RulesAdvancedID = '" & IdPk & "'")
                Using adapter As New AMLDAL.CaseManagementTableAdapters.CaseManagementTableAdapter
                    For Each orow As AMLDAL.CaseManagement.CaseManagementRow In adapter.GetDataByPK(IdPk).Rows
                        Rows.Add(orow)
                    Next

                End Using
            Next
            Me.GridMSUserView.DataSource = Rows
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0,1,6 & 7 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(1).Visible = False


        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' export button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=SuspiciousTrans.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub


    ''' <summary>
    ''' Setting Property Searching
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SettingPropertySearch()
        Me.SearchCaseID = txtCaseID.Text.Trim
        Me.SearchCaseDescription = txtDescription.Text.Trim
        Me.SearchCaseStatus = txtCaseStatus.Text.Trim
        Me.SearchWorkFlowStep = txtWorkFlowStep.Text.Trim
        Me.SearchAccountOwnerName = txtAccountOwner.Text.Trim
        Me.SearchLastProposedAction = txtLastProposedAction.Text.Trim
        Me.SearchLastUpdatedDateFirst = txtLastUpdatedDate1.Text.Trim
        Me.SearchLastUpdatedDateLast = txtLastUpdatedDate2.Text.Trim
        Me.SearchCreateDateFirst = txtCreatedDate1.Text.Trim
        Me.SearchCreateDateLast = txtCreatedDate2.Text.Trim
        Me.SearchPIC = txtPIC.Text.Trim
        Me.SearchHasOpenIssue = txtHasOpenIssue.Text.Trim
        Me.SearchAging = txtAging.Text.Trim
        Me.SearchReportedDateFirst = txtReportedDateAwal.Text
        Me.SearchReportedDateLast = txtReportedDateAkhir.Text
        Me.SearchReportedBy = txtReportedBy.Text.Trim
    End Sub
    Private Sub SettingControlSearch()
        txtCaseID.Text = Me.SearchCaseID
        txtDescription.Text = Me.SearchCaseDescription
        txtCaseStatus.Text = Me.SearchCaseStatus
        txtWorkFlowStep.Text = Me.SearchWorkFlowStep
        txtAccountOwner.Text = Me.SearchAccountOwnerName
        txtLastProposedAction.Text = Me.SearchLastProposedAction
        txtCreatedDate1.Text = Me.SearchCreateDateFirst
        txtCreatedDate2.Text = Me.SearchCreateDateLast
        txtLastUpdatedDate1.Text = Me.SearchLastUpdatedDateFirst
        txtLastUpdatedDate2.Text = Me.SearchLastUpdatedDateLast
        txtPIC.Text = Me.SearchPIC
        txtHasOpenIssue.Text = Me.SearchHasOpenIssue
        txtAging.Text = Me.SearchAging
        txtReportedDateAwal.Text = Me.SearchReportedDateFirst
        txtReportedDateAkhir.Text = Me.SearchReportedDateLast
        txtReportedBy.Text = Me.SearchReportedBy
    End Sub


    Protected Sub ImageClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageClearSearch.Click
        Me.SearchCaseID = String.Empty
        Me.SearchCaseDescription = String.Empty
        Me.SearchCaseStatus = String.Empty
        Me.SearchWorkFlowStep = String.Empty
        Me.SearchAccountOwnerName = String.Empty
        Me.SearchLastProposedAction = String.Empty
        Me.SearchLastUpdatedDateFirst = String.Empty
        Me.SearchLastUpdatedDateLast = String.Empty
        Me.SearchCreateDateFirst = String.Empty
        Me.SearchCreateDateLast = String.Empty
        Me.SearchPIC = String.Empty
        Me.SearchHasOpenIssue = String.Empty
        Me.SearchAging = String.Empty
        Me.SearchReportedDateFirst = String.Empty
        Me.SearchReportedDateLast = String.Empty
        Me.SearchReportedBy = String.Empty
    End Sub


End Class
