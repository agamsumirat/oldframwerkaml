Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Imports System.IO
Imports System.Collections.Generic

Partial Class MappingUserIdHiportJiveEdit
    Inherits Parent

    Public ReadOnly Property PK_MappingUserIdHIPORTJIVE_ID() As Integer
        Get
            Dim strTemp As String = Request.Params("PK_MappingUserIdHIPORTJIVE_ID")
            Dim intResult As Integer
            If Integer.TryParse(strTemp, intResult) Then
                Return intResult
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Parameter PK_MappingUserIdHIPORTJIVE_ID is invalid."

            End If
        End Get
    End Property

    Public ReadOnly Property Objvw_MappingUserIdHIPORTJIVE() As VList(Of vw_MappingUserIdHiportJive)
        Get
            If Session("Objvw_MappingUserIdHIPORTJIVE.MappingUserIdHIPORTJIVEEdit") Is Nothing Then

                Session("Objvw_MappingUserIdHIPORTJIVE.MappingUserIdHIPORTJIVEEdit") = DataRepository.vw_MappingUserIdHiportJiveProvider.GetPaged("PK_MappingUserIdHIPORTJIVE_ID = '" & PK_MappingUserIdHIPORTJIVE_ID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("Objvw_MappingUserIdHIPORTJIVE.MappingUserIdHIPORTJIVEEdit"), VList(Of vw_MappingUserIdHiportJive))
        End Get
    End Property

    Public Property SetnGetMode() As String
        Get
            If Not Session("MappingUserIdHIPORTJIVEEdit.Mode") Is Nothing Then
                Return CStr(Session("MappingUserIdHIPORTJIVEEdit.Mode"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingUserIdHIPORTJIVEEdit.Mode") = value
        End Set
    End Property

    Private Sub LoadMode()
        Session("MappingUserIdHIPORTJIVEEdit.Mode") = Nothing
        If Objvw_MappingUserIdHIPORTJIVE.Count > 0 Then

            Using objuser As User = UserBLL.GetUserByPkUserID(Objvw_MappingUserIdHIPORTJIVE(0).FK_User_ID.GetValueOrDefault(0))
                If Not objuser Is Nothing Then
                    LblUserID.Text = objuser.UserID
                End If
            End Using

            LabelUserName.Text = Objvw_MappingUserIdHIPORTJIVE(0).UserName


            HUserID.Value = Objvw_MappingUserIdHIPORTJIVE(0).FK_User_ID.GetValueOrDefault(0).ToString
            TxtKodeAO.Text = Objvw_MappingUserIdHIPORTJIVE(0).Kode_AO
            'CboUserId.Items.Clear()
            'Using ObjHIPORTJIVEBll As New AMLBLL.MappingUserIdHiportJiveBLL
            '    CboUserId.AppendDataBoundItems = True
            '    CboUserId.DataSource = ObjHIPORTJIVEBll.GeModeDataEdit
            '    CboUserId.DataTextField = UserColumn.UserName.ToString
            '    CboUserId.DataValueField = UserColumn.UserID.ToString
            '    CboUserId.DataBind()
            '    CboUserId.Items.Insert(0, New ListItem("...", ""))
            '    'CboUserId.Items.Insert(1, New ListItem(Objvw_MappingUserIdHIPORTJIVE(0).UserName, CStr(Objvw_MappingUserIdHIPORTJIVE(0).FK_User_ID)))
            'End Using

        Else
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = MappingUserIdHiportJiveBLL.DATA_NOTVALID
        End If

    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oTrans As SqlTransaction = Nothing
        Try

            Using ObjList As MappingUserIdHiportJive = DataRepository.MappingUserIdHiportJiveProvider.GetByPK_MappingUserIdHiportJive_ID(PK_MappingUserIdHIPORTJIVE_ID)
                If ObjList Is Nothing Then
                    Throw New AMLBLL.SahassaException(MappingUserIdHiportJiveEnum.DATA_NOTVALID)
                End If
            End Using

            Dim UserName As String

            UserName = LblUserID.Text
            Dim UserId As Integer = 0
            Dim ObjCheckUser As TList(Of User) = DataRepository.UserProvider.GetPaged("UserID = '" & UserName & "'", "", 0, Integer.MaxValue, 0)
            If ObjCheckUser.Count > 0 Then
                UserId = CInt(ObjCheckUser(0).pkUserID)
            End If

            If Not UserName = "" Then

                AMLBLL.MappingUserIdHiportJiveBLL.IsDataValidEditApproval(CStr(UserId), Me.PK_MappingUserIdHIPORTJIVE_ID)

                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    Using ObjMappingUserIdHIPORTJIVENEW As New MappingUserIdHiportJive
                        Using ObjMappingUserIdHIPORTJIVE As MappingUserIdHiportJive = DataRepository.MappingUserIdHiportJiveProvider.GetByPK_MappingUserIdHiportJive_ID(PK_MappingUserIdHIPORTJIVE_ID)
                            Using ObjUser As User = DataRepository.UserProvider.GetBypkUserID(CLng(ObjMappingUserIdHIPORTJIVE.FK_User_ID))
                                AMLBLL.MappingUserIdHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingUserIdHIPORTJIVE", "UserName", "Edit", ObjUser.UserName, CStr(UserId), "Acc")
                            End Using
                            AMLBLL.MappingUserIdHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingUserIdHIPORTJIVE", "Kode_AO", "Edit", CStr(ObjMappingUserIdHIPORTJIVE.Kode_AO), TxtKodeAO.Text, "Acc")
                            DataRepository.MappingUserIdHiportJiveProvider.Delete(ObjMappingUserIdHIPORTJIVE)
                        End Using
                        ObjMappingUserIdHIPORTJIVENEW.FK_User_ID = UserId
                        ObjMappingUserIdHIPORTJIVENEW.Kode_AO = TxtKodeAO.Text

                        Using Objlist As TList(Of MappingUserIdHiportJive) = DataRepository.MappingUserIdHiportJiveProvider.GetPaged("FK_User_ID = '" & UserId & "'", "", 0, Integer.MaxValue, 0)
                            If Objlist.Count > 0 Then
                                Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEView.aspx"
                                Me.Response.Redirect("MappingUserIdHIPORTJIVEView.aspx", False)
                            Else

                                DataRepository.MappingUserIdHiportJiveProvider.Save(ObjMappingUserIdHIPORTJIVENEW)
                                Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEView.aspx"
                                Me.Response.Redirect("MappingUserIdHIPORTJIVEView.aspx", False)
                            End If
                        End Using

                    End Using
                Else
                    Using ObjMappingUserIdHIPORTJIVE_Approval As New MappingUserIdHiportJive_Approval

                        Using objUser As TList(Of User) = DataRepository.UserProvider.GetPaged("PKUserID = '" & UserId & "'", "", 0, Integer.MaxValue, 0)
                            UserName = objUser(0).UserName

                            ObjMappingUserIdHIPORTJIVE_Approval.FK_MsUserID = Sahassa.AML.Commonly.SessionPkUserId
                            ObjMappingUserIdHIPORTJIVE_Approval.UserName = UserName
                            ObjMappingUserIdHIPORTJIVE_Approval.FK_ModeID = CInt(Sahassa.AML.Commonly.TypeMode.Edit)
                            ObjMappingUserIdHIPORTJIVE_Approval.CreatedDate = Now
                            DataRepository.MappingUserIdHiportJive_ApprovalProvider.Save(ObjMappingUserIdHIPORTJIVE_Approval)

                            Using ObjMappingUserIdHIPORTJIVE As MappingUserIdHiportJive = DataRepository.MappingUserIdHiportJiveProvider.GetByPK_MappingUserIdHiportJive_ID(PK_MappingUserIdHIPORTJIVE_ID)
                                Using ObjMappingUserIdHIPORTJIVE_approvalDetail As New MappingUserIdHiportJive_ApprovalDetail
                                    ObjMappingUserIdHIPORTJIVE_approvalDetail.FK_MappingUserIdHiportJive_Approval_ID = ObjMappingUserIdHIPORTJIVE_Approval.PK_MappingUserIdHiportJive_Approval_ID
                                    ObjMappingUserIdHIPORTJIVE_approvalDetail.PK_MappingUserIdHiportJive_ID = PK_MappingUserIdHIPORTJIVE_ID
                                    ObjMappingUserIdHIPORTJIVE_approvalDetail.FK_User_ID = UserId
                                    ObjMappingUserIdHIPORTJIVE_approvalDetail.Kode_AO = TxtKodeAO.Text
                                    ObjMappingUserIdHIPORTJIVE_approvalDetail.PK_MappingUserIdHiportJive_ID_Old = PK_MappingUserIdHIPORTJIVE_ID
                                    ObjMappingUserIdHIPORTJIVE_approvalDetail.FK_User_ID_Old = ObjMappingUserIdHIPORTJIVE.FK_User_ID
                                    ObjMappingUserIdHIPORTJIVE_approvalDetail.Kode_AO_OLD = ObjMappingUserIdHIPORTJIVE.Kode_AO
                                    'ObjMappingUserIdHIPORTJIVE_approvalDetail.PK_MappingUserIdHIPORTJIVE_ID_Old = ObjMappingUserIdHIPORTJIVE.PK_MappingUserIdHIPORTJIVE_ID
                                    AMLBLL.MappingUserIdHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingUserIdHIPORTJIVE", "UserName", "Edit", UserName, CStr(UserId), "PendingApproval")
                                    AMLBLL.MappingUserIdHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingUserIdHIPORTJIVE", "Kode_AO", "Edit", CStr(ObjMappingUserIdHIPORTJIVE.Kode_AO), TxtKodeAO.Text, "PendingApproval")
                                    DataRepository.MappingUserIdHiportJive_ApprovalDetailProvider.Save(ObjMappingUserIdHIPORTJIVE_approvalDetail)
                                End Using
                            End Using
                        End Using
                        'Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEView.aspx"
                        'Me.Response.Redirect("MappingUserIdHIPORTJIVEView.aspx", False)
                        Using ObjUser As TList(Of User) = DataRepository.UserProvider.GetPaged("PKUserID = '" & UserId & "'", "", 0, Integer.MaxValue, 0)
                            Me.LblSuccess.Text = "Data saved successfully and waiting for Approval."
                            Me.LblSuccess.Visible = True
                            LoadMode()
                        End Using

                        'End Using
                    End Using
                End If
                MultiView1.ActiveViewIndex = 1
            Else
                Me.LblSuccess.Text = "Please select User"
                Me.LblSuccess.Visible = True
            End If

        Catch ex As Exception
            If Not oTrans Is Nothing Then oTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
                oTrans = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEView.aspx"
            Me.Response.Redirect("MappingUserIdHIPORTJIVEView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Session("Objvw_MappingUserIdHIPORTJIVE.MappingUserIdHIPORTJIVEEdit") = Nothing
            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                '    Transcope.Complete()
            End Using
            AddHandler PopUpUser1.SelectUser, AddressOf SelectUser
            If Not Page.IsPostBack Then
                LoadMode()
                MultiView1.ActiveViewIndex = 0
            End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub SelectUser(ByVal IntUserid As Long)
        HUserID.Value = IntUserid.ToString
        Using objUser As SahassaNettier.Entities.User = UserBLL.GetUserByPkUserID(IntUserid)
            If Not objUser Is Nothing Then
                LblUserID.Text = objUser.UserID
                LabelUserName.Text = objUser.UserName
            Else
                LblUserID.Text = ""
                LabelUserName.Text = ""
            End If
        End Using

    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEView.aspx"
            Me.Response.Redirect("MappingUserIdHIPORTJIVEView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try

            If Not Page.ClientScript.IsClientScriptIncludeRegistered("idpopup1") Then
                Page.ClientScript.RegisterClientScriptInclude(Me.GetType(), "idpopup1", ResolveClientUrl("script/popupdrag.js"))
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub btnBrowse_Click(sender As Object, e As System.EventArgs) Handles btnBrowse.Click
        Try

            Page.ClientScript.RegisterStartupScript(Me.GetType(), "idpopup2", "popUpDrag('" & CType(sender, LinkButton).ClientID & "','divBrowseUser');", True)
            PopUpUser1.initData()

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class


