Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Public Class DataUpdatingParameter
    Inherits Parent

#Region " Property "
    ''' <summary>
    ''' get focus id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TextAbnormalTrans.ClientID
        End Get
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cek approved
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    '''     [Hendry] 15/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Function CekApproved() As Boolean
        Using AccessParameters As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
            'Periksa apakah Data Updating Parameter ada dalam Pending Approval, ParameterItemID = 4 
            Dim count As Int32 = AccessParameters.CountParametersApproval(4)

            If count > 0 Then
                Return False
            Else
                Return True
            End If
        End Using
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get data Potential Customer Verification Parameter
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	03/07/2006	Created
    '''     [Hendry] 15/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GetData()
        Using AccessDataUpdatingParameter As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingParameterTableAdapter
            Using TableDataUpdatingParameter As AMLDAL.AMLDataSet.DataUpdatingParameterDataTable = AccessDataUpdatingParameter.GetData
                If TableDataUpdatingParameter.Rows.Count > 0 Then
                    Dim TableRowDataUpdatingParameter As AMLDAL.AMLDataSet.DataUpdatingParameterRow = TableDataUpdatingParameter.Rows(0)

                    ViewState("AbnormalTransactionCount_Old") = CType(TableRowDataUpdatingParameter.AbnormalTransactionCount, Int32)
                    Me.TextAbnormalTrans.Text = ViewState("AbnormalTransactionCount_Old")

                    ViewState("CreatedDate_Old") = CType(TableRowDataUpdatingParameter.CreatedDate, Date)
                    ViewState("LastUpdateDate_Old") = CType(TableRowDataUpdatingParameter.LastUpdateDate, Date)

                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load page handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>  
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Me.CekApproved() Then
                If Not Me.IsPostBack Then
                    Me.GetData()

                    Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                    'Using Transcope As New Transactions.TransactionScope
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                        Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                        AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                        'Transcope.Complete()
                    End Using
                    'End Using
                End If
            Else
                Throw New Exception("Cannot update Data Updating Parameter because it is currently waiting for approval")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cancel event handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "Default.aspx"

        Me.Response.Redirect("Default.aspx", False)
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' save handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>    
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry] 15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            If Me.CekApproved Then

                Page.Validate()

                If Page.IsValid Then
                    Dim AbnormalTransactionCount As Int32
                    Try
                        AbnormalTransactionCount = Me.TextAbnormalTrans.Text
                    Catch ex As Exception
                        Throw New Exception("Abnormal Transaction Count maximum is " & Int32.MaxValue.ToString())
                    End Try
                    'Using TranScope As New Transactions.TransactionScope
                    Using ParametersPending As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                        oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(ParametersPending, Data.IsolationLevel.ReadUncommitted)
                        'Perika apakah sudah ada entry dalam tabel DataUpdatingParameter, bila belum ada, maka buat entry baru dlm tabel DataUpdatingParameter,
                        'tapi bila entry sudah ada dalam tabel DataUpdatingParameter, maka update entry tsb
                        Using AccessDataUpdatingParameter As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingParameterTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessDataUpdatingParameter, oSQLTrans)
                            Using TableDataUpdatingParameter As AMLDAL.AMLDataSet.DataUpdatingParameterDataTable = AccessDataUpdatingParameter.GetData
                                Using DataUpdatingParameterApproval As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingParameter_ApprovalTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(DataUpdatingParameterApproval, oSQLTrans)
                                    'Buat variabel untuk menampung nilai-nilai baru

                                    Dim ModeID As Int16
                                    Dim DataUpdatingParameterApprovalID As Int64

                                    If Sahassa.AML.Commonly.SessionPkUserId = 1 Then ' SU
                                        If TableDataUpdatingParameter.Rows.Count > 0 Then ' Edit
                                            If Me.InsertAuditTrailSU("Edit", "Accept", oSQLTrans) Then
                                                If AccessDataUpdatingParameter.UpdateDataUpdatingParameter(AbnormalTransactionCount, Now) Then
                                                    Me.LblSucces.Visible = True
                                                    Me.LblSucces.Text = "Success to update data updating parameter."
                                                Else
                                                    Throw New Exception("Failed to update data updating parameter.")
                                                End If
                                            Else
                                                Throw New Exception("Failed to insert audit trail.")
                                            End If
                                        Else 'Add
                                            If Me.InsertAuditTrailSU("Add", "Accept", oSQLTrans) Then
                                                If AccessDataUpdatingParameter.Insert(AbnormalTransactionCount, Now, Now) Then
                                                    Me.LblSucces.Visible = True
                                                    Me.LblSucces.Text = "Success to insert data updating parameter."
                                                Else
                                                    Throw New Exception("Failed to insert data updating parameter.")
                                                End If
                                            Else
                                                Throw New Exception("Failed to insert audit trail.")
                                            End If
                                        End If
                                    Else ' Bukan SU
                                        Dim IDMessage As Integer
                                        'Bila tabel DataUpdatingParameter tidak kosong, maka update entry tsb & set ModeID = 2 (Edit)
                                        If TableDataUpdatingParameter.Rows.Count > 0 Then
                                            'Buat variabel untuk menampung nilai-nilai lama
                                            Dim AbnormalTransactionCount_Old As Int32 = ViewState("AbnormalTransactionCount_Old")
                                            Dim CreatedDate_Old As DateTime = ViewState("CreatedDate_Old")
                                            Dim LastUpdate_Old As DateTime = ViewState("LastUpdateDate_Old")
                                            ModeID = 2
                                            IDMessage = 8842
                                            'DataUpdatingParameterApprovalID adalah primary key dari tabel DataUpdatingParameter_Approval yang sifatnya identity dan baru dibentuk setelah row baru dibuat
                                            DataUpdatingParameterApprovalID = DataUpdatingParameterApproval.InsertDataUpdatingParameter_Approval(AbnormalTransactionCount, CreatedDate_Old, Now, AbnormalTransactionCount_Old, CreatedDate_Old, LastUpdate_Old)
                                        Else  'Bila tabel DataUpdatingParameter kosong, maka buat entry baru & set ModeID = 1 (Add)
                                            ModeID = 1
                                            IDMessage = 8841
                                            'DataUpdatingParameterApprovalID adalah primary key dari tabel DataUpdatingParameter_Approval yang sifatnya identity dan baru dibentuk setelah row baru dibuat
                                            DataUpdatingParameterApprovalID = DataUpdatingParameterApproval.InsertDataUpdatingParameter_Approval(AbnormalTransactionCount, Now, Now, Nothing, Nothing, Nothing)
                                        End If

                                        'ParametersPendingApprovalID adalah primary key dari tabel Parameters_PendingApproval yang sifatnya identity dan baru dibentuk setelah row baru dibuat 
                                        Dim ParametersPendingApprovalID As Int64 = ParametersPending.InsertParameters_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, ModeID)

                                        Using ParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                                            Sahassa.AML.TableAdapterHelper.SetTransaction(ParametersApproval, oSQLTrans)
                                            'ParameterItemID 4 = DataUpdatingParameter
                                            ParametersApproval.Insert(ParametersPendingApprovalID, ModeID, 4, DataUpdatingParameterApprovalID)
                                        End Using

                                        Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & IDMessage

                                        Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & IDMessage, False)
                                    End If
                                End Using
                            End Using
                        End Using
                    End Using

                    oSQLTrans.Commit()
                    'End Using
                End If
            Else
                Throw New Exception("Cannot update Data Updating Parameter because it is currently waiting for approval")
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    ''' <summary>
    ''' Insert Audit Trail SU
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function InsertAuditTrailSU(ByVal mode As String, ByVal Action As String, ByRef oSQLTrans As SqlTransaction) As Boolean
        Try
            'Using TransScope As New Transactions.TransactionScope
            Using AccessDataUpdatingParameter As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingParameterTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessDataUpdatingParameter, oSQLTrans)
                Dim row() As AMLDAL.AMLDataSet.DataUpdatingParameterRow = AccessDataUpdatingParameter.GetData.Select
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                    If mode.ToLower = "add" Then
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdating", "AbnormalTransactionCount", mode, "", Me.TextAbnormalTrans.Text, Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdating", "CreatedDate", mode, Now.ToString("dd-MMMM-yyyy HH:mm"), Now.ToString("dd-MMMM-yyyy HH:mm"), Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdating", "LastUpdateDate", mode, Now.ToString("dd-MMMM-yyyy HH:mm"), Now.ToString("dd-MMMM-yyyy HH:mm"), Action)
                    Else 'Edit
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdating", "MinimumResultPercentage", mode, row(0).AbnormalTransactionCount.ToString, Me.TextAbnormalTrans.Text, Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdating", "CreatedDate", mode, row(0).CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), row(0).CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), Action)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdating", "LastUpdateDate", mode, row(0).LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), Now.ToString("dd-MMMM-yyyy HH:mm"), Action)
                    End If
                End Using
            End Using
            Return True
            'End Using
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class