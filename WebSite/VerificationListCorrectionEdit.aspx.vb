﻿
Imports EkaDataNettier.Entities
Imports AMLBLL
Imports Sahassa.AML.Commonly
Imports System.Data

Partial Class VerificationListCorrectionEdit
    Inherits Parent



    Public ReadOnly Property PendingApprovalID() As Integer
        Get
            Dim strtemp As String = Request.Params("PendingApprovalID")
            Dim intresult As Integer
            If Integer.TryParse(strtemp, intresult) Then
                Return intresult
            Else
                Throw New Exception("PendingApprovalID must integer")
            End If
        End Get
    End Property


    Public Property ObjTVerificationListReviewNotes() As TList(Of VerificationListReviewNotes)
        Get
            If Session("VerificationListCorrectionEdit.ObjTVerificationListReviewNotes") Is Nothing Then
                Session("VerificationListCorrectionEdit.ObjTVerificationListReviewNotes") = VerificationListBLL.GetTlistVerificationListReviewNotes(VerificationListReviewNotesColumn.FKPendingApprovalID.ToString & "=" & Me.PendingApprovalID, "", 0, Integer.MaxValue, 0)
                Return CType(Session("VerificationListCorrectionEdit.ObjTVerificationListReviewNotes"), TList(Of VerificationListReviewNotes))
            Else
                Return CType(Session("VerificationListCorrectionEdit.ObjTVerificationListReviewNotes"), TList(Of VerificationListReviewNotes))
            End If
        End Get
        Set(value As TList(Of VerificationListReviewNotes))
            Session("VerificationListCorrectionEdit.ObjTVerificationListReviewNotes") = value
        End Set
    End Property


    Public Property ObjTVerificationListCategory_Approval() As TList(Of VerificationListCategory_Approval)
        Get
            If Session("VerificationListCorrectionEdit.ObjTVerificationListCategory_Approval") Is Nothing Then
                Session("VerificationListCorrectionEdit.ObjTVerificationListCategory_Approval") = VerificationListBLL.GetTlistVerificationListCategory_Approval(VerificationListCategory_ApprovalColumn.Category_PendingApprovalID.ToString & "=" & PendingApprovalID, "", 0, Integer.MaxValue, 0)
                Return CType(Session("VerificationListCorrectionEdit.ObjTVerificationListCategory_Approval"), TList(Of VerificationListCategory_Approval))
            Else
                Return CType(Session("VerificationListCorrectionEdit.ObjTVerificationListCategory_Approval"), TList(Of VerificationListCategory_Approval))
            End If
        End Get
        Set(value As TList(Of VerificationListCategory_Approval))
            Session("VerificationListCorrectionEdit.ObjTVerificationListCategory_Approval") = value
        End Set
    End Property


    Public Property ObjTVerificationList_IDNumber_Approval() As TList(Of VerificationList_IDNumber_Approval)
        Get
            If Session("VerificationListCorrectionEdit.ObjTVerificationList_IDNumber_Approval") Is Nothing Then
                Session("VerificationListCorrectionEdit.ObjTVerificationList_IDNumber_Approval") = VerificationListBLL.GetTlistVerificationList_IDNumber_Approval(VerificationList_IDNumber_ApprovalColumn.PendingApprovalID.ToString & "=" & PendingApprovalID, "", 0, Integer.MaxValue, 0)
                Return CType(Session("VerificationListCorrectionEdit.ObjTVerificationList_IDNumber_Approval"), TList(Of VerificationList_IDNumber_Approval))
            Else
                Return CType(Session("VerificationListCorrectionEdit.ObjTVerificationList_IDNumber_Approval"), TList(Of VerificationList_IDNumber_Approval))
            End If
        End Get
        Set(value As TList(Of VerificationList_IDNumber_Approval))
            Session("VerificationListCorrectionEdit.ObjTVerificationList_IDNumber_Approval") = value
        End Set
    End Property



    Public Property ObjTVerificationList_Address_Approval() As TList(Of VerificationList_Address_Approval)
        Get
            If Session("VerificationListCorrectionEdit.ObjTVerificationList_Address_Approval") Is Nothing Then
                Session("VerificationListCorrectionEdit.ObjTVerificationList_Address_Approval") = VerificationListBLL.GetTlistVerificationList_Address_Approval(VerificationList_Address_ApprovalColumn.PendingApprovalID.ToString & "=" & PendingApprovalID, "", 0, Integer.MaxValue, 0)
                Return CType(Session("VerificationListCorrectionEdit.ObjTVerificationList_Address_Approval"), TList(Of VerificationList_Address_Approval))
            Else
                Return CType(Session("VerificationListCorrectionEdit.ObjTVerificationList_Address_Approval"), TList(Of VerificationList_Address_Approval))
            End If
        End Get
        Set(value As TList(Of VerificationList_Address_Approval))
            Session("VerificationListCorrectionEdit.ObjTVerificationList_Address_Approval") = value
        End Set
    End Property


    Public Property ObjTVerificationList_Alias_Approval() As TList(Of VerificationList_Alias_Approval)
        Get
            If Session("VerificationListCorrectionEdit.ObjTVerificationList_Alias_Approval") Is Nothing Then
                Session("VerificationListCorrectionEdit.ObjTVerificationList_Alias_Approval") = VerificationListBLL.GetTlistVerificationList_Alias_Approval(VerificationList_Alias_ApprovalColumn.PendingApprovalID.ToString & "=" & Me.PendingApprovalID, "", 0, Integer.MaxValue, 0)
                Return CType(Session("VerificationListCorrectionEdit.ObjTVerificationList_Alias_Approval"), TList(Of VerificationList_Alias_Approval))
            Else
                Return CType(Session("VerificationListCorrectionEdit.ObjTVerificationList_Alias_Approval"), TList(Of VerificationList_Alias_Approval))
            End If
        End Get
        Set(value As TList(Of VerificationList_Alias_Approval))
            Session("VerificationListCorrectionEdit.ObjTVerificationList_Alias_Approval") = value
        End Set
    End Property

    Public Property ObjVerificationList_Master_Approval() As VerificationList_Master_Approval
        Get
            If Session("VerificationListCorrectionEdit.ObjVerificationList_Master_Approval") Is Nothing Then
                Using objtemp As TList(Of VerificationList_Master_Approval) = VerificationListBLL.GetTlistVerificationList_Master_Approval(VerificationList_Master_ApprovalColumn.PendingApprovalID.ToString & "=" & PendingApprovalID, "", 0, Integer.MaxValue, 0)
                    If objtemp.Count > 0 Then
                        Session("VerificationListCorrectionEdit.ObjVerificationList_Master_Approval") = objtemp(0)
                    End If
                End Using

                Return CType(Session("VerificationListCorrectionEdit.ObjVerificationList_Master_Approval"), VerificationList_Master_Approval)
            Else
                Return CType(Session("VerificationListCorrectionEdit.ObjVerificationList_Master_Approval"), VerificationList_Master_Approval)
            End If
        End Get
        Set(value As VerificationList_Master_Approval)
            Session("VerificationListCorrectionEdit.ObjVerificationList_Master_Approval") = value
        End Set
    End Property



    Public Property ObjTCustomRemarks() As Data.DataTable
        Get
            If Session("VerificationListCorrectionEdit.ObjTCustomRemarks") Is Nothing Then
                Session("VerificationListCorrectionEdit.ObjTCustomRemarks") = CreateDataTableCustomRemarks()
            End If
            Return Session("VerificationListCorrectionEdit.ObjTCustomRemarks")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("VerificationListCorrectionEdit.ObjTCustomRemarks") = value
        End Set
    End Property


    Sub clearSession()
        ObjTVerificationListReviewNotes = Nothing
        ObjTCustomRemarks = Nothing
        ObjTVerificationList_Address_Approval = Nothing
        ObjTVerificationList_Alias_Approval = Nothing
        ObjTVerificationList_IDNumber_Approval = Nothing
        ObjTVerificationListCategory_Approval = Nothing
        ObjVerificationList_Master_Approval = Nothing
    End Sub
    Private Function CreateDataTableCustomRemarks() As DataTable
        Dim CustomRemarksDatatable_Edit As DataTable = New DataTable()

        Dim CustomRemarksDataColumn As DataColumn

        CustomRemarksDataColumn = New DataColumn()
        CustomRemarksDataColumn.DataType = Type.GetType("System.String")
        CustomRemarksDataColumn.ColumnName = "id"
        CustomRemarksDatatable_Edit.Columns.Add(CustomRemarksDataColumn)

        CustomRemarksDataColumn = New DataColumn()
        CustomRemarksDataColumn.DataType = Type.GetType("System.String")
        CustomRemarksDataColumn.ColumnName = "CustomRemarks"
        CustomRemarksDatatable_Edit.Columns.Add(CustomRemarksDataColumn)

        Return CustomRemarksDatatable_Edit
    End Function
    Sub BindAlias()
        GridViewAliases.DataSource = ObjTVerificationList_Alias_Approval
        GridViewAliases.DataBind()
    End Sub
    Sub BindAddress()

        GridViewAddresses.DataSource = ObjTVerificationList_Address_Approval
        GridViewAddresses.DataBind()
    End Sub
    Sub BindID()

        GridViewIDNos.DataSource = ObjTVerificationList_IDNumber_Approval
        GridViewIDNos.DataBind()
    End Sub
    Sub BindRemark()
        GridViewCustomRemarks.DataSource = ObjTCustomRemarks
        GridViewCustomRemarks.DataBind()
    End Sub

    Sub BindReviewNotes()
        GridViewReviewNotes.DataSource = ObjTVerificationListReviewNotes
        GridViewReviewNotes.DataBind()
    End Sub

    Sub LoadData()
        If Not ObjVerificationList_Master_Approval Is Nothing Then
            LabelVerificationListID.Text = ObjVerificationList_Master_Approval.VerificationListId
            BindAlias()
            TextDateOfData.Text = ObjVerificationList_Master_Approval.DateOfData.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy").Replace("01-Jan-1900", "")
            TextCustomerDOB.Text = ObjVerificationList_Master_Approval.DateOfBirth.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy").Replace("01-Jan-1900", "")
            TxtBirthPlace.Text = ObjVerificationList_Master_Approval.BirthPlace
            BindAddress()
            BindID()


            Try
                DropDownListType.SelectedValue = ObjVerificationList_Master_Approval.VerificationListTypeId.GetValueOrDefault(0)
            Catch ex As Exception
            End Try
            Try
                DropDownCategoryID.SelectedValue = ObjVerificationList_Master_Approval.VerificationListCategoryId.GetValueOrDefault(0)
            Catch ex As Exception
            End Try

            If ObjVerificationList_Master_Approval.CustomRemark1 <> "" Then
                Dim objnewrow As DataRow = ObjTCustomRemarks.NewRow
                objnewrow("id") = 1
                objnewrow("CustomRemarks") = ObjVerificationList_Master_Approval.CustomRemark1
                ObjTCustomRemarks.Rows.Add(objnewrow)
            End If

            If ObjVerificationList_Master_Approval.CustomRemark2 <> "" Then
                Dim objnewrow As DataRow = ObjTCustomRemarks.NewRow
                objnewrow("id") = 2
                objnewrow("CustomRemarks") = ObjVerificationList_Master_Approval.CustomRemark2
                ObjTCustomRemarks.Rows.Add(objnewrow)
            End If
            If ObjVerificationList_Master_Approval.CustomRemark3 <> "" Then
                Dim objnewrow As DataRow = ObjTCustomRemarks.NewRow
                objnewrow("id") = 3
                objnewrow("CustomRemarks") = ObjVerificationList_Master_Approval.CustomRemark3
                ObjTCustomRemarks.Rows.Add(objnewrow)
            End If

            If ObjVerificationList_Master_Approval.CustomRemark4 <> "" Then
                Dim objnewrow As DataRow = ObjTCustomRemarks.NewRow
                objnewrow("id") = 4
                objnewrow("CustomRemarks") = ObjVerificationList_Master_Approval.CustomRemark4
                ObjTCustomRemarks.Rows.Add(objnewrow)
            End If
            If ObjVerificationList_Master_Approval.CustomRemark5 <> "" Then
                Dim objnewrow As DataRow = ObjTCustomRemarks.NewRow
                objnewrow("id") = 5
                objnewrow("CustomRemarks") = ObjVerificationList_Master_Approval.CustomRemark5
                ObjTCustomRemarks.Rows.Add(objnewrow)
            End If
        

            BindRemark()
            BindReviewNotes()

            TxtNationality.Text = ObjVerificationList_Master_Approval.Nationality



        End If
    End Sub
    Sub LoadVerificationListType()
        DropDownListType.Items.Clear()
        DropDownListType.Items.Add(New ListItem("Please Select Verification List Type", "0"))
        DropDownListType.AppendDataBoundItems = True
        DropDownListType.DataSource = VerificationListBLL.GetTlistVerificationListType("", "", 0, Integer.MaxValue, 0)
        DropDownListType.DataValueField = VerificationListTypeColumn.pk_Verification_List_Type_Id.ToString
        DropDownListType.DataTextField = VerificationListTypeColumn.ListTypeName.ToString
        DropDownListType.DataBind()
    End Sub
    Sub LoadVerificationListCategory()
        DropDownCategoryID.Items.Clear()
        DropDownCategoryID.Items.Add(New ListItem("Please Select Category", "0"))
        DropDownCategoryID.AppendDataBoundItems = True
        DropDownCategoryID.DataSource = VerificationListBLL.GetTlistVerificationListCategory("", "", 0, Integer.MaxValue, 0)
        DropDownCategoryID.DataTextField = VerificationListCategoryColumn.CategoryName.ToString
        DropDownCategoryID.DataValueField = VerificationListCategoryColumn.CategoryID.ToString
        DropDownCategoryID.DataBind()
    End Sub


    Protected Sub Page_Load1(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                clearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Me.cmdDODDatePicker.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextDateOfData.ClientID & "'), 'dd-mmm-yyyy')")
                Me.cmdDOBDatePicker.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextCustomerDOB.ClientID & "'), 'dd-mmm-yyyy')")

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '        Transcope.Complete()
                End Using
                LoadVerificationListType()
                LoadVerificationListCategory()
                LoadData()
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub GridViewAliases_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewAliases.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim objLblNo As Label = CType(e.Item.FindControl("labelno"), Label)
                objLblNo.Text = e.Item.ItemIndex + 1

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewAddresses_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewAddresses.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim objLblNo As Label = CType(e.Item.FindControl("labelno"), Label)
                objLblNo.Text = e.Item.ItemIndex + 1
                Dim objtemp As VerificationList_Address_Approval = CType(e.Item.DataItem, VerificationList_Address_Approval)

                Dim objLblAdddressType As Label = CType(e.Item.FindControl("LabelIDType"), Label)
                Using objAddressType As AddressType = VerificationListBLL.GetAddressTypeByPk(objtemp.AddressTypeId)
                    If Not objAddressType Is Nothing Then
                        objLblAdddressType.Text = objAddressType.AddressTypeDescription
                    End If
                End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewIDNos_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewIDNos.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim objLblNo As Label = CType(e.Item.FindControl("labelno"), Label)
                objLblNo.Text = e.Item.ItemIndex + 1
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewAliases_DeleteCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewAliases.DeleteCommand
        Try

            Dim pkdel As Long = GridViewAliases.DataKeys(e.Item.ItemIndex).ToString

            Using objdel As VerificationList_Alias_Approval = ObjTVerificationList_Alias_Approval.Find(VerificationList_Alias_ApprovalColumn.ApprovalID, pkdel)
                If Not objdel Is Nothing Then
                    ObjTVerificationList_Alias_Approval.Remove(objdel)
                End If
            End Using
            BindAlias()

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub LinkButtonAddAlias_Click(sender As Object, e As System.EventArgs) Handles LinkButtonAddAlias.Click
        Try
            If TextBoxAlias.Text = "" Then
                Throw New Exception("Please Enter Alias Name")
            End If
            Using objnewalias As VerificationList_Alias_Approval = ObjTVerificationList_Alias_Approval.AddNew
                With objnewalias


                    Dim generator As New Random
                    Dim randomValue As Integer
                    randomValue = generator.Next(1, Integer.MaxValue)
                    While Not ObjTVerificationList_Alias_Approval.Find(VerificationList_Alias_ApprovalColumn.ApprovalID, CLng(randomValue)) Is Nothing
                        randomValue = generator.Next(1, Integer.MaxValue)
                    End While

                    .ApprovalID = randomValue
                    .PendingApprovalID = Me.PendingApprovalID
                    .ModeID = 1
                    .VerificationList_Alias_ID = 0
                    .VerificationListId = ObjVerificationList_Master_Approval.VerificationListId
                    .Name = TextBoxAlias.Text
                    .VerificationList_Alias_ID_Old = Nothing
                    .VerificationListId_Old = Nothing
                    .Name_Old = Nothing
                End With
            End Using
            BindAlias()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LinkButtonAddAddress_Click(sender As Object, e As System.EventArgs) Handles LinkButtonAddAddress.Click
        Try
            If TextAddress.Text.Trim = "" Then
                Throw New Exception("Please Enter Address")
            End If
            Using objnewaddress As VerificationList_Address_Approval = ObjTVerificationList_Address_Approval.AddNew
                With objnewaddress
                    Dim generator As New Random
                    Dim randomValue As Integer
                    randomValue = generator.Next(1, Integer.MaxValue)

                    While Not ObjTVerificationList_Address_Approval.Find(VerificationList_Address_ApprovalColumn.ApprovalID, CLng(randomValue)) Is Nothing
                        randomValue = generator.Next(1, Integer.MaxValue)
                    End While

                    .ApprovalID = randomValue
                    .PendingApprovalID = Me.PendingApprovalID
                    .ModeID = 1
                    .VerificationList_Address_Id = 0
                    .VerificationListId = ObjVerificationList_Master_Approval.VerificationListId
                    .Address = TextAddress.Text
                    .AddressTypeId = DropDownListAddressType.SelectedValue
                    .IsLocalAddress = CheckBoxIsLocalAddress.Checked
                    .VerificationList_Address_Id_Old = Nothing
                    .VerificationListId_Old = Nothing
                    .Address_Old = Nothing
                    .AddressTypeId_Old = Nothing
                    .IsLocalAddress_Old = Nothing

                End With
            End Using
            BindAddress()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LinkButtonAddIDNumber_Click(sender As Object, e As System.EventArgs) Handles LinkButtonAddIDNumber.Click
        Try
            If TextBoxIDNo.Text.Trim = "" Then
                Throw New Exception("Please Enter ID Number.")
            End If

            Using objnew As VerificationList_IDNumber_Approval = ObjTVerificationList_IDNumber_Approval.AddNew
                With objnew


                    Dim generator As New Random
                    Dim randomValue As Integer
                    randomValue = generator.Next(1, Integer.MaxValue)


                    While Not ObjTVerificationList_IDNumber_Approval.Find(VerificationList_IDNumber_ApprovalColumn.ApprovalID, CLng(randomValue)) Is Nothing
                        randomValue = generator.Next(1, Integer.MaxValue)

                    End While


                    .ApprovalID = randomValue
                    .PendingApprovalID = Me.PendingApprovalID
                    .ModeID = 1
                    .VerificationList_IDNumber_ID = 0
                    .VerificationListId = ObjVerificationList_Master_Approval.VerificationListId
                    .IDNumber = TextBoxIDNo.Text.Trim
                    .VerificationList_IDNumber_ID_Old = Nothing
                    .VerificationListId_Old = Nothing
                    .IDNumber_Old = Nothing

                End With
            End Using
            BindID()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub LinkButtonAddCustomRemarks_Click(sender As Object, e As System.EventArgs) Handles LinkButtonAddCustomRemarks.Click
        Try
            If TextboxCustomRemarks.Text.Trim = "" Then
                Throw New Exception("Please Enter Custom Remarks")
            End If
            If ObjTCustomRemarks.Rows.Count = 5 Then
                Throw New Exception("Custom Remarks Max 5 record.")
            End If
            Dim objnewcustomremark As DataRow = ObjTCustomRemarks.NewRow
            objnewcustomremark("ID") = ObjTCustomRemarks.Rows.Count + 1
            objnewcustomremark("CustomRemarks") = TextboxCustomRemarks.Text.Trim
            ObjTCustomRemarks.Rows.Add(objnewcustomremark)
            BindRemark()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewAddresses_DeleteCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewAddresses.DeleteCommand
        Try
            'Dim pkdel As Long = GridViewAddresses.DataKeys(e.Item.ItemIndex).ToString
            ObjTVerificationList_Address_Approval.RemoveAt(e.Item.ItemIndex)
            BindAddress()

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewIDNos_DeleteCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewIDNos.DeleteCommand
        Try
            ObjTVerificationList_IDNumber_Approval.RemoveAt(e.Item.ItemIndex)
            BindID()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewCustomRemarks_DeleteCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewCustomRemarks.DeleteCommand
        Try
            ObjTCustomRemarks.Rows.RemoveAt(e.Item.ItemIndex)
            Dim intjml As Integer = 1
            For Each item As DataRow In ObjTCustomRemarks.Rows
                item("id") = intjml
                intjml += 1
            Next
            BindRemark()

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub




    Function IsDataValid() As Boolean
        Dim strErrorMessage As New StringBuilder

        If ObjTVerificationList_Alias_Approval.Count = 0 Then
            strErrorMessage.Append("Name /Alias Minimal 1.</br>")
        End If
        If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TextDateOfData.Text.Trim) Then
            strErrorMessage.Append("Date Of Data must dd-MMM-yyyy.</br>")
        End If
        If TextCustomerDOB.Text.Trim.Length = 0 Then

        ElseIf Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TextCustomerDOB.Text.Trim) Then
            strErrorMessage.Append("Date Of Birth must dd-MMM-yyyy.</br>")
        End If


        'If TxtBirthPlace.Text.Trim = "" Then
        '    strErrorMessage.Append("Please Enter Birth Place.</br>")
        'End If
        If DropDownListType.SelectedValue = "0" Then
            strErrorMessage.Append("Please Enter Verification List Type.</br>")
        End If
        If DropDownCategoryID.SelectedValue = "0" Then
            strErrorMessage.Append("Please Enter Category.</br>")
        End If



        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New SahassaException(strErrorMessage.ToString)
        Else
            Return True
        End If

    End Function


    Protected Sub ImageSave_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If IsDataValid Then
                With ObjVerificationList_Master_Approval



                    

                    .DateUpdated = Now
                    .DateOfData = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TextDateOfData.Text.Trim)
                    .DisplayName = ObjTVerificationList_Alias_Approval(0).Name
                    If TextCustomerDOB.Text.Trim.Length > 0 Then
                        .DateOfBirth = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TextCustomerDOB.Text.Trim)
                    Else
                        .DateOfBirth = Nothing
                    End If

                    .BirthPlace = TxtBirthPlace.Text.Trim
                    .VerificationListTypeId = DropDownListType.SelectedValue
                    .VerificationListCategoryId = DropDownCategoryID.SelectedValue
                    .Nationality = TxtNationality.Text.Trim

                    For Each item As DataRow In ObjTCustomRemarks.Rows
                        If item("id") = 1 Then
                            .CustomRemark1 = item("CustomRemarks")
                        End If
                        If item("id") = 2 Then
                            .CustomRemark2 = item("CustomRemarks")
                        End If
                        If item("id") = 3 Then
                            .CustomRemark3 = item("CustomRemarks")
                        End If
                        If item("id") = 4 Then
                            .CustomRemark4 = item("CustomRemarks")
                        End If
                        If item("id") = 5 Then
                            .CustomRemark5 = item("CustomRemarks")
                        End If
                    Next




                End With
                VerificationListBLL.SaveCorrection(Me.PendingApprovalID, ObjVerificationList_Master_Approval, ObjTVerificationList_Alias_Approval, ObjTVerificationList_Address_Approval, ObjTVerificationList_IDNumber_Approval)
                Response.Redirect("VerificationListCorrectionView.aspx", False)
            End If


        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewReviewNotes_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewReviewNotes.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim objLblNo As Label = CType(e.Row.FindControl("LabelNo"), Label)
                objLblNo.Text = e.Row.RowIndex + 1
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageCancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Response.Redirect("VerificationListCorrectionView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
