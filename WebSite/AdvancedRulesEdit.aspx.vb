Imports System.Data.SqlClient
Imports Sahassa.AML.TableAdapterHelper
Partial Class AdvancedRulesEdit
    Inherits Parent
    Public ReadOnly Property RulesAdvancedID() As String
        Get
            Dim temp As String

            temp = Request.Params("AdvancedRulesID")
            If Not IsNumeric(temp) Then
                Try
                    Throw New Exception("Rules Advanced ID not valid")
                Catch ex As Exception
                    Return ""
                End Try
            Else
                Return temp
            End If
        End Get
    End Property
    Public Property oAdvancedRules() As AMLDAL.RulesAdvanced.RulesAdvancedRow
        Get
            If Not Session("RowRulesAdvanced") Is Nothing Then

                Return Session("RowRulesAdvanced")
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As AMLDAL.RulesAdvanced.RulesAdvancedRow)
            Session("RowRulesAdvanced") = value
        End Set
    End Property
    Public ReadOnly Property oQueryBuilder() As Sahassa.AML.QueryBuilder
        Get
            If Session("AdvanceRuleQueryBuilderEdit") Is Nothing Then
                Return New Sahassa.AML.QueryBuilder
            Else
                Return Session("AdvanceRuleQueryBuilderEdit")
            End If
        End Get

    End Property
    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu1.MenuItemClick
        MultiView1.ActiveViewIndex = Int32.Parse(e.Item.Value)
        Tablestest1.ActiveIndex = MultiView1.ActiveViewIndex
        Fields1.ActiveIndex = MultiView1.ActiveViewIndex
        WhereClausesTest1.ActiveIndex = MultiView1.ActiveViewIndex
        GroupBy1.ActiveIndex = MultiView1.ActiveViewIndex
        HavingCondition1.ActiveIndex = MultiView1.ActiveViewIndex
        SQLExpression1.ActiveIndex = MultiView1.ActiveViewIndex

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
        
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            LoadData()
            SetSession()
        End If
    End Sub
    Private Sub InsertAuditTrail(ByRef oSQLTrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)

            Dim AdvanceRuleDescription As String
            If Me.TextDescription.Text.Trim.Length <= 255 Then
                AdvanceRuleDescription = Me.TextDescription.Text.Trim
            Else
                AdvanceRuleDescription = Me.TextDescription.Text.Substring(0, 255)
            End If
            If Not oAdvancedRules Is Nothing Then


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    SetTransaction(AccessAudit, oSQLTrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Advanced Rule Name", "Edit", oAdvancedRules.RulesAdvancedName.Trim, Me.TextRuleAdvanceName.Text.Trim, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Description", "Edit", oAdvancedRules.RulesAdvancedDescription.Trim, AdvanceRuleDescription, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Enabled", "Edit", oAdvancedRules.Enabled, chkEnabled.Checked, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Case Grouping By", "Edit", oAdvancedRules.CaseGroupingBy, Me.cboGroupingBy.SelectedValue, "Accepted")
                End Using
            End If
        Catch
            Throw
        End Try
    End Sub
    Private Sub LoadData()
        Try
            'Get Data Advanced RulesBy Advancedrules ID
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedTableAdapter
                For Each orows As AMLDAL.RulesAdvanced.RulesAdvancedRow In adapter.GetDataByRulesAdvancedID(Me.RulesAdvancedID).Rows
                    oQueryBuilder.SQlQuery = orows.Expression
                    oQueryBuilder.SetttingDataset()
                    Me.TextRuleAdvanceName.Text = orows.RulesAdvancedName
                    Me.cboGroupingBy.SelectedValue = orows.CaseGroupingBy
                    Me.TextDescription.Text = orows.RulesAdvancedDescription
                    Me.chkEnabled.Checked = orows.Enabled
                    Me.oAdvancedRules = orows
                Next
            End Using
            'load Data

        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub CustomValidator1_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustomValidator1.ServerValidate
        If oQueryBuilder.SQlQuery = "" Then
            args.IsValid = False

            CustomValidator1.ErrorMessage = "Please configure sql expression first!"

        Else
            Try
                oQueryBuilder.ParseSQLQuery()
                args.IsValid = True
                CustomValidator1.ErrorMessage = ""
            Catch ex As Exception
                CustomValidator1.ErrorMessage = "There is an error in sql expression.Please configure sql expression again !"
                args.IsValid = False
            End Try



        End If
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If Page.IsValid AndAlso IsDataValid() Then
                'jika superuser maka 
                If Sahassa.AML.Commonly.SessionUserId.ToLower = "superuser" Then
                    UpdateAdvanceRulesBySU()
                Else
                    UpdateAdvaceRulesToPendingApproval()
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try


    End Sub
    Private Sub SetSession()
        Tablestest1.OQueryBuilder = Me.oQueryBuilder
        Fields1.OQueryBuilder = Me.oQueryBuilder
        WhereClausesTest1.OQueryBuilder = Me.oQueryBuilder
        GroupBy1.OQueryBuilder = Me.oQueryBuilder
        HavingCondition1.OQueryBuilder = Me.oQueryBuilder
    End Sub
    Private Sub AbandonSession()
        Session("RowRulesAdvanced") = Nothing
        Tablestest1.OQueryBuilder = Nothing
        Fields1.OQueryBuilder = Nothing
        WhereClausesTest1.OQueryBuilder = Nothing
        GroupBy1.OQueryBuilder = Nothing
        HavingCondition1.OQueryBuilder = Nothing
        SQLExpression1.OQueryBuilder = Nothing
    End Sub
    ''' <summary>
    ''' Update data AdvanceRule yang dilakukan oleh superuser
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateAdvanceRulesBySU()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedTableAdapter
                oSQLTrans = BeginTransaction(adapter)
                adapter.UpdateAdvancedRulesbyAdvanceRulesID(TextRuleAdvanceName.Text.Trim, Me.TextDescription.Text.Trim, chkEnabled.Checked, Me.cboGroupingBy.SelectedValue, oQueryBuilder.SQlQuery, oAdvancedRules.RulesAdvancedId)
                AbandonSession()
                InsertAuditTrail(oSQLTrans)
                oSQLTrans.Commit()
                SetSession()
                Response.Redirect("AdvancedRulesView.aspx", False)
            End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try

    End Sub
    ''' <summary>
    ''' update data yang dilakukan user selain superuser
    ''' </summary>

    Private Sub UpdateAdvaceRulesToPendingApproval()
        'insert ke header 
        'insert ke detail
        Dim intHeader As Integer
        Dim oSQLTrans As SqlTransaction = Nothing
        Try


            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedPendingApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                intHeader = adapter.InsertRulesAdvancedPendingApproval(oAdvancedRules.RulesAdvancedName, Sahassa.AML.Commonly.SessionUserId, 2, "Advanced Rules Edit", Now)
            End Using

            'insert to detail
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(adapter, oSQLTrans)
                adapter.Insert(intHeader, Me.RulesAdvancedID, TextRuleAdvanceName.Text.Trim, TextDescription.Text.Trim, chkEnabled.Checked, cboGroupingBy.SelectedValue, SQLExpression1.OQueryBuilder.SQlQuery, oAdvancedRules.CreatedDate, False, oAdvancedRules.RulesAdvancedId, oAdvancedRules.RulesAdvancedName, oAdvancedRules.RulesAdvancedDescription, oAdvancedRules.Enabled, oAdvancedRules.CaseGroupingBy, oAdvancedRules.Expression, oAdvancedRules.CreatedDate, oAdvancedRules.IsCreatedBySU)
            End Using
            oSQLTrans.Commit()
            Dim MessagePendingID As Integer = 81402 'MessagePendingID 81402 = Advanced Rules Edit

            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & TextRuleAdvanceName.Text.Trim
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & TextRuleAdvanceName.Text.Trim, False)


        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    Private Function IsDataValid() As Boolean
        Try
            'cek rules name sudah ada belum di rulesadvance selain data yang sedang di edit
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedTableAdapter

                Dim intJmlAdvanceRule As Nullable(Of Integer) = adapter.CountRulesAdvancedExceptOriginal(TextRuleAdvanceName.Text.Trim, oAdvancedRules.RulesAdvancedName)
                If intJmlAdvanceRule.GetValueOrDefault(0) > 0 Then

                    cvalPageError.IsValid = False
                    cvalPageError.ErrorMessage = "Rules Name already exits in Rules Advanced"
                    Return False
                End If
            End Using
            'cek apakah rules name lama ada di table approval
            Using Adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedPendingApprovalTableAdapter
                Dim intJmlApproval As Integer = Adapter.GetCountRulesAdvancePendingByUniqueKey(oAdvancedRules.RulesAdvancedName)
                If intJmlApproval > 0 Then

                    cvalPageError.IsValid = False
                    cvalPageError.ErrorMessage = "Rules Name already exits in Rules Advanced Pending Approval."
                    Return False
                End If
            End Using
            'cek apakah cbogrouping by = field yang ada
            If Not Me.oQueryBuilder.SqlFieldString.ToLower.Contains(cboGroupingBy.SelectedValue.ToLower) Then

                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Selected Case Grouping by not equal with selected Field."
                Return False
            End If
            'cek fieldnya harus hanya 1
            If Not Me.oQueryBuilder.SQLFieldDataset.Tables(0).Rows.Count = 1 Then
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "selected field must 1 field."
                Return False
            End If
            'cek tablesnya harus mengandung transactiondetail
            If Not Me.oQueryBuilder.SqlTableString.ToLower.Contains("transactiondetail") Then
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Tables must contain 'transactiondetail'."
                Return False
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("AdvancedRulesView.aspx", False)
    End Sub
End Class
