﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DataUpdatingAlertEdit.aspx.vb" Inherits="DataUpdatingAlertEdit" ValidateRequest="false" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
 <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td style="width: 7px">
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="20" height="100%" /></td>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%">
                                    <asp:MultiView ID="MtvTransactionAmount" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="VwAdd" runat="server">
                                            <table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                                width="100%" bgcolor="#dddddd" >
                                                <tr>
                                                    <td bgcolor="#ffffff" colspan="3" style="font-size: 18px; border-top-style: none;
                                                        border-right-style: none; border-left-style: none; border-bottom-style: none;
                                                        height: 50px;" valign="top">
                                                        <img src="Images/dot_title.gif" width="17" height="17">
                                                        <asp:Label ID="LblValue" runat="server" Font-Bold="True" Font-Size="Medium" 
                                                            Text="Data Updating Alert Edit"></asp:Label>
                                                        <hr />
                                                    </td>
                                                </tr>
                                                <tr bgcolor="#ffffff">
                                                    <td width="350px">
                                                        <asp:CheckBox Text="High Risk Customer" runat="server" ID="ChkHighRiskCustomer" />
                                                    </td>
                                                    <td width="1px">
                                                        :</td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="TxtHighRiskCustomer" Columns="10" /> &nbsp; Year On Opening CIF
                                                    </td>
                                                </tr>
                                                <tr bgcolor="#ffffff">
                                                    <td width="350">
                                                        <asp:CheckBox Text="Medium Risk Customer" runat="server" ID="ChkMediumRiskCustomer" /></td>
                                                    <td width="1">
                                                        :</td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="TxtMediumRiskCustomer" Columns="10" />
                                                        &nbsp; Year On Opening CIF</td>
                                                </tr>
                                                <tr bgcolor="#ffffff">
                                                    <td width="350px">
                                                        <asp:CheckBox Text="Non High Risk Customer" runat="server" ID="ChkNonHighRiskCustomer" /></td>
                                                    <td width="1px">
                                                        :</td>
                                                    <td>
                                                            <asp:TextBox runat="server" ID="TxtNonHighRiskCustomer" Columns="10" /> &nbsp; Year On Opening CIF</td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                        <asp:View ID="VwConfirmation" runat="server">
                                            <table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                                width="100%" bgcolor="#dddddd" border="0">
                                                <tr bgcolor="#ffffff">
                                                    <td colspan="2" align="center" style="height: 17px">
                                                        <asp:Label ID="LblConfirmation" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr bgcolor="#ffffff">
                                                    <td align="center" colspan="2">
                                                        <asp:ImageButton ID="ImgBack" runat="server" ImageUrl="~/images/button/back.gif"
                                                            CausesValidation="False" /></td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </asp:MultiView>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr class="formText" bgColor="#dddddd" height="30">
            <td style="width: 7px">
            </td>
            <td>
                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                    <table borderColor="#ffffff" cellSpacing="1" width="100%">
                        <tr>
                            <td align="left" background="Images/button-bground.gif" valign="middle">
                                <img height="1" src="Images/blank.gif" width="5" /></td>
                            <td align="left" background="Images/button-bground.gif" valign="middle">
                                <img height="15" src="images/arrow.gif" width="15" />&nbsp;</td>
                            <td background="Images/button-bground.gif">
                                &nbsp;<asp:ImageButton ID="ImgBtnSave" runat="server" ImageUrl="~/images/button/save.gif" /></td>
                            <td background="Images/button-bground.gif">
                                &nbsp;</td>
                            <td background="Images/button-bground.gif">
                                &nbsp;<asp:ImageButton ID="ImgBackAdd" runat="server" ImageUrl="~/images/button/back.gif"
                                    CausesValidation="False" /></td>
                            <td background="Images/button-bground.gif" width="99%">
                                <img height="1" src="Images/blank.gif" width="1" /></td>
                        </tr>
                    </table>
                    <asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" ValidationGroup="handle"></asp:CustomValidator><asp:CustomValidator
                        ID="CvalPageErr" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
            </td>
        </tr>
    </table>

</asp:Content>

