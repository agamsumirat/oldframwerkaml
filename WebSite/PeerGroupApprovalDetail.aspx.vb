Imports System.Data.SqlClient
Partial Class PeerGroupApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Integer
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get DataUpdatingParameterApprovalID
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    '''  [Hendra] 12:24 PM 10/7/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property PeerGroupApprovalID() As Int64
        Get
            Dim Temp As String = String.Empty
            Temp = Me.Request.Params("PeerGroupPendingApprovalID")
            If Temp = "" OrElse Not IsNumeric(Temp) Then
                Throw New Exception("Pending Approval ID not valid or already deleted")
                'cek id yang direquest masih ada atau tidak di database kalau tidak ada maka throw error
            ElseIf IsNumeric(Temp) Then
                Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupPendingApprovalTableAdapter
                    Dim orows() As AMLDAL.PeerGroup.PeerGroupPendingApprovalRow = adapter.GetPeerGroupPendingApprovalByPK(Temp).Select
                    If orows.Length = 0 Then
                        Throw New Exception("Pending Approval ID not exist  or already deleted")
                    Else
                        Return Temp
                    End If
                End Using
            End If
        End Get
    End Property

    Private ReadOnly Property GetHeaderPeerGroupPendingApproval() As AMLDAL.PeerGroup.PeerGroupPendingApprovalRow
        Get
            If Session("PeerGroupApprovalDetailHeaderRow") Is Nothing OrElse CType(Session("PeerGroupApprovalDetailHeaderRow"), AMLDAL.PeerGroup.PeerGroupPendingApprovalRow).PeerGroupPendingApprovalID <> Me.PeerGroupApprovalID Then
                Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupPendingApprovalTableAdapter
                    Dim objDatarow As AMLDAL.PeerGroup.PeerGroupPendingApprovalRow = adapter.GetPeerGroupPendingApprovalByPK(Me.PeerGroupApprovalID).Rows(0)
                    If Not objDatarow Is Nothing Then
                        Using adapter1 As New AMLDAL.WorkingAssignmentTableAdapters.UserWorkingUnitAssignmentTableAdapter
                            If adapter1.GetUserWorkingUnitAssignmentByUserID(Sahassa.AML.Commonly.SessionUserId).Select("userid=" & objDatarow.PeerGroupUserID).Length = 0 Then
                                'Ngak punya hak jadi harus di redirect balik
                                Response.Redirect("PeerGroupApproval.aspx", False)
                            Else
                                Return objDatarow
                            End If
                        End Using
                    ElseIf CType(Session("PeerGroupApprovalDetailHeaderRow"), AMLDAL.PeerGroup.PeerGroupPendingApprovalRow).PeerGroupPendingApprovalID = Me.PeerGroupApprovalID Then
                        Return Session("PeerGroupApprovalDetailHeaderRow")
                    Else

                        Throw New Exception("Pending Approval ID not exist or already deleted")
                    End If
                End Using
            Else
                Return Session("PeerGroupApprovalDetailHeaderRow")
            End If
           
        End Get
    End Property

    Private ReadOnly Property GetDetailPeerGroup() As AMLDAL.PeerGroup.PeerGroupApprovalRow
        Get
            If Session("PeerGroupApprovalDetailDetailRow") Is Nothing OrElse CType(Session("PeerGroupApprovalDetailDetailRow"), AMLDAL.PeerGroup.PeerGroupApprovalRow).FK_PeerGroupPendingApprovalID <> Me.PeerGroupApprovalID Then
                Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupApprovalTableAdapter
                    Dim objDatarow As AMLDAL.PeerGroup.PeerGroupApprovalRow = adapter.GetPeerGroupApprovalByFK(Me.PeerGroupApprovalID).Rows(0)
                    If Not objDatarow Is Nothing Then
                        Session("PeerGroupApprovalDetailDetailRow") = objDatarow
                        Return objDatarow
                    Else
                        Throw New Exception("Pending Approval ID not exist or already deleted")
                    End If
                End Using
            ElseIf CType(Session("PeerGroupApprovalDetailDetailRow"), AMLDAL.PeerGroup.PeerGroupApprovalRow).FK_PeerGroupPendingApprovalID = Me.PeerGroupApprovalID Then
                Return Session("PeerGroupApprovalDetailDetailRow")

            End If
          
        End Get
    End Property
#End Region

#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HandleTampilan()

        Select Case Me.ParamType
            Case TypeConfirm.Add

                Me.TableDetailAdd.Visible = True
                Me.TableDetailEdit.Visible = False
                Me.TableDetailDelete.Visible = False
            Case TypeConfirm.Edit
                Me.TableDetailAdd.Visible = False
                Me.TableDetailEdit.Visible = True
                Me.TableDetailDelete.Visible = False
            Case TypeConfirm.Delete
                Me.TableDetailAdd.Visible = False
                Me.TableDetailEdit.Visible = False
                Me.TableDetailDelete.Visible = True
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select


    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load LoadDataPeerGroupAdd
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 1:04 PM 10/7/2007 created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadDataPeerGroupAdd()
        Me.LabelTitle.Text = "Activity: Add Data Peer Group"
        If Not GetDetailPeerGroup Is Nothing Then
            Me.LabelPeerGroupNameAdd.Text = GetDetailPeerGroup.PeerGroupName
            Me.LabelDescriptionAdd.Text = GetDetailPeerGroup.PeerGroupDescription
            Me.LabelEnabledAdd.Text = GetDetailPeerGroup.Enabled
            Me.LabelSQLExpressionAdd.Text = GetDetailPeerGroup.Expression
        End If

    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load LoadDataPeerGroupEdit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 10:07 AM 10/11/2007
    ''' 
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadDataPeerGroupEdit()
        Me.LabelTitle.Text = "Activity: Edit Data Peer Group"
        If Not GetDetailPeerGroup Is Nothing Then
            Me.LabelPeerGroupNameEditOld.Text = GetDetailPeerGroup.PeerGroupName_Old
            Me.LabelDescriptionEditOld.Text = GetDetailPeerGroup.PeerGroupDescription_Old
            Me.LabelEnabledEditOld.Text = GetDetailPeerGroup.Enabled_Old
            Me.LabelSQLExpressionEditOld.Text = GetDetailPeerGroup.Expression_Old
            Me.LabelPeerGroupNameEditNew.Text = GetDetailPeerGroup.PeerGroupName
            Me.LabelDescriptionEditNew.Text = GetDetailPeerGroup.PeerGroupDescription
            Me.LabelEnabledEditNew.Text = GetDetailPeerGroup.Enabled
            Me.LabelSQLExpressionEditNew.Text = GetDetailPeerGroup.Expression
        End If
    End Sub
    ''' <summary>
    ''' Load LoadDataPeerGroupDelete
    ''' </summary>
    ''' <remarks></remarks>
    ''' <history>
    ''' [Hendra] 10:08 AM 10/11/2007
    ''' </history>
    Private Sub LoadDataPeerGroupDelete()
        Me.LabelTitle.Text = "Activity: Delete Data Peer Group"
        If Not GetDetailPeerGroup Is Nothing Then
            Me.LabelPeerGroupNameDelete.Text = GetDetailPeerGroup.PeerGroupName
            Me.LabelDescriptionDelete.Text = GetDetailPeerGroup.PeerGroupDescription
            Me.LabelEnabledDelete.Text = GetDetailPeerGroup.Enabled
            Me.LabelSQLExpressionDelete.Text = GetDetailPeerGroup.Expression
        End If
    End Sub
#End Region

#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' AcceptDataPeerGroupAdd add accept
    ''' </summary>
    ''' <remarks>
    ''' 1.insert data Peer Group Approval ke table Peer Group
    ''' 2.insert data ke audittrail
    ''' 3.delete data dari table PeerGrouppendingapproval dan PeerGroupapproval
    ''' 
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 10:10 AM 10/11/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptDataPeerGroupAdd()
        Dim oSQLTrans As System.Data.SqlClient.SqlTransaction = Nothing
        Try

            If Not Me.GetDetailPeerGroup Is Nothing Then
                'Step 1
                Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupTableAdapter
                    oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                    adapter.Insert(GetDetailPeerGroup.PeerGroupName, GetDetailPeerGroup.PeerGroupDescription, GetDetailPeerGroup.Enabled, GetDetailPeerGroup.Expression, GetDetailPeerGroup.CreatedDate)
                    'Step 2
                    InsertAuditTrailAdd(oSQLTrans)
                    'Step 3
                    Using adapter1 As New AMLDAL.PeerGroupTableAdapters.PeerGroupApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(adapter1, oSQLTrans)
                        adapter1.DeletePeerGroupApprovalByFK(Me.PeerGroupApprovalID)

                        Using adapter2 As New AMLDAL.PeerGroupTableAdapters.PeerGroupPendingApprovalTableAdapter
                            adapter2.DeletePeerGroupPendingApprovalByPK(Me.PeerGroupApprovalID)
                        End Using
                    End Using
                    oSQLTrans.Commit()
                    AbandonSession()
                End Using
            End If

        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If

            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    ''' <summary>
    ''' Insert audit trail add
    ''' </summary>
    ''' <param name="osqlTrans"></param>
    ''' <remarks></remarks>
    ''' <history>
    ''' [Hendra] 10:16 AM 10/11/2007
    ''' </history>
    Private Sub InsertAuditTrailAdd(ByRef osqlTrans As SqlTransaction)
        Try

            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
            If Not Me.GetDetailPeerGroup Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, osqlTrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Peer Group Name", "Add", "", Me.GetDetailPeerGroup.PeerGroupName, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Description", "Add", "", Me.GetDetailPeerGroup.PeerGroupDescription, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Enabled", "Add", "", Me.GetDetailPeerGroup.Enabled, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Expression", "Add", "", Me.GetDetailPeerGroup.Expression, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Peer Group Created Date", "Add", "", Me.GetDetailPeerGroup.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                End Using
            End If
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' InsertAuditTrailEdit
    ''' </summary>
    ''' <param name="osqlTrans"></param>
    ''' <remarks></remarks>
    ''' <history>
    ''' [Hendra] 10:17 AM 10/11/2007
    ''' </history>
    Private Sub InsertAuditTrailEdit(ByRef osqlTrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
            If Not Me.GetDetailPeerGroup Is Nothing Then

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, osqlTrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Peer Group Name", "Edit", Me.GetDetailPeerGroup.PeerGroupName_Old, Me.GetDetailPeerGroup.PeerGroupName, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Description", "Edit", Me.GetDetailPeerGroup.PeerGroupDescription_Old, Me.GetDetailPeerGroup.PeerGroupDescription, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Enabled", "Edit", Me.GetDetailPeerGroup.Enabled_Old, Me.GetDetailPeerGroup.Enabled, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Expression", "Edit", Me.GetDetailPeerGroup.Expression_Old, Me.GetDetailPeerGroup.Expression, "Accepted")
                End Using
            End If
        Catch
            Throw
        End Try
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' AcceptPeerGroupEdit edit accept
    ''' </summary>
    ''' <remarks>
    ''' Step
    ''' 1.Update Data PeerGroup
    ''' 2.Insert Data ke AuditTrail
    ''' 3.Delete Data dari table PeerGroupApproval dan PeerGroupPendingApproval
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 10:19 AM 10/11/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptPeerGroupEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            'Step 1
            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                adapter.UpdatePeerGroupByPK(Me.GetDetailPeerGroup.PeerGroupName, Me.GetDetailPeerGroup.PeerGroupDescription, Me.GetDetailPeerGroup.Enabled, Me.GetDetailPeerGroup.Expression, Me.GetDetailPeerGroup.CreatedDate, Me.GetDetailPeerGroup.PeerGroupId)

                'Step2
                InsertAuditTrailEdit(oSQLTrans)
                'Step3
                Using adapter1 As New AMLDAL.PeerGroupTableAdapters.PeerGroupApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter1, oSQLTrans)
                    adapter1.DeletePeerGroupApprovalByFK(Me.PeerGroupApprovalID)

                    Using adapter2 As New AMLDAL.PeerGroupTableAdapters.PeerGroupPendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(adapter2, oSQLTrans)
                        adapter2.DeletePeerGroupPendingApprovalByPK(Me.PeerGroupApprovalID)
                    End Using

                End Using
                oSQLTrans.Commit()
                AbandonSession()
            End Using

        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try

    End Sub
    ''' <summary>
    ''' Accept Delete AcceptPeerGroupDelete
    ''' </summary>
    ''' <remarks>
    ''' Step 
    ''' 1.Delete Data PeerGroup
    ''' 2.Insert AuditTrail
    ''' 3.Delete Data dari table PeerGroupApproval dan PeerGroupPendingApproval
    ''' </remarks>
    ''' <history>
    ''' [Hendra] 10:23 AM 10/11/2007
    ''' </history>
    Private Sub AcceptPeerGroupDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            'Step 1
            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                adapter.DeletePeerGroupByPK(Me.GetDetailPeerGroup.PeerGroupId)

                'Step 2
                InsertAuditTrailDelete(oSQLTrans)
                'Step3
                Using adapter1 As New AMLDAL.PeerGroupTableAdapters.PeerGroupApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter1, oSQLTrans)
                    adapter1.DeletePeerGroupApprovalByFK(Me.PeerGroupApprovalID)

                    Using adapter2 As New AMLDAL.PeerGroupTableAdapters.PeerGroupPendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(adapter2, oSQLTrans)
                        adapter2.DeletePeerGroupPendingApprovalByPK(Me.PeerGroupApprovalID)
                    End Using
                End Using
                oSQLTrans.Commit()
                AbandonSession()
            End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            LogError(ex)
            oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If

        End Try
    End Sub
    ''' <summary>
    ''' InsertAuditTrailDelete
    ''' </summary>
    ''' <param name="oSqlTrans"></param>
    ''' <remarks></remarks>
    ''' <history>
    ''' [Hendra] 10:25 AM 10/11/2007
    ''' </history>
    Private Sub InsertAuditTrailDelete(ByRef oSqlTrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
            If Not Me.GetDetailPeerGroup Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSqlTrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Peer Group Name", "Delete", "", Me.GetDetailPeerGroup.PeerGroupName, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Description", "Delete", "", Me.GetDetailPeerGroup.PeerGroupDescription, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Enabled", "Delete", "", Me.GetDetailPeerGroup.Enabled, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Expression", "Delete", "", Me.GetDetailPeerGroup.Expression, "Accepted")


                End Using
            End If
        Catch
            Throw
        End Try
    End Sub
#End Region

#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject Data Peer Group add
    ''' </summary>
    ''' <remarks>
    ''' Step 
    '''
    '''1.Delete Data dari table PeerGroupApproval dan PeerGroupPendingApproval
    '''2.Insert Audit Trail
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 10:27 AM 10/11/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectDataPeerGroupAdd()
        Dim oSQLTrans As System.Data.SqlClient.SqlTransaction = Nothing
        Try

            'Step 1
            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                adapter.DeletePeerGroupApprovalByFK(Me.PeerGroupApprovalID)
                InsertAuditTrailRejectAdd(oSQLTrans)
                Using adapter1 As New AMLDAL.PeerGroupTableAdapters.PeerGroupPendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter1, oSQLTrans)
                    adapter1.DeletePeerGroupPendingApprovalByPK(Me.PeerGroupApprovalID)
                End Using

                'Step 2

                oSQLTrans.Commit()
                AbandonSession()
            End Using

            Me.Response.Redirect("PeerGroupApproval.aspx", False)

        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If

        End Try
    End Sub
    Private Sub AbandonSession()
        Session("PeerGroupApprovalDetailDetailRow") = Nothing
        Session("PeerGroupApprovalDetailHeaderRow") = Nothing
    End Sub

    ''' <summary>
    ''' InsertAuditTrailRejectAdd
    ''' </summary>
    ''' <param name="oSqlTrans"></param>
    ''' <remarks></remarks>
    ''' <history>
    ''' [Hendra] 10:44 AM 10/11/2007
    ''' </history>
    Private Sub InsertAuditTrailRejectAdd(ByRef oSqlTrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
            If Not Me.GetDetailPeerGroup Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSqlTrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Peer Group Name", "Add", "", Me.GetDetailPeerGroup.PeerGroupName, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Description", "Add", "", Me.GetDetailPeerGroup.PeerGroupDescription, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Enabled", "Add", "", Me.GetDetailPeerGroup.Enabled, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Expression", "Add", "", Me.GetDetailPeerGroup.Expression, "Rejected")

                End Using
            End If
        Catch ex As Exception
            Throw
        End Try


    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' RejectDataPeerGroupRejectEdit reject edit
    ''' </summary>
    ''' <remarks>
    ''' Step 
    ''' 
    ''' 1.Delete Data dari table PeerGroupApproval dan PeerGroupPendingApproval
    ''' 2.Insert AuditTrail
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 11:07 AM 10/11/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectDataPeerGroupRejectEdit()
        Dim oSQLTrans As System.Data.SqlClient.SqlTransaction = Nothing
        Try
            'Step 1
            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                adapter.DeletePeerGroupApprovalByFK(Me.PeerGroupApprovalID)
                InsertAuditTrailRejectEdit(oSQLTrans)
                Using adapter2 As New AMLDAL.PeerGroupTableAdapters.PeerGroupPendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter2, oSQLTrans)
                    adapter2.DeletePeerGroupPendingApprovalByPK(Me.PeerGroupApprovalID)
                End Using
                'Step 2

                oSQLTrans.Commit()
                AbandonSession()
            End Using
            Me.Response.Redirect("PeerGroupApproval.aspx", False)
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If


        End Try

    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' RejectDataRulesAdvancedRejectDelete reject Delete
    ''' </summary>
    ''' <remarks>
    ''' Step 
    ''' 1.Insert AuditTrail
    ''' 2.Delete Data dari table RulesAdvancedApproval dan RulesAdvancedPendingApproval
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 11:02 PM 10/7/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectDataPeerGroupRejectDelete()
        Dim oSQLTrans As System.Data.SqlClient.SqlTransaction = Nothing
        Try
            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                adapter.DeletePeerGroupApprovalByFK(Me.PeerGroupApprovalID)
                InsertAuditTrailRejectDelete(oSQLTrans)
                Using adapter1 As New AMLDAL.PeerGroupTableAdapters.PeerGroupPendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter1, oSQLTrans)
                    adapter1.DeletePeerGroupPendingApprovalByPK(Me.PeerGroupApprovalID)
                End Using



                oSQLTrans.Commit()
                AbandonSession()
            End Using

            Me.Response.Redirect("PeerGroupApproval.aspx", False)
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try

    End Sub

#End Region

    Private Sub InsertAuditTrailRejectEdit(ByRef oSQLTrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
            If Not Me.GetDetailPeerGroup Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Peer Group Name", "Edit", Me.GetDetailPeerGroup.PeerGroupName_Old, Me.GetDetailPeerGroup.PeerGroupName, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Description", "Edit", Me.GetDetailPeerGroup.PeerGroupDescription_Old, Me.GetDetailPeerGroup.PeerGroupDescription, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Enabled", "Edit", Me.GetDetailPeerGroup.Enabled_Old, Me.GetDetailPeerGroup.Enabled, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Expression", "Edit", Me.GetDetailPeerGroup.Expression_Old, Me.GetDetailPeerGroup.Expression, "Rejected")
                End Using
            End If
        Catch ex As Exception
            Throw
        End Try


    End Sub
    Private Sub InsertAuditTrailRejectDelete(ByRef oSQLTrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
            If Not Me.GetDetailPeerGroup Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Peer Group Name", "Delete", "", Me.GetDetailPeerGroup.PeerGroupName, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Description", "Delete", "", Me.GetDetailPeerGroup.PeerGroupDescription, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Enabled", "Delete", "", Me.GetDetailPeerGroup.Enabled, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Expression", "Delete", "", Me.GetDetailPeerGroup.Expression, "Rejected")
                End Using
            End If
        Catch ex As Exception
            Throw
        End Try


    End Sub





    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HandleTampilan()
                Select Case Me.ParamType
                    Case TypeConfirm.Add
                        Me.LoadDataPeerGroupAdd()
                    Case TypeConfirm.Edit
                        Me.LoadDataPeerGroupEdit()
                    Case TypeConfirm.Delete
                        Me.LoadDataPeerGroupDelete()
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case TypeConfirm.Add
                    Me.AcceptDataPeerGroupAdd()
                Case TypeConfirm.Edit
                    Me.AcceptPeerGroupEdit()
                Case TypeConfirm.Delete
                    Me.AcceptPeerGroupDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "PeerGroupApproval.aspx"

            Me.Response.Redirect("PeerGroupApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case TypeConfirm.Add
                    Me.RejectDataPeerGroupAdd()
                Case TypeConfirm.Edit
                    Me.RejectDataPeerGroupRejectEdit()
                Case TypeConfirm.Delete
                    Me.RejectDataPeerGroupRejectDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "PeerGroupApproval.aspx"

            Me.Response.Redirect("PeerGroupApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "PeerGroupApproval.aspx"

        AbandonSession()
        Me.Response.Redirect("PeerGroupApproval.aspx", False)
    End Sub
    Public Enum TypeConfirm
        Add = 1
        Edit
        Delete
    End Enum


End Class
