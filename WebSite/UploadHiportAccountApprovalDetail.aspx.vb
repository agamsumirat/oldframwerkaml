﻿Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Partial Class UploadHiportAccountApprovalDetail
    Inherits Parent

    Public ReadOnly Property PkHiportApproval() As Long
        Get
            Dim strtemppk As String = Request.Params("PK_HiportApproval_ID")
            Dim lngRetval As Long
            If Long.TryParse(strtemppk, lngRetval) Then
                Return lngRetval
            Else
                Throw New Exception("PKHiportApproval not valid")
            End If
        End Get
    End Property


    

    Sub ClearSession()

    End Sub

    Sub BindGrid()
        GridView1.DataSource = AMLBLL.HiportAccountBLL.GetHiportApprovalDetailByPk(Me.PkHiportApproval)
        GridView1.PageSize = Sahassa.AML.Commonly.SessionPagingLimit
        GridView1.DataBind()
    End Sub

    Protected Sub Page_Load1(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                LabelTitle.Text = "Upload Hiport Pending Approval Detail"
                LabelActivity.Text = "Edit"
                bindGrid()
            End If

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub GridView1_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        Try
            GridView1.PageIndex = e.NewPageIndex
            BindGrid()

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageAccept_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try

            AMLBLL.HiportAccountBLL.AcceptHiportAccount(Me.PkHiportApproval)
            Response.Redirect("UploadHiportAccountApproval.aspx", False)
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageReject_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            AMLBLL.HiportAccountBLL.RejectHiportAccount(Me.PkHiportApproval)
            Response.Redirect("UploadHiportAccountApproval.aspx", False)
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageBack_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Try

            Response.Redirect("UploadHiportAccountApproval.aspx", False)
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub
End Class
