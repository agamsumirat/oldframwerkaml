﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class SegmentEdit
    Inherits Parent


    ReadOnly Property GetPk_Segment As Long
        Get
            If IsNumeric(Request.Params("SegmentID")) Then
                If Not IsNothing(Session("SegmentEdit.PK")) Then
                    Return CLng(Session("SegmentEdit.PK"))
                Else
                    Session("SegmentEdit.PK") = Request.Params("SegmentID")
                    Return CLng(Session("SegmentEdit.PK"))
                End If
            End If
            Return 0
        End Get
    End Property

    Sub ClearSession()
        Session("SegmentEdit.PK") = Nothing
    End Sub

    Property SetnGetSegmentCode As String
        Get
            Return Session("SegmentEdit.SegmentCode")
        End Get
        Set(ByVal value As String)
            Session("SegmentEdit.SegmentCode") = value
        End Set
    End Property



    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "SegmentView.aspx"

            Me.Response.Redirect("SegmentView.aspx", False)
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Function isValidData() As Boolean
        Try
            If txtSegmentCode.Text.Trim.Length = 0 Then
                Throw New Exception("Segment Code must be filled")
            End If
            If txtSegmentCode.Text.ToLower <> SetnGetSegmentCode.ToLower Then
                If Not SegmentBLL.IsUniqueSegmentCode(txtSegmentCode.Text) Then
                    Throw New Exception("Sement Code " & txtSegmentCode.Text & " existed")
                End If
            End If
            Return True
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
    End Function

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If isValidData() Then
                Using objUser As User = AMLBLL.UserBLL.GetUserByPkUserID(Sahassa.AML.Commonly.SessionPkUserId)
                    If Not IsNothing(objUser) Then
                        Using objSegment As Segment = SegmentBLL.getSegmentByPk(GetPk_Segment)
                            If Not IsNothing(objSegment) Then
                                objSegment.SegmentCode = txtSegmentCode.Text
                                objSegment.SegmentDescription = txtDescription.Text
                                objSegment.Activation = True
                                'objSegment.CreatedDate = Now
                                'objSegment.CreatedBy = objUser.UserName
                                objSegment.LastUpdateDate = Now
                                objSegment.LastUpdateBy = objUser.UserName


                                If Sahassa.AML.Commonly.SessionGroupName.ToLower = "superuser" Then
                                    If SegmentBLL.SaveEdit(objSegment) Then
                                        lblMessage.Text = "Edit Data Success"
                                        mtvSegmentAdd.ActiveViewIndex = 1
                                    End If
                                Else
                                    If SegmentBLL.SaveEditApproval(objSegment) Then
                                        lblMessage.Text = "Edited Data has been inserted to Approval"
                                        mtvSegmentAdd.ActiveViewIndex = 1
                                    End If
                                End If
                            End If
                        End Using

                    End If
                End Using
            End If

        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub LoadData()
        Using objSegment As Segment = SegmentBLL.getSegmentByPk(GetPk_Segment)
            If Not IsNothing(objSegment) Then
                txtDescription.Text = objSegment.SegmentDescription.ToString
                txtSegmentCode.Text = objSegment.SegmentCode.ToString
                SetnGetSegmentCode = objSegment.SegmentCode
            Else
                ImageSave.Visible = False
            End If
        End Using
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                mtvSegmentAdd.ActiveViewIndex = 0
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                LoadData()
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Response.Redirect("SegmentView.aspx")
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

End Class


