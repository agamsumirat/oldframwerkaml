
Partial Class PrintSTR
    Inherits System.Web.UI.Page

    Private ReadOnly Property PkCaseManagementID() As String
        Get
            Return Request.Params("PkCaseManagementID")
        End Get
    End Property

    Private ReadOnly Property CIFNO() As String
        Get
            Return Request.Params("cifno")
        End Get
    End Property
    Private ReadOnly Property asal() As Integer
        Get
            Return Request.Params("Asal")
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ReportViewer1.LocalReport.ReportPath = "ReportSTR.rdlc"
        ReportViewer1.LocalReport.Refresh()

    End Sub

    
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        If Me.asal = 1 Then
            Response.Redirect("CaseManagementConfirmationNumberViewDetail.aspx?PK_CaseManagementID=" & PkCaseManagementID, False)
        Else
            Response.Redirect("CaseManagementViewDetail.aspx?PK_CaseManagementID=" & Me.PkCaseManagementID, False)
        End If
    End Sub
End Class
