Imports System.Data
Imports System.Data.SqlClient

Partial Class CaseManagementWorkflowApprovalDetail
    Inherits Parent

#Region "Property"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetAccountOwnerID() As String
        Get
            Return Me.Request.Params.Item("AccountOwnerID").Trim
        End Get
    End Property


    Private ReadOnly Property GetSegment() As String
        Get
            Return Me.Request.Params.Get("Segment").Trim
        End Get
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetAccountOwnerName() As String
        Get
            Return Me.Request.Params.Item("AccountOwnerName").Trim
        End Get
    End Property

    Private ReadOnly Property GetVIPCode() As String
        Get
            Return Me.Request.Params.Item("VIPCode").Trim
        End Get
    End Property
    Private ReadOnly Property GetCaseAlertDescription() As String
        Get
            Return Me.Request.Params.Item("CaseAlertDescription").Trim
        End Get
    End Property
    Private ReadOnly Property GetInsiderCode() As String
        Get
            Return Me.Request.Params.Item("InsiderCode").Trim
        End Get
    End Property

    Private ReadOnly Property GetRM() As String
        Get
            Return Me.Request.Params.Item("RM").Trim
        End Get
    End Property
    Private ReadOnly Property GetSBU() As String
        Get
            Return Me.Request.Params.Item("SBU").Trim
        End Get
    End Property
    Private ReadOnly Property GetSubSBU() As String
        Get
            Return Me.Request.Params.Item("SubSBU").Trim
        End Get
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetMode() As String
        Get
            Return Me.Page.Request.Params.Item("Mode").Trim
        End Get
    End Property
    ''' <summary>
    ''' SetnGetGeneral_Old
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetGeneral_Old() As Data.DataTable
        Get
            Return Session("General_Old")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("General_Old") = value
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetGeneral_New() As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowGeneral_ApprovalDetailDataTable
        Get
            Return Session("General_New")
        End Get
        Set(ByVal value As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowGeneral_ApprovalDetailDataTable)
            Session("General_New") = value
        End Set
    End Property

    Private Property SetnGetEmailTemplate_Old() As Data.DataTable
        Get
            Return Session("EmailTemplate_Old")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("EmailTemplate_Old") = value
        End Set
    End Property


    Private Property SetnGetEmailTemplate_New() As Data.DataTable
        Get
            Return Session("EmailTemplate_New")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("EmailTemplate_New") = value
        End Set
    End Property

    Private Property SetnGetUserIDCreated() As String
        Get
            Return Session("UserIDCreated")
        End Get
        Set(ByVal value As String)
            Session("UserIDCreated") = value
        End Set
    End Property

    Private Property SetnGetCreatedDate() As DateTime
        Get
            Return Session("CreatedDate")
        End Get
        Set(ByVal value As DateTime)
            Session("CreatedDate") = value
        End Set
    End Property
#End Region

#Region "Reset All Session"
    Private Sub ResetAllSession()
        Me.SetnGetEmailTemplate_New = Nothing
        Me.SetnGetEmailTemplate_Old = Nothing
        Me.SetnGetGeneral_New = Nothing
        Me.SetnGetGeneral_Old = Nothing
        Me.SetnGetUserIDCreated = Nothing
        Me.SetnGetCreatedDate = Nothing
    End Sub
#End Region

#Region "Load"
    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.LblAccountOwner.Text = Me.GetAccountOwnerName
            Me.LblSegment.Text = Me.GetSegment
            Me.LblInsidercode.Text = Me.GetInsiderCode
            Me.LblVIPCode.Text = Me.GetVIPCode
            Me.LblSBU.Text = Me.GetSBU
            Me.LblCaseAlertDesc.Text = Me.GetCaseAlertDescription
            Me.LblSubSBU.Text = Me.GetSUBSBU
            Me.LblRM.Text = Me.GetRM
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")

                Using AccessCMWApproval As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowApprovalByAccountOwnerIDTableAdapter
                    Dim DtApproval As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowApprovalByAccountOwnerIDDataTable = AccessCMWApproval.GetData(Me.GetAccountOwnerID, Me.GetSegment, Me.GetVIPCode, Me.GetInsiderCode, Me.GetCaseAlertDescription, Me.GetSBU, Me.GetSubSBU, Me.GetRM)
                    If DtApproval.Rows.Count > 0 Then
                        Dim RowApproval As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowApprovalByAccountOwnerIDRow = DtApproval.Rows(0)
                        Me.SetnGetUserIDCreated = RowApproval.UserID
                        Me.SetnGetCreatedDate = RowApproval.CreatedDate
                    End If
                End Using

                If Me.GetMode.ToLower = "add" Then
                    Me.MultiViewApprovalDetail.ActiveViewIndex = 0
                    ShowDetailCaseManagementWorkflowAdd()
                    Using oAccessEmail_New As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectAllCaseManagementWorkflowEmailTemplate_ApprovalDetailTableAdapter
                        Me.SetnGetEmailTemplate_New = oAccessEmail_New.GetData(Me.GetAccountOwnerID)
                    End Using
                Else
                    Me.MultiViewApprovalDetail.ActiveViewIndex = 1
                    Me.ShowDetailCaseManagementWorkflowEdit()
                    Using oAccessEmail_New As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectAllCaseManagementWorkflowEmailTemplate_ApprovalDetailTableAdapter
                        Me.SetnGetEmailTemplate_New = oAccessEmail_New.GetData(Me.GetAccountOwnerID)
                    End Using
                    Using oAccessEmail_Old As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowEmailTemplateTableAdapter
                        Me.SetnGetEmailTemplate_Old = oAccessEmail_Old.GetData(Me.GetAccountOwnerID)
                    End Using
                End If
            End If
            Me.ResetEmail()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ShowDetailCaseManagementWorkflowEdit()
        Try

            ' Get Data New Value
            Using oAcessCMWApprovalDetail_General_New As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowGeneral_ApprovalDetailTableAdapter
                Dim DtApprovalDetail_General_New As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowGeneral_ApprovalDetailDataTable = oAcessCMWApprovalDetail_General_New.GetData(Me.GetAccountOwnerID)
                Me.GridViewEdit_New.DataSource = DtApprovalDetail_General_New
                Me.GridViewEdit_New.DataBind()

                Me.SetnGetGeneral_New = DtApprovalDetail_General_New

                For Each Rows As GridViewRow In Me.GridViewEdit_New.Rows
                    Dim GridViewApprovers_New As GridView = Rows.Cells(3).Controls(1) '
                    Dim GridViewDueDate_New As GridView = Rows.Cells(4).Controls(1) '
                    Dim GridViewNotifyOthers_New As GridView = Rows.Cells(5).Controls(1) '
                    Dim SerialNo As Integer = Rows.Cells(0).Text

                    Using oAccessCMWApproval_Detail_New As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowDetail_ApprovalDetailTableAdapter
                        Dim DtApprovalDetail_DetailCMW_New As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowDetail_ApprovalDetailDataTable = oAccessCMWApproval_Detail_New.GetData(Me.GetAccountOwnerID, SerialNo)
                        GridViewApprovers_New.DataSource = DtApprovalDetail_DetailCMW_New
                        GridViewApprovers_New.DataBind()
                        GridViewDueDate_New.DataSource = DtApprovalDetail_DetailCMW_New
                        GridViewDueDate_New.DataBind()
                        GridViewNotifyOthers_New.DataSource = DtApprovalDetail_DetailCMW_New
                        GridViewNotifyOthers_New.DataBind()
                    End Using
                Next
            End Using

            ' Get Data Old Value
            Using oAcessCMWApprovalDetail_General_Old As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowGeneralTableAdapter
                Dim DtApprovalDetail_General_Old As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowGeneralDataTable = oAcessCMWApprovalDetail_General_Old.GetData(Me.GetAccountOwnerID, Me.GetCaseAlertDescription, Me.GetVIPCode, Me.GetInsiderCode, Me.GetSegment, Me.GetSBU, Me.GetSubSBU, Me.GetRM)

                Me.GridViewEdit_Old.DataSource = DtApprovalDetail_General_Old
                Me.GridViewEdit_Old.DataBind()

                For Each Rows As GridViewRow In Me.GridViewEdit_Old.Rows
                    Dim GridViewApprovers_Old As GridView = Rows.Cells(3).Controls(1) '
                    Dim GridViewDueDate_Old As GridView = Rows.Cells(4).Controls(1) '
                    Dim GridViewNotifyOthers_Old As GridView = Rows.Cells(5).Controls(1) '
                    Dim SerialNo_Old As Integer = Rows.Cells(0).Text

                    Using oAccessCMWApproval_Detail_Old As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowDetail_BySerialNoTableAdapter
                        Dim DtApprovalDetail_DetailCMW_Old As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowDetail_BySerialNoDataTable = oAccessCMWApproval_Detail_Old.GetData(Me.GetAccountOwnerID, SerialNo_Old)
                        GridViewApprovers_Old.DataSource = DtApprovalDetail_DetailCMW_Old
                        GridViewApprovers_Old.DataBind()
                        GridViewDueDate_Old.DataSource = DtApprovalDetail_DetailCMW_Old
                        GridViewDueDate_Old.DataBind()
                        GridViewNotifyOthers_Old.DataSource = DtApprovalDetail_DetailCMW_Old
                        GridViewNotifyOthers_Old.DataBind()
                    End Using
                Next
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' ShowDetailCaseManagementWorkflowAdd
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ShowDetailCaseManagementWorkflowAdd()
        Try
            Using oAcessCMWApprovalDetail_General As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowGeneral_ApprovalDetailTableAdapter
                Dim DtApprovalDetail_General As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowGeneral_ApprovalDetailDataTable = oAcessCMWApprovalDetail_General.GetData(Me.GetAccountOwnerID)
                Me.GridViewAddApproval_General.DataSource = DtApprovalDetail_General
                Me.GridViewAddApproval_General.DataBind()

                ' Set General Ke Data Table
                Me.SetnGetGeneral_New = DtApprovalDetail_General

                For Each Rows As GridViewRow In Me.GridViewAddApproval_General.Rows
                    Dim GridViewApprovers As GridView = Rows.Cells(3).Controls(1) '
                    Dim GridViewDueDate As GridView = Rows.Cells(4).Controls(1) '
                    Dim GridViewNotifyOthers As GridView = Rows.Cells(5).Controls(1) '
                    Dim SerialNo As Integer = Rows.Cells(0).Text

                    Using oAccessCMWApproval_Detail As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowDetail_ApprovalDetailTableAdapter
                        Dim DtApprovalDetail_DetailCMW As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowDetail_ApprovalDetailDataTable = oAccessCMWApproval_Detail.GetData(Me.GetAccountOwnerID, SerialNo)
                        GridViewApprovers.DataSource = DtApprovalDetail_DetailCMW
                        GridViewApprovers.DataBind()
                        GridViewDueDate.DataSource = DtApprovalDetail_DetailCMW
                        GridViewDueDate.DataBind()
                        GridViewNotifyOthers.DataSource = DtApprovalDetail_DetailCMW
                        GridViewNotifyOthers.DataBind()
                    End Using
                Next
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Row DataBound Approvers Add
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridViewApprovers_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(0).Text <> "&nbsp;" Then
                    e.Row.Cells(0).Text = Me.GetUserIdByPk(e.Row.Cells(0).Text)
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    ''' <summary>
    ''' Row DataBound Notify Others Add
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridViewNotifyOthers_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(0).Text <> "&nbsp;" Then
                    e.Row.Cells(0).Text = Me.GetUserIdByPk(e.Row.Cells(0).Text)
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    ''' <summary>
    ''' Get UserId By Pk
    ''' </summary>
    ''' <param name="PkUserID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetUserIdByPk(ByVal PkUserID As String) As String
        Try
            Dim StrUserID As String = Nothing
            If PkUserID <> "" Then
                Dim oArrPkUserId As String() = PkUserID.Split("|")
                Using oAccessUserID As New AMLDAL.CaseManagementWorkflowTableAdapters.GetUserIDByPKTableAdapter
                    For i As Integer = 0 To oArrPkUserId.Length - 1
                        If i = oArrPkUserId.Length - 1 Then
                            StrUserID &= Convert.ToString(oAccessUserID.Fill(Convert.ToInt32(oArrPkUserId(i))))
                        Else
                            StrUserID &= Convert.ToString(oAccessUserID.Fill(Convert.ToInt32(oArrPkUserId(i)))) & ";"
                        End If
                    Next
                End Using
            End If
            Return StrUserID
        Catch
            Throw
        End Try
    End Function
#End Region
   
#Region "Reset Email"
    Public Sub ResetEmail()
        Me.LblSubjectAdd.Text = ""
        Me.LblSubjectEdit_New.Text = ""
        Me.LblSubjectEdit_Old.Text = ""
        Me.TxtBodyAdd.Text = ""
        Me.TxtBodyEdit_New.Text = ""
        Me.TxtBodyEdit_Old.Text = ""
    End Sub
#End Region
    
#Region "Accept, Reject dan Cancel"
    ''' <summary>
    ''' Accept 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Me.SaveAccept()
            Me.AuditTrail("Accepted")
            Me.Response.Redirect("CaseManagementWorkflowApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        Finally
            Me.ResetAllSession()
        End Try
    End Sub

    Private Sub SaveAccept()
        Dim Trans As SqlTransaction
        Try
            If Me.GetMode = "Edit" Then
                Using oAccessEmailTemplate_Old As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowEmailTemplateTableAdapter
                    SetnGetEmailTemplate_Old = oAccessEmailTemplate_Old.GetData(Me.GetAccountOwnerID)
                End Using
                Using oAccessDeleteCMW As New AMLDAL.CaseManagementWorkflowTableAdapters.TransTableAdapterTableAdapter
                    Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(oAccessDeleteCMW)

                    oAccessDeleteCMW.DeleteCaseManagementWorkflow(Me.GetAccountOwnerID)
                End Using
            End If

            Using oAccessCMWApproval As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectAllCaseManagementWorkflowApproval_ByAccountOwnerTableAdapter
                Dim Pk_CMW_Id As Int64 = 0
                Dim DtCMWApproval As Data.DataTable = oAccessCMWApproval.GetData(Me.GetAccountOwnerID, Me.GetSegment, Me.GetVIPCode, GetInsiderCode, GetCaseAlertDescription, GetSBU, GetSUBSBU, GetRM)
                If DtCMWApproval.Rows.Count > 0 Then
                    Dim RowCMWApproval As AMLDAL.CaseManagementWorkflow.SelectAllCaseManagementWorkflowApproval_ByAccountOwnerRow = DtCMWApproval.Rows(0)
                    Using oAccessInsertCMW As New AMLDAL.CaseManagementWorkflowTableAdapters.InsertCaseManagementWorkflowTableAdapter
                        If Me.GetMode = "Edit" Then
                            Sahassa.AML.TableAdapterHelper.SetTransaction(oAccessInsertCMW, Trans)
                        Else
                            Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(oAccessInsertCMW)
                        End If

                        ' Insert dan Get Pk
                        Pk_CMW_Id = oAccessInsertCMW.Fill(Me.GetAccountOwnerID, Trim(Me.GetSegment), Trim(Me.GetVIPCode), Trim(Me.GetInsiderCode), Me.GetSBU, Me.GetSUBSBU, Me.GetRM, Trim(Me.GetCaseAlertDescription), RowCMWApproval.CreatedDate, Now)

                        Using oAccessQuery As New AMLDAL.CaseManagementWorkflowTableAdapters.TransTableAdapterTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(oAccessQuery, Trans)

                            ' Get General dan Save
                            If Me.SetnGetGeneral_New.Rows.Count > 0 Then
                                For Each RowsGeneral As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowGeneral_ApprovalDetailRow In Me.SetnGetGeneral_New
                                    oAccessQuery.InsertCaseManagementWorkflowGeneral(Pk_CMW_Id, RowsGeneral.SerialNo_ApprovalDetail, RowsGeneral.WorkflowStep_ApprovalDetail, RowsGeneral.Paralel_ApprovalDetail)
                                Next
                            End If

                            ' Get Detail dan Save
                            Using oAccessApprovalDetail As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectAllCaseManagementWorkflowDetail_ApprovalDetailTableAdapter
                                Dim DtApprovalDetail As AMLDAL.CaseManagementWorkflow.SelectAllCaseManagementWorkflowDetail_ApprovalDetailDataTable = oAccessApprovalDetail.GetData(Me.GetAccountOwnerID)
                                If DtApprovalDetail.Rows.Count > 0 Then
                                    For Each RowsDetail As AMLDAL.CaseManagementWorkflow.SelectAllCaseManagementWorkflowDetail_ApprovalDetailRow In DtApprovalDetail.Rows
                                        oAccessQuery.InsertCaseManagementWorkflowDetail(Pk_CMW_Id, RowsDetail.SerialNo_ApprovalDetail, RowsDetail.Approvers_ApprovalDetail, RowsDetail.DueDate_ApprovalDetail, RowsDetail.NotifyOthers_ApprovalDetail)
                                    Next
                                End If
                            End Using

                            ' Get Email Template dan Save 
                            Using oAccessApprovalEmailTemplate As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectAllCaseManagementWorkflowEmailTemplate_ApprovalDetailTableAdapter
                                Dim DtApprovalEmailTemplate As AMLDAL.CaseManagementWorkflow.SelectAllCaseManagementWorkflowEmailTemplate_ApprovalDetailDataTable = oAccessApprovalEmailTemplate.GetData(Me.GetAccountOwnerID)
                                If DtApprovalEmailTemplate.Rows.Count > 0 Then

                                    For Each RowsEmailTemplate As AMLDAL.CaseManagementWorkflow.SelectAllCaseManagementWorkflowEmailTemplate_ApprovalDetailRow In DtApprovalEmailTemplate.Rows
                                        oAccessQuery.InsertCaseManagementWorkflowEmailTemplate(Pk_CMW_Id, RowsEmailTemplate.SerialNo_ApprovalDetail, RowsEmailTemplate.Subject_EmailTemplate_ApprovalDetail, RowsEmailTemplate.Body_EmailTemplate_ApprovalDetail)
                                    Next
                                End If
                            End Using
                        End Using
                    End Using
                End If
            End Using

            ' change status
            Using oAccessChangeStatus As New AMLDAL.CaseManagementWorkflowTableAdapters.TransTableAdapterTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(oAccessChangeStatus, Trans)

                oAccessChangeStatus.ChangeStatusAccountOwner(1, Me.GetAccountOwnerID)
            End Using

            ' Delete Approval
            Using oAccessDeleteCMW As New AMLDAL.CaseManagementWorkflowTableAdapters.TransTableAdapterTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(oAccessDeleteCMW, Trans)

                oAccessDeleteCMW.DeleteCaseManagementWorkflowApproval(Me.GetAccountOwnerID)
            End Using

            Trans.Commit()
            'oTrans.Complete()
            'End Using
        Catch
            Trans.Rollback()
            Throw
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
            End If
        End Try
    End Sub
    ''' <summary>
    ''' Reject
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            ' Delete All Approval
            Using oAccessDeleteCaseManagement As New AMLDAL.CaseManagementWorkflowTableAdapters.TransTableAdapterTableAdapter
                oAccessDeleteCaseManagement.DeleteCaseManagementWorkflowApproval(Me.GetAccountOwnerID)
            End Using
            Me.AuditTrail("Rejected")
            Me.Response.Redirect("CaseManagementWorkflowApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        Finally
            Me.ResetAllSession()
        End Try
    End Sub
    ''' <summary>
    ''' Cancel
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.ResetAllSession()
        Me.Response.Redirect("CaseManagementWorkflowApproval.aspx", False)
    End Sub
#End Region
    
#Region "Hide Multiview Email Template"

    Protected Sub LinkHideEmailTemplateAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkHideEmailTemplateAdd.Click
        Me.MultiViewEmailTemplate.ActiveViewIndex = -1
    End Sub

    Protected Sub LinkHideEmailTemplateEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkHideEmailTemplateEdit.Click
        Me.MultiViewEmailTemplate.ActiveViewIndex = -1
    End Sub

#End Region

#Region "Show Email Template"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonEmailTemplateOld_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.ShowEmailTemplate(sender)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonEmailTemplateNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.ShowEmailTemplate(sender)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <remarks></remarks>
    Private Sub ShowEmailTemplate(ByVal sender As Object)
        Try
            Me.MultiViewEmailTemplate.ActiveViewIndex = 1

            Dim oLinkEmailTemplate As LinkButton = sender
            Dim oGridViewRow As GridViewRow = CType(oLinkEmailTemplate.NamingContainer, GridViewRow)
            Dim IntSerialNo As Integer = oGridViewRow.Cells(0).Text

            Using oAccessEmailTemplate_New As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowEmailTemplate_ApprovalDetailTableAdapter
                Using Dt_New As Data.DataTable = oAccessEmailTemplate_New.GetData(Me.GetAccountOwnerID, IntSerialNo)
                    If Dt_New.Rows.Count > 0 Then
                        Dim Row As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowEmailTemplate_ApprovalDetailRow = Dt_New.Rows(0)
                        Me.LblSubjectEdit_New.Text = Row.Subject_EmailTemplate_ApprovalDetail
                        Me.TxtBodyEdit_New.Text = Row.Body_EmailTemplate_ApprovalDetail
                    End If
                End Using
            End Using

            Using oAccessEmailTemplate_Old As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowEmailTemplate_BySerialNoTableAdapter
                Using Dt_Old As Data.DataTable = oAccessEmailTemplate_Old.GetData(Me.GetAccountOwnerID, IntSerialNo)
                    If Dt_Old.Rows.Count > 0 Then
                        Dim Row As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowEmailTemplate_BySerialNoRow = Dt_Old.Rows(0)
                        Me.LblSubjectEdit_Old.Text = Row.Subject_EmailTemplate
                        Me.TxtBodyEdit_Old.Text = Row.Body_EmailTemplate
                    End If
                End Using
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' Link Email Add
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim BtnShowEmailTemplate_Add As LinkButton = sender
            Dim GridViewRow_Add As GridViewRow = BtnShowEmailTemplate_Add.NamingContainer
            Dim IntSerial As Integer = GridViewRow_Add.Cells(0).Text
            Using oAccessEmailAddressApproval_Add As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowEmailTemplate_ApprovalDetailTableAdapter
                Dim DtEmail As Data.DataTable = oAccessEmailAddressApproval_Add.GetData(Me.GetAccountOwnerID, IntSerial)
                Me.MultiViewEmailTemplate.ActiveViewIndex = 0 ' untuk add
                Me.LblSubjectAdd.Text = DtEmail.Rows(0)(1).ToString
                Me.TxtBodyAdd.Text = DtEmail.Rows(0)(2).ToString
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

#Region "Insert AuditTrail"
    ''' <summary>
    ''' AuditTrail
    ''' </summary>
    ''' <param name="Action"></param>
    ''' <remarks></remarks>
    Private Sub AuditTrail(ByVal Action As String)
        Try
            If Me.GetMode = "Add" Then
                Sahassa.AML.AuditTrailAlert.CheckMailAlert(11 * Me.GridViewAddApproval_General.Rows.Count)
                Me.InsertIntoAuditTrail(Me.GridViewAddApproval_General, Me.SetnGetUserIDCreated, Me.SetnGetCreatedDate, Action, "Add")
            Else ' Edit
                Sahassa.AML.AuditTrailAlert.CheckMailAlert(11 * (Me.GridViewEdit_Old.Rows.Count + Me.GridViewEdit_New.Rows.Count))
                Me.InsertIntoAuditTrail(Me.GridViewEdit_Old, Me.SetnGetUserIDCreated, Me.SetnGetCreatedDate, Action, "Delete")
                Me.InsertIntoAuditTrail(Me.GridViewEdit_New, Me.SetnGetUserIDCreated, Me.SetnGetCreatedDate, Action, "Add")
            End If

        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Insert Into Audit Trail
    ''' </summary>
    ''' <param name="o_gridview"></param>
    ''' <param name="UserIDCreated"></param>
    ''' <param name="CreatedDate"></param>
    ''' <param name="Action"></param>
    ''' <param name="Mode"></param>
    ''' <remarks></remarks>
    Private Sub InsertIntoAuditTrail(ByVal o_gridview As GridView, ByVal UserIDCreated As String, ByVal CreatedDate As DateTime, ByVal Action As String, ByVal Mode As String)
        Try
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                For Each RowGrid_Old As GridViewRow In o_gridview.Rows
                    Dim SerialNo As String = RowGrid_Old.Cells(0).Text
                    Dim WorkflowStep As String = RowGrid_Old.Cells(1).Text
                    Dim Paralel As String = RowGrid_Old.Cells(2).Text

                    Dim GridViewApprovers As GridView = RowGrid_Old.Cells(3).Controls(1)
                    Dim GridViewDueDate As GridView = RowGrid_Old.Cells(4).Controls(1)
                    Dim GridViewNotifyOthers As GridView = RowGrid_Old.Cells(5).Controls(1)

                    Dim StrTempApprovers As String = ""
                    Dim StrTempDueDate As String = ""
                    Dim StrTempNotify As String = ""
                    For i As Integer = 0 To GridViewApprovers.Rows.Count - 1
                        Dim RowApprovers As GridViewRow = GridViewApprovers.Rows(i)
                        Dim RowDueDate As GridViewRow = GridViewDueDate.Rows(i)
                        Dim RowNotifyOthers As GridViewRow = GridViewNotifyOthers.Rows(i)

                        If i = GridViewApprovers.Rows.Count - 1 Then
                            StrTempApprovers &= RowApprovers.Cells(0).Text
                            StrTempDueDate &= RowDueDate.Cells(0).Text
                            StrTempNotify &= RowNotifyOthers.Cells(0).Text
                        Else
                            StrTempApprovers &= RowApprovers.Cells(0).Text & " | "
                            StrTempDueDate &= RowDueDate.Cells(0).Text & " | "
                            StrTempNotify &= RowNotifyOthers.Cells(0).Text & " | "
                        End If
                    Next
                    Dim RowEmail() As Data.DataRow

                    If Me.GetMode = "Delete" Then
                        RowEmail = Me.SetnGetEmailTemplate_Old.Select("SerialNo=" & SerialNo)
                    Else
                        RowEmail = Me.SetnGetEmailTemplate_New.Select("SerialNo_ApprovalDetail=" & SerialNo)
                    End If

                    Dim Subject As String = ""
                    Dim Body As String = ""
                    If RowEmail.Length > 0 Then
                        Subject = RowEmail(0)(1).ToString
                        Body = RowEmail(0)(2).ToString
                    End If
                    
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "CaseManagementWorkflow", "AccountOwnerName", Mode, "", Me.LblAccountOwner.Text, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "CaseManagementWorkflow", "SerialNo", Mode, "", SerialNo, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "CaseManagementWorkflow", "Workflow Step", Mode, "", WorkflowStep, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "CaseManagementWorkflow", "Paralel", Mode, "", Paralel, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "CaseManagementWorkflow", "Approvers", Mode, "", StrTempApprovers, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "CaseManagementWorkflow", "DueDate", Mode, "", StrTempDueDate, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "CaseManagementWorkflow", "NotifyOthers", Mode, "", StrTempNotify, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "CaseManagementWorkflow", "SubjectEmailTemplate", Mode, "", Subject, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "CaseManagementWorkflow", "BodyEmailTemplate", Mode, "", Body, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "CaseManagementWorkflow", "CreatedDate", Mode, "", CreatedDate, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "CaseManagementWorkflow", "LastUpdatedDate", Mode, "", Now, Action)
                Next
            End Using
        Catch
            Throw
        End Try
    End Sub
#End Region

End Class
