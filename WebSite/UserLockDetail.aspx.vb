Partial Class UserLockDetail
    Inherits Parent

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPkUserId() As String
        Get
            Return Me.Request.Params("PkUserID")
        End Get
    End Property

    ''' <summary>
    ''' get id for focues
    ''' </summary>
    ''' <value></value>
    ''' <returns>element control id</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.CheckBoxInUse.ClientID
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "UserLock.aspx"
        Me.Response.Redirect("UserLock.aspx", False)
    End Sub

    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                Dim pkUserId As Int64 = Session("PkUserId_Old")
                AccessUser.UpdateUserLock(pkUserId, Me.CheckBoxInUse.Checked, Me.CheckBoxDisabled.Checked)
            End Using
            Sahassa.AML.Commonly.SessionIntendedPage = "UserLock.aspx"
            Me.Response.Redirect("UserLock.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
            Using TableUser As AMLDAL.AMLDataSet.UserDataTable = AccessUser.GetDataByUserID(Me.GetPkUserId)
                If TableUser.Rows.Count > 0 Then
                    Dim TableRowUser As AMLDAL.AMLDataSet.UserRow = TableUser.Rows(0)

                    Session("PkUserId_Old") = TableRowUser.pkUserID

                    Me.LabelUserId.Text = TableRowUser.UserID
                    Me.LabelUserName.Text = TableRowUser.UserName
                    Me.LabelIPAddress.Text = TableRowUser.UserIPAddress

                    Session("UserInUsed_Old") = CType(TableRowUser.UserInUsed, Boolean)
                    Me.CheckBoxInUse.Checked = Session("UserInUsed_Old")

                    Session("UserIsDisabled_Old") = CType(TableRowUser.UserIsDisabled, Boolean)
                    Me.CheckBoxDisabled.Checked = Session("UserIsDisabled_Old")
                End If
            End Using
        End Using
    End Sub

    Private Sub ClearThisPageSessions()
        Session("PkUserId_Old") = Nothing
        Session("UserInUsed_Old") = Nothing
        Session("UserIsDisabled_Old") = Nothing
    End Sub

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                If Me.GetPkUserId Is Nothing Or Me.GetPkUserId = 1 Then
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                        Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " failed, unknown data PkUserId='" & Me.GetPkUserId & "'"
                        AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    End Using
                    Throw New Exception("The data supplied is invalid, please try again.")
                Else
                    Me.FillEditData()
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                        Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                        AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    End Using
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class