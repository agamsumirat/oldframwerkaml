Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Imports System.IO
Imports System.Collections.Generic

Partial Class MappingUserIdHiportJiveView

    Inherits Parent

    Public Property PK_MappingUserIdHIPORTJIVE_ID() As String
        Get
            Return CType(Session("MappingUserIdHiportJiveView.PK_MappingUserIdHIPORTJIVE_ID"), String)
        End Get
        Set(ByVal value As String)
            Session("MappingUserIdHiportJiveView.PK_MappingUserIdHIPORTJIVE_ID") = value
        End Set
    End Property

    Public Property SetnGetUserName() As String
        Get
            If Not Session("MappingUserIdHiportJiveView.SetnGetUserName") Is Nothing Then
                Return CStr(Session("MappingUserIdHiportJiveView.SetnGetUserName"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingUserIdHIPORTJIVEView.SetnGetUserName") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("MappingUserIdHIPORTJIVEView.Sort") Is Nothing, "UserName  asc", Session("MappingUserIdHIPORTJIVEView.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("MappingUserIdHIPORTJIVEView.Sort") = Value
        End Set
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("MappingUserIdHIPORTJIVEView.RowTotal") Is Nothing, 0, Session("MappingUserIdHIPORTJIVEView.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MappingUserIdHIPORTJIVEView.RowTotal") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("MappingUserIdHIPORTJIVEView.CurrentPage") Is Nothing, 0, Session("MappingUserIdHIPORTJIVEView.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MappingUserIdHIPORTJIVEView.CurrentPage") = Value
        End Set
    End Property

    Function Getvw_MappingUserIdHiportJive(ByVal strWhereClause As String, ByVal strOrderBy As String, ByVal intStart As Integer, ByVal intPageLength As Integer, ByRef intCount As Integer) As VList(Of vw_MappingUserIdHiportJive)
        Dim Objvw_MappingUserIdHiportJive As VList(Of vw_MappingUserIdHiportJive) = Nothing
        Objvw_MappingUserIdHiportJive = DataRepository.vw_MappingUserIdHiportJiveProvider.GetPaged(strWhereClause, strOrderBy, intStart, intPageLength, intCount)
        Return Objvw_MappingUserIdHiportJive
    End Function

    Public Property SetnGetBindTable() As VList(Of vw_MappingUserIdHiportJive)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            If SetnGetUserName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "UserName like '%" & SetnGetUserName.Trim.Replace("'", "''") & "%'"
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            'If strAllWhereClause.Trim.Length > 0 Then
            '    strAllWhereClause += " and Fk_MsUserApproval_Id=" & Sahassa.AML.Commonly.SessionUserApprovalId & " and " & vw_MsHelp_ApprovalColumn.FK_MsUserID.ToString & " <> " & Sahassa.AML.Commonly.SessionPkUserId
            'Else
            '    strAllWhereClause += "  Fk_MsUserApproval_Id =" & Sahassa.AML.Commonly.SessionUserApprovalId & " and " & vw_MsHelp_ApprovalColumn.FK_MsUserID.ToString & " <> " & Sahassa.AML.Commonly.SessionPkUserId

            'End If
            Session("MappingUserIdHIPORTJIVEView.Table") = Getvw_MappingUserIdHiportJive(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)


            Return CType(Session("MappingUserIdHIPORTJIVEView.Table"), VList(Of vw_MappingUserIdHiportJive))


        End Get
        Set(ByVal value As VList(Of vw_MappingUserIdHiportJive))
            Session("MappingUserIdHIPORTJIVEView.Table") = value
        End Set
    End Property



    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        'Session("MappingUserIdHIPORTJIVEView.PK_MappingUserIdSTRPPATK_ApprovalDetail_ID") = Nothing
        Session("MappingUserIdHIPORTJIVEView.SetnGetUserName") = Nothing
        SetnGetCurrentPage = Nothing
        Me.SetnGetRowTotal = Nothing
        Me.SetnGetSort = Nothing

    End Sub


    Private Sub bindgrid()
        SettingControlSearching()
        Me.GridGMHiportJive.DataSource = Me.SetnGetBindTable
        Me.GridGMHiportJive.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridGMHiportJive.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridGMHiportJive.DataBind()

        'If SetnGetRowTotal > 0 Then
        '    LabelNoRecordFound.Visible = False
        'Else
        '    LabelNoRecordFound.Visible = True
        'End If
    End Sub

    Private Sub SettingPropertySearching()

        SetnGetUserName = TxtUserName.Text.Trim

    End Sub

    Private Sub SettingControlSearching()

        TxtUserName.Text = SetnGetUserName

    End Sub



    Private Sub SetInfoNavigate()

        Me.PageCurrentPage.Text = (Me.SetnGetCurrentPage + 1).ToString
        Me.PageTotalPages.Text = Me.GetPageTotal.ToString
        Me.PageTotalRows.Text = Me.SetnGetRowTotal.ToString
        Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
        Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Try
            Me.bindgrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub ImageButtonSearchCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearchCancel.Click
        Try

            SetnGetUserName = Nothing

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            SettingPropertySearching()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

                ClearThisPageSessions()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                bindgrid()

            End If
            GridGMHiportJive.PageSize = CInt(Sahassa.AML.Commonly.GetDisplayedTotalRow)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LinkButtonAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddNew.Click

        Try
            Response.Redirect("MappingUserIdHIPORTJIVEAdd.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub GridGMHIPORTJIVE_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridGMHiportJive.EditCommand
        Dim PK_MappingUserIdHIPORTJIVE_ID As Integer 'primary key
        'Dim StrUserID As String  'unik
        Try
            PK_MappingUserIdHIPORTJIVE_ID = CType(e.Item.Cells(1).Text, Integer)
            'StrUserID = e.Item.Cells(2).Text

            Response.Redirect("MappingUserIdHIPORTJIVEEdit.aspx?PK_MappingUserIdHIPORTJIVE_ID=" & PK_MappingUserIdHIPORTJIVE_ID, False)
            'Using ObjMsBranchBll As New SahassaBLL.MsHelpBLL
            '    If ObjMsBranchBll.IsNotExistInApproval(StrUserID) Then
            '        Response.Redirect("MsHelpEdit.aspx?PkMsHelpID=" & PkMsHelpID, False)
            '    End If
            'End Using

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridGMHIPORTJIVE_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridGMHiportJive.DeleteCommand
        Dim PK_MappingUserIdHIPORTJIVE_ID As Integer 'primary key
        'Dim StrUnikID As String  'unik
        Try
            PK_MappingUserIdHIPORTJIVE_ID = CType(e.Item.Cells(1).Text, Integer)
            'StrUnikID = e.Item.Cells(2).Text

            Response.Redirect("MappingUserIdHIPORTJIVEDelete.aspx?PK_MappingUserIdHIPORTJIVE_ID=" & PK_MappingUserIdHIPORTJIVE_ID, False)
            'Using ObjMsBranchBll As New SahassaBLL.MsHelpBLL
            '    If ObjMsBranchBll.IsNotExistInApproval(StrUnikID) Then
            '        Response.Redirect("MsHelpDelete.aspx?PkMsHelpID=" & PkMsHelpID, False)
            '    End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub



    Protected Sub GridGMHIPORTJIVE_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridGMHiportJive.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                Dim EditButton As LinkButton = CType(e.Item.Cells(4).FindControl("LnkEdit"), LinkButton)
                Dim DelButton As LinkButton = CType(e.Item.Cells(5).FindControl("LnkDelete"), LinkButton)

                e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try            
            If IsNumeric(Me.TextGoToPage.Text) AndAlso (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    Throw New Exception(Sahassa.AML.CommonlyEnum.EXECPTION_PAGENUMBER_GREATER_TOTALPAGENUMBER)
                End If
            Else
                Throw New Exception(Sahassa.AML.CommonlyEnum.EXECPTION_PAGENUMBER_NUMERICPOSITIF)
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridGMHiportJive_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridGMHiportJive.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class


