<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MappingGroupMenuHiportJiveApproval.aspx.vb" Inherits="MappingGroupMenuHiportJiveApproval" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<script src="script/popcalendar.js"></script>
<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Mapping Group Menu Hiport Jive &nbsp;- Approval
                    <hr />
                </strong>
            </td>
        </tr>
    </table>
    <TABLE borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
        <TR>
            <TD align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></TD>
				<TD vAlign="top" width="98%" bgColor="#ffffff"><TABLE cellSpacing="4" cellPadding="0" width="100%" border="0">
						<TR>
							<TD class="Regtext" noWrap style="height: 154px">Search By :
							</TD>
							<TD vAlign="middle" noWrap style="height: 154px"><ajax:ajaxpanel id="AjaxPanel1" runat="server">
                                &nbsp; <TABLE cellSpacing="1" cellPadding="2" width="100%"
					border="0" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                                    <tr>
                                        <TD style="width: 15%; height: 22px; background-color: #fff7e6" nowrap="noWrap">
                                            <asp:Label id="subject" runat="server" meta:resourcekey="LblGroupMenuNameResource1" Text="GroupMenu"></asp:Label>
                                        </td>
                                        <td nowrap="nowrap" style="width: 367px; height: 22px; background-color: #fff7e6"><asp:TextBox id="TxtGroupName" tabIndex=2 runat="server" CssClass="searcheditbox" Width="200px" meta:resourcekey="TxtGroupMenuNameResource1" MaxLength="100"></asp:TextBox> </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; background-color: #fff7e6" nowrap="noWrap">
                                            <asp:Label ID="LblDescription" runat="server" meta:resourcekey="LblDescriptionResource1"
                                                Text="Mode"></asp:Label>
                                        </td>
                                        <td style="width: 367px; background-color: #fff7e6" nowrap="noWrap">
                                            <asp:TextBox CssClass="searcheditbox" ID="TxtNama" MaxLength="1000" meta:resourcekey="TxtDescriptionResource1" runat="server" TabIndex="3" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; background-color: #fff7e6" nowrap="noWrap">
                                            <asp:Label ID="Label1" runat="server" meta:resourcekey="LblDescriptionResource1"
                                                Text="Preparer"></asp:Label>
                                        </td>
                                        <td style="width: 367px; background-color: #fff7e6" nowrap="noWrap">
                                            <asp:TextBox CssClass="searcheditbox" ID="TxtPreparer" MaxLength="1000" meta:resourcekey="TxtDescriptionResource1" runat="server" TabIndex="3" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; background-color: #fff7e6" nowrap="noWrap">
                                            <asp:Label ID="Label2" runat="server" meta:resourcekey="LblDescriptionResource1"
                                                Text="Created Date"></asp:Label>
                                        </td>
                                        <td style="width: 367px; background-color: #fff7e6" nowrap="noWrap">
                                            &nbsp;<asp:TextBox ID="TxtEntryDateFrom" runat="server" CssClass="searcheditbox"
                                                TabIndex="2"></asp:TextBox><input id="popUpEntryDate" runat="server" name="popUpCalc"
                                                    style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                    background-image: url(Script/Calendar/cal.gif); border-left: #ffffff 0px solid;
                                                    width: 16px; border-bottom: #ffffff 0px solid; height: 17px" title="Click to show calendar"
                                                    type="button" /><asp:Label ID="Label3" runat="server" Text="Until"></asp:Label><asp:TextBox
                                                        ID="TxtEntryDateUntil" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox><input
                                                            id="popUpEntryDateUntil" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                            border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                            border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                            height: 17px" title="Click to show calendar" type="button" /></td>
                                    </tr>
                                </table>
                            </ajax:ajaxpanel></TD>
							<TD vAlign="middle" width="99%" style="height: 154px"><ajax:ajaxpanel id="AjaxPanel2" runat="server">
									<asp:imagebutton id="ImageButtonSearch" tabIndex="3" runat="server" SkinID="SearchButton"></asp:imagebutton>
                                <asp:ImageButton ID="ImageButtonSearchCancel" runat="server"
                                    TabIndex="3" ImageUrl="~/Images/button/clearsearch.gif" /></ajax:ajaxpanel>
								</TD>
						</TR>
					</TABLE><ajax:ajaxpanel id="AjaxPanel3" runat="server">
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:ajaxpanel></TD>
			</TR>
	</TABLE>
	
<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
  <tr> 
    <td >
        <ajax:AjaxPanel ID="AjaxPanel14"
                runat="server" Width="672px">
            <asp:DataGrid ID="GridGMHiportJive" runat="server"
                    AllowSorting="True" AutoGenerateColumns="False" BackColor="White"
                    BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                Font-Size="XX-Small" ForeColor="Black"
                    GridLines="Vertical" Width="100%" AllowCustomPaging="True">
                    <FooterStyle BackColor="#CCCC99" />
                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                        Visible="False" />
                    <AlternatingItemStyle BackColor="White" />
                    <ItemStyle BackColor="#F7F7DE" />
                    <Columns>
                        <asp:BoundColumn HeaderText="No">
                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" ForeColor="#FFFFFF" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="PK_MappingGroupMenuHiportJive_ApprovalDetail_ID" Visible="False">
                            <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" Wrap="False" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Preparer" HeaderText="Preparer" SortExpression="Preparer desc">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="GroupName" HeaderText="Group Menu" SortExpression="GroupName desc">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Nama" HeaderText="Mode" SortExpression="Nama Desc">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="CreatedDate" HeaderText="CreatedDate" SortExpression="CreatedDate Desc">
                        </asp:BoundColumn>
                        <asp:TemplateColumn>
                            <EditItemTemplate>
                                <asp:LinkButton runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                &nbsp;<asp:LinkButton runat="server" CausesValidation="false" CommandName="Cancel" 
                                    Text="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CausesValidation="false" CommandName="Edit" 
                                    Font-Bold="True" ForeColor="Black" Text="Detail" EnableTheming="False"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="XX-Small" ForeColor="White" />
                </asp:DataGrid>
            </ajax:AjaxPanel>
    </td>
  </tr>
  <tr> 
    <td style="background-color:#ffffff"><table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td nowrap>
                &nbsp;</td>
			 <td width="99%">&nbsp;&nbsp;</td>
			<td align="right" nowrap>&nbsp;&nbsp;</td>
		</tr>
      </table></td>
  </tr>
  <tr> 
    <td bgColor="#ffffff"> <TABLE id="Table3" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
        <TR class="regtext" align="center" bgColor="#dddddd"> 
          <TD vAlign="top" align="left" width="50%" bgColor="#ffffff">Page&nbsp;<ajax:ajaxpanel id="AjaxPanel6" runat="server"> 
            <asp:label id="PageCurrentPage" runat="server" CssClass="regtext">0</asp:label>
            &nbsp;of&nbsp; 
            <asp:label id="PageTotalPages" runat="server" CssClass="regtext">0</asp:label>
            </ajax:ajaxpanel></TD>
          <TD vAlign="top" align="right" width="50%" bgColor="#ffffff">Total Records&nbsp; 
            <ajax:ajaxpanel id="AjaxPanel7" runat="server"> 
            <asp:label id="PageTotalRows" runat="server">0</asp:label>
            </ajax:ajaxpanel></TD>
        </TR>
      </TABLE>
      <TABLE id="Table4" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
        <TR bgColor="#ffffff"> 
          <TD class="regtext" vAlign="middle" align="left" colSpan="11" height="7"> 
            <HR color="#f40101" noShade SIZE="1"> </TD>
        </TR>
        <TR> 
          <TD class="regtext" vAlign="middle" align="left" width="63" bgColor="#ffffff">Go 
            to page</TD>
          <TD class="regtext" vAlign="middle" align="left" width="5" bgColor="#ffffff"><FONT face="Verdana, Arial, Helvetica, sans-serif" size="1"> 
            <ajax:ajaxpanel id="AjaxPanel8" runat="server"> 
            <asp:textbox id="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:textbox>
            </ajax:ajaxpanel> </FONT></TD>
          <TD class="regtext" vAlign="middle" align="left" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel9" runat="server"> 
            <asp:imagebutton id="ImageButtonGo" runat="server" SkinID="GoButton"></asp:imagebutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/first.gif" width="6"> 
          </TD>
          <TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel10" runat="server"> 
            <asp:linkbutton id="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First" OnCommand="PageNavigate">First</asp:linkbutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><IMG height="5" src="images/prev.gif" width="6"></TD>
          <TD class="regtext" vAlign="middle" align="right" width="14" bgColor="#ffffff"> 
            <ajax:ajaxpanel id="AjaxPanel11" runat="server"> 
            <asp:linkbutton id="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev" OnCommand="PageNavigate">Previous</asp:linkbutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="right" width="60" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel12" runat="server"><A class="pageNav" href="#"> 
            <asp:linkbutton id="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next" OnCommand="PageNavigate">Next</asp:linkbutton>
            </A></ajax:ajaxpanel></TD>
          <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/next.gif" width="6"></TD>
          <TD class="regtext" vAlign="middle" align="left" width="25" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel13" runat="server"> 
            <asp:linkbutton id="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last" OnCommand="PageNavigate">Last</asp:linkbutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/last.gif" width="6"></TD>
        </TR>
      </TABLE></td>
  </tr>
</table>

</asp:Content>

