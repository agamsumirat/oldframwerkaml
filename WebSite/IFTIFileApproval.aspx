<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="IFTIFileApproval.aspx.vb" Inherits="IFTIFileApproval" title="Untitled Page" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<%@ Register Assembly="SahassaNettier.Web" Namespace="SahassaNettier.Web.Data" TagPrefix="data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <script src="Script/popcalendar.js"></script>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                            </td>
                        <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="5" height="100%" /></td>
                            <td class="divcontentinside" bgcolor="#FFFFFF" width="100%">
                                <ajax:AjaxPanel ID="a" runat="server">
                                    
                                </ajax:AjaxPanel>
                                <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                    style="border-top-style: none; border-right-style: none; border-left-style: none;
                                    border-bottom-style: none" width="100%">
                                    <tr>
                                        <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                            border-right-style: none; border-left-style: none; border-bottom-style: none;
                                            font-family: Tahoma">
                                            <img src="Images/dot_title.gif" width="17" height="17">&nbsp;<b><asp:Label ID="Label1"
                                                runat="server" Text="IFTI File Approval"></asp:Label></b><hr />
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" style="background-color: White;
                                    border-color: White;" width="100%">
                                    <tr>
                                        <td>
                                            <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                                border="2" height="231" style="border-top-style: none; border-right-style: none;
                                                border-left-style: none; border-bottom-style: none">
                                                <tr >
                                                    <td bgcolor="#ffffff" colspan="4">
                                                        <strong><span style="font-size: 18px"></span></strong>
                                                        <hr />
                                                        <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                                                            Width="100%"></asp:Label></td>
                                                </tr>
                                                <tr >
                                                    <td bgcolor="#ffffff" colspan="4">
                                                        <span style="color: #ff0000">* Required</span></td>
                                                </tr>
                                                <tr >
                                                    <td bgcolor="#ffffff" height="24" style="width: 20px">
                                                        <br />
                                                    </td>
                                                    <td width="200" bgcolor="#ffffff" style="width: 200px">
                                                        Transaction Date&nbsp;</td>
                                                    <td width="5" bgcolor="#ffffff" style="width: 2px">
                                                        :</td>
                                                    <td width="90%" bgcolor="#ffffff" style="width: 99%">
                                                        <asp:Label ID="LblTransactionDate" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#ffffff" height="24" style="width: 20px">
                                                        <br />
                                                    </td>
                                                    <td bgcolor="#ffffff" style="width: 200px">
                                                        Report Date</td>
                                                    <td bgcolor="#ffffff" style="width: 2px">
                                                        :</td>
                                                    <td bgcolor="#ffffff" style="width: 99%">
                                                        <asp:Label ID="LblReportdate" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr >
                                                    <td bgcolor="#ffffff" height="24" style="width: 20px">
                                                        &nbsp;</td>
                                                    <td bgcolor="#ffffff" style="width: 200px">
                                                        Transaction Type</td>
                                                    <td bgcolor="#ffffff" style="width: 2px">
                                                        :</td>
                                                    <td bgcolor="#ffffff" style="width: 99%">
                                                        <asp:Label ID="LblTransactionType" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr >
                                                    <td bgcolor="#ffffff" style="width: 20px;">
                                                    </td>
                                                    <td bgcolor="#ffffff" style="width: 200px;">
                                                        Total Transaction
                                                    </td>
                                                    <td bgcolor="#ffffff" style="width: 2px;">
                                                        :</td>
                                                    <td bgcolor="#ffffff" style="width: 99%;">
                                                        <asp:Label ID="LblTotalTransaction" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr >
                                                    <td bgcolor="#ffffff" height="24" style="width: 20px">
                                                        <br />
                                                    </td>
                                                    <td bgcolor="#ffffff" style="width: 200px">
                                                        Valid</td>
                                                    <td bgcolor="#ffffff" style="width: 2px">
                                                        :</td>
                                                    <td bgcolor="#ffffff" style="width: 99%">
                                                        <asp:Label ID="LblValid" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr >
                                                    <td bgcolor="#ffffff" height="24" style="width: 20px">
                                                    </td>
                                                    <td bgcolor="#ffffff" style="width: 200px">
                                                        Invalid</td>
                                                    <td bgcolor="#ffffff" style="width: 2px">
                                                        :</td>
                                                    <td bgcolor="#ffffff" style="width: 99%">
                                                        <asp:Label ID="LblInValid" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr >
                                                    <td bgcolor="#ffffff" height="24" style="width: 20px">
                                                        &nbsp;</td>
                                                    <td bgcolor="#ffffff" style="width: 200px">
                                                        Last Report Type</td>
                                                    <td bgcolor="#ffffff" style="width: 2px">
                                                        :</td>
                                                    <td bgcolor="#ffffff" style="width: 99%">
                                                        <asp:Label ID="LblLastReportType" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr >
                                                    <td bgcolor="#ffffff" height="24" style="width: 20px">
                                                    </td>
                                                    <td bgcolor="#ffffff" style="width: 200px">
                                                        Status Upload to PPATK</td>
                                                    <td bgcolor="#ffffff" style="width: 2px">
                                                        :</td>
                                                    <td bgcolor="#ffffff" style="width: 99%">
                                                        <asp:Label ID="LblStatusUploadPPATK" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr >
                                                    
                                                    <td bgcolor="#ffffff" colspan="4">
                                                        <strong>List Transaction Valid IFTI</strong></td>
                                                </tr>
                                                <tr >
                                                    
                                                    <td bgcolor="#ffffff" colspan="4">
                                                        <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                                        
                                                            <asp:GridView ID="GrdIFTI" DataSourceID="IFTIDS" runat="server" BackColor="White"
                                                                BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black"
                                                                GridLines="Vertical" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False">
                                                                <RowStyle BackColor="#F7F7DE" />
                                                                <FooterStyle BackColor="#CCCC99" />
                                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Left" />
                                                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                <AlternatingRowStyle BackColor="White" />
                                                                <PagerSettings Mode="NumericFirstLast" />
                                                                <Columns>
                                                                    <asp:BoundField DataField="Nama" HeaderText="Nama" SortExpression="Nama" />
                                                                    <asp:BoundField DataField="TransactionSwift" HeaderText="TransactionSwift" SortExpression="TransactionSwift" />
                                                                    <asp:BoundField DataField="TransactionType" HeaderText="TransactionType" SortExpression="TransactionType" />
                                                                    <asp:BoundField DataField="AccountNumber" HeaderText="AccountNumber" SortExpression="AccountNumber" />
                                                                    <asp:BoundField DataField="ReportToPPATK" HeaderText="ReportToPPATK" SortExpression="ReportToPPATK" />
                                                                    <asp:BoundField DataField="TanggalTransaksi" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="TanggalTransaksi"
                                                                        HtmlEncode="False" SortExpression="TanggalTransaksi" />
                                                                    <asp:BoundField DataField="ValueDate_NilaiTransaksi" HeaderText="ValueDate_NilaiTransaksi"
                                                                        SortExpression="ValueDate_NilaiTransaksi" />
                                                                    <asp:BoundField DataField="ValueDate_NilaiTransaksiIDR" HeaderText="ValueDate_NilaiTransaksiIDR"
                                                                        SortExpression="ValueDate_NilaiTransaksiIDR" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            &nbsp;
                                                            <data:vw_IFTIViewDataSource runat="server" ID="IFTIDS" SelectMethod="GetPaged" EnablePaging="true"
                                                                EnableSorting="true" >
                                                               
                                                            </data:vw_IFTIViewDataSource>
                                                        </ajax:AjaxPanel>
                                                       
                                                    </td>
                                                </tr>
                                                </table>
                                                <table>
                                                <tr  bgcolor="#dddddd" height="30" width="100%">
                                                    <td style="width: 22px">
                                                        <img height="15" src="images/arrow.gif" width="15"></td>
                                                    <td colspan="3">
                                                        <table cellspacing="0" cellpadding="3" border="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="ImgAccept" runat="server" ImageUrl="~/Images/button/accept.gif" /></td>
                                                                <td>
                                                                    <asp:ImageButton ID="ImgCancel" runat="server" ImageUrl="~/Images/button/back.gif" /></td>
                                                            </tr>
                                                        </table>
                                                        <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="Images/blank.gif" width="5" height="1" /></td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="images/arrow.gif" width="15" height="15" /></td>
                        <td width="99%" background="Images/button-bground.gif">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td>
                            </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>

