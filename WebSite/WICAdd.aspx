<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
	CodeFile="WICAdd.aspx.vb" Inherits="WICAdd" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

	<script src="Script/popcalendar.js"></script>
	

	<script language="javascript" type="text/javascript">
	  function hidePanel(objhide,objpanel,imgmin,imgmax)
	  {
		document.getElementById(objhide).style.display='none';
		document.getElementById(objpanel).src=imgmax;
	  }
	  // JScript File

	  function popWin2()
	  {
		var height = '600px';
		var width = '550px';
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

		window.showModalDialog("PickerKelurahan.aspx", "#1", winSetting);    
		
	  }

	 
	  function popWinKecamatan()
	  {
		var height = '600px';
		var width = '550px';
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

		window.showModalDialog("PickerKecamatan.aspx", "#2", winSetting);
	  }
	  function popWinNegara()
	  {
		var height = '600px';
		var width = '550px';
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

		window.showModalDialog("PickerNegara.aspx", "#3", winSetting);
	  }
	  function popWinMataUang()
	  {
		var height = '600px';
		var width = '550px';
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

		window.showModalDialog("PickerMataUang.aspx", "#4", winSetting);
		
	  }
	  function popWinProvinsi()
	  {
		var height = '600px';
		var width = '550px';
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

		window.showModalDialog("PickerProvinsi.aspx", "#5", winSetting);
	  }
	  function popWinPekerjaan()
	  {
		var height = '600px';
		var width = '550px';
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

		window.showModalDialog("PickerPekerjaan.aspx", "#6", winSetting);
	  }
	  function popWinKotaKab()
	  {
		var height = '600px';
		var width = '550px';
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

		window.showModalDialog("PickerKotaKab.aspx", "#7", winSetting);
	  }
	  function popWinBidangUsaha()
	  {
		var height = '600px';
		var width = '550px';
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

		window.showModalDialog("PickerBidangUsaha.aspx", "#8", winSetting);
	  }
	</script>

	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td>
				<img src="Images/blank.gif" width="5" height="1" /></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td>
							</td>
						<td width="99%" bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" /></td>
						<td bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" /></td>
					</tr>
				</table>
			</td>
			<td>
				<img src="Images/blank.gif" width="5" height="1" /></td>
		</tr>
		<tr>
			<td>
			</td>
			<td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
				<div id="divcontent" >
					<table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td bgcolor="#ffffff" class="divcontentinside" colspan="2">
								<img src="Images/blank.gif" width="20" height="100%" /><ajax:AjaxPanel ID="a" runat="server">
								</ajax:AjaxPanel>
								<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" border="0"
									style="border-top-style: none; border-right-style: none; border-left-style: none;
									border-bottom-style: none">
									<tr>
										<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
											border-right-style: none; border-left-style: none; border-bottom-style: none">
											<img src="Images/dot_title.gif" width="17" height="17">
											<strong>
												<asp:Label ID="Label1" runat="server" Text="WIC - Add New"></asp:Label></strong>
											<hr />
										</td>
									</tr>
									<tr>
										<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
											border-right-style: none; border-left-style: none; border-bottom-style: none">
											<ajax:AjaxPanel ID="AjaxPanel1" runat="server">
												<asp:Label ID="LblSucces" CssClass="validationok" Width="94%" runat="server" Visible="False"></asp:Label>
											</ajax:AjaxPanel>
										</td>
									</tr>
								</table>
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											&nbsp;</td>
									</tr>
								</table>
								<ajax:AjaxPanel ID="AjaxMultiView" runat="server" Width="100%">
									<asp:MultiView ID="mtvPage" runat="server" ActiveViewIndex="0">
										<asp:View ID="vwTransaksi" runat="server">
											<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
												border="0">
												<tbody>
													<tr>
														<td style="height: 6px" valign="middle" align="left" width="100%" background="Images/search-bar-background.gif">
															<table cellspacing="0" cellpadding="0" border="0">
																<tbody>
																	<tr>
																		<td style="height: 13px" class="formtext">
																			<asp:Label ID="Label8" runat="server" Text="A. UMUM" Font-Bold="True"></asp:Label>&nbsp;</td>
																		<td style="height: 13px">
																			<a title="click to minimize or maximize" onclick="javascript:ShowHidePanel('Umum','searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
																				href="#">
																				<img id="searchimage2" height="12" src="Images/search-bar-minimize.gif" width="12"
																					border="0" /></a></td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
													<tr id="Umum">
														<td style="height: 10px" bgcolor="#ffffff" colspan="2">
															<table>
																<tbody>
																	<tr>
																		<td class="formtext">
																			<asp:Label ID="Label4" runat="server" Text="1. PIHAK PELAPOR" Font-Bold="True"></asp:Label>&nbsp;
																		</td>
																	</tr>
																	<tr>
																		<td class="formtext">
																			<table>
																				<tbody>
																					<tr>
																						<td style="width: 5px">
																						</td>
																						<td class="formText">
																							1.1. Nama PJK Pelapor <span style="color: #cc0000">*</span>
																						</td>
																						<td style="width: 5px">
																							:
																						</td>
																						<td class="formtext">
																							<asp:TextBox ID="txtUmumPJKPelapor" runat="server" CssClass="textBox" Width="240px"></asp:TextBox>
																							<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Nama PJK Pelapor harus diisi"
																								ControlToValidate="txtUmumPJKPelapor">*</asp:RequiredFieldValidator>&nbsp;
																						</td>
																					</tr>
																					<tr>
																						<td style="width: 5px">
																						</td>
																						<td class="formText">
																							1.2. Tanggal Pelaporan <span style="color: #cc0000">*</span>
																						</td>
																						<td style="width: 5px">
																							:
																						</td>
																						<td class="formtext">
																							<asp:TextBox ID="txtTglLaporan" runat="server" CssClass="textBox" Width="96px" Enabled="False"
																								MaxLength="50"></asp:TextBox><input style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid;
																									font-size: 11px; background-image: url(Script/Calendar/cal.gif); border-left: #ffffff 0px solid;
																									width: 16px; border-bottom: #ffffff 0px solid; height: 17px" id="popUpTglLaporan"
																									title="Click to show calendar" onclick="popUpCalendar(this, frmBasicCTRWeb.txtTglLaporan, 'dd-mmm-yyyy')"
																									type="button" runat="server" />
																							<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="None"
																								ErrorMessage="Tanggal Pelaporan harus diisi" ControlToValidate="txtTglLaporan">*</asp:RequiredFieldValidator></td>
																					</tr>
																					<tr>
																						<td style="width: 5px">
																						</td>
																						<td class="formText">
																							1.3. Nama Pejabat PJK Pelapor <span style="color: #cc0000">*</span>
																						</td>
																						<td style="width: 5px">
																							:
																						</td>
																						<td class="formtext">
																							<asp:TextBox ID="txtPejabatPelapor" runat="server" CssClass="textBox" Width="240px"></asp:TextBox>
																							<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="None"
																								ErrorMessage="Nama Pejabat PJK Pelapor harus diisi" ControlToValidate="txtPejabatPelapor">*</asp:RequiredFieldValidator></td>
																					</tr>
                                                                                    <tr>
                                                                                        <td style="width: 5px">
                                                                                        </td>
                                                                                        <td class="formText">
                                                                            Debet/Credit</td>
                                                                                        <td style="width: 5px">
                                                                                        </td>
                                                                                        <td class="formtext">
                                                                            <ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                                                                <asp:DropDownList runat="server" ID="cboDebitCredit" CssClass="combobox">
                                                                                    <asp:ListItem Value="D">Debet</asp:ListItem>
                                                                                    <asp:ListItem Value="C">Credit</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </ajax:AjaxPanel>
                                                                                        </td>
                                                                                    </tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td class="formtext">
																			<asp:Label ID="Label5" runat="server" Text="2. JENIS LAPORAN (PILIH SALAH SATU)"
																				Font-Bold="True"></asp:Label>&nbsp;
																		</td>
																	</tr>
																	<tr>
																		<td class="formtext">
																			<ajax:AjaxPanel ID="pnlUpdate" runat="server">
																				<table>
																					<tbody>
																						<tr>
																							<td style="width: 5px">
																							</td>
																							<td class="formText">
																								2.1. Tipe Laporan <span style="color: #cc0000">*</span>
																							</td>
																							<td style="width: 5px">
																								:
																							</td>
																							<td class="formtext">
																								<asp:DropDownList ID="cboTipeLaporan" runat="server" CssClass="combobox" AutoPostBack="True">
																									<asp:ListItem Value="0">Laporan Baru</asp:ListItem>
																									<asp:ListItem Value="1">Laporan Koreksi</asp:ListItem>
																								</asp:DropDownList>
																							</td>
																						</tr>
																						<tr>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																							</td>
																							<td style="width: 5px">
																								&nbsp;</td>
																							<td class="formtext">
																								<table id="tblWICKoreksi" runat="server" visible="False">
																									<tbody>
																										<tr>
																											<td class="formText">
																												2.1.1. No. WIC yang dikoreksi <span style="color: #cc0000">*</span>
																											</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:TextBox ID="txtNoLTKTKoreksi" runat="server" CssClass="textBox"></asp:TextBox>
																												&nbsp;
																											</td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																					</tbody>
																				</table>
																			</ajax:AjaxPanel>
																		</td>
																	</tr>

																</tbody>
															</table>
															
														</td>
													</tr>
													<tr>
														<td style="height: 6px" valign="middle" align="left" width="100%" background="Images/search-bar-background.gif">
															<table cellspacing="0" cellpadding="0" border="0">
																<tbody>
																	<tr>
																		<td class="formtext">
																			<asp:Label ID="Label2" runat="server" Text="B. IDENTITAS TERLAPOR" Font-Bold="True"></asp:Label>&nbsp;</td>
																		<td>
																			<a title="click to minimize or maximize" onclick="javascript:ShowHidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
																				href="#">
																				<img id="Img1" height="12" src="Images/search-bar-minimize.gif" width="12" border="0" /></a></td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
													<tr id="IdTerlapor">
														<td bgcolor="#ffffff" colspan="2">
															<ajax:AjaxPanel ID="ajxPnlTerlapor" runat="server" Width="100%">
																<table width="100%">
																	<tbody>
																		<tr>
																			<td class="formtext">
																				<asp:Label ID="Label6" runat="server" Text="3. TERLAPOR" Font-Bold="True"></asp:Label>&nbsp;
																			</td>
																		</tr>
																		<tr>
																			<td class="formtext">
																				<table>
																					<tbody>
																						<tr>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																								<strong>3.1. Kepemilikan</strong> <span style="color: #cc0000">*</span>
																							</td>
																							<td style="width: 5px">
																								:
																							</td>
																							<td class="formtext">
																								<asp:DropDownList ID="cboTerlaporKepemilikan" runat="server" CssClass="combobox">
																								</asp:DropDownList></td>
																						</tr>
																						<tr>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																								&nbsp;</td>
																							<td style="width: 5px">
																							</td>
																							<td class="formtext">
																								<table width="100%">
																									<tbody>
																										<tr>
																											<td style="width: 139px" class="formText">
																												<strong>3.1.1. No. Rekening</strong> </td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:TextBox ID="txtTerlaporNoRekening" runat="server" CssClass="textBox" Width="258px"></asp:TextBox>
																												</td>
																										</tr>
																										<tr>
																											<td style="width: 139px" class="formText">
																												Tipe Pelapor</td>
																											<td style="width: 5px">
																												:</td>
																											<td class="formtext">
																												<asp:RadioButtonList ID="rblTerlaporTipePelapor" runat="server" AutoPostBack="True"
																													RepeatDirection="Horizontal">
																													<asp:ListItem Value="1">Perorangan</asp:ListItem>
																													<asp:ListItem Value="2">Korporasi</asp:ListItem>
																												</asp:RadioButtonList>
																											</td>
																										</tr>
																										<tr>
																											<td class="formtext" colspan="3">
																												<div id="divPerorangan" runat="server" hidden="false">
																													<table>
																														<tbody>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<strong>3.1.2. Perorangan</strong>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	a. Gelar</td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:TextBox ID="txtTerlaporGelar" runat="server" CssClass="textBox"></asp:TextBox></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	b. Nama Lengkap <span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:TextBox ID="txtTerlaporNamaLengkap" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	c. Tempat Lahir <span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:TextBox ID="txtTerlaporTempatLahir" runat="server" CssClass="textBox" Width="240px"></asp:TextBox></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	d. Tanggal Lahir <span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<br />
																																	<asp:TextBox ID="txtTerlaporTglLahir" runat="server" CssClass="textBox" Width="96px"
																																		MaxLength="50"></asp:TextBox><input style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid;
																																			font-size: 11px; background-image: url(Script/Calendar/cal.gif); border-left: #ffffff 0px solid;
																																			width: 16px; border-bottom: #ffffff 0px solid; height: 17px" id="popTglLahirTerlapor"
																																			title="Click to show calendar" onclick="popUpCalendar(this, frmBasicCTRWeb.txtTglLahir, 'dd-mmm-yyyy')"
																																			type="button" runat="server" /></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	e. Kewarganegaraan (Pilih salah satu) <span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:RadioButtonList ID="rblTerlaporKewarganegaraan" runat="server" AutoPostBack="True"
																																		RepeatDirection="Horizontal">
																																		<asp:ListItem Value="1">WNI</asp:ListItem>
																																		<asp:ListItem Value="2">WNA</asp:ListItem>
																																	</asp:RadioButtonList></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	f. Negara <span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:DropDownList ID="cboTerlaporNegara" runat="server" CssClass="combobox">
																																	</asp:DropDownList></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	g. Alamat Domisili</td>
																																<td style="width: 5px">
																																</td>
																																<td class="formtext">
																																</td>
																															</tr>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<table>
																																		<tbody>
																																			<tr>
																																				<td class="formtext">
																																					&nbsp; &nbsp;&nbsp;
																																				</td>
																																				<td class="formText">
																																					Nama Jalan</td>
																																				<td style="width: 5px">
																																					:
																																				</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtTerlaporDOMNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					RT/RW</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtTerlaporDOMRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kelurahan</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext" valign="top">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporDOMKelurahan" runat="server" CssClass="textbox" Width="167px"
																																										Enabled="False" ajaxcall="none"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporDOMKelurahan" runat="server" 
                                                                                                                                                                        OnClientClick="javascript:popWin2();" ImageUrl="~/Images/button/browse.gif" 
                                                                                                                                                                        CausesValidation="False">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporDOMKelurahan" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kecamatan</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					&nbsp;<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporDOMKecamatan" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporDOMKecamatan" runat="server"
																																										ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKecamatan();">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporDOMKecamatan" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kota / Kabupaten<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporDOMKotaKab" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporDOMKotaKab" runat="server"
																																										ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKotaKab();">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporDOMKotaKab" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Provinsi<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporDOMProvinsi" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporDOMProvinsi" runat="server"
																																										ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinProvinsi();">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporDOMProvinsi" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
                                                                                                                                            <tr>
                                                                                                                                                <td class="formtext">
                                                                                                                                                </td>
                                                                                                                                                <td class="formText">
                                                                                                                                                    Negara<span style="color: #cc0000">*</span></td>
                                                                                                                                                <td style="width: 5px">
                                                                                                                                                    :</td>
                                                                                                                                                <td class="formtext">
                                                                                                                                                    <table style="width: 127px">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td>
                                                                                                                                                                    <asp:TextBox ID="txtTerlaporDOMNegara" runat="server" CssClass="textBox" Enabled="False"
                                                                                                                                                                        Width="167px"></asp:TextBox></td>
                                                                                                                                                                <td>
                                                                                                                                                                    <asp:ImageButton ID="imgTerlaporDOMNegara" runat="server" CausesValidation="False"
                                                                                                                                                                        ImageUrl="~/Images/button/browse.gif" OnClientClick="javascript:popWinNegara();" /></td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                    <asp:HiddenField ID="hfTerlaporDomNegara" runat="server" />
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kode Pos</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtTerlaporDOMKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                                                                            </tr>
                                                                                                                                        
																																		</tbody>
																																	</table>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	h. Alamat Sesuai Bukti Identitas</td>
																																<td style="width: 5px">
																																</td>
																																<td class="formtext">
																																	<asp:CheckBox ID="chkCopyAlamatDOM" runat="server" Text="Sama dengan Alamat Domisili"
																																		AutoPostBack="True"></asp:CheckBox></td>
																															</tr>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<table>
																																		<tbody>
																																			<tr>
																																				<td class="formText">
																																					&nbsp; &nbsp;&nbsp;
																																				</td>
																																				<td class="formText">
																																					Nama Jalan</td>
																																				<td style="width: 5px">
																																					:
																																				</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtTerlaporIDNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					RT/RW</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtTerlaporIDRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Kelurahan</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext" valign="top">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporIDKelurahan" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporIDKelurahan" runat="server" 
																																										ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWin2();">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporIDKelurahan" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Kecamatan</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					&nbsp;<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporIDKecamatan" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporIDKecamatan" runat="server" 
																																										ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKecamatan();">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporIDKecamatan" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Kota / Kabupaten<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporIDKotaKab" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporIDKotaKab" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																										CausesValidation="False" OnClientClick="javascript:popWinKotaKab();"></asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporIDKotaKab" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Provinsi<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td style="height: 22px">
																																									<asp:TextBox ID="txtTerlaporIDProvinsi" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td style="height: 22px">
																																									<asp:ImageButton ID="imgTerlaporIDProvinsi" runat="server" 
																																										ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinProvinsi();">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporIDProvinsi" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
                                                                                                                                            <tr>
                                                                                                                                                <td class="formtext">
                                                                                                                                                </td>
                                                                                                                                                <td class="formText">
                                                                                                                                                    Negara<span style="color: #cc0000">*</span></td>
                                                                                                                                                <td style="width: 5px">
                                                                                                                                                    :</td>
                                                                                                                                                <td class="formtext">
                                                                                                                                                    <table style="width: 127px">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td>
                                                                                                                                                                    <asp:TextBox ID="txtTerlaporIDNegara" runat="server" CssClass="textBox" 
                                                                                                                                                                        Enabled="False" Width="167px"></asp:TextBox>
                                                                                                                                                                </td>
                                                                                                                                                                <td>
                                                                                                                                                                    <asp:ImageButton ID="imgTerlaporIDNegara" runat="server" 
                                                                                                                                                                        CausesValidation="False" ImageUrl="~/Images/button/browse.gif" 
                                                                                                                                                                        OnClientClick="javascript:popWinNegara();" />
                                                                                                                                                                </td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                    <asp:HiddenField ID="hfTerlaporIDNegara" runat="server" />
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Kode Pos</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtTerlaporIDKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                                                                            </tr>
																																		</tbody>
																																	</table>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	i. Alamat Sesuai Negara Asal</td>
																																<td style="width: 5px">
																																</td>
																																<td>
																																</td>
																															</tr>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<table>
																																		<tbody>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Nama Jalan</td>
																																				<td style="width: 5px">
																																					:
																																				</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtTerlaporNANamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																					&nbsp; &nbsp;&nbsp;
																																				</td>
																																				<td class="formText">
																																					Negara<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporNANegara" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporIDNANegara" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																										CausesValidation="False" OnClientClick="javascript:popWinNegara();"></asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporIDNANegara" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Provinsi</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext" valign="top">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporNAProvinsi" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporNAProvinsi" runat="server" 
																																										ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinProvinsi();">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporNAProvinsi" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Kota
																																				</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporNAKota" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporIDKota" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																										CausesValidation="False" OnClientClick="javascript:popWinKotaKab();"></asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporIDKota" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formText">
																																				</td>
																																				<td class="formText">
																																					Kode Pos</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtTerlaporNAKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
																																			</tr>
																																		</tbody>
																																	</table>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	j. Jenis Dokumen Identitas<span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<ajax:AjaxPanel ID="ajPnlDokId" runat="server">
																																		<asp:DropDownList runat="server" ID="cboTerlaporJenisDocID" CssClass="combobox">
																																		</asp:DropDownList>
																																	</ajax:AjaxPanel>
																																</td>
																															</tr>
																															<tr>
																																<td id="tdNomorId" class="formtext" colspan="3" runat="server">
																																	<table>
																																		<tbody>
																																			<tr>
																																				<td class="formText">
																																					Nomor Identitas <span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td style="width: 260px" class="formtext">
																																					<asp:TextBox ID="txtTerlaporNomorID" runat="server" CssClass="textBox" Width="257px"></asp:TextBox>
																																				</td>
																																			</tr>
																																		</tbody>
																																	</table>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	k. Nomor Pokok Wajib Pajak (NPWP)</td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:TextBox ID="txtTerlaporNPWP" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																															</tr>
																															<tr>
																																<td style="height: 15px" class="formText">
																																	l. Pekerjaan</td>
																																<td style="width: 5px; height: 15px">
																																</td>
																																<td style="height: 15px" class="formtext">
																																</td>
																															</tr>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<table>
																																		<tbody>
																																			<tr>
																																				<td class="formtext">
																																					&nbsp; &nbsp;&nbsp;
																																				</td>
																																				<td class="formText">
																																					Pekerjaan<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td style="width: 260px" class="formtext">
																																					<table>
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporPekerjaan" runat="server" CssClass="textBox" Width="257px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporPekerjaan" runat="server" ImageUrl="~/Images/button/browse.gif"
																																										CausesValidation="False" OnClientClick="javascript:popWinPekerjaan();"></asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporPekerjaan" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Jabatan</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td style="width: 260px" class="formtext">
																																					<asp:TextBox ID="txtTerlaporJabatan" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Penghasilan rata-rata/th (Rp)</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td style="width: 260px" class="formtext">
																																					<asp:TextBox ID="txtTerlaporPenghasilanRataRata" runat="server" CssClass="textBox"
																																						Width="257px"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Tempat kerja</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td style="width: 260px" class="formtext">
																																					<asp:TextBox ID="txtTerlaporTempatKerja" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																																			</tr>
																																		</tbody>
																																	</table>
																																</td>
																															</tr>
																														</tbody>
																													</table>
																												</div>
																												<div id="divKorporasi" runat="server" hidden="true">
																													<table>
																														<tbody>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<strong>3.1.3. Korporasi</strong></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	a. Bentuk Badan Usaha<span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:DropDownList ID="cboTerlaporCORPBentukBadanUsaha" runat="server" 
                                                                                                                                        CssClass="combobox">
																																	</asp:DropDownList></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	b. Nama Korporasi <span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:TextBox ID="txtTerlaporCORPNama" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	c. Bidang Usaha Korporasi<span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<table style="width: 127px">
																																		<tbody>
																																			<tr>
																																				<td>
																																					<asp:TextBox ID="txtTerlaporCORPBidangUsaha" runat="server" CssClass="textBox" Width="167px"
																																						Enabled="False"></asp:TextBox></td>
																																				<td>
																																					<asp:ImageButton ID="imgTerlaporCORPBidangUsaha" runat="server" 
																																						ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinBidangUsaha();">
																																					</asp:ImageButton></td>
																																			</tr>
																																		</tbody>
																																	</table>
																																	<asp:HiddenField ID="hfTerlaporCORPBidangUsaha" runat="server"></asp:HiddenField>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	d. Alamat Korporasi<span style="color: #cc0000">*</span></td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:RadioButtonList ID="rblTerlaporCORPTipeAlamat" runat="server" AutoPostBack="True"
																																		RepeatDirection="Horizontal">
																																		<asp:ListItem>Dalam Negeri</asp:ListItem>
																																		<asp:ListItem>Luar Negeri</asp:ListItem>
																																	</asp:RadioButtonList></td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	e. Alamat Lengkap Korporasi</td>
																																<td style="width: 5px">
																																</td>
																																<td class="formtext">
																																</td>
																															</tr>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<table>
																																		<tbody>
																																			<tr>
																																				<td class="formtext">
																																					&nbsp; &nbsp;&nbsp;
																																				</td>
																																				<td class="formText">
																																					Nama Jalan</td>
																																				<td style="width: 5px">
																																					:
																																				</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtTerlaporCORPDLNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					RT/RW</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtTerlaporCORPDLRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kelurahan</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext" valign="top">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporCORPDLKelurahan" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporCORPDLKelurahan" runat="server" 
																																										ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWin2();">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporCORPDLKelurahan" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kecamatan</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					&nbsp;<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporCORPDLKecamatan" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporCORPDLKecamatan" runat="server" 
																																										ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKecamatan();">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporCORPDLKecamatan" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kota / Kabupaten<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporCORPDLKotaKab" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporCORPDLKotaKab" runat="server" 
																																										ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKotaKab();">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporCORPDLKotaKab" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Kode Pos</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtTerlaporCORPDLKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Provinsi<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporCORPDLProvinsi" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporCORPDLProvinsi" runat="server" 
																																										ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinProvinsi();">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporCORPDLProvinsi" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td class="formText">
																																					Negara<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporCORPDLNegara" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporCORPDLNegara" runat="server" 
																																										ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinNegara();">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporCORPDLNegara" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																		</tbody>
																																	</table>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	i. Alamat Korporasi Luar Negeri</td>
																																<td style="width: 5px">
																																</td>
																																<td class="formtext">
																																</td>
																															</tr>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<table>
																																		<tbody>
																																			<tr>
																																				<td class="formtext">
																																					&nbsp; &nbsp;&nbsp;
																																				</td>
																																				<td style="width: 69px" class="formText">
																																					Nama Jalan</td>
																																				<td style="width: 5px">
																																					:
																																				</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtTerlaporCORPLNNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td style="width: 69px" class="formText">
																																					Negara<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporCORPLNNegara" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporCORPLNNegara" runat="server" 
																																										ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinNegara();">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporCORPLNNegara" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td style="width: 69px" class="formText">
																																					Provinsi</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext" valign="top">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td style="height: 22px">
																																									<asp:TextBox ID="txtTerlaporCORPLNProvinsi" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td style="height: 22px">
																																									<asp:ImageButton ID="imgTerlaporCORPLNProvinsi" runat="server" 
																																										ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinProvinsi();">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporCORPLNProvinsi" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td style="width: 69px" class="formText">
																																					Kota
																																				</td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<table style="width: 127px">
																																						<tbody>
																																							<tr>
																																								<td>
																																									<asp:TextBox ID="txtTerlaporCORPLNKota" runat="server" CssClass="textBox" Width="167px"
																																										Enabled="False"></asp:TextBox></td>
																																								<td>
																																									<asp:ImageButton ID="imgTerlaporCORPLNKota" runat="server" 
																																										ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKotaKab();">
																																									</asp:ImageButton></td>
																																							</tr>
																																						</tbody>
																																					</table>
																																					<asp:HiddenField ID="hfTerlaporCORPLNKota" runat="server"></asp:HiddenField>
																																				</td>
																																			</tr>
																																			<tr>
																																				<td class="formtext">
																																				</td>
																																				<td style="width: 69px" class="formText">
																																					Kode Pos<span style="color: #cc0000">*</span></td>
																																				<td style="width: 5px">
																																					:</td>
																																				<td class="formtext">
																																					<asp:TextBox ID="txtTerlaporCORPLNKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
																																			</tr>
																																		</tbody>
																																	</table>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	k. Nomor Pokok Wajib Pajak (NPWP)</td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:TextBox ID="txtTerlaporCORPNPWP" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																															</tr>
																														</tbody>
																													</table>
																												</div>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																					</tbody>
																				</table>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</ajax:AjaxPanel>
														</td>
													</tr>
													<tr>
														<td style="height: 6px" valign="middle" align="left" width="100%" background="Images/search-bar-background.gif">
															<table cellspacing="0" cellpadding="0" border="0">
																<tbody>
																	<tr>
																		<td class="formtext">
																			<asp:Label ID="Label3" runat="server" Text="C. TRANSAKSI" Font-Bold="True"></asp:Label>&nbsp;</td>
																		<td>
																			<a title="click to minimize or maximize" onclick="javascript:ShowHidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
																				href="#">
																				<img id="Img2" height="12" src="Images/search-bar-minimize.gif" width="12" border="0" /></a></td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
													<tr id="Transaksi">
														<td bgcolor="#ffffff" colspan="2">
															<ajax:AjaxPanel ID="AjaxPanel4" runat="server" Width="100%">
																<table>
																	<tbody>
																		<tr>
																			<td>
																				<asp:Menu ID="Menu1" runat="server" CssClass="tabs" StaticEnableDefaultPopOutImage="False"
																					Orientation="Horizontal">
																					<Items>
																						<asp:MenuItem Text="Kas Masuk" Value="0" Selected="True"></asp:MenuItem>
																						<asp:MenuItem Text="Kas Keluar" Value="1"></asp:MenuItem>
																					</Items>
																					<StaticSelectedStyle CssClass="selectedTab" />
																					<StaticMenuItemStyle CssClass="tab" />
																				</asp:Menu>
																				<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
																					<asp:View ID="ViewKasMasuk" runat="server">
																						<table width="100%">
																							<tbody>
																								<tr>
																									<td style="width: 300px" class="formtext">
																										4.1. Tanggal Transaksi (tgl/bln/thn)<span style="color: #cc0000">*</span></td>
																									<td style="width: 2px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKMTanggalTrx" runat="server" CssClass="textBox" Width="96px"
																											Enabled="False" MaxLength="50"></asp:TextBox><input style="border-right: #ffffff 0px solid;
																												border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
																												border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
																												height: 17px" id="popTRXKMTanggalTrx" title="Click to show calendar" onclick="popUpCalendar(this, frmBasicCTRWeb.txtTglLahir, 'dd-mmm-yyyy')"
																												type="button" runat="server" />
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										4.2. Nama Kantor PJK tempat terjadinya transaksi</td>
																									<td style="width: 2px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<div>
																											<table>
																												<tbody>
																													<tr>
																														<td class="formtext">
																															&nbsp; &nbsp;&nbsp;
																														</td>
																														<td class="formtext">
																															a. Nama Kantor<span style="color: #cc0000">*</span></td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:TextBox ID="txtTRXKMNamaKantor" runat="server" CssClass="textBox" Width="370px"></asp:TextBox>
																															&nbsp;</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																														</td>
																														<td class="formtext">
																															b. Kota Kabupaten<span style="color: #cc0000">*</span></td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<table style="width: 127px">
																																<tbody>
																																	<tr>
																																		<td>
																																			<asp:TextBox ID="txtTRXKMKotaKab" runat="server" CssClass="textBox" Width="167px"
																																				Enabled="False"></asp:TextBox></td>
																																		<td>
																																			<asp:ImageButton ID="imgTRXKMKotaKab" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																				CausesValidation="False" OnClientClick="javascript:popWinKotaKab();"></asp:ImageButton></td>
																																	</tr>
																																</tbody>
																															</table>
																															<asp:HiddenField ID="hfTRXKMKotaKab" runat="server"></asp:HiddenField>
																															&nbsp;
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																														</td>
																														<td class="formtext">
																															c. Provinsi<span style="color: #cc0000">*</span></td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<table style="width: 251px">
																																<tbody>
																																	<tr>
																																		<td style="width: 170px; height: 15px">
																																			<asp:TextBox ID="txtTRXKMProvinsi" runat="server" CssClass="textBox" Width="167px"
																																				Enabled="False"></asp:TextBox></td>
																																		<td style="height: 15px">
																																			<asp:ImageButton ID="imgTRXKMProvinsi" runat="server" ImageUrl="~/Images/button/browse.gif"
																																				CausesValidation="False" OnClientClick="javascript:popWinProvinsi();"></asp:ImageButton></td>
																																	</tr>
																																</tbody>
																															</table>
																															<asp:HiddenField ID="hfTRXKMProvinsi" runat="server"></asp:HiddenField>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																										</div>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										4.3. Detail Kas Masuk</td>
																									<td style="width: 2px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														a. Kas Masuk (Rp)</td>
																													<td style="width: 2px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMDetilKasMasuk" runat="server" CssClass="textBox" Width="167px"
																															AutoPostBack="True"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														b. Kas Masuk Dalam valuta asing (Dapat &gt; 1)</td>
																													<td style="width: 2px">
																													</td>
																													<td class="formtext">
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext" colspan="1">
																													</td>
																													<td class="formtext" colspan="3">
																														<table>
																															<tbody>
																																<tr>
																																	<td class="formtext">
																																		&nbsp; &nbsp;&nbsp;
																																	</td>
																																	<td class="formtext">
																																		i. Mata Uang</td>
																																	<td style="width: 2px">
																																		:</td>
																																	<td class="formtext">
																																		<table style="width: 127px">
																																			<tbody>
																																				<tr>
																																					<td>
																																						<asp:TextBox ID="txtTRXKMDetilMataUang" runat="server" CssClass="textBox" Width="167px"
																																							Enabled="False"></asp:TextBox></td>
																																					<td>
																																						<asp:ImageButton ID="imgTRXKMDetilMataUang" runat="server" 
																																							ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinMataUang();">
																																						</asp:ImageButton></td>
																																				</tr>
																																			</tbody>
																																		</table>
																																		<asp:HiddenField ID="hfTRXKMDetilMataUang" runat="server"></asp:HiddenField>
																																	</td>
																																</tr>
																																<tr>
																																	<td class="formtext">
																																	</td>
																																	<td class="formtext">
																																		ii. Kurs Transaksi</td>
																																	<td style="width: 2px">
																																		:</td>
																																	<td class="formtext">
																																		<asp:TextBox ID="txtTRXKMDetailKursTrx" runat="server" CssClass="textBox" Width="167px"></asp:TextBox></td>
																																</tr>
																																<tr>
																																	<td class="formtext">
																																	</td>
																																	<td class="formtext">
																																		iii. Jumlah&nbsp;</td>
																																	<td style="width: 2px">
																																		:</td>
																																	<td class="formtext">
																																		<asp:TextBox ID="txtTRXKMDetilJumlah" runat="server" CssClass="textBox" Width="167px"></asp:TextBox></td>
																																</tr>
																																<tr>
																																	<td class="formtext">
																																	</td>
																																	<td class="formtext" colspan="3">
																																		<asp:ImageButton ID="imgTRXKMDetilAdd" runat="server"  ImageUrl="~/Images/button/add.gif"
																																			CausesValidation="False"></asp:ImageButton></td>
																																</tr>
																																<tr>
																																	<td class="formtext">
																																	</td>
																																	<td class="formtext" colspan="3">
																																		<asp:GridView ID="grvTRXKMDetilValutaAsing" runat="server" SkinID="grv2" AutoGenerateColumns="False">
																																			<Columns>
																																				<asp:BoundField HeaderText="No" />
																																				<asp:BoundField HeaderText="Mata Uang" DataField="MataUang" />
																																				<asp:BoundField HeaderText="Kurs Transaksi" DataField="KursTransaksi" />
																																				<asp:BoundField HeaderText="Jumlah" DataField="Jumlah" />
																																				<asp:BoundField DataField="JumlahRp" HeaderText="Jumlah (Rp)" ItemStyle-HorizontalAlign="Right" />
																																				<asp:TemplateField>
																																					<ItemTemplate>
																																						<asp:LinkButton ID="BtngrvDetailValutaAsingDelete" runat="server" OnClick="BtngrvDetailValutaAsingDelete_Click"
																																							CausesValidation="False">Delete</asp:LinkButton>
																																					</ItemTemplate>
																																				</asp:TemplateField>
																																			</Columns>
																																		</asp:GridView>
																																	</td>
																																</tr>
																															</tbody>
																														</table>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														c. Total Kas Masuk (a+b) (Rp)</td>
																													<td style="width: 2px">
																														:</td>
																													<td class="formtext">
																														<asp:Label ID="lblTRXKMDetilValutaAsingJumlahRp" runat="server"></asp:Label></td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										4.4. Nomor Rekening Nasabah</td>
																									<td style="width: 2px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKMNoRekening" runat="server" CssClass="textBox" Width="167px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td style="height: 15px" class="formtext">
																										4.5. Identitas pihak terkait dengan Terlapor</td>
																									<td style="width: 2px; height: 15px">
																									</td>
																									<td style="height: 15px" class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										&nbsp; &nbsp; &nbsp; Tipe Pihak Terkait</td>
																									<td style="width: 2px">
																										:</td>
																									<td class="formtext">
																										<asp:RadioButtonList ID="rblTRXKMTipePelapor" runat="server" AutoPostBack="True"
																											RepeatDirection="Horizontal">
																											<asp:ListItem Value="1">Perorangan</asp:ListItem>
																											<asp:ListItem Value="2">Korporasi</asp:ListItem>
																										</asp:RadioButtonList></td>
																								</tr>
																							</tbody>
																						</table>
																						<table id="tblTRXKMTipePelapor" runat="server">
																							<tbody>
																								<tr>
																									<td class="formtext" colspan="3">
																										<strong>4.5.1. Perorangan</strong>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										a. Gelar</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKMINDVGelar" runat="server" CssClass="textBox"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										b. Nama Lengkap
																									</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKMINDVNamaLengkap" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										c. Tempat Lahir</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKMINDVTempatLahir" runat="server" CssClass="textBox" Width="240px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										d. Tanggal Lahir
																									</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<br />
																										<asp:TextBox ID="txtTRXKMINDVTanggalLahir" runat="server" CssClass="textBox" Width="96px"
																											Enabled="False" MaxLength="50"></asp:TextBox><input style="border-right: #ffffff 0px solid;
																												border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
																												border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
																												height: 17px" id="popTRXKMINDVTanggalLahir" title="Click to show calendar" onclick="popUpCalendar(this, frmBasicCTRWeb.txtTglLahir, 'dd-mmm-yyyy')"
																												type="button" runat="server" /></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										e. Kewarganegaraan<span style="color: #cc0000">*</span>
																									</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:RadioButtonList ID="rblTRXKMINDVKewarganegaraan" runat="server" AutoPostBack="True"
																											RepeatDirection="Horizontal">
																											<asp:ListItem Value="1">WNI</asp:ListItem>
																											<asp:ListItem Value="2">WNA</asp:ListItem>
																										</asp:RadioButtonList></td>
																								</tr>
																								<tr style="color: #cc0000">
																									<td class="formtext">
																										f. Negara
																									</td>
																									<td style="width: 5px; color: #000000">
																										:</td>
																									<td class="formtext">
																										<asp:DropDownList ID="cboTRXKMINDVNegara" runat="server" CssClass="combobox">
																										</asp:DropDownList></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										g. Alamat Domisili</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														Nama Jalan</td>
																													<td style="width: 5px">
																														:
																													</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMINDVDOMNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														RT/RW</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMINDVDOMRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kelurahan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext" valign="top">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMINDVDOMKelurahan" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMINDVDOMKelurahan" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWin2();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMINDVDOMKelurahan" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kecamatan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														&nbsp;<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMINDVDOMKecamatan" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMINDVDOMKecamatan" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKecamatan();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMINDVDOMKecamatan" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kota / Kabupaten</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMINDVDOMKotaKab" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMINDVDOMKotaKab" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKotaKab();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMINDVDOMKotaKab" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Provinsi</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMINDVDOMProvinsi" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMINDVDOMProvinsi" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinProvinsi();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMINDVDOMProvinsi" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																											    
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Negara</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <asp:TextBox ID="txtTRXKMINDVDOMNegara" runat="server" CssClass="textBox" 
                                                                                                                                            Enabled="False" Width="167px"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:ImageButton ID="imgTRXKMINDVDOMNegara" runat="server" 
                                                                                                                                            CausesValidation="False" ImageUrl="~/Images/button/browse.gif" 
                                                                                                                                            OnClientClick="javascript:popWinNegara();" />
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTRXKMINDVDOMNegara" runat="server" />
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formtext">
                                                                                                                        Kode Pos</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTRXKMINDVDOMKodePos" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										h. Alamat Sesuai Bukti Identitas</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																										<asp:CheckBox ID="chkTRXKMINDVCopyDOM" runat="server" Text="Sama dengan Alamat Domisili"
																											AutoPostBack="True"></asp:CheckBox></td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														Nama Jalan</td>
																													<td style="width: 5px">
																														:
																													</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMINDVIDNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														RT/RW</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMINDVIDRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kelurahan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext" valign="top">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMINDVIDKelurahan" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMINDVIDKelurahan" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWin2();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMINDVIDKelurahan" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kecamatan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														&nbsp;<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td style="height: 23px">
																																		<asp:TextBox ID="txtTRXKMINDVIDKecamatan" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td style="height: 23px">
																																		<asp:ImageButton ID="imgTRXKMINDVIDKecamatan" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKecamatan();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMINDVIDKecamatan" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kota / Kabupaten</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMINDVIDKotaKab" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMINDVIDKotaKab" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKotaKab();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMINDVIDKotaKab" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Provinsi</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMINDVIDProvinsi" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMINDVIDProvinsi" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinProvinsi();">
																																		</asp:ImageButton>&nbsp;
																																	</td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMINDVIDProvinsi" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																											  
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Negara</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <asp:TextBox ID="txtTRXKMINDVIDNegara" runat="server" CssClass="textBox" 
                                                                                                                                            Enabled="False" Width="167px"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:ImageButton ID="imgTRXKMINDVIDNegara" runat="server" 
                                                                                                                                            CausesValidation="False" ImageUrl="~/Images/button/browse.gif" 
                                                                                                                                            OnClientClick="javascript:popWinNegara();" />
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTRXKMINDVIDNegara" runat="server" />
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        &nbsp;</td>
                                                                                                                </tr>
                                                                                                                  <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formtext">
                                                                                                                        Kode Pos</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTRXKMINDVIDKodePos" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										i. Alamat Sesuai Negara Asal</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														Nama Jalan</td>
																													<td style="width: 5px">
																														:
																													</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMINDVNANamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Negara</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td style="height: 22px">
																																		<asp:TextBox ID="txtTRXKMINDVNANegara" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td style="height: 22px">
																																		<asp:ImageButton ID="imgTRXKMINDVNANegara" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																			CausesValidation="False" OnClientClick="javascript:popWinNegara();"></asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMINDVNANegara" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Provinsi</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext" valign="top">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMINDVNAProvinsi" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMINDVNAProvinsi" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinProvinsi();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMINDVNAProvinsi" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kota
																													</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMINDVNAKota" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMINDVNAKota" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																			CausesValidation="False" OnClientClick="javascript:popWinKotaKab();"></asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMINDVNAKota" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kode Pos</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMINDVNAKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										j. Jenis Dokumen Identitas</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<ajax:AjaxPanel ID="AjaxPanel2" runat="server">
																											<asp:DropDownList runat="server" ID="cboTRXKMINDVJenisID" CssClass="combobox">
																											</asp:DropDownList>
																										</ajax:AjaxPanel>
																									</td>
																								</tr>
																								<tr>
																									<td id="Td1" class="formtext" colspan="3" runat="server">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																													  Nomor Identitas </td>
																													<td style="width: 5px">
																														:</td>
																													<td style="width: 260px" class="formtext">
																														<asp:TextBox ID="txtTRXKMINDVNomorID" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										k. Nomor Pokok Wajib Pajak (NPWP)</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKMINDVNPWP" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										l. Pekerjaan</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td style="height: 40px" class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td style="height: 40px" class="formtext">
																														Pekerjaan</td>
																													<td style="width: 5px; height: 40px">
																														:</td>
																													<td style="width: 260px; height: 40px" class="formtext">
																														<table border="0">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMINDVPekerjaan" runat="server" CssClass="textBox" Width="257px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMINDVPekerjaan" runat="server" ImageUrl="~/Images/button/browse.gif"
																																			CausesValidation="False" OnClientClick="javascript:popWinPekerjaan();"></asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMINDVPekerjaan" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Jabatan</td>
																													<td style="width: 5px">
																														:</td>
																													<td style="width: 260px" class="formtext">
																														<asp:TextBox ID="txtTRXKMINDVJabatan" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Penghasilan rata-rata/th (Rp)</td>
																													<td style="width: 5px">
																														:</td>
																													<td style="width: 260px" class="formtext">
																														<asp:TextBox ID="txtTRXKMINDVPenghasilanRataRata" runat="server" CssClass="textBox"
																															Width="257px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Tempat kerja</td>
																													<td style="width: 5px">
																														:</td>
																													<td style="width: 260px" class="formtext">
																														<asp:TextBox ID="txtTRXKMINDVTempatKerja" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										m. Tujuan Transaksi</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKMINDVTujuanTrx" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										n. Sumber Dana</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKMINDVSumberDana" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														ii. Nama Bank Lain</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMINDVNamaBankLain" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														ii. Nomor Rekening Tujuan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMINDVNoRekeningTujuan" runat="server" CssClass="textBox"
																															Width="257px"></asp:TextBox></td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																						<table id="tblTRXKMTipePelaporKorporasi" runat="server">
																							<tbody>
																								<tr>
																									<td class="formtext" colspan="3">
																										<strong>4.5.2. Korporasi</strong></td>
																								</tr>
																								<tr>
																									<td style="height: 27px" class="formtext">
																										a. Bentuk Badan Usaha<span style="color: #cc0000">*</span></td>
																									<td style="width: 5px; height: 27px">
																										:</td>
																									<td style="height: 27px" class="formtext">
																										&nbsp; &nbsp;
																										<asp:DropDownList ID="cboTRXKMCORPBentukBadanUsaha" runat="server">
																										</asp:DropDownList></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										b. Nama Korporasi
																									</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKMCORPNama" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										c. Bidang Usaha Korporasi<span style="color: #cc0000">*</span></td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<table style="width: 127px">
																											<tbody>
																												<tr>
																													<td>
																														<asp:TextBox ID="txtTRXKMCORPBidangUsaha" runat="server" CssClass="textBox" Width="167px"
																															Enabled="False"></asp:TextBox></td>
																													<td>
																														<asp:ImageButton ID="imgTRXKMCORPBidangUsaha" runat="server" 
																															ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinBidangUsaha();">
																														</asp:ImageButton></td>
																												</tr>
																											</tbody>
																										</table>
																										<asp:HiddenField ID="hfTRXKMCORPBidangUsaha" runat="server"></asp:HiddenField>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										d. Alamat Korporasi</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:RadioButtonList ID="rblTRXKMCORPTipeAlamat" runat="server" AutoPostBack="True"
																											RepeatDirection="Horizontal">
																											<asp:ListItem Selected="True">Dalam Negeri</asp:ListItem>
																											<asp:ListItem>Luar Negeri</asp:ListItem>
																										</asp:RadioButtonList></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										e. Alamat Lengkap Korporasi</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														Nama Jalan</td>
																													<td style="width: 5px">
																														:
																													</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMCORPDLNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														RT/RW</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMCORPDLRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kelurahan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext" valign="top">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td style="height: 22px">
																																		<asp:TextBox ID="txtTRXKMCORPDLKelurahan" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td style="height: 22px">
																																		<asp:ImageButton ID="imgTRXKMCORPDLKelurahan" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWin2();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMCORPDLKelurahan" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kecamatan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														&nbsp;<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMCORPDLKecamatan" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMCORPDLKecamatan" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKecamatan();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMCORPDLKecamatan" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kota / Kabupaten</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMCORPDLKotaKab" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMCORPDLKotaKab" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKotaKab();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMCORPDLKotaKab" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kode Pos</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMCORPDLKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Provinsi</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td style="height: 22px">
																																		<asp:TextBox ID="txtTRXKMCORPDLProvinsi" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td style="height: 22px">
																																		<asp:ImageButton ID="imgTRXKMCORPDLProvinsi" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinProvinsi();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMCORPDLProvinsi" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Negara</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMCORPDLNegara" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMCORPDLNegara" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																			CausesValidation="False" OnClientClick="javascript:popWinNegara();"></asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMCORPDLNegara" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										i. Alamat Korporasi Luar Negeri</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														Nama Jalan</td>
																													<td style="width: 5px">
																														:
																													</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMCORPLNNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Negara</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMCORPLNNegara" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMCORPLNNegara" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																			CausesValidation="False" OnClientClick="javascript:popWinNegara();"></asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMCORPLNNegara" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Provinsi</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext" valign="top">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMCORPLNProvinsi" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMCORPLNProvinsi" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinProvinsi();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMCORPLNProvinsi" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kota
																													</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKMCORPLNKota" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKMCORPLNKota" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																			CausesValidation="False" OnClientClick="javascript:popWinKotaKab();"></asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKMCORPLNKota" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kode Pos</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMCORPLNKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										k. Nomor Pokok Wajib Pajak (NPWP)</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKMCORPNPWP" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										l. Tujuan Transaksi</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKMCORPTujuanTrx" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										m. Sumber Dana</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKMCORPSumberDana" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														ii. Nama Bank Lain</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMCORPNamaBankLain" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														ii. Nomor Rekening Tujuan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKMCORPNoRekeningTujuan" runat="server" CssClass="textBox"
																															Width="257px"></asp:TextBox></td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</asp:View>
																					<asp:View ID="ViewKasKeluar" runat="server">
																						<table width="100%">
																							<tbody>
																								<tr>
																									<td style="width: 300px" class="formtext">
																										5.1. Tanggal Transaksi (tgl/bln/thn)<span style="color: #cc0000">*</span></td>
																									<td style="width: 2px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKKTanggalTransaksi" runat="server" CssClass="textBox" Width="96px"
																											Enabled="False" MaxLength="50"></asp:TextBox><input style="border-right: #ffffff 0px solid;
																												border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
																												border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
																												height: 17px" id="popTRXKKTanggalTransaksi" title="Click to show calendar" onclick="popUpCalendar(this, frmBasicCTRWeb.txtTglLahir, 'dd-mmm-yyyy')"
																												type="button" runat="server" />
																									</td>
																								</tr>
																								<tr>
																									<td style="height: 27px" class="formtext">
																										5.2. Nama Kantor PJK tempat terjadinya transaksi</td>
																									<td style="width: 2px; height: 27px">
																									</td>
																									<td style="height: 27px" class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<div>
																											<table>
																												<tbody>
																													<tr>
																														<td class="formtext">
																															&nbsp; &nbsp;&nbsp;
																														</td>
																														<td class="formtext">
																															a. Nama Kantor<span style="color: #cc0000">*</span></td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:TextBox ID="txtTRXKKNamaKantor" runat="server" CssClass="textBox" Width="370px"></asp:TextBox>
																															&nbsp;</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																														</td>
																														<td class="formtext">
																															b. Kota Kabupaten<span style="color: #cc0000">*</span></td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<table style="width: 127px">
																																<tbody>
																																	<tr>
																																		<td>
																																			<asp:TextBox ID="txtTRXKKKotaKab" runat="server" CssClass="textBox" Width="167px"
																																				Enabled="False"></asp:TextBox></td>
																																		<td>
																																			<asp:ImageButton ID="imgTRXKKKotaKab" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																				CausesValidation="False" OnClientClick="javascript:popWinKotaKab();"></asp:ImageButton></td>
																																	</tr>
																																</tbody>
																															</table>
																															<asp:HiddenField ID="hfTRXKKKotaKab" runat="server"></asp:HiddenField>
																															&nbsp;
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																														</td>
																														<td class="formtext">
																															c. Provinsi<span style="color: #cc0000">*</span></td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<table style="width: 127px">
																																<tbody>
																																	<tr>
																																		<td>
																																			<asp:TextBox ID="txtTRXKKProvinsi" runat="server" CssClass="textBox" Width="167px"
																																				Enabled="False"></asp:TextBox></td>
																																		<td>
																																			<asp:ImageButton ID="imgTRXKKProvinsi" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																				CausesValidation="False" OnClientClick="javascript:popWinProvinsi();"></asp:ImageButton></td>
																																	</tr>
																																</tbody>
																															</table>
																															<asp:HiddenField ID="hfTRXKKProvinsi" runat="server"></asp:HiddenField>
																															&nbsp;
																														</td>
																													</tr>
																												</tbody>
																											</table>
																										</div>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										5.3. Detail Kas Keluar</td>
																									<td style="width: 2px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														a. Kas Keluar (Rp)</td>
																													<td style="width: 2px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKDetailKasKeluar" runat="server" CssClass="textBox" Width="167px"
																															AutoPostBack="True"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																														b. Kas Keluar Dalam valuta asing (Dapat &gt; 1)</td>
																													<td style="width: 2px">
																													</td>
																													<td class="formtext">
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext" colspan="3">
																														<table>
																															<tbody>
																																<tr>
																																	<td class="formtext">
																																		&nbsp; &nbsp;&nbsp;
																																	</td>
																																	<td class="formtext">
																																		i. Mata Uang</td>
																																	<td style="width: 2px">
																																		:</td>
																																	<td class="formtext">
																																		<table style="width: 127px">
																																			<tbody>
																																				<tr>
																																					<td>
																																						<asp:TextBox ID="txtTRXKKDetilMataUang" runat="server" CssClass="textBox" Width="167px"
																																							Enabled="False"></asp:TextBox></td>
																																					<td>
																																						<asp:ImageButton ID="imgTRXKKDetilMataUang" runat="server" 
																																							ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinMataUang();">
																																						</asp:ImageButton></td>
																																				</tr>
																																			</tbody>
																																		</table>
																																		<asp:HiddenField ID="hfTRXKKDetilMataUang" runat="server"></asp:HiddenField>
																																	</td>
																																</tr>
																																<tr>
																																	<td class="formtext">
																																	</td>
																																	<td class="formtext">
																																		ii. Kurs Transaksi</td>
																																	<td style="width: 2px">
																																		:</td>
																																	<td class="formtext">
																																		<asp:TextBox ID="txtTRXKKDetilKursTrx" runat="server" CssClass="textBox" Width="167px"></asp:TextBox></td>
																																</tr>
																																<tr>
																																	<td class="formtext">
																																	</td>
																																	<td class="formtext">
																																		iii. Jumlah&nbsp;</td>
																																	<td style="width: 2px">
																																		:</td>
																																	<td class="formtext">
																																		<asp:TextBox ID="txtTRXKKDetilJumlah" runat="server" CssClass="textBox" Width="167px"></asp:TextBox></td>
																																</tr>
																																<tr>
																																	<td class="formtext">
																																	</td>
																																	<td class="formtext" colspan="3">
																																		<asp:ImageButton ID="imgTRXKKDetilAdd" runat="server"  ImageUrl="~/Images/button/add.gif"
																																			CausesValidation="False"></asp:ImageButton></td>
																																</tr>
																																<tr>
																																	<td class="formtext">
																																	</td>
																																	<td class="formtext" colspan="3">
																																		<asp:GridView ID="grvDetilKasKeluar" runat="server" Width="290px" SkinID="grv2"
																																			AutoGenerateColumns="False">
																																			<Columns>
																																				<asp:BoundField HeaderText="No" />
																																				<asp:BoundField HeaderText="Mata Uang" DataField="MataUang" />
																																				<asp:BoundField HeaderText="Kurs Transaksi" DataField="KursTransaksi" />
																																				<asp:BoundField HeaderText="Jumlah" DataField="Jumlah" />
																																				<asp:BoundField DataField="JumlahRp" HeaderText="Jumlah (Rp)" ItemStyle-HorizontalAlign="Right" />
																																				<asp:TemplateField>
																																					<ItemTemplate>
																																						<asp:LinkButton ID="BtnDeletegrvDetilKasKeluar" runat="server" OnClick="BtnDeletegrvDetilKasKeluar_Click"
																																							CausesValidation="False">Delete</asp:LinkButton>
																																					</ItemTemplate>
																																				</asp:TemplateField>
																																			</Columns>
																																		</asp:GridView>
																																	</td>
																																</tr>
																															</tbody>
																														</table>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																														c. Total Kas Keluar (a+b) (Rp)</td>
																													<td style="width: 2px">
																														:</td>
																													<td class="formtext">
																														<asp:Label ID="lblDetilKasKeluarJumlahRp" runat="server"></asp:Label></td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
                                                                                                <tr>
                                                                                                    <td class="formtext">
                                                                                                        5.4. Nomor Rekening Nasabah</td>
                                                                                                    <td style="width: 2px">
                                                                                                        :</td>
                                                                                                    <td class="formtext">
																										<asp:TextBox ID="txtTRXKKRekeningKK" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                                </tr>
																								<tr>
																									<td class="formtext">
                                                                                                        5.5. Identitas pihak terkait dengan laporan</td>
																									<td style="width: 2px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td style="height: 41px" class="formtext">
																										&nbsp; &nbsp; &nbsp; Tipe Pelapor</td>
																									<td style="width: 2px; height: 41px">
																										:</td>
																									<td style="height: 41px" class="formtext">
																										<asp:RadioButtonList ID="rblTRXKKTipePelapor" runat="server" AutoPostBack="True"
																											RepeatDirection="Horizontal">
																											<asp:ListItem Value="1">Perorangan</asp:ListItem>
																											<asp:ListItem Value="2">Korporasi</asp:ListItem>
																										</asp:RadioButtonList></td>
																								</tr>
																							</tbody>
																						</table>
																						<table id="tblTRXKKTipePelaporPerorangan" runat="server">
																							<tbody>
																								<tr>
																									<td class="formtext" colspan="3">
																										<strong>5.5.1. Perorangan</strong>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										a. Gelar</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKKINDVGelar" runat="server" CssClass="textBox"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										b. Nama Lengkap
																									</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKKINDVNamaLengkap" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										c. Tempat Lahir</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKKINDVTempatLahir" runat="server" CssClass="textBox" Width="240px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										d. Tanggal Lahir
																									</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<br />
																										<asp:TextBox ID="txtTRXKKINDVTglLahir" runat="server" CssClass="textBox" Width="96px"
																											Enabled="False" MaxLength="50"></asp:TextBox><input style="border-right: #ffffff 0px solid;
																												border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
																												border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
																												height: 17px" id="popTRXKKINDVTglLahir" title="Click to show calendar" onclick="popUpCalendar(this, frmBasicCTRWeb.txtTglLahir, 'dd-mmm-yyyy')"
																												type="button" runat="server" /></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										e. Kewarganegaraan<span style="color: #cc0000">*</span>
																									</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:RadioButtonList ID="rblTRXKKINDVKewarganegaraan" runat="server" AutoPostBack="True"
																											RepeatDirection="Horizontal">
																											<asp:ListItem Value="1">WNI</asp:ListItem>
																											<asp:ListItem Value="2">WNA</asp:ListItem>
																										</asp:RadioButtonList></td>
																								</tr>
																								<tr style="color: #cc0000">
																									<td class="formtext">
																										f. Negara
																									</td>
																									<td style="width: 5px; color: #000000">
																										:</td>
																									<td class="formtext">
																										<asp:DropDownList ID="cboTRXKKINDVNegara" runat="server" CssClass="combobox">
																										</asp:DropDownList></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										g. Alamat Domisili</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														Nama Jalan</td>
																													<td style="width: 5px">
																														:
																													</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKINDVDOMNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														RT/RW</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKINDVDOMRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kelurahan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext" valign="top">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKINDVDOMKelurahan" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKINDVDOMKelurahan" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWin2();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKINDVDOMKelurahan" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kecamatan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														&nbsp;<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKINDVDOMKecamatan" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKINDVDOMKecamatan" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKecamatan();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKINDVDOMKecamatan" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kota / Kabupaten</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKINDVDOMKotaKab" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKINDVDOMKotaKab" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKotaKab();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKINDVDOMKotaKab" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Provinsi</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKINDVDOMProvinsi" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKINDVDOMProvinsi" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinProvinsi();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKINDVDOMProvinsi" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																											    <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formtext">
                                                                                                                        Kode Pos</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTRXKKINDVDOMKodePos" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formtext">
                                                                                                                        Negara</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <asp:TextBox ID="txtTRXKKINDVDOMNegara" runat="server" CssClass="textBox" 
                                                                                                                                            Enabled="False" Width="167px"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:ImageButton ID="imgTRXKKINDVDOMNegara" runat="server" 
                                                                                                                                            CausesValidation="False" ImageUrl="~/Images/button/browse.gif" 
                                                                                                                                            OnClientClick="javascript:popWinNegara();" />
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTRXKKINDVDOMNegara" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										h. Alamat Sesuai Bukti Identitas</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																										<asp:CheckBox ID="chkTRXKKINDVIDCopyDOM" runat="server" Text="Sama dengan Alamat Domisili"
																											AutoPostBack="True"></asp:CheckBox></td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														Nama Jalan</td>
																													<td style="width: 5px">
																														:
																													</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKINDVIDNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														RT/RW</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKINDVIDRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kelurahan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext" valign="top">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKINDVIDKelurahan" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKINDVIDKelurahan" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWin2();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKINDVIDKelurahan" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kecamatan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														&nbsp;<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKINDVIDKecamatan" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKINDVIDKecamatan" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKecamatan();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKINDVIDKecamatan" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kota / Kabupaten</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKINDVIDKotaKab" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKINDVIDKotaKab" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKotaKab();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKINDVIDKotaKab" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Provinsi</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKINDVIDProvinsi" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKINDVIDProvinsi" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinProvinsi();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKINDVIDProvinsi" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																											
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formtext">
                                                                                                                        Negara</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tbody>
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <asp:TextBox ID="txtTRXKKINDVIDNegara" runat="server" CssClass="textBox" 
                                                                                                                                            Enabled="False" Width="167px"></asp:TextBox>
                                                                                                                                    </td>
                                                                                                                                    <td>
                                                                                                                                        <asp:ImageButton ID="imgTRXKKINDVIDNegara" runat="server" 
                                                                                                                                            CausesValidation="False" ImageUrl="~/Images/button/browse.gif" 
                                                                                                                                            OnClientClick="javascript:popWinNegara();" />
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTRXKKINDVIDNegara" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                    <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formtext">
                                                                                                                        Kode Pos</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTRXKKINDVIDKodePos" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										i. Alamat Sesuai Negara Asal</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														Nama Jalan</td>
																													<td style="width: 5px">
																														:
																													</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKINDVNANamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Negara</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKINDVNANegara" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKINDVNANegara" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																			CausesValidation="False" OnClientClick="javascript:popWinNegara();"></asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKINDVNANegara" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Provinsi</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext" valign="top">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKINDVNAProvinsi" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKINDVNAProvinsi" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinProvinsi();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKINDVNAProvinsi" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kota
																													</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKINDVNAKota" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKINDVNAKota" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																			CausesValidation="False" OnClientClick="javascript:popWinKotaKab();"></asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKINDVNAKota" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kode Pos</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKINDVNAKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										j. Jenis Dokumen Identitas</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<ajax:AjaxPanel ID="AjaxPanel3" runat="server">
																											<asp:DropDownList runat="server" ID="cboTRXKKINDVJenisID" CssClass="combobox">
																											</asp:DropDownList>
																										</ajax:AjaxPanel>
																									</td>
																								</tr>
																								<tr>
																									<td id="Td2" class="formtext" colspan="3" runat="server">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																													  Nomor Identitas </td>
																													<td style="width: 5px">
																														:</td>
																													<td style="width: 260px" class="formtext">
																														<asp:TextBox ID="txtTRXKKINDVNomorId" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										k. Nomor Pokok Wajib Pajak (NPWP)</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKKINDVNPWP" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										l. Pekerjaan</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														Pekerjaan</td>
																													<td style="width: 5px">
																														:</td>
																													<td style="width: 260px" class="formtext">
																														<table border="0">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKINDVPekerjaan" runat="server" CssClass="textBox" Width="257px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKINDVPekerjaan" runat="server" ImageUrl="~/Images/button/browse.gif"
																																			CausesValidation="False" OnClientClick="javascript:popWinPekerjaan();"></asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKINDVPekerjaan" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Jabatan</td>
																													<td style="width: 5px">
																														:</td>
																													<td style="width: 260px" class="formtext">
																														<asp:TextBox ID="txtTRXKKINDVJabatan" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Penghasilan rata-rata/th (Rp)</td>
																													<td style="width: 5px">
																														:</td>
																													<td style="width: 260px" class="formtext">
																														<asp:TextBox ID="txtTRXKKINDVPenghasilanRataRata" runat="server" CssClass="textBox"
																															Width="257px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Tempat kerja</td>
																													<td style="width: 5px">
																														:</td>
																													<td style="width: 260px" class="formtext">
																														<asp:TextBox ID="txtTRXKKINDVTempatKerja" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										m. Tujuan Transaksi</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKKINDVTujuanTrx" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										n. Sumber Dana</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKKINDVSumberDana" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														ii. Nama Bank Lain</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKINDVNamaBankLain" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														ii. Nomor Rekening Tujuan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKINDVNoRekTujuan" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																						<table id="tblTRXKKTipePelaporKorporasi" runat="server">
																							<tbody>
																								<tr>
																									<td class="formtext" colspan="3">
																										<strong>5.5.2. Korporasi</strong></td>
																								</tr>
																								<tr>
																									<td style="height: 28px" class="formtext">
																										a. Bentuk Badan Usaha<span style="color: #cc0000">*</span></td>
																									<td style="width: 5px; height: 28px">
																										:</td>
																									<td style="height: 28px" class="formtext">
																										&nbsp;&nbsp;
																										<asp:DropDownList ID="cboTRXKKCORPBentukBadanUsaha" runat="server">
																										</asp:DropDownList></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										b. Nama Korporasi
																									</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKKCORPNama" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										c. Bidang Usaha Korporasi<span style="color: #cc0000">*</span></td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<table style="width: 127px">
																											<tbody>
																												<tr>
																													<td>
																														<asp:TextBox ID="txtTRXKKCORPBidangUsaha" runat="server" CssClass="textBox" Width="167px"
																															Enabled="False"></asp:TextBox></td>
																													<td>
																														<asp:ImageButton ID="imgTRXKKCORPBidangUsaha" runat="server" 
																															ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinBidangUsaha();">
																														</asp:ImageButton></td>
																												</tr>
																											</tbody>
																										</table>
																										<asp:HiddenField ID="hfTRXKKCORPBidangUsaha" runat="server"></asp:HiddenField>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										d. Alamat Korporasi</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:RadioButtonList ID="rblTRXKKCORPTipeAlamat" runat="server" AutoPostBack="True"
																											RepeatDirection="Horizontal">
																											<asp:ListItem>Dalam Negeri</asp:ListItem>
																											<asp:ListItem>Luar Negeri</asp:ListItem>
																										</asp:RadioButtonList></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										e. Alamat Lengkap Korporasi</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														Nama Jalan</td>
																													<td style="width: 5px">
																														:
																													</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKCORPDLNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														RT/RW</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKCORPDLRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kelurahan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext" valign="top">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKCORPDLKelurahan" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKCORPDLKelurahan" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWin2();" style="height: 17px">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKCORPDLKelurahan" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kecamatan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														&nbsp;<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKCORPDLKecamatan" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKCORPDLKecamatan" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKecamatan();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKCORPDLKecamatan" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kota / Kabupaten</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKCORPDLKotaKab" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKCORPDLKotaKab" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinKotaKab();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKCORPDLKotaKab" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kode Pos</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKCORPDLKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Provinsi</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKCORPDLProvinsi" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKCORPDLProvinsi" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinProvinsi();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKCORPDLProvinsi" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Negara</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKCORPDLNegara" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKCORPDLNegara" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																			CausesValidation="False"></asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKCORPDLNegara" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										i. Alamat Korporasi Luar Negeri</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														Nama Jalan</td>
																													<td style="width: 5px">
																														:
																													</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKCORPLNNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Negara</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKCORPLNNegara" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKCORPLNNegara" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																			CausesValidation="False" OnClientClick="javascript:popWinNegara();"></asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKCORPLNNegara" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Provinsi</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext" valign="top">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKCORPLNProvinsi" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKCORPLNProvinsi" runat="server" 
																																			ImageUrl="~/Images/button/browse.gif" CausesValidation="False" OnClientClick="javascript:popWinProvinsi();">
																																		</asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKCORPLNProvinsi" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kota
																													</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<table style="width: 127px">
																															<tbody>
																																<tr>
																																	<td>
																																		<asp:TextBox ID="txtTRXKKCORPLNKota" runat="server" CssClass="textBox" Width="167px"
																																			Enabled="False"></asp:TextBox></td>
																																	<td>
																																		<asp:ImageButton ID="imgTRXKKCORPLNKota" runat="server"  ImageUrl="~/Images/button/browse.gif"
																																			CausesValidation="False" OnClientClick="javascript:popWinKotaKab();"></asp:ImageButton></td>
																																</tr>
																															</tbody>
																														</table>
																														<asp:HiddenField ID="hfTRXKKCORPLNKota" runat="server"></asp:HiddenField>
																													</td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														Kode Pos</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKCORPLNKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										k. Nomor Pokok Wajib Pajak (NPWP)</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKKCORPNPWP" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										l. Tujuan Transaksi</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKKCORPTujuanTrx" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										m. Sumber Dana</td>
																									<td style="width: 5px">
																										:</td>
																									<td class="formtext">
																										<asp:TextBox ID="txtTRXKKCORPSumberDana" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																								</tr>
																								<tr>
																									<td class="formtext">
																										o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																									<td style="width: 5px">
																									</td>
																									<td class="formtext">
																									</td>
																								</tr>
																								<tr>
																									<td class="formtext" colspan="3">
																										<table>
																											<tbody>
																												<tr>
																													<td class="formtext">
																														&nbsp; &nbsp;&nbsp;
																													</td>
																													<td class="formtext">
																														ii. Nama Bank Lain</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKCORPNamaBankLain" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
																												</tr>
																												<tr>
																													<td class="formtext">
																													</td>
																													<td class="formtext">
																														ii. Nomor Rekening Tujuan</td>
																													<td style="width: 5px">
																														:</td>
																													<td class="formtext">
																														<asp:TextBox ID="txtTRXKKCORPNoRekeningTujuan" runat="server" CssClass="textBox"
																															Width="257px"></asp:TextBox></td>
																												</tr>
																											</tbody>
																										</table>
																									</td>
																								</tr>
																							</tbody>
																						</table>
																					</asp:View>
																				</asp:MultiView>
																				<table>
																					<tbody>
																						<tr>
																							<td bgcolor="#ffffff">
																								<asp:ImageButton ID="imgAdd" runat="server"  ImageUrl="~/Images/button/add.gif"
																									CausesValidation="false"></asp:ImageButton>
																								<asp:ImageButton ID="imgSaveEdit" runat="server" Visible="False" ImageUrl="~/Images/button/save.gif"
																									CausesValidation="false"></asp:ImageButton>&nbsp;
																							</td>
																							<td style="width: 76px" bgcolor="#ffffff">
																								<asp:ImageButton ID="imgClearAll" runat="server"  ImageUrl="~/Images/button/clear.gif"
																									CausesValidation="false"></asp:ImageButton>
																								<asp:ImageButton ID="imgCancelEdit" runat="server" Visible="False" 
																									ImageUrl="~/Images/button/cancel.gif" CausesValidation="false"></asp:ImageButton>
																							</td>
																						</tr>
																					</tbody>
																				</table>
																				<table>
																					<tbody>
																						<tr>
																							<td colspan="3">
																								<asp:GridView ID="grvTransaksi" runat="server" SkinID="grv2" 
																									AutoGenerateColumns="False" EnableModelValidation="True">
																									<Columns>
																										<asp:BoundField HeaderText="No" />
																										<asp:BoundField HeaderText="Kas" DataField="Kas" />
																										<asp:BoundField HeaderText="Transaction Date" DataField="TransactionDate" 
																											DataFormatString="{0:dd-MMM-yyyy}" />
																										<asp:BoundField HeaderText="Branch" DataField="Branch" />
																										<asp:BoundField HeaderText="Account No" DataField="AccountNumber" />
																										<asp:BoundField HeaderText="Customer / WIC" DataField="Type" Visible="False" />
																										<asp:BoundField HeaderText="Transaction Nominal" DataField="TransactionNominal" 
																											ItemStyle-HorizontalAlign="Right">
																										<ItemStyle HorizontalAlign="Right" />
																										</asp:BoundField>
																										<asp:TemplateField>
																											<ItemTemplate>
																												<asp:LinkButton ID="EditResume" runat="server" CausesValidation="False" OnClick="EditResume_Click">Edit</asp:LinkButton>
																											</ItemTemplate>
																										</asp:TemplateField>
																										<asp:TemplateField>
																											<ItemTemplate>
																												<asp:LinkButton ID="DeleteResume" runat="server" CausesValidation="False" OnClick="DeleteResume_Click">Delete</asp:LinkButton>
																											</ItemTemplate>
																										</asp:TemplateField>
																									</Columns>
																								</asp:GridView>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								Total Seluruh Kas Masuk</td>
																							<td style="width: 5px">
																								:</td>
																							<td class="formtext">
																								<asp:Label ID="lblTotalKasMasuk" runat="server"></asp:Label>
																							</td>
																						</tr>
																						<tr>
																							<td style="height: 15px" class="formtext">
																								Total Seluruh Kas Keluar</td>
																							<td style="width: 5px; height: 15px">
																								:</td>
																							<td style="height: 15px" class="formtext">
																								<asp:Label ID="lblTotalKasKeluar" runat="server"></asp:Label>
																							</td>
																						</tr>
																					</tbody>
																				</table>
																			</td>
																		</tr>
																		<tr>
																		<td>
																		<table border="0">
																<tbody>
																	<tr valign="top">
																		<td style="height: 45px" class="formtext">
                                                                            6.Sebutkan informasi lainnya yang ada</td>
																		<td style="width: 2px; height: 45px">
																			:</td>
																		<td style="height: 45px" class="formtext">
																			<asp:TextBox ID="txtWICInfoLainnya" runat="server" CssClass="textBox" Width="257px"
																				TextMode="MultiLine" Height="46px"></asp:TextBox></td>
																	</tr>
																</tbody>
															</table>
																		</td>
																		</tr>
																	</tbody>
																</table>
															</ajax:AjaxPanel>
														</td>
													</tr>
												</tbody>
											</table>
										</asp:View>
										<asp:View ID="vwMessage" runat="server">
											<table width="100%" style="horiz-align: center;">
												<tr>
													<td class="formtext" align="center">
														<asp:Label runat="server" ID="lblMsg"></asp:Label>
													</td>
												</tr>
												<tr>
													<td class="formtext" align="center">
														<asp:ImageButton ID="imgOKMsg" runat="server"  ImageUrl="~/Images/button/Ok.gif" />
													</td>
												</tr>
											</table>
										</asp:View>
									</asp:MultiView>
								</ajax:AjaxPanel>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
				<ajax:AjaxPanel ID="AjaxPanel5" runat="server">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td background="Images/button-bground.gif" align="left" valign="middle">
								<img src="Images/blank.gif" width="5" height="1" /></td>
							<td background="Images/button-bground.gif" align="left" valign="middle">
								<img src="images/arrow.gif" width="15" height="15" />&nbsp;</td>
							<td background="Images/button-bground.gif" style="width: 5px">
								&nbsp;</td>
							<td background="Images/button-bground.gif">
								<asp:ImageButton ID="ImageSave" runat="server"  ImageUrl="~/Images/Button/Save.gif">
								</asp:ImageButton>&nbsp;</td>
							<td background="Images/button-bground.gif" style="width: 62px">
								<asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" 
									ImageUrl="~/Images/Button/Cancel.gif"></asp:ImageButton>
							</td>
							<td width="99%" background="Images/button-bground.gif">
								<img src="Images/blank.gif" width="1" height="1" /></td>
							<td>
								</td>
						</tr>
					</table>
				</ajax:AjaxPanel>
			</td>
		</tr>
	</table>
	<asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
