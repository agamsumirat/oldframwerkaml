<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MenuApprovalDetail.aspx.vb" Inherits="MenuApprovalDetail" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<%@ Register Src="webcontrol/footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="webcontrol/menu.ascx" TagName="menu" TagPrefix="uc1" %>
<%@ Register Src="webcontrol/userinfo.ascx" TagName="userinfo" TagPrefix="uc1" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML xmlns="http://www.w3.org/1999/xhtml">
	<HEAD>
		<title>:: Anti Money Laundering ::</title>
		
	<link href="theme/aml.css" rel="stylesheet" type="text/css">
	<script src="script/x_load.js" type="text/javascript"></script>
	<script language="JavaScript1.2" src="script/stm31.js" type="text/javascript"></script>
	<script language="javascript">xInclude('script/x_core.js');</script>
    <script src="script/shsmenu.js"></script>
	<script src="script/aml.js" ></script>	
			<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	</HEAD>
	<body bgColor="#ffffff" leftMargin="0" topMargin="0" marginheight="0" marginwidth="0">
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td id="header">
  	<table background="images/main-header-bg.gif" height="81" width="100%" cellpadding="0" cellspacing="0" border="0">
  	    <tr>
            <td width="153"><img src="images/main-uang-dijemur-dan-logo.jpg" width="153" height="81" /></td>
  	        <td width="357"><img src="images/main-title.jpg" width="357" height="25" /></td>
  	        <td width="99%">&nbsp;</td>
  	        <td align="right" background="images/main-bar-merah-kanan.jpg" valign="top" class="toprightmenu"><img src="images/blank.gif" width="425" height="6"><br>
                  <uc1:userinfo ID="UserInfo1" runat="server" /></td>               
  	    </tr>
  	</table>
  	</td>
  </tr>
  <tr>
  	  <td><uc1:menu ID="Menu1" runat="server" /></td>
  </tr>
  <tr>
  	  <td id="td1" height="99%" valign="top"><div id="divcontent" class="divcontent" >
  	  <table cellpadding="0" cellspacing="0" border="0" width="100%">
	  	<tr>
			<td background="images/main-menu-bg-shadow.gif" height="10"></td>
		</tr>
		<tr>
			<td class="divcontentinside">
			            	<table id="tablebasic" cellSpacing="0" cellPadding="0" width="100%" border="0">
			    <form id="Form1" method="post" runat="server">
			        <tbody>
			 
			            <tr>
						    <td id="tdcontent" vAlign="top" background="images/contentbg.gif" colSpan="2" style="height: 1366px">
						           <table cellSpacing="4" cellPadding="0" width="100%">
								        <TBODY>
								     <tr>
										<td rowSpan="7"><IMG height="1" src="images/blank.gif" width="1%"></td>
										<td colSpan="2"><IMG height="2" src="images/blank.gif" width="1%"></td>
										<td rowSpan="7"><IMG height="1" src="images/blank.gif" width="1%"></td>
									</tr>								        
                                            <tr>
                                                <td colspan="2">
                                                </td>
                                            </tr>
									<tr>
										<td background="images/validationbground.gif" colSpan="2"><asp:validationsummary id="ValidSummMenuApprovalDetail" runat="server" HeaderText="There were errors on the page:"
												Width="100%" CssClass="validation"></asp:validationsummary><asp:customvalidator id="ValidCustomPageError" runat="server" Display="None"></asp:customvalidator></td>
									</tr>
									<tr>
										<td colSpan="2" ><IMG height="2" src="images/blank.gif" width="10"></td>
									</tr>
									<tr>
										<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
										<td class="maintitle" vAlign="bottom" width="99%">
											<asp:label id="lblTitle" runat="server">MENU MANAGEMENT DETAIL</asp:label>&nbsp;
										</td>
									</tr>
									
									<tr>
										<td bgColor="#ffffff" colSpan="2">
											<table id="Table1" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
												border="2">
												<TBODY>
												    <tr class="formText">
														<td width="5" bgColor="#ffffff" height="24">&nbsp;</td>
														<td width="100%" bgColor="#ffffff" colSpan="2" height="24"><asp:label id="LabelActivity" runat="server" Width="400px"></asp:label></td>
													</tr>
													<tr class="formText">
														<td width="5" bgColor="#ffffff" height="24">&nbsp;</td>
														<td width="141" bgColor="#ffffff" colSpan="2" height="24"><asp:label id="LabelGroup" runat="server" Width="400px"></asp:label></td>
													</tr>
													<tr class="formText">
														<td width="5" bgColor="#ffffff" height="24">&nbsp;</td>
														<td width="141" bgColor="#ffffff" colSpan="2" height="24">&nbsp;</td>
													</tr>
													<% if LCase(Session("Action")) = "edit" and LCase(Session("Category")) = "access authority" then %>
													<tr class="formText">
														<td width="5" bgColor="#ffffff" height="24">&nbsp;</td>
														<td width="141" bgColor="#ffffff" height="24"><asp:label id="LabelNew" runat="server"><b>Old Values 
																	</b></asp:label></td>
														<td bgColor="#ffffff" height="24"><asp:label id="LabelOld" runat="server"><b>New Values</b></asp:label></td>
													</tr>
													<% end if %>
													
													<% if LCase(Session("Category")) = "access authority" then %>
													<tr class="formText">
														<td width="5" bgColor="#ffffff" height="24">&nbsp;</td>
														<td width="141" bgColor="#ffffff" height="24"><asp:listbox id="ListFileOld" tabIndex="2" runat="server" Width="360px" CssClass="searcheditcbo"
																SelectionMode="Multiple" Height="137px"></asp:listbox></td>
														<td bgColor="#ffffff" height="24">&nbsp;<asp:listbox id="ListFile" tabIndex="2" runat="server" Width="360px" CssClass="searcheditcbo"
																SelectionMode="Multiple" Height="137px"></asp:listbox></td>
													</tr>
													<% end if %>
													
													<% if LCase(Session("Category")) = "menu" and LCase(Session("Action")) = "add" then %>
													<tr class="formText">
														<td width="5" bgColor="#ffffff" height="29">&nbsp;</td>
														<td vAlign="top" bgColor="#b9b6af" colSpan="2" height="200"><iframe id="framemenupreview" style="BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-LEFT-COLOR: white; BORDER-BOTTOM-WIDTH: 0px; BORDER-BOTTOM-COLOR: white; WIDTH: 100%; BORDER-TOP-COLOR: white; HEIGHT: 100%; BORDER-RIGHT-WIDTH: 0px; BORDER-RIGHT-COLOR: white"
																name="framemenupreview" src="MenuPreview.aspx"></iframe>
														</td>
													</tr>
													<% end if %>
													
													<% if LCase(Session("Category")) = "menu" and LCase(Session("Action")) = "edit" then %>
													<tr class="formText">
														<td width="5" bgColor="#ffffff" height="24">&nbsp;</td>
														<td width="141" bgColor="#ffffff" colSpan="2" height="24"><asp:label id="LabelOldMenu" runat="server"><b>Old Menu 
																	</b></asp:label></td>
													</tr>
													<tr class="formText">
														<td width="5" bgColor="#ffffff" height="200">&nbsp;</td>
														<td vAlign="top" bgColor="#b9b6af" colSpan="2" height="200"><iframe id="MenuPreviewOld" style="BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-LEFT-COLOR: white; BORDER-BOTTOM-WIDTH: 0px; BORDER-BOTTOM-COLOR: white; WIDTH: 100%; BORDER-TOP-COLOR: white; HEIGHT: 100%; BORDER-RIGHT-WIDTH: 0px; BORDER-RIGHT-COLOR: white"
																name="MenuPreviewOld" src="MenuPreviewOld.aspx"></iframe>
														</td>
													</tr>
													<tr class="formText">
														<td width="5" bgColor="#ffffff" height="24">&nbsp;</td>
														<td width="141" bgColor="#ffffff" colSpan="2" height="24"><asp:label id="LabelNewMenu" runat="server"><b>New Menu 
																	</b></asp:label></td>
													</tr>
													<tr class="formText">
														<td width="5" bgColor="#ffffff" height="24">&nbsp;</td>
														<td vAlign="top" bgColor="#b9b6af" colSpan="2" height="200"><iframe id="MenuPreviewNew" style="BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-LEFT-COLOR: white; BORDER-BOTTOM-WIDTH: 0px; BORDER-BOTTOM-COLOR: white; WIDTH: 100%; BORDER-TOP-COLOR: white; HEIGHT: 100%; BORDER-RIGHT-WIDTH: 0px; BORDER-RIGHT-COLOR: white"
																name="MenuPreviewNew" src="MenuPreview.aspx"></iframe>
														</td>
													</tr>
													<% end if %>
													
													<tr class="formText" bgColor="#dddddd" height="30">
														<td width="15"><IMG height="15" src="images/arrow.gif" width="15">
														</td>
														<td colSpan="3">&nbsp;
															<asp:imagebutton id="ImageButtonAccept" tabIndex="7" runat="server" ImageUrl="images\button\accept.gif"></asp:imagebutton>&nbsp;
															<asp:imagebutton id="ImageButtonReject" tabIndex="8" runat="server" ImageUrl="images\button\reject.gif"
																CausesValidation="False"></asp:imagebutton>&nbsp; <A title="Back to Menu Management List" href="MenuApproval.aspx">
																<IMG src="images\button\back.gif" border="0"></A>
															<asp:listbox id="lstMenuSubmit" style="DISPLAY: none" runat="server" SelectionMode="Multiple"
																Rows="1"></asp:listbox><asp:listbox id="lstMenuSubmitEditNew" style="DISPLAY: none" runat="server" SelectionMode="Multiple"
																Rows="1"></asp:listbox><asp:listbox id="lstMenuSubmitEditOld" style="DISPLAY: none" runat="server" SelectionMode="Multiple"
																Rows="1"></asp:listbox>
														</td>
													</tr>
                                                    <tr bgcolor="#dddddd" class="formText" height="30">
                                                        <td colspan="4">
                                                            <table>
                                                                <tr>
                                                                    <td><asp:panel id="containerlstmenuitem" style="DISPLAY: none" runat="server" bgColor="#b9b6af"
								align="center" vAlign="top"><SELECT id="lstmenuitem" size="12" name="lstmenuitem"></SELECT></asp:panel></td>
                                                                    <td><asp:panel id="containerlstMenuNew" style="DISPLAY: none" runat="server" bgColor="#b9b6af"
								align="center" vAlign="top"><SELECT id="lstMenuNew" size="12" name="lstMenuNew"></SELECT></asp:panel></td>
                                                                    <td><asp:panel id="containerlstMenuOld" style="DISPLAY: none" runat="server" bgColor="#b9b6af"
								align="center" vAlign="top"><SELECT id="lstMenuOld" size="12" name="lstMenuOld"></SELECT></asp:panel></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
													
													
												</TBODY>
											</table>
										</td>
									</tr>
								        </TBODY>
								   </table>
						    </td>
						</tr>
					</tbody>
			    </form>
			</table>
					

            
            </td>
		</tr>
	  </table></div></td>
  </tr>
  <tr>
  	<td><uc1:footer ID="Footer1" runat="server" /></td>
  </tr>
</table>
<% if (ValidCustomPageError.IsValid) and LCase(Session("Category")) = "menu" and LCase(Session("Action")) = "add" then %>
		<form id="frmmenupreview" style="DISPLAY: none" action="MenuPreview.aspx" method="post"
			target="framemenupreview">
			<textarea id="sothink" name="sothink"></textarea>
		</form>
		<script language="javascript">
			var arrayInJSPlain = <%= jsplain%>;
			var arrayInJS = <%= js %>;
			setMenuTargetFrame('<%= ObjMenu.GetMenuTargetFrame() %>');
			<% if not (ObjMenu.GetTopPattern() is nothing ) then  %> 
				setTopPattern('<%= ObjMenu.GetTopPattern().Replace("'","\\'").Replace(vbCr,"").Replace(vbLf,"") %>');
			<% end if  %>
			setArrowImageIfHasChild('<%= ObjMenu.GetArrowImageIfHasChild() %>');
			setArrowImageIfHasChildHover('<%= ObjMenu.GetArrowImageIfHasChildHover() %>');
			<% if not (BufferLevelPattern is nothing)  then %> 
			setLevelPattern(<%=BufferLevelPattern.ToString()%>);
			<%end if %>
			<% if not (BufferLevelMenuItem is nothing) then %> 
			setLevelMenuItem(<%=BufferLevelMenuItem.ToString()%>);
			<%end if %> 
			<% if not (BufferLevelMenuItemWithLink is nothing) then  %>
			setLevelMenuItemWithLink(<%=BufferLevelMenuItemWithLink.ToString()%>); 
			<%end if %>
			var _strLevelPattern
			var objR = new objRecurse();
			var oInputArray = new Array();
			function menuUpdateTree(){
				for (ij=0;ij<arrayInJS.length;ij++){
					objR.menuLoadFromArray(arrayInJS[ij][0],0,arrayInJS[ij][1]);
				}
				var sH = oInputArray.join(' ');
				var ele = document.getElementById('containerlstmenuitem');
				ele.innerHTML="<select name=\"lstmenuitem\" id=\"lstmenuitem\" size=\"12\" class=\"menuitemlist\" onChange=\"javascript:menuFillDataDetail();menuUpdateStatusMenuButton(this)\" multiple>"+sH+"</select>";
				
			}
			menuUpdateTree();
			menuGenerateMenuRelation();
		</script>
		<% end if  %>
		<% if (ValidCustomPageError.IsValid) and LCase(Session("Category")) = "menu" and LCase(Session("Action")) = "edit" then %>
		<form id="frmmenupreviewold" style="DISPLAY: none" action="MenuPreviewOld.aspx" method="post"
			target="MenuPreviewOld">
			<textarea id="sothinkold" name="sothinkold"></textarea>
		</form>
		<script language="javascript">
			var arrayInJSPlain = <%= jsplainold%>;
			var arrayInJS = <%= jsold %>;
			setMenuTargetFrame('<%= ObjMenu.GetMenuTargetFrame() %>');
			<% if not (ObjMenu.GetTopPattern() is nothing ) then  %> 
				setTopPattern('<%= ObjMenu.GetTopPattern().Replace("'","\\'").Replace(vbCr,"").Replace(vbLf,"") %>');
			<% end if  %>
			setArrowImageIfHasChild('<%= ObjMenu.GetArrowImageIfHasChild() %>');
			setArrowImageIfHasChildHover('<%= ObjMenu.GetArrowImageIfHasChildHover() %>');
			<% if not (BufferLevelPattern is nothing)  then %> 
			setLevelPattern(<%=BufferLevelPattern.ToString()%>);
			<%end if %>
			<% if not (BufferLevelMenuItem is nothing) then %> 
			setLevelMenuItem(<%=BufferLevelMenuItem.ToString()%>);
			<%end if %> 
			<% if not (BufferLevelMenuItemWithLink is nothing) then  %>
			setLevelMenuItemWithLink(<%=BufferLevelMenuItemWithLink.ToString()%>); 
			<%end if %>
			var _strLevelPattern
			var objR = new objRecurse();
			var oInputArray = new Array();
			function menuUpdateTreeOld(){
				for (ij=0;ij<arrayInJS.length;ij++){
					objR.menuLoadFromArray(arrayInJS[ij][0],0,arrayInJS[ij][1]);
				}
				var sH = oInputArray.join(' ');
				var ele = document.getElementById('containerlstMenuOld');
				ele.innerHTML="<select name=\"lstMenuOld\" id=\"lstMenuOld\" size=\"12\" class=\"menuitemlist\" onChange=\"javascript:menuFillDataDetail();menuUpdateStatusMenuButton(this)\" multiple>"+sH+"</select>";
				
			}		
			menuUpdateTreeOld();
			menuGenerateMenuRelationApprovalOld();
		</script>
		<form id="frmmenupreviewnew" style="DISPLAY: none" action="MenuPreview.aspx" method="post"
			target="MenuPreviewNew">
			<textarea id="Textarea1" name="sothink"></textarea>
		</form>
		<script language="javascript">
			var arrayInJSPlain = <%= jsplain%>;
			var arrayInJS = <%= js %>;
			setMenuTargetFrame('<%= ObjMenu.GetMenuTargetFrame() %>');
			<% if not (ObjMenu.GetTopPattern() is nothing ) then  %> 
				setTopPattern('<%= ObjMenu.GetTopPattern().Replace("'","\\'").Replace(vbCr,"").Replace(vbLf,"") %>');
			<% end if  %>
			setArrowImageIfHasChild('<%= ObjMenu.GetArrowImageIfHasChild() %>');
			setArrowImageIfHasChildHover('<%= ObjMenu.GetArrowImageIfHasChildHover() %>');
			<% if not (BufferLevelPattern is nothing)  then %> 
			setLevelPattern(<%=BufferLevelPattern.ToString()%>);
			<%end if %>
			<% if not (BufferLevelMenuItem is nothing) then %> 
			setLevelMenuItem(<%=BufferLevelMenuItem.ToString()%>);
			<%end if %> 
			<% if not (BufferLevelMenuItemWithLink is nothing) then  %>
			setLevelMenuItemWithLink(<%=BufferLevelMenuItemWithLink.ToString()%>); 
			<%end if %>
			var _strLevelPattern
			var objR = new objRecurse();
			var oInputArray = new Array();
			function menuUpdateTreeNew(){
				for (ij=0;ij<arrayInJS.length;ij++){
					objR.menuLoadFromArray(arrayInJS[ij][0],0,arrayInJS[ij][1]);
				}
				var sH = oInputArray.join(' ');
				var ele = document.getElementById('containerlstMenuNew');
				ele.innerHTML="<select name=\"lstMenuNew\" id=\"lstMenuNew\" size=\"12\" class=\"menuitemlist\" onChange=\"javascript:menuFillDataDetail();menuUpdateStatusMenuButton(this)\" multiple>"+sH+"</select>";
				
			}
			menuUpdateTreeNew();
			menuGenerateMenuRelationApprovalNew();
		</script>
		<% end if  %>
<script language="javascript">
window.onresize=arrangeView;
arrangeView();
		</script>
		
	</body>
</HTML>
