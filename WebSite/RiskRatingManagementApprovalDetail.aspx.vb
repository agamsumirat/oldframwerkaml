Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class RiskRatingManagementApprovalDetail
    Inherits System.Web.UI.Page

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param pk RiskRating management
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamPkRiskRatingManagement() As Int64
        Get
            Return Me.Request.Params("RiskRatingID")
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserID() As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.RiskRating_PendingApprovalTableAdapter
                Return AccessPending.SelectRiskRating_PendingApprovalUserID(Me.ParamPkRiskRatingManagement)
            End Using
        End Get
    End Property
#End Region

#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HideAll()
        Dim StrId As String = ""
        Select Case Me.ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.RiskRatingAdd
                StrId = "UserAdd"
            Case Sahassa.AML.Commonly.TypeConfirm.RiskRatingEdit
                StrId = "UserEdi"
            Case Sahassa.AML.Commonly.TypeConfirm.RiskRatingDelete
                StrId = "UserDel"
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select
        For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
            ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID.Substring(0, 7) = StrId
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load RiskRating add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadRiskRatingAdd()
        Me.LabelTitle.Text = "Activity: Add Risk Rating"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.RiskRating_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetRiskRatingApprovalData(Me.ParamPkRiskRatingManagement)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.RiskRating_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelRiskRatingNameAdd.Text = rowData.RiskRatingName
                    Me.TextRiskRatingDescriptionAdd.Text = rowData.Description
                    Me.LblScoreFromAdd.Text = rowData.RiskScoreFrom
                    Me.LblScoreToAdd.Text = rowData.RiskScoreTo
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load RiskRating edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadRiskRatingEdit()
        Me.LabelTitle.Text = "Activity: Edit Risk Rating"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.RiskRating_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetRiskRatingApprovalData(Me.ParamPkRiskRatingManagement)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.RiskRating_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelRiskRatingIDEditNewRiskRatingID.Text = rowData.RiskRatingID
                    Me.LabelRiskRatingEditNewRiskRatingName.Text = rowData.RiskRatingName
                    Me.TextRiskRatingEditNewRiskRatingDescription.Text = rowData.Description
                    Me.LblNewScoreFrom.Text = rowData.RiskScoreFrom
                    Me.LblNewScoreTo.Text = rowData.RiskScoreTo

                    Me.LabelRiskRatingIDEditOldRiskRatingID.Text = rowData.RiskRatingID_Old
                    Me.LabelRiskRatingNameEditOldRiskRatingName.Text = rowData.RiskRatingName_Old
                    Me.TextRiskRatingDescriptionEditOldRiskRatingDescription.Text = rowData.Description_Old
                    Me.LblOldScoreFrom.Text = rowData.RiskScoreFrom_Old
                    Me.LblOldScoreTo.Text = rowData.RiskScoreTo_Old
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load RiskRating delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadRiskRatingDelete()
        Me.LabelTitle.Text = "Activity: Delete Risk Rating"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.RiskRating_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetRiskRatingApprovalData(Me.ParamPkRiskRatingManagement)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.RiskRating_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelRiskRatingDeleteRiskRatingID.Text = rowData.RiskRatingID
                    Me.LabelRiskRatingDeleteRiskRatingName.Text = rowData.RiskRatingName
                    Me.TextRiskRatingDeleteRiskRatingDescription.Text = rowData.Description
                    Me.LblScoreFromDelete.Text = rowData.RiskScoreFrom
                    Me.LblScoreToDelete.Text = rowData.RiskScoreTo
                End If
            End Using
        End Using
    End Sub
#End Region

#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' RiskRating add accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptRiskRatingAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessRiskRating As New AMLDAL.AMLDataSetTableAdapters.RiskRatingTableAdapter
            oSQLTrans = BeginTransaction(AccessRiskRating, IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.RiskRating_ApprovalTableAdapter
                SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetRiskRatingApprovalData(Me.ParamPkRiskRatingManagement)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.RiskRating_ApprovalRow = ObjTable.Rows(0)
                    'tambahkan item tersebut dalam tabel RiskRating
                    AccessRiskRating.Insert(rowData.RiskRatingName, rowData.Description, rowData.RiskScoreFrom, rowData.RiskScoreTo, rowData.CreatedDate)

                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
                    'catat aktifitas dalam tabel Audit Trail
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        SetTransaction(AccessAudit, oSQLTrans)
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "CreatedDate", "Add", "", rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskRatingName", "Add", "", rowData.RiskRatingName, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "Description", "Add", "", rowData.Description, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreFrom", "Add", "", rowData.RiskScoreFrom, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreTo", "Add", "", rowData.RiskScoreTo, "Accepted")

                        'hapus item tersebut dalam tabel RiskRating_Approval
                        AccessPending.DeleteRiskRatingApproval(rowData.RiskRating_PendingApprovalID)

                        'hapus item tersebut dalam tabel RiskRating_PendingApproval
                        Using AccessPendingRiskRating As New AMLDAL.AMLDataSetTableAdapters.RiskRating_PendingApprovalTableAdapter
                            SetTransaction(AccessPendingRiskRating, oSQLTrans)
                            AccessPendingRiskRating.DeleteRiskRatingPendingApproval(rowData.RiskRating_PendingApprovalID)
                        End Using

                        oSQLTrans.Commit()
                        'TranScope.Complete()
                    End Using
                End If
            End Using
        End Using
        'End Using

        Me.Response.Redirect("RiskRatingManagementApproval.aspx", False)

    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' RiskRating edit accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptRiskRatingEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessRiskRating As New AMLDAL.AMLDataSetTableAdapters.RiskRatingTableAdapter
            oSQLTrans = BeginTransaction(AccessRiskRating, IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.RiskRating_ApprovalTableAdapter
                SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetRiskRatingApprovalData(Me.ParamPkRiskRatingManagement)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.RiskRating_ApprovalRow = ObjTable.Rows(0)
                    'update item tersebut dalam tabel RiskRating
                    AccessRiskRating.UpdateRiskRating(rowData.RiskRatingID, rowData.RiskRatingName, rowData.Description, rowData.RiskScoreFrom, rowData.RiskScoreTo)

                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
                    'catat aktifitas dalam tabel Audit Trail
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        SetTransaction(AccessAudit, oSQLTrans)

                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "CreatedDate", "Edit", rowData.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskRatingName", "Edit", rowData.RiskRatingName_Old, rowData.RiskRatingName, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "Description", "Edit", rowData.Description_Old, rowData.Description, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskRatingID", "Edit", rowData.RiskRatingID_Old, rowData.RiskRatingID, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreFrom", "Edit", rowData.RiskScoreFrom_Old, rowData.RiskScoreFrom, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreTo", "Edit", rowData.RiskScoreTo_Old, rowData.RiskScoreTo, "Accepted")

                        'hapus item tersebut dalam tabel RiskRating_Approval
                        AccessPending.DeleteRiskRatingApproval(rowData.RiskRating_PendingApprovalID)

                        'hapus item tersebut dalam tabel RiskRating_PendingApproval
                        Using AccessPendingRiskRating As New AMLDAL.AMLDataSetTableAdapters.RiskRating_PendingApprovalTableAdapter
                            SetTransaction(AccessPendingRiskRating, oSQLTrans)
                            AccessPendingRiskRating.DeleteRiskRatingPendingApproval(rowData.RiskRating_PendingApprovalID)
                        End Using
                        oSQLTrans.Commit()
                        'TranScope.Complete()
                    End Using
                End If
            End Using
        End Using
        'End Using

        Me.Response.Redirect("RiskRatingManagementApproval.aspx", False)

    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' RiskRating delete accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptRiskRatingDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessRiskRating As New AMLDAL.AMLDataSetTableAdapters.RiskRatingTableAdapter
            oSQLTrans = BeginTransaction(AccessRiskRating, IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.RiskRating_ApprovalTableAdapter
                SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetRiskRatingApprovalData(Me.ParamPkRiskRatingManagement)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.RiskRating_ApprovalRow = ObjTable.Rows(0)
                    'hapus item tersebut dari tabel Group
                    AccessRiskRating.DeleteRiskRating(rowData.RiskRatingID)

                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
                    'catat aktifitas dalam tabel Audit Trail
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        SetTransaction(AccessAudit, oSQLTrans)

                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "CreatedDate", "Delete", rowData.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskRatingName", "Delete", rowData.RiskRatingName_Old, rowData.RiskRatingName, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "Description", "Delete", rowData.Description_Old, rowData.Description, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskRatingID", "Delete", rowData.RiskRatingID_Old, rowData.RiskRatingID, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreFrom", "Delete", rowData.RiskScoreFrom_Old, rowData.RiskScoreFrom, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreTo", "Delete", rowData.RiskScoreTo_Old, rowData.RiskScoreTo, "Accepted")

                        'hapus item tersebut dalam tabel RiskRating_Approval
                        AccessPending.DeleteRiskRatingApproval(rowData.RiskRating_PendingApprovalID)

                        'hapus item tersebut dalam tabel Group_PendingApproval
                        Using AccessPendingRiskRating As New AMLDAL.AMLDataSetTableAdapters.RiskRating_PendingApprovalTableAdapter
                            SetTransaction(AccessPendingRiskRating, oSQLTrans)
                            AccessPendingRiskRating.DeleteRiskRatingPendingApproval(rowData.RiskRating_PendingApprovalID)
                        End Using

                        oSQLTrans.Commit()
                        'TranScope.Complete()
                    End Using
                End If
            End Using
        End Using
        'End Using

        Me.Response.Redirect("RiskRatingManagementApproval.aspx", False)

    End Sub

#End Region
#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject RiskRating add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectRiskRatingAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = BeginTransaction(AccessAudit, IsolationLevel.ReadUncommitted)
            Using AccessRiskRating As New AMLDAL.AMLDataSetTableAdapters.RiskRating_ApprovalTableAdapter
                SetTransaction(AccessRiskRating, oSQLTrans)
                Using AccessRiskRatingPending As New AMLDAL.AMLDataSetTableAdapters.RiskRating_PendingApprovalTableAdapter
                    SetTransaction(AccessRiskRatingPending, oSQLTrans)
                    Using TableRiskRating As Data.DataTable = AccessRiskRating.GetRiskRatingApprovalData(Me.ParamPkRiskRatingManagement)
                        If TableRiskRating.Rows.Count > 0 Then
                            Dim ObjRow As AMLDAL.AMLDataSet.RiskRating_ApprovalRow = TableRiskRating.Rows(0)
                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
                            'catat aktifitas dalam tabel Audit Trail
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "CreatedDate", "Add", "", ObjRow.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskRatingName", "Add", "", ObjRow.RiskRatingName, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "Description", "Add", "", ObjRow.Description, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreFrom", "Add", "", ObjRow.RiskScoreFrom, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreTo", "Add", "", ObjRow.RiskScoreTo, "Rejected")


                            'hapus item tersebut dalam tabel RiskRating_Approval
                            AccessRiskRating.DeleteRiskRatingApproval(Me.ParamPkRiskRatingManagement)

                            'hapus item tersebut dalam tabel RiskRating_PendingApproval
                            AccessRiskRatingPending.DeleteRiskRatingPendingApproval(Me.ParamPkRiskRatingManagement)
                            oSQLTrans.Commit()
                            'TranScope.Complete()
                        End If
                    End Using
                End Using
            End Using
        End Using
        'End Using

        Me.Response.Redirect("RiskRatingManagementApproval.aspx", False)

    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject Group edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectRiskRatingEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = BeginTransaction(AccessAudit, IsolationLevel.ReadUncommitted)
            Using AccessRiskRating As New AMLDAL.AMLDataSetTableAdapters.RiskRating_ApprovalTableAdapter
                SetTransaction(AccessRiskRating, oSQLTrans)
                Using AccessRiskRatingPending As New AMLDAL.AMLDataSetTableAdapters.RiskRating_PendingApprovalTableAdapter
                    SetTransaction(AccessRiskRatingPending, oSQLTrans)
                    Using TableRiskRating As Data.DataTable = AccessRiskRating.GetRiskRatingApprovalData(Me.ParamPkRiskRatingManagement)
                        If TableRiskRating.Rows.Count > 0 Then
                            Dim ObjRow As AMLDAL.AMLDataSet.RiskRating_ApprovalRow = TableRiskRating.Rows(0)

                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
                            'catat aktifitas dalam tabel Audit Trail
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "CreatedDate", "Edit", ObjRow.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskRatingName", "Edit", ObjRow.RiskRatingName_Old, ObjRow.RiskRatingName, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "Description", "Edit", ObjRow.Description_Old, ObjRow.Description, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskRatingID", "Edit", ObjRow.RiskRatingID_Old, ObjRow.RiskRatingID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreFrom", "Edit", ObjRow.RiskScoreFrom_Old, ObjRow.RiskScoreFrom, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreTo", "Edit", ObjRow.RiskScoreTo_Old, ObjRow.RiskScoreTo, "Rejected")

                            'hapus item tersebut dalam tabel RiskRating_Approval
                            AccessRiskRating.DeleteRiskRatingApproval(Me.ParamPkRiskRatingManagement)

                            'hapus item tersebut dalam tabel RiskRating_PendingApproval
                            AccessRiskRatingPending.DeleteRiskRatingPendingApproval(Me.ParamPkRiskRatingManagement)

                            oSQLTrans.Commit()
                            'TranScope.Complete()
                        End If
                    End Using
                End Using
            End Using
        End Using
        'End Using

        Me.Response.Redirect("RiskRatingManagementApproval.aspx", False)

    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject Group delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectRiskRatingDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = BeginTransaction(AccessAudit, IsolationLevel.ReadUncommitted)
            Using AccessRiskRating As New AMLDAL.AMLDataSetTableAdapters.RiskRating_ApprovalTableAdapter
                SetTransaction(AccessRiskRating, oSQLTrans)
                Using AccessRiskRatingPending As New AMLDAL.AMLDataSetTableAdapters.RiskRating_PendingApprovalTableAdapter
                    SetTransaction(AccessRiskRatingPending, oSQLTrans)
                    Using TableRiskRating As Data.DataTable = AccessRiskRating.GetRiskRatingApprovalData(Me.ParamPkRiskRatingManagement)
                        If TableRiskRating.Rows.Count > 0 Then
                            Dim ObjRow As AMLDAL.AMLDataSet.RiskRating_ApprovalRow = TableRiskRating.Rows(0)
                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
                            'catat aktifitas dalam tabel Audit Trail
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "CreatedDate", "Delete", ObjRow.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskRatingName", "Delete", ObjRow.RiskRatingName_Old, ObjRow.RiskRatingName, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "Description", "Delete", ObjRow.Description_Old, ObjRow.Description, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskRatingID", "Delete", ObjRow.RiskRatingID_Old, ObjRow.RiskRatingID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreFrom", "Delete", ObjRow.RiskScoreFrom_Old, ObjRow.RiskScoreFrom, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreTo", "Delete", ObjRow.RiskScoreTo_Old, ObjRow.RiskScoreTo, "Rejected")

                            'hapus item tersebut dalam tabel RiskRating_Approval
                            AccessRiskRating.DeleteRiskRatingApproval(Me.ParamPkRiskRatingManagement)

                            'hapus item tersebut dalam tabel RiskRating_PendingApproval
                            AccessRiskRatingPending.DeleteRiskRatingPendingApproval(Me.ParamPkRiskRatingManagement)

                            oSQLTrans.Commit()
                            'TranScope.Complete()
                        End If
                    End Using
                End Using
            End Using
        End Using
        'End Using

        Me.Response.Redirect("RiskRatingManagementApproval.aspx", False)

    End Sub
#End Region

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HideAll()
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.RiskRatingAdd
                        Me.LoadRiskRatingAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.RiskRatingEdit
                        Me.LoadRiskRatingEdit()
                    Case Sahassa.AML.Commonly.TypeConfirm.RiskRatingDelete
                        Me.LoadRiskRatingDelete()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.RiskRatingAdd
                    Me.AcceptRiskRatingAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.RiskRatingEdit
                    Me.AcceptRiskRatingEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.RiskRatingDelete
                    Me.AcceptRiskRatingDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.RiskRatingAdd
                    Me.RejectRiskRatingAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.RiskRatingEdit
                    Me.RejectRiskRatingEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.RiskRatingDelete
                    Me.RejectRiskRatingDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Me.Response.Redirect("RiskRatingManagementApproval.aspx", False)
    End Sub
End Class