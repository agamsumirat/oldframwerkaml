<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MenuAdd.aspx.vb" Inherits="MenuAdd" %>
<%@ Register Src="webcontrol/userinfo.ascx" TagName="userinfo" TagPrefix="uc1" %>
<%@ Register Src="webcontrol/footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="webcontrol/menu.ascx" TagName="menu" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<link href="theme/aml.css" rel="stylesheet" type="text/css">
		<link href="theme/shsmenu.css" rel="stylesheet" type="text/css">
	<script src="script/x_load.js" type="text/javascript"></script>
	<script language="javascript">xInclude('script/x_core.js');</script>
	<script language="JavaScript1.2" src="script/stm31.js" type="text/javascript"></script>	
    <script src="script/shsmenu.js"></script>
	</HEAD>
	<body leftMargin="0" topMargin="0" >
	<table width="100%" cellspacing="0" cellpadding="0" border="0">

          <tr>
  	        <td id="header"><table background="images/main-header-bg.gif" height="81" width="100%" cellpadding="0" cellspacing="0" border="0">
  	            <tr>
                    <td width="153"><img src="images/main-uang-dijemur-dan-logo.jpg" width="153" height="81" /></td>
  	                <td width="357"><img src="images/main-title.jpg" width="357" height="25" /></td>
  	                <td width="99%">&nbsp;</td>
  	                <td align="right" background="images/main-bar-merah-kanan.jpg" valign="top" class="toprightmenu" nowrap><img src="images/blank.gif" width="425" height="6"><br>
                          <uc1:userinfo ID="UserInfo2" runat="server" />
                        </td>
  	            </tr>
  	        </table></td>
          </tr>
          <tr>
  	          <td><uc1:menu ID="Menu2" runat="server" /></td>
          </tr>	    
	    <tr>
	        <td><table id="tablebasic" cellSpacing="0" cellPadding="0" width="100%" border="0">

		        <tr>
			        <td id="tdcontent" vAlign="top" colSpan="2" style="width:100%">

                        <form id="Form1" method="post" runat="server">	
		                        <%--<tr>
			                        <td background="images/menubottomshadowbg.gif" colSpan="2"><IMG height="4" src="images/blank.gif" width="1"></td>
		                        </tr>--%>
				                        <table cellSpacing="4" cellPadding="0" width="100%">							
					                        <tr>
						                        <td bgColor="#ffffff" class="maintitle" vAlign="bottom" width="99%"><asp:label id="lblTitle" runat="server">menu management</asp:label></td>
					                        </tr>
					                        <tr>
						                        <td bgColor="#ffffff" colSpan="2">
							                        <table style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid"
								                        cellSpacing="0" cellPadding="0" width="100%" border="0">
								                        <TBODY>
									                        <tr>
										                        <td background="images/validationbground.gif" colSpan="2"><asp:validationsummary id="ValidSummMenuManagement" runat="server" CssClass="validation" HeaderText="There were errors  on this page:"
												                        Width="100%"></asp:validationsummary><asp:customvalidator id="cvalPageError" runat="server" Display="None"></asp:customvalidator></td>
									                        </tr>
									                        <tr>
										                        <td background="images/validationbground.gif" colSpan="2"><asp:label id="LabelMessage" runat="server" CssClass="validationok" Visible="False"></asp:label></td>
									                        </tr>
									                        <tr>
										                        <td class="menutitle" colSpan="2" height="20">&nbsp;&nbsp;Group Selection</td>
									                        </tr>
									                        <tr>
										                        <td colSpan="2" height="38">
											                        <table style="MARGIN-LEFT: 4px" borderColor="#ffffff" cellSpacing="1" cellPadding="2" bgColor="#dddddd"
												                        border="2">
												                        <tr>
													                        <td bgColor="#ffffff">&nbsp;<asp:customvalidator id="ValidCustomComboGroup" runat="server" Display="Dynamic" ErrorMessage="CustomValidator">*</asp:customvalidator></td>
													                        <td noWrap bgColor="#ffffff">Edit Menu of This Group</td>
													                        <td bgColor="#ffffff">:</td>
													                        <td bgColor="#ffffff"><asp:dropdownlist id="cbomenugroup" runat="server" CssClass="menucombobox" AutoPostBack="True"></asp:dropdownlist></td>
												                        </tr>
											                        </table>
										                        </td>
									                        </tr>
									                        <tr>
										                        <td vAlign="top" width="260">
											                        <table height="330" cellSpacing="0" cellPadding="0" width="100%" border="0">
												                        <tr>
													                        <td class="menutitle" height="20">&nbsp;&nbsp;Menu Item</td>
												                        </tr>
												                        <tr>
													                        <td align="center" background="images/menu/bgroundmenu.gif" height="51">
														                        <table cellSpacing="0" cellPadding="0" align="center" border="0">
															                        <tr>
																                        <td><A href="javascript:menuAddNewMenuItem();javascript:menuUpdateStatusMenuButton();"><IMG onmouseup="javascript:menuButtonUp(this)" class="menubuttonmouseout" onmousedown="javascript:menuButtonDown(this)"
																			                        id="menubuttonnew" onmouseover="javascript:menuButtonHover(this)" title="New menu item" onmouseout="javascript:menuButtonOut(this)" height="30" hspace="4" src="images/menu/add.gif"
																			                        width="30" vspace="4" border="0"></A></td>
																                        <td><A href="javascript:menuDeleteMenuItem();javascript:menuUpdateStatusMenuButton();"><IMG onmouseup="javascript:menuButtonUp(this)" class="menubuttonmouseout" onmousedown="javascript:menuButtonDown(this)"
																			                        id="menubuttondelete" onmouseover="javascript:menuButtonHover(this)" title="Delete menu item" onmouseout="javascript:menuButtonOut(this)" height="30" hspace="4"
																			                        src="images/menu/delete.gif" width="30" vspace="4" border="0"></A></td>
																                        <td><IMG height="38" hspace="5" src="images/menu/separator.gif" width="2"></td>
																                        <td><A href="javascript:menuMoveSelectionLeft();javascript:menuUpdateStatusMenuButton()"><IMG onmouseup="javascript:menuButtonUp(this)" class="menubuttonmouseout" onmousedown="javascript:menuButtonDown(this)"
																			                        id="menubuttonleft" onmouseover="javascript:menuButtonHover(this)" title="Decrease indent" onmouseout="javascript:menuButtonOut(this)" height="30" hspace="4" src="images/menu/arrow_left.gif"
																			                        width="30" vspace="4" border="0"></A></td>
																                        <td><A href="javascript:menuMoveSelectionRight();javascript:menuUpdateStatusMenuButton()"><IMG onmouseup="javascript:menuButtonUp(this)" class="menubuttonmouseout" onmousedown="javascript:menuButtonDown(this)"
																			                        id="menubuttonright" onmouseover="javascript:menuButtonHover(this)" title="Increase indent" onmouseout="javascript:menuButtonOut(this)" height="30" hspace="4" src="images/menu/arrow_right.gif"
																			                        width="30" vspace="4" border="0"></A></td>
																                        <td><A href="javascript:menuMoveSelectionUp();javascript:menuUpdateStatusMenuButton()"><IMG onmouseup="javascript:menuButtonUp(this)" class="menubuttonmouseout" onmousedown="javascript:menuButtonDown(this)"
																			                        id="menubuttonup" onmouseover="javascript:menuButtonHover(this)" title="Move up" onmouseout="javascript:menuButtonOut(this)" height="30" hspace="4" src="images/menu/arrow_up.gif"
																			                        width="30" vspace="4" border="0"></A></td>
																                        <td><A href="javascript:menuMoveSelectionDown();javascript:menuUpdateStatusMenuButton()"><IMG onmouseup="javascript:menuButtonUp(this)" class="menubuttonmouseout" onmousedown="javascript:menuButtonDown(this)"
																			                        id="menubuttondown" onmouseover="javascript:menuButtonHover(this)" title="Move down" onmouseout="javascript:menuButtonOut(this)" height="30" hspace="4" src="images/menu/arrow_down.gif"
																			                        width="30" vspace="4" border="0"></A></td>
															                        </tr>
														                        </table>
													                        </td>
												                        </tr>
												                        <tr>
													                        <td id="containerlstmenuitem" vAlign="top" align="center" bgColor="#b9b6af" style="height: 198px"><select id="lstmenuitem" size="12" name="lstmenuitem" style="width:100%"></select></td>
												                        </tr>
												                        <tr>
													                        <td vAlign="top">
														                        <table style="BORDER-TOP: black 1px solid" cellSpacing="0" cellPadding="0" width="100%">
															                        <tr>
																                        <td class="menutitle" colSpan="3" height="20">&nbsp;&nbsp;Menu Item Properties</td>
															                        </tr>
															                        <tr>
																                        <td bgColor="#ffffff" colSpan="2" height="15">
																	                        <table cellSpacing="4" cellPadding="0" width="100%">
																		                        <tr>
																			                        <td align="center">
																				                        <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="95%" bgColor="#dddddd"
																					                        border="2">
																					                        <tr>
																						                        <td bgColor="#ffffff">Name <input id="txtmenuid" type="hidden" value="-1" name="hidden"></td>
																						                        <td bgColor="#ffffff">:</td>
																						                        <td bgColor="#ffffff"><input class="menutextbox" id="txtmenuname" style="WIDTH: 160px" maxLength="100" onchange="javascript:menuUpdateMenuItemProperties(document.getElementById('txtmenuid').value)"
																								                        name="TextMenuName"></td>
																					                        </tr>
																					                        <tr class="TulisanForm">
																						                        <td bgColor="#ffffff">Hyperlink</td>
																						                        <td bgColor="#ffffff">:</td>
																						                        <td bgColor="#ffffff"><asp:dropdownlist id="cbomenuhyperlink" runat="server" CssClass="menucombobox" onChange="javascript:menuUpdateMenuItemProperties(document.getElementById('txtmenuid').value)"></asp:dropdownlist></td>
																					                        </tr>
																					                        <tr class="TulisanForm">
																						                        <td vAlign="top" bgColor="#ffffff">Description<br>
																							                        (255 Char)</td>
																						                        <td vAlign="top" bgColor="#ffffff">:</td>
																						                        <td bgColor="#ffffff"><textarea class="menutextbox" id="txtmenudescription" style="WIDTH: 160px; HEIGHT: 50px" name="TextMenuDescription"
																								                        onchange="javascript:menuUpdateMenuItemProperties(document.getElementById('txtmenuid').value)"></textarea></td>
																					                        </tr>
																				                        </table>
																			                        </td>
																		                        </tr>
																	                        </table>
																                        </td>
															                        </tr>
														                        </table>
													                        </td>
												                        </tr>
											                        </table>
										                        </td>
										                        <td vAlign="top" width="99%">
											                        <table height="376" cellSpacing="0" cellPadding="0" width="100%" border="0">
												                        <tr>
													                        <td class="menutitle" colSpan="3" height="20">&nbsp;&nbsp;Menu Preview Area</td>
												                        </tr>
												                        <tr>
													                        <td background="images/menu/bgroundmenu.gif" height="20"><IMG height="1" src="images/blank.gif" width="3"><A href="javascript:menuGenerateMenuRelation()"><IMG onmouseup="javascript:menuButtonUp(this)" class="menubuttonmouseout" onmousedown="javascript:menuButtonDown(this)"
																                        onmouseover="javascript:menuButtonHover(this)" title="Refresh menu preview" onmouseout="javascript:menuButtonOut(this)" height="30" hspace="4" src="images/menu/refresh.gif" width="30" vspace="4" border="0"></A></td>
												                        </tr>
												                        <tr>
													                        <td vAlign="top" bgColor="#b9b6af"><iframe id="framemenupreview" style="BORDER-RIGHT: white 0px; BORDER-TOP: white 0px; BORDER-LEFT: white 0px; WIDTH: 100%; BORDER-BOTTOM: white 0px; HEIGHT: 100%"
															                        name="framemenupreview" src="menupreview.aspx"></iframe></td>
												                        </tr>
											                        </table>
										                        </td>
									                        </tr>
									                        <tr>
										                        <td colSpan="2">
											                        <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
												                        border="2">
												                        <tr class="TulisanForm" bgColor="#dddddd" height="30">
													                        <td width="15"><IMG src="images/menu/arrow.gif"></td>
													                        <td width="99%">&nbsp;&nbsp;
														                        <asp:imagebutton id="ImageSave" runat="server" ImageUrl="images/menu/btn_save.gif"></asp:imagebutton><A href="#"></A>&nbsp;&nbsp;&nbsp;
														                        <asp:listbox id="lstMenuSubmit" style="DISPLAY: none" runat="server" Rows="1" SelectionMode="Multiple"></asp:listbox></td>
												                        </tr>
											                        </table>
										                        </td>
									                        </tr>
								                        </TBODY></table>
						                        </td>
					                        </tr>
				                        </table>
		                        </form>
			        </td>
		        </tr>					
		        <tr>
		            <td><uc1:footer ID="Footer1" runat="server" /></td>
		        </tr>
		        </table></td>
	    </tr>
	</table>
	
		
		<form id="frmmenupreview" style="DISPLAY: none" action="MenuPreview.aspx" method="post"
			target="framemenupreview">
			<textarea id="sothink" name="sothink"></textarea>
		</form>
		<% if (cvalPageError.IsValid)  then %>
		<script language="javascript">
		var arrayInJSPlain = <%= jsplain%>;
		var arrayInJS = <%= js %>;
		setMenuTargetFrame('<%= ObjMenu.GetMenuTargetFrame() %>');
		<% if not (ObjMenu.GetTopPattern() is nothing ) then  %> 
			setTopPattern('<%= ObjMenu.GetTopPattern().Replace("'","\\'").Replace(vbCr,"").Replace(vbLf,"") %>');
		 <% end if  %>
		setArrowImageIfHasChild('<%= ObjMenu.GetArrowImageIfHasChild() %>');
		setArrowImageIfHasChildHover('<%= ObjMenu.GetArrowImageIfHasChildHover() %>');
		<% if not (BufferLevelPattern is nothing)  then %> 
		setLevelPattern(<%=BufferLevelPattern.ToString()%>);
		 <%end if %>
		<% if not (BufferLevelMenuItem is nothing) then %> 
		   setLevelMenuItem(<%=BufferLevelMenuItem.ToString()%>);
		  <%end if %> 
		<% if not (BufferLevelMenuItemWithLink is nothing) then  %>
		 setLevelMenuItemWithLink(<%=BufferLevelMenuItemWithLink.ToString()%>); 
		<%end if %>
		var _strLevelPattern
		var objR = new objRecurse();
		var oInputArray = new Array();
		function menuUpdateTree(){
			for (ij=0;ij<arrayInJS.length;ij++){
				objR.menuLoadFromArray(arrayInJS[ij][0],0,arrayInJS[ij][1]);
			}
			var sH = oInputArray.join(' ');
			var ele = document.getElementById('containerlstmenuitem');
			ele.innerHTML="<select name=\"lstmenuitem\" id=\"lstmenuitem\" size=\"12\" class=\"menuitemlist\" onChange=\"javascript:menuFillDataDetail();menuUpdateStatusMenuButton(this)\" multiple>"+sH+"</select>";
			
		}
	
		menuUpdateTree();
		menuGenerateMenuRelation();
		menuUpdateStatusMenuButton();
		</script>
		<% end if  %>
	</body>
</HTML>

