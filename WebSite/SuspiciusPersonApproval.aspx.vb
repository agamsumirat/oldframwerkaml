Imports SahassaNettier.Entities
Partial Class SuspiciusPersonApproval

    Inherits Parent

#Region " Property "
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("MsSuspiciusPersonApprovalViewSelected") Is Nothing, New ArrayList, Session("MsSuspiciusPersonApprovalViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("MsSuspiciusPersonApprovalViewSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("MsSuspiciusPersonApprovalViewFieldSearch") Is Nothing, "", Session("MsSuspiciusPersonApprovalViewFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsSuspiciusPersonApprovalViewFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("MsSuspiciusPersonApprovalViewValueSearch") Is Nothing, "", Session("MsSuspiciusPersonApprovalViewValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsSuspiciusPersonApprovalViewValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("MsSuspiciusPersonApprovalViewSort") Is Nothing, "PK_Suspicius_Approval_ID  asc", Session("MsSuspiciusPersonApprovalViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("MsSuspiciusPersonApprovalViewSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("MsSuspiciusPersonApprovalViewCurrentPage") Is Nothing, 0, Session("MsSuspiciusPersonApprovalViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsSuspiciusPersonApprovalViewCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("MsSuspiciusPersonApprovalViewRowTotal") Is Nothing, 0, Session("MsSuspiciusPersonApprovalViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsSuspiciusPersonApprovalViewRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable(ByVal whereClause As String, ByVal orderBy As String, ByVal start As Integer, ByVal pageLength As Integer) As TList(Of Suspicius_Approval)
        Get
            Return IIf(Session("MsSuspiciusPersonApprovalViewData") Is Nothing, AMLBLL.SuspiciusPersonBLL.GetSuspiciusPersonApproval(whereClause, orderBy, start, pageLength, Me.SetnGetRowTotal), Session("MsSuspiciusPersonApprovalViewData"))
        End Get
        Set(ByVal value As TList(Of Suspicius_Approval))
            Session("MsSuspiciusPersonApprovalViewData") = value
        End Set
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' fill search
    ''' </summary>
    ''' <remarks>
    ''' Group Name
    ''' Group_PendingApprovalUserID
    ''' Group_PendingApprovalEntryDate
    ''' Group_PendingApprovalModeID
    ''' </remarks>
    ''' <history>
    ''' Modification of similar function created on 05/06/2006 by [Ariwibawa]
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillSearch()
        Me.ComboSearch.Items.Add(New ListItem("...", ""))
        Me.ComboSearch.Items.Add(New ListItem("Created By", "UserID Like '%-=Search=-%'"))
        Me.ComboSearch.Items.Add(New ListItem("Entry Date", "EntryDate BETWEEN '-=Search=- 00:00' AND '-=Search=- 23:59'"))
        Me.ComboSearch.Items.Add(New ListItem("Nama", "Category Like '%-=Search=-%'"))
        Me.ComboSearch.Items.Add(New ListItem("Action", "modeid Like '%-=Search=-%'"))
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid edit command handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified 
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Dim TypeConfirm As Sahassa.AML.Commonly.TypeConfirm
        Dim GroupID As Int64
        Try
            GroupID = e.Item.Cells(1).Text

            If e.Item.Cells(5).Text.IndexOf("Add") > -1 Then
                TypeConfirm = Sahassa.AML.Commonly.TypeConfirm.SuspiciusPersonAdd
            End If
            If e.Item.Cells(5).Text.IndexOf("Edit") > -1 Then
                TypeConfirm = Sahassa.AML.Commonly.TypeConfirm.SuspiciusPersonEdit
            End If
            If e.Item.Cells(5).Text.IndexOf("Delete") > -1 Then
                TypeConfirm = Sahassa.AML.Commonly.TypeConfirm.SuspiciusPersonDelete
            End If

            Sahassa.AML.Commonly.SessionIntendedPage = "SuspiciusPersonApprovalDetail.aspx?PK_Suspicius_Approval_ID=" & GroupID & "&TypeConfirm=" & TypeConfirm.ToString("D")

            MagicAjax.AjaxCallHelper.Redirect("SuspiciusPersonApprovalDetail.aspx?PK_Suspicius_Approval_ID=" & GroupID & "&TypeConfirm=" & TypeConfirm.ToString("D"))
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("MsSuspiciusPersonApprovalViewSelected") = Nothing
        Session("MsSuspiciusPersonApprovalViewFieldSearch") = Nothing
        Session("MsSuspiciusPersonApprovalViewValueSearch") = Nothing
        Session("MsSuspiciusPersonApprovalViewSort") = Nothing
        Session("MsSuspiciusPersonApprovalViewCurrentPage") = Nothing
        Session("MsSuspiciusPersonApprovalViewRowTotal") = Nothing
        Session("MsSuspiciusPersonApprovalViewData") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' onclick="popUpCalendar(this, document.getElementById('TextSearch'), 'dd-mmm-yyyy')" 
    ''' </remarks>
    ''' <history>
    ''' Modification of similar function created on 05/06/2006 by [Ariwibawa]
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()

                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check(this, 2, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "');")
                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")

                
                Me.FillSearch()
                Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim UserId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(UserId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(UserId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(UserId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            'Dim Rows() As AMLDAL.AMLDataSet.Group_PendingApprovalRow = 
            Dim strfilter As String = " UserID <> '" & Sahassa.AML.Commonly.SessionUserId & "' AND UserID IN (SELECT UserId FROM UserWorkingUnitAssignment WHERE WorkingUnitId IN (SELECT WorkingUnitId FROM UserWorkingUnitAssignment WHERE UserId = '" & Sahassa.AML.Commonly.SessionUserId & "'))"
            If Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")).Length > 0 Then
                strfilter = strfilter & " and " & Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''"))


            End If
            Me.GridMSUserView.DataSource = Me.SetnGetBindTable(strfilter, Me.SetnGetSort, Me.SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow)
            'Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
            'Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridMSUserView.DataBind()
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            If ComboSearch.SelectedIndex <> 4 Then
                Me.TextSearch.Text = Me.SetnGetValueSearch
            Else
                If SetnGetValueSearch = "1" Then
                    Me.TextSearch.Text = "add"

                ElseIf SetnGetValueSearch = "2" Then
                    Me.TextSearch.Text = "Edit"
                ElseIf SetnGetValueSearch = "3" Then
                    Me.TextSearch.Text = "Delete"
                Else
                    Me.TextSearch.Text = Me.SetnGetValueSearch
                End If

            End If

        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
                Me.SetnGetValueSearch = Me.TextSearch.Text
            ElseIf Me.ComboSearch.SelectedIndex = 2 Then
                Dim DateSearch As DateTime
                Try
                    DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
                Catch ex As ArgumentOutOfRangeException
                    Throw New Exception("Input character cannot be convert to datetime.")
                Catch ex As FormatException
                    Throw New Exception("Unknown format date")
                End Try
                Me.SetnGetValueSearch = DateSearch.ToString("dd-MMM-yyyy")
            ElseIf Me.ComboSearch.SelectedIndex = 4 Then
                If TextSearch.Text.ToLower = "add" Then
                    Me.SetnGetValueSearch = "1"
                ElseIf TextSearch.Text.ToLower = "edit" Then
                    Me.SetnGetValueSearch = "2"
                ElseIf TextSearch.Text.ToLower = "delete" Then
                    Me.SetnGetValueSearch = "3"
                Else
                    Me.SetnGetValueSearch = Me.TextSearch.Text
                End If
            Else

                Me.SetnGetValueSearch = Me.TextSearch.Text

            End If
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If e.Item.Cells(5).Text = 1 Then
                    e.Item.Cells(5).Text = "Add"
                ElseIf e.Item.Cells(5).Text = 2 Then
                    e.Item.Cells(5).Text = "Edit"
                ElseIf e.Item.Cells(5).Text = 3 Then
                    e.Item.Cells(5).Text = "Delete"
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridMSUserView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim UserID As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(UserID) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not Me.SetnGetSelectedItem.Contains(UserID) Then
        '                    ArrTarget.Add(UserID)
        '                End If
        '            Else
        '                ArrTarget.Remove(UserID)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(UserID)
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()
        Try
            '  Dim Rows As New ArrayList

            'Dim strfilter As String = ""
            'Dim rowData As VList(Of vw_SuspiciusPerson)
            'For Each IdPk As Int32 In Me.SetnGetSelectedItem
            '    strfilter += IdPk & ","
            'Next
            'If strfilter.Length > 0 Then
            '    strfilter = strfilter.Substring(0, strfilter.Length - 1)
            '    rowData = Me.SetnGetBindTable(vw_SuspiciusPersonColumn.PK_SuspiciusPerson_ID.ToString & " in (" & strfilter & ")", "", 0, Integer.MaxValue)

            '    Me.GridMSUserView.DataSource = rowData
            '    Me.GridMSUserView.AllowPaging = False
            '    Me.GridMSUserView.DataBind()

            Dim strfilter As String = "0,"
            Dim rowData As TList(Of Suspicius_Approval)

            For Each IdPk As Int64 In Me.SetnGetSelectedItem
                strfilter += IdPk & ","
            Next
            If strfilter.Length > 0 Then
                strfilter = strfilter.Substring(0, strfilter.Length - 1)
                rowData = Me.SetnGetBindTable(Suspicius_ApprovalColumn.PK_Suspicius_Approval_ID.ToString & " in (" & strfilter & ")", "", 0, Integer.MaxValue)

                Me.GridMSUserView.DataSource = rowData
                Me.GridMSUserView.AllowPaging = False
                Me.GridMSUserView.DataBind()

                'Sembunyikan kolom ke 0, 1, 6 & 7 agar tidak ikut diekspor ke excel
                Me.GridMSUserView.Columns(0).Visible = False
                Me.GridMSUserView.Columns(1).Visible = False
                Me.GridMSUserView.Columns(6).Visible = False


            End If
            
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' export button 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"

            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=SuspiciusPersonApproval.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub BindExportAll()
        Try
            Dim strfilter As String = " UserID <> '" & Sahassa.AML.Commonly.SessionUserId & "' AND UserID IN (SELECT UserId FROM UserWorkingUnitAssignment WHERE WorkingUnitId IN (SELECT WorkingUnitId FROM UserWorkingUnitAssignment WHERE UserId = '" & Sahassa.AML.Commonly.SessionUserId & "'))"
            If Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")).Length > 0 Then
                strfilter = strfilter & " and " & Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''"))
            End If
            Me.GridMSUserView.DataSource = Me.SetnGetBindTable(strfilter, Me.SetnGetSort, 0, Int32.MaxValue)
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0, 1, 6 & 7 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(1).Visible = False
            Me.GridMSUserView.Columns(6).Visible = False
        Catch
            Throw
        End Try
    End Sub

    Protected Sub LnkBtnExportAllToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkBtnExportAllToExcel.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"

            Me.BindExportAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=SuspiciusPersonApproval.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
