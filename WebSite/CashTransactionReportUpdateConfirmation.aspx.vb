
Partial Class CashTransactionReportUpdateConfirmation
    Inherits System.Web.UI.Page

    Protected Sub page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.textbox_PKCTRID.Text = Request.QueryString("key").Trim
        Using TableAdapterDisplayData = New AMLDAL.CTRReportTableAdapters.pCTRDisplayUpdateTableAdapter
            Using DataTableDisplayData As AMLDAL.CTRReport.pCTRDisplayUpdateDataTable = TableAdapterDisplayData.getDisplayCTRData(Request.QueryString("key"))
                Dim DataTableDisplayDataRow As AMLDAL.CTRReport.pCTRDisplayUpdateRow
                DataTableDisplayDataRow = DataTableDisplayData.Rows(0)
                If Not DataTableDisplayDataRow Is Nothing Then
                    If Not DataTableDisplayDataRow.IsCaseDescNull Then
                        Me.textbox_CTRDescription.Text = DataTableDisplayDataRow.CaseDesc
                    End If
                    If Not DataTableDisplayDataRow.IsCreateDateNull Then
                        Me.textbox_CreatedDate.Text = DataTableDisplayDataRow.CreateDate
                    End If
                    If Not DataTableDisplayDataRow.IsLastUpdateNull Then
                        Me.textbox_LastUpdated.Text = DataTableDisplayDataRow.LastUpdate
                    End If
                    If Not DataTableDisplayDataRow.IsOwnerNameNull Then
                        Me.textbox_FKAccountOwnerName.Text = DataTableDisplayDataRow.OwnerName
                    End If
                    If Not DataTableDisplayDataRow.IsPICNull Then
                        Me.textbox_pic.Text = DataTableDisplayDataRow.PIC
                    End If
                    If Not DataTableDisplayDataRow.IsReportedDateNull Then
                        Me.textbox_ReportedDate.Text = DataTableDisplayDataRow.ReportedDate
                    End If
                    If Not DataTableDisplayDataRow.IsReportedByNull Then
                        Me.textbox_ReportedBy.Text = DataTableDisplayDataRow.ReportedBy
                    End If
                    If Not DataTableDisplayDataRow.IsPPATKConfirmationNoNull Then
                        Me.textbox_PPATKConfirmationNo.Text = DataTableDisplayDataRow.PPATKConfirmationNo
                    End If
                End If
            End Using
        End Using
    End Sub

    Protected Sub button_cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_cancel.Click
        Response.Redirect("CashTransactionReport.aspx", False)
    End Sub

    Protected Sub button_submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button_submit.Click
        Using TableAdapterUpdateConfirmation = New AMLDAL.CTRReportTableAdapters.updateConfirmationNoTableAdapter
            Me.textbox_ReportedBy.Text = Sahassa.AML.Commonly.SessionUserId.Trim
            Using DataTableUpdateConfirmation As AMLDAL.CTRReport.updateConfirmationNoDataTable = TableAdapterUpdateConfirmation.setconfirmationNo(Request.QueryString("key"), Me.textbox_PPATKConfirmationNo.Text.Trim, Me.textbox_ReportedBy.Text.Trim)
                'Update Confirmation Number
            End Using
        End Using
        Response.Redirect("CashTransactionReport.aspx", False)
    End Sub
End Class
