<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="LTKTTransactionEdit.aspx.vb" Inherits="LTKTTransactionEdit"     %>


<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script src="Script/popcalendar.js"></script>

    <script language="javascript" type="text/javascript">
      function hidePanel(objhide,objpanel,imgmin,imgmax)
      {
        document.getElementById(objhide).style.display='none';
        document.getElementById(objpanel).src=imgmax;
      }
      // JScript File

      function popWin2(txtKelurahan, hiddenField)
      {
      	var paramId;
      	var paramName;
      	var myargs = new Array(paramId,paramName);
      	var height = 480;
	    var width = 640;
        var left = (screen.availWidth - width)/2;
	    var top = (screen.availHeight - height)/2;
      	var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

      	var myargs = window.showModalDialog("PickerKelurahan.aspx", myargs, winSetting);
      	if(myargs == null) 
      	{
      		//window.alert("Nothing returns from Picker Kelurahan");
      	}else 
      	{
      		paramId = myargs[0].toString();
      		paramName = myargs[1].toString();

      		txtKelurahan.value = paramName;
      		hiddenField.value = paramId;
      	}
      }
      
      function popWinKecamatan(txtKecamatan, hiddenField)
      {
      	var paramId;
      	var paramName;
      	var myargs = new Array(paramId,paramName);
      	var height = 480;
	    var width = 640;
        var left = (screen.availWidth - width)/2;
	    var top = (screen.availHeight - height)/2;
      	var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

      	var myargs = window.showModalDialog("PickerKecamatan.aspx", myargs, winSetting);
      	if(myargs == null) 
      	{
      		//window.alert("Nothing returns from Picker Kelurahan");
      	}else 
      	{
      		paramId = myargs[0].toString();
      		paramName = myargs[1].toString();

      		txtKecamatan.value = paramName;
      		hiddenField.value = paramId;
      	}
      }
      function popWinNegara(txtNegara, hiddenField)
      {
      	var paramId;
      	var paramName;
      	var myargs = new Array(paramId,paramName);
      	var height = 480;
	    var width = 640;
        var left = (screen.availWidth - width)/2;
	    var top = (screen.availHeight - height)/2;
      	var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

      	var myargs = window.showModalDialog("PickerNegara.aspx", myargs, winSetting);
      	if(myargs == null) 
      	{
      		//window.alert("Nothing returns from Picker Kelurahan");
      	}else 
      	{
      		paramId = myargs[0].toString();
      		paramName = myargs[1].toString();

      		txtNegara.value = paramName;
      		hiddenField.value = paramId;
      	}
      }
      function popWinMataUang(txtMataUang, hiddenField)
      {
      	var paramId;
      	var paramName;
      	var myargs = new Array(paramId,paramName);
      	var height = 480;
	    var width = 640;
        var left = (screen.availWidth - width)/2;
	    var top = (screen.availHeight - height)/2;
      	var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

      	var myargs = window.showModalDialog("PickerMataUang.aspx", myargs, winSetting);
      	if(myargs == null) 
      	{
      		//window.alert("Nothing returns from Picker Kelurahan");
      	}else 
      	{
      		paramId = myargs[0].toString();
      		paramName = myargs[1].toString();

      		txtMataUang.value = paramId + ' - ' +paramName;
      		hiddenField.value = paramId;
      	}
      }
      function popWinProvinsi(txtProvinsi, hiddenField)
      {
      	var paramId;
      	var paramName;
      	var myargs = new Array(paramId,paramName);
      	var height = 480;
	    var width = 640;
        var left = (screen.availWidth - width)/2;
	    var top = (screen.availHeight - height)/2;
      	var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

      	var myargs = window.showModalDialog("PickerProvinsi.aspx", myargs, winSetting);
      	if(myargs == null) 
      	{
      		//window.alert("Nothing returns from Picker Kelurahan");
      	}else 
      	{
      		paramId = myargs[0].toString();
      		paramName = myargs[1].toString();

      		txtProvinsi.value = paramName;
      		hiddenField.value = paramId;
      	}
      }
      function popWinPekerjaan(txtPekerjaan, hiddenField)
      {
      	var paramId;
      	var paramName;
      	var myargs = new Array(paramId,paramName);
      	var height = 480;
	    var width = 640;
        var left = (screen.availWidth - width)/2;
	    var top = (screen.availHeight - height)/2;
      	var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

      	var myargs = window.showModalDialog("PickerPekerjaan.aspx", myargs, winSetting);
      	if(myargs == null) 
      	{
      		//window.alert("Nothing returns from Picker Kelurahan");
      	}else 
      	{
      		paramId = myargs[0].toString();
      		paramName = myargs[1].toString();

      		txtPekerjaan.value = paramName;
      		hiddenField.value = paramId;
      	}
      }
      function popWinKotaKab(txtKotaKab, hiddenField)
      {
      	var paramId;
      	var paramName;
      	var myargs = new Array(paramId,paramName);
      	var height = 480;
	    var width = 640;
        var left = (screen.availWidth - width)/2;
	    var top = (screen.availHeight - height)/2;
      	var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

      	var myargs = window.showModalDialog("PickerKotaKab.aspx", myargs, winSetting);
      	if(myargs == null) 
      	{
      		//window.alert("Nothing returns from Picker Kelurahan");
      	}else 
      	{
      		paramId = myargs[0].toString();
      		paramName = myargs[1].toString();

      		txtKotaKab.value = paramName;
      		hiddenField.value = paramId;
      	}
      }
      function popWinBidangUsaha(txtBidangUsaha, hiddenField)
      {
      	var paramId;
      	var paramName;
      	var myargs = new Array(paramId,paramName);
      	var height = 480;
	    var width = 640;
        var left = (screen.availWidth - width)/2;
	    var top = (screen.availHeight - height)/2;
      	var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

      	var myargs = window.showModalDialog("PickerBidangUsaha.aspx", myargs, winSetting);
      	if(myargs == null) 
      	{
      		//window.alert("Nothing returns from Picker Kelurahan");
      	}else 
      	{
      		paramId = myargs[0].toString();
      		paramName = myargs[1].toString();

      		txtBidangUsaha.value = paramName;
      		hiddenField.value = paramId;
      	}
      }
    </script>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                            </td>
                        <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td bgcolor="#ffffff" class="divcontentinside" colspan="2" style="width: 694px">
                                <img src="Images/blank.gif" width="20" height="100%" /><ajax:AjaxPanel ID="a" runat="server"><asp:ValidationSummary
                                    ID="ValidationSummary1" runat="server" HeaderText="There were uncompleteness on the page:"
                                    Width="95%" CssClass="validation" ShowMessageBox="True"></asp:ValidationSummary>
                                </ajax:AjaxPanel>
                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" border="0"
                                    style="border-top-style: none; border-right-style: none; border-left-style: none;
                                    border-bottom-style: none">
                                    <tr>
                                        <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                            <img src="Images/dot_title.gif" width="17" height="17">
                                            <strong>
                                                <asp:Label ID="Label1" runat="server" Text="LTKT - Edit"></asp:Label></strong>
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                            <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                                <asp:Label ID="LblSucces" CssClass="validationok" Width="94%" runat="server" Visible="False"></asp:Label>
                                            </ajax:AjaxPanel>
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                                <ajax:AjaxPanel ID="ajaxMultiview" runat="server">
                                <asp:MultiView runat="server" ID="mtvPage" ActiveViewIndex="0">
                                    <asp:View runat="server" ID="vwTransaksi">
                                        <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" border="0"
                                            bgcolor="#dddddd">
                                            <tr>
                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                    style="height: 6px">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td class="formtext">
                                                                <asp:Label ID="Label8" runat="server" Text="A. UMUM" Font-Bold="True"></asp:Label>&nbsp;</td>
                                                            <td>
                                                                <a href="#" onclick="javascript:ShowHidePanel('Umum','searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                    title="click to minimize or maximize">
                                                                    <img id="searchimage2" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                        width="12px"></a></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="Umum">
                                                <td bgcolor="#ffffff" colspan="2" style="height: 10px">
                                                    <table>
                                                        <tr>
                                                            <td class="formtext">
                                                                <asp:Label ID="Label4" runat="server" Text="1. PIHAK PELAPOR" Font-Bold="True"></asp:Label>&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtext">
                                                                <table>
                                                                    <tr>
                                                                        <td style="width: 5px;">
                                                                        </td>
                                                                        <td class="formText">
                                                                            1.1. Nama PJK Pelapor <span style="color: #cc0000">*</span>
                                                                        </td>
                                                                        <td style="width: 5px;">
                                                                            :
                                                                        </td>
                                                                        <td class="formtext">
                                                                            <asp:TextBox runat="server" ID="txtUmumPJKPelapor" CssClass="textBox" Width="240px"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUmumPJKPelapor"
                                                                                ErrorMessage="Nama PJK Pelapor harus diisi">*</asp:RequiredFieldValidator>&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5px;">
                                                                        </td>
                                                                        <td class="formText">
                                                                            1.2. Tanggal Pelaporan <span style="color: #cc0000">*</span>
                                                                        </td>
                                                                        <td style="width: 5px;">
                                                                            :
                                                                        </td>
                                                                        <td class="formtext">
                                                                            <asp:TextBox ID="txtTglLaporan" runat="server" CssClass="textBox" MaxLength="50"
                                                                                Width="96px" Enabled="False"></asp:TextBox><input id="popUpTglLaporan" runat="server" onclick="popUpCalendar(this, frmBasicCTRWeb.txtTglLaporan, 'dd-mmm-yyyy')"
                                                                                    style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                                                    background-image: url(Script/Calendar/cal.gif); border-left: #ffffff 0px solid;
                                                                                    width: 16px; border-bottom: #ffffff 0px solid; height: 17px" title="Click to show calendar"
                                                                                    type="button" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTglLaporan"
                                                                                Display="None" ErrorMessage="Tanggal Pelaporan harus diisi">*</asp:RequiredFieldValidator></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5px;">
                                                                        </td>
                                                                        <td class="formText">
                                                                            1.3. Nama Pejabat PJK Pelapor <span style="color: #cc0000">*</span>
                                                                        </td>
                                                                        <td style="width: 5px;">
                                                                            :
                                                                        </td>
                                                                        <td class="formtext">
                                                                            <asp:TextBox runat="server" ID="txtPejabatPelapor" CssClass="textBox" Width="240px"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPejabatPelapor"
                                                                                Display="None" ErrorMessage="Nama Pejabat PJK Pelapor harus diisi">*</asp:RequiredFieldValidator></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtext">
                                                                <asp:Label ID="Label5" runat="server" Text="2. JENIS LAPORAN (PILIH SALAH SATU)"
                                                                    Font-Bold="True"></asp:Label>&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="formtext">
                                                                <ajax:AjaxPanel runat="server" ID="pnlUpdate">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="width: 5px;">
                                                                            </td>
                                                                            <td class="formText">
                                                                                2.1. Tipe Laporan <span style="color: #cc0000">*</span>
                                                                            </td>
                                                                            <td style="width: 5px;">
                                                                                :
                                                                            </td>
                                                                            <td class="formtext">
                                                                                <asp:DropDownList runat="server" ID="cboTipeLaporan" CssClass="combobox" AutoPostBack="True">
                                                                                    <asp:ListItem Value="0">Laporan Baru</asp:ListItem>
                                                                                    <asp:ListItem Value="1">Laporan Koreksi</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5px;">
                                                                            </td>
                                                                            <td class="formtext">
                                                                            </td>
                                                                            <td style="width: 5px;">
                                                                                &nbsp;</td>
                                                                            <td class="formtext">
                                                                                <table id="tblLTKTKoreksi" runat="server" visible="False">
                                                                                    <tr>
                                                                                        <td class="formText">
                                                                                            2.1.1. No. LTKT yang dikoreksi <span style="color: #cc0000">*</span>
                                                                                        </td>
                                                                                        <td style="width: 5px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td class="formtext">
                                                                                            <asp:TextBox runat="server" ID="txtNoLTKTKoreksi" CssClass="textBox"></asp:TextBox>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ajax:AjaxPanel>
                                                            </td>
                                                        </tr>                                                        
                                                    </table>
                                                    <table border="0">
                                                        <tr valign="top">
                                                            <td class="formtext" style="height: 45px">
                                                               Sebutkan informasi lainnya yang ada</td>
                                                            <td style="width: 2px; height: 45px">
                                                                :</td>
                                                            <td class="formtext" style="height: 45px">
                                                                <asp:TextBox ID="txtLTKTInformasiLainnya" runat="server" CssClass="textBox" Height="46px"
                                                                    TextMode="MultiLine" Width="257px"></asp:TextBox></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                    style="height: 6px">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td class="formtext">
                                                                <asp:Label ID="Label2" runat="server" Text="B. IDENTITAS TERLAPOR" Font-Bold="True"></asp:Label>&nbsp;</td>
                                                            <td>
                                                                <a href="#" onclick="javascript:ShowHidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                    title="click to minimize or maximize">
                                                                    <img id="Img1" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px"></a></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="IdTerlapor">
                                                <td bgcolor="#ffffff" colspan="2">
                                                    <ajax:AjaxPanel runat="server" ID="ajxPnlTerlapor" Width="100%">
                                                        <table width="100%">
                                                            <tr>
                                                                <td class="formtext">
                                                                    <asp:Label ID="Label6" runat="server" Text="3. TERLAPOR" Font-Bold="True"></asp:Label>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="formtext">
                                                                    <table>
                                                                        <tr>
                                                                            <td style="width: 5px;">
                                                                            </td>
                                                                            <td class="formtext">
                                                                                <strong>3.1. Kepemilikan</strong> <span style="color: #cc0000">*</span>
                                                                            </td>
                                                                            <td style="width: 5px;">
                                                                                :
                                                                            </td>
                                                                            <td class="formtext">
                                                                                <asp:DropDownList runat="server" ID="cboTerlaporKepemilikan" CssClass="combobox">
                                                                                </asp:DropDownList></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5px;">
                                                                            </td>
                                                                            <td class="formtext">
                                                                            </td>
                                                                            <td style="width: 5px;">
                                                                            </td>
                                                                            <td class="formtext">
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td class="formText" style="width: 139px">
                                                                                            <strong>3.1.1. No. Rekening</strong> <span style="color: #cc0000">*</span>&nbsp;</td>
                                                                                        <td style="width: 5px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td class="formtext">
                                                                                            <asp:TextBox runat="server" ID="txtTerlaporNoRekening" CssClass="textBox" Width="258px"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtTerlaporNoRekening"
                                                                                                ErrorMessage="No Rekening Harus Diisi">*</asp:RequiredFieldValidator></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="formText" >CIF No.</td>
                                                                                        <td class="formText" >:</td>
                                                                                        <td class="formText" >
                                                                                            <asp:TextBox ID="txtCIFNo" runat="server" CssClass="textBox" Enabled="false" ></asp:TextBox></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="formText" style="width: 139px">
                                                                                            Tipe Pelapor</td>
                                                                                        <td style="width: 5px">
                                                                                            :</td>
                                                                                        <td class="formtext">
                                                                                            <asp:RadioButtonList runat="server" ID="rblTerlaporTipePelapor" AutoPostBack="True"
                                                                                                RepeatDirection="Horizontal">
                                                                                                <asp:ListItem>Perorangan</asp:ListItem>
                                                                                                <asp:ListItem>Korporasi</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="formtext" colspan="3">
                                                                                            <div id="divPerorangan" runat="server" hidden="false">
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td class="formtext" colspan="3">
                                                                                                            <strong>3.1.2. Perorangan</strong>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            a. Gelar</td>
                                                                                                        <td style="width: 5px">
                                                                                                            :</td>
                                                                                                        <td class="formtext">
                                                                                                            <asp:TextBox runat="server" ID="txtTerlaporGelar" CssClass="textBox"></asp:TextBox></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            b. Nama Lengkap <span style="color: #cc0000">*</span></td>
                                                                                                        <td style="width: 5px">
                                                                                                            :</td>
                                                                                                        <td class="formtext">
                                                                                                            <asp:TextBox ID="txtTerlaporNamaLengkap" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            c. Tempat Lahir</td>
                                                                                                        <td style="width: 5px">
                                                                                                            :</td>
                                                                                                        <td class="formtext">
                                                                                                            <asp:TextBox ID="txtTerlaporTempatLahir" runat="server" CssClass="textBox" Width="240px"></asp:TextBox></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            d. Tanggal Lahir <span style="color: #cc0000">*</span></td>
                                                                                                        <td style="width: 5px">
                                                                                                            :</td>
                                                                                                        <td class="formtext">
                                                                                                            <br />
                                                                                                            <asp:TextBox ID="txtTerlaporTglLahir" runat="server" CssClass="textBox" MaxLength="50"
                                                                                                                Width="96px"></asp:TextBox><input id="popTglLahirTerlapor" runat="server" onclick="popUpCalendar(this, frmBasicCTRWeb.txtTglLahir, 'dd-mmm-yyyy')"
                                                                                                                    style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                                                                                    background-image: url(Script/Calendar/cal.gif); border-left: #ffffff 0px solid;
                                                                                                                    width: 16px; border-bottom: #ffffff 0px solid; height: 17px" title="Click to show calendar"
                                                                                                                    type="button" /></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            e. Kewarganegaraan (Pilih salah satu) <span style="color: #cc0000">*</span></td>
                                                                                                        <td style="width: 5px">
                                                                                                            :</td>
                                                                                                        <td class="formtext">
                                                                                                            <asp:RadioButtonList runat="server" ID="rblTerlaporKewarganegaraan" AutoPostBack="True"
                                                                                                                RepeatDirection="Horizontal">
                                                                                                                <asp:ListItem Selected="True">WNI</asp:ListItem>
                                                                                                                <asp:ListItem>WNA</asp:ListItem>
                                                                                                            </asp:RadioButtonList></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            f. Negara <span style="color: #cc0000">*</span></td>
                                                                                                        <td style="width: 5px">
                                                                                                            :</td>
                                                                                                        <td class="formtext">
                                                                                                            <asp:DropDownList runat="server" ID="cboTerlaporNegara" CssClass="combobox">
                                                                                                            </asp:DropDownList></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            g. Alamat Domisili</td>
                                                                                                        <td style="width: 5px">
                                                                                                        </td>
                                                                                                        <td class="formtext">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formtext" colspan="3">
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                        &nbsp; &nbsp;&nbsp;
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Nama Jalan</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :
                                                                                                                    </td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTerlaporDOMNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        RT/RW</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTerlaporDOMRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Kelurahan</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext" valign="top">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporDOMKelurahan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporDOMKelurahan" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporDOMKelurahan" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Kecamatan</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        &nbsp;<table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporDOMKecamatan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporDOMKecamatan" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporDOMKecamatan" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Kota / Kabupaten<span style="color: #cc0000">*</span></td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporDOMKotaKab" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporDOMKotaKab" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporDOMKotaKab" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Kode Pos</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTerlaporDOMKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Provinsi</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporDOMProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporDOMProvinsi" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporDOMProvinsi" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            h. Alamat Sesuai Bukti Identitas</td>
                                                                                                        <td style="width: 5px">
                                                                                                            :</td>
                                                                                                        <td class="formtext">
                                                                                                            <asp:CheckBox ID="chkCopyAlamatDOM" runat="server" Text="Sama dengan Alamat Domisili" AutoPostBack="True" /></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formtext" colspan="3">
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td class="formText">
                                                                                                                        &nbsp; &nbsp;&nbsp;
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Nama Jalan</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :
                                                                                                                    </td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTerlaporIDNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formText">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        RT/RW</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTerlaporIDRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formText">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Kelurahan</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext" valign="top">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporIDKelurahan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporIDKelurahan" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporIDKelurahan" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formText">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Kecamatan</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        &nbsp;<table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporIDKecamatan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporIDKecamatan" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporIDKecamatan" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formText">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Kota / Kabupaten<span style="color: #cc0000">*</span></td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporIDKotaKab" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporIDKotaKab" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporIDKotaKab" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formText">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Kode Pos</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTerlaporIDKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formText">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Provinsi</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td style="height: 22px">
                                                                                                                                    <asp:TextBox ID="txtTerlaporIDProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="height: 22px">
                                                                                                                                    <asp:ImageButton ID="imgTerlaporIDProvinsi" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporIDProvinsi" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            i. Alamat Sesuai Negara Asal</td>
                                                                                                        <td style="width: 5px">
                                                                                                        </td>
                                                                                                        <td>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formtext" colspan="3">
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td class="formText">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Nama Jalan</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :
                                                                                                                    </td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTerlaporNANamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formText">
                                                                                                                        &nbsp; &nbsp;&nbsp;
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Negara</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporNANegara" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporIDNegara" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporIDNegara" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formText">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Provinsi</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext" valign="top">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporNAProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporNAProvinsi" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporNAProvinsi" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formText">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Kota
                                                                                                                    </td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporNAKota" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporIDKota" runat="server" CausesValidation="False" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporIDKota" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formText">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Kode Pos<span style="color: #cc0000">*</span></td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTerlaporNAKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            j. Jenis Dokumen Identitas<span style="color: #cc0000">*</span></td>
                                                                                                        <td style="width: 5px">
                                                                                                            :</td>
                                                                                                        <td class="formtext">
                                                                                                            <ajax:AjaxPanel runat="server" ID="ajPnlDokId">
                                                                                                                <asp:DropDownList runat="server" ID="cboTerlaporJenisDocID" CssClass="combobox">
                                                                                                                </asp:DropDownList>
                                                                                                            </ajax:AjaxPanel>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formtext" colspan="3" runat="server" id="tdNomorId">
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td class="formText">
                                                                                                                        Nomor Identitas <span style="color: #cc0000">*</span></td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext" style="width: 260px">
                                                                                                                        <asp:TextBox ID="txtTerlaporNomorID" runat="server" CssClass="textBox" 
                                                                                                                            Width="257px"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            k. Nomor Pokok Wajib Pajak (NPWP)</td>
                                                                                                        <td style="width: 5px">
                                                                                                            :</td>
                                                                                                        <td class="formtext">
                                                                                                            <asp:TextBox ID="txtTerlaporNPWP" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            l. Pekerjaan</td>
                                                                                                        <td style="width: 5px">
                                                                                                            :</td>
                                                                                                        <td class="formtext">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formtext" colspan="3">
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                        &nbsp; &nbsp;&nbsp;
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Pekerjaan<span style="color: #cc0000">*</span></td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext" style="width: 260px">
                                                                                                                        <table border="0">
                                                                                                                            <tr>
                                                                                                                                <td><asp:TextBox ID="txtTerlaporPekerjaan" runat="server" CssClass="textBox" Width="257px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td><asp:ImageButton ID="imgTerlaporPekerjaan" runat="server" ImageUrl="~/Images/button/browse.gif" CausesValidation="False" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporPekerjaan" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Jabatan</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext" style="width: 260px">
                                                                                                                        <asp:TextBox ID="txtTerlaporJabatan" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Penghasilan rata-rata/th (Rp)</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext" style="width: 260px">
                                                                                                                        <asp:TextBox ID="txtTerlaporPenghasilanRataRata" runat="server" CssClass="textBox"
                                                                                                                            Width="257px"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Tempat kerja</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext" style="width: 260px">
                                                                                                                        <asp:TextBox ID="txtTerlaporTempatKerja" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                            <div id="divKorporasi" runat="server" hidden="true">
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td class="formtext" colspan="3">
                                                                                                            <strong>3.1.3. Korporasi</strong></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            a. Bentuk Badan Usaha<span style="color: #cc0000">*</span></td>
                                                                                                        <td style="width: 5px">
                                                                                                            :</td>
                                                                                                        <td class="formtext">
                                                                                                            <asp:DropDownList ID="cboTerlaporCORPBentukBadanUsaha" runat="server">
                                                                                                            </asp:DropDownList></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            b. Nama Korporasi <span style="color: #cc0000">*</span></td>
                                                                                                        <td style="width: 5px">
                                                                                                            :</td>
                                                                                                        <td class="formtext">
                                                                                                            <asp:TextBox ID="txtTerlaporCORPNama" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            c. Bidang Usaha Korporasi<span style="color: #cc0000">*</span></td>
                                                                                                        <td style="width: 5px">
                                                                                                            :</td>
                                                                                                        <td class="formtext">
                                                                                                            <table style="width: 127px">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="txtTerlaporCORPBidangUsaha" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                    <td>
                                                                                                                        <asp:ImageButton ID="imgTerlaporCORPBidangUsaha" runat="server" CausesValidation="False"
                                                                                                                            ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                            <asp:HiddenField ID="hfTerlaporCORPBidangUsaha" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            d. Alamat Korporasi<span style="color: #cc0000">*</span></td>
                                                                                                        <td style="width: 5px">
                                                                                                            :</td>
                                                                                                        <td class="formtext">
                                                                                                            <asp:RadioButtonList runat="server" ID="rblTerlaporCORPTipeAlamat" AutoPostBack="True"
                                                                                                                RepeatDirection="Horizontal">
                                                                                                                <asp:ListItem>Dalam Negeri</asp:ListItem>
                                                                                                                <asp:ListItem>Luar Negeri</asp:ListItem>
                                                                                                            </asp:RadioButtonList></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            e. Alamat Lengkap Korporasi</td>
                                                                                                        <td style="width: 5px">
                                                                                                        </td>
                                                                                                        <td class="formtext">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formtext" colspan="3">
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                        &nbsp; &nbsp;&nbsp;
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Nama Jalan</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :
                                                                                                                    </td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTerlaporCORPDLNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        RT/RW</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTerlaporCORPDLRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Kelurahan</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext" valign="top">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporCORPDLKelurahan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporCORPDLKelurahan" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporCORPDLKelurahan" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Kecamatan</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        &nbsp;<table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporCORPDLKecamatan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporCORPDLKecamatan" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporCORPDLKecamatan" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Kota / Kabupaten<span style="color: #cc0000">*</span></td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporCORPDLKotaKab" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporCORPDLKotaKab" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporCORPDLKotaKab" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Kode Pos</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTerlaporCORPDLKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Provinsi</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporCORPDLProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporCORPDLProvinsi" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporCORPDLProvinsi" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText">
                                                                                                                        Negara</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporCORPDLNegara" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporCORPDLNegara" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporCORPDLNegara" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            i. Alamat Korporasi Luar Negeri</td>
                                                                                                        <td style="width: 5px">
                                                                                                        </td>
                                                                                                        <td class="formtext">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formtext" colspan="3">
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                        &nbsp; &nbsp;&nbsp;
                                                                                                                    </td>
                                                                                                                    <td class="formText" style="width: 69px">
                                                                                                                        Nama Jalan</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :
                                                                                                                    </td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTerlaporCORPLNNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText" style="width: 69px">
                                                                                                                        Negara</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporCORPLNNegara" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporCORPLNNegara" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporCORPLNNegara" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText" style="width: 69px">
                                                                                                                        Provinsi</td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext" valign="top">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td style="height: 22px">
                                                                                                                                    <asp:TextBox ID="txtTerlaporCORPLNProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td style="height: 22px">
                                                                                                                                    <asp:ImageButton ID="imgTerlaporCORPLNProvinsi" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporCORPLNProvinsi" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText" style="width: 69px">
                                                                                                                        Kota
                                                                                                                    </td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <table style="width: 127px">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtTerlaporCORPLNKota" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgTerlaporCORPLNKota" runat="server" CausesValidation="False"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                        <asp:HiddenField ID="hfTerlaporCORPLNKota" runat="server" />
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="formtext">
                                                                                                                    </td>
                                                                                                                    <td class="formText" style="width: 69px">
                                                                                                                        Kode Pos<span style="color: #cc0000">*</span></td>
                                                                                                                    <td style="width: 5px">
                                                                                                                        :</td>
                                                                                                                    <td class="formtext">
                                                                                                                        <asp:TextBox ID="txtTerlaporCORPLNKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="formText">
                                                                                                            k. Nomor Pokok Wajib Pajak (NPWP)</td>
                                                                                                        <td style="width: 5px">
                                                                                                            :</td>
                                                                                                        <td class="formtext">
                                                                                                            <asp:TextBox ID="txtTerlaporCORPNPWP" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ajax:AjaxPanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                    style="height: 6px">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td class="formtext">
                                                                <asp:Label ID="Label3" runat="server" Text="C. TRANSAKSI" Font-Bold="True"></asp:Label>&nbsp;</td>
                                                            <td>
                                                                <a href="#" onclick="javascript:ShowHidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                    title="click to minimize or maximize">
                                                                    <img id="Img2" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px"></a></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="Transaksi">
                                                <td bgcolor="#ffffff" colspan="2">
                                                    <ajax:AjaxPanel ID="AjaxPanel4" runat="server" Width="100%">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Menu ID="Menu1" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
                                                                        CssClass="tabs">
                                                                        <Items>
                                                                            <asp:MenuItem Text="Kas Masuk" Value="0" Selected="True"></asp:MenuItem>
                                                                            <asp:MenuItem Text="Kas Keluar" Value="1"></asp:MenuItem>
                                                                        </Items>
                                                                        <StaticSelectedStyle CssClass="selectedTab" />
                                                                        <StaticMenuItemStyle CssClass="tab" />
                                                                    </asp:Menu>
                                                                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                                                                        <asp:View ID="ViewKasMasuk" runat="server">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td class="formtext" style="width: 300px">
                                                                                        4.1. Tanggal Transaksi (tgl/bln/thn)<span style="color: #cc0000">*</span></td>
                                                                                    <td style="width: 2px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKMTanggalTrx" runat="server" CssClass="textBox" MaxLength="50"
                                                                                            Width="96px" Enabled="False"></asp:TextBox><input id="popTRXKMTanggalTrx" runat="server" onclick="popUpCalendar(this, frmBasicCTRWeb.txtTglLahir, 'dd-mmm-yyyy')"
                                                                                                style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                                                                background-image: url(Script/Calendar/cal.gif); border-left: #ffffff 0px solid;
                                                                                                width: 16px; border-bottom: #ffffff 0px solid; height: 17px" title="Click to show calendar"
                                                                                                type="button" />
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        4.2. Nama Kantor PJK tempat terjadinya transaksi</td>
                                                                                    <td style="width: 2px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <div>
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td class="formtext">
                                                                                                        &nbsp; &nbsp;&nbsp;
                                                                                                    </td>
                                                                                                    <td class="formtext">
                                                                                                        a. Nama Kantor<span style="color: #cc0000">*</span></td>
                                                                                                    <td style="width: 5px">
                                                                                                        :</td>
                                                                                                    <td class="formtext">
                                                                                                        <asp:TextBox ID="txtTRXKMNamaKantor" runat="server" CssClass="textBox" Width="370px"></asp:TextBox>
                                                                                                        &nbsp;</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="formtext">
                                                                                                    </td>
                                                                                                    <td class="formtext">
                                                                                                        b. Kota Kabupaten<span style="color: #cc0000">*</span></td>
                                                                                                    <td style="width: 5px">
                                                                                                        :</td>
                                                                                                    <td class="formtext">
                                                                                                        <table style="width: 127px">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="txtTRXKMKotaKab" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                <td>
                                                                                                                    <asp:ImageButton ID="imgTRXKMKotaKab" runat="server" CausesValidation="False" ImageUrl="~/Images/button/browse.gif"
                                                                                                                        SkinID="CancelButton" /></td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                        <asp:HiddenField ID="hfTRXKMKotaKab" runat="server" />
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="formtext">
                                                                                                    </td>
                                                                                                    <td class="formtext">
                                                                                                        c. Provinsi<span style="color: #cc0000">*</span></td>
                                                                                                    <td style="width: 5px">
                                                                                                        :</td>
                                                                                                    <td class="formtext">
                                                                                                        <table style="width: 251px">
                                                                                                            <tr>
                                                                                                                <td style="width: 170px; height: 15px">
                                                                                                                    <asp:TextBox ID="txtTRXKMProvinsi" runat="server" CssClass="textBox" Enabled="False"
                                                                                                                        Width="167px"></asp:TextBox></td>
                                                                                                                <td style="height: 15px">
                                                                                                                    <asp:ImageButton ID="imgTRXKMProvinsi" runat="server" ImageUrl="~/Images/button/browse.gif" /></td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                        <asp:HiddenField ID="hfTRXKMProvinsi" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        4.3. Detail Kas Masuk</td>
                                                                                    <td style="width: 2px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    a. Kas Masuk (Rp)</td>
                                                                                                <td style="width: 2px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMDetilKasMasuk" runat="server" CssClass="textBox" Width="167px" AutoPostBack="True"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    b. Kas Masuk Dalam valuta asing (Dapat > 1)</td>
                                                                                                <td style="width: 2px">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext" colspan="1">
                                                                                                </td>
                                                                                                <td class="formtext" colspan="3">
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                                &nbsp; &nbsp;&nbsp;
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                i. Mata Uang</td>
                                                                                                            <td style="width: 2px">
                                                                                                                :</td>
                                                                                                            <td class="formtext">
                                                                                                                <table style="width: 127px">
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <asp:TextBox ID="txtTRXKMDetilMataUang" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td>
                                                                                                                            <asp:ImageButton ID="imgTRXKMDetilMataUang" runat="server" CausesValidation="False"
                                                                                                                                ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                                <asp:HiddenField ID="hfTRXKMDetilMataUang" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                ii. Kurs Transaksi</td>
                                                                                                            <td style="width: 2px">
                                                                                                                :</td>
                                                                                                            <td class="formtext">
                                                                                                                <asp:TextBox ID="txtTRXKMDetailKursTrx" runat="server" CssClass="textBox" Width="167px"></asp:TextBox></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                iii. Jumlah&nbsp;</td>
                                                                                                            <td style="width: 2px">
                                                                                                                :</td>
                                                                                                            <td class="formtext">
                                                                                                                <asp:TextBox ID="txtTRXKMDetilJumlah" runat="server" CssClass="textBox" Width="167px"></asp:TextBox></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                            </td>
                                                                                                            <td class="formtext" colspan="3">
                                                                                                                <asp:ImageButton ID="imgTRXKMDetilAdd" runat="server" CausesValidation="False" ImageUrl="~/Images/button/add.gif"
                                                                                                                    SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                            </td>
                                                                                                            <td class="formtext" colspan="3">
                                                                                                                <asp:GridView runat="server" ID="grvTRXKMDetilValutaAsing" AutoGenerateColumns="False"
                                                                                                                    SkinID="grvTheme">
                                                                                                                    <Columns>
                                                                                                                        <asp:BoundField HeaderText="No" />
                                                                                                                        <asp:BoundField HeaderText="Mata Uang" DataField="MataUang" />
                                                                                                                        <asp:BoundField HeaderText="Kurs Transaksi" DataField="KursTransaksi" />
                                                                                                                        <asp:BoundField HeaderText="Jumlah" DataField="Jumlah" />
                                                                                                                        <asp:BoundField DataField="JumlahRp" HeaderText="Jumlah (Rp)" />
                                                                                                                        <asp:TemplateField>
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:LinkButton ID="BtngrvDetailValutaAsingDelete" runat="server" OnClick="BtngrvDetailValutaAsingDelete_Click" CausesValidation="False">Delete</asp:LinkButton>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateField>
                                                                                                                    </Columns>
                                                                                                                </asp:GridView>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    c. Total Kas Masuk (a+b) (Rp)</td>
                                                                                                <td style="width: 2px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:Label ID="lblTRXKMDetilValutaAsingJumlahRp" runat="server"></asp:Label></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        4.4. Nomor Rekening Nasabah</td>
                                                                                    <td style="width: 2px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKMNoRekening" runat="server" CssClass="textBox" Width="167px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" style="height: 15px">
                                                                                        4.5. Identitas pihak terkait dengan laporan</td>
                                                                                    <td style="width: 2px; height: 15px;">
                                                                                    </td>
                                                                                    <td class="formtext" style="height: 15px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        &nbsp; &nbsp; &nbsp; Tipe Pelapor</td>
                                                                                    <td style="width: 2px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:RadioButtonList runat="server" ID="rblTRXKMTipePelapor" AutoPostBack="True"
                                                                                            RepeatDirection="Horizontal">
                                                                                            <asp:ListItem Selected="True">Perorangan</asp:ListItem>
                                                                                            <asp:ListItem>Korporasi</asp:ListItem>
                                                                                        </asp:RadioButtonList></td>
                                                                                </tr>
                                                                            </table>
                                                                            <table id="tblTRXKMTipePelapor" runat="server">
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <strong>4.5.1. Perorangan</strong>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        a. Gelar</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKMINDVGelar" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        b. Nama Lengkap
                                                                                    </td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKMINDVNamaLengkap" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        c. Tempat Lahir</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKMINDVTempatLahir" runat="server" CssClass="textBox" Width="240px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        d. Tanggal Lahir
                                                                                    </td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <br />
                                                                                        <asp:TextBox ID="txtTRXKMINDVTanggalLahir" runat="server" CssClass="textBox" MaxLength="50"
                                                                                            Width="96px" Enabled="False"></asp:TextBox><input id="popTRXKMINDVTanggalLahir" runat="server" onclick="popUpCalendar(this, frmBasicCTRWeb.txtTglLahir, 'dd-mmm-yyyy')"
                                                                                                style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                                                                background-image: url(Script/Calendar/cal.gif); border-left: #ffffff 0px solid;
                                                                                                width: 16px; border-bottom: #ffffff 0px solid; height: 17px" title="Click to show calendar"
                                                                                                type="button" /></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        e. Kewarganegaraan
                                                                                    </td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:RadioButtonList runat="server" ID="rblTRXKMINDVKewarganegaraan" AutoPostBack="True"
                                                                                            RepeatDirection="Horizontal">
                                                                                            <asp:ListItem Selected="True">WNI</asp:ListItem>
                                                                                            <asp:ListItem>WNA</asp:ListItem>
                                                                                        </asp:RadioButtonList></td>
                                                                                </tr>
                                                                                <tr style="color: #cc0000">
                                                                                    <td class="formtext">
                                                                                        f. Negara
                                                                                    </td>
                                                                                    <td style="width: 5px; color: #000000;">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:DropDownList runat="server" ID="cboTRXKMINDVNegara" CssClass="combobox">
                                                                                        </asp:DropDownList></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        g. Alamat Domisili</td>
                                                                                    <td style="width: 5px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Nama Jalan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMINDVDOMNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    RT/RW</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMINDVDOMRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kelurahan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" valign="top">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKMINDVDOMKelurahan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKMINDVDOMKelurahan" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMINDVDOMKelurahan" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kecamatan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    &nbsp;<table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKMINDVDOMKecamatan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKMINDVDOMKecamatan" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMINDVDOMKecamatan" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kota / Kabupaten</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKMINDVDOMKotaKab" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKMINDVDOMKotaKab" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMINDVDOMKotaKab" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kode Pos</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMINDVDOMKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Provinsi</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKMINDVDOMProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKMINDVDOMProvinsi" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMINDVDOMProvinsi" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        h. Alamat Sesuai Bukti Identitas</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:CheckBox ID="chkTRXKMINDVCopyDOM" runat="server" Text="Sama dengan Alamat Domisili" AutoPostBack="True" /></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Nama Jalan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMINDVIDNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    RT/RW</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMINDVIDRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kelurahan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" valign="top">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKMINDVIDKelurahan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKMINDVIDKelurahan" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMINDVIDKelurahan" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kecamatan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    &nbsp;<table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td style="height: 23px">
                                                                                                                <asp:TextBox ID="txtTRXKMINDVIDKecamatan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td style="height: 23px">
                                                                                                                <asp:ImageButton ID="imgTRXKMINDVIDKecamatan" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMINDVIDKecamatan" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kota / Kabupaten</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKMINDVIDKotaKab" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKMINDVIDKotaKab" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMINDVIDKotaKab" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kode Pos</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMINDVIDKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Provinsi</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKMINDVIDProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKMINDVIDProvinsi" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" />&nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMINDVIDProvinsi" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        i. Alamat Sesuai Negara Asal</td>
                                                                                    <td style="width: 5px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Nama Jalan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMINDVNANamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Negara</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td style="height: 22px">
                                                                                                                <asp:TextBox ID="txtTRXKMINDVNANegara" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td style="height: 22px">
                                                                                                                <asp:ImageButton ID="imgTRXKMINDVNANegara" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMINDVNANegara" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Provinsi</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" valign="top">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKMINDVNAProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKMINDVNAProvinsi" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMINDVNAProvinsi" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kota
                                                                                                </td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKMINDVNAKota" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKMINDVNAKota" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMINDVNAKota" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kode Pos</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMINDVNAKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        j. Jenis Dokumen Identitas</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <ajax:AjaxPanel runat="server" ID="AjaxPanel2">
                                                                                            <asp:DropDownList runat="server" ID="cboTRXKMINDVJenisID" CssClass="combobox">
                                                                                            </asp:DropDownList>
                                                                                        </ajax:AjaxPanel>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3" runat="server" id="Td1">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                  Nomor Identitas <span style="color: #cc0000">*</span></td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" style="width: 260px">
                                                                                                    <asp:TextBox ID="txtTRXKMINDVNomorID" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        k. Nomor Pokok Wajib Pajak (NPWP)</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKMINDVNPWP" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        l. Pekerjaan</td>
                                                                                    <td style="width: 5px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext" style="height: 40px">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext" style="height: 40px">
                                                                                                    Pekerjaan</td>
                                                                                                <td style="width: 5px; height: 40px;">
                                                                                                    :</td>
                                                                                                <td class="formtext" style="width: 260px; height: 40px;">
                                                                                                    <table border="0">
                                                                                                        <tr>
                                                                                                            <td><asp:TextBox ID="txtTRXKMINDVPekerjaan" runat="server" CssClass="textBox" Width="257px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td><asp:ImageButton
                                                                                                        ID="imgTRXKMINDVPekerjaan" runat="server" CausesValidation="False" ImageUrl="~/Images/button/browse.gif" /></td>
                                                                                                        </tr>                                                                                                        
                                                                                                    </table>                                                                                                    
                                                                                                    <asp:HiddenField ID="hfTRXKMINDVPekerjaan" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Jabatan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" style="width: 260px">
                                                                                                    <asp:TextBox ID="txtTRXKMINDVJabatan" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Penghasilan rata-rata/th (Rp)</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" style="width: 260px">
                                                                                                    <asp:TextBox ID="txtTRXKMINDVPenghasilanRataRata" runat="server" CssClass="textBox"
                                                                                                        Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Tempat kerja</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" style="width: 260px">
                                                                                                    <asp:TextBox ID="txtTRXKMINDVTempatKerja" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        m. Tujuan Transaksi</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKMINDVTujuanTrx" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        n. Sumber Dana</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKMINDVSumberDana" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
                                                                                    <td style="width: 5px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    ii. Nama Bank Lain</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMINDVNamaBankLain" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    ii. Nomor Rekening Tujuan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMINDVNoRekeningTujuan" runat="server" CssClass="textBox"
                                                                                                        Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table id="tblTRXKMTipePelaporKorporasi" runat="server">
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <strong>4.5.2. Korporasi</strong></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" style="height: 27px">
                                                                                        a. Bentuk Badan Usaha</td>
                                                                                    <td style="width: 5px; height: 27px;">
                                                                                        :</td>
                                                                                    <td class="formtext" style="height: 27px">
                                                                                        &nbsp; &nbsp;
                                                                                        <asp:DropDownList ID="cboTRXKMCORPBentukBadanUsaha" runat="server">
                                                                                        </asp:DropDownList></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        b. Nama Korporasi
                                                                                    </td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKMCORPNama" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        c. Bidang Usaha Korporasi</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <table style="width: 127px">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="txtTRXKMCORPBidangUsaha" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                <td>
                                                                                                    <asp:ImageButton ID="imgTRXKMCORPBidangUsaha" runat="server" CausesValidation="False"
                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <asp:HiddenField ID="hfTRXKMCORPBidangUsaha" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        d. Alamat Korporasi</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:RadioButtonList runat="server" ID="rblTRXKMCORPTipeAlamat" AutoPostBack="True"
                                                                                            RepeatDirection="Horizontal">
                                                                                            <asp:ListItem Selected="True">Dalam Negeri</asp:ListItem>
                                                                                            <asp:ListItem>Luar Negeri</asp:ListItem>
                                                                                        </asp:RadioButtonList></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        e. Alamat Lengkap Korporasi</td>
                                                                                    <td style="width: 5px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Nama Jalan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMCORPDLNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    RT/RW</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMCORPDLRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kelurahan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" valign="top">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td style="height: 22px">
                                                                                                                <asp:TextBox ID="txtTRXKMCORPDLKelurahan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td style="height: 22px">
                                                                                                                <asp:ImageButton ID="imgTRXKMCORPDLKelurahan" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMCORPDLKelurahan" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kecamatan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    &nbsp;<table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKMCORPDLKecamatan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKMCORPDLKecamatan" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMCORPDLKecamatan" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kota / Kabupaten</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKMCORPDLKotaKab" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKMCORPDLKotaKab" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMCORPDLKotaKab" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kode Pos</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMCORPDLKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Provinsi</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td style="height: 22px">
                                                                                                                <asp:TextBox ID="txtTRXKMCORPDLProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td style="height: 22px">
                                                                                                                <asp:ImageButton ID="imgTRXKMCORPDLProvinsi" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMCORPDLProvinsi" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Negara</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKMCORPDLNegara" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKMCORPDLNegara" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMCORPDLNegara" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        i. Alamat Korporasi Luar Negeri</td>
                                                                                    <td style="width: 5px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Nama Jalan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMCORPLNNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Negara</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKMCORPLNNegara" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKMCORPLNNegara" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMCORPLNNegara" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Provinsi</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" valign="top">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKMCORPLNProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKMCORPLNProvinsi" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMCORPLNProvinsi" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kota
                                                                                                </td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKMCORPLNKota" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKMCORPLNKota" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKMCORPLNKota" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kode Pos</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMCORPLNKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        k. Nomor Pokok Wajib Pajak (NPWP)</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKMCORPNPWP" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        l. Tujuan Transaksi</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKMCORPTujuanTrx" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        m. Sumber Dana</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKMCORPSumberDana" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
                                                                                    <td style="width: 5px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    ii. Nama Bank Lain</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMCORPNamaBankLain" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    ii. Nomor Rekening Tujuan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKMCORPNoRekeningTujuan" runat="server" CssClass="textBox"
                                                                                                        Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:View>
                                                                        <asp:View ID="ViewKasKeluar" runat="server">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td class="formtext" style="width: 300px">
                                                                                        5.1. Tanggal Transaksi (tgl/bln/thn)<span style="color: #cc0000">*</span></td>
                                                                                    <td style="width: 2px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKKTanggalTransaksi" runat="server" CssClass="textBox" MaxLength="50"
                                                                                            Width="96px" Enabled="False"></asp:TextBox><input id="popTRXKKTanggalTransaksi" runat="server" onclick="popUpCalendar(this, frmBasicCTRWeb.txtTglLahir, 'dd-mmm-yyyy')"
                                                                                                style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                                                                background-image: url(Script/Calendar/cal.gif); border-left: #ffffff 0px solid;
                                                                                                width: 16px; border-bottom: #ffffff 0px solid; height: 17px" title="Click to show calendar"
                                                                                                type="button" />
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" style="height: 27px">
                                                                                        5.2. Nama Kantor PJK tempat terjadinya transaksi</td>
                                                                                    <td style="width: 2px; height: 27px;">
                                                                                    </td>
                                                                                    <td class="formtext" style="height: 27px">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <div>
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td class="formtext">
                                                                                                        &nbsp; &nbsp;&nbsp;
                                                                                                    </td>
                                                                                                    <td class="formtext">
                                                                                                        a. Nama Kantor<span style="color: #cc0000">*</span></td>
                                                                                                    <td style="width: 5px">
                                                                                                        :</td>
                                                                                                    <td class="formtext">
                                                                                                        <asp:TextBox ID="txtTRXKKNamaKantor" runat="server" CssClass="textBox" Width="370px"></asp:TextBox>
                                                                                                        &nbsp;</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="formtext">
                                                                                                    </td>
                                                                                                    <td class="formtext">
                                                                                                        b. Kota Kabupaten<span style="color: #cc0000">*</span></td>
                                                                                                    <td style="width: 5px">
                                                                                                        :</td>
                                                                                                    <td class="formtext">
                                                                                                        <table style="width: 127px">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="txtTRXKKKotaKab" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                <td>
                                                                                                                    <asp:ImageButton ID="imgTRXKKKotaKab" runat="server" CausesValidation="False" ImageUrl="~/Images/button/browse.gif"
                                                                                                                        SkinID="CancelButton" /></td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                        <asp:HiddenField ID="hfTRXKKKotaKab" runat="server" />
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="formtext">
                                                                                                    </td>
                                                                                                    <td class="formtext">
                                                                                                        c. Provinsi<span style="color: #cc0000">*</span></td>
                                                                                                    <td style="width: 5px">
                                                                                                        :</td>
                                                                                                    <td class="formtext">
                                                                                                        <table style="width: 127px">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="txtTRXKKProvinsi" runat="server" CssClass="textBox" Width="167px"
                                                                                                                        Enabled="False"></asp:TextBox></td>
                                                                                                                <td>
                                                                                                                    <asp:ImageButton ID="imgTRXKKProvinsi" runat="server" CausesValidation="False" ImageUrl="~/Images/button/browse.gif"
                                                                                                                        SkinID="CancelButton" /></td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                        <asp:HiddenField ID="hfTRXKKProvinsi" runat="server" />
                                                                                                        &nbsp;
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        5.3. Detail Kas Keluar</td>
                                                                                    <td style="width: 2px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    a. Kas Keluar (Rp)</td>
                                                                                                <td style="width: 2px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKDetailKasKeluar" runat="server" CssClass="textBox" Width="167px" AutoPostBack="True"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    b. Kas Keluar Dalam valuta asing (Dapat &gt; 1)</td>
                                                                                                <td style="width: 2px">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext" colspan="3">
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                                &nbsp; &nbsp;&nbsp;
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                i. Mata Uang</td>
                                                                                                            <td style="width: 2px">
                                                                                                                :</td>
                                                                                                            <td class="formtext">
                                                                                                                <table style="width: 127px">
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <asp:TextBox ID="txtTRXKKDetilMataUang" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                                        <td>
                                                                                                                            <asp:ImageButton ID="imgTRXKKDetilMataUang" runat="server" CausesValidation="False"
                                                                                                                                ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                                <asp:HiddenField ID="hfTRXKKDetilMataUang" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                ii. Kurs Transaksi</td>
                                                                                                            <td style="width: 2px">
                                                                                                                :</td>
                                                                                                            <td class="formtext">
                                                                                                                <asp:TextBox ID="txtTRXKKDetilKursTrx" runat="server" CssClass="textBox" Width="167px"></asp:TextBox></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                            </td>
                                                                                                            <td class="formtext">
                                                                                                                iii. Jumlah&nbsp;</td>
                                                                                                            <td style="width: 2px">
                                                                                                                :</td>
                                                                                                            <td class="formtext">
                                                                                                                <asp:TextBox ID="txtTRXKKDetilJumlah" runat="server" CssClass="textBox" Width="167px"></asp:TextBox></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                            </td>
                                                                                                            <td class="formtext" colspan="3">
                                                                                                                <asp:ImageButton ID="imgTRXKKDetilAdd" runat="server" CausesValidation="False" ImageUrl="~/Images/button/add.gif"
                                                                                                                    SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="formtext">
                                                                                                            </td>
                                                                                                            <td class="formtext" colspan="3">
                                                                                                                <asp:GridView runat="server" ID="grvDetilKasKeluar" AutoGenerateColumns="False" SkinID="grv2"
                                                                                                                    Width="290px">
                                                                                                                    <Columns>
                                                                                                                        <asp:BoundField HeaderText="No" />
                                                                                                                        <asp:BoundField HeaderText="Mata Uang" DataField="MataUang" />
                                                                                                                        <asp:BoundField HeaderText="Kurs Transaksi" DataField="KursTransaksi" />
                                                                                                                        <asp:BoundField HeaderText="Jumlah" DataField="Jumlah" />
                                                                                                                        <asp:BoundField DataField="JumlahRp" HeaderText="Jumlah (Rp)" />
                                                                                                                        <asp:TemplateField>
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:LinkButton ID="BtnDeletegrvDetilKasKeluar" runat="server" OnClick="BtnDeletegrvDetilKasKeluar_Click" CausesValidation="False">Delete</asp:LinkButton>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateField>
                                                                                                                    </Columns>
                                                                                                                </asp:GridView>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    c. Total Kas Keluar (a+b) (Rp)</td>
                                                                                                <td style="width: 2px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:Label ID="lblDetilKasKeluarJumlahRp" runat="server"></asp:Label></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        5.4. Identitas pihak terkait dengan laporan</td>
                                                                                    <td style="width: 2px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" style="height: 41px">
                                                                                        &nbsp; &nbsp; &nbsp; Tipe Pelapor</td>
                                                                                    <td style="width: 2px; height: 41px;">
                                                                                        :</td>
                                                                                    <td class="formtext" style="height: 41px">
                                                                                        <asp:RadioButtonList runat="server" ID="rblTRXKKTipePelapor" AutoPostBack="True"
                                                                                            RepeatDirection="Horizontal">
                                                                                            <asp:ListItem Selected="True">Perorangan</asp:ListItem>
                                                                                            <asp:ListItem>Korporasi</asp:ListItem>
                                                                                        </asp:RadioButtonList></td>
                                                                                </tr>
                                                                            </table>
                                                                            <table id="tblTRXKKTipePelaporPerorangan" runat="server">
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <strong>5.4.1. Perorangan</strong>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        a. Gelar</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKKINDVGelar" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        b. Nama Lengkap
                                                                                    </td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKKINDVNamaLengkap" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        c. Tempat Lahir</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKKINDVTempatLahir" runat="server" CssClass="textBox" Width="240px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        d. Tanggal Lahir
                                                                                    </td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <br />
                                                                                        <asp:TextBox ID="txtTRXKKINDVTglLahir" runat="server" CssClass="textBox" MaxLength="50"
                                                                                            Width="96px" Enabled="False"></asp:TextBox><input id="popTRXKKINDVTglLahir" runat="server" onclick="popUpCalendar(this, frmBasicCTRWeb.txtTglLahir, 'dd-mmm-yyyy')"
                                                                                                style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                                                                background-image: url(Script/Calendar/cal.gif); border-left: #ffffff 0px solid;
                                                                                                width: 16px; border-bottom: #ffffff 0px solid; height: 17px" title="Click to show calendar"
                                                                                                type="button" /></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        e. Kewarganegaraan
                                                                                    </td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:RadioButtonList runat="server" ID="rblTRXKKINDVKewarganegaraan" AutoPostBack="True"
                                                                                            RepeatDirection="Horizontal">
                                                                                            <asp:ListItem Selected="True">WNI</asp:ListItem>
                                                                                            <asp:ListItem>WNA</asp:ListItem>
                                                                                        </asp:RadioButtonList></td>
                                                                                </tr>
                                                                                <tr style="color: #cc0000">
                                                                                    <td class="formtext">
                                                                                        f. Negara
                                                                                    </td>
                                                                                    <td style="width: 5px; color: #000000;">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:DropDownList runat="server" ID="cboTRXKKINDVNegara" CssClass="combobox">
                                                                                        </asp:DropDownList></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        g. Alamat Domisili</td>
                                                                                    <td style="width: 5px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Nama Jalan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKINDVDOMNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    RT/RW</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKINDVDOMRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kelurahan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" valign="top">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKINDVDOMKelurahan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKINDVDOMKelurahan" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKINDVDOMKelurahan" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kecamatan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    &nbsp;<table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKINDVDOMKecamatan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKINDVDOMKecamatan" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKINDVDOMKecamatan" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kota / Kabupaten</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKINDVDOMKotaKab" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKINDVDOMKotaKab" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKINDVDOMKotaKab" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kode Pos</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKINDVDOMKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Provinsi</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKINDVDOMProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKINDVDOMProvinsi" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKINDVDOMProvinsi" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        h. Alamat Sesuai Bukti Identitas</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:CheckBox ID="chkTRXKKINDVIDCopyDOM" runat="server" Text="Sama dengan Alamat Domisili" AutoPostBack="True" /></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Nama Jalan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKINDVIDNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    RT/RW</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKINDVIDRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kelurahan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" valign="top">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKINDVIDKelurahan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKINDVIDKelurahan" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKINDVIDKelurahan" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kecamatan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    &nbsp;<table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKINDVIDKecamatan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKINDVIDKecamatan" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKINDVIDKecamatan" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kota / Kabupaten</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKINDVIDKotaKab" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKINDVIDKotaKab" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKINDVIDKotaKab" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kode Pos</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKINDVIDKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Provinsi</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKINDVIDProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKINDVIDProvinsi" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKINDVIDProvinsi" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        i. Alamat Sesuai Negara Asal</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Nama Jalan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKINDVNANamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Negara</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKINDVNANegara" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKINDVNANegara" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                           
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKINDVNANegara" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Provinsi</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" valign="top">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKINDVNAProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKINDVNAProvinsi" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKINDVNAProvinsi" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kota
                                                                                                </td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKINDVNAKota" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKINDVNAKota" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKINDVNAKota" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kode Pos</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKINDVNAKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        j. Jenis Dokumen Identitas</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <ajax:AjaxPanel runat="server" ID="AjaxPanel3">
                                                                                            <asp:DropDownList runat="server" ID="cboTRXKKINDVJenisID" CssClass="combobox">
                                                                                            </asp:DropDownList>
                                                                                        </ajax:AjaxPanel>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3" runat="server" id="Td2">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                  Nomor Identitas <span style="color: #cc0000">*</span></td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" style="width: 260px">
                                                                                                    <asp:TextBox ID="txtTRXKKINDVNomorId" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        k. Nomor Pokok Wajib Pajak (NPWP)</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKKINDVNPWP" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        l. Pekerjaan</td>
                                                                                    <td style="width: 5px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Pekerjaan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" style="width: 260px">
                                                                                                <table border="0">
                                                                                                    <tr>
                                                                                                        <td><asp:TextBox ID="txtTRXKKINDVPekerjaan" runat="server" CssClass="textBox" Width="257px" Enabled="False"></asp:TextBox></td>
                                                                                                        <td><asp:ImageButton ID="imgTRXKKINDVPekerjaan" runat="server" ImageUrl="~/Images/button/browse.gif" CausesValidation="False" /></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKINDVPekerjaan" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Jabatan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" style="width: 260px">
                                                                                                    <asp:TextBox ID="txtTRXKKINDVJabatan" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Penghasilan rata-rata/th (Rp)</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" style="width: 260px">
                                                                                                    <asp:TextBox ID="txtTRXKKINDVPenghasilanRataRata" runat="server" CssClass="textBox"
                                                                                                        Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Tempat kerja</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" style="width: 260px">
                                                                                                    <asp:TextBox ID="txtTRXKKINDVTempatKerja" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        m. Tujuan Transaksi</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKKINDVTujuanTrx" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        n. Sumber Dana</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKKINDVSumberDana" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
                                                                                    <td style="width: 5px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    ii. Nama Bank Lain</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKINDVNamaBankLain" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    ii. Nomor Rekening Tujuan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKINDVNoRekTujuan" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table id="tblTRXKKTipePelaporKorporasi" runat="server">
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <strong>5.4.2. Korporasi</strong></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" style="height: 28px">
                                                                                        a. Bentuk Badan Usaha</td>
                                                                                    <td style="width: 5px; height: 28px;">
                                                                                        :</td>
                                                                                    <td class="formtext" style="height: 28px">
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:DropDownList ID="cboTRXKKCORPBentukBadanUsaha" runat="server">
                                                                                        </asp:DropDownList></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        b. Nama Korporasi
                                                                                    </td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKKCORPNama" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        c. Bidang Usaha Korporasi</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <table style="width: 127px">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="txtTRXKKCORPBidangUsaha" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                <td>
                                                                                                    <asp:ImageButton ID="imgTRXKKCORPBidangUsaha" runat="server" CausesValidation="False"
                                                                                                        ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <asp:HiddenField ID="hfTRXKKCORPBidangUsaha" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        d. Alamat Korporasi</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:RadioButtonList runat="server" ID="rblTRXKKCORPTipeAlamat" AutoPostBack="True"
                                                                                            RepeatDirection="Horizontal">
                                                                                            <asp:ListItem Selected="True">Dalam Negeri</asp:ListItem>
                                                                                            <asp:ListItem>Luar Negeri</asp:ListItem>
                                                                                        </asp:RadioButtonList></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        e. Alamat Lengkap Korporasi</td>
                                                                                    <td style="width: 5px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Nama Jalan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKCORPDLNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    RT/RW</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKCORPDLRTRW" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kelurahan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" valign="top">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKCORPDLKelurahan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKCORPDLKelurahan" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKCORPDLKelurahan" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kecamatan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    &nbsp;<table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKCORPDLKecamatan" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKCORPDLKecamatan" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKCORPDLKecamatan" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kota / Kabupaten</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKCORPDLKotaKab" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKCORPDLKotaKab" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKCORPDLKotaKab" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kode Pos</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKCORPDLKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Provinsi</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKCORPDLProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKCORPDLProvinsi" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKCORPDLProvinsi" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Negara</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKCORPDLNegara" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKCORPDLNegara" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKCORPDLNegara" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        i. Alamat Korporasi Luar Negeri</td>
                                                                                    <td style="width: 5px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Nama Jalan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKCORPLNNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Negara</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKCORPLNNegara" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKCORPLNNegara" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKCORPLNNegara" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Provinsi</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext" valign="top">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKCORPLNProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKCORPLNProvinsi" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKCORPLNProvinsi" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kota
                                                                                                </td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtTRXKKCORPLNKota" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox></td>
                                                                                                            <td>
                                                                                                                <asp:ImageButton ID="imgTRXKKCORPLNKota" runat="server" CausesValidation="False"
                                                                                                                    ImageUrl="~/Images/button/browse.gif" SkinID="CancelButton" /></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfTRXKKCORPLNKota" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    Kode Pos</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKCORPLNKodePos" runat="server" CssClass="textBox"></asp:TextBox></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        k. Nomor Pokok Wajib Pajak (NPWP)</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKKCORPNPWP" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        l. Tujuan Transaksi</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKKCORPTujuanTrx" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        m. Sumber Dana</td>
                                                                                    <td style="width: 5px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKKCORPSumberDana" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                </tr>   
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
                                                                                    <td style="width: 5px">
                                                                                    </td>
                                                                                    <td class="formtext">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext" colspan="3">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    ii. Nama Bank Lain</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKCORPNamaBankLain" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    ii. Nomor Rekening Tujuan</td>
                                                                                                <td style="width: 5px">
                                                                                                    :</td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTRXKKCORPNoRekeningTujuan" runat="server" CssClass="textBox"
                                                                                                        Width="257px"></asp:TextBox></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>                                                                             
                                                                            </table>
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        5.5. Rekening Lain Yang Terkait Dengan Transaksi</td>
                                                                                    <td style="width: 2px">
                                                                                        </td>
                                                                                    <td class="formtext">
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="formtext">
                                                                                        5.5.1 Nomor Rekening Tujuan</td>
                                                                                    <td style="width: 2px">
                                                                                        :</td>
                                                                                    <td class="formtext">
                                                                                        <asp:TextBox ID="txtTRXKKRekeningKK" runat="server" CssClass="textBox" Width="257px"></asp:TextBox></td>
                                                                                </tr>
                                                                                
                                                                            </table>
                                                                        </asp:View>
                                                                    </asp:MultiView>
                                                                    <table>
                                                                        <tr>
                                                                            <td bgcolor="#ffffff">
                                                                                <asp:ImageButton ID="imgAdd" runat="server" SkinID="AddButton" ImageUrl="~/Images/button/add.gif" CausesValidation="false" />
                                                                                <asp:ImageButton ID="imgSaveEdit" runat="server" ImageUrl="~/Images/button/save.gif" CausesValidation="false" Visible="False"/>&nbsp;
                                                                            </td>
                                                                            <td bgcolor="#ffffff" style="width: 76px">
                                                                                <asp:ImageButton ID="imgClearAll" runat="server" SkinID="AddButton" ImageUrl="~/Images/button/clear.gif" CausesValidation="false" />
                                                                                <asp:ImageButton ID="imgCancelEdit" runat="server" SkinID="AddButton" ImageUrl="~/Images/button/cancel.gif" CausesValidation="false" Visible="False" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <asp:GridView runat="server" ID="grvTransaksi" AutoGenerateColumns="False" 
                                                                                    SkinID="grv2">
                                                                                    <Columns>
                                                                                        <asp:BoundField HeaderText="No" />
                                                                                        <asp:BoundField HeaderText="Kas" DataField="Kas" />
                                                                                        <asp:BoundField HeaderText="Transaction Date" DataField="TransactionDate" />
                                                                                        <asp:BoundField HeaderText="Branch" DataField="Branch" />
                                                                                        <asp:BoundField HeaderText="Account No" DataField="AccountNumber" />
                                                                                        <asp:BoundField HeaderText="Customer / LTKT" DataField="Type" />
                                                                                        <asp:BoundField HeaderText="Transaction Nominal" DataField="TransactionNominal" />
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="EditResume" runat="server" CausesValidation="False" OnClick="EditResume_Click">Edit</asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="DeleteResume" runat="server" CausesValidation="False" OnClick="DeleteResume_Click">Delete</asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtext">
                                                                                Total Seluruh Kas Masuk</td>
                                                                            <td style="width: 5px">
                                                                                :</td>
                                                                            <td class="formtext">
                                                                                <asp:Label runat="server" ID="lblTotalKasMasuk"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtext" style="height: 15px">
                                                                                Total Seluruh Kas Keluar</td>
                                                                            <td style="width: 5px; height: 15px;">
                                                                                :</td>
                                                                            <td class="formtext" style="height: 15px">
                                                                                <asp:Label runat="server" ID="lblTotalKasKeluar"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ajax:AjaxPanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                    <asp:View runat="server" ID="vwMessage">
                                        <table width="100%" style="horiz-align: center;">
                                            <tr>
                                                <td class="formtext" align="center" style="height: 15px">
                                                    <asp:Label runat="server" ID="lblMsg"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="formtext" align="center">
                                                    <asp:ImageButton ID="imgOKMsg" runat="server" SkinID="AddButton" ImageUrl="~/Images/button/Ok.gif" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </asp:MultiView>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td background="Images/button-bground.gif" align="left" valign="middle">
                                <img src="Images/blank.gif" width="5" height="1" /></td>
                            <td background="Images/button-bground.gif" align="left" valign="middle">
                                <img src="images/arrow.gif" width="15" height="15" />&nbsp;</td>
                            <td background="Images/button-bground.gif" style="width: 5px">
                                &nbsp;</td>
                            <td background="Images/button-bground.gif">
                                <asp:ImageButton ID="ImageSave" runat="server" SkinID="AddButton" ImageUrl="~/Images/Button/Save.gif">
                                </asp:ImageButton>&nbsp;</td>
                            <td background="Images/button-bground.gif">
                                <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"
                                    ImageUrl="~/Images/Button/Cancel.gif"></asp:ImageButton>&nbsp;</td>
                            <td width="99%" background="Images/button-bground.gif">
                                <img src="Images/blank.gif" width="1" height="1" /></td>
                            <td>
                                </td>
                        </tr>
                    </table>
                </ajax:AjaxPanel>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>

