<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SuspiciusPersonApprovalDetail.aspx.vb" Inherits="SuspiciusPersonApprovalDetail"  %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<%@ Register Src="webcontrol/PopUpAccountOwner.ascx" TagName="PopUpAccountOwner"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">

<ajax:AjaxPanel id="AjaxPanel30" runat="server">
    <uc1:PopUpAccountOwner id="PopUpAccountOwner1" runat="server">
    </uc1:PopUpAccountOwner>
</ajax:AjaxPanel>

	<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <table style="width: 100%">
        <tr>
            <td style="width: 200px" valign="top">
                <asp:Label ID="LblMode" runat="server" Text="Mode"></asp:Label></td>
            <td style="width: 1px" valign="top">
                :</td>
            <td valign="top">
                <asp:Label ID="LblAction" runat="server"></asp:Label></td>
        </tr>
    </table>
    <TABLE id="SearchBar" runat="server" borderColor="#ffffff" cellSpacing="0" cellPadding="0" width="99%" border="1">
        <TR>
            <TD align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD vAlign="top" width="100%" >
		        <TABLE  borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd" border="2">
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">Account Owner :
							</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel14" runat="server">
                                <asp:Label ID="LblAccountOwner" runat="server">Please Select Account Owner</asp:Label>&nbsp;
                                <asp:HiddenField ID="haccountowner" runat="server" />
                                <asp:ImageButton ID="ImgBrowse" runat="server" ImageUrl="~/Images/button/browse.gif" />
                                <asp:ImageButton ID="ImgCancel" runat="server" ImageUrl="~/Images/button/cancel.gif" /></ajax:ajaxpanel>
                                &nbsp;&nbsp;
                            </TD>
						</TR>
                            	<TR>
							<TD class="Regtext" noWrap bgcolor="White">VIP Code :</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel17" runat="server">
                                <asp:dropdownlist id="CboVIPCode" tabIndex="1" runat="server" Width="400px" 
                                    CssClass="searcheditcbo" AutoPostBack="true" ></asp:dropdownlist>&nbsp; 
                            </ajax:ajaxpanel>
                            </TD>
						</TR>
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">Insider Code :</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel18" runat="server">
                                <asp:dropdownlist id="CboInsidercode" tabIndex="1" runat="server" Width="400px" 
                                    CssClass="searcheditcbo" AutoPostBack="true" ></asp:dropdownlist>&nbsp; 
                            </ajax:ajaxpanel>
                            </TD>
						</TR>
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">Segment :</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel19" runat="server">
                                <asp:dropdownlist id="CboSegment" tabIndex="1" runat="server" Width="400px" 
                                    CssClass="searcheditcbo" AutoPostBack="true" ></asp:dropdownlist>&nbsp; 
                            </ajax:ajaxpanel>
                            </TD>
						</TR>
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">SBU :</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel20" runat="server">
                                <asp:dropdownlist id="cbosbu" tabIndex="1" runat="server" Width="400px" 
                                    CssClass="searcheditcbo" AutoPostBack="true" ></asp:dropdownlist>&nbsp; 
                            </ajax:ajaxpanel>
                            </TD>
						</TR>
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">Sub Sbu:</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel23" runat="server">
                                <asp:dropdownlist id="cbosubsbu" tabIndex="1" runat="server" Width="400px" 
                                    CssClass="searcheditcbo" AutoPostBack="true" ></asp:dropdownlist>&nbsp; 
                            </ajax:ajaxpanel>
                            </TD>
						</TR>
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">RM</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel22" runat="server">
                                <asp:dropdownlist id="CboRM" tabIndex="1" runat="server" Width="400px" 
                                    CssClass="searcheditcbo" AutoPostBack="true" ></asp:dropdownlist>&nbsp; 
                            </ajax:ajaxpanel>
                            </TD>
						</TR>
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">PIC :
							</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
                                <asp:Label ID="LabelPIC" runat="server" Text=""></asp:Label>
                            </TD>
						</TR>
                    
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">Description :
							</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel1" runat="server">
                                <asp:textbox id="TextDescription" tabIndex="2" runat="server" CssClass="searcheditbox" TextMode="MultiLine" MaxLength="255" Width="300px" Height="50px" Enabled="False"></asp:textbox>
                            </ajax:ajaxpanel>
                            </TD>
						</TR>
				</TABLE><ajax:ajaxpanel id="AjaxPanel3" runat="server">
                    <asp:CustomValidator ID="CustomValidator1" runat="server" Display="None"></asp:CustomValidator></ajax:ajaxpanel>
            </TD>
	    </TR>
	</TABLE>
    <table style="width: 100%">
        <tr>
            <td valign="top">
                <asp:Panel ID="PanelOld" runat="server" GroupingText="Old Value" Width="100%">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblOldName" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label14" runat="server" Text="CIFNo"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblOldCIF" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label2" runat="server" Text="Birth Date"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblOldBirthDate" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label3" runat="server" Text="Birth Place"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblOldBirthPlace" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label4" runat="server" Text="Identity No"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblOldIdentityNo" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label5" runat="server" Text="Address"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblOldAlamat" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label6" runat="server" Text="Kecamatan /Kelurahan"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblOldKecamatanKelurahan" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label7" runat="server" Text="Phone No"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblOLdPhoneNo" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label8" runat="server" Text="Position"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="lblOldJob" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label9" runat="server" Text="Company Name"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblOldWorkPlace" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label10" runat="server" Text="NPWP"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblOldNPWP" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label11" runat="server" Text="Description"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblOldDescription" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label12" runat="server" Text="Created By"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblOldCreatedBy" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top">
                <asp:Panel ID="PanelNew" runat="server" GroupingText="New Value" Width="100%">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label13" runat="server" Text="Name"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblNewName" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label16" runat="server" Text="CIFNo"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="lblNewCIF" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label15" runat="server" Text="Birth Date"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblNewBirthDate" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label17" runat="server" Text="Birth Place"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblNewBirthPlace" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label19" runat="server" Text="Identity No"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblNewIdentityNo" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label21" runat="server" Text="Address"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblNewAlamat" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label23" runat="server" Text="Kecamatan /Kelurahan"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblNewKecamatanKelurahan" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label25" runat="server" Text="Phone No"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblNewPhoneNo" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label27" runat="server" Text="Position"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="lblNewJob" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label29" runat="server" Text="Company Name"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblNewWorkPlace" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label31" runat="server" Text="NPWP"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblNewNPWP" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label33" runat="server" Text="Description"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblNewDescription" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label35" runat="server" Text="Created By"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblNewCreatedBy" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                <asp:ImageButton ID="ImageAccept" runat="server" CausesValidation="False" SkinID="AcceptButton" /><asp:ImageButton
                    ID="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton" /><asp:ImageButton
                        ID="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton" /><asp:CustomValidator
                            ID="cvalPageError" runat="server" Display="none"></asp:CustomValidator></td>
        </tr>
    </table>
  
</asp:Content>
