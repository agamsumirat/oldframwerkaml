#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
#End Region

Partial Class MsBentukBidangUsaha_Delete
    Inherits Parent

#Region "Function"

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = Listmaping
        LBMapping.DataTextField = "Nama"
        LBMapping.DataValueField = "IdBentukBidangusahaNCBS"
        LBMapping.DataBind()
    End Sub


#End Region

#Region "events..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                Listmaping = New TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim ObjMsBentukBidangUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetPaged(MsBentukBidangUsahaColumn.IdBentukBidangUsaha.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsBentukBidangUsaha Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsBentukBidangUsaha
            SafeDefaultValue = "-"
            lblIdBentukBidangUsaha.Text = .IdBentukBidangUsaha
            lblBentukBidangUsaha.Text = Safe(.BentukBidangUsaha)

            'other info
            Dim Omsuser As TList(Of User)
            Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
            If Omsuser.Count > 0 Then
                lblCreatedBy.Text = Omsuser(0).UserName
            End If
            lblCreatedDate.Text = FormatDate(.CreatedDate)
            Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
            If Omsuser.Count > 0 Then
                lblUpdatedby.Text = Omsuser(0).UserName
            End If
            lblUpdatedDate.Text = FormatDate(.LastUpdatedDate)
            lblActivation.Text = SafeActiveInactive(.Activation)
            Dim L_objMappingMsBentukBidangUsahaNCBSPPATK As TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
            L_objMappingMsBentukBidangUsahaNCBSPPATK = DataRepository.MappingMsBentukBidangUsahaNCBSPPATKProvider.GetPaged(MappingMsBentukBidangUsahaNCBSPPATKColumn.IdBentukBidangUsaha.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            Listmaping.AddRange(L_objMappingMsBentukBidangUsahaNCBSPPATK)
        End With
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

#Region "Property..."

    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsBentukBidangUsahaNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property

#End Region

#Region "events..."
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsBentukBidangUsaha_View.aspx")
    End Sub

    Protected Sub imgOk_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOk.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then

                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsBentukBidangUsaha_Approval As New MsBentukBidangUsaha_Approval
                    With ObjMsBentukBidangUsaha_Approval
                        .FK_MsMode_Id = 3
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsBentukBidangUsaha_ApprovalProvider.Save(ObjMsBentukBidangUsaha_Approval)
                    KeyHeaderApproval = ObjMsBentukBidangUsaha_Approval.PK_MsBentukBidangUsaha_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsBentukBidangUsaha_ApprovalDetail As New MsBentukBidangUsaha_ApprovalDetail()
                    With objMsBentukBidangUsaha_ApprovalDetail
                        Dim ObjMsBentukBidangUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(parID)
                        FillOrNothing(.IdBentukBidangUsaha, lblIdBentukBidangUsaha.Text, True, oInt)
                        FillOrNothing(.BentukBidangUsaha, lblBentukBidangUsaha.Text, True, Ovarchar)

                        FillOrNothing(.IdBentukBidangUsaha, ObjMsBentukBidangUsaha.IdBentukBidangUsaha)
                        FillOrNothing(.Activation, ObjMsBentukBidangUsaha.Activation)
                        FillOrNothing(.CreatedDate, ObjMsBentukBidangUsaha.CreatedDate)
                        FillOrNothing(.CreatedBy, ObjMsBentukBidangUsaha.CreatedBy)
                        FillOrNothing(.FK_MsBentukBidangUsaha_Approval_Id, KeyHeaderApproval)
                    End With
                    DataRepository.MsBentukBidangUsaha_ApprovalDetailProvider.Save(objMsBentukBidangUsaha_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsBentukBidangUsaha_ApprovalDetail.PK_MsBentukBidangUsaha_ApprovalDetail_Id

                    '========= Insert mapping item 
                    Dim LobjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail As New TList(Of MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)

                    Dim L_ObjMappingMsBentukBidangUsahaNCBSPPATK As TList(Of MappingMsBentukBidangUsahaNCBSPPATK) = DataRepository.MappingMsBentukBidangUsahaNCBSPPATKProvider.GetPaged(MappingMsBentukBidangUsahaNCBSPPATKColumn.IdBentukBidangUsaha.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

                    For Each objMappingMsBentukBidangUsahaNCBSPPATK As MappingMsBentukBidangUsahaNCBSPPATK In L_ObjMappingMsBentukBidangUsahaNCBSPPATK
                        Dim objMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail As New MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail
                        With objMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail
                            FillOrNothing(.IdBentukBidangUsaha, objMappingMsBentukBidangUsahaNCBSPPATK.PK_MappingMsBentukBidangUsahaNCBSPPATK_Id)
                            FillOrNothing(.IdBentukBidangUsahaNCBS, objMappingMsBentukBidangUsahaNCBSPPATK.IdBentukBidangUsahaNCBS)
                            FillOrNothing(.PK_MsBentukBidangUsaha_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.PK_MappingMsBentukBidangUsahaNCBSPPATK_Id, objMappingMsBentukBidangUsahaNCBSPPATK.PK_MappingMsBentukBidangUsahaNCBSPPATK_Id)
                            .Nama = objMappingMsBentukBidangUsahaNCBSPPATK.Nama
                            LobjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail.Add(objMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
                        End With
                        DataRepository.MappingMsBentukBidangUsahaNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
                    Next
                    'session Maping item Clear
                    Listmaping.Clear()
                    Me.imgOk.Visible = False
                    Me.ImageCancel.Visible = False
                    LblConfirmation.Text = "Data has been Delete and waiting for approval"
                    MtvMsUser.ActiveViewIndex = 1
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Response.Redirect("MsBentukBidangUsaha_View.aspx")
    End Sub

#End Region

End Class



