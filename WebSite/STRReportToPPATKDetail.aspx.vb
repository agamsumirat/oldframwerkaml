﻿Imports EkaDataNettier.Entities
Imports EkaDataNettier.Data
Imports AMLBLL

Partial Class STRReportToPPATKDetail
    Inherits Parent

    Private ReadOnly Property GetPK_CaseManagement() As Long
        Get
            If IsNumeric(Request.Params("STRReportToPPATKID")) Then
                Session("STRReportToPPATKViewDetail.PK_CaseManagement") = Request.Params("STRReportToPPATKID")
            Else
                Session("STRReportToPPATKViewDetail.PK_CaseManagement") = 0
            End If
            Return CLng(Session("STRReportToPPATKViewDetail.PK_CaseManagement"))
        End Get
    End Property

    Function isValidData() As Boolean
        Try
            If rblIsReportedToPPATK.SelectedIndex = -1 Then
                Throw New Exception("Reported To PPATK must be choosen")
            End If
            If txtReportingDate.Text.Trim.Length = 0 Then
                Throw New Exception("Reporting Date must be filled")
            End If
            If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", txtReportingDate.Text) = False Then
                Throw New Exception("Reporting date format invalid")
            End If
            If Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", txtReportingDate.Text) > Today Then
                Throw New Exception("Reporting date should not greater than today")
            End If
            If txtNoSTRPPATK.Text.Trim.Length = 0 Then
                Throw New Exception("No STR PPATK must be filled")
            End If


            Return True
        Catch ex As Exception
            LblSuccess.Visible = True
            LblSuccess.Text = ex.Message
            LogError(ex)
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
            Return False
        End Try
    End Function


    Sub loadData()
        Dim objVwSTRReportedToPPATK As vw_STRReportToPPATK = STRReportToPPATKBLL.GetVw_STRReportedToPPATKByPKCASEID(GetPK_CaseManagement)
        If Not IsNothing(objVwSTRReportedToPPATK) Then
            lblCASEID.Text = objVwSTRReportedToPPATK.PK_CaseManagementID
            lblCaseDescription.Text = objVwSTRReportedToPPATK.CaseDescription
            lblStatus.Text = objVwSTRReportedToPPATK.ProposedAction
            lblCreatedDate.Text = objVwSTRReportedToPPATK.CreatedDate
            lblLastUpdateDate.Text = objVwSTRReportedToPPATK.LastUpdated
            lblAssignBranch.Text = objVwSTRReportedToPPATK.assignedbranch
            lblPIC.Text = objVwSTRReportedToPPATK.PIC
            lblAging.Text = objVwSTRReportedToPPATK.Aging

            If objVwSTRReportedToPPATK.IsReportedtoRegulator Then
                rblIsReportedToPPATK.SelectedValue = 1
            Else
                rblIsReportedToPPATK.SelectedValue = 0
            End If
            If objVwSTRReportedToPPATK.ReportedDate.GetValueOrDefault <> Date.MinValue Then
                txtReportingDate.Text = CDate(objVwSTRReportedToPPATK.ReportedDate).ToString("dd-MM-yyyy")
            End If
            txtNoSTRPPATK.Text = objVwSTRReportedToPPATK.PPATKConfirmationno

            'Using objMapCaseManagementReportedToPPATK As MapCaseManagementReportedToPPATK = STRReportToPPATKBLL.GetSTRReportToPPATKByPK(GetPK_CaseManagement)
            '    If Not IsNothing(objMapCaseManagementReportedToPPATK) Then
            '        If objMapCaseManagementReportedToPPATK.ReportedToPPATK Then
            '            rblIsReportedToPPATK.SelectedValue = 1
            '        Else
            '            rblIsReportedToPPATK.SelectedValue = 0
            '        End If
            '        txtReportingDate.Text = CDate(objMapCaseManagementReportedToPPATK.ReportingDate).ToString("dd-MM-yyyy")
            '        txtNoSTRPPATK.Text = objMapCaseManagementReportedToPPATK.NoSTRPPPATK
            '    End If
            'End Using
        End If
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then


                loadData()
                mtvSTRReporedToPPATKDetail.ActiveViewIndex = 0
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Me.popUpReportingDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtReportingDate.ClientID & "'), 'dd-mm-yyyy')")

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub



    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            LblSuccess.Visible = False
            If isValidData() Then
                Using objCaseManagement As CaseManagement = STRReportToPPATKBLL.GetCaseManagementByPK(GetPK_CaseManagement)
                    If Not IsNothing(objCaseManagement) Then
                        'objCaseManagement.FK_CaseManagementID = GetPK_CaseManagement
                        objCaseManagement.IsReportedtoRegulator = rblIsReportedToPPATK.SelectedValue
                        objCaseManagement.ReportedDate = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", txtReportingDate.Text.Trim)
                        objCaseManagement.PPATKConfirmationno = txtNoSTRPPATK.Text

                        If STRReportToPPATKBLL.SaveData(objCaseManagement) Then
                            mtvSTRReporedToPPATKDetail.ActiveViewIndex = 1
                        End If

                    End If
                End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Response.Redirect("STRReportToPPATK.aspx")
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("STRReportToPPATK.aspx")
    End Sub
End Class
