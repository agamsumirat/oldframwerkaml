﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class VIPCodeDelete
    Inherits Parent


    ReadOnly Property GetPk_VIPCode As Long
        Get
            If IsNumeric(Request.Params("VIPCodeID")) Then
                If Not IsNothing(Session("VIPCodeDelete.PK")) Then
                    Return CLng(Session("VIPCodeDelete.PK"))
                Else
                    Session("VIPCodeDelete.PK") = Request.Params("VIPCodeID")
                    Return CLng(Session("VIPCodeDelete.PK"))
                End If
            End If
            Return 0
        End Get
    End Property


    Sub clearSession()
        Session("VIPCodeDelete.PK") = Nothing
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "VIPCodeView.aspx"

            Me.Response.Redirect("VIPCodeView.aspx", False)
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImageDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageDelete.Click
        Try




            If Sahassa.AML.Commonly.SessionGroupName.ToLower = "superuser" Then
                If VIPCodeBLL.Delete(GetPk_VIPCode) Then
                    lblMessage.Text = "Delete Data Success"
                    mtvVIPCodeAdd.ActiveViewIndex = 1
                End If
            Else
                If VIPCodeBLL.DeleteApproval(GetPk_VIPCode) Then
                    lblMessage.Text = "Data has been inserted to Approval"
                    mtvVIPCodeAdd.ActiveViewIndex = 1
                End If
            End If


        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub LoadData()
        Using objVIPCode As VIPCode = VIPCodeBLL.getVIPCodeByPk(GetPk_VIPCode)
            If Not IsNothing(objVIPCode) Then
                txtDescription.Text = objVIPCode.VIPCodeDescription.ToString
                txtVIPCode.Text = objVIPCode.VIPCODE.ToString
                txtActivation.Text = objVIPCode.Activation.GetValueOrDefault(False)
            Else
                ImageDelete.Visible = False
            End If
        End Using
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                mtvVIPCodeAdd.ActiveViewIndex = 0
                clearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                LoadData()
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Response.Redirect("VIPCodeView.aspx")
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class


