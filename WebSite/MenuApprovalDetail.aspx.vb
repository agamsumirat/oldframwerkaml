Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class MenuApprovalDetail
    Inherits Parent

    Private InMenuManagementId As Integer
    Private InGroupId As String
    Public js As String = ""
    Public jsold As String = ""
    Public jsplain As String = ""
    Public jsplainold As String = ""
    Public sothink As String = ""
    Public sothinkold As String = ""
    Public ObjMenu As Sahassa.AML.AMLMenu
    Public BufferLevelPattern, BufferLevelMenuItem, BufferLevelMenuItemWithLink As StringBuilder

    Public ReadOnly Property GetPkMenuPendingApproval() As Integer
        Get
            Return CInt(Me.Request.Params("MenuManagementId"))
        End Get
    End Property
    Private Sub Init_Data(ByVal intGroupID As Integer, ByVal strType As String)
        Try
            ObjMenu = New Sahassa.AML.AMLMenu
            If strType = "Old" Then
                ObjMenu.GenerateMenu(intGroupID, 1)

                sothinkold = ObjMenu.GetResultInSothinkFormat()
                jsold = ObjMenu.GetResultInArray()
                jsplainold = ObjMenu.GetResultInArrayPlain()
            Else
                ObjMenu.GenerateMenu(intGroupID, 2)

                sothink = ObjMenu.GetResultInSothinkFormat()
                js = ObjMenu.GetResultInArray()
                jsplain = ObjMenu.GetResultInArrayPlain()
            End If

            Dim i As Integer = 0
            Dim stmp As String() = Nothing

            BufferLevelPattern = New StringBuilder
            stmp = ObjMenu.GetLevelPattern()
            BufferLevelPattern.Append("[")
            For i = 0 To stmp.Length - 1
                If Not (stmp(i) Is Nothing) Then
                    BufferLevelPattern.Append("'" & stmp(i).Replace("'", "\'").Replace(vbLf, "").Replace(vbCr, "") & "'")
                    BufferLevelPattern.Append(",")
                End If
            Next i
            BufferLevelPattern.Remove(BufferLevelPattern.Length - 1, 1)
            BufferLevelPattern.Append("]")

            BufferLevelMenuItem = New StringBuilder
            stmp = ObjMenu.GetLevelMenuITem()
            BufferLevelMenuItem.Append("[")
            For i = 0 To stmp.Length - 1
                If Not (stmp(i) Is Nothing) Then
                    BufferLevelMenuItem.Append("'" & stmp(i).Replace("'", "\'").Replace(vbLf, "").Replace(vbCr, "") & "'")
                    BufferLevelMenuItem.Append(",")
                End If
            Next i
            BufferLevelMenuItem.Remove(BufferLevelMenuItem.Length - 1, 1)
            BufferLevelMenuItem.Append("]")

            BufferLevelMenuItemWithLink = New StringBuilder
            stmp = ObjMenu.GetLevelMenuItemWithLink()
            BufferLevelMenuItemWithLink.Append("[")
            For i = 0 To stmp.Length - 1
                If Not (stmp(i) Is Nothing) Then
                    BufferLevelMenuItemWithLink.Append("'" & stmp(i).Replace("'", "\'").Replace(vbLf, "").Replace(vbCr, "") & "'")
                    BufferLevelMenuItemWithLink.Append(",")
                End If
            Next i
            BufferLevelMenuItemWithLink.Remove(BufferLevelMenuItemWithLink.Length - 1, 1)
            BufferLevelMenuItemWithLink.Append("]")

        Catch ex As Exception
            Throw
        End Try
    End Sub 'Init_Data 

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim ObjGroup As MsGroup
        Try
            InMenuManagementId = Request.QueryString("MenuManagementId")
            InGroupId = CInt(Request.QueryString("GroupId"))

            If Not Page.IsPostBack Then
                Me.ImageButtonAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageButtonReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")

                'ObjGroup = New MsGroup
                'ObjGroup = DataRepository.MsGroupProvider.GetByPk_MsGroup_Id(InGroupId)
                Dim GroupName As String = ""
                Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                    GroupName = AccessGroup.GetDataByGroupID(InGroupId).Rows(0)("GroupName")
                End Using

                Session("Action") = Request.QueryString("Action")
                Session("Category") = Request.QueryString("Category")
                Session("Group") = GroupName

                LabelActivity.Text = "Activity : " & Session("Action") & " " & Session("Category")
                LabelGroup.Text = "   Group : " & Session("Group")

                Select Case LCase(Session("Category"))
                    Case "menu"
                        Select Case LCase(Session("Action"))
                            Case "add"
                                Init_Data(InGroupId, "New")
                                ImageButtonAccept.Attributes.Add("onclick", "javascript:return menuConfirmAllUpdate();")
                            Case "edit"
                                Init_Data(InGroupId, "Old")
                                Init_Data(InGroupId, "New")
                                ImageButtonAccept.Attributes.Add("onclick", "javascript:return menuConfirmAllUpdate();")
                        End Select
                    Case "access authority" : GetDataAccessAuthority()
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            ValidCustomPageError.IsValid = False
            ValidCustomPageError.ErrorMessage += ex.Message & "<br>"
            LogError(ex)
        Finally
            'ObjGroup = Nothing
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Untuk mengambil data dari AccessAuthority
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Administrator]	6/13/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GetDataAccessAuthority()
        'Dim ObjFileSecurityApproval As FileSecurity_ApprovalCollection
        'Dim ObjFileSecurityApprovalOld As FileSecurity_ApprovalCollection
        Try
            Select Case LCase(Session("Action"))
                Case "add"
                    Using AccessFileSecurityApproval As New AMLDAL.AMLDataSetTableAdapters.FileSecurity_ApprovalTableAdapter
                        Using dtFileSecurityApproval As AMLDAL.AMLDataSet.FileSecurity_ApprovalDataTable = AccessFileSecurityApproval.GetData
                            For Each Row As AMLDAL.AMLDataSet.FileSecurity_ApprovalRow In dtFileSecurityApproval.Select("fk_Menu_PendingApproval_id=" & InMenuManagementId)
                                ListFileOld.Items.Add(New ListItem(Split(Row.FileSecurityFileName), Row.FileSecurityFileName))
                            Next
                        End Using
                    End Using
                    'ObjFileSecurityApproval = New FileSecurity_ApprovalCollection
                    'ObjFileSecurityApproval = DataRepository.FileSecurity_ApprovalProvider.GetByFk_Menu_PendingApproval_id(InMenuManagementId)
                    'ListFile.DataSource = ObjFileSecurityApproval
                    'ListFile.DataTextField = "FileName"
                    'ListFile.DataValueField = "FileName"
                    'ListFile.DataBind()
                    ListFile.Visible = False
                Case "edit"
                    ' New
                    Using AccessFileSecurityApproval As New AMLDAL.AMLDataSetTableAdapters.FileSecurity_ApprovalTableAdapter
                        Using dtFileSecurityApproval As AMLDAL.AMLDataSet.FileSecurity_ApprovalDataTable = AccessFileSecurityApproval.GetData
                            For Each Row As AMLDAL.AMLDataSet.FileSecurity_ApprovalRow In dtFileSecurityApproval.Select("fk_Menu_PendingApproval_id=" & InMenuManagementId & " AND TypeID=2 AND FileSecurityFileName <> ''", "FileSecurityFileName")
                                ListFile.Items.Add(New ListItem(Split(Row.FileSecurityFileName), Row.FileSecurityFileName))
                            Next
                        End Using
                    End Using
                    'ObjFileSecurityApproval = New FileSecurity_ApprovalCollection
                    'ObjFileSecurityApproval = DataRepository.FileSecurity_ApprovalProvider.GetPaged("Fk_Menu_PendingApproval_id=" & InMenuManagementId & " AND fk_MsType_id=2 AND FileName <> ''", "FileName", 0, 1000, 0)
                    'ListFile.DataSource = ObjFileSecurityApproval
                    'ListFile.DataTextField = "FileName"
                    'ListFile.DataValueField = "FileName"
                    'ListFile.DataBind()

                    ' Old
                    Using AccessFileSecurityApprovalOld As New AMLDAL.AMLDataSetTableAdapters.FileSecurity_ApprovalTableAdapter
                        Using dtFileSecurityApprovalOld As AMLDAL.AMLDataSet.FileSecurity_ApprovalDataTable = AccessFileSecurityApprovalOld.GetData
                            For Each Row As AMLDAL.AMLDataSet.FileSecurity_ApprovalRow In dtFileSecurityApprovalOld.Select("fk_Menu_PendingApproval_id=" & InMenuManagementId & " AND TypeID=1", "FileSecurityFileName")
                                ListFileOld.Items.Add(New ListItem(Split(Row.FileSecurityFileName), Row.FileSecurityFileName))
                            Next
                        End Using
                    End Using
                    'ObjFileSecurityApprovalOld = New FileSecurity_ApprovalCollection
                    'ObjFileSecurityApprovalOld = DataRepository.FileSecurity_ApprovalProvider.GetPaged("Fk_Menu_PendingApproval_id=" & InMenuManagementId & " AND TypeID=1", "FileName", 0, 1000, 0)
                    'ListFileOld.DataSource = ObjFileSecurityApprovalOld
                    'ListFileOld.DataTextField = "FileName"
                    'ListFileOld.DataValueField = "FileName"
                    'ListFileOld.DataBind()
            End Select
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Split
    ''' </summary>
    ''' <param name="FileName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Split(ByVal FileName As String) As String
        Dim OriFilename As String = FileName
        Dim RemoveExt As String = OriFilename.Substring(0, OriFilename.LastIndexOf("."))
        Dim r As Regex = New Regex("[A-Z]")
        Dim GroupColl As MatchCollection = r.Matches(RemoveExt)
        Dim last, curr As Integer
        curr = 0
        last = 0
        Dim jadi As String = ""
        For Each i As Match In GroupColl
            curr = i.Index
            If curr <> last Then
                jadi = jadi + RemoveExt.Substring(last, curr - last) & " "
            End If
            last = curr
        Next
        jadi += RemoveExt.Substring(last, RemoveExt.Length - last)
        Return jadi
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Untuk menerima perubahan
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Administrator]	6/13/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonAccept_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAccept.Click
        'Dim oSQLTrans As SqlTransaction = Nothing
        'Dim ObjSHSMenuApproval As ShS_Menu_ApprovalCollection
        'Dim ObjTransaction As TransactionManager = SqlClient.SqlDataRepository.CreateTransaction
        Try
            Select Case LCase(Session("Category"))
                Case "menu"
                    'ObjTransaction.BeginTransaction(IsolationLevel.ReadUncommitted)
                    'Using TransScope As New Transactions.TransactionScope
                    ObjMenu = New Sahassa.AML.AMLMenu
                    If LCase(Session("Action")) = "add" Then
                        'strMenuItems = Request.Form.GetValues("lstMenuSubmit")
                        ObjMenu.SaveMenuApproveNew(InMenuManagementId, Integer.Parse(InGroupId), Session("Action"))
                    Else
                        'strMenuItemsOld = Request.Form.GetValues("lstMenuSubmitEditOld")
                        ObjMenu.SaveMenuApproveOld(InMenuManagementId, Integer.Parse(InGroupId), Session("Action"))
                        'strMenuItems = Request.Form.GetValues("lstMenuSubmitEditNew")
                        ObjMenu.SaveMenuApproveNew(InMenuManagementId, Integer.Parse(InGroupId), Session("Action"))
                    End If

                    'ObjSHSMenuApproval = New ShS_Menu_ApprovalCollection
                    'ObjSHSMenuApproval = DataRepository.ShS_Menu_ApprovalProvider.GetPaged("fk_Menu_PendingApproval_Id=" & InMenuManagementId & "", "null", 0, 1000, 0)

                    ' delete SHS_Menu_Approval dan Menu Pending Approval berdasarkan fk_Menu_PendingApproval_Id
                    Using AccessDelete As New AMLDAL.AMLDataSetTableAdapters.QueriesSHSMenu
                        'oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessDelete, Data.IsolationLevel.ReadUncommitted)
                        AccessDelete.DeleteSHSMenuApprovalByFkMenuPending(InMenuManagementId)
                        AccessDelete.DeleteMenuPendingApproval(InMenuManagementId)
                    End Using


                    'SqlClient.SqlDataRepository.ShS_Menu_ApprovalProvider.Delete(ObjTransaction, ObjSHSMenuApproval)
                    'SqlClient.SqlDataRepository.Menu_PendingApprovalProvider.Delete(ObjTransaction, InMenuManagementId)
                    ObjMenu.UpdateWebConfig(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile.ToString())
                    ObjMenu.GenerateAllMenu(Server.MapPath("./script"))

                    'oSQLTrans.Commit()

                    'End Using
                Case "access authority" : AccessAuthorityApprove()
            End Select
            Sahassa.AML.Commonly.SessionIntendedPage = "Login.aspx"
            Response.Redirect("Login.aspx", False)
        Catch ex As Exception
            'If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            ValidCustomPageError.IsValid = False
            ValidCustomPageError.ErrorMessage += ex.Message & "<br>"
            'Logger.LogError(ex)
            'Try
            '    ObjTransaction.Rollback()
            'Catch
            'End Try
            LogError(ex)
        Finally
            'If Not oSQLTrans Is Nothing Then
            '    oSQLTrans.Dispose()
            '    oSQLTrans = Nothing
            'End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Untuk menolak perubahan
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Administrator]	6/13/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonReject_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonReject.Click
        'Dim oSQLTrans As SqlTransaction = Nothing

        Dim strMenuItems As String()
        Dim strMenuItemsOld As String()
        'Dim ObjSHSMenuApproval As ShS_Menu_ApprovalCollection
        'Dim Transaction As TransactionManager = SqlClient.SqlDataRepository.CreateTransaction
        Try
            Select Case LCase(Session("Category"))
                Case "menu"
                    'Using TransScope As New Transactions.TransactionScope

                    'Transaction.BeginTransaction(IsolationLevel.ReadUncommitted)
                    ObjMenu = New Sahassa.AML.AMLMenu
                    If LCase(Session("Action")) = "add" Then
                        'strMenuItems = Request.Form.GetValues("lstMenuSubmit")
                        ObjMenu.SaveMenuRejectNew(InMenuManagementId, Integer.Parse(InGroupId), Session("Action"))
                    Else
                        'strMenuItemsOld = Request.Form.GetValues("lstMenuSubmitEditOld")
                        ObjMenu.SaveMenuRejectOld(InMenuManagementId, Integer.Parse(InGroupId), Session("Action"))
                        'strMenuItems = Request.Form.GetValues("lstMenuSubmitEditNew")
                        ObjMenu.SaveMenuRejectNew(InMenuManagementId, Integer.Parse(InGroupId), Session("Action"))
                    End If

                    'ObjSHSMenuApproval = New ShS_Menu_ApprovalCollection
                    'ObjSHSMenuApproval = DataRepository.ShS_Menu_ApprovalProvider.GetPaged("fk_Menu_PendingApproval_Id=" & InMenuManagementId & "", "null", 0, 1000, 0)

                    Using AccessDelete As New AMLDAL.AMLDataSetTableAdapters.QueriesSHSMenu
                        'oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessDelete, Data.IsolationLevel.ReadUncommitted)
                        AccessDelete.DeleteSHSMenuApprovalByFkMenuPending(InMenuManagementId)
                        AccessDelete.DeleteMenuPendingApproval(InMenuManagementId)
                    End Using

                    'SqlClient.SqlDataRepository.ShS_Menu_ApprovalProvider.Delete(Transaction, ObjSHSMenuApproval)
                    'SqlClient.SqlDataRepository.Menu_PendingApprovalProvider.Delete(Transaction, InMenuManagementId)

                    'Transaction.Commit()
                    'oSQLTrans.Commit()
                    'End Using
                Case "access authority" : AccessAuthorityReject()
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "MenuApproval.aspx"

            Response.Redirect("MenuApproval.aspx", False)
        Catch ex As Exception
            'If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            ValidCustomPageError.IsValid = False
            ValidCustomPageError.ErrorMessage += ex.Message & "<br>"
            'Logger.LogError(ex)
            'Try
            '    Transaction.Rollback()
            'Catch
            'End Try
            LogError(ex)
        Finally
            'If Not oSQLTrans Is Nothing Then
            '    oSQLTrans.Dispose()
            '    oSQLTrans = Nothing
            'End If
        End Try
    End Sub
    ''' <summary>
    ''' Cek Existing FileSecurity Approval
    ''' </summary>
    ''' <param name="pk_menu_pending_id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CekExistingFileSecurityApproval(ByVal pk_menu_pending_id As Integer) As Boolean
        Try
            Using accessFileSecurity As New AMLDAL.AMLDataSetTableAdapters.Menu_PendingApprovalByIDTableAdapter
                Using dt As AMLDAL.AMLDataSet.Menu_PendingApprovalByIDDataTable = accessFileSecurity.GetData(pk_menu_pending_id)
                    If dt.Rows.Count > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Untuk menerima perubahan terhadap AccessAuthority dan memasukkan ke 
    '''     Audit_MenuManagement
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Administrator]	6/13/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AccessAuthorityApprove()
        'Dim oSQLTrans As SqlTransaction = Nothing

        'Dim ObjFileSecurity As FileSecurityCollection
        'Dim ObjMenuPendingApproval As Menu_PendingApproval
        'Dim ObjFileSecurityApproval As FileSecurity_ApprovalCollection
        'Dim ObjAudit_MenuManagement As Audit_MenuManagementCollection
        'Dim ObjTransaction As TransactionManager = SqlClient.SqlDataRepository.CreateTransaction
        Dim i As Integer
        Try
            'ObjFileSecurity = New FileSecurityCollection
            'ObjFileSecurity = DataRepository.FileSecurityProvider.GetByFk_MsGroup_id(InGroupId)

            'ObjMenuPendingApproval = New Menu_PendingApproval
            'ObjMenuPendingApproval = DataRepository.Menu_PendingApprovalProvider.GetByPk_Menu_PendingApproval_Id(InMenuManagementId)
            If Me.CekExistingFileSecurityApproval(Me.GetPkMenuPendingApproval) Then
                Dim UserId As String = ""
                'Using TransScope As New Transactions.TransactionScope

                Using AccessMenuPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Menu_PendingApprovalByIDTableAdapter
                    UserId = AccessMenuPendingApproval.GetData(InMenuManagementId).Rows(0)("UserId")


                    'ObjFileSecurityApproval = New FileSecurity_ApprovalCollection
                    'ObjFileSecurityApproval = DataRepository.FileSecurity_ApprovalProvider.GetByFk_Menu_PendingApproval_id(InMenuManagementId)

                    'Dim Pk_FileSecurityApproval_Id As Integer
                    Using AccessFileSecurityApproval As New AMLDAL.AMLDataSetTableAdapters.FileSecurity_ApprovalTableAdapter
                        'oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessFileSecurityApproval, IsolationLevel.ReadUncommitted)

                        'ObjTransaction.BeginTransaction(IsolationLevel.ReadUncommitted)

                        Select Case LCase(Session("Action"))
                            Case "add"
                                'ObjAudit_MenuManagement = New Audit_MenuManagementCollection
                                Using AccessMenuQuery As New AMLDAL.AMLDataSetTableAdapters.QueriesSHSMenu
                                    'Sahassa.AML.TableAdapterHelper.SetTransaction(AccessMenuQuery, oSQLTrans)
                                    Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                        'Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrail, oSQLTrans)
                                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(2 * ListFileOld.Items.Count)

                                        For i = 0 To ListFileOld.Items.Count - 1
                                            AccessMenuQuery.InsertFileSecurity(InGroupId, ListFileOld.Items(i).Value)
                                            'ObjFileSecurity.Add(InGroupId, ListFile.Items(i).ToString)
                                            'SqlClient.SqlDataRepository.FileSecurityProvider.Save(ObjTransaction, ObjFileSecurity)
                                            AccessAuditTrail.Insert(Now, UserId, Sahassa.AML.Commonly.SessionUserId, Session("Category"), "GroupName", Session("Action"), Session("Group"), "", "Accepted")
                                            AccessAuditTrail.Insert(Now, UserId, Sahassa.AML.Commonly.SessionUserId, Session("Category"), "FileName", Session("Action"), ListFileOld.Items(i).ToString, "", "Accepted")
                                            'ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "GroupName", Nothing, Session("Group"), "Accepted")
                                            'ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "FileName", Nothing, ListFile.Items(i).ToString, "Accepted")
                                        Next
                                    End Using
                                End Using
                            Case "edit"
                                'DataRepository.FileSecurityProvider.Delete(ObjFileSecurity)
                                Using AccessFileSecurity As New AMLDAL.AMLDataSetTableAdapters.FileSecurityTableAdapter
                                    'Sahassa.AML.TableAdapterHelper.SetTransaction(AccessFileSecurity, oSQLTrans)
                                    ' Delete File Security
                                    AccessFileSecurity.DeleteFileSecurityByGroupID(InGroupId)

                                    Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                        'Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrail, oSQLTrans)
                                        'ObjAudit_MenuManagement = New Audit_MenuManagementCollection

                                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(2 * (ListFile.Items.Count + ListFileOld.Items.Count))
                                        ' delete file security yang lama
                                        AccessFileSecurity.DeleteFileSecurityByGroupID(InGroupId)

                                        For i = 0 To ListFile.Items.Count - 1
                                            ' insert file security yg baru
                                            AccessFileSecurity.Insert(InGroupId, ListFile.Items(i).Value)
                                            AccessAuditTrail.Insert(Now, UserId, Sahassa.AML.Commonly.SessionUserId, Session("Category"), "GroupName", Session("Action"), "", Session("Group"), "Accepted")
                                            AccessAuditTrail.Insert(Now, UserId, Sahassa.AML.Commonly.SessionUserId, Session("Category"), "FileName", Session("Action"), "", ListFile.Items(i).ToString, "Accepted")

                                            'ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "GroupName", Session("Group"), Nothing, "Accepted")
                                            'ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "FileName", ListFileOld.Items(i).ToString, Nothing, "Accepted")
                                        Next

                                        For i = 0 To ListFileOld.Items.Count - 1
                                            'AccessFileSecurity.Insert(InGroupId, ListFileOld.Items(i).Value)
                                            'ObjFileSecurity.Add(InGroupId, ListFile.Items(i).ToString)
                                            'SqlClient.SqlDataRepository.FileSecurityProvider.Save(ObjTransaction, ObjFileSecurity)


                                            AccessAuditTrail.Insert(Now, UserId, Sahassa.AML.Commonly.SessionUserId, Session("Category"), "GroupName", Session("Action"), Session("Group"), "", "Accepted")
                                            AccessAuditTrail.Insert(Now, UserId, Sahassa.AML.Commonly.SessionUserId, Session("Category"), "FileName", Session("Action"), ListFileOld.Items(i).ToString, "", "Accepted")

                                            'ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "GroupName", Nothing, Session("Group"), "Accepted")
                                            'ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "FileName", Nothing, ListFile.Items(i).ToString, "Accepted")
                                        Next
                                    End Using
                                End Using
                        End Select

                        'SqlClient.SqlDataRepository.Audit_MenuManagementProvider.Save(ObjTransaction, ObjAudit_MenuManagement)
                        AccessFileSecurityApproval.DeleteFileSecurityApproval(InMenuManagementId)
                        'SqlClient.SqlDataRepository.FileSecurity_ApprovalProvider.Delete(ObjTransaction, ObjFileSecurityApproval.Item(0).Pk_FileSecurity_Approval_Id)
                        Using AccessQuery As New AMLDAL.AMLDataSetTableAdapters.QueriesSHSMenu
                            'Sahassa.AML.TableAdapterHelper.SetTransaction(AccessQuery, oSQLTrans)
                            AccessQuery.DeleteMenuPendingApproval(InMenuManagementId)
                        End Using
                        'SqlClient.SqlDataRepository.Menu_PendingApprovalProvider.Delete(ObjTransaction, InMenuManagementId)
                        'oSQLTrans.Commit()
                    End Using

                End Using
                'End Using
            Else
                Throw New Exception("File Security is not available. ")
            End If
        Catch
            'If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
            'Try
            '    ObjTransaction.Rollback()
            'Catch
            'End Try
        Finally
            'If Not oSQLTrans Is Nothing Then
            '    oSQLTrans.Dispose()
            '    oSQLTrans = Nothing
            'End If
            'If Not ObjFileSecurity Is Nothing Then ObjFileSecurity.Dispose()
            'ObjFileSecurity = Nothing
            'If Not ObjFileSecurityApproval Is Nothing Then ObjFileSecurityApproval.Dispose()
            'ObjFileSecurityApproval = Nothing
            'ObjMenuPendingApproval = Nothing
            'If Not ObjAudit_MenuManagement Is Nothing Then ObjAudit_MenuManagement.Dispose()
            'ObjAudit_MenuManagement = Nothing
            'ObjTransaction = Nothing
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Untuk menolak perubahan terhadap AccessAuthority dan memasukkan ke
    '''     Audit_MenuManagement
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Administrator]	6/13/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AccessAuthorityReject()
        Dim oSQLTrans As SqlTransaction = Nothing

        'Dim ObjFileSecurity As FileSecurityCollection
        'Dim ObjMenuPendingApproval As Menu_PendingApproval
        'Dim ObjFileSecurityApproval As FileSecurity_ApprovalCollection
        'Dim ObjAudit_MenuManagement As Audit_MenuManagementCollection
        'Dim ObjTransaction As TransactionManager = SqlClient.SqlDataRepository.CreateTransaction
        Dim i As Integer
        Try
            'ObjFileSecurity = New FileSecurityCollection
            'ObjFileSecurity = DataRepository.FileSecurityProvider.GetByFk_MsGroup_id(InGroupId)
            'ObjMenuPendingApproval = New Menu_PendingApproval
            'ObjMenuPendingApproval = DataRepository.Menu_PendingApprovalProvider.GetByPk_Menu_PendingApproval_Id(InMenuManagementId)

            'ObjFileSecurityApproval = New FileSecurity_ApprovalCollection
            'ObjFileSecurityApproval = DataRepository.FileSecurity_ApprovalProvider.GetByFk_Menu_PendingApproval_id(InMenuManagementId)
            'ObjTransaction.BeginTransaction(IsolationLevel.ReadUncommitted)
            If Me.CekExistingFileSecurityApproval(Me.GetPkMenuPendingApproval) Then


                Dim UserId As String
                Using AccessMenuPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Menu_PendingApprovalByIDTableAdapter
                    UserId = AccessMenuPendingApproval.GetData(InMenuManagementId).Rows(0)("UserId")

                    Using AccessFileSecurityApproval As New AMLDAL.AMLDataSetTableAdapters.FileSecurity_ApprovalTableAdapter
                        'Using TransScope As New Transactions.TransactionScope

                        Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAuditTrail, Data.IsolationLevel.ReadUncommitted)
                            Select Case LCase(Session("Action"))
                                Case "add"
                                    'ObjAudit_MenuManagement = New Audit_MenuManagementCollection

                                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(2 * ListFileOld.Items.Count)

                                    For i = 0 To ListFileOld.Items.Count - 1
                                        AccessAuditTrail.Insert(Now, UserId, Sahassa.AML.Commonly.SessionUserId, Session("Category"), "GroupName", Session("Action"), Session("Group"), "", "Rejected")
                                        AccessAuditTrail.Insert(Now, UserId, Sahassa.AML.Commonly.SessionUserId, Session("Category"), "FileName", Session("Action"), ListFileOld.Items(i).ToString, "", "Rejected")

                                        'ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "GroupName", Nothing, Session("Group"), "Rejected")
                                        'ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "FileName", Nothing, ListFile.Items(i).ToString, "Rejected")
                                    Next
                                Case "edit"
                                    'ObjAudit_MenuManagement = New Audit_MenuManagementCollection

                                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(2 * (ListFile.Items.Count + ListFileOld.Items.Count))

                                    For i = 0 To ListFile.Items.Count - 1
                                        AccessAuditTrail.Insert(Now, UserId, Sahassa.AML.Commonly.SessionUserId, Session("Category"), "GroupName", Session("Action"), "", Session("Group"), "Rejected")
                                        AccessAuditTrail.Insert(Now, UserId, Sahassa.AML.Commonly.SessionUserId, Session("Category"), "FileName", Session("Action"), "", ListFile.Items(i).ToString, "Rejected")

                                        'ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "GroupName", Session("Group"), Nothing, "Rejected")
                                        'ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "FileName", ListFileOld.Items(i).ToString, Nothing, "Rejected")
                                    Next

                                    For i = 0 To ListFileOld.Items.Count - 1
                                        AccessAuditTrail.Insert(Now, UserId, Sahassa.AML.Commonly.SessionUserId, Session("Category"), "GroupName", Session("Action"), Session("Group"), "", "Rejected")
                                        AccessAuditTrail.Insert(Now, UserId, Sahassa.AML.Commonly.SessionUserId, Session("Category"), "FileName", Session("Action"), ListFileOld.Items(i).ToString, "", "Rejected")

                                        'ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "GroupName", Nothing, Session("Group"), "Rejected")
                                        'ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "FileName", Nothing, ListFile.Items(i).ToString, "Rejected")
                                    Next
                            End Select

                        End Using
                        AccessFileSecurityApproval.DeleteFileSecurityApproval(InMenuManagementId)
                        Using AccessQuery As New AMLDAL.AMLDataSetTableAdapters.QueriesSHSMenu
                            'Sahassa.AML.TableAdapterHelper.SetTransaction(AccessQuery, oSQLTrans)
                            AccessQuery.DeleteMenuPendingApproval(InMenuManagementId)
                        End Using
                        oSQLTrans.Commit()
                    End Using
                End Using
                'End Using
                'Select Case LCase(Session("Action"))
                '    Case "add"
                '        ObjAudit_MenuManagement = New Audit_MenuManagementCollection
                '        For i = 0 To ListFile.Items.Count - 1
                '            ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "GroupName", Nothing, Session("Group"), "Rejected")
                '            ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "FileName", Nothing, ListFile.Items(i).ToString, "Rejected")
                '        Next
                '    Case "edit"
                '        ObjAudit_MenuManagement = New Audit_MenuManagementCollection
                '        For i = 0 To ListFileOld.Items.Count - 1
                '            ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "GroupName", Session("Group"), Nothing, "Rejected")
                '            ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "FileName", ListFileOld.Items(i).ToString, Nothing, "Rejected")
                '        Next

                '        For i = 0 To ListFile.Items.Count - 1
                '            ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "GroupName", Nothing, Session("Group"), "Rejected")
                '            ObjAudit_MenuManagement.Add(ObjMenuPendingApproval.UserId, Commonly.SessionUserId, Now, Session("Category"), Session("Action"), "FileName", Nothing, ListFile.Items(i).ToString, "Rejected")
                '        Next
                'End Select

                'SqlClient.SqlDataRepository.Audit_MenuManagementProvider.Save(ObjTransaction, ObjAudit_MenuManagement)

                'SqlClient.SqlDataRepository.FileSecurity_ApprovalProvider.Delete(ObjTransaction, ObjFileSecurityApproval.Item(0).Pk_FileSecurity_Approval_Id)
                'SqlClient.SqlDataRepository.Menu_PendingApprovalProvider.Delete(ObjTransaction, InMenuManagementId)

                'ObjTransaction.Commit()
            Else
                Throw New Exception("File Security is not available. ")
            End If
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()

            Throw
            'Try
            '    ObjTransaction.Rollback()
            'Catch
            'End Try
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
            'If Not ObjFileSecurity Is Nothing Then ObjFileSecurity.Dispose()
            'ObjFileSecurity = Nothing
            'If Not ObjFileSecurityApproval Is Nothing Then ObjFileSecurityApproval.Dispose()
            'ObjFileSecurityApproval = Nothing
            'ObjMenuPendingApproval = Nothing
            'If Not ObjAudit_MenuManagement Is Nothing Then ObjAudit_MenuManagement.Dispose()
            'ObjAudit_MenuManagement = Nothing
            'ObjTransaction = Nothing
        End Try
    End Sub

End Class
