<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CDDLNPApproval.aspx.vb" Inherits="CDDLNPApproval" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script src="Script/popcalendar.js"></script>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div>
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="10" height="100%" /></td>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <img src="Images/dot_title.gif" width="17" height="17">
                                                <strong>
                                                    <asp:Label ID="Label1" runat="server" Text="EDD - Approval"></asp:Label>
                                                </strong>
                                                <hr />
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Label6" runat="server" Text="Search Criteria" Font-Bold="True"></asp:Label>
                                                            &nbsp;</td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('SearchCriteria','searchimage4','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="searchimage4" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px"></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="SearchCriteria">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                    <tr runat="server" visible="false">
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            EDD Type</td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:DropDownList ID="cboCDDType" runat="server" CssClass="comboBox">
                                                            </asp:DropDownList></td>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            PIC</td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtLastApproval" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="180px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            Customer Name</td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtName" TabIndex="2" runat="server" CssClass="searcheditbox" Width="250px"></asp:TextBox></td>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            Politically Exposed Person</td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:DropDownList ID="cboIsPEP" runat="server" CssClass="comboBox">
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            Customer Type</td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:DropDownList ID="cboNasabahType" runat="server" CssClass="comboBox">
                                                            </asp:DropDownList></td>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            Beneficial Owner</td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtParentBO" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="180px" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            CIF</td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtCIF" TabIndex="2" runat="server" CssClass="searcheditbox" Width="180px"
                                                                MaxLength="50"></asp:TextBox></td>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            Created Date</td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtCreatedDateStart" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="125px" MaxLength="50"></asp:TextBox>
                                                            <input id="popupCreatedDateStart" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                                border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                                border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                                width: 16px;" type="button" onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')"
                                                                runat="server">&nbsp; s/d
                                                            <asp:TextBox ID="txtCreatedDateEnd" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px"></asp:TextBox>
                                                            <input id="popupCreatedDateEnd" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                                border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                                border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                                width: 16px;" type="button" onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')"
                                                                runat="server"></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            AML/CFT Risk Rating Personal</td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtRatingPersonal" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="180px" MaxLength="50"></asp:TextBox></td>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            EDD Initiator</td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtCreatedBy" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="180px" MaxLength="50"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            AML/CFT Risk Rating Gabungan (Final)</td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtRatingGabungan" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                Width="180px" MaxLength="50"></asp:TextBox></td>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            Submission Status</td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:DropDownList ID="cboApprovalStatus" runat="server" CssClass="comboBox">
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="10">
                                                            &nbsp;<asp:ImageButton ID="ImageButtonSearch" TabIndex="3" runat="server" SkinID="SearchButton"
                                                                CausesValidation="False" ImageUrl="~/Images/Button/Search.gif"></asp:ImageButton>&nbsp;<asp:ImageButton
                                                                    ID="ImageClear" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/clearSearch.gif"
                                                                    TabIndex="3" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr id="TR1">
                                            <td valign="top" width="98%" bgcolor="#ffffff">
                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="0" width="100%" bgcolor="#dddddd"
                                                    border="2">
                                                    <tr>
                                                        <td bgcolor="#ffffff">
                                                            <asp:DataGrid ID="GridViewCDDLNP" runat="server" AutoGenerateColumns="False" Font-Size="XX-Small"
                                                                BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
                                                                Width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE" ForeColor="Black">
                                                                <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                                <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
                                                                <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                                <ItemStyle BackColor="#F7F7DE"></ItemStyle>
                                                                <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B" HorizontalAlign="Center">
                                                                </HeaderStyle>
                                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Visible="false" />
                                                                <Columns>
                                                                    <%--0--%>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="2%" />
                                                                    </asp:TemplateColumn>
                                                                    <%--1--%>
                                                                    <asp:BoundColumn DataField="PK_CDDLNP_ID" Visible="false" />
                                                                    <%--2--%>
                                                                    <asp:BoundColumn HeaderText="No" HeaderStyle-ForeColor="White" ItemStyle-Width="3%" />
                                                                    <%--3--%>
                                                                    <asp:BoundColumn DataField="FK_CDD_Type_ID" Visible="false" />
                                                                    <%--4--%>
                                                                    <asp:BoundColumn DataField="CDD_Type" HeaderText="EDD Type" SortExpression="CDD_Type  desc"
                                                                        ItemStyle-Width="9%" />
                                                                    <%--5--%>
                                                                    <asp:BoundColumn DataField="FK_CDD_NasabahType_ID" Visible="false" />
                                                                    <%--6--%>
                                                                    <asp:BoundColumn DataField="CDD_NasabahType_ID" HeaderText="Customer Type" SortExpression="CDD_NasabahType_ID  desc"
                                                                        ItemStyle-Width="8%" Visible="false" />
                                                                    <%--7--%>
                                                                    <asp:BoundColumn DataField="Nama" HeaderText="Customer Name" SortExpression="Nama  desc"
                                                                        ItemStyle-Width="10%" />
                                                                    <%--8--%>
                                                                    <asp:BoundColumn DataField="CIF" HeaderText="CIF" SortExpression="CIF  desc" ItemStyle-Width="6%" />
                                                                    <%--9--%>
                                                                    <asp:BoundColumn DataField="AMLRiskRatingPersonal" HeaderText="AML/CFT Risk Rating Personal"
                                                                        SortExpression="AMLRiskRatingPersonal  desc" ItemStyle-Width="5%" />
                                                                    <%--10--%>
                                                                    <asp:BoundColumn DataField="AMLRiskRatingGabungan" HeaderText="AML/CFT Risk Rating Gabungan (Final)"
                                                                        SortExpression="AMLRiskRatingGabungan  desc" ItemStyle-Width="5%" />
                                                                    <%--11--%>
                                                                    <asp:BoundColumn DataField="IsPEP" HeaderText="PEP Related" SortExpression="IsPEP  desc"
                                                                        ItemStyle-Width="5%" />
                                                                    <%--12--%>
                                                                    <asp:BoundColumn DataField="ParentBO" HeaderText="Beneficial Owner" SortExpression="ParentBO  desc"
                                                                        ItemStyle-Width="10%" />
                                                                    <%--13--%>
                                                                    <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" SortExpression="CreatedDate  desc"
                                                                        DataFormatString="{0:dd-MMM-yyyy}" ItemStyle-Width="7%" Visible="false" />
                                                                    <%--14--%>
                                                                    <asp:BoundColumn DataField="CreatedBy" HeaderText="EDD Initiator" SortExpression="CreatedBy  desc"
                                                                        ItemStyle-Width="6%" />
                                                                    <%--15--%>
                                                                    <asp:BoundColumn DataField="ApprovalStatus" HeaderText="Submission Status" SortExpression="ApprovalStatus  desc"
                                                                        ItemStyle-Width="8%" />
                                                                    <%--16--%>
                                                                    <asp:BoundColumn DataField="LastApprovalStatus" Visible="false" />
                                                                    <%--17--%>
                                                                    <asp:BoundColumn DataField="LastWorkflowGroup" HeaderText="PIC" HeaderStyle-ForeColor="white"
                                                                        ItemStyle-Width="8%" />
                                                                    <%--18--%>
                                                                    <asp:BoundColumn DataField="LastApprovalDate" HeaderText="Submission Date" HeaderStyle-ForeColor="white"
                                                                        SortExpression="LastApprovalDate  asc" DataFormatString="{0:dd-MMM-yyyy}" ItemStyle-Width="8%" />
                                                                    <%--19--%>
                                                                    <asp:BoundColumn DataField="Aging" HeaderText="Aging" HeaderStyle-ForeColor="white"
                                                                        SortExpression="Aging  desc" ItemStyle-Width="8%" DataFormatString="{0} day(s)" />
                                                                    <asp:TemplateColumn HeaderStyle-Width="5%">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="LnkDetail" runat="server" CausesValidation="false" CommandName="Detail"
                                                                                Text="Detail" ajaxcall="none"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="LnkPrint" runat="server" CausesValidation="false" CommandName="Print"
                                                                                Text="Print" ajaxcall="none"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="5%" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="LnkEdit" runat="server" CausesValidation="false" CommandName="Edit"
                                                                                Text="Edit"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="5%" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="LnkDelete" runat="server" CausesValidation="false" CommandName="Delete"
                                                                                Text="Delete"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="5%" />
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #ffffff">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td nowrap style="height: 20px">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td nowrap style="height: 20px;">
                                                                        <asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" /></td>
                                                                    <td width="100%" style="height: 20px">
                                                                        &nbsp;<asp:LinkButton ID="lnkExportData" runat="server" ajaxcall="none" Text="Export Selected"></asp:LinkButton>
                                                                        &nbsp;<asp:LinkButton ID="lnkExportAll" runat="server" ajaxcall="none" Text="Export All"></asp:LinkButton></td>
                                                                    <td align="right" nowrap style="height: 20px">
                                                                        &nbsp; &nbsp;&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <tr>
                                                    <td bgcolor="#ffffff">
                                                        <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                            bgcolor="#ffffff" border="2">
                                                            <tr class="regtext" align="center" bgcolor="#dddddd">
                                                                <td valign="top" align="left" width="50%" bgcolor="#ffffff" style="height: 19px">
                                                                    Page&nbsp;<asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>&nbsp;of&nbsp;
                                                                    <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label></td>
                                                                <td valign="top" align="right" width="50%" bgcolor="#ffffff" style="height: 19px">
                                                                    Total Records&nbsp;
                                                                    <asp:Label ID="PageTotalRows" runat="server">0</asp:Label></td>
                                                            </tr>
                                                        </table>
                                                        <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                            bgcolor="#ffffff" border="2">
                                                            <tr bgcolor="#ffffff">
                                                                <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                                                                    <hr>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                                                                    Go to page</td>
                                                                <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                                                                    <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                                        <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox>
                                                                    </font>
                                                                </td>
                                                                <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                                                                    <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton" ImageUrl="~/Images/Button/Go.gif">
                                                                    </asp:ImageButton>
                                                                </td>
                                                                <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                                    <img height="5" src="images/first.gif" width="6">
                                                                </td>
                                                                <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff">
                                                                    <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                                                        OnCommand="PageNavigate">First</asp:LinkButton>
                                                                </td>
                                                                <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                                                    <img height="5" src="images/prev.gif" width="6"></td>
                                                                <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff">
                                                                    <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                                                        OnCommand="PageNavigate">Previous</asp:LinkButton>
                                                                </td>
                                                                <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff">
                                                                    <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                                                        OnCommand="PageNavigate">Next</asp:LinkButton></td>
                                                                <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                                    <img height="5" src="images/next.gif" width="6"></td>
                                                                <td class="regtext" valign="middle" align="left" width="50" bgcolor="#ffffff">
                                                                    <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                                                        OnCommand="PageNavigate">Last</asp:LinkButton>
                                                                </td>
                                                                <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                                    <img height="5" src="images/last.gif" width="6"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                    </table>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td align="left" valign="middle">
                            <img src="Images/blank.gif" width="5" height="1" /></td>
                        <td align="left" valign="middle">
                            <img src="images/arrow.gif" width="15" height="15" /></td>
                        <td width="99%">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
