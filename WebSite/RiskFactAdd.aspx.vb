Imports System.Data.SqlClient
Imports Sahassa.AML.TableAdapterHelper
Partial Class RiskFactAdd
    Inherits Parent

    Public ReadOnly Property strSessionName() As String
        Get
            Return "RiskFactAdd"
        End Get        
    End Property

    Public ReadOnly Property oQueryBuilder() As Sahassa.AML.QueryBuilder
        Get
            If Session(strSessionName) Is Nothing Then

                Dim TempQuery As Sahassa.AML.QueryBuilder = New Sahassa.AML.QueryBuilder
                TempQuery.AddTableString("[CFMAST]", "[CFMAST]")
                TempQuery.AddFieldString(" Distinct [CFMAST].[CFCIF#] ")
                Session(strSessionName) = TempQuery
                Return Session(strSessionName)
            Else
                Return CType(Session(strSessionName), Sahassa.AML.QueryBuilder)
            End If
        End Get
       
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                Me.AbandonSession()
                SetSession()
            End If
            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
            End Using

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Private Sub SetSession()
        Tablestest1.OQueryBuilder = Me.oQueryBuilder

        WhereClausesTest1.OQueryBuilder = Me.oQueryBuilder
        GroupBy1.OQueryBuilder = Me.oQueryBuilder
        HavingCondition1.OQueryBuilder = Me.oQueryBuilder
    End Sub
    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu1.MenuItemClick
        MultiView1.ActiveViewIndex = Int32.Parse(e.Item.Value)
        Tablestest1.ActiveIndex = MultiView1.ActiveViewIndex

        WhereClausesTest1.ActiveIndex = MultiView1.ActiveViewIndex + 1
        GroupBy1.ActiveIndex = MultiView1.ActiveViewIndex + 1
        HavingCondition1.ActiveIndex = MultiView1.ActiveViewIndex + 1


    End Sub
    Private Sub AbandonSession()
        Session(strSessionName) = Nothing
        Tablestest1.OQueryBuilder = Nothing

        WhereClausesTest1.OQueryBuilder = Nothing
        GroupBy1.OQueryBuilder = Nothing
        HavingCondition1.OQueryBuilder = Nothing

    End Sub
    Private Sub InsertRiskFactBySU()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactTableAdapter
                oSQLTrans = BeginTransaction(adapter)
                adapter.Insert(TextRiskFactName.Text.Trim, TextDescription.Text.Trim, oQueryBuilder.SQlQuery, TextRiskScore.Text.Trim, chkEnabled.Checked)
            End Using
            AbandonSession()
            InsertAuditTrail(oSQLTrans)
            SetSession()
            oSQLTrans.Commit()
            Me.LblSuccess.Visible = True
            Me.LblSuccess.Text = "Success to Insert Risk Fact."
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    Private Sub InsertAuditTrail(ByRef oSQLTrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)

            Dim RiskFactDescription As String
            If Me.TextDescription.Text.Trim.Length <= 255 Then
                RiskFactDescription = Me.TextDescription.Text.Trim
            Else
                RiskFactDescription = Me.TextDescription.Text.Substring(0, 255)
            End If

            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                SetTransaction(AccessAudit, oSQLTrans)
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Fact Name", "Add", "", Me.TextRiskFactName.Text.Trim, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk FactDescription", "Add", "", RiskFactDescription, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Enabled", "Add", "", chkEnabled.Checked, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Fact Score", "Add", "", Me.TextRiskScore.Text.Trim, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Fact Expression", "Add", "", oQueryBuilder.SQlQuery, "Accepted")
            End Using
        Catch
            Throw
        End Try
    End Sub
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If Page.IsValid Then
                If IsDataValid() Then
                    If Sahassa.AML.Commonly.SessionUserId.ToLower = "superuser" Then
                        InsertRiskFactBySU()

                    Else
                        InsertRiskFactToPendingApproval()
                    End If
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' untuk menyimpan data ke pending approval
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function InsertRiskFactToPendingApproval() As Boolean
        Dim intHeader As Integer
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'insert to header

            Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                oSQLTrans = BeginTransaction(adapter)
                intHeader = adapter.InsertRiskFactPendingApproval(TextRiskFactName.Text.Trim, Sahassa.AML.Commonly.SessionUserId, 1, "Risk Fact Add", Now)
            End Using
            'insert to detail
            Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactApprovalTableAdapter
                SetTransaction(adapter, oSQLTrans)
                adapter.Insert(intHeader, 0, TextRiskFactName.Text.Trim, TextDescription.Text, oQueryBuilder.SQlQuery, TextRiskScore.Text, chkEnabled.Checked, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)

            End Using
            oSQLTrans.Commit()
            AbandonSession()
            SetSession()
            Dim MessagePendingID As Integer = 81501 'MessagePendingID 81501 = Risk Fact Add 

            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & TextRiskFactName.Text.Trim
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & TextRiskFactName.Text.Trim, False)
        Catch tex As Threading.ThreadAbortException
            ' ignore
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Function
    ''' <summary>
    ''' Untuk mengecek apakah data yang di input sudah valid
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsDataValid() As Boolean


        'cek rules name sudah ada belum di table riskfact
        Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactTableAdapter
            Dim intJmlRiskFact As Nullable(Of Integer) = adapter.CountRiskFactByRiskFactName(TextRiskFactName.Text.Trim)
            If intJmlRiskFact.GetValueOrDefault(0) > 0 Then

                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Risk Fact Name already exits in Risk Fact Table."
                Return False
            End If
        End Using

        'cek apakah rules name ada di tabel approval
        Using Adapter As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
            Dim intJmlApproval As Integer = Adapter.CountRiskFactPendingApprovalByUniquekey(TextRiskFactName.Text.Trim)
            If intJmlApproval > 0 Then

                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Risk Fact Name already exits in Risk Fact Pending Approval."
                Return False
            End If
        End Using
        Return True
    End Function
    Protected Sub CustomValidator1_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustomValidator1.ServerValidate
        If oQueryBuilder.SQlQuery = "" Then
            args.IsValid = False

            CustomValidator1.ErrorMessage = "Please configure sql expression first!"

        Else
            Try
                oQueryBuilder.ParseSQLQuery()
                args.IsValid = True
                CustomValidator1.ErrorMessage = ""
            Catch ex As Exception
                CustomValidator1.ErrorMessage = "There is an error in sql expression.Please configure sql expression again !"
                args.IsValid = False
            End Try



        End If
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.AbandonSession()
        Response.Redirect("RiskFactView.aspx", False)

    End Sub
End Class
