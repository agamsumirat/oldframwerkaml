Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Imports System.IO
Imports System.Collections.Generic

Partial Class MappingUserIdHiportJiveApproval
    Inherits Parent

    Public Property PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID() As String
        Get
            Return CType(Session("MappingUserIdHiportJiveApproval.PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID"), String)
        End Get
        Set(ByVal value As String)
            Session("MappingUserIdHIPORTJIVEApproval.PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID") = value
        End Set
    End Property

    Public Property SetnGetUserName() As String
        Get
            If Not Session("MappingUserIdHiportJiveApproval.SetnGetUserName") Is Nothing Then
                Return CStr(Session("MappingUserIdHiportJiveApproval.SetnGetUserName"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingUserIdHiportJiveApproval.SetnGetUserName") = value
        End Set
    End Property

    Public Property SetnGetKode_AO() As String
        Get
            If Not Session("MappingUserIdHiportJiveApproval.SetnGetKode_AO") Is Nothing Then
                Return CStr(Session("MappingUserIdHiportJiveApproval.SetnGetKode_AO"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingUserIdHiportJiveApproval.SetnGetKode_AO") = value
        End Set
    End Property

    Public Property SetnGetMode() As String
        Get
            If Not Session("MappingUserIdHiportJiveApproval.SetnGetMode") Is Nothing Then
                Return CStr(Session("MappingUserIdHiportJiveApproval.SetnGetMode"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingUserIdHiportJiveApproval.SetnGetMode") = value
        End Set
    End Property

    Public Property SetnGetPreparer() As String
        Get
            If Not Session("MappingUserIdHiportJiveApproval.SetnGetPreparer") Is Nothing Then
                Return CStr(Session("MappingUserIdHiportJiveApproval.SetnGetPreparer"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingUserIdHiportJiveApproval.SetnGetPreparer") = value
        End Set
    End Property

    Public Property SetnGetEntryDateFrom() As String
        Get
            If Not Session("MappingUserIdHiportJiveApproval.CreatedDate") Is Nothing Then
                Return CStr(Session("MappingUserIdHiportJiveApproval.CreatedDate"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingUserIdHiportJiveApproval.CreatedDate") = value
        End Set
    End Property

    Public Property SetnGetEntryDateUntil() As String
        Get
            If Not Session("MappingUserIdHiportJiveApproval.CreatedDateUntil") Is Nothing Then
                Return CStr(Session("MappingUserIdHiportJiveApproval.CreatedDateUntil"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingUserIdHiportJiveApproval.CreatedDateUntil") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("MappingUserIdHiportJiveApproval.Sort") Is Nothing, "UserName  asc", Session("MappingUserIdHiportJiveApproval.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("MappingUserIdHiportJiveApproval.Sort") = Value
        End Set
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("MappingUserIdHiportJiveApproval.RowTotal") Is Nothing, 0, Session("MappingUserIdHiportJiveApproval.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MappingUserIdHiportJiveApproval.RowTotal") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("MappingUserIdHiportJiveApproval.CurrentPage") Is Nothing, 0, Session("MappingUserIdHiportJiveApproval.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MappingUserIdHiportJiveApproval.CurrentPage") = Value
        End Set
    End Property

    Public Shared Function FORMAT_DATE_BEGIN_GREATER_THAN_DATE_END(ByVal strTanggal As String) As String
        If Not Sahassa.AML.Commonly.SessionLanguage Is Nothing Then
            If Sahassa.AML.Commonly.SessionLanguage.ToLower = "en-us" Then
                Return "Start Date must Earlier than End Date for " & strTanggal
            ElseIf Sahassa.AML.Commonly.SessionLanguage.ToLower = "id-id" Then
                Return "Tanggal Mulai harus lebih Muda dari pada Tanggal Akhir untuk " & strTanggal
            Else
                Return "Start Date must Earlier than End Date for " & strTanggal
            End If

        Else
            Return "Start Date must Earlier than End Date for " & strTanggal
        End If
    End Function

    Function Getvw_MappingUserIdHiportJiveApproval(ByVal strWhereClause As String, ByVal strOrderBy As String, ByVal intStart As Integer, ByVal intPageLength As Integer, ByRef intCount As Integer) As VList(Of vw_MappingUserIdHiportJiveApproval)
        Dim Objvw_MappingUserIdHiportJiveApproval As VList(Of vw_MappingUserIdHiportJiveApproval) = Nothing
        Objvw_MappingUserIdHiportJiveApproval = DataRepository.vw_MappingUserIdHiportJiveApprovalProvider.GetPaged(strWhereClause, strOrderBy, intStart, intPageLength, intCount)
        Return Objvw_MappingUserIdHiportJiveApproval
    End Function

    Public Property SetnGetBindTable() As VList(Of vw_MappingUserIdHiportJiveApproval)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            If SetnGetUserName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "UserName like '%" & SetnGetUserName.Trim.Replace("'", "''") & "%'"
            End If

            If SetnGetKode_AO.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Kode_AO like '%" & SetnGetKode_AO.Trim.Replace("'", "''") & "%'"
            End If

            If SetnGetMode.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Nama like '%" & SetnGetMode.Trim.Replace("'", "''") & "%'"
            End If

            If SetnGetPreparer.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Preparer like '%" & SetnGetPreparer.Trim.Replace("'", "''") & "%'"
            End If

            If SetnGetEntryDateFrom.Length > 0 AndAlso SetnGetEntryDateUntil.Length > 0 Then

                If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetEntryDateFrom) AndAlso Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetEntryDateFrom) Then

                    Dim tanggal As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateFrom).ToString("yyyy-MM-dd")
                    Dim tanggalAkhir As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateUntil).ToString("yyyy-MM-dd")
                    If DateDiff(DateInterval.Day, Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateFrom), Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateUntil)) > -1 Then

                        ReDim Preserve strWhereClause(strWhereClause.Length)

                        strWhereClause(strWhereClause.Length - 1) = vw_MappingUserIdHiportJiveApprovalColumn.CreatedDate.ToString & " between '" & tanggal & " 00:00:00' and '" & tanggalAkhir & " 23:59:59'"
                    Else
                        Throw New SahassaException(FORMAT_DATE_BEGIN_GREATER_THAN_DATE_END("CreatedDate"))
                    End If
                End If
            End If


            strAllWhereClause = String.Join(" and ", strWhereClause)
            If strAllWhereClause.Trim.Length > 0 Then
                strAllWhereClause += " and FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            Else
                strAllWhereClause += " FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            End If
            Session("MappingUserIdHiportJiveApproval.Table") = Getvw_MappingUserIdHiportJiveApproval(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)


            Return CType(Session("MappingUserIdHiportJiveApproval.Table"), VList(Of vw_MappingUserIdHiportJiveApproval))


        End Get
        Set(ByVal value As VList(Of vw_MappingUserIdHiportJiveApproval))
            Session("MappingUserIdHiportJiveApproval.Table") = value
        End Set
    End Property



    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("MappingUserIdHiportJiveApproval.PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID") = Nothing
        Session("MappingUserIdHiportJiveApproval.SetnGetUserName") = Nothing
        Session("MappingUserIdHiportJiveApproval.SetnGetMode") = Nothing
        Session("MappingUserIdHiportJiveApproval.SetnGetKode_AO") = Nothing
        Session("MappingUserIdHiportJiveApproval.SetnGetPreparer") = Nothing
        Session("MappingUserIdHiportJiveApproval.CreatedDate") = Nothing
        Session("MappingUserIdHiportJiveApproval.CreatedDateUntil") = Nothing

        SetnGetCurrentPage = Nothing
        Me.SetnGetRowTotal = Nothing
        Me.SetnGetSort = Nothing

    End Sub


    Private Sub bindgrid()
        SettingControlSearching()
        Me.GridUIHiportJive.DataSource = Me.SetnGetBindTable
        Me.GridUIHiportJive.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridUIHiportJive.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridUIHiportJive.DataBind()


    End Sub

    Private Sub SettingPropertySearching()

        SetnGetUserName = TxtUserName.Text.Trim
        SetnGetMode = TxtNama.Text.Trim
        SetnGetKode_AO = TxtKodeAO.Text.Trim
        SetnGetPreparer = TxtPreparer.Text.Trim
        Me.SetnGetEntryDateFrom = TxtEntryDateFrom.Text.Trim
        Me.SetnGetEntryDateUntil = TxtEntryDateUntil.Text.Trim

    End Sub

    Private Sub SettingControlSearching()

        TxtUserName.Text = SetnGetUserName
        TxtNama.Text = SetnGetMode
        TxtKodeAO.Text = SetnGetKode_AO
        TxtPreparer.Text = SetnGetPreparer
        TxtEntryDateFrom.Text = SetnGetEntryDateFrom
        TxtEntryDateUntil.Text = SetnGetEntryDateUntil

    End Sub



    Private Sub SetInfoNavigate()

        Me.PageCurrentPage.Text = (Me.SetnGetCurrentPage + 1).ToString
        Me.PageTotalPages.Text = Me.GetPageTotal.ToString
        Me.PageTotalRows.Text = Me.SetnGetRowTotal.ToString
        Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
        Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Try
            Me.bindgrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub ImageButtonSearchCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearchCancel.Click
        Try

            SetnGetUserName = Nothing
            SetnGetMode = Nothing
            SetnGetKode_AO = Nothing
            SetnGetPreparer = Nothing
            Me.SetnGetEntryDateFrom = Nothing
            Me.SetnGetEntryDateUntil = Nothing
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            SettingPropertySearching()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

                ClearThisPageSessions()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Me.popUpEntryDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtEntryDateFrom.ClientID & "'), 'dd-mm-yyyy')")
                Me.popUpEntryDateUntil.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtEntryDateUntil.ClientID & "'), 'dd-mm-yyyy')")

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                bindgrid()

            End If
            GridUIHiportJive.PageSize = CInt(Sahassa.AML.Commonly.GetDisplayedTotalRow)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridGMHIPORTJIVE_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridUIHiportJive.EditCommand
        Dim PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID As Integer 'primary key
        'Dim StrUserID As String  'unik
        Try
            PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID = CType(e.Item.Cells(1).Text, Integer)
            'StrUserID = e.Item.Cells(2).Text

            Response.Redirect("MappingUserIdHIPORTJIVEApprovalDetail.aspx?PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID=" & PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID, False)
            'Using ObjMsBranchBll As New SahassaBLL.MsHelpBLL
            '    If ObjMsBranchBll.IsNotExistInApproval(StrUserID) Then
            '        Response.Redirect("MsHelpEdit.aspx?PkMsHelpID=" & PkMsHelpID, False)
            '    End If
            'End Using

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridGMHIPORTJIVE_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridUIHiportJive.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                Dim EditButton As LinkButton = CType(e.Item.Cells(7).FindControl("LnkEdit"), LinkButton)

                e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try            
            If IsNumeric(Me.TextGoToPage.Text) AndAlso (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    Throw New Exception(Sahassa.AML.CommonlyEnum.EXECPTION_PAGENUMBER_GREATER_TOTALPAGENUMBER)
                End If
            Else
                Throw New Exception(Sahassa.AML.CommonlyEnum.EXECPTION_PAGENUMBER_NUMERICPOSITIF)
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridUIHiportJive_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridUIHiportJive.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class


