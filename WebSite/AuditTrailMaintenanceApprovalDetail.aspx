<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AuditTrailMaintenanceApprovalDetail.aspx.vb" Inherits="AuditTrailMaintenanceApprovalDetail" title="AuditTrail Maintenance Approval Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server">Audit Trail Maintenance Approval Detail</asp:label></td>
		</tr>
	</table>
    <TABLE id="TableDetail" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="2" runat="server">
	    <TR class="formText" id="Netral1">
		    <TD bgColor="#ffffff" height="24" style="width: 22px">&nbsp;</TD>
		    <TD bgColor="#ffffff">
                Purge By</TD>
		    <TD bgColor="#ffffff" width="1%">:</TD>
		    <TD width="80%" bgColor="#ffffff"><asp:label id="LabelCreatedBy" runat="server"></asp:label></TD>
	    </TR>
	    <TR class="formText" id="Netral2">
		    <TD bgColor="#ffffff" style="height: 23px; width: 22px;">&nbsp;</TD>
		    <TD bgColor="#ffffff" style="height: 23px;">
                Type Purge</TD>
		    <TD bgColor="#ffffff" style="height: 23px; width: 44px;">:</TD>
		    <TD width="80%" bgColor="#ffffff" style="height: 23px"><asp:label id="LabelPurgeType" runat="server"></asp:label></TD>
	    </TR>
        <tr class="formText" id="PurgeDate1">
            <td bgcolor="#ffffff" style="width: 22px; height: 23px">
            </td>
            <td bgcolor="#ffffff" style="height: 23px">Purge From
            </td>
            <td bgcolor="#ffffff" style="width: 44px; height: 23px">
                :
            </td>
            <td bgcolor="#ffffff" style="height: 23px" width="80%">
                <asp:Label ID="LabelStartDate" runat="server"></asp:Label>&nbsp; To
                <asp:Label ID="LabelEndDate" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="PurgeCount">
            <td bgcolor="#ffffff" style="width: 22px; height: 23px">
            </td>
            <td bgcolor="#ffffff" style="height: 23px">
                Count</td>
            <td bgcolor="#ffffff" style="width: 44px; height: 23px">
                :</td>
            <td bgcolor="#ffffff" style="height: 23px" width="80%">
                <asp:Label ID="LabelCount" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="Netral7">
            <td bgcolor="#ffffff" style="width: 22px; height: 23px">
            </td>
            <td bgcolor="#ffffff" style="height: 23px">
                Create Date</td>
            <td bgcolor="#ffffff" style="width: 44px; height: 23px">
                :</td>
            <td bgcolor="#ffffff" style="height: 23px" width="80%">
                <asp:Label ID="LabelCreatedDate" runat="server"></asp:Label></td>
        </tr>
	    <TR class="formText" id="Netral4" bgColor="#dddddd" height="30">
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="False" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="none"></asp:CustomValidator></TD>
	    </TR>
    </TABLE>
</asp:Content>