Imports AMLDAL
Imports System.Data.SqlClient
Partial Class BranchTypeMappingEdit
    Inherits Parent

    Private ReadOnly Property BranchMappingID() As Long
        Get
            Dim temp As String
            temp = Request.Params("BranchMappingID")
            If temp = "" Or Not IsNumeric(temp) Then
                Throw New Exception("Branch Mapping ID is invalid")
            Else
                Return temp
            End If
        End Get        
    End Property
    Private _orowBranchMapping As AMLDAL.MappingBranch.BranchTypeMappingRow
    Private ReadOnly Property orowBranchMapping() As AMLDAL.MappingBranch.BranchTypeMappingRow
        Get
            If _orowBranchMapping Is Nothing Then
                Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeMappingTableAdapter
                    Using otable As AMLDAL.MappingBranch.BranchTypeMappingDataTable = adapter.GetDataByPK(Me.BranchMappingID)
                        If otable.Rows.Count > 0 Then
                            _orowBranchMapping = otable.Rows(0)
                            Return _orowBranchMapping
                        Else
                            _orowBranchMapping = Nothing
                            Return _orowBranchMapping
                        End If
                    End Using
                End Using
            Else
                Return _orowBranchMapping
            End If
        End Get
    End Property

    Private Function IsDataValid() As Boolean
        Try
            'cek apakah data yang mau didelete tidak boleh ada di pending approval
            Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeMapping_PendingApprovalTableAdapter
                Dim jml As Integer = 0
                jml = adapter.CountByBranchTypeIDName(LabelunmapBranch.Text)
                If jml > 0 Then
                    cvalPageError.IsValid = False
                    cvalPageError.ErrorMessage = "Branch " & LabelunmapBranch.Text & " Already in waiting for approval."
                    Return False
                Else
                    Return True
                End If
            End Using

        Catch ex As Exception
            Return False
        End Try
    End Function
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("BranchTypeMappingView.aspx", False)
    End Sub
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim otrans As SqlTransaction = Nothing
        Dim intPK As Long = 0
        Try
            If Page.IsValid AndAlso isDatavalid Then

                'insert header
                Using adapter As New MappingBranchTableAdapters.BranchTypeMapping_PendingApprovalTableAdapter
                    otrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                    intPK = adapter.InsertBranchTypeMappingPendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, LabelunmapBranch.Text, "Edit", Sahassa.AML.Commonly.TypeMode.Edit)
                End Using

                'insert detail
                Using adapter As New MappingBranchTableAdapters.BranchTypeMappingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter, otrans)
                    adapter.Insert(intPK, Me.BranchMappingID, cboBranchType.SelectedValue, orowBranchMapping.BranchId, orowBranchMapping.BranchTypeMappingId, orowBranchMapping.BranchTypeId, orowBranchMapping.BranchId)
                End Using
                otrans.Commit()
                Dim MessagePendingID As Integer = 82592
                Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier= " & LabelunmapBranch.Text

                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & LabelunmapBranch.Text, False)
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub BindBranchType()
        Try
            cboBranchType.Items.Clear()
            Using adapter As New MappingBranchTableAdapters.BranchTypeTableAdapter
                cboBranchType.DataSource = adapter.GetData
                cboBranchType.DataTextField = "BranchTypeName"
                cboBranchType.DataValueField = "BranchTypeId"
                cboBranchType.DataBind()
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)


            End Using
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub LoadData()
        Try
            If Not orowBranchMapping Is Nothing Then
                Using adapter As New AMLDAL.MappingBranchTableAdapters.JHDATATableAdapter
                    Using otable As AMLDAL.MappingBranch.JHDATADataTable = adapter.GetDataByPK(orowBranchMapping.BranchId)
                        If otable.Rows.Count > 0 Then
                            Dim orow As AMLDAL.MappingBranch.JHDATARow = otable.Rows(0)
                            LabelunmapBranch.Text = orow.JDBR & " - " & orow.JDNAME
                        End If
                    End Using
                End Using
                cboBranchType.SelectedValue = orowBranchMapping.BranchTypeId
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Not IsPostBack Then
                BindBranchType()
                LoadData()

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
