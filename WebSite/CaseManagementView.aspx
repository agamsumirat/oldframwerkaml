<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="CaseManagementView.aspx.vb"Inherits="CaseManagementView" Title="Case Management - View"  Culture="id-ID" UICulture ="id-ID" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Alert Case Management - View
                </strong>
            </td>
        </tr>
    </table>
  
        <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
            border="2">
            <tr id="searchbox">
                <td valign="top" width="98%" bgcolor="#ffffff">
                    <table cellspacing="4" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td valign="middle" nowrap>
                                <ajax:AjaxPanel ID="AjaxPanel14" runat="server">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td nowrap style="height: 26px">
                                                Alert
                                                Case ID</td>
                                            <td nowrap style="height: 26px">
                                                :</td>
                                            <td nowrap style="height: 26px">
                                                <asp:TextBox ID="txtCaseID" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td nowrap style="height: 26px">
                                                Created Date<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtCreatedDate1"
                                                    ControlToValidate="txtCreatedDate2" Display="Dynamic" ErrorMessage="Created Date Last Must Greater or Equal  than Created Date First"
                                                    Operator="GreaterThanEqual" Type="Date" >*</asp:CompareValidator></td>
                                            <td nowrap style="height: 26px">
                                                :</td>
                                            <td nowrap style="height: 26px; width: 332px;">
                                                <asp:TextBox ID="txtCreatedDate1" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox>
                                                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtCreatedDate1"
                                                    Display="Dynamic" ErrorMessage="Created Date First must between 1-1-1753 and 31-12-9999"
                                                    MaximumValue="31-12-9999" MinimumValue="1-1-1753" Type="Date">*</asp:RangeValidator>
                                                <input id="popupCreatedFirst" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                    border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                    border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                    width: 16px;" type="button" name="popupCreatedFirst" runat="server">
                                                to<asp:TextBox ID="txtCreatedDate2" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox>
                                                <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtCreatedDate2"
                                                    Display="Dynamic" ErrorMessage="Created Date Last must between 1-1-1753 and 31-12-9999"
                                                    MaximumValue="31-12-9999" MinimumValue="1-1-1753" Type="Date">*</asp:RangeValidator>
                                                <input id="popupCreatedLast" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                    border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                    border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                    width: 16px;" type="button" name="popupCreatedLast" runat="server">
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td nowrap style="height: 26px">
                                               Alert Type</td>
                                            <td nowrap style="height: 26px">
                                                :</td>
                                            <td nowrap style="height: 26px">
                                                <asp:TextBox ID="txtDescription" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td nowrap style="height: 26px">
                                                Last Updated Date<asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="txtLastUpdatedDate1"
                                                    ControlToValidate="txtLastUpdatedDate2" Display="Dynamic" ErrorMessage="Last Updated Date Last Must Greater or Equal than  Last Update Date First"
                                                    Operator="GreaterThanEqual" Type="Date">*</asp:CompareValidator></td>
                                            <td nowrap style="height: 26px">
                                                :</td>
                                            <td nowrap style="height: 26px; width: 332px;">
                                                <asp:TextBox ID="txtLastUpdatedDate1" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox>
                                                <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtLastUpdatedDate1"
                                                    Display="Dynamic" ErrorMessage="Last Update Date First must between 1-1-1753 and 31-12-9999"
                                                    MaximumValue="31-12-9999" MinimumValue="1-1-1753" Type="Date">*</asp:RangeValidator>
                                                <input id="popupLastUpdatedFirst" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                    border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                    border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                    width: 16px;" type="button" name="popupLastUpdatedFirst" runat="server">
                                                to<asp:TextBox ID="txtLastUpdatedDate2" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox>
                                                <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="txtLastUpdatedDate2"
                                                    Display="Dynamic" ErrorMessage="Last Update Date Last must between 1-1-1753 and 31-12-9999"
                                                    MaximumValue="31-12-9999" MinimumValue="1-1-1753" Type="Date">*</asp:RangeValidator>
                                                <input id="popupLastUpdatedLast" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                    border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                    border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                    width: 16px;" type="button" name="popupLastUpdatedLast" runat="server">
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" style="height: 26px">
                                                Case Status</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                <asp:TextBox ID="txtCaseStatus" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                PIC</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px; width: 332px;">
                                                <asp:TextBox ID="txtPIC" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" style="height: 26px">
                                                Workflow Step</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                <asp:TextBox ID="txtWorkFlowStep" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                AML/CFT Risk</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px; width: 332px;">
                                                <asp:TextBox ID="TxtHighRiskCustomer" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" style="height: 26px">
                                                Account Owner</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                <asp:TextBox ID="txtAccountOwner" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                <%--Aging--%></td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                <%--:--%></td>
                                            <td nowrap="nowrap" style="height: 26px; width: 332px;">
                                                <asp:TextBox ID="txtAging" runat="server" CssClass="searcheditbox" TabIndex="2" Visible="false"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" style="height: 26px">
                                                Last Proposed Action</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                <asp:TextBox ID="txtLastProposedAction" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                Count Same Case</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px; width: 332px;">
                                                <asp:TextBox ID="txtCountSameCase" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" style="height: 26px">
                                                Customer Name</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                <asp:TextBox ID="txtCustomerName" runat="server" CssClass="searcheditbox" 
                                                    TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                RM Code</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px; width: 332px;">
                                                <asp:TextBox ID="txtRMCODE" runat="server" CssClass="searcheditbox" 
                                                    TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="10">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <asp:ImageButton ID="ImageButtonSearch" TabIndex="3" runat="server" SkinID="SearchButton"
                                                    ImageUrl="~/Images/button/search.gif"></asp:ImageButton>
                                                <asp:ImageButton ID="ImageClearSearch" runat="server" ImageUrl="~/Images/button/clearsearch.gif"  CausesValidation ="false"  /></td>
                                        </tr>
                                    </table>
                                    &nbsp;&nbsp;&nbsp;
                                </ajax:AjaxPanel>
                            </td>
                            <td valign="middle" width="99%">
                                &nbsp;</td>
                        </tr>
                    </table>
                    <ajax:AjaxPanel ID="AjaxPanel15" runat="server">
                        <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
                </td>
            </tr>
        </table>
    
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2">
        <tr>
            <td bgcolor="#ffffff">
                <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                    <asp:DataGrid ID="GridMSUserView" runat="server" AutoGenerateColumns="False" Font-Size="XX-Small"
                        BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
                        Width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE" ForeColor="Black">
                        <FooterStyle BackColor="#CCCC99"></FooterStyle>
                        <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
                        <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                        <ItemStyle BackColor="#F7F7DE"></ItemStyle>
                        <HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B">
                        </HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PK_CaseManagementID" SortExpression="CaseManagement.PK_CaseManagementID  desc"
                                HeaderText="Alert Case ID"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CustomerName" HeaderText="Customer Name" 
                                SortExpression="CustomerName desc"></asp:BoundColumn>
            <%--                <asp:TemplateColumn HeaderText="RM Code">
                                <EditItemTemplate>
                                    <asp:TextBox runat="server" 
                                        Text='<%# DataBinder.Eval(Container, "DataItem.RMCode") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label runat="server" ID ="RMCODE"
                                        Text='<%# DataBinder.Eval(Container, "DataItem.RMCode") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>--%>
                            <asp:BoundColumn DataField="RMCode" HeaderText="RM Code" 
                                SortExpression="CaseStatus.CaseStatusDescription  desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CaseDescription" HeaderText="Alert Type" 
                                SortExpression="CaseManagement.CaseDescription  desc">
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                    Font-Strikeout="False" Font-Underline="False" Wrap="False" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CaseStatusDescription" HeaderText="Case Status" 
                                SortExpression="CaseStatus.CaseStatusDescription  desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="WorkflowStep" SortExpression="CaseManagement.WorkflowStep  desc"
                                HeaderText="Workflow Step"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date"
                                SortExpression="CaseManagement.CreatedDate  desc" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="LastUpdated" HeaderText="Last Updated Date"
                                SortExpression="CaseManagement.LastUpdated  desc" DataFormatString="{0:dd-MM-yyyy HH:mm}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountOwnerIDName" HeaderText="Account Owner"
                                SortExpression="CAST(CaseManagement.FK_AccountOwnerID AS VARCHAR) + ' - ' + AccountOwner.AccountOwnerName desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="PIC" HeaderText="PIC" SortExpression="CaseManagement.PIC  desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="HighRiskCustomer" HeaderText="AML/CFT Risk"
                                SortExpression="dbo.ufn_GetHighRiskCustomerByPkCaseManagementID(casemanagement.PK_CaseManagementID) desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Aging" HeaderText="Aging" SortExpression="casemanagement.Aging  desc" Visible="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ProposedAction" HeaderText="Last Proposed Action"
                                SortExpression="CaseManagementProposedAction.ProposedAction desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="SameCaseCount" HeaderText="Count Same Case" SortExpression="(SELECT COUNT(1) FROM CaseManagementSameCaseHistory cmsch WHERE cmsch.FK_CaseManagement_ID=CaseManagement.PK_CaseManagementID)  desc">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn EditText="Detail"></asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </ajax:AjaxPanel>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="background-color: #ffffff">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td nowrap>
                            <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                                &nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
                                &nbsp;
                            </ajax:AjaxPanel>
                        </td>
                        <td width="99%">
                            &nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" runat="server">Export 
		        to Excel</asp:LinkButton>
                            <asp:LinkButton ID="LnkBtnExportAllToExcel" runat="server">Export All to Excel</asp:LinkButton></td>
                        <td align="right" nowrap>
                            &nbsp;&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                    bgcolor="#ffffff" border="2">
                    <tr class="regtext" align="center" bgcolor="#dddddd">
                        <td valign="top" align="left" width="50%" bgcolor="#ffffff">
                            Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                <asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>
                                &nbsp;of&nbsp;
                                <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label>
                            </ajax:AjaxPanel>
                        </td>
                        <td valign="top" align="right" width="50%" bgcolor="#ffffff">
                            Total Records&nbsp;
                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                <asp:Label ID="PageTotalRows" runat="server">0</asp:Label>
                            </ajax:AjaxPanel>
                        </td>
                    </tr>
                </table>
                <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                    bgcolor="#ffffff" border="2">
                    <tr bgcolor="#ffffff">
                        <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                            <hr color="#f40101" noshade size="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                            Go to page</td>
                        <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                    <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox>
                                </ajax:AjaxPanel>
                            </font>
                        </td>
                        <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton"></asp:ImageButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/first.gif" width="6">
                        </td>
                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                    OnCommand="PageNavigate">First</asp:LinkButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/prev.gif" width="6"></td>
                        <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                    OnCommand="PageNavigate">Previous</asp:LinkButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                <a class="pageNav" href="#">
                                    <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                        OnCommand="PageNavigate">Next</asp:LinkButton>
                                </a>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/next.gif" width="6"></td>
                        <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                    OnCommand="PageNavigate">Last</asp:LinkButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/last.gif" width="6"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
