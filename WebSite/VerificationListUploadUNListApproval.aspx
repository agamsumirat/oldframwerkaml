<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="VerificationListUploadUNListApproval.aspx.vb" Inherits="VerificationListUploadUNListApproval"
    Title="Verification List Upload UN List Approval" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpcontent" runat="Server">
    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Pending Approval Verification List - UN List</strong>
            </td>
        </tr>
    </table>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2" id="TableUploadData" runat="server">
        <tr>
            <td bgcolor="#ffffff">
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
        </tr>
        <tr>
            <td bgcolor="#ffffff" style="height: 19px">
                There is an upload for UN List that need for approval with following data
                supplied:</td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                UN List Data</td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                    <div style="overflow: auto; height: 200px; width: 800px">
                        <asp:GridView ID="GridPendingApprovalUNList" runat="server" AutoGenerateColumns="False"
                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                            CellPadding="4" DataKeyNames="RefId" ForeColor="Black" GridLines="Vertical">
                            <FooterStyle BackColor="#CCCC99" />
                            <Columns>
                                <asp:BoundField DataField="RefId" HeaderText="RefId" ReadOnly="True" SortExpression="RefId" />
                                <asp:BoundField DataField="DisplayName" HeaderText="Name" SortExpression="DisplayName" />
                                <asp:BoundField DataField="DateOfBirth" HeaderText="Date of Birth" SortExpression="DateOfBirth" DataFormatString="{0:dd-MMM-yyyy}" />
                                <asp:BoundField DataField="CustomRemark1" HeaderText="Custom Remark 1" SortExpression="CustomRemark1" />
                                <asp:BoundField DataField="CustomRemark2" HeaderText="Custom Remark 2" SortExpression="CustomRemark2" />
                                <asp:BoundField DataField="CustomRemark3" HeaderText="Custom Remark 3" SortExpression="CustomRemark3" />
                                <asp:BoundField DataField="CustomRemark4" HeaderText="Custom Remark 4" SortExpression="CustomRemark4" />
                                <asp:BoundField DataField="CustomRemark5" HeaderText="Custom Remark 5" SortExpression="CustomRemark5" />
                            </Columns>
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                        &nbsp;&nbsp;
                    </div>
                </ajax:AjaxPanel>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                &nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                UN List Alternative Name Data</td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                <ajax:AjaxPanel ID="Ajaxpanel2" runat="server">
                    <div style="overflow: auto; height: 200px; width: 800px">
                        &nbsp;<asp:GridView ID="GridPendingApprovalUNAlias" runat="server" AutoGenerateColumns="False"
                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                            CellPadding="4" DataKeyNames="RefId" ForeColor="Black" GridLines="Vertical">
                            <FooterStyle BackColor="#CCCC99" />
                            <Columns>
                                <asp:BoundField DataField="RefId" HeaderText="RefId" ReadOnly="True" SortExpression="RefId" />
                                <asp:BoundField DataField="Name" HeaderText="Alias Name" SortExpression="Name" />
                            </Columns>
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                        &nbsp;
                    </div>
                </ajax:AjaxPanel>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                &nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                UN List Address Data</td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                <ajax:AjaxPanel ID="Ajaxpanel3" runat="server">
                    <div style="overflow: auto; height: 200px; width: 800px">
                        &nbsp;<asp:GridView ID="GridPendingApprovalUNAddress" runat="server" AutoGenerateColumns="False"
                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                            CellPadding="4" DataKeyNames="RefId" ForeColor="Black" GridLines="Vertical">
                            <FooterStyle BackColor="#CCCC99" />
                            <Columns>
                                <asp:BoundField DataField="RefId" HeaderText="RefId" SortExpression="RefId" />
                                <asp:BoundField DataField="Address" HeaderText="Address" ReadOnly="True" SortExpression="Address" />
                                <asp:BoundField DataField="PostalCode" HeaderText="Postal Code" SortExpression="PostalCode" />
                                <asp:BoundField DataField="AddressTypeDescription" HeaderText="Address Type" SortExpression="AddressTypeDescription" />
                                <asp:BoundField DataField="LocalAddress" HeaderText="Local Address" SortExpression="LocalAddress" />
                            </Columns>
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                        &nbsp;
                    </div>
                </ajax:AjaxPanel>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                &nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                UN List ID Number</td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                <ajax:AjaxPanel ID="Ajaxpanel6" runat="server">
                    <div style="overflow: auto; height: 200px; width: 800px">
                        &nbsp;<asp:GridView ID="GridPendingApprovalUNIDNumber" runat="server" AutoGenerateColumns="False"
                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                            CellPadding="4" DataKeyNames="RefId" ForeColor="Black" GridLines="Vertical">
                            <FooterStyle BackColor="#CCCC99" />
                            <Columns>
                                <asp:BoundField DataField="RefId" HeaderText="RefId" ReadOnly="True" SortExpression="RefId" />
                                <asp:BoundField DataField="IDNumber" HeaderText="ID Number" SortExpression="IDNumber" />
                            </Columns>
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                        &nbsp;
                    </div>
                </ajax:AjaxPanel>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
            </td>
        </tr>
    </table>
    <table id="TableNoPendingApproval" runat="server" visible="false">
        <tr>
            <td>
                There is no pending approval for Verification List UN List</td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td nowrap style="height: 93px">
                <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                    &nbsp; &nbsp;
                </ajax:AjaxPanel>
                <asp:ImageButton ID="ImageAccept" runat="server" CausesValidation="False" SkinID="AcceptButton">
                </asp:ImageButton></td>
            <td style="height: 93px" nowrap="noWrap">
                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                    &nbsp; &nbsp;
                </ajax:AjaxPanel>
                <asp:ImageButton ID="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton">
                </asp:ImageButton></td>
            <td nowrap="nowrap" style="height: 93px" width="99%">
                <ajax:AjaxPanel ID="Ajaxpanel14" runat="server">
                    &nbsp; &nbsp;</ajax:AjaxPanel>
                <asp:ImageButton ID="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton" /></td>
        </tr>
    </table>
</asp:Content>
