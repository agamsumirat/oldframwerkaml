<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReportManagementOverdueWeekly.aspx.vb" Inherits="ReportManagementOverdueWeekly" %>



<%@ Register Src="webcontrol/footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="webcontrol/menu.ascx" TagName="menu" TagPrefix="uc1" %>
<%@ Register Src="webcontrol/userinfo.ascx" TagName="userinfo" TagPrefix="uc1" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>:: Anti Money Laundering ::</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="theme/aml.css" rel="stylesheet" type="text/css">
	<script src="script/x_load.js"></script>
	<script language="javascript">xInclude('script/x_core.js');</script>
	<script src="script/stm31.js"></script>	
	<script src="script/aml.js"></script>
</head>

<body leftmargin="0" topmargin="0" scroll="no" background="images/main-contentbg.gif">
<form id="Form1" runat="server">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td id="header"><table background="images/main-header-bg.gif" height="81" width="100%" cellpadding="0" cellspacing="0" border="0">
  	    <tr>
            <td width="153"><img src="images/main-uang-dijemur-dan-logo.jpg" width="153" height="81" /></td>
  	        <td width="357"><img src="images/main-title.jpg" width="357" height="25" /></td>
  	        <td width="99%">&nbsp;</td>
  	        <td align="right" background="images/main-bar-merah-kanan.jpg" valign="top" class="toprightmenu"><img src="images/blank.gif" width="425" height="6"><br>
                  <uc1:userinfo ID="UserInfo1" runat="server" />
                </td>
  	    </tr>
  	</table></td>
  </tr>
  <tr>
  	  <td><uc1:menu ID="Menu1" runat="server" /></td>
  </tr>
  <tr>
  	  <td id="tdcontent" height="99%" valign="top"><div id="divcontent" class="divcontent" ><table cellpadding="0" cellspacing="0" border="0" width="100%">
	  	<tr>
			<td background="images/main-menu-bg-shadow.gif" height="10"></td>
		</tr>
		<tr>
			<td class="divcontentinside">
                <ajax:ajaxpanel id="a" runat="server"  Width="100%"> <asp:validationsummary id="ValidationSummary1" runat="server" HeaderText="There were errors on the page:" Width="100%" CssClass="validation" ></asp:validationsummary></ajax:ajaxpanel>
	<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong> Report Management Overdue Weekly&nbsp;
                    <hr />
                </strong>
                </td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px"></td>
			<td bgColor="#ffffff" style="height: 24px" colspan="3">
                <strong><span style="color: #ff0000">
                    <object id="ChartSpace1" classid="clsid:0002E55D-0000-0000-C000-000000000046" style="width: 100%;
                                    height: 350px" viewastext="">
                                    <param name="XMLData" value='<xml xmlns:x="urn:schemas-microsoft-com:office:excel">&#13;&#10; <x:ChartSpace>&#13;&#10;  <x:OWCVersion>10.0.0.6619         </x:OWCVersion>&#13;&#10;  <x:Width>12621</x:Width>&#13;&#10;  <x:Height>9260</x:Height>&#13;&#10;  <x:AllowPropertyBrowser/>&#13;&#10;  <x:DisplayFieldList/>&#13;&#10;  <x:Palette>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#000000</x:Entry>&#13;&#10;   <x:Entry>#8080FF</x:Entry>&#13;&#10;   <x:Entry>#802060</x:Entry>&#13;&#10;   <x:Entry>#FFFFA0</x:Entry>&#13;&#10;   <x:Entry>#A0E0E0</x:Entry>&#13;&#10;   <x:Entry>#600080</x:Entry>&#13;&#10;   <x:Entry>#FF8080</x:Entry>&#13;&#10;   <x:Entry>#008080</x:Entry>&#13;&#10;   <x:Entry>#C0C0FF</x:Entry>&#13;&#10;   <x:Entry>#000080</x:Entry>&#13;&#10;   <x:Entry>#FF00FF</x:Entry>&#13;&#10;   <x:Entry>#80FFFF</x:Entry>&#13;&#10;   <x:Entry>#0080FF</x:Entry>&#13;&#10;   <x:Entry>#FF8080</x:Entry>&#13;&#10;   <x:Entry>#C0FF80</x:Entry>&#13;&#10;   <x:Entry>#FFC0FF</x:Entry>&#13;&#10;   <x:Entry>#FF80FF</x:Entry>&#13;&#10;  </x:Palette>&#13;&#10;  <x:DefaultFont>Arial</x:DefaultFont>&#13;&#10;  <x:Interior>&#13;&#10;   <x:FillEffect>&#13;&#10;    <x:PicturePlacement>Front</x:PicturePlacement>&#13;&#10;    <x:PicturePlacement>Side</x:PicturePlacement>&#13;&#10;    <x:PicturePlacement>End</x:PicturePlacement>&#13;&#10;    <x:fill x:type="tile" patternbuiltin="64"/>&#13;&#10;   </x:FillEffect>&#13;&#10;  </x:Interior>&#13;&#10;  <x:HasSelectionFeedback>True</x:HasSelectionFeedback>&#13;&#10;  <x:DisplayToolbar/>&#13;&#10; </x:ChartSpace>&#13;&#10;</xml>'>
                                    <param name="ScreenUpdating" value="-1">
                                    <param name="EnableEvents" value="-1">
                                </object>  
		    
                </span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 11px;" rowspan="6">
                &nbsp;</td>
			<td bgColor="#ffffff" rowspan="6" style="height: 11px" colspan="3">
			<object  id="PivotTable1" classid="clsid:0002E55A-0000-0000-C000-000000000046" style="font-size: xx-small;
                                    width: 737px; font-family: Arial Narrow; height: 217px" viewastext="" width="100%">
                                    <param name="XMLData" value='<xml xmlns:x="urn:schemas-microsoft-com:office:excel">&#13;&#10; <x:PivotTable>&#13;&#10;  <x:OWCVersion>10.0.0.6619         </x:OWCVersion>&#13;&#10;  <x:DisplayScreenTips/>&#13;&#10;  <x:NoAllowPropertyToolbox/>&#13;&#10;  <x:CubeProvider>msolap.2</x:CubeProvider>&#13;&#10;  <x:CacheDetails/>&#13;&#10;  <x:PivotView>&#13;&#10;   <x:IsNotFiltered/>&#13;&#10;   <x:AllowAdditions>false</x:AllowAdditions>&#13;&#10;   <x:AllowDeletions>false</x:AllowDeletions>&#13;&#10;  </x:PivotView>&#13;&#10;  <x:PivotAxis>&#13;&#10;   <x:Orientation>Filter</x:Orientation>&#13;&#10;   <x:Label>&#13;&#10;    <x:Caption>Drop Filter Fields Here</x:Caption>&#13;&#10;    <x:NotVisible/>&#13;&#10;   </x:Label>&#13;&#10;  </x:PivotAxis>&#13;&#10;  <x:PivotAxis>&#13;&#10;   <x:Orientation>Row</x:Orientation>&#13;&#10;   <x:Label>&#13;&#10;    <x:Caption>Drop Row Fields Here</x:Caption>&#13;&#10;    <x:NotVisible/>&#13;&#10;   </x:Label>&#13;&#10;  </x:PivotAxis>&#13;&#10;  <x:PivotAxis>&#13;&#10;   <x:Orientation>Column</x:Orientation>&#13;&#10;   <x:Label>&#13;&#10;    <x:Caption>Drop Column Fields Here</x:Caption>&#13;&#10;    <x:NotVisible/>&#13;&#10;   </x:Label>&#13;&#10;  </x:PivotAxis>&#13;&#10;  <x:PivotAxis>&#13;&#10;   <x:Orientation>Data</x:Orientation>&#13;&#10;   <x:Label>&#13;&#10;    <x:Caption>Drop Totals or Detail Fields Here</x:Caption>&#13;&#10;    <x:NotVisible/>&#13;&#10;   </x:Label>&#13;&#10;  </x:PivotAxis>&#13;&#10; </x:PivotTable>&#13;&#10;</xml>'>
                                </object>
            </td>
		</tr>
		
	</table>
	<script language="javascript" type="text/javascript">
			document.Form1.PivotTable1.XMLData=document.Form1.txtXMLData.value;
			//document.Form1.ChartSpace1.Clear();
			document.Form1.ChartSpace1.dataSource  =  document.Form1.PivotTable1;
		</script>
</td>
		</tr>
	  </table></div></td>
  </tr>
  <tr>
  	<td><uc1:footer ID="Footer1" runat="server" /></td>
  </tr>
</table></form><script>arrangeView();</script></body>
</html>








