#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports System.Collections.Generic
Imports System.IO
Imports Sahassa.AML.Commonly
Imports amlbll.ValidateBLL
Imports amlbll.auditTrailBLL
Imports System.Drawing
Imports System.web
#End Region

Partial Class IFTIView
    Inherits Parent
    Private BindGridFromExcel As Boolean

#Region " Property "
    Public Property SetName_IFTI() As String
        Get
            If Not Session("IFTI.SetName_IFTI") Is Nothing Then
                Return CStr(Session("IFTI.SetName_IFTI"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("IFTI.SetName_IFTI") = value
        End Set
    End Property
    Public Property SetTransactionSwift() As Integer
        Get
            If Not Session("IFTI.SetTransactionSwift") Is Nothing Then
                Return CStr(Session("IFTI.SetTransactionSwift"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As Integer)
            Session("IFTI.SetTransactionSwift") = value
        End Set
    End Property


    Public Property SetTypeTransaction() As Integer
        Get
            If Not Session("IFTI.SetTypeTransaction") Is Nothing Then
                Return CStr(Session("IFTI.SetTypeTransaction"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As Integer)
            Session("IFTI.SetTypeTransaction") = value
        End Set
    End Property

    Public Property SetCurrency() As Integer
        Get
            If Not Session("IFTI.SetCurrency") Is Nothing Then
                Return CStr(Session("IFTI.SetCurrency"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As Integer)
            Session("IFTI.SetCurrency") = value
        End Set
    End Property
    Public Property SetNominalBegin() As String
        Get
            If Not Session("IFTI.SetNominalBegin") Is Nothing Then
                Return CStr(Session("IFTI.SetNominalBegin"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("IFTI.SetNominalBegin") = value
        End Set
    End Property
    Public Property SetNominalEnd() As String
        Get
            If Not Session("IFTI.SetNominalEnd") Is Nothing Then
                Return CStr(Session("IFTI.SetNominalEnd"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("IFTI.SetNominalEnd") = value
        End Set
    End Property

    Public Property SetReportToPPATK() As Integer
        Get
            If Not Session("IFTI.SetReportToPPATK") Is Nothing Then
                Return CStr(Session("IFTI.SetReportToPPATK"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As Integer)
            Session("IFTI.SetReportToPPATK") = value
        End Set
    End Property
    Public Property SetAccount() As String
        Get
            If Not Session("IFTI.SetAccount") Is Nothing Then
                Return CStr(Session("IFTI.SetAccount"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("IFTI.SetAccount") = value
        End Set
    End Property

    Public Property SetLocalEquivalenBegin() As String
        Get
            If Not Session("IFTI.SetLocalEquivalenBegin") Is Nothing Then
                Return CStr(Session("IFTI.SetLocalEquivalenBegin"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("IFTI.SetLocalEquivalenBegin") = value
        End Set
    End Property
    Public Property SetLocalEquivalenEnd() As String
        Get
            If Not Session("IFTI.SetLocalEquivalenEnd") Is Nothing Then
                Return CStr(Session("IFTI.SetLocalEquivalenEnd"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("IFTI.SetLocalEquivalenEnd") = value
        End Set
    End Property
    Public Property SetDataValid() As Integer
        Get
            If Not Session("IFTI.SetDataValid") Is Nothing Then
                Return CStr(Session("IFTI.SetDataValid"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As Integer)
            Session("IFTI.SetDataValid") = value
        End Set
    End Property

    Public Property SetDateType() As Integer
        Get
            If Not Session("IFTI.SetDateType") Is Nothing Then
                Return CStr(Session("IFTI.SetDateType"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As Integer)
            Session("IFTI.SetDateType") = value
        End Set
    End Property
    Public Property SetDateBegin() As String
        Get
            If Not Session("IFTI.SetDateBegin") Is Nothing Then
                Return CStr(Session("IFTI.SetDateBegin"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("IFTI.SetDateBegin") = value
        End Set
    End Property
  
    Public Property SetDateEnd() As String
        Get
            If Not Session("IFTI.SetDateEnd") Is Nothing Then
                Return CStr(Session("IFTI.SetDateEnd"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("IFTI.SetDateEnd") = value
        End Set
    End Property

    Public Property SetAging() As String
        Get
            If Not Session("IFTI.SetAging") Is Nothing Then
                Return CStr(Session("IFTI.SetAging"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("IFTI.SetAging") = value
        End Set
    End Property
    Public Property SetPaymentDetail() As String
        Get
            If Not Session("IFTI.SetPaymentDetail") Is Nothing Then
                Return CStr(Session("IFTI.SetPaymentDetail"))
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("IFTI.SetPaymentDetail") = value
        End Set
    End Property
    Public Property SetSenderReference() As String
        Get
            If Not Session("IFTI.SetSenderReference") Is Nothing Then
                Return CStr(Session("IFTI.SetSenderReference"))
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("IFTI.SetSenderReference") = value
        End Set
    End Property
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("IFTIViewSelected") Is Nothing, New ArrayList, Session("IFTIViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("IFTIViewSelected") = value
        End Set
    End Property

    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("IFTIViewFieldSearch") Is Nothing, "", Session("IFTIViewFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("IFTIViewFieldSearch") = Value
        End Set
    End Property

    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("IFTIViewValueSearch") Is Nothing, "", Session("IFTIViewValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("IFTIViewValueSearch") = Value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("IFTIViewSort") Is Nothing, "Pk_IFTI_Id  asc", Session("IFTIViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("IFTIViewSort") = Value
        End Set
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("IFTIViewCurrentPage") Is Nothing, 0, Session("IFTIViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("IFTIViewCurrentPage") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("IFTIViewRowTotal") Is Nothing, 0, Session("IFTIViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("IFTIViewRowTotal") = Value
        End Set
    End Property

    'Private Function SetnGetBindTable() As VList(Of Vw_WICView)
    '    Return DataRepository.Vw_WICViewProvider.GetPaged(SearchFilter, SetnGetSort, SetnGetCurrentPage, Sahassa.aml.Commonly.SessionPagingLimit, SetnGetRowTotal)
    'End Function

    'Private Function SetnGetBindTableAll() As VList(Of Vw_WICView)
    '    Return DataRepository.Vw_WICViewProvider.GetPaged(SearchFilter, SetnGetSort, 0, Integer.MaxValue, SetnGetRowTotal)
    'End Function
    Public ReadOnly Property SetnGetBindTable(Optional ByVal AllRecord As Boolean = False) As VList(Of vw_IFTIView)
        Get
            Dim TotalDisplay As Integer = 0
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
                SetnGetCurrentPage = GetPageTotal - 1
            End If

            If SetName_IFTI.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Nama like '%" & SetName_IFTI.Trim.Replace("'", "''") & "%'"
            End If
            If SetTransactionSwift <> 0 Then
                If SetTransactionSwift = 1 Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "TransactionSwift like 'swift%'"
                Else
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "TransactionSwift like 'non%'"
                End If
            End If
            If SetTypeTransaction <> 0 Then
                If SetTypeTransaction = 1 Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "TransactionType like '%incoming%'"
                Else
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "TransactionType like '%outgoing%'"
                End If
            End If
            If Me.cboCurrency.SelectedValue <> 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "ValueDate_FK_Currency_ID = " & SetCurrency.ToString.Trim.Replace("'", "''")
            End If
            If SetNominalBegin.ToString.Trim.Length > 0 And SetNominalEnd.ToString.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "ValueDate_NilaiTransaksi between " & SetNominalBegin.ToString.Trim.Replace("'", "''") & " and " & SetNominalEnd.ToString.Trim.Replace("'", "''")
            End If
            If SetReportToPPATK <> 0 Then
                If SetReportToPPATK = 1 Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "ReportToPPATK = 1"
                Else
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "ReportToPPATK =0"
                End If
            End If
            If SetAccount.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "AccountNumber like '%" & SetAccount.Trim.Replace("'", "''") & "%'"
            End If
            If Me.SetSenderReference.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "SenderReference like '%" & SetSenderReference.Trim.Replace("'", "''") & "%'"
            End If

            If SetLocalEquivalenBegin.ToString.Trim.Length > 0 And SetLocalEquivalenEnd.ToString.Trim.Length = 0 Then
                Throw New Exception("Nilai Local Equivalen akhir harus diisi!")
            End If
            If SetLocalEquivalenBegin.ToString.Trim.Length = 0 And SetLocalEquivalenEnd.ToString.Trim.Length > 0 Then
                Throw New Exception("Nilai Local Equivalen awal harus diisi!")
            End If
            If SetLocalEquivalenBegin.ToString.Trim.Length > 0 And SetLocalEquivalenEnd.ToString.Trim.Length > 0 Then
                If SetLocalEquivalenBegin.ToString.Trim <= SetLocalEquivalenEnd.ToString.Trim Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "ValueDate_NilaiTransaksiIDR between " & SetLocalEquivalenBegin.ToString.Trim.Replace("'", "''") & " And " & SetLocalEquivalenEnd.ToString.Trim.Replace("'", "''")
                Else
                    Throw New Exception("Nilai Local Equivalen awal harus lebih kecil dari nilai Local Equivalen akhir!")
                End If
            End If

            If SetNominalBegin.ToString.Trim.Length > 0 And SetNominalEnd.ToString.Trim.Length = 0 Then
                Throw New Exception("Nilai nominal akhir harus diisi!")
            End If
            If SetNominalBegin.ToString.Trim.Length = 0 And SetNominalEnd.ToString.Trim.Length > 0 Then
                Throw New Exception("Nilai nominal awal harus diisi!")
            End If
            If SetNominalBegin.ToString.Trim.Length > 0 And SetNominalEnd.ToString.Trim.Length > 0 Then
                If SetNominalBegin.ToString.Trim <= SetNominalEnd.ToString.Trim Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "ValueDate_NilaiTransaksi between " & SetNominalBegin.ToString.Trim.Replace("'", "''") & " and " & SetNominalEnd.ToString.Trim.Replace("'", "''")
                Else
                    Throw New Exception("Nilai nominal awal harus lebih kecil dari nilai nominal akhir!")
                End If
            End If

            If SetDataValid <> 0 Then
                If SetDataValid = 1 Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "IsDataValid =1"
                Else
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "IsDataValid =0"
                End If
            End If
            If SetDateBegin.ToString.Trim.Length > 0 And SetDateEnd.ToString.Trim.Length > 0 Then
                If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", SetDateBegin) AndAlso Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", SetDateEnd) Then
                    Dim tanggal As String = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", SetDateBegin).ToString("yyyy-MM-dd")
                    Dim tanggalAkhir As String = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", SetDateEnd).ToString("yyyy-MM-dd")
                    If DateDiff(DateInterval.Day, Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", SetDateBegin), Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", SetDateEnd)) > -1 Then
                        If SetDateType = 0 Then
                            ReDim Preserve strWhereClause(strWhereClause.Length)
                            strWhereClause(strWhereClause.Length - 1) = "TanggalTransaksi between '" & tanggal.Trim.Replace("'", "''") & "' and '" & tanggalAkhir.Trim.Replace("'", "''") & "'"
                        Else
                            ReDim Preserve strWhereClause(strWhereClause.Length)
                            strWhereClause(strWhereClause.Length - 1) = "TanggalLaporan between '" & tanggal.Trim.Replace("'", "''") & "' and '" & tanggalAkhir.Trim.Replace("'", "''") & "'"
                        End If
                    Else
                        Throw New Exception("End date must later than begin date.")
                    End If
                End If

            End If
            If SetAging.ToString.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Aging = " & SetAging.ToString.Trim.Replace("'", "''") & ""
            End If
            If SetPaymentDetail.ToString.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "PaymentDetail like '%" & SetPaymentDetail.ToString.Trim.Replace("'", "''") & "%'"
            End If

            If AllRecord = False Then
                TotalDisplay = GetDisplayedTotalRow
            Else
                TotalDisplay = Integer.MaxValue
                SetnGetCurrentPage = 0
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            Return DataRepository.vw_IFTIViewProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, TotalDisplay, SetnGetRowTotal)
        End Get

    End Property

    Private Property SetAndGetSearchingCriteria() As String
        Get
            Return IIf(Session("IFTIViewSearchCriteria") Is Nothing, "", Session("IFTIViewSearchCriteria"))
        End Get
        Set(ByVal Value As String)
            Session("IFTIViewSearchCriteria") = Value
        End Set
    End Property

#End Region

#Region "Bind Combo Box"
    Private Sub BindCurrency()
        Try
            Using ObjList As New AMLBLL.IFTIBLL
                Me.cboCurrency.Items.Clear()
                Me.cboCurrency.DataSource = ObjList.GetViewIdType("", "IdCurrency", 0, Integer.MaxValue, 0)
                Me.cboCurrency.DataValueField = "IdCurrency"
                Me.cboCurrency.DataTextField = "Code"
                Me.cboCurrency.DataBind()
                Me.cboCurrency.Items.Insert(0, New ListItem("[All]", "0"))
                Me.cboCurrency.SelectedIndex = 0
            End Using
        Catch
            Throw
        End Try
    End Sub
#End Region


#Region "Function"

    Private Sub BindSelected()
        Dim Rows As New ArrayList

        For Each IdDIN As Int64 In SetnGetSelectedItem
            Using rowData As VList(Of vw_IFTIView) = DataRepository.vw_IFTIViewProvider.GetPaged(" PK_IFTI_ID = " & IdDIN & "", "", 0, Integer.MaxValue, 0)
                If rowData.Count > 0 Then
                    Rows.Add(rowData(0))
                End If
            End Using
        Next

        GridDataView.DataSource = Rows
        GridDataView.AllowPaging = False
        GridDataView.DataBind()
        For i As Integer = 0 To GridDataView.Items.Count - 1
            For y As Integer = 0 To GridDataView.Columns.Count - 1
                GridDataView.Items(i).Cells(y).Attributes.Add("class", "text")
            Next
        Next

        HideButtonGrid()

    End Sub

    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                Dim PKMsUserId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = SetnGetSelectedItem
                If ArrTarget.Contains(PKMsUserId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKMsUserId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKMsUserId)
                End If
                SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    Private Sub CollectSelected2()
        Dim i As Integer = 0
        Dim j As Integer = 0
        For Each gridRow As DataGridItem In GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                j = j + 1
                If chkBox.Checked = False Then
                    i = i + 1
                End If
                Dim PKMsUserId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = SetnGetSelectedItem
                If ArrTarget.Contains(PKMsUserId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKMsUserId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKMsUserId)
                End If
                SetnGetSelectedItem = ArrTarget
            End If

        Next
        If i = j Then
            Throw New Exception("No Data Selected")
        End If

    End Sub
    Private Sub SettingControlSearching()
        Me.TxtFullName.Text = SetName_IFTI
        Me.rbTransactionSwift.SelectedValue = SetTransactionSwift
        Me.rbTransactionType.SelectedValue = SetTypeTransaction

        Me.cboCurrency.SelectedValue = CInt(SetCurrency)

        Me.txtNominalBegin.Text = SetNominalBegin
        Me.txtNominalEnd.Text = SetNominalEnd
        Me.rbReporttoPPATK.SelectedValue = SetReportToPPATK
        Me.txtAccountNumber.Text = SetAccount
        Me.txtLocalEquivalenBegin.Text = SetLocalEquivalenBegin
        Me.txtLocalEquivalenEnd.Text = SetLocalEquivalenEnd
        Me.rbDataValid.SelectedValue = SetDataValid
        Me.rbDateType.SelectedValue = SetDateType
        Me.txtTransactionDateBegin.Text = SetDateBegin
        Me.txtTransactionDateEnd.Text = SetDateEnd
        Me.txtAging.Text = SetAging
        Me.txtPaymentDetail.Text = SetPaymentDetail
        Me.txtSenderReference.Text = SetSenderReference
    End Sub
    Private Sub SettingPropertySearching()
        SetnGetCurrentPage = 0
        SetName_IFTI = Me.TxtFullName.Text.Trim
        SetTransactionSwift = Me.rbTransactionSwift.SelectedValue
        SetTypeTransaction = Me.rbTransactionType.SelectedValue
        SetCurrency = Me.cboCurrency.SelectedValue
        SetNominalBegin = Me.txtNominalBegin.Text.Trim
        SetNominalEnd = Me.txtNominalEnd.Text.Trim
        SetReportToPPATK = Me.rbReporttoPPATK.SelectedValue
        SetAccount = Me.txtAccountNumber.Text.Trim
        SetLocalEquivalenBegin = Me.txtLocalEquivalenBegin.Text.Trim
        SetLocalEquivalenEnd = Me.txtLocalEquivalenEnd.Text.Trim
        SetDataValid = Me.rbDataValid.SelectedValue
        SetDateType = Me.rbDateType.SelectedValue
        SetDateBegin = Me.txtTransactionDateBegin.Text.Trim
        SetDateEnd = Me.txtTransactionDateEnd.Text.Trim
        SetAging = Me.txtAging.Text.Trim
        SetPaymentDetail = Me.txtPaymentDetail.Text.Trim
        SetSenderReference = Me.txtSenderReference.Text.Trim
    End Sub

    Private Sub ClearThisPageSessions()
        Clearproperty()
        'LblMessage.Visible = False
        'LblMessage.Text = ""
        SetnGetRowTotal = Nothing
        SetnGetSort = Nothing
        SetnGetCurrentPage = 0
    End Sub

    Private Sub Clearproperty()
        SetName_IFTI = Nothing
        SetTransactionSwift = Nothing
        SetTypeTransaction = Nothing
        SetCurrency = Nothing
        SetNominalBegin = Nothing
        SetNominalEnd = Nothing
        SetReportToPPATK = Nothing
        SetAccount = Nothing
        SetLocalEquivalenBegin = Nothing
        SetLocalEquivalenEnd = Nothing
        SetDataValid = Nothing
        SetDateType = Nothing
        SetDateBegin = Nothing
        SetDateEnd = Nothing
        SetAging = Nothing
        SetPaymentDetail = Nothing
        SetSenderReference = Nothing
        'SetBentukBidangUsaha = Nothing
        'SetActivation = 2
        SetnGetCurrentPage = Nothing
        SetnGetSelectedItem = Nothing
    End Sub

    Private Sub bindgrid()
        SettingControlSearching()
        'ValidateBLL.ButtonAccess2(GridDataView, "IFTI_Approval_view.aspx")
        GridDataView.DataSource = SetnGetBindTable
        GridDataView.CurrentPageIndex = SetnGetCurrentPage
        GridDataView.VirtualItemCount = SetnGetRowTotal
        GridDataView.DataBind()
        If SetnGetRowTotal > 0 Then
            Me.LabelDataNotFound.Visible = False
        Else
            LabelDataNotFound.Visible = True
        End If
    End Sub

#End Region

#Region "events..."



    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IsPostBack Then

                Me.ClearThisPageSessions()

                Me.popUpTransactionDateBegin.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTransactionDateBegin.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUpTransactionDateBegin.Style.Add("display", "")
                Me.popUpTransactionDateEnd.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTransactionDateEnd.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUpTransactionDateEnd.Style.Add("display", "")
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                End Using

                Me.GridDataView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                BindCurrency()
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : SetnGetCurrentPage = 0
                Case "Prev" : SetnGetCurrentPage -= 1
                Case "Next" : SetnGetCurrentPage += 1
                Case "Last" : SetnGetCurrentPage = GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Co mmandname " & e.CommandName)
            End Select
            CollectSelected()
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            SettingPropertySearching()

        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImagebuttonClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImagebuttonClear.Click
        Try
            Clearproperty()
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub SetInfoNavigate()
        PageCurrentPage.Text = (SetnGetCurrentPage + 1).ToString
        PageTotalPages.Text = GetPageTotal
        PageTotalRows.Text = SetnGetRowTotal.ToString
        LinkButtonNext.Enabled = (Not SetnGetCurrentPage + 1 = GetPageTotal) AndAlso GetPageTotal <> 0
        LinkButtonLast.Enabled = (Not SetnGetCurrentPage + 1 = GetPageTotal) AndAlso GetPageTotal <> 0
        Me.LinkButtonFirst.Enabled = Not SetnGetCurrentPage = 0
        LinkButtonPrevious.Enabled = Not SetnGetCurrentPage = 0
    End Sub
    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim TotalRow As Int16 = 0
        For Each gridRow As DataGridItem In GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                TotalRow = CType(TotalRow + 1, Int16)
            End If
        Next
        If TotalRow = 0 Then
            CheckBoxSelectAll.Checked = False
        Else
            If i = TotalRow Then
                CheckBoxSelectAll.Checked = True
            Else
                CheckBoxSelectAll.Checked = False
            End If
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            bindgrid()
            SetInfoNavigate()
            SetCheckedAll()
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In GridDataView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim pkid As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = SetnGetSelectedItem
        '        If SetnGetSelectedItem.Contains(pkid) Then
        '            If CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            Else
        '                ArrTarget.Remove(pkid)
        '            End If
        '        Else
        '            If CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            End If
        '        End If
        '        SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub

    Protected Sub GridDataView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridDataView.EditCommand
        Dim PKIFTI As Integer
        Try
            PKIFTI = CInt(e.Item.Cells(1).Text)
            Select Case (e.Item.Cells(22).Text)
                Case 1
                    Response.Redirect("IFTI_Edit_Swift_Outgoing.aspx?ID=" & PKIFTI, False)
                Case 2
                    Response.Redirect("IFTI_Edit_Swift_Incoming.aspx?ID=" & PKIFTI, False)
                Case 3
                    Response.Redirect("IFTI_Edit_NonSwift_Outgoing.aspx?ID=" & PKIFTI, False)
                Case 4
                    Response.Redirect("IFTI_Edit_NonSwift_Incoming.aspx?ID=" & PKIFTI, False)
            End Select
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try


    End Sub

    Protected Sub GridDataView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridDataView.ItemCommand
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Select Case e.CommandName
                Case "detail"
                    Try
                        Dim pkifti As Integer
                        pkifti = CInt(e.Item.Cells(1).Text)
                        Select Case (e.Item.Cells(22).Text)
                            Case 1
                                Response.Redirect("IFTI_Detail_Swift_Outgoing.aspx?ID=" & pkifti, False)
                            Case 2
                                Response.Redirect("IFTI_Detail_Swift_Incoming.aspx?ID=" & pkifti, False)
                            Case 3
                                Response.Redirect("IFTI_Detail_NonSwift_Outgoing.aspx?ID=" & pkifti, False)
                            Case 4
                                Response.Redirect("IFTI_Detail_NonSwift_Incoming.aspx?ID=" & pkifti, False)
                        End Select
                    Catch ex As Exception
                        LogError(ex)
                        cvalPageErr.IsValid = False
                        cvalPageErr.ErrorMessage = ex.Message
                    End Try
            End Select
        End If
    End Sub
    Protected Sub GridDataView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridDataView.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                chkBox.Checked = SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (SetnGetCurrentPage * GetDisplayedTotalRow))
                End If

                Try
                    If e.Item.Cells(CollCount - 4).Text = True Then
                        e.Item.Cells(CollCount - 4).Text = "Yes"
                    Else
                        e.Item.Cells(CollCount - 4).Text = "No"
                    End If
                Catch ex As Exception
                    e.Item.Cells(CollCount - 4).Text = "-"
                End Try
                'Try
                '    Dim objerrorDesc As TList(Of IFTI_ErrorDescription) = DataRepository.IFTI_ErrorDescriptionProvider.GetPaged("FK_IFTI_ID=" & e.Item.Cells(1).Text, "", 0, Integer.MaxValue, 0)
                '    Dim errorDesc As String = ""

                '    For i As Integer = 0 To objerrorDesc.Count - 1
                '        errorDesc = errorDesc & objerrorDesc(i).ErrorDescription & "</br>"
                '    Next
                '    e.Item.Cells(CollCount - 3).Text = errorDesc

                'Catch ex As Exception
                '    e.Item.Cells(CollCount - 3).Text = "-"
                'End Try
                Dim obj As BulletedList = e.Item.Cells(CollCount - 5).FindControl("BLErrorDescription")
                obj.DataSource = DataRepository.IFTI_ErrorDescriptionProvider.GetPaged("FK_IFTI_ID=" & e.Item.Cells(1).Text, "", 0, Integer.MaxValue, 0)
                obj.DataTextField = "ErrorDescription"
                obj.DataBind()

                'Try
                '    If e.Item.Cells(CollCount - 5).Text = True Then
                '        e.Item.Cells(CollCount - 5).Text = "Yes"
                '    Else
                '        e.Item.Cells(CollCount - 5).Text = "No"
                '    End If
                'Catch ex As Exception
                '    e.Item.Cells(CollCount - 5).Text = "-"
                'End Try
                Try
                    If e.Item.Cells(CollCount - 8).Text = True Then
                        e.Item.Cells(CollCount - 8).Text = "Yes"
                    Else
                        e.Item.Cells(CollCount - 8).Text = "No"
                    End If
                Catch ex As Exception
                    e.Item.Cells(CollCount - 8).Text = "-"
                End Try
                Try
                    If e.Item.Cells(CollCount - 13).Text = True Then
                        e.Item.Cells(CollCount - 13).Text = "Yes"
                    Else
                        e.Item.Cells(CollCount - 13).Text = "No"
                    End If
                Catch ex As Exception
                    e.Item.Cells(CollCount - 13).Text = "-"
                End Try
                Try

                    Dim objcurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged("IdCurrency='" & e.Item.Cells(7).Text & "'", "", 0, 1, 0)
                    If objcurrency.Count > 0 Then
                        e.Item.Cells(7).Text = objcurrency(0).Name
                    End If


                Catch ex As Exception
                    e.Item.Cells(7).Text = "-"
                End Try
                Using CekIFTI_ApprovalDetail As TList(Of IFTI_Approval_Detail) = DataRepository.IFTI_Approval_DetailProvider.GetPaged("PK_IFTI_ID = '" & CInt(e.Item.Cells(1).Text) & "'", "", 0, Integer.MaxValue, 0)
                    If CekIFTI_ApprovalDetail.Count > 0 Then
                        'e.Item.Cells(CollCount - 3).Text = "Pending Approval"
                        e.Item.Cells(CollCount - 1).Enabled = False
                        'e.Item.Cells(CollCount - 2).Enabled = False
                    Else
                        e.Item.Cells(CollCount - 1).Enabled = True
                    End If

                End Using
            End If
            Me.GridDataView.Columns(CollCount - 2).Visible = True
            Me.GridDataView.Columns(CollCount).Visible = False
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub GridDataView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridDataView.PageIndexChanged
        Me.GridDataView.CurrentPageIndex = e.NewPageIndex
    End Sub
    Protected Sub GridDataView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridDataView.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            SetnGetSort = ChangeSortCommand(e.SortExpression)
            GridUser.Columns(IndexSort(GridUser, e.SortExpression)).SortExpression = SetnGetSort
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub HideButtonGrid()
        With GridDataView
            '.Columns(.Columns.Count - 4).Visible = False
            .Columns(.Columns.Count - 3).Visible = False
            .Columns(.Columns.Count - 1).Visible = False
            .Columns(.Columns.Count - 2).Visible = False
        End With
    End Sub
    Private Sub BindSelectedAll()
        Try
            GridDataView.DataSource = SetnGetBindTable(True)
            GridDataView.AllowPaging = False
            GridDataView.DataBind()

            For i As Integer = 0 To GridDataView.Items.Count - 1
                For y As Integer = 0 To GridDataView.Columns.Count - 1
                    GridDataView.Items(i).Cells(y).Attributes.Add("class", "text")
                Next
            Next
            HideButtonGrid()
        Catch
            Throw
        End Try


    End Sub
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                    'Skip
                End Try
                If control.GetType.ToString <> "System.Web.UI.WebControls.BulletedList" Then
                    control.Parent.Controls.Remove(control)
                End If

            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    If control.GetType.ToString <> "System.Web.UI.WebControls.BulletedList" Then
                        control.Parent.Controls.Remove(control)
                    End If

                End If
            End If
        End If
    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered 

    End Sub
    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) AndAlso (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    'Throw New Exception("SahassaCommonly.CommonlyEnum.EXECPTION_PAGENUMBER_GREATER_TOTALPAGENUMBER")
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                'Throw New Exception("SahassaCommonly.CommonlyEnum.EXECPTION_PAGENUMBER_NUMERICPOSITIF")
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
            CollectSelected()
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    'Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
    '    If CvalHandleErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub
    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageErr.PreRender
        If cvalPageErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub


    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            BindGridFromExcel = True
            Me.CollectSelected()
            Me.BindSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Response.Clear()
            Response.ClearHeaders()
            Response.Charset = ""
            Response.AddHeader("content-disposition", "attachment;filename=IFTIView.xls")

            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridDataView)
            GridDataView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub lnkExportAllData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAllData.Click
        Try
            Me.BindSelectedAll()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Response.Clear()
            Response.ClearHeaders()
            Response.Charset = ""
            Response.AddHeader("content-disposition", "attachment;filename=IFTIView.xls")

            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridDataView)
            GridDataView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


#End Region
  
End Class