Imports System.Data
Imports System.Data.SqlClient

Partial Class ProposalSTRWorkflowSettings
    Inherits Parent

#Region "Property"
    ''' <summary>
    ''' SetnGet TxtApprovers ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetTxtApproversID_STR() As String
        Get
            Return Session("TxtApproversID_STR")
        End Get
        Set(ByVal value As String)
            Session("TxtApproversID_STR") = value
        End Set
    End Property
    ''' <summary>
    ''' SetnGet TxtNotifyOthersID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetTxtNotifyOthersID_STR()
        Get
            Return Session("TxtNotifyOthersID_STR")
        End Get
        Set(ByVal value)
            Session("TxtNotifyOthersID_STR") = value
        End Set
    End Property
    ''' <summary>
    ''' SetnGetApprovers Values
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetApproversValues_STR() As ArrayList
        Get
            Return Session("ArrApproversValues_STR")
        End Get
        Set(ByVal value As ArrayList)
            Session("ArrApproversValues_STR") = value
        End Set
    End Property
    ''' <summary>
    ''' SetnGet DueDateValues
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetDueDateValues_STR() As ArrayList
        Get
            Return Session("ArrDueDateValues_STR")
        End Get
        Set(ByVal value As ArrayList)
            Session("ArrDueDateValues_STR") = value
        End Set
    End Property
    ''' <summary>
    ''' SetnGetNotifyOthers Values
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetNotifyOthersValues_STR() As ArrayList
        Get
            Return Session("ArrNotifyOthersValues_STR")
        End Get
        Set(ByVal value As ArrayList)
            Session("ArrNotifyOthersValues_STR") = value
        End Set
    End Property


    ''' <summary>
    ''' SetnGetEmailTemplate
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetEmailTemplate_STR() As Data.DataTable
        Get
            Return Session("EmailTemplate_STR")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("EmailTemplate_STR") = value
        End Set
    End Property
    ''' <summary>
    ''' SetnGetValuesGeneralCaseManagement
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRootDetail_STR() As Data.DataTable
        Get
            Return Session("RootDetail_STR")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("RootDetail_STR") = value
        End Set
    End Property
    Private Property SetnGetRootGeneral_STR() As Data.DataTable
        Get
            Return Session("RootGeneral_STR")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("RootGeneral_STR") = value
        End Set
    End Property
    ''' <summary>
    ''' SetnGetMode
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetMode_STR() As String
        Get
            Return Session("Mode_STR")
        End Get
        Set(ByVal value As String)
            Session("Mode_STR") = value
        End Set
    End Property
    ''' <summary>
    ''' Is Updated
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SetnGetIsUpdated_STR() As Boolean
        Get
            If Session("IsUpdated_STR") Is Nothing Then
                Return False
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            Session("IsUpdated_STR") = value
        End Set
    End Property
    ''' <summary>
    ''' OldGeneralValues
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetOldGeneralValues_STR() As Data.DataTable
        Get
            Return Session("OldGeneralValues_STR")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("OldGeneralValues_STR") = value
        End Set
    End Property

    Private Property SetnGetOldDetailValues_STR() As Data.DataTable
        Get
            Return Session("OldDetailValues_STR")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("OldDetailValues_STR") = value
        End Set
    End Property

    Private Property SetnGetOldEmailTemplateValues_STR() As Data.DataTable
        Get
            Return Session("OldEmailTemplateValues_STR")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("OldEmailTemplateValues_STR") = value
        End Set
    End Property

    Private Property SetnGetJmlSerial() As Integer
        Get
            Return Session("JmlSerial")
        End Get
        Set(ByVal value As Integer)
            Session("JmlSerial") = value
        End Set
    End Property
#End Region

#Region "SetnGet Value Approvers, DueDate(days), dan NotifyOthers serta General"
    ''' <summary>
    ''' Set Temp Values
    ''' </summary>
    ''' <param name="GridviewCategory"></param>
    ''' <param name="ArrayTempValues"></param>
    ''' <remarks></remarks>
    Private Sub SetTempValues(ByVal GridviewCategory As GridView, ByRef ArrayTempValues As ArrayList)
        Try
            If GridviewCategory.Rows.Count > 0 AndAlso ArrayTempValues IsNot Nothing Then
                For i As Integer = 0 To ArrayTempValues.Count - 1
                    Dim RowsCategory As GridViewRow = GridviewCategory.Rows(i)
                    Dim TxtCategory As TextBox = RowsCategory.Cells(0).Controls(1)
                    TxtCategory.Text = ArrayTempValues.Item(i).ToString
                Next
                ' Clear session
                ArrayTempValues.Clear()
                ArrayTempValues = Nothing
            End If
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Get Temp Values
    ''' </summary>
    ''' <param name="GridviewCategory"></param>
    ''' <param name="Category"></param>
    ''' <remarks></remarks>
    Private Sub GetTempValues(ByVal GridviewCategory As GridView, ByVal Category As String, ByVal intParalel As Integer)
        Try
            If GridviewCategory.Rows.Count > 0 Then
                Dim oArrValueCategory As New ArrayList
                Dim IntCounter As Integer = 0
                If intParalel < GridviewCategory.Rows.Count Then
                    IntCounter = intParalel - 1
                Else
                    IntCounter = GridviewCategory.Rows.Count - 1
                End If
                For i As Integer = 0 To IntCounter
                    Dim RowCategory As GridViewRow = GridviewCategory.Rows(i)
                    Dim TxtCategory As TextBox = RowCategory.Cells(0).Controls(1)
                    oArrValueCategory.Add(TxtCategory.Text)
                Next
                Select Case Category.ToLower
                    Case "approvers"
                        Me.SetnGetApproversValues_STR = oArrValueCategory
                    Case "duedate"
                        Me.SetnGetDueDateValues_STR = oArrValueCategory
                    Case "notifyothers"
                        Me.SetnGetNotifyOthersValues_STR = oArrValueCategory
                End Select
            End If
        Catch
            Throw
        End Try
    End Sub


    ''' <summary>
    ''' Temp values
    ''' </summary>
    ''' <param name="RowIndexFirstGrid"></param>
    ''' <param name="Actifity"></param>
    ''' <param name="Category"></param>
    ''' <remarks></remarks>
    Private Sub TempValues(ByVal RowIndexFirstGrid As Integer, ByVal Actifity As String, ByVal Category As String, Optional ByVal intParalel As Integer = 0)
        Try
            Dim FirstRowsGrid As GridViewRow = GridViewParent.Rows(RowIndexFirstGrid) ' looping row in first grid 
            Dim GridApprovers As GridView = FirstRowsGrid.Cells(3).Controls(1)
            Dim GridDueDate As GridView = FirstRowsGrid.Cells(4).Controls(1)
            Dim GridNotifyOthers As GridView = FirstRowsGrid.Cells(5).Controls(1)
            Select Case Actifity.ToLower
                Case "set"
                    Select Case Category.ToLower
                        Case "approvers"
                            Me.SetTempValues(GridApprovers, Me.SetnGetApproversValues_STR)
                        Case "duedate"
                            Me.SetTempValues(GridDueDate, Me.SetnGetDueDateValues_STR)
                        Case "notifyothers"
                            Me.SetTempValues(GridNotifyOthers, Me.SetnGetNotifyOthersValues_STR)
                    End Select
                Case "get"
                    Select Case Category.ToLower
                        Case "approvers"
                            Me.GetTempValues(GridApprovers, "Approvers", intParalel)
                        Case "duedate"
                            Me.GetTempValues(GridDueDate, "DueDate", intParalel)
                        Case "notifyothers"
                            Me.GetTempValues(GridNotifyOthers, "NotifyOthers", intParalel)
                    End Select
            End Select
        Catch
            Throw
        End Try
    End Sub

#End Region

#Region "Pick UserID"
    ''' <summary>
    ''' ButtonApprovers Init
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ButtonApprovers_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim BtnApprovers As Button = sender
            Dim GridApproversRow As GridViewRow = CType(BtnApprovers.NamingContainer, GridViewRow)
            Dim TxtApprovers As TextBox = GridApproversRow.Cells(0).Controls(1)
            BtnApprovers.Attributes.Add("onClick", "return popWin('" & TxtApprovers.ClientID & "');")
            'Me.SetnGetClientID = TxtApprovers.ClientID
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    ''' <summary>
    ''' BtnNotifyOther Init
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub BtnNotifyOther_Init(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim BtnNotifyOther As Button = sender
            Dim GridNotifyOthersRow As GridViewRow = CType(BtnNotifyOther.NamingContainer, GridViewRow)

            Dim TxtNotifyOthers As TextBox = GridNotifyOthersRow.Cells(0).Controls(1)
            BtnNotifyOther.Attributes.Add("onClick", "return popWin('" & TxtNotifyOthers.ClientID & "');")
            'Me.SetnGetClientID = TxtNotifyOthers.ClientID
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

#Region "Load Data and PageLoad"
    ''' <summary>
    ''' Create First grid
    ''' </summary>
    ''' <param name="JmlUpdate"></param>
    ''' <remarks></remarks>
    Private Sub CreateFirstTable(ByVal JmlUpdate As Integer)
        Try
            Dim DtTableFirst As New Data.DataTable
            DtTableFirst.Columns.Add(New Data.DataColumn("IDSerial"))
            DtTableFirst.Columns.Add(New Data.DataColumn("Workflow Step"))
            DtTableFirst.Columns.Add(New Data.DataColumn("OfParalel"))
            DtTableFirst.Columns.Add(New Data.DataColumn("Approvers"))
            DtTableFirst.Columns.Add(New Data.DataColumn("Due Date(days)"))
            DtTableFirst.Columns.Add(New Data.DataColumn("Notify Others"))
            DtTableFirst.Columns.Add(New Data.DataColumn("Change Email Template"))
            For i As Integer = 1 To JmlUpdate
                Dim Rows As Data.DataRow = DtTableFirst.NewRow
                Rows(0) = i
                DtTableFirst.Rows.Add(Rows)
            Next
            Me.GridViewParent.DataSource = DtTableFirst
            Me.GridViewParent.DataBind()
            Me.DisableOrEnableBtnEmailTemplate(False)
        Catch
            Throw
        End Try
    End Sub

    Private Function IsEditProposalSTRWorkflow() As Boolean
        Try
            Using oAccessJmlSTR As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.CountProposalSTRWorkflowTableAdapter
                Dim JmlStr As Integer = oAccessJmlSTR.GetCount
                If JmlStr > 0 Then
                    Me.SetnGetJmlSerial = JmlStr
                    Return True
                Else
                    Me.SetnGetJmlSerial = 0
                    Return False
                End If
            End Using
        Catch
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageSave.Visible = False

                Dim DtRootGeneral As New Data.DataTable
                DtRootGeneral.Columns.Add(New Data.DataColumn("WorkflowName", GetType(String)))
                DtRootGeneral.Columns.Add(New Data.DataColumn("Paralel", GetType(Integer)))
                Me.SetnGetRootGeneral_STR = DtRootGeneral
                ' Check Status
                If IsEditProposalSTRWorkflow() Then
                    ' berarti dia sudah pernah dibuat
                    Me.GenerateCaseManagementWorkflowConfigurated()
                    Me.DisableOrEnableBtnEmailTemplate(True)
                    ' set value email template
                    Using oAccessEmailTemplate As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowEmailTemplateTableAdapter
                        Me.SetnGetEmailTemplate_STR = oAccessEmailTemplate.GetData()
                    End Using
                    Me.SetnGetMode_STR = "Edit"
                    Me.ImageSave.Visible = True
                Else
                    Me.SetnGetMode_STR = "Add"
                    Me.SetnGetEmailTemplate_STR = Nothing
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    ''' <summary>
    ''' Create First Grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageButtonUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonUpdate.Click
        Try
            If Me.TxtJumlahSerial.Text <> "" Then
                If Regex.IsMatch(Me.TxtJumlahSerial.Text, "\b\d+\b") = True Then
                    If Convert.ToInt16(Me.TxtJumlahSerial.Text) <= 0 Then
                        Throw New Exception("The Number of Serial is must be greater than 0.")
                    Else

                        'Checking Email Template jika Root terjadi pengurangan
                        If (Convert.ToInt16(Me.TxtJumlahSerial.Text) < Me.GridViewParent.Rows.Count) And Me.GridViewParent.Rows.Count <> 0 Then
                            If Me.SetnGetEmailTemplate_STR IsNot Nothing Then
                                If Me.SetnGetEmailTemplate_STR.Rows.Count <> 0 Then
                                    For i As Integer = Me.GridViewParent.Rows.Count - 1 To Convert.ToInt16(Me.TxtJumlahSerial.Text) Step -1
                                        Dim j As Integer = 0
                                        For Each RowEmail As Data.DataRow In Me.SetnGetEmailTemplate_STR.Rows
                                            If i + 1 = RowEmail(0) Then
                                                Me.SetnGetEmailTemplate_STR.Rows(j).Delete()
                                                Exit For
                                            End If
                                            j += 1
                                        Next
                                    Next
                                End If
                            End If
                        End If
                        ' Get value general
                        GetRootTempValuesGeneral()
                        Me.CreateFirstTable(Convert.ToInt16(Me.TxtJumlahSerial.Text))
                        SetRootTempValuesGeneral()
                        If Me.SetnGetIsUpdated_STR Then
                            Me.DisableOrEnableBtnEmailTemplate(True)
                        End If
                        Me.SetnGetIsUpdated_STR = True
                        Me.ImageSave.Visible = True
                    End If
                Else
                    Throw New Exception("The number of serial must be numeric.")
                End If
            Else
                Throw New Exception("The number of serial is required.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        Finally
            If SetnGetRootDetail_STR IsNot Nothing Then
                Me.SetnGetRootDetail_STR.Clear()
                Me.SetnGetRootDetail_STR = Nothing
            End If
        End Try
    End Sub
    ''' <summary>
    ''' GenerateCaseManagementWorkflowConfigurated
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GenerateCaseManagementWorkflowConfigurated()
        Try
           
            Me.TxtJumlahSerial.Text = Me.SetnGetJmlSerial
            'Create First Grid
            Me.CreateFirstTable(Me.SetnGetJmlSerial)

            ' Get Data General Case Management Workflow
            Using oAccessGetGeneral As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowGeneralTableAdapter
                Dim DtGeneral As Data.DataTable = oAccessGetGeneral.GetData()
                Me.SetnGetOldGeneralValues_STR = DtGeneral
                Me.SetValueToGridSerialWorkflowAndParalel(DtGeneral)
            End Using

            ' Get Data Detail Case Management Workflow
            Using oAccessGetDetail As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowDetailTableAdapter
                Dim DtDetail As Data.DataTable = oAccessGetDetail.GetData()
                Me.SetnGetOldDetailValues_STR = DtDetail
                Me.GenerateAndSetValueToGridApproversDueDateNotifyOthers(DtDetail)
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' SetValue To GridSerialWorkflowAndParalel
    ''' </summary>
    ''' <param name="DtGeneral"></param>
    ''' <remarks></remarks>
    Private Sub SetValueToGridSerialWorkflowAndParalel(ByVal DtGeneral As Data.DataTable)
        Try
            Dim Counter As Integer = 0
            For Each RowsGridSerialWorkflowAndParalel As GridViewRow In Me.GridViewParent.Rows
                Dim TxtWorkflowStepName As TextBox = CType(RowsGridSerialWorkflowAndParalel.Cells(1).Controls(1), TextBox)
                Dim TxtParalel As TextBox = CType(RowsGridSerialWorkflowAndParalel.Cells(2).Controls(1), TextBox)
                Dim dtGeneralCaseManagement As Data.DataTable = DtGeneral
                Dim RowGeneralCaseManagement As Data.DataRow = dtGeneralCaseManagement.Rows(Counter)
                TxtWorkflowStepName.Text = RowGeneralCaseManagement("WorkflowStep")
                TxtParalel.Text = RowGeneralCaseManagement("Paralel")
                Counter += 1
            Next
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Generate And SetValue To GridApproversDueDateNotifyOthers
    ''' </summary>
    ''' <param name="DtDetail"></param>
    ''' <remarks></remarks>
    Private Sub GenerateAndSetValueToGridApproversDueDateNotifyOthers(ByVal DtDetail As Data.DataTable)
        Try
            Dim IntLastRowIndex As Integer = 0
            For Each FirstRowsGrid As GridViewRow In GridViewParent.Rows ' looping row in first grid 

                Dim TxtParalel As TextBox = CType(FirstRowsGrid.Cells(2).Controls(1), TextBox)

                Dim GridApprovers As GridView = FirstRowsGrid.Cells(3).Controls(1) ' ambil id grid approvers
                Dim GridDueDate As GridView = FirstRowsGrid.Cells(4).Controls(1) ' ambil id grid DueDate
                Dim GridNotifyOthers As GridView = FirstRowsGrid.Cells(5).Controls(1) ' ambil id grid Notify Others

                CreateGridDetailCaseManagement(Convert.ToInt16(TxtParalel.Text), GridApprovers, GridDueDate, GridNotifyOthers)

                ' Set value
                For i As Integer = 0 To Convert.ToInt16(TxtParalel.Text) - 1
                    Dim Rows As Data.DataRow = DtDetail.Rows(IntLastRowIndex)

                    Dim RowsApproversGrid As GridViewRow = GridApprovers.Rows(i)
                    Dim RowsDueDate As GridViewRow = GridDueDate.Rows(i)
                    Dim RowsNotifyOthers As GridViewRow = GridNotifyOthers.Rows(i)

                    Dim TxtApprovers As TextBox = RowsApproversGrid.Cells(0).Controls(1)
                    Dim TxtDueDate As TextBox = RowsDueDate.Cells(0).Controls(1)
                    Dim TxtNotifyOthers As TextBox = RowsNotifyOthers.Cells(0).Controls(1)

                    TxtApprovers.Text = Me.GetUserIdByPk(Rows(1)) ' TXT Approvers
                    TxtDueDate.Text = Rows(2) ' TXT DueDate
                    TxtNotifyOthers.Text = Me.GetUserIdByPk(Rows(3)) ' TXT Notify Others
                    IntLastRowIndex += 1
                Next
            Next
        Catch
            Throw
        End Try
    End Sub
#End Region

#Region "Change Email Template"
    ''' <summary>
    ''' Disable/Enable Button Email
    ''' </summary>
    ''' <param name="boolEnable"></param>
    ''' <remarks></remarks>
    Public Sub DisableOrEnableBtnEmailTemplate(ByVal boolEnable As Boolean)
        Try
            For Each s As GridViewRow In Me.GridViewParent.Rows
                Dim BtnChangeEmailTemplate As Button = s.Cells(6).Controls(1) ' button change email template
                BtnChangeEmailTemplate.Enabled = boolEnable
            Next
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Change email template per serial
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub BtnChangeEmailTemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim BtnChangeEmailTemplate As Button = sender
            Dim FirstGridRow As GridViewRow = BtnChangeEmailTemplate.NamingContainer
            Dim oTextWorkFlowStep As TextBox = FirstGridRow.Cells(1).Controls(1)
            Dim WorkflowName As String = oTextWorkFlowStep.Text
            Dim Serial As String = FirstGridRow.Cells(0).Text

            MultiViewCaseManagement.ActiveViewIndex = 1
            Me.LblSerialEmail.Text = Serial
            Me.LblWorkflowStepNameEmail.Text = WorkflowName
            Me.LblHeader.Text = "Proposal STR Workflow Settings Email Template"

            'clear
            Me.TxtSubjectEmail.Text = ""
            Me.TxtBodyEmail.Text = ""

            If Me.SetnGetMode_STR = "Edit" Then
                ' edit
                If Me.SetnGetEmailTemplate_STR Is Nothing Then
                    Using oAccessEmailTemplate As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowEmailTemplateTableAdapter
                        Me.SetnGetEmailTemplate_STR = oAccessEmailTemplate.GetData()
                    End Using
                End If
                Dim Row() As Data.DataRow = Me.SetnGetEmailTemplate_STR.Select("SerialNo=" & Serial)
                If Row.Length > 0 Then
                    Me.TxtSubjectEmail.Text = Row(0)(1).ToString ' Subject Email Template
                    Me.TxtBodyEmail.Text = Row(0)(2).ToString ' Body Email Template
                End If
            Else
                If Me.SetnGetEmailTemplate_STR IsNot Nothing Then
                    For Each Row As Data.DataRow In Me.SetnGetEmailTemplate_STR.Rows
                        If Row(0).ToString = Me.LblSerialEmail.Text Then
                            Me.TxtSubjectEmail.Text = Row(1) ' SUBJECT
                            Me.TxtBodyEmail.Text = Row(2) ' BODY
                            Exit For
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' Cancel Email Template
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancelEmail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancelEmail.Click
        Me.MultiViewCaseManagement.ActiveViewIndex = 0
        Me.LblHeader.Text = "Proposal STR Workflow Settings"
    End Sub

    ''' <summary>
    ''' Save Email Template
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSaveEmail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSaveEmail.Click
        Try
            If Not Me.CheckValidEmailTemplate() Then
                Throw New Exception("Subject Email is required.")
            Else
                Dim DtEmailTemplate As Data.DataTable

                If Me.SetnGetEmailTemplate_STR Is Nothing Then
                    DtEmailTemplate = New Data.DataTable
                    DtEmailTemplate.Columns.Add(New Data.DataColumn("SerialNo", GetType(Integer)))
                    DtEmailTemplate.Columns.Add(New Data.DataColumn("Subject", GetType(String)))
                    DtEmailTemplate.Columns.Add(New Data.DataColumn("Body", GetType(String)))
                    Me.SaveCaseManagementEmailTemplate("Add", Convert.ToInt16(Me.LblSerialEmail.Text), Me.TxtSubjectEmail.Text, Me.TxtBodyEmail.Text, DtEmailTemplate)
                Else
                    '' cek jika sudah ada / edit email template (antara ADD ato Edit)
                    DtEmailTemplate = CType(Me.SetnGetEmailTemplate_STR, Data.DataTable)
                    Dim RowIndexEmail As Integer = 0
                    Dim flag As Boolean = False ' Add

                    For i As Integer = 0 To DtEmailTemplate.Rows.Count - 1
                        If DtEmailTemplate.Rows(i)(0).ToString = Me.LblSerialEmail.Text Then
                            RowIndexEmail = i
                            flag = True 'Edit
                            Exit For
                        End If
                    Next

                    If flag Then ' Edit
                        Me.SaveCaseManagementEmailTemplate("Edit", Convert.ToInt16(Me.LblSerialEmail.Text), Me.TxtSubjectEmail.Text, Me.TxtBodyEmail.Text, DtEmailTemplate, RowIndexEmail)
                    Else ' Add
                        Me.SaveCaseManagementEmailTemplate("Add", Convert.ToInt16(Me.LblSerialEmail.Text), Me.TxtSubjectEmail.Text, Me.TxtBodyEmail.Text, DtEmailTemplate)
                    End If
                End If
                ' Change View 
                Me.MultiViewCaseManagement.ActiveViewIndex = 0
                Me.LblHeader.Text = "Proposal STR Workflow Settings"
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    ''' <summary>
    ''' Save Email Template
    ''' </summary>
    ''' <param name="Mode"></param>
    ''' <param name="intSerial"></param>
    ''' <param name="strSubject"></param>
    ''' <param name="strBody"></param>
    ''' <remarks></remarks>
    Private Sub SaveCaseManagementEmailTemplate(ByVal Mode As String, ByVal intSerial As Integer, ByVal strSubject As String, ByVal strBody As String, ByVal DtEmail As Data.DataTable, Optional ByVal RowIndex As Integer = 0)
        Try
            Dim RowEmail As Data.DataRow
            If Mode = "Add" Then
                RowEmail = DtEmail.NewRow
                RowEmail(0) = intSerial
                RowEmail(1) = strSubject
                RowEmail(2) = strBody
                DtEmail.Rows.Add(RowEmail)
            ElseIf Mode = "Edit" Then
                RowEmail = DtEmail.Rows(RowIndex)
                RowEmail(0) = intSerial
                RowEmail(1) = strSubject
                RowEmail(2) = strBody
            End If
            Me.SetnGetEmailTemplate_STR = DtEmail
        Catch
            Throw
        End Try
    End Sub

#End Region

#Region "Generate Approvers, DueDate(days), dan NotifyOthers"
    ''' <summary>
    ''' CreateGrid Detail CaseManagement
    ''' </summary>
    ''' <param name="JmlParalel"></param>
    ''' <param name="ApproversGrid"></param>
    ''' <param name="DueDateGrid"></param>
    ''' <param name="NotifyOthersGrid"></param>
    ''' <remarks></remarks>
    Private Sub CreateGridDetailCaseManagement(ByVal JmlParalel As Integer, ByVal ApproversGrid As GridView, ByVal DueDateGrid As GridView, ByVal NotifyOthersGrid As GridView)
        Try
            Dim DtTableApprovers As New Data.DataTable
            Dim DtTableDueDate As New Data.DataTable
            Dim DtTableNotifyOthers As New Data.DataTable

            DtTableApprovers.Columns.Add(New Data.DataColumn(""))
            DtTableApprovers.Columns.Add(New Data.DataColumn("RowIndexApprovers"))
            DtTableNotifyOthers.Columns.Add(New Data.DataColumn(""))
            DtTableNotifyOthers.Columns.Add(New Data.DataColumn("RowIndexNotifyOthers"))

            For i As Integer = 1 To JmlParalel
                ' Approvers
                Dim ApproversRows As Data.DataRow = DtTableApprovers.NewRow
                DtTableApprovers.Rows.Add(ApproversRows)

                ' Due Date
                Dim DueDateRows As Data.DataRow = DtTableDueDate.NewRow
                DtTableDueDate.Rows.Add(DueDateRows)

                ' Notify Others
                Dim NotifyOthersRows As Data.DataRow = DtTableNotifyOthers.NewRow
                DtTableNotifyOthers.Rows.Add(NotifyOthersRows)
            Next

            ' Approvers
            ApproversGrid.DataSource = DtTableApprovers
            ApproversGrid.DataBind()

            ' Due Date
            DueDateGrid.DataSource = DtTableDueDate
            DueDateGrid.DataBind()

            ' Notify Others
            NotifyOthersGrid.DataSource = DtTableNotifyOthers
            NotifyOthersGrid.DataBind()

            '------------Clear Data Table----------------------------------------------------------------
            DtTableApprovers.Clear()
            DtTableDueDate.Clear()
            DtTableNotifyOthers.Clear()
            DtTableApprovers = Nothing
            DtTableDueDate = Nothing
            DtTableNotifyOthers = Nothing
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Generate
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageButtonParalel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim BtnParalel As ImageButton = sender
            Dim FirstGrid As GridViewRow = BtnParalel.NamingContainer
            Dim TxtPararel As TextBox = FirstGrid.Cells(2).Controls(1) ' column paralel dan ambil control textbox paralel
            Dim StrPattern As String = "\b\d+\b"

            If TxtPararel.Text = "" Then
                Throw New Exception("# of Paralel is empty. Please insert a number. ")
            ElseIf Not Regex.IsMatch(TxtPararel.Text, StrPattern) Then
                Throw New Exception("Please insert a number. Format input is not valid")
            ElseIf Convert.ToInt16(TxtPararel.Text) <= 0 Then
                Throw New Exception("Please input # of Paralel greater than 0.")
            Else
                Me.TempValues(Convert.ToInt16(FirstGrid.Cells(0).Text) - 1, "Get", "Approvers", Convert.ToInt16(TxtPararel.Text))
                Me.TempValues(Convert.ToInt16(FirstGrid.Cells(0).Text) - 1, "Get", "DueDate", Convert.ToInt16(TxtPararel.Text))
                Me.TempValues(Convert.ToInt16(FirstGrid.Cells(0).Text) - 1, "Get", "NotifyOthers", Convert.ToInt16(TxtPararel.Text))
                Me.DisableOrEnableBtnEmailTemplate(True)
                Dim JmlParalel As Integer = TxtPararel.Text
                '-------------Approvers-----------------------------------------------------------------
                Dim ApproversGrid As GridView = FirstGrid.Cells(3).Controls(1) 'ambil control gridview Approvers
                Dim DueDateGrid As GridView = FirstGrid.Cells(4).Controls(1) ' ambil control gridview DueDate
                Dim NotifyOthersGrid As GridView = FirstGrid.Cells(5).Controls(1) ' ambil control gridview Notify Others

                ' Create Grid Detail
                Me.CreateGridDetailCaseManagement(JmlParalel, ApproversGrid, DueDateGrid, NotifyOthersGrid)

                '-------------Set Value Ke Textbox Masing-Masing---------------------------------------------
                Me.TempValues(Convert.ToInt16(FirstGrid.Cells(0).Text) - 1, "Set", "Approvers")
                Me.TempValues(Convert.ToInt16(FirstGrid.Cells(0).Text) - 1, "Set", "DueDate")
                Me.TempValues(Convert.ToInt16(FirstGrid.Cells(0).Text) - 1, "Set", "NotifyOthers")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            ClientScript.RegisterClientScriptBlock(Page.GetType, "Script", "<script language='javascript'>alert('" & ex.Message & "');</script>")
        End Try
    End Sub

#End Region

#Region "Checking"

    ''' <summary>
    ''' Check Valid EmailTemplate
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CheckValidEmailTemplate() As Boolean
        Try
            If Me.TxtSubjectEmail.Text = "" Then
                Return False
            Else
                Return True
            End If
        Catch
            Throw
        End Try
    End Function

#End Region

#Region "Save Case Management"

    ''' <summary>
    ''' Save Case Management workflow
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            ' Cek apakah superuser apa tidak
            If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                Me.SaveCaseManagementWorkflow(True)
                Me.LblSuccess.Visible = True
            Else ' masuk pending approval
                Me.SaveCaseManagementWorkflow(False)

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    ''' <summary>
    ''' SaveCaseManagementWorkflow
    ''' </summary>
    ''' <param name="boolIsSuperUser"></param>
    ''' <remarks></remarks>
    Public Sub SaveCaseManagementWorkflow(ByVal boolIsSuperUser As Boolean)
        Try
            If boolIsSuperUser Then                
                Me.InsertCaseManagementWorkflowBySuperUser()
            Else
                Me.InsertCaseManagementWorkflowByNonSuperUser()
                ' Message Pending
                Dim MessagePendingID As Integer = 9002 'MessagePendingID 8201 = Case Management Workflow Add /Edit
                Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID
                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID, False)
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            ' clear all session
            ' Email template
            If Me.SetnGetEmailTemplate_STR IsNot Nothing Then
                Me.SetnGetEmailTemplate_STR.Clear()
                Me.SetnGetEmailTemplate_STR = Nothing
            End If
            'Approvers
            If Me.SetnGetApproversValues_STR IsNot Nothing Then
                Me.SetnGetApproversValues_STR.Clear()
                Me.SetnGetApproversValues_STR = Nothing
            End If
            'DueDate
            If Me.SetnGetDueDateValues_STR IsNot Nothing Then
                Me.SetnGetDueDateValues_STR.Clear()
                Me.SetnGetDueDateValues_STR = Nothing
            End If
            'Notify Others 
            If Me.SetnGetNotifyOthersValues_STR IsNot Nothing Then
                Me.SetnGetNotifyOthersValues_STR.Clear()
                Me.SetnGetNotifyOthersValues_STR = Nothing
            End If
            ' Root General
            If Me.SetnGetRootGeneral_STR IsNot Nothing Then
                Me.SetnGetRootGeneral_STR.Clear()
                Me.SetnGetRootGeneral_STR = Nothing
            End If
            'Root Detail 
            If Me.SetnGetRootDetail_STR IsNot Nothing Then
                Me.SetnGetRootDetail_STR.Clear()
                Me.SetnGetRootDetail_STR = Nothing
            End If
            ' Email Template
            If Me.SetnGetEmailTemplate_STR IsNot Nothing Then
                Me.SetnGetEmailTemplate_STR.Clear()
                Me.SetnGetEmailTemplate_STR = Nothing
            End If
        End Try
    End Sub

#Region "Insert By SuperUser / Non SuperUser"

    ''' <summary>
    ''' Insert By SuperUser
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertCaseManagementWorkflowBySuperUser()
        Dim Trans As SqlTransaction = Nothing
        Try
            'Using oTrans As New Transactions.TransactionScope

            If Me.SetnGetMode_STR = "Edit" Then
                ' Delete Case Management WorkFlow 
                Using oAccessSTRQueries As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.TransTableAdapterTableAdapter
                    Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(oAccessSTRQueries)
                    oAccessSTRQueries.DeleteAllProposalSTRWorkflow()
                End Using
            End If

            Dim DtTempAuditTrail As New Data.DataTable
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("SerialNo", GetType(String)))
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("WorkflowName", GetType(String)))
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("Paralel", GetType(String)))
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("Approvers", GetType(String)))
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("DueDate", GetType(String)))
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("NotifyOthers", GetType(String)))
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("SubjectEmailTemplate", GetType(String)))
            DtTempAuditTrail.Columns.Add(New Data.DataColumn("BodyEmailTemplate", GetType(String)))



            Using oAccessDML_ProposalSTR As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.TransTableAdapterTableAdapter
                If Me.SetnGetMode_STR = "Edit" Then
                    Sahassa.AML.TableAdapterHelper.SetTransaction(oAccessDML_ProposalSTR, Trans)
                Else
                    Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(oAccessDML_ProposalSTR)
                End If
                For Each FirstRowsGrid As GridViewRow In Me.GridViewParent.Rows ' looping row in first grid 
                    Dim IntSerial As Int32 = Convert.ToInt32(FirstRowsGrid.Cells(0).Text)
                    Dim TxtWorkflowName As TextBox = CType(FirstRowsGrid.Cells(1).Controls(1), TextBox)
                    Dim TxtParalel As TextBox = CType(FirstRowsGrid.Cells(2).Controls(1), TextBox)

                    ' Checking ValidationGeneralCaseManagement
                    CheckingValidationGeneralProposalSTR(TxtWorkflowName.Text, Convert.ToInt64(TxtParalel.Text), IntSerial)

                    ' insert Case Management General (Serial,Workflow Name,Paralel)
                    oAccessDML_ProposalSTR.InsertProposalSTRWorkflowGeneral(IntSerial, TxtWorkflowName.Text, TxtParalel.Text, Today, Today)

                    Dim StrTempApprovers As String = ""
                    Dim StrTempDueDate As String = ""
                    Dim StrTempNotifyOthers As String = ""

                    For i As Integer = 0 To Convert.ToInt16(TxtParalel.Text) - 1
                        Dim GridApprovers As GridView = FirstRowsGrid.Cells(3).Controls(1) ' ambil id grid approvers
                        Dim GridDueDate As GridView = FirstRowsGrid.Cells(4).Controls(1) ' ambil id grid DueDate
                        Dim GridNotifyOthers As GridView = FirstRowsGrid.Cells(5).Controls(1) ' ambil id grid Notify Others
                        Dim RowsApproversGrid As GridViewRow = GridApprovers.Rows(i)
                        Dim RowsDueDate As GridViewRow = GridDueDate.Rows(i)
                        Dim RowsNotifyOthers As GridViewRow = GridNotifyOthers.Rows(i)

                        Dim TxtApprovers As TextBox = RowsApproversGrid.Cells(0).Controls(1)
                        Dim TxtDueDate As TextBox = RowsDueDate.Cells(0).Controls(1)
                        Dim TxtNotifyOthers As TextBox = RowsNotifyOthers.Cells(0).Controls(1)

                        ' Checking if there is nothing or null
                        CheckingValidationDetailProposalSTR(TxtApprovers.Text, TxtDueDate.Text, TxtNotifyOthers.Text)

                        ' Insert Case Management Detail (Approvers, Due Date, Notify Others)
                        oAccessDML_ProposalSTR.InsertProposalSTRWorkflowDetail(IntSerial, Me.GetPkUserId(TxtApprovers.Text), Convert.ToInt64(TxtDueDate.Text), Me.GetPkUserId(TxtNotifyOthers.Text))

                        If i = Convert.ToInt16(TxtParalel.Text) - 1 Then
                            StrTempApprovers &= TxtApprovers.Text
                            StrTempDueDate &= TxtDueDate.Text
                            StrTempNotifyOthers &= TxtNotifyOthers.Text
                        Else
                            StrTempApprovers &= TxtApprovers.Text & " | "
                            StrTempDueDate &= TxtDueDate.Text & " | "
                            StrTempNotifyOthers &= TxtNotifyOthers.Text & " | "
                        End If
                    Next

                    Dim Rows() As Data.DataRow = Me.SetnGetEmailTemplate_STR.Select("SerialNo=" & IntSerial)
                    Dim Subject As String = Nothing
                    Dim Body As String = Nothing
                    If Rows.Length > 0 Then
                        Subject = Rows(0)(1).ToString
                        Body = Rows(0)(2).ToString
                    End If

                    '--------------------- Temp Value General for Audit Trail -------------------------------
                    Dim RowAuditTrail As Data.DataRow
                    RowAuditTrail = DtTempAuditTrail.NewRow
                    RowAuditTrail(0) = FirstRowsGrid.Cells(0).Text
                    RowAuditTrail(1) = TxtWorkflowName.Text
                    RowAuditTrail(2) = TxtParalel.Text
                    RowAuditTrail(3) = StrTempApprovers
                    RowAuditTrail(4) = StrTempDueDate
                    RowAuditTrail(5) = StrTempNotifyOthers
                    RowAuditTrail(6) = Subject
                    RowAuditTrail(7) = Body

                    DtTempAuditTrail.Rows.Add(RowAuditTrail)
                    '----------------------------------------------------------------------------------------

                    ' Save EmailTemplate 
                    Me.InsertEmailTemplate(IntSerial, True, Trans)
                Next
            End Using

            ' Insert into AuditTrail
            Trans.Commit()
            Me.InsertIntoAuditTrail(DtTempAuditTrail)

            ' transscope is complete
            'oTrans.Complete()
            'End Using
        Catch ex As Exception
            If Not Trans Is Nothing Then Trans.Rollback()
            Throw ex
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
                Trans = Nothing
            End If
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="DtAuditTrail"></param>
    ''' <remarks></remarks>
    Private Sub InsertIntoAuditTrail(ByRef DtAuditTrail As Data.DataTable)
        Try
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                Dim CreatedDate As DateTime
                Dim LastUpdatedDate As DateTime

                If Me.SetnGetMode_STR = "Edit" Then
                    ' Checking AuditTrail
                    Sahassa.AML.AuditTrailAlert.CheckMailAlert(10 * (DtAuditTrail.Rows.Count + Me.SetnGetOldGeneralValues_STR.Rows.Count))

                    'Get All General Old Values
                    For Each OldRow As Data.DataRow In SetnGetOldGeneralValues_STR.Rows
                        Dim StrApprovers_Old As String = ""
                        Dim StrDueDate_Old As String = ""
                        Dim StrNotifyOthers_Old As String = ""
                        Dim StrSubject_Old As String = ""
                        Dim StrBody_Old As String = ""

                        ' Get detail CMW Old per serial No
                        Using oAccessDetailSTR_Old As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowDetail_BySerialNoTableAdapter
                            Dim DtDetailSTR_Old As Data.DataTable = oAccessDetailSTR_Old.GetData(OldRow(0))
                            If DtDetailSTR_Old.Rows.Count > 0 Then
                                For i As Integer = 0 To DtDetailSTR_Old.Rows.Count - 1
                                    Dim RowsDetail As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowDetail_BySerialNoRow = DtDetailSTR_Old.Rows(i)
                                    If i = DtDetailSTR_Old.Rows.Count - 1 Then
                                        StrApprovers_Old &= RowsDetail.Approvers
                                        StrDueDate_Old &= RowsDetail.DueDate
                                        StrNotifyOthers_Old &= RowsDetail.NotifyOthers
                                    Else
                                        StrApprovers_Old &= RowsDetail.Approvers & " | "
                                        StrDueDate_Old &= RowsDetail.DueDate & " | "
                                        StrNotifyOthers_Old &= RowsDetail.NotifyOthers & " | "
                                    End If
                                Next
                            End If
                        End Using

                        ' Get email template per serial no
                        Using oAccessEmailTemplate_Old As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowEmailTemplate_BySerialNoTableAdapter
                            Dim DtEmail As Data.DataTable = oAccessEmailTemplate_Old.GetData(OldRow(0))
                            If DtEmail.Rows.Count > 0 Then
                                Dim RowEmail As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowEmailTemplate_BySerialNoRow = DtEmail.Rows(0)
                                StrSubject_Old = RowEmail.Subject_EmailTemplate
                                StrBody_Old = RowEmail.Body_EmailTemplate
                            End If
                        End Using

                        ' insert Audit Trail                                
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "SerialNo", "Delete", "", OldRow(0), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "WorkflowName", "Delete", "", OldRow(1), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "Paralel", "Delete", "", OldRow(2), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "Approvers", "Delete", "", StrApprovers_Old, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "DueDate", "Delete", "", StrDueDate_Old, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "NotifyOthers", "Delete", "", StrNotifyOthers_Old, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "SubjectEmailTemplate", "Delete", "", StrSubject_Old, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "BodyEmailTemplate", "Delete", "", StrBody_Old, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "CreatedDate", "Delete", "", CreatedDate, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "LastModifiedDate", "Delete", "", LastUpdatedDate, "Accepted")
                    Next
                Else 'Edit
                    Sahassa.AML.AuditTrailAlert.CheckMailAlert(10 * DtAuditTrail.Rows.Count)
                End If

                ' Insert Audit trail untuk yang baru
                For Each RowAuditTrail As Data.DataRow In DtAuditTrail.Rows
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "SerialNo", "Add", "", RowAuditTrail(0), "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "WorkflowName", "Add", "", RowAuditTrail(1), "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "Paralel", "Add", "", RowAuditTrail(2), "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "Approvers", "Delete", "", RowAuditTrail(3), "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "DueDate", "Delete", "", RowAuditTrail(4), "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "NotifyOthers", "Delete", "", RowAuditTrail(5), "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "SubjectEmailTemplate", "Delete", "", RowAuditTrail(6), "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "BodyEmailTemplate", "Delete", "", RowAuditTrail(7), "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "CreatedDate", "Add", "", CreatedDate, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "LastModifiedDate", "Add", "", Now, "Accepted")
                Next
            End Using

        Catch
            Throw
        Finally
            ' clear data table
            If DtAuditTrail IsNot Nothing Then
                DtAuditTrail.Clear()
                DtAuditTrail = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Insert By Non SuperUser
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertCaseManagementWorkflowByNonSuperUser()
        Dim Trans As SqlTransaction = Nothing
        Try
            'Using oTrans As New Transactions.TransactionScope
            Using oAccessSTRQuery As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.TransTableAdapterTableAdapter
                Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(oAccessSTRQuery)
                For Each FirstRowsGrid As GridViewRow In Me.GridViewParent.Rows ' looping row in first grid 
                    Dim IntSerial As Int32 = Convert.ToInt32(FirstRowsGrid.Cells(0).Text)
                    Dim TxtWorkflowName As TextBox = CType(FirstRowsGrid.Cells(1).Controls(1), TextBox)
                    Dim TxtParalel As TextBox = CType(FirstRowsGrid.Cells(2).Controls(1), TextBox)

                    CheckingValidationGeneralProposalSTR(TxtWorkflowName.Text, Convert.ToInt64(TxtParalel.Text), IntSerial)

                    ' insert Case Management General Approval (Serial,Workflow Name,Paralel)
                    oAccessSTRQuery.InsertProposalSTRWorkflowGeneralApproval(IntSerial, TxtWorkflowName.Text, TxtParalel.Text, Sahassa.AML.Commonly.SessionUserId, Me.SetnGetMode_STR, Today, Today)

                    For i As Integer = 0 To Convert.ToInt16(TxtParalel.Text) - 1

                        Dim GridApprovers As GridView = FirstRowsGrid.Cells(3).Controls(1) ' ambil id grid approvers
                        Dim GridDueDate As GridView = FirstRowsGrid.Cells(4).Controls(1) ' ambil id grid DueDate
                        Dim GridNotifyOthers As GridView = FirstRowsGrid.Cells(5).Controls(1) ' ambil id grid Notify Others
                        Dim RowsApproversGrid As GridViewRow = GridApprovers.Rows(i)
                        Dim RowsDueDate As GridViewRow = GridDueDate.Rows(i)
                        Dim RowsNotifyOthers As GridViewRow = GridNotifyOthers.Rows(i)

                        Dim TxtApprovers As TextBox = RowsApproversGrid.Cells(0).Controls(1)
                        Dim TxtDueDate As TextBox = RowsDueDate.Cells(0).Controls(1)
                        Dim TxtNotifyOthers As TextBox = RowsNotifyOthers.Cells(0).Controls(1)

                        ' Checking if there is nothing or null
                        CheckingValidationDetailProposalSTR(TxtApprovers.Text, TxtDueDate.Text, TxtNotifyOthers.Text)

                        ' Insert Case Management Detail Approval(Approvers, Due Date, Notify Others)
                        oAccessSTRQuery.InsertProposalSTRWorkflowDetailApproval(IntSerial, Me.GetPkUserId(TxtApprovers.Text), Convert.ToInt64(TxtDueDate.Text), Me.GetPkUserId(TxtNotifyOthers.Text))
                    Next
                    ' Insert Email Template
                    Me.InsertEmailTemplate(IntSerial, False, Trans)
                Next
            End Using
            Trans.Commit()
            ' transcope is complete
            'oTrans.Complete()
            'End Using
        Catch ex As Exception
            If Not Trans Is Nothing Then Trans.Rollback()
            Throw ex
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
                Trans = Nothing
            End If
        End Try
    End Sub

#Region "Insert EmailTemplate By SuperUser / NonSuperUser"
    ''' <summary>
    ''' InsertEmailTemplate
    ''' </summary>    
    ''' <param name="intSerial"></param>
    ''' <param name="isSuperUser"></param>
    ''' <remarks></remarks>
    Private Sub InsertEmailTemplate(ByVal intSerial As Integer, ByVal isSuperUser As Boolean, ByRef TransRef As SqlTransaction)
        Try
            Using oAccessEmailTemplateSTR As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.TransTableAdapterTableAdapter
                If Me.SetnGetEmailTemplate_STR IsNot Nothing Then
                    Sahassa.AML.TableAdapterHelper.SetTransaction(oAccessEmailTemplateSTR, TransRef)
                    Dim RowEmailTemplate() As Data.DataRow = Me.SetnGetEmailTemplate_STR.Select("SerialNo=" & intSerial)
                    Dim StrSubject As String = ""
                    Dim StrBody As String = ""
                    If Not RowEmailTemplate Is Nothing Then
                        If RowEmailTemplate.Length > 0 Then
                            StrSubject = RowEmailTemplate(0)(1)
                            StrBody = RowEmailTemplate(0)(2)
                        End If
                    End If

                    If isSuperUser Then
                        oAccessEmailTemplateSTR.InsertProposalSTRWorkflowEmailTemplate(intSerial, StrSubject, StrBody)
                    Else
                        oAccessEmailTemplateSTR.InsertProposalSTRWorkflowEmailTemplateApproval(intSerial, StrSubject, StrBody)
                    End If
                End If
            End Using
        Catch
            Throw
        End Try
    End Sub
#End Region

#End Region

#Region "Checking"

    ''' <summary>
    ''' CheckingValidationGeneralCaseManagement
    ''' </summary>
    ''' <param name="strWorkflowName"></param>
    ''' <param name="intParalel"></param>
    ''' <remarks></remarks>
    Public Sub CheckingValidationGeneralProposalSTR(ByVal strWorkflowName As String, ByVal intParalel As Int64, ByVal IntSerial As Integer)
        Try
            If strWorkflowName = "" Then
                Throw New Exception("Workflow Name for Serial '" & IntSerial & "' is required.")
            End If
            If intParalel <= 0 Then
                Throw New Exception("Paralel for Serial '" & IntSerial & "' must be greater than zero.")
            End If
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' CheckingValidation DetailCaseManagement
    ''' </summary>
    ''' <param name="Approvers"></param>
    ''' <param name="DueDate"></param>
    ''' <param name="NotifyOthers"></param>
    ''' <remarks></remarks>
    Public Sub CheckingValidationDetailProposalSTR(ByVal Approvers As String, ByVal DueDate As String, ByVal NotifyOthers As String)
        Try
            Dim StrPattern As String = "\b\d+\b"
            If Approvers = "" Then
                Throw New Exception("Approvers is required. Minimum one User id selected.")
            End If
            If Regex.IsMatch(DueDate, StrPattern) = False Then
                Throw New Exception("Wrong input duedate. Please insert a numeric.")
            End If
            ' Commented by Johan 11:22 AM 12/3/2007
            ' Caused by there is no mandatory
            'If NotifyOthers = "" Then
            '    Throw New Exception("Notify Others is required. Minimum one User id selected.")
            'End If
        Catch
            Throw
        End Try
    End Sub

#End Region

    ''' <summary>
    ''' get Pk user id
    ''' </summary>
    ''' <param name="strUserID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetPkUserId(ByVal strUserID As String) As String
        Try
            Dim StrPkUserID As String = Nothing
            If strUserID <> "" Then
                Dim oArrUserId As String() = strUserID.Split(";")
                Using oAccessUserID As New AMLDAL.CaseManagementWorkflowTableAdapters.GetPkUserIdTableAdapter
                    For i As Integer = 0 To oArrUserId.Length - 1
                        If i = oArrUserId.Length - 1 Then
                            StrPkUserID &= Convert.ToString(oAccessUserID.GetPkUserID(oArrUserId(i)))
                        Else
                            StrPkUserID &= Convert.ToString(oAccessUserID.GetPkUserID(oArrUserId(i))) & "|"
                        End If
                    Next
                End Using
            Else
                StrPkUserID = "0"
            End If

            Return StrPkUserID
        Catch
            Throw
        End Try
    End Function
    ''' <summary>
    ''' Get UserId By Pk
    ''' </summary>
    ''' <param name="PkUserID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetUserIdByPk(ByVal PkUserID As String) As String
        Try
            Dim StrUserID As String = Nothing
            If PkUserID <> "" Then
                Dim oArrPkUserId As String() = PkUserID.Split("|")
                Using oAccessUserID As New AMLDAL.CaseManagementWorkflowTableAdapters.GetUserIDByPKTableAdapter
                    For i As Integer = 0 To oArrPkUserId.Length - 1
                        If i = oArrPkUserId.Length - 1 Then
                            StrUserID &= Convert.ToString(oAccessUserID.Fill(Convert.ToInt32(oArrPkUserId(i))))
                        Else
                            StrUserID &= Convert.ToString(oAccessUserID.Fill(Convert.ToInt32(oArrPkUserId(i)))) & ";"
                        End If
                    Next
                End Using
            End If
            Return StrUserID
        Catch
            Throw
        End Try
    End Function
#End Region

#Region "Change ROOT"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="o_RowGridRoot"></param>
    ''' <param name="intParalel"></param>
    ''' <param name="Category"></param>
    ''' <param name="intSerial"></param>
    ''' <remarks></remarks>
    Public Sub GetTempValueFromRoot(ByVal o_RowGridRoot As GridViewRow, ByVal intParalel As Int16, ByVal Category As String, Optional ByVal intSerial As Integer = 1)
        Try
            Dim GridviewApprovers As GridView = CType(o_RowGridRoot.Cells(3).Controls(1), GridView)
            Dim GridviewDueDate As GridView = CType(o_RowGridRoot.Cells(4).Controls(1), GridView)
            Dim GridviewNotifyOthers As GridView = CType(o_RowGridRoot.Cells(5).Controls(1), GridView)
            Select Case Category.ToLower
                Case "get"
                    For j As Integer = 0 To intParalel
                        If GridviewApprovers.Rows.Count <> 0 Then
                            Dim RowApprovers As GridViewRow = GridviewApprovers.Rows(j)
                            Dim RowDueDate As GridViewRow = GridviewDueDate.Rows(j)
                            Dim RowNotifyOthers As GridViewRow = GridviewNotifyOthers.Rows(j)
                            Dim TxtApprovers As TextBox = CType(RowApprovers.Cells(0).Controls(1), TextBox)
                            Dim TxtDueDate As TextBox = CType(RowDueDate.Cells(0).Controls(1), TextBox)
                            Dim TxtNotifyOthers As TextBox = CType(RowNotifyOthers.Cells(0).Controls(1), TextBox)

                            Dim RowTempGeneral As Data.DataRow = Me.SetnGetRootDetail_STR.NewRow
                            RowTempGeneral(0) = Convert.ToInt16(o_RowGridRoot.Cells(0).Text) ' No Serial
                            RowTempGeneral(1) = TxtApprovers.Text
                            If TxtDueDate.Text = "" Then
                                TxtDueDate.Text = 0
                            End If
                            RowTempGeneral(2) = TxtDueDate.Text
                            RowTempGeneral(3) = TxtNotifyOthers.Text
                            Me.SetnGetRootDetail_STR.Rows.Add(RowTempGeneral)
                        Else
                            Exit For
                        End If
                    Next
                Case "set"
                    Dim RowTempDetail() As Data.DataRow = Me.SetnGetRootDetail_STR.Select("Serial=" & intSerial)
                    For i As Integer = 0 To RowTempDetail.Length - 1
                        Dim RowApprovers As GridViewRow = GridviewApprovers.Rows(i)
                        Dim RowDueDate As GridViewRow = GridviewDueDate.Rows(i)
                        Dim RowNotifyOthers As GridViewRow = GridviewNotifyOthers.Rows(i)
                        Dim TxtApprovers As TextBox = CType(RowApprovers.Cells(0).Controls(1), TextBox)
                        Dim TxtDueDate As TextBox = CType(RowDueDate.Cells(0).Controls(1), TextBox)
                        Dim TxtNotifyOthers As TextBox = CType(RowNotifyOthers.Cells(0).Controls(1), TextBox)
                        Dim SelectedRow As Data.DataRow = RowTempDetail(i)
                        TxtApprovers.Text = SelectedRow(1)
                        TxtNotifyOthers.Text = SelectedRow(3)
                        TxtDueDate.Text = SelectedRow(2)
                    Next
            End Select
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetRootTempValuesGeneral()
        Try
            If Me.GridViewParent.Rows.Count > 0 Then
                Dim RowsValueGeneral As Data.DataRow
                If Me.SetnGetRootGeneral_STR IsNot Nothing Then
                    Me.SetnGetRootGeneral_STR.Rows.Clear()
                End If
                Dim IntCounter As Integer = 0
                If Convert.ToInt16(Me.TxtJumlahSerial.Text) > Me.GridViewParent.Rows.Count Then
                    IntCounter = Me.GridViewParent.Rows.Count - 1
                Else
                    IntCounter = Convert.ToInt16(Me.TxtJumlahSerial.Text) - 1
                End If
                For i As Integer = 0 To IntCounter
                    Dim RowGeneral As GridViewRow = Me.GridViewParent.Rows(i)
                    Dim TxtWorkflow As TextBox = CType(RowGeneral.Cells(1).Controls(1), TextBox)
                    Dim TxtParalel As TextBox = CType(RowGeneral.Cells(2).Controls(1), TextBox)

                    If Me.SetnGetRootDetail_STR Is Nothing Then
                        Dim DtTempAllDetail As New Data.DataTable
                        DtTempAllDetail.Columns.Add(New Data.DataColumn("Serial", GetType(Integer)))
                        DtTempAllDetail.Columns.Add(New Data.DataColumn("Approvers", GetType(String)))
                        DtTempAllDetail.Columns.Add(New Data.DataColumn("DueDate", GetType(Integer)))
                        DtTempAllDetail.Columns.Add(New Data.DataColumn("NotifyOthers", GetType(String)))
                        Me.SetnGetRootDetail_STR = DtTempAllDetail
                    End If

                    If TxtParalel.Text <> "" And Regex.IsMatch(TxtParalel.Text, "\b\d+\b") = True Then
                        Me.GetTempValueFromRoot(RowGeneral, Convert.ToInt16(TxtParalel.Text) - 1, "Get", Convert.ToInt16(RowGeneral.Cells(0).Text))
                    End If

                    RowsValueGeneral = Me.SetnGetRootGeneral_STR.NewRow
                    RowsValueGeneral(0) = TxtWorkflow.Text
                    If TxtParalel.Text = "" Or Regex.IsMatch(TxtParalel.Text, "\b\d+\b") = False Then
                        TxtParalel.Text = 0
                    End If
                    RowsValueGeneral(1) = TxtParalel.Text
                    Me.SetnGetRootGeneral_STR.Rows.Add(RowsValueGeneral)
                Next
            End If
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetRootTempValuesGeneral()
        Try
            If Me.SetnGetRootGeneral_STR.Rows.Count > 0 Then
                For i As Integer = 0 To Me.SetnGetRootGeneral_STR.Rows.Count - 1
                    Dim RowGeneral As GridViewRow = Me.GridViewParent.Rows(i)
                    Dim Txtworkflow As TextBox = CType(RowGeneral.Cells(1).Controls(1), TextBox)
                    Dim TxtParalel As TextBox = CType(RowGeneral.Cells(2).Controls(1), TextBox)
                    Txtworkflow.Text = Me.SetnGetRootGeneral_STR.Rows(i)(0)
                    TxtParalel.Text = Me.SetnGetRootGeneral_STR.Rows(i)(1)
                    Dim ApproversGrid As GridView = RowGeneral.Cells(3).Controls(1) 'ambil control gridview Approvers
                    Dim DueDateGrid As GridView = RowGeneral.Cells(4).Controls(1) ' ambil control gridview DueDate
                    Dim NotifyOthersGrid As GridView = RowGeneral.Cells(5).Controls(1) ' ambil control gridview Notify Others
                    If TxtParalel.Text <> "" Or Regex.IsMatch(TxtParalel.Text, "\b\d+\b") Then
                        CreateGridDetailCaseManagement(Convert.ToInt16(TxtParalel.Text), ApproversGrid, DueDateGrid, NotifyOthersGrid)
                        Me.GetTempValueFromRoot(RowGeneral, Convert.ToInt16(TxtParalel.Text), "Set", Convert.ToInt16(RowGeneral.Cells(0).Text))
                    End If
                Next

            End If
        Catch
            Throw
        End Try
    End Sub
#End Region

    ''' <summary>
    '''  Cancel Save
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("Default.aspx", False)
    End Sub
End Class
