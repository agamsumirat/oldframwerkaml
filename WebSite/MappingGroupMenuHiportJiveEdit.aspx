<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="MappingGroupMenuHiportJiveEdit.aspx.vb" Inherits="MappingGroupMenuHiportJiveEdit"
     %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <ajax:AjaxPanel runat="server" ID="Ajax1">
        <table id="Table1" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
            height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
            border-bottom-style: none" width="100%">
            <tr>
                <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                    <strong>Mapping Group Menu Hiport Jive - Edit&nbsp;
                        <hr />
                    </strong>
                    <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                        Width="100%"></asp:Label>
                </td>
            </tr>
        </table>
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="view1" runat="server">
                <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                    style="border-top-style: none; border-right-style: none; border-left-style: none;
                    border-bottom-style: none" width="100%">
                    <tr class="formText">
                        <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                            border-left-style: none; height: 24px; border-bottom-style: none">
                            <span style="color: #ff0000">* Required</span>
                        </td>
                    </tr>
                </table>
                <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                    border="2" height="72" style="border-top-style: none; border-right-style: none;
                    border-left-style: none; border-bottom-style: none">
                    <tr class="formText">
                        <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                        </td>
                        <td width="20%" bgcolor="#ffffff" style="height: 24px">
                            Group Menu
                        </td>
                        <td width="5" bgcolor="#ffffff" style="height: 24px">
                            :
                        </td>
                        <td width="80%" bgcolor="#ffffff" style="height: 24px">
                            &nbsp;<asp:DropDownList ID="CboGroupMenu" runat="server" CssClass="combobox" Width="182px">
                            </asp:DropDownList>
                            <strong><span style="color: #ff0000"></span></strong>
                        </td>
                    </tr>
                    <tr class="formText" bgcolor="#dddddd" height="30">
                        <td style="width: 24px">
                            <img height="15" src="images/arrow.gif" width="15">
                        </td>
                        <td colspan="3" style="height: 9px">
                            <table cellspacing="0" cellpadding="3" border="0">
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="True" SkinID="SaveButton">
                                        </asp:ImageButton>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton">
                                        </asp:ImageButton>
                                    </td>
                                </tr>
                            </table>
                            <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
                        </td>
                    </tr>
                </table>
            </asp:View>
            <asp:View ID="dialog" runat="server">
                <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#ffffff"
                    border="2" id="TABLE2">
                    <tr class="formText" align="center">
                        <td>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="formText" align="center">
                        <td>
                            <asp:ImageButton ID="btnOK" runat="server" ImageUrl="~/Images/button/ok.gif" />
                        </td>
                    </tr>
                </table>
            </asp:View>
        </asp:MultiView>
    </ajax:AjaxPanel>
</asp:Content>
