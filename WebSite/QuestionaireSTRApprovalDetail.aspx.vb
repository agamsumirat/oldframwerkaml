Option Explicit On
Option Strict On
Imports SahassaNettier.Entities
Imports SahassaNettier.Data

Imports AMLBLL

Partial Class QuestionaireSTRApprovalDetail
    Inherits Parent

#Region "Sesion"
    Public ReadOnly Property PKMsQuestionaireSTRApprovalID() As Integer
        Get
            Dim strTemp As String = Request.Params("PKMsQuestionaireSTRApprovalID")
            Dim intResult As Integer
            If Integer.TryParse(strTemp, intResult) Then
                Return intResult
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Parameter PKMsQuestionaireSTRApprovalID is invalid."

            End If
        End Get
    End Property
    Public ReadOnly Property ObjMsQuestionaireSTR() As MsQuestionaireSTR
        Get
            If Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTR") Is Nothing Then

                Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTR") = DataRepository.MsQuestionaireSTRProvider.GetByPK_MsQuestionaireSTR_ID(ObjMsQuestionaireSTR_approvalDetail(0).PK_MsQuestionaireSTR_ID)

            End If
            Return CType(Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTR"), MsQuestionaireSTR)
        End Get
    End Property
    Public ReadOnly Property ObjMsQuestionaireSTRMultipleChoice() As TList(Of MsQuestionaireSTRMultipleChoice)
        Get
            If Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTRMultipleChoice") Is Nothing Then

                Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTRMultipleChoice") = DataRepository.MsQuestionaireSTRMultipleChoiceProvider.GetPaged("FK_MsQuestionaireSTR_ID = '" & ObjMsQuestionaireSTR.PK_MsQuestionaireSTR_ID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTRMultipleChoice"), TList(Of MsQuestionaireSTRMultipleChoice))
        End Get
    End Property
    Public ReadOnly Property ObjMsQuestionaireSTR_approval() As MsQuestionaireSTR_Approval
        Get
            If Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTR_approval") Is Nothing Then

                Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTR_approval") = DataRepository.MsQuestionaireSTR_ApprovalProvider.GetByPK_MsQuestionaireSTR_ApprovalID(PKMsQuestionaireSTRApprovalID)

            End If
            Return CType(Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTR_approval"), MsQuestionaireSTR_Approval)
        End Get
    End Property
    Public ReadOnly Property ObjMsQuestionaireSTR_approvalDetail() As TList(Of MsQuestionaireSTR_ApprovalDetail)
        Get
            If Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTR_approvalDetail") Is Nothing Then

                Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTR_approvalDetail") = DataRepository.MsQuestionaireSTR_ApprovalDetailProvider.GetPaged("FK_MsQuestionaireSTR_ApprovalID = '" & ObjMsQuestionaireSTR_approval.PK_MsQuestionaireSTR_ApprovalID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTR_approvalDetail"), TList(Of MsQuestionaireSTR_ApprovalDetail))
        End Get
    End Property
    Public ReadOnly Property ObjMsQuestionaireSTRMultipleChoice_approvaldetail() As TList(Of MsQuestionaireSTRMultipleChoice_ApprovalDetail)
        Get
            If Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTRMultipleChoice_approvaldetail") Is Nothing Then

                Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTRMultipleChoice_approvaldetail") = DataRepository.MsQuestionaireSTRMultipleChoice_ApprovalDetailProvider.GetPaged("FK_MsQuestionaireSTR_ApprovalID = '" & ObjMsQuestionaireSTR_approval.PK_MsQuestionaireSTR_ApprovalID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTRMultipleChoice_approvaldetail"), TList(Of MsQuestionaireSTRMultipleChoice_ApprovalDetail))
        End Get
    End Property
    Private Property SetnGetCurrentPageNew() As Integer
        Get
            Return CInt(IIf(Session("SetnGetCurrentPageNew") Is Nothing, "", Session("SetnGetCurrentPageNew")))
        End Get
        Set(ByVal value As Integer)
            Session("SetnGetCurrentPageNew") = value
        End Set
    End Property
    Private Property SetnGetCurrentPageOld() As Integer
        Get
            Return CInt(IIf(Session("SetnGetCurrentPageOld") Is Nothing, "", Session("SetnGetCurrentPageOld")))
        End Get
        Set(ByVal value As Integer)
            Session("SetnGetCurrentPageOld") = value
        End Set
    End Property
#End Region

#Region "ClearSession"
    Sub ClearSession()
        Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTR") = Nothing
        Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTRMultipleChoice") = Nothing
        Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTR_approval") = Nothing
        Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTR_approvalDetail") = Nothing
        Session("QuestionaireSTRApprovalDetail.ObjMsQuestionaireSTRMultipleChoice_approvaldetail") = Nothing
        Session("SetnGetCurrentPageNew") = Nothing
        Session("SetnGetCurrentPageOld") = Nothing
    End Sub
#End Region

#Region "load mode"
    Private Sub DataNew()
        LabelTitle.Text = "Questionaire STR"
        Dim abc As Integer = CInt(ObjMsQuestionaireSTR_approval.FK_ModeID)
        If abc = 1 Then
            LblModeEditNew.Text = "ADD"
        ElseIf abc = 2 Then
            LblModeEditNew.Text = "EDIT"
        ElseIf abc = 3 Then
            LblModeEditNew.Text = "DELETE"
        ElseIf abc = 4 Then
            LblModeEditNew.Text = "ACTIVATION"
        End If

        Using ObjQuestionType As QuestionType = DataRepository.QuestionTypeProvider.GetByPK_QuestionType_ID(ObjMsQuestionaireSTR_approvalDetail(0).FK_QuestionType_ID)
            LblQuestionTypeEditNew.Text = ObjQuestionType.QuestionType
        End Using

        LblDescriptionAlertSTREditNew.Text = ObjMsQuestionaireSTR_approvalDetail(0).DescriptionAlertSTR
        LblQuestionNoEditNew.Text = CStr(ObjMsQuestionaireSTR_approvalDetail(0).QuestionNo)
        TxtQuestionEditNew.Text = ObjMsQuestionaireSTR_approvalDetail(0).Question
        LblCreatedEditNew.Text = ObjMsQuestionaireSTR_approvalDetail(0).CreatedBy & " / " & CStr(ObjMsQuestionaireSTR_approvalDetail(0).CreatedDate)
        LblLastUpDateEditNew.Text = ObjMsQuestionaireSTR_approvalDetail(0).LastUpdateBy & " / " & CStr(ObjMsQuestionaireSTR_approvalDetail(0).LastUpdateDate)
        If Not ObjMsQuestionaireSTR_approvalDetail(0).ApprovedBy Is Nothing Then
            LblApprovedByEditNew.Text = ObjMsQuestionaireSTR_approvalDetail(0).ApprovedBy & " / " & CStr(ObjMsQuestionaireSTR_approvalDetail(0).ApprovedDate)
        End If

        If ObjMsQuestionaireSTR_approvalDetail(0).FK_QuestionType_ID = 1 Then
            ViewNew.Visible = False
        ElseIf ObjMsQuestionaireSTR_approvalDetail(0).FK_QuestionType_ID = 2 Then
            ViewNew.Visible = True
            gridViewEditNew.DataSource = ObjMsQuestionaireSTRMultipleChoice_approvaldetail
            gridViewEditNew.DataBind()
        End If
    End Sub

    Private Sub DataOld()
        Dim bcd As Integer = CInt(ObjMsQuestionaireSTR_approval.FK_ModeID)
        If bcd = 1 Then
            LblModeEditOld.Text = "ADD"
        ElseIf bcd = 2 Then
            LblModeEditOld.Text = "EDIT"
        ElseIf bcd = 3 Then
            LblModeEditOld.Text = "DELETE"
        ElseIf bcd = 4 Then
            LblModeEditOld.Text = "ACTIVATION"
        End If

        Using ObjQuestionType As QuestionType = DataRepository.QuestionTypeProvider.GetByPK_QuestionType_ID(ObjMsQuestionaireSTR_approvalDetail(0).FK_QuestionType_ID_Old)
            LblQuestionTypeEditOld.Text = ObjQuestionType.QuestionType
        End Using

        LblDescriptionAlertSTREditOld.Text = ObjMsQuestionaireSTR_approvalDetail(0).DescriptionAlertSTR_Old
        LblQuestionNoEditOld.Text = CStr(ObjMsQuestionaireSTR_approvalDetail(0).QuestionNo_Old)
        TxtQuestionEditOld.Text = ObjMsQuestionaireSTR_approvalDetail(0).Question_Old
        LblCreatedEditOld.Text = ObjMsQuestionaireSTR_approvalDetail(0).CreatedBy_Old & " / " & CStr(ObjMsQuestionaireSTR_approvalDetail(0).CreatedDate_Old)
        LblLastUpDateEditOld.Text = ObjMsQuestionaireSTR_approvalDetail(0).LastUpdateBy_Old & " / " & CStr(ObjMsQuestionaireSTR_approvalDetail(0).LastUpdateDate_Old)
        If Not ObjMsQuestionaireSTR_approvalDetail(0).ApprovedBy_Old Is Nothing Then
            LblApprovedByEditOld.Text = ObjMsQuestionaireSTR_approvalDetail(0).ApprovedBy_Old & " / " & CStr(ObjMsQuestionaireSTR_approvalDetail(0).ApprovedDate_Old)
        End If

        If ObjMsQuestionaireSTR_approvalDetail(0).FK_QuestionType_ID = 1 Then
            ViewOld.Visible = False
        ElseIf ObjMsQuestionaireSTR_approvalDetail(0).FK_QuestionType_ID = 2 Then
            ViewOld.Visible = True
            gridViewEditOld.DataSource = ObjMsQuestionaireSTRMultipleChoice
            gridViewEditOld.DataBind()
        End If
    End Sub

    Private Sub loaddata()

        If Not ObjMsQuestionaireSTR_approval Is Nothing Then
            Using ObjCekMode As MsQuestionaireSTR_Approval = DataRepository.MsQuestionaireSTR_ApprovalProvider.GetByPK_MsQuestionaireSTR_ApprovalID(PKMsQuestionaireSTRApprovalID)
                If CInt(ObjCekMode.FK_ModeID) = 1 Then
                    ViewEditNew.Visible = True
                    ViewEditOld.Visible = False
                    DataNew()
                ElseIf CInt(ObjCekMode.FK_ModeID) = 2 Then
                    ViewEditNew.Visible = True
                    ViewEditOld.Visible = True
                    DataNew()
                    DataOld()
                ElseIf CInt(ObjCekMode.FK_ModeID) = 3 Then
                    ViewEditNew.Visible = True
                    ViewEditOld.Visible = False
                    DataNew()
                ElseIf CInt(ObjCekMode.FK_ModeID) = 4 Then
                    ViewEditNew.Visible = True
                    ViewEditOld.Visible = False
                    DataNew()
                End If
            End Using
        Else
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = QuestionaireSTRBLL.DATA_NOTVALID
        End If
    End Sub
#End Region

#Region "IMGBack"
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "QuestionaireSTRApproval.aspx"
            Me.Response.Redirect("QuestionaireSTRApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

#Region "IMGREJECT"
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Using ObjListApproval As MsQuestionaireSTR_Approval = DataRepository.MsQuestionaireSTR_ApprovalProvider.GetByPK_MsQuestionaireSTR_ApprovalID(PKMsQuestionaireSTRApprovalID)
                If ObjListApproval Is Nothing Then
                    Throw New AMLBLL.SahassaException(MsQuestionaireSTREnum.DATA_NOTVALID)
                End If
            End Using

            If LblModeEditNew.Text = "ADD" Then
                AMLBLL.QuestionaireSTRBLL.RejectAddQUESTIONAIRESTR(PKMsQuestionaireSTRApprovalID)
            ElseIf LblModeEditNew.Text = "EDIT" Then
                AMLBLL.QuestionaireSTRBLL.RejectEditQUESTIONAIRESTR(PKMsQuestionaireSTRApprovalID)
            ElseIf LblModeEditNew.Text = "DELETE" Then
                AMLBLL.QuestionaireSTRBLL.RejectDeleteQUESTIONAIRESTR(PKMsQuestionaireSTRApprovalID)
            ElseIf LblModeEditNew.Text = "ACTIVATION" Then
                AMLBLL.QuestionaireSTRBLL.RejectActivationQUESTIONAIRESTR(PKMsQuestionaireSTRApprovalID)
            End If

            Sahassa.AML.Commonly.SessionIntendedPage = "QuestionaireStrApproval.aspx"

            Me.Response.Redirect("QuestionaireStrApproval.aspx", False)


        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
#End Region

#Region "IMGACCEPT"
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
           
            Using ObjListApproval As MsQuestionaireSTR_Approval = DataRepository.MsQuestionaireSTR_ApprovalProvider.GetByPK_MsQuestionaireSTR_ApprovalID(PKMsQuestionaireSTRApprovalID)
                If ObjListApproval Is Nothing Then
                    Throw New AMLBLL.SahassaException(MsQuestionaireSTREnum.DATA_NOTVALID)
                End If
            End Using

            If LblModeEditNew.Text = "ADD" Then
                AMLBLL.QuestionaireSTRBLL.AcceptAddQUESTIONAIRESTR(PKMsQuestionaireSTRApprovalID)
            ElseIf LblModeEditNew.Text = "EDIT" Then
                Using ObjList As MsQuestionaireSTR = DataRepository.MsQuestionaireSTRProvider.GetByPK_MsQuestionaireSTR_ID(ObjMsQuestionaireSTR_approvalDetail(0).PK_MsQuestionaireSTR_ID)
                    If ObjList Is Nothing Then
                        Throw New AMLBLL.SahassaException(MsQuestionaireSTREnum.DATA_NOTVALID)
                    End If
                End Using
                AMLBLL.QuestionaireSTRBLL.AcceptEditQUESTIONAIRESTR(PKMsQuestionaireSTRApprovalID)
            ElseIf LblModeEditNew.Text = "DELETE" Then
                Using ObjList As MsQuestionaireSTR = DataRepository.MsQuestionaireSTRProvider.GetByPK_MsQuestionaireSTR_ID(ObjMsQuestionaireSTR_approvalDetail(0).PK_MsQuestionaireSTR_ID)
                    If ObjList Is Nothing Then
                        Throw New AMLBLL.SahassaException(MsQuestionaireSTREnum.DATA_NOTVALID)
                    End If
                End Using
                AMLBLL.QuestionaireSTRBLL.AcceptDeleteQUESTIONAIRESTR(PKMsQuestionaireSTRApprovalID)
            ElseIf LblModeEditNew.Text = "ACTIVATION" Then
                Using ObjList As MsQuestionaireSTR = DataRepository.MsQuestionaireSTRProvider.GetByPK_MsQuestionaireSTR_ID(ObjMsQuestionaireSTR_approvalDetail(0).PK_MsQuestionaireSTR_ID)
                    If ObjList Is Nothing Then
                        Throw New AMLBLL.SahassaException(MsQuestionaireSTREnum.DATA_NOTVALID)
                    End If
                End Using
                AMLBLL.QuestionaireSTRBLL.AcceptActivationQUESTIONAIRESTR(PKMsQuestionaireSTRApprovalID)
            End If

            Sahassa.AML.Commonly.SessionIntendedPage = "QuestionaireStrApproval.aspx"

            Me.Response.Redirect("QuestionaireStrApproval.aspx", False)

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                '    Transcope.Complete()
            End Using

            If Not Page.IsPostBack Then
                ClearSession()
                loaddata()
                'abc.Visible = False
                'cde.Visible = False
            End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

#Region "Grid"
    Protected Sub gridviewEditNew_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridViewEditNew.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                Dim attachment As String = e.Item.Cells(1).Text

                e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub gridViewEditNew_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gridViewEditNew.PageIndexChanged

        gridViewEditNew.DataSource = ObjMsQuestionaireSTRMultipleChoice_approvaldetail
        SetnGetCurrentPageNew = e.NewPageIndex
        gridViewEditNew.CurrentPageIndex = SetnGetCurrentPageNew
        gridViewEditNew.DataBind()
    End Sub

    Protected Sub gridviewEditOld_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridViewEditOld.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                Dim attachment As String = e.Item.Cells(1).Text

                e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub gridviewEditOld_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gridViewEditOld.PageIndexChanged

        gridViewEditOld.DataSource = ObjMsQuestionaireSTRMultipleChoice
        SetnGetCurrentPageOld = e.NewPageIndex
        gridViewEditOld.CurrentPageIndex = SetnGetCurrentPageOld
        gridViewEditOld.DataBind()
    End Sub
#End Region


End Class
