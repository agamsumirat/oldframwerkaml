﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class OutlierToleransiDelete
    Inherits Parent

    ReadOnly Property GetPk_OutlierToleransi As Long
        Get
            If IsNumeric(Request.Params("OutlierToleransiID")) Then
                If Not IsNothing(Session("OutlierToleransiDelete.PK")) Then
                    Return CLng(Session("OutlierToleransiDelete.PK"))
                Else
                    Session("OutlierToleransiDelete.PK") = Request.Params("OutlierToleransiID")
                    Return CLng(Session("OutlierToleransiDelete.PK"))
                End If
            End If
            Return 0
        End Get
    End Property

    Sub ClearSession()
        Session("OutlierToleransiDelete.PK") = Nothing
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Sub SelectUser(ByVal IntUserid As Long)
        haccountowner.Value = IntUserid
        LblAccountOwner.Text = AMLBLL.SuspiciusPersonBLL.GetAccountOwnerIDbyPk(IntUserid)
    End Sub


    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "OutlierToleransiView.aspx"

            Me.Response.Redirect("OutlierToleransiView.aspx", False)
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try

            If Sahassa.AML.Commonly.SessionGroupName.ToLower = "superuser" Then
                If OutlierToleransiBLL.Delete(GetPk_OutlierToleransi) Then
                    lblMessage.Text = "Delete Data Success"
                    mtvOutLinerToleransiAdd.ActiveViewIndex = 1
                End If
            Else
                If OutlierToleransiBLL.DeleteApproval(GetPk_OutlierToleransi) Then
                    lblMessage.Text = "Data has been inserted to Approval"
                    mtvOutLinerToleransiAdd.ActiveViewIndex = 1
                End If
            End If

        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Sub LoadData()
        Using objOutlierToleransi As OutlierToleransi = OutlierToleransiBLL.getOutlierToleransiByPk(GetPk_OutlierToleransi)
            If Not IsNothing(objOutlierToleransi) Then
                haccountowner.Value = objOutlierToleransi.AccountOwnerID.ToString
                'LblAccountOwner.Text = objOutlierToleransi..ToString
                txtSegment.Text = objOutlierToleransi.Segment
                SelectUser(objOutlierToleransi.AccountOwnerID)
                txtToleransiDebet.Text = objOutlierToleransi.ToleransiDebet.ToString
                txtToleransiKredit.Text = objOutlierToleransi.ToleransiKredit.ToString
                If CBool(objOutlierToleransi.Activation) Then
                    lblActiveOrInactive.Text = "Activated"
                Else
                    lblActiveOrInactive.Text = "Deactivated"
                End If
            Else
                ImageSave.Visible = False
            End If
        End Using
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                mtvOutLinerToleransiAdd.ActiveViewIndex = 0

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                LoadData()
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            mtvOutLinerToleransiAdd.ActiveViewIndex = 0

        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Response.Redirect("OutlierToleransiView.aspx")
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


End Class


