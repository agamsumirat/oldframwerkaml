Imports Sahassa.AML.Commonly
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports AMLBLL
Partial Class IFTIFileApproval
    Inherits Parent






    Public ReadOnly Property PK_ListOfGeneratedFileCTR_ID() As Long
        Get
            Dim temp As String = Request.Params("PKListOfGeneratedFile")
            Dim retvalue As Long
            If Not Long.TryParse(temp, retvalue) Then
                Throw New Exception("PKListOfGeneratedFile not valid")
            End If
            Return retvalue
        End Get
    End Property


    Public Property ObjListOfGeneratedIFTI() As ListOfGeneratedIFTI
        Get
            If Session("IFTIFileApproval.ObjListOfGeneratedIFTI") Is Nothing Then
                Session("IFTIFileApproval.ObjListOfGeneratedIFTI") = AMLBLL.ListOfGeneratedIFTIBLL.GetListOfGeneratedIFTIByPk(Me.PK_ListOfGeneratedFileCTR_ID)
                Return CType(Session("IFTIFileApproval.ObjListOfGeneratedIFTI"), ListOfGeneratedIFTI)
            Else
                Return CType(Session("IFTIFileApproval.ObjListOfGeneratedIFTI"), ListOfGeneratedIFTI)
            End If
        End Get
        Set(ByVal value As ListOfGeneratedIFTI)
            Session("IFTIFileApproval.ObjListOfGeneratedIFTI") = value
        End Set
    End Property


    Sub ClearSession()
        ObjListOfGeneratedIFTI = Nothing
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    '    Transcope.Complete()
                End Using


                LoadData()
            End If

        Catch ex As Exception

            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Sub LoadData()
        If Not ObjListOfGeneratedIFTI Is Nothing Then

            LblInValid.Text = ObjListOfGeneratedIFTI.TotalInvalid.GetValueOrDefault(0)


            Using objIFTI_JenisLaporan As IFTI_JenisLaporan = AMLBLL.ListOfGeneratedIFTIBLL.GetIFTI_JenisLaporanByPk(ObjListOfGeneratedIFTI.Fk_MsCTRReportTypeFile_Id.GetValueOrDefault(0))
                If Not objIFTI_JenisLaporan Is Nothing Then
                    LblLastReportType.Text = objIFTI_JenisLaporan.JenisLaporan
                End If
            End Using
            LblReportdate.Text = ObjListOfGeneratedIFTI.ReportDate.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate()).ToString("dd-MM-yyyy").Replace("01-01-1900", "")


            Using objMsStatusUploadPPATK As MsStatusUploadPPATK = AMLBLL.ListOfGeneratedIFTIBLL.GetMsStatusUploadPPATKByPk(ObjListOfGeneratedIFTI.Fk_MsStatusUploadPPATK_Id.GetValueOrDefault(0))
                If Not objMsStatusUploadPPATK Is Nothing Then
                    LblStatusUploadPPATK.Text = objMsStatusUploadPPATK.MsStatusUploadPPATK_Name
                End If
            End Using


            LblTotalTransaction.Text = ObjListOfGeneratedIFTI.TotalTransaksi.GetValueOrDefault(0)
            LblTransactionDate.Text = ObjListOfGeneratedIFTI.TransactionDate.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate()).ToString("dd-MM-yyyy").Replace("01-01-1900", "")
            LblTransactionType.Text = ObjListOfGeneratedIFTI.SwiftType
            LblValid.Text = ObjListOfGeneratedIFTI.TotalValid.GetValueOrDefault(0)
            Dim intIFTItype As Integer
            Using objIFTI_Type As TList(Of IFTI_Type) = DataRepository.IFTI_TypeProvider.GetPaged(IFTI_TypeColumn.IFTIType.ToString & "='" & ObjListOfGeneratedIFTI.SwiftType & "'", "", 0, Integer.MaxValue, 0)
                If objIFTI_Type.Count > 0 Then
                    intIFTItype = objIFTI_Type(0).PK_IFTI_Type_ID
                End If
            End Using


            Dim strfilter As New StringBuilder
            strfilter.Append(" IsDataValid=1 ")
            strfilter.Append(" and  TanggalTransaksi ='" & ObjListOfGeneratedIFTI.TransactionDate.GetValueOrDefault.ToString("yyyy-MM-dd") & "'")
            strfilter.Append(" and LastUpdateDate ='" & ObjListOfGeneratedIFTI.LastUpdateIFTIDate.GetValueOrDefault.ToString("yyyy-MM-dd") & "'")
            strfilter.Append(" and FK_IFTI_Type_ID='" & intIFTItype & "'")

            Dim objparameter As New System.Web.UI.WebControls.Parameter
            objparameter.Name = "WhereClause"
            objparameter.DefaultValue = strfilter.ToString()
            IFTIDS.Parameters.Add(objparameter)

        End If


    End Sub

    Protected Sub ImgAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgAccept.Click
        Try

            ListOfGeneratedIFTIBLL.confirm(Me.PK_ListOfGeneratedFileCTR_ID)
            Response.Redirect("ListOfGeneratedIFTIFile.aspx", False)
        Catch ex As Exception

            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
      
    End Sub

    Protected Sub ImgCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgCancel.Click
        Try
            Response.Redirect("ListofGeneratedIFTIFile.aspx", False)
        Catch ex As Exception

            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
