#Region "Imports..."
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Excel
Imports Sahassa.AML.Commonly
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports AMLBLL.DataType
#End Region

Partial Class MsKotaKab_upload
    Inherits Parent

#Region "Structure..."

    Structure GrouP_MsKotaKab_success
        Dim MsKotaKab As MsKotaKab_ApprovalDetail
        Dim L_MsKotaKab_NCBS As TList(Of MsKotaKabNCBS_approvalDetail)

    End Structure

#End Region

#Region "Property umum"
    Private Property GroupTableSuccessData() As List(Of GrouP_MsKotaKab_success)
        Get
            Return Session("GroupTableSuccessData")
        End Get
        Set(ByVal value As List(Of GrouP_MsKotaKab_success))
            Session("GroupTableSuccessData") = value
        End Set
    End Property

    Private Property GroupTableAnomalyData() As List(Of AMLBLL.MsKotaKabBll.DT_Group_MsKotaKab)
        Get
            Return Session("GroupTableAnomalyData")
        End Get
        Set(ByVal value As List(Of AMLBLL.MsKotaKabBll.DT_Group_MsKotaKab))
            Session("GroupTableAnomalyData") = value
        End Set
    End Property

    Public ReadOnly Property GET_PKUserID() As Integer
        Get
            Return SessionPkUserId
        End Get
    End Property

    Public ReadOnly Property GET_UserName() As String
        Get
            'Return SessionNIK
            Return SessionUserId
        End Get
    End Property

    Private Property SetnGetDataTableForSuccessData() As TList(Of MsKotaKab_ApprovalDetail)
        Get
            Return CType(IIf(Session("MsKotaKab_upload.TableSuccessUpload") Is Nothing, Nothing, Session("MsKotaKab_upload.TableSuccessUpload")), TList(Of MsKotaKab_ApprovalDetail))
        End Get
        Set(ByVal value As TList(Of MsKotaKab_ApprovalDetail))
            Session("MsKotaKab_upload.TableSuccessUpload") = value
        End Set
    End Property

    Private Property SetnGetDataTableForAnomalyData() As Data.DataTable
        Get
            Return CType(IIf(Session("MsKotaKab_upload.TableAnomalyUpload") Is Nothing, Nothing, Session("MsKotaKab_upload.TableAnomalyUpload")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsKotaKab_upload.TableAnomalyUpload") = value
        End Set
    End Property

    Private Property DSexcelTemp() As Data.DataSet
        Get
            Return CType(IIf(Session("MsKotaKab_upload.DSexcelTemp") Is Nothing, Nothing, Session("MsKotaKab_upload.DSexcelTemp")), DataSet)
        End Get
        Set(ByVal value As Data.DataSet)
            Session("MsKotaKab_upload.DSexcelTemp") = value
        End Set
    End Property

#End Region

#Region "Property Tabel Temporary"
    Private Property DT_AllMsKotaKab() As Data.DataTable
        Get
            Return CType(IIf(Session("MsKotaKab_upload.DT_MsKotaKab") Is Nothing, Nothing, Session("MsKotaKab_upload.DT_MsKotaKab")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsKotaKab_upload.DT_MsKotaKab") = value
        End Set
    End Property

    Private Property DT_AllMsKotaKab_NCBS() As Data.DataTable
        Get
            Return CType(IIf(Session("MsKotaKab_upload.DT_MsKotaKab_NCBS") Is Nothing, Nothing, Session("MsKotaKab_upload.DT_MsKotaKab_NCBS")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsKotaKab_upload.DT_MsKotaKab_NCBS") = value
        End Set
    End Property

#End Region

#Region "Property Table Sukses Data"

    Private Property TableSuccessMsKotaKab() As TList(Of MsKotaKab_ApprovalDetail)
        Get
            Return Session("TableSuccessMsKotaKab")
        End Get
        Set(ByVal value As TList(Of MsKotaKab_ApprovalDetail))
            Session("TableSuccessMsKotaKab") = value
        End Set
    End Property

    Private Property TableSuccessMsKotaKab_NCBS() As TList(Of MsKotaKab_NCBS)
        Get
            Return Session("TableSuccessMsKotaKab_NCBS")
        End Get
        Set(ByVal value As TList(Of MsKotaKab_NCBS))
            Session("TableSuccessMsKotaKab_NCBS") = value
        End Set
    End Property


#End Region

#Region "Property Table Anomaly Data"

    Private Property TableAnomalyMsKotaKab() As Data.DataTable
        Get
            Return Session("TableAnomalyMsKotaKab")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsKotaKab") = value
        End Set
    End Property

    Private Property TableAnomalyMsKotaKab_NCBS() As Data.DataTable
        Get
            Return Session("TableAnomalyMsKotaKab_NCBS")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsKotaKab_NCBS") = value
        End Set
    End Property

#End Region

#Region "validasi.."

    Private Function ValidateInput(ByVal MsKotaKab As AMLBLL.MsKotaKabBll.DT_Group_MsKotaKab) As List(Of String)
        Dim temp As New List(Of String)
        Dim msgError As New StringBuilder
        Dim errorline As New StringBuilder
        Try
            'validasi
            ValidateMsKotaKab(MsKotaKab.MsKotaKab, msgError, errorline)
            If msgError.ToString = "" Then
                For Each ci As DataRow In MsKotaKab.L_MsKotaKab_NCBS.Rows
                    'validasi NCBS
                    ValidateMsKotaKab_NCBS(ci, msgError, errorline)
                Next
            End If
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        Catch ex As Exception
            msgError.Append(ex.Message)
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        End Try
        Return temp
    End Function

    Private Shared Sub ValidateMsKotaKab_NCBS(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorLine As StringBuilder)
        Try

            '======================== Validasi textbox :  IDKotaKab_NCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("IDKotaKabBank")) = False Then
                msgError.AppendLine(" &bull; IDKotaKabBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsKotaKab_Bank : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  NamaKotaKab_NCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("NamaKotaKabBank")) = False Then
                msgError.AppendLine(" &bull; NamaKotaKabBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsKotaKab_Bank : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorLine.AppendLine(" &bull;MsKotaKab_Bank : Line " & DTR("NO") & "<BR/>")
        End Try
    End Sub

    Private Shared Sub ValidateMsKotaKab(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorline As StringBuilder)
        Try

            Using checkBlocking As TList(Of MsKotaKab_ApprovalDetail) = DataRepository.MsKotaKab_ApprovalDetailProvider.GetPaged("IDKotaKab=" & DTR("IDKotaKab"), "", 0, 1, Nothing)
                If checkBlocking.Count > 0 Then
                    msgError.AppendLine(" &bull;MsKotaKab is waiting for approval <BR/>")
                    errorline.AppendLine(" &bull;MsKotaKab : Line " & DTR("NO") & "<BR/>")
                End If
            End Using

            '======================== Validasi textbox :  IDKotaKab   canot Null (#)====================================================
            If ObjectAntiNull(DTR("IDKotaKab")) = False Then
                msgError.AppendLine(" &bull; ID must be filled <BR/>")
                errorline.AppendLine(" &bull; MsKotaKab : Line " & DTR("NO") & "<BR/>")
            End If
            If ObjectAntiNull(DTR("IDKotaKab")) Then
                If objectNumOnly(DTR("IDKotaKab")) = False Then
                    msgError.AppendLine(" &bull; ID must be Numeric <BR/>")
                    errorline.AppendLine(" &bull; MsKotaKab : Line " & DTR("NO") & "<BR/>")
                End If
            End If
            '======================== Validasi textbox :  NamaKotaKab   canot Null (#)====================================================
            If ObjectAntiNull(DTR("NamaKotaKab")) = False Then
                msgError.AppendLine(" &bull; Nama must be filled <BR/>")
                errorline.AppendLine(" &bull; MsKotaKab : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorline.AppendLine(" &bull;MsKotaKab  : Line " & DTR("NO") & "<BR/>")
            LogError(ex)
        End Try
    End Sub

#End Region

#Region "Upload File"

    Public Function SaveFileImportToSave() As String
        Dim filePath As String
        Dim FileNameToGetData As String = ""
        Try
            If FileUploadKPI.PostedFile.ContentLength = 0 Then
                Return FileNameToGetData
            End If

            If Not FileUploadKPI.HasFile Then
                Return FileNameToGetData
            End If

            filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName)

            FileNameToGetData = FileUploadKPI.FileName
            If System.IO.File.Exists(filePath) Then
                Dim bexist As Boolean = True
                Dim i As Integer = 1
                While bexist
                    filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & FileUploadKPI.FileName.Split(CChar("."))(1))
                    FileNameToGetData = FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & Me.FileUploadKPI.FileName.Split(CChar("."))(1)
                    If Not System.IO.File.Exists(filePath) Then
                        bexist = False
                        Exit While
                    End If
                    i = i + 1
                End While
            End If
            Me.FileUploadKPI.PostedFile.SaveAs(filePath)
            Session("Filepath") = filePath
            Return FileNameToGetData
        Catch
            Throw
        End Try
    End Function

    ReadOnly Property FilePath() As String
        Get
            Return Session("FilePath")
        End Get
    End Property

    Private Sub ProcessExcelFile(ByVal FullPathFileNameToExportExcel As String)

        Using stream As FileStream = File.Open(FullPathFileNameToExportExcel, FileMode.Open, FileAccess.Read)
            Using excelReader As IExcelDataReader = ExcelReaderFactory.CreateBinaryReader(stream)
                excelReader.IsFirstRowAsColumnNames = True
                DSexcelTemp = excelReader.AsDataSet()
                If DSexcelTemp.Tables("MsKotaKab").Columns(0).ColumnName Like "*Column*" Then
                    DSexcelTemp.Tables("MsKotaKab").Rows.Clear()
                End If

            End Using
        End Using

    End Sub


#End Region

#Region "Function..."

    Function getErrorNo(ByVal Id As String, ByVal nama As String) As String
        Dim temp As String = ""
        Try
            Dim DV As New DataView(DT_AllMsKotaKab_NCBS)
            'jika Null maka harus is null bukan kosong disini belum di handle
            DV.RowFilter = "IDKotaKab = '" & Id & "' and NamaKotaKab = '" & nama & "'"

            Dim DT As Data.DataTable = DV.ToTable
            If DT.Rows.Count > 0 Then
                temp = Safe(DT.Rows(0)("NO"))
            End If
        Catch ex As Exception
            temp = ex.Message
        End Try

        Return temp
    End Function
    ''' <summary>
    ''' Merubah excel ke Object Databale sekaligus memecahnya
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ImportExcelTempToDTproperty()
        Dim DT As New Data.DataTable
        'DT_MsKotaKab = DSexcelTemp.Tables("MsKotaKab")
        DT = DSexcelTemp.Tables("MsKotaKab")
        Dim DV As New DataView(DT)
        DT_AllMsKotaKab = DV.ToTable(True, "IDKotaKab", "NamaKotaKab", "IDPropinsi", "activation")
        DT_AllMsKotaKab_NCBS = DSexcelTemp.Tables("MsKotaKab")
        DT_AllMsKotaKab.Columns.Add("NO")
        For Each i As DataRow In DT_AllMsKotaKab.Rows
            Dim PK As String = Safe(i("IDKotaKab"))
            Dim nama As String = Safe(i("NamaKotaKab"))
            i("NO") = getErrorNo(PK, nama)
        Next
    End Sub

    Private Sub ClearThisPageSessions()
        DT_AllMsKotaKab = Nothing
        DT_AllMsKotaKab_NCBS = Nothing
        GroupTableSuccessData = Nothing
        GroupTableAnomalyData = Nothing
        DSexcelTemp = Nothing
        SetnGetDataTableForAnomalyData = Nothing
        SetnGetDataTableForSuccessData = Nothing
    End Sub


    Function ConvertDTtoMsKotaKab_NCBS(ByVal DT As Data.DataTable) As TList(Of MsKotaKabNCBS_approvalDetail)
        Dim temp As New TList(Of MsKotaKabNCBS_approvalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsKotaKab_NCBS As New MsKotaKabNCBS_approvalDetail
                With MsKotaKab_NCBS
                    FillOrNothing(.IDKotaKabNCBS, DTR("IDKotaKabBank"), True, oInt)
                    FillOrNothing(.NamaKotaKabNCBS, DTR("NamaKotaKabBank"), True, oString)
                    FillOrNothing(.Activation, 1, True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsKotaKab_NCBS)
                End With
            End Using

        Next

        Return temp
    End Function



    Function ConvertDTtoMsKotaKab(ByVal DTR As DataRow) As MsKotaKab_ApprovalDetail
        Dim temp As New MsKotaKab_ApprovalDetail
        Using MsKotaKab As New MsKotaKab_ApprovalDetail()
            With MsKotaKab
                FillOrNothing(.IDPropinsi, DTR("IDPropinsi"), True, oInt)
                FillOrNothing(.IDKotaKab, DTR("IDKotaKab"), True, oInt)
                FillOrNothing(.NamaKotaKab, DTR("NamaKotaKab"), True, Ovarchar)

                FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                FillOrNothing(.CreatedDate, Now, True, oDate)
            End With
            temp = MsKotaKab
        End Using

        Return temp
    End Function

    Function ConvertDTtoMsKotaKab(ByVal DT As Data.DataTable) As TList(Of MsKotaKab_ApprovalDetail)
        Dim temp As New TList(Of MsKotaKab_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsKotaKab As New MsKotaKab_ApprovalDetail()
                With MsKotaKab
                    FillOrNothing(.IDPropinsi, DTR("IDPropinsi"), True, oInt)
                    FillOrNothing(.IDKotaKab, DTR("IDKotaKab"), True, oInt)
                    FillOrNothing(.NamaKotaKab, DTR("NamaKotaKab"), True, Ovarchar)

                    FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsKotaKab)
                End With
            End Using
        Next
        Return temp
    End Function

    Private Sub ChangeMultiView(ByVal index As Integer)
        MultiViewKPIOvertimeUpload.ActiveViewIndex = index
        If index = 0 Then
            Imgbtnsave.Visible = False
            Imgbtncancel.Visible = False
        ElseIf index = 1 Then
            Imgbtncancel.Visible = True
        ElseIf index = 2 Then
            Imgbtncancel.Visible = False
            Imgbtnsave.Visible = False
        End If
    End Sub

    Private Function GenerateGroupingMsKotaKab() As List(Of AMLBLL.MsKotaKabBll.DT_Group_MsKotaKab)
        Dim Ltemp As New List(Of AMLBLL.MsKotaKabBll.DT_Group_MsKotaKab)
        For i As Integer = 0 To DT_AllMsKotaKab.Rows.Count - 1
            Dim MsKotaKab As New AMLBLL.MsKotaKabBll.DT_Group_MsKotaKab
            With MsKotaKab
                .MsKotaKab = DT_AllMsKotaKab.Rows(i)
                .L_MsKotaKab_NCBS = DT_AllMsKotaKab_NCBS.Clone

                'Insert MsKotaKab_NCBS
                Dim MsKotaKab_ID As String = Safe(.MsKotaKab("IDKotaKab"))
                Dim MsKotaKab_nama As String = Safe(.MsKotaKab("NamaKotaKab"))
                Dim MsKotaKab_Activation As String = Safe(.MsKotaKab("Activation"))
                If DT_AllMsKotaKab_NCBS.Rows.Count > 0 Then
                    Using DV As New DataView(DT_AllMsKotaKab_NCBS)
                        DV.RowFilter = "IDKotaKab='" & MsKotaKab_ID & "' and " & _
                                                  "NamaKotaKab='" & MsKotaKab_nama & "' and " & _
                                                  "activation='" & MsKotaKab_Activation & "'"
                        If DV.Count > 0 Then
                            Dim temp As Data.DataTable = DV.ToTable
                            For Each H As DataRow In temp.Rows
                                .L_MsKotaKab_NCBS.ImportRow(H)
                            Next
                        End If
                    End Using
                End If




            End With
            Ltemp.Add(MsKotaKab)
        Next
        Return Ltemp
    End Function

    Public Sub BindGrid()
        lblValueFailedData.Text = SetnGetDataTableForAnomalyData.Rows.Count
        GridAnomalyList.DataSource = SetnGetDataTableForAnomalyData
        GridAnomalyList.DataBind()
        lblValueSuccessData.Text = SetnGetDataTableForSuccessData.Count
        GridSuccessList.DataSource = SetnGetDataTableForSuccessData
        lblValueuploadeddata.Text = DT_AllMsKotaKab.Rows.Count
        GridSuccessList.DataBind()
    End Sub

#End Region

#Region "events..."


    Protected Sub ImgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtncancel.Click
        ClearThisPageSessions()
        ChangeMultiView(0)
        LinkButtonExportExcel.Visible = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If IsPostBack Then
                Return
            End If

            ClearThisPageSessions()
            SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            ChangeMultiView(0)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        ChangeMultiView(0)
    End Sub

    Protected Sub GridAnomalyList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridAnomalyList.PageIndexChanged
        GridAnomalyList.CurrentPageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GridSuccessList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridSuccessList.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                'Show mapping item
                Dim StringMaping As New StringBuilder
                Dim DV As New DataView(DT_AllMsKotaKab_NCBS)
                Dim DT As New Data.DataTable
                DV.RowFilter = "IDKotaKab='" & e.Item.Cells(1).Text & "' and " & "NamaKotaKab='" & e.Item.Cells(2).Text & "'"
                DT = DV.ToTable
                If DT.Rows.Count > 0 Then
                    Dim countField As Integer = 7
                    Dim lengthField As Integer = 0
                    For Each idx As DataRow In DT.Rows
                        With idx
                            If lengthField > countField Then
                                StringMaping.AppendLine(idx("NamaKotaKabBank") & "(" & idx("IDKotaKabBank") & ");")
                                lengthField = 0
                            Else
                                StringMaping.AppendLine(idx("NamaKotaKabBank") & "(" & idx("IDKotaKabBank") & ");")
                                lengthField += 1
                            End If

                        End With
                    Next
                Else
                    StringMaping.Append("-")
                End If
                e.Item.Cells(CollCount).Text = StringMaping.ToString

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridSuccessList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridSuccessList.PageIndexChanged
        Dim GridTarget As DataGrid = CType(source, DataGrid)
        Try
            GridSuccessList.CurrentPageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgSaveSuccessData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtnsave.Click
        Try
            'File belum diupload
            If DT_AllMsKotaKab Is Nothing Then
                Throw New Exception("Load data failed")
            End If

            'Save MsKotaKab Approval (Header)
            Dim KeyApp As String
            Using Aproval As New MsKotaKab_Approval()
                With Aproval
                    .RequestedBy = GET_PKUserID
                    .RequestedDate = Date.Now
                    .FK_MsMode_Id = 2 'Edit
                    .IsUpload = True
                End With
                DataRepository.MsKotaKab_ApprovalProvider.Save(Aproval)
                'ambil key approval
                KeyApp = Aproval.PK_MsKotaKab_Approval_Id
            End Using

            For k As Integer = 0 To GroupTableSuccessData.Count - 1

                Dim MsKotaKab As GrouP_MsKotaKab_success = GroupTableSuccessData(k)


                With MsKotaKab
                    'save MsKotaKab approval detil
                    .MsKotaKab.FK_MsKotaKab_Approval_Id = KeyApp
                    DataRepository.MsKotaKab_ApprovalDetailProvider.Save(.MsKotaKab)
                    'ambil key MsKotaKab approval detil
                    Dim IdMsKotaKab As String = .MsKotaKab.IDKotaKab
                    Dim L_MappingMsKotaKabNCBSPPATK_Approval_Detail As New TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail)
                    'save mapping dan NCBS
                    For Each OMsKotaKab_NCBS_ApprovalDetail As MsKotaKabNCBS_approvalDetail In .L_MsKotaKab_NCBS
                        OMsKotaKab_NCBS_ApprovalDetail.PK_MsKotaKab_Approval_id = KeyApp
                        Using OMappingMsKotaKabNCBSPPATK_Approval_Detail As New MappingMsKotaKabNCBSPPATK_Approval_Detail
                            With OMappingMsKotaKabNCBSPPATK_Approval_Detail
                                FillOrNothing(.IDKotaKabNCBS, OMsKotaKab_NCBS_ApprovalDetail.IDKotaKabNCBS, True, oInt)
                                FillOrNothing(.IDKotaKab, IdMsKotaKab, True, oInt)
                                FillOrNothing(.PK_MsKotaKab_Approval_Id, KeyApp, True, oInt)
                                FillOrNothing(.Nama, OMsKotaKab_NCBS_ApprovalDetail.NamaKotaKabNCBS, True, oString)
                            End With
                            L_MappingMsKotaKabNCBSPPATK_Approval_Detail.Add(OMappingMsKotaKabNCBSPPATK_Approval_Detail)
                        End Using
                    Next
                    'save maping dan NCBS
                    DataRepository.MappingMsKotaKabNCBSPPATK_Approval_DetailProvider.Save(L_MappingMsKotaKabNCBSPPATK_Approval_Detail)
                    DataRepository.MsKotaKabNCBS_approvalDetailProvider.Save(.L_MsKotaKab_NCBS)

                End With
            Next

            LblConfirmation.Text = "MsKotaKab data has edited (" & SetnGetDataTableForSuccessData.Count & " Rows / MsKotaKab)"
            ChangeMultiView(2)

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgUpload.Click
        Dim CombinedFileName As String
        GroupTableSuccessData = New List(Of GrouP_MsKotaKab_success)
        GroupTableAnomalyData = New List(Of AMLBLL.MsKotaKabBll.DT_Group_MsKotaKab)
        Try
            'validasi File
            If FileUploadKPI.FileName = "" Then
                Throw New Exception("Data required")
            End If

            If Path.GetExtension(FileUploadKPI.FileName) <> ".xls" Then
                Throw New Exception("Invalid File Upload Extension")
            End If

            CombinedFileName = SaveFileImportToSave()
            If String.Compare(CombinedFileName, "", False) <> 0 Then
                Try
                    ProcessExcelFile(FilePath)
                Catch ex As Exception
                    Throw New Exception("Invalid File Format")
                End Try
            Else
                Throw New Exception("Invalid File Format")
            End If

            'Import from Excell to property Global
            ImportExcelTempToDTproperty()

            Dim LG_MsKotaKab As List(Of AMLBLL.MsKotaKabBll.DT_Group_MsKotaKab) = GenerateGroupingMsKotaKab()
            'tambahkan kolom untuk tampilan di grid anomaly
            DT_AllMsKotaKab.Columns.Add("Description")
            DT_AllMsKotaKab.Columns.Add("errorline")

            'Membuat tempory untuk yang anomali dan yang sukses
            Dim DTnormal As Data.DataTable = DT_AllMsKotaKab.Clone
            Dim DTAnomaly As Data.DataTable = DT_AllMsKotaKab.Clone

            'proses pemilahan data anomaly dan data sukses melalui validasi 
            For i As Integer = 0 To LG_MsKotaKab.Count - 1
                Dim Lerror As List(Of String) = ValidateInput(LG_MsKotaKab(i))
                Dim MSG As String = Lerror(0)
                Dim LINE As String = Lerror(1)
                If MSG = "" Then
                    DTnormal.ImportRow(LG_MsKotaKab(i).MsKotaKab)
                    Dim GroupMsKotaKab As New GrouP_MsKotaKab_success
                    With GroupMsKotaKab
                        'konversi dari datatable ke Object Nettier
                        .MsKotaKab = ConvertDTtoMsKotaKab(LG_MsKotaKab(i).MsKotaKab)
                        .L_MsKotaKab_NCBS = ConvertDTtoMsKotaKab_NCBS(LG_MsKotaKab(i).L_MsKotaKab_NCBS)
                    End With
                    'masukan ke group sukses
                    GroupTableSuccessData.Add(GroupMsKotaKab)
                Else
                    DT_AllMsKotaKab.Rows(i)("Description") = MSG
                    DT_AllMsKotaKab.Rows(i)("errorline") = LINE
                    DTAnomaly.ImportRow(DT_AllMsKotaKab.Rows(i))
                    GroupTableAnomalyData.Add(LG_MsKotaKab(i))
                End If

            Next

            SetnGetDataTableForSuccessData = ConvertDTtoMsKotaKab(DTnormal)
            SetnGetDataTableForAnomalyData = DTAnomaly

            If DTAnomaly.Rows.Count > 0 Then
                Imgbtnsave.Visible = False
                Imgbtncancel.Visible = True
            Else
                Imgbtnsave.Visible = True
                Imgbtncancel.Visible = True
            End If

            BindGrid()
            If GridAnomalyList.Items.Count < 1 Then
                LinkButtonExportExcel.Visible = False
            Else
                LinkButtonExportExcel.Visible = True
            End If
            ChangeMultiView(1)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Dim Exp As New AMLBLL.MsKotaKabBll
            Dim FolderPath As String = Server.MapPath("FolderTemplate")

            Dim buffer As Byte() = Exp.ExportDTtoExcel(GroupTableAnomalyData, FolderPath)
            Response.AddHeader("Content-type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment; filename=MsKotaKabUpload.xls")
            Response.BinaryWrite(buffer)
            Response.Flush()
            Response.End()

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered 

    End Sub

    Protected Sub GridSuccessList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridSuccessList.SelectedIndexChanged

    End Sub
End Class





