#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
#End Region

Partial Class MsKelurahan_add
    Inherits Parent



#Region "Function"

    ''' <summary>
    ''' Validasi men-handle seluruh syarat2 data valid
    ''' data yang lolos dari validasi dipastikan lolos ke database 
    ''' karena menggunakan FillorNothing
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ValidasiControl()

        '========================== validasi popup ref :  Kecamatan canot null or empty string ==================================================
        If ObjectAntiNull(HFKecamatan.Value) = False Then Throw New Exception("Kecamatan Must Be Filled ")
        '======================== Validasi textbox :  ID canot Null ====================================================
        If ObjectAntiNull(txtIDKelurahan.Text) = False Then Throw New Exception("ID  must be filled  ")
        If Not IsNumeric(Me.txtIDKelurahan.Text) Then
            Throw New Exception("ID must be in number format")
        End If
        '======================== Validasi textbox :  Nama canot Null ====================================================
        If ObjectAntiNull(txtNamaKelurahan.Text) = False Then Throw New Exception("Nama  must be filled  ")


        If Not DataRepository.MsKelurahanProvider.GetByIDKelurahan(txtIDKelurahan.Text) Is Nothing Then
            Throw New Exception("ID already exist in the database")
        End If

        If DataRepository.MsKelurahan_ApprovalDetailProvider.GetPaged(MsKelurahan_ApprovalDetailColumn.IDKelurahan.ToString & "=" & txtIDKelurahan.Text, "", 0, 1, Nothing).Count > 0 Then
            Throw New Exception("Data exist in List Waiting Approval")
        End If


        'If LBMapping.Items.Count = 0 Then Throw New Exception("data doesnt have list Mapping")
    End Sub

#End Region

#Region "events..."
    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Response.Redirect("MsKelurahan_View.aspx")
    End Sub
    Protected Sub imgBrowse_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBrowse.Click
        Try
            If Not Get_DataFromPicker Is Nothing Then
                Listmaping.AddRange(Get_DataFromPicker)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgDeleteItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDeleteItem.Click
        Try
            If LBMapping.SelectedIndex < 0 Then
                Throw New Exception("No data Selected")
            End If
            Listmaping.RemoveAt(LBMapping.SelectedIndex)

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(Sahassa.CommonCTRWeb.Commonly.SessionCurrentPage)
                Listmaping = New TList(Of MappingMsKelurahanNCBSPPATK)
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Protected Sub imgBrowseNameKecamatan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBrowseNameKecamatan.Click
        Try
            If Session("PickerKecamatan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKecamatan.Data")).Split(";")
                LBSearchNamaKecamatan.Text = strData(1)
                HFKecamatan.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then
                ValidasiControl()
                'create audittrailtype
                'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsKelurahan_Approval As New MsKelurahan_Approval
                    With ObjMsKelurahan_Approval
                        FillOrNothing(.FK_MsMode_Id, 1, True, oInt)
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsKelurahan_ApprovalProvider.Save(ObjMsKelurahan_Approval)
                    KeyHeaderApproval = ObjMsKelurahan_Approval.PK_MsKelurahan_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsKelurahan_ApprovalDetail As New MsKelurahan_ApprovalDetail()
                    With objMsKelurahan_ApprovalDetail
                        FillOrNothing(.IDKecamatan, HFKecamatan.Value, True, oInt)
                        FillOrNothing(.IDKelurahan, txtIDKelurahan.Text, True, oInt)
                        FillOrNothing(.NamaKelurahan, txtNamaKelurahan.Text, True, Ovarchar)

                        FillOrNothing(.Activation, 1, True, oBit)
                        FillOrNothing(.CreatedDate, Date.Now)
                        FillOrNothing(.CreatedBy, SessionUserId)
                        FillOrNothing(.FK_MsKelurahan_Approval_Id, KeyHeaderApproval)
                    End With
                    DataRepository.MsKelurahan_ApprovalDetailProvider.Save(objMsKelurahan_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsKelurahan_ApprovalDetail.PK_MsKelurahan_ApprovalDetail_Id
                    'Insert auditrail
                    'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "Request for approval inserted  data MsKelurahan", objMsKelurahan_ApprovalDetail, Nothing)
                    '========= Insert mapping item 
                    Dim LobjMappingMsKelurahanNCBSPPATK_Approval_Detail As New TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail)
                    For Each objMappingMsKelurahanNCBSPPATK As MappingMsKelurahanNCBSPPATK In Listmaping
                        Dim Mapping As New MappingMsKelurahanNCBSPPATK_Approval_Detail
                        With Mapping
                            FillOrNothing(.IDKelurahan, objMsKelurahan_ApprovalDetail.IDKelurahan)
                            FillOrNothing(.IDKelurahanNCBS, objMappingMsKelurahanNCBSPPATK.IDKelurahanNCBS)
                            FillOrNothing(.PK_MsKelurahan_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.PK_MappingMsKelurahanNCBSPPATK_approval_detail_Id, keyHeaderApprovalDetail)
                            FillOrNothing(.Nama, objMappingMsKelurahanNCBSPPATK.Nama)
                            LobjMappingMsKelurahanNCBSPPATK_Approval_Detail.Add(Mapping)
                        End With
                    Next
                    DataRepository.MappingMsKelurahanNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsKelurahanNCBSPPATK_Approval_Detail)
                    'session Maping item Clear
                    Listmaping.Clear()
                    LblConfirmation.Text = "MsKelurahan data is waiting approval for Insert"
                    MtvMsUser.ActiveViewIndex = 1
                    ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Try
            ClearAllControl(VwAdd)
            MtvMsUser.ActiveViewIndex = 0
            ImgBtnSave.Visible = True
            ImgBackAdd.Visible = True
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Response.Redirect("MsKelurahan_View.aspx")
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub

#End Region

#Region "Property..."
    ''' <summary>
    ''' Mengambil Data dari Picker
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Get_DataFromPicker() As TList(Of MappingMsKelurahanNCBSPPATK)
        Get
            Dim Temp As New TList(Of MappingMsKelurahanNCBSPPATK)
            Dim L_MsKelurahanNCBS As TList(Of MsKelurahanNCBS)
            Try
                If Session("PickerMsKelurahanNCBS.Data") Is Nothing Then
                    Throw New Exception
                End If
                L_MsKelurahanNCBS = Session("PickerMsKelurahanNCBS.Data")
                For Each i As MsKelurahanNCBS In L_MsKelurahanNCBS
                    Dim Tempmapping As New MappingMsKelurahanNCBSPPATK
                    With Tempmapping
                        .IDKelurahanNCBS = i.IDKelurahanNCBS
                        .Nama = i.NamaKelurahanNCBS
                    End With
                    Temp.Add(Tempmapping)
                Next
            Catch ex As Exception
                L_MsKelurahanNCBS = Nothing
            End Try
            Return Temp
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsKelurahanNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKelurahanNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKelurahanNCBSPPATK In Listmaping.FindAllDistinct("IDKelurahanNCBS")
                Temp.Add(i.IDKelurahanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

End Class


