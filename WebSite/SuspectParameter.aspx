<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SuspectParameter.aspx.vb" Inherits="SuspectParameter" title="Suspect Parameter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
	    border="2">
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="5" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Suspect Parameter&nbsp;
                    <hr />
                </strong>
                <asp:Label CssClass="validationok"  ID="LblSucces" runat="server"  Visible="False" Width="100%"></asp:Label></td>
        </tr>
	    <tr class="formText">
		    <td width="5" bgColor="#ffffff" height="24"><asp:requiredfieldvalidator id="RequiredFieldValidatorPercentageSuspect" runat="server" ErrorMessage="Percentage Suspect cannot be blank"
				    Display="Dynamic" ControlToValidate="TextPercentageSuspect">*</asp:requiredfieldvalidator><br />
                <asp:RangeValidator ID="RangeValidatorPercentageSuspect" runat="server" ControlToValidate="TextPercentageSuspect"
                    Display="Dynamic" ErrorMessage="Percentage Suspect must be numeric value between 0 and 100"
                    MaximumValue="100" MinimumValue="0" Type="Integer">*</asp:RangeValidator>
            </td>
            <td bgcolor="#ffffff" height="24" style="width: 15px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
		    <td width="20%" bgColor="#ffffff" colspan="2">
                Percentage Suspect &gt;=</td>
		    <td width="70%" bgColor="#ffffff"><asp:textbox id="TextPercentageSuspect" runat="server" CssClass="textBox" MaxLength="20" Width="50px"></asp:textbox>&nbsp;%</td>
	    </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="width: 15px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
            <td width="10%" bgcolor="#ffffff" height="24" style="width: 15px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <strong>Field</strong></td>
            <td bgcolor="#ffffff" colspan="2" style="width: 15px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="width: 15px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
        </tr>
	    <tr class="formText">
		    <td bgcolor="#ffffff" height="24">
			    <asp:RequiredFieldValidator id="RequiredFieldValidatorName" runat="server" Display="Dynamic" ErrorMessage="Name cannot be blank"
				    ControlToValidate="TextName">*</asp:RequiredFieldValidator><br />
                <asp:RangeValidator ID="RangeValidatorName" runat="server" ControlToValidate="TextName"
                    Display="Dynamic" ErrorMessage="Name Percentage must be numeric value between 0 and 100"
                    MaximumValue="100" MinimumValue="0" Type="Integer">*</asp:RangeValidator>&nbsp;</td>
            <td bgcolor="#ffffff" height="24" style="width: 15px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
		    <td bgcolor="#ffffff" style="text-align: right" colspan="2">
                Name</td>
		    <td bgcolor="#ffffff">
			    <asp:TextBox id="TextName" runat="server" Width="50px" CssClass="textBox"></asp:TextBox>&nbsp;%</td>
	    </tr>
	    <tr class="formText">
		    <td bgcolor="#ffffff" style="height: 32px">
			    <asp:RequiredFieldValidator id="RequiredFieldValidatorDOB" runat="server" Display="Dynamic" ErrorMessage="Date of Birth cannot be blank"
				    ControlToValidate="TextDOB">*</asp:RequiredFieldValidator><br />
                <asp:RangeValidator ID="RangeValidatorDOB" runat="server" ControlToValidate="TextDOB"
                    Display="Dynamic" ErrorMessage="Date of Birth Percentage must be numeric value between 0 and 100"
                    MaximumValue="100" MinimumValue="0" Type="Integer">*</asp:RangeValidator>&nbsp;</td>
            <td bgcolor="#ffffff" style="width: 15px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
		    <td bgcolor="#ffffff" style="height: 32px; text-align: right;" colspan="2">
                Date of 
                Birth</td>
		    <td bgcolor="#ffffff" style="height: 32px">
			    <asp:TextBox id="TextDOB" runat="server" Width="50px" CssClass="textBox"></asp:TextBox>&nbsp;%</td>
	    </tr>
	    <tr class="formText">
		    <td bgcolor="#ffffff" height="24">
			    <asp:RequiredFieldValidator id="RequiredFieldValidatorIDNo" runat="server" Display="Dynamic" ErrorMessage="ID Number cannot be blank"
				    ControlToValidate="TextIDNo">*</asp:RequiredFieldValidator><br />
                <asp:RangeValidator ID="RangeValidatorIDNo" runat="server" ControlToValidate="TextIDNo"
                    Display="Dynamic" ErrorMessage="ID Number Percentage must be numeric value between 0 and 100"
                    MaximumValue="100" MinimumValue="0" Type="Integer">*</asp:RangeValidator>&nbsp;</td>
            <td bgcolor="#ffffff" height="24" style="width: 15px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
		    <td bgcolor="#ffffff" style="text-align: right" colspan="2">
                ID Number</td>
		    <td bgcolor="#ffffff">
			    <asp:TextBox id="TextIDNo" runat="server" Width="50px" CssClass="textBox"></asp:TextBox>&nbsp;%</td>
	    </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24">
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorAddress" runat="server" ControlToValidate="TextAddress"
                    Display="Dynamic" ErrorMessage="Address cannot be blank">*</asp:RequiredFieldValidator><br />
                <asp:RangeValidator ID="RangeValidatorAddress" runat="server" ControlToValidate="TextAddress"
                    Display="Dynamic" ErrorMessage="Address Percentage must be numeric value between 0 and 100"
                    MaximumValue="100" MinimumValue="0" Type="Integer">*</asp:RangeValidator>&nbsp;</td>
            <td bgcolor="#ffffff" height="24" style="width: 15px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" colspan="2" style="text-align: right">
                Address</td>
            <td bgcolor="#ffffff">
                <asp:TextBox ID="TextAddress" runat="server" CssClass="textBox" Width="50px"></asp:TextBox>
                %</td>
        </tr>
	    <tr class="formText" bgColor="#dddddd" height="30">
		    <td width="15"><IMG height="15" src="images/arrow.gif" width="15"></td>
            <td style="width: 15px">
            </td>
		    <td colSpan="3">
			    <table cellSpacing="0" cellPadding="3" border="0">
				    <tr>
					    <td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="SaveButton"></asp:imagebutton></td>
					    <td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
				    </tr>
			    </table>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator></td>
	    </tr>
    </table>
	<script language="javascript">
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>