Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper
Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Partial Class ForceChangePassword
    Inherits System.Web.UI.Page

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPkUserID() As String
        Get
            Return Me.Request.Params("UId")
        End Get
    End Property

    ''' <summary>
    ''' get id for focues
    ''' </summary>
    ''' <value></value>
    ''' <returns>element control id</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TextPassword.ClientID
        End Get
    End Property
    Public Shared Sub CekPasswordChar(ByVal strPassword As String)
        Dim ObjLoginParameter As TList(Of LoginParameter)
        Try
            Dim dataParameter As LoginParameter = DataRepository.LoginParameterProvider.GetPaged("PK_LoginParameter_ID = 1", "", 0, Integer.MaxValue, 0)(0)
            ObjLoginParameter = DataRepository.LoginParameterProvider.GetPaged("PK_LoginParameter_ID = 1", Nothing, 0, 1, 1)
            ' Alphanumeric & Symbol
            If ObjLoginParameter.Count > 0 Then
                If dataParameter.PasswordChar.ToString = True Then
                    If Not (System.Text.RegularExpressions.Regex.IsMatch(strPassword, "^((?=.*[a-z])|(?=.*[A-Z]))(?=.*\d)(?=.*[\W]).*$")) Then
                        Throw New Exception("Password must has alphanumeric and symbol character")
                    End If
                End If
            End If
        Finally
            ObjLoginParameter = Nothing
        End Try
    End Sub
    Public Shared Sub CekPasswordCombination(ByVal strPassword As String)
        Dim ObjLoginParameter As TList(Of LoginParameter)
        Try
            Dim dataParameter As LoginParameter = DataRepository.LoginParameterProvider.GetPaged("PK_LoginParameter_ID = 1", "", 0, Integer.MaxValue, 0)(0)
            ObjLoginParameter = DataRepository.LoginParameterProvider.GetPaged("PK_LoginParameter_ID = 1", Nothing, 0, 1, 1)
            ' passwordcombination
            If ObjLoginParameter.Count > 0 Then
                If dataParameter.PasswordCombination.ToString = True Then
                    If Not (System.Text.RegularExpressions.Regex.IsMatch(strPassword, "^(?=.*[a-z])(?=.*[A-Z]).*$")) Then
                        Throw New Exception("Password must has a combination of lower and upper case character")

                    End If
                End If
            End If
        Finally
            ObjLoginParameter = Nothing
        End Try
    End Sub

    Private Sub LogError(ByVal ex As Exception)
        Dim Logger As log4net.ILog = log4net.LogManager.GetLogger("AMLError")
        Logger.Error("Exception has occured", ex)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillEditData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
            Using TableUser As AMLDAL.AMLDataSet.UserDataTable = AccessUser.GetDataByUserID(Me.GetPkUserID)
                If TableUser.Rows.Count > 0 Then
                    Dim TableRowUser As AMLDAL.AMLDataSet.UserRow = TableUser.Rows(0)
                    Session("ForceChangePasswordUserID_Old") = TableRowUser.UserID
                    Me.LabelUserId.Text = Session("ForceChangePasswordUserID_Old")

                    Session("ForceChangePasswordSalt") = TableRowUser.UserPasswordSalt
                Else
                    Throw New Exception("Cannot load data from the following User: " & Me.LabelUserId.Text & " because that User no longer exists in the database.")
                End If
            End Using
        End Using
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Page.Validate()

        If Page.IsValid Then
            
            Dim oTrans As SqlTransaction = Nothing
            Try
                CekPasswordChar(TextPassword.Text)
                CekPasswordCombination(TextPassword.Text)
                Dim MinimumPasswordLength As Int32 = Me.GetMinimumPasswordLength
                If Me.TextPassword.Text.Length < MinimumPasswordLength Then
                    Throw New Exception("Cannot change password for the following User : " & Me.LabelUserId.Text & " because the minimum password length is " & MinimumPasswordLength & " characters")
                Else
                    Dim PasswordRecycleCount As Int32 = 3

                    'Encrypt password baru
                    Dim salt As String = Session("ForceChangePasswordSalt")
                    If Not salt Is Nothing Then
                        Dim NewPassword As String = Sahassa.AML.Commonly.Encrypt(Me.TextPassword.Text, salt)

                        'Periksa apakah password itu melanggar aturan Password Recycle Count atau tidak
                        Using AccessLoginParameter As New AMLDAL.AMLDataSetTableAdapters.LoginParameterTableAdapter
                            Using LoginParamterTable As AMLDAL.AMLDataSet.LoginParameterDataTable = AccessLoginParameter.GetData

                                If LoginParamterTable.Rows.Count <> 0 Then
                                    Dim LoginParameterRow As AMLDAL.AMLDataSet.LoginParameterRow = LoginParamterTable.Rows(0)
                                    PasswordRecycleCount = LoginParameterRow.PasswordRecycleCount
                                End If
                            End Using
                        End Using

                        If Me.CheckIfPasswordIsOK(Me.LabelUserId.Text, NewPassword, PasswordRecycleCount) Then
                            '  Using TranScope As New Transactions.TransactionScope
                            Using AccessHistoryPassword As New AMLDAL.AMLDataSetTableAdapters.HistoryPasswordTableAdapter
                                oTrans = BeginTransaction(AccessHistoryPassword)
                                AccessHistoryPassword.Insert(Me.LabelUserId.Text, NewPassword, Now)
                            End Using

                            Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                                SetTransaction(AccessUser, oTrans)
                                AccessUser.ChangeUserPassword(Me.LabelUserId.Text, NewPassword, Now)
                            End Using


                            oTrans.Commit()
                            Sahassa.AML.Commonly.SessionIntendedPage = "Login.aspx"
                            Me.Response.Redirect("Login.aspx", False)
                        Else
                            Throw New Exception("Cannot change password for the following User : " & Me.LabelUserId.Text & " because the new password violates the Password Recycle Count rule.")
                        End If
                    Else
                        Throw New Exception("Invalid session data, please retry again. It this error occurred persistance, please report to your administrator.")
                    End If
                End If
            Catch ex As Exception
                If Not oTrans Is Nothing Then
                    oTrans.Rollback()
                End If
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oTrans Is Nothing Then
                    oTrans.Dispose()
                End If
            End Try
        End If
    End Sub

    Private Function CheckIfPasswordIsOK(ByVal UserId As String, ByVal EncryptedPassword As String, ByVal PasswordRecycleCount As Int32) As Boolean
        Try
            Dim connectionString As String = WebConfigurationManager.ConnectionStrings("AMLConnectionString").ConnectionString
            Dim con As New SqlConnection(connectionString)
            Dim QueryString As String
            'QueryString = String.Format("select count(*) from (select top {0} HistoryPasswordUserPassword from HistoryPassword order by HistoryPasswordEntryDate desc)a where HistoryPasswordUserPassword = '{1}'", PasswordRecycleCount, EncryptedPassword)
            QueryString = String.Format("select count(*) from (select top {0} HistoryPasswordUserPassword from HistoryPassword where HistoryPasswordUserId='{1}' order by HistoryPasswordEntryDate desc)a where HistoryPasswordUserPassword = '{2}'", PasswordRecycleCount, UserId, EncryptedPassword)


            Dim cmd As New SqlCommand()
            Dim count As Int32

            cmd.Connection = con
            cmd.CommandType = CommandType.Text
            cmd.CommandText = QueryString

            con.Open()
            count = CType(cmd.ExecuteScalar(), Int32)
            con.Close()

            If count > 0 Then
                Return False
            Else
                Return True
            End If
        Catch
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Get Minimum Password Length
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetMinimumPasswordLength() As Int32
        Using AccessLoginParameter As New AMLDAL.AMLDataSetTableAdapters.LoginParameterTableAdapter
            Using LoginParameterTable As AMLDAL.AMLDataSet.LoginParameterDataTable = AccessLoginParameter.GetData

                If LoginParameterTable.Rows.Count = 0 Then
                    Return 6 'Default minimum password length = 6
                Else
                    Dim LoginParameterRow As AMLDAL.AMLDataSet.LoginParameterRow = LoginParameterTable.Rows(0)
                    Return LoginParameterRow.MinimumPasswordLength
                End If
            End Using
        End Using
    End Function

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "Login.aspx"
        Me.Response.Redirect("Login.aspx", False)
    End Sub
End Class
