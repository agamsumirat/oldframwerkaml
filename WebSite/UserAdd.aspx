<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UserAdd.aspx.vb" Inherits="UserAdd" title="User Add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="231" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4">
                <strong><span style="font-size: 18px">User - Add New</span></strong><br />
                <hr />
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4"><span style="color: #ff0000">* Required</span></td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24" style="width: 22px"><asp:requiredfieldvalidator id="RequiredFieldValidatorUserID" runat="server" ErrorMessage="User ID is required" Display="Dynamic"
					ControlToValidate="TextUserid">*</asp:requiredfieldvalidator><br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorUserID" runat="server" ControlToValidate="TextUserid"
                    ErrorMessage="User ID must starts with a letter then it can be followed by any character" ValidationExpression="[a-zA-Z](\w*\s*)*">*</asp:RegularExpressionValidator></td>
			<td width="20%" bgColor="#ffffff">User ID</td>
			<td width="5" bgColor="#ffffff">:</td>
			<td width="80%" bgColor="#ffffff"><asp:textbox id="TextUserid" runat="server" CssClass="textBox" MaxLength="10" Width="200px"></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong>&nbsp;</td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24" style="width: 22px"><asp:requiredfieldvalidator id="RequiredFieldValidatorFirstName" runat="server" ErrorMessage="First Name is required" Display="Dynamic"
					ControlToValidate="TextUserName">*</asp:requiredfieldvalidator><br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorFirstName" runat="server" ControlToValidate="TextUserName"
                    ErrorMessage="User Name must starts with a letter then it can be followed by other characters." ValidationExpression="[a-zA-Z](\w*\s*)*">*</asp:RegularExpressionValidator></td>
			<td bgColor="#ffffff">
                User Name</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff"><asp:textbox id="TextUserName" runat="server" CssClass="textBox" MaxLength="50" Width="200px"></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24" style="width: 22px"><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ErrorMessage="Password is required" Display="Dynamic"
					ControlToValidate="TextPassword">*</asp:requiredfieldvalidator>
				</td>
			<td bgColor="#ffffff">Password</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff"><asp:textbox id="TextPassword" runat="server" CssClass="textBox" MaxLength="50" TextMode="Password"  Width="200px"></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24" style="width: 22px"><asp:comparevalidator id="CompareValidator1" runat="server" ErrorMessage="Your entry in the Confirm Password textbox must be the same as the entry in the Password textbox " Display="Dynamic"
					ControlToValidate="TextPassword" ControlToCompare="TextBoxRetype">*</asp:comparevalidator></td>
			<td bgColor="#ffffff">
                Confirm Password</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff"><asp:textbox id="TextBoxRetype" runat="server" CssClass="textBox" MaxLength="50" TextMode="Password"  Width="200px"></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24" style="width: 22px"><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextboxEmailAddr"
                    Display="Dynamic" ErrorMessage="Email Address is required">*</asp:RequiredFieldValidator><br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmailAddress" runat="server" ControlToValidate="TextboxEmailAddr"
                    ErrorMessage="Email format is incorrect" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
			<td bgColor="#ffffff">
                Email Address</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff">
                <asp:TextBox ID="TextboxEmailAddr" runat="server" CssClass="textBox" MaxLength="50"  Width="200px"></asp:TextBox>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24" style="width: 22px">
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorMobilePhone" runat="server" ControlToValidate="TextboxMobilePhone"
                    ErrorMessage="Mobile Phone must be digits only and at least 8 digits long" ValidationExpression="\d{8}\d*">*</asp:RegularExpressionValidator></td>
			<td bgColor="#ffffff">
                Mobile Phone</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff">
                <asp:TextBox ID="TextboxMobilePhone" runat="server" CssClass="textBox" MaxLength="15"  Width="200px"></asp:TextBox></td>
		</tr>
        <tr class="formText">
			<td bgColor="#ffffff" height="24">
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorTellerID" runat="server" ControlToValidate="TextboxTellerID"
                    Display="Dynamic" ErrorMessage="Teller ID cannot be blank">*</asp:RequiredFieldValidator><br />
			<td bgColor="#ffffff">
                Teller ID</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff">
                <asp:TextBox ID="TextboxTellerID" runat="server" CssClass="textBox" MaxLength="10" Width="200px" ></asp:TextBox>
                <strong><span style="color: #ff0000">*</span></strong>
			</td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24" style="width: 22px">&nbsp;</td>
			<td bgColor="#ffffff">
                Group</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff"><asp:dropdownlist id="DropdownlistGroup" runat="server" CssClass="comboBox"></asp:dropdownlist>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="AddButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>              
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	<script>
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>

