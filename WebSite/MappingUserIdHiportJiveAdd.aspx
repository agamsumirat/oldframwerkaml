<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MappingUserIdHiportJiveAdd.aspx.vb" Inherits="MappingUserIdHiportJiveAdd"  %>
<%@ Register Src="webcontrol/PopUpUser.ascx" TagName="PopUpUser" TagPrefix="uc1" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<ajax:AjaxPanel ID="AjaxPanel2" runat="server" >
    <uc1:PopUpUser ID="PopUpUser1" runat="server" />
</ajax:AjaxPanel> 
<ajax:AjaxPanel runat="server" ID="Ajax1">

<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Mapping UserID Hiport Jive - Add 
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000">* Required</span></td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px">
                </td>
			<td width="20%" bgColor="#ffffff" style="height: 24px">
                User Id</td>
			<td width="5" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgcolor="#ffffff" style="height: 24px">
                            <strong><span>
                            <asp:HiddenField ID="HUserID" runat="server" />
                            <asp:Label ID="LblUserID" runat="server">Click Browse Button To Choose User</asp:Label>
                            </span></strong>
                            &nbsp;&nbsp;
                        
                            <asp:LinkButton ID="btnBrowse" runat="server" CausesValidation="False" CssClass="ovalbutton"><span>Browse</span></asp:LinkButton>
                            
                        </td>
		</tr>
		<tr class="formText">
            <td bgColor="#ffffff" style="width: 24px; height: 24px">
                &nbsp;</td>
            <td bgColor="#ffffff" style="height: 24px" width="20%">
                User Name</td>
            <td bgColor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <strong><span>
                <asp:Label ID="LabelUserName" runat="server" Width="173px"></asp:Label>
                </span></strong>
            </td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px">
                </td>
			<td width="20%" bgColor="#ffffff" style="height: 24px">
                Kode AO</td>
			<td width="5" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px">
                &nbsp;<asp:TextBox ID="TxtKodeAO" runat="server" Width="175px"></asp:TextBox><strong><span style="color: #ff0000"></span></strong></td>
		</tr>

		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="AddButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	</ajax:AjaxPanel>
</asp:Content>


