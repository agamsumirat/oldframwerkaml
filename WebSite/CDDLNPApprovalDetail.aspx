<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CDDLNPApprovalDetail.aspx.vb" Inherits="CDDLNPApprovalDetail" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script src="Script/popcalendar.js"></script>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td style="width: 3px">
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div>
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <table style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" id="title" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                        width="100%" border="2">
                                        <tbody>
                                            <tr>
                                                <td style="font-size: 18px; border-top-style: none; border-right-style: none; border-left-style: none;
                                                    border-bottom-style: none" bgcolor="#ffffff" colspan="3">
                                                    <img height="17" src="Images/dot_title.gif" width="17" />
                                                    <strong>
                                                        <asp:Label ID="lblHeader" runat="server" Text="EDD Detail"></asp:Label>
                                                    </strong>
                                                    <hr />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%" align="center"
                                        bgcolor="#dddddd" border="2">
                                        <tbody>
                                            <tr id="SearchCriteria">
                                                <td valign="top" bgcolor="#ffffff">
                                                    <table style="border-top-style: none; border-right-style: none; border-left-style: none;
                                                        border-bottom-style: none" cellspacing="1" cellpadding="2" width="100%" border="0">
                                                        <tbody>
                                                            <%--<tr>
                                                                <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                </td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    Nasabah</td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    Beneficial Owner</td>
                                                            </tr>--%>
                                                            <tr>
                                                                <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                    EDD Type</td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <asp:Label ID="lblCDDTypeNasabah" runat="server"></asp:Label>
                                                                    <asp:HiddenField ID="hfCDDTypeNasabah" runat="server"></asp:HiddenField>
                                                                </td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <asp:Label ID="lblCDDTypeBO" runat="server" Visible="false">-</asp:Label>
                                                                    <asp:HiddenField ID="hfCDDTypeBO" runat="server"></asp:HiddenField>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                    Customer Type</td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <strong>
                                                                        <asp:Label ID="lblCustomerTypeNasabah" runat="server"></asp:Label>
                                                                    </strong>
                                                                    <asp:HiddenField ID="hfCustomerTypeNasabah" runat="server"></asp:HiddenField>
                                                                </td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <strong>
                                                                        <asp:Label ID="lblCustomerTypeBO" runat="server">-</asp:Label>
                                                                    </strong>
                                                                    <asp:HiddenField ID="hfCustomerTypeBO" runat="server"></asp:HiddenField>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                    Customer Name</td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <asp:Label ID="lblNameNasabah" runat="server"></asp:Label></td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <asp:Label ID="lblNameBO" runat="server">-</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                    AML Risk Rating Personal</td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <asp:Label ID="lblRatingPersonalNasabah" runat="server"></asp:Label></td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <asp:Label ID="lblRatingPersonalBO" runat="server">-</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                    AML Risk Rating Gabungan (Final)</td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <asp:Label ID="lblRatingGabunganNasabah" runat="server"></asp:Label></td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <asp:Label ID="lblRatingGabunganBO" runat="server">-</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                    Politically Exposed Person</td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <asp:Label ID="lblIsPEPNasabah" runat="server"></asp:Label></td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <asp:Label ID="lblIsPEPBO" runat="server">-</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                    Screening Result</td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <asp:LinkButton ID="lnkSuspectNasabah" runat="server" ajaxcall="none"></asp:LinkButton><asp:Label
                                                                        ID="lblScreeningNasabah" runat="server">-</asp:Label></td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <asp:LinkButton ID="lnkSuspectBO" runat="server" ajaxcall="none"></asp:LinkButton><asp:Label
                                                                        ID="lblScreeningBO" runat="server">-</asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                    EDD Form</td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <asp:LinkButton ID="lnkCDDNasabah" runat="server" ajaxcall="none">View EDD Nasabah</asp:LinkButton><asp:Label
                                                                        ID="lblCDDNasabah" runat="server">-</asp:Label></td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    <asp:LinkButton ID="lnkCDDBO" runat="server" ajaxcall="none">View EDD Beneficial Owner</asp:LinkButton><asp:Label
                                                                        ID="lblCDDBO" runat="server">-</asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td nowrap="nowrap" style="width: 15%; background-color: #fff7e6" valign="top">
                                                                    Attachment</td>
                                                                <td nowrap="nowrap" style="width: 35%; background-color: #ffffff" id="RowAttachmentNasabah" runat="server">
                                                                </td>
                                                                <td nowrap="nowrap" style="width: 35%; background-color: #ffffff" id="RowAttachmentBO" runat="server">
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%" align="center"
                                        bgcolor="#dddddd" border="2">
                                        <tbody>
                                            <tr id="TR1">
                                                <td valign="top" width="98%" bgcolor="#ffffff">
                                                    <table bordercolor="#ffffff" cellspacing="1" cellpadding="0" width="100%" bgcolor="#dddddd"
                                                        border="2">
                                                        <tbody>
                                                            <tr>
                                                                <td bgcolor="#ffffff">
                                                                    <asp:DataGrid ID="GridViewCDDLNP" runat="server" Width="100%" ForeColor="Black" BorderColor="#DEDFDE"
                                                                        AllowSorting="True" GridLines="Vertical" BorderStyle="None" BorderWidth="1px"
                                                                        CellPadding="4" BackColor="White" Font-Size="XX-Small" AutoGenerateColumns="False">
                                                                        <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                                        <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
                                                                        <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                                        <ItemStyle BackColor="#F7F7DE"></ItemStyle>
                                                                        <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B"></HeaderStyle>
                                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Visible="False" />
                                                                        <Columns>
                                                                            <asp:TemplateColumn Visible="False">
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                                                                </ItemTemplate>
                                                                                <HeaderStyle Width="2%" />
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn DataField="PK_CDDLNP_Approval_ID" HeaderText="Ref. ID">
                                                                                <HeaderStyle ForeColor="White" />
                                                                                <ItemStyle Width="5%" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn HeaderText="No" Visible="false">
                                                                                <HeaderStyle ForeColor="White" />
                                                                                <ItemStyle Width="3%" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="LastApprovalStatus" Visible="False" />
                                                                            <asp:BoundColumn DataField="LastApprovalBy" Visible="False" />
                                                                            <asp:BoundColumn DataField="LastApprovalDate" HeaderText="Date Time" SortExpression="LastApprovalDate  desc"
                                                                                DataFormatString="{0:dd-MMM-yyyy HH:mm}">
                                                                                <ItemStyle Width="15%" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="LastWorkflowGroup" Visible="False" />
                                                                            <asp:BoundColumn DataField="LastWorkflowLevel" Visible="False" />
                                                                            <asp:BoundColumn HeaderText="Approval Track Records">
                                                                                <HeaderStyle ForeColor="White" />
                                                                                <ItemStyle Width="30%" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="Comments" HeaderText="Comments">
                                                                                <HeaderStyle ForeColor="White" />
                                                                                <ItemStyle Width="50%" />
                                                                            </asp:BoundColumn>
                                                                        </Columns>
                                                                    </asp:DataGrid>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="RowComment" runat="server" visible="false">
                                                <td>
                                                    Comments:<br />
                                                    <asp:TextBox ID="txtComments" runat="server" CssClass="textbox" Width="500px" TextMode="MultiLine"
                                                        Height="121px"></asp:TextBox></td>
                                            </tr>
                                            <tr id="RowApprover" runat="server" visible="false">
                                                <td>
                                                    <asp:CheckBox ID="chkPernyataanReview" runat="server" Text="Saya menyatakan bahwa saya telah melakukan review terhadap CDD ini dan menyatakan setuju dengan hasil CDD yang dilakukan oleh RM, dan berdasarkan pengetahuan saya, ketentuan CDD PT Bank CIMB Niaga telah dipenuhi."
                                                        Font-Bold="True"></asp:CheckBox><br />
                                                    <table cellspacing="0" cellpadding="3" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <ajax:AjaxPanel ID="AjaxPanel2" runat="server">
                                                                        <asp:ImageButton ID="ImageApprove" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Approve.gif"
                                                                            AlternateText="Approve" /></ajax:AjaxPanel></td>
                                                                <td>
                                                                    <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                                                                        <asp:ImageButton ID="ImageDecline" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Decline.gif"
                                                                            AlternateText="Decline"></asp:ImageButton></ajax:AjaxPanel></td>
                                                                <td style="width: 7px">
                                                                    <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                                                                        <asp:ImageButton ID="ImageRequestChange" runat="server" ImageUrl="~/Images/Button/RequestChange.gif"
                                                                            AlternateText="Request Change" /></ajax:AjaxPanel></td>
                                                                <td style="width: 7px">
                                                                    <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                                                                        <asp:ImageButton ID="ImageBackApproval" runat="server" ImageUrl="~/Images/Button/Back.gif">
                                                                        </asp:ImageButton></ajax:AjaxPanel></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="RowRequestor" runat="server" visible="false">
                                                <td>
                                                    <asp:CheckBox ID="chkPernyataanSubmit" runat="server" Text="Saya menyatakan bahwa berdasarkan pengetahuan saya yang sebenar-benarnya, informasi yang terdapat pada lembar CDD ini adalah benar dan telah diverifikasi sesuai dengan ketentuan CDD PT Bank CIMB Niaga yang berlaku, dan bahwa ketentuan CDD PT Bank CIMB Niaga telah dipenuhi."
                                                        Font-Bold="True"></asp:CheckBox><br />
                                                    <table cellspacing="0" cellpadding="3" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                                                        <asp:ImageButton ID="ImageSubmit" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Submit.gif"
                                                                            AlternateText="Submit" /></ajax:AjaxPanel></td>
                                                                <td>
                                                                    <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                                                        <asp:ImageButton ID="ImageBackRequestor" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Back.gif"
                                                                            AlternateText="Back"></asp:ImageButton></ajax:AjaxPanel></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="RowViewer" runat="server" visible="false">
                                                <td>
                                                    <table cellspacing="0" cellpadding="3" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                                                        <asp:ImageButton ID="ImageBackViewer" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Back.gif"
                                                                            AlternateText="Back"></asp:ImageButton></ajax:AjaxPanel></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </ajax:AjaxPanel>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
