﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LTKT_upload.aspx.vb" Inherits="LTKT_upload"
    MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            
                        </td>
                        <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="divcontentinside" bgcolor="#FFFFFF" style="width: 100%">
                                &nbsp;<ajax:AjaxPanel ID="a" runat="server" meta:resourcekey="aResource1"><asp:ValidationSummary
                                    ID="ValidationSummaryunhandle" runat="server" CssClass="validation" meta:resourcekey="ValidationSummaryunhandleResource1" />
                                    <asp:ValidationSummary ID="ValidationSummaryhandle" runat="server" CssClass="validationok"
                                        ForeColor="Black" ValidationGroup="handle" meta:resourcekey="ValidationSummaryhandleResource1" />
                                </ajax:AjaxPanel>
                                <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%" meta:resourcekey="AjaxPanel5Resource1">
                                    <asp:MultiView ID="MultiViewKPIOvertimeUpload" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="VwUpload" runat="server">
                                            <table width="100%">
                                                <tr>
                                                    <td height="20px" valign="middle" style="width: 852px">
                                                        <table>
                                                            <tr>
                                                                <td valign="bottom" align="left" bgcolor="#ffffff" width="15">
                                                                    <img height="15" src="images/dot_title.gif" width="15">
                                                                </td>
                                                                <td class="maintitle" valign="bottom" bgcolor="#ffffff">
                                                                    <asp:Label ID="lblMode" runat="server" Text="Mode">
                                                                    </asp:Label>
                                                                    &nbsp;:&nbsp;<asp:Label ID="LabelTitle" runat="server">CIF UPLOAD</asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                            bgcolor="#dddddd" border="2">
                                                            <tr id="ModifyRETBDI">
                                                                <td valign="top" width="100%" bgcolor="#ffffff" style="height: 132px" id="UploadExcel">
                                                                    <table cellspacing="4" cellpadding="0" width="100%" border="0">
                                                                        <tr>
                                                                            <td valign="middle" width="100%" nowrap="">
                                                                                <table style="width: 100%; height: 100%;">
                                                                                    <tr>
                                                                                        <td style="width: 82px; height: 18px">
                                                                                        </td>
                                                                                        <td style="width: 9%; height: 18px">
                                                                                            &nbsp;</td>
                                                                                        <td align="center" style="width: 2%; height: 18px">
                                                                                            &nbsp;</td>
                                                                                        <td style="width: 100%; height: 18px">
                                                                                            &nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 82px; height: 18px">
                                                                                        </td>
                                                                                        <td style="width: 9%; height: 18px" align="left">
                                                                                            <asp:Label ID="Label2" runat="server">File</asp:Label>
                                                                                        </td>
                                                                                        <td align="center" style="width: 2%; height: 18px">
                                                                                            :
                                                                                        </td>
                                                                                        <td style="width: 100%; height: 18px">
                                                                                            &nbsp;<asp:FileUpload ID="FileUploadKPI" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                                                ajaxcall="none" runat="server" CssClass="textbox" Width="400px" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 82px; height: 10px">
                                                                                        </td>
                                                                                        <td style="width: 9%; height: 10px">
                                                                                            <asp:Label ID="Label3" runat="server">
                                                                                            </asp:Label>
                                                                                        </td>
                                                                                        <td align="center" style="width: 2%; height: 10px">
                                                                                        </td>
                                                                                        <td style="width: 100%; height: 10px">
                                                                                            &nbsp;<asp:Label ID="Label36" runat="server" Text="">
                                                                                            </asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 82px; height: 21px">
                                                                                        </td>
                                                                                        <td style="width: 9%; height: 21px">
                                                                                        </td>
                                                                                        <td align="center" style="width: 2%; height: 21px">
                                                                                        </td>
                                                                                        <td style="width: 100%; height: 21px">
                                                                                            <asp:ImageButton ID="imgUpload" runat="server" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                                                ajaxcall="none" ImageUrl="~/Images/button/upload.gif" OnClick="imgUpload_Click" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 82px; height: 21px">
                                                                                            &nbsp;</td>
                                                                                        <td style="width: 9%; height: 21px">
                                                                                            &nbsp;</td>
                                                                                        <td align="center" style="width: 2%; height: 21px">
                                                                                            &nbsp;</td>
                                                                                        <td style="width: 100%; height: 21px">
                                                                                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/FolderTemplate/LTKT.zip">Download Template</asp:HyperLink>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 852px;">
                                                                    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None">
                                                                    </asp:CustomValidator>
                                                                </td>
                                                            </tr>
                                                        </table>
                                            </table>
                                        </asp:View>
                                        <asp:View ID="VwSuccessData" runat="server">
                                            <table align="center" bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="2"
                                                cellspacing="1" width="100%">
                                                <tr id="Tr1">
                                                    <td id="Td1" bgcolor="#ffffff" style="height: 80px" valign="top" width="100%">
                                                        <table border="0" cellpadding="0" cellspacing="4" width="100%">
                                                            <tr>
                                                                <td style="width: 82px; height: 18px">
                                                                </td>
                                                                <td style="width: 11%; height: 18px; font-weight: bold">
                                                                    Total Success Data
                                                                </td>
                                                                <td align="center" style="width: 2%; height: 18px">
                                                                    :
                                                                </td>
                                                                <td style="width: 100%; height: 18px">
                                                                    <asp:Label ID="lblValueSuccessData" runat="server">0</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 82px; height: 18px">
                                                                </td>
                                                                <td align="left" style="width: 11%; height: 18px; font-weight: bold">
                                                                    Total Failed Data
                                                                </td>
                                                                <td align="center" style="width: 2%; height: 18px">
                                                                    :
                                                                </td>
                                                                <td style="width: 100%; height: 18px">
                                                                    <asp:Label ID="lblValueFailedData" runat="server">0</asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 82px; height: 10px">
                                                                </td>
                                                                <td style="width: 11%; height: 10px; font-weight: bold">
                                                                    Total Uploaded data
                                                                </td>
                                                                <td align="center" style="width: 2%; height: 10px">
                                                                    :
                                                                </td>
                                                                <td style="width: 100%; height: 10px">
                                                                    <asp:Label ID="lblValueuploadeddata" runat="server">0</asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tablesukses" width="99%">
                                                <tr>
                                                    <td background="images/search-bar-background.gif" valign="middle" style="width: 99%;
                                                        height: 20px; border-right: gray thin solid; border-top: gray thin solid; border-left: gray thin solid;
                                                        border-bottom: gray thin solid;">
                                                        <table cellspacing="0" cellpadding="3" border="0" width="99%">
                                                            <tr>
                                                                <td valign="bottom" align="left" width="15">
                                                                    <img height="15" src="images/done.png" width="15">
                                                                </td>
                                                                <td class="maintitle" valign="bottom">
                                                                    <asp:Label ID="Label6" runat="server" Text="Success Data">
                                                                    </asp:Label>
                                                                    &nbsp;:&nbsp;<asp:Label ID="lbljumlahsukses" runat="server"></asp:Label>
                                                                </td>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100%;">
                                                        <table cellspacing="0" cellpadding="3" border="0" width="99%">
                                                            <tr>
                                                                <td bgcolor="#ffffff" style="width: 99%;">
                                                                    <asp:DataGrid ID="GridSuccessList" runat="server" CellPadding="4" AllowPaging="True"
                                                                        Width="100%" GridLines="None" AllowSorting="True" ForeColor="White" BorderColor="#003300"
                                                                        BorderStyle="Solid" AutoGenerateColumns="False" Font-Bold="False" Font-Italic="False"
                                                                        Font-Overline="False" Font-Strikeout="False" Font-Underline="False">
                                                                        <PagerStyle HorizontalAlign="Center" ForeColor="White" BackColor="#666666"></PagerStyle>
                                                                        <HeaderStyle Font-Bold="True" ForeColor="White" Wrap="False" BackColor="#1C5E55" />
                                                                        <Columns>
                                                                            <%--  <asp:TemplateColumn HeaderText="No" HeaderStyle-ForeColor="White">
																				<ItemTemplate>
																					<asp:Label ID="lblNo" runat="server"></asp:Label>
																				</ItemTemplate>
																				<HeaderStyle ForeColor="White" />
																			</asp:TemplateColumn>--%>
                                                                            <asp:BoundColumn DataField="CIFno" HeaderText="CIF">
                                                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                    Font-Underline="False" ForeColor="White" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="NamaPJKPelapor" HeaderText="Name">
                                                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                    Font-Underline="False" ForeColor="White" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="INDV_TanggalLahir" HeaderText="Date Of Birth" DataFormatString="{0:dd-MM-yyyy}">
                                                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                    Font-Underline="False" ForeColor="White" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="TipeTerlapor" HeaderText="ID Type">
                                                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                    Font-Underline="False" ForeColor="White" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="INDV_NomorId" HeaderText="ID Number">
                                                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                    Font-Underline="False" ForeColor="White" />
                                                                            </asp:BoundColumn>
                                                                        </Columns>
                                                                        <EditItemStyle BackColor="#7C6F57" />
                                                                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                                                        <SelectedItemStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                                                                        <AlternatingItemStyle BackColor="White" />
                                                                        <ItemStyle BackColor="#E3EAEB" />
                                                                    </asp:DataGrid>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tableanomaly" width="99%">
                                                <tr>
                                                    <td background="images/search-bar-background.gif" valign="middle" style="width: 99%;
                                                        height: 20px; border-right: gray thin solid; border-top: gray thin solid; border-left: gray thin solid;
                                                        border-bottom: gray thin solid;">
                                                        <table cellspacing="0" cellpadding="3" border="0" width="99%">
                                                            <tr>
                                                                <td valign="bottom" align="left" width="15">
                                                                    <img height="15" src="images/warning.png" width="15">
                                                                </td>
                                                                <td class="maintitle" valign="bottom">
                                                                    <asp:Label ID="Label4" runat="server" Text="Anomaly Data"></asp:Label>
                                                                    &nbsp;:&nbsp;<asp:Label ID="lbljumlahanomalydata" runat="server"></asp:Label>
                                                                </td>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 100%;">
                                                        <table cellspacing="0" cellpadding="3" border="0" width="99%">
                                                            <tr>
                                                                <td bgcolor="#ffffff" style="width: 99%;">
                                                                    <asp:DataGrid ID="GridAnomalyList" runat="server" CellPadding="4" AllowPaging="True"
                                                                        Width="100%" GridLines="None" AllowSorting="True" ForeColor="#333333" AutoGenerateColumns="False"
                                                                        BorderColor="Maroon" BorderStyle="Solid">
                                                                        <PagerStyle HorizontalAlign="Center" ForeColor="#333333" BackColor="#FFCC66"></PagerStyle>
                                                                        <HeaderStyle Font-Bold="True" ForeColor="White" Wrap="False" BackColor="#990000" />
                                                                        <Columns>
                                                                            <%--  <asp:BoundColumn DataField="" HeaderText="No" ItemStyle-Width="30">
																				<HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
																					Font-Underline="False" ForeColor="White" />
																				<ItemStyle Width="30px" />
																			</asp:BoundColumn>--%>
                                                                            <asp:BoundColumn DataField="Description" HeaderText="Description">
                                                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                    Font-Underline="False" ForeColor="White" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="errorline" HeaderText="Error Line Number">
                                                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                    Font-Underline="False" ForeColor="White" />
                                                                                <ItemStyle Width="20%" />
                                                                            </asp:BoundColumn>
                                                                        </Columns>
                                                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                                        <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                                        <AlternatingItemStyle BackColor="White" />
                                                                        <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                                    </asp:DataGrid>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="LinkButtonExportExcel" runat="server" ajaxcall="none" meta:resourcekey="LinkButtonExportExcelResource1"
                                                                        OnClientClick="aspnetForm.encoding = 'multipart/form-data';">Export 
				to Excel</asp:LinkButton>
                                                            </tr>
                                                    </td>
                                            </table>
                            </td>
                        </tr>
                    </table>
                    </asp:View>
                    <asp:View ID="VwConfirmation" runat="server">
                        <table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                            width="100%" bgcolor="#dddddd" border="0">
                            <tr bgcolor="#ffffff">
                                <td colspan="2" align="center" style="height: 17px">
                                    <asp:Label ID="LblConfirmation" runat="server" meta:resourcekey="LblConfirmationResource1"></asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td align="center" colspan="2">
                                    <asp:ImageButton ID="ImgBtnAdd" runat="server" ImageUrl="~/images/button/Ok.gif"
                                        CausesValidation="False" meta:resourcekey="ImgBtnAddResource1" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView> </ajax:AjaxPanel>
            </td>
        </tr>
    </table>
    </div> </td> </tr>
    <tr>
        <td>
        </td>
        <td>
            <ajax:AjaxPanel ID="AjaxPanel1" runat="server" meta:resourcekey="AjaxPanel1Resource1">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" background="Images/button-bground.gif" valign="middle">
                            <img height="1" src="Images/blank.gif" width="5" />
                        </td>
                        <td align="left" background="Images/button-bground.gif" valign="middle">
                            <img height="15" src="images/arrow.gif" width="15" />&nbsp;
                        </td>
                        <td background="Images/button-bground.gif">
                            &nbsp;
                        </td>
                        <td background="Images/button-bground.gif" style="width: 5px" align="center" valign="middle">
                            <asp:ImageButton ID="Imgbtnsave" runat="server" ImageUrl="~/images/button/save.gif"
                                CausesValidation="False" meta:resourcekey="ImgBackAddResource1" Visible="False" />
                        </td>
                        <td background="Images/button-bground.gif">
                            &nbsp;<asp:ImageButton ID="Imgbtncancel" runat="server" ImageUrl="~/images/button/cancel.gif"
                                CausesValidation="False" meta:resourcekey="ImgBackAddResource1" Visible="False" />
                        </td>
                        <td background="Images/button-bground.gif" width="99%">
                            <img height="1" src="Images/blank.gif" width="1" />
                        </td>
                        <td style="width: 25px">
                            
                        </td>
                    </tr>
                </table>
                <asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" ValidationGroup="handle"
                    meta:resourcekey="CvalHandleErrResource1"></asp:CustomValidator><asp:CustomValidator
                        ID="CustomValidator1" runat="server" Display="None" meta:resourcekey="CvalPageErrResource1"></asp:CustomValidator></ajax:AjaxPanel>
        </td>
    </tr>
    </table>
</asp:Content>
