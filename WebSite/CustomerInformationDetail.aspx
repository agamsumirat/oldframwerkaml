<%--<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeFile="CustomerInformationDetail.aspx.vb" Inherits="CustomerInformationDetail" title="Customer Information" %>--%>

<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/masterpage.master"
    CodeFile="CustomerInformationDetail.aspx.vb" Inherits="CustomerInformationDetail"
    Title="Customer Information" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
        <table id="TABLE1" bordercolor="#fffff" cellspacing="0" cellpadding="0" width="99%"
            bgcolor="#dddddd" border="2">
            <tr>
                <td style="width: 5px; height: 129px">
                </td>
                <td style="width: 100%; height: 129px; background-color: #ffffff" valign="top">
                    <table width="100%">
                        <tr>
                            <td style="width: 1%">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/dot_title.gif"></asp:Image>
                            </td>
                            <td style="width: 100px">
                                <strong style="font-size: medium; width: 50%">
                                    <asp:Label ID="Label1" runat="server" Width="400px" Text="CUSTOMER INFORMATION DETAIL"></asp:Label></strong>
                            </td>
                        </tr>
                    </table>
                    <table bordercolor="#ffffff" cellspacing="1" cellpadding="1" width="100%" bgcolor="#dddddd"
                        border="2">
                        <tr style="background-color: #ffffff">
                            <td width="8%">
                                CIF No
                            </td>
                            <td style="width: 1%">
                                :
                            </td>
                            <td style="width: 15%">
                                &nbsp;<asp:Label ID="LblCIFNo" runat="server"></asp:Label>
                            </td>
                            <td style="width: 3%">
                            </td>
                            <td width="10%">
                                Customer Type
                            </td>
                            <td width="1%">
                                :
                            </td>
                            <td width="25%">
                                &nbsp;<asp:Label ID="LblCustomerType" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td>
                                Name
                            </td>
                            <td style="width: 100px; height: 17px">
                                :
                            </td>
                            <td style="width: 100px; height: 17px">
                                &nbsp;<asp:Label ID="LblName" runat="server" Width="320px"></asp:Label>
                            </td>
                            <td style="width: 100px; height: 17px">
                            </td>
                            <td>
                                Customer Sub Type
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                &nbsp;<asp:Label ID="LblCustomerSubType" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td>
                                Date Of Birth
                            </td>
                            <td style="width: 100px">
                                :
                            </td>
                            <td style="width: 100px">
                                &nbsp;<asp:Label ID="LblDOB" runat="server" Width="224px"></asp:Label>
                            </td>
                            <td style="width: 100px">
                            </td>
                            <td>
                                Business Type
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                &nbsp;<asp:Label ID="LblBusinessType" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td>
                                Opening Date
                            </td>
                            <td style="width: 100px; height: 17px">
                                :
                            </td>
                            <td style="width: 100px; height: 17px">
                                &nbsp;<asp:Label ID="LblOpeningDate" runat="server" Width="250px"></asp:Label>
                            </td>
                            <td style="width: 100px; height: 17px">
                            </td>
                            <td>
                                Industry Code
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                &nbsp;<asp:Label ID="LblIndustryCode" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td>
                                Evaluating Branch
                            </td>
                            <td style="width: 100px; height: 17px">
                                :
                            </td>
                            <td style="width: 100px; height: 17px">
                                &nbsp;<asp:Label ID="LblAccountOwner" runat="server" Width="250px"></asp:Label>
                            </td>
                            <td style="width: 100px; height: 17px">
                            </td>
                            <td>
                                AML Risk
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <asp:Label ID="LblAMLRisk" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td valign="top">
                                Income
                            </td>
                            <td>
                                :
                            </td>
                            <td>
                                <asp:GridView ID="GrdIncome" runat="server" AutoGenerateColumns="False" DataKeyNames="PK_CIFIncome_ID"
                                    EnableModelValidation="True" Width="100%" BackColor="White" BorderColor="#DEDFDE"
                                    BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical">
                                    <Columns>
                                        <asp:BoundField DataField="IncomeType" HeaderText="Income Type" />
                                        <asp:BoundField DataField="Income" DataFormatString="{0:d}" HeaderText="Income" />
                                    </Columns>
                                    <RowStyle BackColor="#F7F7DE" />
                                    <FooterStyle BackColor="#CCCC99" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </td>
                            <td style="width: 100px; height: 17px" valign="top">
                                &nbsp;
                            </td>
                            <td valign="top">
                                Last CDD Updated Date
                            </td>
                            <td valign="top">
                                :
                            </td>
                            <td valign="top">
                                <asp:Label Text="" runat="server" ID="LbllastModifiedDate" />
                            </td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td valign="top">Source Of Fund</td>
                            <td>:</td>
                            <td>
                                <asp:Label ID="LblSourceOfFund" runat="server" Width="250px"></asp:Label>
                            </td>
                            <td style="width: 100px; height: 17px" valign="top">&nbsp;</td>
                            <td valign="top">CIF Creation Bank </td>
                            <td valign="top">:</td>
                            <td valign="top">
                                <asp:Label ID="lblCIFCreationBank" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td>
                                CIF Status
                            </td>
                            <td style="width: 100px; height: 17px">
                                :
                            </td>
                            <td style="width: 100px; height: 17px">
                                <asp:Label ID="lblCifStatus" runat="server" Width="250px"></asp:Label>
                            </td>
                            <td style="width: 100px; height: 17px">
                            </td>
                            <td>
                                CIF Creation Branch</td>
                            <td>
                                :
                            </td>
                            <td>
                                <asp:Label ID="lblCIFCreationBranch" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td>
                                RM Code
                            </td>
                            <td style="width: 100px; height: 17px">
                                :
                            </td>
                            <td style="width: 100px; height: 17px">
                                <asp:Label ID="lblRM" runat="server" Width="250px"></asp:Label>
                            </td>
                            <td style="width: 100px; height: 17px">
                            </td>
                            <td>
                                &nbsp;CIF Controlling Branch</td>
                            <td>
                                :
                            </td>
                            <td>
                                <asp:Label ID="lblCIFControllingBranch" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td>
                                SBU
                            </td>
                            <td style="width: 100px; height: 17px">
                                :
                            </td>
                            <td style="width: 100px; height: 17px">
                                <asp:Label ID="lblSbu" runat="server" Width="250px"></asp:Label>
                            </td>
                            <td style="width: 100px; height: 17px">
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                :
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td>
                                Sub SBU
                            </td>
                            <td style="width: 100px; height: 17px">
                                :
                            </td>
                            <td style="width: 100px; height: 17px">
                                <asp:Label ID="lblSubSBU" runat="server" Width="350px"></asp:Label>
                            </td>
                            <td style="width: 100px; height: 17px">
                                &nbsp;
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 5px">
                </td>
                <td style="background-color: #ffffff" valign="top">
                    <table cellspacing="0" cellpadding="0" width="99%">
                        <tr>
                            <td>
                                <asp:Menu ID="Menu1" runat="server" Orientation="Horizontal" StaticEnableDefaultPopOutImage="False"
                                    CssClass="tabs" OnMenuItemClick="Menu1_MenuItemClick">
                                    <Items>
                                        <%--<asp:MenuItem Text="Risk Information" Value="7" Enabled="false"></asp:MenuItem>--%>
                                        <asp:MenuItem Selected="true" Text="General" Value="0"></asp:MenuItem>
                                        <asp:MenuItem Text="Address" Value="1"></asp:MenuItem>
                                        <asp:MenuItem Text="ID Number" Value="2"></asp:MenuItem>
                                        <asp:MenuItem Text="Phone" Value="3"></asp:MenuItem>
                                        <asp:MenuItem Text="Electronic Address" Value="4"></asp:MenuItem>
                                        <asp:MenuItem Text="Employement History" Value="5"></asp:MenuItem>
                                        <asp:MenuItem Text="Account Information" Value="6"></asp:MenuItem>
                                        <asp:MenuItem Text="Watched/Negative List Information" Value="8"></asp:MenuItem>
                                        <asp:MenuItem Text="Link Analysis" Value="9"></asp:MenuItem>
                                        <asp:MenuItem Text="EDD" Value="10"></asp:MenuItem>
                                        <asp:MenuItem Text="Stakeholder/BO" Value="11"></asp:MenuItem>
                                        <asp:MenuItem Text="Risk Rating History" Value="12"></asp:MenuItem>
                                        <asp:MenuItem Text="General Credit Card" Value="13"></asp:MenuItem>
                                        <asp:MenuItem Text="Address Credit Card " Value="14"></asp:MenuItem>
                                        <asp:MenuItem Text="ID Number CreditCard" Value="15"></asp:MenuItem>
                                        <asp:MenuItem Text="Phone Credit Card" Value="16"></asp:MenuItem>
                                        <asp:MenuItem Text="Employment History Credit Card" Value="17"></asp:MenuItem>
                                    </Items>
                                    <StaticSelectedStyle CssClass="selectedTab" />
                                    <StaticMenuItemStyle CssClass="tab" />
                                </asp:Menu>
                                <table class="tabContents">
                                    <tr>
                                        <td>
                                            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                                                <asp:View ID="TabGeneral" runat="server">
                                                    <table width="100%" class="TabArea">
                                                        <tr id="Tr13" runat="server">
                                                            <td id="Td31" runat="server" align="left" class="maintitle" style="height: 18px">
                                                                GENERAL
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr14">
                                                            <td id="TdGeneralNotAvailable" align="left" style="color: red;" runat="server">
                                                                Data Not Available
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr15">
                                                            <td id="TdDataGeneralAvailable" style="width: 50%" runat="server">
                                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="1" bgcolor="#dddddd" border="2"
                                                                    width="50%">
                                                                    <tr style="background-color: #ffffff">
                                                                        <td style="width: 10%; height: 17px;">
                                                                            Birth Place
                                                                        </td>
                                                                        <td width="1%" style="height: 17px">
                                                                            :
                                                                        </td>
                                                                        <td style="width: 20%; height: 17px;">
                                                                            <asp:Label ID="LblBirthPlace" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td style="height: 17px">
                                                                            Maiden Mother Name
                                                                        </td>
                                                                        <td width="1%" style="height: 17px">
                                                                            :
                                                                        </td>
                                                                        <td style="width: 100px; height: 17px;">
                                                                            <asp:Label ID="LblMaidenMotherName" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td style="height: 17px">
                                                                            Gender
                                                                        </td>
                                                                        <td width="1%" style="height: 17px">
                                                                            :
                                                                        </td>
                                                                        <td style="width: 100px; height: 17px;">
                                                                            <asp:Label ID="LblGender" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td>
                                                                            Marital Status
                                                                        </td>
                                                                        <td width="1%">
                                                                            :
                                                                        </td>
                                                                        <td style="width: 100px">
                                                                            <asp:Label ID="LblMartialStatus" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td>
                                                                            Country
                                                                        </td>
                                                                        <td width="1%">
                                                                            :
                                                                        </td>
                                                                        <td style="width: 100px">
                                                                            <asp:Label ID="LblCountry" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td>
                                                                            Citizenship
                                                                        </td>
                                                                        <td width="1%">
                                                                            :
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="LblCitizen" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <%--                                                  <tr style="BACKGROUND-COLOR: #ffffff">
                                                    <td>
                                                        Source of Fund</td>
                                                    <td width="1%">
                                                        :</td>
                                                    <td>
                                                        <asp:Label ID="LblSourceOfFundDataNotAvailable" runat="server" ForeColor="Red" Visible="False">Source Of Fund Data is not available</asp:Label>
                                                        <asp:DataGrid ID="DgSourceOfFund" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="500px">
                                                            <FooterStyle BackColor="#CCCC99" />
                                                            <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
                                                            <AlternatingItemStyle BackColor="White" />
                                                            <ItemStyle BackColor="#F7F7DE" />
                                                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                            <Columns>
                                                                <asp:BoundColumn DataField="SourceOfFundDescription" HeaderText="Source Of Fund">
                                                                    <HeaderStyle Width="100px" />
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="CurrencyType" HeaderText="Currency Type">
                                                                    <HeaderStyle Width="100px" />
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="Amount" DataFormatString="{0:###,###.00}" HeaderText="Amount">
                                                                </asp:BoundColumn>
                                                            </Columns>
                                                        </asp:DataGrid></td>
                                                </tr>
                                                                    --%>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View ID="TabAddress" runat="server">
                                                    <table width="100%" style="clip: rect(auto auto auto auto)">
                                                        <tr id="Tr1" runat="server" valign="top">
                                                            <td id="Td1" runat="server" align="left" class="maintitle">
                                                                Address
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="AddressNotFound">
                                                            <td id="Td2" align="left" style="color: #ff0000" runat="server" valign="top">
                                                                Data Not Available
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="AddresFound">
                                                            <td id="Td3" runat="server" valign="top">
                                                                <asp:DataGrid ID="GridAddress" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                                                    CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="500px">
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
                                                                    <ItemStyle BackColor="#F7F7DE" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <Columns>
                                                                        <%--<asp:BoundColumn DataField="AddressType" HeaderText="Address Type" SortExpression="AddressType desc">
                                    </asp:BoundColumn>--%>
                                                                        <asp:BoundColumn DataField="Address" HeaderText="Address" SortExpression="Adress1 desc"><HeaderStyle Width="400px" /></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="PostalCode" HeaderText="Postal Code" SortExpression="PostalCode desc"><HeaderStyle Width="100px" /></asp:BoundColumn>
                                                                    </Columns>
                                                                    <AlternatingItemStyle BackColor="White" />
                                                                </asp:DataGrid>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View ID="TabIDNumber" runat="server">
                                                    <table width="100%" class="TabArea">
                                                        <tr id="Tr2" runat="server">
                                                            <td id="Td4" runat="server" align="left" class="maintitle">
                                                                ID Number
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="TrIdNumberFound">
                                                            <td id="Td5" align="left" style="color: #ff0000; height: 15px;" runat="server">
                                                                Data Not Available
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr3">
                                                            <td id="Td6" runat="server">
                                                                <asp:DataGrid ID="GridIdNumber" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                                                    CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="300px">
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
                                                                    <ItemStyle BackColor="#F7F7DE" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="IDNumber" HeaderText="ID Number" SortExpression="IDNumber desc"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="CFIDSC" HeaderText="Category" SortExpression="CFIDSC desc"></asp:BoundColumn>
                                                                    </Columns>
                                                                    <AlternatingItemStyle BackColor="White" />
                                                                </asp:DataGrid>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View ID="TabPhone" runat="server">
                                                    <table width="100%" class="TabArea">
                                                        <tr id="Tr16" runat="server">
                                                            <td id="Td32" runat="server" align="left" class="maintitle">
                                                                PHONE Number
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr17">
                                                            <td id="TdPhoneNotAvailable" align="left" style="color: #ff0000; height: 15px;" runat="server">
                                                                Data Not Available
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr18">
                                                            <td id="TdPhodeAvailable" runat="server">
                                                                <asp:DataGrid ID="GridPhone" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                                                    CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="300px">
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
                                                                    <AlternatingItemStyle BackColor="White" />
                                                                    <ItemStyle BackColor="#F7F7DE" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="Type" HeaderText="Type" SortExpression="Type desc"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Description" HeaderText="Description" SortExpression="Description desc"></asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View ID="TabElectronicAddress" runat="server">
                                                    <table width="100%" class="TabArea">
                                                        <tr id="Tr19" runat="server">
                                                            <td id="Td33" runat="server" align="left" class="maintitle">
                                                                Electronic Address
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr20">
                                                            <td id="TdElecAddNotAvailable" align="left" style="color: #ff0000; height: 15px;"
                                                                runat="server">
                                                                Data Not Available
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr21">
                                                            <td id="TdElecAddAvailable" runat="server">
                                                                <asp:DataGrid ID="GridEmail" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                                                    CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="300px">
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
                                                                    <AlternatingItemStyle BackColor="White" />
                                                                    <ItemStyle BackColor="#F7F7DE" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="Type" HeaderText="Type" SortExpression="Type desc"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Description" HeaderText="Description" SortExpression="Description desc"></asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View ID="TabEmployeeHistory" runat="server">
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="maintitle">
                                                                <span>Employement History</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:GridView runat="server" ID="GrdEmployeeHistory" BackColor="White" BorderColor="#DEDFDE"
                                                                    BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True"
                                                                    ForeColor="Black" AutoGenerateColumns="False" DataKeyNames="PK_CIFEmployeeHistory_ID">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Employer" HeaderText="Employer" />
                                                                        <asp:BoundField DataField="Title" HeaderText="Title" />
                                                                        <asp:BoundField DataField="StartDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="StartDate"
                                                                            HtmlEncode="False" />
                                                                        <asp:BoundField DataField="EndDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="EndDate"
                                                                            HtmlEncode="False" />
                                                                        <asp:BoundField DataField="WorkingYear" HeaderText="WorkingYear" />
                                                                    </Columns>
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                    <RowStyle BackColor="#F7F7DE" />
                                                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View ID="TabAccountInformation" runat="server">
                                                    <table width="100%" class="TabArea">
                                                        <tr id="Tr25" runat="server">
                                                            <td id="Td35" runat="server" align="left" class="maintitle">
                                                                Account information
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr34">
                                                            <td runat="server" align="left" id="Td46">
                                                                <strong>SUMMARY<br />
                                                                    <table border="1">
                                                                        <tr>
                                                                            <td style="width: 100px">
                                                                                ASSET :
                                                                            </td>
                                                                            <td style="width: 100px">
                                                                                LOAN
                                                                            </td>
                                                                            <td align="right" style="width: 400px">
                                                                                &nbsp;<asp:Label ID="LblTotalLoan1" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 100px">
                                                                            </td>
                                                                            <td style="width: 100px">
                                                                                CREDIT CARD
                                                                            </td>
                                                                            <td align="right" style="width: 400px">
                                                                                <asp:Label ID="LblTotalCreditCard1" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" colspan="2">
                                                                                TOTAL ASSET
                                                                            </td>
                                                                            <td style="width: 400px" align="right">
                                                                                <asp:Label ID="LblTotalAsset1" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 100px; height: 15px">
                                                                                LIABILITIES :
                                                                            </td>
                                                                            <td style="width: 100px; height: 15px">
                                                                                SAVING
                                                                            </td>
                                                                            <td style="width: 400px;" align="right">
                                                                                <asp:Label ID="LblLiabilitis" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="right" colspan="2">
                                                                                TOTAL LIABILITIES
                                                                            </td>
                                                                            <td style="width: 400px" align="right">
                                                                                <asp:Label ID="LblTotalLiabilities1" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr runat="server" id="RowTotalJIVE">
                                                                            <td>
                                                                                WEALTH MANAGEMENT :
                                                                            </td>
                                                                            <td>
                                                                                WEALTH MANAGEMENT
                                                                            </td>
                                                                            <td align="right" style="width: 400px; margin-left: 40px;">
                                                                                <strong>
                                                                                    <asp:Label ID="LblTotalJive1" runat="server" />
                                                                                </strong>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="RowTotalHipor" runat="server">
                                                                            <td>
                                                                                CUSTODY :
                                                                            </td>
                                                                            <td>
                                                                                CUSTODY
                                                                            </td>
                                                                            <td align="right" style="width: 400px; margin-left: 40px;">
                                                                                <strong>
                                                                                    <asp:Label ID="LblTotalHipor1" runat="server" />
                                                                                </strong>
                                                                            </td>
                                                                        </tr>
                                                                        <tr runat="server">
                                                                            <td>
                                                                                URS CUSTODY :&nbsp;
                                                                            </td>
                                                                            <td>
                                                                                URS CUSTODY
                                                                            </td>
                                                                            <td align="right" style="margin-left: 40px; width: 400px">
                                                                                <asp:Label ID="LblTotalURSCustody1" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr runat="server">
                                                                            <td>
                                                                                <strong>BANCASSURANCE:</strong>
                                                                            </td>
                                                                            <td>
                                                                                <strong>BANCASSURANCE</strong>
                                                                            </td>
                                                                            <td align="right" style="margin-left: 40px; width: 400px">
                                                                                <strong>
                                                                                    <asp:Label ID="LblTotalBancassurance" runat="server" />
                                                                                </strong>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </strong>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server">
                                                            <td runat="server">
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr53">
                                                            <td id="Td66" runat="server" style="font-size: large">
                                                                Bancassurance
                                                            </td>
                                                        </tr>
                                                        <tr runat="server">
                                                            <td id="Td67" runat="server">
                                                                Filter :
                                                                <asp:DropDownList ID="CbFilBa" runat="server" CssClass="combobox">
                                                                </asp:DropDownList>
                                                                <asp:TextBox ID="txtBa" runat="server" CssClass="combobox"></asp:TextBox>
                                                                <input id="Button1" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                    border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                    border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                    height: 17px" title="Click to show calendar" type="button" />
                                                                <asp:ImageButton ID="ImgBtnBA" runat="server" ImageUrl="~/Images/button/search.gif" />
                                                            </td>
                                                            <td runat="server">
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr54">
                                                            <td id="Td68" runat="server">
                                                                <strong><span style="font-size: 12px"></span></strong>
                                                                <asp:GridView runat="server" ID="GdViewBA" BackColor="White" BorderColor="#DEDFDE"
                                                                    BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True"
                                                                    ForeColor="Black" AutoGenerateColumns="False" EmptyDataText="There is no Asset Account">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Cif_No" HeaderText="CIFNo" />
                                                                        <asp:BoundField DataField="Account_Owner" HeaderText="Accont Owner" />
                                                                        <asp:BoundField DataField="Account_No" HeaderText="Account_No" />
                                                                        <asp:BoundField DataField="Name" HeaderText="Name" />
                                                                        <asp:BoundField DataField="Cif_Relation" HeaderText="Cif_Relation" />
                                                                        <asp:BoundField DataField="Account_Type" HeaderText="Account_Type" />
                                                                        <asp:BoundField DataField="Opening_Date" HeaderText="Opening_Date" />
                                                                        <asp:BoundField DataField="Status" HeaderText="Status" />
                                                                        <asp:BoundField DataField="Joint_Account" HeaderText="Joint_Account" />
                                                                        <asp:BoundField DataField="Account_OwnerShip_Type" HeaderText="Account_OwnerShip_Type" />
                                                                        <asp:BoundField DataField="Product_Description" HeaderText="Product_Description" />
                                                                        <asp:BoundField DataField="Currency" HeaderText="Currency" />
                                                                        <asp:BoundField DataField="Saldo" DataFormatString="{0:#,##0.00}" HeaderText="Saldo"
                                                                            HtmlEncode="False"><ItemStyle HorizontalAlign="Right" /></asp:BoundField>
                                                                    </Columns>
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                    <RowStyle BackColor="#F7F7DE" />
                                                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <caption>
                                                            &gt;
                                                            <tr runat="server">
                                                                <td runat="server">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr runat="server">
                                                                <td runat="server">
                                                                    <strong>SAFE DEPOSIT BOX</strong>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr runat="server">
                                                                <td runat="server">
                                                                    Filter :
                                                                    <asp:DropDownList ID="CboFilterSDB" runat="server" CssClass="combobox">
                                                                    </asp:DropDownList>
                                                                    <asp:TextBox ID="txtSDB" runat="server" CssClass="combobox"></asp:TextBox>
                                                                    <input id="popupsdb" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                        border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                        border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                        height: 17px" title="Click to show calendar" type="button" />
                                                                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/button/search.gif" />
                                                                </td>
                                                            </tr>
                                                            <tr runat="server">
                                                                <td runat="server">
                                                                    <asp:GridView ID="GrdSafedeposit" runat="server" AutoGenerateColumns="False" BackColor="White"
                                                                        BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black"
                                                                        GridLines="Vertical">
                                                                        <RowStyle BackColor="#F7F7DE" />
                                                                        <Columns>
                                                                            <asp:BoundField DataField="cifno" HeaderText="CIFNo" />
                                                                            <asp:BoundField DataField="AccountOwnerName" HeaderText="AccountOwner" />
                                                                            <asp:BoundField DataField="acctno" HeaderText="Account No" />
                                                                            <asp:BoundField DataField="sname" HeaderText="Name" />
                                                                            <asp:BoundField DataField="boxno" HeaderText="Box No" />
                                                                            <asp:BoundField DataField="tglopen" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Open Date"
                                                                                HtmlEncode="False" />
                                                                            <asp:BoundField DataField="tglexpired" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Expired Date"
                                                                                HtmlEncode="False" />
                                                                            <asp:BoundField DataField="tglclosed" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Closed Date"
                                                                                HtmlEncode="False" />
                                                                        </Columns>
                                                                        <FooterStyle BackColor="#CCCC99" />
                                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                        <AlternatingRowStyle BackColor="White" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server">
                                                                <td runat="server">
                                                                </td>
                                                            </tr>
                                                            <tr id="Tr35" runat="server">
                                                                <td id="Td47" runat="server">
                                                                    <strong><span style="font-size: 9pt">ASSET DETAIL</span></strong>
                                                                    <hr />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="Td38" runat="server" style="font-size: 12px">
                                                                    <strong>&nbsp;Loan</strong>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="Td39" runat="server" style="font-size: 12px">
                                                                    Filter :
                                                                    <asp:DropDownList ID="CboFilterLoan" runat="server" CssClass="combobox">
                                                                    </asp:DropDownList>
                                                                    &nbsp;<asp:TextBox ID="txtfilterloan" runat="server" CssClass="combobox"></asp:TextBox>
                                                                    <input id="popUploan" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                        border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                        border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                        height: 17px" title="Click to show calendar" type="button" />
                                                                    <asp:ImageButton ID="ImgSearchLoan" runat="server" ImageUrl="~/Images/button/search.gif" />
                                                                </td>
                                                            </tr>
                                                            <tr id="Tr27" runat="server">
                                                                <td id="Td40" runat="server">
                                                                    <asp:GridView ID="GrdAccountLoan" runat="server" AutoGenerateColumns="False" BackColor="White"
                                                                        BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" EmptyDataText="There is no Loan Account"
                                                                        EnableModelValidation="True" ForeColor="Black">
                                                                        <AlternatingRowStyle BackColor="White" />
                                                                        <Columns>
                                                                            <asp:BoundField DataField="CIFNo" HeaderText="CIFNo" />
                                                                            <asp:BoundField DataField="AccountOwnerName" HeaderText="Accont Owner" />
                                                                            <asp:TemplateField HeaderText="Account No">
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("AccountNo") %>'></asp:TextBox>
                                                                                </EditItemTemplate>
                                                                                <ItemTemplate>
                                                                                    <%#GenerateAccountLink(Eval("AccountNo")) %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="AccountName" HeaderText="Name" />
                                                                            <asp:BoundField DataField="CIFRelation" HeaderText="CIFRelation"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                            <asp:BoundField DataField="AccountTypeDescription" HeaderText="Account Type" />
                                                                            <asp:BoundField DataField="OpeningDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Opening Date"
                                                                                HtmlEncode="False" />
                                                                            <asp:BoundField DataField="Closing_Date" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Closing Date"
                                                                                HtmlEncode="False" />
                                                                            <asp:BoundField DataField="Maturity_Date" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Maturity Date"
                                                                                HtmlEncode="False" />
                                                                            <asp:BoundField DataField="AccountStatusDescription" HeaderText="Status"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                            <asp:BoundField DataField="IsJointAccount" HeaderText="Joint Account"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                            <asp:BoundField DataField="JointAccountOwnership" HeaderText="Account Ownership Type"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                            <asp:BoundField DataField="CurrencyType" HeaderText="Currency" />
                                                                            <asp:BoundField DataField="Plafon" DataFormatString="{0:#,##0.00}" HeaderText="Plafon"
                                                                                HtmlEncode="False"><ItemStyle HorizontalAlign="Right" /></asp:BoundField>
                                                                            <asp:BoundField DataField="Saldo" DataFormatString="{0:#,##0.00}" HeaderText="OutStanding"
                                                                                HtmlEncode="False"><ItemStyle HorizontalAlign="Right" /></asp:BoundField>
                                                                        </Columns>
                                                                        <FooterStyle BackColor="#CCCC99" />
                                                                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                        <RowStyle BackColor="#F7F7DE" />
                                                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                            <tr id="Tr29" runat="server">
                                                                <td id="Td41" runat="server" align="right">
                                                                    Total Loan :
                                                                    <asp:Label ID="LblTotalLoan" runat="server" Text="" />
                                                                </td>
                                                            </tr>
                                                            <tr id="Tr30" runat="server">
                                                                <td id="Td42" runat="server">
                                                                    <strong><span style="font-size: 12px">&nbsp;Credit Card</span></strong>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr id="Tr31" runat="server">
                                                                <td id="Td43" runat="server">
                                                                    Filter :
                                                                    <asp:DropDownList ID="CboFilterCreditCard" runat="server" CssClass="combobox">
                                                                    </asp:DropDownList>
                                                                    &nbsp;<asp:TextBox ID="txtfiltercreditcard" runat="server" CssClass="combobox"></asp:TextBox>
                                                                    <input id="popupcreditcard" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                        border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                        border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                        height: 17px" title="Click to show calendar" type="button" />
                                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/button/search.gif" />
                                                                </td>
                                                            </tr>
                                                            <tr id="Tr32" runat="server">
                                                                <td id="Td44" runat="server">
                                                                    <asp:GridView ID="GrdAccountCreditCard" runat="server" AutoGenerateColumns="False"
                                                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                                                        CellPadding="4" EmptyDataText="There is no Credit Card Account" EnableModelValidation="True"
                                                                        ForeColor="Black">
                                                                        <AlternatingRowStyle BackColor="White" />
                                                                        <Columns>
                                                                            <asp:BoundField DataField="CIFNO" HeaderText="CIFNo" />
                                                                            <asp:BoundField DataField="AccountOwnerName" HeaderText="Accont Owner" />
                                                                            <asp:TemplateField HeaderText="Account No">
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("AccountNo") %>'></asp:TextBox>
                                                                                </EditItemTemplate>
                                                                                <ItemTemplate>
                                                                                    <%#GenerateAccountLink(Eval("AccountNo")) %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="CardNo" HeaderText="Card No" />
                                                                            <asp:BoundField DataField="AccountName" HeaderText="Name" />
                                                                            <asp:BoundField DataField="CIFRelation" HeaderText="CIFRelation"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                            <asp:BoundField DataField="AccountTypeDescription" HeaderText="Account Type" />
                                                                            <asp:BoundField DataField="OpeningDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Opening Date"
                                                                                HtmlEncode="False" />
                                                                            <asp:BoundField DataField="AccountStatusDescription" HeaderText="Status"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                            <asp:BoundField DataField="LimitCreditCard" DataFormatString="{0:#,##0.00}" HeaderText="LimitCreditCard"
                                                                                HtmlEncode="False" />
                                                                            <asp:BoundField DataField="JointAccountOwnership" HeaderText="Account Ownership Type"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                            <asp:BoundField DataField="CurrencyType" HeaderText="Currency" />
                                                                            <asp:BoundField DataField="Product" HeaderText="Product" />
                                                                            <asp:BoundField DataField="OutStanding" DataFormatString="{0:#,##0.00}" HeaderText="OutStanding"
                                                                                HtmlEncode="False"><ItemStyle HorizontalAlign="Right" /></asp:BoundField>
                                                                        </Columns>
                                                                        <FooterStyle BackColor="#CCCC99" />
                                                                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                        <RowStyle BackColor="#F7F7DE" />
                                                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                            <tr id="Tr33" runat="server">
                                                                <td id="Td45" runat="server" align="right">
                                                                    Total Credit Card :
                                                                    <asp:Label ID="LblTotalCreditCard" runat="server" Text="" />
                                                                </td>
                                                            </tr>
                                                            <tr id="Tr36" runat="server">
                                                                <td id="Td48" runat="server" align="right" style="font-size: 12">
                                                                    <strong>Total Asset :
                                                                        <asp:Label ID="LblTotalAsset" runat="server" Text="" />
                                                                    </strong>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="Td49" runat="server" style="font-size: 12px">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="Td50" runat="server" style="font-size: 12px">
                                                                    <strong>Liabilities Detail</strong>
                                                                    <hr />
                                                                </td>
                                                            </tr>
                                                            <tr id="Tr26" runat="server">
                                                                <td id="Td36" runat="server">
                                                                    Filter :
                                                                    <asp:DropDownList ID="CboFilterLiabilities" runat="server" CssClass="combobox">
                                                                    </asp:DropDownList>
                                                                    &nbsp;<asp:TextBox ID="txtFilterliabilities" runat="server" CssClass="combobox"></asp:TextBox>
                                                                    <input id="popupliabilities" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                        border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                        border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                        height: 17px" title="Click to show calendar" type="button" />
                                                                    &nbsp;
                                                                    <asp:ImageButton ID="ImgSearchLiabilities" runat="server" ImageUrl="~/Images/button/search.gif" />
                                                                </td>
                                                            </tr>
                                                            <tr id="Tr28" runat="server">
                                                                <td id="Td37" runat="server">
                                                                    <strong><span style="font-size: 12px"></span></strong>
                                                                    <asp:GridView ID="GrdLiabilities" runat="server" AutoGenerateColumns="False" BackColor="White"
                                                                        BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" EmptyDataText="There is no Asset Account"
                                                                        EnableModelValidation="True" ForeColor="Black">
                                                                        <AlternatingRowStyle BackColor="White" />
                                                                        <Columns>
                                                                            <asp:BoundField DataField="CIFNo" HeaderText="CIFNo" />
                                                                            <asp:BoundField DataField="AccountOwnerName" HeaderText="Accont Owner" />
                                                                            <asp:TemplateField HeaderText="Account No">
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("AccountNo") %>'></asp:TextBox>
                                                                                </EditItemTemplate>
                                                                                <ItemTemplate>
                                                                                    <%#GenerateAccountLink(Eval("AccountNo")) %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="AccountName" HeaderText="Name" />
                                                                            <asp:BoundField DataField="CIFRelation" HeaderText="CIFRelation"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                            <asp:BoundField DataField="AccountTypeDescription" HeaderText="Account Type" />
                                                                            <asp:BoundField DataField="OpeningDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Opening Date"
                                                                                HtmlEncode="False" />
                                                                            <asp:BoundField DataField="AccountStatusDescription" HeaderText="Status"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                            <asp:BoundField DataField="IsJointAccount" HeaderText="Joint Account"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                            <asp:BoundField DataField="JointAccountOwnership" HeaderText="Account Ownership Type"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                            <asp:BoundField DataField="CurrencyType" HeaderText="Currency" />
                                                                            <asp:BoundField DataField="Saldo" DataFormatString="{0:#,##0.00}" HeaderText="Saldo"
                                                                                HtmlEncode="False"><ItemStyle HorizontalAlign="Right" /></asp:BoundField>
                                                                        </Columns>
                                                                        <FooterStyle BackColor="#CCCC99" />
                                                                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                        <RowStyle BackColor="#F7F7DE" />
                                                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                            <tr id="Tr37" runat="server">
                                                                <td id="Td51" runat="server" align="right" style="font-size: 12px">
                                                                    <strong>Total Liabilities :
                                                                        <asp:Label ID="LblTotalLiabilities" runat="server" Text="" />
                                                                    </strong>
                                                                </td>
                                                            </tr>
                                                        </caption>
                                                    </table>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="JIVEPanel" runat="server">
                                                                    <tr runat="server" id="Tr38">
                                                                        <td runat="server" id="Td52">
                                                                            <strong><span style="font-size: 9pt">WEALTH MANAGEMENT </span></strong>
                                                                            <hr />
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="Tr39" runat="server">
                                                                        <td id="Td53" runat="server">
                                                                            Filter :
                                                                            <asp:DropDownList ID="CboFilterJIVE" runat="server" CssClass="combobox" AutoPostBack="True">
                                                                            </asp:DropDownList>
                                                                            &nbsp;<asp:TextBox ID="txtFilterJIVE" runat="server" CssClass="combobox"></asp:TextBox>
                                                                            <input id="popupJIVE" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                                border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                                border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                                height: 17px" title="Click to show calendar" type="button" />&nbsp;
                                                                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/button/search.gif" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr runat="server" id="Tr40">
                                                                        <td id="Td54" runat="server">
                                                                            <strong><span style="font-size: 12px"></span></strong>
                                                                            <asp:GridView runat="server" ID="GrdJIVE" BackColor="White" BorderColor="#DEDFDE"
                                                                                BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True"
                                                                                ForeColor="Black" AutoGenerateColumns="False" EmptyDataText="There is no JIVE Account">
                                                                                <AlternatingRowStyle BackColor="White" />
                                                                                <Columns>
                                                                                    <asp:BoundField DataField="CIFNo" HeaderText="CIFNo" />
                                                                                    <asp:BoundField DataField="AccountOwnerName" HeaderText="Accont Owner" />
											<asp:TemplateField HeaderText="Account No">
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("AccountNo") %>'></asp:TextBox>
                                                                                </EditItemTemplate>
                                                                                <ItemTemplate>
                                                                                    <%#GenerateAccountLink(Eval("AccountNo")) %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                                    <asp:BoundField DataField="AccountName" HeaderText="Name" />
                                                                                    <asp:BoundField DataField="CIFRelation" HeaderText="CIFRelation"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                                    <asp:BoundField DataField="AccountTypeDescription" HeaderText="Account Type" />
                                                                                    <asp:BoundField DataField="OpeningDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Opening Date"
                                                                                        HtmlEncode="False" />
                                                                                    <asp:BoundField DataField="AccountStatusDescription" HeaderText="Status"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                                    <asp:BoundField DataField="IsJointAccount" HeaderText="Joint Account"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                                    <asp:BoundField DataField="JointAccountOwnership" HeaderText="Account Ownership Type"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                                    <asp:BoundField DataField="ProductDescription" HeaderText="Product Description" />
                                                                                    <asp:BoundField DataField="CurrencyType" HeaderText="Currency" />
                                                                                    <asp:BoundField DataField="Saldo" DataFormatString="{0:#,##0.00}" HeaderText="Saldo"
                                                                                        HtmlEncode="False"><ItemStyle HorizontalAlign="Right" /></asp:BoundField>
                                                                                </Columns>
                                                                                <FooterStyle BackColor="#CCCC99" />
                                                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                                <RowStyle BackColor="#F7F7DE" />
                                                                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                            </asp:GridView>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="Tr41" runat="server">
                                                                        <td id="Td55" runat="server" align="right" style="font-size: 12px">
                                                                            <strong>Total WEALTH MANAGEMENT :
                                                                                <asp:Label ID="LblTotalJIVE" runat="server" />
                                                                            </strong>
                                                                        </td>
                                                                    </tr>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="HiporPanel" runat="server">
                                                                    <tr runat="server" id="Tr42">
                                                                        <td runat="server" id="Td56">
                                                                            <strong><span style="font-size: 9pt">CUSTODY </span></strong>
                                                                            <hr />
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="Tr43" runat="server">
                                                                        <td id="Td57" runat="server">
                                                                            Filter :
                                                                            <asp:DropDownList ID="CboFilterHipor" runat="server" CssClass="combobox">
                                                                            </asp:DropDownList>
                                                                            &nbsp;<asp:TextBox ID="txtFilterHipor" runat="server" CssClass="combobox"></asp:TextBox>
                                                                            <input id="popUpHipor" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                                border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                                border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                                height: 17px" title="Click to show calendar" type="button" />&nbsp;
                                                                            <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/button/search.gif" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr runat="server" id="Tr44">
                                                                        <td id="Td58" runat="server">
                                                                            <strong><span style="font-size: 12px"></span></strong>
                                                                            <asp:GridView runat="server" ID="GrdHipor" BackColor="White" BorderColor="#DEDFDE"
                                                                                BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True"
                                                                                ForeColor="Black" AutoGenerateColumns="False" EmptyDataText="There is no Hipor Account">
                                                                                <AlternatingRowStyle BackColor="White" />
                                                                                <Columns>
                                                                                    <asp:BoundField DataField="NAV_TXN_DATE" HeaderText="NAV_TXN_DATE"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                                    <asp:BoundField DataField="FUND_ID" HeaderText="FUND_ID"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                                    <asp:BoundField DataField="Description" HeaderText="Description"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                                    <asp:BoundField DataField="CIF" HeaderText="CIF"></asp:BoundField>
                                                                                    <asp:BoundField DataField="NAV" HeaderText="NAV" DataFormatString="{0:#,##0.00}"
                                                                                        HtmlEncode="False"><ItemStyle HorizontalAlign="Right" /></asp:BoundField>
                                                                                    <asp:BoundField DataField="AccountNo" HeaderText="AccountNo"></asp:BoundField>
                                                                                </Columns>
                                                                                <FooterStyle BackColor="#CCCC99" />
                                                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                                <RowStyle BackColor="#F7F7DE" />
                                                                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                            </asp:GridView>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="Tr45" runat="server">
                                                                        <td id="Td59" runat="server" align="right" style="font-size: 12px">
                                                                            <strong>Total CUSTODY :
                                                                                <asp:Label ID="LblTotalHipor" runat="server" />
                                                                            </strong>
                                                                        </td>
                                                                    </tr>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="PanelURSCustody" runat="server" Width="100%">
                                                                    <tr runat="server" id="Tr46">
                                                                        <td runat="server" id="Td60">
                                                                            <strong><span style="font-size: 9pt">URS CUSTODY&nbsp;</span></strong>
                                                                            <hr />
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="Tr47" runat="server">
                                                                        <td id="Td61" runat="server">
                                                                            Filter :
                                                                            <asp:DropDownList ID="CboFilterURSCustody" runat="server" CssClass="combobox">
                                                                            </asp:DropDownList>
                                                                            &nbsp;<asp:TextBox ID="txtFilterURSCustody" runat="server" CssClass="combobox"></asp:TextBox>
                                                                            <input id="popupUSRCustody" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                                border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                                border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                                height: 17px" title="Click to show calendar" type="button" />&nbsp;
                                                                            <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/button/search.gif" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr runat="server" id="Tr48">
                                                                        <td id="Td62" runat="server">
                                                                            <strong><span style="font-size: 12px"></span></strong>
                                                                            <asp:GridView runat="server" ID="GrdURSCustody" BackColor="White" BorderColor="#DEDFDE"
                                                                                BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True"
                                                                                ForeColor="Black" AutoGenerateColumns="False" EmptyDataText="There is no Hipor Account">
                                                                                <AlternatingRowStyle BackColor="White" />
                                                                                <Columns>
                                                                                    <asp:BoundField DataField="TanggalHolding" HeaderText="TanggalHolding" DataFormatString="{0:dd-MMM-yyyy}"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                                    <asp:BoundField DataField="NamaInvestor" HeaderText="NamaInvestor" />
                                                                                    <asp:BoundField DataField="CIF" HeaderText="CIF"></asp:BoundField>
                                                                                    <asp:BoundField DataField="FUND_ID" HeaderText="FUND_ID"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                                    <asp:BoundField DataField="NamaFund" HeaderText="NamaFund" />
                                                                                    <asp:BoundField DataField="Unit" HeaderText="Unit" DataFormatString="{0:#,##0.00}"
                                                                                        HtmlEncode="False"><ItemStyle HorizontalAlign="Center" /></asp:BoundField>
                                                                                    <asp:BoundField DataField="NAVPerUnit" HeaderText="NAVPerUnit" DataFormatString="{0:#,##0.00}"
                                                                                        HtmlEncode="False"><ItemStyle HorizontalAlign="Right" /></asp:BoundField>
                                                                                    <asp:BoundField DataField="Amount" HeaderText="Amount" DataFormatString="{0:#,##0.00}"
                                                                                        HtmlEncode="False"></asp:BoundField>
                                                                                </Columns>
                                                                                <FooterStyle BackColor="#CCCC99" />
                                                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                                <RowStyle BackColor="#F7F7DE" />
                                                                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                            </asp:GridView>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="Tr49" runat="server">
                                                                        <td id="Td63" runat="server" align="right" style="font-size: 12px">
                                                                            <strong>Total URS CUSTODY :
                                                                                <asp:Label ID="LblTotalURSCustody" runat="server" />
                                                                            </strong>
                                                                        </td>
                                                                    </tr>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View ID="TabRiskInformation" runat="server">
                                                    <table width="100%" class="TabArea">
                                                        <tr id="Tr22" runat="server">
                                                            <td id="Td34" runat="server" align="left" class="maintitle">
                                                                Risk information
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr23">
                                                            <td id="TdRiskInformationNotAvailable" align="left" style="color: #ff0000; height: 15px;"
                                                                runat="server">
                                                                Data Not Available
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr24">
                                                            <td id="TdRiskInformationAvailable" runat="server">
                                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="1" bgcolor="#dddddd" border="2"
                                                                    width="75%" id="TABLE2">
                                                                    <tr style="background-color: #ffffff">
                                                                        <td style="width: 10%;">
                                                                            <strong>Customer Risk Rating </strong>
                                                                        </td>
                                                                        <td width="1%">
                                                                            :
                                                                        </td>
                                                                        <td style="width: 20%;">
                                                                            <strong>
                                                                                <asp:Label ID="RiskRatingNameLabel" runat="server" Font-Bold="True"></asp:Label>(<asp:Label
                                                                                    ID="LblTotalRating" runat="server" Font-Bold="True"></asp:Label>)</strong>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td colspan="3">
                                                                            <asp:DataGrid ID="GridRiskRating" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                                                                CellPadding="4" ForeColor="Black" GridLines="Vertical">
                                                                                <FooterStyle BackColor="#CCCC99" />
                                                                                <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
                                                                                <AlternatingItemStyle BackColor="White" />
                                                                                <ItemStyle BackColor="#F7F7DE" />
                                                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                                <Columns>
                                                                                    <asp:BoundColumn DataField="RiskFactName" HeaderText="Risk Fact" SortExpression="Type desc"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="RiskScore" HeaderText="Risk Score" SortExpression="RiskScore desc"></asp:BoundColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View ID="TabVerificationList" runat="server">
                                                    <table border="0" bordercolor="#ffffff" cellpadding="1" cellspacing="1" width="100%">
                                                        <tr id="Tr4" runat="server">
                                                            <td id="Td7" runat="server" align="left" class="maintitle" style="height: 18px">
                                                                Watch List / Negative List Information
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="NotData">
                                                            <td id="Td8" align="left" style="color: #ff0033; background-color: #ffffff;" runat="server">
                                                                Data Not Available
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr5">
                                                            <td id="Td9" align="left" style="color: #ff0033; background-color: #ffffff" runat="server">
                                                                <asp:DataGrid ID="GridWatch" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                                                    CellPadding="4" ForeColor="Black" GridLines="Vertical">
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="False" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
                                                                    <ItemStyle BackColor="#F7F7DE" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="VerificationListId" Visible="False"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="DateEntered" DataFormatString="{0:dd MMMM yyyy hh:mm:ss}"
                                                                            HeaderText="Entry Date" SortExpression="DateEntered desc"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="DisplayName" HeaderText="Name" SortExpression="DisplayName desc"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="DateOfBirth" HeaderText="Date Of Birth" SortExpression="DateOfBirth desc"
                                                                            DataFormatString="{0:dd-MMMM-yyyy}"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="CategoryName" HeaderText="Category" SortExpression="CategoryName desc"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="ListTypeName" HeaderText="List Type" SortExpression="ListTypeName desc"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="PercentMatch" HeaderText="Persent Match" SortExpression="PercentMatch desc"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="JudgeBy" HeaderText="JudgeBy" SortExpression="JudgeBy desc"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="ApprovedBy" HeaderText="Approved By" SortExpression="ApprovedBy desc"></asp:BoundColumn>
                                                                        <asp:EditCommandColumn CancelText="Cancel" EditText="Detail" UpdateText="Update"></asp:EditCommandColumn>
                                                                    </Columns>
                                                                    <AlternatingItemStyle BackColor="White" />
                                                                </asp:DataGrid>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <strong>Verification Detail :<br />
                                                    </strong>
                                                    <table bgcolor="#dddddd" border="0" bordercolor="#ffffff" cellpadding="1" cellspacing="1"
                                                        style="font-weight: bold" width="100%" runat="server" id="DetailData">
                                                        <tr id="Tr6" style="background-color: #ffffff" runat="server">
                                                            <td id="Td10" style="width: 25%; background-color: lemonchiffon; height: 15px;" runat="server">
                                                                Alias
                                                            </td>
                                                            <td id="Td11" style="width: 35%; background-color: lemonchiffon; height: 15px;" runat="server">
                                                                Address
                                                            </td>
                                                            <td id="Td12" style="width: 35%; background-color: lemonchiffon; height: 15px;" runat="server">
                                                                Id Number
                                                            </td>
                                                        </tr>
                                                        <tr id="Tr7" style="background-color: #ffffff" runat="server">
                                                            <td id="Td13" style="width: 100px" runat="server">
                                                                <asp:ListBox ID="ListAlias" runat="server" Font-Size="8pt" SelectionMode="Multiple"
                                                                    Width="224px" CssClass="textbox"></asp:ListBox>
                                                            </td>
                                                            <td id="Td14" style="width: 100px" runat="server">
                                                                <asp:ListBox ID="ListAddress" runat="server" Font-Size="8pt" SelectionMode="Multiple"
                                                                    Width="344px" CssClass="textbox"></asp:ListBox>
                                                            </td>
                                                            <td id="Td15" style="width: 100px" runat="server">
                                                                <asp:ListBox ID="ListIdNumber" runat="server" Font-Size="8pt" SelectionMode="Multiple"
                                                                    Width="272px" CssClass="textbox"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                        <tr id="Tr8" style="background-color: #ffffff" runat="server">
                                                            <td id="Td16" style="width: 100px" runat="server">
                                                            </td>
                                                            <td id="Td17" style="width: 100px" runat="server">
                                                            </td>
                                                            <td id="Td18" style="width: 100px" runat="server">
                                                            </td>
                                                        </tr>
                                                        <tr id="Tr9" style="background-color: #ffffff" runat="server">
                                                            <td id="Td19" style="width: 200px; background-color: lemonchiffon;" runat="server">
                                                                Date Of Data
                                                            </td>
                                                            <td id="Td20" style="width: 200px; background-color: lemonchiffon;" runat="server">
                                                                Custom Remark 1
                                                            </td>
                                                            <td id="Td21" style="width: 200px; background-color: lemonchiffon;" runat="server">
                                                                Custom Remark 2
                                                            </td>
                                                        </tr>
                                                        <tr id="Tr10" style="background-color: #ffffff" runat="server">
                                                            <td id="Td22" style="width: 100px" valign="top" runat="server">
                                                                <asp:Label ID="LblDateOfData" runat="server"></asp:Label>
                                                            </td>
                                                            <td id="Td23" style="width: 100px" runat="server">
                                                                <asp:TextBox ID="TxtCustomRemark1" runat="server" CssClass="textbox" Height="42px"
                                                                    TextMode="MultiLine" Width="344px"></asp:TextBox>
                                                            </td>
                                                            <td id="Td24" style="width: 100px" runat="server">
                                                                <asp:TextBox ID="TxtCustomRemark2" runat="server" CssClass="textbox" Height="42px"
                                                                    TextMode="MultiLine" Width="272px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr id="Tr11" style="background-color: #ffffff" runat="server">
                                                            <td id="Td25" style="width: 200px; background-color: lemonchiffon;" runat="server">
                                                                Custom Remark 3
                                                            </td>
                                                            <td id="Td26" style="width: 200px; background-color: lemonchiffon;" runat="server">
                                                                Custom Remark 4
                                                            </td>
                                                            <td id="Td27" style="width: 200px; background-color: lemonchiffon;" runat="server">
                                                                Custom Remark 5
                                                            </td>
                                                        </tr>
                                                        <tr id="Tr12" style="background-color: #ffffff" runat="server">
                                                            <td id="Td28" style="width: 100px" runat="server">
                                                                <asp:TextBox ID="TxtCustomRemark3" runat="server" CssClass="textbox" Height="42px"
                                                                    TextMode="MultiLine" Width="224px"></asp:TextBox>
                                                            </td>
                                                            <td id="Td29" style="width: 100px" runat="server">
                                                                <asp:TextBox ID="TxtCustomRemark4" runat="server" CssClass="textbox" Height="42px"
                                                                    TextMode="MultiLine" Width="344px"></asp:TextBox>
                                                            </td>
                                                            <td id="Td30" style="width: 100px" runat="server">
                                                                <asp:TextBox ID="TxtCustomRemark5" runat="server" CssClass="textbox" Height="42px"
                                                                    TextMode="MultiLine" Width="272px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View ID="TabLinkAnalysis" runat="server">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                Transaction Amount
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td>
                                                                &nbsp; From &nbsp;
                                                                <asp:TextBox ID="txtFromAmount" runat="server"></asp:TextBox>
                                                                &nbsp; To &nbsp;
                                                                <asp:TextBox ID="txtToAmount" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <asp:ImageButton ID="imgBtnGOLinkAnalysis" runat="server" ImageUrl="~/Images/button/go.gif" />
                                                                &nbsp;
                                                                <asp:ImageButton ID="imgBtnClearSearchLinkAnalysis" runat="server" ImageUrl="~/Images/button/clearsearch.gif" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <iframe id="IFrameLinkAnalysis" src="linkanalysisview.aspx" scrolling="yes" height="600"
                                                        width="100%"></iframe>
                                                </asp:View>
                                                <asp:View ID="TabCDDLNP" runat="server">
                                                    <table align="top" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                        bgcolor="#dddddd" border="2" style="vertical-align: top">
                                                        <tr id="Tr51" runat="server" valign="top">
                                                            <td id="Td64" runat="server" align="left" class="maintitle" style="height: 18px">
                                                                EDD Inquiry
                                                            </td>
                                                        </tr>
                                                        <tr id="TR50" valign="top">
                                                            <td valign="top" width="98%" bgcolor="#ffffff">
                                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="0" width="100%" bgcolor="#dddddd"
                                                                    border="2">
                                                                    <tr>
                                                                        <td bgcolor="#ffffff">
                                                                            <asp:DataGrid ID="GridViewCDDLNP" runat="server" AutoGenerateColumns="False" Font-Size="XX-Small"
                                                                                BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None" Width="100%"
                                                                                GridLines="Vertical" BorderColor="#DEDFDE" ForeColor="Black">
                                                                                <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                                                <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
                                                                                <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                                                <ItemStyle BackColor="#F7F7DE"></ItemStyle>
                                                                                <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B" HorizontalAlign="Center">
                                                                                </HeaderStyle>
                                                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Visible="false" />
                                                                                <Columns>
                                                                                    <%--0--%>
                                                                                    <asp:TemplateColumn Visible="False"><ItemTemplate><asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" /></ItemTemplate><HeaderStyle Width="2%" /></asp:TemplateColumn>
                                                                                    <%--1--%>
                                                                                    <asp:BoundColumn DataField="PK_CDDLNP_ID" Visible="false" />
                                                                                    <%--2--%>
                                                                                    <asp:BoundColumn HeaderText="No" HeaderStyle-ForeColor="White" ItemStyle-Width="3%"><HeaderStyle ForeColor="White" /><ItemStyle Width="3%" /></asp:BoundColumn>
                                                                                    <%--3--%>
                                                                                    <asp:BoundColumn DataField="FK_CDD_Type_ID" Visible="false" />
                                                                                    <%--4--%>
                                                                                    <asp:BoundColumn DataField="CDD_Type" HeaderText="CDD Type" SortExpression="CDD_Type  desc"
                                                                                        ItemStyle-Width="9%"><HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" ForeColor="White" /><ItemStyle Width="9%" /></asp:BoundColumn>
                                                                                    <%--5--%>
                                                                                    <asp:BoundColumn DataField="FK_CDD_NasabahType_ID" Visible="false" />
                                                                                    <%--6--%>
                                                                                    <asp:BoundColumn DataField="CDD_NasabahType_ID" HeaderText="Customer Type" SortExpression="CDD_NasabahType_ID  desc"
                                                                                        ItemStyle-Width="8%"><HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" ForeColor="White" /><ItemStyle Width="8%" /></asp:BoundColumn>
                                                                                    <%--7--%>
                                                                                    <asp:BoundColumn DataField="Nama" HeaderText="Customer Name" SortExpression="Nama  desc"
                                                                                        ItemStyle-Width="10%"><HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" ForeColor="White" /><ItemStyle Width="10%" /></asp:BoundColumn>
                                                                                    <%--8--%>
                                                                                    <asp:BoundColumn DataField="CIF" HeaderText="CIF" SortExpression="CIF  desc" ItemStyle-Width="6%"><HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" ForeColor="White" /><ItemStyle Width="6%" /></asp:BoundColumn>
                                                                                    <%--9--%>
                                                                                    <asp:BoundColumn DataField="AMLRiskRatingPersonal" HeaderText="AML/CFT Risk Rating Personal"
                                                                                        SortExpression="AMLRiskRatingPersonal  desc" ItemStyle-Width="5%"><HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" ForeColor="White" /><ItemStyle Width="5%" /></asp:BoundColumn>
                                                                                    <%--10--%>
                                                                                    <asp:BoundColumn DataField="AMLRiskRatingGabungan" HeaderText="AML/CFT Risk Rating Gabungan (Final)"
                                                                                        SortExpression="AMLRiskRatingGabungan  desc" ItemStyle-Width="5%"><HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" ForeColor="White" /><ItemStyle Width="5%" /></asp:BoundColumn>
                                                                                    <%--11--%>
                                                                                    <asp:BoundColumn DataField="IsPEP" HeaderText="PEP Related" SortExpression="IsPEP  desc"
                                                                                        ItemStyle-Width="5%"><HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" ForeColor="White" /><ItemStyle Width="5%" /></asp:BoundColumn>
                                                                                    <%--12--%>
                                                                                    <asp:BoundColumn DataField="ParentBO" HeaderText="Beneficial Owner" SortExpression="ParentBO  desc"
                                                                                        ItemStyle-Width="10%"><HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" ForeColor="White" /><ItemStyle Width="10%" /></asp:BoundColumn>
                                                                                    <%--13--%>
                                                                                    <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" SortExpression="CreatedDate  desc"
                                                                                        DataFormatString="{0:dd-MMM-yyyy}" ItemStyle-Width="7%"><HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" ForeColor="White" /><ItemStyle Width="7%" /></asp:BoundColumn>
                                                                                    <%--14--%>
                                                                                    <asp:BoundColumn DataField="CreatedBy" HeaderText="CDD Initiator" SortExpression="CreatedBy  desc"
                                                                                        ItemStyle-Width="6%"><HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" ForeColor="White" /><ItemStyle Width="6%" /></asp:BoundColumn>
                                                                                    <%--15--%>
                                                                                    <asp:BoundColumn DataField="ApprovalStatus" HeaderText="Submission Status" SortExpression="ApprovalStatus  desc"
                                                                                        ItemStyle-Width="8%"><HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                            Font-Underline="False" ForeColor="White" /><ItemStyle Width="8%" /></asp:BoundColumn>
                                                                                    <%--16--%>
                                                                                    <asp:BoundColumn DataField="LastApprovalStatus" Visible="false" />
                                                                                    <%--17--%>
                                                                                    <asp:BoundColumn DataField="LastWorkflowGroup" HeaderText="PIC" HeaderStyle-ForeColor="white"
                                                                                        ItemStyle-Width="8%"><HeaderStyle ForeColor="White" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                                                            Font-Strikeout="False" Font-Underline="False" /><ItemStyle Width="8%" /></asp:BoundColumn>
                                                                                    <%--18--%>
                                                                                    <asp:BoundColumn DataField="LastApprovalDate" HeaderText="Submission Date" HeaderStyle-ForeColor="white"
                                                                                        SortExpression="LastApprovalDate  asc" DataFormatString="{0:dd-MMM-yyyy}" ItemStyle-Width="8%"><HeaderStyle ForeColor="White" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                                                            Font-Strikeout="False" Font-Underline="False" /><ItemStyle Width="8%" /></asp:BoundColumn>
                                                                                    <%--19--%>
                                                                                    <asp:BoundColumn DataField="Aging" HeaderText="Aging" HeaderStyle-ForeColor="white"
                                                                                        SortExpression="Aging  desc" ItemStyle-Width="8%" DataFormatString="{0} day(s)"><HeaderStyle ForeColor="White" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                                                            Font-Strikeout="False" Font-Underline="False" /><ItemStyle Width="8%" /></asp:BoundColumn>
                                                                                    <asp:TemplateColumn HeaderStyle-Width="5%"><ItemTemplate><asp:LinkButton ID="LnkDetail" runat="server" CausesValidation="false" CommandName="Detail"
                                                                                                Text="View" ajaxcall="none"></asp:LinkButton></ItemTemplate><HeaderStyle Width="5%" /></asp:TemplateColumn>
                                                                                    <asp:TemplateColumn Visible="false"><ItemTemplate><asp:LinkButton ID="LnkPrint" runat="server" CausesValidation="false" CommandName="Print"
                                                                                                Text="Print" ajaxcall="none"></asp:LinkButton></ItemTemplate><HeaderStyle Width="5%" /></asp:TemplateColumn>
                                                                                    <asp:TemplateColumn Visible="false"><ItemTemplate><asp:LinkButton ID="LnkEdit" runat="server" CausesValidation="false" CommandName="Edit"
                                                                                                Text="Edit"></asp:LinkButton></ItemTemplate><HeaderStyle Width="5%" /></asp:TemplateColumn>
                                                                                    <asp:TemplateColumn Visible="false"><ItemTemplate><asp:LinkButton ID="LnkDelete" runat="server" CausesValidation="false" CommandName="Delete"
                                                                                                Text="Delete"></asp:LinkButton></ItemTemplate><HeaderStyle Width="5%" /></asp:TemplateColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                            <asp:Label ID="lblNoRecord" runat="server" Text="No record is found" Visible="false"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table id="TableCDDDetail" align="top" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                                        width="99%" bgcolor="#dddddd" border="2" style="vertical-align: top" runat="server">
                                                        <tr id="Tr52" runat="server" valign="top">
                                                            <td id="Td65" runat="server" align="left" class="maintitle" style="height: 18px">
                                                                CDD Detail
                                                            </td>
                                                        </tr>
                                                        <tr id="SearchCriteria">
                                                            <td valign="top" bgcolor="#ffffff">
                                                                <table style="border-top-style: none; border-right-style: none; border-left-style: none;
                                                                    border-bottom-style: none" cellspacing="1" cellpadding="2" width="100%" border="0">
                                                                    <tbody>
                                                                        <%--<tr>
                                                                <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                </td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    Nasabah</td>
                                                                <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                    Beneficial Owner</td>
                                                            </tr>--%>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                                CDD Type
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <asp:Label ID="lblCDDTypeNasabah" runat="server"></asp:Label>
                                                                                <asp:HiddenField ID="hfCDDTypeNasabah" runat="server"></asp:HiddenField>
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <asp:Label ID="lblCDDTypeBO" runat="server" Visible="false">-</asp:Label>
                                                                                <asp:HiddenField ID="hfCDDTypeBO" runat="server"></asp:HiddenField>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                                Customer Type
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <strong>
                                                                                    <asp:Label ID="lblCustomerTypeNasabah" runat="server"></asp:Label>
                                                                                </strong>
                                                                                <asp:HiddenField ID="hfCustomerTypeNasabah" runat="server"></asp:HiddenField>
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <strong>
                                                                                    <asp:Label ID="lblCustomerTypeBO" runat="server">-</asp:Label>
                                                                                </strong>
                                                                                <asp:HiddenField ID="hfCustomerTypeBO" runat="server"></asp:HiddenField>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                                Customer Name
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <asp:Label ID="lblNameNasabah" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <asp:Label ID="lblNameBO" runat="server">-</asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                                AML Risk Rating Personal
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <asp:Label ID="lblRatingPersonalNasabah" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <asp:Label ID="lblRatingPersonalBO" runat="server">-</asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                                AML Risk Rating Gabungan (Final)
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <asp:Label ID="lblRatingGabunganNasabah" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <asp:Label ID="lblRatingGabunganBO" runat="server">-</asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                                Politically Exposed Person
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <asp:Label ID="lblIsPEPNasabah" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <asp:Label ID="lblIsPEPBO" runat="server">-</asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                                Screening Result
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <asp:LinkButton ID="lnkSuspectNasabah" runat="server" ajaxcall="none"></asp:LinkButton><asp:Label
                                                                                    ID="lblScreeningNasabah" runat="server">-</asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <asp:LinkButton ID="lnkSuspectBO" runat="server" ajaxcall="none"></asp:LinkButton><asp:Label
                                                                                    ID="lblScreeningBO" runat="server">-</asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6" nowrap>
                                                                                CDD Form
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <asp:LinkButton ID="lnkCDDNasabah" runat="server" ajaxcall="none">View CDD Nasabah</asp:LinkButton><asp:Label
                                                                                    ID="lblCDDNasabah" runat="server">-</asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%; background-color: #ffffff" nowrap>
                                                                                <asp:LinkButton ID="lnkCDDBO" runat="server" ajaxcall="none">View CDD Beneficial Owner</asp:LinkButton><asp:Label
                                                                                    ID="lblCDDBO" runat="server">-</asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td nowrap="nowrap" style="width: 15%; background-color: #fff7e6" valign="top">
                                                                                Attachment
                                                                            </td>
                                                                            <td nowrap="nowrap" style="width: 35%; background-color: #ffffff" id="RowAttachmentNasabah"
                                                                                runat="server">
                                                                            </td>
                                                                            <td nowrap="nowrap" style="width: 35%; background-color: #ffffff" id="RowAttachmentBO"
                                                                                runat="server">
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                
                                                <asp:View ID="TabStakeHolder" runat="server">
                                                    <table width="100%" class="TabArea">
                                                        <tr>
                                                            <td id="Td70" runat="server" style="font-size: 12px">
                                                                <strong>STAKEHOLDE DETAIL</strong>
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr id="Tr56" runat="server">
                                                            <td id="Td71" runat="server">
                                                                Filter :
                                                                <asp:DropDownList ID="CbFilStakeholder" runat="server" CssClass="combobox">
                                                                </asp:DropDownList>
                                                                &nbsp;<asp:TextBox ID="TxtFilStakeholder" runat="server" CssClass="combobox"></asp:TextBox>
                                                                <input id="popStakeholder" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                    border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                    border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                    height: 17px" title="Click to show calendar" type="button" />
                                                                &nbsp;
                                                                <asp:ImageButton ID="ImgFilStakeholder" runat="server" ImageUrl="~/Images/button/search.gif" />

                                                            </td>
                                                        </tr>
                                                        <tr id="Tr57" runat="server">
                                                            <td id="Td72" runat="server">
                                                                <strong><span style="font-size: 12px"></span></strong>
                                                                <asp:GridView ID="GridStakeholderDetail" runat="server" AutoGenerateColumns="False" BackColor="White"
                                                                    BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" EmptyDataText="There is no Stakeholder Detail"
                                                                    EnableModelValidation="True" ForeColor="Black">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="BUSINESS_DATE" HeaderText="Bussiness Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                                        <asp:BoundField DataField="CUST_NO" HeaderText="Customer No" />
                                                                        <asp:BoundField DataField="STAKEHOLDER_SEQ_NO" HeaderText="Stakeholder Seq No" />
                                                                        <asp:BoundField DataField="STAKEHOLDER_CUST_NO" HeaderText="Stakeholder Customer No" />
                                                                        <asp:BoundField DataField="STAKEHOLDER_NAME" HeaderText="Stakeholder Name" />
                                                                        <asp:BoundField DataField="ID_NUMBER" HeaderText="ID Number" />
                                                                        <asp:BoundField DataField="ID_TYPE" HeaderText="ID Type" />
                                                                        <asp:BoundField DataField="ALTERNATE_ID_NUMBER" HeaderText="Alternate ID Number" />
                                                                        <asp:BoundField DataField="ALTERNATE_ID_TYPE" HeaderText="Alternate ID Type" />
                                                                        <asp:BoundField DataField="LAST_MAINTENANCE_DATE" HeaderText="Last Maintenance Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                                        <asp:BoundField DataField="DOB" HeaderText="DOB" DataFormatString="{0:dd-MMM-yyyy}" />
                                                                        <asp:BoundField DataField="DESIGNATION" HeaderText="Designation" />
                                                                        <asp:BoundField DataField="OWNERSHIP" HeaderText="Ownership" />
                                                                        <asp:BoundField DataField="GENDER" HeaderText="Gender" />
                                                                        <asp:BoundField DataField="NATIONALITY" HeaderText="Nationality" />
                                                                    </Columns>
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                    <RowStyle BackColor="#F7F7DE" />
                                                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td id="Td69" runat="server" style="font-size: 12px">
                                                                <strong>BO/BENEFICIAL OWNERDETAIL</strong>
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr id="Tr55" runat="server">
                                                            <td id="Td73" runat="server">
                                                                Filter :
                                                                <asp:DropDownList ID="CbFilBeneficial" runat="server" CssClass="combobox">
                                                                </asp:DropDownList>
                                                                &nbsp;<asp:TextBox ID="TxtFilBeneficial" runat="server" CssClass="combobox"></asp:TextBox>
                                                                <input id="popBeneficial" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                    border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                    border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                    height: 17px" title="Click to show calendar" type="button" />
                                                                &nbsp;
                                                                <asp:ImageButton ID="ImgFilBeneficial" runat="server" ImageUrl="~/Images/button/search.gif" />
                                                            </td>
                                                        </tr>
                                                        <tr id="Tr58" runat="server">
                                                            <td id="Td74" runat="server">
                                                                <strong><span style="font-size: 12px"></span></strong>
                                                                <asp:GridView ID="GridBeneficialOwner" runat="server" AutoGenerateColumns="False" BackColor="White"
                                                                    BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" EmptyDataText="There is no Beneficial Owner Detail"
                                                                    EnableModelValidation="True" ForeColor="Black">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="BUSINESS_DATE" HeaderText="Bussiness Date" DataFormatString="{0:dd-MMM-yyyy}" />
                                                                        <asp:BoundField DataField="CUST_NO" HeaderText="Customer No" />
                                                                        <asp:BoundField DataField="BENEFICIAL_CUST_NO" HeaderText="Beneficial Customer No" />
                                                                        <asp:BoundField DataField="BENEFICIARY_NAME" HeaderText="Beneficiary Name" />
                                                                        <asp:BoundField DataField="BUSINESS_TYPE" HeaderText="Business Type" />
                                                                        <asp:BoundField DataField="JOB_TYTLE" HeaderText="Job Title" />
                                                                        <asp:BoundField DataField="COMPANY_ADDRESS1" HeaderText="Address 1" />
                                                                        <asp:BoundField DataField="COMPANY_ADDRESS2" HeaderText="Address 2" />
                                                                        <asp:BoundField DataField="COMPANY_ADDRESS3" HeaderText="Address 3" />
                                                                        <asp:BoundField DataField="COMPANY_ADDRESS4" HeaderText="Address 4" />
                                                                        <asp:BoundField DataField="COMPANY_ADDRESS5" HeaderText="Address 5" />
                                                                        <asp:BoundField DataField="COMPANY_ADDRESS6" HeaderText="Address 6" />
                                                                        <asp:BoundField DataField="COMPANY_NAME" HeaderText="Company Name" />
                                                                        <asp:BoundField DataField="DOB" HeaderText="DOB" DataFormatString="{0:dd-MMM-yyyy}" />
                                                                        <asp:BoundField DataField="NATIONALITY" HeaderText="Nationality" />
                                                                    </Columns>
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                    <RowStyle BackColor="#F7F7DE" />
                                                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View ID="TabRiskRatingHistory" runat="server">
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="maintitle">
                                                                <span>Current AML Risk Score Detail: </span>
                                                                <asp:label id="lblCurrentScore" runat="server" />
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:GridView runat="server" ID="GrdRiskCurrentRatingDetail" BackColor="White" BorderColor="#DEDFDE" EmptyDataText="There is no Current Rating Detail"
                                                                    BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True"
                                                                    ForeColor="Black" AutoGenerateColumns="False" DataKeyNames="PK_RiskFactDetail_ID">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="RiskFactDetail" HeaderText="Risk Factor" />
                                                                        <asp:BoundField DataField="RiskFactDetailValue" HeaderText="Risk Factor Description" />
                                                                        <asp:BoundField DataField="RiskFactScore" HeaderText="Risk Score" />
                                                                    </Columns>
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                    <RowStyle BackColor="#F7F7DE" />
                                                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr id="trHeader1" runat="server" visible="false">
                                                            <td class="maintitle">
                                                                <span>Previous AML Risk Score: <asp:label id="lblPrevious1" runat="server" /></span>
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr id="trDetail1" runat="server" visible="false">
                                                            <td>
                                                                <asp:GridView runat="server" ID="GrdRiskPreviousRatingHistory1" BackColor="White" BorderColor="#DEDFDE" EmptyDataText="There is no Previous Rating Detail"
                                                                    BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True"
                                                                    ForeColor="Black" AutoGenerateColumns="False" DataKeyNames="PK_RiskFactFinalHistory_ID">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="RiskFactDetail" HeaderText="Risk Factor" />
                                                                        <asp:BoundField DataField="RiskFactDetailValue" HeaderText="Risk Factor Description" />
                                                                        <asp:BoundField DataField="RiskFactScore" HeaderText="Risk Score" />
                                                                        <asp:BoundField DataField="CreatedDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Last Date"
                                                                            HtmlEncode="False" />
                                                                    </Columns>
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                    <RowStyle BackColor="#F7F7DE" />
                                                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>

                                                        <tr id="trHeader2" runat="server" visible="false">
                                                            <td class="maintitle">
                                                                <span>Previous AML Risk Score: <asp:label id="lblPrevious2" runat="server" /></span>
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr id="trDetail2" runat="server" visible="false">
                                                            <td>
                                                                <asp:GridView runat="server" ID="GrdRiskPreviousRatingHistory2" BackColor="White" BorderColor="#DEDFDE"
                                                                    BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True"
                                                                    ForeColor="Black" AutoGenerateColumns="False" DataKeyNames="PK_RiskFactFinalHistory_ID">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="RiskFactDetail" HeaderText="Risk Factor" />
                                                                        <asp:BoundField DataField="RiskFactDetailValue" HeaderText="Risk Factor Description" />
                                                                        <asp:BoundField DataField="RiskFactScore" HeaderText="Risk Score" />
                                                                        <asp:BoundField DataField="CreatedDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Last Date"
                                                                            HtmlEncode="False" />
                                                                    </Columns>
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                    <RowStyle BackColor="#F7F7DE" />
                                                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>

                                                        <tr id="trHeader3" runat="server" visible="false">
                                                            <td class="maintitle">
                                                                <span>Previous AML Risk Score: <asp:label id="lblPrevious3" runat="server" /></span>
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr id="trDetail3" runat="server" visible="false">
                                                            <td>
                                                                <asp:GridView runat="server" ID="GrdRiskPreviousRatingHistory3" BackColor="White" BorderColor="#DEDFDE"
                                                                    BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True"
                                                                    ForeColor="Black" AutoGenerateColumns="False" DataKeyNames="PK_RiskFactFinalHistory_ID">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="RiskFactDetail" HeaderText="Risk Factor" />
                                                                        <asp:BoundField DataField="RiskFactDetailValue" HeaderText="Risk Factor Description" />
                                                                        <asp:BoundField DataField="RiskFactScore" HeaderText="Risk Score" />
                                                                        <asp:BoundField DataField="CreatedDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Last Date"
                                                                            HtmlEncode="False" />
                                                                    </Columns>
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                    <RowStyle BackColor="#F7F7DE" />
                                                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>

                                                       <tr id="trHeader4" runat="server" visible="false">
                                                            <td class="maintitle">
                                                                <span>Previous AML Risk Score: <asp:label id="lblPrevious4" runat="server" /></span>
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr id="trDetail4" runat="server" visible="false">
                                                            <td>
                                                                <asp:GridView runat="server" ID="GrdRiskPreviousRatingHistory4" BackColor="White" BorderColor="#DEDFDE"
                                                                    BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True"
                                                                    ForeColor="Black" AutoGenerateColumns="False" DataKeyNames="PK_RiskFactFinalHistory_ID">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="RiskFactDetail" HeaderText="Risk Factor" />
                                                                        <asp:BoundField DataField="RiskFactDetailValue" HeaderText="Risk Factor Description" />
                                                                        <asp:BoundField DataField="RiskFactScore" HeaderText="Risk Score" />
                                                                        <asp:BoundField DataField="CreatedDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Last Date"
                                                                            HtmlEncode="False" />
                                                                    </Columns>
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                    <RowStyle BackColor="#F7F7DE" />
                                                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>

                                                        <tr id="trHeader5" runat="server" visible="false">
                                                            <td class="maintitle">
                                                                <span>Previous AML Risk Score: <asp:label id="lblPrevious5" runat="server" /></span>
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr id="trDetail5" runat="server" visible="false">
                                                            <td>
                                                                <asp:GridView runat="server" ID="GrdRiskPreviousRatingHistory5" BackColor="White" BorderColor="#DEDFDE"
                                                                    BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True"
                                                                    ForeColor="Black" AutoGenerateColumns="False" DataKeyNames="PK_RiskFactFinalHistory_ID">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="RiskFactDetail" HeaderText="Risk Factor" />
                                                                        <asp:BoundField DataField="RiskFactDetailValue" HeaderText="Risk Factor Description" />
                                                                        <asp:BoundField DataField="RiskFactScore" HeaderText="Risk Score" />
                                                                        <asp:BoundField DataField="CreatedDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Last Date"
                                                                            HtmlEncode="False" />
                                                                    </Columns>
                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                    <RowStyle BackColor="#F7F7DE" />
                                                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </asp:View>

                                                <%--CREDIT CARD GENERAL--%>
                                                <asp:View ID="TabCardGeneral" runat="server">
                                                    <table width="100%" class="TabArea">
                                                        <tr id="Tr77" runat="server">
                                                            <td id="Td93" runat="server" align="left" class="maintitle" style="height: 18px">
                                                                GENERAL CREDIT CARD
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr78">
                                                            <td id="TdNoPOB" align="left" style="color: red;" runat="server">
                                                                Data Not Available
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr79">
                                                            <td id="Td95" style="width: 50%" runat="server">
                                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="1" bgcolor="#dddddd" border="2"
                                                                    width="50%">
                                                                    <tr style="background-color: #ffffff">
                                                                        <asp:DataGrid ID="GridCreditCardPlaceOfBirth" runat="server" AllowSorting="True" AutoGenerateColumns="False"
																	        BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
																	        CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="500px">
																	        <FooterStyle BackColor="#CCCC99" />
																	        <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
																	        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
																	        <ItemStyle BackColor="#F7F7DE" />
																	        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
																	        <Columns>
																		        <asp:BoundColumn DataField="NO_KARTU_KREDIT" HeaderText="No.Card" SortExpression="NO_KARTU_KREDIT desc"><HeaderStyle Width="400px" /></asp:BoundColumn>
																		        <asp:BoundColumn DataField="ACCOUNT_NO" HeaderText="Account No" SortExpression="ACCOUNT_NO desc"><HeaderStyle Width="100px" /></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="TEMPAT_LAHIR" HeaderText="Place of Birth" SortExpression="TEMPAT_LAHIR desc"><HeaderStyle Width="400px" /></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="PENDAPATAN_RATA_RATA" HeaderText="Average Incoming" SortExpression="PENDAPATAN_RATA_RATA desc"><HeaderStyle Width="400px" /></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="JENIS_KELAMIN" HeaderText="Sex" SortExpression="JENIS_KELAMIN desc"><HeaderStyle Width="400px" /></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="STATUS_PERKAWINAN" HeaderText="Marriage Status" SortExpression="STATUS_PERKAWINAN desc"><HeaderStyle Width="400px" /></asp:BoundColumn>
																	        </Columns>
																	        <AlternatingItemStyle BackColor="White" />
																        </asp:DataGrid>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>

                                                <%--CREDIT CARD ADDRESS--%>
                                                <asp:View ID="TabCardAddress" runat="server">
                                                    <table width="100%" class="TabArea">
                                                        <tr id="Tr71" runat="server">
                                                            <td id="Td87" runat="server" align="left" class="maintitle" style="height: 18px">
                                                                ADDRESS DETAIL CREDIT CARD
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr72">
                                                            <td id="TdNoIDAddress" align="left" style="color: red;" runat="server">
                                                                Data Not Available
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr73">
                                                            <td id="Td89" style="width: 50%" runat="server">
                                                                <asp:DataGrid ID="GridCreditCardAddress" runat="server" AllowSorting="True" AutoGenerateColumns="False"
																	BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
																	CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="500px">
																	<FooterStyle BackColor="#CCCC99" />
																	<SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
																	<PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
																	<ItemStyle BackColor="#F7F7DE" />
																	<HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
																	<Columns>
																		<asp:BoundColumn DataField="NO_KARTU_KREDIT" HeaderText="No.Card" SortExpression="NO_KARTU_KREDIT desc"><HeaderStyle Width="400px" /></asp:BoundColumn>
																		<asp:BoundColumn DataField="ACCOUNT_NO" HeaderText="Account No" SortExpression="ACCOUNT_NO desc"><HeaderStyle Width="100px" /></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="ALAMAT_SESUAI_ID" HeaderText="ID Based Address" SortExpression="ALAMAT_SESUAI_ID desc"><HeaderStyle Width="800px" /></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="ALAMAT_TERKINI" HeaderText="Current Address" SortExpression="ALAMAT_TERKINI desc"><HeaderStyle Width="800px" /></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="ALAMAT_TEMPAT_BEKERJA" HeaderText="Workplace Address" SortExpression="ALAMAT_TEMPAT_BEKERJA desc"><HeaderStyle Width="800px" /></asp:BoundColumn>
																	</Columns>
																	<AlternatingItemStyle BackColor="White" />
																</asp:DataGrid>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>

                                                <%--CREDIT CARD ID NUMBER--%>
                                                <asp:View ID="TabCardIDNumber" runat="server">
                                                    <table width="100%" class="TabArea">
                                                        <tr id="Tr65" runat="server">
                                                            <td id="Td81" runat="server" align="left" class="maintitle" style="height: 18px">
                                                                ID NUMBER CREDIT CARD
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr66">
                                                            <td id="tdNoIDNumber" align="left" style="color: red;" runat="server">
                                                                Data Not Available
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr67">
                                                            <td id="Td83" style="width: 50%" runat="server">
                                                                <asp:DataGrid ID="GridCreditCardCategory" runat="server" AllowSorting="True" AutoGenerateColumns="False"
					                                                BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
					                                                CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="500px">
					                                                <FooterStyle BackColor="#CCCC99" />
					                                                <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
					                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
					                                                <ItemStyle BackColor="#F7F7DE" />
					                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
					                                                <Columns>
						                                                <asp:BoundColumn DataField="NO_KARTU_KREDIT" HeaderText="No.Card" SortExpression="NO_KARTU_KREDIT desc"><HeaderStyle Width="400px" /></asp:BoundColumn>
						                                                <asp:BoundColumn DataField="ACCOUNT_NO" HeaderText="Account No" SortExpression="ACCOUNT_NO desc"><HeaderStyle Width="100px" /></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="JENIS_ID_1" HeaderText="Category ID 1" SortExpression="JENIS_ID_1 desc"><HeaderStyle Width="100px" /></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="JENIS_ID_2" HeaderText="Category ID 2" SortExpression="JENIS_ID_2 desc"><HeaderStyle Width="100px" /></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="NO_ID_1" HeaderText="ID Number 1" SortExpression="NO_ID_1 desc"><HeaderStyle Width="100px" /></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="NO_ID_2" HeaderText="ID Number 2" SortExpression="NO_ID_2 desc"><HeaderStyle Width="100px" /></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="NPWP" HeaderText="NPWP Number" SortExpression="NPWP desc"><HeaderStyle Width="100px" /></asp:BoundColumn>
					                                                </Columns>
					                                                <AlternatingItemStyle BackColor="White" />
				                                                </asp:DataGrid>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>

                                                <%--CREDIT CARD WORKPLACE PHONE NUMBER--%>
                                                <asp:View ID="TabCardWorkPhone" runat="server">
                                                    <table width="100%" class="TabArea">
                                                        <tr id="Tr101" runat="server">
                                                            <td id="Td117" runat="server" align="left" class="maintitle" style="height: 18px">
                                                                PHONE CREDIT CARD
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr102">
                                                            <td id="TdNoWorkPhone" align="left" style="color: red;" runat="server">
                                                                Data Not Available
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr103">
                                                            <td id="Td119" style="width: 50%" runat="server">
                                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="1" bgcolor="#dddddd" border="2"
                                                                    width="50%">
                                                                    <tr style="background-color: #ffffff">
                                                                        <asp:DataGrid ID="GridCreditCardWorkplacePhoneNumber" runat="server" AllowSorting="True" AutoGenerateColumns="False"
																	        BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
																	        CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="500px">
																	        <FooterStyle BackColor="#CCCC99" />
																	        <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
																	        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
																	        <ItemStyle BackColor="#F7F7DE" />
																	        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
																	        <Columns>
																		        <asp:BoundColumn DataField="NO_KARTU_KREDIT" HeaderText="No.Card" SortExpression="NO_KARTU_KREDIT desc"><HeaderStyle Width="400px" /></asp:BoundColumn>
																		        <asp:BoundColumn DataField="ACCOUNT_NO" HeaderText="Account No" SortExpression="ACCOUNT_NO desc"><HeaderStyle Width="100px" /></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="NOMOR_TELEPON_TEMPAT_BEKERJA" HeaderText="Phone Number" SortExpression="NOMOR_TELEPON_TEMPAT_BEKERJA desc"><HeaderStyle Width="400px" /></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="EKSTENSI_TELEPON_KARYAWAN" HeaderText="Extension" SortExpression="EKSTENSI_TELEPON_KARYAWAN desc"><HeaderStyle Width="400px" /></asp:BoundColumn>
																	        </Columns>
																	        <AlternatingItemStyle BackColor="White" />
																        </asp:DataGrid>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>

                                                <%--CREDIT CARD WORKPLACE--%>
                                                <asp:View ID="TabCardWorkplace" runat="server">
                                                    <table width="100%" class="TabArea">
                                                        <tr id="Tr89" runat="server">
                                                            <td id="Td105" runat="server" align="left" class="maintitle" style="height: 18px">
                                                                EMPLOYMENT HISTORY CREDIT CARD
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr90">
                                                            <td id="TdNoEmp" align="left" style="color: red;" runat="server">
                                                                Data Not Available
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="Tr91">
                                                            <td id="Td107" style="width: 50%" runat="server">
                                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="1" bgcolor="#dddddd" border="2"
                                                                    width="50%">
                                                                    <asp:DataGrid ID="GridCreditCardEmployer" runat="server" AllowSorting="True" AutoGenerateColumns="False"
																	    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
																	    CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="500px">
																	    <FooterStyle BackColor="#CCCC99" />
																	    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
																	    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
																	    <ItemStyle BackColor="#F7F7DE" />
																	    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
																	    <Columns>
																		    <asp:BoundColumn DataField="NO_KARTU_KREDIT" HeaderText="No.Card" SortExpression="NO_KARTU_KREDIT desc"><HeaderStyle Width="400px" /></asp:BoundColumn>
																		    <asp:BoundColumn DataField="ACCOUNT_NO" HeaderText="Account No" SortExpression="ACCOUNT_NO desc"><HeaderStyle Width="100px" /></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="TEMPAT_BEKERJA" HeaderText="Workplace Name" SortExpression="TEMPAT_BEKERJA desc"><HeaderStyle Width="400px" /></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="JABATAN" HeaderText="Position" SortExpression="JABATAN desc"><HeaderStyle Width="400px" /></asp:BoundColumn>
																	    </Columns>
																	    <AlternatingItemStyle BackColor="White" />
																    </asp:DataGrid>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>

                                            </asp:MultiView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;<asp:ImageButton ID="ImageBack" runat="server" SkinID="BackButton" OnClientClick="javascript:history.back()">
                    </asp:ImageButton>
                    <asp:CustomValidator ID="CValPageError" runat="server" Display="None"></asp:CustomValidator><br />
                </td>
            </tr>
        </table>
    </ajax:AjaxPanel>
</asp:Content>
