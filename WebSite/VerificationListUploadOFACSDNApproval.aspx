<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="VerificationListUploadOFACSDNApproval.aspx.vb" Inherits="VerificationListUploadOFACSDNApproval" title="Verification List Upload OFAC SDN List Approval" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpcontent" Runat="Server">
	 <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Pending Approval Verification List - OFAC SDN List
                </strong>
            </td>
        </tr>
    </table>
		<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" id="TableUploadData" runat="server">
            <tr>
                <td bgcolor="#ffffff">
                            <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
            </tr>
            <tr>
                <td bgcolor="#ffffff" style="height: 19px">
                    There is an upload for OFAC SDN List that need for approval with following data
                    supplied:</td>
            </tr>
            <tr>
                <td bgcolor="#ffffff">
                    SDN Data</td>
            </tr>
		<tr>
			<td bgColor="#ffffff">
       
                <ajax:ajaxpanel id="AjaxPanel4" runat="server">       
                    <div style="overflow:auto;height:200px;width:800px">
                    <asp:GridView ID="GridPendingApprovalSDN" runat="server" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                        CellPadding="4" DataKeyNames="ent_num" ForeColor="Black" GridLines="Vertical">
                        <FooterStyle BackColor="#CCCC99" />
                        <Columns>
                            <asp:BoundField DataField="ent_num" HeaderText="ent_num" ReadOnly="True" SortExpression="ent_num" />
                            <asp:BoundField DataField="SDN_Name" HeaderText="SDN_Name" SortExpression="SDN_Name" />
                            <asp:BoundField DataField="SDN_Type" HeaderText="SDN_Type" SortExpression="SDN_Type" />
                            <asp:BoundField DataField="Program" HeaderText="Program" SortExpression="Program" />
                            <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
                            <asp:BoundField DataField="Call_Sign" HeaderText="Call_Sign" SortExpression="Call_Sign" />
                            <asp:BoundField DataField="Vess_type" HeaderText="Vess_type" SortExpression="Vess_type" />
                            <asp:BoundField DataField="Tonnage" HeaderText="Tonnage" SortExpression="Tonnage" />
                            <asp:BoundField DataField="GRT" HeaderText="GRT" SortExpression="GRT" />
                            <asp:BoundField DataField="Vess_flag" HeaderText="Vess_flag" SortExpression="Vess_flag" />
                            <asp:BoundField DataField="Vess_owner" HeaderText="Vess_owner" SortExpression="Vess_owner" />
                            <asp:BoundField DataField="Remarks" HeaderText="Remarks" SortExpression="Remarks" />
                        </Columns>
                        <RowStyle BackColor="#F7F7DE" />
                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                    &nbsp;&nbsp;
                    </div>
                </ajax:ajaxpanel>
				</td>
				
		</tr>
            <tr>
                <td bgcolor="#ffffff">
                    &nbsp;</td>
            </tr>
            <tr>
                <td bgcolor="#ffffff">
                    SDN Alternative Name Data</td>
            </tr>
            <tr>
                <td bgcolor="#ffffff">
                    <ajax:ajaxpanel id="Ajaxpanel2" runat="server">
                        <div style="overflow:auto;height:200px;width:800px">
                            &nbsp;<asp:GridView ID="GridPendingApprovalSDNALT" runat="server" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="alt_num"
                                ForeColor="Black" GridLines="Vertical">
                                <FooterStyle BackColor="#CCCC99" />
                                <Columns>
                                    <asp:BoundField DataField="alt_num" HeaderText="alt_num" ReadOnly="True" SortExpression="alt_num" />
                                    <asp:BoundField DataField="ent_num" HeaderText="ent_num" SortExpression="ent_num" />
                                    <asp:BoundField DataField="alt_type" HeaderText="alt_type" SortExpression="alt_type" />
                                    <asp:BoundField DataField="alt_name" HeaderText="alt_name" SortExpression="alt_name" />
                                    <asp:BoundField DataField="alt_remarks" HeaderText="alt_remarks" SortExpression="alt_remarks" />
                                </Columns>
                                <RowStyle BackColor="#F7F7DE" />
                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                            &nbsp;
                        </div>
                    </ajax:AjaxPanel></td>
            </tr>
            <tr>
                <td bgcolor="#ffffff">
                    &nbsp;</td>
            </tr>
            <tr>
                <td bgcolor="#ffffff">
                    SDN Address Data</td>
            </tr>
            <tr>
                <td bgcolor="#ffffff">
                    <ajax:ajaxpanel id="Ajaxpanel3" runat="server">
                        <div style="overflow:auto;height:200px;width:800px">
                        &nbsp;<asp:GridView ID="GridPendingApprovalSDNADD" runat="server" AutoGenerateColumns="False" BackColor="White"
                            BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="Add_num"
                            ForeColor="Black" GridLines="Vertical">
                            <FooterStyle BackColor="#CCCC99" />
                            <Columns>
                                <asp:BoundField DataField="Ent_num" HeaderText="Ent_num" SortExpression="Ent_num" />
                                <asp:BoundField DataField="Add_num" HeaderText="Add_num" ReadOnly="True" SortExpression="Add_num" />
                                <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                                <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
                                <asp:BoundField DataField="Country" HeaderText="Country" SortExpression="Country" />
                                <asp:BoundField DataField="Add_remarks" HeaderText="Add_remarks" SortExpression="Add_remarks" />
                            </Columns>
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                        &nbsp;
                        </div>
                    </ajax:AjaxPanel></td>
            </tr>
            
            <tr>
                <td bgcolor="#ffffff">
                </td>
            </tr>		
			</table>
		<table id="TableNoPendingApproval" runat=server visible=false>
	        <tr>
	            <td>There is no pending approval for Verification List OFAC SDN List</td>
	        </tr>
	    </table>
		   <table cellpadding="0" cellspacing="0" border="0" width="100%">
		    <tr>
			<td nowrap style="height: 93px"><ajax:ajaxpanel id="AjaxPanel5" runat="server">&nbsp; &nbsp; </ajax:ajaxpanel> <asp:imagebutton id="ImageAccept" runat="server" CausesValidation="False" SkinID="AcceptButton"></asp:imagebutton></td>
			 <td style="height: 93px" nowrap="noWrap">
			 <ajax:ajaxpanel id="AjaxPanel1" runat="server">&nbsp; &nbsp; </ajax:ajaxpanel>
			    <asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton">
			    </asp:imagebutton></td>
            <td nowrap="nowrap" style="height: 93px" width="99%">
                <ajax:ajaxpanel id="Ajaxpanel14" runat="server">&nbsp; &nbsp;</ajax:AjaxPanel>
                    <asp:ImageButton ID="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton" /></td>
		    </tr>
            </table>
			

</asp:Content>
