﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MsSystemParameterView.aspx.vb" Inherits="MsSystemParameterView" %>


<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <script language="javascript" type="text/javascript">

        function hidePanel(objhide, objpanel, imgmin, imgmax) {
            document.getElementById(objhide).style.display = 'none';
            document.getElementById(objpanel).src = imgmax;
        }

		   

		
	</script>
	
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td>
					<img src="Images/blank.gif" width="5" height="1" />
				</td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td>
								<%----%>
							</td>
							<td width="99%" bgcolor="#FFFFFF">
								<img src="Images/blank.gif" width="1" height="1" />
							</td>
							<td bgcolor="#FFFFFF">
								<img src="Images/blank.gif" width="1" height="1" />
							</td>
						</tr>
					</table>
				</td>
				<td>
					<img src="Images/blank.gif" width="5" height="1" />
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
					<div id="divcontent" >
						<table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
									<img src="Images/blank.gif" width="20" height="100%" />
								</td>
								<td class="divcontentinside" bgcolor="#FFFFFF">
									&nbsp;<ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%" meta:resourcekey="AjaxPanel5Resource1"><table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
											height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
											border-bottom-style: none" bgcolor="#dddddd" width="100%">
											<tr bgcolor="#ffffff">
												<td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
													border-right-style: none; border-left-style: none; border-bottom-style: none">
													<strong>
														<img src="Images/dot_title.gif" width="17" height="17">
														<asp:Label ID="Label7" runat="server" Text="Master System Parameter - View"></asp:Label>
														<hr />
													</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ajax:AjaxPanel ID="AjxMessage" runat="server"
														meta:resourcekey="AjxMessageResource1">
														<asp:Label ID="LblMessage" background="images/validationbground.gif" runat="server"
															CssClass="validationok" Width="100%" meta:resourcekey="LblMessageResource1"></asp:Label>
													</ajax:AjaxPanel>
												</td>
											</tr>
										</table>
										<asp:Panel ID="Panel1" runat="server" DefaultButton="ImgBtnSearch" Width="100%" meta:resourcekey="Panel1Resource1">
											<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
												border="2">
												<tr>
													<td colspan="2" background="images/search-bar-background.gif" height="18" valign="middle"
														width="100%" align="right" style="text-align: left">
														<table cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td style="height: 16px">
																	<strong>Search box</strong> &nbsp;
																</td>
																<td style="height: 16px">
																	<a href="#" onclick="javascript:UpdateSearchBox()" title="click to minimize or maximize">
																		<img id="searchimage" src="images/search-bar-minimize.gif" border="0" style="width: 19px;
																			height: 14px"></a>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr id="searchbox">
													<td colspan="2" valign="top" width="98%" bgcolor="#ffffff">
														<table cellpadding="0" width="100%" border="0">
															<tr>
																<td valign="middle" nowrap>
																	<ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="885px" meta:resourcekey="AjaxPanel1Resource1">
																		<table style="height: 100%">
																			<tr>
																				<td colspan="3" style="height: 7px">
																				</td>
																			</tr>
																			<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="LabelMsSystemParameter_Name" runat="server" 
            text="Name"></asp:label>
        <span style="color: #ff0000">*</span>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td colspan="7" style="height: 24px" valign="top">
        <asp:textbox id="txtMsSystemParameter_Name" runat="server" cssclass="textBox" 
            tabindex="2" width="400px" tooltip="MsSystemParameter_Name" MaxLength="1000"></asp:textbox>
    </td>
</tr>

<tr bgcolor="#ffffff">
    <td style="width: 22px; height: 24px">
    </td>
    <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
        <asp:label id="LabelMsSystemParameter_Value" runat="server" 
            text="Value"></asp:label>
        <span style="color: #ff0000">*</span>
    </td>
    <td style="width: 6px; height: 24px" valign="top">
        :
    </td>
    <td colspan="7" style="height: 24px" valign="top">
        <asp:textbox id="txtMsSystemParameter_Value" runat="server" cssclass="textBox" 
            tabindex="2" width="400px" tooltip="MsSystemParameter_Value" MaxLength="50"></asp:textbox>
    </td>
</tr>
																			<tr bgcolor="#ffffff">
																				<td style="width: 22px; height: 24px">
																					&nbsp;</td>
																				<td bgcolor="#fff7e6" colspan="2" style="height: 24px" 
																					valign="top">
																					<asp:ImageButton ID="ImgBtnSearch" runat="server" 
																						ImageUrl="~/Images/button/search.gif" meta:resourcekey="ImgBtnSearchResource1" 
																						SkinID="SearchButton" TabIndex="3" />
																					<asp:ImageButton ID="ImgBtnClearSearch" runat="server" 
																						ImageUrl="~/Images/button/clearsearch.gif" 
																						meta:resourcekey="ImgBtnClearSearchResource1" />
																				</td>
																				<td style="width: 6px; height: 24px" valign="top">
																					&nbsp;</td>
																				<td style="height: 24px" valign="top">
																					&nbsp;</td>
																			</tr>
																			<tr>
																				<td colspan="2">
																					&nbsp;</td>
																				<td colspan="2">
																					&nbsp;</td>
																			</tr>
																		</table>
																		&nbsp;&nbsp;&nbsp;
																	</ajax:AjaxPanel>
																</td>
															</tr>
														</table>
														<ajax:AjaxPanel ID="AjaxPanel3" runat="server" meta:resourcekey="AjaxPanel3Resource1">
															<asp:CustomValidator ID="CvalPageErr" runat="server" Display="None" meta:resourcekey="CvalPageErrResource1"></asp:CustomValidator></ajax:AjaxPanel>
														<ajax:AjaxPanel ID="Ajaxpanel2" runat="server" meta:resourcekey="Ajaxpanel2Resource1">
															<asp:CustomValidator ID="CvalHandleErr" runat="server" ValidationGroup="handle" Display="None"
																meta:resourcekey="CvalHandleErrResource1"></asp:CustomValidator></ajax:AjaxPanel>
													</td>
												</tr>
											</table>
										</asp:Panel>
										<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
											border="2">
											<tr>
												<td bgcolor="#ffffff">
													<ajax:AjaxPanel ID="AjaxPanel4" runat="server" meta:resourcekey="AjaxPanel4Resource1">
														<asp:DataGrid ID="GridMsUserView" runat="server" AllowCustomPaging="True" AutoGenerateColumns="False"
															Font-Size="XX-Small" CellPadding="4" SkinID="gridview" AllowPaging="True"
															Width="100%" GridLines="Vertical" AllowSorting="True" ForeColor="Black" Font-Bold="False"
															Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
															HorizontalAlign="Left" meta:resourcekey="GridMsUserViewResource1" BackColor="White" BorderColor="#DEDFDE" 
                                                            BorderStyle="None" BorderWidth="1px">
															<FooterStyle BackColor="#CCCC99"></FooterStyle>
															<SelectedItemStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedItemStyle>
															<PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" 
                                                                Visible="False"></PagerStyle>
															<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
															<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" BackColor="#F7F7DE"></ItemStyle>
															<HeaderStyle Width="200px" Font-Bold="False" Wrap="False" HorizontalAlign="Left"
																VerticalAlign="Middle" BackColor="#6B696B" ForeColor="White" Font-Italic="False" Font-Overline="False" 
                                                                Font-Strikeout="False" Font-Underline="False"></HeaderStyle>
															<Columns>
																<asp:TemplateColumn>
																	<ItemTemplate>
																		<asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" meta:resourcekey="CheckBoxExporttoExcelResource1">
																		</asp:CheckBox>
																	</ItemTemplate>
																	<HeaderStyle Width="10px" />
																</asp:TemplateColumn>
																<asp:BoundColumn DataField="Pk_MsSystemParameter_Id" Visible="False"></asp:BoundColumn>
																<asp:BoundColumn HeaderText="No">
																	<HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
																		Font-Underline="False" ForeColor="White" Width="30px"  />
																</asp:BoundColumn>
																<asp:BoundColumn DataField="MsSystemParameter_Name" HeaderText="Name" SortExpression="MsSystemParameter_Name asc"></asp:BoundColumn>
                                                                <asp:BoundColumn DataField="MsSystemParameter_Value" HeaderText="Value" SortExpression="MsSystemParameter_Value asc"></asp:BoundColumn>
															
															<asp:HyperLinkColumn Text="Edit" HeaderStyle-Width="50" DataNavigateUrlField="Pk_MsSystemParameter_Id"
																DataNavigateUrlFormatString="SystemParameterEdit.aspx?pkedit={0}">
																<HeaderStyle Width="50px" />
															</asp:HyperLinkColumn>

															</Columns>
														</asp:DataGrid>
														<br />
														<br />
														<asp:Label ID="LabelNoRecordFound" runat="server" Text="No record match with your criteria"
															CssClass="text" Visible="False" meta:resourcekey="LabelNoRecordFoundResource1"></asp:Label></ajax:AjaxPanel>
												</td>
											</tr>
											<tr>
												<td style="background-color: #ffffff">
													<table cellpadding="0" cellspacing="0" border="0" width="100%">
														<tr>
															<td nowrap>
																<ajax:AjaxPanel ID="AjaxPanel6" runat="server" meta:resourcekey="AjaxPanel6Resource1">
																	&nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True"
																		meta:resourcekey="CheckBoxSelectAllResource1" />
																	&nbsp;
																</ajax:AjaxPanel>
															</td>
															<td style="width: 92%">
																&nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" ajaxcall="none" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
																	runat="server" meta:resourcekey="LinkButtonExportExcelResource1">Export 
				to Excel</asp:LinkButton>&nbsp;
																<asp:LinkButton ID="lnkExportAllData" runat="server" ajaxcall="none" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
																	Text="Export All to Excel" meta:resourcekey="lnkExportAllDataResource1"></asp:LinkButton>
															</td>
															<td width="99%" style="text-align: right">
																&nbsp;</td>
															<td align="right" nowrap>
																&nbsp;
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td bgcolor="#ffffff">
													<table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
														bgcolor="#ffffff" border="2">
														<tr class="regtext" align="center" bgcolor="#dddddd">
															<td valign="top" align="left" width="50%" bgcolor="#ffffff" style="height: 99px">
																Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel7" runat="server" meta:resourcekey="AjaxPanel7Resource1"><asp:Label
																	ID="PageCurrentPage" runat="server" CssClass="regtext" meta:resourcekey="PageCurrentPageResource1">0</asp:Label>&nbsp;of&nbsp;
																	<asp:Label ID="PageTotalPages" runat="server" CssClass="regtext" meta:resourcekey="PageTotalPagesResource1">0</asp:Label></ajax:AjaxPanel>
															</td>
															<td valign="top" align="right" width="50%" bgcolor="#ffffff" style="height: 99px">
																Total Records&nbsp;
																<ajax:AjaxPanel ID="AjaxPanel8" runat="server" meta:resourcekey="AjaxPanel8Resource1">
																	<asp:Label ID="PageTotalRows" runat="server" meta:resourcekey="PageTotalRowsResource1">0</asp:Label></ajax:AjaxPanel>
															</td>
														</tr>
													</table>
													<table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
														bgcolor="#ffffff" border="2">
														<tr bgcolor="#ffffff">
															<td class="regtext" valign="middle" align="left" colspan="11" height="7">
																<hr color="#f40101" noshade size="1">
															</td>
														</tr>
														<tr>
															<td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
																Go to page
															</td>
															<td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
																<font face="Verdana, Arial, Helvetica, sans-serif" size="1">
																	<ajax:AjaxPanel ID="AjaxPanel9" runat="server" meta:resourcekey="AjaxPanel9Resource1">
																		<asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"
																			meta:resourcekey="TextGoToPageResource1"></asp:TextBox></ajax:AjaxPanel>
																</font>
															</td>
															<td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
																<ajax:AjaxPanel ID="AjaxPanel10" runat="server" meta:resourcekey="AjaxPanel10Resource1">
																	<asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton" ImageUrl="~/Images/button/go.gif"
																		meta:resourcekey="ImageButtonGoResource1"></asp:ImageButton></ajax:AjaxPanel>
															</td>
															<td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
																<img height="5" src="images/first.gif" width="6">
															</td>
															<td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
																<ajax:AjaxPanel ID="AjaxPanel11" runat="server" meta:resourcekey="AjaxPanel11Resource1">
																	<asp:LinkButton ID="First" runat="server" CssClass="regtext" CommandName="First"
																		OnCommand="PageNavigate" meta:resourcekey="LinkButtonFirstResource1">First</asp:LinkButton></ajax:AjaxPanel>
															</td>
															<td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
																<img height="5" src="images/prev.gif" width="6">
															</td>
															<td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
																<ajax:AjaxPanel ID="AjaxPanel12" runat="server" meta:resourcekey="AjaxPanel12Resource1">
																	<asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
																		OnCommand="PageNavigate" meta:resourcekey="LinkButtonPreviousResource1">Previous</asp:LinkButton></ajax:AjaxPanel>
															</td>
															<td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
																<ajax:AjaxPanel ID="AjaxPanel13" runat="server" meta:resourcekey="AjaxPanel13Resource1">
																	<a class="pageNav" href="#">
																		<asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
																			OnCommand="PageNavigate" meta:resourcekey="LinkButtonNextResource1">Next</asp:LinkButton></a></ajax:AjaxPanel>
															</td>
															<td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
																<img height="5" src="images/next.gif" width="6">
															</td>
															<td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
																<ajax:AjaxPanel ID="AjaxPanel16" runat="server" meta:resourcekey="AjaxPanel16Resource1">
																	<asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
																		OnCommand="PageNavigate" meta:resourcekey="LinkButtonLastResource1">Last</asp:LinkButton></ajax:AjaxPanel>
															</td>
															<td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
																<img height="5" src="images/last.gif" width="6">
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</ajax:AjaxPanel>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td>
					<ajax:AjaxPanel ID="AjaxPanel14" runat="server" meta:resourcekey="AjaxPanel14Resource1">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td align="left" background="Images/button-bground.gif" valign="middle">
									<img height="1" src="Images/blank.gif" width="5" />
								</td>
								<td align="left" background="Images/button-bground.gif" valign="middle">
									<img height="15" src="images/arrow.gif" width="15" />&nbsp;
								</td>
								<td background="Images/button-bground.gif">
									&nbsp;
								</td>
								<td background="Images/button-bground.gif">
									&nbsp;
								</td>
								<td background="Images/button-bground.gif">
									&nbsp;
								</td>
								<td background="Images/button-bground.gif" width="99%">
									<img height="1" src="Images/blank.gif" width="1" />
								</td>
								<td>
									<%----%>
								</td>
							</tr>
						</table>
						<asp:CustomValidator ID="CustomValidator1" runat="server" Display="None" ValidationGroup="handle"
							meta:resourcekey="CustomValidator1Resource1"></asp:CustomValidator><asp:CustomValidator
								ID="CustomValidator2" runat="server" Display="None" meta:resourcekey="CustomValidator2Resource1"></asp:CustomValidator></ajax:AjaxPanel>
				</td>
			</tr>
		</table>
</asp:Content>

