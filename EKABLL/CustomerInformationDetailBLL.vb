﻿Public Class CustomerInformationDetailBLL
    Implements IDisposable

    Public Shared Function IsAllowedAccessCCTab(currentGroupNameParam As String) As Boolean

        Try
            Dim lstAllowed As List(Of String) = GetAllCCGroupUserAccess()

            For Each groupName As String In lstAllowed

                If groupName = currentGroupNameParam Then
                    Return True
                End If

            Next

            Return False

        Catch
            Throw
        End Try

    End Function
     
    Private Shared Function GetAllCCGroupUserAccess() As List(Of String)

        Try
            Dim dataSet As DataSet = Sahassa.AML.Commonly.ExecuteDataset("SELECT xx.GroupName AS [AllowedUserGroupName] FROM CreditCardDataAccess AS cc INNER JOIN [Group] AS xx ON cc.AllowedUserGroup = xx.GroupID WHERE cc.Active = '1'")
            Dim result As New List(Of String)

            If dataSet.Tables.Count = 0 Then Return New List(Of String)

            For Each currentRow As DataRow In dataSet.Tables(0).Rows
                result.Add(currentRow("AllowedUserGroupName").ToString())
            Next

            Return result

        Catch
            Throw
        End Try

    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
