﻿Public Class CustomTaskListBLL
    Implements IDisposable
     
    Public Shared Function GetAllCustomTaskListModule(userIDParam As String) As List(Of CustomTaskListData)

        Try
            Dim dataSet As DataSet = Sahassa.AML.Commonly.ExecuteDataset("SELECT PK_CustomTaskList_ID, QuerySQL, QueryUser, Name FROM CustomTaskList where active=1")
            Dim dataSetValidUser As DataSet
            Dim newData As CustomTaskListData
            Dim result As New List(Of CustomTaskListData)

            If dataSet.Tables.Count = 0 Then Return New List(Of CustomTaskListData)

            For Each currentRow As DataRow In dataSet.Tables(0).Rows
                
                newData = New CustomTaskListData()

                '===========================================================

                dataSetValidUser = Sahassa.AML.Commonly.ExecuteDataset(currentRow("QueryUser"))

                If dataSetValidUser.Tables.Count = 0 OrElse dataSetValidUser.Tables(0).Rows.Count = 0 Then
                    Return New List(Of CustomTaskListData)
                End If


                For Each row As DataRow In dataSetValidUser.Tables(0).Select("UserID='" & Sahassa.AML.Commonly.SessionUserId & "'")

                    Dim strsql As String = currentRow("QuerySQL")
                    strsql = strsql.ToLower.Replace("@userid", Sahassa.AML.Commonly.SessionUserId)
                    Dim dscek As DataSet = Sahassa.AML.Commonly.ExecuteDataset(strsql)


                    'Apakah user yang login sesuai dengan hasil dari Query untuk user penerima ? 
                    If row("UserID").ToString = userIDParam Then
                        If dscek.Tables(0).Rows.Count > 0 Then

                            With newData
                                .CustomTaskListID = currentRow("PK_CustomTaskList_ID").ToString()
                                .QuerySQL = currentRow("QuerySQL").ToString()
                                .QueryUser = currentRow("QueryUser").ToString()
                                .Name = currentRow("Name").ToString()
                            End With

                            result.Add(newData)
                        End If
                    End If

                Next

            Next

            Return result

        Catch
            Throw
        End Try

    End Function
    
    'Public Shared Function IsSQLReturnValue(sqlParam As String, userIDParam As String) As Integer

    '    Try
    '        Dim realSQL As String = sqlParam.Replace("@UserID", userIDParam)
    '        Dim dataSet As DataSet = Sahassa.AML.Commonly.ExecuteDataset(realSQL)

    '        If dataSet.Tables.Count = 0 Then Return 0

    '        Return dataSet.Tables(0).Rows.Count

    '    Catch
    '        Throw
    '    End Try

    '    'Try
    '    '    Using trans As TransactionManager = New TransactionManager(Data    Repository.ConnectionStrings("netTiersConnectionString").ConnectionString)

    '    '        Try
    '    '            Dim dataReader As SqlDataReader
    '    '            Dim totalData As Integer
                    
    '    '            trans.BeginTransaction()

    '    '            Using cmd As SqlCommand = Sahassa.AML.Commonly.GetSQLCommand(sqlParam)
    '    '                dataReader = cmd.ExecuteReader()
                        
    '    '                If Not dataReader.HasRows() Then Return 0

    '    '                While dataReader.Read()
    '    '                    totalData += 1
    '    '                End While
                        
    '    '            End Using

    '    '            trans.Commit()

    '    '            Return totalData
    '    '        Catch ex As Exception
    '    '            trans.Rollback()
    '    '            Throw
    '    '        End Try

    '    '    End Using
    '    'Catch
    '    '    Throw
    '    'End Try
        
    'End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
