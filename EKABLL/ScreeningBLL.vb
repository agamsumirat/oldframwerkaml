﻿Imports System.Data.SqlClient
Imports System.IO
Imports EkaDataNettier.Data
Imports EkaDataNettier.Entities
Imports Excel
Imports NPOI.HSSF.UserModel

Public Class ScreeningBLL

    Shared Function Getrelatetypeidbyname(strrelatedtypename As String) As Integer

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_GetRelatedTypeIDByname")
            objCommand.Parameters.Add(New SqlParameter("@relatedtypename", strrelatedtypename))
            Dim strresult As Integer = DataRepository.Provider.ExecuteScalar(objCommand)
            Return strresult
        End Using

    End Function

    Shared Function GetBulkSearchingData(struserid As String) As Data.DataTable

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_GetCustomerScreeningRequest")
            objCommand.Parameters.Add(New SqlParameter("@userid", struserid))

            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using
    End Function

    Shared Function GetRelationcustomerScreening(strverificationID As String) As Data.DataTable

        Dim sql As String = "SELECT RefId, Relation_Customer_ID, Relation_Category_ID, Relation_Category_Name, RelationTypeName, Relation_Customer_Name, Nationality 
                                FROM vw_AML_WebAPI_MappingRelationTableWatchList WHERE VerificationListId=" & strverificationID

        Dim dataset As DataSet = Sahassa.AML.Commonly.ExecuteDataset(sql)

        If dataset.Tables.Count = 0 Then Return New DataTable

        Return dataset.Tables(0)

        ' Using objCommand As SqlCommand = Sahassa.AML.Commonly.ExecuteDataset("SELECT CategoryName, Relation_Customer_Name, Nationality FROM vw_AML_WebAPI_MappingRelationTableWatchList WHERE VerificationListId=" & struserid)
        'objCommand.Parameters.Add(New SqlParameter("@userid", struserid))

        '    Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        'End Using
    End Function

    Shared Function GenerateFileExcel(pk As Long) As Byte()
        Dim Ishavefile As Boolean
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_IsHaveFile")
            objCommand.Parameters.Add(New SqlParameter("@pkrequest", pk))
            Ishavefile = DataRepository.Provider.ExecuteScalar(objCommand)
        End Using

        If Not Ishavefile Then
            Dim objtblresult As New Data.DataTable
            Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_GetResultBulkBypkRequest")
                objCommand.Parameters.Add(New SqlParameter("@pkrequest", pk))
                objtblresult = DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
            End Using
            Dim objbyteresult As Byte() = WriteExcelWithNPOI(objtblresult)
            Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_UpdateFileContentRequest")

                objCommand.Parameters.Add(New SqlParameter("@pkrequest", pk))
                objCommand.Parameters.Add(New SqlParameter("@file", objbyteresult))
                DataRepository.Provider.ExecuteNonQuery(objCommand)
            End Using
            Return objbyteresult
        Else



            Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_GetFilebulk")
                objCommand.Parameters.Add(New SqlParameter("@pkrequest", pk))
                Dim fileresult As Byte() = DataRepository.Provider.ExecuteScalar(objCommand)
                Return fileresult
            End Using


        End If



    End Function

    Shared Function AcceptJudgement(PK_Aml_Customer_Judgement_ID As Long, struserid As String) As Boolean

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_AcceptJudgement")
            objCommand.Parameters.Add(New SqlParameter("@pkamlcustomerjudgementid", PK_Aml_Customer_Judgement_ID))
            objCommand.Parameters.Add(New SqlParameter("@useridapprover", struserid))

            DataRepository.Provider.ExecuteNonQuery(objCommand)
        End Using

        Return true
    End Function

    Shared Function WriteExcelWithNPOI(ByVal dt As DataTable) As Byte()
        ' dll refered NPOI.dll and NPOI.OOXML

        Dim workbook As HSSFWorkbook
        workbook = New HSSFWorkbook()

        Dim sheet1 As HSSFSheet = workbook.CreateSheet("Sheet Result")

        'make a header row
        Dim row1 As NPOI.SS.UserModel.Row = sheet1.CreateRow(0)

        Dim j As Integer
        For j = 0 To dt.Columns.Count - 1
            Dim cell As NPOI.SS.UserModel.Cell = row1.CreateCell(j)

            Dim columnName As String = dt.Columns(j).ToString()
            cell.SetCellValue(columnName)
        Next

        'loops through data
        Dim i As Integer
        For i = 0 To dt.Rows.Count - 1
            Dim row As NPOI.SS.UserModel.Row = sheet1.CreateRow(i + 1)

            For k As Integer = 0 To dt.Columns.Count - 1

                Dim cell As NPOI.SS.UserModel.Cell = row.CreateCell(k)
                Dim columnName As String = dt.Columns(k).ToString()
                cell.SetCellValue(dt.Rows(i)(columnName).ToString())
            Next
        Next

        Using exportData As New MemoryStream
            workbook.Write(exportData)
            Return exportData.GetBuffer()

        End Using

    End Function

    Shared Function rejectJudgement(PK_Aml_Customer_Judgement_ID As Long, struserid As String) As Boolean

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_RejectJudgement")
            objCommand.Parameters.Add(New SqlParameter("@pkamlcustomerjudgementid", PK_Aml_Customer_Judgement_ID))
            objCommand.Parameters.Add(New SqlParameter("@useridapprover", struserid))

            DataRepository.Provider.ExecuteNonQuery(objCommand)
        End Using

        Return True
    End Function



    Shared Function saveScreening(objresult As Data.DataTable) As Boolean

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_SaveJudgement")
            objCommand.Parameters.Add(New SqlParameter("@pkamlcustomerjudgementid", objresult.Rows(0).Item("PK_Aml_Customer_Judgement_ID")))
            objCommand.Parameters.Add(New SqlParameter("@bjudgementresult", objresult.Rows(0).Item("JudgementResult")))
            objCommand.Parameters.Add(New SqlParameter("@judgementby", objresult.Rows(0).Item("JudgementBy")))
            objCommand.Parameters.Add(New SqlParameter("@judgementcomment", objresult.Rows(0).Item("JudgementComment")))

            DataRepository.Provider.ExecuteNonQuery(objCommand)
        End Using

        Return True

    End Function

    Shared Function GetCountWebAPIScreeningUnjudged(struserid As String) As Integer

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_GetCountJudgementWebAPI")
            objCommand.Parameters.Add(New SqlParameter("@userid", struserid))
            Return DataRepository.Provider.ExecuteScalar(objCommand)
        End Using

    End Function

    Shared Function GetcountJudgement(struserid As String) As Integer
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_GetCountJudgement")
            objCommand.Parameters.Add(New SqlParameter("@userid", struserid))
            Return DataRepository.Provider.ExecuteScalar(objCommand)
        End Using
    End Function
    Shared Function GetWatchListData(verificationlistid, pkscreeningid) As Data.DataTable
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_GetwatlistByID")
            objCommand.Parameters.Add(New SqlParameter("@verificationilistID", verificationlistid))
            objCommand.Parameters.Add(New SqlParameter("@pkscreeningid", pkscreeningid))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using

    End Function

    Shared Function GetJudgetmentApproval(userid As String) As Data.DataTable

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_GetJudgementApproval")
            objCommand.Parameters.Add(New SqlParameter("@userid", userid))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using
    End Function

    Shared Function GetUserAuditInfo(userid As String) As Data.DataTable

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_GetUserAuditInfoResult")
            objCommand.Parameters.Add(New SqlParameter("@userid", userid))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using
    End Function
    Shared Function GetSystemParameterByPK(IDParam As Integer) As Data.DataTable
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_GetSystemParameterByPK")
            objCommand.Parameters.Add(New SqlParameter("@IDParam", IDParam))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using
    End Function

    Shared Function SetUserAuditInfo(userid As String, loginsuccess As DateTime, loginfail As DateTime, lastchangepassword As DateTime)

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_SetUserAuditInfo")
            objCommand.Parameters.Add(New SqlParameter("@userid", userid))
            objCommand.Parameters.Add(New SqlParameter("@loginsucess", loginsuccess))
            objCommand.Parameters.Add(New SqlParameter("@loginfail", loginfail))
            objCommand.Parameters.Add(New SqlParameter("@lastchange", lastchangepassword))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using
    End Function

    Shared Function SetUserAuditInfoUser(userid As String)

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_SetUserAuditInfo")
            objCommand.Parameters.Add(New SqlParameter("@userid", userid))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using
    End Function

    'Shared Function GetSystemParameterByPK(idParam As Integer) As 



    Shared Sub SetUserAuditInfoLoginNewframework(userid As String, action As String, ipaddress As String, ket As String, mode As Integer)

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_InsertAuditTrailLoginNewFramework")
            objCommand.Parameters.Add(New SqlParameter("@userid", userid))
            objCommand.Parameters.Add(New SqlParameter("@action", action))
            objCommand.Parameters.Add(New SqlParameter("@ipaddress", ipaddress))
            objCommand.Parameters.Add(New SqlParameter("@ket", ket))
            objCommand.Parameters.Add(New SqlParameter("@mode", mode))
            DataRepository.Provider.ExecuteNonQuery(objCommand)
        End Using
    End Sub

    Shared Function getAMLCustomerJudgementDistinctCIF(cifno As String) As Data.DataTable
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_GetcustomerJudgementdistinctcif")
            objCommand.Parameters.Add(New SqlParameter("@cifno", cifno))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using

    End Function

    Shared Function getAMLWatchlistJudgement(cifno As String) As Data.DataTable

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_GetWatchListJudgement")
            objCommand.Parameters.Add(New SqlParameter("@cifno", cifno))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using
    End Function


    Shared Function getAMLCustomerJudgement(cifno As String) As Data.DataTable

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_GetcustomerJudgement")
            objCommand.Parameters.Add(New SqlParameter("@cifno", cifno))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using
    End Function

    Shared Function getValue(ByVal strdata As NPOI.SS.UserModel.Cell) As String
        If strdata Is Nothing Then
            Return ""
        End If
        Select Case strdata.CellType
            Case NPOI.SS.UserModel.CellType.BLANK
                Return ""
            Case NPOI.SS.UserModel.CellType.BOOLEAN
                Return strdata.BooleanCellValue
            Case NPOI.SS.UserModel.CellType.ERROR
                Return strdata.ErrorCellValue
            Case NPOI.SS.UserModel.CellType.FORMULA
                Return strdata.CellFormula.ToLower
            Case NPOI.SS.UserModel.CellType.NUMERIC
                Return strdata.NumericCellValue
            Case NPOI.SS.UserModel.CellType.STRING
                Return strdata.StringCellValue
            Case NPOI.SS.UserModel.CellType.Unknown
                Return strdata.StringCellValue
            Case Else
                Return ""
        End Select
    End Function

    Shared Function MakeMappingColumns(objdt As DataTable) As Dictionary(Of String, String)
        Dim mappingColumns As New Dictionary(Of String, String)()

        mappingColumns.Add("Nama/Alias", "Nama")
        mappingColumns.Add("Date Of Birth(dd-MM-yyyy)", "DOB")
        mappingColumns.Add("Nationality", "Nationality")
        mappingColumns.Add("userid", "userid")
        mappingColumns.Add("KeteranganError", "KeteranganError")
        mappingColumns.Add("recordnumber", "recordnumber")

        Return mappingColumns
    End Function

    Shared Function ProcessExcelBaru(ByVal strpath As String, ByVal strpathHasil As String) As Long
        Using stream As FileStream = File.Open(strpath, FileMode.Open, FileAccess.Read)
            Using excelReader As IExcelDataReader = ExcelReaderFactory.CreateBinaryReader(stream)
                excelReader.IsFirstRowAsColumnNames = True
                Using DSexcelTemp As DataSet = excelReader.AsDataSet()

                    If DSexcelTemp.Tables(0).Columns(0).ColumnName <> "Nama/Alias" Then
                        Throw New Exception("First Column Must Nama/Alias.")
                    End If
                    If DSexcelTemp.Tables(0).Columns(1).ColumnName <> "Date Of Birth(dd-MM-yyyy)" Then
                        Throw New Exception("Second Column Must Date Of Birth(dd-MM-yyyy).")
                    End If

                    If DSexcelTemp.Tables(0).Columns(2).ColumnName <> "Nationality" Then
                        Throw New Exception("Second Column Must Nationality.")
                    End If

                    Dim objreader As New Data.DataTableReader(DSexcelTemp.Tables(0))
                    Dim objnewdt As New Data.DataTable

                    Dim auto As DataColumn = New DataColumn("recordnumber", GetType(Integer))
                    auto.AllowDBNull = False
                    auto.AutoIncrement = True
                    auto.AutoIncrementSeed = 1
                    auto.AutoIncrementStep = 1

                    objnewdt.Columns.Add(auto)

                    Dim autouserid As DataColumn = New DataColumn("userid", GetType(String))
                    autouserid.DefaultValue = Sahassa.AML.Commonly.SessionUserId
                    autouserid.AllowDBNull = False
                    objnewdt.Columns.Add(autouserid)

                    Dim colketerror As DataColumn = New DataColumn("KeteranganError", GetType(String))
                    colketerror.DefaultValue = ""
                    colketerror.AllowDBNull = False
                    objnewdt.Columns.Add(colketerror)

                    objnewdt.Load(objreader)
                    EkaDataNettier.Data.DataRepository.Provider.ExecuteNonQuery(CommandType.Text, "DELETE FROM AML_Screening_Customer_Bulk WHERE userid='" & Sahassa.AML.Commonly.SessionUserId & "'")

                    Using sqlBulk As New SqlBulkCopy(EkaDataNettier.Data.DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
                        sqlBulk.BatchSize = System.Configuration.ConfigurationManager.AppSettings("BatchSize")
                        sqlBulk.BulkCopyTimeout = System.Configuration.ConfigurationManager.AppSettings("SQLCommandTimeout")
                        sqlBulk.DestinationTableName = "AML_Screening_Customer_Bulk"
                        Dim mapColumns As Dictionary(Of String, String) = MakeMappingColumns(objnewdt)
                        If mapColumns IsNot Nothing Then
                            For Each mapping In mapColumns
                                sqlBulk.ColumnMappings.Add(mapping.Key, mapping.Value)
                            Next mapping

                        End If
                        sqlBulk.WriteToServer(objnewdt)
                    End Using

                    Dim strResult As String = ""
                    Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_ValidateBulk")
                        objCommand.Parameters.Add(New SqlParameter("@userid", Sahassa.AML.Commonly.SessionUserId))
                        strResult = DataRepository.Provider.ExecuteScalar(objCommand)
                    End Using

                    If strResult <> "" Then
                        Throw New Exception("There is Invalid Data in Excel with error :" & strResult)
                    End If

                    'bersih
                    Dim pkrequest As Long = 0
                    Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_ProcessScreeningbulk")
                        objCommand.Parameters.Add(New SqlParameter("@userid", Sahassa.AML.Commonly.SessionUserId))
                        pkrequest = DataRepository.Provider.ExecuteScalar(objCommand)
                    End Using

                    'Menggunakan SSIS untuk mencari data screening
                    'SaveEOD(Now, 3, "3")
                    SaveEOD(Now, 41, "43")

                    'Dim thread As New Threading.Thread(AddressOf DoProcessScreeningBulk)
                    'thread.Start(pkrequest)

                    'Menggunakan SP untuk mencari data screening
                    'DoProcessScreeningBulk(pkrequest)

                    Return pkrequest
                End Using
            End Using
        End Using

    End Function

    'Private Shared Sub RunStoredProcedureByPartition(nameToSearchParam As String, dobToSearchParam As Date?, nationalityToSearchParam As String,
    '                                     requestScreeningPKParam As Long)

    '    Dim builder As New Text.StringBuilder()
    '    Dim thread As Threading.Thread

    '    'Dim uniqueID As String = Guid.NewGuid().ToString().ToUpper()
    '    Dim list_thread As New List(Of Threading.Thread)
    '    Dim var_loop As Integer = CInt(GetSystemParameterByValueByID(20003))
    '    Dim fix_prm As String
    '    Dim prm As String

    '    With builder
    '        .Append("#")
    '        .Append(nameToSearchParam)
    '        .Append("#")
    '        .Append(dobToSearchParam)
    '        .Append("#")
    '        .Append(nationalityToSearchParam)
    '        .Append("#")
    '        .Append(requestScreeningPKParam)
    '    End With

    '    fix_prm = builder.ToString()

    '    For index = 1 To var_loop
    '        builder.Length = 0

    '        builder.Insert(0, index.ToString())
    '        builder.Append(fix_prm)

    '        prm = builder.ToString()

    '        thread = New Threading.Thread(Sub() SPCall(prm))
    '        list_thread.Add(thread)
    '        thread.Start()

    '    Next

    '    For Each th As Threading.Thread In list_thread
    '        th.Join()
    '    Next

    'End Sub

    'Private Shared Sub SPCall(param As String)

    '    Dim list_param As String() = param.Split("#")

    '    Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_WebAPI_ScreeningCustomer_By_Split_Table")
    '        objCommand.Parameters.Add(New SqlParameter("@TableIndex", list_param(0)))
    '        objCommand.Parameters.Add(New SqlParameter("@Name", list_param(1)))
    '        objCommand.Parameters.Add(New SqlParameter("@DOB", list_param(2)))
    '        objCommand.Parameters.Add(New SqlParameter("@Nationality", list_param(3)))
    '        objCommand.Parameters.Add(New SqlParameter("@RequestScreeningPK", list_param(4)))

    '        DataRepository.Provider.ExecuteNonQuery(objCommand)
    '    End Using

    'End Sub

    'Shared Function processFileUPload(strpath As String) As Boolean
    '    Using objFs As New FileStream(strpath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
    '        Using objWB As HSSFWorkbook = New HSSFWorkbook(objFs, True)
    '            If objWB Is Nothing Then
    '                Throw New Exception("File is not valid")
    '            Else
    '                Using objsheet As HSSFSheet = objWB.GetSheet("Search Potensial Verification")
    '                    If objsheet Is Nothing Then
    '                        Throw New Exception("Sheet Search Potensial Verification is not exist.")
    '                    Else

    '                        If getValue(objsheet.GetRow(0).GetCell(0)).ToString.ToLower <> "nama/alias" Then
    '                            Throw New Exception("There is no Nama/Alias column </br>")
    '                        End If
    '                        If getValue(objsheet.GetRow(0).GetCell(1)).ToString.ToLower <> "date of birth(dd-mm-yyyy)" Then
    '                            Throw New Exception("Thre is no Date Of Birth(dd-MM-yyyy) column </br>")
    '                        End If
    '                        If getValue(objsheet.GetRow(0).GetCell(0)).ToString.ToLower <> "Nationality" Then
    '                            Throw New Exception("There is no Nationality column </br>")
    '                        End If

    '                        Dim intjmlrecord As Integer
    '                        intjmlrecord = 1
    '                        While (Not objsheet.GetRow(intjmlrecord) Is Nothing) AndAlso (getValue(objsheet.GetRow(intjmlrecord).GetCell(0)).Trim.ToString <> "")
    '                            intjmlrecord += 1
    '                        End While

    '                        Using objFsHasil As New FileStream(strpathHasil, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
    '                            Using objWBHasil As HSSFWorkbook = New HSSFWorkbook(objFsHasil, True)
    '                                Using objsheetHasil As HSSFSheet = objWBHasil.GetSheet("Search")

    '                                End Using
    '                            End Using
    '                        End Using

    '                    End If
    '                End Using

    '            End If
    '        End Using
    '    End Using

    'End Function
    Enum AMLScreening_RequestType
        Satuan = 1
        Upload
    End Enum

    Shared Sub SetUserAuditInfoSuccess(userid As String, successDate As Date)
        Try
            Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_SetUserAuditInfoSuccessLogin")
                objCommand.Parameters.Add(New SqlParameter("@userid", userid))
                objCommand.Parameters.Add(New SqlParameter("@loginsuccess", successDate))
                DataRepository.Provider.ExecuteNonQuery(objCommand)
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Shared Sub SetUserAuditInfoFail(userID As String, faildate As Date)
        Try
            Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_SetUserAuditInfoFailLogin")
                objCommand.Parameters.Add(New SqlParameter("@userid", userID))
                objCommand.Parameters.Add(New SqlParameter("@loginfail", faildate))
                DataRepository.Provider.ExecuteNonQuery(objCommand)
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Shared Sub SetUserAuditInfoChangePassword(userID As String, ChangePassword As Date)
        Try
            Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_SetUserAuditInfoLastChangePassword")
                objCommand.Parameters.Add(New SqlParameter("@userid", userID))
                objCommand.Parameters.Add(New SqlParameter("@loginfail", ChangePassword))
                DataRepository.Provider.ExecuteNonQuery(objCommand)
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Shared Function GetWorldcheckData(uid As String) As Data.DataTable

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_GetWorldcheckDataByUID")
            objCommand.Parameters.Add(New SqlParameter("@uid", uid))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using
    End Function

    Shared Function GetSystemParameterByValueByID(parameterid As Integer) As String

        Return DataRepository.Provider.ExecuteScalar(CommandType.Text, "SELECT spn.SettingValue  FROM SystemParameterNewFramework spn WHERE spn.PK_SystemParameter_ID=" & parameterid)

    End Function

    Shared Function GetWatchListData(verificationlistid As Long) As Data.DataTable

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_GetWatchListByID")
            objCommand.Parameters.Add(New SqlParameter("@verificationlistid", verificationlistid))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using

    End Function

    Shared Function GetScreeningResultByPk(intPk As Integer) As AML_Screening_Customer_Request_Detail
        Using objScreeningResult As TList(Of AML_Screening_Customer_Request_Detail) = DataRepository.AML_Screening_Customer_Request_DetailProvider.GetPaged("FK_AML_Screening_Customer_Request_ID=" & intPk, "", 0, Integer.MaxValue, 0)
            If objScreeningResult.Count > 0 Then
                Return objScreeningResult(0)
            Else
                Return Nothing
            End If
        End Using
    End Function

    Shared Function GetTlistScreeningResult(ByVal whereClause As String, ByVal orderBy As String, ByVal start As Integer, ByVal pageLength As Integer, ByRef count As Integer) As TList(Of AML_Screening_Customer_Request)
        Dim ObjScreeningResult As TList(Of AML_Screening_Customer_Request) = Nothing
        ObjScreeningResult = DataRepository.AML_Screening_Customer_RequestProvider.GetPaged(whereClause, orderBy, start, pageLength, count)
        Return ObjScreeningResult
    End Function

    Shared Function GetResult(intpk As Long) As Data.DataTable

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_GetResultScreening")
            objCommand.Parameters.Add(New SqlParameter("@pkrequestid", intpk))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using

    End Function

    Shared Function PotentialScreeningSatuan(strName As String, strDOB As Nullable(Of Date), strNationality As String) As Integer
        Try
            Dim retvalue As Integer
            Using OTrans As TransactionManager = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
                Try
                    OTrans.BeginTransaction()
                    Using objAML_Screening_Customer_Request As New AML_Screening_Customer_Request
                        With objAML_Screening_Customer_Request

                            .PK_AML_Screening_Customer_Request_ID = 0
                            .UserID = Sahassa.AML.Commonly.SessionUserId
                            .CreatedDate = Now
                            .FK_AML_Screening_Request_Type_Id = AMLScreening_RequestType.Satuan
                            .ProcessStatus = 0
                        End With

                        DataRepository.AML_Screening_Customer_RequestProvider.Save(OTrans, objAML_Screening_Customer_Request)

                        Using objAML_Screening_Customer_Request_Detail As New AML_Screening_Customer_Request_Detail
                            With objAML_Screening_Customer_Request_Detail
                                .PK_AML_Screening_Customer_Request_Detail_ID = 0
                                .FK_AML_Screening_Customer_Request_ID = objAML_Screening_Customer_Request.PK_AML_Screening_Customer_Request_ID
                                .Nama = strName
                                .DOB = strDOB
                                .Nationality = strNationality
                            End With

                            DataRepository.AML_Screening_Customer_Request_DetailProvider.Save(OTrans, objAML_Screening_Customer_Request_Detail)

                        End Using

                        retvalue = objAML_Screening_Customer_Request.PK_AML_Screening_Customer_Request_ID
                    End Using

                    OTrans.Commit()

                    'Menggunakan SSIS untuk mencari data screening
                    'SaveEOD(Now, 3, "3")
                    'SaveEOD(Now, 41, "43")
                    ' SaveEOD(Now, 18, "41")

                    'Menggunakan SP untuk mencari data screening
                    DoProcessScreening(strName, strDOB, strNationality, retvalue)

                    Return retvalue
                Catch ex As Exception
                    OTrans.Rollback()
                    Throw
                End Try
            End Using
            Return True
        Catch
            Throw
        End Try

    End Function

    Shared Function SaveEOD(datadate As Date, intprocessid As Long, strtasklist As String) As Boolean

        Dim cmd As New SqlCommand()

        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "usp_insertEODSchedulerManual"
        cmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings("SQLCommandTimeout")

        Dim paramtasklist = New Data.SqlClient.SqlParameter

        paramtasklist.ParameterName = "@Datadate"
        paramtasklist.DbType = DbType.DateTime
        paramtasklist.Value = datadate
        cmd.Parameters.Add(paramtasklist)

        Dim paramtasklist1 = New Data.SqlClient.SqlParameter

        paramtasklist1 = New Data.SqlClient.SqlParameter
        paramtasklist1.ParameterName = "@processid"
        paramtasklist1.DbType = DbType.Int64
        paramtasklist1.Value = intprocessid
        cmd.Parameters.Add(paramtasklist1)

        Dim paramtasklist2 = New Data.SqlClient.SqlParameter
        paramtasklist2 = New Data.SqlClient.SqlParameter
        paramtasklist2.ParameterName = "@userID"
        paramtasklist2.DbType = DbType.String
        paramtasklist2.Value = Sahassa.AML.Commonly.SessionUserId
        cmd.Parameters.Add(paramtasklist2)

        Dim paramtasklist3 = New Data.SqlClient.SqlParameter
        paramtasklist3 = New Data.SqlClient.SqlParameter
        paramtasklist3.ParameterName = "@taskid"
        paramtasklist3.DbType = DbType.String
        paramtasklist3.Value = strtasklist
        cmd.Parameters.Add(paramtasklist3)

        DataRepository.Provider.ExecuteNonQuery(cmd)

        Return true

    End Function

    Public Shared Function GetAllAMLCustomerJudgementWebAPI() As Data.DataTable

        Dim dt As New DataTable()

        Using objCommand As System.Data.SqlClient.SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_WebAPI_GetScreeningList")
            objCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@userID", Sahassa.AML.Commonly.SessionUserId))
            dt = EkaDataNettier.Data.DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using

        Return dt

    End Function

    Public Shared Function GetAllAMLCustomerJudgementWebAPICIF() As Data.DataTable

        Dim dt As New DataTable()

        Using objCommand As System.Data.SqlClient.SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_WebAPI_GetScreeningListCIF")
            objCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@userid", Sahassa.AML.Commonly.SessionUserId))
            dt = EkaDataNettier.Data.DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using

        Return dt

    End Function

    Public Shared Function GetAMLCustomerJudgementWebAPIByRequestID(requestID As String) As Data.DataTable

        Dim dt As New DataTable()

        Using objCommand As System.Data.SqlClient.SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_WebAPI_GetScreeningListByRequestID")
            objCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@requestID", requestID))
            dt = EkaDataNettier.Data.DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using

        Return dt

    End Function

    Public Shared Function GetAMLCustomerJudgementWebAPIByCIFNo(CIFNo As String) As Data.DataTable

        Dim dt As New DataTable()

        Using objCommand As System.Data.SqlClient.SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_WebAPI_GetScreeningListByCIFNo")
            objCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CIFNo", CIFNo))
            dt = EkaDataNettier.Data.DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using

        Return dt

    End Function

    Public Shared Function GetAMLScreeningResultDetailWebAPI(requestID As String) As Data.DataTable
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_WebAPI_GetWatchListData")
            objCommand.Parameters.Add(New SqlParameter("@requestID", requestID))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using

    End Function

    Public Shared Function GetAMLScreeningResultDetailWebAPIByCIF(CIFNo As String) As Data.DataTable
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_WebAPI_GetWatchListDataByCIF")
            objCommand.Parameters.Add(New SqlParameter("@CIFNo", CIFNo))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using

    End Function

    Public Shared Function GetAMLWatchListSuspectDataDetail(verificationlistid As Long) As Data.DataTable

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_WebAPI_GetWatchListSuspectDataDetail")
            objCommand.Parameters.Add(New SqlParameter("@verificationListID", verificationlistid))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using

    End Function

    Public Shared Function GetVerificationListMasterRelationData(verificationListIDParam As Long) As Data.DataTable

        Dim dataSet As DataSet
        Dim sql As String = "SELECT vlm.DisplayName, ISNULL(vlm.DateOfBirth, ''), DATEPART(YEAR, ISNULL(vlm.DateOfBirth, '')), 
                               vlm.Nationality, vlm.BirthPlace, vlt.ListTypeName, vlc.CategoryName, vlm.CustomRemark1, vlm.CustomRemark2, vlm.CustomRemark3, 
                               vlm.CustomRemark4, vlm.CustomRemark5 FROM VerificationList_Master vlm 
                             INNER JOIN VerificationListCategory vlc ON vlm.VerificationListCategoryId = vlc.CategoryID 
                             INNER JOIN VerificationListType vlt ON vlt.pk_Verification_List_Type_Id = vlm.VerificationListTypeId 
                             WHERE vlm.VerificationListId = @verificationListID;"

        Using command As SqlCommand = Sahassa.AML.Commonly.GetSQLCommand(sql)

            command.Parameters.Add(New SqlParameter("@verificationListID", verificationListIDParam))

            dataSet = DataRepository.Provider.ExecuteDataSet(command)

        End Using

        If dataSet Is Nothing OrElse dataSet.Tables.Count = 0 Then
            Return New DataTable()
        End If

        Return dataSet.Tables(0)

    End Function

    Public Shared Sub SaveAMLScreeningJudgementWebAPI(dataTable As Data.DataTable)

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_WebAPI_SaveScreeningJudgement")
            objCommand.Parameters.Add(New SqlParameter("@PK_AML_WebAPIScreening_Result_Detail_ID", dataTable.Rows(0).Item("PK_AML_WebAPIScreening_Result_Detail_ID")))
            objCommand.Parameters.Add(New SqlParameter("@judgementResult", dataTable.Rows(0).Item("JudgementResult")))
            objCommand.Parameters.Add(New SqlParameter("@judgementBy", dataTable.Rows(0).Item("JudgementBy")))
            objCommand.Parameters.Add(New SqlParameter("@judgementComment", dataTable.Rows(0).Item("JudgementComment")))

            DataRepository.Provider.ExecuteNonQuery(objCommand)
        End Using

    End Sub

    Public Shared Function GetAllAMLScreeningJudgementHistoryWebAPI() As DataTable

        Try
            Dim dataSet As DataSet = Sahassa.AML.Commonly.ExecuteDataset("SELECT * FROM vw_AML_WebAPI_CustomerJudgementScreeningHistory")
            
            Return dataSet.Tables(0)
        Catch
            Throw
        End Try

    End Function

    Public Shared Sub DoProcessScreening(nameToSearchParam As String, dobToSearchParam As Date?, nationalityToSearchParam As String,
                                         requestScreeningPKParam As Long)

        Dim dobvalue As Date? = Nothing

        If dobToSearchParam.HasValue Then
            dobvalue = dobToSearchParam
        End If

        'RunStoredProcedureByPartition(nameToSearchParam, dobvalue, nationalityToSearchParam, requestScreeningPKParam)

        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_ScreeningCustomer_New")
            objCommand.Parameters.Add(New SqlParameter("@Name", nameToSearchParam))
            objCommand.Parameters.Add(New SqlParameter("@DOB", dobvalue))
            objCommand.Parameters.Add(New SqlParameter("@Nationality", nationalityToSearchParam))
            objCommand.Parameters.Add(New SqlParameter("@RequestScreeningPK", requestScreeningPKParam))

            DataRepository.Provider.ExecuteNonQuery(objCommand)
        End Using

        'Hardcode, nanti diubah agar menggunakan sp
        Sahassa.AML.Commonly.ExecuteDataset("UPDATE AML_Screening_Customer_Request SET ProcessStatus = 1 
                                                            WHERE PK_AML_Screening_Customer_Request_ID = " & requestScreeningPKParam.ToString())

    End Sub

    Public Shared Sub DoProcessScreeningBulk(requestScreeningPKParam As Long)

        Dim dataset As DataSet = Sahassa.AML.Commonly.ExecuteDataset("SELECT * FROM AML_Screening_Customer_Request_Detail 
                                                            WHERE FK_AML_Screening_Customer_Request_ID = " & requestScreeningPKParam.ToString())
        Dim dt As DataTable = dataset.Tables(0)
        Dim nameData As String
        Dim dobData As Date?
        Dim nationalityData As String
        Dim dr As DataRow

        Dim builder As New Text.StringBuilder()

        For index = 0 To dt.Rows.Count - 1

            dr = dt.Rows(index)

            nameData = dr("Nama").ToString()

            If IsDBNull(dr("DOB")) Then
                dobData = Nothing
            Else
                dobData = dr("DOB")
            End If

            nationalityData = dr("Nationality").ToString()

            'RunStoredProcedureByPartition(nameData, dobData, nationalityData, requestScreeningPKParam)

            Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("AML_usp_ScreeningCustomer_New")
                objCommand.Parameters.Add(New SqlParameter("@Name", nameData))
                objCommand.Parameters.Add(New SqlParameter("@DOB", dobData))
                objCommand.Parameters.Add(New SqlParameter("@Nationality", nationalityData))
                objCommand.Parameters.Add(New SqlParameter("@RequestScreeningPK", requestScreeningPKParam))

                DataRepository.Provider.ExecuteNonQuery(objCommand)
            End Using

        Next

        With builder
            .Append("UPDATE AML_Screening_Customer_Request SET ProcessStatus = 1 WHERE PK_AML_Screening_Customer_Request_ID = ")
            .Append(requestScreeningPKParam.ToString())
        End With

        EkaDataNettier.Data.DataRepository.Provider.ExecuteNonQuery(CommandType.Text, builder.ToString())

    End Sub
    
End Class