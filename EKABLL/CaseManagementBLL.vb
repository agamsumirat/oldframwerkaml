﻿Imports System.Data.SqlClient
Imports EkaDataNettier.Data
Imports EkaDataNettier.Entities
Public Class CaseManagementBLL


    Public Shared Function CaseManagementtransactionNew(ByVal intFKCaseManagementID As Long) As String
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_GetMapCaseManagementTransactionNew")
            objCommand.Parameters.Add(New SqlParameter("@PKCaseManagementID", intFKCaseManagementID))
            Return DataRepository.Provider.ExecuteScalar(objCommand)
        End Using

    End Function


    Public Shared Function getnextworkflowstep(ByVal accountownerid As String, casealertdesc As String, vipcode As String, insidercode As String, segment As String, sbu As String, subsbu As String, rm As String, currentworkflowstep As String, previousproposeactionid As Integer, CurrentProposedActionid As Integer) As String
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_GetWorkflowNextStep")
            objCommand.Parameters.Add(New SqlParameter("@accountownerid", accountownerid))
            objCommand.Parameters.Add(New SqlParameter("@casealertdesc", casealertdesc))
            objCommand.Parameters.Add(New SqlParameter("@vipcode", vipcode))
            objCommand.Parameters.Add(New SqlParameter("@insidercode", insidercode))
            objCommand.Parameters.Add(New SqlParameter("@segment", segment))
            objCommand.Parameters.Add(New SqlParameter("@sbu", sbu))
            objCommand.Parameters.Add(New SqlParameter("@subsbu", subsbu))
            objCommand.Parameters.Add(New SqlParameter("@rm", rm))
            objCommand.Parameters.Add(New SqlParameter("@currentworkflowstep", currentworkflowstep))
            objCommand.Parameters.Add(New SqlParameter("@previousproposeactionid", previousproposeactionid))
            objCommand.Parameters.Add(New SqlParameter("@CurrentProposedActionid", CurrentProposedActionid))

            Return DataRepository.Provider.ExecuteScalar(objCommand)
        End Using

    End Function

    'ambil casemanagement proposed action 
    Public Shared Function GetCaseManagementProposedAction(ByVal intFKCaseManagementID As Long, strWorkflowStep As String) As DataTable
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_AML_GetSTRWorkflowDecisionProposedAction")
            objCommand.Parameters.Add(New SqlParameter("@PKCaseid", intFKCaseManagementID))
            objCommand.Parameters.Add(New SqlParameter("@workflowstep", strWorkflowStep))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)

        End Using

    End Function


End Class
