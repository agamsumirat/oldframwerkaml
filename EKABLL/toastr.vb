﻿
Public Class toastr
    Public Const SessionName As String = "toastr"

    Private t_Title As String
    Private t_Text As String
    Private t_Type As ToastTypes
    Private t_Position As ToastPositions
    Public Enum ToastTypes
        info
        success
        warning
        [error]
    End Enum
    Public Enum ToastPositions
        toast_top_right
        toast_bottom_right
        toast_bottom_left
        toast_top_left
        toast_top_full_width
        toast_bottom_full_width
        toast_top_center
        toast_bottom_center
        toast_center_center
    End Enum
    ''' <summary>
    ''' Show info toast at top right
    ''' </summary>
    ''' <param name="text">Text to be shown in the toast</param>
    ''' <remarks></remarks>
    Public Sub New(text As String)
        t_Text = text
        t_Title = String.Empty
        t_Type = ToastTypes.info
        t_Position = ToastPositions.toast_top_right
    End Sub
    ''' <summary>
    ''' Show toast at top right
    ''' </summary>
    ''' <param name="text">Text to be shown in the toast</param>
    ''' <param name="type">Type of Toast. Could be info, success, warning or errors.</param>
    ''' <remarks></remarks>
    Public Sub New(text As String, type As ToastTypes)
        t_Text = text
        t_Type = type.ToString
        t_Title = String.Empty
        t_Position = ToastPositions.toast_top_right
    End Sub
    ''' <summary>
    ''' Show info toast
    ''' </summary>
    ''' <param name="text">Text to be shown in the toast</param>
    ''' <param name="position">Position of Toast. Several options available</param>
    ''' <remarks></remarks>
    Public Sub New(text As String, position As ToastPositions)
        t_Text = text
        t_Type = ToastTypes.info
        t_Title = String.Empty
        t_Position = position
    End Sub
    ''' <summary>
    ''' Show toast
    ''' </summary>
    ''' <param name="text">Text to be shown in the toast</param>
    ''' <param name="type">Type of Toast. Could be info, success, warning or errors.</param>
    ''' <param name="position">Position of Toast. Several options available</param>
    ''' <remarks></remarks>
    Public Sub New(text As String, type As ToastTypes, position As ToastPositions)
        t_Text = text
        t_Type = type
        t_Position = position
        t_Title = String.Empty
    End Sub
    Public ReadOnly Property toastTitle As String
        Get
            Return t_Title
        End Get
    End Property
    Public ReadOnly Property toastText As String
        Get
            Return t_Text
        End Get
    End Property
    Public ReadOnly Property toastType As String
        Get
            Return t_Type.ToString
        End Get
    End Property
    Public ReadOnly Property toastPosition As String
        Get
            Return t_Position.ToString.Replace("_", "-")
        End Get
    End Property
    ''' <summary>
    ''' Retrieve Toast from Session and remove Sessionvalue afterwards
    ''' </summary>
    ''' <returns>Toast to be displayed</returns>
    ''' <remarks></remarks>
    Public Shared Function GetToast() As toastr
        If Not System.Web.HttpContext.Current.Session(SessionName) Is Nothing Then
            GetToast = CType(System.Web.HttpContext.Current.Session(SessionName), toastr)
            'System.Web.HttpContext.Current.Session.Remove(SessionName)
        Else
            GetToast = Nothing

        End If
    End Function
    'Public Shared Function GetToast1() As toastr
    '    If Not System.Web.HttpContext.Current.Session(SessionName) Is Nothing Then
    '        GetToast1 = CType(System.Web.HttpContext.Current.Session(SessionName), toastr)
    '        'System.Web.HttpContext.Current.Session.Remove(SessionName)
    '    Else
    '        GetToast1 = Nothing

    '    End If
    'End Function
    ''' <summary>
    ''' Store new toast in user session
    ''' </summary>
    ''' <remarks></remarks>
    Public Overloads Shared Sub SetToast(toast As toastr)
        System.Web.HttpContext.Current.Session(SessionName) = toast
    End Sub
    ''' <summary>
    ''' Store new info toast in user session to be shown in upper right
    ''' </summary>
    ''' <param name="text">Text to be shown in the toast</param>
    ''' <remarks></remarks>
    Public Overloads Shared Sub SetToast(text As String)
        System.Web.HttpContext.Current.Session(SessionName) = New toastr(text)
    End Sub
    ''' <summary>
    ''' Store new toast in user session to be shown in upper right
    ''' </summary>
    ''' <param name="text">Text to be shown in the toast</param>
    ''' <param name="type">Type of Toast. Could be info, success, warning or errors.</param>
    ''' <remarks></remarks>
    Public Overloads Shared Sub SetToast(text As String, type As ToastTypes)
        System.Web.HttpContext.Current.Session(SessionName) = New toastr(text, type)
    End Sub
    ''' <summary>
    ''' Store new info toast in user session
    ''' </summary>
    ''' <param name="text">Text to be shown in the toast</param>
    ''' <param name="position">Position of Toast. Several options available</param>
    ''' <remarks></remarks>
    Public Overloads Shared Sub SetToast(text As String, position As ToastPositions)
        System.Web.HttpContext.Current.Session(SessionName) = New toastr(text, position)
    End Sub
    ''' <summary>
    ''' Store new toast in user session
    ''' </summary>
    ''' <param name="text">Text to be shown in the toast</param>
    ''' <param name="type">Type of Toast. Could be info, success, warning or errors.</param>
    ''' <param name="position">Position of Toast. Several options available</param>
    ''' <remarks></remarks>
    Public Overloads Shared Sub SetToast(text As String, type As ToastTypes, position As ToastPositions)
        System.Web.HttpContext.Current.Session(SessionName) = New toastr(text, type, position)
    End Sub




End Class
