﻿Imports System.Data.SqlClient
Imports EkaDataNettier.Data
Imports EkaDataNettier.Entities
Public Class CDDBLL






    Shared Function GetSourceOffund(ByVal CIF As String) As String
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_AML_GetSourceoffund")
            objCommand.Parameters.Add(New SqlParameter("@cifno", CIF))
            Return DataRepository.Provider.ExecuteScalar(objCommand)
        End Using
    End Function

    Shared Function GetInvalidError(ByVal CIF As String) As Data.DataTable
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_AML_GetCDDInvalidValidation")
            objCommand.Parameters.Add(New SqlParameter("@cifno", CIF))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using
    End Function


    Shared Function getdataupdatingandtglopen(ByVal dataupdatingid As String) As Data.DataSet
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_AMl_GetTglBukaAndReasonCDD")
            objCommand.Parameters.Add(New SqlParameter("@dataupdatingid", dataupdatingid))
            Return DataRepository.Provider.ExecuteDataSet(objCommand)
        End Using
    End Function


    Shared Function GetMandatoryError(ByVal CIF As String) As Data.DataTable
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_AM_GetCDDMandatoryValidation")
            objCommand.Parameters.Add(New SqlParameter("@cifno", CIF))
            Return DataRepository.Provider.ExecuteDataSet(objCommand).Tables(0)
        End Using
    End Function

    Public Shared Function GetTotalSuspectScreening(currentPKParam As Long) As Long

        Dim dataSet As DataSet = Sahassa.AML.Commonly.ExecuteDataset("SELECT * FROM AML_Screening_Customer_Request_Result WHERE FK_AML_Screening_Customer_Request_ID = " & currentPKParam)

        If dataSet.Tables.Count = 0 OrElse dataSet.Tables(0).Rows.Count = 0 Then
            Return 0
        End If

        Return dataSet.Tables(0).Rows.Count

    End Function
End Class
